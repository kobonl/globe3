unit MenuGroupFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseFRM, Db, ActnList, dxBarDBNav, dxBar, StdCtrls, dxCntner, dxTL,
  dxDBCtrl, dxDBGrid, ExtCtrls, dxEditor, dxExEdtr, dxDBEdtr, dxDBELib,
  Dblup1a, ImgList, dxTLClms, Menus;

type
  TMenuGroupF = class(TGridBaseF)
    dxTreeList: TdxTreeList;
    dxTreeListPARENT_MENU_NUMBER: TdxTreeListColumn;
    dxTreeListMENU_NUMBER: TdxTreeListColumn;
    ImageList3: TImageList;
    Label1: TLabel;
    dxTreeListDESCRIPTIONIMG: TdxTreeListImageColumn;
    dxTreeListDESCRIPTION: TdxTreeListColumn;
    MainMenu: TMainMenu;
    miFile: TMenuItem;
    N2: TMenuItem;
    miExit: TMenuItem;
    miEdit: TMenuItem;
    miNewItem: TMenuItem;
    miNewSubItem: TMenuItem;
    miDelete: TMenuItem;
    N1: TMenuItem;
    miFullexpand: TMenuItem;
    miFullCollapse: TMenuItem;
    miView: TMenuItem;
    miShowGrid: TMenuItem;
    miShowHeader: TMenuItem;
    miShowLines: TMenuItem;
    miShowBands: TMenuItem;
    N6: TMenuItem;
    miFlat: TMenuItem;
    N4: TMenuItem;
    miSave: TMenuItem;
    dxTreeListVISIBLE_YN: TdxTreeListCheckColumn;
    dxTreeListEdit_YN: TdxTreeListCheckColumn;
    PopupMenu: TPopupMenu;
    piNewItem: TMenuItem;
    piNewSubItem: TMenuItem;
    piDelete: TMenuItem;
    N5: TMenuItem;
    PopupMenuEdit: TPopupMenu;
    MenuItemEdit: TMenuItem;
    MenuItemVisible: TMenuItem;
    MenuItem4: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormDestroy(Sender: TObject);
   
    procedure FormShow(Sender: TObject);
    procedure miNewItemClick(Sender: TObject);
    procedure miNewSubItemClick(Sender: TObject);
    procedure miDeleteClick(Sender: TObject);
    procedure dxTreeListEdited(Sender: TObject; Node: TdxTreeListNode);
    procedure miShowGridClick(Sender: TObject);
    procedure miShowHeaderClick(Sender: TObject);
    procedure miShowLinesClick(Sender: TObject);
    procedure miShowBandsClick(Sender: TObject);
    procedure miFlatClick(Sender: TObject);
    procedure miFullexpandClick(Sender: TObject);
    procedure miFullCollapseClick(Sender: TObject);
    procedure dxTreeListGetSelectedIndex(Sender: TObject;
      Node: TdxTreeListNode; var Index: Integer);
    procedure miSaveClick(Sender: TObject);
    procedure MenuItemEditClick(Sender: TObject);
    procedure MenuItemVisibleClick(Sender: TObject);
    procedure PopupMenuEditPopup(Sender: TObject);
    procedure miExitClick(Sender: TObject);
    procedure PopupMenuPopup(Sender: TObject);
  private
    { Private declarations }
    FGroupName: String;
    FSaveAllGroup: Boolean;
  public
    { Public declarations }
    property GroupName: String read FGroupName write FGroupName;
    procedure FillMenuTree(TreeList: TdxTreeList);
    procedure InsertNodeIntoTable(N: TdxTreeListNode; Parent_Menu, Seq_Index: Integer);
  end;

function MenuGroupF: TMenuGroupF;

implementation

{$R *.DFM}

uses
  UPimsConst,  UAdmConst, UAdmMessageRes, MenuGroupDMT, ListProcsFRM,
  UserGroupFRM, SystemDMT, UserGroupDMT;

var
  MenuGroupF_HND: TMenuGroupF;

function MenuGroupF: TMenuGroupF;
begin
  if (MenuGroupF_HND = nil) then
    MenuGroupF_HND := TMenuGroupF.Create(Application);
  Result := MenuGroupF_HND;
end;



procedure TMenuGroupF.FormCreate(Sender: TObject);
begin
  MenuGroupDM := CreateFormDM(TMenuGroupDM);
  inherited;
  FSaveAllGroup := False;
  dxTreeList.BandFont.Color := clWhite; // PIM-250
end;

procedure TMenuGroupF.FillMenuTree(TreeList: TdxTreeList);
const
  C0ParentField = 'PARENT_MENU_NUMBER';
  C1KeyField    = 'MENU_NUMBER';
  C2            = 'DESCRIPTION';
  C3            = 'VISIBLE_YN';
  C4            = 'EDIT_YN';
var
  Item: TdxTreeListNode;
  IDColumn: TdxTreeListColumn;
  ParentID: Variant;
  function FindRecursivNode(N: TdxTreelistNode; Index, Value: Integer): TdxTreeListNode ;
  var
    i: Integer;
  begin
    Result := Nil;
    if N.Values[Index] = Value then
    begin
      Result := N;
      exit;
    end;
    for i := 0 to N.Count - 1 do
    begin
      Result := FindRecursivNode(N.Items[I], Index, Value);
      if Result <> Nil then
        exit;
    end;
  end;

  function FindNode(Index, Value: Integer): TdxTreeListNode;
  var
    i: Integer;
  begin
    Result := Nil;
    for i := 0 to dxTreeList.Count -1 do
    begin
      if dxTreeList.Items[i].Values[Index] = Value then
      begin
        Result := dxTreeList.Items[i];
        exit;
      end;
      Result := FindRecursivNode(dxTreeList.Items[i], Index, Value);
      if Result <> Nil then
        exit;
    end;
  end;
begin

  Screen.Cursor := crHourGlass;
  MenuGroupDM.TableDetail.DisableControls;
  with TreeList do
    try
      IDColumn := ColumnByName(Name + C1KeyField);
      BeginUpdate;
      ClearNodes;
      MenuGroupDM.TableDetail.First;
      while not MenuGroupDM.TableDetail.Eof do
      begin
        ParentID := MenuGroupDM.TableDetail.FieldByName(C0ParentField).Text;
        Item:= FindNode(IDColumn.Index, ParentID);
        if Item = nil then
          Item := Add
        else
          Item := Item.AddChild;

        // other fields
        Item.Values[0] := MenuGroupDM.TableDetail.FieldByName(C0ParentField).Text;
        Item.Values[2] := MenuGroupDM.TableDetail.FieldByName(C1KeyField).Text;
        Item.Values[3] := MenuGroupDM.TableDetail.FieldByName(C2).Text;
        Item.Values[4] := MenuGroupDM.TableDetail.FieldByName(C3).Text;
        Item.Values[5] := MenuGroupDM.TableDetail.FieldByName(C4).Text;
       { Item.Values[IDColumn.Index] :=
          MenuGroupDM.TableDetail.FieldByName(C1KeyField).Text;}

        MenuGroupDM.TableDetail.Next;
      end;
      if TopNode <> nil then
        TopNode.Focused := True;
    finally
      EndUpdate;
      MenuGroupDM.TableDetail.EnableControls;
      Screen.Cursor := crDefault;
    end;
  TreeList.FullExpand;
end;

procedure TMenuGroupF.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  if miSave.Enabled then
  begin
    if DisplayMessage(SAdmSave, mtConfirmation, [mbYes, mbNo])= mrYes then
    begin
      miSaveClick(Sender);
    end;
  end;
  // Fill default with menu changed all existing user groups
  if FSaveAllGroup and (MenuGroupF.GroupName = ADMINGROUP) then
    UserGroupDM.FillDefaultMenu_AllUserGroups;
  Action := caFree;
  UserGroupF.SetGridForm(UserGroupF);
end;

procedure TMenuGroupF.FormDestroy(Sender: TObject);
begin
  inherited;
  MenuGroupF_HND := nil;
end;

procedure TMenuGroupF.FormShow(Sender: TObject);
begin
  inherited;
  MenuGroupF.Caption := MenuGroupF.Caption + ' - ' + MenuGroupF.GroupName;
  if (MenuGroupDM.TableDetail.FieldByName('GROUP_NAME').Value <> ADMINGROUP) then
  begin
    miNewItem.Enabled := False;
    miNewSubItem.Enabled := False;
    miDelete.Enabled := False;
    dxTreeList.Columns[3].DisableEditor := True;
    dxTreeList.Columns[2].DisableEditor := True;
    if(SystemDM.AplEditYN = CHECKEDVALUE) then
      dxTreeList.PopupMenu := PopupMenuEdit
    else
    begin
      dxTreeList.PopupMenu := Nil;
      miSave.Enabled:= False;
    end;
  end;
  if (MenuGroupDM.TableDetail.FieldByName('GROUP_NAME').Value = ADMINGROUP) then
  begin
    miNewItem.Enabled := True;
    miNewSubItem.Enabled := True;
    miDelete.Enabled := True;
    dxTreeList.Columns[2].DisableEditor := False;
    dxTreeList.Columns[3].DisableEditor := False;
    dxTreeList.PopupMenu := PopupMenu;
  end;
  FillMenuTree(dxTreeList);
  miSave.Enabled := False;
end;

procedure TMenuGroupF.miNewItemClick(Sender: TObject);
var
  NewItem : TdxTreeListNode;
  ParentValue: Integer;
begin
  inherited;
  if (dxTreeList.FocusedNode <> Nil) and
     (dxTreeList.FocusedNode.Parent <> Nil) then
  begin
    NewItem := dxTreeList.FocusedNode.Parent.AddChild;
    ParentValue := dxTreeList.FocusedNode.Parent.Values[2];
  end
  else
  begin
    NewItem := dxTreeList.Add;
    ParentValue := dxTreeList.FocusedNode.Values[0];
  end;
  //If Item is added successfull then make it visible
  if NewItem <> nil then NewItem.MakeVisible;

  NewItem.Values[0] := ParentValue;
  NewItem.Values[4] := CHECKEDVALUE;
  NewItem.Values[5] :=  CHECKEDVALUE;
  miSave.Enabled := True;
end;

procedure TMenuGroupF.miNewSubItemClick(Sender: TObject);
var
  NewItem : TdxTreeListNode;
begin
  Inherited;
  if dxTreeList.FocusedNode <> Nil then
  begin
    NewItem := dxTreeList.FocusedNode.AddChild;
    if NewItem <> nil then NewItem.MakeVisible;
    NewItem.Values[0] := dxTreeList.FocusedNode.Values[2];
    NewItem.Values[4] := CHECKEDVALUE;
    NewItem.Values[5] :=  CHECKEDVALUE;
  end;
  miSave.Enabled := True;
end;

procedure TMenuGroupF.miDeleteClick(Sender: TObject);
var
  OldNode, Node : TdxTreeListNode;
begin
  inherited;
  if (dxTreeList.FocusedNode.Values[3] = MenuDefaultItems[1]) or
    (dxTreeList.FocusedNode.Values[3] = MenuDefaultItems[2]) or
    (dxTreeList.FocusedNode.Values[3] = MenuDefaultItems[3]) or
    (dxTreeList.FocusedNode.Values[3] = MenuDefaultItems[4]) then
  begin
    DisplayMessage(SAdmNotDeleteMenuItem, mtError, [mbOk]);
    Exit;
  end;
  With dxTreeList Do
  //If there are the selected nodes then
  if (SelectedCount > 0) and
     (MessageBox(Self.Handle, 'Delete selected nodes?', 'Warning !',
        MB_ICONQUESTION or MB_OKCANCEL) <> IDCANCEL) then
  Begin
    //User chooses OK.
    //Save the previous node, to select it after deleting the selected nodes
    OldNode := FocusedNode;
    if OldNode <> Nil then OldNode := OldNode.GetPriorNode;
    //Freez control on delete operation to avoid flickers
    BeginUpdate;
    try
      //Delete all selected nodes
      While SelectedCount > 0 Do
      begin
        Node := SelectedNodes[0];
        if Node = OldNode then OldNode := Nil;
        Node.Free;
      end;
    finally
      //Make the the previous (saved before node) selected
      if (OldNode <> nil) then
      begin
        OldNode.Focused := True;
        OldNode.Selected := True;
        if OldNode.HasChildren = False then
        begin
          OldNode.Values[4] := CHECKEDVALUE;
          OldNode.Values[5] := CHECKEDVALUE;
        end;
      end;
      //Update control
      EndUpdate;
      miSave.Enabled := True;
    end;
  End;
   FSaveAllGroup := True; 
end;

procedure TMenuGroupF.dxTreeListEdited(Sender: TObject;
  Node: TdxTreeListNode);
 
begin
  inherited;
  if TdxInplaceTextEdit(dxTreeList.InplaceEditor).Modified then
     miSave.Enabled := True;
end;

procedure TMenuGroupF.miShowGridClick(Sender: TObject);
begin
  inherited;
  miShowGrid.Checked := not miShowGrid.Checked;
  dxTreeList.ShowGrid := miShowGrid.Checked;
end;

procedure TMenuGroupF.miShowHeaderClick(Sender: TObject);
begin
  inherited;
  miShowHeader.Checked := not miShowHeader.Checked;
  dxTreeList.ShowHeader := miShowHeader.Checked;
end;

procedure TMenuGroupF.miShowLinesClick(Sender: TObject);
begin
  inherited;
  miShowLines.Checked := not miShowLines.Checked;
  dxTreeList.ShowLines := miShowLines.Checked;
end;

procedure TMenuGroupF.miShowBandsClick(Sender: TObject);
begin
  inherited;
  miShowBands.Checked := not miShowBands.Checked;
  dxTreeList.ShowBands := miShowBands.Checked;
end;

procedure TMenuGroupF.miFlatClick(Sender: TObject);
begin
  inherited;
  with dxTreeList do
  begin
    if LookAndFeel = lfFlat then
      LookAndFeel := lfStandard
    else
      LookAndFeel := lfFlat;
    miFlat.Checked := (LookAndFeel = lfFlat);
  end;
end;

procedure TMenuGroupF.miFullexpandClick(Sender: TObject);
begin
  inherited;
  dxTreeList.FullExpand;
end;

procedure TMenuGroupF.miFullCollapseClick(Sender: TObject);
begin
  inherited;
  dxTreeList.FullCollapse;
end;

procedure TMenuGroupF.dxTreeListGetSelectedIndex(Sender: TObject;
  Node: TdxTreeListNode; var Index: Integer);
begin
  inherited;
  Index := Node.ImageIndex;
end;

procedure TMenuGroupF.InsertNodeIntoTable(N: TdxTreeListNode; Parent_Menu,
  Seq_Index: Integer);
var
  Expand, Edit: String;
begin
  if N.HasChildren then
  begin
    Expand := CHECKEDVALUE;
    Edit := UNCHECKEDVALUE;
  end
  else
  begin
    Expand := UNCHECKEDVALUE;
    Edit := N.Values[5];
  end;
  MenuGroupDM.InsertRecordInMenuGroup(Parent_Menu, N.Values[2], Seq_Index,
    N.Values[3], Expand, Edit, N.Values[4] );
end;

procedure TMenuGroupF.miSaveClick(Sender: TObject);
var
  Count, ParentValue: Integer;
procedure RecursivSaveTree( Node: TdxTreeListNode);
var
  Count: Integer;
  NewItem : TdxTreeListNode;
begin
  for count:= 0 to (Node.count - 1) do
  begin
    NewItem := Node.Items[Count];
    InsertNodeIntoTable(Node.Items[Count], Node.Values[2],
      Node.Items[Count].Index + 1);
    RecursivSaveTree(NewItem);
  end;
end;
begin
  inherited;
  dxTreeList.FullExpand;
  if MenuGroupF.GroupName = ADMINGROUP then
  begin
    MenuGroupDM.QueryDelete.ExecSQL;
    MenuGroupDM.TableGroupMenu.Refresh;
  end;
  ParentValue := MENU_PARENT;
  for Count := 0 to (dxTreeList.Count -1) do
  begin

    InsertNodeIntoTable(dxTreeList.Items[Count], ParentValue,
      dxTreeList.Items[Count].Index + 1);

    RecursivSaveTree(dxTreeList.Items[Count]);
  end;

  miSave.Enabled := False;
  FSaveAllGroup := True;
end;

procedure TMenuGroupF.MenuItemEditClick(Sender: TObject);
begin
  inherited;
  miSave.Enabled := True;
  if dxTreeList.FocusedNode <> Nil then
  begin
    if not dxTreeList.FocusedNode.HasChildren then
    begin
      MenuItemEdit.Checked :=
        not (dxTreeList.FocusedNode.Values[5] = CHECKEDVALUE);

      if MenuItemEdit.Checked then
      begin
        dxTreeList.FocusedNode.Values[5] := CHECKEDVALUE;
        dxTreeList.FocusedNode.Values[4] := CHECKEDVALUE;
      end
      else
        dxTreeList.FocusedNode.Values[5] := UNCHECKEDVALUE;
    end;    
  end;
end;

procedure TMenuGroupF.MenuItemVisibleClick(Sender: TObject);
  procedure RecursivSetVisible(N: TdxTreeListNode);
  var
    i: Integer;
  begin
    if N <> Nil then
      for i:= 0 to N.Count -1 do
      begin
        N.Items[I].Values[4] := UNCHECKEDVALUE;
        N.Items[I].Values[5] := UNCHECKEDVALUE;
        RecursivSetVisible(N.Items[i]);
      end;
  end;
  procedure RecursivParentSetVisible(N: TdxTreeListNode);
  begin
    if (N <> Nil) then
    begin
      if (N.Values[4] = CHECKEDVALUE) and (N.Parent <> Nil) then
      begin
        N.Parent.Values[4] := CHECKEDVALUE;
        RecursivParentSetVisible(N.Parent);
      end;
    end;
  end;
begin
  inherited;
  miSave.Enabled := True;
  MenuItemVisible.Checked :=
   not (dxTreeList.FocusedNode.Values[4] = CHECKEDVALUE);
  if dxTreeList.FocusedNode <> Nil then
  begin
    if MenuItemVisible.Checked then
      dxTreeList.FocusedNode.Values[4] := CHECKEDVALUE
    else
    begin
      dxTreeList.FocusedNode.Values[4] := UNCHECKEDVALUE;
      dxTreeList.FocusedNode.Values[5] := UNCHECKEDVALUE;
    end;
    if (dxTreeList.FocusedNode.HasChildren) and
      (dxTreeList.FocusedNode.Values[4] = UNCHECKEDVALUE) then
      RecursivSetVisible(dxTreeList.FocusedNode);
    RecursivParentSetVisible(dxTreeList.FocusedNode);
  end;
end;

procedure TMenuGroupF.PopupMenuEditPopup(Sender: TObject);
begin
  inherited;
  MenuItemVisible.Checked := (dxTreeList.FocusedNode.Values[4] = CHECKEDVALUE);
  MenuItemEdit.Checked := (dxTreeList.FocusedNode.Values[5] = CHECKEDVALUE);

  if (SystemDM.CurrentLoginUser <> ADMINUSER) and
     (SystemDM.CurrentLoginUser = GroupName) and
    (MenuDefaultItems[2] = dxTreeList.FocusedNode.Values[3]) then
  begin
    MenuItemEdit.Enabled := False;
    MenuItemVisible.Enabled := False;
    exit;
  end;
  MenuItemEdit.Enabled := True;
  MenuItemVisible.Enabled := True;
  if dxTreeList.FocusedNode.HasChildren then
    MenuItemEdit.Enabled := False;
end;

procedure TMenuGroupF.miExitClick(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TMenuGroupF.PopupMenuPopup(Sender: TObject);
begin
  inherited;
  piDelete.Enabled := True;
  if (dxTreeList.FocusedNode.Values[3] = MenuDefaultItems[1]) or
    (dxTreeList.FocusedNode.Values[3] = MenuDefaultItems[2]) or
    (dxTreeList.FocusedNode.Values[3] = MenuDefaultItems[3]) or
    (dxTreeList.FocusedNode.Values[3] = MenuDefaultItems[4]) then
     piDelete.Enabled := False;
end;

end.

