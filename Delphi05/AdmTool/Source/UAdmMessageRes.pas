unit UAdmMessageRes;

interface

uses
  SysUtils, Dialogs;

resourcestring

{$IFDEF PIMSDUTCH}
  SAplPasswordConfirm =
    'Het wachtwoord is niet gelijk aan het bevestigingswachtwoord';

  SAdmNotDeleteUser = 'Deze gebruiker kan niet worden verwijderd';
  SAdmNotDeleteGroup = 'Deze groep kan niet worden verwijderd';
  SAdmNotDeleteMenuItem = 'Deze node kan niet worden verwijderd';

  SAdmDescAdminGroup = 'ABS Administratie groep';
  SAdmMenu_AdmTool = 'Administratie';
  SAdmMenu_Pims = 'Pims';
  SAdmMenu_TreePims = 'Applicaties';
  SAdmMenu_Production_Screen = 'Productie scherm';
  SAdmConfig = 'Configuratie';
  SAdmMenu = 'Menu';
  SAdmSave = 'Wilt u de laatste wijzigingen opslaan ?';
  SAdmCopyFinished = 'Kopieer-actie is klaar!';
  SAdmSelectOtherUserGroup = 'Kies a.u.b. een andere gebruikersgroep';
  SAdmTeamSelection =
   'Team selectie is verwijderd. Alle teams zijn beschikbaar voor de gebruiker';
  SAdmSaveBefore = 'Sla a.u.b. eerst de laatste wijzigingen op!';

{$ELSE}
  {$IFDEF PIMSFRENCH}
  SAplPasswordConfirm =
    'The password does not match the confirmation password';

  SAdmNotDeleteUser = 'This user cannot be deleted';
  SAdmNotDeleteGroup = 'This group cannot be deleted';
  SAdmNotDeleteMenuItem = 'This node cannot be deleted';

  SAdmDescAdminGroup = 'ABS Administrator group';
  SAdmMenu_AdmTool = 'Administrator';
  SAdmMenu_Pims = 'Pims';
  SAdmMenu_TreePims = 'Applications';
  SAdmMenu_Production_Screen = 'Production screen';
  SAdmConfig = 'Config';
  SAdmMenu = 'Menu';
  SAdmSave = 'Do you want to save last changes ?';
  SAdmCopyFinished = 'Copy is finished!';
  SAdmSelectOtherUserGroup = 'Please select other user group';
  SAdmTeamSelection =
   'Team selection is deleted. All teams are available for the user';
  SAdmSaveBefore = 'Please first save last changes!';

{$ELSE}
  {$IFDEF PIMSGERMAN}
  SAplPasswordConfirm =
    'Das Passwort stimmt nicht mit dem Best�tigungspasswort �berein';

  SAdmNotDeleteUser = 'Dieser Benutzer kann nicht gel�scht werden';
  SAdmNotDeleteGroup = 'Diese Gruppe kann nicht gel�scht werden';
  SAdmNotDeleteMenuItem = 'Dieser Knoten kann nicht gel�scht werden';

  SAdmDescAdminGroup = 'ABS Administratorgruppe';
  SAdmMenu_AdmTool = 'Administrator';
  SAdmMenu_Pims = 'Pims';
  SAdmMenu_TreePims = 'Anwendungen';
  SAdmMenu_Production_Screen = 'Produktionsschirm';
  SAdmConfig = 'Config';
  SAdmMenu = 'Men�';
  SAdmSave = 'M�chten Sie die letzten �nderungen speichern?';
  SAdmCopyFinished = 'Kopieren ist beendet!';
  SAdmSelectOtherUserGroup = 'Bitte andere Benutzergruppe ausw�hlen';
  SAdmTeamSelection =
   'Teamauswahl wird gel�scht. Alle Teams stehen dem Benutzer zur Verf�gung.';
  SAdmSaveBefore = 'Bitte vorher speichern!';

{$ELSE}
  {$IFDEF PIMSDANISH}
  SAplPasswordConfirm =
    'Adgangskoden matcher ikke bekr�ftelsen adgangskode';

  SAdmNotDeleteUser = 'Denne bruger kan ikke slettes';
  SAdmNotDeleteGroup = 'Denne gruppe kan ikke slettes';
  SAdmNotDeleteMenuItem = 'Denne node kan ikke slettes';

  SAdmDescAdminGroup = 'ABS Administrator gruppe';
  SAdmMenu_AdmTool = 'Administrator';
  SAdmMenu_Pims = 'Pims';
  SAdmMenu_TreePims = 'Applikationer';
  SAdmMenu_Production_Screen = 'Produktion sk�rm';
  SAdmConfig = 'Konfiguration';
  SAdmMenu = 'Menu';
  SAdmSave = '�nsker du at spare seneste �ndringer?';
  SAdmCopyFinished = 'Kopier er f�rdig!';
  SAdmSelectOtherUserGroup = 'V�lg anden brugergruppe';
  SAdmTeamSelection =
   'Team valg slettes. Alle hold st�r til r�dighed for brugeren';
  SAdmSaveBefore = 'Venligst f�rst gemme seneste �ndringer!';

{$ELSE}
  {$IFDEF PIMSJAPANESE}
  SAplPasswordConfirm =
    'The password does not match the confirmation password';

  SAdmNotDeleteUser = 'This user cannot be deleted';
  SAdmNotDeleteGroup = 'This group cannot be deleted';
  SAdmNotDeleteMenuItem = 'This node cannot be deleted';

  SAdmDescAdminGroup = 'ABS Administrator group';
  SAdmMenu_AdmTool = 'Administrator';
  SAdmMenu_Pims = 'Pims';
  SAdmMenu_TreePims = 'Applications';
  SAdmMenu_Production_Screen = 'Production screen';
  SAdmConfig = 'Config';
  SAdmMenu = 'Menu';
  SAdmSave = 'Do you want to save last changes ?';
  SAdmCopyFinished = 'Copy is finished!';
  SAdmSelectOtherUserGroup = 'Please select other user group';
  SAdmTeamSelection =
   'Team selection is deleted. All teams are available for the user';
  SAdmSaveBefore = 'Please first save last changes!';

{$ELSE}
  {$IFDEF PIMSCHINESE}

  SAplPasswordConfirm =
    '�������';

  SAdmNotDeleteUser = '�޷�ɾ�����û�';
  SAdmNotDeleteGroup = '�޷�ɾ����';
  SAdmNotDeleteMenuItem = '�޷�ɾ���˽ڵ�';

  SAdmDescAdminGroup = 'ABS ����Ա��';
  SAdmMenu_AdmTool = '����Ա';
  SAdmMenu_Pims = '������Ϣ������';
  SAdmMenu_TreePims = 'Ӧ������';
  SAdmMenu_Production_Screen = '������';
  SAdmConfig = '����';
  SAdmMenu = '�˵�';
  SAdmSave = '�Ƿ񱣴����±�� ?';
  SAdmCopyFinished = '�������!';
  SAdmSelectOtherUserGroup = '��ѡ�������û���';
  SAdmTeamSelection =
   '����ѡ����ɾ���� �û���ѡ�����ж���';
  SAdmSaveBefore = '���ȱ������±��!';

{$ELSE}
  {$IFDEF PIMSNORWEGIAN}


  SAplPasswordConfirm =
    'Passordet samsvarer ikke med det bekreftede passordet';

  SAdmNotDeleteUser = 'Denne brukeren kan ikke slettes';
  SAdmNotDeleteGroup = 'Denne gruppen kan ikke slettes';
  SAdmNotDeleteMenuItem = 'Denne noden kan ikke slettes';

  SAdmDescAdminGroup = 'ABS Administrator gruppe';
  SAdmMenu_AdmTool = 'Administrator';
  SAdmMenu_Pims = 'Pims';
  SAdmMenu_TreePims = 'Applikasjoner';
  SAdmMenu_Production_Screen = 'Produksjons skjerm';
  SAdmConfig = 'Konfigurasjon';
  SAdmMenu = 'Meny';
  SAdmSave = '�nsker du � lagre de siste endringene?';
  SAdmCopyFinished = 'Kopieringen er fullf�rt!';
  SAdmSelectOtherUserGroup = 'Vennligst velg en annen brukergruppe';
  SAdmTeamSelection =
   'Gruppe valg er slettet. Alle grupper er tilgjengelige for brukeren';
  SAdmSaveBefore = 'Vennligst lagre siste endringer!';

{$ELSE}
// Default ENGLISH

  SAplPasswordConfirm =
    'The password does not match the confirmation password';

  SAdmNotDeleteUser = 'This user cannot be deleted';
  SAdmNotDeleteGroup = 'This group cannot be deleted';
  SAdmNotDeleteMenuItem = 'This node cannot be deleted';

  SAdmDescAdminGroup = 'ABS Administrator group';
  SAdmMenu_AdmTool = 'Administrator';
  SAdmMenu_Pims = 'Pims';
  SAdmMenu_TreePims = 'Applications';
  SAdmMenu_Production_Screen = 'Production screen';
  SAdmConfig = 'Config';
  SAdmMenu = 'Menu';
  SAdmSave = 'Do you want to save last changes ?';
  SAdmCopyFinished = 'Copy is finished!';
  SAdmSelectOtherUserGroup = 'Please select other user group';
  SAdmTeamSelection =
   'Team selection is deleted. All teams are available for the user';
  SAdmSaveBefore = 'Please first save last changes!';

            {$ENDIF}
          {$ENDIF}
        {$ENDIF}
      {$ENDIF}
    {$ENDIF}
  {$ENDIF}
{$ENDIF}


implementation

end.
