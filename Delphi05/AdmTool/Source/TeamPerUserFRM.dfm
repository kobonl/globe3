inherited TeamPerUserF: TTeamPerUserF
  Left = 247
  Top = 149
  HorzScrollBar.Range = 0
  VertScrollBar.Range = 0
  BorderStyle = bsDialog
  Caption = 'Teams per user'
  ClientHeight = 445
  ClientWidth = 626
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [1]
    Left = 8
    Top = 20
    Width = 58
    Height = 13
    Caption = 'Group name'
  end
  object Label3: TLabel [2]
    Left = 8
    Top = 60
    Width = 51
    Height = 13
    Caption = 'User name'
  end
  inherited pnlMasterGrid: TPanel
    Width = 626
    Height = 71
    TabOrder = 0
    Visible = False
    inherited spltMasterGrid: TSplitter
      Top = 67
      Width = 624
    end
    inherited dxMasterGrid: TdxDBGrid
      Width = 624
      Height = 66
      Visible = False
    end
  end
  inherited pnlDetail: TPanel
    Top = 272
    Width = 626
    Height = 173
    TabOrder = 3
    object GroupBoxDetail: TGroupBox
      Left = 1
      Top = 1
      Width = 624
      Height = 171
      Align = alLeft
      Caption = 'Users'
      TabOrder = 0
      object Label2: TLabel
        Left = 8
        Top = 68
        Width = 26
        Height = 13
        Caption = 'Team'
      end
      object Label4: TLabel
        Left = 8
        Top = 28
        Width = 51
        Height = 13
        Caption = 'User name'
      end
      object dxDBLookupEditTeam: TdxDBLookupEdit
        Tag = 1
        Left = 81
        Top = 68
        Width = 232
        Style.BorderStyle = xbsSingle
        TabOrder = 1
        DataField = 'TEAMDESC'
        DataSource = UserDM.DataSourceTeamPerUser
        DropDownRows = 6
        ListFieldName = 'DESCRIPTION;TEAM_CODE'
      end
      object DBEdit1: TDBEdit
        Tag = 1
        Left = 83
        Top = 28
        Width = 230
        Height = 19
        CharCase = ecUpperCase
        Ctl3D = False
        DataField = 'USER_NAME'
        DataSource = UserDM.DataSourceTeamPerUser
        Enabled = False
        ParentCtl3D = False
        TabOrder = 0
      end
    end
  end
  inherited pnlDetailGrid: TPanel
    Tag = 1
    Top = 97
    Width = 626
    Height = 175
    inherited spltDetail: TSplitter
      Top = 171
      Width = 624
    end
    inherited dxDetailGrid: TdxDBGrid
      Tag = 1
      Width = 624
      Height = 170
      Bands = <
        item
          Alignment = taLeftJustify
          Caption = 'Teams per user'
        end>
      DefaultLayout = False
      KeyField = 'USER_NAME'
      Color = clWhite
      ParentFont = False
      BandColor = clInactiveBorder
      DataSource = UserDM.DataSourceTeamPerUser
      ShowBands = True
      object dxDetailGridColumnUser: TdxDBGridColumn
        Caption = 'User name'
        DisableEditor = True
        Width = 63
        BandIndex = 0
        RowIndex = 0
        FieldName = 'USER_NAME'
      end
      object dxDetailGridColumnTeam: TdxDBGridLookupColumn
        Caption = 'Team code'
        Width = 116
        BandIndex = 0
        RowIndex = 0
        FieldName = 'TEAMLU'
      end
      object dxDetailGridColumnDesc: TdxDBGridColumn
        Caption = 'Team description'
        DisableEditor = True
        BandIndex = 0
        RowIndex = 0
        FieldName = 'TeamDesc'
      end
    end
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = False
        WholeRow = False
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <
          item
            Item = dxBarButtonFirst
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonPrior
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonNext
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonLast
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarBDBNavInsert
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavDelete
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavPost
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavCancel
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonEditMode
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonRecordDetails
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSort
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSearch
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCustCol
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonShowGroup
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExpand
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCollapse
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonResetColumns
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonExportAllHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportAllXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    DockControlHeights = (
      0
      0
      26
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
  end
  inherited dsrcActive: TDataSource
    Left = 56
    Top = 64
  end
end
