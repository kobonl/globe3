(*
 * Title:        FSideMenuUnitMaintenance
 * Description:  Form/Class which shows a Side-Menu with menu-options
 *               at the left.
 * Copyright:    Copyright (c) 2001
 * Company:      ABS
 * Author        Carmen Panturu
 * Version       1.0
 MRA: 22-JUL-2016 PIM-12
 - New look
*)
unit SideMenuUnitMaintenanceAdmFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ImgList, Menus, StdCtrls, ExtCtrls, dxsbar, ComCtrls, {UGlobals,}
  OneSolar, ActnList, dxBar, dxBarDBNav, dxDBGrid, ComDrvN, UMenuOptions,
  SideMenuBaseFRM, UPimsConst, jpeg;

type
  TSideMenuUnitMaintenanceAdmF = class(TSideMenuBaseF)
    GoActionList: TActionList;
    GoUserGroup: TAction;
    GoUsers: TAction;
    GoMenuGroup: TAction;
    dxSideBar: TdxSideBar;
    procedure dxSideBarItemClick(Sender: TObject; Item: TdxSideBarItem);
    procedure dxSideBarChangeActiveGroup(Sender: TObject);

    procedure FormCreate(Sender: TObject);
    procedure GoPSUserGroupsexecute(Sender: TObject);
    procedure GoUserExecute(Sender: TObject);
    procedure GoMenuGroupExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    FLastMenuOption: Integer;
    procedure SwitchSideMenuGroup(const GroupIndex: Integer);
    function MenuOptionInMenuSet(const AOption: Integer;
      const AMenuSet: TPimsMenuSet): Boolean;
    procedure SwitchToMenuOption(const PimsMenuOption: Integer);
    procedure MakeAllGroupsVisible;
    procedure UpdateSideGroups(const PimsMenuOption: Integer);
    procedure UpdateTopMenu(const PimsMenuOption: Integer);
    property  LastMenuOption: Integer
      read FLastMenuOption write FLastMenuOption;
    procedure SwitchSelectedItem(const ATag: Integer);

  public
    { Public declarations }
    //FPrevTag: Integer;
    procedure SwitchForm(const AForm: TForm; const ATag: Integer);

    procedure ShowAdminUserGroup;
  end;

var
  SideMenuUnitMaintenanceAdmF: TSideMenuUnitMaintenanceAdmF;

implementation

{$R *.DFM}

uses
  GridBaseFRM, GridBaseDMT,
  UserGroupFRM, SystemDMT, UPimsMessageRes, UserFRM,
  MenuGroupFRM;


procedure TSideMenuUnitMaintenanceAdmF.SwitchSelectedItem(
  const ATag: Integer);
var
  MyDxSideBarItem: TdxSideBarItem;
  I, J: Integer;
begin
  MyDxSideBarItem := nil;
  for I:=0 to dxSideBar.GroupCount - 1 do
  begin
    for J:=0 to dxSideBar.Groups.Items[I].ItemCount - 1 do
    begin
      MyDxSideBarItem := dxSideBar.Groups.Items[I].Items[J];
      if (MyDxSideBarItem <> nil) then
        if MyDxSideBarItem.Tag = ATag then
          Break;
    end;
    if (MyDxSideBarItem <> nil) then
      if MyDxSideBarItem.Tag = ATag then
        Break;
  end;
  if (MyDxSideBarItem <> nil) then
    if MyDxSideBarItem.Tag = ATag then
      dxSideBar.SelectedItem := MyDxSideBarItem;
end;

function TSideMenuUnitMaintenanceAdmF.MenuOptionInMenuSet(
  const AOption: Integer;
  const AMenuSet: TPimsMenuSet): Boolean;
begin
  Result := (AOption in AMenuSet);
end;

// Actions needed when user has selected an option
// in the Side-Menu.
procedure TSideMenuUnitMaintenanceAdmF.dxSideBarItemClick(Sender: TObject;
  Item: TdxSideBarItem);
begin
  inherited;
  case Item.Tag of
    0                              : (Item.ItemObject as TdxBarButton).Click;
    MENU_ITEM_USERGROUP            : GoPSUserGroupsexecute(Sender);
    MENU_ITEM_USER                 : GoUserExecute(Sender);
//  MENU_ITEM_MENUGROUP            : GoMenuGroupExecute(Sender);
  end;
end;

procedure TSideMenuUnitMaintenanceAdmF.SwitchSideMenuGroup(
  const GroupIndex: Integer);
begin
  dxSideBar.ActiveGroup := dxSideBar.Groups[GroupIndex];
  dxSideBar.Repaint;
end;

procedure TSideMenuUnitMaintenanceAdmF.SwitchToMenuOption(
  const PimsMenuOption: Integer);
begin
  UpdateTopMenu(PimsMenuOption);
  UpdateSideGroups(PimsMenuOption);
  Update;
  Show;
end;

procedure TSideMenuUnitMaintenanceAdmF.MakeAllGroupsVisible;
var
  I: Integer;
begin
  for I := 0 to dxSideBar.GroupCount - 1 do
    dxSideBar.Groups[I].Visible := True;
end;

procedure TSideMenuUnitMaintenanceAdmF.UpdateSideGroups(
  const PimsMenuOption: Integer);
var
  GroupIndex: Integer;
begin
  dxSideBar.OnChangeActiveGroup := nil;
  dxSideBar.IsMakingUpdate := True;
  MakeAllGroupsVisible;
  for GroupIndex := 0 to dxSideBar.GroupCount - 1 do
    case PimsMenuOption of
      MENU_OPTION_USER : dxSideBar.Groups[GroupIndex].Visible :=
        MenuOptionInMenuSet(GroupIndex, MENU_GROUPS_ADMIN);
    end; { case }
  dxSideBar.IsMakingUpdate := False;
  dxSideBar.OnChangeActiveGroup := dxSideBarChangeActiveGroup;
end;

procedure TSideMenuUnitMaintenanceAdmF.UpdateTopMenu(
  const PimsMenuOption: Integer);
var
  ShowMenuOption: Boolean;
  CategoryIndex: Integer;
begin
  ShowMenuOption := False;
  dxBarManBase.LockUpdate := True;
  for CategoryIndex := 7 to dxBarManBase.Categories.Count - 1 do
  begin
    case PimsMenuOption of
      MENU_OPTION_USER : ShowMenuOption :=
        MenuOptionInMenuSet((CategoryIndex - 7), MENU_GROUPS_ADMIN);
     end; { case }
    if ShowMenuOption then
      dxBarManBase.CategoryItemsVisible[CategoryIndex] := ivAlways
    else
      dxBarManBase.CategoryItemsVisible[CategoryIndex] := ivNever;
  end;
  dxBarManBase.LockUpdate := False;
end;

procedure TSideMenuUnitMaintenanceAdmF.SwitchForm(const AForm: TForm;
  const ATag: Integer);
begin
  SwitchSelectedItem(ATag);
  InsertForm := AForm;
  if  dxSideBar.SelectedItem.CustomData <> '' then
    (AForm As TGridBaseF).Form_Edit_YN := dxSideBar.SelectedItem.CustomData;
  AForm.Show;
end;

procedure TSideMenuUnitMaintenanceAdmF.GoPSUserGroupsexecute(Sender: TObject);
begin
  inherited;
  SwitchSideMenuGroup(GROUP_ADMIN_USER);
  SwitchForm(UserGroupF, MENU_ITEM_USERGROUP);
end;

procedure TSideMenuUnitMaintenanceAdmF.GoUserExecute(Sender: TObject);
begin
  inherited;
  SwitchSideMenuGroup(GROUP_ADMIN_USER);
  SwitchForm(UserF, MENU_ITEM_USER);
end;

procedure TSideMenuUnitMaintenanceAdmF.FormCreate(Sender: TObject);
begin
  inherited;
  // MR:23-01-2008 RV002: Also show ComputerName.
  Caption := SystemDM.DatabaseServerName + ' - Admin Tool' + ' - ' +
    SystemDM.CurrentComputerName;  
  LastMenuOption := 999;
  dxSideBar.Color := clWhite; // clPimsBlue; // 20014450.50 / PIM-12 // PIM-250
  dxSideBar.ItemFont.Color := clDarkRed; // PIM-250
end;

// Change the Side-Menu-Bar to the correct group.
procedure TSideMenuUnitMaintenanceAdmF.dxSideBarChangeActiveGroup(
  Sender: TObject);
begin
  inherited;
// set correct insertform
  if InsertForm <> Nil then
    ActiveTables((InsertForm as TGridBaseF).GridFormDM, False);
  case (Sender as TdxSideBar).ActiveGroup.Index of
    GROUP_ADMIN_USER: SwitchForm(UserF, MENU_ITEM_USERGROUP);
  end; { case }
// focus grid if desired
  case (Sender as TdxSideBar).ActiveGroup.Index of
    GROUP_ADMIN_USER  :
      begin
        with (InsertForm as TGridBaseF) do
        begin
          if (ActiveGrid is TdxDBGrid) then
          begin
            if not (ActiveGrid as TdxDBGrid).DataSource.DataSet.Active then
              (ActiveGrid as TdxDBGrid).DataSource.DataSet.Active := True;
            ActiveGrid.SetFocus;
          end;
        end; { with }
      end;
  end; { case }
end;

procedure TSideMenuUnitMaintenanceAdmF.ShowAdminUserGroup;
begin
  Hide;
  SwitchToMenuOption(MENU_OPTION_USER);
  SwitchForm(UserF, MENU_ITEM_USERGROUP);
  InsertForm := UserGroupF;
  SwitchSideMenuGroup(GROUP_ADMIN_USER);
end;

procedure TSideMenuUnitMaintenanceAdmF.GoMenuGroupExecute(Sender: TObject);
begin
  inherited;
  SwitchSideMenuGroup(GROUP_ADMIN_USER);
  InsertForm := MenuGroupF;
  SwitchSelectedItem(MENU_ITEM_MENUGROUP);
end;

procedure TSideMenuUnitMaintenanceAdmF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  // MR:28-02-2005 This must be done, otherwise an 'access violation'
  // will happen during closing of the application.
  NilInsertForm;
end;

end.
