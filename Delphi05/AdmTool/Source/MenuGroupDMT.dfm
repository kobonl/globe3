inherited MenuGroupDM: TMenuGroupDM
  OldCreateOrder = True
  Left = 285
  Top = 161
  Height = 479
  Width = 741
  inherited TableDetail: TTable
    DatabaseName = 'Pims'
    Filtered = True
    OnFilterRecord = TableDetailFilterRecord
    IndexFieldNames = 'GROUP_NAME;PARENT_MENU_NUMBER;SEQ_NUMBER'
    MasterSource = nil
    TableName = 'PIMSMENUGROUP'
    Top = 124
    object TableDetailGROUP_NAME: TStringField
      FieldName = 'GROUP_NAME'
      Required = True
      Size = 31
    end
    object TableDetailMENU_NUMBER: TIntegerField
      FieldName = 'MENU_NUMBER'
      Required = True
    end
    object TableDetailVISIBLE_YN: TStringField
      FieldName = 'VISIBLE_YN'
      Size = 1
    end
    object TableDetailEDIT_YN: TStringField
      FieldName = 'EDIT_YN'
      Size = 1
    end
    object TableDetailEXPAND_YN: TStringField
      FieldName = 'EXPAND_YN'
      Size = 1
    end
    object TableDetailPARENT_MENU_NUMBER: TIntegerField
      FieldName = 'PARENT_MENU_NUMBER'
      Required = True
    end
    object TableDetailDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 60
    end
    object TableDetailSEQ_NUMBER: TIntegerField
      FieldName = 'SEQ_NUMBER'
      Required = True
    end
    object TableDetailCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
      Required = True
    end
    object TableDetailMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
      Required = True
    end
    object TableDetailMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
  end
  inherited DataSourceDetail: TDataSource
    Left = 200
    Top = 124
  end
  inherited TableExport: TTable
    DatabaseName = 'Pims'
  end
  inherited DataSourceExport: TDataSource
    DataSet = TableExport
  end
  object QueryDelete: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    DataSource = DataSourceDetail
    SQL.Strings = (
      'DELETE FROM PIMSMENUGROUP '
      'WHERE GROUP_NAME = :GROUP_NAME'
      '')
    Left = 208
    Top = 200
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'GROUP_NAME'
        ParamType = ptUnknown
      end>
  end
  object TableGroupMenu: TTable
    BeforePost = DefaultBeforePost
    OnNewRecord = DefaultNewRecord
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    IndexFieldNames = 'GROUP_NAME;MENU_NUMBER'
    TableName = 'PIMSMENUGROUP'
    Left = 304
    Top = 48
  end
end
