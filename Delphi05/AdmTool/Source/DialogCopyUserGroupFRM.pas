unit DialogCopyUserGroupFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DialogSelectionFRM, ComCtrls, StdCtrls, Buttons, ExtCtrls, dxCntner,
  dxEditor, dxExEdtr, dxEdLib, Dblup1a, Db, DBTables;

type
  TDialogCopyUserGroupF = class(TDialogSelectionF)
    GroupBoxCopyFrom: TGroupBox;
    Label1: TLabel;
    cmbPlusGroupFrom: TComboBoxPlus;
    Label2: TLabel;
    EditToUserGroup: TEdit;
    procedure FormShow(Sender: TObject);

 
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnOkClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FGroup: String;
    procedure CopyUserGroup;
  end;

var
  DialogCopyUserGroupF: TDialogCopyUserGroupF;

implementation

{$R *.DFM}
uses ListProcsFRM, SystemDMT, UserGroupDMT, UAdmMessageRes, UPimsMessageRes;

procedure TDialogCopyUserGroupF.FormShow(Sender: TObject);
begin
  inherited;
  ListProcsF.FillComboBoxMaster(UserGroupDM.TablePimsGroup, 'GROUP_NAME',
    True, cmbPlusGroupFrom);
  EditToUserGroup.Text := DialogCopyUserGroupF.FGroup + ' | ' +
    UserGroupDM.TableDetail.FieldByName('DESCRIPTION').AsString;
end;

procedure TDialogCopyUserGroupF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  cmbPlusGroupFrom.ClearGridData;

end;

procedure TDialogCopyUserGroupF.btnOkClick(Sender: TObject);
begin
  inherited;
  if GetStrValue(cmbPlusGroupFrom.Value) = DialogCopyUserGroupF.FGroup then
  begin
    DisplayMessage(SCopySameValues, mtInformation, [mbOk]);
    Exit;
  end;
  CopyUserGroup;
  DisplayMessage(SAdmCopyFinished, mtInformation, [mbOk]);
  DialogCopyUserGroupF.Close;
   
end;

procedure TDialogCopyUserGroupF.CopyUserGroup;
var
//  Result, Empl: Integer;
  InsertGroup: Boolean;
  SelectStr, EditYN, VisibleYN: String;
  CurrentDate: TDateTime;
begin

  CurrentDate := Now;
  //check if there are items in group
  SelectStr:= 
    'SELECT ' +
    '  MENU_NUMBER, PARENT_MENU_NUMBER, SEQ_NUMBER, DESCRIPTION, ' +
    '  EXPAND_YN, PARENT_MENU_NUMBER, EDIT_YN, VISIBLE_YN ' +
    'FROM ' +
    '  PIMSMENUGROUP ' +
    'WHERE ' +
    '  GROUP_NAME = ''' + FGroup + '''';
  OpenSql(UserGroupDM.QuerySelect, SelectStr);
  InsertGroup := UserGroupDM.QuerySelect.IsEmpty;
  //
  SelectStr:= 
    'SELECT ' +
    '  MENU_NUMBER, PARENT_MENU_NUMBER, SEQ_NUMBER, DESCRIPTION, ' +
    '  EXPAND_YN, PARENT_MENU_NUMBER, EDIT_YN, VISIBLE_YN '+
    'FROM ' +
    '  PIMSMENUGROUP ' +
    'WHERE ' +
    '  GROUP_NAME = ''' +
    GetStrValue(cmbPlusGroupFrom.Value) + '''';
  OpenSql(UserGroupDM.QuerySelect, SelectStr);

  UserGroupDM.QuerySelect.First;
  while (not UserGroupDM.QuerySelect.Eof) do
  begin
    EditYN := UserGroupDM.QuerySelect.FieldByName('EDIT_YN').AsString;
    VisibleYN := UserGroupDM.QuerySelect.FieldByName('VISIBLE_YN').AsString;
    if InsertGroup then
      SelectStr:= 
        'INSERT INTO PIMSMENUGROUP (GROUP_NAME, MENU_NUMBER, VISIBLE_YN, ' +
        'EDIT_YN, EXPAND_YN, PARENT_MENU_NUMBER, DESCRIPTION, SEQ_NUMBER, ' +
        'CREATIONDATE, MUTATIONDATE, MUTATOR) ' +
	   '	VALUES (:GROUP_NAME, :MENU_NUMBER, :VISIBLE_YN, ' +
        ':EDIT_YN, :EXPAND_YN, :PARENT_MENU_NUMBER, :DESCRIPTION, :SEQ_NUMBER, ' +
        ':CREATIONDATE, :MUTATIONDATE, :MUTATOR) '
    else
      SelectStr:= 
        'UPDATE PIMSMENUGROUP SET EDIT_YN = :EDIT_YN, '+
        'VISIBLE_YN = :VISIBLE_YN, MUTATIONDATE = :MUTATIONDATE, MUTATOR = :MUTATOR ' +
        'WHERE GROUP_NAME = :GROUP_NAME AND MENU_NUMBER = :MENU_NUMBER ';

    UserGroupDM.QueryExec.Active := False;
    UserGroupDM.QueryExec.SQL.Clear;
    UserGroupDM.QueryExec.SQL.Add(SelectStr);
    UserGroupDM.QueryExec.ParamByName('MUTATIONDATE').AsDateTime := CurrentDate;
    UserGroupDM.QueryExec.ParamByName('MENU_NUMBER').AsInteger :=
        UserGroupDM.QuerySelect.FieldByName('MENU_NUMBER').AsInteger;
    UserGroupDM.QueryExec.ParamByName('VISIBLE_YN').AsString := VisibleYN;
    UserGroupDM.QueryExec.ParamByName('EDIT_YN').AsString := EditYN;
    UserGroupDM.QueryExec.ParamByName('MUTATOR').AsString :=
        SystemDM.CurrentProgramUser ;
    UserGroupDM.QueryExec.ParamByName('GROUP_NAME').AsString := FGroup;
    if InsertGroup then
    begin
      UserGroupDM.QueryExec.ParamByName('EXPAND_YN').AsString :=
        UserGroupDM.QuerySelect.FieldByName('EXPAND_YN').AsString;
      UserGroupDM.QueryExec.ParamByName('PARENT_MENU_NUMBER').AsInteger :=
        UserGroupDM.QuerySelect.FieldByName('PARENT_MENU_NUMBER').AsInteger;
      UserGroupDM.QueryExec.ParamByName('DESCRIPTION').AsString :=
        UserGroupDM.QuerySelect.FieldByName('DESCRIPTION').AsString;
      UserGroupDM.QueryExec.ParamByName('SEQ_NUMBER').AsInteger :=
        UserGroupDM.QuerySelect.FieldByName('SEQ_NUMBER').AsInteger;
      UserGroupDM.QueryExec.ParamByName('CREATIONDATE').AsDateTime := CurrentDate;
    end;

    UserGroupDM.QueryExec.ExecSQL;

    UserGroupDM.QuerySelect.Next;
  end;{while QueryDetail}
end;

end.
