unit UAdmConst;

interface

uses
  Graphics, UAdmMessageRes;

const

  MenuDefaultItems: Array[1..4] of string  =
   (SAdmMenu_TreePims, SAdmMenu_AdmTool, SAdmMenu_Pims, SAdmMenu_Production_Screen);
  MENU_PARENT = 0;
  MENU_NUMBER_APPLICATIONS = 100000;
  MENU_NUMBER_ADMINISTRATOR = 110000;
  MENU_NUMBER_PIMS = 120000;
  MENU_NUMBER_PRODUCTION_SCREEN = 130000;

implementation

end.
