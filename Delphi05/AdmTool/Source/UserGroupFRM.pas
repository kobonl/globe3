unit UserGroupFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseFRM, Db, ActnList, dxBarDBNav, dxBar, dxCntner, dxTL, dxDBCtrl,
  dxDBGrid, ExtCtrls, DBCtrls, StdCtrls, Mask, dxDBTLCl, dxGrClms,
  dxEditor, dxExEdtr, dxDBEdtr, dxDBELib;

type
  TUserGroupF = class(TGridBaseF)
    GroupBoxDetail: TGroupBox;
    dxDetailGridColumn1: TdxDBGridColumn;
    dxDetailGridColumn2: TdxDBGridColumn;
    ButtonMenu: TButton;
    GroupBoxGroupDetail: TGroupBox;
    Label1: TLabel;
    DBEditGroupName: TDBEdit;
    Label3: TLabel;
    DBEditName: TDBEdit;
    ButtonCopyMenu: TButton;
    procedure dxBarBDBNavInsertClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure dxDetailGridChangeNode(Sender: TObject; OldNode,
      Node: TdxTreeListNode);
    procedure FormShow(Sender: TObject);
    procedure ButtonMenuClick(Sender: TObject);
    procedure ButtonCopyMenuClick(Sender: TObject);
  private
    { Private declarations }
     InsertForm: TForm;
  public
    { Public declarations }
  end;


function UserGroupF: TUserGroupF;

implementation

{$R *.DFM}

uses
  UserGroupDMT, UAdmConst, UAdmMessageRes,  UPimsConst, 
  MenuGroupFRM, SystemDMT, UPimsMessageRes,
  DialogCopyUserGroupFRM;

var
  UserGroupF_HND: TUserGroupF;

function UserGroupF: TUserGroupF;
begin
  if (UserGroupF_HND = nil) then
    UserGroupF_HND := TUserGroupF.Create(Application);
  Result := UserGroupF_HND;
end;

procedure TUserGroupF.dxBarBDBNavInsertClick(Sender: TObject);
begin
  inherited;
  DBEditGroupName.SetFocus;
end;

procedure TUserGroupF.FormCreate(Sender: TObject);
begin
  UserGroupDM := CreateFormDM(TUserGroupDM);
  if dxDetailGrid.DataSource = Nil then
    dxDetailGrid.DataSource := UserGroupDM.DataSourceDetail;
  inherited;
  if SystemDM.AplEditYN = UNCHECKEDVALUE then
  begin
    dxBarBDBNavInsert.Visible := ivNever;
    dxBarBDBNavDelete.Visible := ivNever;
    dxBarBDBNavPost.Visible := ivNever;
    dxBarBDBNavCancel.Visible := ivNever;
    dxBarButtonEditMode.Visible := ivNever;
    GroupBoxGroupDetail.Enabled := False;
   end;
end;

procedure TUserGroupF.FormDestroy(Sender: TObject);
begin
  inherited;
  UserGroupF_HND := nil;
end;

procedure TUserGroupF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
end;

procedure TUserGroupF.dxDetailGridChangeNode(Sender: TObject; OldNode,
  Node: TdxTreeListNode);
begin
  inherited;
  ButtonMenu.Caption := SAdmConfig + ' ' +
      UserGroupDM.TableDetail.FieldByName('GROUP_NAME').AsString +' ' + SAdmMenu;
end;

procedure TUserGroupF.FormShow(Sender: TObject);
begin
  inherited;
  ButtonMenu.Caption := SAdmConfig + ' ' +
    UserGroupDM.TableDetail.FieldByName('GROUP_NAME').AsString +' ' + SAdmMenu;
  if SystemDM.AplEditYN = UNCHECKEDVALUE then
    ButtonCopyMenu.Enabled := False;  
end;

procedure TUserGroupF.ButtonMenuClick(Sender: TObject);
begin
  inherited;
  if dxBarBDBNavPost.Enabled then
  begin
    DisplayMessage(SPimsSaveBefore, mtError, [mbOk]);
    Exit;
  end;
  if UserGroupDM.TableDetail.FieldByName('GROUP_NAME').AsString = ''  then
    Exit;
  // not necessary  
 { !!if UserGroupDM.TableDetail.FieldByName('GROUP_NAME').AsString <> ADMINGROUP then
  begin
    UserGroupDM.FillDefaultMenu(UserGroupDM.
      TableDetail.FieldByName('GROUP_NAME').AsString);
  end;}
  InsertForm := MenuGroupF;
  MenuGroupF.GroupName :=
    UserGroupDM.TableDetail.FieldByName('GROUP_NAME').AsString;
  InsertForm.ShowModal;
end;

procedure TUserGroupF.ButtonCopyMenuClick(Sender: TObject);
begin
  inherited;
  if UserGroupDM.TableDetail.FieldByName('GROUP_NAME').AsString <> ADMINGROUP then
  begin
    DialogCopyUserGroupF := TDialogCopyUserGroupF.Create(Application);
    DialogCopyUserGroupF.FGroup :=
      UserGroupDM.TableDetail.FieldByName('GROUP_NAME').AsString;
    try
      DialogCopyUserGroupF.ShowModal;
    finally
      DialogCopyUserGroupF.Free;
    end;
  end
  else
    DisplayMessage(SAdmSelectOtherUserGroup, mtInformation, [mbOk]);  
end;

end.
