unit DialogCopySelectionSHSFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DialogSelectionFRM, ComCtrls, StdCtrls, Buttons, ExtCtrls, dxCntner,
  dxEditor, dxExEdtr, dxEdLib, Dblup1a, Db, DBTables;

type
  TDialogCopySelectionSHSF = class(TDialogSelectionF)
    GroupBoxCopyTo: TGroupBox;
    GroupBoxCopyFrom: TGroupBox;
    Label1: TLabel;
    cmbPlusEmplFrom: TComboBoxPlus;
    Label2: TLabel;
    CmbPlusEmplTo: TComboBoxPlus;
    dxSpinEditWeekFrom: TdxSpinEdit;
    Label4: TLabel;
    dxSpinEditWeekTo: TdxSpinEdit;
    dxSpinEditStartWeek: TdxSpinEdit;
    TableCopy: TTable;
    dxSpinEditYearFrom: TdxSpinEdit;
    Label6: TLabel;
    dxSpinEditYearTo: TdxSpinEdit;
    Label9: TLabel;
    dxSpinEditStartYear: TdxSpinEdit;
    procedure FormShow(Sender: TObject);
    procedure cmbPlusEmplFromChange(Sender: TObject);
    procedure CmbPlusEmplToChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnOkClick(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FStartWeek: Integer;
    FYear: Word;
    FCopy : Boolean;
    procedure CopyEmployee;
  end;

var
  DialogCopySelectionSHSF: TDialogCopySelectionSHSF;

implementation

{$R *.DFM}
uses ListProcsFRM, SystemDMT, UPimsMessageRes, ShiftScheduleDMT;

procedure TDialogCopySelectionSHSF.FormShow(Sender: TObject);
begin
  inherited;

  ListProcsF.FillComboBoxMaster(ShiftScheduleDM.QueryEmpl, 'EMPLOYEE_NUMBER',
    True, cmbPlusEmplFrom);
  ListProcsF.FillComboBoxMaster(ShiftScheduleDM.QueryEmpl, 'EMPLOYEE_NUMBER',
    False, CmbPlusEmplTo);
  dxSpinEditWeekFrom.Value := FStartWeek;
  dxSpinEditWeekTo.Value := FStartWeek;
  dxSpinEditStartWeek.Value := FStartWeek + 1;
  dxSpinEditYearFrom.Value := FYear;
  dxSpinEditYearTo.Value := FYear;
  dxSpinEditStartYear.Value := FYear;
end;

procedure TDialogCopySelectionSHSF.cmbPlusEmplFromChange(Sender: TObject);
begin
  inherited;
  if (CmbPlusEmplFrom.DisplayValue <> '') and
     (CmbPlusEmplTo.DisplayValue <> '') then
  begin
    if GetIntValue(CmbPlusEmplFrom.Value) >
       GetIntValue(CmbPlusEmplTo.Value) then
      CmbPlusEmplTo.DisplayValue := CmbPlusEmplFrom.DisplayValue;
  end;
end;

procedure TDialogCopySelectionSHSF.CmbPlusEmplToChange(Sender: TObject);
begin
  inherited;
  if (CmbPlusEmplFrom.DisplayValue <> '') and
     (CmbPlusEmplTo.DisplayValue <> '') then
  begin
    if GetIntValue(CmbPlusEmplFrom.Value) >
       GetIntValue(CmbPlusEmplTo.Value) then
      CmbPlusEmplFrom.DisplayValue := CmbPlusEmplTo.DisplayValue;
  end;
end;

procedure TDialogCopySelectionSHSF.FormCreate(Sender: TObject);
begin
  inherited;
  TableCopy.Active := True;
end;

procedure TDialogCopySelectionSHSF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  CmbPlusEmplFrom.ClearGridData;
  CmbPlusEmplTo.ClearGridData;
  TableCopy.Close;
end;

procedure TDialogCopySelectionSHSF.btnOkClick(Sender: TObject);
begin
  inherited;
  if ((dxSpinEditWeekFrom.Value > dxSpinEditWeekTo.Value) and
   (dxSpinEditYearFrom.Value = dxSpinEditYearTo.Value)) or
   (dxSpinEditYearFrom.Value > dxSpinEditYearTo.Value) then
  begin
    DisplayMessage(SPimsStartEndDate, mtInformation, [mbOk]);
    Exit;
  end;

  if ((dxSpinEditStartWeek.Value < dxSpinEditWeekTo.Value) and
      (dxSpinEditStartYear.Value = dxSpinEditYearTo.Value)) or
    (dxSpinEditStartYear.Value < dxSpinEditYearTo.Value) then
  begin
    DisplayMessage(SSHSWeekCopy, mtInformation, [mbOk]);
    Exit;
  end;
  CopyEmployee;
  DisplayMessage( SCopyFinished, mtInformation, [mbOk]);
  DialogCopySelectionSHSF.Close;
  FCopy := True;
end;
procedure TDialogCopySelectionSHSF.CopyEmployee;
var
  DateMin, DateMax, DateCopySHS, DateSHS: TDateTime;
  Result, Empl: Integer;
begin
  Result := 0;
  DateMin := ListProcsF.DateFromWeek(Round(dxSpinEditYearFrom.Value),
    Round(dxSpinEditWeekFrom.Value), 1);
  DateMax := ListProcsF.DateFromWeek(Round(dxSpinEditYearTo.Value),
    Round(dxSpinEditWeekTo.Value), 7);
  with ShiftScheduleDM do
  begin
  // select all employees
    QueryDetail.First;
    while (not QueryDetail.Eof) and (QueryDetail.FieldByName('EMPLOYEE_NUMBER').AsInteger <
       GetIntValue(cmbPlusEmplFrom.Value)) do
        QueryDetail.Next;
    while (not QueryDetail.Eof) and
     (QueryDetail.FieldByName('EMPLOYEE_NUMBER').AsInteger <= GetIntValue(cmbPlusEmplTo.Value)) do
    begin
// select records from SHS Table
      DateSHS := DateMin;
      Empl := QueryDetail.FieldByName('EMPLOYEE_NUMBER').AsInteger;
      DateCopySHS :=  ListProcsF.DateFromWeek(Round(dxSpinEditStartYear.Value),
        Round(dxSpinEditStartWeek.Value), 1);
      while DateSHS <= DateMax do
      begin
        CopyFunction(TableSHS, TableCopy, Empl, Empl, DateSHS, DateCopySHS, Result);
        DateSHS := DateSHS + 1;
        DateCopySHS := DateCopySHS + 1;
      end;
      QueryDetail.Next;
    end;{while QueryDetail}
  end;{with}
end;

procedure TDialogCopySelectionSHSF.btnCancelClick(Sender: TObject);
begin
  inherited;
  FCopy := False;
end;

end.
