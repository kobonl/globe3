unit TeamPerUserFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseFRM, Db, ActnList, dxBarDBNav, dxBar, dxCntner, dxTL, dxDBCtrl,
  dxDBGrid, ExtCtrls, StdCtrls, Mask, DBCtrls, DBTables, dxEditor,
  dxExEdtr, dxDBEdtr, dxDBELib, dxEdLib, dxDBTLCl, dxGrClms;

const
  ValidKeys  : set of Char = [#08, #13] + [#48..#57];
type

  TTeamPerUserF = class(TGridBaseF)
    dxDetailGridColumnUser: TdxDBGridColumn;
    GroupBoxDetail: TGroupBox;
    Label1: TLabel;
    Label3: TLabel;
    Label2: TLabel;
    dxDBLookupEditTeam: TdxDBLookupEdit;
    Label4: TLabel;
    DBEdit1: TDBEdit;
    dxDetailGridColumnDesc: TdxDBGridColumn;
    dxDetailGridColumnTeam: TdxDBGridLookupColumn;
    procedure dxBarBDBNavInsertClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

function TeamPerUserF: TTeamPerUserF;

implementation

{$R *.DFM}

uses
   SystemDMT, UserDMT, UPimsConst, UAdmMessageRes, UserFRM;

var
  TeamPerUserF_HND: TTeamPerUserF;

function TeamPerUserF: TTeamPerUserF;
begin
  if (TeamPerUserF_HND = nil) then
    TeamPerUserF_HND := TTeamPerUserF.Create(Application);
  Result := TeamPerUserF_HND;
end;

procedure TTeamPerUserF.dxBarBDBNavInsertClick(Sender: TObject);
begin
  inherited;
 
  dxDBLookupEditTeam.SetFocus;
end;

procedure TTeamPerUserF.FormDestroy(Sender: TObject);
begin
  inherited;
  TeamPerUserF_HND := nil;
end;

procedure TTeamPerUserF.FormCreate(Sender: TObject);
begin
  inherited;
  if SystemDM.AplEditYN = UNCHECKEDVALUE then
  begin
    dxBarBDBNavInsert.Visible := ivNever;
    dxBarBDBNavDelete.Visible := ivNever;
    dxBarBDBNavPost.Visible := ivNever;
    dxBarBDBNavCancel.Visible := ivNever;
    dxBarButtonEditMode.Visible := ivNever;
    GroupBoxDetail.Enabled := False;
   end;
end;

procedure TTeamPerUserF.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  if dxDetailGrid.DataSource.DataSet.State in [dsEdit, dsInsert] then
    dxDetailGrid.DataSource.DataSet.Cancel;
  UserF.SetGridForm(UserF);
  Action := caFree;
end;

end.
