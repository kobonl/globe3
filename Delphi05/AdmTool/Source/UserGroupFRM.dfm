inherited UserGroupF: TUserGroupF
  Left = 240
  Top = 129
  Width = 677
  Height = 502
  Caption = 'User groups'
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlMasterGrid: TPanel
    Width = 669
    Height = 63
    TabOrder = 0
    Visible = False
    inherited spltMasterGrid: TSplitter
      Top = 59
      Width = 667
    end
    inherited dxMasterGrid: TdxDBGrid
      Width = 667
      Height = 58
      Visible = False
    end
  end
  inherited pnlDetail: TPanel
    Top = 310
    Width = 669
    TabOrder = 3
    object GroupBoxDetail: TGroupBox
      Left = 1
      Top = 1
      Width = 672
      Height = 163
      Align = alLeft
      Caption = 'User groups'
      TabOrder = 0
      object ButtonMenu: TButton
        Left = 432
        Top = 16
        Width = 209
        Height = 33
        Caption = 'Config Menu'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        OnClick = ButtonMenuClick
      end
      object GroupBoxGroupDetail: TGroupBox
        Left = 8
        Top = 16
        Width = 401
        Height = 129
        TabOrder = 1
        object Label1: TLabel
          Left = 8
          Top = 20
          Width = 58
          Height = 13
          Caption = 'Group name'
        end
        object Label3: TLabel
          Left = 8
          Top = 63
          Width = 53
          Height = 13
          Caption = 'Description'
        end
        object DBEditGroupName: TDBEdit
          Tag = 1
          Left = 83
          Top = 20
          Width = 230
          Height = 19
          CharCase = ecUpperCase
          Ctl3D = False
          DataField = 'GROUP_NAME'
          DataSource = UserGroupDM.DataSourceDetail
          ParentCtl3D = False
          TabOrder = 0
        end
        object DBEditName: TDBEdit
          Tag = 1
          Left = 83
          Top = 61
          Width = 302
          Height = 19
          Ctl3D = False
          DataField = 'DESCRIPTION'
          DataSource = UserGroupDM.DataSourceDetail
          ParentCtl3D = False
          TabOrder = 1
        end
      end
      object ButtonCopyMenu: TButton
        Left = 432
        Top = 72
        Width = 209
        Height = 33
        Caption = 'Copy Menu'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
        OnClick = ButtonCopyMenuClick
      end
    end
  end
  inherited pnlDetailGrid: TPanel
    Top = 89
    Width = 669
    Height = 221
    inherited spltDetail: TSplitter
      Top = 217
      Width = 667
    end
    inherited dxDetailGrid: TdxDBGrid
      Tag = 1
      Width = 667
      Height = 216
      Bands = <
        item
          Alignment = taLeftJustify
          Caption = 'Groups'
        end>
      KeyField = 'GROUP_NAME'
      ShowBands = True
      OnChangeNode = dxDetailGridChangeNode
      object dxDetailGridColumn1: TdxDBGridColumn
        Caption = 'Group name'
        Width = 69
        BandIndex = 0
        RowIndex = 0
        FieldName = 'GROUP_NAME'
      end
      object dxDetailGridColumn2: TdxDBGridColumn
        Caption = 'Description'
        Width = 184
        BandIndex = 0
        RowIndex = 0
        FieldName = 'DESCRIPTION'
      end
    end
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = False
        WholeRow = False
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <
          item
            Item = dxBarButtonFirst
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonPrior
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonNext
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonLast
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarBDBNavInsert
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavDelete
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavPost
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavCancel
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonEditMode
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonRecordDetails
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSort
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSearch
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCustCol
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonShowGroup
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExpand
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCollapse
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonResetColumns
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonExportAllHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportAllXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    DockControlHeights = (
      0
      0
      26
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
  end
  inherited dsrcActive: TDataSource
    Top = 112
  end
end
