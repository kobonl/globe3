(*
  MRA:21-FEB-2014 20011800
  - Final Run System
  - Addition of SYSADMIN_YN-field for users.
*)
unit UserDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseDMT, Db, DBTables;

type
  TUserDM = class(TGridBaseDM)
    TableMasterPLANT_CODE: TStringField;
    TableMasterDESCRIPTION: TStringField;
    TableMasterADDRESS: TStringField;
    TableMasterZIPCODE: TStringField;
    TableMasterCITY: TStringField;
    TableMasterSTATE: TStringField;
    TableMasterPHONE: TStringField;
    TableMasterFAX: TStringField;
    TableMasterCREATIONDATE: TDateTimeField;
    TableMasterINSCAN_MARGIN_EARLY: TIntegerField;
    TableMasterINSCAN_MARGIN_LATE: TIntegerField;
    TableMasterMUTATIONDATE: TDateTimeField;
    TableMasterMUTATOR: TStringField;
    TableMasterOUTSCAN_MARGIN_EARLY: TIntegerField;
    TableMasterOUTSCAN_MARGIN_LATE: TIntegerField;
    TableGroup: TTable;
    TableGroupGROUP_NAME: TStringField;
    TableGroupDESCRIPTION: TStringField;
    TableGroupCREATIONDATE: TDateTimeField;
    TableGroupMUTATIONDATE: TDateTimeField;
    TableGroupMUTATOR: TStringField;
    TableDetailGROUPLU: TStringField;
    TableDetailUSER_NAME: TStringField;
    TableDetailGROUP_NAME: TStringField;
    TableDetailUSER_PASSWORD: TStringField;
    TableDetailCREATIONDATE: TDateTimeField;
    TableDetailMUTATIONDATE: TDateTimeField;
    TableDetailMUTATOR: TStringField;
    TableDetailSET_TEAM_SELECTION_YN: TStringField;
    TableTeamPerUser: TTable;
    TableTeamPerUserUSER_NAME: TStringField;
    TableTeamPerUserTEAM_CODE: TStringField;
    TableTeamPerUserCREATIONDATE: TDateTimeField;
    TableTeamPerUserMUTATIONDATE: TDateTimeField;
    TableTeamPerUserMUTATOR: TStringField;
    TableTeam: TTable;
    DataSourceTeamPerUser: TDataSource;
    TableTeamTEAM_CODE: TStringField;
    TableTeamDESCRIPTION: TStringField;
    TableTeamCREATIONDATE: TDateTimeField;
    TableTeamRESP_EMPLOYEE_NUMBER: TIntegerField;
    TableTeamMUTATIONDATE: TDateTimeField;
    TableTeamMUTATOR: TStringField;
    TableTeamPerUserTeamDesc: TStringField;
    TableTeamPerUserTEAMLU: TStringField;
    QueryDeleteTeamSelection: TQuery;
    TableDetailSYSADMIN_YN: TStringField;
    procedure TableDetailBeforeDelete(DataSet: TDataSet);
    procedure TableDetailBeforeScroll(DataSet: TDataSet);
    procedure TableDetailAfterDelete(DataSet: TDataSet);
    procedure TableDetailAfterPost(DataSet: TDataSet);
    procedure TableDetailAfterCancel(DataSet: TDataSet);
    procedure TableDetailNewRecord(DataSet: TDataSet);
    procedure TableDetailBeforePost(DataSet: TDataSet);
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }

  public
    { Public declarations }

  end;

var
  UserDM: TUserDM;

implementation

uses SystemDMT,  UPimsConst,  UAdmMessageRes, UAdmConst, UserFRM,
  UPimsMessageRes;


{$R *.DFM}

procedure TUserDM.TableDetailBeforeDelete(DataSet: TDataSet);
begin
  inherited;
  DefaultBeforeDelete(DataSet);
  if DataSet.FieldByName('USER_NAME').AsString = SystemDM.CurrentLoginUser then
  begin
    DisplayMessage(SAdmNotDeleteUser, mtWarning,  [mbOk]);
    Abort;
  end;
end;

procedure TUserDM.TableDetailBeforeScroll(DataSet: TDataSet);
begin
  inherited;
  UserF.dxEditConfirmPassword.Text :=
    TableDetail.FieldByName('USER_PASSWORD').AsString;
end;

procedure TUserDM.TableDetailAfterDelete(DataSet: TDataSet);
begin
  inherited;
  UserF.dxEditConfirmPassword.Text :=
    TableDetail.FieldByName('USER_PASSWORD').AsString;
end;

procedure TUserDM.TableDetailAfterPost(DataSet: TDataSet);
begin
  inherited;
  UserF.dxEditConfirmPassword.Text :=
    TableDetail.FieldByName('USER_PASSWORD').AsString;
end;

procedure TUserDM.TableDetailAfterCancel(DataSet: TDataSet);
begin
  inherited;
  UserF.dxEditConfirmPassword.Text :=
    TableDetail.FieldByName('USER_PASSWORD').AsString;
  UserF.ButtonTeamSelection.Enabled :=
    DataSet.FieldByName('SET_TEAM_SELECTION_YN').AsString = CHECKEDVALUE;
end;

procedure TUserDM.TableDetailNewRecord(DataSet: TDataSet);
begin
  inherited;
  SystemDM.DefaultNewRecord(DataSet);
  TableDetail.FieldByName('SET_TEAM_SELECTION_YN').AsString := UNCHECKEDVALUE;
end;

procedure TUserDM.TableDetailBeforePost(DataSet: TDataSet);
begin
  inherited;
  // MR:13-05-2005 Validate input!
  // Password must be the same as confirmation-password.
  if (UserF.dxEditConfirmPassword.Text <> UserF.DBEditPassword.Text) then
  begin
    DisplayMessage(SAplPasswordConfirm, mtInformation, [mbOk]);
    SysUtils.Abort;
    Exit;
//    UserF.DBEditPassword.SetFocus;
  end;
  // MR:13-05-2005 Validate input!
  // An empty password is not allowed.
  if Trim(UserF.DBEditPassword.Text) = '' then
  begin
    DisplayMessage(SPimsNoEmptyPasswordAllowed, mtInformation, [mbOk]);
    SysUtils.Abort;
    Exit;
  end;

  SystemDM.DefaultBeforePost(DataSet);
  if DataSet.FieldByName('SET_TEAM_SELECTION_YN').AsString = UNCHECKEDVALUE then
    QueryDeleteTeamSelection.ExecSQL;
end;

procedure TUserDM.DataModuleCreate(Sender: TObject);
begin
  inherited;
  QueryDeleteTeamSelection.Prepare;
end;

procedure TUserDM.DataModuleDestroy(Sender: TObject);
begin
  inherited;
  QueryDeleteTeamSelection.Close;
  QueryDeleteTeamSelection.UnPrepare;
end;

end.
