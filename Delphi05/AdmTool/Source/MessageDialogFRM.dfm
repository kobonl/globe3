object MessageDialogF: TMessageDialogF
  Left = 458
  Top = 238
  BorderStyle = bsDialog
  Caption = 'PIMS'
  ClientHeight = 98
  ClientWidth = 425
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object lblMessage: TLabel
    Left = 32
    Top = 16
    Width = 53
    Height = 13
    Caption = 'lblMessage'
  end
  object btnYes: TButton
    Left = 32
    Top = 56
    Width = 75
    Height = 25
    Caption = '&Yes'
    ModalResult = 6
    TabOrder = 0
  end
  object btnNo: TButton
    Left = 120
    Top = 56
    Width = 75
    Height = 25
    Caption = '&No'
    ModalResult = 7
    TabOrder = 1
  end
  object btnAll: TButton
    Left = 208
    Top = 56
    Width = 75
    Height = 25
    Caption = '&All'
    ModalResult = 8
    TabOrder = 2
  end
  object btnNoToAll: TButton
    Left = 296
    Top = 56
    Width = 75
    Height = 25
    Caption = 'N&o To All'
    ModalResult = 9
    TabOrder = 3
  end
end
