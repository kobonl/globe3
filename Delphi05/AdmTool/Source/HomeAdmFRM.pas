unit HomeAdmFRM;
(*******************************************************************************
Unit     : FHome  TfrmHome = (TfrmPims)
Function : Application main form.
--------------------------------------------------------------------------------
Inheritance tree:
TForm                          w
frmPims
  Changed design properties :
  + Font.Name := Tahoma (MicroSoft Outlook Style)
  + Position  := poScreenCenter

  Added controls:
  -
  Events:
  + FormCreate : Update the progressbar on the splashscreen

  Methods:
  -
TfrmHome
  Changed design properties :
  + Heigth := 385
  + Width  := 485
  + Position  := 0, 0

  Added controls:
  4 buttons

  Events:
  -

  Methods:
  -

*******************************************************************************)

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ComCtrls, ExtCtrls,
  Db, DBTables, Dblup1a, dxCntner, dxTL, dxDBCtrl, dxDBGrid,
  Dblup1b,  OleCtrls, SHDocVw, PimsFRM;

type
  THomeAdmF = class(TPimsF)
    imgHome: TImage;
    labelDefinitions: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure labelDefinitionsMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure imgHomeMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure labelDefinitionsMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure labelDefinitionsMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure btnDefinitionsClick(Sender: TObject);
   
  private
    { Private declarations }
    procedure DisableOptions(Options: String);
  public
    { Public declarations }
  end;

var
  HomeAdmF: THomeAdmF;

implementation


uses
  SystemDMT,
  SideMenuUnitMaintenanceAdmFRM;

{$R *.DFM}
// disable option of menu using parameters
procedure THomeAdmF.DisableOptions(Options: String);
begin
  labelDefinitions.Enabled := Pos('1', Options) > 0;
  
end;

procedure THomeAdmF.FormCreate(Sender: TObject);
begin
  inherited;
  Left := 0;
  Top  := 0;
  if ParamCount <> 0 then
    DisableOptions(ParamStr(1));
end;

procedure THomeAdmF.labelDefinitionsMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
procedure SetDisableLabel(FLabel: TLabel; FTag: Integer);
begin
  if FLabel.Tag <> FTag then
  begin
    FLabel.Font.Color := clWhite;
    FLabel.Font.Style := FLabel.Font.Style - [fsUnderline];
  end;
end;
begin
  inherited;
  SetDisableLabel(labelDefinitions, (Sender as TLabel).Tag);
   
  if Sender is TLabel then
    if (Sender as TLabel).Tag > 0 then
    begin
      (Sender as TLabel).Font.Style :=
         (Sender as TLabel).Font.Style + [fsUnderline];
      (Sender as TLabel).Font.Color := clYellow;
    end;
end;

procedure THomeAdmF.imgHomeMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
procedure SetDisableLabel(FLabel: TLabel);
begin
  FLabel.Font.Color := clWhite;
  FLabel.Font.Style := FLabel.Font.Style - [fsUnderline];
end;

begin
  inherited;
  SetDisableLabel(labelDefinitions);
 
end;

procedure THomeAdmF.labelDefinitionsMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  inherited;
  (Sender as TLabel).Font.Style :=
     (Sender as TLabel).Font.Style + [fsUnderline];
  (Sender as TLabel).Font.Color := clLime;
end;

procedure THomeAdmF.labelDefinitionsMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  inherited;
  (Sender as TLabel).Font.Style :=
     (Sender as TLabel).Font.Style + [fsUnderline];
  (Sender as TLabel).Font.Color := clWhite;
end;

procedure THomeAdmF.btnDefinitionsClick(Sender: TObject);
begin
  inherited;
  SideMenuUnitMaintenanceAdmF.ShowAdminUserGroup;
end;
 
end.
