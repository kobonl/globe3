unit MenuGroupFRM1;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseFRM, Db, ActnList, dxBarDBNav, dxBar, StdCtrls, dxCntner, dxTL,
  dxDBCtrl, dxDBGrid, ExtCtrls, dxEditor, dxExEdtr, dxDBEdtr, dxDBELib;

type
  TMenuGroupF = class(TGridBaseF)
    Panel1: TPanel;
    Label1: TLabel;
    dxDBLookupEditGroup: TdxDBLookupEdit;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

function MenuGroupF: TMenuGroupF;

implementation

{$R *.DFM}

uses
  AdmToolDMT, UPimsConst, UAdmMessageRes, MenuGroupDMT;

var
  MenuGroupF_HND: TMenuGroupF;

function MenuGroupF: TMenuGroupF;
begin
  if (MenuGroupF_HND = nil) then
    MenuGroupF_HND := TMenuGroupF.Create(Application);
  Result := MenuGroupF_HND;
end;


 
procedure TMenuGroupF.FormCreate(Sender: TObject);
begin
  MenuGroupDM := CreateFormDM(TMenuGroupDM);
  inherited;

end;

procedure TMenuGroupF.FormDestroy(Sender: TObject);
begin
  inherited;
  MenuGroupF_HND := nil;
end;

procedure TMenuGroupF.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
end;

end.
