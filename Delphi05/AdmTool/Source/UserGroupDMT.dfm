inherited UserGroupDM: TUserGroupDM
  OldCreateOrder = True
  Left = 285
  Top = 161
  Height = 479
  Width = 741
  inherited TableMaster: TTable
    Left = 84
    Top = 40
    object TableMasterPLANTLU: TStringField
      FieldKind = fkLookup
      FieldName = 'PLANTLU'
      LookupKeyFields = 'PLANT_CODE'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'PLANT_CODE'
      Size = 30
      Lookup = True
    end
    object TableMasterBUSINESSUNIT_CODE: TStringField
      FieldName = 'BUSINESSUNIT_CODE'
      Required = True
      OnValidate = DefaultNotEmptyValidate
      Size = 6
    end
    object TableMasterDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      OnValidate = DefaultNotEmptyValidate
      Size = 30
    end
    object TableMasterPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Required = True
      OnValidate = DefaultNotEmptyValidate
      Size = 6
    end
    object TableMasterCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object TableMasterBONUS_FACTOR: TFloatField
      DefaultExpression = '0'
      FieldName = 'BONUS_FACTOR'
      DisplayFormat = '0.##'
      MaxValue = 999.99
      Precision = 5
    end
    object TableMasterNORM_ILL_VS_DIRECT_HOURS: TFloatField
      DefaultExpression = '0'
      FieldName = 'NORM_ILL_VS_DIRECT_HOURS'
      DisplayFormat = '0.##'
      MaxValue = 999.99
      Precision = 5
    end
    object TableMasterMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TableMasterMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object TableMasterNORM_ILL_VS_INDIRECT_HOURS: TFloatField
      DefaultExpression = '0'
      FieldName = 'NORM_ILL_VS_INDIRECT_HOURS'
      DisplayFormat = '0.##'
      MaxValue = 999.99
      Precision = 5
    end
    object TableMasterNORM_ILL_VS_TOTAL_HOURS: TFloatField
      DefaultExpression = '0'
      FieldName = 'NORM_ILL_VS_TOTAL_HOURS'
      DisplayFormat = '0.##'
      MaxValue = 999.99
      Precision = 5
    end
  end
  inherited TableDetail: TTable
    BeforeDelete = TableDetailBeforeDelete
    AfterScroll = TableDetailAfterScroll
    DatabaseName = 'Pims'
    Filtered = True
    OnFilterRecord = TableDetailFilterRecord
    TableName = 'PIMSUSERGROUP'
    Left = 76
    Top = 100
    object TableDetailGROUP_NAME: TStringField
      FieldName = 'GROUP_NAME'
      Required = True
      Size = 31
    end
    object TableDetailDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 40
    end
    object TableDetailCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
      Required = True
    end
    object TableDetailMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
      Required = True
    end
    object TableDetailMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
  end
  inherited DataSourceMaster: TDataSource
    Top = 40
  end
  inherited DataSourceDetail: TDataSource
    Left = 200
    Top = 100
  end
  inherited TableExport: TTable
    DatabaseName = 'Pims'
    Top = 332
  end
  inherited DataSourceExport: TDataSource
    DataSet = TableExport
    Left = 504
    Top = 332
  end
  object QuerySelect: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    Left = 272
    Top = 168
  end
  object TablePimsGroup: TTable
    BeforePost = DefaultBeforePost
    OnNewRecord = DefaultNewRecord
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    TableName = 'PIMSUSERGROUP'
    Left = 312
    Top = 40
  end
  object TableGroupMenu: TTable
    BeforePost = DefaultBeforePost
    OnNewRecord = DefaultNewRecord
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    TableName = 'PIMSMENUGROUP'
    Left = 424
    Top = 40
  end
  object QuerySelectUserGroups: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  GROUP_NAME, DESCRIPTION '
      'FROM '
      '  PIMSUSERGROUP '
      'WHERE '
      '  GROUP_NAME <> '#39'ABSADMIN'#39)
    Left = 96
    Top = 168
  end
  object QueryExec: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    Left = 208
    Top = 168
  end
end
