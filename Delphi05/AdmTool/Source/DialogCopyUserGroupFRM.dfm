inherited DialogCopyUserGroupF: TDialogCopyUserGroupF
  Left = 333
  Width = 419
  Height = 210
  Caption = 'Copy from other user group'
  OldCreateOrder = True
  Position = poMainFormCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlBottom: TPanel
    Top = 111
    Width = 403
    inherited btnOk: TBitBtn
      Left = 64
      ModalResult = 0
      OnClick = btnOkClick
    end
    inherited btnCancel: TBitBtn
      Left = 202
    end
  end
  inherited stbarBase: TStatusBar
    Top = 152
    Width = 403
  end
  object GroupBoxCopyFrom: TGroupBox
    Left = 0
    Top = 0
    Width = 403
    Height = 111
    Align = alClient
    TabOrder = 2
    object Label1: TLabel
      Left = 24
      Top = 32
      Width = 76
      Height = 13
      Caption = 'From user group'
    end
    object Label2: TLabel
      Left = 24
      Top = 76
      Width = 66
      Height = 13
      Caption = 'To user group'
    end
    object cmbPlusGroupFrom: TComboBoxPlus
      Left = 120
      Top = 30
      Width = 185
      Height = 19
      ColCount = 49
      Ctl3D = False
      DefaultRowHeight = 19
      DropDownAlign = Left
      ListColor = clWindow
      ListDefaultDrawing = True
      ListFont.Charset = DEFAULT_CHARSET
      ListFont.Color = clWindowText
      ListFont.Height = -11
      ListFont.Name = 'Tahoma'
      ListFont.Style = []
      ListCursor = crDefault
      ButtonCursor = crDefault
      ParentCtl3D = False
      TabOrder = 0
      TitleColor = clBtnFace
    end
    object EditToUserGroup: TEdit
      Left = 120
      Top = 72
      Width = 185
      Height = 21
      Enabled = False
      TabOrder = 1
      Text = 'EditToUserGroup'
    end
  end
end
