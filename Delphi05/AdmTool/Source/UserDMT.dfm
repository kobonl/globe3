inherited UserDM: TUserDM
  OldCreateOrder = True
  OnDestroy = DataModuleDestroy
  Left = 248
  Top = 156
  Height = 479
  Width = 747
  inherited TableMaster: TTable
    Left = 76
    object TableMasterPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Required = True
      OnValidate = DefaultNotEmptyValidate
      Size = 6
    end
    object TableMasterDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      OnValidate = DefaultNotEmptyValidate
      Size = 30
    end
    object TableMasterADDRESS: TStringField
      FieldName = 'ADDRESS'
      Size = 30
    end
    object TableMasterZIPCODE: TStringField
      FieldName = 'ZIPCODE'
      Size = 15
    end
    object TableMasterCITY: TStringField
      FieldName = 'CITY'
      Size = 30
    end
    object TableMasterSTATE: TStringField
      FieldName = 'STATE'
    end
    object TableMasterPHONE: TStringField
      FieldName = 'PHONE'
      Size = 15
    end
    object TableMasterFAX: TStringField
      FieldName = 'FAX'
      Size = 15
    end
    object TableMasterCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object TableMasterINSCAN_MARGIN_EARLY: TIntegerField
      FieldName = 'INSCAN_MARGIN_EARLY'
      OnValidate = DefaultNotEmptyValidate
    end
    object TableMasterINSCAN_MARGIN_LATE: TIntegerField
      FieldName = 'INSCAN_MARGIN_LATE'
      OnValidate = DefaultNotEmptyValidate
    end
    object TableMasterMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TableMasterMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object TableMasterOUTSCAN_MARGIN_EARLY: TIntegerField
      FieldName = 'OUTSCAN_MARGIN_EARLY'
      OnValidate = DefaultNotEmptyValidate
    end
    object TableMasterOUTSCAN_MARGIN_LATE: TIntegerField
      FieldName = 'OUTSCAN_MARGIN_LATE'
      OnValidate = DefaultNotEmptyValidate
    end
  end
  inherited TableDetail: TTable
    BeforePost = TableDetailBeforePost
    AfterCancel = TableDetailAfterCancel
    BeforeDelete = TableDetailBeforeDelete
    OnNewRecord = TableDetailNewRecord
    DatabaseName = 'Pims'
    Filtered = True
    TableName = 'PIMSUSER'
    Left = 76
    Top = 108
    object TableDetailGROUPLU: TStringField
      FieldKind = fkLookup
      FieldName = 'GROUPLU'
      LookupDataSet = TableGroup
      LookupKeyFields = 'GROUP_NAME'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'GROUP_NAME'
      Size = 40
      Lookup = True
    end
    object TableDetailUSER_NAME: TStringField
      FieldName = 'USER_NAME'
      Required = True
      Size = 31
    end
    object TableDetailGROUP_NAME: TStringField
      FieldName = 'GROUP_NAME'
      Required = True
      Size = 31
    end
    object TableDetailUSER_PASSWORD: TStringField
      FieldName = 'USER_PASSWORD'
      Required = True
      Size = 31
    end
    object TableDetailCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
      Required = True
    end
    object TableDetailMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
      Required = True
    end
    object TableDetailMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object TableDetailSET_TEAM_SELECTION_YN: TStringField
      FieldName = 'SET_TEAM_SELECTION_YN'
      Size = 1
    end
    object TableDetailSYSADMIN_YN: TStringField
      FieldName = 'SYSADMIN_YN'
      Size = 1
    end
  end
  inherited DataSourceDetail: TDataSource
    Left = 200
    Top = 116
  end
  inherited TableExport: TTable
    DatabaseName = 'Pims'
    Left = 372
  end
  inherited DataSourceExport: TDataSource
    DataSet = TableExport
    Left = 464
  end
  object TableGroup: TTable
    DatabaseName = 'Pims'
    Filtered = True
    SessionName = 'SessionPims'
    TableName = 'PIMSUSERGROUP'
    Left = 80
    Top = 168
    object TableGroupGROUP_NAME: TStringField
      FieldName = 'GROUP_NAME'
      Required = True
      Size = 30
    end
    object TableGroupDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 40
    end
    object TableGroupCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
      Required = True
    end
    object TableGroupMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
      Required = True
    end
    object TableGroupMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
  end
  object TableTeamPerUser: TTable
    BeforePost = DefaultBeforePost
    BeforeDelete = DefaultBeforeDelete
    OnDeleteError = DefaultDeleteError
    OnEditError = DefaultEditError
    OnNewRecord = DefaultNewRecord
    OnPostError = DefaultPostError
    DatabaseName = 'Pims'
    Filtered = True
    SessionName = 'SessionPims'
    IndexFieldNames = 'USER_NAME;TEAM_CODE'
    MasterFields = 'USER_NAME'
    MasterSource = DataSourceDetail
    TableName = 'TEAMPERUSER'
    Left = 80
    Top = 224
    object TableTeamPerUserUSER_NAME: TStringField
      FieldName = 'USER_NAME'
      Required = True
      Size = 31
    end
    object TableTeamPerUserTEAM_CODE: TStringField
      FieldName = 'TEAM_CODE'
      Required = True
      Size = 6
    end
    object TableTeamPerUserCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
      Required = True
    end
    object TableTeamPerUserMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
      Required = True
    end
    object TableTeamPerUserMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object TableTeamPerUserTeamDesc: TStringField
      FieldKind = fkLookup
      FieldName = 'TEAMDESC'
      LookupDataSet = TableTeam
      LookupKeyFields = 'TEAM_CODE'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'TEAM_CODE'
      Size = 30
      Lookup = True
    end
    object TableTeamPerUserTEAMLU: TStringField
      FieldKind = fkLookup
      FieldName = 'TEAMLU'
      LookupDataSet = TableTeam
      LookupKeyFields = 'TEAM_CODE'
      LookupResultField = 'TEAM_CODE'
      KeyFields = 'TEAM_CODE'
      Size = 6
      Lookup = True
    end
  end
  object TableTeam: TTable
    DatabaseName = 'Pims'
    Filtered = True
    SessionName = 'SessionPims'
    TableName = 'TEAM'
    Left = 80
    Top = 280
    object TableTeamTEAM_CODE: TStringField
      FieldName = 'TEAM_CODE'
      Required = True
      Size = 6
    end
    object TableTeamDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 30
    end
    object TableTeamCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object TableTeamRESP_EMPLOYEE_NUMBER: TIntegerField
      FieldName = 'RESP_EMPLOYEE_NUMBER'
    end
    object TableTeamMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TableTeamMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
  end
  object DataSourceTeamPerUser: TDataSource
    DataSet = TableTeamPerUser
    Left = 208
    Top = 220
  end
  object QueryDeleteTeamSelection: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    DataSource = DataSourceDetail
    SQL.Strings = (
      'DELETE FROM TEAMPERUSER '
      'WHERE USER_NAME = :USER_NAME')
    Left = 376
    Top = 80
    ParamData = <
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end>
  end
end
