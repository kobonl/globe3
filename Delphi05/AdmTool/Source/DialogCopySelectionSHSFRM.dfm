inherited DialogCopySelectionSHSF: TDialogCopySelectionSHSF
  Left = 333
  Width = 525
  Height = 299
  Caption = 'Copy from other weeks'
  OldCreateOrder = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlBottom: TPanel
    Top = 212
    Width = 517
    inherited btnOk: TBitBtn
      ModalResult = 0
      OnClick = btnOkClick
    end
    inherited btnCancel: TBitBtn
      OnClick = btnCancelClick
    end
  end
  inherited stbarBase: TStatusBar
    Top = 253
    Width = 517
  end
  object GroupBoxCopyTo: TGroupBox
    Left = 0
    Top = 113
    Width = 517
    Height = 99
    Align = alClient
    Caption = 'Copy to'
    TabOrder = 2
    object Label9: TLabel
      Left = 16
      Top = 40
      Width = 80
      Height = 13
      Caption = 'Start Year/Week'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object dxSpinEditStartWeek: TdxSpinEdit
      Left = 184
      Top = 36
      Width = 65
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      MaxValue = 52
      MinValue = 1
      Value = 1
      StoredValues = 48
    end
    object dxSpinEditStartYear: TdxSpinEdit
      Left = 104
      Top = 36
      Width = 65
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      MaxValue = 2099
      MinValue = 1900
      Value = 1900
      StoredValues = 48
    end
  end
  object GroupBoxCopyFrom: TGroupBox
    Left = 0
    Top = 0
    Width = 517
    Height = 113
    Align = alTop
    Caption = 'Copy shift schedule of'
    TabOrder = 3
    object Label1: TLabel
      Left = 16
      Top = 24
      Width = 46
      Height = 13
      Caption = 'Employee'
    end
    object Label2: TLabel
      Left = 264
      Top = 24
      Width = 18
      Height = 13
      Caption = 'thru'
    end
    object Label4: TLabel
      Left = 264
      Top = 72
      Width = 18
      Height = 13
      Caption = 'thru'
    end
    object Label6: TLabel
      Left = 16
      Top = 72
      Width = 53
      Height = 13
      Caption = 'Year/Week'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object cmbPlusEmplFrom: TComboBoxPlus
      Left = 104
      Top = 22
      Width = 145
      Height = 19
      ColCount = 44
      Ctl3D = False
      DefaultRowHeight = 19
      DropDownAlign = Left
      ListColor = clWindow
      ListDefaultDrawing = True
      ListFont.Charset = DEFAULT_CHARSET
      ListFont.Color = clWindowText
      ListFont.Height = -11
      ListFont.Name = 'Tahoma'
      ListFont.Style = []
      ListCursor = crDefault
      ButtonCursor = crDefault
      ParentCtl3D = False
      TabOrder = 0
      TitleColor = clBtnFace
      OnChange = cmbPlusEmplFromChange
    end
    object CmbPlusEmplTo: TComboBoxPlus
      Left = 304
      Top = 22
      Width = 145
      Height = 19
      ColCount = 45
      Ctl3D = False
      DefaultRowHeight = 19
      DropDownAlign = Left
      ListColor = clWindow
      ListDefaultDrawing = True
      ListFont.Charset = DEFAULT_CHARSET
      ListFont.Color = clWindowText
      ListFont.Height = -11
      ListFont.Name = 'Tahoma'
      ListFont.Style = []
      ListCursor = crDefault
      ButtonCursor = crDefault
      ParentCtl3D = False
      TabOrder = 1
      TitleColor = clBtnFace
      OnChange = CmbPlusEmplToChange
    end
    object dxSpinEditWeekFrom: TdxSpinEdit
      Left = 184
      Top = 70
      Width = 65
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      MaxValue = 52
      MinValue = 1
      Value = 1
      StoredValues = 48
    end
    object dxSpinEditWeekTo: TdxSpinEdit
      Left = 392
      Top = 70
      Width = 65
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
      MaxValue = 52
      MinValue = 1
      Value = 1
      StoredValues = 48
    end
    object dxSpinEditYearFrom: TdxSpinEdit
      Left = 104
      Top = 70
      Width = 65
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 4
      MaxValue = 2099
      MinValue = 1900
      Value = 1900
      StoredValues = 48
    end
    object dxSpinEditYearTo: TdxSpinEdit
      Left = 304
      Top = 70
      Width = 65
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 5
      MaxValue = 2099
      MinValue = 1900
      Value = 1900
      StoredValues = 48
    end
  end
  object TableCopy: TTable
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    TableName = 'SHIFTSCHEDULE'
    Left = 456
    Top = 136
  end
end
