inherited UserF: TUserF
  Left = 247
  Top = 149
  Width = 634
  Height = 472
  Caption = 'Users'
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlMasterGrid: TPanel
    Width = 618
    Height = 71
    TabOrder = 0
    Visible = False
    inherited spltMasterGrid: TSplitter
      Top = 67
      Width = 616
    end
    inherited dxMasterGrid: TdxDBGrid
      Width = 616
      Height = 66
      Visible = False
    end
  end
  inherited pnlDetail: TPanel
    Top = 260
    Width = 618
    Height = 173
    TabOrder = 3
    object GroupBoxDetail: TGroupBox
      Left = 1
      Top = 1
      Width = 624
      Height = 171
      Align = alLeft
      Caption = 'Users'
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 368
        Top = 16
        Width = 249
        Height = 153
        Caption = 'Team selection'
        TabOrder = 0
        object dxDBCheckEditTeamSelection: TdxDBCheckEdit
          Left = 16
          Top = 32
          Width = 121
          TabOrder = 0
          Caption = 'Team selection'
          DataField = 'SET_TEAM_SELECTION_YN'
          DataSource = UserDM.DataSourceDetail
          ValueChecked = 'Y'
          ValueUnchecked = 'N'
          OnChange = dxDBCheckEditTeamSelectionChange
        end
        object ButtonTeamSelection: TButton
          Left = 20
          Top = 64
          Width = 149
          Height = 25
          Caption = 'Show/define team selection'
          TabOrder = 1
          OnClick = ButtonTeamSelectionClick
        end
      end
      object GroupBoxUserInf: TGroupBox
        Left = 8
        Top = 14
        Width = 353
        Height = 155
        Caption = 'User information'
        TabOrder = 1
        object Label1: TLabel
          Left = 8
          Top = 20
          Width = 58
          Height = 13
          Caption = 'Group name'
        end
        object Label3: TLabel
          Left = 8
          Top = 49
          Width = 51
          Height = 13
          Caption = 'User name'
        end
        object Label2: TLabel
          Left = 8
          Top = 77
          Width = 46
          Height = 13
          Caption = 'Password'
        end
        object Label4: TLabel
          Left = 8
          Top = 106
          Width = 86
          Height = 13
          Caption = 'Confirm Password'
        end
        object dxDBLookupEditGroup: TdxDBLookupEdit
          Tag = 1
          Left = 107
          Top = 18
          Width = 232
          Style.BorderStyle = xbsSingle
          TabOrder = 0
          DataField = 'GROUPLU'
          DataSource = UserDM.DataSourceDetail
          DropDownRows = 4
          ListFieldName = 'DESCRIPTION;GROUP_NAME'
        end
        object DBEditName: TDBEdit
          Tag = 1
          Left = 107
          Top = 45
          Width = 230
          Height = 19
          CharCase = ecUpperCase
          Ctl3D = False
          DataField = 'USER_NAME'
          DataSource = UserDM.DataSourceDetail
          ParentCtl3D = False
          TabOrder = 1
        end
        object DBEditPassword: TDBEdit
          Tag = 1
          Left = 107
          Top = 73
          Width = 230
          Height = 19
          Ctl3D = False
          DataField = 'USER_PASSWORD'
          DataSource = UserDM.DataSourceDetail
          ParentCtl3D = False
          PasswordChar = '*'
          TabOrder = 2
        end
        object dxEditConfirmPassword: TdxEdit
          Tag = 1
          Left = 107
          Top = 100
          Width = 230
          Style.BorderStyle = xbsSingle
          TabOrder = 3
          PasswordChar = '*'
        end
        object dxDBCheckEditSysAdmin: TdxDBCheckEdit
          Left = 104
          Top = 128
          Width = 241
          TabOrder = 4
          Caption = 'System Administrator'
          DataField = 'SYSADMIN_YN'
          DataSource = UserDM.DataSourceDetail
          ValueChecked = 'Y'
          ValueUnchecked = 'N'
        end
      end
    end
  end
  inherited pnlDetailGrid: TPanel
    Tag = 1
    Top = 97
    Width = 618
    Height = 163
    inherited spltDetail: TSplitter
      Top = 159
      Width = 616
    end
    inherited dxDetailGrid: TdxDBGrid
      Tag = 1
      Width = 616
      Height = 158
      Bands = <
        item
          Alignment = taLeftJustify
          Caption = 'Users'
        end>
      DefaultLayout = False
      KeyField = 'USER_NAME'
      Color = clWhite
      BandColor = clInactiveBorder
      ShowBands = True
      OnChangeNode = dxDetailGridChangeNode
      object dxDetailGridColumn1: TdxDBGridColumn
        Caption = 'Group name'
        Width = 63
        BandIndex = 0
        RowIndex = 0
        FieldName = 'GROUP_NAME'
      end
      object dxDetailGridColumn2: TdxDBGridColumn
        Caption = 'User Name'
        Width = 116
        BandIndex = 0
        RowIndex = 0
        FieldName = 'USER_NAME'
      end
    end
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = False
        WholeRow = False
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <
          item
            Item = dxBarButtonFirst
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonPrior
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonNext
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonLast
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarBDBNavInsert
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavDelete
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavPost
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavCancel
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonEditMode
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonRecordDetails
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSort
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSearch
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCustCol
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonShowGroup
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExpand
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCollapse
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonResetColumns
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonExportAllHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportAllXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    DockControlHeights = (
      0
      0
      26
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
  end
end
