(*
  MRA:28-FEB-2011 RV087.3. Dialog Login Change
  - Dialog Login
    - The Dialog Login is kept open all the time,
      because of 2 variables:
      - UserTeamSelectionYN
      - LoginUser
    - This must be changed by storing them in SystemDM.
*)
program AdmTool;

uses
  Forms,
  Dialogs,
  AboutFRM in '..\..\Pims Shared\AboutFRM.pas' {AboutF},
  LogoPimsFRM in '..\..\Pims Shared\LogoPimsFRM.pas' {PimsLogoF},
  TopMenuBaseFRM in '..\..\Pims Shared\TopMenuBaseFRM.pas' {TopMenuBaseF},
  TopPimsLogoFRM in '..\..\Pims Shared\TopPimsLogoFRM.pas' {TopPimsLogoF},
  GridBaseFRM in '..\..\Pims Shared\GridBaseFRM.pas' {GridBaseF},
  GridBaseDMT in '..\..\Pims Shared\GridBaseDMT.pas' {GridBaseDM: TDataModule},
  SideMenuBaseFRM in '..\..\Pims Shared\SideMenuBaseFRM.pas' {SideMenuBaseF},
  WaitFRM in '..\..\Pims Shared\WaitFRM.pas' {WaitF},
  UPIMSVersion in '..\..\Pims Shared\UPIMSVersion.pas',
  DialogSelectionFRM in '..\..\Pims Shared\DialogSelectionFRM.PAS' {DialogSelectionF},
  UPimsConst in '..\..\Pims Shared\UPimsConst.pas',
  UPimsMessageRes in '..\..\Pims Shared\UPimsMessageRes.pas',
  SystemDMT in '..\..\Pims Shared\SystemDMT.pas' {SystemDM: TDataModule},
  DialogLoginFRM in '..\..\Pims Shared\DialogLoginFRM.pas' {DialogLoginF},
  DialogLoginDMT in '..\..\Pims Shared\DialogLoginDMT.pas' {DialogLoginDM: TDataModule},
  ListProcsFRM in '..\..\Pims Shared\ListProcsFRM.pas' {ListProcsF},
  EncryptIt in '..\..\Pims Shared\EncryptIt.pas',
  ULoginConst in '..\..\Pims Shared\ULoginConst.pas',
  OrderInfoFRM in '..\..\Pims Shared\OrderInfoFRM.pas' {OrderInfoF},
  ReportBaseDMT in '..\..\Pims\Source\ReportBaseDMT.pas' {ReportBaseDM: TDataModule},
  UGlobalFunctions in '..\..\Pims Shared\UGlobalFunctions.pas',
  PimsFRM in '..\..\Pims Shared\PimsFRM.pas' {PimsF},
  CalculateTotalHoursDMT in '..\..\Pims Shared\CalculateTotalHoursDMT.pas' {CalculateTotalHoursDM: TDataModule},
  DialogCopyUserGroupFRM in 'DialogCopyUserGroupFRM.pas' {DialogCopyUserGroupF},
  HomeAdmFRM in 'HomeAdmFRM.pas' {HomeAdmF},
  MenuGroupDMT in 'MenuGroupDMT.pas' {MenuGroupDM: TDataModule},
  MenuGroupFRM in 'MenuGroupFRM.pas' {MenuGroupF},
  SideMenuUnitMaintenanceAdmFRM in 'SideMenuUnitMaintenanceAdmFRM.pas' {SideMenuUnitMaintenanceAdmF},
  SplashFRM in 'SplashFRM.pas' {SplashF},
  UAdmMessageRes in 'UAdmMessageRes.pas',
  TeamPerUserFRM in 'TeamPerUserFRM.pas' {TeamPerUserF},
  UserDMT in 'UserDMT.pas' {UserDM: TDataModule},
  UserFRM in 'UserFRM.pas' {UserF},
  UserGroupFRM in 'UserGroupFRM.pas' {UserGroupF},
  UserGroupDMT in 'UserGroupDMT.pas' {UserGroupDM: TDataModule},
  UScannedIDCard in '..\..\Pims Shared\UScannedIDCard.pas',
  GlobalDMT in '..\..\Pims Shared\GlobalDMT.pas' {GlobalDM: TDataModule},
  MessageDialogFRM in 'MessageDialogFRM.pas' {MessageDialogF},
  UTimeZone in '..\..\Pims Shared\UTimeZone.pas',
  DialogSelectPrinterFRM in 'DialogSelectPrinterFRM.pas' {DialogSelectPrinterF},
  DialogBaseFRM in '..\..\Pims Shared\DialogBaseFRM.pas' {DialogBaseF};

var
  PasswordOK: Boolean;

{$R *.RES}
begin
  Application.Initialize;
  SplashF := TSplashF.Create(Application);
  Starting := True;
  SplashF.Show;
  SplashF.Update;

  Application.Title := 'ORCL - ADMINISTRATION - TOOL';
  Application.CreateForm(TSystemDM, SystemDM);
  SystemDM.AppName := 'AdmTool';
  Application.CreateForm(TGlobalDM, GlobalDM);

  DialogLoginF := TDialogLoginF.Create(Application);
  PasswordOK := DialogLoginF.VerifyPassword;
  DialogLoginF.Hide;
  DialogLoginF.Close;
  DialogLoginF.Release;

//  if DialogLoginF.VerifyPassword then
  if PasswordOK then
  begin
    Application.CreateForm(TSideMenuUnitMaintenanceAdmF,
      SideMenuUnitMaintenanceAdmF);
  end;

  SplashF.Hide;
  SplashF.Close;
  SplashF.Release;
  Starting := False;
  Application.Run;
end.
