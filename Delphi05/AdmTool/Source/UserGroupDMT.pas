unit UserGroupDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseDMT, Db, DBTables;

type
  TUserGroupDM = class(TGridBaseDM)
    TableMasterPLANTLU: TStringField;
    TableMasterBUSINESSUNIT_CODE: TStringField;
    TableMasterDESCRIPTION: TStringField;
    TableMasterPLANT_CODE: TStringField;
    TableMasterCREATIONDATE: TDateTimeField;
    TableMasterBONUS_FACTOR: TFloatField;
    TableMasterNORM_ILL_VS_DIRECT_HOURS: TFloatField;
    TableMasterMUTATIONDATE: TDateTimeField;
    TableMasterMUTATOR: TStringField;
    TableMasterNORM_ILL_VS_INDIRECT_HOURS: TFloatField;
    TableMasterNORM_ILL_VS_TOTAL_HOURS: TFloatField;
    TableDetailGROUP_NAME: TStringField;
    TableDetailDESCRIPTION: TStringField;
    TableDetailCREATIONDATE: TDateTimeField;
    TableDetailMUTATIONDATE: TDateTimeField;
    TableDetailMUTATOR: TStringField;
    QuerySelect: TQuery;
    TablePimsGroup: TTable;
    TableGroupMenu: TTable;
    QuerySelectUserGroups: TQuery;
    QueryExec: TQuery;
    procedure TableDetailFilterRecord(DataSet: TDataSet;
      var Accept: Boolean);
    procedure DataModuleCreate(Sender: TObject);
    procedure TableDetailBeforeDelete(DataSet: TDataSet);
    procedure TableDetailAfterScroll(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure FillDefaultMenu(GROUP_NAME: String);
    procedure FillDefaultMenu_AllUserGroups;
    procedure CreateDefaultGroupMenu(Group_Name, EditAdmToolYN: String);
    procedure CreateDefaultMenuItem(Group_Name, Menu_Description: String;
      VisibleYN, EditYN, ExpantYN: String;
      SEQ_NUMBER, Menu_Number, PARENT_MENU_NUMBER: Integer);
    procedure CreateDefaultRecordsSYSDBA(User_Name, Group_Name,
      Password, Description_Group: String);
  end;

var
  UserGroupDM: TUserGroupDM;

implementation

uses  UPimsConst,  UAdmConst, UAdmMessageRes, SystemDMT, UserGroupFRM;

{$R *.DFM}
procedure TUserGroupDM.CreateDefaultMenuItem(Group_Name, Menu_Description: String;
  VisibleYN, EditYN, ExpantYN: String;
  SEQ_NUMBER, Menu_Number, PARENT_MENU_NUMBER: Integer);
begin
  if not TableGroupMenu.FindKey([Group_Name, Menu_Number]) then
  begin
    TableGroupMenu.Insert;
    TableGroupMenu.FieldByName('MENU_NUMBER').Value := Menu_Number;
    TableGroupMenu.FieldByName('DESCRIPTION').Value := Menu_Description;
    TableGroupMenu.FieldByName('GROUP_NAME').Value := Group_Name;
    TableGroupMenu.FieldByName('VISIBLE_YN').Value := VisibleYN;
    TableGroupMenu.FieldByName('EDIT_YN').Value := EditYN;
    TableGroupMenu.FieldByName('EXPAND_YN').Value := ExpantYN;
    TableGroupMenu.FieldByName('SEQ_NUMBER').Value := SEQ_NUMBER;
    TableGroupMenu.FieldByName('PARENT_MENU_NUMBER').Value := PARENT_MENU_NUMBER;
    TableGroupMenu.Post;
  end
  else
  begin
  // this part is use for updating a group_name with values of ADMINGROUP
    if (Group_Name <> ADMINGROUP) then
    begin
      TableGroupMenu.Edit;
      TableGroupMenu.FieldByName('DESCRIPTION').Value := Menu_Description;
      TableGroupMenu.FieldByName('EXPAND_YN').Value := ExpantYN;
      TableGroupMenu.FieldByName('SEQ_NUMBER').Value := SEQ_NUMBER;
      TableGroupMenu.FieldByName('PARENT_MENU_NUMBER').Value := PARENT_MENU_NUMBER;
      TableGroupMenu.Post;
    end;
  end;
end;

procedure TUserGroupDM.CreateDefaultGroupMenu(Group_Name, EditAdmToolYN: String);
begin

// create PIMS Tree
  CreateDefaultMenuItem(Group_Name, MenuDefaultItems[1],
    CHECKEDVALUE, EditAdmToolYN, UNCHECKEDVALUE, 1, MENU_NUMBER_APPLICATIONS, 0);

// create Adm - menu item
  CreateDefaultMenuItem(Group_Name, MenuDefaultItems[2],
    CHECKEDVALUE, EditAdmToolYN, UNCHECKEDVALUE, 1, MENU_NUMBER_ADMINISTRATOR,
      MENU_NUMBER_APPLICATIONS);
// create pims - menu item
  CreateDefaultMenuItem(Group_Name, MenuDefaultItems[3],
    CHECKEDVALUE, CHECKEDVALUE, CHECKEDVALUE, 2, MENU_NUMBER_PIMS,
    MENU_NUMBER_APPLICATIONS);
// create production screen menu item
  CreateDefaultMenuItem(Group_Name, MenuDefaultItems[4],
    CHECKEDVALUE, CHECKEDVALUE, UNCHECKEDVALUE, 3, MENU_NUMBER_PRODUCTION_SCREEN,
    MENU_NUMBER_APPLICATIONS);
end;

procedure TUserGroupDM.CreateDefaultRecordsSYSDBA(User_Name, Group_Name,
  Password, Description_Group: String);
begin
  if not TablePimsGroup.FindKey([Group_Name]) then
  begin
    TablePimsGroup.Insert;
    TablePimsGroup.FieldByName('DESCRIPTION').Value := Description_Group;
    TablePimsGroup.FieldByName('GROUP_NAME').Value := Group_Name;
    TablePimsGroup.Post;
    CreateDefaultGroupMenu(Group_Name, CHECKEDVALUE);
  end;
end;

procedure TUserGroupDM.TableDetailFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  inherited;
  if (SystemDM.LoginGroup <> ADMINGROUP) then
    Accept := (DataSet.FieldByName('GROUP_NAME').Value <> ADMINGROUP);
end;

procedure TUserGroupDM.DataModuleCreate(Sender: TObject);
begin
  inherited;
  // add default record of ADMINGROUP
  TablePimsGroup.Open;
  TableGroupMenu.Open;
  CreateDefaultRecordsSYSDBA(ADMINUSER, ADMINGROUP, ADMINPASSWORD,
    SAdmDescAdminGroup)
end;

procedure TUserGroupDM.TableDetailBeforeDelete(DataSet: TDataSet);
begin
  inherited;
  DefaultBeforeDelete(DataSet);
  if (DataSet.FieldByName('GROUP_NAME').AsString = SystemDM.LoginGroup) or
    (DataSet.FieldByName('GROUP_NAME').AsString = ADMINGROUP)  then
  begin
    DisplayMessage(SAdmNotDeleteGroup, mtWarning,  [mbOk]);
    Abort;
  end;
end;

procedure  TUserGroupDM.FillDefaultMenu_AllUserGroups;
begin
  QuerySelectUserGroups.Open;
  QuerySelectUserGroups.First;
  while Not QuerySelectUserGroups.Eof do
  begin
    FillDefaultMenu(QuerySelectUserGroups.FieldByName('GROUP_NAME').AsString);
    QuerySelectUserGroups.Next;
  end;
  QuerySelectUserGroups.Close;
end;

// fill default menu for group <> ADMINGROUP the menu tree has to contains
//the same node only Visible, Edit fields could be diferent
procedure TUserGroupDM.FillDefaultMenu(GROUP_NAME: String);
begin
// check for records not equal
  if GROUP_NAME = ADMINGROUP then
    Exit;

  QuerySelect.Close;
  QuerySelect.Sql.Clear;
  QuerySelect.Sql.Add(
    'DELETE FROM PIMSMENUGROUP WHERE GROUP_NAME = ''' +
    GROUP_NAME + '''' + ' AND ' +
    'MENU_NUMBER NOT IN ( SELECT MENU_NUMBER FROM PIMSMENUGROUP ' +
    'WHERE GROUP_NAME = ''' + ADMINGROUP + ''')');
  QuerySelect.ExecSQL;
  
  QuerySelect.Close;
  QuerySelect.Sql.Clear;
  QuerySelect.Sql.Add(
    'SELECT ' +
    '  GROUP_NAME, MENU_NUMBER, VISIBLE_YN, ' +
    '  EDIT_YN, EXPAND_YN, PARENT_MENU_NUMBER, ' +
    '  DESCRIPTION, SEQ_NUMBER ' +
    'FROM ' +
    '  PIMSMENUGROUP ' +
    'WHERE ' +
    '  GROUP_NAME = ''' + ADMINGROUP + '''');
  QuerySelect.Open;
  while not QuerySelect.Eof do
  begin
    CreateDefaultMenuItem(Group_Name,
      QuerySelect.FieldByName('DESCRIPTION').Value,
      CHECKEDVALUE, UNCHECKEDVALUE,
      QuerySelect.FieldByName('EXPAND_YN').Value,
      QuerySelect.FieldByName('SEQ_NUMBER').Value,
      QuerySelect.FieldByName('MENU_NUMBER').Value,
      QuerySelect.FieldByName('PARENT_MENU_NUMBER').Value);
    QuerySelect.Next;
  end;
end;

procedure TUserGroupDM.TableDetailAfterScroll(DataSet: TDataSet);
begin
  inherited;
  if TableDetail.FieldByName('GROUP_NAME').AsString = ADMINGROUP then
    UserGroupF.DBEditGroupName.Enabled := False
  else
    UserGroupF.DBEditGroupName.Enabled := True;
end;

end.
