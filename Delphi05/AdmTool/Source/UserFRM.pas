(*
  MRA:21-FEB-2014 20011800
  - Final Run System
  - Addition System Administrator-checkbox
  MRA:10-NOV-2014 20013438.60 Rework
  - When checkbox 'System Administrator' is checked or unchecked
    it gives a message about the teams-settings. It should not
    do this.
*)
unit UserFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseFRM, Db, ActnList, dxBarDBNav, dxBar, dxCntner, dxTL, dxDBCtrl,
  dxDBGrid, ExtCtrls, StdCtrls, Mask, DBCtrls, DBTables, dxEditor,
  dxExEdtr, dxDBEdtr, dxDBELib, dxEdLib;

const
  ValidKeys  : set of Char = [#08, #13] + [#48..#57];
type

  TUserF = class(TGridBaseF)
    dxDetailGridColumn1: TdxDBGridColumn;
    dxDetailGridColumn2: TdxDBGridColumn;
    GroupBoxDetail: TGroupBox;
    GroupBox1: TGroupBox;
    dxDBCheckEditTeamSelection: TdxDBCheckEdit;
    ButtonTeamSelection: TButton;
    GroupBoxUserInf: TGroupBox;
    Label1: TLabel;
    dxDBLookupEditGroup: TdxDBLookupEdit;
    Label3: TLabel;
    DBEditName: TDBEdit;
    Label2: TLabel;
    DBEditPassword: TDBEdit;
    Label4: TLabel;
    dxEditConfirmPassword: TdxEdit;
    dxDBCheckEditSysAdmin: TdxDBCheckEdit;
    procedure dxBarBDBNavInsertClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
//    procedure dxBarBDBNavPostClick(Sender: TObject);
    procedure dxDetailGridChangeNode(Sender: TObject; OldNode,
      Node: TdxTreeListNode);
    procedure FormShow(Sender: TObject);
    procedure dxDBCheckEditTeamSelectionChange(Sender: TObject);
    procedure ButtonTeamSelectionClick(Sender: TObject);
  private
    { Private declarations }
    InsertForm: TForm;
  public
    { Public declarations }
  end;

function UserF: TUserF;

implementation

{$R *.DFM}

uses
  SystemDMT, UserDMT, UPimsConst, UAdmMessageRes,
  TeamPerUserFRM;

var
  UserF_HND: TUserF;

function UserF: TUserF;
begin
  if (UserF_HND = nil) then
    UserF_HND := TUserF.Create(Application);
  Result := UserF_HND;
end;

procedure TUserF.dxBarBDBNavInsertClick(Sender: TObject);
begin
  inherited;

  dxDBLookupEditGroup.SetFocus;
end;

procedure TUserF.FormDestroy(Sender: TObject);
begin
  inherited;
  UserF_HND := nil;
end;

procedure TUserF.FormCreate(Sender: TObject);
begin
  UserDM := CreateFormDM(TUserDM);
  if dxDetailGrid.DataSource = Nil then
    dxDetailGrid.DataSource := UserDM.DataSourceDetail;
  inherited;
  if SystemDM.AplEditYN = UNCHECKEDVALUE then
  begin
    dxBarBDBNavInsert.Visible := ivNever;
    dxBarBDBNavDelete.Visible := ivNever;
    dxBarBDBNavPost.Visible := ivNever;
    dxBarBDBNavCancel.Visible := ivNever;
    dxBarButtonEditMode.Visible := ivNever;
    GroupBoxUserInf.Enabled := False;
    dxDBCheckEditTeamSelection.Enabled := False;
   end;
   // 20011800
   dxDBCheckEditSysAdmin.Enabled := False;
   if SystemDM.CurrentLoginUser = ADMINUSER then
     dxDBCheckEditSysAdmin.Enabled := True;
end;

procedure TUserF.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
{  if InsertForm.Visible then
    InsertForm.Close;}
  Action := caFree;
end;

// MR:13-05-2005
// This will only be called if user presses the save-key, not
// if he closes the form.
// Moved to 'BeforePost' in 'UserDMT'.
(*
procedure TUserF.dxBarBDBNavPostClick(Sender: TObject);
begin
  inherited;
  if (dxEditConfirmPassword.Text <> DBEditPassword.Text) then
  begin
    DisplayMessage(SAplPasswordConfirm, mtInformation, [mbOk]);
    DBEditPassword.SetFocus;
  end
  else
    UserDM.TableDetail.Post;
end;
*)
procedure TUserF.dxDetailGridChangeNode(Sender: TObject; OldNode,
  Node: TdxTreeListNode);
begin
  inherited;
  dxEditConfirmPassword.Text :=
    UserDM.TableDetail.FieldByName('USER_PASSWORD').AsString;
  ButtonTeamSelection.Enabled :=
   (UserDM.TableDetail.FieldByName('SET_TEAM_SELECTION_YN').AsString =
    CHECKEDVALUE);
end;

procedure TUserF.FormShow(Sender: TObject);
begin
  inherited;
  InsertForm := Nil;
  dxEditConfirmPassword.Text :=  DBEditPassword.Text;
  ButtonTeamSelection.Enabled :=
   (UserDM.TableDetail.FieldByName('SET_TEAM_SELECTION_YN').AsString =
    CHECKEDVALUE);
end;

procedure TUserF.dxDBCheckEditTeamSelectionChange(Sender: TObject);
begin
  inherited;
  ButtonTeamSelection.Enabled := dxDBCheckEditTeamSelection.Checked ;
  if (not dxDBCheckEditTeamSelection.Checked ) then
  begin
    DisplayMessage(SAdmTeamSelection + ' ' +
      DBEditName.Text, mtInformation, [mbOk]);
  end;
end;

procedure TUserF.ButtonTeamSelectionClick(Sender: TObject);
begin
  inherited;
  if dxBarBDBNavPost.Enabled then
  begin
    DisplayMessage(SAdmSaveBefore, mtInformation, [mbOk]);
    Exit;
  end;
  UserDM.TableTeamPerUser.Refresh;
  InsertForm := TeamPerUserF;
  InsertForm.ShowModal;
end;

end.
