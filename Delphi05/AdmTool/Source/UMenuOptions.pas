unit UMenuOptions;

interface

type
  TPimsMenuSet    = set of byte;
  TPimsVersionSet = set of byte;

const
  MENU_ITEM_USERGROUP                    = 1;
  MENU_ITEM_USER                         = 2;
  MENU_ITEM_MENUGROUP                    = 3;

  GROUP_ADMIN_USER                       = 0;
  MENU_GROUPS_ADMIN: TPimsMenuSet = [GROUP_ADMIN_USER];

  MENU_OPTION_USER          = 0;
 
implementation

end.
