unit MenuGroupDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseDMT, DBTables, Db;

type
  TMenuGroupDM = class(TGridBaseDM)
    TableDetailGROUP_NAME: TStringField;
    TableDetailMENU_NUMBER: TIntegerField;
    TableDetailVISIBLE_YN: TStringField;
    TableDetailEDIT_YN: TStringField;
    TableDetailEXPAND_YN: TStringField;
    TableDetailPARENT_MENU_NUMBER: TIntegerField;
    TableDetailDESCRIPTION: TStringField;
    TableDetailSEQ_NUMBER: TIntegerField;
    TableDetailCREATIONDATE: TDateTimeField;
    TableDetailMUTATIONDATE: TDateTimeField;
    TableDetailMUTATOR: TStringField;
    QueryDelete: TQuery;
    TableGroupMenu: TTable;
    procedure TableDetailFilterRecord(DataSet: TDataSet;
      var Accept: Boolean);
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
   
    procedure InsertRecordInMenuGroup(Parent_Menu_Number, Menu_Number,
      Seq_Number: Integer;
      Description, Expand_YN, Edit_YN, VISIBLE_YN: String);
  end;

var
  MenuGroupDM: TMenuGroupDM;

implementation

uses MenuGroupFRM, SystemDMT;

{$R *.DFM}

procedure TMenuGroupDM.TableDetailFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  inherited;
  Accept := (DataSet.FieldByName('GROUP_NAME').Value = MenuGroupF.GroupName);
end;


procedure TMenuGroupDM.InsertRecordInMenuGroup(
  Parent_Menu_Number, Menu_Number,  Seq_Number: Integer;
  Description, Expand_YN, Edit_YN, VISIBLE_YN: String);
begin
  if not TableGroupMenu.FindKey([ MenuGroupF.GroupName, Menu_Number]) then
  begin
    TableGroupMenu.Insert;
    TableGroupMenu.FieldByName('GROUP_NAME').Value := MenuGroupF.GroupName;
    TableGroupMenu.FieldByName('PARENT_MENU_NUMBER').Value := Parent_Menu_Number;
    TableGroupMenu.FieldByName('MENU_NUMBER').Value := Menu_Number;
    TableGroupMenu.FieldByName('SEQ_NUMBER').Value := Seq_Number;
    TableGroupMenu.FieldByName('EXPAND_YN').Value := Expand_YN;
    TableGroupMenu.FieldByName('EDIT_YN').Value := Edit_YN;
    TableGroupMenu.FieldByName('VISIBLE_YN').Value := VISIBLE_YN;
    TableGroupMenu.FieldByName('DESCRIPTION').Value := Description;
    TableGroupMenu.Post;
  end
  else
  begin
    TableGroupMenu.Edit;
    TableGroupMenu.FieldByName('PARENT_MENU_NUMBER').Value := Parent_Menu_Number;
    TableGroupMenu.FieldByName('SEQ_NUMBER').Value := Seq_Number;
    TableGroupMenu.FieldByName('EXPAND_YN').Value := Expand_YN;
    TableGroupMenu.FieldByName('EDIT_YN').Value := Edit_YN;
    TableGroupMenu.FieldByName('VISIBLE_YN').Value := VISIBLE_YN;
    TableGroupMenu.FieldByName('DESCRIPTION').Value := Description;
    TableGroupMenu.Post;
  end;
end;



procedure TMenuGroupDM.DataModuleCreate(Sender: TObject);
begin
  inherited;
  TableGroupMenu.Open;

end;

end.
