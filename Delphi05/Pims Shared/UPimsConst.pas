(*
  MRA:28-MAY-2013 New look Pims
  - Some pictures/colors are changed.
  MRA:23-SEP-2014 20015586
  - Added 2 constants:
    - DOWNJOB_1 (Mechanical down)
    - DOWNJOB_2 (No Merchandize)
  MRA:10-NOV-2014 20014826
  - Log error messages optionally to database.
  MRA:24-JUN-2015 20014450.50
  - Real Time Eff.
  MRA:28-MAY-2018 GLOB3-94
  - Added const for Record/Key deleted
  MRA:15-JUN-2018 GLOB3-60
  - Extension 4 to 10 blocks for planning, added const MAX_TBS.
*)
unit UPimsConst;

interface

uses
  Graphics, Classes, ButtonWithColor;

const
  // DBIERR translated errors
  PIMS_POSTING                   = 1000;
  PIMS_DELETING                  = 1001;
  PIMS_EDITING                   = 1002;
  PIMS_DBIERR_LOCKED             = 10241;
  PIMS_DBIERR_GENERAL_SQL_ERROR  = 13059;
  PIMS_DBIERR_KEYVIOL            = 9729;
  PIMS_DBIERR_REQDERR            = 9732;
  PIMS_DBIERR_FORIEGNKEYERR      = 9733;
  PIMS_DBIERR_DETAILRECORDSEXIST = 9734;
  PIMS_RECORD_KEY_DELETED        = 8708;
//---------------------begin------exceptions--------------------------//
  PIMS_EX_UNIQUEFIELD            = 1;
  PIMS_EX_STARTENDDATE           = 2;
  PIMS_EX_STARTENDTIME           = 3;
  PIMS_EX_DELETE_LAST_RECORD     = 4;
  PIMS_EX_MAX_RECORD             = 5;
  PIMS_EX_DATEINPREVIOUSPERIOD   = 6;
  PIMS_EX_CHECKILLMSGCLOSE       = 7;
  PIMS_EX_CHECKILLMSGSTARTDATE   = 8;
  PIMS_EX_CHECKEMPLCONTRDATEPREVIOUS = 9;
//--------------------end---------exceptions--------------------------//
  // Stock related data { required fields }

  // MR:18-02-2005 NewLine, for use in Select-statements.
  NL = #32#13#10;

  // Amount of characters to show for the barcode
  DISPLAYLENGTH = 15;

  // PIMS Registry
  // MR: Changed for Pims 'ABS\Solar' to 'ABS\Pims'
  PIMS_REGROOT_PATH_GRID         = '\Software\ABS\Pims\Grids\';
  PIMS_REGROOT_PATH_SYS          = '\Software\ABS\Pims\System\';

   // 20014289 // 20014450.50
   clPimsBlue                       = $00C89800; // $00996F58; // 20014450.50
   clPimsLBlue                      = $00FFEFDD; // light blue
   clRequired                       = clPimsLBlue; // $00EADED9; { soft violet } // 20014450.50
   clPimsDarkBlue                   = $00977200; // 20014450.50
   clHighLight                      = clPimsDarkBlue; // 20014450.50

  // grid colours
  clTreeParent                    = $00FFF0DD; { solar blue }
//  clSecondLine                    = clInfoBk;  { windows setting std: soft yellow }
//  clInfoBk is the Windows Hint color; too many custmers have their own scheme
//  so now its hardcoded
  clSecondLine                    = $00E1FFFF; { soft yellow }
//  clRequired                      = $00FFD9EC; { soft violet }
//  clDisabled                      = $00FFF0DD; { solar blue }
//  clDisabled                      = clInfoBk;  { windows setting std: soft yellow }
  clDisabled                      = $00E1FFFF; { soft yellow }
  clGridEdit                      = $00EECCFF; { soft red }
  clGridNoFocus                   = clPimsLBlue; // $00C4FFC4; { soft green } // 20014450.50
  clFontReadOnly                  = clGrayText;{ windows settings std: gray }
  // scan colours
 // clScanOk                        = $00A5F7AD; { green }
   clScanError                     = $007474FD; { red }
//  clScanFlag                      = $0065F1E9; { yellow }
//  clScanIn                        = $00A4F4A8; { green }
//  clScanRepair                    = $00ECB85C; { blue }
   clOrange                        = $0000CCFF; { orange }
   clScanRouteP1                   = $00DDDDDD; { light gray }
   clScanScrap                     = $009E9E9E; { gray }
//  clScanStockNew                  = $00FFD9EC; { soft violet }
//  clScanStockUsed                 = $00FFF0DD; { solar blue }
   clScanOut                       = $009E9CF4; { red }
   clScanPToAStock                  = $00C4FFC4; { soft green }
//  clRouteGridFocus                = $002020F0; { red }
//  clRouteArticleNoOrder           = $00E1FFFF; { soft yellow }
//  clRouteArticleNotStarted        = $00FFF0DD; { solar blue }
//  clRouteArticleBusy              = clScanRewash; //$0000CCFF; { orange }
//  clRouteArticleFinished          = clScanOk; //$00A5F7AD; { green }
   clOtherPlant                     = clScanError; // Order 550349
   clDarkRed                        = $002525A7; // PIM-250

  // Navigation in the grids
  NAV_FIRST  = 0;
  NAV_PRIOR  = 1;
  NAV_NEXT   = 2;
  NAV_LAST   = 3;


//PIMS
  CHECKEDVALUE                    = 'Y';
  UNCHECKEDVALUE                  = 'N';
  PERMANENT                       = 1;
  TEMPORARY                       = 2;
  PARTTIME                        = 3;

// Settings
  DETAILEDPRODDEFAULT     = 52;
  DAYPRODINFODEFAULT      = 52;
  UNITWEIGHTDEFAULT       = 'Kg';
  UNITPIECESDEFAULT       = 'pcs';
  CURRENTUSER             = 'PimsUSR1';
  PASSWORDDEFAULT         = 'pimsabs';
  WEEKDAYSDEFAULT         = 2;
  FIRSTWEEKOFYEARDEFAULT  = 1;
  HOURSONSTARTDAYDEFAULT  = CHECKEDVALUE;
  VERSIONNUMBERDEFAULT    = 0;
  CHANGESCANYNDEFAULT     = UNCHECKEDVALUE;
  TIMERECORDINGSTATIONDEFAULT = UNCHECKEDVALUE;

  // MR:14-07-2004 Following is not used... it was also using 'UStrings', that
  // also isn't used anymore.
(*
  DAYSDESCRIPTIONWEEK: Array[1..7] of string  =
   (SPimsMonday, SPimsTuesday, SPimsWednesday, SPimsThursday, SPimsFriday,
   	SPimsSaturday, SPimsSunday);
  DAYSCODEWEEK: Array[1..7] of string =
   (SPimsMondayShort, SPimsTuesdayShort, SPimsWednesdayShort, SPimsThursdayShort,
    SPimsFridayShort, SPimsSaturdayShort, SPimsSundayShort);
*)

// Time Recording
  LONGDATE     	= 'dddddd - hh : nn : ss';
  NullStr = '';
  NullInt = 0;
  NullDate = 0;
  DayToMin = 24*60;
  MaxTimeBlocks = 4;
  DUMMYSTR = '0';
  DummyWKStr = 'Dummy Workspot';
  NullTime = '00:00';
  LevelA = 'A';
  LevelB = 'B';
  LevelC = 'C';


 // Hours Per Employee
  HOLIDAYAVAILABLETB              = 'H';
  TIMEFORTIMEAVAILABILITY         = 'T';
  WORKTIMEREDUCTIONAVAILABILITY   = 'W';
  //!!
  HOLIDAYPREVIOUSYEARAVAILABILITY = 'R';
  SENIORITYHOLIDAYAVAILABILITY    = 'E';
  ADDBANKHOLIDAYAVAILABILITY      = 'F';
  SATCREDITAVAILABILITY           = 'S';
  BANKHOLIDAYRESERVEAVAILABILITY  = 'C';
  SHORTERWORKWEEKAVAILABILITY     = 'D';
  // 20013183
  TRAVELTIMEAVAILABILITY          = 'N';
  // Illness Messages
  DEFAULTAVAILABILITYCODE = '*';

 // Staff Availability
  ALLTEAMS = 'All Teams';
  ALLPLANTS = 'All Plants';

  // Absence Types
  ILLNESS        = 'I';
  PAID           = 'P';
  UNPAID         = 'U';
  RSNB           = 'B';
  HOLIDAY        = 'H';
  WTR            = 'W';
  LABOURTHERAPY  = 'L';
  // RV035.1. Two extra absence types:
  MATERNITYLEAVE = 'M';
  LAY_DAYS       = 'A';
  // RV071.11 550497 Extra absence type:
  TIMECREDIT     = 'J';
  // 20013183
  TRAVELTIME     = 'N';
  USED_TRAVELTIME = 'M'; // 20013183 Used in GlobalFunctions.

  CHR_SEP = CHR(ORD(254));

  UNAVAILABLE = '-';
// name of temporary tables
  TMPSTAFFPLANNINGEMP = 'PIVOTEMPSTAFFPLANNING';
  TMPSTAFFPLANNINGOCI = 'PIVOTOCISTAFFPLANNING';
  TMPWKPEREMPL = 'PIVOTWKPEREMP';
  TMPEMPLOYEEAVAILABILITY = 'PIVOTEMPAVAILABLE';
//
  TABLETEMPPROD = 'HREPROD';
  TABLETEMPSAL = 'HRESALARY';
  TABLETEMPABS = 'HREABSENCE';
// Char separator - string
  STR_SEP = ' | ';
// grid form
  TAG_RESTORECOLUMN = -1;
//
  EXPORTPAYROLL_WEEK = 'W';
  EXPORTPAYROLL_PERIOD = 'P';
  EXPORTPAYROLL_MONTH = 'M';
//CONST for export
  ExportDescriptonLU = 'EXP_DESCLU';
  ExportSortLU = 'EXP_SORTLU';
// separator char
  CHR_SEP1 = CHR(ORD(253));
// user rights
  ADMINUSER = 'SYSDBA';
  ADMINPASSWORD = '61BC5D5D5C96EC';
  ADMINGROUP = 'ABSADMIN';
  PIMSPASSWORD = 'pims';

  ITEM_INVISIBIL    = '0';
  ITEM_VISIBIL      = '1';
  ITEM_VISIBIL_EDIT = '2';
  ITEM_DEFAULT      = '';

  // MRA:14-SEP-2009 Order 550470.
  PIMS_TEMPLATE_YEAR = 2099;

  // RV063.1.
  NO_SCAN = 'NOSCAN';

  // RV085.15.
  COUNTRYCODE_BELGIUM = 'BEL';

  // 20012085.3
  ALL_DAYS = 8;
  PERIODWEEK = 0;
  PERIODDAY = 1;

  // 20012858. Personal Screen special job.
  NOJOB = 'NOJOB';

  Quote = '''';

  DOWNJOB_1 = 'TDOWN'; // Mechanical Down // 20015586
  DOWNJOB_2 = 'PDOWN'; // No Merchandize // 20015586
  DOWNTIME_NO = 0;   // 20015586
  DOWNTIME_YES = 1;  // 20015586
  DOWNTIME_ONLY = 2; // 20015586

  // 20014826
  PRIORITY_MESSAGE = 1;
  PRIORITY_WARNING = 2;
  PRIORITY_ERROR = 3;
  PRIORITY_STACKTRACE = 4;
  PRIORITY_DEBUG = 5;
  // 20014826
  APPNAME_PIMS = 'Pims';
  APPNAME_AT = 'AdminTool';
  APPNAME_PS = 'PersonalScreen';
  APPNAME_PRS = 'ProductionScreen';
  APPNAME_TRS = 'TimeRecording';
  APPNAME_MANREG = 'ManualRegistrations';
  APPNAME_ADC1 = 'AutoDataCol1';
  APPNAME_ADC2 = 'AutoDataCol2';
  APPNAME_ADC3 = 'AutoDataCol3';

  MAX_TBS = 10; // GLOB3-60
  MIN_TBS = 4; // GLOB3-60
  ISHIFT = 11; // GLOB3-60

Type
 // Hours Per Employee
  TweekDates = array [1..7] of TDateTime;
  TWeekDaysTotal = array [1..7] of Integer;
  TDailyTime = array[1..8] of string;

// 20014450.50
type
  TBitBtn = class( ButtonWithColor.TBitBtnWithColor )
  published
    constructor Create(AOwner: TComponent); override;
  private
    FMyTag: Integer;
    FInit: Boolean;
  public
    property Init: Boolean read FInit write FInit;
    property MyTag: Integer read FMyTag write FMyTag;
  end;

implementation

{ TBitBtn }

// 20014450.50
constructor TBitBtn.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  Self.Color := clDarkRed; // PIM-250 // clPimsBlue;
  Self.Font.Color := clWhite;
  Self.ParentFont := True;
end;

end.
