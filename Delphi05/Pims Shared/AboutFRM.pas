unit AboutFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, PimsFRM, ShellAPI;

type
  TAboutF = class(TPimsF)
    pnlBack: TPanel;
    imgAbs: TImage;
    lblVersion: TLabel;
    lblWebsite: TLabel;
    procedure imgAbsClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure lblWebsiteClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  AboutF: TAboutF;

implementation

uses
  SystemDMT, UPimsConst, UPimsMessageRes;

{$R *.DFM}

procedure TAboutF.imgAbsClick(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TAboutF.FormKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  if Key in [#13, #32] then // Enter + Spacebar
    Close;
end;

procedure TAboutF.FormCreate(Sender: TObject);
  function DetermineUser: String;
  var
    I: Integer;
  begin
    Result := '';
    try
      for I := 0 to SystemDM.Pims.Params.Count - 1 do
        if Pos('USER NAME', SystemDM.Pims.Params.Strings[I]) > 0 then
          Result := UpperCase(
            Copy(SystemDM.Pims.Params.Strings[I],
              Pos('=', SystemDM.Pims.Params.Strings[I]) + 1,
                Length(SystemDM.Pims.Params.Strings[I]))
            );
    except
      Result := '';
    end;
  end;
begin
  inherited;
  lblVersion.Caption := lblVersion.Caption + ' ' + DisplayVersionInfo +
    ' ' + DetermineUser;
end;

procedure TAboutF.lblWebsiteClick(Sender: TObject);
begin
  inherited;
  if ShellExecute(Handle, PChar('OPEN'), // PIM-250
    PChar('http://www.gotlilabs.com'), nil, nil, SW_SHOWMAXIMIZED) <= 32 then
      DisplayMessage(SPimsCannotOpenHTML, mtInformation, [mbOk]);
end;

end.
