object DialogLoginDM: TDialogLoginDM
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Left = 259
  Top = 230
  Height = 480
  Width = 696
  object LoginPims: TDatabase
    AliasName = 'PIMSDBOR'
    DatabaseName = 'LoginPims'
    KeepConnection = False
    SessionName = 'SessionPims'
    OnLogin = LoginPimsLogin
    Left = 72
    Top = 24
  end
  object TableUser: TTable
    DatabaseName = 'LoginPims'
    SessionName = 'SessionPims'
    TableName = 'PIMSUSER'
    Left = 192
    Top = 24
  end
  object TableGroupMenu: TTable
    DatabaseName = 'LoginPims'
    SessionName = 'SessionPims'
    TableName = 'PIMSMENUGROUP'
    Left = 416
    Top = 24
  end
  object TablePimsGroup: TTable
    DatabaseName = 'LoginPims'
    SessionName = 'SessionPims'
    TableName = 'PIMSUSERGROUP'
    Left = 304
    Top = 24
  end
  object QueryCheckOpenApl: TQuery
    DatabaseName = 'LoginPims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  MENU_NUMBER, VISIBLE_YN, EDIT_YN '
      'FROM '
      '  PIMSMENUGROUP '
      'WHERE '
      '  GROUP_NAME = :GROUP_NAME AND '
      '  MENU_NUMBER = :APL_MENU')
    Left = 72
    Top = 120
    ParamData = <
      item
        DataType = ftString
        Name = 'GROUP_NAME'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'APL_MENU'
        ParamType = ptUnknown
      end>
  end
  object QueryServerTime: TQuery
    DatabaseName = 'LoginPims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  CURRENT_TIMESTAMP AS SERVERDATETIME  '
      'FROM '
      '  RDB$DATABASE')
    Left = 200
    Top = 120
    object QueryServerTimeSERVERDATETIME: TDateTimeField
      FieldName = 'SERVERDATETIME'
    end
  end
  object QueryServerTimeOrcl: TQuery
    DatabaseName = 'LoginPims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  SYSDATE AS SERVERDATETIME  '
      'FROM '
      '  DUAL'
      ' ')
    Left = 312
    Top = 120
    object DateTimeField1: TDateTimeField
      FieldName = 'SERVERDATETIME'
    end
  end
end
