(*
  Changes:
    RV001: (Revision 001)
    2.0.139.122 - Logging added. When DEBUG is defined for this project,
                  all hour-calculations are logged.
    RV004: (Revision 004) Overtime fix.
    2.0.139.125 - Determination of Overtime is not corrected afterwards, but
                  in BooksOvertime-procedure.
    RV015. (Revision 015). Round Minutes.
    2.0.139.12x  - Rounding minutes-procedure rewritten, because Except. Hours were not
                   included during rounding of the salary-hours.
    RV025. (Revision 025). MRA:1-APR-2009.
      An extra check must be made to determine exceptional hours made during
      overtime hours. For this purpose the EXCEPTIONALHOURDEF-table has an
      extra hour-type (OVERTIME_HOUR_TYPE) that can be used to book this type
      of hours. During BooksOvertime these combined hours must be handled.
    RV028. (Revision 028). MRA:26-MAY-2009. Order-502383.
      Store extra field 'prod_min_excl_break' in ProdHourPerEmplPerType-table.
    RV033.1. MRA:17-SEP-2009.
      - When scan is processed for hour-calculation, check for 'ignore overtime'
        that can be set for the workspot when it has an hourtype has been set.
    RV033.5. MRA:21-SEP-2009 Bugfix.
      - CurrentIDCard is not filled before UpdateAbsenceTotal!
        Use EmployeeIDCard instead.
    RV040. MRA:30-OCT-2009.
      - Moved ProcessTimeRecording and UnProcessProcessScans to CalcSalaryDMT,
        so the queries can be made fixed instead of building them each time
        when they are needed.
    RV040. MRA:30-OCT-2009.
      - Do not use 'Prepare' for TQuery. This costs lots of memory!
    RV040.2. MRA:30-OCT-2009.
      - Fix for overtime minutes. Minutes that were booked on exceptional
        hours must also booked on overtime when it is in the overtime period.
    RV040.4. MRA:11-NOV-2009.
      - PayedBreaks problem.
      - Add the PayedBreaks in ProcessBookings! Or they are not
        included when a scans is equal to a payed break!
             Example: When scan is 8:00-8:10 and payed break is the same,
                      then ProdMin will be 0, so it will not be booked!
    MRA:26-FEB-2010. RV055.1.
    - EmptyIDCard is replaced by a procedure that is now in UScannedIDCard.
    SO:08-JUL-2010 RV067.4. 550497
    - updated absence total function in order to deal with the new absence types
    MRA:10-SEP-2010 RV067.MRA.31 Order 550515
    - Use function to absencetotal-fieldname.
    MRA:4-OCT-2010 RV071.3. Order 550497
    - When the flag 'Norm Bank Holiday Reserve Minute Manual YN' is
      set to 'Y', then this norm must not be recalculated during
      Update Counters-action.
    MRA:7-OCT-2010
    MRA:7-OCT-2010 RV071.7. 550497
    - ComputePlanned is always determined based on current date!
      This is wrong, because it must be based on the current
      selected year and week (end-of-week).
    MRA:11-OCT-2010 RV071.10 550497
    - Saturday Day Credit handling.
    MRA:2-NOV-2011 RV100.1.
    - Debug-info: Also show ContractGroupCode of employee (CG=)
    MRA:2-DEC-2011 RV103.1. SO-20011799. Worked hours during bank holiday.
    - Modification calculation worked hours on bank holiday.
      When hours were made during a bank holiday, then these
      hours must be booked on an hourtype that is defined
      for that bank holiday or on the contract group.
      During this the availability must be changed from
      bank holiday to available and the hours that were booked
      for bank holiday must be reprocessed.
    MRA:1-MAY-2012 20012858
    - Addition of function to determine efficieny by quantity or time.
    MRA:31-DEC-2012 SC-50027247 I/O Error 103
    - Sometimes it gives I/O error 103 when used with Terminal service.
      - To avoid problems, use try/except when accessing a file in log-folder.
    MRA:5-NOV-2013 TD-23416 (PCO)
    - Missing hours, sometimes some salary-hours and/or production-hours are
      missing.
    - Keep track of some values in Bookmarkrecord, during UnProcessProcessScans.
    MRA:11-NOV-2013 TD-23386
    - Hourtype defined on Shift-level should not result in double hours!
      - See: BooksHoursForShift-procedure
        - This procedure is NOT used anymore. It is handled in a different way.
    MRA:10-NOV-2014 20014826
    - Log error messages optionally to database.
    MRA:16-MAR-2015 20015346
    - Store scans in seconds
    MRA:22-DEC-2015 PIM-12 Bugfix
    - ComputeTotalPlannedDayTime: A 'next' was missing resulting
      in out-of-memory, during a search for breaks.
    MRA:12-JAN-2016 PIM-23
    - Added function that can show a datetime-value as '00:00' (hrs:mins)
    - DateTime2StringTime
    MRA:22-JUN-2018 GLOB3-60
    - Extension 4 to 10 blocks for planning.
    MRA:25-JUL-2018 GLOB3-60
    - Extension 4 to 10 blocks for planning.
    - Rework
*)
(*
  IMPORTANT NOTE: (MR:26-09-2003)
  Procedures and CalculateTotalHoursDMT
  - Some procedures are defined in file CalculateTotalHoursDMT:
    - GetShiftDay
    - ComputeBreaks
    - EmployeeCutOfTime
    - GetProdMin
    These are called indirectly by UGlobalFunctions:
    - BooksExceptionalTime
    - ProcessTimeRecording
    - UnprocessProcessScans

    These procedures start with 'AProdMinClass'. In order to use them
    you must do the following:
    - Include 'CalculateTotalHoursDMT' in uses-part, in DMT-file of
      the part where you want to use it.

  Note: 'CalculateTotalHoursDMT' is (after SystemDMT) always created.
        This means also the Class-variables can always be used.
*)
unit UGlobalFunctions;

interface

uses SysUtils, Dialogs, dbTables, Classes, db, UpimsConst, SystemDMT,
  CalculateTotalHoursDMT, ListProcsFRM, FileCtrl, UScannedIDCard,
  GlobalDMT;

const
  UGLOGFILENAME_DEFAULT = 'UGLOG';
  UGERRORLOGFILENAME_DEFAULT = 'UGERRORLOG';

type
  TTimeBlock = array [1..MAX_TBS] of Boolean;

type
  PTBookmarkRecord = ^TBookmarkRecord;
  TBookmarkRecord = record
    Bookmark: TBookmark;
    AddToSalList, AddToProdListOut, AddToProdListIn: Boolean;
    BookingDay: TDateTime; // TD-23416
  end;

// RV055.1.
(* procedure EmptyIDCard(var IDCard : TScannedIDCard); *)

function Min(int1,int2: variant): variant;
function Max(int1,int2: variant): variant;

function IntMin2TimeMin(Minutes: integer): TDateTime;
{ Convert Minute(int) into datetime }

function TimeMin2IntMin(ADate:TDateTime): integer;
{ Convert The time from ADateTime in minutes (integer) }

function IntMin2StringTime(Minutes: integer; ShowZero: boolean=False): String;
{ Convert the Minute in string like hours:min
  If minute=0 and ShowZero=False then result='' else result='00:00' }

function IntMin2StrDigTime(Minutes,HrsDigits:integer;ShowZero:boolean): String;
{Convert the Minute in string like hours:min
  If minute=0 and ShowZero=False then result='' else result='00:00'
  The minutes will be displayed with HrsDigits}

function StrTime2IntMin(ATime: String): integer;
{ Convert the ATime string hrs:min(06:12) to amount of minutes }

function RoundTime(const ADateTime: TDateTime; Target: Integer): TDateTime;
{case target of
  0: round milisecond
	1: round second
  2: round minute	}

function RoundTimeConditional(const ADateTime: TDateTime; const ATarget: Integer): TDateTime; // 20015346
{ Conditional based on ORASystemDM-setting TimerecordingInSecs:
  false: Round time
  true: do not round time
{case target of
  0: round milisecond
	1: round second
  2: round minute	}

function BuildMinTime(AStrTime: String): Integer;
{ This function are useful for TdxTimeEdit component
  formation strings like: '__:_3' into '00:03' and after convert into minutes }

function DayInWeek (WeekStartOn: Integer; CurrentDate: TDateTime):Integer;
{	WeekStartOn any possibilities 1-7; Fixed week days: MO-TU-WE-T
  1 = Monday
  result = day from CurrenTDate in week}

function DateInInterval(ADate, TestDate: TDateTime;
				 	MinTo, MinAfter: Integer): TDateTime;
// function check if TestDate is in DateTime interval
// if ADate-MinTo <= TestDate <= ADate+MinAfter then result=ADate
// else result=TestDate

function ComputeMinutesOfIntersection(Start1, End1,
  Start2, End2: TDateTime): Integer;
// This function retrieve the intersection between datetime intervals
// start1 - end1 and start2 - end2 in minutes;

//function GetCurrentWeek(ADate: TDateTime): Integer;
// Procedure retrieve the week no, containing day from ADate.

procedure QueryCopy(var QuerySource, QueryDestination: TQuery);
// This procedure copys the property from Source to destination
// Both queries must be created.

procedure GetStartEndDay(var StartDateTime, EndDateTime: TDateTime);
// Considering the date from StartDateTime, procedure build start-end of day
// If StartDateTime='21/12/01' will result in
// StartDateTime='21/12/01 00:00:00:000'
// EndDateTime='21/12/01 23:59:59:999'

function WhatBreaks(AQuery: TQuery; Employee_number,Shift_Number: integer;
  Plant_Code,Department_Code: string; Payed: boolean): string;
// This function determine if Breaks are definde per Employee, Department or
// Shift
// so the function will return the table name of breaks.
// ex: BREAKPEREMPLOYEE

function WhatTimeBlock(AQuery: TQuery; Employee_number,Shift_Number: integer;
  Plant_Code,Department_Code: string;var TB: TTimeBlock): string;
// This function determine if the TimeBlocks are defined per Employee,Department
// or Shift and return the table name ex: TIMEBLOCKPEREMPLOYEE
// return valid Time blocks in TB structure

(* RV040. Moved to CalcSalaryDMT
// MR:30-10-2003 'UpdateProdMin' divided in 'UpdateProdMinOut' and
// 'UpdateProdMinIn', because of overnight-scans
procedure ProcessTimeRecording(AQuery: TQuery; EmployeeIDCard: TScannedIDCard;
  FirstScan, LastScan, UpdateSalMin, UpdateProdMinOut, UpdateProdMinIn: Boolean;
  DeleteAction: Boolean; DeleteEmployeeIDCard: TScannedIDCard;
  LastScanRecordForDay: Boolean);
// This procedure process time recording for a particular scan = EmployeeIDCard.
// can be used only for manual records !!
*)

(* RV040. Moved to CalcSalaryDMT
procedure UnprocessProcessScans(AQuery: TQuery; FromDate: TDateTime;
  EmployeeIDCard: TScannedIDCard; ProcessSalary: Boolean; DeleteAction: Boolean;
  ProdDate1, ProdDate2, ProdDate3, ProdDate4: TDateTime);
*)
// This procedure subtract Production minute for employee - scan EmployeeIDCard
// Delete all salary hours with booking date in overtimeperiod of EmployeeIDCard
// Recalculate the salary hours for scans resulting booking date in overtime
// period.
// If ProcessScan=true then the curent scan EMployeeIDCard is processed also in
// order with the others scan by datetime_in.

// RV067.MRA.31
function AbsenceTotalField(const AAbsenceTypeCode: Char): String;

procedure UpdateAbsenceTotalMode(AQuery: TQuery; EmployeeNumber,
  AbsenceYear: Integer; Minutes: Double;	AbsenceTypeCode: Char; AAddToOldMinutes: Boolean = True);
// This procedure looking for record in table ABSENCETOTAL
// if found then update minutes (add or subtract)
// if not, add a record with Minutes.
// depending on WhatToUpdate the procedure update minute for
// "I"	ILLNESS_TOTAL_MINUTES
// "W"  USED_WTR_MINUTES
// "T"	USED_TFT_MINUTES
// "H"  USED_HOLIDAY_MINUTES

function CharFromStr(str: string; CharIndex: integer; BlankChar: char): char;
// this function check if str is not empty
// if empty return BlankChar
// else return the char CharIndex from str
// charindex is starting from 1

// RV071.7. Addition of FromDate.
function ComputePlanned(AQuery: TQuery; EmpNo: integer;
  WhatPlan: String; AFromDate: TDateTime = 0): integer;
// This function compute the Planned minutes of (AbsenceType) WhatPlan
// 'H' Holiday
// 'T' Time for Time
// 'W' Worked TFT
// FromDate (when 0 then current date is taken).

function GetNoFromStr(MyStr: String): integer;
// this function get the number part from a string
// exampl: GetNoFromStr('Edit23') = 23.

procedure shd(adate: tdatetime);
// show date variables.

function ComputeTotalPlannedDayTime(AQuery: TQuery;
  AllBreaks: Boolean; Day, EmplNo, ShiftNo: Integer;
  PlantCode, DepCode: String): Integer;
// This function is calculating the total planned work time for employee,weekday,
// plantcode,shiftnumber ...
// result = sum(all timeblocks with "*" in StandardAvailability and substract
// the minutes of breaks. If not Allbreaks then will substract only not payed
// else all breaks minutes are substracted.

// MR:19-11-2002
procedure BooksHoursForShift(AQuery: TQuery; BookingDay: TDateTime;
  EmployeeIDCard: TScannedIDCard; ProdMinPlusPayedBreaks: Integer;
  Manual: String);
// Book hours if there is a hourtype set for Shift where Employee worked.

// MR:20-11-2002
procedure ProcessBookings(AQuery: TQuery; EmployeeIDCard: TScannedIDCard;
  BookingDay: TDateTime; UpdateProdMin: Boolean;
  ProdMin, PayedBreaks: Integer;
  Manual: String; RoundMinutes: Boolean);
// Combine some booking-procedures to ONE procedure.

// MR:30-1-2004
(*
procedure ProcessBookingsForContractRoundMinutes(AQuery: TQuery;
  EmployeeIDCard: TScannedIDCard;
  BookingDay: TDateTime; UpdateProdMin: Boolean;
  ProdMin, PayedBreaks: Integer;
  Manual: String; RoundMinutes: Boolean);
*)
// MR:28-02-2003
function SearchAbsenceReasonID(
  AQuery: TQuery; AbsenceReasonCode: String): Integer;

function MidnightCorrection(STime, ETime: TDateTime): TDateTime;

// MR:17-12-2003 Logging is a public procedure
procedure WLogMain(AMsg: String; APriority: Integer);
procedure WLog(AMsg: String);
procedure WErrorLog(AMsg: String);
procedure WDebugLog(AMsg: String);
procedure UGLogFilenameSet(AValue: String);
procedure UGErrorLogFilenameSet(AValue: String);

// 20012858
function DetermineEfficiency(ANorm, AActualTime,
  AActualProd: Double; var ATheoTime: Double;
  var ATheoProd: Double): Double;

// Return boolean as string (True or False).
function BoolToStr(ABoolean: Boolean): String;
function Bool2Str(ABoolean: Boolean): String;

// 20014550.50 To be used in reports
function MyQuotes(AValue: String): String;
function ToDate(ADate: TDateTime): String;

// PIM-23
function DateTime2StringTime(ADateTime: TDateTime; AShowZero: Boolean=False): String;

// GLOB3-60
function ShiftTBCount(APlantCode: String; AShiftNumber: Integer): Integer;

// GLOB3-60
function PlantShiftMaxTBCount(APlantCode: String): Integer;

// GLOB3-60
function ShiftMaxTBCount: Integer;

var
  UGLogFilename: String;
  UGErrorLogFilename: String;

implementation

procedure UGLogFilenameSet(AValue: String);
begin
  UGLogFilename := AValue;
end;

procedure UGErrorLogFilenameSet(AValue: String);
begin
  UGErrorLogFilename := AValue;
end;

// 'WLogMain' = WriteLog
procedure WLogMain(AMsg: String; APriority: Integer);
var
  TFile: TextFile;
  Exists: Boolean;
  UGLogPath: String;
  LogPath: String;
  GoOn: Boolean; // SC-50027247

  function MyZeroFormat(Value: String; Len: Integer): String;
  begin
    while Length(Value) < Len do
      Value := '0' + Value;
    Result := Value;
  end;
  function PathCheck(Path: String): String;
  begin
    if Path <> '' then
      if Path[length(Path)] <> '\' then
        Path := Path + '\';
    Result := Path;
  end;
  function NewLogPath(ALogRootPath, ALogFileName: String): String;
  var
    Year, Month, Day: Word;
    DateString: String;
  begin
    try
      DecodeDate(Date, Year, Month, Day);
      DateString :=
        MyZeroFormat(IntToStr(Year), 4) +
        MyZeroFormat(IntToStr(Month), 2) +
        MyZeroFormat(IntToStr(Day), 2);
    except
      DateString := '20020601';
    end;
    Result := PathCheck(ALogRootPath) +
      SystemDM.CurrentComputerName + '_' + ALogFileName + DateString + '.TXT';
  end;
begin
  if SystemDM.DBLog(AMsg, APriority) then // 20014826
    Exit;
  // SC-50027247
  GoOn := True;
  Exists := False;
  // SC-50027247
  try
{$IFDEF DEBUG1}
    ; // do nothing
{$ELSE}
    AMsg := DateTimeToStr(Now) + ' - ' + AMsg;
{$ENDIF}
    UGLogPath := '..\Log';
    // SC-50027247
    try
      if not DirectoryExists(UGLogPath) then
        ForceDirectories(UGLogPath);
    except
      GoOn := False;
    end;

    // SC-50027247
    if GoOn then
    begin
      if APriority = PRIORITY_ERROR then // 20014826
        LogPath := NewLogPath(UGLogPath, UGErrorLogFilename)
      else
        LogPath := NewLogPath(UGLogPath, UGLogFilename);
      // SC-50027247
      try
        Exists := FileExists(LogPath);
        AssignFile(TFile, LogPath);
      except
        GoOn := False;
      end;
      // SC-50027247
      if GoOn then
      begin
        try
          try
            if not Exists then
              Rewrite(TFile)
            else
              Append(TFile);
            WriteLn(TFile, AMsg);
          except
          end;
        finally
          CloseFile(TFile);
        end;
      end;
    end;
  except
  end;
end; // WLog

// 20014826
procedure WLog(AMsg: String);
begin
  WLogMain(AMsg, PRIORITY_MESSAGE);
end;

// 20014826
procedure WErrorLog(AMsg: String);
begin
  WLogMain(AMsg, PRIORITY_ERROR);
end;

// 20014826
procedure WDebugLog(AMsg: String);
begin
  WLogMain(AMsg, PRIORITY_DEBUG);
end;

// RV055.1.
(*
procedure EmptyIDCard(var IDCard : TScannedIDCard);
begin
  IDCard.EmployeeCode := NullInt; IDCard.IDCard := NullStr;
  IDCard.ShiftNumber := NullInt;
  IDCard.InscanEarly := NullInt; IDCard.InscanLate := NullInt;
  IDCard.OutscanEarly := NullInt; IDCard.OutScanLate := NullInt;
  IDCard.WorkSpotCode := NullStr; IDCard.WorkSpot := NullStr;
  IDCard.JobCode := NullStr; IDCard.Job := NullStr;
  IDCard.DateIn:=NullDate; IDCard.DateOut:=NullDate;
  IDCard.PlantCode := NullStr; IDCard.Plant := NullStr;
  IDCard.DepartmentCode := NullStr;
  IDCard.EmplName := NullStr; IDCard.Plant := NullStr;
  IDCard.DateInCutOff := NullDate; IDCard.DateOutCutOff := NullDate;
  // MR:27-10-2005
  IDCard.FromTimeRecordingApp := False;
  // MRA:17-SEP-2009 RV033.1.
  IDCard.IgnoreForOvertimeYN := NullStr;
end;
*)

function Min(int1,int2: variant): variant;
begin
  if int1 <= int2 then
    Result := int1
  else
    Result := int2;
end;

function Max(int1,int2: variant): variant;
begin
  if int1 >= int2 then
    Result := int1
  else
    Result := int2;
end;

function IntMin2TimeMin(Minutes: integer): TDateTime;
var
  Hrs, Min: word;
begin
  if Minutes <= 0 then
    ReplaceTime(Result, EncodeTime(0,0,0,0))
  else
  begin
    case Minutes of
      1..1439 :
        begin
          Hrs := Minutes div 60; Min := Minutes mod 60;
          ReplaceTime(Result, EncodeTime(Hrs,Min,0,0));
        end
      else
        ReplaceTime(Result, EncodeTime(23,59,59,999));
    end;
  end;
end;

function TimeMin2IntMin(ADate:TDateTime): integer;
var
  hour,min,sec,msec: word;
begin
  DecodeTime(ADate,hour,min,sec,msec);
  Result := Hour*60 + min;
end;

// PIM-23
function DateTime2StringTime(ADateTime: TDateTime; AShowZero: Boolean=False): String;
begin
  Result := IntMin2StringTime(TimeMin2IntMin(ADateTime), AShowZero);
end;

function IntMin2StringTime(Minutes: integer; ShowZero: boolean=False): String;
var
  Negative: boolean;
begin
  Negative := False;
  if Minutes = 0 then
  begin
    if ShowZero then
      Result:=NullTime
    else
      Result:='';
  end
  else
  begin
    if Minutes < 0 then
    begin
      Negative := True;
      Minutes := Minutes*(-1);
    end;
    Result := Format('%.2d:%.2d',[(Minutes div 60),(Minutes mod 60)]);
    if Negative then
      Result := '-' + Result;
  end;
end;

function IntMin2StrDigTime(Minutes,HrsDigits:integer;ShowZero:boolean): String;
var
  Negative: boolean;
begin
  Negative := False;
  if Minutes = 0 then
  begin
    if ShowZero then
      Result:=NullTime
    else
      Result:='';
  end
  else
  begin
    if Minutes < 0 then
    begin
      Negative := True;
      Minutes := Minutes*(-1);
    end;
    Result := Format('%.'+IntToStr(HrsDigits)+'d:%.2d',
      [(Minutes div 60),(Minutes mod 60)]);
    if Negative then
      Result := '-' + Result;
  end;
end;

function StrTime2IntMin(ATime: String): integer;
var
  index: integer;
  hours, minutes, temp: string;
  negative,ready: boolean;
begin
  result := 0;
  hours := ''; minutes := '';
  ready := False; negative := false;
  Temp := Trim(ATime);
  if (length(Temp)>=1) and (Temp[1]='-') then
  begin
    negative:=True;
    Delete(Temp,1,1);
  end;
  if length(temp)=0 then
    Exit;
  for index := 1 to length(Temp) do
  begin
    if Temp[index] = ':' then
    begin
      Ready := True;
      Continue;
    end;
    if not Ready then
      hours := hours + Temp[index]
    else
      minutes := minutes + Temp[index];
  end;
  while (length(hours)>0) and (hours[1]='0') do
    Delete(hours,1,1);
  while (length(minutes)>0) and (minutes[1]='0') do
    Delete(minutes,1,1);
  if hours = '' then
    hours := '0';
  if minutes = '' then
    minutes := '0';
  try
    result := (StrToInt(hours))*60 + StrToInt(minutes);
    if negative then
      result:=result*(-1);
  except
    result := 0;
  end;
end;

// 20015346
function RoundTimeConditional(const ADateTime: TDateTime; const ATarget: Integer): TDateTime;
begin
  if SystemDM.TimerecordingInSecs then
    Result := ADateTime
  else
    Result := RoundTime(ADateTime, ATarget);
end;

// MR:15-09-2004 Function changed to solve compare-error.
// Otherwise you can get 'rounding'-error.
function RoundTime(const ADateTime: TDateTime; Target: Integer): TDateTime;
const
  MinSecRounder = 30;
  MSecRounder = 500;
var
  Hour, Min, Sec, Msec: Word;
begin
  Result := ADateTime;
  DecodeTime(ADateTime, Hour, Min, Sec, Msec);
  case Target of
    // round to seconds
    0:
      begin
        if Msec > MSecRounder then
        begin
          Sec := Sec + 1;
          if Sec = 60 then
          begin
            Sec := 0;
            Min := Min + 1;
          end;
        end;
      end;
    // round to minutes
    1:
      begin
        if Sec > MinSecRounder then
          Min := Min + 1;
        Sec := 0;
      end;
  end; // case
  if Min = 60 then
  begin
    Min := 0;
    Hour := Hour + 1;
    if Hour = 24 then
    begin
      Hour := 0;
      Result := Result + 1;
    end;
  end;
  Result := Trunc(Result) + Frac(EncodeTime(Hour, Min, Sec, 0));
end;

// MR:03-12-2003
function DayInWeek(WeekStartOn: Integer; CurrentDate: TDateTime):Integer;
begin
  Result := ListProcsF.PimsDayOfWeek(CurrentDate);
end;

function DateInInterval(ADate, TestDate : TDateTime;
  MinTo, MinAfter: Integer): TDateTime;
var
  MinDate, MaxDate: TDateTime;
begin
  result := TestDate;
  MinDate := ADate - MinTo / DayToMin;
  MaxDate := ADate + MinAfter / DayToMin;
  if (MinDate <= TestDate) and (TestDate <= MaxDate) then
    Result := ADate;
end;

function ComputeMinutesOfIntersection(start1,end1,start2,
  end2: TDateTime): Integer;
begin
  result:= 0;
  if (end1 < start1) or (end2 < start2) then
    Exit;
  Result:= (min(end1, end2) - max(start1,start2)) * DayToMin;
  if Result < 0 then
    Result:= 0;
end;

// MR:07-11-2003 Corrects dates if end-date is '00:00' (midnight)
function MidnightCorrection(STime, ETime: TDateTime): TDateTime;
var
  Hours, Mins, Secs, MSecs: Word;
begin
  Result := ETime;
  if Trunc(STime) = Trunc(ETime) then
  begin
    DecodeTime(Result, Hours, Mins, Secs, MSecs);
    // If midnight then set 'datetime'-variable to next day
    if (Hours = 0) and (Mins = 0) then
      Result := Trunc(Result + 1) + Frac(Result);
  end;
end;

procedure QueryCopy(var QuerySource, QueryDestination: TQuery);
begin
  QueryDestination.SessionName := QuerySource.SessionName;
  QueryDestination.DataBaseName := QuerySource.DatabaseName;
end;

procedure shd(adate: tdatetime);
begin
  showmessage(FormatDateTime(LONGDATE,adate));
end;

procedure GetStartEndDay(var StartDateTime, EndDateTime: TDateTime);
begin
  ReplaceTime(StartDateTime,EncodeTime(0,0,0,0));
  EndDateTime := StartDateTime;
  ReplaceTime(EndDateTime,EncodeTime(23,59,59,999));
end;

function WhatTimeBlock(AQuery: TQuery; Employee_number,Shift_Number: integer;
  Plant_Code,Department_Code: string;var TB: TTimeBlock): string;
var
  QueryWork: TQuery;
  index: Integer;
begin
  // GLOB3-10 Use the number of record found as index, not TIMEBLOCK_NUMBER,
  // because that can give a wrong index.
  Result := NullStr;
  QueryWork := TQuery.Create(Nil);
  QueryCopy(AQuery,QueryWork);
  for index := 1 to length(TB) do
    TB[index] := False;
  // TIMEBLOCKPEREMPLOYEE
  QueryWork.Active := False;
  QueryWork.SQL.Clear;
  QueryWork.SQL.Add(
    'SELECT ' +
    '  T.TIMEBLOCK_NUMBER ' +
    'FROM ' +
    '  TIMEBLOCKPEREMPLOYEE T ' +
    'WHERE ' +
    '  T.EMPLOYEE_NUMBER = :EMPNO AND T.PLANT_CODE = :PCODE ' +
    '  AND T.SHIFT_NUMBER = :SHNO');
//  QueryWork.Prepare; // RV040.
  QueryWork.ParamByName('EMPNO').AsInteger := Employee_Number;
  QueryWork.ParamByName('PCODE').AsString := Plant_Code;
  QueryWork.ParamByName('SHNO').AsInteger := Shift_Number;
  QueryWork.Active := True;
  index := 0;
  while not QueryWork.Eof do
  begin
    inc(index);
    Result := 'TIMEBLOCKPEREMPLOYEE';
//    index := QueryWork.FieldByName('TIMEBLOCK_NUMBER').AsInteger;
    if index <= length(TB) then
      TB[index] := True;
    QueryWork.Next;
  end;
  if Result = Nullstr then
  begin
    // TIMEBLOCKPERDEPARTMENT
    QueryWork.Active := False;
    QueryWork.SQL.Clear;
    QueryWork.SQL.Add(
      'SELECT ' +
      '  T.TIMEBLOCK_NUMBER ' +
      'FROM ' +
      '  TIMEBLOCKPERDEPARTMENT T ' +
      'WHERE ' +
      '  T.PLANT_CODE = :PCODE AND T.DEPARTMENT_CODE = :DCODE AND ' +
      '  T.SHIFT_NUMBER = :SHNO');
//    QueryWork.Prepare; // RV040.
    QueryWork.ParamByName('PCODE').AsString := Plant_Code;
    QueryWork.ParamByName('DCODE').AsString := Department_Code;
    QueryWork.ParamByName('SHNO').AsInteger := Shift_Number;
    QueryWork.Active := True;
    index := 0;
    while not QueryWork.Eof do
    begin
      inc(index);
      Result := 'TIMEBLOCKPERDEPARTMENT';
//      index := QueryWork.FieldByName('TIMEBLOCK_NUMBER').AsInteger;
      if index <= length(TB) then
        TB[index] := True;
      QueryWork.Next;
    end;
    if Result = Nullstr then
    begin
      // TIMEBLOCKPERSHIFT
      QueryWork.Active := False;
      QueryWork.SQL.Clear;
      QueryWork.SQL.Add(
        'SELECT ' +
        '  T.TIMEBLOCK_NUMBER ' +
        'FROM ' +
        '  TIMEBLOCKPERSHIFT T ' +
        'WHERE ' +
        '  T.PLANT_CODE = :PCODE AND T.SHIFT_NUMBER = :SHNO');
//      QueryWork.Prepare; // RV040.
      QueryWork.ParamByName('PCODE').AsString := Plant_Code;
      QueryWork.ParamByName('SHNO').AsInteger := Shift_Number;
      QueryWork.Active := True;
      index := 0;
      while not QueryWork.Eof do
      begin
        inc(index);
        Result := 'TIMEBLOCKPERSHIFT';
//        index := QueryWork.FieldByName('TIMEBLOCK_NUMBER').AsInteger;
        if index <= length(TB) then
          TB[index] := True;
        QueryWork.Next;
      end;
    end;
  end;
  QueryWork.Free;
end;

function WhatBreaks(AQuery: TQuery; Employee_number,Shift_Number: integer;
  Plant_Code,Department_Code: string; Payed: boolean): string;
var
  QueryWork: TQuery;
begin
  Result := NullStr;
  QueryWork := TQuery.Create(Nil);
  QueryCopy(AQuery,QueryWork);
  // BREAKPEREMPLOYEE
  QueryWork.Active := False;
  QueryWork.SQL.Clear;
  QueryWork.SQL.Add(
    'SELECT ' +
    '  B.PLANT_CODE ' +
    'FROM ' +
    '  BREAKPEREMPLOYEE B ' +
    'WHERE ' +
    '  B.EMPLOYEE_NUMBER = :EMPNO AND B.PLANT_CODE = :PCODE ' +
    '  AND B.SHIFT_NUMBER = :SHNO AND B.PAYED_YN = :PAYED_YN');
//  QueryWork.Prepare; // RV040.
  QueryWork.ParamByName('EMPNO').AsInteger := Employee_Number;
  QueryWork.ParamByName('PCODE').AsString := Plant_Code;
  QueryWork.ParamByName('SHNO').AsInteger := Shift_Number;
  if Payed then
    QueryWork.ParamByName('PAYED_YN').AsString := CHECKEDVALUE
  else
    QueryWork.ParamByName('PAYED_YN').AsString := UNCHECKEDVALUE;
  QueryWork.Active := True;
  if not QueryWork.IsEmpty then
    Result := 'BREAKPEREMPLOYEE'
  else
  begin
    // BREAKPERDEPARTMENT
    QueryWork.Active := False;
    QueryWork.SQL.Clear;
    QueryWork.SQL.Add(
      'SELECT ' +
      '  B.PLANT_CODE ' +
      'FROM ' +
      '  BREAKPERDEPARTMENT B ' +
      'WHERE ' +
      '  B.DEPARTMENT_CODE = :DCODE AND B.PLANT_CODE = :PCODE ' +
      '  AND B.SHIFT_NUMBER = :SHNO AND B.PAYED_YN = :PAYED_YN');
//    QueryWork.Prepare; // RV040.
    QueryWork.ParamByName('DCODE').AsString := Department_Code;
    QueryWork.ParamByName('PCODE').AsString := Plant_Code;
    QueryWork.ParamByName('SHNO').AsInteger := Shift_Number;
    if Payed then
      QueryWork.ParamByName('PAYED_YN').AsString := CHECKEDVALUE
    else
      QueryWork.ParamByName('PAYED_YN').AsString := UNCHECKEDVALUE;
    QueryWork.Active := True;
    if not QueryWork.IsEmpty then
      Result := 'BREAKPERDEPARTMENT'
    else
    begin
      // BREAKPERSHIFT
      QueryWork.Active := False;
      QueryWork.SQL.Clear;
      QueryWork.SQL.Add(
        'SELECT ' +
        '  B.PLANT_CODE ' +
        'FROM ' +
        '  BREAKPERSHIFT B ' +
        'WHERE ' +
        '  B.PLANT_CODE = :PCODE ' +
        '  AND B.SHIFT_NUMBER = :SHNO AND B.PAYED_YN = :PAYED_YN');
//      QueryWork.Prepare; // RV040.
      QueryWork.ParamByName('PCODE').AsString := Plant_Code;
      QueryWork.ParamByName('SHNO').AsInteger := Shift_Number;
      if Payed then
      	QueryWork.ParamByName('PAYED_YN').AsString := CHECKEDVALUE
      else
        QueryWork.ParamByName('PAYED_YN').AsString := UNCHECKEDVALUE;
      QueryWork.Active := True;
      if not QueryWork.IsEmpty then
        Result := 'BREAKPERSHIFT';
    end;
  end;
  QueryWork.Free;
end;

procedure ProcessBookings(AQuery: TQuery; EmployeeIDCard: TScannedIDCard;
  BookingDay: TDateTime; UpdateProdMin: Boolean;
  ProdMin, PayedBreaks: Integer;
  Manual: String; RoundMinutes: Boolean);
var
  NonOverTime: Integer;
  BookExceptionalBeforeOvertime: Boolean;
  OvertimeMin, NonOvertimeMin: Integer; {RV025}
  OldDateInCutOff, OldDateIn: TDateTime; {RV025}
  OverTime: Integer; {RV025}
begin
  BookExceptionalBeforeOvertime :=
    GlobalDM.DetermineExceptionalBeforeOvertime(EmployeeIDCard);
  GlobalDM.PayedBreakSave := PayedBreaks; // RV028.
{$IFDEF DEBUG}
begin
  // RV100.1. ContractGroupCode added for debugging.
  WDebugLog('Start ProcessBookings');
  WDebugLog('EmployeeIDCard');
  WDebugLog('- EMP=' + IntToStr(EmployeeIDCard.EmployeeCode) +
    ' PL=' + EmployeeIDCard.PlantCode +
    ' SHFT=' + IntToStr(EmployeeIDCard.ShiftNumber) +
    ' WC=' + EmployeeIDCard.WorkSpotCode +
    ' JC=' + EmployeeIDCard.JobCode +
    ' DEPT=' + EmployeeIDCard.DepartmentCode +
    ' CG=' + EmployeeIDCard.ContractGroupCode
    );
  WDebugLog(
    '- DIN=' + DateTimeToStr(EmployeeIDCard.DateIn) +
    ' DOUT=' + DateTimeToStr(EmployeeIDCard.DateOut) +
    ' SHIFTDATE=' + DateToStr(EmployeeIDCard.ShiftDate)
    );
  WDebugLog(
    '- InscanEarly=' + IntToStr(EmployeeIDCard.InscanEarly) +
    ' InscanLate=' + IntToStr(EmployeeIDCard.InscanLate) +
    ' OutscanEarly=' + IntToStr(EmployeeIDCard.OutscanEarly) +
    ' OutscanLate=' + IntToStr(EmployeeIDCard.OutscanLate) +
    ' CutOfTime=' + EmployeeIDCard.CutOfTime
    );
end;
{$ENDIF}
  // Production Hours
  // RV040.4. PayedBreaks problem.
  //          Add the PayedBreaks here to ProdMin in if() ! Or they are not
  //          included when a scans is equal to a payed break!
  //          Example: When scan is 8:00-8:10 and payed break is the same,
  //                   then ProdMin will be 0, so it will not be booked!
  if (ProdMin+PayedBreaks > 0) or RoundMinutes then
  begin
{$IFDEF DEBUG1}
  WDebugLog(' ProdMin=' + IntToStr(ProdMin) +
    ' PayedBreaks=' + IntToStr(PayedBreaks)
    );
{$ENDIF}
{$IFDEF DEBUG3}
  WDebugLog('ProdMin=' + IntToStr(ProdMin) +
    ' (' + IntMin2StringTime(ProdMin, False) + ')' +
    ' PayedBreaks=' + IntToStr(PayedBreaks) +
    ' (' + IntMin2StringTime(PayedBreaks, False) + ')'
    );
{$ENDIF}
    // Process salary hours per Employee

    // RV103.1.
    // When worked hours are detected on bank holiday (with an hourtype for
    // 'worked on bank holiday'), then when the hours are booked on holiday,
    // this will return 0 minutes, in which case no further hours have to
    // be booked.
    if GlobalDM.BooksWorkedHoursOnBankHoliday(BookingDay, ProdMin+PayedBreaks,
      EmployeeIDCard, Manual) <> 0 then
    begin
      // RV025.
      // Detect Overtime and Non-overtime combination. This is needed
      // when combined hours (exceptional during overtime hours) must be stored.
      // For this purpose the scan that has non-overtime and overtime minutes,
      // must be handled in 2 parts:
      // - Part1: The scan-part before the overtime: Non-overtime minutes.
      // - Part2: The scan-part falling in overtime: Overtime minutes.
      if GlobalDM.DetectOvertimeNonOvertime(BookingDay,
        ProdMin+PayedBreaks, EmployeeIDCard, Manual,
        NonOvertimeMin, OvertimeMin) then
      begin
        OldDateIn := EmployeeIDCard.DateIn;
        OldDateInCutOff := EmployeeIDCard.DateInCutOff;
        // Non-overtime minutes:
        // - Book as Exceptional and Regular hours.
        if NonOvertimeMin <> 0 then
        begin
          NonOverTime := GlobalDM.BooksExceptionalTime(BookingDay,
            NonOvertimeMin, EmployeeIDCard, Manual);
          GlobalDM.BooksRegularSalaryHours(BookingDay,
            EmployeeIDCard, NonOverTime, Manual);
          // Add NonOvertimeMin temporary to the scan!
          // Because it is used later during 'Books'-functions
          EmployeeIDCard.DateIn :=
            EmployeeIDCard.DateIn + (NonOvertimeMin / 60 / 24);
          EmployeeIDCard.DateInCutOff :=
            EmployeeIDCard.DateInCutOff + (NonOvertimeMin / 60 / 24);
        end;
        // Overtime minutes:
        // - Book as Exceptional made during overtime,
        //   Overtime, and Regular hours.
        if OvertimeMin <> 0 then
        begin
          OverTime := GlobalDM.BooksExceptionalTimeDuringOvertime(
            BookingDay, OvertimeMin, EmployeeIDCard, Manual);
          OverTime := GlobalDM.BooksOverTimeCombinedHours(BookingDay,
            OverTime, EmployeeIDCard, Manual);
          GlobalDM.BooksRegularSalaryHours(BookingDay,
            EmployeeIDCard, OverTime, Manual);
        end;
        EmployeeIDCard.DateIn := OldDateIn;
        EmployeeIDCard.DateInCutOff := OldDateInCutOff;
      end // if GlobalDM.DetectOvertimeNonOvertime
      else
      begin
        if not BookExceptionalBeforeOvertime then
        begin
          // Default: First book overtime, then exceptional
          NonOverTime := GlobalDM.BooksOverTime(BookingDay, ProdMin+PayedBreaks,
            EmployeeIDCard, Manual, 0, 0);
          NonOverTime := GlobalDM.BooksExceptionalTime((* AQuery, *) BookingDay,
            NonOverTime, EmployeeIDCard, Manual);
          GlobalDM.BooksRegularSalaryHours((* AQuery, *) BookingDay,
            EmployeeIDCard, NonOverTime, Manual);
        end
        else
        begin
          NonOvertimeMin := 0;
          OvertimeMin := 0;
          // First book exceptional then overtime
          NonOverTime := GlobalDM.BooksExceptionalTime((* AQuery, *) BookingDay,
            ProdMin+PayedBreaks, EmployeeIDCard, Manual);
          NonOverTime := GlobalDM.BooksOverTime(BookingDay, NonOverTime,
            EmployeeIDCard, Manual, OvertimeMin, NonOvertimeMin);
          GlobalDM.BooksRegularSalaryHours((* AQuery, *) BookingDay,
            EmployeeIDCard, NonOverTime, Manual);
        end; // if not BookExceptionalBeforeOvertime then
      end; // if GlobalDM.DetectOvertimeNonOvertime
    end; // if GlobalDM.BooksWorkedHoursOnBankHoliday
    // TD-23386 Do NOT use this anymore! It is handled in a different way.
{
    // MR:19-11-2002
    // Book hours for hourtype of Shift
    BooksHoursForShift(AQuery, BookingDay, EmployeeIDCard, ProdMin+PayedBreaks,
      Manual);
}
  end;
  GlobalDM.PayedBreakSave := 0; // RV028.
end;

// MR:26-1-2004
procedure ContractRoundMinutes(AQuery: TQuery; AEmployeeIDCard: TScannedIDCard;
  BookingDay: TDateTime; var AProdMin: Integer);
var
  WorkQuery: TQuery;
  NewTotalMinutes, TotalMinutes: Integer;
  RoundTruncSalaryHours, RoundMinute: Integer;
  function RoundMinutes(RoundTruncSalaryHours, RoundMinute: Integer;
    Minutes: Integer): Integer;
  var
    Hours, Mins: Integer;
  begin
    Hours := Minutes DIV 60;
    Mins := Minutes MOD 60;
    if RoundTruncSalaryHours = 1 then // Round
      Mins := Round(Mins / RoundMinute) * RoundMinute
    else
      Mins := Trunc(Mins / RoundMinute) * RoundMinute;
    Result := Hours * 60 + Mins;
  end;
begin
  AProdMin := 0;
  WorkQuery := TQuery.Create(nil);
  QueryCopy(AQuery, WorkQuery);
  WorkQuery.Close;
  WorkQuery.SQL.Clear;
  WorkQuery.SQL.Add(
    'SELECT ' +
    '  SUM(SALARY_MINUTE) AS TOTALMINUTE ' +
    'FROM ' +
    '  SALARYHOURPEREMPLOYEE ' +
    'WHERE ' +
    '  SALARY_DATE = :SALARY_DATE AND ' +
    '  EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER');
  WorkQuery.ParamByName('SALARY_DATE').AsDateTime := BookingDay;
  WorkQuery.ParamByName('EMPLOYEE_NUMBER').AsInteger :=
    AEmployeeIDCard.EmployeeCode;
  WorkQuery.Open;
  if not WorkQuery.IsEmpty then
  begin
    WorkQuery.First;
    TotalMinutes := Round(WorkQuery.FieldByName('TOTALMINUTE').AsFloat);
    AProdMinClass.ReadContractRoundMinutesParams(AEmployeeIDCard,
      RoundTruncSalaryHours, RoundMinute);
    NewTotalMinutes := RoundMinutes(RoundTruncSalaryHours, RoundMinute,
      TotalMinutes);
    AProdMin := NewTotalMinutes - TotalMinutes;
{$IFDEF DEBUG1}
WDebugLog('- ContractRoundMinutes()' +
  ' OldSalarySum=' + IntMin2StringTime(TotalMinutes, False) +
  ' NewSalarySum=' + IntMin2StringTime(NewTotalMinutes, False) +
  ' ProdMin=' + IntMin2StringTime(AProdMin, False));
{$ENDIF}
  end;
  WorkQuery.Active := False;
  WorkQuery.Free;
end;

// RV067.MRA.31
function AbsenceTotalField(const AAbsenceTypeCode: Char): String;
begin
  case AAbsenceTypeCode of
    'H':  Result := 'USED_HOLIDAY_MINUTE';
    'I':  Result := 'ILLNESS_MINUTE';
    'T':  Result := 'USED_TFT_MINUTE';
    'W':  Result := 'USED_WTR_MINUTE';
    //RV067.4.
    'R':  Result := 'USED_HLD_PREV_YEAR_MINUTE';
    'E':  Result := 'USED_SENIORITY_HOLIDAY_MINUTE';
    'F':  Result := 'USED_ADD_BANK_HLD_MINUTE';
    'S':  Result := 'USED_SAT_CREDIT_MINUTE';
    'C':  Result := 'USED_BANK_HLD_RESERVE_MINUTE';
    'D':  Result := 'USED_SHORTWWEEK_MINUTE';
    'X':  Result := 'NORM_BANK_HLD_RESERVE_MINUTE';
    // RV071.10.
    'Y':  Result := 'EARNED_SAT_CREDIT_MINUTE';
    // 20013183
    TRAVELTIME:  Result := 'EARNED_TRAVELTIME_MINUTE';
    USED_TRAVELTIME:  Result := 'USED_TRAVELTIME_MINUTE';
    else
      Result := '';
  end;
end;

procedure UpdateAbsenceTotalMode(AQuery: TQuery; EmployeeNumber,
  AbsenceYear: Integer;	Minutes: Double; AbsenceTypeCode: Char;
  AAddToOldMinutes: Boolean = True);
var
  WorkQuery: TQuery;
  UpdateField: String;
  OldMinutes: Double;
  SelectFlag1, FlagFieldName1: String;
  function TestFlag1: Boolean;
  begin
    Result := False;
    if UpdateField = 'NORM_BANK_HLD_RESERVE_MINUTE' then
      if (WorkQuery.FieldByName(FlagFieldName1).AsString = CHECKEDVALUE) then
        Result := True;
  end;
begin
  // RV067.MRA.31
  UpdateField := AbsenceTotalField(AbsenceTypeCode);
  if UpdateField = '' then
    Exit;

  SelectFlag1 := '';
  FlagFieldName1 := '';
  if UpdateField = 'NORM_BANK_HLD_RESERVE_MINUTE' then
  begin
    FlagFieldName1 := 'NORM_BANK_HLD_RES_MIN_MAN_YN';
    SelectFlag1 := ', ' + FlagFieldName1 + ' ';
  end;

  // RV071.3. Check on field NORM_BANK_HLD_RES_MIN_MAN_YN.
  WorkQuery := Tquery.Create(Nil);
  QueryCopy(AQuery,WorkQuery);
  WorkQuery.Active := False;
  WorkQuery.SQL.Clear;
  WorkQuery.SQL.Add(
    Format('SELECT %s ',[UpdateField]) +
    SelectFlag1 +
    'FROM ' +
    '  ABSENCETOTAL ' +
    'WHERE ' +
    '  EMPLOYEE_NUMBER=:EMPNO AND ' +
    '  ABSENCE_YEAR=:AYEAR');
//  WorkQuery.Prepare; // RV040.
  WorkQuery.ParamByName('EMPNO').AsInteger := EmployeeNumber;
  WorkQuery.ParamByName('AYEAR').AsInteger := AbsenceYear;
  WorkQuery.Active := True;
  if not WorkQuery.IsEmpty then
  begin
    // RV071.3. Only update under certain condition.
    if not TestFlag1 then
    begin
      //RV067.4.
      if AAddToOldMinutes then
       OldMinutes := WorkQuery.Fields[0].AsFloat
      else
        OldMinutes := 0;
      WorkQuery.Active := False;
      WorkQuery.SQL.Clear;
      WorkQuery.SQL.Add(
        'UPDATE ABSENCETOTAL ' +
        Format('SET %s=:NEWMIN ',[UpdateField]) +
        ', MUTATOR = :MUTATOR, MUTATIONDATE = :MUTATIONDATE ' +
        'WHERE EMPLOYEE_NUMBER=:EMPNO AND ' +
        'ABSENCE_YEAR=:AYEAR');
//     WorkQuery.Prepare; RV040.
      WorkQuery.ParamByName('EMPNO').AsInteger := EmployeeNumber;
      WorkQuery.ParamByName('AYEAR').AsInteger := AbsenceYear;
      WorkQuery.ParamByName('NEWMIN').AsFloat := OldMinutes + Minutes;
      WorkQuery.ParamByName('MUTATOR').AsString :=  SystemDM.CurrentProgramUser;
      WorkQuery.ParamByName('MUTATIONDATE').AsDateTime := Now;
      WorkQuery.ExecSQL;
    end;
  end
  else
  begin
    WorkQuery.Active := False;
    WorkQuery.SQL.Clear;
    WorkQuery.SQL.Add(
      'INSERT INTO ABSENCETOTAL( ' +
      'EMPLOYEE_NUMBER,ABSENCE_YEAR,CREATIONDATE,MUTATIONDATE, ' +
      Format('MUTATOR,%s) ',[UpdateField]) +
      'VALUES(:EMPNO,:AYEAR,:ADATE,:ADATE,:MUT,:NEWMIN)');
//     WorkQuery.Prepare; // RV040.
    WorkQuery.ParamByName('EMPNO').AsInteger := EmployeeNumber;
    WorkQuery.ParamByName('AYEAR').AsInteger := AbsenceYear;
    WorkQuery.ParamByName('ADATE').AsDateTime := Now;
    WorkQuery.ParamByName('MUT').AsString := SystemDM.CurrentProgramUser;
    WorkQuery.ParamByName('NEWMIN').AsFloat := Minutes;
    WorkQuery.ExecSQL;
  end;
  WorkQuery.Active := False;
  WorkQuery.free;
end;

function CharFromStr(Str: string; CharIndex: integer; BlankChar: char): char;
begin
  Result := BlankChar;
  if Length(str) < CharIndex then
    Exit;
  Result := Str[Charindex];
end;

function BuildMinTime(AStrTime: String): Integer;
begin
  try
    Result := StrTime2IntMin(Trim(AStrTime));
  except
    Result := 0;
  end;
end;

// MR:15-02-2006 Optimised. For calculating the minutes now procedures
// are used that were made for this purpose.
// RV071.7. Addition of FromDate.
function ComputePlanned(AQuery: TQuery; EmpNo: integer;
  WhatPlan: String; AFromDate: TDateTime = 0): integer;
var
  Department_Code, Plant_Code, TimeBlock, WhatBreak, AbsenceType: String;
{  AvailableWeekDay, UnPayedBreakMin: integer; }
{  StartD, StartTB, EndTB, StartBK, EndBk: TDateTime; }
{  year, month, day: word; }
{  TB: TTimeBlock; }
  WorkQuery, DetailQuery, BreakQuery: TQuery;
  AEmployeeData: TScannedIDCard;
  WeekDay, ProdMin, BreaksMin, PayedBreaks: Integer;
  StartD, AStart, AEnd: TDateTime;
begin
  // MR:13-10-2003 Instead of an 'exit' an 'if' is used,
  // to prevent errors like free-ing 'TQuery'-variables.
  Department_Code := NullStr;
  Plant_Code := NullStr;
  TimeBlock := NullStr;
  WhatBreak := NullStr;
  AbsenceType := NullStr;
  Result := 0;
  // RV071.7.
  if AFromDate <> 0 then
    StartD := Trunc(AFromDate)
  else
    StartD := Trunc(Now);
  WorkQuery := TQuery.Create(nil);
  QueryCopy(AQuery,WorkQuery);
  WorkQuery.Active := False;
  WorkQuery.SQL.Clear;
  WorkQuery.SQL.Add(
    'SELECT ' +
    '  DEPARTMENT_CODE, PLANT_CODE ' +
    'FROM ' +
    '  EMPLOYEE ' +
    'WHERE ' +
    '  EMPLOYEE_NUMBER=:EMPNO');
//   WorkQuery.Prepare; RV040.
  WorkQuery.ParamByName('EMPNO').AsInteger := EmpNo;
  WorkQuery.Active := True;
  if not WorkQuery.IsEmpty then
  begin
    Department_Code := WorkQuery.FieldByName('DEPARTMENT_CODE').AsString;
    Plant_Code := WorkQuery.FieldByName('PLANT_CODE').AsString;
    DetailQuery := TQuery.Create(nil);
    QueryCopy(AQuery,DetailQuery);
    BreakQuery := TQuery.Create(nil);
    QueryCopy(AQuery,BreakQuery);
    ComputePlanned := 0;
    // MR:14-02-2006 Query rewritten so it will get only records that
    // are needed for the 'whatplan'-parameter.
    WorkQuery.Active := False;
    WorkQuery.SQL.Clear;
    WorkQuery.SQL.Add(
      'SELECT ' +
      '  1 AS TB, ' +
      '  EA.AVAILABLE_TIMEBLOCK_1, ' +
      '  EA.SHIFT_NUMBER, EA.EMPLOYEEAVAILABILITY_DATE ' +
      'FROM ' +
      '  EMPLOYEEAVAILABILITY EA INNER JOIN ABSENCEREASON A ON ' +
      '    EA.AVAILABLE_TIMEBLOCK_1 = A.ABSENCEREASON_CODE ' +
      'WHERE ' +
      '  (EA.PLANT_CODE = :PCODE) AND (EA.EMPLOYEE_NUMBER = :EMPNO) AND ' +
      '  (EA.EMPLOYEEAVAILABILITY_DATE >= :STARTDATE) AND ' +
      '  A.ABSENCETYPE_CODE = :WHATPLAN ' +
      'UNION ' +
      'SELECT ' +
      '  2 AS TB, ' +
      '  EA.AVAILABLE_TIMEBLOCK_2, ' +
      '  EA.SHIFT_NUMBER, EA.EMPLOYEEAVAILABILITY_DATE ' +
      'FROM ' +
      '  EMPLOYEEAVAILABILITY EA INNER JOIN ABSENCEREASON A ON ' +
      '    EA.AVAILABLE_TIMEBLOCK_2 = A.ABSENCEREASON_CODE ' +
      'WHERE ' +
      '  (EA.PLANT_CODE = :PCODE) AND (EA.EMPLOYEE_NUMBER = :EMPNO) AND ' +
      '  (EA.EMPLOYEEAVAILABILITY_DATE >= :STARTDATE) AND ' +
      '  A.ABSENCETYPE_CODE = :WHATPLAN ' +
      'UNION ' +
      'SELECT ' +
      '  3 AS TB, ' +
      '  EA.AVAILABLE_TIMEBLOCK_3, ' +
      '  EA.SHIFT_NUMBER, EA.EMPLOYEEAVAILABILITY_DATE ' +
      'FROM ' +
      '  EMPLOYEEAVAILABILITY EA INNER JOIN ABSENCEREASON A ON ' +
      '    EA.AVAILABLE_TIMEBLOCK_3 = A.ABSENCEREASON_CODE ' +
      'WHERE ' +
      '  (EA.PLANT_CODE = :PCODE) AND (EA.EMPLOYEE_NUMBER = :EMPNO) AND ' +
      '  (EA.EMPLOYEEAVAILABILITY_DATE >= :STARTDATE) AND ' +
      '  A.ABSENCETYPE_CODE = :WHATPLAN ' +
      'UNION ' +
      'SELECT ' +
      '  4 AS TB, ' +
      '  EA.AVAILABLE_TIMEBLOCK_4, ' +
      '  EA.SHIFT_NUMBER, EA.EMPLOYEEAVAILABILITY_DATE ' +
      'FROM ' +
      '  EMPLOYEEAVAILABILITY EA INNER JOIN ABSENCEREASON A ON ' +
      '    EA.AVAILABLE_TIMEBLOCK_4 = A.ABSENCEREASON_CODE ' +
      'WHERE ' +
      '  (EA.PLANT_CODE = :PCODE) AND (EA.EMPLOYEE_NUMBER = :EMPNO) AND ' +
      '  (EA.EMPLOYEEAVAILABILITY_DATE >= :STARTDATE) AND ' +
      '  A.ABSENCETYPE_CODE = :WHATPLAN ' +
      'UNION ' +
      'SELECT ' +
      '  5 AS TB, ' +
      '  EA.AVAILABLE_TIMEBLOCK_5, ' +
      '  EA.SHIFT_NUMBER, EA.EMPLOYEEAVAILABILITY_DATE ' +
      'FROM ' +
      '  EMPLOYEEAVAILABILITY EA INNER JOIN ABSENCEREASON A ON ' +
      '    EA.AVAILABLE_TIMEBLOCK_5 = A.ABSENCEREASON_CODE ' +
      'WHERE ' +
      '  (EA.PLANT_CODE = :PCODE) AND (EA.EMPLOYEE_NUMBER = :EMPNO) AND ' +
      '  (EA.EMPLOYEEAVAILABILITY_DATE >= :STARTDATE) AND ' +
      '  A.ABSENCETYPE_CODE = :WHATPLAN ' +
      'UNION ' +
      'SELECT ' +
      '  6 AS TB, ' +
      '  EA.AVAILABLE_TIMEBLOCK_6, ' +
      '  EA.SHIFT_NUMBER, EA.EMPLOYEEAVAILABILITY_DATE ' +
      'FROM ' +
      '  EMPLOYEEAVAILABILITY EA INNER JOIN ABSENCEREASON A ON ' +
      '    EA.AVAILABLE_TIMEBLOCK_6 = A.ABSENCEREASON_CODE ' +
      'WHERE ' +
      '  (EA.PLANT_CODE = :PCODE) AND (EA.EMPLOYEE_NUMBER = :EMPNO) AND ' +
      '  (EA.EMPLOYEEAVAILABILITY_DATE >= :STARTDATE) AND ' +
      '  A.ABSENCETYPE_CODE = :WHATPLAN ' +
      'UNION ' +
      'SELECT ' +
      '  7 AS TB, ' +
      '  EA.AVAILABLE_TIMEBLOCK_7, ' +
      '  EA.SHIFT_NUMBER, EA.EMPLOYEEAVAILABILITY_DATE ' +
      'FROM ' +
      '  EMPLOYEEAVAILABILITY EA INNER JOIN ABSENCEREASON A ON ' +
      '    EA.AVAILABLE_TIMEBLOCK_7 = A.ABSENCEREASON_CODE ' +
      'WHERE ' +
      '  (EA.PLANT_CODE = :PCODE) AND (EA.EMPLOYEE_NUMBER = :EMPNO) AND ' +
      '  (EA.EMPLOYEEAVAILABILITY_DATE >= :STARTDATE) AND ' +
      '  A.ABSENCETYPE_CODE = :WHATPLAN ' +
      'UNION ' +
      'SELECT ' +
      '  8 AS TB, ' +
      '  EA.AVAILABLE_TIMEBLOCK_8, ' +
      '  EA.SHIFT_NUMBER, EA.EMPLOYEEAVAILABILITY_DATE ' +
      'FROM ' +
      '  EMPLOYEEAVAILABILITY EA INNER JOIN ABSENCEREASON A ON ' +
      '    EA.AVAILABLE_TIMEBLOCK_8 = A.ABSENCEREASON_CODE ' +
      'WHERE ' +
      '  (EA.PLANT_CODE = :PCODE) AND (EA.EMPLOYEE_NUMBER = :EMPNO) AND ' +
      '  (EA.EMPLOYEEAVAILABILITY_DATE >= :STARTDATE) AND ' +
      '  A.ABSENCETYPE_CODE = :WHATPLAN ' +
      'UNION ' +
      'SELECT ' +
      '  9 AS TB, ' +
      '  EA.AVAILABLE_TIMEBLOCK_9, ' +
      '  EA.SHIFT_NUMBER, EA.EMPLOYEEAVAILABILITY_DATE ' +
      'FROM ' +
      '  EMPLOYEEAVAILABILITY EA INNER JOIN ABSENCEREASON A ON ' +
      '    EA.AVAILABLE_TIMEBLOCK_9 = A.ABSENCEREASON_CODE ' +
      'WHERE ' +
      '  (EA.PLANT_CODE = :PCODE) AND (EA.EMPLOYEE_NUMBER = :EMPNO) AND ' +
      '  (EA.EMPLOYEEAVAILABILITY_DATE >= :STARTDATE) AND ' +
      '  A.ABSENCETYPE_CODE = :WHATPLAN ' +
      'UNION ' +
      'SELECT ' +
      '  10 AS TB, ' +
      '  EA.AVAILABLE_TIMEBLOCK_10, ' +
      '  EA.SHIFT_NUMBER, EA.EMPLOYEEAVAILABILITY_DATE ' +
      'FROM ' +
      '  EMPLOYEEAVAILABILITY EA INNER JOIN ABSENCEREASON A ON ' +
      '    EA.AVAILABLE_TIMEBLOCK_10 = A.ABSENCEREASON_CODE ' +
      'WHERE ' +
      '  (EA.PLANT_CODE = :PCODE) AND (EA.EMPLOYEE_NUMBER = :EMPNO) AND ' +
      '  (EA.EMPLOYEEAVAILABILITY_DATE >= :STARTDATE) AND ' +
      '  A.ABSENCETYPE_CODE = :WHATPLAN'
      );
//     WorkQuery.Prepare; // RV040.
    WorkQuery.ParamByName('PCODE').AsString := Plant_Code;
    WorkQuery.ParamByName('EMPNO').AsInteger := EmpNo;
    WorkQuery.ParamByName('STARTDATE').AsDateTime := StartD;
    WorkQuery.ParamByName('WHATPLAN').AsString := WhatPlan;
    WorkQuery.Active := True;
    while not WorkQuery.Eof do
    begin
      WeekDay := DayInWeek(SystemDM.WeekStartsOn,
        WorkQuery.FieldByName('EMPLOYEEAVAILABILITY_DATE').AsDateTime);
      ATBLengthClass.FillTBLength(
        EmpNo, WorkQuery.FieldByName('SHIFT_NUMBER').AsInteger,
        Plant_Code, Department_Code, True);
      ATBLengthClass.AEmployeeNumber := EmpNo;
      ATBLengthClass.APlantCode := Plant_Code;
      ATBLengthClass.AShiftNumber :=
        WorkQuery.FieldByName('SHIFT_NUMBER').AsInteger;
      ATBLengthClass.ADepartmentCode := Department_Code;
      ATBLengthClass.ARemoveNotPaidBreaks := False;
      AStart := Trunc(
        WorkQuery.FieldByName('EMPLOYEEAVAILABILITY_DATE').AsDateTime) +
        Frac(ATBLengthClass.GetStartTime(
          WorkQuery.FieldByName('TB').AsInteger, WeekDay));
      AEnd := Trunc(
        WorkQuery.FieldByName('EMPLOYEEAVAILABILITY_DATE').AsDateTime) +
        Frac(ATBLengthClass.GetEndTime(
          WorkQuery.FieldByName('TB').AsInteger, WeekDay));

      EmptyIDCard(AEmployeeData);
      AEmployeeData.EmployeeCode := EmpNo;
      AEmployeeData.PlantCode := Plant_Code;
      AEmployeeData.ShiftNumber :=
        WorkQuery.FieldByName('SHIFT_NUMBER').AsInteger;
      AEmployeeData.DepartmentCode := Department_Code;
      // Get the ProdMin (without breaks).
      AProdMinClass.ComputeBreaks(AEmployeeData, AStart, AEnd, 0,
        ProdMin, BreaksMin, PayedBreaks);
      Result := Result + ProdMin;
      WorkQuery.Next;
    end; // while not WorkQuery.Eof do
    DetailQuery.Free;
    BreakQuery.Free;
  end; // if not WorkQuery.IsEmpty then
  WorkQuery.Free;
end; // ComputePlanned

function GetNoFromStr(MyStr: String): integer;
var
  index: integer;
  NoStr: string;
begin
  Nostr := '';
  For index:=1 to Length(MyStr) do
    if MyStr[index] in ['-','0'..'9'] then
      Nostr := NoStr + MyStr[index]
    else
      if (length(NoStr)>0) then break;
  try
    result := StrToInt(NoStr);
  except
    result := 0;
  end;
end;

function ComputeTotalPlannedDayTime(AQuery: TQuery;
  AllBreaks: boolean; Day, EmplNo, ShiftNo: integer;
  PlantCode,DepCode: string): integer;
type
  BreakTimes = record
  StartTime, EndTime: TDateTime;
end;
var
  QueryWork: TQuery;
  TimeBlock{, Breaks}: string;
  TB: TTimeBlock;
  index, ib, PlanedTime: integer;
  StartTime, EndTime: TdateTime;
  PDates: ^BreakTimes;
  BreaksList: TList;
begin
  StartTime := 0; EndTime := 0;
  QueryWork := Tquery.Create(nil);
  QueryCopy(AQuery,QueryWork);
  PlanedTime := 0;
//  Result := PlanedTime;
  TimeBlock := WhatTimeBlock(QueryWork,EmplNo,ShiftNo,PlantCode,DepCode,TB);
  if TimeBlock > NullStr then
  begin
    //First Determins Breaks start/stop
    //BreaksPer
    QueryWork.Active := False;
    QueryWork.SQL.Clear;
    QueryWork.SQL.Add(
      Format('SELECT STARTTIME%d, ENDTIME%0:d ',[Day]) +
      'FROM ' +
      '  BREAKPEREMPLOYEE ' +
      'WHERE ' +
      '  EMPLOYEE_NUMBER = :EMPNO AND ' +
      '  PLANT_CODE = :PCODE ' +
      '  AND SHIFT_NUMBER = :SHNO ');
    if not AllBreaks then
      QueryWork.SQL.Add('AND PAYED_YN=:PAYED_YN');
//     QueryWork.Prepare; // RV040.
    QueryWork.ParamByName('EMPNO').AsInteger := EmplNo;
    QueryWork.ParamByName('PCODE').AsString := PlantCode;
    QueryWork.ParamByName('SHNO').AsInteger := ShiftNo;
    if not AllBreaks then
      QueryWork.ParamByName('PAYED_YN').AsString := CHECKEDVALUE;
    QueryWork.Active := True;
    if QueryWork.IsEmpty then
    begin
      // BREAKPERDEPARTMENT
      QueryWork.Active := False;
      QueryWork.SQL.Clear;
      QueryWork.SQL.Add(
        Format('SELECT STARTTIME%d, ENDTIME%0:d ',[Day]) +
        'FROM ' +
        '  BREAKPERDEPARTMENT ' +
        'WHERE ' +
        '  DEPARTMENT_CODE = :DCODE AND ' +
        '  PLANT_CODE = :PCODE ' +
        '  AND SHIFT_NUMBER = :SHNO ');
      if not AllBreaks then
        QueryWork.SQL.Add('AND PAYED_YN=:PAYED_YN');
//       QueryWork.Prepare; // RV040.
      QueryWork.ParamByName('DCODE').AsString := DepCode;
      QueryWork.ParamByName('PCODE').AsString := PlantCode;
      QueryWork.ParamByName('SHNO').AsInteger := ShiftNo;
      if not AllBreaks then
        QueryWork.ParamByName('PAYED_YN').AsString := CHECKEDVALUE;
      QueryWork.Active := True;
    end;
    if QueryWork.IsEmpty then
    begin
      // BREAKPERSHIFT
      QueryWork.Active := False;
      QueryWork.SQL.Clear;
      QueryWork.SQL.Add(
        Format('SELECT STARTTIME%d, ENDTIME%0:d ',[day]) +
        'FROM ' +
        '  BREAKPERSHIFT ' +
        'WHERE ' +
        '  PLANT_CODE = :PCODE ' +
        '  AND SHIFT_NUMBER = :SHNO ');
      if not Allbreaks then
        QueryWork.SQL.Add('AND PAYED_YN=:PAYED_YN');
//       QueryWork.Prepare; // RV040.
      QueryWork.ParamByName('PCODE').AsString := PlantCode;
      QueryWork.ParamByName('SHNO').AsInteger := ShiftNo;
      if not AllBreaks then
        QueryWork.ParamByName('PAYED_YN').AsString := CHECKEDVALUE;
      QueryWork.Active := True;
    end;
    BreaksList := Tlist.Create;
    while not QueryWork.Eof do
    begin
      New(PDates);
      PDates^.StartTime :=
      	QueryWork.FieldByName(Format('STARTTIME%d',[day])).AsDateTime;
      PDates^.EndTime :=
      	QueryWork.FieldByName(Format('ENDTIME%d',[day])).AsDateTime;
      if PDates^.StartTime > PDates^.EndTime then
        PDates^.EndTime := PDates^.EndTime + 1;
      BreaksList.Add(PDates);
      QueryWork.Next;  // MRA:22-12-2015 This was missing!!!
    end;
    // TimeBlocks
    QueryWork.Active := False;
    QueryWork.SQL.Clear;
    QueryWork.SQL.Add(
      'SELECT ' +
      '  AVAILABLE_TIMEBLOCK_1, AVAILABLE_TIMEBLOCK_2, ' +
      '  AVAILABLE_TIMEBLOCK_3, AVAILABLE_TIMEBLOCK_4, ' +
      '  AVAILABLE_TIMEBLOCK_5, AVAILABLE_TIMEBLOCK_6, ' +
      '  AVAILABLE_TIMEBLOCK_7, AVAILABLE_TIMEBLOCK_8, ' +
      '  AVAILABLE_TIMEBLOCK_9, AVAILABLE_TIMEBLOCK_10 ' +
      'FROM ' +
      '  STANDARDAVAILABILITY ' +
      'WHERE ' +
      '  PLANT_CODE = :PCODE AND DAY_OF_WEEK = :WDAY AND ' +
      '  SHIFT_NUMBER = :SHNO AND EMPLOYEE_NUMBER = :EMPNO');
//     QueryWork.Prepare; // RV040.
  for index := 1 to SystemDM.MaxTimeblocks do
    if TB[index] then
    begin
      QueryWork.Active := False;
      QueryWork.ParamByName('PCODE').AsString := PlantCode;
      QueryWork.ParamByName('WDAY').AsInteger := Day;
      QueryWork.ParamByName('SHNO').AsInteger := ShiftNo;
      QueryWork.ParamByName('EMPNO').AsInteger := EmplNo;
      QueryWork.Active := True;
      if (not QueryWork.IsEmpty) and
      	(QueryWork.FieldByName(
          Format('AVAILABLE_TIMEBLOCK_%d',[index])).AsString
          <> DEFAULTAVAILABILITYCODE) then
        TB[index] := False;
    end;
    QueryWork.Active := False;
    QueryWork.SQL.clear;
    QueryWork.SQL.Add(
      Format('SELECT STARTTIME%d, ENDTIME%0:d FROM %s ', [Day,TimeBlock]) +
      'WHERE ' +
      '  PLANT_CODE = :PCODE AND TIMEBLOCK_NUMBER = :TBNO AND ' +
      '  SHIFT_NUMBER = :SHNO ');
    if TimeBlock='TIMEBLOCKPEREMPLOYEE' then
      QueryWork.SQL.Add('AND EMPLOYEE_NUMBER=:EMPNO');
    if TimeBlock='TIMEBLOCKPERDEPARTMENT' then
      QueryWork.SQL.Add('AND DEPARTMENT_CODE=:DEPCODE');
//     QueryWork.Prepare; // RV040.
    QueryWork.ParamByName('PCODE').AsString := PlantCode;
    QueryWork.ParamByName('SHNO').AsInteger := ShiftNo;
    if TimeBlock='TIMEBLOCKPEREMPLOYEE' then
    	QueryWork.ParamByName('EMPNO').AsInteger := EmplNo;
    if TimeBlock='TIMEBLOCKPERDEPARTMENT' then
      QueryWork.ParamByName('DEPCODE').AsString := DepCode;
  for index := 1 to SystemDM.MaxTimeblocks do
    if TB[index] then
    begin
      QueryWork.Active := False;
      QueryWork.ParamByName('TBNO').AsInteger := index;
      QueryWork.Active := True;
      if not QueryWork.IsEmpty then
      begin
        StartTime :=
          QueryWork.FieldByName(Format('STARTTIME%d',[Day])).AsDateTime;
        EndTime := QueryWork.FieldByName(Format('ENDTIME%d',[Day])).AsDateTime;
        if StartTime > EndTime then
          EndTime := EndTime + 1;
        PlanedTime := Round(PlanedTime + (EndTime-StartTime)*DayToMin);
      end;
      For ib:= 0 to BreaksList.Count - 1 do
      begin
      	PDates := BreaksList[ib];
        PlanedTime := PlanedTime -
          ComputeMinutesOfIntersection(StartTime,EndTime,PDates^.StartTime,
            Pdates^.EndTime);
      end;
    end;
    // dealocate memory
    for ib:=0 to BreaksList.Count - 1 do
    begin
      PDates := BreaksList[ib];
      Dispose(Pdates);
    end;
    BreaksList.Free;
  end;
  Result := PlanedTime;
  QueryWork.Free;
end; // ComputeTotalPlannedDayTime

// MR:19-11-2002
// Book hours for shift, if shift has a hourtype
procedure BooksHoursForShift(AQuery: TQuery; BookingDay: TDateTime;
  EmployeeIDCard: TScannedIDCard; ProdMinPlusPayedBreaks: Integer;
  Manual: String);
var
  QueryWork: TQuery;
  HourtypeNumber: Integer;
begin
  if ProdMinPlusPayedBreaks > 0 then
  begin
    QueryWork := TQuery.Create(Nil);
    QueryCopy(AQuery, QueryWork);
    QueryWork.SQL.Clear;
    QueryWork.SQL.Add(
      'SELECT ' +
      '  HOURTYPE_NUMBER ' +
      'FROM ' +
      '  SHIFT ' +
      'WHERE ' +
      '  PLANT_CODE = :PLANT_CODE AND ' +
      '  SHIFT_NUMBER = :SHIFT_NUMBER');
    QueryWork.ParamByName('PLANT_CODE').AsString :=
      EmployeeIDCard.PlantCode;
    QueryWork.ParamByName('SHIFT_NUMBER').AsInteger :=
      EmployeeIDCard.ShiftNumber;
//     QueryWork.Prepare; // RV040.
    QueryWork.Open;
    if QueryWork.RecordCount > 0 then
    begin
      QueryWork.First;
      if QueryWork.FieldByName('HOURTYPE_NUMBER').AsString <> '' then
      begin
        HourtypeNumber := QueryWork.FieldByName('HOURTYPE_NUMBER').Value;
        GlobalDM.UpdateSalaryHour(BookingDay,
          EmployeeIDCard{.EmployeeCode}, HourtypeNumber,
          ProdMinPlusPayedBreaks, Manual, False { Don't replace! },
          True);
      end;
    end;
    QueryWork.Free;
  end;
end; // BooksHoursForShift

function SearchAbsenceReasonID(
  AQuery: TQuery; AbsenceReasonCode: String): Integer;
var
  QueryWork: TQuery;
begin
  Result := 0;
  QueryWork := TQuery.Create(nil);
  QueryCopy(AQuery, QueryWork);
  QueryWork.SQL.Clear;
  QueryWork.SQL.Add(
    'SELECT ' +
    '  ABSENCEREASON_ID ' +
    'FROM ' +
    '  ABSENCEREASON ' +
    'WHERE ' +
    '  ABSENCEREASON_CODE = :ABSENCEREASON_CODE');
  QueryWork.ParamByName('ABSENCEREASON_CODE').AsString := AbsenceReasonCode;
//   QueryWork.Prepare; // RV040.
  QueryWork.Open;
  if not QueryWork.IsEmpty then
    Result := QueryWork.FieldByName('ABSENCEREASON_ID').Value;
  QueryWork.Free;
end; // SearchAbsenceReasonID

// 20012858
function DetermineEfficiency(ANorm, AActualTime,
  AActualProd: Double; var ATheoTime: Double;
  var ATheoProd: Double): Double;
begin
  Result := 0;
  if ATheoTime = -1 then
  begin
    ATheoTime := 0;
    if ANorm <> 0 then
      ATheoTime := AActualProd / ANorm * 60;
  end;
  if ATheoProd = -1 then
  begin
    ATheoProd := 0;
    if ANorm <> 0 then
      ATheoProd := AActualTime / 60 * ANorm;
  end;
  if SystemDM.EffBasedOnQuantYN = 'Y' then
  begin
    // Eff. based on quantity
    if ATheoProd <> 0 then
      Result := AActualProd / ATheoProd * 100;
  end
  else
  begin
    // Eff. based on time
    if AActualTime <> 0 then
      Result := ATheoTime / AActualTime * 100;
  end;
end; // DetermineEfficiency

function BoolToStr(ABoolean: Boolean): String;
begin
  if ABoolean then
    Result := 'T'
  else
    Result := 'F';
end;

function Bool2Str(ABoolean: Boolean): String;
begin
  Result := BoolToStr(ABoolean);
end;

function MyQuotes(AValue: String): String;
begin
  Result := '''' + Trim(AValue) + '''';
end;

function ToDate(ADate: TDateTime): String;
var
  Year, Month, Day, Hours, Minutes, Seconds, MSeconds: Word;
  function FillZero(Value: String; Len: Integer): String;
  begin
    Result := Trim(Value);
    while Length(Result) < Len do
      Result := '0' + Result;
  end;
begin
  Result := '';
  try
    // Make a string like this:
    // TO_DATE('2013-09-10 08:16', 'YYYY-MM-DD HH24:MI')
    DecodeDate(ADate, Year, Month, Day);
    DecodeTime(ADate, Hours, Minutes, Seconds, MSeconds);
    Result := 'TO_DATE(''' +
      Format('%s-%s-%s %s:%s',
        [IntToStr(Year),
         FillZero(IntToStr(Month), 2),
         FillZero(IntToStr(Day), 2),
         FillZero(IntToStr(Hours), 2),
         FillZero(IntToStr(Minutes), 2)
        ]) +
      '''' + ', ''YYYY-MM-DD HH24:MI'')';
  except
    Result := '';
    WErrorLog('Error during ToDate-function!');
  end;
end; // ToDate

// GLOB3-60
// Find number of timeblocks for given plant and shift
function ShiftTBCount(APlantCode: String; AShiftNumber: Integer): Integer;
var
  IndexMax: Integer;
  SelectStr: String;
begin
  SelectStr :=
    'SELECT ' + NL +
    '  COUNT(*) AS RECNO ' + NL +
    'FROM ' + NL +
    '  TIMEBLOCKPERSHIFT ' + NL +
    'WHERE ' + NL +
    '  PLANT_CODE = ''' + DoubleQuote(APlantCode) + '''' + ' AND ' + NL +
    '  SHIFT_NUMBER = ' + IntToStr(AShiftNumber);
  OpenSql(SystemDM.qryTemp, SelectStr);
  if not SystemDM.qryTemp.Eof then
    IndexMax := Round(SystemDM.qryTemp.FieldByName('RECNO').AsFloat)
  else
    IndexMax := MIN_TBS;
  SystemDM.qryTemp.Close;
  // Determine max. timeblocks to show in planning dialog.
  if IndexMax < MIN_TBS then
    IndexMax := MIN_TBS;
  if IndexMax > SystemDM.MaxTimeblocks then
    IndexMax := SystemDM.MaxTimeblocks;
  Result := IndexMax;
end; // ShiftTBCount

// GLOB3-60
// Find max. number of  timeblocks for given plant
function PlantShiftMaxTBCount(APlantCode: String): Integer;
var
  IndexMax: Integer;
  SelectStr: String;
begin
  SelectStr :=
    'SELECT MAX(A.RECNO) RECNO ' + NL +
    'FROM ' + NL +
    '( ' + NL +
    'SELECT ' + NL +
    '  PLANT_CODE, SHIFT_NUMBER, COUNT(*) AS RECNO ' + NL +
    'FROM ' + NL +
    '  TIMEBLOCKPERSHIFT ' + NL +
    'WHERE ' + NL +
    '  PLANT_CODE = ''' + DoubleQuote(APlantCode) + '''' + ' ' + NL +
    'GROUP BY PLANT_CODE, SHIFT_NUMBER ' + NL +
    ') A';
  OpenSql(SystemDM.qryTemp, SelectStr);
  if not SystemDM.qryTemp.Eof then
    IndexMax := Round(SystemDM.qryTemp.FieldByName('RECNO').AsFloat)
  else
    IndexMax := MIN_TBS;
  SystemDM.qryTemp.Close;
  // Determine max. timeblocks to show in planning dialog.
  if IndexMax < MIN_TBS then
    IndexMax := MIN_TBS;
  if IndexMax > SystemDM.MaxTimeblocks then
    IndexMax := SystemDM.MaxTimeblocks;
  Result := IndexMax;
end; // PlantShiftMaxTBCount

// GLOB3-60
// Find max. number of  timeblocks (for all plants)
function ShiftMaxTBCount: Integer;
var
  IndexMax: Integer;
  SelectStr: String;
begin
  SelectStr :=
    'SELECT MAX(A.RECNO) RECNO ' + NL +
    'FROM ' + NL +
    '( ' + NL +
    'SELECT ' + NL +
    '  PLANT_CODE, SHIFT_NUMBER, COUNT(*) AS RECNO ' + NL +
    'FROM ' + NL +
    '  TIMEBLOCKPERSHIFT ' + NL +
    'GROUP BY PLANT_CODE, SHIFT_NUMBER ' + NL +
    ') A';
  OpenSql(SystemDM.qryTemp, SelectStr);
  if not SystemDM.qryTemp.Eof then
    IndexMax := Round(SystemDM.qryTemp.FieldByName('RECNO').AsFloat)
  else
    IndexMax := MIN_TBS;
  SystemDM.qryTemp.Close;
  // Determine max. timeblocks to show in planning dialog.
  if IndexMax < MIN_TBS then
    IndexMax := MIN_TBS;
  if IndexMax > SystemDM.MaxTimeblocks then
    IndexMax := SystemDM.MaxTimeblocks;
  Result := IndexMax;
end; // ShiftMaxTBCount

initialization
  UGLogFilename := UGLOGFILENAME_DEFAULT;
  UGErrorLogFilename := UGERRORLOGFILENAME_DEFAULT;

finalization

end.

