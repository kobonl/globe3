(*
  MRA:28-MAY-2013 New look Pims
  - Some pictures/colors are changed.
  MRA:24-JUN-2015 20014450.50
  - Real Time Eff.
  MRA:25-JAN-2017 PIM-250
  - Related to this issue: When 'embed dialogs' is used, then only show
    OK-button and center this, instead of showing OK+Cancel-button where
    Cancel-button is not enabled.
*)
unit DialogBaseFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ActnList, dxBarDBNav, dxBar, ComCtrls, ExtCtrls, StdCtrls,
  Buttons, TopPimsLogoFRM, jpeg, UPimsConst;

type
  TDialogBaseF = class(TTopPimsLogoF)
    pnlBottom: TPanel;
    btnOk: TBitBtn;
    btnCancel: TBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public

  end;
  
  TDialogBaseClassF= Class of TDialogBaseF;
var
  DialogBaseF: TDialogBaseF;

implementation

{$R *.DFM}

procedure TDialogBaseF.FormCreate(Sender: TObject);
begin
  inherited;
  pnlBottom.Color := pnlInsertBase.Color; // 20014450.50 // pnlImageBase.Color; // 20014289
  btnOK.Font.Color := clWhite;
  btnCancel.Font.Color := clWhite;
end;

procedure TDialogBaseF.FormShow(Sender: TObject);
begin
  inherited;
  pnlInsertBase.Caption := ''; // PIM-12
  // PIM-250
  btnCancel.Visible := btnCancel.Enabled;
  // Center btnOK
  if not btnCancel.Enabled then
    btnOK.Left := Round((pnlBottom.Width / 2) - (btnOK.Width / 2)); 

end;

end.
