(*
  Changes:
    MRA:25-FEB-2010. RV055.1.
    - Originally this was UGlobalDefs, but changed to UScannedIDCard
      to make it compatible with Delphi7-Pims-sources.
    MRA:25-FEB-2010. RV055.1.
    - Added 4 fields to IDCard:
      - ContractgroupCode
      - ExceptionalBeforeOvertime
      - RoundTruncSalaryHours
      - RoundMinute
    MRA:24-NOV-2010. RV080.6.
    - Added 1 field to IDCard:
      - WSHourtypeNumber
    MRA:7-FEB-2012. 2.0.162.215 20012085.3
    - Changes Belgium Market:
      - Overtime-hour calculation and Week/Day combined
    MRA:25-MAY-2012. 20012085.4
    - Disable Changes Belgium Market.
    MRA:1-OCT-2012 20013489 ShiftTrack.
    - Addition of ShiftDate-field to IDCard.
    - Addition of OvernightShift-field to IDCard, to know if it
      is about an overnight shift.
    MRA:26-OCT-2012 20013489
    - We do not need variable 'AOvernightShift'.
    MRA:13-NOV-2013 TD-23386
    - Hourtype on shift should not result in double hours.
    MRA:17-MAR-2015 20015346
    - Store scans in seconds:
      - Store original scans in 2 extra variable in TScannedIDCard
    MRA:23-MAR-2018 GLOB3-81 Rework
    - Keep track of first made scan (date-in).
*)
unit UScannedIDCard;

interface

uses
  DB;

type
  TScannedIDCard = record
    EmployeeCode, ShiftNumber,
    InscanEarly, InscanLate,
    OutScanEarly, OutScanLate: Integer;
    EmplName, IDCard,
    WorkSpotCode, WorkSpot,
    JobCode, Job,
    PlantCode, Plant, DepartmentCode,
    CutOfTime: String;
    DateIn, DateOut: TDateTime;
    DateInCutOff, DateOutCutOff: TDateTime; // MR:03-12-2003
    FromTimeRecordingApp: Boolean; // MR:26-10-2005
    IgnoreForOvertimeYN: String; // MRA:17-SEP-2009 RV033. Bugfix 1.
    ContractgroupCode: String; // RV055.1.
    ExceptionalBeforeOvertime: String; // RV055.1.
    RoundTruncSalaryHours: Integer; // RV055.1.
    RoundMinute: Integer; // RV055.1.
    WSHourtypeNumber: Integer; // RV080.6.
    ShiftHourtypeNumber: Integer; // TD-23386
    // 20012085.4
{    OvertimePerDayWeekPeriod: Integer; // 20012085.3
    WeekDayCombined: Boolean; // 20012085.3 }
    // 20013489
    ShiftDate: TDateTime;
    DateInOriginal, DateOutOriginal: TDateTime; // 20015346
    DateInFirstScan: TDateTime; // GLOB3-81
  end;

(*
procedure CopyIDCard(var ADestIDCard: TScannedIDCard;
  const ASourceIDCard: TScannedIDCard);
*)
procedure EmptyIDCard(var AIDCard : TScannedIDCard);
procedure PopulateIDCard(var IDCard: TScannedIDCard;
  ADataSet: TDataSet);
procedure CopyIDCard(var ADestIDCard: TScannedIDCard;
  const ASourceIDCard: TScannedIDCard);

implementation

uses
  GlobalDMT, UPimsConst;

//!!
procedure CopyIDCard(var ADestIDCard: TScannedIDCard;
  const ASourceIDCard: TScannedIDCard);
begin
  ADestIDCard.EmployeeCode   := ASourceIDCard.EmployeeCode;
  ADestIDCard.ShiftNumber    := ASourceIDCard.ShiftNumber;
  ADestIDCard.InscanEarly    := ASourceIDCard.InscanEarly;
  ADestIDCard.InscanLate     := ASourceIDCard.InscanLate;
  ADestIDCard.OutscanEarly   := ASourceIDCard.OutscanEarly;
  ADestIDCard.OutScanLate    := ASourceIDCard.OutScanLate;
  ADestIDCard.EmplName       := ASourceIDCard.EmplName;
  ADestIDCard.IDCard         := ASourceIDCard.IDCard;
  ADestIDCard.WorkSpotCode   := ASourceIDCard.WorkSpotCode;
  ADestIDCard.WorkSpot       := ASourceIDCard.WorkSpot;
  ADestIDCard.JobCode        := ASourceIDCard.JobCode;
  ADestIDCard.Job            := ASourceIDCard.Job;
  ADestIDCard.PlantCode      := ASourceIDCard.PlantCode;
  ADestIDCard.Plant          := ASourceIDCard.Plant;
  ADestIDCard.DepartmentCode := ASourceIDCard.DepartmentCode;
  ADestIDCard.CutOfTime      := ASourceIDCard.CutOfTime;
  ADestIDCard.DateIn         := ASourceIDCard.DateIn;
  ADestIDCard.DateOut        := ASourceIDCard.DateOut;
  ADestIDCard.DateInCutOff   := ASourceIDCard.DateInCutOff;
  ADestIDCard.DateOutCutOff  := ASourceIDCard.DateOutCutOff;
  // MRA:07-MAR-2008
  ADestIDCard.FromTimeRecordingApp      := ASourceIDCard.FromTimeRecordingApp;
  // MRA:17-SEP-2009 RV033.1.
  ADestIDCard.IgnoreForOvertimeYN       := ASourceIDCard.IgnoreForOvertimeYN;
  //!!
  ADestIDCard.ContractgroupCode         := ASourceIDCard.ContractgroupCode;
  ADestIDCard.ExceptionalBeforeOvertime := ASourceIDCard.ExceptionalBeforeOvertime;
  ADestIDCard.RoundTruncSalaryHours     := ASourceIDCard.RoundTruncSalaryHours;
  ADestIDCard.RoundMinute               := ASourceIDCard.RoundMinute;
  // RV080.6.
  ADestIDCard.WSHourtypeNumber := ASourceIDCard.WSHourtypeNumber;
  // 20012085.3
  // 20012085.4
{
  ADestIDCard.OvertimePerDayWeekPeriod := ASourceIDCard.OvertimePerDayWeekPeriod;
  ADestIDCard.WeekDayCombined := ASourceIDCard.WeekDayCombined;
}
  // TD-23386
  ADestIDCard.ShiftHourtypeNumber := ASourceIDCard.ShiftHourtypeNumber;
  // 20013489
  ADestIDCard.ShiftDate := ASourceIDCard.ShiftDate;
  ADestIDCard.DateInOriginal := ASourceIDCard.DateInOriginal; // 20015346
  ADestIDCard.DateOutOriginal := ASourceIDCard.DateOutOriginal; // 20015346
  ADestIDCard.DateInFirstScan := ASourceIDCard.DateInFirstScan; // GLOB3-81
end;

procedure EmptyIDCard(var AIDCard : TScannedIDCard);
begin
  AIDCard.EmployeeCode   := NullInt;
  AIDCard.IDCard         := NullStr;
  AIDCard.ShiftNumber    := NullInt;
  AIDCard.InscanEarly    := NullInt;
  AIDCard.InscanLate     := NullInt;
  AIDCard.OutscanEarly   := NullInt;
  AIDCard.OutScanLate    := NullInt;
  AIDCard.WorkSpotCode   := NullStr;
  AIDCard.WorkSpot       := NullStr;
  AIDCard.JobCode        := NullStr;
  AIDCard.Job            := NullStr;
  AIDCard.DateIn         := NullDate;
  AIDCard.DateOut        := NullDate;
  AIDCard.PlantCode      := NullStr;
  AIDCard.Plant          := NullStr;
  AIDCard.DepartmentCode := NullStr;
  AIDCard.EmplName       := NullStr;
  AIDCard.Plant          := NullStr;
  AIDCard.DateInCutOff   := NullDate;
  AIDCard.DateOutCutOff  := NullDate;
  AIDCard.ContractgroupCode         := NullStr; // RV055.1.
  AIDCard.ExceptionalBeforeOvertime := NullStr; // RV055.1.
  AIDCard.RoundTruncSalaryHours     := NullInt; // RV055.1.
  AIDCard.RoundMinute               := NullInt; // RV055.1.
  // MR:07-MAR-2008 RV004.
  AIDCard.FromTimeRecordingApp := False;
  // MRA:17-SEP-2009 RV033.1.
  AIDCard.IgnoreForOvertimeYN := NullStr;
  // RV080.6.
  AIDCard.WSHourtypeNumber := -1;
  // 20012085.3
  // 20012085.4
{
  AIDCard.OvertimePerDayWeekPeriod := 0;
  AIDCard.WeekDayCombined := False;
}
  // TD-23386
  AIDCard.ShiftHourtypeNumber := -1;
  // 20013489
  AIDCard.ShiftDate := NullDate;
  AIDCard.DateInOriginal := NullDate; // 20015346
  AIDCard.DateOutOriginal := NullDate; // 20015346
  AIDCard.DateInFirstScan := NullDate; // GLOB3-81
end;

procedure PopulateIDCard(var IDCard: TScannedIDCard;
  ADataSet: TDataSet);
begin
  GlobalDM.PopulateIDCard(IDCard, ADataSet);
end;

end.
