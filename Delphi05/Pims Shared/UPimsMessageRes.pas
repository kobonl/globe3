(*
  MRA:29-JAN-2016 PIM-143
  - Pims Translate to Japanese
  - Related to this issue:
    - Put all languages in 1 file with IFDEF's to separate them per language.
  MRA:28-MAY-2018 GLOB3-94
  - Added message about record-key-deleted.
*)
unit UPimsMessageRes;

interface

uses
  SysUtils, Dialogs;

resourcestring

{$IFDEF PIMSDUTCH}

  SPimsIncorrectPassword = 'Verkeerd wachtwoord .'#13'Probeer het nogmaals of '+
    'annuleer.';
  SPimsNoEmptyPasswordAllowed = 'Een leeg wachtwoord is niet'+
    'toegestaan.'#13'Probeer het nogmaals of annuleer.';
  SPimsPasswordChanged = 'Nieuw wachtwoord opgeslagen.';
  SPimsPasswordNotChanged = 'Wachtwoord niet opgeslagen.';
  SPimsPasswordNotConfirmed = 'Wachtwoord en bevestiging zijn '+
    'verschillend.'#13'Probeer het nogmaals of annuleer.';
  SPimsSaveChanges = 'Opslaan? ';
  SPimsRecordLocked = 'Dit record wordt door een andere gebruiker gebruikt.';
  SPimsOpenXLS = 'Excelbestand openen?';
  SPimsOpenHTML = 'Open HTML bestand';
  SPimsNotUnique = 'Niet uniek';
  SPimsNotFound = 'Niet gevonden';
  SPimsNotEnoughStock = 'Er zijn niet genoeg producten op voorraad!'#13'Items '+
    'toch uitgeven?';
  SPimsNoItems = 'Niets geselecteerd';
  SPimsNoEmployee = 'Er is geen werknemer!';
  SPimsNoCustomer = 'Er zijn geen klanten!';
  SPimsLoading = 'Laden';
  SPimsKeyViolation = 'Deze regel bestaat al.';
  SPimsForeignKeyPost = 'Dit record heeft een connectie met andere records.' + #13 +
    'Uw verzoek is geannuleerd.';
  SPimsForeignKeyDel = 'Dit record heeft een connectie met andere records.' + #13 +
    'Uw verzoek is geannuleerd.';
  SPimsFieldRequiredFmt = 'Vul aub de gevraagde invoer';
  SPimsFieldRequired = 'Aub alle verplichte velden vullen.';
  SPimsDragColumnHeader = 'Sleep kolomkop om kolom te groeperen';
  SPimsDetailsExist = 'Dit record heeft een koppeling met andere records. Verwijder deze records eerst.'; 
  SPimsDBUpdate = 'Update PIMS database';
  SPimsDBUpdateButtonCaption = '&Uitvoeren PIMS Update';
  SPimsDBUpdateConflict = 'Er zijn problemen met het updaten van de database. Neem kontakt op'+
    'met de helpdesk';
  SPimsDBNeedsUpdating = 'Oude databaseversie, database moet eerst worden geupdate!';
  SPimsCurrentCode = 'Huidige item code. ';
  SPimsContinue = ' Doorgaan?';
  SPimsConfirmCancelNote = 'Wilt u de afleverbon annuleren?';
  SPimsConfirmDeleteNote = 'Wilt u de afleverbon verwijderen?';
  SPimsConfirmDeleteLicense = 'Wilt u deze PIMS licentie verwijderen?';
  SPimsConfirmDelete = 'Wilt u dit record verwijderen?';
  SPimsCannotPrintLabel = 'You cannot print a label for the currently selected '+
    'item.';
  SPimsCannotPlaceFlag = 'You cannot place a flag on the currently selected '+
    'item.';

  // FOR PIMS:
  SPimsCannotOpenXLS = 'Excel installeren';
  SPimsCannotOpenHTML = 'U moet eerst een internet browser op uw pc installeren.';
  SSelectDummyWK = 'U kunt geen dummy werkplek selecteren!';
  SPimsPercentageNotCorrect = 'Fout: totaal % moet 100% zijn';
  SPimsTimeMustBeYounger = 'Fout: starttijd moet voor eindtijd liggen';
  SPimsNoMaster = 'Geen bedrijf gedefinieerd';
  SPimsNoSubDetail = 'Geen afdelingen gedefinieerd';
  SPimsNoDetail = 'Geen werkplek gedefinieerd';
//  SPIMSTimeIntervalError = 'Starttijd is vroeger dan vorige eindtijd.';
  SPIMSContractTypePermanent = 'Vast';
  SPIMSContractTypeTemporary = 'Tijdelijk';
  SPIMSContractTypePartTime = 'Extern';
  SPIMSContractOverlap = 'Starttijd van het contract is vroeger dan de vorige '+
    ' eindtijd contract';
  SPimsTBDeleteLastRecord = 'Alleen het laatste record kan verwijderd worden';
  SPimsTBInsertRecord = 'Het maximum is %d tijdblokken';
  SPimsStartEndDate = 'Startdatum is later dan einddatum';
  SPimsStartEndTime = 'Starttijd is later dan eindtijd';
  SPimsStartEndActiveDate = 'Startdatum aktief is later dan einddatum actief';
  SPimsStartEndInActiveDate = 'Startdatum inactief is later dan einddatum'+
    'inactief';
  SPimsWeekStartEnd = 'Startweek is later dan eindweek';
  SPimsBUPerWorkspotPercentages = 'Totaal moet 100% zijn';
  {SPimsBUPerWorkspotPercentagesBig = 'Het totaal van de percentages ' +
    'is groter dan 100';
  SPimsBUPerWorkspotPercentagesLess = 'Totaal van de percentages is ' +
   'minder dan 100, u kunt het scherm niet verlaten';}
  SPimsTBStartTime = 'Starttijd tijdblok ligt voor starttijd van de ploeg';
  SPimsTBEndTime = 'Eindtijd tijdblok ligt na eindtijd ploeg';
  SPimsBUPerWorkspotInsert = 'Er is geen werkplek beschikbaar, invoer onmogelijk';
  SPimsDateinPreviousPeriod = 'Er bestaat al een scan met deze tijd';
  // Time Recording
  SPimsExistJobCodes = 'Wisselen is niet mogelijk, jobcodes zijn gedefnieerd';
  SPimsExistBUWorkspot = 'Wisselen is niet mogelijk, bedrijfsonderdelen zijn gedefinieerd';
  SPimsIDCardNotFound = 'Pas niet gevonden';
  SPimsIDCardExpired = 'Pas is ongeldig';
  SPimsEmployeeNotFound = 'Werknemer niet gevonden';
  SPimsInactiveEmployee = 'Werknemer inactief';
  SPimsScanningsInMinute = 'Meerdere scanningen in 1 minuut is niet '+
    'toegestaan';
  SPimsStartDay = 'Start  van de dag';
  SpimsEndDay = 'Einde van de dag';
  SPimsNoWorkspot = 'Geen werkplekken gedefinieerd voor deze PC';
  SPimsNoJob = 'Geen jobcodes gedefinieerd voor deze werkplek';
  SPimsDeleteJob = 'Er zijn jobcodes gekoppeld, deze worden verwijderd!';
  // Selector Form - Grid Column name
  SPimsDeleteBusinessUnit = 'Bedrijfsonderdelen zijn gekoppeld, deze worden '+
    'verwijderd!';
  SPimsDeleteJobBU = 'Gekoppelde jobcodes + bedrijfsonderdelen worden verwijderd!';
  SPimsColumnPlant = 'Bedrijf';
  SPimsColumnWorkspot = 'Werkplek';
  // Selector Form - Captions
  SPImsColumnCode = 'Kode';
  SPimsColumnDescription = 'Omschrijving';
  SPimsWorkspot = 'Selecteer werkplek';
  SPimsJob = 'Selecteer job';
  SPimsJobCodeWorkSpot = 'Afsluiten is niet mogelijk, u moet eerst een jobcode defini�ren';
  SPimsSaveBefore = 'eerst opslaan';
  SPimsNoPlant = 'Geen bedrijf gedefinieerd';
//  SPimsNoEmployee = 'Er zijn geen werknemers';
// Time Recording Scanning
  SPimsScanDone = 'Scanning verwerkt';
  SPimsScanError = 'Scanning NIET verwerkt: database FOUT';
// team per department - extra check
  SPimsNoShift = 'Ploeg niet gedefinieerd';
  SPimsNoCompletedScans = 'Wijzigen afgeronde scanningen onmogelijk';

// Hours per Employee
  SPimsNoMorePlantsinTeam = 'De afdelingen binnen een team moeten tot hetzelfde '+
    'bedrijf behoren';
  SPimsTeamPerEmployee = 'Afdeling van de werknemer behoort niet tot het team';
  SPimsInvalidTime = 'Invoer onbekend';
  SPimsNegativeTime = 'Dubbelklik voor invoer van negatieve tijd (rood)';
  SPimsExistingRecord = 'Record met dezelfde parameters bestaat al';
  SPimsProductionHour = 'Update van productie uren per werknemer onmogelijk';
// bank holiday process
  SPimsSalaryHour = 'Update van salarisuren per werknemer onmogelijk';

// for reports
  SPimsAbsenceHour = 'Update van afwezigheidsuren per werknemer onmogelijk';
  SPimsConfirmProcessEmployee = 'Wilt u de feestdagen verwerken naar '+
    'de beschikbaarheid?';
  SPimsReportWTR_HOL_ILL_Checks = 'Minstens een afwezigheidsreden selecteren!';
  SPimsMaxNumberOfWeeks = 'Maximaal aantal weken is 13';
  SPimsEndTime = 'Eindtijd';
// update database
  SPimsStartTime = '< Starttijd';
  SPimsOn = 'aan';
  SPimsInvalidVersion = 'Ongeldige PIMS Versie!';
// Standard staff availability
  SUpdateDatabaseFrom = 'Update van database';
  SUpdateDatabaseTo = 'naar versie';
  SCheckAvailableTB = 'Toegestane waarden zijn * en  -';
  SCheckAvailableTBNotEmpty = 'Voer aub alle verplichte velden in!';
  SCopySTA1 = 'Beschikbaarheid bestaat reeds voor deze dag';
  SCopySTA2 = ',overschrijven ?';
  SCopySameValues = 'Hetzelfde record is geselecteerd !';
  SEmplSTARecords = ' Er is geen standaardbeschikbaarheid gedefinieerd voor deze werknemer ';
// shift schedule
  SCopySTANotValid = 'Geen tijdblokken gedefinieerd voor het geselecteerde record';
  SEmptyValues = 'Voer aub alle verplichte velden in !';
  SShiftSchedule = 'Toegestane waarden zijn 1..9 en -';
  SSHSShift = 'Dit zijn geen geldige waardes voor deze ploeg';
  SSHSWeekCopy = 'Startdatum voor kopie moet later liggen dan einddatum';
  SInvalidShiftSchedule = 'Ploegnummer is niet gedefinieerd of de start/eindtijd '+
    'per dag is leeg';
  SEmployeeAvail_0  = 'Werknemer ';
  SEmployeeAvail_1 = ' is al beschikbaar in deze ploeg op deze dag  ';
//  SEmployeeAvail_2 = ', Dag';
  SEmployeeAvail_3 = ', onmogelijk ploegnummer te wisselen.';
  SUndoChanges = 'De laatste wijzigingen worden niet opgeslagen';
  SStartWeek = 'Startweek moet later liggen dan de vorige eindweek';
  SCopySelectionShiftSchedule_1 = 'Ploegschema voor werknemer ';
  SCopySelectionShiftSchedule_2 = ' bestaat al. Overschrijven?';
// employee availability
  SCopyFinished = 'Kopi�ren gereed!';
  SEmptyRecord = 'Geen werknemer geselecteerd!';
  SEmplAvail = 'Toegestane waarden zijn  *,- of afwezigheidsreden';
  SValidTimeBlock = 'Alle tijdblokken invullen!';
  SEmployeePlanned = 'Werknemer is gepland, bij verdere invoer wordt de planning '+
    'verwijderd. Doorgaan?';
  SCopyFrom = 'Aub een record selectieren!';
  SCopyTo = 'Onmogelijke invoer.';
  SEMAOverwrite = 'Werknemer beschikbaar voor';
// staff planning
  SEMAShift = 'Ploeg';
  SEMAExists = 'Bestaat al. Overschrijven?';
  SSelectedWorkspots = 'Meer als 401 werkplekken gedefinieerd, maximum bereikt!';
  SEmptyWK = 'Geen werkplek gedefinieerd';
  SOverplanning = 'Meer dan 200% overpland';
  SMakeAvailable = 'Maak beschikbaar';
  SNotAcceptPlanned = 'De planning van deze werknemer wordt niet geaccepteerd!';
  SSaveChanges = 'Opslaan? ';
  SReadStdPlnIntoDaySelection = 'Planning is al gevuld voor meerdere '+
    'bedrijven/data';
  SContinueYN = 'Wilt u doorgaan ?';
  SDateFromTo = 'Einddatum moet na startdatum liggen ';
  SDateIntervalValidate = 'Datum mag niet gelijk zijn';
  SNoEmployee = 'Geen werknemer geselecteerd';
  SOCILevel = 'Nivo bezetting';
  SPlannedLevel = 'Gepland met nivo';
// report staff planning
  SPlanned200 = 'Meer dan 200% gepland met nivo ';
  SSameRangeDate = 'De interval periode moet hetzelfde zijn !';
// Workspot per employee
  SStartEndYear = 'Eindjaar moet na startjaar liggen';
  SWeek = 'Het is niet mogelijk meer dan een week te selecteren';
// Staff Availability
  SLEVELWK = 'Alleen nivo A,B,C toegestaan!';
  SInvalidWK = 'Alleen A..Z,a..z,1..9, en _ zijn toegestaan!';
  SChangePlaned = 'Werknemer is al gepland, bij verdere invoer wordt deze '+
    'planning verwijderd. Doorgaan?';
//  E&R
  SInvalidShiftNo = 'Verkeerd ploegnummer voor dag:%d';
  SInvalidAvailability = 'Verkeerde beschikbaarheid voor dag:%d,TB:%d';
  SMoreThan = 'Meer dan';
  SMonthReport = 'Maand';
  SCalculationwithOvertime = 'Berekening kwaliteitsbonus met overwerk report';
  SInvalidMistakeValue = 'Aantal fouten moet groter dan 0 zijn !';
 //
// productivity reports

  SNotDeleteRecord = 'Het is niet mogelijk dit record te verwijderen';
//
// jobcode
  SPimsDBError = 'Report id in gebruik.';
  SDaySelect = 'Startdatum moet voor einddatum liggen';
  SInterfaceCode = 'Koppelingscode bestaat al!';
//
  SWKInterfaceCode = 'Koppelingscode bestaat al';
  SWKRescanInterfaceCode = 'Meerdere koppelingscodes voor deze '+
    'werkplek gedefinieerd!';
// export payroll report
  SExceptionDataSetReport = 'Data set is leeg,report kan niet worden gemaakt, vul data set '+
    'eerst en probeer het dan opnieuw';
  SExceptionDataSetForms = 'Data set is leeg, grid kan niet ververst worden, vul data set '+
    'eerst en probeer het dan opnieuw';
// report quality incentive
  SPeriodReport = 'Periode';

  // Added at 6-9-2002
  SPimsHomePage  = 'Pims Homepage wordt binnenkort in gebruik genomen!';
  SPimsStartEndDateTime = 'Startdatum/tijd ligt na einddatum/tijd';
  SShowBUWK         = 'Bedrijfsonderd. per werkplek';
  SShowJobCode = 'Toon jobcodes';
  SPimsAutomaticRecord = 'Automatisch toegevoegde records kunnen niet gewijzigd worden ';
  SPimsNoTimeValue = 'Vul voor minstens een dag de tijd';
//  SReadStdPlnIntoDaySelection = 'Planning is al ingelezen voor enkele bedrijven/data';
  SReadStdPlnIntoDaySelectionFinish = 'Kopi�ren standaard planning naar dagplanning gereed';
  SADDWKDateCollection = 'Records toegevoegd';
  SDate = 'Datum ';
  STotalWeek = 'Totaal wk ';

  SCaptionProdRepHrs = 'Productieuren per werknemer';
  SCaptionSalRepHrs =  'Salarisuren per werknemer';

  SShowActiveEmpl = 'Toon actieve werknemers';
  SShowNotActiveEmpl = 'Toon inactieve werknemers';

  SRepHrsCUMWK = 'Werkplek';
  SRepHrsTotalCUM = 'Totaal werkplek';

  SRepHrsCUMWKBU = 'Bedrijfsonderdeel';
  SRepHrsTotalCUMBU = 'Totaal bedrijfsonderdeel';

  SRepHrsCUMWKPlant = 'Bedrijf';
  SRepHrsTotalCUMPlant = 'Totaal bedr.';

  SRepHrsCUMWKDept = 'Afdeling';
  SRepHrsTotalCUMDept = 'Totaal Afdeling';
  SRepJobCode = 'Jobcode';
  SRepTotJobCode = 'Totaal jobcode';

  SHeaderAbsRsn = 'Afwezigheidsreden';
  SHeaderWeekAbsRsn = 'Week';
  SHdTotalWeek = 'Totaal wk';
  SRepExpPayroll = 'Van';

  SReport = 'Report ';
  SReportWorkTimeReduction = 'Variabele werktijden ';
  SReportIllness = 'Ziekte';
  SReportHoliday = 'Vakantie';
  SActiveEmpl =  'Actieve werknemers';
  SInactiveEmpl = 'Inactieve werknemers';

  SRepHrsPerEmplActiveEmpl = 'Toon actieve werknemers';
  SRepHrsPerEmplInActiveEmpl = 'Toon inactieve werknemers';
  SRepProdHrs = 'Productieuren';
  SRepNonProdHrs = 'Niet-productieuren';
  SAll = 'Alle';
  SActive = 'Actief';
  SInactive = 'Inactief';
  SName = 'Naam ';
  SShortName = 'Verkorte naam';

  SShowProcessed =  'Toon afgerond';
  SShowNotProcessed  = 'Toon niet afgerond';

  SErrorOpenRep = 'Fout bij het openen van de database, report kan niet geopend worden';
  SErrorCreateRep = 'Fout report kan niet gemaakt worden';

  SLogEmpNo = 'Aantal gevonden records=';
  SLogChangedAbsenceTotal = '%d records gewijzigd';
  SLogDeletedRec = '%d records verwijderd';
  SLogEndAbsence = 'Einde: Defini�ren afwezigheidsuren per werknemer';
  SLogStartSalaryHours = 'Start: Defini�ren salarisuren per werknemer';
  SLogStartAvailability  = 'Start: Defini�ren werknemer beschikbaarheid';
  SLogChangedAbsence = '%d records gewijzigd';
  SlogAddedAbsence = '%d records toegevoegd';
  SLogEndAvailability = 'Einde: Defini�ren beschikbaarheid werknemer';
  SLogStartNonScanning = 'Start: Defini�ren beschikbaarheid  niet-scanners';
  SLogEndNonScanning  = 'Einde: Defini�ren beschikbaarheid niet-scanners';

  SReadPastIntoDayPlanFinish = 'Kopi�ren planning uit het verleden gereed';

  SStaffPlnEmployee = 'Werknemer';
  SStaffPlnName = 'Naam';
  SPimsTo = 'Tot';

  SPimsInterfaceCode  =
    'U kunt het scherm niet afsluiten, definieer minstens een koppelingscode die anders is dan de koppelingscode voor de job';
  SPimsOtherInterfaceCode  = 'Definieer een koppelingscode die anders is dan de koppelingscode voor de jobcode';
  SDeleteOtherInterfaceCode = 'Alle andere koppelingscodes worden verwijderd!';
  SPimsNoOtherInterfaceCodes = 'Er zijn geen andere koppelingscodes gedefinieerd, u kunt het scherm niet openen';

  SLogFoundEmployeeNo = 'Aantal records gevonden: %d';

   // MR: 18-09-2002 DialogProcessAbsenceHrsFRM
  SSProcessFinished = 'Proces voltooid';

  // 22-11-2002 - Translation
// Contract group form
   SOvertimeDay = 'Dag';
   SOvertimeWeek = 'Week';
   SValidMinutes = 'Minuten dienen korter dan 59 te zijn';
   SQuaranteedHourType = 'Kies aub een uren type';
//
//update cascade
   SConfirmUpdateCascadeEmpl =
     'Werknemer zal aangepast worden in alle betrokken records, doorgaan ? ';
   SConfirmDeleteCascadeEmpl =
     'Alle betrokken records van deze werknemer zullen worden gewist, doorgaan ? ';
   SEmpExist = 'Deze werknemer bestaat al ';
//export payroll MRA:RV095.4. Give better message: 'date' added.
   SNotDefaultSelection = 'Geen standaard datum-selectie. Weet u het zeker ?';
   SPeriodAlreadyExported = 'Deze periode is al ge�xporteerd. Weet u het zeker ?';

   SPimsSameSourceAndDest = 'De bron= en doel-computernaam mogen ' +
    'niet dezelfde zijn';

   SPimsUseJobCode = 'Kies een jobcode van een werkplek die ingesteld is om jobcodes te gebruiken';
   SNoTeam = 'Legen team: ';
   
   SPimsScanTooBig = 'Scanperiode langer dan 12 uren is niet toegestaan';

   SPimsInvalidAbsReason = 'Geen geldige afwezigheidsreden!';
   SPimsEmptyAbsReason = 'Vul het verplichte veld in voor de afwezigheidsreden!';
   SPimsEmptyDescription = 'Vul het verplichte veld in voor de omschrijving!';

// transfer free time
   SPimsDifferentYears = 'Doel- en bron-jaar mogen niet gelijk zijn!';
   SPimsCheckBox = 'Minstens een van de checkboxen moet zijn aangevinkt!';
   SPimsProcessFinish = 'Proces voltooid!';

   // MR:17-01-2003
   SPimsUseShift = 'Kies a.u.b. een ploeg!';
   SPimsUseWorkspot = 'Kies a.u.b. een werkplek!';

   // MR:28-01-2003
   SPimsNoEmployeeLine = 'No employee';

   SPimsNonScanningStation = 'Dit is geen tijdregistratie-station';
   SPimsNoPrevScanningFound = 'Geen vorige scanning gevonden';
   SPimsNoMatch = 'Geen match voor werknemer + pasje';

   SpimsScanExists = 'Er is al een scan-record op die dag voor deze werknemer. ' + #13 +
     'U mag dit item niet wijzigen.';

   SCopySTANotValidFrom = 'Er zijn geen geldige tijdblokken voor dit record!';
   SPimsDayPlanning = 'Dag planning: ';
   SPimsSTDPlanning = 'Standaard planning: ';
   SPimsTeamSelection = 'Team selectie: ';
   SPimsTeamSelectionAll = 'Alle teams';
   SPimsStaffTo =  ' tot ';
   SPimsPlant =  '      Bedrijf:  ';
   SPimsShift = '      Ploeg: ';
   SPimsTeam = 'Team ';

   SCopySTANotValidTo = 'Er zijn geen geldige tijdblokken voor dit record!';

   SPimsEXCheckIllMsgClose =
     'Er is al een openstaande ziektemelding, sluit deze eerst af';
   SPimsEXCheckIllMsgStartDate =
     'Startdatum moet groter zijn dan einddatum van laatst aangemaakte ziektemelding ';

   // MR:03-03-2003
   SPimsContract = 'Contract';
   SPimsPlanned = 'Gepland';
   SPimsAvailable = 'Beschikb.';

   // MR:07-03-2003
   SpimsYear = 'Jaar';
   SPimsWeek = 'Week';
   SPimsDate = 'Datum';

   //CAR - REPORT staff planning per day
   SPimsNewPageWK = 'Nieuwe pag. per werkplek';
   SPimsNewPageEmpl = 'Nieuwe pag. per werknemer';
   // report checklist
   SPimsAvailability = 'Beschikbaar: ';

   SConfirmDeleteCascadePlant =
     'Alle gekoppelde records van dit bedrijf zullen worden verwijderd. Bedrijf verwijderen ? ';
    SConfirmUpdateCascadePlant =
     'Bedrijf zal worden gewijzigd voor alle gekoppelde records. Bedrijf wijzigen ? ';
   SPlantExist = 'Dit bedrijf bestaat al ';
   //CAR: 27-03-2003
   SRepTeamTotEmpl ='Tot. werkn.';
   //CAR 29-03-2003
   SRepHRSPerEmplCorrection = 'Handmatige correcties';
   SRepHrsPerEmplTotal = 'Totaal';
   //CAR 31-03-2003
   SJanuary = 'Januari';
   SFebruary = 'Februari';
   SMarch = 'Maart';
   SApril = 'April';
   SMay = 'Mei';
   SJune = 'Juni';
   SJuly = 'Juli';
   SAugust = 'Augustus';
   SSeptember = 'September';
   SOctober = 'Oktober';
   SNovember = 'November';
   SDecember = 'December';
//CAR  2-04-2003 - report Production Detail
   SRepProdDetPerformance = 'Performance';
   SRepProdDetBonus = 'Bonus';
// CAR 2-4-2003 - Stand Staff availability
   SStdStaffAvailPlanned =
     'Werknemer is gepland in standaard-planning. De planning zal nu gewist worden. ' +
     'Wilt u doorgaan ?';
//CAR 7-4-2003
   SPimsJobs  = 'Jobcode';     
//CAR 15-4-2003
  SPimsStartEndDateTimeEQ = 'Start datum+tijd is groter of gelijk aan eind datum+tijd';
  SPimsStartEndTimeNull = 'Start tijd- of eind-tijd zijn leeg, wilt u toch doorgaan ?';

  SPimsNoData = 'Geen data';

//CAR 5-5-2003
  SPimsAbsence = 'Afwezig';
  SPimsDifference = 'Verschil';
// car 12-5-2003
  SExportDescEMPL = 'Employee description';
  SExportDescDept = 'Dept description';
  SExportDescPlant = 'Plant description';
  SExportDescShift = 'Shift description';
//
  SDummyWK = 'Dit is een ''dummy'' werkplek en kan daarom niet worden bewaard';
// CAR 28-8-2003 - Stand Staff availability
   SStdStaffAvailFuturePlanned =
     'Werknemer is al gepland in deze ploeg. Hierdoor zal de toekomstige planning worden gewist.';
// 9-4-2003
  SPimsInvalidWeekNr = 'Ongeldig weeknummer!';
// 9-4-2003
  SPimsHoursPerDayProduction = ' productie ';
  SPimsHoursPerDaySalary = ' salaris ';
  // MR:12-09-2003
  
  SPimsCompletedToNotScans = 'U kunt een afgesloten scan niet '#13'her-openen.';
// user rights
  SAplErrorLogin = 'Ongeldige gebruikersnaam of wachtwoord!';
  SAplNotConnect = 'Ongeldige connectie naar de database!';
  SAplNotVisible = 'Deze gebruiker mag de applicatie niet gebruiken';
// MR:3-11-2003 Moved from 'UStrings.pas'
  LSPimsEnterPassword = 'Geef het wachtwoord';
// MR:11-11-2003
  SPimsEmployeesAll = 'Alle werknemers';
  SPimsEmployeesOnWrongWorkspot = 'Op verkeerde werkplek';
  SPimsEmployeesNotScannedIn = 'Niet ingescand';
  SPimsEmployeesAbsentWithReason = 'Ziek met reden';
  SPimsEmployeesAbsentWithoutReason = 'Ziek zonder reden';
  SPimsEmployeesWithFirstAid = 'Met EHBO';
  SPimsEmployeesTooLate = 'Te laat';
// 550119 - Workspot per Employee form
  SPimsLevel = 'Nivo';
// Car: 15-1-2004
  SPimsEmplContractInDatePrevious =
   'Er is al een contract met deze datum';
// 550287
// Contract group form
   STruncateHrs = 'Afbreken';
   SRoundHrs = 'Afronden';
//550124
  SPimsPieces = 'Stuks';
  SPimsWeight = 'Gewicht';
//06-04-2004
  SPlannedAllLevels = 'Gepland ';
  SOCIAllLevel = 'Bezetting  ';
// MR:03-05-2004 Order 550313
  SPimsCleanUpProductionQuantities = 'Opschonen Productie Aantallen ouder dan %d weken?';
  SPimsCleanUpEmployeeHours = 'Opschonen Werknemer Uren ouder dan %d weken?';
  SPimsCleanUpTimerecordingScans = 'Opschonen Tijdregistratie Scans ouder dan %d weken?';
// MR:17-05-2004 Order 550315
  SPimsLabelCode = 'Code';
  SPimsLabelInterfaceCode = 'Interface Code';
// MR:26-07-2004 Order 550327
  SPimsNotPlannedWorkspot = 'Niet gepland';
// MR:09-08-2004 Order 550336
  SNumber = 'Nummer';
// MR:13-10-2004
  SPimsGeneralSQLError = 'BDE reported a general SQL Error';
// MR:26-10-2004
  SPimsEfficiencyAt = 'Eff. op';
  SPimsEfficiencySince = 'Eff. sinds';
// MR:28-10-2004
  SPimsConnectionLost = 'Error: Database-connectie verbroken. Probeer nogmaals.';
// MR:10-12-2004
  SSHSWeekDatesCross = 'Fout: Geselecteerde Van/Tot-datums overlappen elkaar!';  
// MR:15-12-2004
  SPimsHours = 'uren';
  SPimsDays = 'dagen';
  SPimsBonus = 'bonus';
  SPimsEfficiency = 'eff.';
// MR:11-01-2005
  SPimsTotalProduction = 'Totaal produktie';
// MR:11-01-2005
  SPimsProductionOutput = 'Produktie uitvoer';
  SPimsChartHelp = 'Kies tijd: klik op grafiek'; //, move: right-click between graph-items'; 
  SPimsMaxSalBalMin = 'Max.Sal.Balans';
// MR:11-01-2005 -> Pims Oracle!
  SPimsInvalidScan = 'Fout: U heeft een ongeldige scan opgegeven.';
  SPimsScanOverlap = 'Fout: Deze scan heeft een ongeldige tijd-interval.';
// MR:16-06-2005
  SPimsConfirmProcessEmployeeSelectedDate = 'Wilt u de feestdagen voor datum ' + #13 +
    '%s' + #13 + 'verwerken naar de beschikbaarheid?';
// MR:20-06-2005
  SPimsNoBankHoliday = 'Kies a.u.b. een feestdag datum';
// MR:27-09-2005
  SPimsUseJobcodesNotAllowed = 'Er bestaan nog niet-afgeronde scans voor deze werkplek. ' + #13 +
    'Wijzigen is niet mogelijk.';
// MR:14-11-2005
  SPimsUnknownBusinessUnit = 'Onbekend bedrijfsond.';
// MR:07-12-2006
  SOvertimeMonth = 'Maand';
// MR:05-01-2007
  SErrorWrongTimeBlock = 'FOUT: Verkeerde tijdblok voor werknemer';
  SErrorDuringProcess = 'Er waren fouten! Zie foutmeldingen hier boven.';
  SSProcessFinishedWithErrors = 'Proces voltooid met fouten!';
// MR:26-01-2007
  SPimsMonthGroupEfficiency = 'Maand. Groep Effic.';
  SPimsGroupEfficiency = 'Maand. Effic.';
// MR:14-SEP-2009
  SPimsTemplateYearCopyNotAllowed = 'Het is niet toegestaan om te kopi�ren naar dit jaar!';
  SPimsTemplateYearSelectNotAllowed = 'Het is niet toegestaan om dit jaar te selecteren!';
// MRA:8-DEC-2009 RV048.1.
  SPimsEmpPlanPWD = 'Het is niet toegestaan om de afdeling te veranderen, ' + #13 +
    ' omdat er medewerkers gepland zijn op deze werkplek/afdeling combinatie.';
// MRA:19-JAN-2010.
  SPimsNrOfRecsProcessed = 'Aantal verwerkte records: ';
// MRA:22-FEB-2010.
  SPimsNoShiftScheduleFound = 'Er is bestaat geen ploegschema voor deze week.';  
// MRA:18-MAY-2010. Order 550478.
  SPimsDeleteMachineWorkspotsNotAllowed = 'Delete not possible! There are workspots linked to this machine.';    
  SPimsDeleteMachineJobsNotAllowed = 'Delete not possible! There are jobs defined for this machine.';
  SPimsUpdateMachineCodeNotAllowed = 'Machine is linked to workspot(s). Changing the Machine Code is therefore not allowed.';
  SPimsUpdateMachineJobCodeNotAllowed = 'Machine-job is in use by a workspot-job. Changing the Machine Job Code is therefore not allowed.';
// SO:07-JUN-2010.
  SPimsIncorrectHourType = 'Voer a.u.b. een correct urensoort in!';
  SPimsScanLongerThan24h = 'De scan mag niet langer dan 24 uur zijn!';
// MRA:30-JUN-2010. Order 550478.
  SPimsMachineWorkspotNonMatchingJobs = 'WARNING: There are non-matching jobs for this workspot for the entered machine.' + #13 + 'Please be sure the jobs match for workspots that are linked to the entered machine!';
  SPimsPleaseDefineJobForMachine = 'WARNING: Please make changes for jobs codes on machine level.';
  SPimsDeleteNotAllowedForMachineJob = 'This is a machine job. Deletion is not allowed here, only on machine level.';
// MRA:24-AUG-2010. Order 550497. RV067.MRA.8.
  SPimsAbsenceReasons = 'afw. redenen';
  SPimsHourTypes = 'urensoorten';
  SPimsAbsenceTypes = 'afw. soorten';
// MRA:31-AUG-2010. RV067.MRA.21
  SPimsDescriptionNotEmpty = 'Voer a.u.b. een omschrijving in, u kunt dit veld niet leeg laten!';
  SPimsCounterNameFilled = 'De teller-naam moet worden ingevuld als de teller actief is!';
// MRA:21-SEP-2010. RV067.MRA.34
  SPimsPaste = 'Plakken';
  SPimsPasteExceptHours = 'Plakken bijz. uren definities vanaf "%s"';
  SPimsDefExceptHoursFound = 'Definities gevonden, deze zullen worden verwijderd. Doorgaan Ja / Nee?';
// MRA:4-OCT-2010. RV071.3. 550497
  SPimsCountersHaveBeenUpdated = 'Tellers zijn geupdate.';
  SPimsProcessToAvailIsReady = 'Verwerken naar beschikbaarheid is gereed.';
  SPimsRecalcHoursConfirm = 'Herberekenen van uren is inbegrepen. Doorgaan ?';
// MRA:11-OCT-2010. RV071.10. 550497
  SPimMaxSatCreditReached = 'Let op: Het maximum van de Zaterdag Krediet is nu bereikt!';
// MRA:1-NOV-2010.
  SPimsStartEndDateSmaller = 'Startdatum is kleiner dan einddatum' + #13 +
     'van een bestaande ziektemelding voor dezelfde werknemer.';
// MRA:8-NOV-2010.
  SPimsIsNoScannerWarning =
    'LET OP: Deze werknemer is geen scanner en van het type: ' + #13 +
    'Autom. scans obv standaard planning.' + #13 +
    'Het aanpassen van scans zal daarom verkeerde uren geven! ' + #13 +
    'Aanpassen is niet mogelijk!';
// MRA:17-JAN-2011. RV085.1.
  SPimsEmpPlanPWDFound = 'WAARSCHUWING: Er is gepland op deze werkplek/afdeling-combinatie' + #13 +
    'vanaf %s en/of later. Weet u zeker dat u de afdeling wilt wijzigen?';
// MRA:14-FEB-2011. RV086.4.
  SPimsSelPlantsCountry = 'De gekozen bedrijven moeten bij hetzelfde land horen!';
  SPimsSelNotPosMultExports = 'Keuze niet mogelijk, meerdere export types zijn niet toegestaan.';
// MRA:18-APR-2011. RV089.5
  SPimsAddAllWorkspots = 'Toevoegen ALLE werkplekken?';
// MRA:21-APR-2011. RV090.1.
  SPimsFinishLastTransactionFirst = 'Bevestig a.u.b. eerst de laatste kopieer-actie!';  
  SPimsConfirmLastTransactionFirst = 'De laatste kopieer-actie is nog niet bevestigd.' + #13 +
                                     'Bevestig/Annuleer deze eerst!' + #13 +
                                     'Ja=Bevestig Nee=Annuleer';
  SPimsConfirmLastTransaction = 'Wilt u de laatste kopieer-actie BEVESTIGEN?';
  SPimsUndoLastTransaction = 'Wilt u de laatste kopieer-actie ANNULEREN?';
// MRA:26-APR-2011. RV089.1.
  SPimsYes = '&Ja';
  SPimsNo = '&Nee';
  SPimsAll = '&Alle';
  SPimsNoToAll = 'N&ee v. Alle';
// MRA:27-APR-2011. RV091.1
  SPimsPasteOvertimeDef = 'Plakken overuren definities vanaf "%s"';
  SPimsOvertimeDefFound = 'Definities gevonden, deze zullen worden verwijderd. Doorgaan Ja / Nee?';
// MRA:24-MAY-2011. RV092.6
  SPimsEndTimeError = 'Eind-tijd moet groter zijn dan start-tijd!';  
// MRA:26-MAY-2011. RV093.1.
  SPimsFooterTotalContractGroup = 'Totaal Contr. gr.';
  SPimsFooterContractGroup = 'Contractgroep';
// MRA:1-JUN-2011. RV093.3.
  SPimsLoggingEnable = 'Voor in/uitschakelen van loggen is een herstart van Pims nodig.';
// MRA:6-JUN-2011. RV093.3.
  SPimsCleanUpEmpHrsLog = 'Opschonen gelogde werknemeruren ouder dan %d weken?';
  SPimsCleanUpTRSLog = 'Opschonen gelogde tijdregistratiescans ouder dan %d weken?';
// MRA:6-JUL-2011. RV094.5.
  SPimsFilenameAFAS = 'Loonmutaties ABS PIMS';
// MRA:11-JUL-2011. RV094.7.
  SPimsPlantDeleteNotAllowed = 'Verwijderen van een bedrijf is niet toegestaan.';
  SPimsPlantUpdateNotAllowed = 'Wijzigen van een bedrijf is niet toegestaan.';
  SPimsEmployeeDeleteNotAllowed = 'Verwijderen van een werknemer is niet toegestaan.';
  SPimsEmployeeUpdateNotAllowed = 'Wijzigen van een werknemer is niet toegestaan.';
  SPimsWorkspotDeleteNotAllowed = 'Verwijderen van een werkplek is niet toegestaan.';
  SPimsWorkspotUpdateNotAllowed = 'Wijzigen van een werkplek is niet toegestaan.';
// MRA:11-JUL-2011. RV04.8.
  SPimsEmployeeNotActive = 'Werknemer is niet-actief.';  
// MRA:19-OCT-2011. RV099.1.
  SPimsPleaseFillBothAbsReasons = 'Vul a.u.b. beide afwezigheidsredenen in.';
  SPimsLogStartTFTHolCorrection = 'Start TVT/Vacantie correctie.';
  SPimsLogEndTFTHolCorrection = 'Einde TVT/Vacantie correctie.';
  SPimsLogChangesMade = 'Aantal wijzigingen: %d';
// MRA:28-OCT-2011. RV100.2.
  SPimsEarnedTFT = 'TVT Verdiend';
  SPimsEarnedTFTSign = 'TVT-V';
// MRA:9-JAN-2012. RV103.4.
  SPimsHolidayCardTitle = 'Vakantie/Snipper kaart';
// MRA:13-JAN-2012. RV103.4.
  SJanuaryS = 'Jan.';
  SFebruaryS = 'Febr.';
  SMarchS = 'Mrt.';
  SAprilS = 'Apr.';
  SMayS = 'Mei';
  SJuneS = 'Jun.';
  SJulyS = 'Jul.';
  SAugustS = 'Aug.';
  SSeptemberS = 'Sept.';
  SOctoberS = 'Okt.';
  SNovemberS = 'Nov.';
  SDecemberS = 'Dec.';
// 20011796.2.
  SPimsRepHolCardOpeningBalance = 'Beginsaldo:';
  SPimsRepHolCardUsed = 'Opgenomen:';
  SPimsRepHolCardAvailable = 'Beschikbaar:';
  SPimsRepHolCardTFTEarned = 'TVT-V Verdiend:';
  SPimsRepHolCardTUsed = 'T Opgenomen:';
  SPimsRepHolCardTotAvail = 'Totaal beschikbaar:';
  SPimsRepHolCardLegenda = 'T = Tijd voor Tijd opgenomen, TVT-V = Tijd voor Tijd verdiend, V = Vakantie/snipper';
  SPimsRepHolCardLegendaTFTEarned = 'TVT-V = Tijd voor Tijd verdiend';
  SPimsInvalidCheckDigits = 'Ongeldig controle-getal! Geef a.u.b. een waarde op tussen 10 en 99.';
  SPimsEnteredDateRemark = 'Geef a.u.b. een Datum-IN op gelijk aan de gekozen datum of de volgende datum.';
  SPimsShifts = 'Ploegen'; // 20013551
  SPimsWorkspotHeader = 'Werkplek'; // 20013551
  // 20013723
  SPimsExportPeriod = 'Export overuren';
  SPimsExportMonth = 'Export loon';
  // 20013288.130
  SPimsExportEasyLaborChoice = 'Maak a.u.b. een keuze wat u wilt exporteren.';
  // 20014002
  SPimsTotalWorkspot = 'Totaal Werkplek';
  // 20014037.60
  SPimsPimsUser = 'Pims gebr.';
  SPimsWorkstation = 'Werkstation';
  // TD-22429
  SPimsStart = 'Start';
  // TD-22475
  SPimsPasteDatacolConnection = 'Plakken datacollectie-connecties vanaf "%s"';
  SPimsDatacolConnectionFound = 'Datacollectie-connecties gevonden, deze zullen worden verwijderd. Doorgaan Ja / Nee?';
  // TD-22503
  SChangeEmplAvail = 'Werknemer Beschikbaarheid gevonden, dit zal worden verwijderd vanwege niet-beschikbaar. Wilt u doorgaan?';
  // SO-20014715
  SPimsReportYes = 'Ja';
  SPimsReportNo = 'Nee';
  SPimsReportTill = 't/m';
  SPimsReportLate = 'laat';
  SPimsReportEarly = 'vroeg';
  // TD-23315 Changed this message:
  SPIMSTimeIntervalError = 'Starttijd is kleiner dan vorige eindtijd,' + #13 +
   'of eindtijd is groter dan volgende starttijd.';
  // 20013196
  SPimsDeleteLinkJob = 'Er is een link-job gedefinieerd. Deze zal worden verwijderd. Doorgaan Ja / Nee?';
  SPimsLinkJobExist = 'Het is niet toegestaan meer dan 1 link-job per werkplek te defini�ren.' + #13 +
    'Deze link-job zal niet worden opgeslagen.';
  // 20011800
  SPimsOK = '&Ok';
  SPimsFinalRun = '&Defin. Run';
  SPimsFinalRunWarning = 'DEFINITIEVE RUN, WEET U HET ZEKER?' + #13#13 +
    'Mutaties over deze periode kunnen niet meer worden uitgevoerd.';
  SPimsFinalRunExportDateWarning = 'DEFINITIEVE RUN' + #13#13 +
    'Geselecteerde export-periode is al ge�xporteerd (deels of compleet).' + #13#13 +
    'Het is niet toegestaan om deze export uit te voeren.';
  SPimsFinalRunExportDateWarningAdmin = 'DEFINITIEVE RUN' + #13#13 +
    'Geselecteerde export-periode is al ge�xporteerd (deels of compleet).' + #13#13 +
    'Wilt u de export starten?';
  SPimsTestRunExportDateWarning = 'TEST RUN' + #13#13 +
    'Geselecteerde export-periode is al ge�xporteerd (deels of compleet).' + #13#13 +
    'Wilt u de export starten?';
  SPimsFinalRunExportGapWarning = 'DEFINITIEVE RUN' + #13#13 +
    'Er zit een hiaat van %d dagen tussen de laatste-export-datum' + #13 +
    'en start-datum van de selectie.' + #13#13 +
    'Het is niet toegestaan om deze export uit te voeren.';
  SPimsFinalRunExportGapWarningAdmin = 'DEFINITIEVE RUN' + #13#13 +
    'Er zit een hiaat van %d dagen tussen de laatste-export-datum' + #13 +
    'en start-datum van de selectie.' + #13#13 +
    'Wilt u de export starten?';
  SPimsTestRunExportGapWarning = 'TEST RUN' + #13#13 +
    'Er zit een hiaat van %d dagen tussen de laatste-export-datum' + #13 +
    'en start-datum van de selectie.' + #13#13 +
    'Wilt u de export starten?';
  SPimsCloseScansSkipped = 'WAARSCHUWING: Sommige scans konden niet worden afgesloten vanwege een vergelijking' + #13 +
    'met de laatste-export-datum.';
  SPimsFinalRunAddAvaililityNotAllowed = 'Toevoegen van beschikbaarheid voor deze week is niet toegestaan' + #13 +
    'omdat het al is ge�xporteerd.';
  SPimsFinalRunDateChange = 'WAARSCHUWING: De datum is aangepast naar een datum volgend op de laatste-export-datum.';
  SPimsFinalRunAvailabilityChange = 'WAARSCHUWING: Sommige werknemer-beschikbaarheid kon niet worden aangepast vanwege de laatste-export-datum.';
  SPimsFinalRunTestRun = 'TEST RUN';
  SPimsFinalRunDeleteNotAllowed = 'WAARSCHUWING: Verwijderen is niet toegestaan omdat het al is ge�xporteerd.';
  SPimsFinalRunOnlyPartDelete = 'WAARSCHUWING: Slechts een deel van de week zal worden verwijderd, omdat een deel al is ge�xporteerd.';
  SPimsFinalRunShiftSchedule = 'WAARSCHUWING: Niet alles kon worden gekopieerd omdat een deel al was ge�xporteerd.';
  // SO-20014442
  SPimsYearMonth = 'Jaar/Maand';
  SPimsStartEndYearMonth = 'Eind jaar/maand moet hoger zijn dan start jaar/maand!';
  SPimsOnlySANAEmps = 'Alleen SANA werknemers';
  SPimWrongYearSelection = 'Verkeerde jaar-selectie: Van-tot-jaar moet hetzelfde zijn!';
  // 20015178
  SPimsPleaseEnterFakeEmpNr = 'Geef a.u.b. een uniek Dummy-Werknemernummer,' + #13 +
    'voor gebruik in het Persoonlijke Scherm.';
  SPimsFakeEmpNrInUse = 'Het ingevoerd Dummy-Werknemernummer is al in gebruik' + #13 +
    'bij een andere werkplek. Geef a.u.b. een ander nummer.';
  // 20015223
  SPimsWorkspots = 'Werkplekken';
  SPimsPlantTitle = 'Bedrijf';
  SPimsAllWorkspots = '<Alles>';
  SPimsMultipleWorkspots = '<Meerdere>';
  // 20013476
  SPimsValueSampleTimeMins = 'Sample Tijd: Ongeldige waarde! Geef a.u.b. een waarde tussen 1 en 30 minuten.';
  // 20015586
  SPimsIncludeDowntime = 'Incl. storingstijd aantallen:';
  SPimsIncludeDowntimeYes = 'Ja';
  SPimsIncludeDowntimeNo = 'Nee';
  SPimsIncludeDowntimeOnly = 'Toon alleen storingstijd';
  // TD-25948
  SPimsOpenCSV = 'Open CSV bestand?';
  SPimsCannotOpenCSV = 'Installeer a.u.b. Microsoft Excel (of een soortgelijk programma).';
  // PIM-53
  SPimsNoWorkingDays = 'Kies a.u.b. een of meer werk-dagen.';
  SPimsAddHoliday = 'Wilt u %d afwezigheidsdag(en) toevoegen aan werknemer-beschikbaarheid?';
  // PIM-53
  SPimsAddHolidayEmplAvail = 'Geef a.u.b. een geldige afwezigheidsreden-code op.';
  SPimsAddHolidayNoStandAvail = 'Verwerking is afgebroken: Er is geen standaard beschikbaarheid voor de bedrijf/ploeg-combinatie!';
  // PIM-52
  SPimsWorkScheduleDetailsExists = 'Werkschema Details bestaan al! Overschrijven?';
  SPimsWorkScheduleWrongRepeats = 'Verkeerde waarde voor wekelijkse herhaling! Geef een waarde op van 1 tot 26.';
  SPimsWorkScheduleInUse = 'Werkschema is nog in gebruik (Werknemer Contract) en kan daarom niet worden verwijderd!';
  SPimsInvalidShift = 'Geef a.u.b. een geldige ploeg op.';
  SPimsReady = 'Gereed';
  SPimsPlanningMessage1 = 'Niet verwerkt! Voor werknemer';
  SPimsPlanningMessage2 = 'en datum';
  SPimsPlanningMessage3 = 'er is planning gevonden.';
  SPimsNoStandAvailFound1 = 'Fout: Geen Standaard Beschikbaarheid gevonden voor werknemer';
  SPimsNoStandAvailFound2 = 'en ploeg';
  SPimsProcessWorkScheduleMessage1 = 'Verwerken werkschema voor werknemer';
  SPimsProcessWorkScheduleMessage2 = 'vanaf referentiedatum';
  // PIM-23
  SPimsTotals = 'Totaal';
  SPimsOTDay = 'Dag';
  SPimsOTWeek = 'Week';
  SPimsOTMonth = 'Maand';
  SPimsMO = 'MA';
  SPimsTU = 'DI';
  SPimsWE = 'WO';
  SPimsTH = 'DO';
  SPimsFR = 'VR';
  SPimsSA = 'ZA';
  SPimsSU = 'ZO';
  SPimsNormalHrs = 'Normale uren per dag:';
  SPimsOTPeriod = 'Periode';
  SPimsOTPeriodStarts = 'Start in week';
  SPimsOTWeeksInPeriod = 'Weken in periode:';
  SPimsOTWorkingDays = 'Gewerkte dagen:';
  SPimsCGRound = 'Afronden';
  SPimsCGTRunc = 'Afbreken';
  SPimsCGMinute = 'minuten';
  SPimsCGTo = 'tot';
  SPimsStandStaffAvailUsed = 'OPMERKING: Voor 1 of meer dagen is er een vaste afwezigheidsreden'#13'ingevuld gebaseerd op standaard beschikbaarheid.'#13'Dit zal niet worden overschreven.';
  SPimsShiftDate = 'Ploeg Datum';
  // ABS-27052
  SPimsSelectPrinter = 'Printer Selectie';
  SPimsSelectDefaultPrinter = 'Kies een default printer';
  SPimsNoPrinterFound = 'Geen printers gevonden!';
  SPimsPleaseSelectPrinter = 'Kies a.u.b. een printer.';
  // PIM-52
  SPimsNoCurrentEmpContFound = 'Geen huidig werknemer-contract gevonden voor werknemer';
  // PIM-203
  SPimsHostPortExists = 'Deze Host+Poort-combinatie bestaat al voor een andere werkplek.';
  // PIM-174
  SPimsCopied = 'Gekopieerd:';
  SPimsRows = 'rij(en).';
  SPimsEmp = 'Werknemer';
  SPimsEmpContract = 'Werknemer contract';
  SPimsWSPerEmp = 'Werkplekken per werknemer';
  SPimsStandStaffAvail = 'Standaard beschikbaarheid personeel';
  SPimsShiftSched = 'Ploeg schema';
  SPimsEmpAvail = 'Werknemer beschikbaarheid';
  SPimsStandEmpPlan = 'Werknemer standaard planning';
  SPimsEmpPlan = 'Werknemer planning';
  SPimsAbsenceCard = 'Afwezigheidskaart';
  SPimsRecordKeyDeleted = 'Dit record is verwijderd door een andere gebruiker.';
  SPimsWorkScheduleWrongStartDate = 'Referentie week moet een datum zijn, die begint op de eerste dag van de week.'#13'Dit is gecorrigeerd.';
  SPimsRoamingWorkspotExists = 'Er is al een andere werkplek ingesteld als transfer-werkplek.';
  SPimsHourlyWage = 'Uurloon:';
  SPimsTotalExtraPayments = 'Totaal Extra Betalingen';
  SPimsTBPEmp = 'Tijdblokken per werknemer';

{$ELSE}
  {$IFDEF PIMSFRENCH}

  SPimsHomePage = 'Pims Homepage will be implemented soon!';
  SPimsIncorrectPassword = 'Incorrect Password.'#13'Enter another password or '+
    'Cancel.';
  SPimsNoEmptyPasswordAllowed = 'An empty password is not allowed.'#13'Enter '+
    'another password or Cancel.';
  SPimsPasswordChanged = 'New password saved.';
  SPimsPasswordNotChanged = 'Password not changed.';
  SPimsPasswordNotConfirmed = 'Password and confirmation are '+
    'different.'#13'Enter another password or Cancel.';
  SPimsSaveChanges = 'Save changes?';
  SPimsRecordLocked = 'This record is in use by another user';
  SPimsOpenXLS = 'Open Excel file?';
  SPimsOpenHTML = 'Open HTML file?';
  SPimsNotUnique = 'Not Unique';
  SPimsNotFound = 'Nothing found!';
  SPimsNotEnoughStock = 'There are not enough products in stock!'#13'Issue the '+
    'items anyway?';
  SPimsNoItems = 'No items yet';
  SPimsNoEmployee = 'There are no employee!';
  SPimsNoCustomer = 'There are no customers!';
  SPimsLoading = 'Loading ';
  SPimsKeyViolation = 'This line already exists.';
  SPimsForeignKeyPost = 'This record has a connection to other records. Your '+
    'request is denied.';
  SPimsForeignKeyDel = 'This record has a connection to other records. Your '+
    'request is denied.';
  SPimsFieldRequiredFmt = 'Please fill required field %s.';
  SPimsFieldRequired = 'Please fill all required fields.';
  SPimsDragColumnHeader = 'Drag a column header here to group that column.';
  SPimsDetailsExist = 'This record has a connection to other records. Please '+
    'remove those records first.';
  SPimsDBUpdate = 'Update Pims database';
  SPimsDBUpdateButtonCaption = '&Run Pims update';
  SPimsDBUpdateConflict = 'Pims detected a conflict while updating the '+
    'database.'#13'Please call the ABS helpdesk and refer to this message.';
  SPimsDBNeedsUpdating = ' Pims detected on older database version.'#13+
   ' Pims needs to update your database to run properly.';
  SPimsCurrentCode = 'Current item code. ';
  SPimsContinue = ' Continue?';
  SPimsConfirmCancelNote = 'Do you want to cancel this delivery note?';
  SPimsConfirmDeleteNote = 'Do you want to delete this delivery note?';
  SPimsConfirmDeleteLicense = 'Do you want to delete this Pims License?';
  SPimsConfirmDelete = 'Do you want to delete the current record?';
  SPimsCannotPrintLabel = 'You cannot print a label for the currently selected '+
    'item.';
  SPimsCannotPlaceFlag = 'You cannot place a flag on the currently selected '+
    'item.';
  SPimsCannotOpenXLS = 'Please install Microsoft Excel.';
  SPimsCannotOpenHTML = 'Please install an Internet Browser on your machine.';

  // FOR PIMS:
  SSelectDummyWK = 'You can not select a dummy workspot!';
  SPimsPercentageNotCorrect = 'Error: Total percentage for workspot is not 100%';
  SPimsTimeMustBeYounger = 'Error: Start-time must be less than End-time!';
  SPimsNoMaster = 'There are no Process Units';
  SPimsNoSubDetail = 'There are no Departments';
  SPimsNoDetail = 'There are no Workspots';
  SPIMSContractTypePermanent = 'Permanent';
  SPIMSContractTypeTemporary = 'Temporary';
  SPIMSContractTypePartTime = 'External';
  SPIMSContractOverlap = 'There is an overlap for this contract with last contract';
  SPimsTBDeleteLastRecord = 'Only the last record can be deleted';
  SPimsTBInsertRecord = 'Only %d timeblocks allowed';
  SPimsStartEndDate = 'Start date is bigger than end date';
  SPimsStartEndTime = 'Start time is bigger than end time';
  SPimsStartEndDateTime = 'Start date time is bigger than end date time';
  SPimsStartEndActiveDate = 'Start active date is bigger than end active date';
  SPimsStartEndInActiveDate = 'Start inactive date is bigger than end inactive date';
  SPimsWeekStartEnd = 'End week < Start week !';
  SPimsBUPerWorkspotPercentages = 'Total of percentages should be 100,  ' +
    'you can not leave the form';
  SPimsTBStartTime = 'Starttime TimeBlock < Starttime Shift on ';
  SPimsTBEndTime = 'Endtime TimeBlock > Endtime Shift on ';
  SPimsBUPerWorkspotInsert = 'There is no workspot you can not insert records';
  SPimsDateinPreviousPeriod =  'There already is a scan that includes this time';
  SPimsExistJobCodes = 'You cannot change it, there are jobcodes defined';
  SPimsExistBUWorkspot = 'You cannot change it, there are relateted records in Business Units per Workspots';
  // Time Recording
  SPimsIDCardNotFound = 'ID Card not found';
  SPimsIDCardExpired	= 'ID Card has expired';
  SPimsEmployeeNotFound = 'Employee not found';
  SPimsInactiveEmployee = 'Employee is inactive';
  SPimsScanningsInMinute = 'Not allow multiple scannings within one minute';
  SPimsStartDay = 'Start of day';
  SpimsEndDay = 'End of day';
  SPimsNoWorkspot = 'No Workspot defined on current workstation';
//Workspot
  SPimsNoJob = 'No Jobs defined on current Workspot';
  SPimsDeleteJob = 'There are jobcodes assigned, they will be deleted!';
  SPimsDeleteBusinessUnit = 'There are business units assigned, they will be deleted!';
  SPimsDeleteJobBU =
    'All connected records with this workspot will be deleted, delete workspot ? ';
  SShowBUWK         = 'Show business units per workspot';
  SShowJobCode = 'Show job codes';
//workspot per workstation
  SPimsSameSourceAndDest = 'The source and destination computer name could ' +
   	'not be the same';
  // Selector Form - Grid Column name
  SPimsColumnPlant 	= 'Plant';
  SPimsColumnWorkspot = 'Workspot';
  SPImsColumnCode = 'Code';
  SPimsColumnDescription = 'Description';
  // Selector Form - Captions
  SPimsWorkspot = 'Select Workspot';
  SPimsJob 	= 'Select Job';
  SPimsJobCodeWorkSpot = 'You can not close form, first define at least one jobcode';
  SPimsInterfaceCode  =
   'You can not close form, first define at least one interface code other than the interface code for the job';
  SPimsOtherInterfaceCode  = 'Please define other interface code than the interface code of the job code';
  SPimsNoOtherInterfaceCodes = 'There are no other interface codes defined, you can not open form';

  SPimsSaveBefore = 'Please save the record before';
  SPimsNoPlant = 'There are no plants';
  SPimsScanDone = 'Scanning processed';
  SPimsScanError = 'Scanning NOT processed: Data-Base ERROR';
//  SPimsNoEmployee = 'There are no employees';
// Time Recording Scanning
  SPimsNoShift = 'No Shift defined';
  SPimsNoCompletedScans = 'Completed scans cannot be modify / deleted';
  SPimsNonScanningStation = 'This is not a time recording station';
  SPimsScanTooBig = 'Scan period longer than 12 hours is not allowed';
  SPimsUseJobCode = 'Please select a job code, workspot it is defined to use jobcodes';
  SPimsNoMatch = 'No match for Employee + ID-Card';
// team per department - extra check
  SPimsNoMorePlantsinTeam =
    'All department of one team should belong to the same plant !';
  SPimsTeamPerEmployee =
    'Department of the employee should be in the team !';

// Hours per Employee
  SPimsAutomaticRecord = 'Automatic inserted record cannot be changed ';
  SPimsNegativeTime = 'Double click to set a negative time (Red color)';
  SPimsExistingRecord = 'A record with the same parameters already exist.'+#13+
  	'Please select that record';
  SPimsProductionHour = 'ProductionHourPerEmployee table cannot updated';
  SPimsNoTimeValue = 'Please insert time value for at least one day';
  SPimsSalaryHour = 'SalaryHourPerEmployee table cannot updated';
  SPimsAbsenceHour = 'AbsenceHourPerEmployee table cannot updated';
  SPimsTo = 'To';
  SPimsInvalidTime = 'Value not set! (Invalid time value)';
// bank holiday process
  SPimsConfirmProcessEmployee = 'Do you want to process all bank holidays of ' +
    ' the year to employee availability ?';
  SPimsInvalidAbsReason = 'Not a valid absence reason!';
  SPimsEmptyAbsReason = 'Please fill required field absence reason!';
  SPimsEmptyDescription = 'Please fill required field description!';
// for reports
  SPimsReportWTR_HOL_ILL_Checks = 'You should select at least one absence reason!';
  SPimsMaxNumberOfWeeks = 'Maximal number of selected weeks is 13!';
  SPimsEndTime = 'End time';
  SPimsStartTime = ' < Start time';
  SPimsOn = 'on' ;
// update database
  SPimsInvalidVersion = 'Invalid PIMS version !';
  SUpdateDatabaseFrom = 'Database will be updated from version ';
  SUpdateDatabaseTo = ' to version ';
// Standard staff availability
  SCheckAvailableTB = 'Permitted values are * and -';
  SCheckAvailableTBNotEmpty = 'Please fill all required fields !';
  SCopySTA1 = 'Standard availability already exists for day ';
  SCopySTA2 = ', overwrite ? ';
  SCopySameValues = 'You selected the same record !';
  SCopySTANotValidFrom = 'There are no valid timeblocks for selected record!';
  SCopySTANotValidTo = 'There are no valid timeblocks for selected record of grid!';
  SEmptyValues = 'Please fill all fields before!';
  SEmplSTARecords = ' There are no standard availabilities defined for this employee ';
// shift schedule
  SShiftSchedule = 'Permitted values are numbers 0 .. 99 or -';
  SSHSShift = 'Permitted values are only shift defined';
  SSHSWeekCopy = 'Start date of copy should be bigger than end date';
  SInvalidShiftSchedule = 'Shift number has not defined or values start/end' +
    ' time per day are zero!';
  SEmployeeAvail_0  = 'Employee ';
  SEmployeeAvail_1  = ' already has availability in this shift on year, week, day ';
//  SEmployeeAvail_2 = ', day ';
  SEmployeeAvail_3 = ', you can not change the shift number.';
  SUndoChanges = 'Last changes will not be saved!';
  SStartWeek = 'Start week should be bigger than end week!';
  SCopySelectionShiftSchedule_1 = 'Shift schedule for employee ';
  SCopySelectionShiftSchedule_2 = ' already exists. Overwrite ?';
  SCopyFinished = 'Copy is finished!';
  SEmptyRecord = 'There is no employee selected!';
// employee availability
  SEmplAvail = 'Permitted values are *, - or an absence reason';
  SValidTimeBlock = 'All valid timeblocks should be filled!';
  SEmployeePlanned = 'Employee is already planned, continuing will delete planning. ' +
    'Do you want to continue ?';
  SCopyFrom  = 'Please select a record!';
  SCopyTo  = 'Records are the same, please select a different record!';
  SEMAOverwrite = 'Employee availability for ';
  SEMAShift = ' shift ';
  SEMAExists = 'already exists. Do you want to overwrite?';
// staff planning
  SSelectedWorkspots = 'There are more than 401 selected workspots, only the first 401  will ' +
    ' be displayed ';
  SEmptyWK = 'There are no workspots selected';
  SOverplanning = 'Overplanning more than 200%';
  SMakeAvailable = 'Make available';
  SNotAcceptPlanned = 'This planning of employee is not accepted!';
  SSaveChanges = 'Save changes?';
  SReadStdPlnIntoDaySelection = 'Planning already read for a plant/team/date within the selection';
  SReadStdPlnIntoDaySelectionFinish = 'Read standard planning into day planning finished';
  SReadPastIntoDayPlanFinish = 'Read past into day planning finished';
  SContinueYN = 'Do you want to continue ?';
  SDateFromTo = 'End date should be bigger than start date!';
  SDateIntervalValidate = ' Interval of target date should be different than interval' +
    ' of source date';
  SNoEmployee = 'There are no employees selected!';
  SOCILevel = 'Occupation level ';
  SPlannedLevel = 'Planned with level ';
  SPlannedAllLevels = 'Planned ';
  SOCIAllLevel = 'Occupation  ';
  SSameRangeDate = 'Date intervals should have the same length!';
// report staff planning
  SStartEndYear = 'End year should be bigger than start year!';
  SWeek = 'You can not select more than one week!';
  SNoTeam = 'Empty team: ';
// Workspot per employee
  SLEVELWK = 'Only A, B, C are allowed!';
  SInvalidWK = 'Only A..Z, a..z, 0..9, and ''_'' are allowed !';
// Staff Availability
  SChangePlaned = 'Employee is already planned, continuing will delete planning'+
    #13+'Do you want to continue?';
  SInvalidShiftNo = 'Invalid Shift number for day: %d';
  SInvalidAvailability = 'Invalid Availability value for day: %d, TB: %d';
//  E&R
  SMoreThan = ' More than ';
  SInvalidMistakeValue =  'Number of mistakes should be bigger than zero !';
  SNotDeleteRecord = ' You can not delete this record !';
  SPimsDBError = 'Report openned by other station or Database error. '+#13+
		'Please run report again';
 //
// productivity reports

  SDaySelect = 'Start day should be less then end day!';
//
// jobcode
  SInterfaceCode = 'This interface code is already used for other workspots!';
  SWKInterfaceCode = 'This interface code is already used for this workspot!';
  SWKRescanInterfaceCode =
   'There are interface codes used more than once for this workspot!';
  SDeleteOtherInterfaceCode = 'All other interface codes defined will be deleted!';

//
  SExceptionDataSetReport =
   'Data set is empty cannot create report, please set it first and rebuild pims';
  SExceptionDataSetForms  =
   'Data set is empty cannot refresh grids, please set it first and rebuild pims';
// export payroll report
   SPeriodReport = 'Period';
   SMonthReport = 'Month';
// report quality incentive
   SCalculationwithOvertime = 'Quality incentive calculation with overtime report';
// add wk into datacollection
  SADDWKDateCollection = 'Records are added';
// strings for mistake per employee
  SDate = 'Date ';
  STotalWeek = 'Total week ';
// report hrs per empl
  SCaptionProdRepHrs = 'Production hours per employee';
  SCaptionSalRepHrs =  'Salary hours per employee';
//report CompHours
  SShowActiveEmpl = 'Show active employee';
  SShowNotActiveEmpl = 'Show not active employee';
//report HrsWKCum
  SRepHrsCUMWK = 'Workspot';
  SRepHrsTotalCUM = 'Total workspot';

  SRepHrsCUMWKBU = 'Business unit';
  SRepHrsTotalCUMBU = 'Total business unit';

  SRepHrsCUMWKPlant = 'Plant';
  SRepHrsTotalCUMPlant = 'Total plant';

  SRepHrsCUMWKDept = 'Department';
  SRepHrsTotalCUMDept = 'Total department';
  SRepJobCode = 'Job code';
  SRepTotJobCode = 'Total job code';
// report absence
  SHeaderAbsRsn = 'Absence reason';
  SHeaderWeekAbsRsn = 'Week';
  SHdTotalWeek = 'Total week';
  SRepExpPayroll = 'From';
// report absence schedule
  SReport = 'Report ';
  SReportWorkTimeReduction = 'work time reduction ';
  SReportIllness = 'illness';
  SReportHoliday = 'holiday';
  SActiveEmpl =  'Active employees';
  SInactiveEmpl = 'Inactive employees';
// report
  SRepHrsPerEmplActiveEmpl = 'Show active employee';
  SRepHrsPerEmplInActiveEmpl = 'Show not active employee';
  SRepProdHrs = 'Productive hours';
  SRepNonProdHrs = 'Non-productive hours';
  SAll = 'All';
  SActive = 'Active';
  SInactive = 'Inactive';
  SName = 'Name ';
  SShortName = 'Short name';
// REPORT check list
  SShowProcessed =  'Show processed';
  SShowNotProcessed  = 'Show not processed';
//Report
  SErrorOpenRep = 'Error open data base, report can not be opened';
  SErrorCreateRep = 'Error report can not be created';
//DialogProcessAbsenceHrsFRM
   SLogEmpNo = 'Found Number of Records=';
   SLogChangedAbsenceTotal = 'Changed %d Absence Total-records';
   SLogDeletedRec = 'Deleted %d Absence Hour-records';
   SLogEndAbsence = 'End: Defining Absence Hours Per Employee';
   SLogStartSalaryHours = 'Start: Defining Salary Hours per Employee';
   SLogStartAvailability  = 'Start: Defining Employee Availability';
   SLogChangedAbsence = 'Changed %d Absence Hours-records';
   SlogAddedAbsence = 'Added %d Absence Hours-records';
   SLogEndAvailability = 'End: Defining Employee Availability';
   SLogStartNonScanning = 'Start: Defining Non-Scanning Employee Availability';
   SLogEndNonScanning  = 'End: Defining Non-Scanning Employee Availability';
   SLogFoundEmployeeNo = 'Found Number of Records = %d';
   SSProcessFinished = 'Process finished';
// Staffplanning
   SStaffPlnEmployee = 'Employee';
   SStaffPlnName = 'name';
// Contract group form
   SOvertimeDay = 'Day';
   SOvertimeWeek = 'Week';
   SValidMinutes = 'Minutes defined should be less than 59';
   SQuaranteedHourType = 'Please select an hour type';
//
//update cascade
   SConfirmUpdateCascadeEmpl =
     'Employee will be changed for all connected records, update employee ? ';
   SConfirmDeleteCascadeEmpl =
     'All connected records with this employee will be deleted also, delete employee ? ';
   SEmpExist = 'This employee already exist ';
//export payroll MRA:RV095.4. Give better message: 'date' added.
   SNotDefaultSelection = 'Not the default date-selection. Are you sure ?';
   SPeriodAlreadyExported = 'This period is already exported. Are you sure ?';
// transfer free time
   SPimsDifferentYears = 'Target year should be different than source year!';
   SPimsCheckBox = 'At least one of check boxes must be selected!';
   SPimsProcessFinish = 'Process has finished!';

   // MR:17-01-2003
   SPimsUseShift = 'Please select a shift!';
   SPimsUseWorkspot = 'Please select a workspot!';
//  staff planning -CAR 30-01-2003
   SPimsDayPlanning = 'Day planning: ';
   SPimsTeamSelection = 'Team selection: ';
   SPimsTeamSelectionAll = 'All teams';
   SPimsSTDPlanning = 'Standard planning: ';
   SPimsStaffTo =  ' to ';
   SPimsPlant =  '      Plant:  ';
   SPimsShift = '      Shift: ';
   SPimsTeam = 'Team ';
   // MR:28-01-2003
   SPimsNoEmployeeLine = 'No employee';

   SPimsNoPrevScanningFound = 'No previous scanning found';
   SpimsScanExists = 'There''s already a scanning-record on that day for that employee. ' + #13 +
     'You cannot change this item.';

 //car: 27.02.2003
   SPimsEXCheckIllMsgClose =
     'There already is one outstanding illness message, please close it before';
   SPimsEXCheckIllMsgStartDate =
     'Start date should be bigger than the end date of last created illness message ';

   // MR:03-03-2003
   SPimsContract = 'Contract';
   SPimsPlanned = 'Planned';
   SPimsAvailable = 'Avail.';

   // MR:07-03-2003
   SpimsYear = 'Year';
   SPimsWeek = 'Week';
   SPimsDate = 'Date';
   //CAR:21-03-2003 - REPORT staff planning per day
   SPimsNewPageWK = 'New page per workspot';
   SPimsNewPageEmpl = 'New page per employee';
   //CAR:21-03-2003 report checklist
   SPimsAvailability = 'Availability: ';
   //CAR:21-03-2003 update cascade -Plant
   SConfirmDeleteCascadePlant =
     'All connected records with this plant will be deleted, delete plant ? ';
   SConfirmUpdateCascadePlant =
     'Plant will be changed for all connected records, update plant ? ';
   SPlantExist = 'This plant already exists ';
   //CAR: 27-03-2003
   SRepTeamTotEmpl ='Total employee';
   //CAR 29-03-2003
   SRepHRSPerEmplCorrection = 'Manual corrections';
   SRepHrsPerEmplTotal = 'Total';
   //CAR 31-03-2003  - Report AbsenceCard
   SJanuary = 'January';
   SFebruary = 'February';
   SMarch = 'March';
   SApril = 'April';
   SMay = 'May';
   SJune = 'June';
   SJuly = 'July';
   SAugust = 'August';
   SSeptember = 'September';
   SOctober = 'October';
   SNovember = 'November';
   SDecember = 'December';
//CAR  2-04-2003 - report Production Detail
   SRepProdDetPerformance = 'Performance';
   SRepProdDetBonus = 'Bonus';
// CAR 2-4-2003 - Stand Staff availability
   SStdStaffAvailPlanned =
     'Employee is planned in standard planning, continuing will delete planning. ' +
     'Do you want to continue ?';
//CAR 7-4-2003
   SPimsJobs  = 'Jobcode';
//CAR 15-4-2003
  SPimsStartEndDateTimeEQ = 'Start date time is bigger or equal with end date time';
  SPimsStartEndTimeNull = 'Start time or end time are empty, do you want to continue ?';
// MR:24-04-2003
  SPimsNoData = 'No Data';
//CAR 5-5-2003
  SPimsAbsence = 'Absence';
  SPimsDifference = 'Difference';
// car 12-5-2003
  SExportDescEMPL = 'Employee description';
  SExportDescDept = 'Dept description';
  SExportDescPlant = 'Plant description';
  SExportDescShift = 'Shift description';
//
  SDummyWK = 'This is a dummy workspot it can not be saved';
// CAR 28-8-2003 - Stand Staff availability
   SStdStaffAvailFuturePlanned =
     'Employee is already planned in this shift. Continue will delete future planning. ';
// 9-4-2003
  SPimsInvalidWeekNr = 'Invalid week number!';
// 9-4-2003
  SPimsHoursPerDayProduction = 'Production ';
  SPimsHoursPerDaySalary = 'Salary ';
  // MR:12-09-2003
  SPimsCompletedToNotScans = 'You cannot change a completed scan back '#13'to a not-completed one.';
// user rights
  SAplErrorLogin = 'Invalid user name or password!';
  SAplNotConnect = 'Invalid connection to database!';
  SAplNotVisible = 'This user can not open the application';
// MR:3-11-2003 Moved from 'UStrings.pas'
  LSPimsEnterPassword = 'Enter Password';
// MR:11-11-2003
  SPimsEmployeesAll = 'All employees';
  SPimsEmployeesOnWrongWorkspot = 'On wrong workspot';
  SPimsEmployeesNotScannedIn = 'Not scanned in';
  SPimsEmployeesAbsentWithReason = 'Absent with reason';
  SPimsEmployeesAbsentWithoutReason = 'Absent without reason';
  SPimsEmployeesWithFirstAid = 'With first aid';
  SPimsEmployeesTooLate = 'Too late';
// 550119 - Workspot per Employee form
  SPimsLevel = 'Level';
// Car: 15-1-2004
  SPimsEmplContractInDatePrevious =
   'There already is a contract that includes this date';
// 550287
// Contract group form
  STruncateHrs = 'Truncate';
  SRoundHrs = 'Round';
//550124
  SPimsPieces = 'Pieces';
  SPimsWeight = 'Weights';
// MR:03-05-2004 Order 550313
  SPimsCleanUpProductionQuantities = 'Cleanup Production Quantities older than %d weeks?';
  SPimsCleanUpEmployeeHours = 'Cleanup Employee Hours older than %d weeks?';
  SPimsCleanUpTimerecordingScans = 'Cleanup Timerecording Scans older than %d weeks?';
// MR:17-05-2004 Order 550315
  SPimsLabelCode = 'Code';
  SPimsLabelInterfaceCode = 'Interface Code';
// MR:26-07-2004 Order 550327
  SPimsNotPlannedWorkspot = 'Not Planned';
// MR:09-08-2004 Order 550336
  SNumber = 'Number';
// MR:13-10-2004
  SPimsGeneralSQLError = 'BDE reported a general SQL Error';
// MR:26-10-2004
  SPimsEfficiencyAt = 'Efficiency at';
  SPimsEfficiencySince = 'Efficiency since';
// MR:28-10-2004
  SPimsConnectionLost = 'Error: Database connection lost. Please restart application';
// MR:10-12-2004
  SSHSWeekDatesCross = 'Error: Selected From/To-dates will cross each other!';
// MR:15-12-2004
  SPimsHours = 'hours';
  SPimsDays = 'days';
  SPimsBonus = 'bonus';
  SPimsEfficiency = 'efficiency';
// MR:11-01-2005
  SPimsTotalProduction = 'Total production';
// MR:11-01-2005
  SPimsProductionOutput = 'Production Output';
  SPimsChartHelp = 'select time: click on graph'; //, move: right-click between graph-items';
  SPimsInvalidScan = 'Error: You entered an invalid scan.';
// MR:11-01-2005 -> Pims Oracle!
  SPimsScanOverlap = 'Error: Scan has wrong time interval.';
  SPimsMaxSalBalMin = 'Max.Sal.Balance';
// MR:16-06-2005
  SPimsConfirmProcessEmployeeSelectedDate = 'Do you want to process the selected date ' + #13 +
    '%s' + #13 + 'to employee availability ?';
// MR:20-06-2005
  SPimsNoBankHoliday = 'Please select a bank holiday date';
// MR:27-09-2005
  SPimsUseJobcodesNotAllowed = 'There are not-completed scans for this workspot. ' + #13 +
    'Change is not allowed.';
// MR:14-11-2005
  SPimsUnknownBusinessUnit = 'Unknown Business Unit';
// MR:07-12-2006
  SOvertimeMonth = 'Month';
// MR:05-01-2007
  SErrorWrongTimeBlock = 'ERROR: Wrong timeblock for employee';
  SErrorDuringProcess = 'There were errors! Please see error-messages above.';
  SSProcessFinishedWithErrors = 'Process finished with errors!';
// MR:26-01-2007
  SPimsMonthGroupEfficiency = 'Monthly Group Efficiency';
  SPimsGroupEfficiency = 'Monthly Efficiency';
// MR:14-SEP-2009
  SPimsTemplateYearCopyNotAllowed = 'It is not allowed to copy to this year!';
  SPimsTemplateYearSelectNotAllowed = 'It is not allowed to select this year!';
// MRA:8-DEC-2009 RV048.1.
  SPimsEmpPlanPWD = 'It is not allowed to change the department, ' + #13 +
    ' because employees are planned on this workspot/department combination.';
// MRA:19-JAN-2010.
  SPimsNrOfRecsProcessed = 'Number of records processed: ';
// MRA:22-FEB-2010.
  SPimsNoShiftScheduleFound = 'There is no shift schedule defined for the used week.';
// MRA:18-MAY-2010. Order 550478.
  SPimsDeleteMachineWorkspotsNotAllowed = 'Delete not possible! There are workspots linked to this machine.';    
  SPimsDeleteMachineJobsNotAllowed = 'Delete not possible! There are jobs defined for this machine.';
  SPimsUpdateMachineCodeNotAllowed = 'Machine is linked to workspot(s). Changing the Machine Code is therefore not allowed.';
  SPimsUpdateMachineJobCodeNotAllowed = 'Machine-job is in use by a workspot-job. Changing the Machine Job Code is therefore not allowed.';      
// SO:07-JUN-2010.
  SPimsIncorrectHourType = 'Please enter a correct hour type !!';
  SPimsScanLongerThan24h = 'The scan cannot be longer than 24 hours !!';
// MRA:30-JUN-2010. Order 550478.
  SPimsMachineWorkspotNonMatchingJobs = 'WARNING: There are non-matching jobs for this workspot for the entered machine.' + #13 + 'Please be sure the jobs match for workspots that are linked to the entered machine!';
  SPimsPleaseDefineJobForMachine = 'WARNING: Please make changes for jobs codes on machine level.';
  SPimsDeleteNotAllowedForMachineJob = 'This is a machine job. Deletion is not allowed here, only on machine level.';
// MRA:24-AUG-2010. Order 550497. RV067.MRA.8.
  SPimsAbsenceReasons = 'absence reasons';
  SPimsHourTypes = 'hour types';
  SPimsAbsenceTypes = 'absence types';
// MRA:31-AUG-2010. RV067.MRA.21
  SPimsDescriptionNotEmpty = 'Please enter a description, it cannot be left empty!';
  SPimsCounterNameFilled = 'The counter name should be filled if the counter is active!';
// MRA:21-SEP-2010. RV067.MRA.34
  SPimsPaste = 'Paste';
  SPimsPasteExceptHours = 'Paste exceptional hours from "%s"';
  SPimsDefExceptHoursFound = 'Definitions found, these will be deleted. Proceed Yes / No?';
// MRA:4-OCT-2010. RV071.3. 550497
  SPimsCountersHaveBeenUpdated = 'Counters have been updated.';
  SPimsProcessToAvailIsReady = 'Process to availability is ready.';
  SPimsRecalcHoursConfirm = 'Recalculation of hours is included. Proceed ?';
// MRA:11-OCT-2010. RV071.10. 550497
  SPimMaxSatCreditReached = 'Maximum Saturday Credit has been reached!';
// MRA:1-NOV-2010.
  SPimsStartEndDateSmaller = 'Start date is smaller than end date' + #13 +
     'of an existing illness message for the same employee.';
// MRA:8-NOV-2010.
// MRA:1-DEC-2010. RV082.4. Text changed.
  SPimsIsNoScannerWarning =
    'WARNING: This employee is a non-scanner and of type: ' + #13 +
    'Autom. scans based on standard planning.' + #13 +
    'Updating scans can lead to incorrect hours! ' + #13 +
    'Changes are not allowed!';
// MRA:17-JAN-2011. RV085.1.
  SPimsEmpPlanPWDFound = 'WARNING: Planning exists for this workspot/department-combination' + #13 +
    'on %s and/or later. Are you sure you want to change the department?';
// MRA:14-FEB-2011. RV086.4.
  SPimsSelPlantsCountry = 'The selected plants should belong to the same country!';
  SPimsSelNotPosMultExports = 'Selection not possible, multiple export types are not allowed.';
// MRA:18-APR-2011. RV089.5
  SPimsAddAllWorkspots = 'Add ALL workspots?';
// MRA:21-APR-2011. RV090.1.
  SPimsFinishLastTransactionFirst = 'Please confirm/undo the last copy-action first!';  
  SPimsConfirmLastTransactionFirst = 'The last copy-action is not confirmed yet.' + #13 +
                                     'Please confirm/undo it first!' + #13 +
                                     'Yes=Confirm No=Undo';
  SPimsConfirmLastTransaction = 'Do you want to CONFIRM the last copy-action?';
  SPimsUndoLastTransaction = 'Do you want to UNDO the last copy-action?';
// MRA:26-APR-2011. RV089.1.
  SPimsYes = '&Yes';
  SPimsNo = '&No';
  SPimsAll = '&All';
  SPimsNoToAll = 'N&o To All';
// MRA:27-APR-2011. RV091.1
  SPimsPasteOvertimeDef = 'Paste overtime definitions from "%s"';
  SPimsOvertimeDefFound = 'Definitions found, these will be deleted. Proceed Yes / No?';
// MRA:24-MAY-2011. RV092.6
  SPimsEndTimeError = 'End time should be bigger than start time!';
// MRA:26-MAY-2011. RV093.1.
  SPimsFooterTotalContractGroup = 'Total Contr. gr.';
  SPimsFooterContractGroup = 'Contractgroup';
// MRA:1-JUN-2011. RV093.3.
  SPimsLoggingEnable = 'To enable/disable logging please restart Pims.';
// MRA:6-JUN-2011. RV093.3.
  SPimsCleanUpEmpHrsLog = 'Cleanup Employee Hours Logging older than %d weeks?';
  SPimsCleanUpTRSLog = 'Cleanup Timerecording Scans Logging older than %d weeks?';
// MRA:6-JUL-2011. RV094.5.
  SPimsFilenameAFAS = 'Salarymutations ABS PIMS';
// MRA:11-JUL-2011. RV094.7.
  SPimsPlantDeleteNotAllowed = 'It is not allowed to delete a plant.';
  SPimsPlantUpdateNotAllowed = 'It is not allowed to change a plant.';
  SPimsEmployeeDeleteNotAllowed = 'It is not allowed to delete an employee.';
  SPimsEmployeeUpdateNotAllowed = 'It is not allowed to change an employee.';
  SPimsWorkspotDeleteNotAllowed = 'It is not allowed to delete a workspot.';
  SPimsWorkspotUpdateNotAllowed = 'It is not allowed to change a workspot.';
// MRA:11-JUL-2011. RV04.8.
  SPimsEmployeeNotActive = 'Employee is non-active.';
// MRA:19-OCT-2011. RV099.1.
  SPimsPleaseFillBothAbsReasons = 'Please fill in both absence reasons.';
  SPimsLogStartTFTHolCorrection = 'Start TFT/Vacation correction.';
  SPimsLogEndTFTHolCorrection = 'End TFT/Vacation correction.';
  SPimsLogChangesMade = 'Number of changes made: %d';
// MRA:28-OCT-2011. RV100.2. 20011796.2. Changed.
  SPimsEarnedTFT = 'TFT Earned';
  SPimsEarnedTFTSign = 'TFT-E';
// MRA:9-JAN-2012. RV103.4.
  SPimsHolidayCardTitle = 'Holiday Card';
// MRA:13-JAN-2012. RV103.4.
  SJanuaryS = 'Jan.';
  SFebruaryS = 'Febr.';
  SMarchS = 'March';
  SAprilS = 'Apr.';
  SMayS = 'May';
  SJuneS = 'June';
  SJulyS = 'July';
  SAugustS = 'Aug.';
  SSeptemberS = 'Sept.';
  SOctoberS = 'Oct.';
  SNovemberS = 'Nov.';
  SDecemberS = 'Dec.';
// 20011796.2.
  SPimsRepHolCardOpeningBalance = 'Opening balance:';
  SPimsRepHolCardUsed = 'Used:';
  SPimsRepHolCardAvailable = 'Available:';
  SPimsRepHolCardTFTEarned = 'TFT-E Earned:';
  SPimsRepHolCardTUsed = 'T Used:';
  SPimsRepHolCardTotAvail = 'Total Available:';
  SPimsRepHolCardLegenda = 'T = Time for Time used, TFT-E = Time for Time earned, H = Holiday';
  SPimsRepHolCardLegendaTFTEarned = 'TFT-E = Time for Time earned';
  SPimsInvalidCheckDigits = 'Invalid checkdigits! Please enter a value from 10 to 99.';
  SPimsEnteredDateRemark = 'Please enter a Date-IN equal to selected date or next date.';
  SPimsShifts = 'Shifts'; // 20013551
  SPimsWorkspotHeader = 'Workspot'; // 20013551
  // 20013723
  SPimsExportPeriod = 'Export overtime';
  SPimsExportMonth = 'Export payroll';
  // 20013288.130
  SPimsExportEasyLaborChoice = 'Please make a choice about what to export.';
  // 20014002
  SPimsTotalWorkspot = 'Total Workspot';
  // 20014037.60
  SPimsPimsUser = 'Pims User';
  SPimsWorkstation = 'Workstation';
  // TD-22429
  SPimsStart = 'Start';
  // TD-22475
  SPimsPasteDatacolConnection = 'Paste datacol connections from "%s"';
  SPimsDatacolConnectionFound = 'DatacolConnections found, these will be deleted. Proceed Yes / No?';
  // TD-22503
  SChangeEmplAvail = 'Employee Availability was found, this will be deleted for non-available. Do you want to continue?';
  // SO-20014715
  SPimsReportYes = 'Yes';
  SPimsReportNo = 'No';
  SPimsReportTill = 'till';
  SPimsReportLate = 'late';
  SPimsReportEarly = 'early';
  // TD-23315 Changed this message:
  SPIMSTimeIntervalError = 'Starting time is less than previous end time,' + #13 +
   'or end time is higher then next start time.';
  // 20013196
  SPimsDeleteLinkJob = 'There is a link job defined. This will be removed. Proceed Yes/No ?';
  SPimsLinkJobExist = 'It is not allowed to define more than 1 link job per workspot.' + #13 +
    'This link job will not be saved.';
  // 20011800
  SPimsOK = '&Ok';
  SPimsFinalRun = '&Final Run';
  SPimsFinalRunWarning = 'FINAL RUN, ARE YOU SURE?' + #13#13 +
    'Any mutations over this period cannot be made afterwards.';
  SPimsFinalRunExportDateWarning = 'FINAL RUN' + #13#13 +
    'Selected export-period is already exported (partly or completely).' + #13#13 +
    'It is NOT allowed to make this export.';
  SPimsFinalRunExportDateWarningAdmin = 'FINAL RUN' + #13#13 +
    'Selected export-period is already exported (partly or completely).' + #13#13 +
    'Do you want to start the export?';
  SPimsTestRunExportDateWarning = 'TEST RUN' + #13#13 +
    'Selected export-period is already exported (partly or completely).' + #13#13 +
    'Do you want to start the export?';
  SPimsFinalRunExportGapWarning = 'FINAL RUN' + #13#13 +
    'There is a gap of %d days between last-export-date' + #13 +
    'and start-date of selection.' + #13#13 +
    'It is NOT allowed to make this export.';
  SPimsFinalRunExportGapWarningAdmin = 'FINAL RUN' + #13#13 +
    'There is a gap of %d days between last-export-date' + #13 +
    'and start-date of selection.' + #13#13 +
    'Do you want to start the export?';
  SPimsTestRunExportGapWarning = 'TEST RUN' + #13#13 +
    'There is a gap of %d days between last-export-date' + #13 +
    'and start-date of selection.' + #13#13 +
    'Do you want to start the export?';
  SPimsCloseScansSkipped = 'WARNING: Some scans could not be closed because of a compare' + #13 +
    'with last-export-date.';
  SPimsFinalRunAddAvaililityNotAllowed = 'Adding availability for this week is not allowed' + #13 +
    'because it has already been exported.';
  SPimsFinalRunDateChange = 'WARNING: The date has been changed to a date after the last-export-date.';
  SPimsFinalRunAvailabilityChange = 'WARNING: Some employee availability could not be changed because of the last-export-date.';
  SPimsFinalRunTestRun = 'TEST RUN';
  SPimsFinalRunDeleteNotAllowed = 'WARNING: Delete is not allowed, because it was already exported.';
  SPimsFinalRunOnlyPartDelete = 'WARNING: Only a part of the week will be deleted, because some part was already exported.';
  SPimsFinalRunShiftSchedule = 'WARNING: Not all could be copied because some part was already exported.';
  // SO-20014442
  SPimsYearMonth = 'Year/Month';
  SPimsStartEndYearMonth = 'End year/month should be bigger than start year/month!';
  SPimsOnlySANAEmps = 'Only SANA employees';
  SPimWrongYearSelection = 'Wrong year-selection: The From-To-Year must be the same!';
  // 20015178
  SPimsPleaseEnterFakeEmpNr = 'Please enter a unique Fake Employee Number,' + #13 +
    'that will be used in Personal Screen.';
  SPimsFakeEmpNrInUse = 'The entered Fake Employee Number is already in use' + #13 +
    'by another workspot. Please enter a different number.';
  // 20015223
  SPimsWorkspots = 'Workspots';
  SPimsPlantTitle = 'Plant';
  SPimsAllWorkspots = '<All>';
  SPimsMultipleWorkspots = '<Multiple>';
  // 20013476
  SPimsValueSampleTimeMins = 'Sample Time: Invalid value entered. Please enter a value from 1 to 30 minutes.';
  // 20015586
  SPimsIncludeDowntime = 'Include down-time quantities:';
  SPimsIncludeDowntimeYes = 'Yes';
  SPimsIncludeDowntimeNo = 'No';
  SPimsIncludeDowntimeOnly = 'Show only down-time';
  // TD-25948
  SPimsOpenCSV = 'Open CSV file?';
  SPimsCannotOpenCSV = 'Please install Microsoft Excel (or a similar program).';
  // PIM-53
  SPimsNoWorkingDays = 'Please select one or more working days.';
  SPimsAddHoliday = 'Do you want to add %d absence day(s) to employee availability?';
  // PIM-53
  SPimsAddHolidayEmplAvail = 'Please enter a valid absence reason code.';
  SPimsAddHolidayNoStandAvail = 'Action is aborted: There is no standard availability defined for the plant/shift-combination!';
  // PIM-52
  SPimsWorkScheduleDetailsExists = 'Work Schedule Details already exist! Overwrite?';
  SPimsWorkScheduleWrongRepeats = 'Wrong value for Weekly Repeats! Please enter a value between 1 and 26.';
  SPimsWorkScheduleInUse = 'Work Schedule is still in use (Employee Contract) and cannot be deleted!';
  SPimsInvalidShift = 'Please enter a valid shift.';
  SPimsReady = 'Ready';
  SPimsPlanningMessage1 = 'Not processed! For employee';
  SPimsPlanningMessage2 = 'and date';
  SPimsPlanningMessage3 = 'there was planning found.';
  SPimsNoStandAvailFound1 = 'Error: No standard availability found for employee';
  SPimsNoStandAvailFound2 = 'and shift';
  SPimsProcessWorkScheduleMessage1 = 'Process Workschedule for employee';
  SPimsProcessWorkScheduleMessage2 = 'and referencedate';
  // PIM-23
  SPimsTotals = 'Totals';
  SPimsOTDay = 'Day';
  SPimsOTWeek = 'Week';
  SPimsOTMonth = 'Month';
  SPimsMO = 'MO';
  SPimsTU = 'TU';
  SPimsWE = 'WE';
  SPimsTH = 'TH';
  SPimsFR = 'FR';
  SPimsSA = 'SA';
  SPimsSU = 'SU';
  SPimsNormalHrs = 'Normal hours per day:';
  SPimsOTPeriod = 'Period';
  SPimsOTPeriodStarts = 'Starts in week';
  SPimsOTWeeksInPeriod = 'Weeks in period:';
  SPimsOTWorkingDays = 'Working days:';
  SPimsCGRound = 'Round';
  SPimsCGTRunc = 'Trunc';
  SPimsCGMinute = 'minutes';
  SPimsCGTo = 'to';
  SPimsStandStaffAvailUsed = 'NOTE: For one or more days a fixed absence reason is used'#13'based on standard staff availability.'#13'This will not be overwritten.';
  SPimsShiftDate = 'Shift Date';
  // ABS-27052
  SPimsSelectPrinter = 'Select Printer';
  SPimsSelectDefaultPrinter = 'Select a printer as default printer';
  SPimsNoPrinterFound = 'No printers found!';
  SPimsPleaseSelectPrinter = 'Please select a printer.';
  // PIM-52
  SPimsNoCurrentEmpContFound = 'No current employee contract found for employee';
  // PIM-203
  SPimsHostPortExists = 'This Host+Port-combination already exists for another workspot.';
  // PIM-174
  SPimsCopied = 'Copied:';
  SPimsRows = 'row(s).';
  SPimsEmp = 'Employee';
  SPimsEmpContract = 'Employee Contract';
  SPimsWSPerEmp = 'Workspots per Employee';
  SPimsStandStaffAvail = 'Standard Staff Avail.';
  SPimsShiftSched = 'Shift Schedule';
  SPimsEmpAvail = 'Employee Avail.';
  SPimsStandEmpPlan = 'Employee Standard Planning';
  SPimsEmpPlan = 'Employee Planning';
  SPimsAbsenceCard = 'Absence card';
  SPimsRecordKeyDeleted = 'This record is deleted by another user.';
  SPimsWorkScheduleWrongStartDate = 'Reference week must be a date that starts at the first day of the week.'#13'This has been corrected.';
  SPimsRoamingWorkspotExists = 'There is already another workspot defined as roaming.';
  SPimsHourlyWage = 'Hourly Wage:';
  SPimsTotalExtraPayments = 'Total Extra Payments';
  SPimsTBPEmp = 'Timeblocks per Employee';

{$ELSE}
  {$IFDEF PIMSGERMAN}

  SPimsIncorrectPassword = 'Falsches Kennwort.'#13'Versuche auf neues oder '+
    'annullier.';
  SPimsNoEmptyPasswordAllowed = 'Ein leeres Kennwort ist nicht '+
    'erlaubt.'#13'Versuche auf neues oder annullier.';
  SPimsPasswordChanged = 'Neues Kennwort gespeichert.';
  SPimsPasswordNotChanged = 'Kennwort nicht gespeichert.';
  SPimsPasswordNotConfirmed = 'Kennwort und Bestaetigung sind '+
    'unterschiedlich.'#13'Versuche auf neues oder annulier.';
  SPimsSaveChanges = 'Speichern? ';
  SPimsRecordLocked = 'Dieses Record wird von einem anderen Benutzer benutzt.';
  SPimsOpenXLS = '�ffne Exceldatei ?';
  SPimsOpenHTML = '�ffne HTML Datei';
  SPimsNotUnique = 'Nicht Einzigartig';
  SPimsNotFound = 'Nichts gefunden';
  SPimsNotEnoughStock = 'There are not enough products in stock!'#13'Issue the '+
    'items anyway?';
  SPimsNoItems = 'Nichts selektiert';
  SPimsNoEmployee = 'There are no employee!';
  SPimsNoCustomer = 'There are no customers!';
  SPimsLoading = 'Ladend';
  SPimsKeyViolation = 'Diese Linie existiert schon.';
  SPimsForeignKeyPost = 'Eingabe nicht erlaubt';
  SPimsForeignKeyDel = 'Eingabe nicht erlaubt';
  SPimsFieldRequiredFmt = 'Bitte gib gefragte Eingabe';
  SPimsFieldRequired = 'F�llen Sie bitte alle verpflichteten Felder aus.';
  SPimsDragColumnHeader = 'Zieh Kolonne Kopf zum gruppieren der Kolonne';
  SPimsDetailsExist = 'Eingabe nicht erlaubt';
  SPimsDBUpdate = 'Update PIMS Datenbank';
  SPimsDBUpdateButtonCaption = '&Prozess PIMS Update';
  SPimsDBUpdateConflict = 'Probleme bei Updaten Datenbank. Bitte Helpdesk '+
    'kontakten';
  SPimsDBNeedsUpdating = 'Alte Datenbankversion, zuerst Datenbank updaten';
  SPimsCurrentCode = 'Current item code. ';
  SPimsContinue = ' Weitergehen?';
  SPimsConfirmCancelNote = 'Do you want to cancel this delivery note?';
  SPimsConfirmDeleteNote = 'Produktgruppe';
  SPimsConfirmDeleteLicense = 'Wollen Sie diese PIMS Lizenz l�schen?';
  SPimsConfirmDelete = 'M�chten Sie den aktuellen Record entfernen?';
  SPimsCannotPrintLabel = 'You cannot print a label for the currently selected '+
    'item.';
  SPimsCannotPlaceFlag = 'You cannot place a flag on the currently selected '+
    'item.';

  // FOR PIMS:
  SPimsCannotOpenXLS = 'Excel installieren';
  SPimsCannotOpenHTML = 'Please install an Internet Browser on your machine.';
  SSelectDummyWK = 'You can not select a dummy workspot!';
  SPimsPercentageNotCorrect = 'Fehler: Total % muss 100% sein';
  SPimsTimeMustBeYounger = 'Fehler: Startzeit muss fr�her als Endzeit sein';
  SPimsNoMaster = 'Kein Betrieb definiert';
  SPimsNoSubDetail = 'Keine Abteilungen definiert';
  SPimsNoDetail = 'Kein Arbeitsplatz definiert';
//  SPIMSTimeIntervalError = 'Startzeit ist fr�her als Vorherige Endzeit';
  SPIMSContractTypePermanent = 'Permanent';
  SPIMSContractTypeTemporary = 'Tempor�r';
  SPIMSContractTypePartTime = 'Extern';
  SPIMSContractOverlap = 'Startzeit des Vertrages ist fr�her als vorherige '+
    'Endzeit Vertrag';
  SPimsTBDeleteLastRecord = 'Nur das letzte Record kan gel�scht werden';
  SPimsTBInsertRecord = 'Maximum ist %d Zeitbl�cke';
  SPimsStartEndDate = 'Startdatum ist gr�sser als Enddatum';
  SPimsStartEndTime = 'Startzeit ist gr�sser als Endzeit';
  SPimsStartEndActiveDate = 'Startdatum Aktiv ist gr�sser als Enddatum Aktiv';
  SPimsStartEndInActiveDate = 'Startdatum Inaktiv ist gr�sser als Enddatum '+
    'Inaktiv';
  SPimsWeekStartEnd = 'Startwoche ist gr�sser als Endwoche';
  SPimsBUPerWorkspotPercentages = 'Total muss 100% sein';
  {SPimsBUPerWorkspotPercentagesBig = 'The total of percentages ' +
    'is be bigger than 100';
  SPimsBUPerWorkspotPercentagesLess = 'Total of percentages is ' +
   'less than 100, you can not leave form';}
  SPimsTBStartTime = 'Startzeit Zeiblock ist fr�her als Startzeit Schicht ';
  SPimsTBEndTime = 'Endzeit Zeitblock ist sp�ter als Endzeit Schicht';
  SPimsBUPerWorkspotInsert = 'Kein Arbeitsplatz vorhanden, eingabe unm�glich';
  SPimsDateinPreviousPeriod = 'Konflikt zwischen eingabe Start / Enddatum der '+
    'Perioden';
  // Time Recording
  SPimsExistJobCodes = 'Wechseln nicht m�glich, Jobcodes sind definiert';
  SPimsExistBUWorkspot = 'Wechseln unm�glich, Gesch�ftsbereichen sind definiert';
  SPimsIDCardNotFound = 'Karte nicht gefunden';
  SPimsIDCardExpired = 'Karte ist ung�ltig';
  SPimsEmployeeNotFound = 'Mitarbeiter nicht gefunden';
  SPimsInactiveEmployee = 'Mitarbeiter inaktiv';
  SPimsScanningsInMinute = 'Mehrere Scannungen innerhalb 1 Minute ist nicht '+
    'erlaubt';
  SPimsStartDay = 'Start vom Tag';
  SpimsEndDay = 'Ende vom Tag';
  SPimsNoWorkspot = 'Keine Arbeitspl�tze definiert f�r diese PC';
  SPimsNoJob = 'Keine Jobcodes definiert f�r diese Arbeitsplatz';
  SPimsDeleteJob = 'Jobcodes sind verkn�pft, diese werden gel�scht!';
  // Selector Form - Grid Column name
  SPimsDeleteBusinessUnit = 'Gesch�ftsbereiche sind verkn�pft, diese werden '+
    'gel�scht!';
  SPimsDeleteJobBU = 'Verkn�pfte Jobcodes + Gesch�ftsbereiche werden gel�scht!';
  SPimsColumnPlant = 'Betrieb';
  SPimsColumnWorkspot = 'Arbeitsplatz';
  // Selector Form - Captions
  SPImsColumnCode = 'Kode';
  SPimsColumnDescription = 'Beschreibung';
  SPimsWorkspot = 'Selektiere Arbeitsplatz';
  SPimsJob = 'Selektiere Job';
  SPimsJobCodeWorkSpot = 'Abschluss nicht m�glich, erst Jobcode definieren';
  SPimsSaveBefore = 'erst speichern';
  SPimsNoPlant = 'Keine Betriebe definiert';
//  SPimsNoEmployee = 'There are no employees';
// Time Recording Scanning
  SPimsScanDone = 'Scannung verarbeitet';
  SPimsScanError = 'Scannung NICHT verarbeitet: Datenbank FEHLER';
// team per department - extra check
  SPimsNoShift = 'Schicht nicht definiert';
  SPimsNoCompletedScans = '�nderungen kompletter Scannungen unm�glich';

// Hours per Employee
  SPimsNoMorePlantsinTeam = 'Abteilungen innerhalb des Teams m�ssen gleichem '+
    'Betrieb angeh�ren';
  SPimsTeamPerEmployee = 'Abteilung vom Mitarbeiter geh�rt nicht zum Team';
  SPimsInvalidTime = 'Eingabe unbekannt';
  SPimsNegativeTime = 'Doppellklick f�r Eingabe Minuszeit (rot)';
  SPimsExistingRecord = 'Recod mit gleichen Parametern exeztiert schon';
  SPimsProductionHour = 'Unm�gliche Update von ProductionHourPerEmployee Tabelle ';
// bank holiday process
  SPimsSalaryHour = 'Unm�gliche Update von SalaryHourPerEmployee Tabelle';

// for reports
  SPimsAbsenceHour = 'Unm�glich Update von AbsenceHourPerEmployee Tabelle';
  SPimsConfirmProcessEmployee = 'Wollen Sie staatlichen Feiertage umwandeln in '+
    'der Mitarbeiter Verf�gbarkeit?';
  SPimsReportWTR_HOL_ILL_Checks = 'Minimum ein Abwesenheitsgrund selektieren!';
  SPimsMaxNumberOfWeeks = 'Maximum Anzahl KW ist 13';
  SPimsEndTime = 'Endzeit';
// update database
  SPimsStartTime = '< Startzeit';
  SPimsOn = 'on';
  SPimsInvalidVersion = 'Ung�ltige PIMS Version!';
// Standard staff availability
  SUpdateDatabaseFrom = 'Datenbank update';
  SUpdateDatabaseTo = 'nach Version';
  SCheckAvailableTB = 'Erlaubte Werte sind * und -';
  SCheckAvailableTBNotEmpty = 'Bitte f�lle alle Pflichtfelder!';
  SCopySTA1 = 'Verf�gbarkeit exzeztiert f�r diesen Tag schon';
  SCopySTA2 = ',�berschreibe ?';
  SCopySameValues = 'Gleiche Record ist selektiert !';
// shift schedule
  SCopySTANotValid = 'Keine Zeitbl�cke definiert f�r selektierte record';
  SEmptyValues = 'Bitte f�lle alle Pflichtfelder !';
  SShiftSchedule = 'Erlaubte Werte sind 1..9 und -';
  SSHSShift = 'Erlaubte Werte sind nur Schicht definiert';
  SInvalidShiftSchedule = 'Schichtnummer ist nicht definiert oder Start Endzeit '+
    'pro Tag ist Null';
  SEmployeeAvail_1 = ' hat schon Verf�gbarkeit f�r diese Schicht am Jahr, Woche, Tag ';
//  SEmployeeAvail_2 = ', Tag';
  SEmployeeAvail_3 = ', unm�glich Schichtnummer zu wechseln.';
  SUndoChanges = 'Letzte �nderungen werden nicht gespeichert';
  SStartWeek = 'Start KW muss gr��er sein als vorherige End KW';
  SCopySelectionShiftSchedule_1 = 'Schichtschema f�r Mitarbeiter ';
  SCopySelectionShiftSchedule_2 = ' existiert schon. �berschreiben?';
// employee availability
  SCopyFinished = 'Kopie beendet!';
  SEmptyRecord = 'Keine Mitarbeiter selektiert!';
  SEmplAvail = 'Erlaubte Werte sind *,- oder Abwesenheitsgrund';
  SValidTimeBlock = 'Alle Zeitbl�cke ausf�llen!';
  SEmployeePlanned = 'Mitarbeiter ist geplant, bei weitere Eingabe wird Planung '+
    'gel�scht. Weitergehen?';
  SCopyFrom = 'Bitte Record selektieren!';
  SCopyTo = 'Unm�gliche Eingabe.';
  SEMAOverwrite = 'Mitarbeiter Verf�gbar f�r';
// staff planning
  SEMAShift = 'Schicht';
  SEMAExists = 'Exzeztiert schon. �berschreiben?';
  SSelectedWorkspots = 'Mehr als 401 Arbeitspl�tze definiert, Maximum erreicht!';
  SEmptyWK = 'Kein Arbeitsplatz definiert';
  SOverplanning = '�ber 200% geplant';
  SMakeAvailable = 'Mach Verf�gbar';
  SNotAcceptPlanned = 'Planung dieser Mitarbeiter ist nicht akzeptiert!';
  SSaveChanges = 'Speichern? ';
  SReadStdPlnIntoDaySelection = 'Planung schon ausgef�llt f�r mehrere '+
    'Betriebe/Datum';
  SContinueYN = 'Do you want to continue ?';
  SDateFromTo = 'Enddatum muss gr��er sein als Startdatum ';
  SDateIntervalValidate = 'Datum soll nicht gleich sein';
  SNoEmployee = 'Keine Mitarbeiter selektiert';
  SOCILevel = 'Level Besetzung';
  SPlannedLevel = 'Geplant mit Level';
// report staff planning
  SPlanned200 = 'Geplant �ber 200% mit Level ';
  SSameRangeDate = 'Intervallperiode muss gleich sein !';
// Workspot per employee
  SStartEndYear = 'Endjahr muss gr��er sein als Startjahr';
  SWeek = 'Unm�glich mehr als eine Woche zu selektieren';
// Staff Availability
  SLEVELWK = 'Nur A,B,C Level erlaubt!';
  SInvalidWK = 'Nur A..Z,a..z,1..9, und _ sind erlaubt!';
  SChangePlaned = 'Mitarbeiter ist schon geplant, beim Weitergehen wird diese '+
    'Planung gel�scht. Weitergehen?';
//  E&R
  SInvalidShiftNo = 'Falsche Schichtnummer f�r Tag:%d';
  SInvalidAvailability = 'Falsche Verf�gbarkeit f�r Tag:%d,TB:%d';
  SMoreThan = 'Mehr als';
  SMonthReport = 'Month';
  SCalculationwithOvertime = 'Quality incentive calculation with overtime report';
  SInvalidMistakeValue = 'Number of mistakes should be bigger than zero !';
 //
// productivity reports

  SNotDeleteRecord = 'Unm�glich diesen Record zu l�schen';
//
// jobcode
  SPimsDBError = 'Report wird schon genutzt.';
  SDaySelect = 'Starttag muss kleiner sein als Enddatum';
  SInterfaceCode = 'Schnittstelle Code exzeztiert schon!';
//
  SWKInterfaceCode = 'Schnittstelle Code exzestiert schon';
  SWKRescanInterfaceCode = 'Mehrere Schnittstellen Codes f�r diesen '+
    'Arbeitsplatz definiert!';
// export payroll report
  SExceptionDataSetReport = 'Data set is empty cannot create report, please set '+
    'it first and rebuild pims';
  SExceptionDataSetForms = 'Data set is empty cannot refresh grids, please set '+
    'it first and rebuild pims';
// report quality incentive
  SPeriodReport = 'Period';

  // Added at 6-9-2002
  SPimsHomePage  = 'Pims Homepage will be implemented soon!';
  SPimsStartEndDateTime = 'Start Datum/Zeit ist sp�ter dann Ende Zeit';
  SShowBUWK         = 'Zeig Gesch�ftsber. pro Arb.Pl.';
  SShowJobCode = 'Zeig Job codes';
  SPimsAutomaticRecord = 'Automatisch zugef�gte Records k�nnen nicht ge�ndert werden ';
  SPimsNoTimeValue = 'Bitte gib Zeit ein f�r mindestens ein Tag';
//  SReadStdPlnIntoDaySelection = 'Planung schon gelesen f�r einige Betriebe/ Datums';
  SReadStdPlnIntoDaySelectionFinish = '�bernehmen Standard in Tagesplan fertig';
  SADDWKDateCollection = 'Records zugef�gt';
  SDate = 'Datum ';
  STotalWeek = 'Total KW ';

  SCaptionProdRepHrs = 'Produktionstunden pro Mitarbeiter';
  SCaptionSalRepHrs =  'Lohnstunden pro Mitarbeiter';

  SShowActiveEmpl = 'Zeig Aktive Mitarbeiter';
  SShowNotActiveEmpl = 'Zeig inaktive Mitarbeiter';

  SRepHrsCUMWK = 'Arbeitsplatz';
  SRepHrsTotalCUM = 'Total Arbeitsplatz';

  SRepHrsCUMWKBU = 'Gesch�ftsbereich';
  SRepHrsTotalCUMBU = 'Total Gesch�ftsbereich';

  SRepHrsCUMWKPlant = 'Betrieb';
  SRepHrsTotalCUMPlant = 'Total Betrieb';

  SRepHrsCUMWKDept = 'Abteilung';
  SRepHrsTotalCUMDept = 'Total Abteilung';
  SRepJobCode = 'Jobcode';
  SRepTotJobCode = 'Total Jobcode';

  SHeaderAbsRsn = 'Abwesenheitsgrund';
  SHeaderWeekAbsRsn = 'Woche';
  SHdTotalWeek = 'Total KW';
  SRepExpPayroll = 'Von';

  SReport = 'Report ';
  SReportWorkTimeReduction = 'Gleitzeit ';
  SReportIllness = 'Krank';
  SReportHoliday = 'Urlaub';
  SActiveEmpl =  'Aktive Mitarbeiter';
  SInactiveEmpl = 'Inaktive Mitarbeiter';

  SRepHrsPerEmplActiveEmpl = 'Zeig Aktive Mitarbeiter';
  SRepHrsPerEmplInActiveEmpl = 'Zeig Inaktive Mitarbeiter';
  SRepProdHrs = 'Produktion Stunden';
  SRepNonProdHrs = 'Nicht-Produktion Stunden';
  SAll = 'Alle';
  SActive = 'Aktive';
  SInactive = 'Inaktive';
  SName = 'Name ';
  SShortName = 'Kurzname';

  SShowProcessed =  'Zeig verarbeitet';
  SShowNotProcessed  = 'Show nicht verarbeitet';

  SErrorOpenRep = 'Fehler �ffnen Datenbank, Report kann nicht ge�ffnet werden';
  SErrorCreateRep = 'Fehler Report kann nicht gemacht werden';

  SLogEmpNo = 'Anzahl gefunden=';
  SLogChangedAbsenceTotal = 'Ge�ndert %d Records';
  SLogDeletedRec = 'Entfernte %d Records';
  SLogEndAbsence = 'Ende: Definieren Abwesenheitsstunden Stunden pro Mitarbeiter';
  SLogStartSalaryHours = 'Start: Definieren Lohnstunden pro Mitarbeiter';
  SLogStartAvailability  = 'Start: Definieren Mitarbeiter Verf�gbarkeit';
  SLogChangedAbsence = 'Ge�ndert %d Records';
  SlogAddedAbsence = 'Zugef�gt %d Records';
  SLogEndAvailability = 'Ende: Definieren Mitarbeiter Verf�gbarkeit';
  SLogStartNonScanning = 'Start: Definieren Nicht-Scanner Mitarbeiter Verf�gbarkeit';
  SLogEndNonScanning  = 'Ende: Definieren Nicht-Scanner Mitarbeiter Verf�gbarkeit';

  SReadPastIntoDayPlanFinish = 'Ubernehmen vergangene Planung fertig';

  SStaffPlnEmployee = 'Mitarbeiter';
  SStaffPlnName = 'Name';
  SPimsTo = 'Zu';

  SPimsInterfaceCode  =
    'Sie k�nnen die Maske nicht slie�en, definiere mindestens eine Schnitstelle Kode anders als die Schnittstelle Kode f�r die Job';
  SPimsOtherInterfaceCode  = 'Bitte definiere eine Schnitstelle Kode anders als die Schnittstelle Kode f�r die Job Code';
  SDeleteOtherInterfaceCode = 'Alle andere Schnittstelle Kodes wirden entfernt!';
  SPimsNoOtherInterfaceCodes = 'Kein andere Schnittstelle Kodes sind definiert, Sie k�nnen die Maske nicht �ffnen';

  // MR: 18-09-2002 DialogProcessAbsenceHrsFRM
  SSProcessFinished = 'Prozess fertig';
  SLogFoundEmployeeNo = 'Anzahl Records gefunden = %d';

  // MR:29-01-2003
  SPimsNoEmployeeLine = 'No employee';

  SPimsSameSourceAndDest = 'The source and destination computer name could ' +
   	'not be the same';
  SOvertimeDay = 'Tag';
  SOvertimeWeek = 'Woche';
  SValidMinutes = 'Minutes defined should be less than 59';
  SQuaranteedHourType = 'Please select an hour type';
  SConfirmUpdateCascadeEmpl =
    'Employee will be changed for all connected records, update employee ? ';
  SConfirmDeleteCascadeEmpl =
    'All connected records with this employee will be deleted also, delete employee ? ';
  SEmpExist = 'This employee already exist ';
  SPimsUseShift = 'Please select a shift!';
  SPimsUseWorkspot = 'Please select a workspot!';
  SPimsUseJobCode = 'Please select a job code, workspot it is defined to use jobcodes';
  SNoTeam = 'Empty team: ';
  SPimsScanTooBig = 'Scan period longer than 12 hours is not allowed';
  SPimsDifferentYears = 'Target year should be different than source year!';
  SPimsCheckBox = 'At least one of check boxes should be selected!';
  SPimsProcessFinish = 'Process has finished!';
  SPimsEmptyDescription = 'Please fill required field description!';
  SPimsEmptyAbsReason = 'Please fill required field absence reason!';
  SPimsInvalidAbsReason = 'Not a valid absence reason!';
  SEmployeeAvail_0  = 'Mitarbeiter ';
  SSHSWeekCopy = 'Start date of copy should be bigger than end date';
  SEmplSTARecords = ' There are no standard availabilities defined for this employee '; 
//export payroll MRA:RV095.4. Give better message: 'date' added.
  SNotDefaultSelection = 'Kein standard Datum-Wahl. Sind Sie sicher ?';
  SPeriodAlreadyExported = 'Diese Periode ist schon exportiert. Sind Sie sicher ?';

  SpimsScanExists = 'There''s already a scanning-record on that day for that employee. ' + #13 +
    'You cannot change this item.';

  SCopySTANotValidFrom = 'There are no valid timeblocks for this record!';
  SPimsDayPlanning = 'Tag Planung: ';
  SPimsSTDPlanning = 'Standard Planung: ';
  SPimsTeamSelection = 'Team Selektion: ';
  SPimsTeamSelectionAll = 'Alle Teams';
  SPimsStaffTo =  ' bis ';
  SPimsPlant =  '      Betrieb:  ';
  SPimsShift = '      Schicht: ';
  SPimsTeam = 'Team ';
  SCopySTANotValidTo = 'There are no valid timeblocks for selected record of grid!';

   SPimsEXCheckIllMsgClose =
     'There already is one outstanding illness message, please close it before';
   SPimsEXCheckIllMsgStartDate =
     'Start date should be bigger than the end date of last created illness message ';

   // MR:03-03-2003
   SPimsContract = 'Vertrag';
   SPimsPlanned = 'Geplannt';
   SPimsAvailable = 'Verf�gb.';

   // MR:07-03-2003
   SpimsYear = 'Jahr';
   SPimsWeek = 'Woche';
   SPimsDate = 'Datum';

   SPimsNonScanningStation = 'This is no timerecording-station';
   SPimsNoPrevScanningFound = 'No previous scan found';
   SPimsNoMatch = 'No match found for employee + idcard';

   //CAR - REPORT staff planning per day
   SPimsNewPageWK = 'New page per workspot';
   SPimsNewPageEmpl = 'New page per employee';
   // report checklist
   SPimsAvailability = 'Availability: ';
   //CAR:21-03-2003 update cascade -Plant
   SConfirmDeleteCascadePlant =
     'All connected records with this plant will be deleted, delete plant ? ';
   SConfirmUpdateCascadePlant =
     'Plant will be changed for all connected records, update plant ? ';
   SPlantExist = 'This plant already exist ';
   //CAR: 27-03-2003
   SRepTeamTotEmpl ='Total Mitarb.';
   //CAR 29-03-2003
   SRepHRSPerEmplCorrection = 'Manual corrections';
   SRepHrsPerEmplTotal = 'Total';
   //CAR 31-03-2003
   SJanuary = 'January';
   SFebruary = 'February';
   SMarch = 'March';
   SApril = 'April';
   SMay = 'May';
   SJune = 'June';
   SJuly = 'July';
   SAugust = 'August';
   SSeptember = 'September';
   SOctober = 'October';
   SNovember = 'November';
   SDecember = 'December';
//CAR  2-04-2003 - report Production Detail
   SRepProdDetPerformance = 'Performance';
   SRepProdDetBonus = 'Bonus';
// CAR 2-4-2003 - Stand Staff availability
   SStdStaffAvailPlanned =
     'Employee is planned in standard planning, continuing will delete planning. ' +
     'Do you want to continue ?';
//CAR 7-4-2003
   SPimsJobs  = 'Jobcode';     
//CAR 15-4-2003
   SPimsStartEndDateTimeEQ = 'Start date time is bigger or equal with end date time';
   SPimsStartEndTimeNull = 'Start time or end time are empty, do you want to continue ?';
// MR:24-04-2003
  SPimsNoData = 'No Data';
//CAR 5-5-2003
  SPimsAbsence = 'Abwesend';
  SPimsDifference = 'Unterschied';
// car 12-5-2003
  SExportDescEMPL = 'Employee description';
  SExportDescDept = 'Dept description';
  SExportDescPlant = 'Plant description';
  SExportDescShift = 'Shift description';
//
  SDummyWK = 'This is a dummy workspot it can not be saved';
// CAR 28-8-2003 - Stand Staff availability
   SStdStaffAvailFuturePlanned =
     'Employee is already planned in this shift. Continue will delete future planning. ';
// 9-4-2003
  SPimsInvalidWeekNr = 'Invalid Woche Nummer!';
// 9-4-2003
  SPimsHoursPerDayProduction = ' Production ';
  SPimsHoursPerDaySalary = ' salary ';

  // MR:12-09-2003
  SPimsCompletedToNotScans = 'You cannot change a completed scan back '#13'to a not-completed one.';
// user rights
  SAplErrorLogin = 'Invalid Benutzername oder Kennwort!';
  SAplNotConnect = 'Invalid connection to database!';
  SAplNotVisible = 'This user cannot open the application';
// MR:3-11-2003 Moved from 'UStrings.pas'
  LSPimsEnterPassword = 'Eingabe Kennwort';
// MR:11-11-2003
  SPimsEmployeesAll = 'Alle Mitarbeiter';
  SPimsEmployeesOnWrongWorkspot = 'Am falschen Arbeitsplatz';
  SPimsEmployeesNotScannedIn = 'Nicht eingescannt';
  SPimsEmployeesAbsentWithReason = 'Abwesend mit Reden';
  SPimsEmployeesAbsentWithoutReason = 'Abwesend ohne Reden';
  SPimsEmployeesWithFirstAid = 'Mit Erste Hilfe';
  SPimsEmployeesTooLate = 'Zu sp�t';
// 550119 - Workspot per Employee form
  SPimsLevel = 'Level';
// Car: 15-1-2004
  SPimsEmplContractInDatePrevious =
   'Ein Vertrag existiert schon mit dieser Datum';
// 550287
// Contract group form
   STruncateHrs = 'Kappen';
   SRoundHrs = 'Abrunden';
//550124
  SPimsPieces = 'St�ck';
  SPimsWeight = 'Gewicht';
//06-04-2004
  SPlannedAllLevels = 'Geplant ';
  SOCIAllLevel = 'Besetzung  ';
// MR:03-05-2004 Order 550313
  SPimsCleanUpProductionQuantities = 'Cleanup Produkt. Anzahlen alter dann %d Wochen?';
  SPimsCleanUpEmployeeHours = 'Cleanup Mitarbeiter Stunden alter dann %d Wochen?';
  SPimsCleanUpTimerecordingScans = 'Cleanup Zeiterfassung Scans alter dann %d Wochen?';
// MR:17-05-2004 Order 550315
  SPimsLabelCode = 'Code';
  SPimsLabelInterfaceCode = 'Interface Code';
// MR:26-07-2004 Order 550327
  SPimsNotPlannedWorkspot = 'Nicht geplant';
// MR:09-08-2004 Order 550336
  SNumber = 'Nummer';
// MR:13-10-2004
  SPimsGeneralSQLError = 'BDE reported a general SQL Error';
// MR:26-10-2004
  SPimsEfficiencyAt = 'Effizienz am';
  SPimsEfficiencySince = 'Effizienz zeit';
// MR:28-10-2004
  SPimsConnectionLost = 'Error: Database connection lost. Please try again.';
// MR:10-12-2004
  SSHSWeekDatesCross = 'Fehler: Selektierte Von/Zu-Datums kreuzen sich!';  
// MR:15-12-2004
  SPimsHours = 'Stunden';
  SPimsDays = 'Tagen';
  SPimsBonus = 'Bonus';
  SPimsEfficiency = 'Effizienz';
// MR:11-01-2005
  SPimsTotalProduction = 'Total Produktion';
// MR:11-01-2005
  SPimsProductionOutput = 'Produktion Leist.';
  SPimsChartHelp = 'Selektier Zeit: Klick auf Graph'; //, move: right-click between graph-items'; 
  SPimsMaxSalBalMin = 'Max.Lohn Bilanz';
// MR:11-01-2005 -> Pims-Oracle!
  SPimsInvalidScan = 'Fehler: Dieser Scann ist nicht g�ltig.';
  SPimsScanOverlap = 'Fehler: Scann hat ein falscher Zeit-bereich.';
// MR:16-06-2005
  SPimsConfirmProcessEmployeeSelectedDate = 'Wollen Sie staatlichen Feiertage f�r Datum' + #13 +
    '%s' + #13 + 'umwandeln in der Mitarbeiter Verf�gbarkeit?';
// MR:20-06-2005
  SPimsNoBankHoliday = 'Bitte selektier ein Feiertag Datum';
// MR:27-09-2005
  SPimsUseJobcodesNotAllowed = 'Es gibt nicht-komplette Scannungen f�r diese Arbeitsplatz. ' + #13 +
    '�nderung nicht erlaubt.';
// MR:14-11-2005
  SPimsUnknownBusinessUnit = 'Gesch�ftsber. unbekannt';
// MR:07-12-2006
  SOvertimeMonth = 'Monat';
// MR:05-01-2007
  SErrorWrongTimeBlock = 'FEHLER: Falsches Zeitblock f�r Mitarbeiter';
  SErrorDuringProcess = 'Es gab Fehler! Betrachten Sie die Fehlermeldungen oben.';
  SSProcessFinishedWithErrors = 'Prozess fertig mit Fehler!';
// MR:26-01-2007
  SPimsMonthGroupEfficiency = 'Monat. Gruppe Eff.';
  SPimsGroupEfficiency = 'Monat. Eff.';
// MR:14-SEP-2009
  SPimsTemplateYearCopyNotAllowed = 'Kopieren nach dieses Jahr ist nicht erlaubt!';
  SPimsTemplateYearSelectNotAllowed = 'Es ist nicht erlaubt dieses Jahr zu selektieren!';
// MRA:8-DEC-2009 RV048.1.
  SPimsEmpPlanPWD = 'Es ist nicht erlaubt diese Abteilung zu �ndern, ' + #13 +
    'weil est gibt Mitarbeiter mit Planung f�r diese Arbeitsplatz/Abteilung.';
// MRA:19-JAN-2010.
  SPimsNrOfRecsProcessed = 'Number of records processed: ';
// MRA:22-FEB-2010.
  SPimsNoShiftScheduleFound = 'Schichtscheme nicht definiert f�r diese Woche!';  
// MRA:18-MAY-2010. Order 550478.
  SPimsDeleteMachineWorkspotsNotAllowed = 'Delete not possible! There are workspots linked to this machine.';    
  SPimsDeleteMachineJobsNotAllowed = 'Delete not possible! There are jobs defined for this machine.';
  SPimsUpdateMachineCodeNotAllowed = 'Machine is linked to workspot(s). Changing the Machine Code is therefore not allowed.';
  SPimsUpdateMachineJobCodeNotAllowed = 'Machine-job is in use by a workspot-job. Changing the Machine Job Code is therefore not allowed.';      
// SO:07-JUN-2010.
  SPimsIncorrectHourType = 'Bitte geben Sie eine richtige Stunden Type ein!';
  SPimsScanLongerThan24h = 'Der Scan kann nicht l�nger sein dann 24 Stunden!';
// MRA:30-JUN-2010. Order 550478.
  SPimsMachineWorkspotNonMatchingJobs = 'WARNING: There are non-matching jobs for this workspot for the entered machine.' + #13 + 'Please be sure the jobs match for workspots that are linked to the entered machine!';
  SPimsPleaseDefineJobForMachine = 'WARNING: Please make changes for jobs codes on machine level.';
  SPimsDeleteNotAllowedForMachineJob = 'This is a machine job. Deletion is not allowed here, only on machine level.';
// MRA:24-AUG-2010. Order 550497. RV067.MRA.8.
  SPimsAbsenceReasons = 'Abw. Grund';
  SPimsHourTypes = 'Stunden Types';
  SPimsAbsenceTypes = 'Abw. Types';
// MRA:31-AUG-2010. RV067.MRA.21
  SPimsDescriptionNotEmpty = 'Bitte geben Sie eine Beschreibung ein, es darf nicht leer sein!';
  SPimsCounterNameFilled = 'Der Z�hler Name sollen Sie ausf�llen wenn der Z�hler Aktiv ist!';
// MRA:21-SEP-2010. RV067.MRA.34
  SPimsPaste = 'Paste';
  SPimsPasteExceptHours = 'Paste exceptional hours from "%s"';
  SPimsDefExceptHoursFound = 'Definitionen gefunden, diese werden entfernt. Fortfahren Ja / Nein?';
// MRA:4-OCT-2010. RV071.3. 550497
  SPimsCountersHaveBeenUpdated = 'Z�hler sind updated.';
  SPimsProcessToAvailIsReady = 'Proze� zu Verf�gbarkeit ist fertig.';
  SPimsRecalcHoursConfirm = 'Neuberechnung von Stunden ist einbegriffen. Fortfahren ?';
// MRA:11-OCT-2010. RV071.10. 550497
  SPimMaxSatCreditReached = 'Maximum Samstag Kredit ist erreicht!';
// MRA:1-NOV-2010.
  SPimsStartEndDateSmaller = 'Start Datum ist kleiner dann End Datum' + #13 +
     'von ein bestehende Krankheitsmeldung f�r derselbe Mitarbeiter.';
// MRA:8-NOV-2010.
// MRA:1-DEC-2010. RV082.4. Text changed.
  SPimsIsNoScannerWarning =
    'WARNING: This employee is a non-scanner and of type: ' + #13 +
    'Autom. scans based on standard planning.' + #13 +
    'Updating scans can lead to incorrect hours! ' + #13 +
    'Changes are not allowed!';
// MRA:17-JAN-2011. RV085.1.
  SPimsEmpPlanPWDFound = 'WARNUNG: Planung besteht f�r diese Arbeitsplatz/Afteilung-Kombination' + #13 +
    'am %s und/oder sp�ter. Wollen Sie dieser Abteilung �ndern?';
// MRA:14-FEB-2011. RV086.4.
  SPimsSelPlantsCountry = 'Der selektierte Betriebe sollen geh�ren zum gleiche Land!';
  SPimsSelNotPosMultExports = 'Selektion nich m�glich, mehrfache Export Types sind nicht erlaubt.';
// MRA:18-APR-2011. RV089.5
  SPimsAddAllWorkspots = 'Add ALL workspots?';
// MRA:21-APR-2011. RV090.1.
  SPimsFinishLastTransactionFirst = 'Please confirm/undo the last copy-action first!';  
  SPimsConfirmLastTransactionFirst = 'The last copy-action is not confirmed yet.' + #13 +
                                     'Please confirm/undo it first!' + #13 +
                                     'Yes=Confirm No=Undo';
  SPimsConfirmLastTransaction = 'Do you want to CONFIRM the last copy-action?';
  SPimsUndoLastTransaction = 'Do you want to UNDO the last copy-action?';
// MRA:26-APR-2011. RV089.1.
  SPimsYes = '&Yes';
  SPimsNo = '&No';
  SPimsAll = '&All';
  SPimsNoToAll = 'N&o To All';
// MRA:27-APR-2011. RV091.1
  SPimsPasteOvertimeDef = 'Paste overtime definitions from "%s"';
  SPimsOvertimeDefFound = 'Definitions found, these will be deleted. Proceed Yes / No?';
// MRA:24-MAY-2011. RV092.6
  SPimsEndTimeError = 'End time should be bigger than start time!';  
// MRA:26-MAY-2011. RV093.1.
  SPimsFooterTotalContractGroup = 'Total Vertr.Grp.';
  SPimsFooterContractGroup = 'Vertraggruppe';
// MRA:1-JUN-2011. RV093.3.
  SPimsLoggingEnable = 'Zur Aktivierung/Deaktivierung Logging sollen Sie Pims neustarten.';
// MRA:6-JUN-2011. RV093.3.
  SPimsCleanUpEmpHrsLog = 'Cleanup Mitarbeiterstunden-Logging �lter als %d Wochen?';
  SPimsCleanUpTRSLog = 'Cleanup Zeiterfassung-Scans-Logging �lter als %d Wochen?';
// MRA:6-JUL-2011. RV094.5.
  SPimsFilenameAFAS = 'Lohnmutationen ABS PIMS';
// MRA:11-JUL-2011. RV094.7.
  SPimsPlantDeleteNotAllowed = 'Es ist nicht erlaubt, ein Betrieb zu l�schen.';
  SPimsPlantUpdateNotAllowed = 'Es ist nicht erlaubt, ein Betrieb zu �ndern.';
  SPimsEmployeeDeleteNotAllowed = 'Es ist nicht erlaubt, ein Mitarbeiter zu l�schen.';
  SPimsEmployeeUpdateNotAllowed = 'Es ist nicht erlaubt, ein Mitarbeiter zu �ndern.';
  SPimsWorkspotDeleteNotAllowed = 'Es ist nicht erlaubt, ein Arbeitsplatz zu l�schen.';
  SPimsWorkspotUpdateNotAllowed = 'Es ist nicht erlaubt, ein Arbeitsplatz zu �ndern.';
// MRA:11-JUL-2011. RV04.8.
  SPimsEmployeeNotActive = 'Mitarbeiter ist nicht-Activ.';  
// MRA:19-OCT-2011. RV099.1.
  SPimsPleaseFillBothAbsReasons = 'Bitte f�llen Sie beide Abwesenheitsgr�nde.';
  SPimsLogStartTFTHolCorrection = 'Starten ZFZ/Urlaub Korrektur.';
  SPimsLogEndTFTHolCorrection = 'Ende ZFZ/Urlaub Korrektur.';
  SPimsLogChangesMade = 'Anzahl �nderungen: %d';
// MRA:28-OCT-2011. RV100.2.
  SPimsEarnedTFT = 'ZFZ Verdient';
  SPimsEarnedTFTSign = 'ZFZ-V';
// MRA:9-JAN-2012. RV103.4.
  SPimsHolidayCardTitle = 'Urlaub Karte';
// MRA:13-JAN-2012. RV103.4.
  SJanuaryS = 'Jan.';
  SFebruaryS = 'Febr.';
  SMarchS = 'March';
  SAprilS = 'Apr.';
  SMayS = 'May';
  SJuneS = 'June';
  SJulyS = 'July';
  SAugustS = 'Aug.';
  SSeptemberS = 'Sept.';
  SOctoberS = 'Oct.';
  SNovemberS = 'Nov.';
  SDecemberS = 'Dec.';
// 20011796.2.
  SPimsRepHolCardOpeningBalance = 'Opening balance:';
  SPimsRepHolCardUsed = 'Used:';
  SPimsRepHolCardAvailable = 'Available:';
  SPimsRepHolCardTFTEarned = 'TFT-E Earned:';
  SPimsRepHolCardTUsed = 'T Used:';
  SPimsRepHolCardTotAvail = 'Total Available:';
  SPimsRepHolCardLegenda = 'T = Time for Time used, TFT-E = Time for Time earned, H = Holiday';
  SPimsRepHolCardLegendaTFTEarned = 'TFT-E = Time for Time earned';
  SPimsInvalidCheckDigits = 'Invalid checkdigits! Please enter a value from 10 to 99.';
  SPimsEnteredDateRemark = 'Please enter a Date-IN equal to selected date or next date.';
  SPimsShifts = 'Shifts'; // 20013551
  SPimsWorkspotHeader = 'Workspot'; // 20013551
  // 20013723
  SPimsExportPeriod = 'Export overtime';
  SPimsExportMonth = 'Export payroll';
  // 20013288.130
  SPimsExportEasyLaborChoice = 'Please make a choice about what to export.';
  // 20014002
  SPimsTotalWorkspot = 'Total Arb.Pl.';
  // 20014037.60
  SPimsPimsUser = 'Pims User';
  SPimsWorkstation = 'Workstation';
  // TD-22429
  SPimsStart = 'Start';
  // TD-22475
  SPimsPasteDatacolConnection = 'Paste datacol connections from "%s"';
  SPimsDatacolConnectionFound = 'DatacolConnections found, these will be deleted. Proceed Yes / No?';
  // TD-22503
  SChangeEmplAvail = 'Mitarbeiter Verf�gbarkeit gefunden, dies wird entfernt wegen Nicht-Verf�gbar. Weitergehen?';
  // SO-20014715
  SPimsReportYes = 'Ja';
  SPimsReportNo = 'Nein';
  SPimsReportTill = 'zu';
  SPimsReportLate = 'Sp�t';
  SPimsReportEarly = 'Fr�h';
  // TD-23315 Changed this message:
  SPIMSTimeIntervalError = 'Startzeit ist kleiner dann vorige Endezeit,' + #13 +
   'oder Endezeit ist gr�sser dann volgende Startzeit.';
  // 20013196
  SPimsDeleteLinkJob = 'Ein Link-Job ist definiert. Dieser wird entfernt. Weitergehen Ja/Nein?';
  SPimsLinkJobExist = 'Sie k�nnen nur 1 Link-Job pro Arbeitsplatz definieren.' + #13 +
    'Diese Link-Job wird nicht gespeichert.';
  // 20011800
  SPimsOK = '&Ok';
  SPimsFinalRun = '&Final Run';
  SPimsFinalRunWarning = 'FINAL RUN, ARE YOU SURE?' + #13#13 +
    'Any mutations over this period cannot be made afterwards.';
  SPimsFinalRunExportDateWarning = 'FINAL RUN' + #13#13 +
    'Selected export-period is already exported (partly or completely).' + #13#13 +
    'It is NOT allowed to make this export.';
  SPimsFinalRunExportDateWarningAdmin = 'FINAL RUN' + #13#13 +
    'Selected export-period is already exported (partly or completely).' + #13#13 +
    'Do you want to start the export?';
  SPimsTestRunExportDateWarning = 'TEST RUN' + #13#13 +
    'Selected export-period is already exported (partly or completely).' + #13#13 +
    'Do you want to start the export?';
  SPimsFinalRunExportGapWarning = 'FINAL RUN' + #13#13 +
    'There is a gap of %d days between last-export-date' + #13 +
    'and start-date of selection.' + #13#13 +
    'It is NOT allowed to make this export.';
  SPimsFinalRunExportGapWarningAdmin = 'FINAL RUN' + #13#13 +
    'There is a gap of %d days between last-export-date' + #13 +
    'and start-date of selection.' + #13#13 +
    'Do you want to start the export?';
  SPimsTestRunExportGapWarning = 'TEST RUN' + #13#13 +
    'There is a gap of %d days between last-export-date' + #13 +
    'and start-date of selection.' + #13#13 +
    'Do you want to start the export?';
  SPimsCloseScansSkipped = 'WARNING: Some scans could not be closed because of a compare' + #13 +
    'with last-export-date.';
  SPimsFinalRunAddAvaililityNotAllowed = 'Adding availability for this week is not allowed' + #13 +
    'because it has already been exported.';
  SPimsFinalRunDateChange = 'WARNING: The date has been changed to a date after the last-export-date.';
  SPimsFinalRunAvailabilityChange = 'WARNING: Some employee availability could not be changed because of the last-export-date.';
  SPimsFinalRunTestRun = 'TEST RUN';
  SPimsFinalRunDeleteNotAllowed = 'WARNING: Delete is not allowed, because it was already exported.';
  SPimsFinalRunOnlyPartDelete = 'WARNING: Only a part of the week will be deleted, because some part was already exported.';
  SPimsFinalRunShiftSchedule = 'WARNING: Not all could be copied because some part was already exported.';
  // SO-20014442
  SPimsYearMonth = 'Jahr/Monat';
  SPimsStartEndYearMonth = 'Ende Jahr / Monat sollte gr��er sein als Start Jahr / Monat!';
  SPimsOnlySANAEmps = 'Nur SANA Mitarbeiter';
  SPimWrongYearSelection = 'Falsche Jahr-Auswahl: Die Von-Bis-Jahr m�ssen gleich sein!';
  // 20015178
  SPimsPleaseEnterFakeEmpNr = 'Bitte geben Sie eine eindeutige Fake-Mitarbeiternummer,' + #13 +
    'die in Personal Screen verwendet wird.';
  SPimsFakeEmpNrInUse = 'Der eingegebene Fake-Mitarbeiternummer ist bereits von einem anderen' + #13 +
    'Arbeitsplatz. Bitte geben Sie eine andere Nummer.';
  // 20015223
  SPimsWorkspots = 'Arbeitspl�tze';
  SPimsPlantTitle = 'Betrieb';
  SPimsAllWorkspots = '<Alle>';
  SPimsMultipleWorkspots = '<Mehrere>';
  // 20013476
  SPimsValueSampleTimeMins = 'Sample Zeit: Ung�ltiger Wert eingegeben. Bitte geben Sie einen Wert von 1 bis 30 Minuten.';
  // 20015586
  SPimsIncludeDowntime = 'Einschlie�en Ausfallzeiten Mengen:';
  SPimsIncludeDowntimeYes = 'Ja';
  SPimsIncludeDowntimeNo = 'Nein';
  SPimsIncludeDowntimeOnly = 'Nur anzeigen Ausfallzeiten';
  // TD-25948
  SPimsOpenCSV = 'Offene CSV-Datei?';
  SPimsCannotOpenCSV = 'Bitte installieren Sie Microsoft Excel (oder ein �hnliches Programm).';
  // PIM-53
  SPimsNoWorkingDays = 'Bitte w�hlen Sie eine oder mehrere Arbeitstage.';
  SPimsAddHoliday = 'Wollen Sie %d Abwesenheit Tag(e), um die Verf�gbarkeit Mitarbeiter hinzuf�gen?';
  // PIM-53
  SPimsAddHolidayEmplAvail = 'Bitte geben Sie eine g�ltige Abwesenheit Ursachencode.';
  SPimsAddHolidayNoStandAvail = 'Aktion abgebrochen: Es gibt keine Standard-Verf�gbarkeit f�r die Betrieb/Schicht-Kombination!';
  // PIM-52
  SPimsWorkScheduleDetailsExists = 'Arbeitszeitplaninformationen bereits vorhanden! �berschreiben?';
  SPimsWorkScheduleWrongRepeats = 'Falscher Wert f�r Wiederholungen pro Woche! Bitte geben Sie einen Wert zwischen 1 und 26.';
  SPimsWorkScheduleInUse = 'Arbeitszeitplan ist noch in Gebrauch (Mitarbeiter Vertrag) und k�nnen nicht gel�scht werden!';
  SPimsInvalidShift = 'Bitte geben Sie eine g�ltige Schicht.';
  SPimsReady = 'Fertig';
  SPimsPlanningMessage1 = 'Nicht verarbeitet! F�r Mitarbeiter';
  SPimsPlanningMessage2 = 'und datum';
  SPimsPlanningMessage3 = 'existiert Planung.';
  SPimsNoStandAvailFound1 = 'Fehler: Kein Standard-Verf�gbarkeit f�r Mitarbeiter gefunden';
  SPimsNoStandAvailFound2 = 'und Schicht';
  SPimsProcessWorkScheduleMessage1 = 'Verarbeiten Arbeitsplan f�r Mitarbeiter';
  SPimsProcessWorkScheduleMessage2 = 'und Referenzdatum';
  // PIM-23
  SPimsTotals = 'Totalen';
  SPimsOTDay = 'Tag';
  SPimsOTWeek = 'Woche';
  SPimsOTMonth = 'Monat';
  SPimsMO = 'MO';
  SPimsTU = 'DI';
  SPimsWE = 'MI';
  SPimsTH = 'DO';
  SPimsFR = 'FR';
  SPimsSA = 'SA';
  SPimsSU = 'SO';
  SPimsNormalHrs = 'Normalstunden pro Tag:';
  SPimsOTPeriod = 'Periode';
  SPimsOTPeriodStarts = 'Beginnt in der Woche';
  SPimsOTWeeksInPeriod = 'Wochen im Zeitraum:';
  SPimsOTWorkingDays = 'Arbeitstage:';
  SPimsCGRound = 'Runden';
  SPimsCGTRunc = 'K�rzen';
  SPimsCGMinute = 'Minuten';
  SPimsCGTo = 'zu';
  SPimsStandStaffAvailUsed = 'HINWEIS: F�r einen oder mehrere Tage wurde eine feste Abwesenheitsgrund verwendet,'#13'basiert auf Standard-Personalverf�gbarkeit.'#13'Dies wird nicht �berschrieben.';
  SPimsShiftDate = 'Schicht Datum';
  // ABS-27052
  SPimsSelectPrinter = 'Druckerauswahl';
  SPimsSelectDefaultPrinter = 'W�hlen Sie einen Drucker als Standarddrucker';
  SPimsNoPrinterFound = 'Kein Drucker gefunden!';
  SPimsPleaseSelectPrinter = 'Bitte w�hlen Sie einen Drucker.';
  // PIM-52
  SPimsNoCurrentEmpContFound = 'Kein aktueller Vertrag gefunden f�r Mitarbeiter';
  // PIM-203
  SPimsHostPortExists = 'Diese Host+Port-Kombination existiert bereits f�r einen anderen Arbeitsplatz.';
  // PIM-174
  SPimsCopied = 'Kopiert:';
  SPimsRows = 'Reihen.';
  SPimsEmp = 'Mitarbeiter';
  SPimsEmpContract = 'Mitarbeiter Vertrag';
  SPimsWSPerEmp = 'Arbeitspl�tze pro Mitarbeiter';
  SPimsStandStaffAvail = 'Standard Personal Verf�gbarkeit';
  SPimsShiftSched = 'Schichtschema';
  SPimsEmpAvail = 'Mitarbeiter Verf�gbarkeit';
  SPimsStandEmpPlan = 'Mitarbeiter Standard Planung';
  SPimsEmpPlan = 'Personalsplanung';
  SPimsAbsenceCard = 'Abwesenheitskarte';
  SPimsRecordKeyDeleted = 'Dieser Datensatz wurde von einem anderen Benutzer gel�scht.';
  SPimsWorkScheduleWrongStartDate = 'Die Referenzwoche muss ein Datum sein, das am ersten Tag der Woche beginnt.'#13'Dies wurde korrigiert';
  SPimsRoamingWorkspotExists = 'Es gibt bereits einen anderen Arbeitsbereich, der als Roaming definiert ist.';
  SPimsHourlyWage = 'Stundenlohn:';
  SPimsTotalExtraPayments = 'Total zus�tzlichen Zahlungen';
  SPimsTBPEmp = 'Timeblocks per Employee';

{$ELSE}
  {$IFDEF PIMSDANISH}

  SPimsHomePage = 'Pims Startside bliver startet snarest!';
  SPimsIncorrectPassword = 'Forkert kendeord.'#13'Tast andet kendeord eller'+
    'Annull.';
  SPimsNoEmptyPasswordAllowed = 'Tomt kendeord er ikke tilladt.'#13'Tast '+
    'andet kendeord eller annull.';
  SPimsPasswordChanged = 'Nyt kendeord gemt';
  SPimsPasswordNotChanged = 'Kendeord ikke �ndr.';
  SPimsPasswordNotConfirmed = 'Kendeord og bekr�ftelse er '+
    'forskell.'#13'Tast andet kendeord eller annull.';
  SPimsSaveChanges = 'Gem �ndr.?';
  SPimsRecordLocked = 'Denne fil bruges af anden bruger';
  SPimsOpenXLS = '�bn Excel fil?';
  SPimsOpenHTML = '�bn HTML fil?';
  SPimsNotUnique = 'Ikke unik';
  SPimsNotFound = 'Intet fundet!';
  SPimsNotEnoughStock = 'Der er ikke nok produkter p� lager!'#13'Tildel '+
    'dele uanset?';
  SPimsNoItems = 'Intet endnu';
  SPimsNoEmployee = 'Der er ingen medarb.!';
  SPimsNoCustomer = 'Der er ingen kunder!';
  SPimsLoading = 'Henter ';
  SPimsKeyViolation = 'Linie eksisterer allerede';
  SPimsForeignKeyPost = 'Denne fil har forbindelse til andre filer. Dit '+
    '�nske er afvist.';
  SPimsForeignKeyDel = 'Denne fil har forbindelse til andre filer. Dit '+
    '�nske er afvist.';
  SPimsFieldRequiredFmt = 'Udfyld n�dvendige felter %s.';
  SPimsFieldRequired = 'Udfyld alle n�dvendige felter.';
  SPimsDragColumnHeader = 'Tegn kolonneoverskrift her til gruppering.';
  SPimsDetailsExist = 'Denne fil har forbindelse til andre filer.  '+
    'fjern f�rst disse filer.';
  SPimsDBUpdate = 'Updat. Pims database';
  SPimsDBUpdateButtonCaption = '&K�r Pims updat.';
  SPimsDBUpdateConflict = 'Pims opdagede en konflikt under updatering af '+
    'database.'#13'Kontakt ABS helpdesk med henvisning til denne besked.';
  SPimsDBNeedsUpdating = ' Pims opdagede en �ldre database version.'#13+
   ' Pims m� opdatere databasen for at k�re korrekt.';
  SPimsCurrentCode = 'Nuv�rende kode. ';
  SPimsContinue = ' Forts�t?';
  SPimsConfirmCancelNote = 'Vil du annullere denne f�lgeseddel?';
  SPimsConfirmDeleteNote = 'Vil du annullere denne f�lgeseddel?';
  SPimsConfirmDeleteLicense = 'Vil du slette denne Pims Licens?';
  SPimsConfirmDelete = 'Vil du slette nuv�rende fil?';
  SPimsCannotPrintLabel = 'Du kan ikke udskrive en label for den valgte '+
    'del.';
  SPimsCannotPlaceFlag = 'Du kan ikke s�tte et flag p� den valgte '+
    'del.';
  SPimsCannotOpenXLS = 'Installer Microsoft Excel.';
  SPimsCannotOpenHTML = 'Installer en Internet Browser p� din maskine.';

  // FOR PIMS:
  SSelectDummyWK = 'Et dummy arb.sted kan ikke v�lges!';
  SPimsPercentageNotCorrect = 'Fejl: Total procent for arbejdsstedet er ikke 100';
  SPimsTimeMustBeYounger = 'Fejl: Starttid skal v�re f�r sluttid!';
  SPimsNoMaster = 'Ingen h�ndteringsafd.';
  SPimsNoSubDetail = 'der er ingen afd.';
  SPimsNoDetail = 'Ingen arbejdssteder';
  SPIMSContractTypePermanent = 'Permanent';
  SPIMSContractTypeTemporary = 'Midlert.';
  SPIMSContractTypePartTime = 'Ekstern';
  SPIMSContractOverlap = 'Denne kontrakt overlapper sidste kontrakt';
  SPimsTBDeleteLastRecord = 'Kun seneste fil kan slettes';
  SPimsTBInsertRecord = 'Kun %d tidsblokke tilladt';
  SPimsStartEndDate = 'Startdato er senere end slutdato';
  SPimsStartEndTime = 'Starttid er senere end sluttid';
  SPimsStartEndDateTime = 'Startdato/tid er senere end slutdato/tid';
  SPimsStartEndActiveDate = 'Start aktiv-dato er senere end slut aktiv-dato';
  SPimsStartEndInActiveDate = 'Start inaktiv-dato er senere end slut inaktiv dato';
  SPimsWeekStartEnd = 'Slut uge < Start uge !';
  SPimsBUPerWorkspotPercentages = 'Total procent b�r v�re 100,  ' +
    'form. kan ikke forlades';
  SPimsTBStartTime = 'Starttid TidsBlok < Starttid Skift p� ';
  SPimsTBEndTime = 'Sluttid Tidsblok > Sluttid Skift p� ';
  SPimsBUPerWorkspotInsert = 'Der er ingen arb.sted, filer kan ikke inds�ttes';
  SPimsDateinPreviousPeriod =  'Denne tid findes p� en eksisterende skanning';
  SPimsExistJobCodes = 'Kan ikke �ndres, der er defineret jobkoder';
  SPimsExistBUWorkspot = 'Kan ikke �ndres, der er relaterede filer i forretningsomr�der per arbejdssted';
  // Time Recording
  SPimsIDCardNotFound = 'ID Kort ej fundet';
  SPimsIDCardExpired	= 'ID Kort udl�bet';
  SPimsEmployeeNotFound = 'Medarb. ikke fundet';
  SPimsInactiveEmployee = 'Medarb. er inaktiv';
  SPimsScanningsInMinute = 'Tillad ikke flere skanninger i 1 minut';
  SPimsStartDay = 'Start p� dag';
  SpimsEndDay = 'Slut dag';
  SPimsNoWorkspot = 'Arb.sted ikke defineret p� denne arb.station';
//Workspot
  SPimsNoJob = 'Jobs ikke defineret p� dette arb.sted';
  SPimsDeleteJob = 'Der er tildelt jobkoder, de vil blive slettet!';
  SPimsDeleteBusinessUnit = 'Der er tildelt jobkoder, de vil blive slettet!';
  SPimsDeleteJobBU =
    'Alle filer forbundet til dette arbejdssted vil blive slettet, slet arbejdssted ? ';
  SShowBUWK         = 'Vis forr.omr. per arbejdssted';
  SShowJobCode = 'Vis jobkoder';
//workspot per workstation
  SPimsSameSourceAndDest = 'Afsender- og modtager- computernavn kan' +
   	'ikke v�re ens';
  // Selector Form - Grid Column name
  SPimsColumnPlant 	= 'Virks';
  SPimsColumnWorkspot = 'Arb.sted';
  SPImsColumnCode = 'Kode';
  SPimsColumnDescription = 'Beskrivelse';
  // Selector Form - Captions
  SPimsWorkspot = 'V�lg arb.sted';
  SPimsJob 	= 'V�lg Job';
  SPimsJobCodeWorkSpot = 'Form. kan ikke lukkes, definer f�rst mindst en jobkode';
  SPimsInterfaceCode  =
   'Formular kan ikke lukkes. F�rst skal defineres mindst en link kode forskellig fra link kode for jobbet';
  SPimsOtherInterfaceCode  = 'Definer linkkode forskellig fra linkkode for jobkode';
  SPimsNoOtherInterfaceCodes = 'Der er ikke defineret andre link koder, du kan ikke �bne form.';

  SPimsSaveBefore = 'Gem filen f�r';
  SPimsNoPlant = 'Ingen vaskerier';
  SPimsScanDone = 'Scanning gennemf.';
  SPimsScanError = 'Scanning IKKE gennemf.: Data-Base FEJL';
//  SPimsNoEmployee = 'Der er ingen medarb.';
// Time Recording Scanning
  SPimsNoShift = 'Skift ikke def.';
  SPimsNoCompletedScans = 'F�rdige scanninger kan ikke �ndres/slettes';
  SPimsNonScanningStation = 'Denne station er ikke til tidsreg.';
  SPimsScanTooBig = 'Scanningsperiode over 12 timer er ikke tilladt';
  SPimsUseJobCode = 'V�lg en job kode, arb.sted er defineret til at bruge job koder';
  SPimsNoMatch = 'Ingen match for medarb.+ID-kort';
// team per department - extra check
  SPimsNoMorePlantsinTeam =
    'Alle afdelinger i et team skal tilh�re samme vaskeri!';
  SPimsTeamPerEmployee =
    'Medarbejderens afdeling skal v�re i teamet !';

// Hours per Employee
  SPimsAutomaticRecord = 'Automatisk indsat record kan ikke �ndres';
  SPimsNegativeTime = 'Dobbeltklik for at s�tte negative tid (R�d)';
  SPimsExistingRecord = 'En fil med samme parametre eksisterer allerede.'+#13+
  	'V�lg den fil';
  SPimsProductionHour = 'ProductionHourPerEmployee tabel kan ikke opdat.';
  SPimsNoTimeValue = 'Inds�t tidsv�rdien for mindst en dag';
  SPimsSalaryHour = 'SalaryHourPerEmployee tabel kan ikke opdat.';
  SPimsAbsenceHour = 'AbsenceHourPerEmployee tabel kan ikke opdat.';
  SPimsTo = 'To';
  SPimsInvalidTime = 'V�rdi ikke sat! (Ugyldig tidsv.)';
// bank holiday process
  SPimsConfirmProcessEmployee = 'Vil du �ndre alle sk�ve helligdage for ' +
    ' �ret til medarb. tilg�ngelighed ?';
  SPimsInvalidAbsReason = 'Ikke gyldig frav�rsgrund!';
  SPimsEmptyAbsReason = 'Udfyld feltet frav�rsgrund!';
  SPimsEmptyDescription = 'Udfyld feltet beskrivelse!';
// for reports
  SPimsReportWTR_HOL_ILL_Checks = 'Der skal v�lges mindst en frav�rsgrund!';
  SPimsMaxNumberOfWeeks = 'Max antal uger, der kan v�lges er 13!';
  SPimsEndTime = 'Slut tid';
  SPimsStartTime = ' < Start tid';
  SPimsOn = 'p�' ;
// update database
  SPimsInvalidVersion = 'Ugyldig PIMS version !';
  SUpdateDatabaseFrom = 'Database bliver opdateret fra version';
  SUpdateDatabaseTo = ' til version';
// Standard staff availability
  SCheckAvailableTB = 'Tilladte v�rdier er * og �';
  SCheckAvailableTBNotEmpty = 'Udfyld n�dvendige felter !';
  SCopySTA1 = 'Std. r�dighed eksisterer allerede for dag';
  SCopySTA2 = ', overskriv ? ';
  SCopySameValues = 'Du valgte den samme fil!';
  SCopySTANotValidFrom = 'Der er ingen gyldig tidsblok for den valgte fil!';
  SCopySTANotValidTo = 'Der er ingen gyldig tidsblok for den valgte grid-fil!';
  SEmptyValues = 'Udfyld alle felter f�r!';
  SEmplSTARecords = ' Standard ledighedsperiode ikke defineret for denne medarb. ';    
// shift schedule
  SShiftSchedule = 'Tilladte v�rdier er tallene 0 .. 99 eller -';
  SSHSShift = 'Till. v�rdier er kun defineret p� skift';
  SSHSWeekCopy = 'Kopiens startdato skal v�re st�rre end slut dato';
  SInvalidShiftSchedule = 'Skift nr. har ikke def. eller v�rdier start/slut' +
    ' tid pr. dag er nul!';
  SEmployeeAvail_0  = 'Medarb. ';
  SEmployeeAvail_1  = ' har allerede r�dighed for dette skift p� �r, uge, dag ';
//  SEmployeeAvail_2 = ', dag ';
  SEmployeeAvail_3 = ', Du kan ikke �ndre skift nummeret.';
  SUndoChanges = 'Sidste �ndr. bliver ikke gemt!';
  SStartWeek = 'Start uge skal v�re st�rre end slut uge!';
  SCopySelectionShiftSchedule_1 = 'Skift plan for medarbejder ';
  SCopySelectionShiftSchedule_2 = ' Eksist. allerede Overskriv ?';
  SCopyFinished = 'Kopi er f�rdig!';
  SEmptyRecord = 'Ingen medarbejder valgt !';
// employee availability
  SEmplAvail = 'Tilladte v�rdier er *, - eller en frav�rsgrund';
  SValidTimeBlock = 'Alle tidsblokke skal v�re udfyldt';
  SEmployeePlanned = 'Medarb. er allerede planlagt, forts. vil slette planl�gning. ' +
    'Vil du forts�tte ?';
  SCopyFrom  = 'V�lg en fil!';
  SCopyTo  = 'Records are the same, please select a different record!';
  SEMAOverwrite = 'Medarb. ledig til ';
  SEMAShift = ' skift ';
  SEMAExists = 'eksisterer allerede. Vil du overskrive?';
// staff planning
  SSelectedWorkspots = 'Mere end 401 arbejdssteder er valgt, kun de f�rste 401 vil ' +
    ' blive vist ';
  SEmptyWK = 'Ingen arbejdssted valgt';
  SOverplanning = 'Overbookning mere end 200%';
  SMakeAvailable = 'Marker ledig';
  SNotAcceptPlanned = 'Accepterer ikke denne planl. for medarb.!';
  SSaveChanges = 'Gem �ndr.?';
  SReadStdPlnIntoDaySelection = 'Planl. er l�st for virksomhed/team/dato inden for markering';
  SReadStdPlnIntoDaySelectionFinish = 'Indl�sn. af std. plan til dagens plan er f�rdig';
  SReadPastIntoDayPlanFinish = 'Indl. tidl. til dagsplan f�rdig';
  SContinueYN = 'Vil du forts�tte ?';
  SDateFromTo = 'Slut dato skal v�re st�rre end start dato!';
  SDateIntervalValidate = ' Sk�ringsdatointerval skal v�re forskelligt fra opr.' +
    ' datointerval';
  SNoEmployee = 'Ingen medarbejdere valgt!';
  SOCILevel = 'Besk. niveau ';
  SPlannedLevel = 'Planl. p� niveau ';
  SPlannedAllLevels = 'Planlagt';
  SOCIAllLevel = 'Besk�ft.';
  SSameRangeDate = 'Dato interval skal v�re af samme l�ngde!';
// report staff planning
  SStartEndYear = 'Slut �r skal v�re st�rre end start �r!';
  SWeek = 'Du kan ikke v�lge mere end en uge!';
  SNoTeam = 'T�m team: ';
// Workspot per employee
  SLEVELWK = 'Kun A, B, C er tilladt!';
  SInvalidWK = 'Kun A..Z, a..z, 0..9, og ''_'' er tilladt !';
// Staff Availability
  SChangePlaned = 'Medarb. er allerede planl., forts. vil slette planl�gning'+
    #13+'Vil du forts�ttte?';
  SInvalidShiftNo = 'Ugyldigt skift nr. for dag: %d';
  SInvalidAvailability = 'Ugyldig r�dighedsv�rid for dag: %d, TB: %d';
//  E&R
  SMoreThan = ' Mere end ';
  SInvalidMistakeValue =  'Antal fejl skal v�re over nul !';
  SNotDeleteRecord = ' Du kan ikke slette denne fil !';
  SPimsDBError = 'Rapport �bnet af anden station eller databasefejl. '+#13+
		'K�r rapporten igen';
 //
// productivity reports

  SDaySelect = 'Startdag m� v�re mindre end slutdag!';
//
// jobcode
  SInterfaceCode = 'Interfacekode bruges allerede af andre arbejdssteder!';
  SWKInterfaceCode = 'Interfacekode er allerede brugt til dette arbejdssted!';
  SWKRescanInterfaceCode =
   'Der er interfacekoder brugt mere end en gang til dette arb.sted!';
  SDeleteOtherInterfaceCode = 'Alle andre def. interfacekoder vil blive slettet!';

//
  SExceptionDataSetReport =
   'Dataops�tn. er tom kan ikke danne rapport, ops�t f�rst og gendan pims';
  SExceptionDataSetForms  =
   'Dataops�tn. er tom kan ikke danne grids, ops�t f�rst og gendan pims';
// export payroll report
   SPeriodReport = 'Per.';
   SMonthReport = 'M�ned';
// report quality incentive
   SCalculationwithOvertime = 'Kvalitetsansporende kalkulation med overtid rapp.'; 
// add wk into datacollection
  SADDWKDateCollection = 'Filer er tilf.';
// strings for mistake per employee
  SDate = 'Dato ';
  STotalWeek = 'Total uge ';
// report hrs per empl
  SCaptionProdRepHrs = 'Produktion timer pr. medarb.';
  SCaptionSalRepHrs =  'L�n timer pr. medarb.';
//report CompHours
  SShowActiveEmpl = 'Vis aktive medarb.';
  SShowNotActiveEmpl = 'Vis ikke aktive medarb.';
//report HrsWKCum
  SRepHrsCUMWK = 'Arb.sted';
  SRepHrsTotalCUM = 'Total arb.sted';

  SRepHrsCUMWKBU = 'Forr.omr.';
  SRepHrsTotalCUMBU = 'Total forr.omr.';

  SRepHrsCUMWKPlant = 'Vask.';
  SRepHrsTotalCUMPlant = 'Total vask.';

  SRepHrsCUMWKDept = 'Afdeling';
  SRepHrsTotalCUMDept = 'Total afdeling';
  SRepJobCode = 'Job kode';
  SRepTotJobCode = 'Total job kode';
// report absence
  SHeaderAbsRsn = 'Frav�r �rsag';
  SHeaderWeekAbsRsn = 'Uge';
  SHdTotalWeek = 'Total uge';
  SRepExpPayroll = 'Fra';
// report absence schedule
  SReport = 'Rapport';
  SReportWorkTimeReduction = 'arb.tid reduktion ';
  SReportIllness = 'sygdom';
  SReportHoliday = 'ferie';
  SActiveEmpl =  'Aktive medarb.';
  SInactiveEmpl = 'Inaktive medarb.';
// report
  SRepHrsPerEmplActiveEmpl = 'Vis aktive medarb.';
  SRepHrsPerEmplInActiveEmpl = 'Vis ikek aktive medarb.';
  SRepProdHrs = 'Produktive timer';
  SRepNonProdHrs = 'Ikkeproduktive timer';
  SAll = 'All';
  SActive = 'Aktive';
  SInactive = 'Inaktive';
  SName = 'Navn ';
  SShortName = 'S�ge navn';
// REPORT check list
  SShowProcessed =  'Vis bearb.';
  SShowNotProcessed  = 'Vis ikke bearb.';
//Report
  SErrorOpenRep = 'Fejl database�bning, rapport kan ikke �bnes';
  SErrorCreateRep = 'Fejl rapport kan ikke dannes';
//DialogProcessAbsenceHrsFRM
   SLogEmpNo = 'Antal filer fundet=';
   SLogChangedAbsenceTotal = '�ndret %d Frav�r total-filer';
   SLogDeletedRec = 'Slettet %d Frav�r time-filer';
   SLogEndAbsence = 'End: Definerer frav�rstimer pr. medarb.';
   SLogStartSalaryHours = 'Start: Definerer l�ntimer pr. medarb.';
   SLogStartAvailability  = 'Start: Definerer medarb. ledighed';
   SLogChangedAbsence = '�ndret %d Frav�r time-filer';
   SlogAddedAbsence = 'Tilf. %d Frav�r time-filer';
   SLogEndAvailability = 'End: Definerer medarb. ledighed';
   SLogStartNonScanning = 'Start: Definerer ikke-skannet medarb. ledighed';
   SLogEndNonScanning  = 'End: Definerer ikke-skannet medarb. ledighed';
   SLogFoundEmployeeNo = 'Antal filer fundet = %d';
   SSProcessFinished = 'Behandl. slut';
// Staffplanning
   SStaffPlnEmployee = 'Medarb.';
   SStaffPlnName = 'navn';
// Contract group form
   SOvertimeDay = 'Dag';
   SOvertimeWeek = 'Uge';
   SValidMinutes = 'Definerede min. skal v�re mindre end 59';
   SQuaranteedHourType = 'V�lg en time type';
//
//update cascade
   SConfirmUpdateCascadeEmpl =
     'Alle tilh�rende filer for medarb. vil blive �ndret, opdater medarb. ? ';
   SConfirmDeleteCascadeEmpl =
     'Alle tilh�rende filer med denne medarb. vil blive slettet, slet medarb. ? ';
   SEmpExist = 'Medarb. eksisterer allerede ';
//export payroll MRA:RV095.4. Give better message: 'dato' added.
   SNotDefaultSelection = 'Ikke standard dato-valg. Er du sikker ?';
   SPeriodAlreadyExported = 'Periode er allerede eksporteret. Er du sikker ?';
// transfer free time
   SPimsDifferentYears = 'Sk�rings�r skal v�re forskellig fra opr. �r!';
   SPimsCheckBox = 'Mindst et felt skal v�lges!';
   SPimsProcessFinish = 'Proces er f�rdig!';

   // MR:17-01-2003
   SPimsUseShift = 'V�lg et skift!';
   SPimsUseWorkspot = 'V�lg et arbejdssted!';
//  staff planning -CAR 30-01-2003
   SPimsDayPlanning = 'Dagsplanl.: ';
   SPimsTeamSelection = 'Team valg: ';
   SPimsTeamSelectionAll = 'All teams';
   SPimsSTDPlanning = 'Standard planl.: ';
   SPimsStaffTo =  ' til';
   SPimsPlant =  '      Vask.:  ';
   SPimsShift = '      Skift: ';
   SPimsTeam = 'Team ';
   // MR:28-01-2003
   SPimsNoEmployeeLine = 'Nul medarb.';

   SPimsNoPrevScanningFound = 'Ikke fundet tidl. skanning';
   SpimsScanExists = 'Der er allerede en scanningsfil p� denne dag for denne medarb. ' + #13 +
     'Du kan ikke �ndre denne.';

 //car: 27.02.2003
   SPimsEXCheckIllMsgClose =
     'Der er allerede en �ben sygdomsmedd., luk denne f�r';
   SPimsEXCheckIllMsgStartDate =
     'Start dato b�r v�re st�rre end slut dato for sidst dannede sygdomsmedd. ';

   // MR:03-03-2003
   SPimsContract = 'Kontrakt';
   SPimsPlanned = 'Plan.l';
   SPimsAvailable = 'Ledig';

   // MR:07-03-2003
   SpimsYear = '�r';
   SPimsWeek = 'Uge';
   SPimsDate = 'Dato';
   //CAR:21-03-2003 - REPORT staff planning per day
   SPimsNewPageWK = 'Ny side pr. arb.sted';
   SPimsNewPageEmpl = 'Ny side pr. medarb.';
   //CAR:21-03-2003 report checklist
   SPimsAvailability = 'Ledighed: ';
   //CAR:21-03-2003 update cascade -Plant
   SConfirmDeleteCascadePlant =
     'Alle filer forbundet til dette vask. vil blive slettet, slet vask. ? ';
   SConfirmUpdateCascadePlant =
     'Alle tilh�rende filer til vaskeri �ndres, opdater vaskeri ?';
   SPlantExist = 'This plant already exists ';
   //CAR: 27-03-2003
   SRepTeamTotEmpl ='Total medarb.';
   //CAR 29-03-2003
   SRepHRSPerEmplCorrection = 'Manuelle korr.';
   SRepHrsPerEmplTotal = 'Total';
   //CAR 31-03-2003  - Report AbsenceCard
   SJanuary = 'Januar';
   SFebruary = 'Februar';
   SMarch = 'Marts';
   SApril = 'April';
   SMay = 'Maj';
   SJune = 'Juni';
   SJuly = 'Juli';
   SAugust = 'August';
   SSeptember = 'September';
   SOctober = 'Oktober';
   SNovember = 'November';
   SDecember = 'December';
//CAR  2-04-2003 - report Production Detail
   SRepProdDetPerformance = 'Ydelse';
   SRepProdDetBonus = 'Bonus';
// CAR 2-4-2003 - Stand Staff availability
   SStdStaffAvailPlanned =
     'Medarb. er planlagt i std.planl�gning, forts�ttelse vil slette planl. ' +
     'Vil du forts�tte ?';
//CAR 7-4-2003
   SPimsJobs  = 'Jobkode';
//CAR 15-4-2003
  SPimsStartEndDateTimeEQ = 'Start dato/tid er st�rre eller lig slut dato/tid';
  SPimsStartEndTimeNull = 'Start tid eller slut tid er tom, vil du forts�tte ?';
// MR:24-04-2003
  SPimsNoData = '0 Data';
//CAR 5-5-2003
  SPimsAbsence = 'Frav�r';
  SPimsDifference = 'Difference';
// car 12-5-2003
  SExportDescEMPL = 'Medarb. beskrivelse';
  SExportDescDept = 'Afd. beskrivelse';
  SExportDescPlant = 'Vask. beskrivelse';
  SExportDescShift = 'Skift beskrivelse';
//
  SDummyWK = 'Dette er et dummy arb.sted, kan ikke gemmes';
// CAR 28-8-2003 - Stand Staff availability
   SStdStaffAvailFuturePlanned =
     'Medarb. er allerede planlagt i dette skift. Forts. vil slette fremt. planl. ';
// 9-4-2003
  SPimsInvalidWeekNr = 'Ugyldigt ugenr. !';
// 9-4-2003
  SPimsHoursPerDayProduction = 'Produktion ';
  SPimsHoursPerDaySalary = 'L�n ';
  // MR:12-09-2003
  SPimsCompletedToNotScans = 'Du kan ikke �ndre en f�rdig returskann.'#13'til en ikke-f�rdig.';
// user rights
  SAplErrorLogin = 'Ugyldigt brugernavn eller PW!';
  SAplNotConnect = 'Ugyldig forb. til database!';
  SAplNotVisible = 'Bruger kan ikke �bne dette program';
// MR:3-11-2003 Moved from 'UStrings.pas'
  LSPimsEnterPassword = 'Indt. Password';
// MR:11-11-2003
  SPimsEmployeesAll = 'Alle medarb.';
  SPimsEmployeesOnWrongWorkspot = 'Forkert arb.sted';
  SPimsEmployeesNotScannedIn = 'Ikke indskann.';
  SPimsEmployeesAbsentWithReason = 'Absent with reason';
  SPimsEmployeesAbsentWithoutReason = 'Frav�r med grund';
  SPimsEmployeesWithFirstAid = 'Med f�rstehj.';
  SPimsEmployeesTooLate = 'For sent';
// 550119 - Workspot per Employee form
  SPimsLevel = 'Trin';
// Car: 15-1-2004
  SPimsEmplContractInDatePrevious =
   'En kontrakt indeholder allerede denne dato';
// 550287
// Contract group form
  STruncateHrs = 'Afkorte';
  SRoundHrs = 'Rund';
//550124
  SPimsPieces = 'Stk.';
  SPimsWeight = 'V�gt';
// MR:03-05-2004 Order 550313
  SPimsCleanUpProductionQuantities = 'Fjern produktionsantal �ldre end %d uger?';
  SPimsCleanUpEmployeeHours = 'Fjern medarb. timer �ldre end %d uger?';
  SPimsCleanUpTimerecordingScans = 'Fjern Timerecording Scans �ldre end %d uger?';
// MR:17-05-2004 Order 550315
  SPimsLabelCode = 'Kode';
  SPimsLabelInterfaceCode = 'Interface kode';
// MR:26-07-2004 Order 550327
  SPimsNotPlannedWorkspot = 'Ikke planl.';
// MR:09-08-2004 Order 550336
  SNumber = 'Nummer';
// MR:13-10-2004
  SPimsGeneralSQLError = 'BDE rapp. en generel SQL fejl';
// MR:26-10-2004
  SPimsEfficiencyAt = 'Effektiv p�';
  SPimsEfficiencySince = 'Effektiv siden';
// MR:28-10-2004
  SPimsConnectionLost = 'Fejl: Database forbindelse mistet. Genstart program';
// MR:10-12-2004
  SSHSWeekDatesCross = 'Fejl: Selected Fra/Til datoer krydser hinanden!';
// MR:15-12-2004
  SPimsHours = 'timer';
  SPimsDays = 'dage';
  SPimsBonus = 'bonus';
  SPimsEfficiency = 'effektiv';
// MR:11-01-2005
  SPimsTotalProduction = 'Total produktion';
// MR:11-01-2005
  SPimsProductionOutput = 'Produktion Output';
  SPimsChartHelp = 'V�lg tid: Klik p� graf'; //, flyt: h�jre-klik mellem graf-dele';
  SPimsInvalidScan = 'fejl: Du indtastede ugyldig skan.';
// MR:11-01-2005 -> Pims Oracle!
  SPimsScanOverlap = 'Fejl: Skan har forktert tidsinterval.';
  SPimsMaxSalBalMin = 'Max.Sal.Balance';
// MR:16-06-2005
  SPimsConfirmProcessEmployeeSelectedDate = '�nsker du at bearbejde valgte dato ' + #13 +
    '%s' + #13 + 'til medarb. ledighed ?';
// MR:20-06-2005
  SPimsNoBankHoliday = 'V�lg en helligdag';
// MR:27-09-2005
  SPimsUseJobcodesNotAllowed = 'Der er ikke-kompl. skanninger for dette arb.sted ' + #13 +
    '�ndr. er ikke tilladt.';
// MR:14-11-2005
  SPimsUnknownBusinessUnit = 'Ukendt forr.omr.';
// MR:07-12-2006
  SOvertimeMonth = 'M�ned';
// MR:05-01-2007
  SErrorWrongTimeBlock = 'FEJL: Forkert tidsblok for medarb.';
  SErrorDuringProcess = 'Der var fejl! Se fejl-beskeder herover.';
  SSProcessFinishedWithErrors = 'Proces f�rdiggjort med fejl!';
// MR:26-01-2007
  SPimsMonthGroupEfficiency = 'Mdl. gruppe effektivitet';
  SPimsGroupEfficiency = 'Mdl. effektivitet';
// MR:14-SEP-2009
  SPimsTemplateYearCopyNotAllowed = 'At kopiere dette �r er ikke tilladt!';
  SPimsTemplateYearSelectNotAllowed = 'At v�lge dette �r er ikke tilladt!';
// MRA:8-DEC-2009 RV048.1.
  SPimsEmpPlanPWD = 'At �ndre afdelingen er ikke tilladt, ' + #13 +
    ' da medarb. er planlagt p� dette arb.sted/afdeling kombination.';
// MRA:19-JAN-2010.
  SPimsNrOfRecsProcessed = 'Antal bearb. filer: ';
// MRA:22-FEB-2010.
  SPimsNoShiftScheduleFound = 'Der er ikke defineret skift-skema for benyttede uge.';
// MRA:18-MAY-2010. Order 550478.
  SPimsDeleteMachineWorkspotsNotAllowed = 'Sletning ikke mulig! Der er arb.steder knyttet til denne maskine';    
  SPimsDeleteMachineJobsNotAllowed = 'Sletning ikke mulig! Jobs er defineret til denne maskine.';
  SPimsUpdateMachineCodeNotAllowed = 'Maskine er knyttet til arb.sted(er). �ndring af maskinkoden er derfor ikke tilladt..';
  SPimsUpdateMachineJobCodeNotAllowed = 'Maskin-job bruges af arb.sted-job. Derfor er �ndring af maskin-job kode ikke tilladt.';      
// SO:07-JUN-2010.
  SPimsIncorrectHourType = 'Indtast korrekt time type !!';
  SPimsScanLongerThan24h = 'Skanning m� ikke v�re mere end 24 timer!!';
// MRA:30-JUN-2010. Order 550478.
  SPimsMachineWorkspotNonMatchingJobs = 'ADVARSEL: Der er ikke-matchende jobs for dette arb.sted for valgte maskine..' + #13 +
  'S�rg for jobs mathcer marb.steder der er koblet til indtastede maskine !';
  SPimsPleaseDefineJobForMachine = 'ADVAARSEL: Lav �ndringer p� job koder p� maskin niveau.';
  SPimsDeleteNotAllowedForMachineJob = 'Dette er et maskin job. Sletning ikke tilladt her, kun p� maskin niveau.';
// MRA:24-AUG-2010. Order 550497. RV067.MRA.8.
  SPimsAbsenceReasons = 'frav�r �rsager';
  SPimsHourTypes = 'time typer';
  SPimsAbsenceTypes = 'frav�r typer';
// MRA:31-AUG-2010. RV067.MRA.21
  SPimsDescriptionNotEmpty = 'Indtast beskrivelse, skal indtastes!';
  SPimsCounterNameFilled = 'T�llernavn skal udfyldes hvis t�ller er aktiv!';
// MRA:21-SEP-2010. RV067.MRA.34
  SPimsPaste = 'Paste';
  SPimsPasteExceptHours = 'Inds�t ekstraord. timer fra "%s"';
  SPimsDefExceptHoursFound = 'Definitioner fundet, vil blive slettet. Forts�t Ja / Nej?';
// MRA:4-OCT-2010. RV071.3. 550497
  SPimsCountersHaveBeenUpdated = 'T�llere er blevet opdateret';
  SPimsProcessToAvailIsReady = 'Bearbejd til ledighed er klar.';
  SPimsRecalcHoursConfirm = 'Rekalkulation af timer er indkl. Forts�t ?';
// MRA:11-OCT-2010. RV071.10. 550497
  SPimMaxSatCreditReached = 'Max. L�rdag kredit er n�et!';
// MRA:1-NOV-2010.
  SPimsStartEndDateSmaller = 'Start dato er mindre end slut dato' + #13 +
     'p� eksisterende syge-besked for samme medarb.';
// MRA:8-NOV-2010.
// MRA:1-DEC-2010. RV082.4. Text changed.
  SPimsIsNoScannerWarning =
    'ADVARSEL: Denne medarb. er ikke-skanner og type: ' + #13 +
    'Autom. skans baseret p� standard planl..' + #13 +
    'Skanopdatering kan medf�re ukorrekte timer! ' + #13 +
    '�ndringer ikke tilladt!';
// MRA:17-JAN-2011. RV085.1.
  SPimsEmpPlanPWDFound = 'ADVARSEL: Planl. eskisterer for denne arb.sted/afd.-kombination' + #13 +
    'den %s og/eller senere. Sikker p� du vil �ndre afdelingen?';
// MRA:14-FEB-2011. RV086.4.
  SPimsSelPlantsCountry = 'Valgte vaskerier skal tilh�re samme land!';
  SPimsSelNotPosMultExports = 'Valg ikke muligt, flere eksport typer ikke tilladt.';
// MRA:18-APR-2011. RV089.5
  SPimsAddAllWorkspots = 'Tilf. ALL arb.st?';
// MRA:21-APR-2011. RV090.1.
  SPimsFinishLastTransactionFirst = 'Bekr./fortr. sidste kopiering f�rst!';  
  SPimsConfirmLastTransactionFirst = 'Sidste kopierering endnu ikke bekr�ftet.' + #13 +
                                     'Bekr./fortr. f�rst!' + #13 +
                                     'Ja=Bekr. Nej=Fortr.';
  SPimsConfirmLastTransaction = '�nsker du at BEKR. sidste kopiering?';
  SPimsUndoLastTransaction = '�nsker du at FORTR. sidste kopiering?';
// MRA:26-APR-2011. RV089.1.
  SPimsYes = '&Ja';
  SPimsNo = '&No';
  SPimsAll = '&All';
  SPimsNoToAll = 'N&o To All';
// MRA:27-APR-2011. RV091.1
  SPimsPasteOvertimeDef = 'Inds�t overtidsdef. fra "%s"';
  SPimsOvertimeDefFound = 'Def. fundet, disse vil blive slettet. Forts�t Ja / Nej?';
// MRA:24-MAY-2011. RV092.6
  SPimsEndTimeError = 'Slut tid skal v�re st�rre end start tid!';
// MRA:26-MAY-2011. RV093.1.
  SPimsFooterTotalContractGroup = 'Total Kontr. gr.';
  SPimsFooterContractGroup = 'Kontraktgrp.';
// MRA:1-JUN-2011. RV093.3.
  SPimsLoggingEnable = 'For at sl� logging til/fra genstart Pims.';
// MRA:6-JUN-2011. RV093.3.
  SPimsCleanUpEmpHrsLog = 'Rens medarb. time logning �ldre end %d uger?';
  SPimsCleanUpTRSLog = 'Rens tidsreg. scanningslogning �ldre end %d uger?';
// MRA:6-JUL-2011. RV094.5.
  SPimsFilenameAFAS = 'Salarymutations ABS PIMS';
// MRA:11-JUL-2011. RV094.7.
  SPimsPlantDeleteNotAllowed = 'Ikke tilladt at slette et vaskeri.';
  SPimsPlantUpdateNotAllowed = 'Ikke tilladt at �ndre et vaskeri';
  SPimsEmployeeDeleteNotAllowed = 'Ikke tilladt at slette en medarb.';
  SPimsEmployeeUpdateNotAllowed = 'Ikke tilladt at �ndre en medarb.';
  SPimsWorkspotDeleteNotAllowed = 'Ikke tilladt at slette et arb.sted.';
  SPimsWorkspotUpdateNotAllowed = 'Ikke tilladt at �ndre et arb.sted.';
// MRA:11-JUL-2011. RV04.8.
  SPimsEmployeeNotActive = 'Medarb. ikke-aktiv.';
// MRA:19-OCT-2011. RV099.1.
  SPimsPleaseFillBothAbsReasons = 'Udfyld begge frav�rs �rsager.';
  SPimsLogStartTFTHolCorrection = 'Start TFT/Ferie korrektion.';
  SPimsLogEndTFTHolCorrection = 'Slut TFT/Ferie korrektion.';
  SPimsLogChangesMade = 'Antal �ndringer: %d';
// MRA:28-OCT-2011. RV100.2. 20011796.2. Changed.
  SPimsEarnedTFT = 'TFT optj.';
  SPimsEarnedTFTSign = 'TFT-E';
// MRA:9-JAN-2012. RV103.4.
  SPimsHolidayCardTitle = 'Feriekort';
// MRA:13-JAN-2012. RV103.4.
  SJanuaryS = 'Jan.';
  SFebruaryS = 'Feb.';
  SMarchS = 'Mar';
  SAprilS = 'Apr.';
  SMayS = 'Maj';
  SJuneS = 'Juni';
  SJulyS = 'Juli';
  SAugustS = 'Aug.';
  SSeptemberS = 'Sept.';
  SOctoberS = 'Okt.';
  SNovemberS = 'Nov.';
  SDecemberS = 'Dec.';
// 20011796.2.
  SPimsRepHolCardOpeningBalance = '�bningsbalance:';
  SPimsRepHolCardUsed = 'Brug:';
  SPimsRepHolCardAvailable = 'Ledig:';
  SPimsRepHolCardTFTEarned = 'TFT-E Optj.:';
  SPimsRepHolCardTUsed = 'T Brug:';
  SPimsRepHolCardTotAvail = 'Total Ledig:';
  SPimsRepHolCardLegenda = 'T = Time for Time brugt, TFT-E = Time for Time optj., H = Ferie';
  SPimsRepHolCardLegendaTFTEarned = 'TFT-E = Time for Time optj.';
  SPimsInvalidCheckDigits = 'Ugyldige tegn. Indtast en v�rdi fra 10 til 99.';
  SPimsEnteredDateRemark = 'Indtast ind-dato lig med valgte dato eller n�ste dato.';
  SPimsShifts = 'Shifts'; // 20013551
  SPimsWorkspotHeader = 'Arb.sted'; // 20013551
  // 20013723
  SPimsExportPeriod = 'Eksport overtid';
  SPimsExportMonth = 'Eksport l�n';
  // 20013288.130
  SPimsExportEasyLaborChoice = 'V�lg hvad der skal eksporteres.';
  // 20014002
  SPimsTotalWorkspot = 'Total Arb.sted';
  // 20014037.60
  SPimsPimsUser = 'Pims User';
  SPimsWorkstation = 'Arb.station';
  // TD-22429
  SPimsStart = 'Start';
  // TD-22475
  SPimsPasteDatacolConnection = 'Indl�s datacol connections fra "%s"';
  SPimsDatacolConnectionFound = 'DatacolConnections fundet, vil blive slettet. Forts�t Ja / Nej?';   
  // TD-22503
  SChangeEmplAvail = 'Medarb. ledighed fundet, disse vil blive slettet for ikke-ledige. Vil du forts�tte?';
  // SO-20014715
  SPimsReportYes = 'Ja';
  SPimsReportNo = 'No';
  SPimsReportTill = 'til';
  SPimsReportLate = 'sen';
  SPimsReportEarly = 'tidl.';
  // TD-23315 Changed this message:
  SPIMSTimeIntervalError = 'Start tid er mindre end tidl. slut tid,' + #13 +
   'eller sluttid er st�rre end n�ste starttid.';
  // 20013196
  SPimsDeleteLinkJob = 'Link job er defineret. Dette vil blive slettet. Forts�t Ja/Nej ?';
  SPimsLinkJobExist = 'Det er ikke tilladt at def. mere end 1 link job pr. arb.sted' + #13 +
    'Dette linkjob bliver ikke gemt.';
  // 20011800
  SPimsOK = '&Ok';
  SPimsFinalRun = '&Final Run';
  SPimsFinalRunWarning = 'FINAL RUN, ER DU SIKKER?' + #13#13 +
    'Mutations til denne periode kan ikke laves efterf�lgende.';			
  SPimsFinalRunExportDateWarning = 'FINAL RUN' + #13#13 +
    'Valgte periode er allerede eksporteret (helt eller delvist).' + #13#13 +
    'IKKE tilladt at lave denne eksport.';
  SPimsFinalRunExportDateWarningAdmin = 'FINAL RUN' + #13#13 +
    'Valgte periode er allerede eksporteret (helt eller delvist).' + #13#13 +
    'Vil du starte eksport?';
  SPimsTestRunExportDateWarning = 'TEST RUN' + #13#13 +
    'Valgte periode er allerede eksporteret (helt eller delvist).' + #13#13 +
    'Vil du starte eksport?';
  SPimsFinalRunExportGapWarning = 'FINAL RUN' + #13#13 +
    'There is a gap of %d days between last-export-date' + #13 +
    'and start-date of selection.' + #13#13 +
    'It is NOT allowed to make this export.';
  SPimsFinalRunExportGapWarningAdmin = 'FINAL RUN' + #13#13 +
    'There is a gap of %d days between last-export-date' + #13 +
    'and start-date of selection.' + #13#13 +
    'Do you want to start the export?';
  SPimsTestRunExportGapWarning = 'TEST RUN' + #13#13 +
    'Der er en diff. of %d dage mellem sidste eksp.dato' + #13 +
    'og valgte startdato.' + #13#13 +
    'Vil du starte eksport?';
  SPimsCloseScansSkipped = 'ADVARSEL: Nogle skanninger kunne ikke lukkes pga. sammenfald' + #13 +
    'med sidst eksp.dato.';
  SPimsFinalRunAddAvaililityNotAllowed = 'Tilf�jelse af ledighed for denne uge ikke till.' + #13 +
    'da den allerede er eksporteret.';
  SPimsFinalRunDateChange = 'ADVARSE: Datoen er �ndret til en dato efter sidste eksportdato.';
  SPimsFinalRunAvailabilityChange = 'ADVARSEL: Nogen medarb. ledighed kunne ikke �ndres pga sidste eksportdato.';
  SPimsFinalRunTestRun = 'TEST RUN';
  SPimsFinalRunDeleteNotAllowed = 'ADVARSEL: Sletning ikke tilladt, den er allerede eksporteret.';
  SPimsFinalRunOnlyPartDelete = 'ADVARSEL: Kun en del af ugen vil blive slettet, da dele er eksporteret.';
  SPimsFinalRunShiftSchedule = 'ADVARSEL: Ikke alle kunne kopieres da dele allerede var eksporteret.';
  // SO-20014442
  SPimsYearMonth = '�r/M�ned';
  SPimsStartEndYearMonth = 'Slut �r/m�ned skal v�re st�rre end start �r/m�ned!';
  SPimsOnlySANAEmps = 'Mun SANA medarb.';
  SPimWrongYearSelection = 'Forkert �r-valg: Den Fra til �r skal v�re den samme!';
  // 20015178
  SPimsPleaseEnterFakeEmpNr = 'Angiv et unikt Fake medarbejdernummer,' + #13 +
    'der vil blive anvendt i Personal Screen.';
  SPimsFakeEmpNrInUse = 'Den indtastede Fake Medarbejder nummer er allerede i brug af en anden' + #13 +
    'arb.sted. Indtast et andet nummer.';
  // 20015223
  SPimsWorkspots = 'Arb.sted';
  SPimsPlantTitle = 'Vaskeri';
  SPimsAllWorkspots = '<Alle>';
  SPimsMultipleWorkspots = '<Multiple>';
  // 20013476
  SPimsValueSampleTimeMins = 'Sample tid: Ugyldig v�rdi indtastet. Indtast venligst en v�rdi fra 1 til 30 minutter.';
  // 20015586
  SPimsIncludeDowntime = 'Medtag nedetid m�ngder:';
  SPimsIncludeDowntimeYes = 'Ja';
  SPimsIncludeDowntimeNo = 'Ingen';
  SPimsIncludeDowntimeOnly = 'Vis kun ned-tid';
  // TD-25948
  SPimsOpenCSV = '�bn CSV-fil?';
  SPimsCannotOpenCSV = 'Du skal installere Microsoft Excel (eller et lignende program).';
  // PIM-53
  SPimsNoWorkingDays = 'V�lg en eller flere arbejdsdage.';
  SPimsAddHoliday = 'Har du �nsker at tilf�je %d frav�r dag(e) til medarbejderen tilg�ngelighed?';
  // PIM-53
  SPimsAddHolidayEmplAvail = 'Angiv en gyldig frav�r �rsagskode.';
  SPimsAddHolidayNoStandAvail = 'Handling er afbrudt: Der er ingen standard tilg�ngelighed defineret for Vaskeri/Skift-kombination!';
  // PIM-52
  SPimsWorkScheduleDetailsExists = 'Tidsplanen Detaljer findes allerede! Overskriv?';
  SPimsWorkScheduleWrongRepeats = 'Forkert v�rdi for Gentager om ugen! Indtast venligst en v�rdi mellem 1 og 26.';
  SPimsWorkScheduleInUse = 'Arbejde tidsplan er stadig i brug (Medarbejder Kontrakt), og kan ikke slettes!';
  SPimsInvalidShift = 'Indtast en gyldig skift.';
  SPimsReady = 'F�rdig';
  SPimsPlanningMessage1 = 'Ikke behandlet! For medarbejder';
  SPimsPlanningMessage2 = 'og dato';
  SPimsPlanningMessage3 = 'der findes planl�gning.';
  SPimsNoStandAvailFound1 = 'Fejl: Ingen standard tilg�ngelighed fundet for medarbejder';
  SPimsNoStandAvailFound2 = 'og skift';
  SPimsProcessWorkScheduleMessage1 = 'Proces arbejdsplan for medarbejder';
  SPimsProcessWorkScheduleMessage2 = 'og referencedato';
  // PIM-23
  SPimsTotals = 'Totaler';
  SPimsOTDay = 'Dag';
  SPimsOTWeek = 'Uge';
  SPimsOTMonth = 'M�ned';
  SPimsMO = 'MA';
  SPimsTU = 'TI';
  SPimsWE = 'ON';
  SPimsTH = 'TO';
  SPimsFR = 'FR';
  SPimsSA = 'L�';
  SPimsSU = 'S�';
  SPimsNormalHrs = 'Normale timer per dag:';
  SPimsOTPeriod = 'Periode';
  SPimsOTPeriodStarts = 'Starter i uge';
  SPimsOTWeeksInPeriod = 'Uger i perioden:';
  SPimsOTWorkingDays = 'Arbejdsdage:';
  SPimsCGRound = 'Afrunding';
  SPimsCGTRunc = 'Afkorte';
  SPimsCGMinute = 'minutter';
  SPimsCGTo = 'til';
  SPimsStandStaffAvailUsed = 'BEM�RK: For en eller flere dage en fast frav�r grund bruges,'#13' baseret p� standard personale tilg�ngelighed.'#13'Dette er ikke overskrives';
  SPimsShiftDate = 'Skift Dato';
  // ABS-27052
  SPimsSelectPrinter = 'Udv�lgelse Printer';
  SPimsSelectDefaultPrinter = 'V�lg en printer som standardprinter';
  SPimsNoPrinterFound = 'Ingen printere fundet!';
  SPimsPleaseSelectPrinter = 'V�lg printer.';
  // PIM-52
  SPimsNoCurrentEmpContFound = 'Ingen nuv�rende kontrakt fundet for medarbejderen';
  // PIM-203
  SPimsHostPortExists = 'Denne V�rt+Port-kombination findes allerede for anden arbejdssted.';
  // PIM-174
  SPimsCopied = 'Kopieret:';
  SPimsRows = 'r�kker.';
  SPimsEmp = 'Medarbejder';
  SPimsEmpContract = 'Medarbejderkontrakt';
  SPimsWSPerEmp = 'Arbejdsplads per medarbejder';
  SPimsStandStaffAvail = 'Nom. personaletilg�ngelighed';
  SPimsShiftSched = 'Skiftskema';
  SPimsEmpAvail = 'Medarbejdertilg�ngelighed';
  SPimsStandEmpPlan = 'Personale Nom. Planl�gning';
  SPimsEmpPlan = 'Personale Planl�gning';
  SPimsAbsenceCard = 'Frav�rskort';
  SPimsRecordKeyDeleted = 'Denne post er slettet af en anden bruger.';
  SPimsWorkScheduleWrongStartDate = 'Reference uge skal v�re en dato, der starter p� den f�rste dag i ugen.'#13'Dette er blevet korrigeret.';
  SPimsRoamingWorkspotExists = 'Der er allerede en anden workspot defineret som roaming.';
  SPimsHourlyWage = 'Timel�n:';
  SPimsTotalExtraPayments = 'Total ekstra betalinger';
  SPimsTBPEmp = 'Timeblocks per Employee';

{$ELSE}
  {$IFDEF PIMSJAPANESE}

  SPimsHomePage = 'Pims Homepage will be implemented soon!';
  SPimsIncorrectPassword = 'Incorrect Password.'#13'Enter another password or '+
    'Cancel.';
  SPimsNoEmptyPasswordAllowed = 'An empty password is not allowed.'#13'Enter '+
    'another password or Cancel.';
  SPimsPasswordChanged = 'New password saved.';
  SPimsPasswordNotChanged = 'Password not changed.';
  SPimsPasswordNotConfirmed = 'Password and confirmation are '+
    'different.'#13'Enter another password or Cancel.';
  SPimsSaveChanges = 'Save changes?';
  SPimsRecordLocked = 'This record is in use by another user';
  SPimsOpenXLS = 'Open Excel file?';
  SPimsOpenHTML = 'Open HTML file?';
  SPimsNotUnique = 'Not Unique';
  SPimsNotFound = 'Nothing found!';
  SPimsNotEnoughStock = 'There are not enough products in stock!'#13'Issue the '+
    'items anyway?';
  SPimsNoItems = 'No items yet';
  SPimsNoEmployee = 'There are no employee!';
  SPimsNoCustomer = 'There are no customers!';
  SPimsLoading = 'Loading ';
  SPimsKeyViolation = 'This line already exists.';
  SPimsForeignKeyPost = 'This record has a connection to other records. Your '+
    'request is denied.';
  SPimsForeignKeyDel = 'This record has a connection to other records. Your '+
    'request is denied.';
  SPimsFieldRequiredFmt = 'Please fill required field %s.';
  SPimsFieldRequired = 'Please fill all required fields.';
  SPimsDragColumnHeader = 'Drag a column header here to group that column.';
  SPimsDetailsExist = 'This record has a connection to other records. Please '+
    'remove those records first.';
  SPimsDBUpdate = 'Update Pims database';
  SPimsDBUpdateButtonCaption = '&Run Pims update';
  SPimsDBUpdateConflict = 'Pims detected a conflict while updating the '+
    'database.'#13'Please call the ABS helpdesk and refer to this message.';
  SPimsDBNeedsUpdating = ' Pims detected on older database version.'#13+
   ' Pims needs to update your database to run properly.';
  SPimsCurrentCode = 'Current item code. ';
  SPimsContinue = ' Continue?';
  SPimsConfirmCancelNote = 'Do you want to cancel this delivery note?';
  SPimsConfirmDeleteNote = 'Do you want to delete this delivery note?';
  SPimsConfirmDeleteLicense = 'Do you want to delete this Pims License?';
  SPimsConfirmDelete = 'Do you want to delete the current record?';
  SPimsCannotPrintLabel = 'You cannot print a label for the currently selected '+
    'item.';
  SPimsCannotPlaceFlag = 'You cannot place a flag on the currently selected '+
    'item.';
  SPimsCannotOpenXLS = 'Please install Microsoft Excel.';
  SPimsCannotOpenHTML = 'Please install an Internet Browser on your machine.';

  // FOR PIMS:
  SSelectDummyWK = 'You can not select a dummy workspot!';
  SPimsPercentageNotCorrect = 'Error: Total percentage for workspot is not 100%';
  SPimsTimeMustBeYounger = 'Error: Start-time must be less than End-time!';
  SPimsNoMaster = 'There are no Process Units';
  SPimsNoSubDetail = 'There are no Departments';
  SPimsNoDetail = 'There are no Workspots';
  SPIMSContractTypePermanent = 'Permanent';
  SPIMSContractTypeTemporary = 'Temporary';
  SPIMSContractTypePartTime = 'External';
  SPIMSContractOverlap = 'There is an overlap for this contract with last contract';
  SPimsTBDeleteLastRecord = 'Only the last record can be deleted';
  SPimsTBInsertRecord = 'Only %d timeblocks allowed';
  SPimsStartEndDate = 'Start date is bigger than end date';
  SPimsStartEndTime = 'Start time is bigger than end time';
  SPimsStartEndDateTime = 'Start date time is bigger than end date time';
  SPimsStartEndActiveDate = 'Start active date is bigger than end active date';
  SPimsStartEndInActiveDate = 'Start inactive date is bigger than end inactive date';
  SPimsWeekStartEnd = 'End week < Start week !';
  SPimsBUPerWorkspotPercentages = 'Total of percentages should be 100,  ' +
    'you can not leave the form';
  SPimsTBStartTime = 'Starttime TimeBlock < Starttime Shift on ';
  SPimsTBEndTime = 'Endtime TimeBlock > Endtime Shift on ';
  SPimsBUPerWorkspotInsert = 'There is no workspot you can not insert records';
  SPimsDateinPreviousPeriod =  'There already is a scan that includes this time';
  SPimsExistJobCodes = 'You cannot change it, there are jobcodes defined';
  SPimsExistBUWorkspot = 'You cannot change it, there are relateted records in Business Units per Workspots';
  // Time Recording
  SPimsIDCardNotFound = 'ID Card not found';
  SPimsIDCardExpired	= 'ID Card has expired';
  SPimsEmployeeNotFound = 'Employee not found';
  SPimsInactiveEmployee = 'Employee is inactive';
  SPimsScanningsInMinute = 'Not allow multiple scannings within one minute';
  SPimsStartDay = 'Start of day';
  SpimsEndDay = 'End of day';
  SPimsNoWorkspot = 'No Workspot defined on current workstation';
//Workspot
  SPimsNoJob = 'No Jobs defined on current Workspot';
  SPimsDeleteJob = 'There are jobcodes assigned, they will be deleted!';
  SPimsDeleteBusinessUnit = 'There are business units assigned, they will be deleted!';
  SPimsDeleteJobBU =
    'All connected records with this workspot will be deleted, delete workspot ? ';
  SShowBUWK         = 'Show business units per workspot';
  SShowJobCode = 'Show job codes';
//workspot per workstation
  SPimsSameSourceAndDest = 'The source and destination computer name could ' +
   	'not be the same';
  // Selector Form - Grid Column name
  SPimsColumnPlant 	= 'Plant';
  SPimsColumnWorkspot = 'Workspot';
  SPImsColumnCode = 'Code';
  SPimsColumnDescription = 'Description';
  // Selector Form - Captions
  SPimsWorkspot = 'Select Workspot';
  SPimsJob 	= 'Select Job';
  SPimsJobCodeWorkSpot = 'You can not close form, first define at least one jobcode';
  SPimsInterfaceCode  =
   'You can not close form, first define at least one interface code other than the interface code for the job';
  SPimsOtherInterfaceCode  = 'Please define other interface code than the interface code of the job code';
  SPimsNoOtherInterfaceCodes = 'There are no other interface codes defined, you can not open form';

  SPimsSaveBefore = 'Please save the record before';
  SPimsNoPlant = 'There are no plants';
  SPimsScanDone = 'Scanning processed';
  SPimsScanError = 'Scanning NOT processed: Data-Base ERROR';
//  SPimsNoEmployee = 'There are no employees';
// Time Recording Scanning
  SPimsNoShift = 'No Shift defined';
  SPimsNoCompletedScans = 'Completed scans cannot be modify / deleted';
  SPimsNonScanningStation = 'This is not a time recording station';
  SPimsScanTooBig = 'Scan period longer than 12 hours is not allowed';
  SPimsUseJobCode = 'Please select a job code, workspot it is defined to use jobcodes';
  SPimsNoMatch = 'No match for Employee + ID-Card';
// team per department - extra check
  SPimsNoMorePlantsinTeam =
    'All department of one team should belong to the same plant !';
  SPimsTeamPerEmployee =
    'Department of the employee should be in the team !';

// Hours per Employee
  SPimsAutomaticRecord = 'Automatic inserted record cannot be changed ';
  SPimsNegativeTime = 'Double click to set a negative time (Red color)';
  SPimsExistingRecord = 'A record with the same parameters already exist.'+#13+
  	'Please select that record';
  SPimsProductionHour = 'ProductionHourPerEmployee table cannot updated';
  SPimsNoTimeValue = 'Please insert time value for at least one day';
  SPimsSalaryHour = 'SalaryHourPerEmployee table cannot updated';
  SPimsAbsenceHour = 'AbsenceHourPerEmployee table cannot updated';
  SPimsTo = 'To';
  SPimsInvalidTime = 'Value not set! (Invalid time value)';
// bank holiday process
  SPimsConfirmProcessEmployee = 'Do you want to process all bank holidays of ' +
    ' the year to employee availability ?';
  SPimsInvalidAbsReason = 'Not a valid absence reason!';
  SPimsEmptyAbsReason = 'Please fill required field absence reason!';
  SPimsEmptyDescription = 'Please fill required field description!';
// for reports
  SPimsReportWTR_HOL_ILL_Checks = 'You should select at least one absence reason!';
  SPimsMaxNumberOfWeeks = 'Maximal number of selected weeks is 13!';
  SPimsEndTime = 'End time';
  SPimsStartTime = ' < Start time';
  SPimsOn = 'on' ;
// update database
  SPimsInvalidVersion = 'Invalid PIMS version !';
  SUpdateDatabaseFrom = 'Database will be updated from version ';
  SUpdateDatabaseTo = ' to version ';
// Standard staff availability
  SCheckAvailableTB = 'Permitted values are * and -';
  SCheckAvailableTBNotEmpty = 'Please fill all required fields !';
  SCopySTA1 = 'Standard availability already exists for day ';
  SCopySTA2 = ', overwrite ? ';
  SCopySameValues = 'You selected the same record !';
  SCopySTANotValidFrom = 'There are no valid timeblocks for selected record!';
  SCopySTANotValidTo = 'There are no valid timeblocks for selected record of grid!';
  SEmptyValues = 'Please fill all fields before!';
  SEmplSTARecords = ' There are no standard availabilities defined for this employee ';
// shift schedule
  SShiftSchedule = 'Permitted values are numbers 0 .. 99 or -';
  SSHSShift = 'Permitted values are only shift defined';
  SSHSWeekCopy = 'Start date of copy should be bigger than end date';
  SInvalidShiftSchedule = 'Shift number has not defined or values start/end' +
    ' time per day are zero!';
  SEmployeeAvail_0  = 'Employee ';
  SEmployeeAvail_1  = ' already has availability in this shift on year, week, day ';
//  SEmployeeAvail_2 = ', day ';
  SEmployeeAvail_3 = ', you can not change the shift number.';
  SUndoChanges = 'Last changes will not be saved!';
  SStartWeek = 'Start week should be bigger than end week!';
  SCopySelectionShiftSchedule_1 = 'Shift schedule for employee ';
  SCopySelectionShiftSchedule_2 = ' already exists. Overwrite ?';
  SCopyFinished = 'Copy is finished!';
  SEmptyRecord = 'There is no employee selected!';
// employee availability
  SEmplAvail = 'Permitted values are *, - or an absence reason';
  SValidTimeBlock = 'All valid timeblocks should be filled!';
  SEmployeePlanned = 'Employee is already planned, continuing will delete planning. ' +
    'Do you want to continue ?';
  SCopyFrom  = 'Please select a record!';
  SCopyTo  = 'Records are the same, please select a different record!';
  SEMAOverwrite = 'Employee availability for ';
  SEMAShift = ' shift ';
  SEMAExists = 'already exists. Do you want to overwrite?';
// staff planning
  SSelectedWorkspots = 'There are more than 401 selected workspots, only the first 401  will ' +
    ' be displayed ';
  SEmptyWK = 'There are no workspots selected';
  SOverplanning = 'Overplanning more than 200%';
  SMakeAvailable = 'Make available';
  SNotAcceptPlanned = 'This planning of employee is not accepted!';
  SSaveChanges = 'Save changes?';
  SReadStdPlnIntoDaySelection = 'Planning already read for a plant/team/date within the selection';
  SReadStdPlnIntoDaySelectionFinish = 'Read standard planning into day planning finished';
  SReadPastIntoDayPlanFinish = 'Read past into day planning finished';
  SContinueYN = 'Do you want to continue ?';
  SDateFromTo = 'End date should be bigger than start date!';
  SDateIntervalValidate = ' Interval of target date should be different than interval' +
    ' of source date';
  SNoEmployee = 'There are no employees selected!';
  SOCILevel = 'Occupation level ';
  SPlannedLevel = 'Planned with level ';
  SPlannedAllLevels = 'Planned ';
  SOCIAllLevel = 'Occupation  ';
  SSameRangeDate = 'Date intervals should have the same length!';
// report staff planning
  SStartEndYear = 'End year should be bigger than start year!';
  SWeek = 'You can not select more than one week!';
  SNoTeam = 'Empty team: ';
// Workspot per employee
  SLEVELWK = 'Only A, B, C are allowed!';
  SInvalidWK = 'Only A..Z, a..z, 0..9, and ''_'' are allowed !';
// Staff Availability
  SChangePlaned = 'Employee is already planned, continuing will delete planning'+
    #13+'Do you want to continue?';
  SInvalidShiftNo = 'Invalid Shift number for day: %d';
  SInvalidAvailability = 'Invalid Availability value for day: %d, TB: %d';
//  E&R
  SMoreThan = ' More than ';
  SInvalidMistakeValue =  'Number of mistakes should be bigger than zero !';
  SNotDeleteRecord = ' You can not delete this record !';
  SPimsDBError = 'Report openned by other station or Database error. '+#13+
		'Please run report again';
 //
// productivity reports

  SDaySelect = 'Start day should be less then end day!';
//
// jobcode
  SInterfaceCode = 'This interface code is already used for other workspots!';
  SWKInterfaceCode = 'This interface code is already used for this workspot!';
  SWKRescanInterfaceCode =
   'There are interface codes used more than once for this workspot!';
  SDeleteOtherInterfaceCode = 'All other interface codes defined will be deleted!';

//
  SExceptionDataSetReport =
   'Data set is empty cannot create report, please set it first and rebuild pims';
  SExceptionDataSetForms  =
   'Data set is empty cannot refresh grids, please set it first and rebuild pims';
// export payroll report
   SPeriodReport = 'Period';
   SMonthReport = 'Month';
// report quality incentive
   SCalculationwithOvertime = 'Quality incentive calculation with overtime report';
// add wk into datacollection
  SADDWKDateCollection = 'Records are added';
// strings for mistake per employee
  SDate = 'Date ';
  STotalWeek = 'Total week ';
// report hrs per empl
  SCaptionProdRepHrs = 'Production hours per employee';
  SCaptionSalRepHrs =  'Salary hours per employee';
//report CompHours
  SShowActiveEmpl = 'Show active employee';
  SShowNotActiveEmpl = 'Show not active employee';
//report HrsWKCum
  SRepHrsCUMWK = 'Workspot';
  SRepHrsTotalCUM = 'Total workspot';

  SRepHrsCUMWKBU = 'Business unit';
  SRepHrsTotalCUMBU = 'Total business unit';

  SRepHrsCUMWKPlant = 'Plant';
  SRepHrsTotalCUMPlant = 'Total plant';

  SRepHrsCUMWKDept = 'Department';
  SRepHrsTotalCUMDept = 'Total department';
  SRepJobCode = 'Job code';
  SRepTotJobCode = 'Total job code';
// report absence
  SHeaderAbsRsn = 'Absence reason';
  SHeaderWeekAbsRsn = 'Week';
  SHdTotalWeek = 'Total week';
  SRepExpPayroll = 'From';
// report absence schedule
  SReport = 'Report ';
  SReportWorkTimeReduction = 'work time reduction ';
  SReportIllness = 'illness';
  SReportHoliday = 'holiday';
  SActiveEmpl =  'Active employees';
  SInactiveEmpl = 'Inactive employees';
// report
  SRepHrsPerEmplActiveEmpl = 'Show active employee';
  SRepHrsPerEmplInActiveEmpl = 'Show not active employee';
  SRepProdHrs = 'Productive hours';
  SRepNonProdHrs = 'Non-productive hours';
  SAll = 'All';
  SActive = 'Active';
  SInactive = 'Inactive';
  SName = 'Name ';
  SShortName = 'Short name';
// REPORT check list
  SShowProcessed =  'Show processed';
  SShowNotProcessed  = 'Show not processed';
//Report
  SErrorOpenRep = 'Error open data base, report can not be opened';
  SErrorCreateRep = 'Error report can not be created';
//DialogProcessAbsenceHrsFRM
   SLogEmpNo = 'Found Number of Records=';
   SLogChangedAbsenceTotal = 'Changed %d Absence Total-records';
   SLogDeletedRec = 'Deleted %d Absence Hour-records';
   SLogEndAbsence = 'End: Defining Absence Hours Per Employee';
   SLogStartSalaryHours = 'Start: Defining Salary Hours per Employee';
   SLogStartAvailability  = 'Start: Defining Employee Availability';
   SLogChangedAbsence = 'Changed %d Absence Hours-records';
   SlogAddedAbsence = 'Added %d Absence Hours-records';
   SLogEndAvailability = 'End: Defining Employee Availability';
   SLogStartNonScanning = 'Start: Defining Non-Scanning Employee Availability';
   SLogEndNonScanning  = 'End: Defining Non-Scanning Employee Availability';
   SLogFoundEmployeeNo = 'Found Number of Records = %d';
   SSProcessFinished = 'Process finished';
// Staffplanning
   SStaffPlnEmployee = 'Employee';
   SStaffPlnName = 'name';
// Contract group form
   SOvertimeDay = 'Day';
   SOvertimeWeek = 'Week';
   SValidMinutes = 'Minutes defined should be less than 59';
   SQuaranteedHourType = 'Please select an hour type';
//
//update cascade
   SConfirmUpdateCascadeEmpl =
     'Employee will be changed for all connected records, update employee ? ';
   SConfirmDeleteCascadeEmpl =
     'All connected records with this employee will be deleted also, delete employee ? ';
   SEmpExist = 'This employee already exist ';
//export payroll MRA:RV095.4. Give better message: 'date' added.
   SNotDefaultSelection = 'Not the default date-selection. Are you sure ?';
   SPeriodAlreadyExported = 'This period is already exported. Are you sure ?';
// transfer free time
   SPimsDifferentYears = 'Target year should be different than source year!';
   SPimsCheckBox = 'At least one of check boxes must be selected!';
   SPimsProcessFinish = 'Process has finished!';

   // MR:17-01-2003
   SPimsUseShift = 'Please select a shift!';
   SPimsUseWorkspot = 'Please select a workspot!';
//  staff planning -CAR 30-01-2003
   SPimsDayPlanning = 'Day planning: ';
   SPimsTeamSelection = 'Team selection: ';
   SPimsTeamSelectionAll = 'All teams';
   SPimsSTDPlanning = 'Standard planning: ';
   SPimsStaffTo =  ' to ';
   SPimsPlant =  '      Plant:  ';
   SPimsShift = '      Shift: ';
   SPimsTeam = 'Team ';
   // MR:28-01-2003
   SPimsNoEmployeeLine = 'No employee';

   SPimsNoPrevScanningFound = 'No previous scanning found';
   SpimsScanExists = 'There''s already a scanning-record on that day for that employee. ' + #13 +
     'You cannot change this item.';

 //car: 27.02.2003
   SPimsEXCheckIllMsgClose =
     'There already is one outstanding illness message, please close it before';
   SPimsEXCheckIllMsgStartDate =
     'Start date should be bigger than the end date of last created illness message ';

   // MR:03-03-2003
   SPimsContract = 'Contract';
   SPimsPlanned = 'Planned';
   SPimsAvailable = 'Avail.';

   // MR:07-03-2003
   SpimsYear = 'Year';
   SPimsWeek = 'Week';
   SPimsDate = 'Date';
   //CAR:21-03-2003 - REPORT staff planning per day
   SPimsNewPageWK = 'New page per workspot';
   SPimsNewPageEmpl = 'New page per employee';
   //CAR:21-03-2003 report checklist
   SPimsAvailability = 'Availability: ';
   //CAR:21-03-2003 update cascade -Plant
   SConfirmDeleteCascadePlant =
     'All connected records with this plant will be deleted, delete plant ? ';
   SConfirmUpdateCascadePlant =
     'Plant will be changed for all connected records, update plant ? ';
   SPlantExist = 'This plant already exists ';
   //CAR: 27-03-2003
   SRepTeamTotEmpl ='Total employee';
   //CAR 29-03-2003
   SRepHRSPerEmplCorrection = 'Manual corrections';
   SRepHrsPerEmplTotal = 'Total';
   //CAR 31-03-2003  - Report AbsenceCard
   SJanuary = 'January';
   SFebruary = 'February';
   SMarch = 'March';
   SApril = 'April';
   SMay = 'May';
   SJune = 'June';
   SJuly = 'July';
   SAugust = 'August';
   SSeptember = 'September';
   SOctober = 'October';
   SNovember = 'November';
   SDecember = 'December';
//CAR  2-04-2003 - report Production Detail
   SRepProdDetPerformance = 'Performance';
   SRepProdDetBonus = 'Bonus';
// CAR 2-4-2003 - Stand Staff availability
   SStdStaffAvailPlanned =
     'Employee is planned in standard planning, continuing will delete planning. ' +
     'Do you want to continue ?';
//CAR 7-4-2003
   SPimsJobs  = 'Jobcode';
//CAR 15-4-2003
  SPimsStartEndDateTimeEQ = 'Start date time is bigger or equal with end date time';
  SPimsStartEndTimeNull = 'Start time or end time are empty, do you want to continue ?';
// MR:24-04-2003
  SPimsNoData = 'No Data';
//CAR 5-5-2003
  SPimsAbsence = 'Absence';
  SPimsDifference = 'Difference';
// car 12-5-2003
  SExportDescEMPL = 'Employee description';
  SExportDescDept = 'Dept description';
  SExportDescPlant = 'Plant description';
  SExportDescShift = 'Shift description';
//
  SDummyWK = 'This is a dummy workspot it can not be saved';
// CAR 28-8-2003 - Stand Staff availability
   SStdStaffAvailFuturePlanned =
     'Employee is already planned in this shift. Continue will delete future planning. ';
// 9-4-2003
  SPimsInvalidWeekNr = 'Invalid week number!';
// 9-4-2003
  SPimsHoursPerDayProduction = 'Production ';
  SPimsHoursPerDaySalary = 'Salary ';
  // MR:12-09-2003
  SPimsCompletedToNotScans = 'You cannot change a completed scan back '#13'to a not-completed one.';
// user rights
  SAplErrorLogin = 'Invalid user name or password!';
  SAplNotConnect = 'Invalid connection to database!';
  SAplNotVisible = 'This user can not open the application';
// MR:3-11-2003 Moved from 'UStrings.pas'
  LSPimsEnterPassword = 'Enter Password';
// MR:11-11-2003
  SPimsEmployeesAll = 'All employees';
  SPimsEmployeesOnWrongWorkspot = 'On wrong workspot';
  SPimsEmployeesNotScannedIn = 'Not scanned in';
  SPimsEmployeesAbsentWithReason = 'Absent with reason';
  SPimsEmployeesAbsentWithoutReason = 'Absent without reason';
  SPimsEmployeesWithFirstAid = 'With first aid';
  SPimsEmployeesTooLate = 'Too late';
// 550119 - Workspot per Employee form
  SPimsLevel = 'Level';
// Car: 15-1-2004
  SPimsEmplContractInDatePrevious =
   'There already is a contract that includes this date';
// 550287
// Contract group form
  STruncateHrs = 'Truncate';
  SRoundHrs = 'Round';
//550124
  SPimsPieces = 'Pieces';
  SPimsWeight = 'Weights';
// MR:03-05-2004 Order 550313
  SPimsCleanUpProductionQuantities = 'Cleanup Production Quantities older than %d weeks?';
  SPimsCleanUpEmployeeHours = 'Cleanup Employee Hours older than %d weeks?';
  SPimsCleanUpTimerecordingScans = 'Cleanup Timerecording Scans older than %d weeks?';
// MR:17-05-2004 Order 550315
  SPimsLabelCode = 'Code';
  SPimsLabelInterfaceCode = 'Interface Code';
// MR:26-07-2004 Order 550327
  SPimsNotPlannedWorkspot = 'Not Planned';
// MR:09-08-2004 Order 550336
  SNumber = 'Number';
// MR:13-10-2004
  SPimsGeneralSQLError = 'BDE reported a general SQL Error';
// MR:26-10-2004
  SPimsEfficiencyAt = 'Efficiency at';
  SPimsEfficiencySince = 'Efficiency since';
// MR:28-10-2004
  SPimsConnectionLost = 'Error: Database connection lost. Please restart application';
// MR:10-12-2004
  SSHSWeekDatesCross = 'Error: Selected From/To-dates will cross each other!';
// MR:15-12-2004
  SPimsHours = 'hours';
  SPimsDays = 'days';
  SPimsBonus = 'bonus';
  SPimsEfficiency = 'efficiency';
// MR:11-01-2005
  SPimsTotalProduction = 'Total production';
// MR:11-01-2005
  SPimsProductionOutput = 'Production Output';
  SPimsChartHelp = 'select time: click on graph'; //, move: right-click between graph-items';
  SPimsInvalidScan = 'Error: You entered an invalid scan.';
// MR:11-01-2005 -> Pims Oracle!
  SPimsScanOverlap = 'Error: Scan has wrong time interval.';
  SPimsMaxSalBalMin = 'Max.Sal.Balance';
// MR:16-06-2005
  SPimsConfirmProcessEmployeeSelectedDate = 'Do you want to process the selected date ' + #13 +
    '%s' + #13 + 'to employee availability ?';
// MR:20-06-2005
  SPimsNoBankHoliday = 'Please select a bank holiday date';
// MR:27-09-2005
  SPimsUseJobcodesNotAllowed = 'There are not-completed scans for this workspot. ' + #13 +
    'Change is not allowed.';
// MR:14-11-2005
  SPimsUnknownBusinessUnit = 'Unknown Business Unit';
// MR:07-12-2006
  SOvertimeMonth = 'Month';
// MR:05-01-2007
  SErrorWrongTimeBlock = 'ERROR: Wrong timeblock for employee';
  SErrorDuringProcess = 'There were errors! Please see error-messages above.';
  SSProcessFinishedWithErrors = 'Process finished with errors!';
// MR:26-01-2007
  SPimsMonthGroupEfficiency = 'Monthly Group Efficiency';
  SPimsGroupEfficiency = 'Monthly Efficiency';
// MR:14-SEP-2009
  SPimsTemplateYearCopyNotAllowed = 'It is not allowed to copy to this year!';
  SPimsTemplateYearSelectNotAllowed = 'It is not allowed to select this year!';
// MRA:8-DEC-2009 RV048.1.
  SPimsEmpPlanPWD = 'It is not allowed to change the department, ' + #13 +
    ' because employees are planned on this workspot/department combination.';
// MRA:19-JAN-2010.
  SPimsNrOfRecsProcessed = 'Number of records processed: ';
// MRA:22-FEB-2010.
  SPimsNoShiftScheduleFound = 'There is no shift schedule defined for the used week.';
// MRA:18-MAY-2010. Order 550478.
  SPimsDeleteMachineWorkspotsNotAllowed = 'Delete not possible! There are workspots linked to this machine.';    
  SPimsDeleteMachineJobsNotAllowed = 'Delete not possible! There are jobs defined for this machine.';
  SPimsUpdateMachineCodeNotAllowed = 'Machine is linked to workspot(s). Changing the Machine Code is therefore not allowed.';
  SPimsUpdateMachineJobCodeNotAllowed = 'Machine-job is in use by a workspot-job. Changing the Machine Job Code is therefore not allowed.';      
// SO:07-JUN-2010.
  SPimsIncorrectHourType = 'Please enter a correct hour type !!';
  SPimsScanLongerThan24h = 'The scan cannot be longer than 24 hours !!';
// MRA:30-JUN-2010. Order 550478.
  SPimsMachineWorkspotNonMatchingJobs = 'WARNING: There are non-matching jobs for this workspot for the entered machine.' + #13 + 'Please be sure the jobs match for workspots that are linked to the entered machine!';
  SPimsPleaseDefineJobForMachine = 'WARNING: Please make changes for jobs codes on machine level.';
  SPimsDeleteNotAllowedForMachineJob = 'This is a machine job. Deletion is not allowed here, only on machine level.';
// MRA:24-AUG-2010. Order 550497. RV067.MRA.8.
  SPimsAbsenceReasons = 'absence reasons';
  SPimsHourTypes = 'hour types';
  SPimsAbsenceTypes = 'absence types';
// MRA:31-AUG-2010. RV067.MRA.21
  SPimsDescriptionNotEmpty = 'Please enter a description, it cannot be left empty!';
  SPimsCounterNameFilled = 'The counter name should be filled if the counter is active!';
// MRA:21-SEP-2010. RV067.MRA.34
  SPimsPaste = 'Paste';
  SPimsPasteExceptHours = 'Paste exceptional hours from "%s"';
  SPimsDefExceptHoursFound = 'Definitions found, these will be deleted. Proceed Yes / No?';
// MRA:4-OCT-2010. RV071.3. 550497
  SPimsCountersHaveBeenUpdated = 'Counters have been updated.';
  SPimsProcessToAvailIsReady = 'Process to availability is ready.';
  SPimsRecalcHoursConfirm = 'Recalculation of hours is included. Proceed ?';
// MRA:11-OCT-2010. RV071.10. 550497
  SPimMaxSatCreditReached = 'Maximum Saturday Credit has been reached!';
// MRA:1-NOV-2010.
  SPimsStartEndDateSmaller = 'Start date is smaller than end date' + #13 +
     'of an existing illness message for the same employee.';
// MRA:8-NOV-2010.
// MRA:1-DEC-2010. RV082.4. Text changed.
  SPimsIsNoScannerWarning =
    'WARNING: This employee is a non-scanner and of type: ' + #13 +
    'Autom. scans based on standard planning.' + #13 +
    'Updating scans can lead to incorrect hours! ' + #13 +
    'Changes are not allowed!';
// MRA:17-JAN-2011. RV085.1.
  SPimsEmpPlanPWDFound = 'WARNING: Planning exists for this workspot/department-combination' + #13 +
    'on %s and/or later. Are you sure you want to change the department?';
// MRA:14-FEB-2011. RV086.4.
  SPimsSelPlantsCountry = 'The selected plants should belong to the same country!';
  SPimsSelNotPosMultExports = 'Selection not possible, multiple export types are not allowed.';
// MRA:18-APR-2011. RV089.5
  SPimsAddAllWorkspots = 'Add ALL workspots?';
// MRA:21-APR-2011. RV090.1.
  SPimsFinishLastTransactionFirst = 'Please confirm/undo the last copy-action first!';  
  SPimsConfirmLastTransactionFirst = 'The last copy-action is not confirmed yet.' + #13 +
                                     'Please confirm/undo it first!' + #13 +
                                     'Yes=Confirm No=Undo';
  SPimsConfirmLastTransaction = 'Do you want to CONFIRM the last copy-action?';
  SPimsUndoLastTransaction = 'Do you want to UNDO the last copy-action?';
// MRA:26-APR-2011. RV089.1.
  SPimsYes = '&Yes';
  SPimsNo = '&No';
  SPimsAll = '&All';
  SPimsNoToAll = 'N&o To All';
// MRA:27-APR-2011. RV091.1
  SPimsPasteOvertimeDef = 'Paste overtime definitions from "%s"';
  SPimsOvertimeDefFound = 'Definitions found, these will be deleted. Proceed Yes / No?';
// MRA:24-MAY-2011. RV092.6
  SPimsEndTimeError = 'End time should be bigger than start time!';
// MRA:26-MAY-2011. RV093.1.
  SPimsFooterTotalContractGroup = 'Total Contr. gr.';
  SPimsFooterContractGroup = 'Contractgroup';
// MRA:1-JUN-2011. RV093.3.
  SPimsLoggingEnable = 'To enable/disable logging please restart Pims.';
// MRA:6-JUN-2011. RV093.3.
  SPimsCleanUpEmpHrsLog = 'Cleanup Employee Hours Logging older than %d weeks?';
  SPimsCleanUpTRSLog = 'Cleanup Timerecording Scans Logging older than %d weeks?';
// MRA:6-JUL-2011. RV094.5.
  SPimsFilenameAFAS = 'Salarymutations ABS PIMS';
// MRA:11-JUL-2011. RV094.7.
  SPimsPlantDeleteNotAllowed = 'It is not allowed to delete a plant.';
  SPimsPlantUpdateNotAllowed = 'It is not allowed to change a plant.';
  SPimsEmployeeDeleteNotAllowed = 'It is not allowed to delete an employee.';
  SPimsEmployeeUpdateNotAllowed = 'It is not allowed to change an employee.';
  SPimsWorkspotDeleteNotAllowed = 'It is not allowed to delete a workspot.';
  SPimsWorkspotUpdateNotAllowed = 'It is not allowed to change a workspot.';
// MRA:11-JUL-2011. RV04.8.
  SPimsEmployeeNotActive = 'Employee is non-active.';
// MRA:19-OCT-2011. RV099.1.
  SPimsPleaseFillBothAbsReasons = 'Please fill in both absence reasons.';
  SPimsLogStartTFTHolCorrection = 'Start TFT/Vacation correction.';
  SPimsLogEndTFTHolCorrection = 'End TFT/Vacation correction.';
  SPimsLogChangesMade = 'Number of changes made: %d';
// MRA:28-OCT-2011. RV100.2. 20011796.2. Changed.
  SPimsEarnedTFT = 'TFT Earned';
  SPimsEarnedTFTSign = 'TFT-E';
// MRA:9-JAN-2012. RV103.4.
  SPimsHolidayCardTitle = 'Holiday Card';
// MRA:13-JAN-2012. RV103.4.
  SJanuaryS = 'Jan.';
  SFebruaryS = 'Febr.';
  SMarchS = 'March';
  SAprilS = 'Apr.';
  SMayS = 'May';
  SJuneS = 'June';
  SJulyS = 'July';
  SAugustS = 'Aug.';
  SSeptemberS = 'Sept.';
  SOctoberS = 'Oct.';
  SNovemberS = 'Nov.';
  SDecemberS = 'Dec.';
// 20011796.2.
  SPimsRepHolCardOpeningBalance = 'Opening balance:';
  SPimsRepHolCardUsed = 'Used:';
  SPimsRepHolCardAvailable = 'Available:';
  SPimsRepHolCardTFTEarned = 'TFT-E Earned:';
  SPimsRepHolCardTUsed = 'T Used:';
  SPimsRepHolCardTotAvail = 'Total Available:';
  SPimsRepHolCardLegenda = 'T = Time for Time used, TFT-E = Time for Time earned, H = Holiday';
  SPimsRepHolCardLegendaTFTEarned = 'TFT-E = Time for Time earned';
  SPimsInvalidCheckDigits = 'Invalid checkdigits! Please enter a value from 10 to 99.';
  SPimsEnteredDateRemark = 'Please enter a Date-IN equal to selected date or next date.';
  SPimsShifts = 'Shifts'; // 20013551
  SPimsWorkspotHeader = 'Workspot'; // 20013551
  // 20013723
  SPimsExportPeriod = 'Export overtime';
  SPimsExportMonth = 'Export payroll';
  // 20013288.130
  SPimsExportEasyLaborChoice = 'Please make a choice about what to export.';
  // 20014002
  SPimsTotalWorkspot = 'Total Workspot';
  // 20014037.60
  SPimsPimsUser = 'Pims User';
  SPimsWorkstation = 'Workstation';
  // TD-22429
  SPimsStart = 'Start';
  // TD-22475
  SPimsPasteDatacolConnection = 'Paste datacol connections from "%s"';
  SPimsDatacolConnectionFound = 'DatacolConnections found, these will be deleted. Proceed Yes / No?';
  // TD-22503
  SChangeEmplAvail = 'Employee Availability was found, this will be deleted for non-available. Do you want to continue?';
  // SO-20014715
  SPimsReportYes = 'Yes';
  SPimsReportNo = 'No';
  SPimsReportTill = 'till';
  SPimsReportLate = 'late';
  SPimsReportEarly = 'early';
  // TD-23315 Changed this message:
  SPIMSTimeIntervalError = 'Starting time is less than previous end time,' + #13 +
   'or end time is higher then next start time.';
  // 20013196
  SPimsDeleteLinkJob = 'There is a link job defined. This will be removed. Proceed Yes/No ?';
  SPimsLinkJobExist = 'It is not allowed to define more than 1 link job per workspot.' + #13 +
    'This link job will not be saved.';
  // 20011800
  SPimsOK = '&Ok';
  SPimsFinalRun = '&Final Run';
  SPimsFinalRunWarning = 'FINAL RUN, ARE YOU SURE?' + #13#13 +
    'Any mutations over this period cannot be made afterwards.';
  SPimsFinalRunExportDateWarning = 'FINAL RUN' + #13#13 +
    'Selected export-period is already exported (partly or completely).' + #13#13 +
    'It is NOT allowed to make this export.';
  SPimsFinalRunExportDateWarningAdmin = 'FINAL RUN' + #13#13 +
    'Selected export-period is already exported (partly or completely).' + #13#13 +
    'Do you want to start the export?';
  SPimsTestRunExportDateWarning = 'TEST RUN' + #13#13 +
    'Selected export-period is already exported (partly or completely).' + #13#13 +
    'Do you want to start the export?';
  SPimsFinalRunExportGapWarning = 'FINAL RUN' + #13#13 +
    'There is a gap of %d days between last-export-date' + #13 +
    'and start-date of selection.' + #13#13 +
    'It is NOT allowed to make this export.';
  SPimsFinalRunExportGapWarningAdmin = 'FINAL RUN' + #13#13 +
    'There is a gap of %d days between last-export-date' + #13 +
    'and start-date of selection.' + #13#13 +
    'Do you want to start the export?';
  SPimsTestRunExportGapWarning = 'TEST RUN' + #13#13 +
    'There is a gap of %d days between last-export-date' + #13 +
    'and start-date of selection.' + #13#13 +
    'Do you want to start the export?';
  SPimsCloseScansSkipped = 'WARNING: Some scans could not be closed because of a compare' + #13 +
    'with last-export-date.';
  SPimsFinalRunAddAvaililityNotAllowed = 'Adding availability for this week is not allowed' + #13 +
    'because it has already been exported.';
  SPimsFinalRunDateChange = 'WARNING: The date has been changed to a date after the last-export-date.';
  SPimsFinalRunAvailabilityChange = 'WARNING: Some employee availability could not be changed because of the last-export-date.';
  SPimsFinalRunTestRun = 'TEST RUN';
  SPimsFinalRunDeleteNotAllowed = 'WARNING: Delete is not allowed, because it was already exported.';
  SPimsFinalRunOnlyPartDelete = 'WARNING: Only a part of the week will be deleted, because some part was already exported.';
  SPimsFinalRunShiftSchedule = 'WARNING: Not all could be copied because some part was already exported.';
  // SO-20014442
  SPimsYearMonth = 'Year/Month';
  SPimsStartEndYearMonth = 'End year/month should be bigger than start year/month!';
  SPimsOnlySANAEmps = 'Only SANA employees';
  SPimWrongYearSelection = 'Wrong year-selection: The From-To-Year must be the same!';
  // 20015178
  SPimsPleaseEnterFakeEmpNr = 'Please enter a unique Fake Employee Number,' + #13 +
    'that will be used in Personal Screen.';
  SPimsFakeEmpNrInUse = 'The entered Fake Employee Number is already in use' + #13 +
    'by another workspot. Please enter a different number.';
  // 20015223
  SPimsWorkspots = 'Workspots';
  SPimsPlantTitle = 'Plant';
  SPimsAllWorkspots = '<All>';
  SPimsMultipleWorkspots = '<Multiple>';
  // 20013476
  SPimsValueSampleTimeMins = 'Sample Time: Invalid value entered. Please enter a value from 1 to 30 minutes.';
  // 20015586
  SPimsIncludeDowntime = 'Include down-time quantities:';
  SPimsIncludeDowntimeYes = 'Yes';
  SPimsIncludeDowntimeNo = 'No';
  SPimsIncludeDowntimeOnly = 'Show only down-time';
  // TD-25948
  SPimsOpenCSV = 'Open CSV file?';
  SPimsCannotOpenCSV = 'Please install Microsoft Excel (or a similar program).';
  // PIM-53
  SPimsNoWorkingDays = 'Please select one or more working days.';
  SPimsAddHoliday = 'Do you want to add %d absence day(s) to employee availability?';
  // PIM-53
  SPimsAddHolidayEmplAvail = 'Please enter a valid absence reason code.';
  SPimsAddHolidayNoStandAvail = 'Action is aborted: There is no standard availability defined for the plant/shift-combination!';
  // PIM-52
  SPimsWorkScheduleDetailsExists = 'Work Schedule Details already exist! Overwrite?';
  SPimsWorkScheduleWrongRepeats = 'Wrong value for Weekly Repeats! Please enter a value between 1 and 26.';
  SPimsWorkScheduleInUse = 'Work Schedule is still in use (Employee Contract) and cannot be deleted!';
  SPimsInvalidShift = 'Please enter a valid shift.';
  SPimsReady = 'Ready';
  SPimsPlanningMessage1 = 'Not processed! For employee';
  SPimsPlanningMessage2 = 'and date';
  SPimsPlanningMessage3 = 'there was planning found.';
  SPimsNoStandAvailFound1 = 'Error: No standard availability found for employee';
  SPimsNoStandAvailFound2 = 'and shift';
  SPimsProcessWorkScheduleMessage1 = 'Process Workschedule for employee';
  SPimsProcessWorkScheduleMessage2 = 'and referencedate';
  // PIM-23
  SPimsTotals = 'Totals';
  SPimsOTDay = 'Day';
  SPimsOTWeek = 'Week';
  SPimsOTMonth = 'Month';
  SPimsMO = 'MO';
  SPimsTU = 'TU';
  SPimsWE = 'WE';
  SPimsTH = 'TH';
  SPimsFR = 'FR';
  SPimsSA = 'SA';
  SPimsSU = 'SU';
  SPimsNormalHrs = 'Normal hours per day:';
  SPimsOTPeriod = 'Period';
  SPimsOTPeriodStarts = 'Starts in week';
  SPimsOTWeeksInPeriod = 'Weeks in period:';
  SPimsOTWorkingDays = 'Working days:';
  SPimsCGRound = 'Round';
  SPimsCGTRunc = 'Trunc';
  SPimsCGMinute = 'minutes';
  SPimsCGTo = 'to';
  SPimsStandStaffAvailUsed = 'NOTE: For one or more days a fixed absence reason is used'#13'based on standard staff availability.'#13'This will not be overwritten.';
  SPimsShiftDate = 'Shift Date';
  // ABS-27052
  SPimsSelectPrinter = 'Select Printer';
  SPimsSelectDefaultPrinter = 'Select a printer as default printer';
  SPimsNoPrinterFound = 'No printers found!';
  SPimsPleaseSelectPrinter = 'Please select a printer.';
  // PIM-52
  SPimsNoCurrentEmpContFound = 'No current employee contract found for employee';
  // PIM-203
  SPimsHostPortExists = 'This Host+Port-combination already exists for another workspot.';
  // PIM-174
  SPimsCopied = 'Copied:';
  SPimsRows = 'row(s).';
  SPimsEmp = 'Employee';
  SPimsEmpContract = 'Employee Contract';
  SPimsWSPerEmp = 'Workspots per Employee';
  SPimsStandStaffAvail = 'Standard Staff Avail.';
  SPimsShiftSched = 'Shift Schedule';
  SPimsEmpAvail = 'Employee Avail.';
  SPimsStandEmpPlan = 'Employee Standard Planning';
  SPimsEmpPlan = 'Employee Planning';
  SPimsAbsenceCard = 'Absence card';
  SPimsRecordKeyDeleted = 'This record is deleted by another user.';
  SPimsWorkScheduleWrongStartDate = 'Reference week must be a date that starts at the first day of the week.'#13'This has been corrected.';
  SPimsRoamingWorkspotExists = 'There is already another workspot defined as roaming.';
  SPimsHourlyWage = 'Hourly Wage:';
  SPimsTotalExtraPayments = 'Total Extra Payments';
  SPimsTBPEmp = 'Timeblocks per Employee';

{$ELSE}
  {$IFDEF PIMSCHINESE}

  SPimsHomePage = 'Pims Homepage will be implemented soon!';
  SPimsIncorrectPassword = 'Incorrect Password.'#13'Enter another password or '+
    'Cancel.';
  SPimsNoEmptyPasswordAllowed = 'An empty password is not allowed.'#13'Enter '+
    'another password or Cancel.';
  SPimsPasswordChanged = 'New password saved.';
  SPimsPasswordNotChanged = 'Password not changed.';
  SPimsPasswordNotConfirmed = 'Password and confirmation are '+
    'different.'#13'Enter another password or Cancel.';
  SPimsSaveChanges = 'Save changes?';
  SPimsRecordLocked = 'This record is in use by another user';
  SPimsOpenXLS = 'Open Excel file?';
  SPimsOpenHTML = 'Open HTML file?';
  SPimsNotUnique = 'Not Unique';
  SPimsNotFound = 'Nothing found!';
  SPimsNotEnoughStock = 'There are not enough products in stock!'#13'Issue the '+
    'items anyway?';
  SPimsNoItems = 'No items yet';
  SPimsNoEmployee = 'There are no employee!';
  SPimsNoCustomer = 'There are no customers!';
  SPimsLoading = 'Loading ';
  SPimsKeyViolation = 'This line already exists.';
  SPimsForeignKeyPost = 'This record has a connection to other records. Your '+
    'request is denied.';
  SPimsForeignKeyDel = 'This record has a connection to other records. Your '+
    'request is denied.';
  SPimsFieldRequiredFmt = 'Please fill required field %s.';
  SPimsFieldRequired = 'Please fill all required fields.';
  SPimsDragColumnHeader = 'Drag a column header here to group that column.';
  SPimsDetailsExist = 'This record has a connection to other records. Please '+
    'remove those records first.';
  SPimsDBUpdate = 'Update Pims database';
  SPimsDBUpdateButtonCaption = '&Run Pims update';
  SPimsDBUpdateConflict = 'Pims detected a conflict while updating the '+
    'database.'#13'Please call the ABS helpdesk and refer to this message.';
  SPimsDBNeedsUpdating = ' Pims detected on older database version.'#13+
   ' Pims needs to update your database to run properly.';
  SPimsCurrentCode = 'Current item code. ';
  SPimsContinue = ' Continue?';
  SPimsConfirmCancelNote = 'Do you want to cancel this delivery note?';
  SPimsConfirmDeleteNote = 'Do you want to delete this delivery note?';
  SPimsConfirmDeleteLicense = 'Do you want to delete this Pims License?';
  SPimsConfirmDelete = 'Do you want to delete the current record?';
  SPimsCannotPrintLabel = 'You cannot print a label for the currently selected '+
    'item.';
  SPimsCannotPlaceFlag = 'You cannot place a flag on the currently selected '+
    'item.';
  SPimsCannotOpenXLS = 'Please install Microsoft Excel.';
  SPimsCannotOpenHTML = 'Please install an Internet Browser on your machine.';

  // FOR PIMS:
  SSelectDummyWK = 'You can not select a dummy workspot!';
  SPimsPercentageNotCorrect = 'Error: Total percentage for workspot is not 100%';
  SPimsTimeMustBeYounger = 'Error: Start-time must be less than End-time!';
  SPimsNoMaster = 'There are no Process Units';
  SPimsNoSubDetail = 'There are no Departments';
  SPimsNoDetail = 'There are no Workspots';
  SPIMSContractTypePermanent = 'Permanent';
  SPIMSContractTypeTemporary = 'Temporary';
  SPIMSContractTypePartTime = 'External';
  SPIMSContractOverlap = 'There is an overlap for this contract with last contract';
  SPimsTBDeleteLastRecord = 'Only the last record can be deleted';
  SPimsTBInsertRecord = 'Only %d timeblocks allowed';
  SPimsStartEndDate = 'Start date is bigger than end date';
  SPimsStartEndTime = 'Start time is bigger than end time';
  SPimsStartEndDateTime = 'Start date time is bigger than end date time';
  SPimsStartEndActiveDate = 'Start active date is bigger than end active date';
  SPimsStartEndInActiveDate = 'Start inactive date is bigger than end inactive date';
  SPimsWeekStartEnd = 'End week < Start week !';
  SPimsBUPerWorkspotPercentages = 'Total of percentages should be 100,  ' +
    'you can not leave the form';
  SPimsTBStartTime = 'Starttime TimeBlock < Starttime Shift on ';
  SPimsTBEndTime = 'Endtime TimeBlock > Endtime Shift on ';
  SPimsBUPerWorkspotInsert = 'There is no workspot you can not insert records';
  SPimsDateinPreviousPeriod =  'There already is a scan that includes this time';
  SPimsExistJobCodes = 'You cannot change it, there are jobcodes defined';
  SPimsExistBUWorkspot = 'You cannot change it, there are relateted records in Business Units per Workspots';
  // Time Recording
  SPimsIDCardNotFound = 'ID Card not found';
  SPimsIDCardExpired	= 'ID Card has expired';
  SPimsEmployeeNotFound = 'Employee not found';
  SPimsInactiveEmployee = 'Employee is inactive';
  SPimsScanningsInMinute = 'Not allow multiple scannings within one minute';
  SPimsStartDay = 'Start of day';
  SpimsEndDay = 'End of day';
  SPimsNoWorkspot = 'No Workspot defined on current workstation';
//Workspot
  SPimsNoJob = 'No Jobs defined on current Workspot';
  SPimsDeleteJob = 'There are jobcodes assigned, they will be deleted!';
  SPimsDeleteBusinessUnit = 'There are business units assigned, they will be deleted!';
  SPimsDeleteJobBU =
    'All connected records with this workspot will be deleted, delete workspot ? ';
  SShowBUWK         = 'Show business units per workspot';
  SShowJobCode = 'Show job codes';
//workspot per workstation
  SPimsSameSourceAndDest = 'The source and destination computer name could ' +
   	'not be the same';
  // Selector Form - Grid Column name
  SPimsColumnPlant 	= 'Plant';
  SPimsColumnWorkspot = 'Workspot';
  SPImsColumnCode = 'Code';
  SPimsColumnDescription = 'Description';
  // Selector Form - Captions
  SPimsWorkspot = 'Select Workspot';
  SPimsJob 	= 'Select Job';
  SPimsJobCodeWorkSpot = 'You can not close form, first define at least one jobcode';
  SPimsInterfaceCode  =
   'You can not close form, first define at least one interface code other than the interface code for the job';
  SPimsOtherInterfaceCode  = 'Please define other interface code than the interface code of the job code';
  SPimsNoOtherInterfaceCodes = 'There are no other interface codes defined, you can not open form';

  SPimsSaveBefore = 'Please save the record before';
  SPimsNoPlant = 'There are no plants';
  SPimsScanDone = 'Scanning processed';
  SPimsScanError = 'Scanning NOT processed: Data-Base ERROR';
//  SPimsNoEmployee = 'There are no employees';
// Time Recording Scanning
  SPimsNoShift = 'No Shift defined';
  SPimsNoCompletedScans = 'Completed scans cannot be modify / deleted';
  SPimsNonScanningStation = 'This is not a time recording station';
  SPimsScanTooBig = 'Scan period longer than 12 hours is not allowed';
  SPimsUseJobCode = 'Please select a job code, workspot it is defined to use jobcodes';
  SPimsNoMatch = 'No match for Employee + ID-Card';
// team per department - extra check
  SPimsNoMorePlantsinTeam =
    'All department of one team should belong to the same plant !';
  SPimsTeamPerEmployee =
    'Department of the employee should be in the team !';

// Hours per Employee
  SPimsAutomaticRecord = 'Automatic inserted record cannot be changed ';
  SPimsNegativeTime = 'Double click to set a negative time (Red color)';
  SPimsExistingRecord = 'A record with the same parameters already exist.'+#13+
  	'Please select that record';
  SPimsProductionHour = 'ProductionHourPerEmployee table cannot updated';
  SPimsNoTimeValue = 'Please insert time value for at least one day';
  SPimsSalaryHour = 'SalaryHourPerEmployee table cannot updated';
  SPimsAbsenceHour = 'AbsenceHourPerEmployee table cannot updated';
  SPimsTo = 'To';
  SPimsInvalidTime = 'Value not set! (Invalid time value)';
// bank holiday process
  SPimsConfirmProcessEmployee = 'Do you want to process all bank holidays of ' +
    ' the year to employee availability ?';
  SPimsInvalidAbsReason = 'Not a valid absence reason!';
  SPimsEmptyAbsReason = 'Please fill required field absence reason!';
  SPimsEmptyDescription = 'Please fill required field description!';
// for reports
  SPimsReportWTR_HOL_ILL_Checks = 'You should select at least one absence reason!';
  SPimsMaxNumberOfWeeks = 'Maximal number of selected weeks is 13!';
  SPimsEndTime = 'End time';
  SPimsStartTime = ' < Start time';
  SPimsOn = 'on' ;
// update database
  SPimsInvalidVersion = 'Invalid PIMS version !';
  SUpdateDatabaseFrom = 'Database will be updated from version ';
  SUpdateDatabaseTo = ' to version ';
// Standard staff availability
  SCheckAvailableTB = 'Permitted values are * and -';
  SCheckAvailableTBNotEmpty = 'Please fill all required fields !';
  SCopySTA1 = 'Standard availability already exists for day ';
  SCopySTA2 = ', overwrite ? ';
  SCopySameValues = 'You selected the same record !';
  SCopySTANotValidFrom = 'There are no valid timeblocks for selected record!';
  SCopySTANotValidTo = 'There are no valid timeblocks for selected record of grid!';
  SEmptyValues = 'Please fill all fields before!';
  SEmplSTARecords = ' There are no standard availabilities defined for this employee ';
// shift schedule
  SShiftSchedule = 'Permitted values are numbers 0 .. 99 or -';
  SSHSShift = 'Permitted values are only shift defined';
  SSHSWeekCopy = 'Start date of copy should be bigger than end date';
  SInvalidShiftSchedule = 'Shift number has not defined or values start/end' +
    ' time per day are zero!';
  SEmployeeAvail_0  = 'Employee ';
  SEmployeeAvail_1  = ' already has availability in this shift on year, week, day ';
//  SEmployeeAvail_2 = ', day ';
  SEmployeeAvail_3 = ', you can not change the shift number.';
  SUndoChanges = 'Last changes will not be saved!';
  SStartWeek = 'Start week should be bigger than end week!';
  SCopySelectionShiftSchedule_1 = 'Shift schedule for employee ';
  SCopySelectionShiftSchedule_2 = ' already exists. Overwrite ?';
  SCopyFinished = 'Copy is finished!';
  SEmptyRecord = 'There is no employee selected!';
// employee availability
  SEmplAvail = 'Permitted values are *, - or an absence reason';
  SValidTimeBlock = 'All valid timeblocks should be filled!';
  SEmployeePlanned = 'Employee is already planned, continuing will delete planning. ' +
    'Do you want to continue ?';
  SCopyFrom  = 'Please select a record!';
  SCopyTo  = 'Records are the same, please select a different record!';
  SEMAOverwrite = 'Employee availability for ';
  SEMAShift = ' shift ';
  SEMAExists = 'already exists. Do you want to overwrite?';
// staff planning
  SSelectedWorkspots = 'There are more than 401 selected workspots, only the first 401  will ' +
    ' be displayed ';
  SEmptyWK = 'There are no workspots selected';
  SOverplanning = 'Overplanning more than 200%';
  SMakeAvailable = 'Make available';
  SNotAcceptPlanned = 'This planning of employee is not accepted!';
  SSaveChanges = 'Save changes?';
  SReadStdPlnIntoDaySelection = 'Planning already read for a plant/team/date within the selection';
  SReadStdPlnIntoDaySelectionFinish = 'Read standard planning into day planning finished';
  SReadPastIntoDayPlanFinish = 'Read past into day planning finished';
  SContinueYN = 'Do you want to continue ?';
  SDateFromTo = 'End date should be bigger than start date!';
  SDateIntervalValidate = ' Interval of target date should be different than interval' +
    ' of source date';
  SNoEmployee = 'There are no employees selected!';
  SOCILevel = 'Occupation level ';
  SPlannedLevel = 'Planned with level ';
  SPlannedAllLevels = 'Planned ';
  SOCIAllLevel = 'Occupation  ';
  SSameRangeDate = 'Date intervals should have the same length!';
// report staff planning
  SStartEndYear = 'End year should be bigger than start year!';
  SWeek = 'You can not select more than one week!';
  SNoTeam = 'Empty team: ';
// Workspot per employee
  SLEVELWK = 'Only A, B, C are allowed!';
  SInvalidWK = 'Only A..Z, a..z, 0..9, and ''_'' are allowed !';
// Staff Availability
  SChangePlaned = 'Employee is already planned, continuing will delete planning'+
    #13+'Do you want to continue?';
  SInvalidShiftNo = 'Invalid Shift number for day: %d';
  SInvalidAvailability = 'Invalid Availability value for day: %d, TB: %d';
//  E&R
  SMoreThan = ' More than ';
  SInvalidMistakeValue =  'Number of mistakes should be bigger than zero !';
  SNotDeleteRecord = ' You can not delete this record !';
  SPimsDBError = 'Report openned by other station or Database error. '+#13+
		'Please run report again';
 //
// productivity reports

  SDaySelect = 'Start day should be less then end day!';
//
// jobcode
  SInterfaceCode = 'This interface code is already used for other workspots!';
  SWKInterfaceCode = 'This interface code is already used for this workspot!';
  SWKRescanInterfaceCode =
   'There are interface codes used more than once for this workspot!';
  SDeleteOtherInterfaceCode = 'All other interface codes defined will be deleted!';

//
  SExceptionDataSetReport =
   'Data set is empty cannot create report, please set it first and rebuild pims';
  SExceptionDataSetForms  =
   'Data set is empty cannot refresh grids, please set it first and rebuild pims';
// export payroll report
   SPeriodReport = 'Period';
   SMonthReport = 'Month';
// report quality incentive
   SCalculationwithOvertime = 'Quality incentive calculation with overtime report';
// add wk into datacollection
  SADDWKDateCollection = 'Records are added';
// strings for mistake per employee
  SDate = 'Date ';
  STotalWeek = 'Total week ';
// report hrs per empl
  SCaptionProdRepHrs = 'Production hours per employee';
  SCaptionSalRepHrs =  'Salary hours per employee';
//report CompHours
  SShowActiveEmpl = 'Show active employee';
  SShowNotActiveEmpl = 'Show not active employee';
//report HrsWKCum
  SRepHrsCUMWK = 'Workspot';
  SRepHrsTotalCUM = 'Total workspot';

  SRepHrsCUMWKBU = 'Business unit';
  SRepHrsTotalCUMBU = 'Total business unit';

  SRepHrsCUMWKPlant = 'Plant';
  SRepHrsTotalCUMPlant = 'Total plant';

  SRepHrsCUMWKDept = 'Department';
  SRepHrsTotalCUMDept = 'Total department';
  SRepJobCode = 'Job code';
  SRepTotJobCode = 'Total job code';
// report absence
  SHeaderAbsRsn = 'Absence reason';
  SHeaderWeekAbsRsn = 'Week';
  SHdTotalWeek = 'Total week';
  SRepExpPayroll = 'From';
// report absence schedule
  SReport = 'Report ';
  SReportWorkTimeReduction = 'work time reduction ';
  SReportIllness = 'illness';
  SReportHoliday = 'holiday';
  SActiveEmpl =  'Active employees';
  SInactiveEmpl = 'Inactive employees';
// report
  SRepHrsPerEmplActiveEmpl = 'Show active employee';
  SRepHrsPerEmplInActiveEmpl = 'Show not active employee';
  SRepProdHrs = 'Productive hours';
  SRepNonProdHrs = 'Non-productive hours';
  SAll = 'All';
  SActive = 'Active';
  SInactive = 'Inactive';
  SName = 'Name ';
  SShortName = 'Short name';
// REPORT check list
  SShowProcessed =  'Show processed';
  SShowNotProcessed  = 'Show not processed';
//Report
  SErrorOpenRep = 'Error open data base, report can not be opened';
  SErrorCreateRep = 'Error report can not be created';
//DialogProcessAbsenceHrsFRM
   SLogEmpNo = 'Found Number of Records=';
   SLogChangedAbsenceTotal = 'Changed %d Absence Total-records';
   SLogDeletedRec = 'Deleted %d Absence Hour-records';
   SLogEndAbsence = 'End: Defining Absence Hours Per Employee';
   SLogStartSalaryHours = 'Start: Defining Salary Hours per Employee';
   SLogStartAvailability  = 'Start: Defining Employee Availability';
   SLogChangedAbsence = 'Changed %d Absence Hours-records';
   SlogAddedAbsence = 'Added %d Absence Hours-records';
   SLogEndAvailability = 'End: Defining Employee Availability';
   SLogStartNonScanning = 'Start: Defining Non-Scanning Employee Availability';
   SLogEndNonScanning  = 'End: Defining Non-Scanning Employee Availability';
   SLogFoundEmployeeNo = 'Found Number of Records = %d';
   SSProcessFinished = 'Process finished';
// Staffplanning
   SStaffPlnEmployee = 'Employee';
   SStaffPlnName = 'name';
// Contract group form
   SOvertimeDay = 'Day';
   SOvertimeWeek = 'Week';
   SValidMinutes = 'Minutes defined should be less than 59';
   SQuaranteedHourType = 'Please select an hour type';
//
//update cascade
   SConfirmUpdateCascadeEmpl =
     'Employee will be changed for all connected records, update employee ? ';
   SConfirmDeleteCascadeEmpl =
     'All connected records with this employee will be deleted also, delete employee ? ';
   SEmpExist = 'This employee already exist ';
//export payroll MRA:RV095.4. Give better message: 'date' added.
   SNotDefaultSelection = 'Not the default date-selection. Are you sure ?';
   SPeriodAlreadyExported = 'This period is already exported. Are you sure ?';
// transfer free time
   SPimsDifferentYears = 'Target year should be different than source year!';
   SPimsCheckBox = 'At least one of check boxes must be selected!';
   SPimsProcessFinish = 'Process has finished!';

   // MR:17-01-2003
   SPimsUseShift = 'Please select a shift!';
   SPimsUseWorkspot = 'Please select a workspot!';
//  staff planning -CAR 30-01-2003
   SPimsDayPlanning = 'Day planning: ';
   SPimsTeamSelection = 'Team selection: ';
   SPimsTeamSelectionAll = 'All teams';
   SPimsSTDPlanning = 'Standard planning: ';
   SPimsStaffTo =  ' to ';
   SPimsPlant =  '      Plant:  ';
   SPimsShift = '      Shift: ';
   SPimsTeam = 'Team ';
   // MR:28-01-2003
   SPimsNoEmployeeLine = 'No employee';

   SPimsNoPrevScanningFound = 'No previous scanning found';
   SpimsScanExists = 'There''s already a scanning-record on that day for that employee. ' + #13 +
     'You cannot change this item.';

 //car: 27.02.2003
   SPimsEXCheckIllMsgClose =
     'There already is one outstanding illness message, please close it before';
   SPimsEXCheckIllMsgStartDate =
     'Start date should be bigger than the end date of last created illness message ';

   // MR:03-03-2003
   SPimsContract = 'Contract';
   SPimsPlanned = 'Planned';
   SPimsAvailable = 'Avail.';

   // MR:07-03-2003
   SpimsYear = 'Year';
   SPimsWeek = 'Week';
   SPimsDate = 'Date';
   //CAR:21-03-2003 - REPORT staff planning per day
   SPimsNewPageWK = 'New page per workspot';
   SPimsNewPageEmpl = 'New page per employee';
   //CAR:21-03-2003 report checklist
   SPimsAvailability = 'Availability: ';
   //CAR:21-03-2003 update cascade -Plant
   SConfirmDeleteCascadePlant =
     'All connected records with this plant will be deleted, delete plant ? ';
   SConfirmUpdateCascadePlant =
     'Plant will be changed for all connected records, update plant ? ';
   SPlantExist = 'This plant already exists ';
   //CAR: 27-03-2003
   SRepTeamTotEmpl ='Total employee';
   //CAR 29-03-2003
   SRepHRSPerEmplCorrection = 'Manual corrections';
   SRepHrsPerEmplTotal = 'Total';
   //CAR 31-03-2003  - Report AbsenceCard
   SJanuary = 'January';
   SFebruary = 'February';
   SMarch = 'March';
   SApril = 'April';
   SMay = 'May';
   SJune = 'June';
   SJuly = 'July';
   SAugust = 'August';
   SSeptember = 'September';
   SOctober = 'October';
   SNovember = 'November';
   SDecember = 'December';
//CAR  2-04-2003 - report Production Detail
   SRepProdDetPerformance = 'Performance';
   SRepProdDetBonus = 'Bonus';
// CAR 2-4-2003 - Stand Staff availability
   SStdStaffAvailPlanned =
     'Employee is planned in standard planning, continuing will delete planning. ' +
     'Do you want to continue ?';
//CAR 7-4-2003
   SPimsJobs  = 'Jobcode';
//CAR 15-4-2003
  SPimsStartEndDateTimeEQ = 'Start date time is bigger or equal with end date time';
  SPimsStartEndTimeNull = 'Start time or end time are empty, do you want to continue ?';
// MR:24-04-2003
  SPimsNoData = 'No Data';
//CAR 5-5-2003
  SPimsAbsence = 'Absence';
  SPimsDifference = 'Difference';
// car 12-5-2003
  SExportDescEMPL = 'Employee description';
  SExportDescDept = 'Dept description';
  SExportDescPlant = 'Plant description';
  SExportDescShift = 'Shift description';
//
  SDummyWK = 'This is a dummy workspot it can not be saved';
// CAR 28-8-2003 - Stand Staff availability
   SStdStaffAvailFuturePlanned =
     'Employee is already planned in this shift. Continue will delete future planning. ';
// 9-4-2003
  SPimsInvalidWeekNr = 'Invalid week number!';
// 9-4-2003
  SPimsHoursPerDayProduction = 'Production ';
  SPimsHoursPerDaySalary = 'Salary ';
  // MR:12-09-2003
  SPimsCompletedToNotScans = 'You cannot change a completed scan back '#13'to a not-completed one.';
// user rights
  SAplErrorLogin = 'Invalid user name or password!';
  SAplNotConnect = 'Invalid connection to database!';
  SAplNotVisible = 'This user can not open the application';
// MR:3-11-2003 Moved from 'UStrings.pas'
  LSPimsEnterPassword = 'Enter Password';
// MR:11-11-2003
  SPimsEmployeesAll = 'All employees';
  SPimsEmployeesOnWrongWorkspot = 'On wrong workspot';
  SPimsEmployeesNotScannedIn = 'Not scanned in';
  SPimsEmployeesAbsentWithReason = 'Absent with reason';
  SPimsEmployeesAbsentWithoutReason = 'Absent without reason';
  SPimsEmployeesWithFirstAid = 'With first aid';
  SPimsEmployeesTooLate = 'Too late';
// 550119 - Workspot per Employee form
  SPimsLevel = 'Level';
// Car: 15-1-2004
  SPimsEmplContractInDatePrevious =
   'There already is a contract that includes this date';
// 550287
// Contract group form
  STruncateHrs = 'Truncate';
  SRoundHrs = 'Round';
//550124
  SPimsPieces = 'Pieces';
  SPimsWeight = 'Weights';
// MR:03-05-2004 Order 550313
  SPimsCleanUpProductionQuantities = 'Cleanup Production Quantities older than %d weeks?';
  SPimsCleanUpEmployeeHours = 'Cleanup Employee Hours older than %d weeks?';
  SPimsCleanUpTimerecordingScans = 'Cleanup Timerecording Scans older than %d weeks?';
// MR:17-05-2004 Order 550315
  SPimsLabelCode = 'Code';
  SPimsLabelInterfaceCode = 'Interface Code';
// MR:26-07-2004 Order 550327
  SPimsNotPlannedWorkspot = 'Not Planned';
// MR:09-08-2004 Order 550336
  SNumber = 'Number';
// MR:13-10-2004
  SPimsGeneralSQLError = 'BDE reported a general SQL Error';
// MR:26-10-2004
  SPimsEfficiencyAt = 'Efficiency at';
  SPimsEfficiencySince = 'Efficiency since';
// MR:28-10-2004
  SPimsConnectionLost = 'Error: Database connection lost. Please restart application';
// MR:10-12-2004
  SSHSWeekDatesCross = 'Error: Selected From/To-dates will cross each other!';
// MR:15-12-2004
  SPimsHours = 'hours';
  SPimsDays = 'days';
  SPimsBonus = 'bonus';
  SPimsEfficiency = 'efficiency';
// MR:11-01-2005
  SPimsTotalProduction = 'Total production';
// MR:11-01-2005
  SPimsProductionOutput = 'Production Output';
  SPimsChartHelp = 'select time: click on graph'; //, move: right-click between graph-items';
  SPimsInvalidScan = 'Error: You entered an invalid scan.';
// MR:11-01-2005 -> Pims Oracle!
  SPimsScanOverlap = 'Error: Scan has wrong time interval.';
  SPimsMaxSalBalMin = 'Max.Sal.Balance';
// MR:16-06-2005
  SPimsConfirmProcessEmployeeSelectedDate = 'Do you want to process the selected date ' + #13 +
    '%s' + #13 + 'to employee availability ?';
// MR:20-06-2005
  SPimsNoBankHoliday = 'Please select a bank holiday date';
// MR:27-09-2005
  SPimsUseJobcodesNotAllowed = 'There are not-completed scans for this workspot. ' + #13 +
    'Change is not allowed.';
// MR:14-11-2005
  SPimsUnknownBusinessUnit = 'Unknown Business Unit';
// MR:07-12-2006
  SOvertimeMonth = 'Month';
// MR:05-01-2007
  SErrorWrongTimeBlock = 'ERROR: Wrong timeblock for employee';
  SErrorDuringProcess = 'There were errors! Please see error-messages above.';
  SSProcessFinishedWithErrors = 'Process finished with errors!';
// MR:26-01-2007
  SPimsMonthGroupEfficiency = 'Monthly Group Efficiency';
  SPimsGroupEfficiency = 'Monthly Efficiency';
// MR:14-SEP-2009
  SPimsTemplateYearCopyNotAllowed = 'It is not allowed to copy to this year!';
  SPimsTemplateYearSelectNotAllowed = 'It is not allowed to select this year!';
// MRA:8-DEC-2009 RV048.1.
  SPimsEmpPlanPWD = 'It is not allowed to change the department, ' + #13 +
    ' because employees are planned on this workspot/department combination.';
// MRA:19-JAN-2010.
  SPimsNrOfRecsProcessed = 'Number of records processed: ';
// MRA:22-FEB-2010.
  SPimsNoShiftScheduleFound = 'There is no shift schedule defined for the used week.';
// MRA:18-MAY-2010. Order 550478.
  SPimsDeleteMachineWorkspotsNotAllowed = 'Delete not possible! There are workspots linked to this machine.';    
  SPimsDeleteMachineJobsNotAllowed = 'Delete not possible! There are jobs defined for this machine.';
  SPimsUpdateMachineCodeNotAllowed = 'Machine is linked to workspot(s). Changing the Machine Code is therefore not allowed.';
  SPimsUpdateMachineJobCodeNotAllowed = 'Machine-job is in use by a workspot-job. Changing the Machine Job Code is therefore not allowed.';      
// SO:07-JUN-2010.
  SPimsIncorrectHourType = 'Please enter a correct hour type !!';
  SPimsScanLongerThan24h = 'The scan cannot be longer than 24 hours !!';
// MRA:30-JUN-2010. Order 550478.
  SPimsMachineWorkspotNonMatchingJobs = 'WARNING: There are non-matching jobs for this workspot for the entered machine.' + #13 + 'Please be sure the jobs match for workspots that are linked to the entered machine!';
  SPimsPleaseDefineJobForMachine = 'WARNING: Please make changes for jobs codes on machine level.';
  SPimsDeleteNotAllowedForMachineJob = 'This is a machine job. Deletion is not allowed here, only on machine level.';
// MRA:24-AUG-2010. Order 550497. RV067.MRA.8.
  SPimsAbsenceReasons = 'absence reasons';
  SPimsHourTypes = 'hour types';
  SPimsAbsenceTypes = 'absence types';
// MRA:31-AUG-2010. RV067.MRA.21
  SPimsDescriptionNotEmpty = 'Please enter a description, it cannot be left empty!';
  SPimsCounterNameFilled = 'The counter name should be filled if the counter is active!';
// MRA:21-SEP-2010. RV067.MRA.34
  SPimsPaste = 'Paste';
  SPimsPasteExceptHours = 'Paste exceptional hours from "%s"';
  SPimsDefExceptHoursFound = 'Definitions found, these will be deleted. Proceed Yes / No?';
// MRA:4-OCT-2010. RV071.3. 550497
  SPimsCountersHaveBeenUpdated = 'Counters have been updated.';
  SPimsProcessToAvailIsReady = 'Process to availability is ready.';
  SPimsRecalcHoursConfirm = 'Recalculation of hours is included. Proceed ?';
// MRA:11-OCT-2010. RV071.10. 550497
  SPimMaxSatCreditReached = 'Maximum Saturday Credit has been reached!';
// MRA:1-NOV-2010.
  SPimsStartEndDateSmaller = 'Start date is smaller than end date' + #13 +
     'of an existing illness message for the same employee.';
// MRA:8-NOV-2010.
// MRA:1-DEC-2010. RV082.4. Text changed.
  SPimsIsNoScannerWarning =
    'WARNING: This employee is a non-scanner and of type: ' + #13 +
    'Autom. scans based on standard planning.' + #13 +
    'Updating scans can lead to incorrect hours! ' + #13 +
    'Changes are not allowed!';
// MRA:17-JAN-2011. RV085.1.
  SPimsEmpPlanPWDFound = 'WARNING: Planning exists for this workspot/department-combination' + #13 +
    'on %s and/or later. Are you sure you want to change the department?';
// MRA:14-FEB-2011. RV086.4.
  SPimsSelPlantsCountry = 'The selected plants should belong to the same country!';
  SPimsSelNotPosMultExports = 'Selection not possible, multiple export types are not allowed.';
// MRA:18-APR-2011. RV089.5
  SPimsAddAllWorkspots = 'Add ALL workspots?';
// MRA:21-APR-2011. RV090.1.
  SPimsFinishLastTransactionFirst = 'Please confirm/undo the last copy-action first!';  
  SPimsConfirmLastTransactionFirst = 'The last copy-action is not confirmed yet.' + #13 +
                                     'Please confirm/undo it first!' + #13 +
                                     'Yes=Confirm No=Undo';
  SPimsConfirmLastTransaction = 'Do you want to CONFIRM the last copy-action?';
  SPimsUndoLastTransaction = 'Do you want to UNDO the last copy-action?';
// MRA:26-APR-2011. RV089.1.
  SPimsYes = '&Yes';
  SPimsNo = '&No';
  SPimsAll = '&All';
  SPimsNoToAll = 'N&o To All';
// MRA:27-APR-2011. RV091.1
  SPimsPasteOvertimeDef = 'Paste overtime definitions from "%s"';
  SPimsOvertimeDefFound = 'Definitions found, these will be deleted. Proceed Yes / No?';
// MRA:24-MAY-2011. RV092.6
  SPimsEndTimeError = 'End time should be bigger than start time!';
// MRA:26-MAY-2011. RV093.1.
  SPimsFooterTotalContractGroup = 'Total Contr. gr.';
  SPimsFooterContractGroup = 'Contractgroup';
// MRA:1-JUN-2011. RV093.3.
  SPimsLoggingEnable = 'To enable/disable logging please restart Pims.';
// MRA:6-JUN-2011. RV093.3.
  SPimsCleanUpEmpHrsLog = 'Cleanup Employee Hours Logging older than %d weeks?';
  SPimsCleanUpTRSLog = 'Cleanup Timerecording Scans Logging older than %d weeks?';
// MRA:6-JUL-2011. RV094.5.
  SPimsFilenameAFAS = 'Salarymutations ABS PIMS';
// MRA:11-JUL-2011. RV094.7.
  SPimsPlantDeleteNotAllowed = 'It is not allowed to delete a plant.';
  SPimsPlantUpdateNotAllowed = 'It is not allowed to change a plant.';
  SPimsEmployeeDeleteNotAllowed = 'It is not allowed to delete an employee.';
  SPimsEmployeeUpdateNotAllowed = 'It is not allowed to change an employee.';
  SPimsWorkspotDeleteNotAllowed = 'It is not allowed to delete a workspot.';
  SPimsWorkspotUpdateNotAllowed = 'It is not allowed to change a workspot.';
// MRA:11-JUL-2011. RV04.8.
  SPimsEmployeeNotActive = 'Employee is non-active.';
// MRA:19-OCT-2011. RV099.1.
  SPimsPleaseFillBothAbsReasons = 'Please fill in both absence reasons.';
  SPimsLogStartTFTHolCorrection = 'Start TFT/Vacation correction.';
  SPimsLogEndTFTHolCorrection = 'End TFT/Vacation correction.';
  SPimsLogChangesMade = 'Number of changes made: %d';
// MRA:28-OCT-2011. RV100.2. 20011796.2. Changed.
  SPimsEarnedTFT = 'TFT Earned';
  SPimsEarnedTFTSign = 'TFT-E';
// MRA:9-JAN-2012. RV103.4.
  SPimsHolidayCardTitle = 'Holiday Card';
// MRA:13-JAN-2012. RV103.4.
  SJanuaryS = 'Jan.';
  SFebruaryS = 'Febr.';
  SMarchS = 'March';
  SAprilS = 'Apr.';
  SMayS = 'May';
  SJuneS = 'June';
  SJulyS = 'July';
  SAugustS = 'Aug.';
  SSeptemberS = 'Sept.';
  SOctoberS = 'Oct.';
  SNovemberS = 'Nov.';
  SDecemberS = 'Dec.';
// 20011796.2.
  SPimsRepHolCardOpeningBalance = 'Opening balance:';
  SPimsRepHolCardUsed = 'Used:';
  SPimsRepHolCardAvailable = 'Available:';
  SPimsRepHolCardTFTEarned = 'TFT-E Earned:';
  SPimsRepHolCardTUsed = 'T Used:';
  SPimsRepHolCardTotAvail = 'Total Available:';
  SPimsRepHolCardLegenda = 'T = Time for Time used, TFT-E = Time for Time earned, H = Holiday';
  SPimsRepHolCardLegendaTFTEarned = 'TFT-E = Time for Time earned';
  SPimsInvalidCheckDigits = 'Invalid checkdigits! Please enter a value from 10 to 99.';
  SPimsEnteredDateRemark = 'Please enter a Date-IN equal to selected date or next date.';
  SPimsShifts = 'Shifts'; // 20013551
  SPimsWorkspotHeader = 'Workspot'; // 20013551
  // 20013723
  SPimsExportPeriod = 'Export overtime';
  SPimsExportMonth = 'Export payroll';
  // 20013288.130
  SPimsExportEasyLaborChoice = 'Please make a choice about what to export.';
  // 20014002
  SPimsTotalWorkspot = 'Total Workspot';
  // 20014037.60
  SPimsPimsUser = 'Pims User';
  SPimsWorkstation = 'Workstation';
  // TD-22429
  SPimsStart = 'Start';
  // TD-22475
  SPimsPasteDatacolConnection = 'Paste datacol connections from "%s"';
  SPimsDatacolConnectionFound = 'DatacolConnections found, these will be deleted. Proceed Yes / No?';
  // TD-22503
  SChangeEmplAvail = 'Employee Availability was found, this will be deleted for non-available. Do you want to continue?';
  // SO-20014715
  SPimsReportYes = 'Yes';
  SPimsReportNo = 'No';
  SPimsReportTill = 'till';
  SPimsReportLate = 'late';
  SPimsReportEarly = 'early';
  // TD-23315 Changed this message:
  SPIMSTimeIntervalError = 'Starting time is less than previous end time,' + #13 +
   'or end time is higher then next start time.';
  // 20013196
  SPimsDeleteLinkJob = 'There is a link job defined. This will be removed. Proceed Yes/No ?';
  SPimsLinkJobExist = 'It is not allowed to define more than 1 link job per workspot.' + #13 +
    'This link job will not be saved.';
  // 20011800
  SPimsOK = '&Ok';
  SPimsFinalRun = '&Final Run';
  SPimsFinalRunWarning = 'FINAL RUN, ARE YOU SURE?' + #13#13 +
    'Any mutations over this period cannot be made afterwards.';
  SPimsFinalRunExportDateWarning = 'FINAL RUN' + #13#13 +
    'Selected export-period is already exported (partly or completely).' + #13#13 +
    'It is NOT allowed to make this export.';
  SPimsFinalRunExportDateWarningAdmin = 'FINAL RUN' + #13#13 +
    'Selected export-period is already exported (partly or completely).' + #13#13 +
    'Do you want to start the export?';
  SPimsTestRunExportDateWarning = 'TEST RUN' + #13#13 +
    'Selected export-period is already exported (partly or completely).' + #13#13 +
    'Do you want to start the export?';
  SPimsFinalRunExportGapWarning = 'FINAL RUN' + #13#13 +
    'There is a gap of %d days between last-export-date' + #13 +
    'and start-date of selection.' + #13#13 +
    'It is NOT allowed to make this export.';
  SPimsFinalRunExportGapWarningAdmin = 'FINAL RUN' + #13#13 +
    'There is a gap of %d days between last-export-date' + #13 +
    'and start-date of selection.' + #13#13 +
    'Do you want to start the export?';
  SPimsTestRunExportGapWarning = 'TEST RUN' + #13#13 +
    'There is a gap of %d days between last-export-date' + #13 +
    'and start-date of selection.' + #13#13 +
    'Do you want to start the export?';
  SPimsCloseScansSkipped = 'WARNING: Some scans could not be closed because of a compare' + #13 +
    'with last-export-date.';
  SPimsFinalRunAddAvaililityNotAllowed = 'Adding availability for this week is not allowed' + #13 +
    'because it has already been exported.';
  SPimsFinalRunDateChange = 'WARNING: The date has been changed to a date after the last-export-date.';
  SPimsFinalRunAvailabilityChange = 'WARNING: Some employee availability could not be changed because of the last-export-date.';
  SPimsFinalRunTestRun = 'TEST RUN';
  SPimsFinalRunDeleteNotAllowed = 'WARNING: Delete is not allowed, because it was already exported.';
  SPimsFinalRunOnlyPartDelete = 'WARNING: Only a part of the week will be deleted, because some part was already exported.';
  SPimsFinalRunShiftSchedule = 'WARNING: Not all could be copied because some part was already exported.';
  // SO-20014442
  SPimsYearMonth = 'Year/Month';
  SPimsStartEndYearMonth = 'End year/month should be bigger than start year/month!';
  SPimsOnlySANAEmps = 'Only SANA employees';
  SPimWrongYearSelection = 'Wrong year-selection: The From-To-Year must be the same!';
  // 20015178
  SPimsPleaseEnterFakeEmpNr = 'Please enter a unique Fake Employee Number,' + #13 +
    'that will be used in Personal Screen.';
  SPimsFakeEmpNrInUse = 'The entered Fake Employee Number is already in use' + #13 +
    'by another workspot. Please enter a different number.';
  // 20015223
  SPimsWorkspots = 'Workspots';
  SPimsPlantTitle = 'Plant';
  SPimsAllWorkspots = '<All>';
  SPimsMultipleWorkspots = '<Multiple>';
  // 20013476
  SPimsValueSampleTimeMins = 'Sample Time: Invalid value entered. Please enter a value from 1 to 30 minutes.';
  // 20015586
  SPimsIncludeDowntime = 'Include down-time quantities:';
  SPimsIncludeDowntimeYes = 'Yes';
  SPimsIncludeDowntimeNo = 'No';
  SPimsIncludeDowntimeOnly = 'Show only down-time';
  // TD-25948
  SPimsOpenCSV = 'Open CSV file?';
  SPimsCannotOpenCSV = 'Please install Microsoft Excel (or a similar program).';
  // PIM-53
  SPimsNoWorkingDays = 'Please select one or more working days.';
  SPimsAddHoliday = 'Do you want to add %d absence day(s) to employee availability?';
  // PIM-53
  SPimsAddHolidayEmplAvail = 'Please enter a valid absence reason code.';
  SPimsAddHolidayNoStandAvail = 'Action is aborted: There is no standard availability defined for the plant/shift-combination!';
  // PIM-52
  SPimsWorkScheduleDetailsExists = 'Work Schedule Details already exist! Overwrite?';
  SPimsWorkScheduleWrongRepeats = 'Wrong value for Weekly Repeats! Please enter a value between 1 and 26.';
  SPimsWorkScheduleInUse = 'Work Schedule is still in use (Employee Contract) and cannot be deleted!';
  SPimsInvalidShift = 'Please enter a valid shift.';
  SPimsReady = 'Ready';
  SPimsPlanningMessage1 = 'Not processed! For employee';
  SPimsPlanningMessage2 = 'and date';
  SPimsPlanningMessage3 = 'there was planning found.';
  SPimsNoStandAvailFound1 = 'Error: No standard availability found for employee';
  SPimsNoStandAvailFound2 = 'and shift';
  SPimsProcessWorkScheduleMessage1 = 'Process Workschedule for employee';
  SPimsProcessWorkScheduleMessage2 = 'and referencedate';
  // PIM-23
  SPimsTotals = 'Totals';
  SPimsOTDay = 'Day';
  SPimsOTWeek = 'Week';
  SPimsOTMonth = 'Month';
  SPimsMO = 'MO';
  SPimsTU = 'TU';
  SPimsWE = 'WE';
  SPimsTH = 'TH';
  SPimsFR = 'FR';
  SPimsSA = 'SA';
  SPimsSU = 'SU';
  SPimsNormalHrs = 'Normal hours per day:';
  SPimsOTPeriod = 'Period';
  SPimsOTPeriodStarts = 'Starts in week';
  SPimsOTWeeksInPeriod = 'Weeks in period:';
  SPimsOTWorkingDays = 'Working days:';
  SPimsCGRound = 'Round';
  SPimsCGTRunc = 'Trunc';
  SPimsCGMinute = 'minutes';
  SPimsCGTo = 'to';
  SPimsStandStaffAvailUsed = 'NOTE: For one or more days a fixed absence reason is used'#13'based on standard staff availability.'#13'This will not be overwritten.';
  SPimsShiftDate = 'Shift Date';
  // ABS-27052
  SPimsSelectPrinter = 'Select Printer';
  SPimsSelectDefaultPrinter = 'Select a printer as default printer';
  SPimsNoPrinterFound = 'No printers found!';
  SPimsPleaseSelectPrinter = 'Please select a printer.';
  // PIM-52
  SPimsNoCurrentEmpContFound = 'No current employee contract found for employee';
  // PIM-203
  SPimsHostPortExists = 'This Host+Port-combination already exists for another workspot.';
  // PIM-174
  SPimsCopied = 'Copied:';
  SPimsRows = 'row(s).';
  SPimsEmp = 'Employee';
  SPimsEmpContract = 'Employee Contract';
  SPimsWSPerEmp = 'Workspots per Employee';
  SPimsStandStaffAvail = 'Standard Staff Avail.';
  SPimsShiftSched = 'Shift Schedule';
  SPimsEmpAvail = 'Employee Avail.';
  SPimsStandEmpPlan = 'Employee Standard Planning';
  SPimsEmpPlan = 'Employee Planning';
  SPimsAbsenceCard = 'Absence card';
  SPimsRecordKeyDeleted = 'This record is deleted by another user.';
  SPimsWorkScheduleWrongStartDate = 'Reference week must be a date that starts at the first day of the week.'#13'This has been corrected.';
  SPimsRoamingWorkspotExists = 'There is already another workspot defined as roaming.';
  SPimsHourlyWage = 'Hourly Wage:';
  SPimsTotalExtraPayments = 'Total Extra Payments';
  SPimsTBPEmp = 'Timeblocks per Employee';

{$ELSE}
{$IFDEF PIMSNORWEGIAN}

  SPimsHomePage = 'Pims hjemmeside vil bli implementert snart!';
  SPimsIncorrectPassword = 'Feil passord.'#13'Skriv inn et nytt passord eller'+
    'Avbryt.';
  SPimsNoEmptyPasswordAllowed = 'Et tomt passord er ikke tillatt.'#13'Skriv inn '+
    'et nytt passord eller Avbryt.';
  SPimsPasswordChanged = 'Nytt passord er lagret';
  SPimsPasswordNotChanged = 'Passord er ikke endret.';
  SPimsPasswordNotConfirmed = 'Passord og bekreftelsen '+
    'samsvarer ikke.'#13'Skriv inn et nytt passord eller Avbryt.';
  SPimsSaveChanges = 'Lagre endringene?';
  SPimsRecordLocked = 'Denne posten brukes av en annen bruker';
  SPimsOpenXLS = '�pne Excel fil?';
  SPimsOpenHTML = '�pne HTML fil?';
  SPimsNotUnique = 'Ikke unik';
  SPimsNotFound = 'Ingenting funnet!';
  SPimsNotEnoughStock = 'Det er ikke nok produkter p� lager!'#13'Send ut '+
    'varen likevel?';
  SPimsNoItems = 'Ingen produkter enda';
  SPimsNoEmployee = 'Det finnes ingen ansatte!';
  SPimsNoCustomer = 'Det finnes ingen kunder!';
  SPimsLoading = 'Laster ';
  SPimsKeyViolation = 'Denne linjen finnes allerede.';
  SPimsForeignKeyPost = 'Denne posten har en forbindelse til andre poster. Din '+
    'foresp�rsel nektes.';
  SPimsForeignKeyDel = 'Denne posten har en forbindelse til andre poster. Din '+
    'foresp�rsel nektes.';
  SPimsFieldRequiredFmt = 'Vennligst fyll ut obligatorisk felt %s.';
  SPimsFieldRequired = 'Vennligst fyll ut obligatorisk felt.';
  SPimsDragColumnHeader = 'Dra en kolonne header hit for � gruppere den kolonnen.';
  SPimsDetailsExist = 'Denne posten har en forbindelse til andre poster. Vennligst '+
    'fjern posten f�rst.';
  SPimsDBUpdate = 'Oppdater Pims database';
  SPimsDBUpdateButtonCaption = '&Kj�r Pims oppdatering';
  SPimsDBUpdateConflict = 'Pims oppdaget en konflikt mens du oppdaterte '+
    'database.'#13'Vennligst ring til ABS-helpdesken og oppgi denne feilmeldingen.';
  SPimsDBNeedsUpdating = ' Pims oppdaget p� eldre databaseversjon.'#13+
   ' Pims trenger � oppdatere databasen din for � kj�re riktig.';
  SPimsCurrentCode = 'N�v�rende vare kode. ';
  SPimsContinue = 'Fortsett?';
  SPimsConfirmCancelNote = '�nsker du � kansellere denne leveringsseddelen?';
  SPimsConfirmDeleteNote = '�nsker du � slette denne leveringsseddelen?';
  SPimsConfirmDeleteLicense = '�nsker du � slette denne Pims Lisensen?';
  SPimsConfirmDelete = '�nsker du � slette gjeldende post?';
  SPimsCannotPrintLabel = 'Du kan ikke skrive ut en etikett for det valgte '+
    'produktet.';
  SPimsCannotPlaceFlag = 'Du kan ikke markere det valgte '+
    'Produktet.';
  SPimsCannotOpenXLS = 'Vennligst installer Microsoft Excel.';
  SPimsCannotOpenHTML = 'Vennligst installer en Internett leser p� din PC.';

  // FOR PIMS:
  SSelectDummyWK = 'Du kan ikke velge en dummy Arbeidsposisjon!';
  SPimsPercentageNotCorrect = 'Feil: Total prosentandel for Arbeidsposisjon er ikke 100%';
  SPimsTimeMustBeYounger = 'Feil: Starttidspunktet m� v�re mindre enn sluttidspunktet!';
  SPimsNoMaster = 'Det finnes ingen prosessenheter';
  SPimsNoSubDetail = 'Det er ingen avdelinger';
  SPimsNoDetail = 'Det finnes ingen Arbeidsposisjoner';
  SPIMSContractTypePermanent = 'Permanent';
  SPIMSContractTypeTemporary = 'Midlertidig';
  SPIMSContractTypePartTime = 'Eksternt';
  SPIMSContractOverlap = 'Det er en overlapping for denne kontrakten med den forrige kontrakt';
  SPimsTBDeleteLastRecord = 'Bare den siste posten kan slettes';
  SPimsTBInsertRecord = 'Kun %d Tidsblokker er tillatt';
  SPimsStartEndDate = 'Startdato er st�rre enn sluttdato';
  SPimsStartEndTime = 'Starttiden er st�rre enn sluttidspunktet';
  SPimsStartEndDateTime = 'Startdato tid er st�rre enn sluttdato tid';
  SPimsStartEndActiveDate = 'Start aktiv dato er st�rre enn sluttdato';
  SPimsStartEndInActiveDate = 'Start inaktiv dato er st�rre enn slutt inaktivt date';
  SPimsWeekStartEnd = 'Slutt uke < Start uke!';
  SPimsBUPerWorkspotPercentages = 'Totalt prosenter b�r v�re 100%,  ' +
    'Du kan ikke lukke skjemaet';
  SPimsTBStartTime = 'Starttid TidsBlokk <Starttid Skift p� ';
  SPimsTBEndTime = 'Slutttid TidsBlokk> Slutttid Skift p� ';
  SPimsBUPerWorkspotInsert = 'Det er ingen Arbeidsposisjoner der du kan ikke f�re inn poster';
  SPimsDateinPreviousPeriod =  'Det er allerede en skanning som inkluderer denne tiden';
  SPimsExistJobCodes = 'Dette kan ikke endres, det er allerede definert jobbkoder';
  SPimsExistBUWorkspot = 'Du kan ikke endre denne, det er relaterte poster i Business Units per Arbeidsposisjon';
  // Time Recording
  SPimsIDCardNotFound = 'ID-kort ikke funnet';
  SPimsIDCardExpired	= 'ID-kort er utl�pt';
  SPimsEmployeeNotFound = 'Ansatte ikke funnet';
  SPimsInactiveEmployee = 'Ansatt er inaktiv';
  SPimsScanningsInMinute = 'Ikke tillate flere scanninger i l�pet av ett minutt';
  SPimsStartDay = 'Start p� dagen';
  SpimsEndDay = 'Slutt p� dagen';
  SPimsNoWorkspot = 'Ingen Arbeidsposisjon definert p� gjeldende arbeidsstasjon';
//Workspot
  SPimsNoJob = 'Ingen jobber definert p� n�v�rende Arbeidsposisjon';
  SPimsDeleteJob = 'Det er jobbkoder tildelt, disse vil bli slettet!';
  SPimsDeleteBusinessUnit = 'Det er tildelt forretningsenheter, disse vil bli slettet!';
  SPimsDeleteJobBU =
    'Alle tilkoblede poster med denne arbeidsposisjon vil bli slettet, slett arbeidsposisjon ? ';
  SShowBUWK         = 'Vis forretningsenheter per arbeidsposisjon';
  SShowJobCode = 'Vis jobbkoder';
//workspot per workstation
  SPimsSameSourceAndDest = 'Kilden og destinasjons PC navnet kan ' +
   	'v�re likt';
  // Selector Form - Grid Column name
  SPimsColumnPlant 	= 'Anlegg';
  SPimsColumnWorkspot = 'Arbeidsposisjon';
  SPImsColumnCode = 'Kode';
  SPimsColumnDescription = 'Beskrivelse';
  // Selector Form - Captions
  SPimsWorkspot = 'Velg arbeidsposisjon';
  SPimsJob 	= 'Velg jobb';
  SPimsJobCodeWorkSpot = 'Du kan ikke lukke skjemaet, f�rst minst �n jobbkode defineres';
  SPimsInterfaceCode  =
   'Du kan ikke lukke skjemaet, f�rst m� minst �n grensesnittkode defineres, ulik grensesnittkoden for jobben';
  SPimsOtherInterfaceCode  = 'Vennligst definer en annen grensesnittkode enn grensesnittkoden tilh�rende jobbkoden';
  SPimsNoOtherInterfaceCodes = 'Det er ingen andre grensesnittkoder definert, du kan ikke �pne skjemaet';

  SPimsSaveBefore = 'Vennligst lagre posten f�r';
  SPimsNoPlant = 'Det er ikke oppf�rt noen anleggs ID';
  SPimsScanDone = 'Skanning utf�rt';
  SPimsScanError = 'Skanning IKKE utf�rt: Data-Base FEIL';
//  SPimsNoEmployee = 'Ingen Ansatte tilgjengelig';
// Time Recording Scanning
  SPimsNoShift = 'Skift er ikke definert';
  SPimsNoCompletedScans = 'Fullf�rte skanninger kan ikke endres / slettes';
  SPimsNonScanningStation = 'Dette er ikke en stasjon for tids registrering';
  SPimsScanTooBig = 'Skanneperiode lengre enn 12 timer er ikke tillatt';
  SPimsUseJobCode = 'Vennligst velg en jobbkode, arbeidsstasjon er definert for � bruke jobbkoder';
  SPimsNoMatch = 'Ingen kobbling mellom Ansatt + ID-kort';
// team per department - extra check
  SPimsNoMorePlantsinTeam =
    'Alle avdelinger av en gruppe skal tilh�re samme anlegg!';
  SPimsTeamPerEmployee =
    'Avdelingen for den ansatte b�r v�re i gruppen!';

// Hours per Employee
  SPimsAutomaticRecord = 'Automatisk innsatt post kan ikke endres ';
  SPimsNegativeTime = 'Dobbeltklikk for � angi en negativ tid (r�d farge)';
  SPimsExistingRecord = 'En post med de samme parametrene eksisterer allerede.'+#13+
  	'Vennligst velg den posten';
  SPimsProductionHour = 'Tabellen ProductionHourPerEmployee kan ikke oppdateres';
  SPimsNoTimeValue = 'Vennligst sett inn tidsverdien for minst en dag';
  SPimsSalaryHour = 'tabellen SalaryHourPerEmployee kan ikke oppdateres';
  SPimsAbsenceHour = 'Tabellen AbsenceHourPerEmployee kan ikke oppdateres';
  SPimsTo = 'Til';
  SPimsInvalidTime = 'Verdi er ikke satt! (Ugyldig tidsverdi)';
// bank holiday process
  SPimsConfirmProcessEmployee = '�nsker du � behandle alle helligdager for ' +
    '�ret til ansattes tilgjengelighet ?';
  SPimsInvalidAbsReason = 'Ikke en gyldig frav�rs�rsak!';
  SPimsEmptyAbsReason = 'Vennligst fyll ut obligatorisk felt for frav�rsgrunn!';
  SPimsEmptyDescription = 'Vennligst skriv inn obligatorisk feltbeskrivelse!';
// for reports
  SPimsReportWTR_HOL_ILL_Checks = 'Du b�r velge minst en frav�rs�rsak!';
  SPimsMaxNumberOfWeeks = 'Maksismalt antall utvalgte uker er 13!';
  SPimsEndTime = 'Slutt tid';
  SPimsStartTime = ' < Start tid';
  SPimsOn = 'P�' ;
// update database
  SPimsInvalidVersion = 'Ugyldig PIMS versjon !';
  SUpdateDatabaseFrom = 'Database vil bli oppdatert fra versjon ';
  SUpdateDatabaseTo = ' til versjon ';
// Standard staff availability
  SCheckAvailableTB = 'Tillatte verdier er * og -';
  SCheckAvailableTBNotEmpty = 'Vennligst fyll ut alle obligatoriske felt !';
  SCopySTA1 = 'Standard tilgjengelighet finnes allerede for dag ';
  SCopySTA2 = ', Skriv over ? ';
  SCopySameValues = 'Du valgte den samme posten !';
  SCopySTANotValidFrom = 'Det er ingen gyldige klokkeslett for den valgte posten!';
  SCopySTANotValidTo = 'Det er ingen gyldige klokkeslett for valgt rutenett!';
  SEmptyValues = 'Vennligst fyll ut alle feltene f�r!';
  SEmplSTARecords = 'Det er ingen standardtilgjengelighet definert for denne ansatt';
// shift schedule
  SShiftSchedule = 'Tillatte verdier er mellom 0 .. 99 eller -';
  SSHSShift = 'Tillatte verdier er bare definert pr skift';
  SSHSWeekCopy = 'Startdatoen for kopieringen skal v�re st�rre enn sluttdatoen';
  SInvalidShiftSchedule = 'Skift nummer er ikke definert eller verdier start / slutt' +
    'tiden per dag er null!';
  SEmployeeAvail_0  = 'Ansatt ';
  SEmployeeAvail_1  = ' har allerede tilgjengelighet i dette skiftet p� �r, uke og dag ';
//  SEmployeeAvail_2 = ', dag ';
  SEmployeeAvail_3 = ', Du kan ikke endre skiftnummeret.';
  SUndoChanges = 'Siste endringer vil ikke bli lagret!';
  SStartWeek = 'Start uke skal v�re st�rre enn siste uke!';
  SCopySelectionShiftSchedule_1 = 'Skift tidsplan for ansatt ';
  SCopySelectionShiftSchedule_2 = ' eksisterer allerede. skrive over?';
  SCopyFinished = 'Kopieringeb er fullf�rt!';
  SEmptyRecord = 'Det er ikke valg en ansatt!';
// employee availability
  SEmplAvail = 'Tillatte verdier er *, - eller en frav�rs grunn';
  SValidTimeBlock = 'Alle gyldige klokkeslett skal fylles ut!';
  SEmployeePlanned = 'Den ansatte er allerede planlagt, velger du og fortsette vil du slette planlegging. ' +
    '�nsker du � fortsette ?';
  SCopyFrom  = 'Vennligst velg en post!';
  SCopyTo  = 'Postene er like, velg en annen post!';
  SEMAOverwrite = 'Ansattes tilgjengelighet for ';
  SEMAShift = ' Skift ';
  SEMAExists = 'eksisterer allerede. �nsker du � skrive over?';
// staff planning
  SSelectedWorkspots = 'Det er mer enn 401 utvalgte arbeidsposisoner, bare de f�rste 401 vil ' +
    'vises ';
  SEmptyWK = 'Det er ikke valgt noen arbeidsposisjon';
  SOverplanning = 'Overplanlegging med mer enn 200%';
  SMakeAvailable = 'Gj�r tilgjengelig';
  SNotAcceptPlanned = 'Planleggingen av denne ansatte er ikke akseptert!';
  SSaveChanges = 'Lagre endringene?';
  SReadStdPlnIntoDaySelection = 'Planlegging er allerede lest for et anlegg/team/dato innenfor valgen som er gjort';
  SReadStdPlnIntoDaySelectionFinish = 'Les standard planlegging til dag planlegging er fullf�rt';
  SReadPastIntoDayPlanFinish = 'Les tidligere i dag planlegger er fullf�rt';
  SContinueYN = 'Vil du forsette?';
  SDateFromTo = 'Sluttdato b�r v�re st�rre enn startdato!';
  SDateIntervalValidate = 'Intervallet for m�ldatoen b�r v�re ikke v�re lik intervallet' +
    ' av kildedatoen';
  SNoEmployee = 'Du har ikke valgt noen ansatte';
  SOCILevel = 'Stillingsgrad';
  SPlannedLevel = 'Planlagt med niv�';
  SPlannedAllLevels = 'Planlagt ';
  SOCIAllLevel = 'Stilling  ';
  SSameRangeDate = 'Datointervallene skal ha b�r lengde!';
// report staff planning
  SStartEndYear = 'Slutt�r skal v�re st�rre enn start�r!';
  SWeek = 'Du kan ikke velge mer enn en uke!';
  SNoTeam = 'Tomt team: ';
// Workspot per employee
  SLEVELWK = 'Bare A, B, C er tillatt!';
  SInvalidWK = 'Bare A..Z, a..z, 0..9, og ''_'' er tillatt !';
// Staff Availability
  SChangePlaned = 'Ansatt er allerede planlagt, om du velger fortsett vil du slette gjeldene planlegging'+
    #13+ 'Vil du fortsette?';
  SInvalidShiftNo = 'Ugyldig skift-nummer for dag: %d';
  SInvalidAvailability = 'Ugyldig tilgjengelighet for dag: %d, TB: %d';
//  E&R
  SMoreThan = ' Mere enn ';
  SInvalidMistakeValue =  'Antall feil skal v�re st�rre enn null!';
  SNotDeleteRecord = 'Du kan ikke slette denne posten!';
  SPimsDBError = 'Rapport er �pnet p� annen stasjon eller databasefeil. '+#13+
		'Vennligst kj�r rapporten igjen';
 //
// productivity reports

  SDaySelect = 'Startdagen skal v�re mindre enn sluttdagen!';
//
// jobcode
  SInterfaceCode = 'Denne grensesnittkoden brukes allerede p� andre arbeidsposisjoner!';
  SWKInterfaceCode = 'Denne grensesnittkoden er allerede brukt for denne arbeidsposisjonen!';
  SWKRescanInterfaceCode =
   'Det er grensesnittkoder som er brukt mer enn en gang for denne arbeidsposisjonen!';
  SDeleteOtherInterfaceCode = 'Alle andre grensesnittkoder som er definert, vil bli slettet!';

//
  SExceptionDataSetReport =
   'Datafeltene er tomme, og kan ikke opprette rapport. Vennligst fyll inn feltene f�rst og gjenopprett pims';
  SExceptionDataSetForms  =
   'Datafeltene er tomme, kan ikke oppdatere rutene. Vennligst fyll inn feltene f�rst og gjenopprett pims';
// export payroll report
   SPeriodReport = 'Periode';
   SMonthReport = 'M�ned';
// report quality incentive
   SCalculationwithOvertime = 'Kvalitets kalkulasjon md overtids rapport';
// add wk into datacollection
  SADDWKDateCollection = 'Postene er lagt til';
// strings for mistake per employee
  SDate = 'Dato ';
  STotalWeek = 'Totalt i Uke ';
// report hrs per empl
  SCaptionProdRepHrs = 'Produksjonstimer per ansatt';
  SCaptionSalRepHrs =  'L�nnstimer per ansatt';
//report CompHours
  SShowActiveEmpl = 'Vis aktiv ansatt';
  SShowNotActiveEmpl = 'Vis ikke aktiv ansatt';
//report HrsWKCum
  SRepHrsCUMWK = 'Arbeidsposisjon';
  SRepHrsTotalCUM = 'Total for Arbeids posisjon';

  SRepHrsCUMWKBU = 'foretaks enhet';
  SRepHrsTotalCUMBU = 'Totalt for foretaks enheten';

  SRepHrsCUMWKPlant = 'Anlegg';
  SRepHrsTotalCUMPlant = 'Totalt for anlegg';

  SRepHrsCUMWKDept = 'Avdeling';
  SRepHrsTotalCUMDept = 'Totalt for avdeling';
  SRepJobCode = 'Jobb kode';
  SRepTotJobCode = 'Totalt for jobb kode';
// report absence
  SHeaderAbsRsn = 'Frav�rs grunn';
  SHeaderWeekAbsRsn = 'Uke';
  SHdTotalWeek = 'Totalt for uke';
  SRepExpPayroll = 'Fra';
// report absence schedule
  SReport = 'Rapport';
  SReportWorkTimeReduction = 'Arbeidstids reduksjon ';
  SReportIllness = 'Sykdom';
  SReportHoliday = 'Ferie';
  SActiveEmpl =  'Aktive ansatte';
  SInactiveEmpl = 'Inaktive ansatte';
// report
  SRepHrsPerEmplActiveEmpl = 'Vis aktive ansatte';
  SRepHrsPerEmplInActiveEmpl = 'Vis inaktive ansatte';
  SRepProdHrs = 'Produksjons timer';
  SRepNonProdHrs = 'Ikke produktive timer';
  SAll = 'Alt';
  SActive = 'Aktive';
  SInactive = 'Inaktive';
  SName = 'Navn ';
  SShortName = 'Forkortet navn';
// REPORT check list
  SShowProcessed =  'Vis behandlet';
  SShowNotProcessed  = 'Vis ikke behandlet';
//Report
  SErrorOpenRep = 'Feil n�r �pnet database, rapport kan ikke �pnes';
  SErrorCreateRep = 'Feil rapport kan ikke genereres';
//DialogProcessAbsenceHrsFRM
   SLogEmpNo = 'Funnet antall poster=';
   SLogChangedAbsenceTotal = 'Endret %d Frav�r Total-poster';
   SLogDeletedRec = 'Slettet %d Frav�r Time-poster';
   SLogEndAbsence = 'Avslutt: Definere frav�rstimer per ansatt';
   SLogStartSalaryHours = 'Startet: Definere l�nnstimer per ansatt';
   SLogStartAvailability  = 'Startet: Definere ansattes tilgjengelighet';
   SLogChangedAbsence = 'Endret %d Frav�r Time-poster';
   SlogAddedAbsence = 'Lagt til %d Frav�r Time-poster';
   SLogEndAvailability = 'Avsluttet: Definere ansattes tilgjengelighet';
   SLogStartNonScanning = 'Startet: Definere ikke-innskannet ansattes tilgjengelighet';
   SLogEndNonScanning  = 'Avsluttet: Definere ikke-innskannet ansattes tilgjengelighet';
   SLogFoundEmployeeNo = 'Funnet antall poster = %d';
   SSProcessFinished = 'Prosessen er ferdig';
// Staffplanning
   SStaffPlnEmployee = 'Ansatt';
   SStaffPlnName = 'Navn';
// Contract group form
   SOvertimeDay = 'Dag';
   SOvertimeWeek = 'Uke';
   SValidMinutes = 'Definertet minutter skal v�re mindre enn 59';
   SQuaranteedHourType = 'Vennligst velg en time type';
//
//update cascade
   SConfirmUpdateCascadeEmpl =
     'Ansatt vil bli endret for alle tilkoblede poster, oppdaterer medarbeider? ';
   SConfirmDeleteCascadeEmpl =
     'Alle tilknyttede poster med denne ansatt vil bli slettet ogs�, slett medarbeider? ';
   SEmpExist = 'Denne ansatte finnes allerede ';
//export payroll MRA:RV095.4. Give better message: 'Dato' Lagt til.
   SNotDefaultSelection = 'Ikke standard dato valg. Er du sikker?';
   SPeriodAlreadyExported = 'Denne perioden er allerede eksportert. Er du sikker ?';
// transfer free time
   SPimsDifferentYears = 'M�l�r skal v�re annerledes enn kilde�r!';
   SPimsCheckBox = 'Minst en av avmerkingsboksene m� velges!';
   SPimsProcessFinish = 'Prosessen er fullf�rt!';

   // MR:17-01-2003
   SPimsUseShift = 'Vennligst velg et skift!';
   SPimsUseWorkspot = 'Vennligst velg en arbeidsposisjon!';
//  staff planning -CAR 30-01-2003
   SPimsDayPlanning = 'Dags planlegging: ';
   SPimsTeamSelection = 'Team valg: ';
   SPimsTeamSelectionAll = 'Alle teams';
   SPimsSTDPlanning = 'Standard planlegging: ';
   SPimsStaffTo =  ' til ';
   SPimsPlant =  '      Anlegg:  ';
   SPimsShift = '      Skift: ';
   SPimsTeam = 'Team ';
   // MR:28-01-2003
   SPimsNoEmployeeLine = 'Ingen ansatt';

   SPimsNoPrevScanningFound = 'Ingen tidligere innskanning funnet';
   SpimsScanExists = 'Det er allerede en skanningspost p� den dagen for den ansatte. ' + #13 +
     'Du kan ikke endre dette.';

 //car: 27.02.2003
   SPimsEXCheckIllMsgClose =
     'Det er allerede en utest�ende sykdomsmelding,  vennlist lukk denne';
   SPimsEXCheckIllMsgStartDate =
     'Startdato b�r v�re st�rre enn sluttdatoen for siste opprettede sykdomsmelding';

   // MR:03-03-2003
   SPimsContract = 'Kontrakt';
   SPimsPlanned = 'Planlagt';
   SPimsAvailable = 'Tilgje.';

   // MR:07-03-2003
   SpimsYear = '�r';
   SPimsWeek = 'Uke';
   SPimsDate = 'Dato';
   //CAR:21-03-2003 - REPORT staff planning per day
   SPimsNewPageWK = 'Ny side pr arbeidsposisjon';
   SPimsNewPageEmpl = 'Ny side pr ansatt';
   //CAR:21-03-2003 report checklist
   SPimsAvailability = 'Tilgjengelighet: ';
   //CAR:21-03-2003 update cascade -Plant
   SConfirmDeleteCascadePlant =
     'Alle tilkoblede poster med dette anlegget vil bli slettet, slett anlegget? ';
   SConfirmUpdateCascadePlant =
     'Anlegget vil bli endret for alle tilkoblede poster, oppdaterings anlegget? ';
   SPlantExist = 'Dette anlegget eksisterer allerede ';
   //CAR: 27-03-2003
   SRepTeamTotEmpl ='Totalt for ansatt';
   //CAR 29-03-2003
   SRepHRSPerEmplCorrection = 'Manuelle endringer';
   SRepHrsPerEmplTotal = 'Totalt';
   //CAR 31-03-2003  - Report AbsenceCard
   SJanuary = 'Januar';
   SFebruary = 'Februar';
   SMarch = 'Mars';
   SApril = 'April';
   SMay = 'Mai';
   SJune = 'Juni';
   SJuly = 'Juli';
   SAugust = 'August';
   SSeptember = 'September';
   SOctober = 'Oktober';
   SNovember = 'November';
   SDecember = 'Desember';
//CAR  2-04-2003 - report Production Detail
   SRepProdDetPerformance = 'Ytelse';
   SRepProdDetBonus = 'Bonus';
// CAR 2-4-2003 - Stand Staff availability
   SStdStaffAvailPlanned =
     'Ansatt er planlagt i standard planlegging, denne vil sli slettet om du fortsetter. ' +
     'Vil du fortsette ?';
//CAR 7-4-2003
   SPimsJobs  = 'Jobb kode';
//CAR 15-4-2003
  SPimsStartEndDateTimeEQ = 'Startdatoen er st�rre eller er lik med sluttdatoen';
  SPimsStartEndTimeNull = 'Starttid eller sluttid er tom, vil du fortsette?';
// MR:24-04-2003
  SPimsNoData = 'Ingen Data';
//CAR 5-5-2003
  SPimsAbsence = 'Frav�r';
  SPimsDifference = 'Differanse';
// car 12-5-2003
  SExportDescEMPL = 'Ansatt beskrivelse';
  SExportDescDept = 'Avdelnings beskrivelse';
  SExportDescPlant = 'Anleggs beskrivelse';
  SExportDescShift = 'Skift beskrivelse';
//
  SDummyWK = 'Dette er en virituell arbeidsposisjon, dette kan ikke lagres';
// CAR 28-8-2003 - Stand Staff availability
   SStdStaffAvailFuturePlanned =
     'Ansatt er allerede planlagt i dette skiftet. Fortsett vil slette fremtidig planlegging. ';
// 9-4-2003
  SPimsInvalidWeekNr = 'Ugyldig ukenummer!';
// 9-4-2003
  SPimsHoursPerDayProduction = 'Produksjon';
  SPimsHoursPerDaySalary = 'L�nn ';
  // MR:12-09-2003
  SPimsCompletedToNotScans = 'Du kan ikke endre en fullf�rt skanning'#13'til en ikke-fullf�rt en.';
// user rights
  SAplErrorLogin = 'Ugyldig brukernavn eller passord!';
  SAplNotConnect = 'Ugyldig tilkobling til database!';
  SAplNotVisible = 'Denne brukeren kan ikke �pne applikasjonen';
// MR:3-11-2003 Moved from 'UStrings.pas'
  LSPimsEnterPassword = 'Oppgi passord';
// MR:11-11-2003
  SPimsEmployeesAll = 'Alle ansatte';
  SPimsEmployeesOnWrongWorkspot = 'P� feil arbeidsposisjon';
  SPimsEmployeesNotScannedIn = 'Ikke skannet inn';
  SPimsEmployeesAbsentWithReason = 'Frav�rende med grunn';
  SPimsEmployeesAbsentWithoutReason = 'Frav�rende uten grunn';
  SPimsEmployeesWithFirstAid = 'Industrivern';
  SPimsEmployeesTooLate = 'For sent';
// 550119 - Workspot per Employee form
  SPimsLevel = 'Niv�';
// Car: 15-1-2004
  SPimsEmplContractInDatePrevious =
   'Det er allerede en kontrakt som inneholder denne datoen';
// 550287
// Contract group form
  STruncateHrs = 'Avkortet';
  SRoundHrs = 'Rundet';
//550124
  SPimsPieces = 'Delene';
  SPimsWeight = 'Vekter';
// MR:03-05-2004 Order 550313
  SPimsCleanUpProductionQuantities = 'Rydd opp produsjons antallet eldre enn %d uker?';
  SPimsCleanUpEmployeeHours = 'Rydd opp ansattes rbeidstimer  eldre enn %d uker?';
  SPimsCleanUpTimerecordingScans = 'Rydd opp tids stemplinger eldre enn %d uker?';
// MR:17-05-2004 Order 550315
  SPimsLabelCode = 'Kode';
  SPimsLabelInterfaceCode = 'Grensesnittkode';
// MR:26-07-2004 Order 550327
  SPimsNotPlannedWorkspot = 'Ikke planlagt';
// MR:09-08-2004 Order 550336
  SNumber = 'Nummer';
// MR:13-10-2004
  SPimsGeneralSQLError = 'BDE rapporterte en generell SQL-feil';
// MR:26-10-2004
  SPimsEfficiencyAt = 'Effiktivitet p�';
  SPimsEfficiencySince = 'Effektivitet siden';
// MR:28-10-2004
  SPimsConnectionLost = 'Feil: Databaseforbindelse mistet. Vennligst start programmet p� nytt';
// MR:10-12-2004
  SSHSWeekDatesCross = 'Feil: Utvalgte fra / til datoer vil krysse hverandre!';
// MR:15-12-2004
  SPimsHours = 'Timer';
  SPimsDays = 'dager';
  SPimsBonus = 'bonus';
  SPimsEfficiency = 'Effektivitet';
// MR:11-01-2005
  SPimsTotalProduction = 'Total produksjon';
// MR:11-01-2005
  SPimsProductionOutput = 'Produksjon ut';
  SPimsChartHelp = 'velg tid: klikk p� grafen '; //, flytt: H�yreklikk mellom graf-elementer';
  SPimsInvalidScan = 'Feil: Du har angitt en ugyldig skanning.';
// MR:11-01-2005 -> Pims Oracle!
  SPimsScanOverlap = 'Feil: Scan har feil tidsintervall.';
  SPimsMaxSalBalMin = 'Max.Sal.Balance';
// MR:16-06-2005
  SPimsConfirmProcessEmployeeSelectedDate = '�nsker du � behandle den valgte datoen ' + #13 +
    '%s' + #13 + 'til den ansattes tilgjengelighet ?';
// MR:20-06-2005
  SPimsNoBankHoliday = 'Vennligst velg en nasjonalferie dato';
// MR:27-09-2005
  SPimsUseJobcodesNotAllowed = 'Det er ikke fullf�rte skanninger for denne arbeidsposisjon. ' + #13 +
    'Endringer er ikke tillatt.';
// MR:14-11-2005
  SPimsUnknownBusinessUnit = 'Ukjent foretaks enhet';
// MR:07-12-2006
  SOvertimeMonth = 'M�ned';
// MR:05-01-2007
  SErrorWrongTimeBlock = 'FEIL: Feil tidsblokk for ansatt';
  SErrorDuringProcess = 'En feil oppstod! Vennligst se feilmeldinger ovenfor.';
  SSProcessFinishedWithErrors = 'Prosessen ble fullf�rt med feil!';
// MR:26-01-2007
  SPimsMonthGroupEfficiency = 'M�nedlig gruppeeffektivitet';
  SPimsGroupEfficiency = 'M�nedlig effektivitet';
// MR:14-SEP-2009
  SPimsTemplateYearCopyNotAllowed = 'Det er ikke tillatt � kopiere til dette �ret!';
  SPimsTemplateYearSelectNotAllowed = 'Det er ikke lov � velge dette �ret!';
// MRA:8-DEC-2009 RV048.1.
  SPimsEmpPlanPWD = 'Det er ikke tillatt � endre denne avdelingen, ' + #13 +
    ' fordi ansatte er planlagt p� denne kombinasjonen av arbeidsposisjon / avdeling.';
// MRA:19-JAN-2010.
  SPimsNrOfRecsProcessed = 'Antall prosesser behandlet: ';
// MRA:22-FEB-2010.
  SPimsNoShiftScheduleFound = 'Det er ingen skiftplan definert for den valgte uken.';
// MRA:18-MAY-2010. Order 550478.
  SPimsDeleteMachineWorkspotsNotAllowed = 'Slett ikke mulig! Det er arbeidsposisjoner knyttet til denne maskinen.';    
  SPimsDeleteMachineJobsNotAllowed = 'Ikke mulig � slette! Det er jobber definert for denne maskinen.';
  SPimsUpdateMachineCodeNotAllowed = 'Maskinen er koblet til arbeidsposisjon(er). Endring av maskinkoden er derfor ikke tillatt.';
  SPimsUpdateMachineJobCodeNotAllowed = 'Maskinsjobb er i bruk av en arbeidsposisjon-jobb. Det er derfor ikke tillatt � endre maskinens jobbkode.';      
// SO:07-JUN-2010.
  SPimsIncorrectHourType = 'Vennligst skriv inn en riktig timetype !!';
  SPimsScanLongerThan24h = 'Skanningen kan ikke vare lenger enn 24 timer!!';
// MRA:30-JUN-2010. Order 550478.
  SPimsMachineWorkspotNonMatchingJobs = 'ADVARSEL: Det er ikke-matchende jobber for denne arbeidsposisjon for denne maskinen. ' + #13 + 'Pass p� at jobbene stemmer overens med arbeidsplasser som er knyttet til denne maskinen!';
  SPimsPleaseDefineJobForMachine = 'ADVARSEL: Vennligst gj�r endringer for jobbkoder p� maskinniv�.';
  SPimsDeleteNotAllowedForMachineJob = 'Dette er en maskinjobb. Sletting er ikke tillatt her, bare p� maskinniv�.';
// MRA:24-AUG-2010. Order 550497. RV067.MRA.8.
  SPimsAbsenceReasons = 'Frav�rs grunn';
  SPimsHourTypes = 'Time typer';
  SPimsAbsenceTypes = 'Frav�rs typer';
// MRA:31-AUG-2010. RV067.MRA.21
  SPimsDescriptionNotEmpty = 'Vennligst skriv inn en beskrivelse, den kan ikke st� tom!';
  SPimsCounterNameFilled = 'Tellernavnet skal fylles hvis telleren er aktiv!';
// MRA:21-SEP-2010. RV067.MRA.34
  SPimsPaste = 'Lim inn';
  SPimsPasteExceptHours = 'Lim inn eksepsjonelle timer fra "%s"';
  SPimsDefExceptHoursFound = 'Definisjoner funnet, disse blir slettet. Fortsett Ja / Nei?';
// MRA:4-OCT-2010. RV071.3. 550497
  SPimsCountersHaveBeenUpdated = 'Regnere er oppdatert.';
  SPimsProcessToAvailIsReady = 'Prosess for tilgjengelighet er klar.';
  SPimsRecalcHoursConfirm = 'Rekalkulering av timer er inkludert. Fortsette?';
// MRA:11-OCT-2010. RV071.10. 550497
  SPimMaxSatCreditReached = 'Maksimum l�rdags tilleg er n�dd!';
// MRA:1-NOV-2010.
  SPimsStartEndDateSmaller = 'Startdato er mindre enn sluttdato' + #13 +
     'av en eksisterende sykdomsmelding for samme ansatt.';
// MRA:8-NOV-2010.
// MRA:1-DEC-2010. RV082.4. Text changed.
  SPimsIsNoScannerWarning =
    'ADVARSEL: Denne ansatt skanner ikke inn og av typen: ' + #13 +
    'Autom. skanning basert p� standard planlegging.' + #13 +
    'Oppdatering av skanning kan f�re til feil timer! ' + #13 +
    'Endringer er ikke tillatt!';
// MRA:17-JAN-2011. RV085.1.
  SPimsEmpPlanPWDFound = 'ADVARSEL: Planleggingen eksisterer for denne kombinasjonen av arbeidsposisjon / avdeling' + #13 +
    'p� %s og / eller senere. Er du sikker p� at du vil bytte avdelingen?';
// MRA:14-FEB-2011. RV086.4.
  SPimsSelPlantsCountry = 'De valgte anleggene skal tilh�re det samme landet!';
  SPimsSelNotPosMultExports = 'Valg ikke mulig, flere eksporttyper er ikke tillatt.';
// MRA:18-APR-2011. RV089.5
  SPimsAddAllWorkspots = 'Legg til ALLE arbeidsposisjoner?';
// MRA:21-APR-2011. RV090.1.
  SPimsFinishLastTransactionFirst = 'Vennligst bekreft / avbryt det siste kopieringsvalget f�rst!';  
  SPimsConfirmLastTransactionFirst = 'Den siste kopieringsvalget er ikke bekreftet.' + #13 +
                                     'Vennligst Bekreft / avbryt det f�rst!' + #13 +
                                     'Ja=Bekreft Nei=Avbryt';
  SPimsConfirmLastTransaction = 'Vil du BEKREFTe det siste kopieringsvalget?';
  SPimsUndoLastTransaction = 'Vil du AVBRYTE det siste kopieringsvalget?';
// MRA:26-APR-2011. RV089.1.
  SPimsYes = '&Ja';
  SPimsNo = '&Nei';
  SPimsAll = '&Alle';
  SPimsNoToAll = 'Nei Til Alle';
// MRA:27-APR-2011. RV091.1
  SPimsPasteOvertimeDef = 'Lim inn overtidsdefinisjoner fra "%s"';
  SPimsOvertimeDefFound = 'Definisjoner funnet, disse blir slettet. Fortsett Ja / Nei?';
// MRA:24-MAY-2011. RV092.6
  SPimsEndTimeError = 'Sluttidspunktet skal v�re st�rre enn starttidspunktet!';
// MRA:26-MAY-2011. RV093.1.
  SPimsFooterTotalContractGroup = 'Total Kontraksgruppe';
  SPimsFooterContractGroup = 'Kontraktsgruppe';
// MRA:1-JUN-2011. RV093.3.
  SPimsLoggingEnable = 'For � aktivere / deaktivere logging, start p� nytt Pims.';
// MRA:6-JUN-2011. RV093.3.
  SPimsCleanUpEmpHrsLog = 'Rydd opp i Ansattes Timer Logg eldre enn %d uker?';
  SPimsCleanUpTRSLog = 'Rydd opp i tidsinnscanning, Logg eldre enn %d uker?';
// MRA:6-JUL-2011. RV094.5.
  SPimsFilenameAFAS = 'L�nnsmutasjoner ABS PIMS';
// MRA:11-JUL-2011. RV094.7.
  SPimsPlantDeleteNotAllowed = 'Det er ikke tillatt � slette et anlegg.';
  SPimsPlantUpdateNotAllowed = 'Det er ikke tillatt � endre et anlegg.';
  SPimsEmployeeDeleteNotAllowed = 'Det er ikke tillatt � slette en ansatt.';
  SPimsEmployeeUpdateNotAllowed = 'Det er ikke tillatt � endre en ansatt.';
  SPimsWorkspotDeleteNotAllowed = 'Det er ikke tillatt � slette en arbeidsposisjon.';
  SPimsWorkspotUpdateNotAllowed = 'Det er ikke tillatt � endre en arbeidsposisjon.';
// MRA:11-JUL-2011. RV04.8.
  SPimsEmployeeNotActive = 'Ansatte er ikke aktiv.';
// MRA:19-OCT-2011. RV099.1.
  SPimsPleaseFillBothAbsReasons = 'Vennligst fyll inn begge frav�rsgrunnene.';
  SPimsLogStartTFTHolCorrection = 'Begynn ferie redigering.';
  SPimsLogEndTFTHolCorrection = 'Avslutt ferei redigeringen.';
  SPimsLogChangesMade = 'Antall endringer gjort: %d';
// MRA:28-OCT-2011. RV100.2. 20011796.2. Changed.
  SPimsEarnedTFT = 'TFT Earned';
  SPimsEarnedTFTSign = 'TFT-E';
// MRA:9-JAN-2012. RV103.4.
  SPimsHolidayCardTitle = 'Ferie kort';
// MRA:13-JAN-2012. RV103.4.
  SJanuaryS = 'Jan.';
  SFebruaryS = 'Feb.';
  SMarchS = 'Mar';
  SAprilS = 'Apr.';
  SMayS = 'Mai';
  SJuneS = 'Jun';
  SJulyS = 'Jul';
  SAugustS = 'Aug.';
  SSeptemberS = 'Sept.';
  SOctoberS = 'Okt.';
  SNovemberS = 'Nov.';
  SDecemberS = 'Des.';
// 20011796.2.
  SPimsRepHolCardOpeningBalance = 'Inng�ende balanse:';
  SPimsRepHolCardUsed = 'Brukt:';
  SPimsRepHolCardAvailable = 'Tilgjengelig:';
  SPimsRepHolCardTFTEarned = 'TFT-E opptjent:';
  SPimsRepHolCardTUsed = 'T brukt:';
  SPimsRepHolCardTotAvail = 'Totalt tilgjengelig:';
  SPimsRepHolCardLegenda = 'T = Time for time brukt, TFT-E = Time for time opptjent, H = Ferie';
  SPimsRepHolCardLegendaTFTEarned = 'TFT-E = Time for time opptjent';
  SPimsInvalidCheckDigits = 'Ugyldige sjekksum! Vennligst skriv inn en verdi fra 10 til 99.';
  SPimsEnteredDateRemark = 'Vennligst skriv inn en dato-IN lik den valgte dato eller neste dato.';
  SPimsShifts = 'Skift'; // 20013551
  SPimsWorkspotHeader = 'Arbeidsposisjon'; // 20013551
  // 20013723
  SPimsExportPeriod = 'Eksporter overtid';
  SPimsExportMonth = 'Eksporter l�nn';
  // 20013288.130
  SPimsExportEasyLaborChoice = 'Vennligst velg hva du skal eksportere.';
  // 20014002
  SPimsTotalWorkspot = 'Totalt Arbeidsposisjon';
  // 20014037.60
  SPimsPimsUser = 'Pims Bruker';
  SPimsWorkstation = 'Arbeidsposisjon';
  // TD-22429
  SPimsStart = 'Start';
  // TD-22475
  SPimsPasteDatacolConnection = 'Lim datacol tilkoblinger fra "%s"';
  SPimsDatacolConnectionFound = 'Datacol tilkoblinger funnet, disse vil bli slettet. Fortsett Ja / Nei?';
  // TD-22503
  SChangeEmplAvail = 'Ansattes tilgjengelighet ble funnet, dette vil bli slettet for ikke tilgjengelig. Vil du fortsette?';
  // SO-20014715
  SPimsReportYes = 'Ja';
  SPimsReportNo = 'Nei';
  SPimsReportTill = 'Frem til';
  SPimsReportLate = 'sent';
  SPimsReportEarly = 'Tidlig';
  // TD-23315 Changed this message:
  SPIMSTimeIntervalError = 'Starttidspunktet er mindre enn forrige sluttid,' + #13 +
   'eller sluttid er h�yere enn neste starttid.';
  // 20013196
  SPimsDeleteLinkJob = 'Det er en definert jobb. Denne vil bli fjernet. Fortsett Ja / Nei ?';
  SPimsLinkJobExist = 'Det er ikke tillatt � definere mer enn 1 koblingsjobb per arbeidsposisjon.' + #13 +
    'Denne koblingsjobben blir ikke lagret.';
  // 20011800
  SPimsOK = '&Ok';
  SPimsFinalRun = '&Siste kj�ring';
  SPimsFinalRunWarning = 'SISTE KJ�RING, ER DU SIKKER?' + #13#13 +
    'Eventuelle mutasjoner over denne perioden kan ikke gj�res i ettertid.';
  SPimsFinalRunExportDateWarning = 'SISTE UTKJ�RING' + #13#13 +
    'Utvalgt eksportperiode er allerede eksportert (delvis eller fullstendig).' + #13#13 +
    'Det er IKKE tillatt � gj�re denne eksporten.';
  SPimsFinalRunExportDateWarningAdmin = 'SISTE UTKJ�RING' + #13#13 +
    'Utvalgt eksportperiode er allerede eksportert (delvis eller fullstendig).' + #13#13 +
    '�nsker du � starte eksporten?';
  SPimsTestRunExportDateWarning = 'TEST UTKJ�RING' + #13#13 +
    'Utvalgt eksportperiode er allerede eksportert (delvis eller fullstendig).' + #13#13 +
    '�nsker du � starte eksporten?';
  SPimsFinalRunExportGapWarning = 'SISTE UTKJ�RING' + #13#13 +
    'There is a gap of %d days between last-export-date' + #13 +
    'and start-date of selection.' + #13#13 +
    'Det er IKKE tillatt � gj�re denne eksporten.';
  SPimsFinalRunExportGapWarningAdmin = 'SISTE UTKJ�RING' + #13#13 +
    'Det er et opphold p� %d dager mellom siste eksportdato' + #13 +
    'og startdato for utvelget.' + #13#13 +
    '�nsker du � starte eksporten?';
  SPimsTestRunExportGapWarning = 'TEST KJ�RING' + #13#13 +
    'Det er et opphold p� %d dager mellom siste eksportdato' + #13 +
    'og startdato for utvelget.' + #13#13 +
    '�nsker du � starte eksporten?';
  SPimsCloseScansSkipped = 'ADVARSEL: Enkelte skanninger kan ikke lukkes p� grunn av en sammenligning' + #13 +
    'med siste eksportdato.';
  SPimsFinalRunAddAvaililityNotAllowed = 'Det er ikke tillatt � legge til tilgjengelighet for denne uken' + #13 +
    'fordi den allerede er blitt eksportert.';
  SPimsFinalRunDateChange = 'ADVARSEL: Datoen er endret til en dato etter den siste eksportdatoen.';
  SPimsFinalRunAvailabilityChange = 'ADVARSEL: Enkelte ansattes tilgjengelighet kunne ikke endres p� grunn av den siste eksportdatoen.';
  SPimsFinalRunTestRun = 'TEST UTKJ�RING';
  SPimsFinalRunDeleteNotAllowed = 'ADVARSEL: Sletting er ikke tillatt, fordi det allerede ble eksportert.';
  SPimsFinalRunOnlyPartDelete = 'ADVARSEL: Bare en del av uken blir slettet, fordi en del allerede ble eksportert.';
  SPimsFinalRunShiftSchedule = 'ADVARSEL: Ikke alle kunne kopieres fordi en del allerede ble eksportert.';
  // SO-20014442
  SPimsYearMonth = '�r/M�ned';
  SPimsStartEndYearMonth = 'Slutt �r/m�ned skal v�re st�rre enn start �r/m�ned!';
  SPimsOnlySANAEmps = 'Kun SANA-ansatte';
  SPimWrongYearSelection = 'Feil �rs utvalg: Fra-til-�r m� v�re det samme!';
  // 20015178
  SPimsPleaseEnterFakeEmpNr = 'Vennligst skriv inn et unikt Falskt-Ansatt-Nummer,' + #13 +
    'som vil bli brukt p� personlig skjerm.';
  SPimsFakeEmpNrInUse = 'Det oppgitte Falske-Ansattes-Nummer er allerede i bruk ' + #13 +
    'av en annen arbeidsposisjon. Vennligst skriv inn et annet nummer.';
  // 20015223
  SPimsWorkspots = 'Arbeidsposisjoner';
  SPimsPlantTitle = 'Anlegg';
  SPimsAllWorkspots = '<Alle>';
  SPimsMultipleWorkspots = '<Flere>';
  // 20013476
  SPimsValueSampleTimeMins = 'Pr�vetid: Ugyldig verdi angitt. Vennligst skriv inn en verdi fra 1 til 30 minutter.';
  // 20015586
  SPimsIncludeDowntime = 'Inkluder nedetids antall:';
  SPimsIncludeDowntimeYes = 'Ja';
  SPimsIncludeDowntimeNo = 'Nei';
  SPimsIncludeDowntimeOnly = 'Vis bare nedetid';
  // TD-25948
  SPimsOpenCSV = '�pne CSV-fil?';
  SPimsCannotOpenCSV = 'Vennligst installer Microsoft Excel (eller et lignende program).';
  // PIM-53
  SPimsNoWorkingDays = 'Vennligst velg en eller flere arbeidsdager.';
  SPimsAddHoliday = 'Vil du legge til %d frav�rsdag (er) til ansattes tilgjengelighet?';
  // PIM-53
  SPimsAddHolidayEmplAvail = 'Vennligst skriv inn en gyldig frav�rskode.';
  SPimsAddHolidayNoStandAvail = 'Handlingen er avbrutt: Det er ingen standard tilgjengelighet som er definert for anlegget / skift-kombinasjonen!';
  // PIM-52
  SPimsWorkScheduleDetailsExists = 'Arbeidsplan Detaljer finnes allerede! Skrive over?';
  SPimsWorkScheduleWrongRepeats = 'Feil verdi for ukentlige gjentakelser! Vennligst skriv inn en verdi mellom 1 og 12.';
  SPimsWorkScheduleInUse = 'Arbeidsplan er fortsatt i bruk (Ansattskontrakt) og kan ikke slettes!';
  SPimsInvalidShift = 'Vennligst skriv inn et gyldig skift.';
  SPimsReady = 'Klar';
  SPimsPlanningMessage1 = 'Ikke behandlet! For ansatt';
  SPimsPlanningMessage2 = 'og dato';
  SPimsPlanningMessage3 = 'Det ble funnet en planlegging.';
  SPimsNoStandAvailFound1 = 'Feil: Ingen standard tilgjengelighet funnet for ansatt';
  SPimsNoStandAvailFound2 = 'og skift';
  SPimsProcessWorkScheduleMessage1 = 'Behandle Arbeidsplan for ansatt';
  SPimsProcessWorkScheduleMessage2 = 'og referansedato';
  // PIM-23
  SPimsTotals = 'Totalt';
  SPimsOTDay = 'Dag';
  SPimsOTWeek = 'Uke';
  SPimsOTMonth = 'M�ned';
  SPimsMO = 'MA';
  SPimsTU = 'TI';
  SPimsWE = 'ON';
  SPimsTH = 'TO';
  SPimsFR = 'FR';
  SPimsSA = 'L�';
  SPimsSU = 'S�';
  SPimsNormalHrs = 'Normale timer pr dag:';
  SPimsOTPeriod = 'Periode';
  SPimsOTPeriodStarts = 'Starter i uke';
  SPimsOTWeeksInPeriod = 'Uker i perioden:';
  SPimsOTWorkingDays = 'Arbeidsdager:';
  SPimsCGRound = 'Runde';
  SPimsCGTRunc = 'Trunc';
  SPimsCGMinute = 'mintter';
  SPimsCGTo = 'til';
  SPimsStandStaffAvailUsed = 'MERK: I en eller flere dager brukes en fast frav�rs�rsak "#13" basert p� standard ansattes tilgjengelighet. '#13'Dette vil ikke bli overskrevet.';
  SPimsShiftDate = 'Skift dato';
  // ABS-27052
  SPimsSelectPrinter = 'Velg Skriver';
  SPimsSelectDefaultPrinter = 'Velg en standard skriver';
  SPimsNoPrinterFound = 'Ingen skrivere funnet!';
  SPimsPleaseSelectPrinter = 'Vennligst velg en skriver.';
  // PIM-52
  SPimsNoCurrentEmpContFound = 'Ingen n�v�rende ansattskontrakt funnet for ansatt';
  // PIM-203
  SPimsHostPortExists = 'Denne Host + Port-kombinasjonen eksisterer allerede for en annen arbeidsposisjon.';
  // PIM-174
  SPimsCopied = 'Kopiert:';
  SPimsRows = 'rad(er).';
  SPimsEmp = 'Ansatt';
  SPimsEmpContract = 'Ansatte Kontrakt';
  SPimsWSPerEmp = 'Arbeidsposisjoner per ansatt';
  SPimsStandStaffAvail = 'Standardpersonal tilgjengelighet.';
  SPimsShiftSched = 'Skiftplan';
  SPimsEmpAvail = 'Ansatt tilgjengelighet';
  SPimsStandEmpPlan = 'Ansatts standard plan';
  SPimsEmpPlan = 'Ansatts planlegging';
  SPimsAbsenceCard = 'Frav�rskort';
  SPimsRecordKeyDeleted = 'Denne posten slettes av en annen bruker.';
  SPimsWorkScheduleWrongStartDate = 'Referanse uke m� v�re en dato som starter p� den f�rste dagen i uken.'#13'Dette er blitt korrigert.';
  SPimsRoamingWorkspotExists = 'Det er allerede en annen workspot definert som roaming.';
  SPimsHourlyWage = 'Timel�nn:';
  SPimsTotalExtraPayments = 'Total ekstra utbetalinger';
  SPimsTBPEmp = 'Timeblocks per Employee';

{$ELSE}
// Default ENGLISH

  SPimsHomePage = 'Pims Homepage will be implemented soon!';
  SPimsIncorrectPassword = 'Incorrect Password.'#13'Enter another password or '+
    'Cancel.';
  SPimsNoEmptyPasswordAllowed = 'An empty password is not allowed.'#13'Enter '+
    'another password or Cancel.';
  SPimsPasswordChanged = 'New password saved.';
  SPimsPasswordNotChanged = 'Password not changed.';
  SPimsPasswordNotConfirmed = 'Password and confirmation are '+
    'different.'#13'Enter another password or Cancel.';
  SPimsSaveChanges = 'Save changes?';
  SPimsRecordLocked = 'This record is in use by another user';
  SPimsOpenXLS = 'Open Excel file?';
  SPimsOpenHTML = 'Open HTML file?';
  SPimsNotUnique = 'Not Unique';
  SPimsNotFound = 'Nothing found!';
  SPimsNotEnoughStock = 'There are not enough products in stock!'#13'Issue the '+
    'items anyway?';
  SPimsNoItems = 'No items yet';
  SPimsNoEmployee = 'There are no employee!';
  SPimsNoCustomer = 'There are no customers!';
  SPimsLoading = 'Loading ';
  SPimsKeyViolation = 'This line already exists.';
  SPimsForeignKeyPost = 'This record has a connection to other records. Your '+
    'request is denied.';
  SPimsForeignKeyDel = 'This record has a connection to other records. Your '+
    'request is denied.';
  SPimsFieldRequiredFmt = 'Please fill required field %s.';
  SPimsFieldRequired = 'Please fill all required fields.';
  SPimsDragColumnHeader = 'Drag a column header here to group that column.';
  SPimsDetailsExist = 'This record has a connection to other records. Please '+
    'remove those records first.';
  SPimsDBUpdate = 'Update Pims database';
  SPimsDBUpdateButtonCaption = '&Run Pims update';
  SPimsDBUpdateConflict = 'Pims detected a conflict while updating the '+
    'database.'#13'Please call the ABS helpdesk and refer to this message.';
  SPimsDBNeedsUpdating = ' Pims detected on older database version.'#13+
   ' Pims needs to update your database to run properly.';
  SPimsCurrentCode = 'Current item code. ';
  SPimsContinue = ' Continue?';
  SPimsConfirmCancelNote = 'Do you want to cancel this delivery note?';
  SPimsConfirmDeleteNote = 'Do you want to delete this delivery note?';
  SPimsConfirmDeleteLicense = 'Do you want to delete this Pims License?';
  SPimsConfirmDelete = 'Do you want to delete the current record?';
  SPimsCannotPrintLabel = 'You cannot print a label for the currently selected '+
    'item.';
  SPimsCannotPlaceFlag = 'You cannot place a flag on the currently selected '+
    'item.';
  SPimsCannotOpenXLS = 'Please install Microsoft Excel.';
  SPimsCannotOpenHTML = 'Please install an Internet Browser on your machine.';

  // FOR PIMS:
  SSelectDummyWK = 'You can not select a dummy workspot!';
  SPimsPercentageNotCorrect = 'Error: Total percentage for workspot is not 100%';
  SPimsTimeMustBeYounger = 'Error: Start-time must be less than End-time!';
  SPimsNoMaster = 'There are no Process Units';
  SPimsNoSubDetail = 'There are no Departments';
  SPimsNoDetail = 'There are no Workspots';
  SPIMSContractTypePermanent = 'Permanent';
  SPIMSContractTypeTemporary = 'Temporary';
  SPIMSContractTypePartTime = 'External';
  SPIMSContractOverlap = 'There is an overlap for this contract with last contract';
  SPimsTBDeleteLastRecord = 'Only the last record can be deleted';
  SPimsTBInsertRecord = 'Only %d timeblocks allowed';
  SPimsStartEndDate = 'Start date is bigger than end date';
  SPimsStartEndTime = 'Start time is bigger than end time';
  SPimsStartEndDateTime = 'Start date time is bigger than end date time';
  SPimsStartEndActiveDate = 'Start active date is bigger than end active date';
  SPimsStartEndInActiveDate = 'Start inactive date is bigger than end inactive date';
  SPimsWeekStartEnd = 'End week < Start week !';
  SPimsBUPerWorkspotPercentages = 'Total of percentages should be 100,  ' +
    'you can not leave the form';
  SPimsTBStartTime = 'Starttime TimeBlock < Starttime Shift on ';
  SPimsTBEndTime = 'Endtime TimeBlock > Endtime Shift on ';
  SPimsBUPerWorkspotInsert = 'There is no workspot you can not insert records';
  SPimsDateinPreviousPeriod =  'There already is a scan that includes this time';
  SPimsExistJobCodes = 'You cannot change it, there are jobcodes defined';
  SPimsExistBUWorkspot = 'You cannot change it, there are relateted records in Business Units per Workspots';
  // Time Recording
  SPimsIDCardNotFound = 'ID Card not found';
  SPimsIDCardExpired	= 'ID Card has expired';
  SPimsEmployeeNotFound = 'Employee not found';
  SPimsInactiveEmployee = 'Employee is inactive';
  SPimsScanningsInMinute = 'Not allow multiple scannings within one minute';
  SPimsStartDay = 'Start of day';
  SpimsEndDay = 'End of day';
  SPimsNoWorkspot = 'No Workspot defined on current workstation';
//Workspot
  SPimsNoJob = 'No Jobs defined on current Workspot';
  SPimsDeleteJob = 'There are jobcodes assigned, they will be deleted!';
  SPimsDeleteBusinessUnit = 'There are business units assigned, they will be deleted!';
  SPimsDeleteJobBU =
    'All connected records with this workspot will be deleted, delete workspot ? ';
  SShowBUWK         = 'Show business units per workspot';
  SShowJobCode = 'Show job codes';
//workspot per workstation
  SPimsSameSourceAndDest = 'The source and destination computer name could ' +
   	'not be the same';
  // Selector Form - Grid Column name
  SPimsColumnPlant 	= 'Plant';
  SPimsColumnWorkspot = 'Workspot';
  SPImsColumnCode = 'Code';
  SPimsColumnDescription = 'Description';
  // Selector Form - Captions
  SPimsWorkspot = 'Select Workspot';
  SPimsJob 	= 'Select Job';
  SPimsJobCodeWorkSpot = 'You can not close form, first define at least one jobcode';
  SPimsInterfaceCode  =
   'You can not close form, first define at least one interface code other than the interface code for the job';
  SPimsOtherInterfaceCode  = 'Please define other interface code than the interface code of the job code';
  SPimsNoOtherInterfaceCodes = 'There are no other interface codes defined, you can not open form';

  SPimsSaveBefore = 'Please save the record before';
  SPimsNoPlant = 'There are no plants';
  SPimsScanDone = 'Scanning processed';
  SPimsScanError = 'Scanning NOT processed: Data-Base ERROR';
//  SPimsNoEmployee = 'There are no employees';
// Time Recording Scanning
  SPimsNoShift = 'No Shift defined';
  SPimsNoCompletedScans = 'Completed scans cannot be modify / deleted';
  SPimsNonScanningStation = 'This is not a time recording station';
  SPimsScanTooBig = 'Scan period longer than 12 hours is not allowed';
  SPimsUseJobCode = 'Please select a job code, workspot it is defined to use jobcodes';
  SPimsNoMatch = 'No match for Employee + ID-Card';
// team per department - extra check
  SPimsNoMorePlantsinTeam =
    'All department of one team should belong to the same plant !';
  SPimsTeamPerEmployee =
    'Department of the employee should be in the team !';

// Hours per Employee
  SPimsAutomaticRecord = 'Automatic inserted record cannot be changed ';
  SPimsNegativeTime = 'Double click to set a negative time (Red color)';
  SPimsExistingRecord = 'A record with the same parameters already exist.'+#13+
  	'Please select that record';
  SPimsProductionHour = 'ProductionHourPerEmployee table cannot updated';
  SPimsNoTimeValue = 'Please insert time value for at least one day';
  SPimsSalaryHour = 'SalaryHourPerEmployee table cannot updated';
  SPimsAbsenceHour = 'AbsenceHourPerEmployee table cannot updated';
  SPimsTo = 'To';
  SPimsInvalidTime = 'Value not set! (Invalid time value)';
// bank holiday process
  SPimsConfirmProcessEmployee = 'Do you want to process all bank holidays of ' +
    ' the year to employee availability ?';
  SPimsInvalidAbsReason = 'Not a valid absence reason!';
  SPimsEmptyAbsReason = 'Please fill required field absence reason!';
  SPimsEmptyDescription = 'Please fill required field description!';
// for reports
  SPimsReportWTR_HOL_ILL_Checks = 'You should select at least one absence reason!';
  SPimsMaxNumberOfWeeks = 'Maximal number of selected weeks is 13!';
  SPimsEndTime = 'End time';
  SPimsStartTime = ' < Start time';
  SPimsOn = 'on' ;
// update database
  SPimsInvalidVersion = 'Invalid PIMS version !';
  SUpdateDatabaseFrom = 'Database will be updated from version ';
  SUpdateDatabaseTo = ' to version ';
// Standard staff availability
  SCheckAvailableTB = 'Permitted values are * and -';
  SCheckAvailableTBNotEmpty = 'Please fill all required fields !';
  SCopySTA1 = 'Standard availability already exists for day ';
  SCopySTA2 = ', overwrite ? ';
  SCopySameValues = 'You selected the same record !';
  SCopySTANotValidFrom = 'There are no valid timeblocks for selected record!';
  SCopySTANotValidTo = 'There are no valid timeblocks for selected record of grid!';
  SEmptyValues = 'Please fill all fields before!';
  SEmplSTARecords = ' There are no standard availabilities defined for this employee ';
// shift schedule
  SShiftSchedule = 'Permitted values are numbers 0 .. 99 or -';
  SSHSShift = 'Permitted values are only shift defined';
  SSHSWeekCopy = 'Start date of copy should be bigger than end date';
  SInvalidShiftSchedule = 'Shift number has not defined or values start/end' +
    ' time per day are zero!';
  SEmployeeAvail_0  = 'Employee ';
  SEmployeeAvail_1  = ' already has availability in this shift on year, week, day ';
//  SEmployeeAvail_2 = ', day ';
  SEmployeeAvail_3 = ', you can not change the shift number.';
  SUndoChanges = 'Last changes will not be saved!';
  SStartWeek = 'Start week should be bigger than end week!';
  SCopySelectionShiftSchedule_1 = 'Shift schedule for employee ';
  SCopySelectionShiftSchedule_2 = ' already exists. Overwrite ?';
  SCopyFinished = 'Copy is finished!';
  SEmptyRecord = 'There is no employee selected!';
// employee availability
  SEmplAvail = 'Permitted values are *, - or an absence reason';
  SValidTimeBlock = 'All valid timeblocks should be filled!';
  SEmployeePlanned = 'Employee is already planned, continuing will delete planning. ' +
    'Do you want to continue ?';
  SCopyFrom  = 'Please select a record!';
  SCopyTo  = 'Records are the same, please select a different record!';
  SEMAOverwrite = 'Employee availability for ';
  SEMAShift = ' shift ';
  SEMAExists = 'already exists. Do you want to overwrite?';
// staff planning
  SSelectedWorkspots = 'There are more than 401 selected workspots, only the first 401  will ' +
    ' be displayed ';
  SEmptyWK = 'There are no workspots selected';
  SOverplanning = 'Overplanning more than 200%';
  SMakeAvailable = 'Make available';
  SNotAcceptPlanned = 'This planning of employee is not accepted!';
  SSaveChanges = 'Save changes?';
  SReadStdPlnIntoDaySelection = 'Planning already read for a plant/team/date within the selection';
  SReadStdPlnIntoDaySelectionFinish = 'Read standard planning into day planning finished';
  SReadPastIntoDayPlanFinish = 'Read past into day planning finished';
  SContinueYN = 'Do you want to continue ?';
  SDateFromTo = 'End date should be bigger than start date!';
  SDateIntervalValidate = ' Interval of target date should be different than interval' +
    ' of source date';
  SNoEmployee = 'There are no employees selected!';
  SOCILevel = 'Occupation level ';
  SPlannedLevel = 'Planned with level ';
  SPlannedAllLevels = 'Planned ';
  SOCIAllLevel = 'Occupation  ';
  SSameRangeDate = 'Date intervals should have the same length!';
// report staff planning
  SStartEndYear = 'End year should be bigger than start year!';
  SWeek = 'You can not select more than one week!';
  SNoTeam = 'Empty team: ';
// Workspot per employee
  SLEVELWK = 'Only A, B, C are allowed!';
  SInvalidWK = 'Only A..Z, a..z, 0..9, and ''_'' are allowed !';
// Staff Availability
  SChangePlaned = 'Employee is already planned, continuing will delete planning'+
    #13+'Do you want to continue?';
  SInvalidShiftNo = 'Invalid Shift number for day: %d';
  SInvalidAvailability = 'Invalid Availability value for day: %d, TB: %d';
//  E&R
  SMoreThan = ' More than ';
  SInvalidMistakeValue =  'Number of mistakes should be bigger than zero !';
  SNotDeleteRecord = ' You can not delete this record !';
  SPimsDBError = 'Report openned by other station or Database error. '+#13+
		'Please run report again';
 //
// productivity reports

  SDaySelect = 'Start day should be less then end day!';
//
// jobcode
  SInterfaceCode = 'This interface code is already used for other workspots!';
  SWKInterfaceCode = 'This interface code is already used for this workspot!';
  SWKRescanInterfaceCode =
   'There are interface codes used more than once for this workspot!';
  SDeleteOtherInterfaceCode = 'All other interface codes defined will be deleted!';

//
  SExceptionDataSetReport =
   'Data set is empty cannot create report, please set it first and rebuild pims';
  SExceptionDataSetForms  =
   'Data set is empty cannot refresh grids, please set it first and rebuild pims';
// export payroll report
   SPeriodReport = 'Period';
   SMonthReport = 'Month';
// report quality incentive
   SCalculationwithOvertime = 'Quality incentive calculation with overtime report';
// add wk into datacollection
  SADDWKDateCollection = 'Records are added';
// strings for mistake per employee
  SDate = 'Date ';
  STotalWeek = 'Total week ';
// report hrs per empl
  SCaptionProdRepHrs = 'Production hours per employee';
  SCaptionSalRepHrs =  'Salary hours per employee';
//report CompHours
  SShowActiveEmpl = 'Show active employee';
  SShowNotActiveEmpl = 'Show not active employee';
//report HrsWKCum
  SRepHrsCUMWK = 'Workspot';
  SRepHrsTotalCUM = 'Total workspot';

  SRepHrsCUMWKBU = 'Business unit';
  SRepHrsTotalCUMBU = 'Total business unit';

  SRepHrsCUMWKPlant = 'Plant';
  SRepHrsTotalCUMPlant = 'Total plant';

  SRepHrsCUMWKDept = 'Department';
  SRepHrsTotalCUMDept = 'Total department';
  SRepJobCode = 'Job code';
  SRepTotJobCode = 'Total job code';
// report absence
  SHeaderAbsRsn = 'Absence reason';
  SHeaderWeekAbsRsn = 'Week';
  SHdTotalWeek = 'Total week';
  SRepExpPayroll = 'From';
// report absence schedule
  SReport = 'Report ';
  SReportWorkTimeReduction = 'work time reduction ';
  SReportIllness = 'illness';
  SReportHoliday = 'holiday';
  SActiveEmpl =  'Active employees';
  SInactiveEmpl = 'Inactive employees';
// report
  SRepHrsPerEmplActiveEmpl = 'Show active employee';
  SRepHrsPerEmplInActiveEmpl = 'Show not active employee';
  SRepProdHrs = 'Productive hours';
  SRepNonProdHrs = 'Non-productive hours';
  SAll = 'All';
  SActive = 'Active';
  SInactive = 'Inactive';
  SName = 'Name ';
  SShortName = 'Short name';
// REPORT check list
  SShowProcessed =  'Show processed';
  SShowNotProcessed  = 'Show not processed';
//Report
  SErrorOpenRep = 'Error open data base, report can not be opened';
  SErrorCreateRep = 'Error report can not be created';
//DialogProcessAbsenceHrsFRM
   SLogEmpNo = 'Found Number of Records=';
   SLogChangedAbsenceTotal = 'Changed %d Absence Total-records';
   SLogDeletedRec = 'Deleted %d Absence Hour-records';
   SLogEndAbsence = 'End: Defining Absence Hours Per Employee';
   SLogStartSalaryHours = 'Start: Defining Salary Hours per Employee';
   SLogStartAvailability  = 'Start: Defining Employee Availability';
   SLogChangedAbsence = 'Changed %d Absence Hours-records';
   SlogAddedAbsence = 'Added %d Absence Hours-records';
   SLogEndAvailability = 'End: Defining Employee Availability';
   SLogStartNonScanning = 'Start: Defining Non-Scanning Employee Availability';
   SLogEndNonScanning  = 'End: Defining Non-Scanning Employee Availability';
   SLogFoundEmployeeNo = 'Found Number of Records = %d';
   SSProcessFinished = 'Process finished';
// Staffplanning
   SStaffPlnEmployee = 'Employee';
   SStaffPlnName = 'name';
// Contract group form
   SOvertimeDay = 'Day';
   SOvertimeWeek = 'Week';
   SValidMinutes = 'Minutes defined should be less than 59';
   SQuaranteedHourType = 'Please select an hour type';
//
//update cascade
   SConfirmUpdateCascadeEmpl =
     'Employee will be changed for all connected records, update employee ? ';
   SConfirmDeleteCascadeEmpl =
     'All connected records with this employee will be deleted also, delete employee ? ';
   SEmpExist = 'This employee already exist ';
//export payroll MRA:RV095.4. Give better message: 'date' added.
   SNotDefaultSelection = 'Not the default date-selection. Are you sure ?';
   SPeriodAlreadyExported = 'This period is already exported. Are you sure ?';
// transfer free time
   SPimsDifferentYears = 'Target year should be different than source year!';
   SPimsCheckBox = 'At least one of check boxes must be selected!';
   SPimsProcessFinish = 'Process has finished!';

   // MR:17-01-2003
   SPimsUseShift = 'Please select a shift!';
   SPimsUseWorkspot = 'Please select a workspot!';
//  staff planning -CAR 30-01-2003
   SPimsDayPlanning = 'Day planning: ';
   SPimsTeamSelection = 'Team selection: ';
   SPimsTeamSelectionAll = 'All teams';
   SPimsSTDPlanning = 'Standard planning: ';
   SPimsStaffTo =  ' to ';
   SPimsPlant =  '      Plant:  ';
   SPimsShift = '      Shift: ';
   SPimsTeam = 'Team ';
   // MR:28-01-2003
   SPimsNoEmployeeLine = 'No employee';

   SPimsNoPrevScanningFound = 'No previous scanning found';
   SpimsScanExists = 'There''s already a scanning-record on that day for that employee. ' + #13 +
     'You cannot change this item.';

 //car: 27.02.2003
   SPimsEXCheckIllMsgClose =
     'There already is one outstanding illness message, please close it before';
   SPimsEXCheckIllMsgStartDate =
     'Start date should be bigger than the end date of last created illness message ';

   // MR:03-03-2003
   SPimsContract = 'Contract';
   SPimsPlanned = 'Planned';
   SPimsAvailable = 'Avail.';

   // MR:07-03-2003
   SpimsYear = 'Year';
   SPimsWeek = 'Week';
   SPimsDate = 'Date';
   //CAR:21-03-2003 - REPORT staff planning per day
   SPimsNewPageWK = 'New page per workspot';
   SPimsNewPageEmpl = 'New page per employee';
   //CAR:21-03-2003 report checklist
   SPimsAvailability = 'Availability: ';
   //CAR:21-03-2003 update cascade -Plant
   SConfirmDeleteCascadePlant =
     'All connected records with this plant will be deleted, delete plant ? ';
   SConfirmUpdateCascadePlant =
     'Plant will be changed for all connected records, update plant ? ';
   SPlantExist = 'This plant already exists ';
   //CAR: 27-03-2003
   SRepTeamTotEmpl ='Total employee';
   //CAR 29-03-2003
   SRepHRSPerEmplCorrection = 'Manual corrections';
   SRepHrsPerEmplTotal = 'Total';
   //CAR 31-03-2003  - Report AbsenceCard
   SJanuary = 'January';
   SFebruary = 'February';
   SMarch = 'March';
   SApril = 'April';
   SMay = 'May';
   SJune = 'June';
   SJuly = 'July';
   SAugust = 'August';
   SSeptember = 'September';
   SOctober = 'October';
   SNovember = 'November';
   SDecember = 'December';
//CAR  2-04-2003 - report Production Detail
   SRepProdDetPerformance = 'Performance';
   SRepProdDetBonus = 'Bonus';
// CAR 2-4-2003 - Stand Staff availability
   SStdStaffAvailPlanned =
     'Employee is planned in standard planning, continuing will delete planning. ' +
     'Do you want to continue ?';
//CAR 7-4-2003
   SPimsJobs  = 'Jobcode';
//CAR 15-4-2003
  SPimsStartEndDateTimeEQ = 'Start date time is bigger or equal with end date time';
  SPimsStartEndTimeNull = 'Start time or end time are empty, do you want to continue ?';
// MR:24-04-2003
  SPimsNoData = 'No Data';
//CAR 5-5-2003
  SPimsAbsence = 'Absence';
  SPimsDifference = 'Difference';
// car 12-5-2003
  SExportDescEMPL = 'Employee description';
  SExportDescDept = 'Dept description';
  SExportDescPlant = 'Plant description';
  SExportDescShift = 'Shift description';
//
  SDummyWK = 'This is a dummy workspot it can not be saved';
// CAR 28-8-2003 - Stand Staff availability
   SStdStaffAvailFuturePlanned =
     'Employee is already planned in this shift. Continue will delete future planning. ';
// 9-4-2003
  SPimsInvalidWeekNr = 'Invalid week number!';
// 9-4-2003
  SPimsHoursPerDayProduction = 'Production ';
  SPimsHoursPerDaySalary = 'Salary ';
  // MR:12-09-2003
  SPimsCompletedToNotScans = 'You cannot change a completed scan back '#13'to a not-completed one.';
// user rights
  SAplErrorLogin = 'Invalid user name or password!';
  SAplNotConnect = 'Invalid connection to database!';
  SAplNotVisible = 'This user can not open the application';
// MR:3-11-2003 Moved from 'UStrings.pas'
  LSPimsEnterPassword = 'Enter Password';
// MR:11-11-2003
  SPimsEmployeesAll = 'All employees';
  SPimsEmployeesOnWrongWorkspot = 'On wrong workspot';
  SPimsEmployeesNotScannedIn = 'Not scanned in';
  SPimsEmployeesAbsentWithReason = 'Absent with reason';
  SPimsEmployeesAbsentWithoutReason = 'Absent without reason';
  SPimsEmployeesWithFirstAid = 'With first aid';
  SPimsEmployeesTooLate = 'Too late';
// 550119 - Workspot per Employee form
  SPimsLevel = 'Level';
// Car: 15-1-2004
  SPimsEmplContractInDatePrevious =
   'There already is a contract that includes this date';
// 550287
// Contract group form
  STruncateHrs = 'Truncate';
  SRoundHrs = 'Round';
//550124
  SPimsPieces = 'Pieces';
  SPimsWeight = 'Weights';
// MR:03-05-2004 Order 550313
  SPimsCleanUpProductionQuantities = 'Cleanup Production Quantities older than %d weeks?';
  SPimsCleanUpEmployeeHours = 'Cleanup Employee Hours older than %d weeks?';
  SPimsCleanUpTimerecordingScans = 'Cleanup Timerecording Scans older than %d weeks?';
// MR:17-05-2004 Order 550315
  SPimsLabelCode = 'Code';
  SPimsLabelInterfaceCode = 'Interface Code';
// MR:26-07-2004 Order 550327
  SPimsNotPlannedWorkspot = 'Not Planned';
// MR:09-08-2004 Order 550336
  SNumber = 'Number';
// MR:13-10-2004
  SPimsGeneralSQLError = 'BDE reported a general SQL Error';
// MR:26-10-2004
  SPimsEfficiencyAt = 'Efficiency at';
  SPimsEfficiencySince = 'Efficiency since';
// MR:28-10-2004
  SPimsConnectionLost = 'Error: Database connection lost. Please restart application';
// MR:10-12-2004
  SSHSWeekDatesCross = 'Error: Selected From/To-dates will cross each other!';
// MR:15-12-2004
  SPimsHours = 'hours';
  SPimsDays = 'days';
  SPimsBonus = 'bonus';
  SPimsEfficiency = 'efficiency';
// MR:11-01-2005
  SPimsTotalProduction = 'Total production';
// MR:11-01-2005
  SPimsProductionOutput = 'Production Output';
  SPimsChartHelp = 'select time: click on graph'; //, move: right-click between graph-items';
  SPimsInvalidScan = 'Error: You entered an invalid scan.';
// MR:11-01-2005 -> Pims Oracle!
  SPimsScanOverlap = 'Error: Scan has wrong time interval.';
  SPimsMaxSalBalMin = 'Max.Sal.Balance';
// MR:16-06-2005
  SPimsConfirmProcessEmployeeSelectedDate = 'Do you want to process the selected date ' + #13 +
    '%s' + #13 + 'to employee availability ?';
// MR:20-06-2005
  SPimsNoBankHoliday = 'Please select a bank holiday date';
// MR:27-09-2005
  SPimsUseJobcodesNotAllowed = 'There are not-completed scans for this workspot. ' + #13 +
    'Change is not allowed.';
// MR:14-11-2005
  SPimsUnknownBusinessUnit = 'Unknown Business Unit';
// MR:07-12-2006
  SOvertimeMonth = 'Month';
// MR:05-01-2007
  SErrorWrongTimeBlock = 'ERROR: Wrong timeblock for employee';
  SErrorDuringProcess = 'There were errors! Please see error-messages above.';
  SSProcessFinishedWithErrors = 'Process finished with errors!';
// MR:26-01-2007
  SPimsMonthGroupEfficiency = 'Monthly Group Efficiency';
  SPimsGroupEfficiency = 'Monthly Efficiency';
// MR:14-SEP-2009
  SPimsTemplateYearCopyNotAllowed = 'It is not allowed to copy to this year!';
  SPimsTemplateYearSelectNotAllowed = 'It is not allowed to select this year!';
// MRA:8-DEC-2009 RV048.1.
  SPimsEmpPlanPWD = 'It is not allowed to change the department, ' + #13 +
    ' because employees are planned on this workspot/department combination.';
// MRA:19-JAN-2010.
  SPimsNrOfRecsProcessed = 'Number of records processed: ';
// MRA:22-FEB-2010.
  SPimsNoShiftScheduleFound = 'There is no shift schedule defined for the used week.';
// MRA:18-MAY-2010. Order 550478.
  SPimsDeleteMachineWorkspotsNotAllowed = 'Delete not possible! There are workspots linked to this machine.';    
  SPimsDeleteMachineJobsNotAllowed = 'Delete not possible! There are jobs defined for this machine.';
  SPimsUpdateMachineCodeNotAllowed = 'Machine is linked to workspot(s). Changing the Machine Code is therefore not allowed.';
  SPimsUpdateMachineJobCodeNotAllowed = 'Machine-job is in use by a workspot-job. Changing the Machine Job Code is therefore not allowed.';      
// SO:07-JUN-2010.
  SPimsIncorrectHourType = 'Please enter a correct hour type !!';
  SPimsScanLongerThan24h = 'The scan cannot be longer than 24 hours !!';
// MRA:30-JUN-2010. Order 550478.
  SPimsMachineWorkspotNonMatchingJobs = 'WARNING: There are non-matching jobs for this workspot for the entered machine.' + #13 + 'Please be sure the jobs match for workspots that are linked to the entered machine!';
  SPimsPleaseDefineJobForMachine = 'WARNING: Please make changes for jobs codes on machine level.';
  SPimsDeleteNotAllowedForMachineJob = 'This is a machine job. Deletion is not allowed here, only on machine level.';
// MRA:24-AUG-2010. Order 550497. RV067.MRA.8.
  SPimsAbsenceReasons = 'absence reasons';
  SPimsHourTypes = 'hour types';
  SPimsAbsenceTypes = 'absence types';
// MRA:31-AUG-2010. RV067.MRA.21
  SPimsDescriptionNotEmpty = 'Please enter a description, it cannot be left empty!';
  SPimsCounterNameFilled = 'The counter name should be filled if the counter is active!';
// MRA:21-SEP-2010. RV067.MRA.34
  SPimsPaste = 'Paste';
  SPimsPasteExceptHours = 'Paste exceptional hours from "%s"';
  SPimsDefExceptHoursFound = 'Definitions found, these will be deleted. Proceed Yes / No?';
// MRA:4-OCT-2010. RV071.3. 550497
  SPimsCountersHaveBeenUpdated = 'Counters have been updated.';
  SPimsProcessToAvailIsReady = 'Process to availability is ready.';
  SPimsRecalcHoursConfirm = 'Recalculation of hours is included. Proceed ?';
// MRA:11-OCT-2010. RV071.10. 550497
  SPimMaxSatCreditReached = 'Maximum Saturday Credit has been reached!';
// MRA:1-NOV-2010.
  SPimsStartEndDateSmaller = 'Start date is smaller than end date' + #13 +
     'of an existing illness message for the same employee.';
// MRA:8-NOV-2010.
// MRA:1-DEC-2010. RV082.4. Text changed.
  SPimsIsNoScannerWarning =
    'WARNING: This employee is a non-scanner and of type: ' + #13 +
    'Autom. scans based on standard planning.' + #13 +
    'Updating scans can lead to incorrect hours! ' + #13 +
    'Changes are not allowed!';
// MRA:17-JAN-2011. RV085.1.
  SPimsEmpPlanPWDFound = 'WARNING: Planning exists for this workspot/department-combination' + #13 +
    'on %s and/or later. Are you sure you want to change the department?';
// MRA:14-FEB-2011. RV086.4.
  SPimsSelPlantsCountry = 'The selected plants should belong to the same country!';
  SPimsSelNotPosMultExports = 'Selection not possible, multiple export types are not allowed.';
// MRA:18-APR-2011. RV089.5
  SPimsAddAllWorkspots = 'Add ALL workspots?';
// MRA:21-APR-2011. RV090.1.
  SPimsFinishLastTransactionFirst = 'Please confirm/undo the last copy-action first!';  
  SPimsConfirmLastTransactionFirst = 'The last copy-action is not confirmed yet.' + #13 +
                                     'Please confirm/undo it first!' + #13 +
                                     'Yes=Confirm No=Undo';
  SPimsConfirmLastTransaction = 'Do you want to CONFIRM the last copy-action?';
  SPimsUndoLastTransaction = 'Do you want to UNDO the last copy-action?';
// MRA:26-APR-2011. RV089.1.
  SPimsYes = '&Yes';
  SPimsNo = '&No';
  SPimsAll = '&All';
  SPimsNoToAll = 'N&o To All';
// MRA:27-APR-2011. RV091.1
  SPimsPasteOvertimeDef = 'Paste overtime definitions from "%s"';
  SPimsOvertimeDefFound = 'Definitions found, these will be deleted. Proceed Yes / No?';
// MRA:24-MAY-2011. RV092.6
  SPimsEndTimeError = 'End time should be bigger than start time!';
// MRA:26-MAY-2011. RV093.1.
  SPimsFooterTotalContractGroup = 'Total Contr. gr.';
  SPimsFooterContractGroup = 'Contractgroup';
// MRA:1-JUN-2011. RV093.3.
  SPimsLoggingEnable = 'To enable/disable logging please restart Pims.';
// MRA:6-JUN-2011. RV093.3.
  SPimsCleanUpEmpHrsLog = 'Cleanup Employee Hours Logging older than %d weeks?';
  SPimsCleanUpTRSLog = 'Cleanup Timerecording Scans Logging older than %d weeks?';
// MRA:6-JUL-2011. RV094.5.
  SPimsFilenameAFAS = 'Salarymutations ABS PIMS';
// MRA:11-JUL-2011. RV094.7.
  SPimsPlantDeleteNotAllowed = 'It is not allowed to delete a plant.';
  SPimsPlantUpdateNotAllowed = 'It is not allowed to change a plant.';
  SPimsEmployeeDeleteNotAllowed = 'It is not allowed to delete an employee.';
  SPimsEmployeeUpdateNotAllowed = 'It is not allowed to change an employee.';
  SPimsWorkspotDeleteNotAllowed = 'It is not allowed to delete a workspot.';
  SPimsWorkspotUpdateNotAllowed = 'It is not allowed to change a workspot.';
// MRA:11-JUL-2011. RV04.8.
  SPimsEmployeeNotActive = 'Employee is non-active.';
// MRA:19-OCT-2011. RV099.1.
  SPimsPleaseFillBothAbsReasons = 'Please fill in both absence reasons.';
  SPimsLogStartTFTHolCorrection = 'Start TFT/Vacation correction.';
  SPimsLogEndTFTHolCorrection = 'End TFT/Vacation correction.';
  SPimsLogChangesMade = 'Number of changes made: %d';
// MRA:28-OCT-2011. RV100.2. 20011796.2. Changed.
  SPimsEarnedTFT = 'TFT Earned';
  SPimsEarnedTFTSign = 'TFT-E';
// MRA:9-JAN-2012. RV103.4.
  SPimsHolidayCardTitle = 'Holiday Card';
// MRA:13-JAN-2012. RV103.4.
  SJanuaryS = 'Jan.';
  SFebruaryS = 'Febr.';
  SMarchS = 'March';
  SAprilS = 'Apr.';
  SMayS = 'May';
  SJuneS = 'June';
  SJulyS = 'July';
  SAugustS = 'Aug.';
  SSeptemberS = 'Sept.';
  SOctoberS = 'Oct.';
  SNovemberS = 'Nov.';
  SDecemberS = 'Dec.';
// 20011796.2.
  SPimsRepHolCardOpeningBalance = 'Opening balance:';
  SPimsRepHolCardUsed = 'Used:';
  SPimsRepHolCardAvailable = 'Available:';
  SPimsRepHolCardTFTEarned = 'TFT-E Earned:';
  SPimsRepHolCardTUsed = 'T Used:';
  SPimsRepHolCardTotAvail = 'Total Available:';
  SPimsRepHolCardLegenda = 'T = Time for Time used, TFT-E = Time for Time earned, H = Holiday';
  SPimsRepHolCardLegendaTFTEarned = 'TFT-E = Time for Time earned';
  SPimsInvalidCheckDigits = 'Invalid checkdigits! Please enter a value from 10 to 99.';
  SPimsEnteredDateRemark = 'Please enter a Date-IN equal to selected date or next date.';
  SPimsShifts = 'Shifts'; // 20013551
  SPimsWorkspotHeader = 'Workspot'; // 20013551
  // 20013723
  SPimsExportPeriod = 'Export overtime';
  SPimsExportMonth = 'Export payroll';
  // 20013288.130
  SPimsExportEasyLaborChoice = 'Please make a choice about what to export.';
  // 20014002
  SPimsTotalWorkspot = 'Total Workspot';
  // 20014037.60
  SPimsPimsUser = 'Pims User';
  SPimsWorkstation = 'Workstation';
  // TD-22429
  SPimsStart = 'Start';
  // TD-22475
  SPimsPasteDatacolConnection = 'Paste datacol connections from "%s"';
  SPimsDatacolConnectionFound = 'DatacolConnections found, these will be deleted. Proceed Yes / No?';
  // TD-22503
  SChangeEmplAvail = 'Employee Availability was found, this will be deleted for non-available. Do you want to continue?';
  // SO-20014715
  SPimsReportYes = 'Yes';
  SPimsReportNo = 'No';
  SPimsReportTill = 'till';
  SPimsReportLate = 'late';
  SPimsReportEarly = 'early';
  // TD-23315 Changed this message:
  SPIMSTimeIntervalError = 'Starting time is less than previous end time,' + #13 +
   'or end time is higher then next start time.';
  // 20013196
  SPimsDeleteLinkJob = 'There is a link job defined. This will be removed. Proceed Yes/No ?';
  SPimsLinkJobExist = 'It is not allowed to define more than 1 link job per workspot.' + #13 +
    'This link job will not be saved.';
  // 20011800
  SPimsOK = '&Ok';
  SPimsFinalRun = '&Final Run';
  SPimsFinalRunWarning = 'FINAL RUN, ARE YOU SURE?' + #13#13 +
    'Any mutations over this period cannot be made afterwards.';
  SPimsFinalRunExportDateWarning = 'FINAL RUN' + #13#13 +
    'Selected export-period is already exported (partly or completely).' + #13#13 +
    'It is NOT allowed to make this export.';
  SPimsFinalRunExportDateWarningAdmin = 'FINAL RUN' + #13#13 +
    'Selected export-period is already exported (partly or completely).' + #13#13 +
    'Do you want to start the export?';
  SPimsTestRunExportDateWarning = 'TEST RUN' + #13#13 +
    'Selected export-period is already exported (partly or completely).' + #13#13 +
    'Do you want to start the export?';
  SPimsFinalRunExportGapWarning = 'FINAL RUN' + #13#13 +
    'There is a gap of %d days between last-export-date' + #13 +
    'and start-date of selection.' + #13#13 +
    'It is NOT allowed to make this export.';
  SPimsFinalRunExportGapWarningAdmin = 'FINAL RUN' + #13#13 +
    'There is a gap of %d days between last-export-date' + #13 +
    'and start-date of selection.' + #13#13 +
    'Do you want to start the export?';
  SPimsTestRunExportGapWarning = 'TEST RUN' + #13#13 +
    'There is a gap of %d days between last-export-date' + #13 +
    'and start-date of selection.' + #13#13 +
    'Do you want to start the export?';
  SPimsCloseScansSkipped = 'WARNING: Some scans could not be closed because of a compare' + #13 +
    'with last-export-date.';
  SPimsFinalRunAddAvaililityNotAllowed = 'Adding availability for this week is not allowed' + #13 +
    'because it has already been exported.';
  SPimsFinalRunDateChange = 'WARNING: The date has been changed to a date after the last-export-date.';
  SPimsFinalRunAvailabilityChange = 'WARNING: Some employee availability could not be changed because of the last-export-date.';
  SPimsFinalRunTestRun = 'TEST RUN';
  SPimsFinalRunDeleteNotAllowed = 'WARNING: Delete is not allowed, because it was already exported.';
  SPimsFinalRunOnlyPartDelete = 'WARNING: Only a part of the week will be deleted, because some part was already exported.';
  SPimsFinalRunShiftSchedule = 'WARNING: Not all could be copied because some part was already exported.';
  // SO-20014442
  SPimsYearMonth = 'Year/Month';
  SPimsStartEndYearMonth = 'End year/month should be bigger than start year/month!';
  SPimsOnlySANAEmps = 'Only SANA employees';
  SPimWrongYearSelection = 'Wrong year-selection: The From-To-Year must be the same!';
  // 20015178
  SPimsPleaseEnterFakeEmpNr = 'Please enter a unique Fake Employee Number,' + #13 +
    'that will be used in Personal Screen.';
  SPimsFakeEmpNrInUse = 'The entered Fake Employee Number is already in use' + #13 +
    'by another workspot. Please enter a different number.';
  // 20015223
  SPimsWorkspots = 'Workspots';
  SPimsPlantTitle = 'Plant';
  SPimsAllWorkspots = '<All>';
  SPimsMultipleWorkspots = '<Multiple>';
  // 20013476
  SPimsValueSampleTimeMins = 'Sample Time: Invalid value entered. Please enter a value from 1 to 30 minutes.';
  // 20015586
  SPimsIncludeDowntime = 'Include down-time quantities:';
  SPimsIncludeDowntimeYes = 'Yes';
  SPimsIncludeDowntimeNo = 'No';
  SPimsIncludeDowntimeOnly = 'Show only down-time';
  // TD-25948
  SPimsOpenCSV = 'Open CSV file?';
  SPimsCannotOpenCSV = 'Please install Microsoft Excel (or a similar program).';
  // PIM-53
  SPimsNoWorkingDays = 'Please select one or more working days.';
  SPimsAddHoliday = 'Do you want to add %d absence day(s) to employee availability?';
  // PIM-53
  SPimsAddHolidayEmplAvail = 'Please enter a valid absence reason code.';
  SPimsAddHolidayNoStandAvail = 'Action is aborted: There is no standard availability defined for the plant/shift-combination!';
  // PIM-52
  SPimsWorkScheduleDetailsExists = 'Work Schedule Details already exist! Overwrite?';
  SPimsWorkScheduleWrongRepeats = 'Wrong value for Weekly Repeats! Please enter a value between 1 and 26.';
  SPimsWorkScheduleInUse = 'Work Schedule is still in use (Employee Contract) and cannot be deleted!';
  SPimsInvalidShift = 'Please enter a valid shift.';
  SPimsReady = 'Ready';
  SPimsPlanningMessage1 = 'Not processed! For employee';
  SPimsPlanningMessage2 = 'and date';
  SPimsPlanningMessage3 = 'there was planning found.';
  SPimsNoStandAvailFound1 = 'Error: No standard availability found for employee';
  SPimsNoStandAvailFound2 = 'and shift';
  SPimsProcessWorkScheduleMessage1 = 'Process Workschedule for employee';
  SPimsProcessWorkScheduleMessage2 = 'and referencedate';
  // PIM-23
  SPimsTotals = 'Totals';
  SPimsOTDay = 'Day';
  SPimsOTWeek = 'Week';
  SPimsOTMonth = 'Month';
  SPimsMO = 'MO';
  SPimsTU = 'TU';
  SPimsWE = 'WE';
  SPimsTH = 'TH';
  SPimsFR = 'FR';
  SPimsSA = 'SA';
  SPimsSU = 'SU';
  SPimsNormalHrs = 'Normal hours per day:';
  SPimsOTPeriod = 'Period';
  SPimsOTPeriodStarts = 'Starts in week';
  SPimsOTWeeksInPeriod = 'Weeks in period:';
  SPimsOTWorkingDays = 'Working days:';
  SPimsCGRound = 'Round';
  SPimsCGTRunc = 'Trunc';
  SPimsCGMinute = 'minutes';
  SPimsCGTo = 'to';
  SPimsStandStaffAvailUsed = 'NOTE: For one or more days a fixed absence reason is used'#13'based on standard staff availability.'#13'This will not be overwritten.';
  SPimsShiftDate = 'Shift Date';
  // ABS-27052
  SPimsSelectPrinter = 'Select Printer';
  SPimsSelectDefaultPrinter = 'Select a printer as default printer';
  SPimsNoPrinterFound = 'No printers found!';
  SPimsPleaseSelectPrinter = 'Please select a printer.';
  // PIM-52
  SPimsNoCurrentEmpContFound = 'No current employee contract found for employee';
  // PIM-203
  SPimsHostPortExists = 'This Host+Port-combination already exists for another workspot.';
  // PIM-174
  SPimsCopied = 'Copied:';
  SPimsRows = 'row(s).';
  SPimsEmp = 'Employee';
  SPimsEmpContract = 'Employee Contract';
  SPimsWSPerEmp = 'Workspots per Employee';
  SPimsStandStaffAvail = 'Standard Staff Avail.';
  SPimsShiftSched = 'Shift Schedule';
  SPimsEmpAvail = 'Employee Avail.';
  SPimsStandEmpPlan = 'Employee Standard Planning';
  SPimsEmpPlan = 'Employee Planning';
  SPimsAbsenceCard = 'Absence card';
  SPimsRecordKeyDeleted = 'This record is deleted by another user.';
  SPimsWorkScheduleWrongStartDate = 'Reference week must be a date that starts at the first day of the week.'#13'This has been corrected.';
  SPimsRoamingWorkspotExists = 'There is already another workspot defined as roaming.';
  SPimsHourlyWage = 'Hourly Wage:';
  SPimsTotalExtraPayments = 'Total Extra Payments';
  SPimsTBPEmp = 'Timeblocks per Employee';

            {$ENDIF}
          {$ENDIF}
        {$ENDIF}
      {$ENDIF}
    {$ENDIF}
  {$ENDIF}
{$ENDIF}

implementation
end.
