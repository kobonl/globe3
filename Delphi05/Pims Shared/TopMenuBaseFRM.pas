unit TopMenuBaseFRM;
(*******************************************************************************
Unit     : FTopMenuBase    TfrmTopMenuBase = (TfrmSerialBase)
Function : Parent form. This form contains all top menu command items and
         : toolbar items for Solar.
Author   : Kees Meijers
Note     : -
Changes  : -
--------------------------------------------------------------------------------
Inheritance tree:
TForm
frmSolar
  Changed design properties :
  + Font.Name := Tahoma (MicroSoft Outlook Style)
  + Position  := poScreenCenter
  + ShowHint  := True

  Added controls:
  -

  Events:
  + FormCreate: Update the progressbar on the splashscreen

  Methods:
  + procedure SetParent(AParent :TWinControl); override;
    : Allmost all Solar froms a placed on a panel in another form.
    : Only via this method it is posible to have multiple toolbars
    : and full control over a form within the other form.

frmSerial
  Changed design properties :
  -

  Added controls:
  + Port1

  Events:
  + FormCreate
    : Assign comportnumber to Port1 according to SystemSettings
    : default Com1
    Port1ReceiveData
    : Reads the Comportbuffer and calls DoCodeRead

  Methods:
  + procedure DoCodeRead(Code: String)
    : descendents need to assign this function in order to receive the
    : comport information. Descendents do not override the Port1ReceiveData
    : event, so this component can/may be changed.
    property OnCodeRead
    : the event passed
    procedure FlushScanner
    : clears the internal scanbuffers to allow the same code to be scanned
    : multiple times
    procedure OpenScanner
    : calls the connect function of the current comportcomponent
    procedure CloseScanner
    : calls the disconnect function of the current comportcomponent

frmTopMenuBase
  Changed design properties :
  -

  Added controls:
  + dxBarManBase: Menu and toolbar manager from Dev Express
  + the solar Menu structure (see below)
  + StandardMenuActionList
  + GoActionList

  Events:
  -

  Methods:
  -

Changes:
  MRA:25-AUG-2010. Addition of Copy and Paste-buttons.

*******************************************************************************)
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  dxBar, ComDrvN, dxBarDBNav, ExtCtrls, ActnList, StdActns, ShellAPI,
  FileCtrl, Dblup1a, dxCntner, dxTL, dxDBCtrl, dxDBGrid, StdCtrls, Dblup1b,
  PimsFRM, Menus, jpeg;

type
  TTopMenuBaseF = class(TPimsF)
    imgBandTexture: TImage;
    dxBarManBase: TdxBarManager;
    { File }
    dxBarSubItemFile: TdxBarSubItem;
    dxBarButtonExit: TdxBarButton;
    { Help }
    dxBarSubItemHelp: TdxBarSubItem;
    dxBarButtonHelpContents: TdxBarButton;
    dxBarButtonHelpIndex: TdxBarButton;
    dxBarButtonHelpABSHome: TdxBarButton;
    dxBarButtonHelpAbout: TdxBarButton;
    dxBarButtonOrderInfo: TdxBarButton;
    { Go }
    dxBarSubItemGo: TdxBarSubItem;
    { Grid Toolbar }
    dxBarDBNavigator: TdxBarDBNavigator;
    dxBarButtonFirst: TdxBarButton;
    dxBarButtonPrior: TdxBarButton;
    dxBarButtonNext: TdxBarButton;
    dxBarButtonLast: TdxBarButton;
    dxBarBDBNavInsert: TdxBarDBNavButton;
    dxBarBDBNavDelete: TdxBarDBNavButton;
    dxBarBDBNavPost: TdxBarDBNavButton;
    dxBarBDBNavCancel: TdxBarDBNavButton;
    dxBarButtonCustCol: TdxBarButton;
    dxBarButtonShowGroup: TdxBarButton;
    dxBarButtonRecordDetails: TdxBarButton;
    dxBarButtonEditMode: TdxBarButton;
    dxBarButtonExpand: TdxBarButton;
    dxBarButtonCollapse: TdxBarButton;
    dxBarButtonResetColumns: TdxBarButton;
    dxBarButtonExportAllHTML: TdxBarButton;
    dxBarButtonExportSelectionHTML: TdxBarButton;
    dxBarButtonExportAllXLS: TdxBarButton;
    dxBarButtonExportSelectionXLS: TdxBarButton;
    dxBarButtonSort: TdxBarButton;
    dxBarButtonSearch: TdxBarButton;
    { StandardMenuActionList and Actions }
    StandardMenuActionList: TActionList;
    FileExitAct: TAction;
    FilePrintAct: TAction;
    HelpAboutABSAct: TAction;
    HelpABSHomePageAct: TAction;
    HelpOrderingInfoAct: TAction;
    HelpSolarHomePageAct: TAction;
    HelpContentsAct: TAction;
    HelpIndexAct: TAction;
    dxBarButtonCopy: TdxBarButton;
    dxBarButtonPaste: TdxBarButton;
    procedure HelpContentsActExecute(Sender: TObject);
    procedure HelpIndexActExecute(Sender: TObject);
    procedure HelpABSHomePageActExecute(Sender: TObject);
    procedure HelpAboutABSActExecute(Sender: TObject);
    procedure HelpOrderingInfoExecute(Sender: TObject);
    procedure HelpSolarHomePageActExecute(Sender: TObject);
    procedure FileExitActExecute(Sender: TObject);
  private
    { Private declarations }
    function GetManualFileName: String;
  public
    { Public declarations }
  end;

var
  TopMenuBaseF: TTopMenuBaseF;

implementation

uses
  SystemDMT, AboutFRM, OrderInfoFRM, UPimsMessageRes;

{$R *.DFM}

procedure TTopMenuBaseF.HelpContentsActExecute(Sender: TObject);
begin
  inherited;
  if ShellExecute(Handle, PChar('OPEN'),
    PChar(GetManualFileName), nil, nil, SW_SHOWMAXIMIZED) <= 32 then
      DisplayMessage('Help Contents will be implemented soon!',
        mtInformation, [mbOk]);
end;

procedure TTopMenuBaseF.HelpIndexActExecute(Sender: TObject);
begin
  inherited;
  if ShellExecute(Handle, PChar('OPEN'),
    PChar(GetManualFileName), nil, nil, SW_SHOWMAXIMIZED) <= 32 then
      DisplayMessage('Help Index will be implemented soon!',
        mtInformation, [mbOk]);
end;

procedure TTopMenuBaseF.HelpABSHomePageActExecute(Sender: TObject);
begin
  inherited;
  if ShellExecute(Handle, PChar('OPEN'), // PIM-250
    PChar('http://www.gotlilabs.com'), nil, nil, SW_SHOWMAXIMIZED) <= 32 then
      DisplayMessage(SPimsCannotOpenHTML, mtInformation, [mbOk]);
end;

procedure TTopMenuBaseF.HelpSolarHomePageActExecute(Sender: TObject);
begin
  inherited;
  DisplayMessage(SPimsHomePage, mtInformation, [mbOk]);
{  if ShellExecute(Handle, PChar('OPEN'),
    PChar('http://212.153.240.15'), nil, nil, SW_SHOWMAXIMIZED) <= 32 then
      DisplayMessage(SSolarCannotOpenHTML, mtInformation, [mbOk]);}
end;

procedure TTopMenuBaseF.HelpAboutABSActExecute(Sender: TObject);
var
  frmAbout: TAboutF;
begin
  inherited;
  frmAbout := TAboutF.Create(Application);
  if SystemDM.EmbedDialogs then
    frmAbout.FormStyle := fsStayOnTop;
  try
    frmAbout.ShowModal;
  finally
    frmAbout.Free;
  end;
end;

procedure TTopMenuBaseF.HelpOrderingInfoExecute(Sender: TObject);
var
  frmOrderInfo: TOrderInfoF;
begin
  inherited;
  frmOrderInfo := TOrderInfoF.Create(Application);
  if SystemDM.EmbedDialogs then
    frmOrderInfo.FormStyle := fsStayOnTop;
  try
    frmOrderInfo.ShowModal;
  finally
    frmOrderInfo.Free;
  end;
end;

procedure TTopMenuBaseF.FileExitActExecute(Sender: TObject);
begin
  inherited;
  Close;
end;

function TTopMenuBaseF.GetManualFileName: String;
var
  SearchRec : TSearchRec;
  ManualFileDir: String;
  ManualFileName: String;
begin
  // find the Manual file
  // Get path for Manual file
  ManualFileDir := ExtractFilePath(Application.ExeName);

  // Strip Bin\   add Manual\
  ManualFileDir :=
    Copy(ManualFileDir, 1, (Length(ManualFileDir) - 4)) + 'Manual\';
  if not (DirectoryExists(ManualFileDir)) then
    ForceDirectories(ManualFileDir);
  ManualFileName := ManualFileDir + '*.pdf';

  if (FindFirst(ManualFileName, faAnyFile, SearchRec) = 0) then
    Result := ManualFileDir + SearchRec.Name
  else
    Result := '';

  // free memory for searchrecs
  FindClose(SearchRec);

end;

end.
