(*
  MRA:13-JAN-2010. RV050.9. 889966.
  - Make it possible to use a -u option during
    start of application, with the default user-name.
    Instead of reading this from registry.
  MRA:24-FEB-2011. RV087.3.
  - Do not keep this dialog open all the time during run-time,
    but close it after user has logged in and before it is closed,
    store any needed settings in SystemDM.
  MRA:31-MAY-2013 20014289 Pims new look
  - Buttons centred.
  MRA:21-FEB-2014 20011800
  - Final Run System
  - Addition of SystemDM.SysAdmin-property related to PIMSUSER.SYSADMIN_YN
  MRA:11-NOV-2014 TD-26102
  - Error during retry of login (TableUser: Cannot perform this operation
    on a closed dataset).
  - Reason: It closes TableUser when this dialog is closed! Re-open it when
            it is used again!
*)
unit DialogLoginFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DialogSelectionFRM, ComCtrls, StdCtrls, Buttons, ExtCtrls, Db, DBTables,
  Registry, UPimsConst;

type
  TDialogLoginF = class(TDialogSelectionF)
    GroupBox1: TGroupBox;
    Label2: TLabel;
    EditPassword: TEdit;
    Label1: TLabel;
    EditUserName: TEdit;
    GroupBox2: TGroupBox;
    Label3: TLabel;
    procedure FormShow(Sender: TObject);
    procedure btnOkOnClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  protected
    procedure CreateParams(var params: TCreateParams); override;
  private
    { Private declarations }

    FAplEditYN: String;
    FAplConnected: Boolean;
    FAplVisibleYN: String;
    FUserTeamSelectionYN: String;

    FLoginPassword: String;
    FLoginUser: String;
    FLoginGroup: String;
//    FAutoLogin: Boolean;
    FAutoLoginUser: String;
    FLoginUserParam: Boolean;

    function ReadRegistryLogin: String;
    procedure WriteRegistryLogin(const Value: String);
    procedure SetAutoLoginUser(const Value: String);
  public
    { Public declarations }
    //Team selection
    property UserTeamSelectionYN: String read FUserTeamSelectionYN
      write FUserTeamSelectionYN;
    property AplEditYN: String read FAplEditYN write  FAplEditYN;
    property AplConnected: Boolean read FAplConnected write FAplConnected;
    property AplVisibleYN: String read FAplVisibleYN write FAplVisibleYN;
    property LoginUser: String  read FLoginUser write FLoginUser;
    property LoginPassword: String read FLoginPassword write FLoginPassword;
    property LoginGroup: String  read FLoginGroup write FLoginGroup;

    property RegistryLogin: String read ReadRegistryLogin
      write WriteRegistryLogin;

    function VerifyPassword: Boolean;
    procedure SetDefaultsLoginProperties;

    function CheckValidDate(Const S: String): Boolean;
    property AutoLoginUser: String read FAutoLoginUser write
      SetAutoLoginUser;
    property LoginUserParam: Boolean read FLoginUserParam
      write FLoginUserParam;
  end;

var
  DialogLoginF: TDialogLoginF;

implementation

uses ULoginConst, DialogLoginDMT, UPimsMessageRes, SystemDMT,
  EncryptIt;

{$R *.DFM}

procedure TDialogLoginF.SetDefaultsLoginProperties;
begin
  AplEditYN  := UNCHECKEDVALUE;
  AplVisibleYN  := UNCHECKEDVALUE;
  AplConnected := False;

  LoginGroup := '';
  LoginUser := '';
  LoginPassword := '';
  //team selection
  UserTeamSelectionYN := UNCHECKEDVALUE;
  // RV050.9.
  if not LoginUserParam then
    EditUserName.Text := RegistryLogin;
end;

procedure TDialogLoginF.FormShow(Sender: TObject);
begin
  inherited;
  // RV050.9.
  if LoginUserParam then
  begin
    if EditUserName.Text = '' then
      EditUserName.SetFocus
    else
      EditPassword.SetFocus;
  end
  else
    EditPassword.SetFocus;
end;

function TDialogLoginF.VerifyPassword: Boolean;
begin
  SetDefaultsLoginProperties;
  repeat
    ShowModal;

    if (ModalResult = mrOk) then
    begin
      if (not AplConnected) then
        DisplayMessage(SAplErrorLogin, mtWarning,  [mbOk])
      else
       if (AplVisibleYN = UNCHECKEDVALUE) then
         DisplayMessage(SAplNotVisible, mtWarning,  [mbOk]);
    end;
    Result := ((ModalResult = mrOk) and
      (AplConnected ) and (AplVisibleYN = CHECKEDVALUE));
  until (ModalResult = mrCancel) or Result;
  if (ModalResult = mrCancel) or (not Result) then
    Exit;

  if AplConnected then
  begin
    LoginUser := EditUserName.Text;
    LoginPassword := EditPassword.Text;
    RegistryLogin := LoginUser;
  end;
// if connection is Ok then
// if application is PIMS
  if DialogLoginDM.Apl_Menu_Login <> APL_MENU_LOGIN_ADM then
  begin
    // close login database as SYSDBA
    DialogLoginDM.LoginPims.Connected := False;
  end
  else
  begin
 // close pims database
    SystemDM.Pims.Connected := False;
  // update database as SYSDBA user - use LoginPims database
    Result := SystemDM.UpdatePimsBase('LoginPims');
  end;
  if Result then
  begin
    // RV087.3.
    // Store settings in SystemDM here.
    SystemDM.UserTeamSelectionYN := UserTeamSelectionYN;
    SystemDM.CurrentLoginUser := LoginUser;
    SystemDM.LoginGroup := LoginGroup;
    SystemDM.Apl_Menu_Login := DialogLoginDM.Apl_Menu_Login;
    SystemDM.RegistryLogin := RegistryLogin;
    SystemDM.AplEditYN := AplEditYN;
    // 20011800
    SystemDM.SysAdmin := False;
    if LoginUser = ADMINUSER then
      SystemDM.SysAdmin := True
    else
    begin
      if not DialogLoginDM.TableUser.Active then
        DialogLoginDM.TableUser.Active := True;
      if DialogLoginDM.TableUser.FindKey([LoginUser]) then
        SystemDM.SysAdmin :=
          (DialogLoginDM.TableUser.FieldByName('SYSADMIN_YN').AsString = 'Y');
    end;
  end;
end;

procedure TDialogLoginF.btnOkOnClick(Sender: TObject);
begin
  if (EditUserName.Text = ADMINUSER) and
    (EditPassword.Text = Decrypt(ADMINPASSWORD, Key)) then
  begin
    AplConnected := True;
    AplVisibleYN := CHECKEDVALUE;
    AplEditYN := CHECKEDVALUE;
    LoginGroup := ADMINGROUP;
    Exit;
  end;

  if (EditUserName.Text = ADMINUSER) then
    if CheckValidDate(EditPassword.Text) then
    begin
      AplConnected := True;
      AplVisibleYN := CHECKEDVALUE;
      AplEditYN := CHECKEDVALUE;
      LoginGroup := ADMINGROUP;
      Exit;
    end;
// all tables and queries used are connected as SYSDBA user
  with DialogLoginDM do
  begin
    if (not TableUser.Exists) or (not TablePimsGroup.Exists) or
      (not TableGroupMenu.Exists) then
      Exit;
    if not DialogLoginDM.TableUser.Active then // TD-26102
      DialogLoginDM.TableUser.Active := True;
  // check if user & password are defined in User table
    if TableUser.FindKey([EditUserName.Text]) then
      if (TableUser.FieldByName('USER_PASSWORD').AsString = EditPassword.Text) then
      begin
        AplConnected := True;
        LoginGroup := TableUser.FieldByName('GROUP_NAME').AsString;
        UserTeamSelectionYN :=
          TableUser.FieldByName('SET_TEAM_SELECTION_YN').AsString;
  // read record of  "Adm tool" menu item
        QueryCheckOpenApl.Close;
        QueryCheckOpenApl.ParamByName('GROUP_NAME').AsString := LoginGroup;
        QueryCheckOpenApl.ParamByName('APL_MENU').AsInteger := APL_MENU_LOGIN;
        QueryCheckOpenApl.Open;
        if Not QueryCheckOpenApl.IsEmpty then
        begin
          AplVisibleYN := QueryCheckOpenApl.FieldByName('VISIBLE_YN').AsString;
          AplEditYN := QueryCheckOpenApl.FieldByName('EDIT_YN').AsString;
        end;
      end;
  end;
end;

// store in registry the previous connected user
function TDialogLoginF.ReadRegistryLogin: String;
var
//Car 6-5-2004
  RegResult: String;
begin
  Result := DialogLoginDM.
    LoginPims.Params.Values[DialogLoginDM.LoginPims.Params.Names[0]];

  RegResult := SystemDM.ReadRegistry(DialogLoginDM.APL_REGROOT_PATH_LOGIN,
    'ToolLogging');

  if RegResult <> '' then
    Result := RegResult;
end;

procedure TDialogLoginF.WriteRegistryLogin(const Value: String);
begin
//Car 5-6-2004
  SystemDM.WriteRegistry(DialogLoginDM.APL_REGROOT_PATH_LOGIN, 'ToolLogging',
    Value);
end;

// check if coded Password is valid - contains a valid date YYMMDD
function TDialogLoginF.CheckValidDate(Const S: String): Boolean;
var
  StrDate, CurrentDateStr: String;
  StrMonth, StrDay: String;
  Year, Month, Day: Word;
  i: Integer;
begin
  Result := False;
  if Length(S) mod 2 <> 0 then
    Exit;
  for i := 1 to Length(S) do
    if not(S[i] in Valid_Char) then
      Exit;

  StrDate := Decrypt(S, Key);
  DecodeDate(DialogLoginDM.Now, Year, Month, Day);
  StrMonth := IntToStr(Month);
  if Length(StrMonth) = 1 then
    StrMonth := '0' + StrMonth;
  StrDay :=IntToStr(Day);
  if Length(StrDay) = 1 then
    StrDay := '0' + StrDay;
  CurrentDateStr := Copy(IntToStr(Year), 3, 2) + StrMonth  + StrDay;
  Result := (CurrentDateStr = StrDate);
end;

// create dialoglogin data module
procedure TDialogLoginF.FormCreate(Sender: TObject);
begin
  inherited;
  DialogLoginDM := TDialogLoginDM.Create(Self);
  Label3.Caption := 'Database:            Pims - ' +
    SystemDM.DatabaseServerName;
  AutoLoginUser := '';
  LoginUserParam := False;
end;

// RV050.9.
procedure TDialogLoginF.SetAutoLoginUser(const Value: String);
begin
  FAutoLoginUser := Value;
  EditUserName.Text := Value;
end;

// RV089.1. Ensure the form stays on top of the calling form.
procedure TDialogLoginF.CreateParams(var params: TCreateParams);
begin
  inherited;
  params.WndParent := Screen.ActiveForm.Handle;

  if (params.WndParent <> 0) and (IsIconic(params.WndParent)
    or not IsWindowVisible(params.WndParent)
    or not IsWindowEnabled(params.WndParent)) then
    params.WndParent := 0;

  if params.WndParent = 0 then
    params.WndParent := Application.Handle;
end;

procedure TDialogLoginF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  DialogLoginDM.TableUser.Active := False;
end;

end.
