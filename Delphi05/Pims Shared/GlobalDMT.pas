(*
  Changes:
    RV001: (Revision 001). Order 550459.
    2.0.139.122 - Bug fix wrong calculation of Overtime-hours. This could
                  happen when also 'exceptional hours' are defined and
                  resulted in wrong overtime-hours (too less).
                - Logging added. When DEBUG is defined for this project,
                  all hour-calculations are logged.
    RV004: (Revision 004). Overtime-fix.
    2.0.139.125 - There is still a bug with Overtime-hours.
                  This is possible if also exceptional hours are defined.
                  Procedure to determine overtime has been changed to solve this.
    RV015. (Revision 015). Round Minutes.
    2.0.139.12x  - Rounding minutes-procedure rewritten, because Except. Hours were not
                  included during rounding of the salary-hours.
    RV020. (Revision 020). Exceptional + Regular hours + non-paid breaks.
    2.0.139.138 - Non-paid breaks were sometimes subtracted again during calculation of
                  exceptional hours, resulting in too much regular hours when
                  exceptional hours should be calculated.
                  This had to do with overnight-scans that were not recognized
                  correct during determination of breaks, during exceptional hours
                  calculations. Now these scans are split to get the correct result.
    RV023. (Revision 023).
      Detect if exceptional hours are made during the period there are also
      Overtime hours made. This is needed for Paris + Export Paryoll.
      - Database-changes: Table PRODHOURPEREMPLPERTYPE is changed !
        Use script 40.
      Also changed:
      - qryProdHour gave an error because it was looking at combination of
        Plant and Shift to get the Hourtype, but when Shifts are defined
        wrong (no shift 1), then it can lead to Prim. Key-violations!
        Used in: UpdateProductionHour().
        The HOURTYPE_NUMBER is now removed from qryProdHour-query, because
        it was not used!
      - Part of BooksExceptionalTime was not the same as in D7-version. This
        is corrected.
      - Parts that used 'InsertQuery' (created/freed) has been remarked,
        because it was not used at all.
      - Round minutes for PRODHOURPEREMPLPERTYPE in a different way! Because
        now it only round parts of this table.
    RV024. (Revision 024).
      Problem when determining combination of Exceptional and Overtime.
      Only use the exception hours that were just found! Otherwise
      it gives too much combined hours.
    RV025. (Revision 025). MRA:1-APR-2009.
      An extra check must be made to determine exceptional hours made during
      overtime hours. For this purpose the EXCEPTIONALHOURDEF-table has an
      extra hour-type (OVERTIME_HOUR_TYPE) that can be used to book this type
      of hours. During BooksOvertime these combined hours must be handled.
    RV028. (Revision 028). MRA:26-MAY-2009. Order-502383.
      Store extra field 'prod_min_excl_break' in ProdHourPerEmplPerType-table.
    RV033.1. MRA:17-SEP-2009.
      - When scan is processed for hour-calculation, check for 'ignore overtime'
        that can be set for the workspot when it has an hourtype has been set.
    RV033.4. MRA:18-SEP-2009.
      - Recalculate ABSENCEHOUR-table for time-for-time hours made during
        overtime. Added a procedure that can do this.
    RV039.1. MRA:21-OCT-2009.
      - Rounding problem. The rounding was done in such a way that
        the total minutes for 1 salary-date could be less then the original
        total of minutes, e.g. a difference of 30 minutes instead of 15 minutes
        at the most. A method must be used that ensures the total
        of minutes on 1 salary-date (that has multiple records for multiple
        hour-types) stays (almost) the same, so it is no more than 15 minutes
        difference, when rounding/truncating is set to 15 minutes.
    RV039.2. MRA:22-OCT-2009.
      - Corrections made for overtime-hour calculation. This went wrong
        when there were multiple overtime-hour definitions.
    RV039.3. MRA:23-OCT-2009.
      - When hour-type is setup as 'ignore overtime' also ignore
        exceptional hours.
    RV040.1. MRA:29-OCT-2009.
      - Determination of 'lastscanofday' was wrong which could give wrong
        results for rounding of minutes.
    RV040.2. MRA:30-OCT-2009.
      - Fix for overtime minutes. Minutes that were booked on exceptional
        hours must also booked on overtime when it is in the overtime period.
    RV040.3. MRA:4-NOV-2009.
      - Rounding problem. This gives wrong rounding for PCO. To solve this
        rounding-correction must only be done for 'truncate'.
    RV045.1. MRA:23-NOV-2009. SR-889942.
      - Addition of 2 fields per hourtype related to Overtime:
        - Time for Time
        - Bonus in Money
      This can be used instead of setting on Contract Group-level, but
      only if there is not any contract group with one of these settings = 'Y'.
    MRA:24-FEB-2010. RV055.1. Round Minutes Version 3.
    - Rounding minutes for Salary + ProdHourPerEmplPerType is done in a
      different way. See comments at the new procedures.
    - Also: Renamed this datamodule from CalcSalaryDMT to GlobalDMT for
            compatibility with Delphi7-version.
    MRA:23-MAR-2010. RV056.1.
    - UnProcessProcessScans:
      - When called from 'ProcessPlannedAbsence' (or 'TimeRecScanning' ?)
        it should always recalculate the Production-hours (PRODHOURPEREMPLOYEE),
        instead of only Salary-hours (and PRODHOURPEREMPLPERTYPE).
        Reason: ProductionHours can be wrong when afterwards e.g. timeblocks,
                breaks are changed.
    MRA:25-MAR-2010. RV057.1.
    - Calculation of productionhours during Process Planned Absence
      gave double values for overtime-period = 1 day.
    MRA:8-APR-2010. RV060.1.
    - Problem with calculation of exceptional hours. There was a bug during
      calculation of breaks, resulting in less exceptional hours.
    SO: 09-JUN-2010 REV065.9
    - the salary hours created from manually entered production hours are shown in
    different color and read only
    - added a new parameter with default value to function UpdateSalaryHour
    MRA:14-JUL-2010. RV063.1. RV067.MRA.7.
    - Put back old method of rounding. New method (RV055.1) is wrong!
    MRA:6-OCT-2010. RV071.6. 550497.
    - When the hourtype is set as 'Overtime' and 'Shorter Working Week'
      (ADV in dutch), then all overtime hours that are calculated for that
      Hourtype will also be booked in the balance of the employee as
      earned shorter working week. Earned shorter working week is
      stored per year and per employee in table ABSENCETOTALS in
      the column EARNED_SHORTWWEEK_MINUTE.
    - Also recalculate Earned_Shortwweek_minute.
  MRA:27-OCT-2010. RV075.2. Bugfix.
  - Add RecalculateBalances procedure.
  MRA:2-NOV-2010. RV075.12. Bugfix.
  - Hours per employee / Hour calculation
    - Sometimes the hours are not calculated OK for
      overtime. This can happen because of rounding.
    - Solved by rounding at end of procedure that determine
      overtime and exceptional hours.
  MRA:2-NOV-2010. RV075.2.
  - Changes for PHEPT-records.
  MRA:2-NOV-2010. RV076.3.
  - Global Book overtime hours-routine
    - Do not round after this routine,
      only after Book except.hours-routine.
  MRA:3-NOV-2010. RV076.4.
  - Rounding after Book except.hours-routine.
    - Because of rounding after Book exc.hrs. routine,
      it is possible hours were found for exc.hrs. but
      they are removed during rounding.
      Example:
        Rounding is 'truncate 15 min.'.
        12 minutes was found, but after rounding this is 0.
      This means 12 minutes are lost, which can result in losing
      more minutes afterwards.
    - Solution: Solve this by returning the minutes that were left
      after rounding in Rounding-routine.
    - EXTRA: Only round for a given hourtype for 'lost minutes'!
  MRA:8-NOV-2010. RV077.2.
  - Because of rounding the PHEPT-table is not
    correct anymore. Rounding is turned off for RFL.
  MRA:12-NOV-2010. RV079.4. 550497. Addition.
  - Balance Counters. Because balances must be shown on multiple places
    in Pims, it is now put in this DMT as a class.
  MRA:22-NOV-2010. RV080.4.
  - Hour Calculation and overtime
    - When hour calculation is done and manual salary hours exists,
      then the calculation gives wrong results.
      Cause: When it processes the scans it already takes
      ALL manual hours for the complete overtime-period to determine
      if the overtime-period has been reached.
    - When determining overtime, only look for registered hours (manual
      and absence hours) from the start of the overtime-period till the
      bookingdate!
  MRA:24-NOV-2010. RV080.6.
  -  Hour Calculation and hourtype on workspot
    - When an hourtype is set to a workspot and a scan
      is processed that is linked to this workspot, then
      it does not always use this workspot-hourtype.
      Only when there were regular hours made, it uses
      this workspot-hourtype.
    - Solution: It should always use this workspot-hourtype!
  MRA:24-NOV-2010. RV080.7.
  - The revision RV076.4. (rounding of except. hrs. in-between
    hour calculations), should be disabled, because it gives more
    problems than that it solves anything.
  - Advice: Use 'round 15 minutes' instead of 'trunc 15 minutes',
            because this gives problems with lost minutes.
  MRA:7-DEC-2010 RV082.13. RV080.4. !!!CANCELLED!!!
  - Hour Calculation and overtime
  - REMARK:
    - This revision must be turned back to it's old state.
    - Reason: It does not calculate overtime when it should do this,
      for example, when there is absence booked on days after
      the scans.
    - Advice: When manual hours are entered which should be booked
      themselves overtime-hours, then the hourtype for these manual hours
      must be set to 'Ignore overtime'.
  MRA:10-DEC-2010 RV082.15.
  - Hour Calculation - Debug
    - Added 'Ignore Overtime' for hourtype to debug-info.
  MRA:27-JAN-2011 RV085.15. Small change. SC-50016904.
  - Added function PlantIsBelgium that looks if there is plant
    linked to country Belgium.
  MRA:28-OCT-2011 RV100.2. 20011796
  - Change of RecalcEarnedTFTHours-procedure so it can also return the
    earned minutes for TFT, without making changes to balance.
  MRA:1-NOV-2011 RV100.1. 20012124. Tailor made CVL.
  - Hour type calculation
    - When 'overruled contract group' is not null for the
      workspot of the scan that is about to be calculated,
      then use that overruled contract group instead of the
      employee-contract group to book the hours for
      exceptional, overtime and regular hour.
  - Changed queries (they now search on contractgroup instead by employee):
    - qryExceptHourDef
    - qryOvertimeDef
    - qryContractGroup
  MRA:2-DEC-2011 RV103.1. SO-20011799. Worked hours during bank holiday.
  - Modification calculation worked hours on bank holiday.
    When hours were made during a bank holiday, then these
    hours must be booked on an hourtype that is defined
    for that bank holiday or on the contract group.
    During this the availability must be changed from
    bank holiday to available and the hours that were booked
    for bank holiday must be reprocessed.
  MRA:8-DEC-2011 RV103.2. 20011796.
  - Change of RecalcEarnedSWWHours-procedure so it can also return the
    earned minutes for SWW, without making changes to balance.
  MRA:11-JAN-2012. RV104.1. 20011799. Worked hours during bank holiday. Addition.
  - An existing function from 'GlobalDMT' is changed/used for this purpose.
  MRA:16-MAY-2012 20013183.
  - Addition of travel time for balance-calculation.
  MRA:4-JUN-2012 20013271.
  - When UnProcessProcess is called from 'Process Planned Absence' it
    can go wrong with overnight-scans resulting in no calculation of production
    and/or salary hours.
  MRA:3-OCT-2012 20013489  Overnight-Shift-System
  - When updating Production Hours Per Employee, then use SHIFT_DATE (based on
    shift) as production-date. This means it can be a different date when
    an overnight-shift is involved.
  - When using Process Planned Absence, it can be needed to update a
    scan-record, because the SHIFT_DATE was not correct yet. In that case
    the scan-record must be updated.
  MRA:19-OCT-2012 20013489 Rework
  - When a (manual) scan was deleted, it did not recalculate the salary.
  MRA:23-OCT-2012 20013489 Overnight-Shift-System.
  - Always store production on start of shift.
  MRA:10-OCT-2013 20014327
  - Make use of Overnight-Shift-System optional.
  - Changed parameters for UnProcessProcessScans to make it equal to the
    same procedure used in Delphi7.
  MRA:5-NOV-2013 TD-23416 (PCO) -> This is NOT needed for Overnight-Shift-System
  - Missing hours:
    - Sometimes salary hours are not booked. This can happen with a scan like:
      - Sunday -> DateIn=2:30 DateOut=9:00
      - This should be booked on previous day (Saturday), but then it can happen
        it is not booked at all, because it is part of the previous week-period,
        resulting in missing hours.
      - To solve it, book it on the same day of the scan.
    - Also when there is a scan like:
      - Saturday -> DateIn=14:30 DateOut=2:30 (next day!)
      - Then this can result in no booking of production hours on the next day,
        because a part falls in one period and the last part falls in next
        period.
  MRA:13-NOV-2013 TD-23386
  - Hourtype on shift should not result in double hours.
    - PopulateIDCard: Also determine hourtype on shift
      - Related to this: Also determine hourtype_number of workspot
      - Related to this: Also determine Ignore_overtime_yn
  MRA:13-DEC-2013 TD-23817  Time correction needs to be removed from system
  - Hour calculation
    - It can happen because of rounding a scan results in an extra salary line
      of -15 minutes, when there are several different hour type involved.
    - Cause: It books negative regular hours that are left afterwards. This
      must be prevented! These must not be booked at all.
      NOTE: Manual negative hours must still be accepted.
  MRA:6-MAR-2015 TD-26818
  - Double production hours
    - When only absence (after adding this in emp. availability)
      should be calculated then when having from-to-date
      pointing to this absence-day, then it still recalculated the prod. hours
      for previous days resulting in double hours.
  MRA:17-MAR-2015 20015346
  - Store scans in seconds:
    - Round scans to minutes, before hours are calculated
  MRA:23-JUN-2015 PIM-49
  - NOTE: When 'Shift Date System' is NOT used.
  - Not all scan / production time are included in report hours per employee.
  - When using Period=Day in Contract Group then production hours are not
    calculated correct! It gives less production hours when there are
    scans involved that go through the night.
  - Cause: It calculates the production hours based on overnight-scan and
           stores it in 2 days, but during calculation it deletes the second
           day, so that hours are missing later, resulting in less hours.
  - Note: The salary hours are correct.
  - After a fix was done:
    - Now it goes OK but only when you use Process Planned Absence and
      recalculate a week-period. When only 1 day is recalculated then it still
      can go wrong (via Process Planned Absence or via
      Time Reg. Scanning-dialog).
  - A function has been added: ProdDateCheck. This checks if a prod-date is
    part of a scan (that can start on one day and end on next day). This
    prevents hours are not calculated because of a scan that ends on next day,
    but the hours till midnight should be calculated as production hours.
  - Example of what went wrong:
            TU  (30-6)                  WE (1-7)           TH (2-7)        FR (3-7)
    |---------------------|--------------------------|-------------------|------------------|
    |Scan 22-02:30 |-------------|   22-02:30 |------------| 22-02:30|-----------|
    |--------------------------|--------------------------|--------------------------|
    |Prod.     2:00|-----|||-----|2:30    2:00|-----|||-----|2:30     2:00|----|||----|
    |--------------------------|--------------------------|--------------------------|
    |Sal.      4:30|-------------|         4:30|-------------|        4:30|-----------|
    |--------------------------|--------------------------|--------------------------|
    Here, when scan of 1-7 22:00-02:30 was changed to 22:00-01:30 it resulted
    in less prod. hours on 2-7 because the scan of 2-7 22:00-02:30 was not taken
    into account. Sal. hours were OK.
  PIM-87 Related to this order
  - IMPORTANT: It does not calculate the whole period when called from Process
    Planned Absence and from-date is about 1 day and period > day.
    For example when overtime-period is week and you entered
    from-to-date that is in the middle of that week, then it does not calculate
    salary-hours before that.
  ABS-19136 Hour Calculation Test
  - When rounding is set to truncate to 15 minutes it can give a wrong result
    when more than 1 hour-type is involved because it divides the missing minutes
    over other hour-types. This can give a result where overtime-time is
    15 minutes higher regular hours is 15 minutes lower then was expected,
    at the end it can give less regular hours then was expected.
  - To solve this it must use a different sequence for the hour-types.
  MRA:23-OCT-2017 PIM-319
  - Do not subtract breaks from production hours
  MRA:12-FEB-2018 GLOB3-81
  - Change for overtime hours (only used for NTS)
  - Add from-to-time as extra restriction for day-period
  - Add day-of-week as extra setting.
  MRA:23-MAR-2018 GLOB3-81 Rework
  - See above.
  - Because of new from-to-time-settings, we need to know at what time the
    overtime started! This is needed when there are multiple overtime lines
    made like:
    - 1: MO: 7:30 - 23:59 - From 0:00 to 7:00 Hourtype=25
    - 2: MO: 7:30 - 23:59 - From 7:00 to 15:00 Hourtype=26
    - 3: MO: 7:30 - 23:59 - From 15:00 to 23:59 Hourtype=27
  - We have to prevent it takes the first line because in that case there will
    no overtime yet, because it did not happen during that time.
  - Because of this, first check if the date-out-time is within the from-to
    that must be checked when it is in overtime. Only if that is the case,
    then check further, otherwise skip the overtime-line and try the
    next one.
  MRA:29-JUN-2018 GLOB3-60
  - Extension 4 to 10 blocks for planning
  MRA:9-NOV-2018 PIM-408
  - Problem with exceptional hour calculation
  MRA:3-DEC-2018 PIM-406
  - Overtime gives a wrong result.
  - Example:
    - 1: MO:  7:30 - 10:30 - From 00:00 to 23:59
    - 2: MO: 10:30 - 24:00 - From 00:00 to 23:59
    Result should be:
    When there are 12:00 hours made.
    Reg.Min | SMin  | EMin  | OTime
    12:00   | 7:30  | 10:30 | 3:00
    12:00   | 10:30 | 24:00 | 1:30
    Regular time: 7:30.
  MRA:22-FEB-2019 GLOB3-223
  - Restructure availability of functionality made for NTS
*)

unit GlobalDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SystemDMT, Db, DBTables, UScannedIDCard, DBClient;

type
  TGlobalDM = class(TDataModule)
    qrySalMin: TQuery;
    qryContractgroup: TQuery;
    qrySalaryMinXXX: TQuery;
    qryAbsenceMinXXX: TQuery;
    qryOvertimeDef: TQuery;
    AQuery: TQuery;
    qryAbsTotTFT: TQuery;
    qryAbsTotTFTUpdate: TQuery;
    qryAbsTotTFTInsert: TQuery;
    qryExceptHourDef: TQuery;
    qrySalary: TQuery;
    qrySalaryUpdate: TQuery;
    qrySalaryDelete: TQuery;
    qrySalaryInsert: TQuery;
    qryWSHourtype: TQuery;
    qryProdHourPEPT: TQuery;
    qryProdHourPEPTUpdate: TQuery;
    qryProdHourPEPTInsert: TQuery;
    qryProdHour: TQuery;
    qryProdHourDelete: TQuery;
    qryProdHourUpdate: TQuery;
    qryProdHourInsert: TQuery;
    qryWSPerEmp: TQuery;
    qryWSPerEmpUpdate: TQuery;
    qryWSPerEmpInsert: TQuery;
    qryExcepBFOvertime: TQuery;
    qryProdHourPEPTDelete: TQuery;
    qryEmpAvailUpdate1: TQuery;
    qryEmpAvailUpdate2: TQuery;
    qryEmpAvailUpdate3: TQuery;
    qryEmpAvailUpdate4: TQuery;
    qryIllnessMsg: TQuery;
    qryIllnessMsgUpdate: TQuery;
    qryPopIDCard: TQuery;
    qryProdHourPEPTDeleteOneRec: TQuery;
    qrySalaryAbsenceMin: TQuery;
    qrySalaryOvertimeMin: TQuery;
    qrySHE: TQuery;
    qryPHEPT: TQuery;
    qryPHEPTUpdate: TQuery;
    qryPEPT: TQuery;
    qryPEPTEB: TQuery;
    qryPEPTEBUpdate: TQuery;
    qryGetTFTHours: TQuery;
    qryCheckEmpTFT: TQuery;
    qryWorkspot: TQuery;
    qryUPSGetTFTBonus: TQuery;
    qryUPSDeleteAbsenceHours: TQuery;
    qryUPSDeleteSalary: TQuery;
    qryUPSDeleteProduction: TQuery;
    qryWork: TQuery;
    qryContractGroupTFT: TQuery;
    cdsSHEprio: TClientDataSet;
    qrySHEround: TQuery;
    cdsSHEprioSALARY_DATE: TDateTimeField;
    cdsSHEprioEMPLOYEE_NUMBER: TIntegerField;
    cdsSHEprioHOURTYPE_NUMBER: TIntegerField;
    cdsSHEprioMANUAL_YN: TStringField;
    cdsSHEprioSALARY_MINUTE: TIntegerField;
    cdsSHEprioPRIORITY: TIntegerField;
    cdsSHEprioSALARY_MINUTE_ROUNDED: TIntegerField;
    cdsSHEprioMINUTE_MOD_PART: TIntegerField;
    cdsPHEPTprio: TClientDataSet;
    qryPHEPTround: TQuery;
    cdsPHEPTprioPRODHOUREMPLOYEE_DATE: TDateField;
    cdsPHEPTprioPLANT_CODE: TStringField;
    cdsPHEPTprioSHIFT_NUMBER: TIntegerField;
    cdsPHEPTprioEMPLOYEE_NUMBER: TIntegerField;
    cdsPHEPTprioWORKSPOT_CODE: TStringField;
    cdsPHEPTprioJOB_CODE: TStringField;
    cdsPHEPTprioMANUAL_YN: TStringField;
    cdsPHEPTprioHOURTYPE_NUMBER: TIntegerField;
    cdsPHEPTprioPRODUCTION_MINUTE: TIntegerField;
    cdsPHEPTprioPROD_MIN_EXCL_BREAK: TIntegerField;
    cdsPHEPTprioMINUTE_MOD_PART: TIntegerField;
    cdsPHEPTprioPRODUCTION_MINUTE_ROUNDED: TIntegerField;
    cdsPHEPTprioPRIORITY: TIntegerField;
    qryAbsenceTotal: TQuery;
    qryGetSWWHours: TQuery;
    qryBalanceCounters: TQuery;
    qryMaxSalaryBalance: TQuery;
    qryActiveCounters: TQuery;
    qryAbsenceTypePerCountryExist: TQuery;
    qryPlantCountryBelgium: TQuery;
    qryWorkspotContractgroup: TQuery;
    qryHTONBK: TQuery;
    qryOvertimeParams: TQuery;
    qryAHE: TQuery;
    qryAHEDelete: TQuery;
    qryTRSUpdate: TQuery;
    qryShiftHT: TQuery;
    qryEmpAvailUpdate5: TQuery;
    qryEmpAvailUpdate6: TQuery;
    qryEmpAvailUpdate7: TQuery;
    qryEmpAvailUpdate8: TQuery;
    qryEmpAvailUpdate9: TQuery;
    qryEmpAvailUpdate10: TQuery;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
    FStandardMinutesPerMonth: Integer;
    FPayedBreakSave: Integer;
    FOvertimeType: TOvertime;
    function FindWorkspotOverruledContractGroup(APlantCode,
      AWorkspotCode, ADefaultContractGroupCode: String): String;
    procedure FindContractGroup(var AIDCard: TScannedIDCard);
  public
    { Public declarations }
    procedure ProcessTimeRecording(AQuery: TQuery; EmployeeIDCard: TScannedIDCard;
      FirstScan, LastScan, UpdateSalMin, UpdateProdMinOut, UpdateProdMinIn: Boolean;
      DeleteAction: Boolean; DeleteEmployeeIDCard: TScannedIDCard;
      LastScanRecordForDay: Boolean);
{    procedure UnprocessProcessScans(AQuery: TQuery; AFromDate: TDateTime;
      AIDCard: TScannedIDCard; AProcessSalary, ADeleteAction: Boolean;
      AProdDate1, AProdDate2, AProdDate3, AProdDate4: TDateTime;
      ARecalcProdHours: Boolean;
      AFromProcessPlannedAbsence: Boolean=False;
      AManual: Boolean=False); }
    procedure UnprocessProcessScans(AQuery: TQuery;
      AIDCard: TScannedIDCard;
      AFromDate, AProdDate1, AProdDate2, AProdDate3, AProdDate4: TDateTime;
      AProcessSalary, ADeleteAction: Boolean;
      ARecalcProdHours: Boolean;
      AFromProcessPlannedAbsence: Boolean=False;
      AManual: Boolean=False);
    procedure GetWorkedMin(var WorkedMin: Integer;
      EmployeeNumber: Integer; StartDate, EndDate: TDateTime);
    procedure ComputeOvertimePeriod(var StartDate, EndDate: TDateTime;
      Employeedata: TScannedIDCard);
    function AbsenceTotalSelect(AFieldName: String; ABookingDay: TDateTime;
      AEmployeeNumber: Integer; var AMinutes: Double): Boolean;
    procedure AbsenceTotalInsert(AFieldName: String; ABookingDay: TDateTime;
      AEmployeeNumber: Integer; AMinutes: Double);
    procedure AbsenceTotalUpdate(AFieldName: String; ABookingDay: TDateTime;
      AEmployeeNumber: Integer; AMinutes: Double);
    function UpdateAbsenceTotal(AFieldName: String;
      ABookingDay: TDateTime; AEmployeeNumber: Integer;
      AMinutes: Double; AOverwrite: Boolean): Integer;
(*    function UpdateAbsenceTotal(BookingDay: TDateTime;
      EmployeeData: TScannedIDCard; Minutes: Double): integer; *)
(*    function UpdateProductionHourXXX(
      ProductionDate: TDateTime;
      EmployeeData: TScannedIDCard; Minutes, PayedBreaks: integer;
      Manual: string):integer; *)
    function UpdateProductionHour((* AQuery: Tquery; RV023. *)
      ProductionDate: TDateTime;
      EmployeeData: TScannedIDCard; Minutes, PayedBreaks: integer;
      Manual: string):integer;
    function DetectExceptHourDefDuringOvertimeHourtype( {RV025}
      AEmployeeData: TScannedIDCard;
      AExceptRegularMin: Integer): boolean;
    function DetectOvertimeNonOvertime(ABookingDay: TDateTime; {RV025}
      AWorkedMin: Integer; AEmployeeData: TScannedIDCard;
      AManual: String;
      VAR ANonOvertimeMin: Integer;
      VAR AOvertimeMin: Integer): boolean;
    function BooksOverTimeMain(BookingDay: TdateTime; {RV025}
      WorkedMin: Integer; EmployeeData: TScannedIDcard;
      Manual: String;
      ACombinedHourSystem: Boolean;
      ADetect: Boolean;
      VAR ANonOvertimeMin: Integer;
      VAR AOvertimeMin: Integer;
      ADetectedOvertimeMin: Integer; // RV040.2.
      ADetectedNonOvertimeMin: Integer // RV040.2.
      ): Integer;
    function BooksOverTime(ABookingDay: TDateTime; {RV025}
      AWorkedMin: Integer; AEmployeeData: TScannedIDcard;
      AManual: String;
      ADetectedOvertimeMin: Integer; // RV040.2.
      ADetectedNonOvertimeMin: Integer // RV040.2.
      ): Integer;
    function BooksOverTimeCombinedHours(ABookingDay: TdateTime;
      AWorkedMin: Integer; AEmployeeData: TScannedIDcard;
      AManual: String
      ): Integer;
    function OverTimeDetect(ABookingDay: TdateTime; {RV025}
      AWorkedMin: Integer; AEmployeeData: TScannedIDcard;
      AManual: String;
      VAR ANonOvertimeMin: Integer;
      VAR AOvertimeMin: Integer
      ): Integer;
    function BooksExceptionalTimeMain((* AQuery: TQuery; RV023.*) {RV025}
      BookingDay: TDateTime;
      ExceptRegularMin: Integer;
      EmployeeData: TScannedIDcard;
      Manual: String;
      ADuringOvertime: Boolean): Integer;
    function BooksExceptionalTime( {RV025}
      ABookingDay: TDateTime;
      AExceptRegularMin: Integer; AEmployeeData: TScannedIDcard;
      AManual: String
      ): Integer;
    function BooksExceptionalTimeDuringOvertime( {RV025}
      ABookingDay: TDateTime;
      AExceptRegularMin: Integer; AEmployeeData: TScannedIDcard;
      AManual: String
      ): Integer;
    //REV065.9 Added FromManualProd parameter with default value 'N'
    //'Y' for the salary records created from manually entered production records
    function UpdateSalaryHour(SalaryDate: TDateTime;
      EmployeeData: TScannedIDCard;
      HourTypeNumber, Minutes: Integer; Manual: string;
      Replace: Boolean;
      UpdateYes: Boolean (* RV023 *) ;
      FromManualProd: String = 'N';
      ARoundMinutes: Boolean = True): Integer;
    procedure RoundMinutesProdHourPerEmpPerType(ABookingDay: TDateTime; (* RV023 *) // RV055.1. // RV063.1.
      AEmployeeData: TScannedIDCard;
      AHourTypeNumber, AMinutes: Integer;
      AManual: String);
    procedure BooksRegularSalaryHours((* AQuery: TQuery; RV023. *)
      BookingDay: TDateTime;
      EmployeeData: TScannedIDcard; BookMinutes: Integer; Manual: String);
    procedure FillProdHourPerHourType(EmployeeData: TScannedIDCard;
     ProductionDate: TDateTime; HourType, Minutes: Integer; Manual: String;
     InsertProdHrs: Boolean);
    function DetermineExceptionalBeforeOvertime(
      EmployeeData: TScannedIDCard): Boolean;
    procedure DeleteProdHourForAllTypes(EmployeeData: TScannedIDCard;
      FromDate, ToDate: TDateTime; Manual: String);
    // MRA:06-MAR-2008 RV004. Overtime-fix.
(*    function OvertimeCorrection(BookingDay: TDateTime;
      WorkedMin: Integer; EmployeeData: TScannedIDcard;
      Manual: String): Integer; *)
    // MRA:03-DEC-2008 RV014.
    // RV076.4.
    function ContractRoundMinutes(AEmployeeIDCard: TScannedIDCard; // RV063.1.
      ABookingDay: TDateTime; AHourTypeNumber: Integer=-1): Integer;
    // RV055.1. RV063.1.
(*
    procedure ContractRoundMinutesSalaryV3(AEmployeeIDCard: TScannedIDCard;
      ABookingDay: TDateTime);
    procedure ContractRoundMinutesPHEPT(AEmployeeIDCard: TScannedIDCard;
      ABookingDay: TDateTime);
*)
    procedure ProcessIllnessMessage(EmployeeIDCard: TScannedIDCard);
    procedure PopulateIDCard(var AIDCard: TScannedIDCard; ADataSet: TDataSet);
    function RecalcEarnedTFTHours(AEmployeeNumber: Integer;
      ADateFrom: TDatetime; ADateTo: TDateTime;
      AUpdateBalance: Boolean=True;
      AMyDate: Boolean=False): Double;
    // RV100.2.
    function DetermineEarnedTFTHours(AEmployeeNumber: Integer;
      ADateFrom: TDatetime; ADateTo: TDateTime; AMyDate: Boolean=False): Double;
    function CalcEarnedTFTHours(ATFTQuery: TQuery): Double;
    // RV103.2.
    function RecalcEarnedSWWHours(AEmployeeNumber: Integer;
      ADateFrom: TDatetime; ADateTo: TDateTime;
      AUpdateBalance: Boolean=True;
      AMyDate: Boolean=False): Double;
    // RV103.2.
    function DetermineEarnedSWWHours(AEmployeeNumber: Integer;
      ADateFrom: TDatetime; ADateTo: TDateTime; AMyDate: Boolean=False): Double;
    procedure RecalculateBalances(AEmployeeNumber: Integer;
      ADateFrom, ADateTo: TDateTime);
    function ContractGroupTFTExists: Boolean;
    procedure UpdatePHEPT(AEmployeeData: TScannedIDCard;
      AProductionDate: TDateTime; AHourType, AMinutes: Integer; AManual: String;
      AInsertProdHrs: Boolean);
    function PlantIsBelgium: Boolean;
    procedure UpdateEMA(AAbsenceReason: String;
      AEmployeeIDCard: TScannedIDCard; AStartDate: TDateTime;  AMore: Integer);
    procedure DetermineTimeForTimeSettings(ADataSet: TDataSet;
      var ATimeForTimeYN, ABonusInMoneyYN,
      AShorterWorkingWeekYN: String); // RV103.1.
    function DetermineTimeForTime(ADataSet: TDataSet; AEarnedTimeTFT: Double;
      AEarnedMin: Integer): Double; // RV103.1.
    function DetermineShorterWorkingWeek(ADataSet: TDataSet;
      AEarnedTimeSWW: Double; AEarnedMin: Integer): Double; // RV103.1.
    procedure BooksTFT(ABookingDay: TDateTime;
      AEmployeeData: TScannedIDCard; AEarnedTimeTFT: Double); // RV103.1.
    procedure BooksSWW(ABookingDay: TDateTime;
      AEmployeeData: TScannedIDCard; AEarnedTimeSWW: Double); // RV103.1.
    function BooksWorkedHoursOnBankHoliday(ABookingDay: TDateTime;
      AWorkedMin: Integer; AEmployeeData: TScannedIDcard;
      AManual: String;
      AUpdateYes: Boolean=True): Integer; // RV103.1.
    procedure UpdateTimeRegScanning(AIDCard: TScannedIDCard;
      AShiftDate: TDateTime);
    property StandardMinutesPerMonth: Integer read FStandardMinutesPerMonth
      write FStandardMinutesPerMonth;
    property PayedBreakSave: Integer read FPayedBreakSave
      write FPayedBreakSave; // RV028.
    property OvertimeType: TOvertime read FOvertimeType write FOvertimeType; // GLOB3-81
  end;

// RV079.4. Balance Counters Routines put in Class.
type
  TBalanceCounters = class
  private
    // Private declarations
    FEmployeeNumber: Integer;
    FYear: Integer;
    FDateFrom: TDateTime;
  public
    // Public declarations
    procedure GetCounters;
    procedure ComputeHoliday(var Remaining, Normal, Used,
      Planned, Available: String; RemainingVisible: Boolean=True);
    procedure ComputeIllness(var CurrentYear, PreviousYears: string);
    procedure ComputeTimeForTime(var Remaining, Earned, Used, Planned,
      Available: String);
    procedure ComputeWorkTime(var Remaining, Earned, Used,
      Planned, Available: String);
    procedure ComputeHldPrevYear(var Remaining, Used, Planned,
      Available: String);
    procedure ComputeSeniorityHld(var Normal, Used, Planned,
      Available: String);
    procedure ComputeAddBnkHoliday(var Remaining, Used, Planned,
      Available: String);
    procedure ComputeSatCredit(var Remaining, Earned, Used, Planned,
      Available: String);
    procedure ComputeRsvBnkHoliday(var Normal, Used, Planned,
      Available: String);
    procedure ComputeShorterWeek(var Remaining, Earned,
      Used, Planned, Available: String);
    function ComputeMaxSalaryBalanceMinutes: String;
    procedure ComputeTravelTime(var Remaining, Earned, Used, Planned,
      Available: String);
    function DetermineCounterValue(const Fieldname: String): String;
    function DetermineNormBankHldResMinManYN: String;
    procedure GetAllCountersActivated;
    function GetCounterActivated(AAbsenceType: String;
      var ACounterDescription: String): Boolean;
    function AbsenceTypePerCountryExist: Boolean;
    property EmployeeNumber: Integer read FEmployeeNumber write FEmployeeNumber;
    property Year: Integer read FYear write FYear;
    property DateFrom: TDateTime read FDateFrom write FDateFrom;
  end;

var
  GlobalDM: TGlobalDM;
  ABalanceCounters: TBalanceCounters;

implementation

{$R *.DFM}

uses
  CalculateTotalHoursDMT, ListProcsFRM,
  UGlobalFunctions, UPimsConst;

// RV045.1. For use in debug-statements.
function BoolToYN(AValue: Boolean): String;
begin
  if AValue then
    Result := 'Y'
  else
    Result := 'N';
end;

// MR: Pims->Oracle NOT USED?
// This function retrieve the total amount of minutes worked between
// StartTime and EndTime
procedure TGlobalDM.GetWorkedMin(var WorkedMin: Integer;
  EmployeeNumber: Integer; StartDate, EndDate: TDateTime);
begin
  qrySalMin.Close;
  qrySalMin.ParamByName('STARTDATE').AsDateTime:= StartDate;
  qrySalMin.ParamByName('ENDDATE').AsDateTime:= EndDate;
  qrySalMin.ParamByName('EMPNO').AsInteger := EmployeeNumber;
  qrySalMin.Open;
  WorkedMin := Round(qrySalMin.FieldByName('SUMSALARYMINUTE').AsFloat);
  qrySalMin.Close;
end;

// Overtime could be defined on
// 1. day
// 2. week
// 3. period in weeks.
// 4. month
// The StartDate/EndDate are necessary to add all employee worked minute from
// 1. SalaryHoursPerEmployee
// 2. AbsenceHoursPerEmployee
// StartDate & EndDate are included in interval !!
procedure TGlobalDM.ComputeOvertimePeriod(
  var StartDate, EndDate: TDateTime;
  EmployeeData: TScannedIDCard);
var
  OverTimeType, MyDayInWeek: Integer;
  PeriodStart, PeriodLength, StartWeek, PeriodNumber: Integer;
  WeekNumber, Year, Month, Day: Word;
  BookingDate: TDateTime;
begin
  BookingDate := StartDate;
  FindContractGroup(EmployeeData); // RV100.1.
  qryContractgroup.Close;
  // RV100.1. Search on Contractgroup instead by Employee.
  qryContractgroup.ParamByName('CONTRACTGROUP_CODE').AsString :=
    EmployeeData.ContractgroupCode;
//    qryContractgroup.ParamByName('EMPNO').AsInteger := EmployeeData.EmployeeCode;
  qryContractgroup.Open;
  if not qryContractgroup.IsEmpty then
  begin
    qryContractgroup.First;
    OverTimeType :=
      qryContractgroup.FieldByName('OVERTIME_PER_DAY_WEEK_PERIOD').AsInteger;
    Self.OvertimeType := TOvertime(OverTimeType); // GLOB3-81
    case TOvertime(OverTimeType) of
      OTDay :
        begin  // period = day
          StartDate := BookingDate;
          EndDate := BookingDate;
        end;
      OTWeek :
        begin  // period = week
          MyDayInWeek := DayInWeek(SystemDM.WeekStartsOn, BookingDate);
          StartDate := BookingDate - MyDayInWeek + 1;
          EndDate := BookingDate + 7 - MyDayInWeek;
      	end;
      OTPeriod :
        begin  // period = period
          PeriodStart :=
            qryContractgroup.FieldByName('PERIOD_STARTS_IN_WEEK').AsInteger;
          PeriodLength :=
            qryContractgroup.FieldByName('WEEKS_IN_PERIOD').AsInteger;
          if (PeriodStart <= 0) or (PeriodLength <= 0) then
          // incorrect values
          begin
            StartDate := BookingDate;
            Enddate := BookingDate;
          end
          else
          begin
            ListProcsF.WeekUitDat(BookingDate, Year, WeekNumber);
            PeriodNumber :=
              Trunc((WeekNumber - PeriodStart) / PeriodLength + 1);
            StartWeek := (PeriodNumber - 1) * PeriodLength + PeriodStart;
            StartDate := StartOfYear(Year);
            StartDate := StartDate + (StartWeek - 1) * 7;
            EndDate := StartDate + (PeriodLength * 7) - 1;
          end;
        end;
      OTMonth :
        begin // period = month
          // Determine month by bookdate and set start, end of month.
          DecodeDate(BookingDate, Year, Month, Day);
          StartDate := EncodeDate(Year, Month, 1);
          EndDate := EncodeDate(Year, Month,
            ListProcsF.LastMounthDay(Year, Month));
        end;
    end; // case
    // MR:17-09-2003 Use '0,0,0,0' istead of '0,0,0,1' for 'EncodeTime'
    // Otherwise comparisons can go wrong.
    ReplaceTime(StartDate, EncodeTime(0, 0, 0, 0));
    ReplaceTime(EndDate, EncodeTime(23, 59, 59, 99));
  end; // if
  qryContractgroup.Close;
end;

(*
// This function find record in ABSENCETOTAL
// if record found then add Minutes to EARNED_TFT_MINUTE
// if not insert a new record with EARNED_TFT_MINUTE = Minutes
// Returns
// -1 if any error
//  0 if update record
//  1 if insert new record
function TGlobalDM.UpdateAbsenceTotal(BookingDay: TDateTime;
  EmployeeData: TScannedIDCard; Minutes: double): integer;
var
  EarnedTime: double;
  Year, Month, Day: word;
begin
//  result := -1;
  EarnedTime := Minutes;
  DecodeDate(BookingDay, Year, Month, Day);
  qryAbsTotTFT.Close;
  qryAbsTotTFT.ParamByName('EMPNO').AsInteger :=
    EmployeeData.EmployeeCode;
  qryAbsTotTFT.ParamByName('AYEAR').AsInteger := Year;
  qryAbsTotTFT.Open;
  if not qryAbsTotTFT.IsEmpty then
  begin
    EarnedTime := EarnedTime +
      qryAbsTotTFT.FieldByName('EARNED_TFT_MINUTE').AsFloat;
    qryAbsTotTFTUpdate.Close;
    qryAbsTotTFTUpdate.ParamByName('EMPNO').AsInteger :=
      EmployeeData.EmployeeCode;
    qryAbsTotTFTUpdate.ParamByName('AYEAR').AsInteger := Year;
    qryAbsTotTFTUpdate.ParamByName('OTIME').AsFloat := EarnedTime;
    qryAbsTotTFTUpdate.ParamByName('MDATE').AsDateTime := Now;
    qryAbsTotTFTUpdate.ParamByName('MUTATOR').AsString :=
      SystemDM.CurrentProgramUser;
    qryAbsTotTFTUpdate.ExecSQL;
    qryAbsTotTFTUpdate.Close;
    Result := 0;
  end
  else
  begin
    qryAbsTotTFTInsert.Close;
    qryAbsTotTFTInsert.ParamByName('EMPNO').AsInteger :=
      EmployeeData.EmployeeCode;
    qryAbsTotTFTInsert.ParamByName('AYEAR').AsInteger := Year;
    qryAbsTotTFTInsert.ParamByName('OTIME').AsFloat := EarnedTime;
    qryAbsTotTFTInsert.ParamByName('CDATE').AsDateTime := Now;
    qryAbsTotTFTInsert.ParamByName('MDATE').AsDateTime := Now;
    qryAbsTotTFTInsert.ParamByName('MUTATOR').AsString :=
      SystemDM.CurrentProgramUser;
    qryAbsTotTFTInsert.ExecSQL;
    qryAbsTotTFTInsert.Close;
    Result := 1;
  end;
  qryAbsTotTFT.Close;
end;
*)

// RV025.
// Detect if the Exceptional Hour Definition for the employee has
// an hour-type defined for OVERTIME_HOUR_TYPE.
function TGlobalDM.DetectExceptHourDefDuringOvertimeHourtype(
  AEmployeeData: TScannedIDCard;
  AExceptRegularMin: Integer): boolean;
var
  StartTime, EndTime: TDateTime;
begin
  Result := False;

  StartTime := AEmployeeData.DateInCutOff;
  EndTime := StartTime + AExceptRegularMin/DayToMin;
  FindContractGroup(AEmployeeData); // RV100.1.
  with qryExceptHourDef do
  begin
    Close;
    // RV025. The query is now put in the component.
    // RV100.1 It now looks directly for contractgroup, not by employee.
//    ParamByName('EMPLOYEE_NUMBER').AsInteger := AEmployeeData.EmployeeCode;
    ParamByName('CONTRACTGROUP_CODE').AsString :=
      AEmployeeData.ContractgroupCode;
    ParamByName('START_TIME').AsDateTime := StartTime;
    ParamByName('END_TIME').AsDateTime := EndTime;
    ParamByName('DAY_OF_WEEK').AsInteger :=
      DayInWeek(SystemDM.WeekStartsOn, StartTime);
    ParamByName('WEEKDAYSTART').AsInteger :=
      DayInWeek(SystemDM.WeekStartsOn, StartTime);
    ParamByName('WEEKDAYEND').AsInteger :=
      DayInWeek(SystemDM.WeekStartsOn, EndTime);
    Open;
    if not Eof then
    begin
      First;
      if FieldByName('OVERTIME_HOURTYPE_NUMBER').AsString <> '' then
        Result := True;
    end;
    Close;
  end;
end;

// RV025.
// Detect if there are overtime/non-overtime minutes made.
// This means the point at which overtime is starting is detected.
function TGlobalDM.DetectOvertimeNonOvertime(ABookingDay: TDateTime;
  AWorkedMin: Integer; AEmployeeData: TScannedIDCard;
  AManual: String;
  VAR ANonOvertimeMin: Integer;
  VAR AOvertimeMin: Integer): boolean;
begin
  Result := DetectExceptHourDefDuringOvertimeHourtype(
    AEmployeeData, AWorkedMin);
  if Result then
  begin
    ANonOvertimeMin := 0;
    AOvertimeMin := 0;
    OvertimeDetect(ABookingDay, AWorkedMin, AEmployeeData,
      AManual, ANonOvertimeMin, AOvertimeMin);
    Result := (AOvertimeMin <> 0);
  end;
end;

// This function save records in SalaryHoursPerEmployee
// WorkedMin= OvertimeMin + ExceptionalMin + RegularMin.
// insert all overtime records (minutes and hourtype).
// result = the remaining minutes = ExceptionalMin + RegularMin.
// RV025. Added Detect-option, so it can be used to:
//  - Only detect if there were non-overtime and overtime-minutes,
//    no actual booking of minutes will take place.
//  - Detect overtime and also book the minutes.
function TGlobalDM.BooksOverTimeMain(BookingDay: TdateTime;
  WorkedMin: Integer; EmployeeData: TScannedIDcard;
  Manual: String;
  ACombinedHourSystem: Boolean;
  ADetect: Boolean;
  VAR ANonOvertimeMin: Integer;
  VAR AOvertimeMin: Integer;
  ADetectedOvertimeMin: Integer; // RV040.2.
  ADetectedNonOvertimeMin: Integer // RV040.2.
  ): Integer;
var
  StartMin,EndMin,HourType: Integer;
  EarnedMin, RegisteredMin, OverTimeMin, TotOverTime: Integer;
  StartDate, EndDate: TDateTime;
  EarnedTimeTFT: Double;
  BookedMin: Integer; // RV025.
  EarnedTimeSWW: Double; // RV071.6. SWW=ShorterWorkingWeek
  FromTime, ToTime: TDateTime; // GLOB3-81
  function DetermineStandardMinutesPerMonth: Integer;
  var
    WorkDays: array[1..7] of Boolean;
    NormalHoursPerDay: Integer; // Is stored in minutes.
    IDay, LastDayMonth: Integer;
    Year, Month, Day: Word;
  begin
    Result := 0;
    FindContractGroup(EmployeeData); // RV100.1.
    qryContractgroup.Close;
    // RV100.1. Search on Contractgroup instead by Employee.
    qryContractgroup.ParamByName('CONTRACTGROUP_CODE').AsString :=
      EmployeeData.ContractgroupCode;
//    qryContractgroup.ParamByName('EMPNO').AsInteger := EmployeeData.EmployeeCode;
    qryContractgroup.Open;
    if not qryContractgroup.IsEmpty then
    begin
      qryContractgroup.First;
      if TOvertime(qryContractgroup.
        FieldByName('OVERTIME_PER_DAY_WEEK_PERIOD').AsInteger) = OTMonth then
      begin
        // In the array 'workdays' the first day=sunday.
        WorkDays[1] :=
          qryContractgroup.FieldByName('WORKDAY_SU_YN').AsString = 'Y';
        WorkDays[2] :=
          qryContractgroup.FieldByName('WORKDAY_MO_YN').AsString = 'Y';
        WorkDays[3] :=
          qryContractgroup.FieldByName('WORKDAY_TU_YN').AsString = 'Y';
        WorkDays[4] :=
          qryContractgroup.FieldByName('WORKDAY_WE_YN').AsString = 'Y';
        WorkDays[5] :=
          qryContractgroup.FieldByName('WORKDAY_TH_YN').AsString = 'Y';
        WorkDays[6] :=
          qryContractgroup.FieldByName('WORKDAY_FR_YN').AsString = 'Y';
        WorkDays[7] :=
          qryContractgroup.FieldByName('WORKDAY_SA_YN').AsString = 'Y';
        NormalHoursPerDay :=
          qryContractgroup.FieldByName('NORMALHOURSPERDAY').AsInteger;
        DecodeDate(BookingDay, Year, Month, Day);
        LastDayMonth := ListProcsF.LastMounthDay(Year, Month);
        // Determine total number of normal working hours for this month
        for IDay := 1 to LastDayMonth do
        begin
           // DayOfWeek returns: 1=sun 2=mon 3=tue ...
          if WorkDays[DayOfWeek(EncodeDate(Year, Month, IDay))] then
            Result := Result + NormalHoursPerDay;
        end;
      end;
    end;
    qryContractgroup.Close;
  end; // DetermineStandardMinutesPerMonth
  // GLOB3-81
  function CheckFromToTime(AOvertimeMin: Integer;
    AFromTime, AToTime: TDateTime): Integer;
  var
    FromTime, MidnightTime, MorningTime, ToTime: TDateTime;
    OTDateIn, OTDateOut: TDateTime;
  begin
    Result := AOvertimeMin;
    if Self.OvertimeType = OTDay then
    begin
      if not ((AFromTime = 0) and (AToTime = 0)) then
      begin
        FromTime := Trunc(BookingDay) + Frac(AFromTime);
        ToTime := Trunc(BookingDay) + Frac(AToTime);
//        NewDateIn := EmployeeData.DateIn;
//        if EmployeeData.DateInFirstScan <> NullDate then
//          NewDateIn := EmployeeData.DateInFirstScan + (RegisteredMin * (1 / 60 / 24));
        // First check if DateOut is within the from-to-time to prevent it
        // calculates overtime when it was not after the overtime we found.

        // Determine period to check were overtime occured.
        OTDateOut := EmployeeData.DateOut;
        OTDateIn := EmployeeData.DateOut - (AOvertimeMin * (1/60/24));
//ShowMessage(DateTimeToStr(OTDateIn) + ' - ' + DateTimeToStr(OTDateOut));        
        if FromTime > ToTime then
        begin
          MidnightTime := Trunc(BookingDay + 1);
          MorningTime := Trunc(BookingDay);
          Result := ComputeMinutesOfIntersection(
            OTDateIn, OTDateOut,
            FromTime, MidnightTime) +
            ComputeMinutesOfIntersection(
            OTDateIn, OTDateOut,
            MorningTime, ToTime);
        end
        else
        begin
          // Compare here with scan-times.
          Result := ComputeMinutesOfIntersection(
            OTDateIn, OTDateOut,
            FromTime, ToTime);
        end;
        if Result > AOvertimeMin then
          Result := AOvertimeMin;
      end;
    end;
  end; // CheckFromToTime
begin
{$IFDEF DEBUG1}
begin
  if (ADetect) then
    WDebugLog('Start BooksOverTime - Detect')
  else
    WDebugLog('Start BooksOverTime');
  WDebugLog('- BookingDay=' + DateToStr(BookingDay) +
  ' WorkedMin=' + IntToStr(WorkedMin));
end;
{$ENDIF}
{$IFDEF DEBUG3}
begin
  if (ADetect) then
    WDebugLog('Start BooksOverTime - Detect')
  else
    if (ACombinedHourSystem) then
      WDebugLog('Start BooksOverTime - Combined')
    else
      WDebugLog('Start BooksOverTime');
  WDebugLog('- BookingDay=' + DateToStr(BookingDay) +
  ' WorkedMin=' + IntToStr(WorkedMin) +
  ' (' + IntMin2StringTime(WorkedMin, False) + ')');
end;
{$ENDIF}

  // RV103.1. Do this here (at begin).
  TotOverTime := 0;
  EarnedTimeTFT := 0;
  EarnedMin := 0; // RV040.2.
  EarnedTimeSWW := 0; // RV071.6.

  // RV080.6.
  // When this scan has workspot with an hourtype assigned, then
  // do not check on overtime.
  if EmployeeData.WSHourtypeNumber <> -1 then
  begin
{$IFDEF DEBUG}
  WDebugLog('- Workspot.hourtype found');
{$ENDIF}
    Result := WorkedMin;
    Exit;
  end;

  // TD-23386 If there is an hourtype defined on shift-level, then
  //          use that for booking hours.
  if EmployeeData.ShiftHourtypeNumber <> -1 then
  begin
{$IFDEF DEBUG}
  WDebugLog('- Shift.hourtype found');
{$ENDIF}
    Result := WorkedMin;
    Exit;
  end;

  // RV033.1.
  // When this scan has workspot with hourtype and 'ignore overtime' is YES,
  // then there is not need to check for overtime.
  if EmployeeData.IgnoreForOvertimeYN = 'Y' then
  begin
{$IFDEF DEBUG1}
  WDebugLog('- IgnoreOvertime=Y');
{$ENDIF}
    Result := WorkedMin;
    Exit;
  end;

  RegisteredMin := WorkedMin;

//  SalaryStored := False; // RV075.12. RV076.3. Not here!

//  TotalWorkedMin := WorkedMin;

  StartDate := BookingDay;
  EndDate := BookingDay;

  ComputeOvertimePeriod(StartDate, EndDate, EmployeeData);

{$IFDEF DEBUG1}
  WDebugLog('- OvertimePeriod. Start=' + DateToStr(StartDate) + ' End=' +
    DateToStr(EndDate));
{$ENDIF}

  StandardMinutesPerMonth := DetermineStandardMinutesPerMonth;

  // SalaryHourPerEmployee - Get total of SalaryMinutes +
  // AbsenceMinutes of all hourtypes.
  with qrySalaryAbsenceMin do
  begin
    Close;
    ParamByName('STARTDATE').AsDateTime := StartDate;
    // RV082.13. RV080.4. !!!CANCELLED!!!
    ParamByName('ENDDATE').AsDateTime := EndDate;
    // RV082.13. RV080.4. !!!CANCELLED!!!
{
    // RV080.4. Do not take all hours for the complete overtime-period!
    //          But only till the BookingDay!
    ParamByName('ENDDATE').AsDateTime := BookingDay; // EndDate;
}
    ParamByName('EMPNO').AsInteger := EmployeeData.EmployeeCode;
    Open;
    BookedMin := Round(FieldByName('SUMSALARYMINUTE').AsFloat);
    // RV040.2. These minutes must be also booked as overtime!
{$IFDEF DEBUG3}
  WDebugLog('- WorkedMin=' + IntMin2StringTime(WorkedMin, False) +
    ' BookedMin=' + IntMin2StringTime(BookedMin, False) +
    ' ADetOMin=' + IntMin2StringTime(ADetectedOvertimeMin, False) +
    ' ADetNOMin=' + IntMin2StringTime(ADetectedNonOvertimeMin, False)
    );
{$ENDIF}
    // RV040.2.
    if (WorkedMin < (ADetectedOvertimeMin + ADetectedNonOvertimeMin)) and
      (ADetectedOvertimeMin > 0) then
    begin
      BookedMin := Round(FieldByName('SUMSALARYMINUTE').AsFloat) -
        ADetectedOvertimeMin + WorkedMin;
{$IFDEF DEBUG3}
  WDebugLog('- New BookedMin=' + IntMin2StringTime(BookedMin, False)
    );
{$ENDIF}
    end;
    RegisteredMin := RegisteredMin +
      Round(FieldByName('SUMSALARYMINUTE').AsFloat);
{$IFDEF DEBUG3}
  WDebugLog('- Sum(SALARYMINUTE)=' +
    IntMin2StringTime(Round(FieldByName('SUMSALARYMINUTE').AsFloat), False) +
    ' RegisteredMin=' + IntMin2StringTime(RegisteredMin, False));
{$ENDIF}
    Close;
  end;

(* RV039.2. Do not use! Gives wrong result for overtime.
  // MRA:05-MAR-2008 Start Overtime-fix. RV004.
  // Determine from Salary-hours the overtime-minutes registered so far.
  if (not ACombinedHourSystem) then {RV025}
  begin
    with qrySalaryOvertimeMin do
    begin
      Close;
      ParamByName('STARTDATE').AsDateTime := StartDate;
      ParamByName('ENDDATE').AsDateTime := EndDate;
      ParamByName('EMPNO').AsInteger := EmployeeData.EmployeeCode;
      Open;
      SalaryOvertimeMin := Round(FieldByName('SUMSALARYOVERTIMEMIN').AsFloat);
      Close;
    end;
    // RV039.2. Do not subtract SalaryOvertimeMin.
    RegisteredMin := RegisteredMin - SalaryOvertimeMin;
  end;
*)

  FindContractGroup(EmployeeData); // RV100.1.
  // RV071.6. Added HT.ADV_YN to query.
  qryOvertimeDef.Close;
  // RV100.1. Search on contractgroup, instead by employee
  qryOvertimeDef.ParamByName('CONTRACTGROUP_CODE').AsString :=
    EmployeeData.ContractgroupCode;
  // GLOB3-81 GLOB3-223
  if SystemDM.OvertimePerDay and (OvertimeType = OTDay) then
    qryOvertimeDef.ParamByName('DAY_OF_WEEK').AsInteger :=
      DayInWeek(SystemDM.WeekStartsOn, BookingDay)
  else
    qryOvertimeDef.ParamByName('DAY_OF_WEEK').AsInteger := 0;
  qryOvertimeDef.Open;
  while not qryOvertimeDef.Eof do
  begin
    // Get Overtime Definition
    StartMin := qryOvertimeDef.FieldByName('STARTTIME').AsInteger +
      StandardMinutesPerMonth;
    EndMin := qryOvertimeDef.FieldByName('ENDTIME').AsInteger +
      StandardMinutesPerMonth;
    HourType := qryOvertimeDef.FieldByName('HOURTYPE_NUMBER').AsInteger;
    // GLOB3-81 GLOB3-223
    if SystemDM.OvertimePerDay and (OvertimeType = OTDay) then
    begin
      FromTime := qryOvertimeDef.FieldByName('FROMTIME').AsDateTime;
      ToTime := qryOvertimeDef.FieldByName('TOTIME').AsDateTime;
    end
    else
    begin
      FromTime := 0;
      ToTime := 0;
    end;

    // RV071.6.
    // RV103.1. Not needed here.
{    ShorterWorkingWeekYN := qryOvertimeDef.FieldByName('HT_ADV_YN').AsString; }

    OverTimeMin := 0;


    // RV025.
    // If the already booked minutes are higher than the lower boundary
    // of the Overtime-definition, then make the lower boundar equal to this.
    // To prevent too much overtime-hours are calculated.
    // Example:
    //   BookedMin is 41:30
    //   StartMin is 40:00
    //   Then StartMin should also become 41:30
    // RV039.2. Do this ALWAYS! Not only for 'ACombinedHourSystem' !
//    if (ACombinedHourSystem) then
      if (BookedMin > StartMin) then
        StartMin := BookedMin;

    if ((RegisteredMin >= StartMin) and (Min(RegisteredMin, EndMin) <= EndMin)) then
      OverTimeMin := Min(RegisteredMin, EndMin) - StartMin;

    if (OverTimeMin < 0) then
      OverTimeMin := 0;
{$IFDEF DEBUG3}
  WDebugLog('- Try: HT=' + IntToStr(HourType) +
    ' RegisteredMin=' + IntMin2StringTime(RegisteredMin, False) +
    ' SMin=' + IntMin2StringTime(StartMin, False) +
    ' EMin=' + IntMin2StringTime(EndMin, False) +
    ' From=' + TimeToStr(FromTime) +
    ' To=' + TimeToStr(ToTime) +
    ' OMin=' + IntMin2StringTime(OverTimeMin, False)
    );
{$ENDIF}
    // MRA:06-MAR-2008 RV004. Overtime-fix.
    // Also check on 'overtimemin' in this if.
    if (StartMin >= RegisteredMin) and (OvertimeMin <= 0) then
    begin
      qryOvertimeDef.Last;
    end
    else
    begin
      // GLOB3-81 First optionally check this here. GLOB3-223
      if SystemDM.OvertimePerDay and (OvertimeType = OTDay) then
        if (OverTimeMin > 0) then
          OverTimeMin := CheckFromToTime(OvertimeMin, FromTime, ToTime);

      TotOverTime := TotOverTime + OverTimeMin;

      // There are overtime minutes made!
      if (OverTimeMin > 0) then
      begin

{$IFDEF DEBUG3}
begin
  WDebugLog('- HT=' + IntToStr(HourType) +
    ' SMin=' + IntMin2StringTime(StartMin, False) +
    ' EMin=' + IntMin2StringTime(EndMin, False) +
    ' From=' + TimeToStr(FromTime) +
    ' To=' + TimeToStr(ToTime) +
    ' SalMin=' + IntMin2StringTime(RegisteredMin - WorkedMin, False) +
    ' RegMin=' + IntMin2StringTime(RegisteredMin, False) +
    ' OTMin=' + IntToStr(OverTimeMin) + ' (' + IntMin2StringTime(OverTimeMin, False) + ')');
end;
{$ENDIF}

        if not (ACombinedHourSystem and ADetect) then
        begin
          EarnedMin := UpdateSalaryHour(BookingDay,
            EmployeeData, HourType, OverTimeMin, Manual, False,
            True
            );
//          SalaryStored := True; // RV075.12. RV076.3. Not Here!
{$IFDEF DEBUG}
  WDebugLog('- UpdateSalaryHour()' +
    ' BookDay=' + DateToStr(BookingDay) +
    ' BookMin=' + IntMin2StringTime(OverTimeMin, False));
{$ENDIF}
        end;
      end
      else
        EarnedMin := 0;

      // Time For Time;
      // RV040.2. Skip when detecting!
      if not (ACombinedHourSystem and ADetect) then
      begin
        // RV071.6.
        EarnedTimeTFT := DetermineTimeForTime(qryOvertimeDef,
          EarnedTimeTFT, EarnedMin);
        // RV071.6.
        EarnedTimeSWW := DetermineShorterWorkingWeek(qryOvertimeDef,
          EarnedTimeSWW, EarnedMin);

        // RV071.6. Replaced by a local function:
(*
        DetermineTimeForTimeSettings; // RV045.1.
//        if qryOvertimeDef.FieldByName('TIME_FOR_TIME_YN').AsString=CHECKEDVALUE then
        if TimeForTimeYN = CHECKEDVALUE then
        begin
          Percentage := qryOvertimeDef.FieldByName('BONUS_PERCENTAGE').AsFloat;
//          if qryOvertimeDef.FieldByName('BONUS_IN_MONEY_YN').AsString = UNCHECKEDVALUE then
          if BonusInMoneyYN = UNCHECKEDVALUE then
            EarnedTime := EarnedTime +
              Round(EarnedMin + EarnedMin * Percentage / 100)
          else
            EarnedTime := EarnedTime + EarnedMin;
        end;
*)
      end;

    end;
    // GLOB3-81 Desrease with already calculated overtime
    
    // PIM-406 Do not do this to prevent it miscalculates the overtime.
{
    if SystemDM.IsNTS and (OvertimeType = OTDay) then
      if (OverTimeMin > 0) then
        RegisteredMin := RegisteredMin - OvertimeMin;
}        
    qryOvertimeDef.Next;
  end;
  qryOvertimeDef.Close;

  // RV025. Only detect if there were overtime minutes made,
  // do not store the hours!
  // RV040.2. Moved this part, ALL overtime must be detected.
  if (ACombinedHourSystem and ADetect) then
  begin
//      qryOvertimeDef.Close;
    AOvertimeMin := TotOverTime;
    ANonOvertimeMin := WorkedMin - TotOverTime;
    if (ANonOvertimeMin < 0) then
      ANonOvertimeMin := 0;
    Result := 0;
{$IFDEF DEBUG3}
  if (ANonOvertimeMin <> 0) or (AOvertimeMin <> 0) then
    WDebugLog('- Detected hours:' +
      ' NonOvertimeMin=' + IntToStr(ANonOvertimeMin) +
      ' (' + IntMin2StringTime(ANonOvertimeMin, False) + ')'+
      ' OvertimeMin=' + IntToStr(AOvertimeMin) +
      ' (' + IntMin2StringTime(AOvertimeMin, False) + ')'
      );
{$ENDIF}
    Exit;
  end;

  // Booking Time for Time
  BooksTFT(BookingDay, EmployeeData, EarnedTimeTFT);
  // Booking Shorter Working Week (SWW)
  BooksSWW(BookingDay, EmployeeData, EarnedTimeSWW);

  Result := WorkedMin - TotOverTime;
  // MRA:05-MAR-2008 End Overtime-fix. RV004.
{$IFDEF DEBUG}
begin
  WDebugLog('- Result=' + IntToStr(Result));
  WDebugLog('End BooksOverTime');
end;
{$ENDIF}

end; // BooksOverTimeMain

// RV025. Book the Overtime minutes. No combined hours (except+overtime).
function TGlobalDM.BooksOverTime(ABookingDay: TdateTime;
  AWorkedMin: Integer; AEmployeeData: TScannedIDcard;
  AManual: String;
  ADetectedOvertimeMin: Integer; // RV040.2.
  ADetectedNonOvertimeMin: Integer // RV040.2.
  ): Integer;
var
  NonOvertimeMinDummy, OvertimeMinDummy: Integer;
begin
  NonOvertimeMinDummy := 0;
  OvertimeMinDummy := 0;
  Result := BooksOvertimeMain(
    ABookingDay, AWorkedMin, AEmployeeData,
    AManual,
    False, // ACombinedHourSystem
    False, // Detect: Detection no
    NonOvertimeMinDummy, OvertimeMinDummy,
    ADetectedOvertimeMin, // RV040.2.
    ADetectedNonOvertimeMin // RV040.2.
  );
end;

// RV025. Book the Overtime minutes, for combined hours (exceptional+overtime).
function TGlobalDM.BooksOverTimeCombinedHours(ABookingDay: TdateTime;
  AWorkedMin: Integer; AEmployeeData: TScannedIDcard;
  AManual: String
  ): Integer;
var
  NonOvertimeMinDummy, OvertimeMinDummy: Integer;
begin
  NonOvertimeMinDummy := 0;
  OvertimeMinDummy := 0;
  Result := BooksOvertimeMain(
    ABookingDay, AWorkedMin, AEmployeeData,
    AManual,
    True, // CombinedHourSystem
    False, // Detect: Detection no
    NonOvertimeMinDummy, OvertimeMinDummy,
    0,
    0);
end;

// RV025. Detect the Overtime minutes/Non Overtime minutes, do not book them.
function TGlobalDM.OverTimeDetect(ABookingDay: TdateTime;
  AWorkedMin: Integer; AEmployeeData: TScannedIDcard;
  AManual: String;
  VAR ANonOvertimeMin: Integer;
  VAR AOvertimeMin: Integer
  ): Integer;
begin
  Result := BooksOvertimeMain(
    ABookingDay, AWorkedMin, AEmployeeData,
    AManual,
    True, // CombinedHourSystem
    True, // Detect: Detection yes
    ANonOvertimeMin,
    AOvertimeMin,
    0,
    0);
end;

// This function save records in SalaryHoursPerEmployee with
// ExceptRegularMin contain the sum of exceptional minute and regular minute
// 1. Exceptional hour (min)
// 2. Regular hours (min)
// result = regular minutes
// RV025.
//   Added option to book exceptional hours during overtime.
//   This must be booked using a different hourtype that is defined
//   in EXCEPTIONALHOURDEF, field OVERTIME_HOUR_TYPE.
// RV082.15. Added Ignore For Overtime for debugging.
function TGlobalDM.BooksExceptionalTimeMain(
  (* AQuery: TQuery; RV023. *)
  BookingDay: TDateTime;
  ExceptRegularMin: Integer; EmployeeData: TScannedIDcard;
  Manual: String;
  ADuringOvertime: Boolean
  ): Integer;
var
//  InsertQuery: TQuery; // RV023. Not used!
  StartTime, EndTime, STime, ETime: TDateTime;
  HourType, ProdMin, BreaksMin, PayedBreaks, IntersectionMin: Integer;
//  TempEndTime: TDateTime; // RV060.1.
//  PreviousBreaksTotal, BreaksTotal: Integer; // RV060.1.
  DayOfWeek: Integer;
  IgnoreForOvertimeYN: String; // RV082.15.
  // RV080.7. Disable this functionality!
{ SalaryStored: Boolean; // RV075.12.
  LostMinutes: Integer; // RV076.4. }
  // MRA:16-JAN-2009 RV020. Scans that are overnight-scans must be
  //                        split in 2 !
  //                        If not, then it will not always find the
  //                        correct breaks !
  // RV060.1. Orginal procedure.
(*
  procedure DetermineBreaksV1(AEndTime: TDateTime;
    var ABreaksMin, APayedBreaks: Integer);
  begin
    TempEndTime := AEndTime;
    PreviousBreaksTotal := 0;
    while (True) do
    begin
      // MR: 25-09-2003 - See: CalculateTotalHoursDMT
      AProdMinClass.ComputeBreaks(EmployeeData, EmployeeData.DateIn, TempEndTime,
        1, ProdMin, ABreaksMin, APayedBreaks);
      BreaksTotal := (ABreaksmin - APayedBreaks);
      if (BreaksTotal = PreviousBreaksTotal) then
        Break;
      TempEndTime := TempEndTime + (1 / 60 / 24); // add 1 minute
      PreviousBreaksTotal := BreaksTotal;
    end;
  end;
*)
  // RV060.1. New procedure
  procedure DetermineBreaks(AEndTime: TDateTime;
    var ABreaksMin, APayedBreaks: Integer);
  var
    BreaksMinTotal, PayedBreaksTotal: Integer;
    DateIn, DateOut: TDateTime;
  begin
    DateIn := EmployeeData.DateIn;
    DateOut := AEndTime;
    BreaksMinTotal := 0;
    PayedBreaksTotal := 0;
    repeat
      // Determine breaks for this scan.
      AProdMinClass.ComputeBreaks(EmployeeData, DateIn, DateOut,
        1, ProdMin, ABreaksMin, APayedBreaks);
      BreaksMinTotal := BreaksMinTotal + ABreaksMin;
      PayedBreaksTotal := PayedBreaksTotal + APayedBreaks;
      // Add found breaks to end-time of scan.
      // Only check for breaksmin here!
      if (ABreaksMin > 0) then
      begin
        DateIn := DateOut;
        DateOut := DateOut + (ABreaksMin / 60 / 24);
      end;
    until (ABreaksMin <= 0);
    ABreaksMin := BreaksMinTotal;
    APayedBreaks := PayedBreaksTotal;
  end;
  procedure DetermineBreaksOvernightScan(AEndTime: TDateTime;
    var ABreaksMin, APayedBreaks: Integer);
  var
    SaveDateIn, SaveDateOut, SaveEndTime: TDateTime;
    MyBreaksMinTotal, MyPayedBreaksTotal: Integer;
    OvernightScan: Boolean;
  begin
    OvernightScan := Trunc(StartTime) <> Trunc(AEndTime);
    MyBreaksMinTotal := 0;
    MyPayedBreaksTotal := 0;
    SaveDateIn := EmployeeData.DateIn;
    SaveDateOut := EmployeeData.DateOut;
    SaveEndTime := AEndTime;
    if OvernightScan then
    begin
      EmployeeData.DateOut := Trunc(EmployeeData.DateOut);
      AEndTime := EmployeeData.DateOut;
      DetermineBreaks(AEndTime, ABreaksMin, APayedBreaks);
      MyBreaksMinTotal := ABreaksMin;
      MyPayedBreaksTotal := APayedBreaks;
      EmployeeData.DateIn := EmployeeData.DateOut;
      EmployeeData.DateOut := SaveDateOut;
      AEndTime := SaveEndTime;
    end;
    DetermineBreaks(AEndTime, ABreaksMin, APayedBreaks);
    MyBreaksMinTotal := MyBreaksMinTotal + ABreaksMin;
    MyPayedBreaksTotal := MyPayedBreaksTotal + APayedBreaks;
    if OvernightScan then
    begin
      EmployeeData.DateIn := SaveDateIn;
      EmployeeData.DateOut := SaveDateOut;
    end;
    ABreaksMin := MyBreaksMinTotal;
    APayedBreaks := MyPayedBreaksTotal;
  end;
begin
  // RV080.7. Disable this functionality!
{  SalaryStored := False; }
  // MR:03-12-2003 Use 'cutoff'-datein-time
  StartTime := EmployeeData.DateInCutOff;
//  StartTime := EmployeeData.DateIn;
  EndTime := StartTime + ExceptRegularMin/DayToMin;

  // RV080.7. Disable this functionality!
{  LostMinutes := 0; // RV076.4. }

{$IFDEF DEBUG1}
begin
  WDebugLog('Start BooksExceptionalTime');
  WDebugLog('- ExceptRegularMin=' + IntToStr(ExceptRegularMin));
  WDebugLog('- StartTime=' + DateTimeToStr(StartTime) +
    ' EndTime=' + DateTimeToStr(EndTime));
end;
{$ENDIF}

  // RV080.6.
  // When this scan has workspot with an hourtype assigned, then
  // do not check on overtime.
  if EmployeeData.WSHourtypeNumber <> -1 then
  begin
{$IFDEF DEBUG}
  WDebugLog('- Workspot.hourtype found');
{$ENDIF}
    Result := ExceptRegularMin;
    Exit;
  end;

  // TD-23386
  if EmployeeData.ShiftHourtypeNumber <> -1 then
  begin
{$IFDEF DEBUG}
  WDebugLog('- Shift.hourtype found');
{$ENDIF}
    Result := ExceptRegularMin;
    Exit;
  end;

  // RV039.3. When hour-type is setup as 'ignore overtime' also ignore
  //          exceptional hours.
  // When this scan has workspot with hourtype and 'ignore overtime' is YES,
  // then there is not need to check for overtime (and for except. hours).
  if EmployeeData.IgnoreForOvertimeYN = 'Y' then
  begin
{$IFDEF DEBUG1}
  WDebugLog('- IgnoreOvertime=Y');
{$ENDIF}
    Result := ExceptRegularMin;
    Exit;
  end;

{$IFDEF DEBUG3}
begin
  if (ADuringOvertime) then
    WDebugLog('Start BooksExceptionalTime - during overtime')
  else
    WDebugLog('Start BooksExceptionalTime');
  WDebugLog('- ExceptRegularMin=' + IntToStr(ExceptRegularMin) +
    ' (' + IntMin2StringTime(ExceptRegularMin, False) + ')' +
    ' StartTime=' + DateTimeToStr(StartTime) +
    ' EndTime=' + DateTimeToStr(EndTime)
    );
end;
{$ENDIF}

  // MRA:16-JAN-2009 RV020.
  //                 Calculation of breaks.
  //                 This gives wrong results when a scan is an overnight-scan.
  //                 The scan must be split in two here to get the correct
  //                 result!

  // MR:16-06-2003 Start
  // Calculate the complete not-payed-break in case the
  // break is partly within the StartTime and EndTime.
  DetermineBreaksOvernightScan(EndTime, BreaksMin, PayedBreaks);

  EndTime := EndTime + (Breaksmin - PayedBreaks)/DayToMin; //Unpaid Breaks.
  // MR:15-09-2004 RoundTime is a function, so 'endtime' will never
  // be rounded + also second argument should be 1 to round to minutes.
//  Roundtime(EndTime,2); // only minute are importants.

{$IFDEF DEBUG3}
  WDebugLog('- After DetermineBreaks: BreaksMin=' + IntToStr(BreaksMin) +
    ' PayedBreaks=' + IntToStr(PayedBreaks) +
    ' New EndTime=' + DateTimeToStr(EndTime)
    );
{$ENDIF}

{$IFDEF DEBUG1}
  WDebugLog('- EndTime=' + DateTimeToStr(EndTime));
{$ENDIF}

  Result := ExceptRegularMin;

//  InsertQuery := TQuery.Create(Nil);
//  QueryCopy(AQuery, InsertQuery);

  // MRA:05-MAR-2009 RV023. Do this like in D7-version!
  // RV082.15. query changed to show Ignore Overtime + order by added.
  FindContractGroup(EmployeeData); // RV100.1.
  with qryExceptHourDef do
  begin
    Close;
    // RV025. The query is now put in the component.
    // RV100.1 It now looks directly for contractgroup, not by employee.
//    ParamByName('EMPLOYEE_NUMBER').AsInteger := EmployeeData.EmployeeCode;
    ParamByName('CONTRACTGROUP_CODE').AsString :=
      EmployeeData.ContractgroupCode;
    ParamByName('START_TIME').AsDateTime := StartTime;
    ParamByName('END_TIME').AsDateTime := EndTime;
    ParamByName('DAY_OF_WEEK').AsInteger :=
      DayInWeek(SystemDM.WeekStartsOn, StartTime);
    ParamByName('WEEKDAYSTART').AsInteger :=
      DayInWeek(SystemDM.WeekStartsOn, StartTime);
    ParamByName('WEEKDAYEND').AsInteger :=
      DayInWeek(SystemDM.WeekStartsOn, EndTime);
    Open;
    First;
    while not Eof do
    begin
      DayOfWeek := FieldByName('DAY_OF_WEEK').AsInteger;
      // RV025. Use other hourtype for exceptional hours made during overtime.
//      HourType := FieldByName('HOURTYPE_NUMBER').AsInteger;
      if (ADuringOvertime) then
      begin
        if FieldByName('OVERTIME_HOURTYPE_NUMBER').AsString <> '' then
        begin
          Hourtype := FieldByName('OVERTIME_HOURTYPE_NUMBER').AsInteger;
          IgnoreForOvertimeYN :=
            FieldByName('OHT_IGNORE_FOR_OVERTIME_YN').AsString; // RV082.15.
        end
        else
        begin
          HourType := FieldByName('HOURTYPE_NUMBER').AsInteger;
          IgnoreForOvertimeYN :=
            FieldByName('HT_IGNORE_FOR_OVERTIME_YN').AsString; // RV082.15.
        end;
      end
      else
      begin
        HourType := FieldByName('HOURTYPE_NUMBER').AsInteger;
        IgnoreForOvertimeYN :=
          FieldByName('HT_IGNORE_FOR_OVERTIME_YN').AsString; // RV082.15.
      end;
      STime := FieldByName('STARTTIME').AsDateTime;
      ETime := FieldByName('ENDTIME').AsDateTime;

{$IFDEF DEBUG3}
  WDebugLog('- EDef HT=' + IntToStr(Hourtype) +
    ' IgnoreOT=[' + IgnoreForOvertimeYN + ']' + // RV082.15.
    ' Day=' + IntToStr(DayOfWeek) +
    ' STime=' + DateTimeToStr(STime) +
    ' ETime=' + DateTimeToStr(ETime)
    );
{$ENDIF}

      // MR:05-11-2003
      // Because a scan can go from one day to another, here
      // we check which day we must use for comparing.
      if DayOfWeek = DayInWeek(SystemDM.WeekStartsOn, StartTime) then
      begin
        ReplaceDate(STime, StartTime);
        ReplaceDate(ETime, StartTime);
      end
      else
      begin
        // PIM-408 We need to know the date related to DayOfWeek
        //         Try next day, related to StartTime
        STime := Trunc(StartTime+1) + Frac(STime);
        ETime := Trunc(StartTime+1) + Frac(ETime);
        // PIM-408 This can give a wrong result!
//        ReplaceDate(STime, EndTime);
//        ReplaceDate(ETime, EndTime);
      end;
      // MR:07-11-2003 Correct midnight-enddate
      ETime := MidnightCorrection(STime, ETime);

{$IFDEF DEBUG3}
  WDebugLog('- Scan STime=' + DateTimeToStr(StartTime) +
    ' ETime=' + DateTimeToStr(EndTime) +
    ' - EDef STime=' + DateTimeToStr(STime) +
    ' ETime=' + DateTimeToStr(ETime)
    );
{$ENDIF}

      IntersectionMin := ComputeMinutesOfIntersection(StartTime, EndTime,
        STime, ETime);
      if IntersectionMin > 0 then
      begin

{$IFDEF DEBUG3}
  WDebugLog('- EDef Int.Min=' + IntToStr(IntersectionMin) +
    ' (' + IntMin2StringTime(IntersectionMin, False) + ')'
    );
{$ENDIF}

        // MR:13-06-2003 Start
        if STime < StartTime then
          STime := StartTime;
        if ETime > EndTime then
          ETime := EndTime;
        if STime > ETime then
          STime := ETime;

{$IFDEF DEBUG3}
  WDebugLog('- EDef STime=' + DateTimeToStr(STime) +
    ' ETime=' + DateTimeToStr(ETime)
    );
{$ENDIF}

        // MR:13-06-2003 End
        // Compute the UnPayed breaks min in interval
        // Because the unpayed breaks were added before, before the
        // except. hours were determined.
        // MR: 25-09-2003
        AProdMinClass.ComputeBreaks(Employeedata, STime, ETime, 0, ProdMin,
          BreaksMin, PayedBreaks);
        IntersectionMin := IntersectionMin - BreaksMin + PayedBreaks;

{$IFDEF DEBUG1}
  WDebugLog('- Hourtype=' + IntToStr(Hourtype) +
    ' STime=' + DateTimeToStr(STime) +
    ' ETime=' + DateTimeToStr(ETime) +
    ' BreaksMin=' + IntToStr(BreaksMin) +
    ' PayedBreaks=' + IntToStr(PayedBreaks) +
    ' Exc.Min=' + IntToStr(IntersectionMin));
{$ENDIF}

        if IntersectionMin > 0 then
        begin

{$IFDEF DEBUG1}
  WDebugLog('- HT=' + IntToStr(Hourtype) +
    ' STime=' + DateTimeToStr(STime) +
    ' ETime=' + DateTimeToStr(ETime) +
    ' EHMin=' + IntToStr(IntersectionMin));
  WDebugLog('- UpdateSalaryHour()' +
    ' BookDay=' + DateToStr(BookingDay) +
    ' BookMin=' + IntMin2StringTime(IntersectionMin, False));
{$ENDIF}
{$IFDEF DEBUG}
  WDebugLog('- HT=' + IntToStr(Hourtype) +
    ' STime=' + DateTimeToStr(STime) +
    ' ETime=' + DateTimeToStr(ETime) +
    ' BreaksMin=' + IntToStr(BreaksMin) +
    ' PayedBreaks=' + IntToStr(PayedBreaks) +
    ' EHMin=' + IntToStr(IntersectionMin) +
    ' (' + IntMin2StringTime(IntersectionMin, False) + ')');
  WDebugLog('- UpdateSalaryHour()' +
    ' BookDay=' + DateToStr(BookingDay) +
    ' BookMin=' + IntMin2StringTime(IntersectionMin, False));
{$ENDIF}

          UpdateSalaryHour(BookingDay,EmployeeData,
            HourType, IntersectionMin, Manual, False,
            True);

          // RV076.4. Round only for this hourtype and keep lost minutes during
          //          rounding.
          //RV080.7. Disable this functionality!
{
          SalaryStored := True;
          LostMinutes := LostMinutes +
            ContractRoundMinutes(EmployeeData, BookingDay, HourType);
}

          // MR:12-06-2003 Included in the 'if'.
          //               To prevent miscalculation if 'IntersectionMin' is
          //               negative.
          Result := Result - IntersectionMin;
        end;
      end; // if IntersectionMin > 0 then
      Next;
    end; // while not Eof do
    Close;
  end; // with qryExceptHourDef do

  // RV080.7. Disable this functionality!
{
  // RV075.12. Return any lost minutes.
  if SalaryStored then
  begin
    Result := Result + (-1 * LostMinutes);
  end;
}
//  InsertQuery.Active := False;
//  InsertQuery.Free;
end; // BooksExceptionalTimeMain

// RV025. Book the exceptional hours (not made during overtime)
function TGlobalDM.BooksExceptionalTime(
  ABookingDay: TDateTime;
  AExceptRegularMin: Integer; AEmployeeData: TScannedIDcard;
  AManual: String
  ): Integer;
begin
  Result := BooksExceptionalTimeMain(
    ABookingDay, AExceptRegularMin, AEmployeeData,
    AManual,
    False // ADuringOvertime
    );
end;

// RV025. Book the exceptional hours (made during overtime)
// This must be booked using a different hourtype that is defined
// in EXCEPTIONALHOURDEF, field OVERTIME_HOUR_TYPE.
function TGlobalDM.BooksExceptionalTimeDuringOvertime(
  ABookingDay: TDateTime;
  AExceptRegularMin: Integer; AEmployeeData: TScannedIDcard;
  AManual: String
  ): Integer;
begin
  Result := BooksExceptionalTimeMain(
    ABookingDay, AExceptRegularMin, AEmployeeData,
    AManual,
    True // ADuringOvertime
    );
end;

// This function find record in SALARYHOURSPEREMPLOYEE
// if record found then add Minutes to SALARY_MINUTE
// if not, insert a new record with SALARY_MINUTE = Minutes
// this function return:
// the number of minute added to existing record
// result = Minutes - Existing Minutes in database
function TGlobalDM.UpdateSalaryHour(SalaryDate: TDateTime;
  EmployeeData: TScannedIDCard;
  HourTypeNumber, Minutes: Integer; Manual: string;
  Replace: Boolean;
  UpdateYes: Boolean (* RV023 *);
  FromManualProd: String = 'N';
  ARoundMinutes: Boolean = True): Integer;
var
  TotMin: integer;
  Employee_Number: Integer;
  InsertNewProdHrs: Boolean;
  MinuteNewProdHrs: Integer;
begin
  Result := Minutes;
  Employee_Number := EmployeeData.EmployeeCode;
  if Replace and (Minutes = 0) then
    Exit;
  InsertNewProdHrs := False;
  MinuteNewProdHrs := 0;

  qrySalary.Close;
  qrySalary.ParamByName('SALARY_DATE').AsDateTime := SalaryDate;
  qrySalary.ParamByName('EMPLOYEE_NUMBER').AsInteger := Employee_Number;
  qrySalary.ParamByName('HOURTYPE_NUMBER').AsInteger := HourTypenumber;
  qrySalary.ParamByName('MANUAL_YN').AsString := Manual;
  qrySalary.Open;
  if not qrySalary.IsEmpty then // Update existing record
  begin
    TotMin := qrySalary.FieldByName('SALARY_MINUTE').AsInteger;
    if (TotMin + Minutes) <> 0 then
    begin
      qrySalaryUpdate.Close;
      if Replace then
      begin
        qrySalaryUpdate.ParamByName('SALARY_MINUTE').AsInteger := Minutes;
        Result := Minutes - TotMin;
      end
      else
        qrySalaryUpdate.ParamByName('SALARY_MINUTE').AsInteger :=
          TotMin + Minutes;
      qrySalaryUpdate.ParamByName('MDATE').AsDateTime := Now;
      qrySalaryUpdate.ParamByName('MUTATOR').AsString :=
        SystemDM.CurrentProgramUser;
      qrySalaryUpdate.ParamByName('SALARY_DATE').AsDateTime := SalaryDate;
      qrySalaryUpdate.ParamByName('EMPLOYEE_NUMBER').AsInteger :=
        Employee_Number;
      qrySalaryUpdate.ParamByName('HOURTYPE_NUMBER').AsInteger :=
        HourTypeNumber;
      qrySalaryUpdate.ParamByName('MANUAL_YN').AsString := Manual;
  //REV065.9
      qrySalaryUpdate.ParamByName('FROMMANUALPROD_YN').AsString := FromManualProd;
      qrySalaryUpdate.ExecSQL;
      qrySalaryUpdate.Close;
      InsertNewProdHrs := True;
      MinuteNewProdHrs := Minutes;
    end
    else
    begin
      qrySalaryDelete.Close;
      qrySalaryDelete.ParamByName('SALARY_DATE').AsDateTime := SalaryDate;
      qrySalaryDelete.ParamByName('EMPLOYEE_NUMBER').AsInteger :=
        Employee_Number;
      qrySalaryDelete.ParamByName('HOURTYPE_NUMBER').AsInteger :=
        HourTypeNumber;
      qrySalaryDelete.ParamByName('MANUAL_YN').AsString := Manual;
      qrySalaryDelete.ExecSQL;
      qrySalaryDelete.Close;
      // RV081.2. When first negative minutes are booked, and after that
      //          positive minutes (like in Hours-Per-Employee), then
      //          it must do the same for 'FillProdHoursPerType'!
      //          Otherwise you can get a difference.
      // Old:
{      InsertNewProdHrs := False;
      MinuteNewProdHrs := 0; }
      // New:
      InsertNewProdHrs := True;
      MinuteNewProdHrs := Minutes;
{$IFDEF DEBUG}
begin
  WDebugLog('> Delete salary');
  WDebugLog('> Salarydate=' + DateToStr(SalaryDate) + ' Empl=' +
    IntToStr(Employee_Number) + ' Hourtype=' + IntToStr(HourTypeNumber));
end;
{$ENDIF}
    end;
  end
  else // Insert new record in SalaryHourerEmployee
  begin
    if Minutes <> 0 then
    begin
      qrySalaryInsert.Close;
      qrySalaryInsert.ParamByName('SALARY_DATE').AsDateTime := SalaryDate;
      qrySalaryInsert.ParamByName('EMPLOYEE_NUMBER').AsInteger :=
        Employee_Number;
      qrySalaryInsert.ParamByName('HOURTYPE_NUMBER').AsInteger :=
        HourTypeNumber;
      qrySalaryInsert.ParamByName('SALARY_MINUTE').AsInteger := Minutes;
      qrySalaryInsert.ParamByName('MANUAL_YN').AsString := Manual;
      qrySalaryInsert.ParamByName('CREATIONDATE').AsDateTime := Now;
      qrySalaryInsert.ParamByName('MUTATIONDATE').AsDateTime := Now;
      qrySalaryInsert.ParamByName('EXPORTED_YN').AsString := UNCHECKEDVALUE;
      qrySalaryInsert.ParamByName('MUTATOR').AsString :=
        SystemDM.CurrentProgramUser;
      //REV065.9
      qrySalaryInsert.ParamByName('FROMMANUALPROD_YN').AsString := FromManualProd;
      qrySalaryInsert.ExecSql;
      qrySalaryInsert.Close;
      Result := Minutes;
      InsertNewProdHrs := True;
      MinuteNewProdHrs := Minutes;
    end;
  end;
  qrySalary.Close;

  if UpdateYes then (* RV023 *)
    FillProdHourPerHourType(EmployeeData, SalaryDate,
      HourTypeNumber, MinuteNewProdHrs, Manual, InsertNewProdHrs)
  else (* RV023 *)
// RV055.1. // RV063.1. Use old method for rounding!
    if not SystemDM.IsRFL then // RV077.2.
      if ARoundMinutes then
        RoundMinutesProdHourPerEmpPerType(SalaryDate, EmployeeData, HourTypeNumber,
          Minutes, Manual);
end;

procedure TGlobalDM.BooksRegularSalaryHours(
  (* AQuery: TQuery; RV023. *)
  BookingDay: TDateTime;
  EmployeeData: TScannedIDcard; BookMinutes: Integer; Manual: String);
var
  HourType: Integer;
//  InsertQuery: TQuery; // RV023. Not used!
begin
{$IFDEF DEBUG}
begin
  WDebugLog('Start BooksRegularSalaryHours');
  WDebugLog('- BookMinutes=' + IntToStr(BookMinutes));
end;
{$ENDIF}
  // TD-23817 Do not book negative values here (when non-manual)
  if { (BookMinutes <> 0) }
    (
      (Manual <> CHECKEDVALUE) and (BookMinutes > 0) // non-manual hours
      or
      (Manual = CHECKEDVALUE) and (BookMinutes <> 0) // manual hours
    ) then
  begin
    // regular salary hours
    // Hour Type = ? for regular hours.
//    InsertQuery := TQuery.Create(Nil);
//    QueryCopy(AQuery, InsertQuery);
    HourType := 1;
    // TD-23386 Workspot hourtype is determined during PopulateIDCard.
    if EmployeeData.WSHourtypeNumber <> -1 then
      HourType := EmployeeData.WSHourtypeNumber;
    // TD-23386
    if EmployeeData.ShiftHourtypeNumber <> -1 then
      HourType := EmployeeData.ShiftHourtypeNumber;
    // TD-23386
{
    qryWSHourtype.Close;
    qryWSHourtype.ParamByName('PCODE').AsString :=
      Employeedata.PlantCode;
    qryWSHourtype.ParamByName('WCODE').AsString :=
      Employeedata.WorkSpotCode;
    qryWSHourtype.Open;
    if (not qryWSHourtype.IsEmpty) and
      (not qryWSHourtype.FieldByName('HOURTYPE_NUMBER').IsNull) then
      HourType := qryWSHourtype.FieldByName('HOURTYPE_NUMBER').AsInteger;
}
{$IFDEF DEBUG}
begin
  WDebugLog('- HourType=' + IntToStr(HourType));
  WDebugLog('- UpdateSalaryHour()' +
    ' BookingDay=' + DateToStr(BookingDay) +
    ' BookMinutes=' + IntMin2StringTime(BookMinutes, False));
end;
{$ENDIF}
    UpdateSalaryHour(BookingDay, EmployeeData,
      HourType, BookMinutes, Manual, False,
      True);
    // TD-23386
{    qryWSHourtype.Close; }
//    InsertQuery.Active := False;
//    InsertQuery.Free;
  end; // if BookMinutes <> 0 then
{$IFDEF DEBUG}
  WDebugLog('End BooksRegularSalaryHours');
{$ENDIF}
end;

// This function fill a new table PRODHOURPEREMPLPERTYPE which keep the relation
// between production minutes and salary minutes per hourtype is call only if
// column  FILLPRODUCTIONHOURS_YN  = 'Y' in pimssetting
// MR:02-01-2003 Changed, also update record! Not only delete+insert new record.
procedure TGlobalDM.FillProdHourPerHourType(
  EmployeeData: TScannedIDCard;
  ProductionDate: TDateTime; HourType, Minutes: Integer; Manual: String;
  InsertProdHrs: Boolean);
var
  TotalMinutes: Integer;
  TotalMinutesExBreaks: Integer; // RV028.
  MyNow: TDateTime;
  procedure DetermineMinExclBreaks; // RV028.
  begin
    if (PayedBreakSave > 0) and
      ((TotalMinutesExBreaks - PayedBreakSave) < 0) then
    begin
      PayedBreakSave := abs(TotalMinutesExBreaks - PayedBreakSave);
      TotalMinutesExBreaks := 0;
    end
    else
    begin
      TotalMinutesExBreaks := TotalMinutesExBreaks - PayedBreakSave;
      PayedBreakSave := 0;
    end;
  end;
  // RV076.2.
  procedure PHEPTDeleteOneRec;
  begin
    // MR:06-09-2005 Order 550407. Delete it, after 'ProcessBookingsForContractRoundMinutes'
    // was processed to compensate for rounded minutes.
    with qryProdHourPEPTDeleteOneRec do
    begin
      Close;
      ParamByName('PDATE').AsDateTime := ProductionDate;
      ParamByName('PCODE').AsString := EmployeeData.PlantCode;
      ParamByName('SHNO').AsInteger := EmployeeData.ShiftNumber;
      ParamByName('EMPNO').AsInteger := EmployeeData.EmployeeCode;
      ParamByName('WCODE').AsString := EmployeeData.WorkSpotCode;
      ParamByName('JCODE').AsString := EmployeeData.JobCode;
      ParamByName('MAN').AsString := Manual;
      ParamByName('HOURTYPE').AsInteger := HourType;
      ExecSQL;
    end;
  end;
begin
  if (SystemDM.GetFillProdHourPerHourTypeYN = UNCHECKEDVALUE) then
    Exit;
  MyNow := Now;

{$IFDEF DEBUG3}
begin
  WDebugLog('Minutes=' + IntToStr(Minutes) +
    ' PayedBreakSave=' + IntToStr(PayedBreakSave));
end;
{$ENDIF}

  if InsertProdHrs then
  begin
    // Exists record?
    qryProdHourPEPT.Close;
    qryProdHourPEPT.ParamByName('PDATE').AsDateTime := ProductionDate;
    qryProdHourPEPT.ParamByName('PCODE').AsString := EmployeeData.PlantCode;
    qryProdHourPEPT.ParamByName('SHNO').AsInteger := EmployeeData.ShiftNumber;
    qryProdHourPEPT.ParamByName('EMPNO').AsInteger := EmployeeData.EmployeeCode;
    qryProdHourPEPT.ParamByName('WCODE').AsString := EmployeeData.WorkSpotCode;
    qryProdHourPEPT.ParamByName('JCODE').AsString := EmployeeData.JobCode;
    qryProdHourPEPT.ParamByName('MAN').AsString := Manual;
    qryProdHourPEPT.ParamByName('HOURTYPE').AsInteger := HourType;
    qryProdHourPEPT.Open;
    if not qryProdHourPEPT.IsEmpty then
    begin
      // Update record
      qryProdHourPEPT.First;
      TotalMinutes :=
        qryProdHourPEPT.FieldByName('PRODUCTION_MINUTE').AsInteger +
          Minutes;
      // RV028. Start
      TotalMinutesExBreaks :=
        qryProdHourPEPT.FieldByName('PROD_MIN_EXCL_BREAK').AsInteger +
          Minutes;
      DetermineMinExclBreaks;
      // RV028. End
      if TotalMinutes <> 0 then
      begin
        with qryProdHourPEPTUpdate do
        begin
          Close;
          ParamByName('PMIN').AsInteger := TotalMinutes;
          // RV028.
          ParamByName('PROD_MIN_EXCL_BREAK').AsInteger := TotalMinutesExBreaks;
          ParamByName('MDATE').AsDateTime := MyNow;
          ParamByName('MUTATOR').AsString := SystemDM.CurrentProgramUser;
          ParamByName('PDATE').AsDateTime := ProductionDate;
          ParamByName('PCODE').AsString := EmployeeData.PlantCode;
          ParamByName('SHNO').AsInteger := EmployeeData.ShiftNumber;
          ParamByName('EMPNO').AsInteger := EmployeeData.EmployeeCode;
          ParamByName('WCODE').AsString := EmployeeData.WorkSpotCode;
          ParamByName('JCODE').AsString := EmployeeData.JobCode;
          ParamByName('MAN').AsString := Manual;
          ParamByName('HOURTYPE').AsInteger := HourType;
          ExecSQL;
        end;
      end
      else
        PHEPTDeleteOneRec; // RV076.2.
    end
    else
    begin
      if Minutes <> 0 then // RV076.2.
      begin
        with qryProdHourPEPTInsert do
        begin
          Close;
          ParamByName('PDATE').AsDateTime := ProductionDate;
          ParamByName('PCODE').AsString := EmployeeData.PlantCode;
          ParamByName('SHNO').AsInteger := EmployeeData.ShiftNumber;
          ParamByName('EMPNO').AsInteger := EmployeeData.EmployeeCode;
          ParamByName('WCODE').AsString := EmployeeData.WorkSpotCode;
          ParamByName('JCODE').AsString := EmployeeData.JobCode;
          ParamByName('PMIN').AsInteger := Minutes;
          // RV028. Start
          TotalMinutesExBreaks := Minutes;
          DetermineMinExclBreaks;
          ParamByName('PROD_MIN_EXCL_BREAK').AsInteger := TotalMinutesExBreaks;
          // RV028. End
          ParamByName('MAN').AsString := Manual;
          ParamByName('HOURTYPE').AsInteger := HourType;
          ParamByName('CDATE').AsDateTime := MyNow;
          ParamByName('MDATE').AsDateTime := MyNow;
          ParamByName('MUTATOR').AsString := SystemDM.CurrentProgramUser;
          ExecSQL;
        end;
      end;
    end;
    qryProdHourPEPT.Close;
  end
  else
  begin
    PHEPTDeleteOneRec;
  end;
end;

function TGlobalDM.DetermineExceptionalBeforeOvertime(
  EmployeeData: TScannedIDCard): Boolean;
begin
  Result := False;
  with qryExcepBFOvertime do
  begin
    Close;
    ParamByName('EMPNO').AsInteger := EmployeeData.EmployeeCode;
    Open;
    if not IsEmpty then
    begin
      First;
      if FieldByName('EXCEPTIONAL_BEFORE_OVERTIME').AsString <> '' then
        if FieldByName('EXCEPTIONAL_BEFORE_OVERTIME').AsString = 'Y' then
          Result := True;
    end;
    Close;
  end;
end;

procedure TGlobalDM.DeleteProdHourForAllTypes(EmployeeData: TScannedIDCard;
  FromDate, ToDate: TDateTime;  Manual: String);
begin
  if (SystemDM.GetFillProdHourPerHourTypeYN = UNCHECKEDVALUE) then
    Exit;
  with qryProdHourPEPTDelete do
  begin
    Close;
    ParamByName('FROMDATE').AsDateTime := Trunc(FromDate);
    ParamByName('TODATE').AsDateTime := Trunc(ToDate);
    ParamByName('EMPNO').AsInteger := EmployeeData.EmployeeCode;
    ParamByName('MAN').AsString := Manual;
    ExecSql;
    Close;
  end;
end;

// Used only in Timerecording-program.
procedure TGlobalDM.ProcessIllnessMessage(EmployeeIDCard: TScannedIDCard);
  function UpdateIllnessMsg(EmployeeNumber: Integer;
    DateStart: TDateTime): Boolean;
  begin
    Result := True;
    with qryIllnessMsgUpdate do
    begin
      Close;
      ParamByName('MUTATIONDATE').AsDateTime := Now;
      ParamByName('MUTATOR').AsString := SystemDM.CurrentProgramUser;
      ParamByName('EDATE').AsDateTime := Trunc(Now - 1);
      ParamByName('SDATE').AsDateTime := DateStart;
      ParamByName('EMPNO').AsInteger := EmployeeNumber;
      ParamByName('MAN').AsString := CHECKEDVALUE;
      try
        ExecSql;
        Close;
      except
        on E: EDatabaseError do
        begin
          DisplayErrorMessage(E, PIMS_POSTING);
          Result := False;
          Close;
        end;
      end;
    end;
  end;
begin
  with qryIllnessMsg do
  begin
    Close;
    ParamByName('EMPNO').AsInteger := EmployeeIDCard.EmployeeCode;
    ParamByName('MAN').AsString := UNCHECKEDVALUE;
    Open;
    First;
    while not Eof do
    begin
      if UpdateIllnessMsg(EmployeeIDCard.EmployeeCode,
        FieldByName('ILLNESSMESSAGE_STARTDATE').AsDateTime) then
        UpdateEMA(FieldByName('ABSENCEREASON_CODE').AsString, EmployeeIDCard,
          Trunc(Now), 1); // RV103.1. Parameters changed.
      Next;
    end;
  end;
end;

procedure TGlobalDM.PopulateIDCard(var AIDCard: TScannedIDCard;
  ADataSet: TDataSet);
//var
//  StartDate, EndDate: TDateTime;
var
  WSHourTypeChecked: Boolean;
begin
  // TD-23386 Related to this order. Initialise values here
  // 20015346 Round time to minutes, also store original DateIn/Out in 2
  //          extra vars.
  AIDCard.DepartmentCode := '';
  AIDCard.ShiftHourtypeNumber := -1;
  AIDCard.WSHourtypeNumber := -1;
  AIDCard.IgnoreForOvertimeYN := '';
  WSHourTypeChecked := False;
  if ADataSet <> nil then
  begin
    AIDCard.EmployeeCode := ADataSet.FieldByName('EMPLOYEE_NUMBER').AsInteger;
    AIDCard.ShiftNumber := ADataSet.FieldByName('SHIFT_NUMBER').AsInteger;
    AIDCard.IDCard := ADataSet.FieldByName('IDCARD_IN').AsString;
    AIDCard.WorkSpotCode := ADataSet.FieldByName('WORKSPOT_CODE').AsString;
    AIDCard.JobCode := ADataSet.FieldByName('JOB_CODE').AsString;
    AIDCard.PlantCode := ADataSet.FieldByName('PLANT_CODE').AsString;
    AIDCard.DateIn := RoundTime(ADataSet.FieldByName('DATETIME_IN').AsDateTime,1);
    AIDCard.DateOut := RoundTime(ADataSet.FieldByName('DATETIME_OUT').AsDateTime,1);
    AIDCard.DateInOriginal  := ADataSet.FieldByName('DATETIME_IN').AsDateTime;
    AIDCard.DateOutOriginal := ADataSet.FieldByName('DATETIME_OUT').AsDateTime;
    AIDCard.DateInCutOff := AIDCard.DateIn;
    AIDCard.DateOutCutOff := AIDCard.DateOut;
    // 20013489
    AIDCard.ShiftDate := ADataSet.FieldByName('SHIFT_DATE').AsDateTime;
    // 20013489
    // Also do this here?
//    AProdMinClass.GetShiftDay(AIDCard, StartDate, EndDate);
    // TD-23386 Related to this: Also get hourtype-of-workspot here.
    if Assigned(ADataSet.FindField('WORKSPOT_HOURTYPE_NUMBER')) then
      if ADataSet.FieldByName('WORKSPOT_HOURTYPE_NUMBER').AsString <> '' then
      begin
        AIDCard.WSHourtypeNumber :=
          ADataSet.FieldByName('WORKSPOT_HOURTYPE_NUMBER').AsInteger;
        // TD-23386 Related to this: Also get IgnoreForOvertime based on
        //          hourtype-of-workspot
        if Assigned(ADataSet.FindField('IGNORE_FOR_OVERTIME_YN')) then
        begin
          WSHourTypeChecked := True;
          AIDCard.IgnoreForOvertimeYN :=
            ADataSet.FieldByName('IGNORE_FOR_OVERTIME_YN').AsString;
        end;
      end;
    // TD-23386 Try to determine Hourtype defined on Shift.
    // TD-23386 Hourtype-on-shift has LOWER priority then Hourtype-on-workspot.
    if AIDCard.WSHourtypeNumber = -1 then
      if Assigned(ADataSet.FindField('SHIFT_HOURTYPE_NUMBER')) then
        if ADataSet.FieldByName('SHIFT_HOURTYPE_NUMBER').AsString <> '' then
        begin
          AIDCard.ShiftHourtypeNumber :=
            ADataSet.FieldByName('SHIFT_HOURTYPE_NUMBER').AsInteger;
          if Assigned(ADataSet.FindField('SHIFT_IGNORE_FOR_OVERTIME_YN')) then
            AIDCard.IgnoreForOvertimeYN :=
              ADataSet.FieldByName('SHIFT_IGNORE_FOR_OVERTIME_YN').AsString
          else
            if Assigned(ADataSet.FindField('IGNORE_FOR_OVERTIME_YN')) then
              AIDCard.IgnoreForOvertimeYN :=
                ADataSet.FieldByName('IGNORE_FOR_OVERTIME_YN').AsString;
        end;
    // TD-23386 Related to this: Also get DepartmentCode.
    if Assigned(ADataSet.FindField('WORKSPOT_DEPARTMENT_CODE')) then
      AIDCard.DepartmentCode :=
        ADataSet.FieldByName('WORKSPOT_DEPARTMENT_CODE').AsString
    else
      if Assigned(ADataSet.FindField('DEPARTMENT_CODE')) then
        AIDCard.DepartmentCode :=
          ADataSet.FieldByName('DEPARTMENT_CODE').AsString;

  end;
  // Filter other fields here.
  with qryPopIDCard do
  begin
    Close;
    ParamByName('EMPNO').AsInteger := AIDCard.EmployeeCode;
    Open;
    if not IsEmpty then
    begin
      // TD-23386 Related to this order.
      // Do not assign department here! Because it MUST be the department
      // based on workspot (based on scan), not based on employee.
//      AIDCard.DepartmentCode := FieldByName('DEPARTMENT_CODE').AsString;
      AIDCard.InscanEarly := FieldByName('INSCAN_MARGIN_EARLY').AsInteger;
      AIDCard.InscanLate := FieldByName('INSCAN_MARGIN_LATE').AsInteger;
      AIDCard.OutScanEarly := FieldByName('OUTSCAN_MARGIN_EARLY').AsInteger;
      AIDCard.OutScanLate := FieldByName('OUTSCAN_MARGIN_LATE').AsInteger;
      AIDCard.CutOfTime := FieldByName('CUT_OF_TIME_YN').AsString;
      AIDCard.ContractgroupCode :=
        FieldByName('CONTRACTGROUP_CODE').AsString; // RV055.1.
      AIDCard.ExceptionalBeforeOvertime :=
        FieldByName('EXCEPTIONAL_BEFORE_OVERTIME').AsString;  // RV055.1.
      AIDCard.RoundTruncSalaryHours :=
        FieldByName('ROUND_TRUNC_SALARY_HOURS').AsInteger; // RV055.1.
      AIDCard.RoundMinute :=
        FieldByName('ROUND_MINUTE').AsInteger; // RV055.1.
    end;
    Close;
  end;
  AIDCard.FromTimeRecordingApp := False;
  // RV100.1.
  if SystemDM.IsCVL then
  begin
    // Use 'overruled contract group' when this is attached to the workspot.
    AIDCard.ContractgroupCode := FindWorkspotOverruledContractGroup(
      AIDCard.PlantCode, AIDCard.WorkSpotCode, AIDCard.ContractgroupCode);
  end;
  // TD-23386 Related to this order.
  // When some values are not filled, do it here!
  if AIDCard.DepartmentCode = '' then
  begin
    with qryWorkspot do
    begin
      Close;
      ParamByName('PLANT_CODE').AsString := AIDCard.PlantCode;
      ParamByName('WORKSPOT_CODE').AsString := AIDCard.WorkSpotCode;
      Open;
      if not Eof then
      begin
        AIDCard.DepartmentCode :=
          FieldByName('DEPARTMENT_CODE').AsString;
        AIDCard.WSHourtypeNumber :=
          FieldByName('HOURTYPE_NUMBER').AsInteger;
        if AIDCard.WSHourTypeNumber <> -1 then
          AIDCard.IgnoreForOvertimeYN :=
            FieldByName('IGNORE_FOR_OVERTIME_YN').AsString;
      end;
      Close;
    end; // with
  end; // if
  // TD-23386 Related to this order.
  // When some values are not filled, do it here!
  // Shift-Hourtype has LOWER priority then Workspot-Hourtype.
  if AIDCard.WSHourTypeNumber = -1 then
    if AIDCard.ShiftHourtypeNumber = -1 then
    begin
      if not WSHourTypeChecked then
      begin
        with qryShiftHT do
        begin
          Close;
          ParamByName('PLANT_CODE').AsString := AIDCard.PlantCode;
          ParamByName('SHIFT_NUMBER').AsInteger := AIDCard.ShiftNumber;
          Open;
          if not Eof then
          begin
            AIDCard.ShiftHourtypeNumber := FieldByName('HOURTYPE_NUMBER').AsInteger;
            AIDCard.IgnoreForOvertimeYN :=
              FieldByName('IGNORE_FOR_OVERTIME_YN').AsString;
          end;
        end; // with
      end; // if
    end; // if
end; // PopulateIDCard

// MRA:3-DEC-2008. RV015. Round Minutes.
// Round minutes for each record found in SALARYHOURPEREMPLOYEE (SHE) for the
// BookingDay and Employee. The rounding-difference (positive or negative) when
// not equal to 0, must be changed in the record that was found,
// for each combination of
// SHE.SALARY_DATE, SHE.EMPLOYEE_NUMBER, SHE.HOURTYPE_NUMBER and SHE.MANUAL_YN.
// Fields that must be changed are SHE.SALARY_MINUTE,
// SHE.MUTATIONDATE and SHE.MUTATOR.
// RV076.4. Return minutes that were lost during rounding.
// RV076.4. Option added to only round for a given hourtypenumber!
//          When argument 'AHourTypeNumber' is not -1 then only
//          round for the given hourtypenumber.
function TGlobalDM.ContractRoundMinutes(
  AEmployeeIDCard: TScannedIDCard; ABookingDay: TDateTime;
   AHourTypeNumber: Integer=-1): Integer;
var
  NewSalMin, OldSalMin, DifferenceMin: Integer;
  PreviousDifferenceMin, LeftOverMin: Integer; // RV039.1.
  RoundTruncSalaryHours, RoundMinute: Integer;
  function RoundMinutes(RoundTruncSalaryHours, RoundMinute: Integer;
    Minutes: Integer): Integer;
  var
    Hours, Mins: Integer;
  begin
    Hours := Minutes DIV 60;
    Mins := Minutes MOD 60;
    if RoundTruncSalaryHours = 1 then // Round
      Mins := Round(Mins / RoundMinute) * RoundMinute
    else
      Mins := Trunc(Mins / RoundMinute) * RoundMinute;
    Result := Hours * 60 + Mins;
  end;
begin
  // RV076.4.
  Result := 0;

  AProdMinClass.ReadContractRoundMinutesParams(AEmployeeIDCard,
    RoundTruncSalaryHours, RoundMinute);
  if RoundMinute <= 1 then
    Exit;

  PreviousDifferenceMin := -999; // RV039.1.
  // RV039.1. Sort qrySHE on HOURTYPE (ASC).
  // ABS-19136 Sort on HOURTYPE DESC to ensure the regular hours are as
  //           expected.
  with qrySHE do
  begin
    Close;
    ParamByName('EMPLOYEE_NUMBER').AsInteger :=
      AEmployeeIDCard.EmployeeCode;
    ParamByName('SALARY_DATE').AsDateTime :=
      ABookingDay;
    ParamByName('HOURTYPE_NUMBER').AsInteger :=
      AHourTypeNumber;
    Open;
    while not Eof do
    begin
      // RV039.1. If there was a previous difference, then first add it to
      //        the current record.
      LeftOverMin := 0;
      // RV040.3. Do this only for 'truncate' !
      if (RoundTruncSalaryHours = 0) then // Truncate
      begin
        if (PreviousDifferenceMin <> -999) then
        begin
          // If difference was negative, it must be added as a plus-value,
          // otherwise it must be subtracted.
          if (PreviousDifferenceMin <> 0) then
          begin
            // Positive must be negative and the other way round.
            if (PreviousDifferenceMin < 0) then
              LeftOverMin := ABS(PreviousDifferenceMin)
            else
              LeftOverMin := PreviousDifferenceMin * -1;
            // Store the LeftOverMin in Salary-record.
            UpdateSalaryHour(ABookingDay, AEmployeeIDCard,
              FieldByName('HOURTYPE_NUMBER').AsInteger,
              LeftOverMin,
              UNCHECKEDVALUE, False,
              False);
          end;
        end;
      end; // if (RoundTruncateSalaryHours = 0)
      // Round minutes and if there was a difference, update the salary-record.
      OldSalMin := FieldByName('SALARY_MINUTE').AsInteger
        + LeftOverMin; // RV039.1.
      NewSalMin :=
        RoundMinutes(RoundTruncSalaryHours, RoundMinute, OldSalMin);
      DifferenceMin := NewSalMin - OldSalMin;
      PreviousDifferenceMin := DifferenceMin; // RV039.1.
      // RV076.4. Do this only for Truncate
      if (RoundTruncSalaryHours = 0) then // Truncate
        Result := Result + DifferenceMin; // RV076.4.
      if DifferenceMin <> 0 then
      begin
        UpdateSalaryHour(ABookingDay, AEmployeeIDCard,
          FieldByName('HOURTYPE_NUMBER').AsInteger,
          DifferenceMin,
          UNCHECKEDVALUE, False,
          False (* RV023. Use other procedure for rounding PHEPT! *) );
{$IFDEF DEBUG}
begin
  WDebugLog('SAL Rounding minutes. Bookingday=' + DateToStr(ABookingDay) +
    ' OldSal= ' + IntToStr(OldSalMin) +
    ' NewSal= ' + IntToStr(NewSalMin) +
    ' Diff=' + IntToStr(DifferenceMin) +
//    ' LeftOver=' + IntToStr(LeftOverMin) +
    ' HT=' + IntToStr(FieldByName('HOURTYPE_NUMBER').AsInteger) +
    ' Emp=' + IntToStr(AEmployeeIDCard.EmployeeCode) +
    ' In=' + DateTimeToStr(AEmployeeIDCard.DateIn) +
    ' Out=' + DateTimeToStr(AEmployeeIDCard.DateOut)
    );
end;
{$ENDIF}

      end;
      Next;
    end;
    Close;
  end;
  // MRA:05-MAR-2009 RV023. Update PHEPT here!
  // MRA:09-MAR-2009 RV023. Don't use this. Round PHEPT during salary-rounding.
//  RoundMinutesPHEPT;
end;

// MRA:09-MAR-2009 RV023.
// Update the first found record for ProdHourPerEmpPerType for the given
// Employee, Date, Hourtype-combination.
// This is used to correct minutes during rounding.
// RV063.1.
procedure TGlobalDM.RoundMinutesProdHourPerEmpPerType(ABookingDay: TDateTime;
  AEmployeeData: TScannedIDCard;
  AHourTypeNumber, AMinutes: Integer;
  AManual: String);
var
  OldProdMin, NewProdMin: Integer;
  // RV028. Store left over minutes in one of last 5 records.
  ProdMinExclBreak: Integer;
  procedure StoreProdMinExclBreak;
  begin
    qryPEPTEB.Close;
    qryPEPTEB.ParamByName('EMPLOYEE_NUMBER').AsInteger :=
      AEmployeeData.EmployeeCode;
    qryPEPTEB.ParamByName('DATEFROM').AsDateTime :=
      ABookingDay - 5;
    qryPEPTEB.ParamByName('DATETO').AsDateTime :=
      ABookingDay;
    qryPEPTEB.ParamByName('MANUAL_YN').AsString := AManual;
    qryPEPTEB.Open;
    if not qryPEPTEB.Eof then
    begin
      with qryPEPTEBUpdate do
      begin
        // Set parameters
        ParamByName('PMIN').AsInteger :=
          qryPEPTEB.FieldByName('PROD_MIN_EXCL_BREAK').AsInteger +
            ProdMinExclBreak;
        ParamByName('MDATE').AsDateTime := Now;
        ParamByName('MUTATOR').AsString := SystemDM.CurrentProgramUser;
        // Search parameters
        ParamByName('PDATE').AsDateTime :=
          qryPEPTEB.FieldByName('PRODHOUREMPLOYEE_DATE').AsDateTime;
        ParamByName('PCODE').AsString :=
          qryPEPTEB.FieldByName('PLANT_CODE').AsString;
        ParamByName('SHNO').AsInteger :=
          qryPEPTEB.FieldByName('SHIFT_NUMBER').AsInteger;
        ParamByName('EMPNO').AsInteger :=
          qryPEPTEB.FieldByName('EMPLOYEE_NUMBER').AsInteger;
        ParamByName('WCODE').AsString :=
          qryPEPTEB.FieldByName('WORKSPOT_CODE').AsString;
        ParamByName('JCODE').AsString :=
          qryPEPTEB.FieldByName('JOB_CODE').AsString;
        ParamByName('MAN').AsString :=
          qryPEPTEB.FieldByName('MANUAL_YN').AsString;
        ParamByName('HOURTYPE').AsInteger :=
          qryPEPTEB.FieldByName('HOURTYPE_NUMBER').AsInteger;
        ExecSQL;
      end;
    end;
    qryPEPTEB.Close;
  end;
begin
  if (SystemDM.GetFillProdHourPerHourTypeYN = UNCHECKEDVALUE) then
    Exit;
  qryPEPT.Close;
  qryPEPT.ParamByName('EMPLOYEE_NUMBER').AsInteger :=
    AEmployeeData.EmployeeCode;
  qryPEPT.ParamByName('PRODHOUREMPLOYEE_DATE').AsDateTime :=
    ABookingDay;
  qryPEPT.ParamByName('MANUAL_YN').AsString := AManual;
  qryPEPT.ParamByName('HOURTYPE_NUMBER').AsInteger := AHourTypeNumber;
  qryPEPT.Open;
  if not qryPEPT.Eof then
  begin
    // Only update first found record!
    qryPEPT.First;
    ProdMinExclBreak := qryPEPT.FieldByName('PROD_MIN_EXCL_BREAK').AsInteger; // RV028.
    OldProdMin := qryPEPT.FieldByName('PRODUCTION_MINUTE').AsInteger;
    NewProdMin := OldProdMin + AMinutes;
    if NewProdMin <> 0 then
    begin
      // ProdMin has changed, so update record
      with qryPHEPTUpdate do
      begin
        // Set parameters
        ParamByName('PMIN').AsInteger := NewProdMin;
        ParamByName('MDATE').AsDateTime := Now;
        ParamByName('MUTATOR').AsString := SystemDM.CurrentProgramUser;
        // Search parameters
        ParamByName('PDATE').AsDateTime :=
          qryPEPT.FieldByName('PRODHOUREMPLOYEE_DATE').AsDateTime;
        ParamByName('PCODE').AsString :=
          qryPEPT.FieldByName('PLANT_CODE').AsString;
        ParamByName('SHNO').AsInteger :=
          qryPEPT.FieldByName('SHIFT_NUMBER').AsInteger;
        ParamByName('EMPNO').AsInteger :=
          qryPEPT.FieldByName('EMPLOYEE_NUMBER').AsInteger;
        ParamByName('WCODE').AsString :=
          qryPEPT.FieldByName('WORKSPOT_CODE').AsString;
        ParamByName('JCODE').AsString :=
          qryPEPT.FieldByName('JOB_CODE').AsString;
        ParamByName('MAN').AsString :=
          qryPEPT.FieldByName('MANUAL_YN').AsString;
        ParamByName('HOURTYPE').AsInteger :=
          qryPEPT.FieldByName('HOURTYPE_NUMBER').AsInteger;
        ExecSQL;
      end;
    end
    else
    begin
      // ProdMin is 0, so delete record
      with qryProdHourPEPTDeleteOneRec do
      begin
        ParamByName('PDATE').AsDateTime :=
          qryPEPT.FieldByName('PRODHOUREMPLOYEE_DATE').AsDateTime;
        ParamByName('PCODE').AsString :=
          qryPEPT.FieldByName('PLANT_CODE').AsString;
        ParamByName('SHNO').AsInteger :=
          qryPEPT.FieldByName('SHIFT_NUMBER').AsInteger;
        ParamByName('EMPNO').AsInteger :=
          qryPEPT.FieldByName('EMPLOYEE_NUMBER').AsInteger;
        ParamByName('WCODE').AsString :=
          qryPEPT.FieldByName('WORKSPOT_CODE').AsString;
        ParamByName('JCODE').AsString :=
          qryPEPT.FieldByName('JOB_CODE').AsString;
        ParamByName('MAN').AsString :=
          qryPEPT.FieldByName('MANUAL_YN').AsString;
        ParamByName('HOURTYPE').AsInteger :=
          qryPEPT.FieldByName('HOURTYPE_NUMBER').AsInteger;
        ExecSQL;
        StoreProdMinExclBreak; // RV028.
      end;
    end;
  end;
  qryPEPT.Close;
end;

function TGlobalDM.CalcEarnedTFTHours(ATFTQuery: TQuery): Double;
var
  EarnedTime: Double;
  TotEarnedTime: Double; // RV045.1.
  CGTFT: Boolean; // RV045.1.
  BonusInMoney: Boolean; // RV045.1.
  BonusPercentage: Double; // RV045.1.
begin
  Result := 0;
  TotEarnedTime := 0; // RV045.1. When 0, this must also be stored!
  if not ATFTQuery.Eof then
  begin
    // RV045.1. Addition of calculation for Bonus_percentage.
    while not ATFTQuery.Eof do
    begin
      // CGTFT_YN, H.HOURTYPE_NUMBER, H.BONUS_IN_MONEY_YN AS HT_BONUS_IN_MONEY,
      // C.BONUS_IN_MONEY_YN,
      // H.BONUS_PERCENTAGE
      CGTFT := (ATFTQuery.FieldByName('CGTFT_YN').AsString = 'Y');
      if CGTFT then
        BonusInMoney := (ATFTQuery.FieldByName('BONUS_IN_MONEY_YN').AsString = 'Y')
      else
        BonusInMoney := (ATFTQuery.FieldByName('HT_BONUS_IN_MONEY_YN').AsString = 'Y');
      BonusPercentage := 0;
      if (ATFTQuery.FieldByName('BONUS_PERCENTAGE').AsString <> '') then
        BonusPercentage := ATFTQuery.FieldByName('BONUS_PERCENTAGE').AsFloat;
      EarnedTime := ATFTQuery.FieldByName('SUMMINUTE').AsFloat;
      if not BonusInMoney then
      begin
        EarnedTime := Round(EarnedTime + EarnedTime *
          BonusPercentage / 100);
      end;
      TotEarnedTime := TotEarnedTime + EarnedTime;
      ATFTQuery.Next;
    end;
    ATFTQuery.Close;
  end; // if not qryGetTFTHours.Eof then
  Result := Result + TotEarnedTime;
end; // CalcEarnedTFTHours

// RV033.4. MRA:18-SEP-2009.
// Procedure that can recalculate time-for-time hours made during overtime
// that is stored in ABSENCETOTAL table.
// This must be done when salary-hours are recalculated and must be
// done for the employee and the complete year.
// RV045.1. Also: Calculation BONUS_PERCENTAGE added, this was missing!
// RV045.1. When 0 earned minutes are found, then this must also be updated!
// RV100.2. Change procedure so it can also return the earned minutes for TFT,
//          without making changes to balance.
function TGlobalDM.RecalcEarnedTFTHours(AEmployeeNumber: Integer;
  ADateFrom: TDatetime; ADateTo: TDateTime;
  AUpdateBalance: Boolean=True;
  AMyDate: Boolean=False): Double;
var
  DateFrom: TDateTime;
  DateTo: TDateTime;
  YearFrom, YearTo, Year, Month, Day: Word;
//  EarnedTime: Double;
  IsTFT: Boolean;
  ContractGroupTFT: Boolean; // RV045.1.
  TotEarnedTime: Double; // RV045.1.
//  CGTFT: Boolean; // RV045.1.
//  BonusInMoney: Boolean; // RV045.1.
//  BonusPercentage: Double; // RV045.1.
  BookingDay: TDateTime;
begin
  // RV045.1. Also check on TFT-settings on Hourtype, when no
  //          TFT-settings were made on ContractGroup-level.
  Result := 0;
  IsTFT := False;
  ContractGroupTFT := False;
  if ContractGroupTFTExists then
  begin
    ContractGroupTFT := True;
    // First check if Employee is of type TFT.
    with qryCheckEmpTFT do
    begin
      Close;
      ParamByName('EMPNO').AsInteger := AEmployeeNumber;
      ParamByName('TFT_YN').AsString := CHECKEDVALUE;
      Open;
      if not Eof then
        if (FieldByName('CSUM').AsInteger = 1) then
          IsTFT := True;
      Close;
    end;
  end;
{$IFDEF DEBUG}
  WDebugLog('- RecalcEarnedTFTHours - CGTFT=' + BoolToYN(ContractGroupTFT) +
    ' IsTFT=' + BoolToYN(IsTFT));
{$ENDIF}
  if (not ContractGroupTFT) or (IsTFT) and
    (ADateFrom <> NullDate) and (ADateTo <> NullDate) then
  begin
    DecodeDate(ADateFrom, YearFrom, Month, Day);
    DecodeDate(ADateTo, YearTo, Month, Day);
    for Year := YearFrom to YearTo do
    begin
      if not AMyDate then
      begin
        DateFrom := EncodeDate(Year, 1, 1);
        DateTo := EncodeDate(Year, 12, 31);
      end
      else
      begin
        DateFrom := ADateFrom;
        DateTo := ADateTo;
      end;
      // RV045.1. Query changed.
      with qryGetTFTHours do
      begin
        // First check for TFT-minutes.
        Close;
        ParamByName('EMPNO').AsInteger := AEmployeeNumber;
        ParamByName('DATEFROM').AsDateTime := DateFrom;
        ParamByName('DATETO').AsDateTime := DateTo;
        // RV045.1. Added extra param for query.
        //          If ContractGroupTFT is true,
        //          then use contractgroup-TFT-settings,
        //          if false, then use hourtype-TFT-settings.
        if ContractGroupTFT then
          ParamByName('CONTRACTGROUPTFT').AsString := CHECKEDVALUE
        else
          ParamByName('CONTRACTGROUPTFT').AsString := UNCHECKEDVALUE;
//        ParamByName('TFT_YN').AsString := CHECKEDVALUE;
        Open;
      end;
//      TotEarnedTime := 0; // RV045.1. When 0, this must also be stored!
      TotEarnedTime := CalcEarnedTFTHours(qryGetTFTHours);
{
      if not qryGetTFTHours.Eof then
      begin
        // RV045.1. Addition of calculation for Bonus_percentage.
        while not qryGetTFTHours.Eof do
        begin
          // CGTFT_YN, H.HOURTYPE_NUMBER, H.BONUS_IN_MONEY_YN AS HT_BONUS_IN_MONEY,
          // C.BONUS_IN_MONEY_YN,
          // H.BONUS_PERCENTAGE
          CGTFT := (qryGetTFTHours.FieldByName('CGTFT_YN').AsString = 'Y');
          if CGTFT then
            BonusInMoney := (qryGetTFTHours.FieldByName('BONUS_IN_MONEY_YN').AsString = 'Y')
          else
            BonusInMoney := (qryGetTFTHours.FieldByName('HT_BONUS_IN_MONEY_YN').AsString = 'Y');
          BonusPercentage := 0;
          if (qryGetTFTHours.FieldByName('BONUS_PERCENTAGE').AsString <> '') then
            BonusPercentage := qryGetTFTHours.FieldByName('BONUS_PERCENTAGE').AsFloat;
          EarnedTime := qryGetTFTHours.FieldByName('SUMMINUTE').AsFloat;
          if not BonusInMoney then
          begin
            EarnedTime := Round(EarnedTime + EarnedTime *
              BonusPercentage / 100);
          end;
          TotEarnedTime := TotEarnedTime + EarnedTime;
          qryGetTFTHours.Next;
        end;
        qryGetTFTHours.Close;
      end; // if not qryGetTFTHours.Eof then
}
      // Now update ABSENCETOTAL, field EARNED_TFT_MINUTE
      // RV071.6.
      Result := Result + TotEarnedTime;
      if AUpdateBalance then
      begin
        BookingDay := EncodeDate(Year, 1, 1);
        UpdateAbsenceTotal('EARNED_TFT_MINUTE', BookingDay, AEmployeeNumber,
          TotEarnedTime, True);
      end;
      // Not year from year!
      if AMyDate then
        Break;
    end; // for Year := YearFrom to YearTo do
  end; // if (IsTFT) and
end;

// RV100.2. Change procedure so it can also return the earned minutes for TFT,
//          without making changes to balance.
function TGlobalDM.DetermineEarnedTFTHours(AEmployeeNumber: Integer;
  ADateFrom: TDatetime; ADateTo: TDateTime; AMyDate: Boolean=False): Double;
begin
  // Only determine earned TFT hours
  Result := RecalcEarnedTFTHours(AEmployeeNumber, ADateFrom, ADateTo, False,
    AMyDate);
end;

// MR:30-10-2003 'UpdateProdMin' has been divided in 'UpdateProdMinIn' and
// 'UpdateProdMinOut', because of overnight scans, to decide if only the
// day before midnight should be processed, or the day after, or both.
procedure TGlobalDM.ProcessTimeRecording(AQuery: TQuery; EmployeeIDCard: TScannedIDCard;
  FirstScan, LastScan, UpdateSalMin, UpdateProdMinOut, UpdateProdMinIn: Boolean;
  DeleteAction: Boolean; DeleteEmployeeIDCard: TScannedIDCard;
  LastScanRecordForDay: Boolean);
var
  BookingDay, SaveDate_Out, SaveDate_In, SaveDate_In_CutOff: TDateTime;
  ProdMin,BreaksMin, PayedBreaks,PayedBreaksTot, ProdMinTot: Integer;
  IsOvernight: Boolean;
  ProdHrsBreaksMin: Integer; // PIM-319
begin
  // TD-23368 This is determined during PopulateIDCard.
{
  // MR:13-06-2003 Start
  // Get Department of Workspot for EmployeeIDCard
  // RV033.1. MRA:17-SEP-2009.
  // Also get the 'ignore for overtime' that can be set for workspot's hourtype.
  // RV080.6. Also determine Hourtype from Workspot and store it in EmployeeIDCard.
  with qryWorkspot do
  begin
    Close;
    ParamByName('PLANT_CODE').AsString := EmployeeIDCard.PlantCode;
    ParamByName('WORKSPOT_CODE').AsString := EmployeeIDCard.WorkspotCode;
    Open;
  end;
  // RV080.6.
  EmployeeIDCard.WSHourtypeNumber := -1;
  // RV033.1.
  EmployeeIDCard.IgnoreForOvertimeYN := '';
  if not qryWorkspot.IsEmpty then
  begin
    // RV080.6.
    EmployeeIDCard.WSHourtypeNumber :=
      qryWorkspot.FieldByName('HOURTYPE_NUMBER').AsInteger;
    EmployeeIDCard.DepartmentCode :=
      qryWorkspot.FieldByName('DEPARTMENT_CODE').AsString;
    // RV033.1.
    if qryWorkspot.FieldByName('IGNORE_FOR_OVERTIME_YN').AsString <> '' then
      EmployeeIDCard.IgnoreForOvertimeYN :=
        qryWorkspot.FieldByName('IGNORE_FOR_OVERTIME_YN').AsString;
  end;
  qryWorkspot.Close;
  // MR:13-06-2003 End
}
  ProdMinTot := 0;
  PayedBreaksTot := 0;
  SaveDate_In_CutOff := 0; // MR:06-01-2004
  SaveDate_Out := EmployeeIDCard.DateOut;
  SaveDate_In := EmployeeIDCard.DateIn;

  // MR:08-09-2003
  // Overnight scan?
  IsOverNight := Trunc(EmployeeIDCard.DateOut) > Trunc(EmployeeIDCard.DateIn);
  if IsOverNight then
  begin
    EmployeeIDCard.DateOut := Trunc(EmployeeIDCard.DateOut);

    // MR:10-10-2003 Added 'DeleteAction' and 'DeleteEmployeeIDCard
    AProdMinClass.GetProdMin(
      EmployeeIDCard, FirstScan, False,
      DeleteAction, DeleteEmployeeIDCard,
      BookingDay, ProdMin, BreaksMin, PayedBreaks);
{$IFDEF DEBUG}
  WDebugLog('GetProdMin. ProdMin=' + IntMin2StringTime(ProdMin, False) +
    ' BreaksMin=' + IntMin2StringTime(BreaksMin, False) +
    ' PayedBreaks=' + IntMin2StringTime(PayedBreaks, False)
    );
{$ENDIF}
    if SystemDM.ProdHrsDoNotSubtractBreaks then // PIM-319
      ProdHrsBreaksMin := BreaksMin
    else
      ProdHrsBreaksMin := 0;

    // MR:03-12-2003 If overnight, the first value must be saved here.
    SaveDate_In_CutOff := EmployeeIDCard.DateInCutOff;
    ProdMinTot := ProdMinTot + ProdMin;
    PayedBreaksTot := PayedBreaksTot + PayedBreaks;
    if ((ProdMin + ProdHrsBreaksMin) > 0) and UpdateProdMinIn then // PIM-319
      GlobalDM.UpdateProductionHour(
        Trunc(EmployeeIDCard.DateIn), EmployeeIDCard,
          ProdMin + ProdHrsBreaksMin, PayedBreaks, UNCHECKEDVALUE); // PIM-319
    EmployeeIDCard.DateIn := EmployeeIDCard.DateOut;
    EmployeeIDCard.DateOut := SaveDate_Out;
  end;
  { FirstScan (if scan is overnight: false) }
  if IsOverNight then
    FirstScan := False;
  // MR:10-10-2003 Added 'DeleteAction' and 'DeleteEmployeeIDCard
  AProdMinClass.GetProdMin(
    EmployeeIDCard, FirstScan, LastScan,
    DeleteAction, DeleteEmployeeIDCard,
    BookingDay, ProdMin, BreaksMin, PayedBreaks);
{$IFDEF DEBUG}
  WDebugLog('GetProdMin. ProdMin=' + IntMin2StringTime(ProdMin, False) +
    ' BreaksMin=' + IntMin2StringTime(BreaksMin, False) +
    ' PayedBreaks=' + IntMin2StringTime(PayedBreaks, False)
    );
{$ENDIF}
  if SystemDM.ProdHrsDoNotSubtractBreaks then // PIM-319
    ProdHrsBreaksMin := BreaksMin
  else
    ProdHrsBreaksMin := 0;
  // MR:03-12-2003 If overnight, the first value must be restored here.
  if IsOverNight then
    EmployeeIDCard.DateInCutOff := SaveDate_In_CutOff;
  ProdMinTot := ProdMinTot + ProdMin;
  PayedBreaksTot := PayedBreaksTot + PayedBreaks;
  if ((ProdMin + ProdHrsBreaksMin) > 0) and UpdateProdMinOut then // PIM-319
    GlobalDM.UpdateProductionHour(
      Trunc(EmployeeIDCard.DateOut),
      EmployeeIDCard, ProdMin + ProdHrsBreaksMin, PayedBreaks, UNCHECKEDVALUE); // PIM-319
  EmployeeIDCard.DateIn := SaveDate_In;
  EmployeeIDCard.DateOut := SaveDate_Out;
  if IsOverNight then
  begin
    AProdMinClass.GetShiftDay(EmployeeIdCard, SaveDate_In, SaveDate_Out);
    BookingDay := Trunc(SaveDate_In);
  end;
  // The total amount of minute for write in
  // PRODHOURPEREMPLOYEE is: ProdMin + PayedBreaks
  // MR:09-09-2003
  if UpdateSalMin then
  begin
    ProcessBookings(AQuery, EmployeeIDCard,
      BookingDay, UpdateProdMinOut or UpdateProdMinIn,
      ProdMinTot , PayedBreaksTot,
      UNCHECKEDVALUE, False);
    if AProdMinClass.AContractRoundMinutes and LastScanRecordForDay then
    begin
      // MRA:03-DEC-2008 RV015. Rounding Minutes.
      // Round in a different way!
      // Do it for each record found in SalaryHourPerEmployee for the
      // BookingDay and Employee. The rounding-difference (positive or negative)
      // must be changed in the record that was found, using the hour-type.

      // RV063.1. Use old method for rounding!
      // OLD METHOD:
      GlobalDM.ContractRoundMinutes(EmployeeIDCard, BookingDay);
    end;
  end;
end;

// 20013489
procedure TGlobalDM.UnprocessProcessScans(AQuery: TQuery;
  AIDCard: TScannedIDCard;
  AFromDate, AProdDate1, AProdDate2, AProdDate3, AProdDate4: TDateTime;
  AProcessSalary, ADeleteAction: Boolean;
  ARecalcProdHours: Boolean;
  AFromProcessPlannedAbsence: Boolean=False;
  AManual: Boolean=False); // 20013489 Call with True when it is about manual hours!
var
  {BookingDay, } StartDate, EndDate, CurrentBookingDay,
  CurrentStart, CurrentEnd { SaveDate_Out,  SaveDate_In}: TDateTime;
  index {ProdMin, BreaksMin, PayedBreaks ,HourTypeNumber}: integer;
  TotEarnedMin: Double;
  CurrentIDCard: TScannedIDCard;
  CurrentTFT, CurrentBonusInMoney {,Processed, DeletedOld}: Boolean;
  RecordsForUpdate: TList;
  WhereSelect: String;
  AddToProdListOut, AddToProdListIn, AddToSalList: Boolean;
  ABookmarkRecord: PTBookmarkRecord;
  ProdDate: array[1..4] of TDateTime;
  I: Integer;
  LastScanRecordForDay: Boolean;
  BookingDayCurrent, BookingDayNext: TDateTime;
  CurrentProdDate1, CurrentProdDate2, CurrentProdDate3, CurrentProdDate4: TDateTime;
  GoOn: Boolean;
  SalDateFound, ProdDateFound: Boolean;
  DateInFirstScan: TDateTime; // GLOB3-81
  procedure GetTFT_Bonus(AEmployee: Integer);
  begin
    CurrentTFT := False;
    CurrentBonusInMoney := False;
    with qryUPSGetTFTBonus do
    begin
      Close;
      ParamByName('EMPNO').AsInteger := AEmployee;
      Open;
      if not Eof then
      begin
        CurrentTFT :=
          (FieldByName('TIME_FOR_TIME_YN').AsString = CHECKEDVALUE);
        CurrentBonusInMoney :=
          (FieldByName('BONUS_IN_MONEY_YN').AsString = CHECKEDVALUE);
      end;
      Close;
    end;
  end;
  procedure DeleteAbsenceHours(AFromDate, ToDate: TDateTime; Employee: Integer);
  var
    BonusPerc: Double;
    // RV045.1.
    // When there are no Contract Group settings for TFT,
    // then get TFT-settings for hourtype used for absence hours,
    // else the settings that were determined earlier are used.
    procedure DetermineTimeForTimeSettings;
    begin
      if not ContractGroupTFTExists then
      begin
        with qryUPSDeleteAbsenceHours do
        begin
          if FieldByName('OVERTIME_YN').AsString = CHECKEDVALUE then
          begin
            CurrentTFT :=
              (FieldByName('TIME_FOR_TIME_YN').AsString = CHECKEDVALUE);
            CurrentBonusInMoney :=
              (FieldByName('BONUS_IN_MONEY_YN').AsString = CHECKEDVALUE);
{$IFDEF DEBUG}
  WDebugLog('- HT CurrentTFT=' + BoolToYN(CurrentTFT) + ' CurrentBIM=' +
    BoolToYN(CurrentBonusInMoney));
{$ENDIF}
          end
          else
          begin
            CurrentTFT := False;
            CurrentBonusInMoney := False;
          end;
        end;
      end
{$IFDEF DEBUG}
      else
        WDebugLog('- CG CurrentTFT=' + BoolToYN(CurrentTFT) + ' CurrentBIM=' +
          BoolToYN(CurrentBonusInMoney));
{$ENDIF}
    end; // procedure DetermineTimeForTimeSettings;
  begin
    with qryUPSDeleteAbsenceHours do // RV045.1. Query changed.
    begin
      Close;
      ParamByName('FROMDATE').AsDateTime := Trunc(AFromDate);
      ParamByName('TODATE').AsDateTime := Trunc(ToDate);
      ParamByName('EMPNO').AsInteger := Employee;
      ParamByName('MANUAL').AsString := UNCHECKEDVALUE;
      Open;
    end;
    while not qryUPSDeleteAbsenceHours.Eof do
    begin
      DetermineTimeForTimeSettings; // RV045.1.
      // Overtime must be substracted from earned time for time
      if CurrentTFT  then
      begin
        TotEarnedMin := qryUPSDeleteAbsenceHours.FieldByName('SALARY_MINUTE').AsInteger;
        // RV045.1. Field BONUS_PERCENTAGE added to qryUPSDeleleteAbsenceHours.
        //          No need to use qryUPSBonusPerc anymore.
        BonusPerc := qryUPSDeleteAbsenceHours.FieldByName('BONUS_PERCENTAGE').AsFloat;
        // RV045.1. BEGIN
        //          Get BonusPercentage from qryUPSDeleteAbsenceHours.
        if (BonusPerc > 0) and
          (not CurrentBonusInMoney) then
          TotEarnedMin := Round(TotEarnedMin + TotEarnedMin * BonusPerc / 100);
        // RV045.1. END
        // MRA:21-SEP-2009 RV033.5. Bugfix. CurrentIDCard is not filled!
        // Use AIDCard instead!
        if TotEarnedMin > 0 then
        begin
{$IFDEF DEBUG}
  WDebugLog('- DelAbsHrs' +
    ' TotEarnedMin=' + Format('%.f', [(-1) * TotEarnedMin]));
{$ENDIF}
          UpdateAbsenceTotal('EARNED_TFT_MINUTE',
            qryUPSDeleteAbsenceHours.FieldByName('SALARY_DATE').AsDateTime,
            {CurrentIDCard}
            AIDCard.EmployeeCode, (-1) * TotEarnedMin, False);
        end;
      end;
      qryUPSDeleteAbsenceHours.Next;
    end;
    qryUPSDeleteAbsenceHours.Close;
  end;
  // RV056.1. Added From-To-Date.
  procedure DeleteProduction(
    AFromDate, AToDate: TDateTime;
    {CurrentBookingDay: TDateTime;}
    AEmployeeNumber: Integer);
  begin
{$IFDEF DEBUG}
begin
  WDebugLog('> Delete production');
  WDebugLog('> Production from date=' + DateToStr(AFromDate) + ' to date=' +
    DateToStr(AToDate) + ' Empl=' + IntToStr(AEmployeeNumber));
end;
{$ENDIF}
    // RV056.1. Query changed, so it uses a from-to-date.
    with qryUPSDeleteProduction do
    begin
//      ParamByName('PRODDATE').AsDateTime := Trunc(CurrentBookingDay);
      ParamByName('FROMDATE').AsDateTime := Trunc(AFromDate);
      ParamByName('TODATE').AsDateTime := Trunc(AToDate);
      ParamByName('EMPNO').AsInteger := AEmployeeNumber;
      ParamByName('MANUAL').AsString := UNCHECKEDVALUE;
      ExecSQL;
    end;
  end;
  procedure DeleteSalary(
    AFromDate, ToDate: TDateTime; Employee: Integer;
    AIDCard: TScannedIDCard);
  begin
    with qryUPSDeleteSalary do
    begin
      ParamByName('FROMDATE').AsDateTime := Trunc(AFromDate);
      ParamByName('TODATE').AsDateTime := Trunc(ToDate);
      ParamByName('EMPNO').AsInteger := Employee;
      ParamByName('MANUAL').AsString := UNCHECKEDVALUE;
      ExecSQL;
    end;
{$IFDEF DEBUG}
begin
  WDebugLog('> Delete salary');
  WDebugLog('> Salary from date=' + DateToStr(AFromDate) + ' to date=' +
    DateToStr(ToDate) + ' Empl=' + IntToStr(Employee));
end;
{$ENDIF}
    // MR:07-11-2003 Changes because 'CurrentIDCard' is probably not filled.
    // Now 'AIDCard' is used and also a parameter.
//    DeleteProdHourForAllTypes(AQuery, CurrentIDCard, FromDate, ToDate,
//      UNCHECKEDVALUE);
    GlobalDM.DeleteProdHourForAllTypes(AIDCard, AFromDate, ToDate,
      UNCHECKEDVALUE);
    // RV056.1.
    if ARecalcProdHours then
      DeleteProduction(AFromDate, ToDate, AIDCard.EmployeeCode);
  end;
  // 20013489
  // It is possible a scan must be booked on a different date, indicated
  // by ShiftDate (which can be an overnight-shift).
  procedure ShiftDateCheckOnProdDates(AIDCard: TScannedIDCard;
    var AProdDate1, AProdDate2, AProdDate3, AProdDate4: TDateTime);
  begin
    // 20013489
    // Always store production on start of shift.
//    if AIDCard.OvernightShift then
    begin
      if AProdDate1 <> NullDate then
        if AIDCard.ShiftDate <> AProdDate1 then
          AProdDate1 := AIDCard.ShiftDate;
      if AProdDate2 <> NullDate then
        if AIDCard.ShiftDate <> AProdDate2 then
          AProdDate2 := AIDCard.ShiftDate;
      if AProdDate3 <> NullDate then
        if AIDCard.ShiftDate <> AProdDate3 then
          AProdDate3 := AIDCard.ShiftDate;
      if AProdDate4 <> NullDate then
        if AIDCard.ShiftDate <> AProdDate4 then
          AProdDate4 := AIDCard.ShiftDate;
    end;
  end; // ShiftDateCheckOnProdDates
  // PIM-49
  // Extra check to see if a scan-date is equal to a production-date.
  function ProdDateCheck(ADateIn, ADateOut: TDateTime): Boolean;
  begin
    Result := False;
    if
      (
        ((AProdDate1 <> NullDate) and (Trunc(ADateIn) = AProdDate1))
        or
        ((AProdDate2 <> NullDate) and (Trunc(ADateIn) = AProdDate2))
        or
        ((AProdDate3 <> NullDate) and (Trunc(ADateIn) = AProdDate3))
        or
        ((AProdDate4 <> NullDate) and (Trunc(ADateIn) = AProdDate4))
        or
        ((AProdDate1 <> NullDate) and (Trunc(ADateOut) = AProdDate1))
        or
        ((AProdDate2 <> NullDate) and (Trunc(ADateOut) = AProdDate2))
        or
        ((AProdDate3 <> NullDate) and (Trunc(ADateOut) = AProdDate3))
        or
        ((AProdDate4 <> NullDate) and (Trunc(ADateOut) = AProdDate4))
      )
      then
      Result := True;
  end; // ProdDateCheck
begin
{$IFDEF DEBUG}
  WDebugLog('Start UnprocessProcessScans');
{$ENDIF}

  if not SystemDM.UseShiftDateSystem then
  begin
    if AFromProcessPlannedAbsence then
    begin
      StartDate := Trunc(AIDCard.DateIn);
      EndDate := Trunc(AIDCard.DateOut);
    end
    else
      AProdMinClass.GetShiftDay(AIDCard, StartDate, EndDate);
    // Calculate the employe booking day
    // This variable is not used!
//    BookingDay := Trunc(StartDate);
  end // not UseShiftDateSystem
  else
  begin // UseShiftDateSystem
    // 20013489.
    // Always recalc production hours, to ensure it is OK with overnight-scans.
    // Always set DeleteAction to False, otherwise no salary is calculated?
    //  DeleteAction := False;
    ARecalcProdHours := True;

    // 20013489.
    if not AFromProcessPlannedAbsence then
    begin
      AProdMinClass.GetShiftDay(AIDCard, StartDate, EndDate);
      // 20013489.
      // When AManual = False, then it is about Scans or Process Planned Absence.
      // When AManual = True, then it is about manual hours, not a real scan or
      // Process Planned Absence: Then the DateIn and DateOut is not about a real
      // scan. This means the manual hours must always be booked on the same day,
      // never on a previous day. Also the booking-period must be based on that day,
      // not on previous day/week/period/month.
      if AManual then
      begin
        StartDate := Trunc(AIDCard.DateIn) + Frac(StartDate);
        EndDate := Trunc(AIDCard.DateIn) + Frac(EndDate);
        if StartDate > EndDate then
          EndDate := EndDate + 1;
      end;
{$IFDEF DEBUG}
  WDebugLog('GetShiftDay: ' + DateTimeToStr(StartDate) + ' - ' +
    DateTimeToStr(EndDate));
{$ENDIF}
    end;

    // 20013489.
    if not AFromProcessPlannedAbsence then
      ShiftDateCheckOnProdDates(AIDCard, AProdDate1, AProdDate2, AProdDate3,
        AProdDate4);
  end; // if UseShiftDateSystem

  // 20013489.
  // ProdDate stores the date for the production-record.
  // Originally this was the same as the dates from the scan,
  // but with overnight-shift-system it can be different!
  // ProdDate1 = Based on New-scan-date-out
  // ProdDate2 = Based on New-scan-date-in
  // ProdDate3 = Based on Old-scan-date-out
  // ProdDate4 = Based on Old-scan-date-in

  ProdDate[1] := AProdDate1;
  ProdDate[2] := AProdDate2;
  ProdDate[3] := AProdDate3;
  ProdDate[4] := AProdDate4;

// IMPORTANT:
// When this procedure is called from ProcessPlannedAbsence, then
// AIDCard is not really filled with data based on a scan!
// For example DateIn and DateOut are not real scan-timestamps,
// and also Shift is not correct!

{$IFDEF DEBUG}
begin
  WDebugLog('AIDCard');
  WDebugLog('- EMP=' + IntToStr(AIDCard.EmployeeCode) +
    ' PL=' + AIDCard.PlantCode +
    ' SHFT=' + IntToStr(AIDCard.ShiftNumber) +
    ' WC=' + AIDCard.WorkSpotCode +
    ' JC=' + AIDCard.JobCode +
    ' DEPT=' + AIDCard.DepartmentCode
    );
  WDebugLog(
    '- DIN=' + DateTimeToStr(AIDCard.DateIn) +
    ' DOUT=' + DateTimeToStr(AIDCard.DateOut) +
    ' SHIFTDATE=' + DateToStr(AIDCard.ShiftDate)
    );
  WDebugLog('- Params' +
    ' AFromDate=' + DateTimeToStr(AFromDate) +
    ' AProdDate1=' + DateTimeToStr(AProdDate1) +
    ' AProdDate2=' + DateTimeToStr(AProdDate2) +
    ' AProdDate3=' + DateTimeToStr(AProdDate3) +
    ' AProdDate4=' + DateTimeToStr(AProdDate4) +
    ' AProcessSalary=' + Bool2Str(AProcessSalary) +
    ' ADeleteAction=' + Bool2Str(ADeleteAction) +
    ' ARecalcProdHours=' + Bool2Str(ARecalcProdHours) +
    ' AFromProcessPlannedAbsence=' + Bool2Str(AFromProcessPlannedAbsence) +
    ' AManual=' + Bool2Str(AManual)
    );
end;
{$ENDIF}

  // 20013271 When called from Process Planned Absence, do
  // NOT call 'getshiftday', because the dateIn is not a real scan!
  // Otherwise it is possible a wrong bookingday/overtimeperiod is calculated.

  // Extra problem:
  // Because the real shift is not filled when called from Process Planned
  // Absence, it does not know the correct bookingday.

  // Booking date
  // Related to 20013489.
  // Always the complete overtime-period must be calculated!
  // IMPORTANT:
  // The StartDate and EndDate must be filled before 'ComputerOverTimePeriod'
  // is called. Otherwise this function can not know for what period it must
  // calculate this.
  if SystemDM.UseShiftDateSystem then
    if AFromProcessPlannedAbsence then
    begin
      StartDate := Trunc(AIDCard.DateIn);
      EndDate := Trunc(AIDCard.DateOut);
    end;
//  else
  // Related to 20013489.
  // This is WRONG!!!
  // This procedure UnProcessProcessScans will recalculate all hours for
  // all scans in the overtime-period. This means the AIDCard here
  // is only about 1 scan that was changed, not about all scans that should
  // be processed. So the shift can be different from other scans!

  // THIS IS DONE EARLIER!
//    AProdMinClass.GetShiftDay(AIDCard, StartDate, EndDate);

  // Calculate the employe booking day
  // Related to 20013489.
  // This will be the bookingday for only 1 scan, because all scans
  // in overtime-period will be recalculated they can all have different
  // bookingdays. So, why determine it here?
  // BookingDay is not needed here!
//  BookingDay := Trunc(StartDate);
  // Determine Over Time Period
  GlobalDM.ComputeOvertimePeriod(StartDate, EndDate, AIDCard);

{$IFDEF DEBUG}
  WDebugLog('- OvertimePeriod' +
    ' StartDate=' + DateTimeToStr(StartDate) +
    ' EndDate=' + DateTimeToStr(EndDate)
    );
{$ENDIF}

  // Related to 20013489.
  // Always process salary?
//  ProcessSalary := True;

  if AProcessSalary then
  begin
    GetTFT_Bonus(AIDCard.EmployeeCode);
    if not SystemDM.UseShiftDateSystem then
    begin
      if (AFromDate > StartDate) and (AFromDate < EndDate) then
      begin
        DeleteAbsenceHours(AFromDate, EndDate,
          AIDCard.EmployeeCode);
        DeleteSalary(AFromDate, EndDate,
          AIDCard.EmployeeCode, AIDCard);
      end
      else
      begin
        DeleteAbsenceHours(StartDate, EndDate,
          AIDCard.EmployeeCode);
        DeleteSalary(StartDate, EndDate,
          AIDCard.EmployeeCode, AIDCard);
      end;
    end // if not UseShiftDateSystem
    else
    begin // if UseShiftDateSystem
      // Related to 20013489
      // When called from Process Planned Absence, then the complete overtime
      // period must be recalculated, instead of starting from the 'AFromDate',
      // which can be a wrong date when only 1 day was selected.

      // Related to 20013489
      // ALWAYS process all records within the overtime-period???
{    if (
      (AFromDate > StartDate) and (AFromDate < EndDate)
      and (not AFromProcessPlannedAbsence)
      ) then
    begin
      DeleteAbsenceHours(AFromDate, EndDate,
        AIDCard.EmployeeCode);
      DeleteSalary(AFromDate, EndDate,
        AIDCard.EmployeeCode, AIDCard);
      // 20013489.
      // Production Hours must always be recalculated.
//      DeleteProduction(AFromDate, EndDate,
//        AIDCard.EmployeeCode);
    end
    else }
      begin
        DeleteAbsenceHours(StartDate, EndDate,
          AIDCard.EmployeeCode);
        DeleteSalary(StartDate, EndDate,
          AIDCard.EmployeeCode, AIDCard);
      // 20013489.
      // Production Hours must always be recalculated?
//      DeleteProduction(StartDate, EndDate,
//        AIDCard.EmployeeCode);
      end;
    end; // if UseShiftDateSystem
  end; // if ProcessSalary

  // RV056.1. Only recalc prodhours when 'ProdDate' is filled.
  if not ARecalcProdHours then
  begin
    for I := 1 to 4 do
      if ProdDate[I] <> NullDate then
      begin
        DeleteProduction(ProdDate[I], ProdDate[I],
          AIDCard.EmployeeCode);
      end;
  end;

  // All data ready. Start write in Database
  RecordsForUpdate := Tlist.Create;
  // Select all records from TIMEREGSCANNING that result booking date for
  // Salary hours in OverTime period:
  // StartDateTime - 1day and EndDateTime + 1day

  // MR:05-09-2003
  if AProcessSalary then
  begin
    // 20013489 Filter on SHIFT_DATE or not
//    if not AFromProcessPlannedAbsence then
    if not SystemDM.UseShiftDateSystem then
    begin
      WhereSelect :=
        ' WHERE ' +
        '  T.DATETIME_IN >= :DSTART AND ' +
        '  T.DATETIME_IN <= :DEND AND ';
    end // not UseShiftDateSystem
    else
    begin // UseShiftDateSystem
      WhereSelect :=
        ' WHERE ' +
        '   T.SHIFT_DATE >= :DSTART AND ' +
        '   T.SHIFT_DATE <  :DEND AND ';
    end; // if UseShiftDateSystem
{
    else
      WhereSelect :=
        ' WHERE ' +
        '  T.DATETIME_IN >= :DSTART AND ' +
        '  T.DATETIME_IN <= :DEND AND ';
}
  end
  else
  begin
    WhereSelect :=
      ' WHERE ' +
      '(((T.DATETIME_IN >= :FROMDATE1) AND  (T.DATETIME_IN < :TODATE1)) OR ' +
      ' ((T.DATETIME_IN >= :FROMDATE2) AND  (T.DATETIME_IN < :TODATE2)) OR ' +
      ' ((T.DATETIME_IN >= :FROMDATE3) AND  (T.DATETIME_IN < :TODATE3)) OR ' +
      ' ((T.DATETIME_IN >= :FROMDATE4) AND  (T.DATETIME_IN < :TODATE4)) OR ' +
      ' ((T.DATETIME_OUT >= :FROMDATE1) AND (T.DATETIME_OUT < :TODATE1)) OR ' +
      ' ((T.DATETIME_OUT >= :FROMDATE2) AND (T.DATETIME_OUT < :TODATE2)) OR ' +
      ' ((T.DATETIME_OUT >= :FROMDATE3) AND (T.DATETIME_OUT < :TODATE3)) OR ' +
      ' ((T.DATETIME_OUT >= :FROMDATE4) AND (T.DATETIME_OUT < :TODATE4))) AND ';
  end;

  // MR:03-02-2005 Also get department from workspot!
  // TD-23386 Also get hourtype on shift: SHIFT_HOURTYPE_NUMBER
  // TD-23386 Related to this todo: Also get workspot-hourtype here.
  qryWork.Close;
  qryWork.SQL.Clear;
  qryWork.SQL.Add(
    'SELECT ' +
    '  T.DATETIME_IN, T.EMPLOYEE_NUMBER, T.PLANT_CODE, ' +
    '  T.WORKSPOT_CODE, T.JOB_CODE, T.SHIFT_NUMBER, ' +
    '  T.DATETIME_OUT, T.PROCESSED_YN, T.IDCARD_IN, ' +
    '  T.IDCARD_OUT, W.DEPARTMENT_CODE, ' +
    '  T.SHIFT_DATE, ' + // 20013489
    '  S.HOURTYPE_NUMBER SHIFT_HOURTYPE_NUMBER, ' +
    '  CASE ' +
    '    WHEN S.HOURTYPE_NUMBER IS NOT NULL THEN ' +
    '      (SELECT H.IGNORE_FOR_OVERTIME_YN FROM HOURTYPE H ' +
    '       WHERE H.HOURTYPE_NUMBER = S.HOURTYPE_NUMBER) ' +
    '    ELSE ' +
    '      '''' ' +
    '  END SHIFT_IGNORE_FOR_OVERTIME_YN, ' +
    '  W.HOURTYPE_NUMBER WORKSPOT_HOURTYPE_NUMBER, ' +
    '  CASE ' +
    '    WHEN W.HOURTYPE_NUMBER IS NOT NULL THEN ' +
    '      (SELECT H.IGNORE_FOR_OVERTIME_YN FROM HOURTYPE H ' +
    '       WHERE H.HOURTYPE_NUMBER = W.HOURTYPE_NUMBER) ' +
    '    ELSE ' +
    '      '''' ' +
    '  END IGNORE_FOR_OVERTIME_YN ' +
    'FROM ' +
    '  TIMEREGSCANNING T INNER JOIN WORKSPOT W ON ' +
    '    T.PLANT_CODE = W.PLANT_CODE AND ' +
    '    T.WORKSPOT_CODE = W.WORKSPOT_CODE ' +
    '  INNER JOIN SHIFT S ON ' +
    '    T.PLANT_CODE = S.PLANT_CODE AND ' +
    '    T.SHIFT_NUMBER = S.SHIFT_NUMBER ' +
    WhereSelect +
    '  T.EMPLOYEE_NUMBER = :EMPNO AND ' +
    '  T.PROCESSED_YN = :PROCESSED ' +
    'ORDER BY ' +
    '  T.DATETIME_IN '
    );
  if AProcessSalary then
  begin
{$IFDEF DEBUG}
  WDebugLog('- Test on Start/End');
{$ENDIF}
    if not SystemDM.UseShiftDateSystem then
    begin
      qryWork.ParamByName('DSTART').AsDateTime := StartDate - 1;
      qryWork.ParamByName('DEND').AsDateTime := EndDate + 1;
    end // if not UseShiftDateSystem
    else
    // 20013489 Compare with SHIFT_DATE
//    if not AFromProcessPlannedAbsence then
    begin // if UseShiftDateSystem
      qryWork.ParamByName('DSTART').AsDate := StartDate;
      qryWork.ParamByName('DEND').AsDate := Trunc(EndDate) + 1;
    end // if UseShiftDateSystem
{
    else
    begin
      qryWork.ParamByName('DSTART').AsDateTime := StartDate - 1;
      qryWork.ParamByName('DEND').AsDateTime := EndDate + 1;
    end;
}
  end
  else
  begin
{$IFDEF DEBUG}
  WDebugLog('- Test on ProdDates');
{$ENDIF}
    qryWork.ParamByName('FROMDATE1').AsDate := Trunc(AProdDate1);
    qryWork.ParamByName('FROMDATE2').AsDate := Trunc(AProdDate2);
    qryWork.ParamByName('FROMDATE3').AsDate := Trunc(AProdDate3);
    qryWork.ParamByName('FROMDATE4').AsDate := Trunc(AProdDate4);
    qryWork.ParamByName('TODATE1').AsDate := Trunc(AProdDate1) + 1;
    qryWork.ParamByName('TODATE2').AsDate := Trunc(AProdDate2) + 1;
    qryWork.ParamByName('TODATE3').AsDate := Trunc(AProdDate3) + 1;
    qryWork.ParamByName('TODATE4').AsDate := Trunc(AProdDate4) + 1;
  end;
  qryWork.ParamByName('EMPNO').AsInteger := AIDCard.EmployeeCode;
  qryWork.ParamByName('PROCESSED').AsString := CHECKEDVALUE;
  qryWork.Open;
  qryWork.First;
{$IFDEF DEBUG}
  WDebugLog('- Number of Time-recording-scans found: ' + IntToStr(qryWork.RecordCount)
    );
{$ENDIF}
// MR:05-09-2003 NEW while...
  // 20015346 Round scans to whole minutes (with RoundTime-function) before
  // hours are calculated.
  DateInFirstScan := NullDate; // GLOB3-81
  while not qryWork.Eof do
  begin
{$IFDEF DEBUG}
  WDebugLog(
    ' - Scan.DateIn=' + DateTimeToStr(RoundTime(qryWork.FieldByName('DATETIME_IN').AsDateTime,1)) +
    '  DateOut=' + DateTimeToStr(RoundTime(qryWork.FieldByName('DATETIME_OUT').AsDateTime,1))
    );
{$ENDIF}
    if SystemDM.UseShiftDateSystem then
    begin
      CurrentProdDate1 := NullDate;
      CurrentProdDate2 := NullDate;
      CurrentProdDate3 := NullDate;
      CurrentProdDate4 := NullDate;
    end;
    // SO-20013271
    // One part of scan must fall between startdate and enddate,
    // if not then skip the scan.
    // 20013489 We already have the records for SHIFT_DATE based on
    //          overtime-period of contractgroup of employee.
    //          So not need to check this!
    //          Only do this when 'not ProcessSalary' ?
{$IFDEF DEBUG}
  SalDateFound := False;
  ProdDateFound := False;
{$ENDIF}
    if (SystemDM.UseShiftDateSystem and (not AProcessSalary))
      or
      (not SystemDM.UseShiftDateSystem) then
    begin
      // PIM-49
      SalDateFound :=
        (
          (RoundTime(qryWork.FieldByName('DATETIME_IN').AsDateTime,1) >= StartDate) and
          (RoundTime(qryWork.FieldByName('DATETIME_IN').AsDateTime,1) <= EndDate)
         or
          (RoundTime(qryWork.FieldByName('DATETIME_OUT').AsDateTime,1) >= StartDate) and
          (RoundTime(qryWork.FieldByName('DATETIME_OUT').AsDateTime,1) <= EndDate)
        );
      ProdDateFound :=
        ProdDateCheck(RoundTime(qryWork.FieldByName('DATETIME_IN').AsDateTime,1),
          RoundTime(qryWork.FieldByName('DATETIME_OUT').AsDateTime,1));
      if not
        (
          SalDateFound
         or
          ProdDateFound
        ) then
        begin
{$IFDEF DEBUG}
  WDebugLog('-> Skipped');
{$ENDIF}
          qryWork.Next;
          Continue;
        end;
    end; // if (not UseShiftDateSystem) or (not ProcessSalary)
{$IFDEF DEBUG}
  WDebugLog('-> ProdDateFound: ' + BoolToStr(ProdDateFound) +
    ' SalDateFound:' + BoolToStr(SalDateFound)
    );
{$ENDIF}
    AddToProdListOut := False;
    AddToProdListIn := False;
    AddToSalList := False;
    if DateInFirstScan = NullDate then // GLOB3-81
      DateInFirstScan := qryWork.FieldByName('DATETIME_IN').AsDateTime;
    EmptyIDCard(CurrentIDCard);
    GlobalDM.PopulateIDCard(CurrentIDCard, qryWork);
    CurrentIDCard.DateInFirstScan := DateInFirstScan;
    // TD-23386 We already assign DepartmentCode in PopulateIDCard
{
    // MR:03-02-2005 Get department from workspot!
    CurrentIDCard.DepartmentCode :=
      qryWork.FieldByName('DEPARTMENT_CODE').AsString;
}
    // TD-23416 Set default value for CurrentBookingDay.
//    CurrentBookingDay := Trunc(CurrentIDCard.DateIn);
    if AProcessSalary then
    begin
      // 20013271
      // NOTE: If GetShiftDay cannot find timeblocks for the same day of the
      //       scan-date-in, then it can give a booking for the wrong day.
      AProdMinClass.GetShiftDay(CurrentIDCard, CurrentStart, CurrentEnd);
      // Calculate the employee booking day
      CurrentBookingDay := Trunc(CurrentStart);
      if SystemDM.UseShiftDateSystem then
      begin
        // 20013489
        CurrentIDCard.ShiftDate := CurrentBookingDay;

        // 20013489 When CurrentBookingDay is not equal to Shiftdate of scan
        //          then skip this record.
        //          When called from ProcessPlannedAbsence, then AIDCard
        //          ShiftDate will be empty, because this is not filled based
        //          on a real scan.
        // 20013489 Do NOT do this, a filtering is already done on SHIFT_DATE
        //          for overtime-period.
{
        if not AFromProcessPlannedAbsence then
          if AIDCard.ShiftDate <> CurrentIDCard.ShiftDate then
          begin
            qryWork.Next;
            Continue;
          end;
}
      end // if UseShiftDateSystem
      else
        CurrentIDCard.ShiftDate := Trunc(CurrentIDCard.DateIn);

      // RV056.1. Fill Proddates here, so it will result in a recalculation
      //          of the productionhours.
      // TD-26818 This must NOT be done, or it will result in double prod. hrs.
(*
      if ARecalcProdHours then
      begin
        ProdDate[1] := Trunc(CurrentIDCard.DateIn);
        ProdDate[2] := Trunc(CurrentIDCard.DateOut);
        if SystemDM.UseShiftDateSystem then
          ShiftDateCheckOnProdDates(CurrentIDCard, ProdDate[1],
            ProdDate[2], ProdDate[1], ProdDate[2]);
      end;
*)
      GlobalDM.ComputeOvertimePeriod(CurrentStart, CurrentEnd,
        CurrentIDCard);
{$IFDEF DEBUG}
  WDebugLog('- DateIn=' + DateTimeToStr(CurrentIDCard.DateIn) +
    ' DateOut=' + DateTimeToStr(CurrentIDCard.DateOut) +
    ' CurrentBookingDay=' + DateToStr(CurrentBookingDay) +
    ' CurrentStart=' + DateTimeToStr(CurrentStart) +
    ' CurrentEnd=' + DateTimeToStr(CurrentEnd) +
    ' FromDate=' + DateTimeToStr(AFromDate)
    );
{$ENDIF}
      // if same overtimeperiod and ...
      if (not SystemDM.UseShiftDateSystem) then
        GoOn := (Trunc(AFromDate) >= Trunc(CurrentStart)) and
          (Trunc(AFromDate) <= Trunc(CurrentEnd)) and
          (CurrentBookingDay >= Trunc(AFromDate))
      else
        GoOn := (Trunc(AFromDate) >= Trunc(CurrentStart)) and
          (Trunc(AFromDate) <= Trunc(CurrentEnd)) and
          (CurrentBookingDay >= Trunc(CurrentIDCard.ShiftDate));
{
      // if same overtimeperiod and ...
      if (Trunc(AFromDate) >= Trunc(CurrentStart)) and
        (Trunc(AFromDate) <= Trunc(CurrentEnd)) and
        (
          (CurrentBookingDay >= Trunc(AFromDate)) or
          (
            (
              SystemDM.UseShiftDateSystem and
                (CurrentBookingDay >= Trunc(CurrentIDCard.ShiftDate))
            )
            or
            (
              not SystemDM.UseShiftDateSystem
            )
          ) // 20013489
        ) then
}
      if GoOn then
      begin
        // Related to 20013489:
        // Also check for 'ARecalcProdHours' that is TRUE when called
        // from ProcessPlannedAbsence, in which case we always want to
        // recalculate.
        // NOTE: When called from ProcessPlannedAbsence, then AIDCard
        //       is not referring to a REAL TimeRecording-record!
        if (not SystemDM.UseShiftDateSystem) then
          GoOn := not (ADeleteAction and
            (CurrentIDCard.DateIn = AIDCard.DateIn) and
            (CurrentIDCard.EmployeeCode = AIDCard.EmployeeCode))
        else
          GoOn := not (ADeleteAction and
            (
              (CurrentIDCard.DateIn = AIDCard.DateIn)
              or
              (ARecalcProdHours)
            ) and // 20013489
            (CurrentIDCard.EmployeeCode = AIDCard.EmployeeCode));

{
        if not (ADeleteAction and
          (
            (CurrentIDCard.DateIn = AIDCard.DateIn)
            or
            (
              (SystemDM.UseShiftDateSystem and ARecalcProdHours) // 20013489
            )
            or
            (
              (not SystemDM.UseShiftDateSystem)
            )
          ) and
          (CurrentIDCard.EmployeeCode = AIDCard.EmployeeCode)) then
}       if GoOn then
        begin
          AddToSalList := True;
{$IFDEF DEBUG}
  WDebugLog(' -> AddToSalList=True');
{$ENDIF}
          // RV056.1. Fill Proddates here, so it will result in a recalculation
          //          of the productionhours.
          // RV057.1. Moved to here!
          if ARecalcProdHours then
          begin
            ProdDate[1] := Trunc(CurrentIDCard.DateIn);
            ProdDate[2] := Trunc(CurrentIDCard.DateOut);
            if SystemDM.UseShiftDateSystem then
              ShiftDateCheckOnProdDates(CurrentIDCard, ProdDate[1],
                ProdDate[2], ProdDate[1], ProdDate[2]);
          end;
        end;
      end;
    end; // if ProcessSalary then
    // Determine if add to Production
    // 20013489 Overnight-Shift-System
    // Only comparing dates of scans is not enough to know if there is an
    // overnight-shift involved.
    // The shift itself indicates if scans must be booked on shift-date or not.
    CurrentProdDate1 := Trunc(CurrentIDCard.DateOut);
    CurrentProdDate2 := Trunc(CurrentIDCard.DateIn);
    if SystemDM.UseShiftDateSystem then
      ShiftDateCheckOnProdDates(CurrentIDCard, CurrentProdDate1,
        CurrentProdDate2, CurrentProdDate3, CurrentProdDate4)
    else
    begin
      // PIM-49
      //   Scan SA 14:13-2:30 falls in end of previous period, but part after
      //   midnight falls in next period!
      if (CurrentIDCard.DateIn <= CurrentEnd) and
        (CurrentIDCard.DateOut > CurrentEnd) then
      begin
{$IFDEF DEBUG}
  WLog(' -> Exception: Part of scan falls in next period!');
{$ENDIF}
        // Part of the scan falls in next period!
        if ARecalcProdHours then
          ProdDate[2] := CurrentIDCard.DateOut;
      end;
    end;
    // Determine if add to Production
    for I := 1 to 4 do
      if (CurrentProdDate1 <> NullDate) and (ProdDate[I] <> NullDate) then
        if (Trunc(CurrentProdDate1) = Trunc(ProdDate[I])) then
        begin
          if not (ADeleteAction and
            (CurrentIDCard.DateIn = AIDCard.DateIn) and
            (CurrentIDCard.EmployeeCode = AIDCard.EmployeeCode)) then
           AddToProdListOut := True;
        end;
    // If Overnight then determine if add to Production
    if (Trunc(CurrentIDCard.DateIn) <> Trunc(CurrentIDCard.DateOut)) then
    begin
      for I := 1 to 4 do
        if (CurrentProdDate2 <> NullDate) and (ProdDate[I] <> NullDate) then
          if (Trunc(CurrentProdDate2) = Trunc(ProdDate[I])) then
          begin
            if not (ADeleteAction and
              (CurrentIDCard.DateIn = AIDCard.DateIn) and
              (CurrentIDCard.EmployeeCode = AIDCard.EmployeeCode)) then
              AddToProdListIn := True;
          end;
      end;
{
    for I := 1 to 4 do
      if (
          (Trunc(CurrentIDCard.DateOut) = Trunc(ProdDate[I]))
          or
          (CurrentIDCard.OvernightShift and (ProdDate[I] <> NullDate))
          )  then
      begin
        if not (DeleteAction and
          (CurrentIDCard.DateIn = AIDCard.DateIn) and
          (CurrentIDCard.EmployeeCode = AIDCard.EmployeeCode)) then
         AddToProdListOut := True;
      end;
    // If Overnight then determine if add to Production
    if (Trunc(CurrentIDCard.DateIn) <> Trunc(CurrentIDCard.DateOut)) then
    begin
      for I := 1 to 4 do
        if (
          (Trunc(CurrentIDCard.DateIn) = Trunc(ProdDate[I]))
          or
          (CurrentIDCard.OvernightShift and (ProdDate[I] <> NullDate))
          )  then
        begin
          if not (DeleteAction and
            (CurrentIDCard.DateIn = AIDCard.DateIn) and
            (CurrentIDCard.EmployeeCode = AIDCard.EmployeeCode)) then
            AddToProdListIn := True;
        end;
    end;
}
{$IFDEF DEBUG}
  WDebugLog('- AddToProdListOut=' + BoolToStr(AddToProdListOut) +
    ' AddToProdListIn=' + BoolToStr(AddToProdListIn) +
    ' AddToSalList=' + BoolToStr(AddToSalList)
    );
{$ENDIF}
    if SystemDM.UseShiftDateSystem then
    begin
      // 20013489 Overnight-Shift-System
      // When prod. should be recalculated, also recalculate salary!
      // Otherwise salary is missing!
      if ADeleteAction then
      begin
        if AddToProdListOut or AddToProdListIn then
          AddToSalList := True;
      end;
    end; // if UseShiftDateSystem
    // Add Production and/or Salary
    if AddToProdListOut or AddToProdListIn or AddToSalList then
    begin
{$IFDEF DEBUG}
  WDebugLog(' -> Bookmarked.');
{$ENDIF}
      new(ABookmarkRecord);
      ABookmarkRecord.Bookmark := qryWork.GetBookmark;
      ABookmarkRecord.AddToSalList := AddToSalList;
      ABookmarkRecord.AddToProdListOut := AddToProdListOut;
      ABookmarkRecord.AddToProdListIn := AddToProdListIn;
      RecordsForUpdate.Add(ABookmarkRecord);
    end;
    // RV057.1. Reset ProdDate-values here.
    if ARecalcProdHours then
    begin
      ProdDate[1] := NullDate;
      ProdDate[2] := NullDate;
    end;
    qryWork.Next;
  end; {while}

// MR:05-09-2003 NEW
  // Recalculate all deleted records from Salary and Production Table
  // Compute Salary hours for deleted Records
  { Processed := False; }
  qryWork.First;
  for index := 0 to RecordsForUpdate.Count - 1 do
  begin
    LastScanRecordForDay := False;
    BookingDayNext := 0;
    // MR:29-07-2004
    if Index = RecordsForUpdate.Count - 1 then
      LastScanRecordForDay := True
    else
    begin
      ABookmarkRecord := RecordsForUpdate.Items[index + 1];
      qryWork.GotoBookmark(ABookmarkRecord.Bookmark);
      // Don't free the bookmark here.
      EmptyIDCard(CurrentIDCard);
      GlobalDM.PopulateIDCard(CurrentIDCard, qryWork);
      CurrentIDCard.DateInFirstScan := DateInFirstScan; // GLOB3-81
      // TD-23386 We already assign DepartmentCode in PopulateIDCard
{
      // MR:03-02-2005 Get department from workspot!
      CurrentIDCard.DepartmentCode :=
        qryWork.FieldByName('DEPARTMENT_CODE').AsString;
}
      // Get Bookingday of Next Scan-record
      AProdMinClass.GetShiftDay(CurrentIDCard, StartDate, EndDate);
      BookingDayNext := StartDate;
    end;
    ABookmarkRecord := RecordsForUpdate.Items[index];
    qryWork.GotoBookmark(ABookmarkRecord.Bookmark);
    qryWork.FreeBookmark(ABookmarkRecord.Bookmark);
    EmptyIDCard(CurrentIDCard);
    GlobalDM.PopulateIDCard(CurrentIDCard, qryWork);
    CurrentIDCard.DateInFirstScan := DateInFirstScan; // GLOB3-81
    // TD-23386 We already assign DepartmentCode in PopulateIDCard
{
    // MR:03-02-2005 Get department from workspot!
    CurrentIDCard.DepartmentCode :=
      qryWork.FieldByName('DEPARTMENT_CODE').AsString;
}
    if not LastScanRecordForDay then
    begin
      // Get Bookingday of Current Scan-record
      AProdMinClass.GetShiftDay(CurrentIDCard, StartDate, EndDate);
      BookingDayCurrent := StartDate;
      // Is Bookingday of Current different from Next?
      // RV040.1. Only compare date-parts!!!
      if Trunc(BookingDayCurrent) <> Trunc(BookingDayNext) then
        LastScanRecordForDay := True;
    end;
    // MR:10-10-2003 Added 'DeleteAction' and 'AIDCard'
    // In case of a ADeleteAction, the data about this scan-to-delete
    // must be known.
    // 20013489.
    // Production Hours must always be recalculated?
//    ABookmarkRecord.AddToProdListIn := True;
//    ABookmarkRecord.AddToProdListOut := True;
    ProcessTimeRecording(AQuery, CurrentIDCard, True, True,
      ABookmarkRecord.AddToSalList, ABookmarkrecord.AddToProdListOut,
      ABookmarkrecord.AddToProdListIn, ADeleteAction, AIDCard,
      LastScanRecordForDay);
    if SystemDM.UseShiftDateSystem then
    begin
      // 20013489
      if AFromProcessPlannedAbsence then
      begin
        // When ShiftDate is not correct (or not set), then update TRS-record
        // Be sure there is a StartDate determined.
        AProdMinClass.GetShiftDay(CurrentIDCard, StartDate, EndDate);
        if ((Trunc(StartDate) <> CurrentIDCard.ShiftDate) or
          (CurrentIDCard.ShiftDate = 0)) then
          UpdateTimeRegScanning(CurrentIDCard, Trunc(StartDate));
      end;
    end; // if UseShiftDateSystem
  end; // for

  // MR:08-09-2003
  // Clear list
  for Index := RecordsForUpdate.Count - 1 downto 0 do
  begin
    ABookmarkRecord := RecordsForUpdate.Items[Index];
    // MR:10-10-2003 'dispose' record
    Dispose(ABookMarkRecord);
    RecordsForUpdate.Remove(ABookmarkRecord);
  end;
  RecordsForUpdate.Free;

  // PIM-87 Related to this order: No hours were stored.
  // Only after adding a commit it was stored.
  try
    if SystemDM.Pims.InTransaction then
      SystemDM.Pims.Commit;
  except
    on E: EDBEngineError do
    begin
    end;
    on E: Exception do
    begin
    end;
  end;

{$IFDEF DEBUG}
  WDebugLog('End UnprocessProcessScans');
{$ENDIF}
end; // UnProcessProcessScans

// RV045.1.
// Is there a contract group with settings for
// TimeForTime = Y ?
function TGlobalDM.ContractGroupTFTExists: Boolean;
begin
  Result := False;
  with qryContractGroupTFT do
  begin
    Open;
    if not Eof then
      Result := True;
    Close;
  end;
end;

procedure TGlobalDM.DataModuleCreate(Sender: TObject);
begin
  // RV079.4.
  ABalanceCounters := TBalanceCounters.Create;
end;

// RV071.6.
function TGlobalDM.AbsenceTotalSelect(AFieldName: String;
  ABookingDay: TDateTime; AEmployeeNumber: Integer;
  var AMinutes: Double): Boolean;
var
  Year, Month, Day: word;
begin
  Result := False;
  AMinutes := 0;
  DecodeDate(ABookingDay, Year, Month, Day);
  with qryAbsenceTotal do
  begin
    Close;
    SQL.Clear;
    SQL.Add(
      'SELECT ' + NL +
      '  ' + AFieldName + ' ' + NL +
      'FROM ' + NL +
      '  ABSENCETOTAL ' + NL +
      'WHERE ' + NL +
      '  EMPLOYEE_NUMBER = :EMPNO AND ' + NL +
      '  ABSENCE_YEAR = :AYEAR '
    );
    ParamByName('EMPNO').AsInteger := AEmployeeNumber;
    ParamByName('AYEAR').AsInteger := Year;
    Open;
    if not IsEmpty then
    begin
      AMinutes := FieldByName(AFieldName).AsFloat;
      Result := True;
    end;
  end;
end; // AbsenceTotalSelect

// RV071.6.
procedure TGlobalDM.AbsenceTotalInsert(AFieldName: String;
  ABookingDay: TDateTime; AEmployeeNumber: Integer; AMinutes: Double);
var
  Year, Month, Day: word;
begin
  DecodeDate(ABookingDay, Year, Month, Day);
  with qryAbsenceTotal do
  begin
    Close;
    SQL.Clear;
    SQL.Add(
      'INSERT INTO ABSENCETOTAL( ' + NL +
      '  EMPLOYEE_NUMBER, ABSENCE_YEAR, ' + NL +
      '  ' + AFieldName + ',' + NL +
      '  CREATIONDATE, ' + NL +
      '  MUTATIONDATE, MUTATOR) ' + NL +
      '  VALUES( ' + NL +
      '  :EMPNO, :AYEAR, :OTIME, :CDATE, :MDATE, ' + NL +
      '  :MUTATOR) '
      );
    ParamByName('EMPNO').AsInteger := AEmployeeNumber;
    ParamByName('AYEAR').AsInteger := Year;
    ParamByName('OTIME').AsFloat := AMinutes;
    ParamByName('CDATE').AsDateTime := Now;
    ParamByName('MDATE').AsDateTime := Now;
    ParamByName('MUTATOR').AsString := SystemDM.CurrentProgramUser;
    ExecSQL;
  end;
end; // AbsenceTotalInsert

// RV071.6.
procedure TGlobalDM.AbsenceTotalUpdate(AFieldName: String;
  ABookingDay: TDateTime; AEmployeeNumber: Integer; AMinutes: Double);
var
  Year, Month, Day: word;
begin
  DecodeDate(ABookingDay, Year, Month, Day);
  with qryAbsenceTotal do
  begin
    Close;
    SQL.Clear;
    SQL.Add(
      'UPDATE ABSENCETOTAL ' + NL +
      '  SET ' +
      '  ' + AFieldName + ' = :OTIME, ' + NL +
      '  MUTATIONDATE = :MDATE, MUTATOR = :MUTATOR ' + NL +
      '  WHERE EMPLOYEE_NUMBER = :EMPNO AND ' + NL +
      '  ABSENCE_YEAR = :AYEAR '
      );
    ParamByName('EMPNO').AsInteger := AEmployeeNumber;
    ParamByName('AYEAR').AsInteger := Year;
    ParamByName('OTIME').AsFloat := AMinutes;
    ParamByName('MDATE').AsDateTime := Now;
    ParamByName('MUTATOR').AsString := SystemDM.CurrentProgramUser;
    ExecSQL;
  end;
end; // AbsenceTotalUpdate

// RV071.6.
function TGlobalDM.UpdateAbsenceTotal(AFieldName: String;
  ABookingDay: TDateTime; AEmployeeNumber: Integer;
  AMinutes: Double; AOverwrite: Boolean): Integer;
var
  TotalMinutes, ExistingMinutes: Double;
begin
  Result := 0;
  if AbsenceTotalSelect(AFieldName, ABookingDay, AEmployeeNumber,
    ExistingMinutes) then
  begin
    if AOverwrite then
      TotalMinutes := AMinutes
    else
      TotalMinutes := ExistingMinutes + AMinutes;
    // Only update if there is a difference
    if TotalMinutes <> ExistingMinutes then
      AbsenceTotalUpdate(AFieldName, ABookingDay, AEmployeeNumber, TotalMinutes)
  end
  else
  begin
    // Only insert new record if it is <> 0
    if AMinutes <> 0 then
      AbsenceTotalInsert(AFieldName, ABookingDay, AEmployeeNumber, AMinutes);
    Result := 1;
  end;
(*



  EarnedTime := Minutes;
  DecodeDate(BookingDay, Year, Month, Day);
  qryAbsTotTFT.Close;
  qryAbsTotTFT.ParamByName('EMPNO').AsInteger :=
    EmployeeData.EmployeeCode;
  qryAbsTotTFT.ParamByName('AYEAR').AsInteger := Year;
  qryAbsTotTFT.Open;
  if not qryAbsTotTFT.IsEmpty then
  begin
    EarnedTime := EarnedTime +
      qryAbsTotTFT.FieldByName('EARNED_TFT_MINUTE').AsFloat;
    qryAbsTotTFTUpdate.Close;
    qryAbsTotTFTUpdate.ParamByName('EMPNO').AsInteger :=
      EmployeeData.EmployeeCode;
    qryAbsTotTFTUpdate.ParamByName('AYEAR').AsInteger := Year;
    qryAbsTotTFTUpdate.ParamByName('OTIME').AsFloat := EarnedTime;
    qryAbsTotTFTUpdate.ParamByName('MDATE').AsDateTime := Now;
    qryAbsTotTFTUpdate.ParamByName('MUTATOR').AsString :=
      SystemDM.CurrentProgramUser;
    qryAbsTotTFTUpdate.ExecSQL;
    qryAbsTotTFTUpdate.Close;
    Result := 0;
  end
  else
  begin
    qryAbsTotTFTInsert.Close;
    qryAbsTotTFTInsert.ParamByName('EMPNO').AsInteger :=
      EmployeeData.EmployeeCode;
    qryAbsTotTFTInsert.ParamByName('AYEAR').AsInteger := Year;
    qryAbsTotTFTInsert.ParamByName('OTIME').AsFloat := EarnedTime;
    qryAbsTotTFTInsert.ParamByName('CDATE').AsDateTime := Now;
    qryAbsTotTFTInsert.ParamByName('MDATE').AsDateTime := Now;
    qryAbsTotTFTInsert.ParamByName('MUTATOR').AsString :=
      SystemDM.CurrentProgramUser;
    qryAbsTotTFTInsert.ExecSQL;
    qryAbsTotTFTInsert.Close;
    Result := 1;
  end;
  qryAbsTotTFT.Close;
*)
end;

// RV071.6
// Procedure that can recalculate shorter-working-week hours made during
// overtime that is stored in ABSENCETOTAL table.
// This must be done when salary-hours are recalculated and must be
// done for the employee and the complete year.
// RV045.1. Also: Calculation BONUS_PERCENTAGE added, this was missing!
// RV045.1. When 0 earned minutes are found, then this must also be updated!
function TGlobalDM.RecalcEarnedSWWHours(AEmployeeNumber: Integer;
  ADateFrom: TDatetime; ADateTo: TDateTime;
  AUpdateBalance: Boolean=True;
  AMyDate: Boolean=False): Double;
var
  DateFrom, DateTo: TDateTime;
  YearFrom, YearTo, Year, Month, Day: Word;
  EarnedTime, TotEarnedTime: Double;
  BonusInMoney: Boolean;
  BonusPercentage: Double;
  BookingDay: TDateTime;
begin
  Result := 0;
{$IFDEF DEBUG}
  WDebugLog('- RecalcEarnedSWWHours - START');
{$ENDIF}
  if (ADateFrom <> NullDate) and (ADateTo <> NullDate) then
  begin
    DecodeDate(ADateFrom, YearFrom, Month, Day);
    DecodeDate(ADateTo, YearTo, Month, Day);
    for Year := YearFrom to YearTo do
    begin
      if not AMyDate then
      begin
        DateFrom := EncodeDate(Year, 1, 1);
        DateTo := EncodeDate(Year, 12, 31);
      end
      else
      begin
        DateFrom := ADateFrom;
        DateTo := ADateTo;
      end;
      with qryGetSWWHours do
      begin
        // First check for SWW-minutes.
        Close;
        ParamByName('EMPNO').AsInteger := AEmployeeNumber;
        ParamByName('DATEFROM').AsDateTime := DateFrom;
        ParamByName('DATETO').AsDateTime := DateTo;
        Open;
      end;
      TotEarnedTime := 0;
      if not qryGetSWWHours.Eof then
      begin
        while not qryGetSWWHours.Eof do
        begin
          // Bonus in Money checkbox can be set at Contract group-level
          // and at Hourtype-level, check both here.
          BonusInMoney :=
            (qryGetSWWHours.FieldByName('CG_BONUS_IN_MONEY_YN').AsString = 'Y');
          if not BonusInMoney then
            BonusInMoney :=
              (qryGetSWWHours.FieldByName('HT_BONUS_IN_MONEY_YN').AsString = 'Y');
{$IFDEF DEBUG}
  WDebugLog('- BonusInMoney=' + BoolToYN(BonusInMoney));
{$ENDIF}
          BonusPercentage := 0;
          if (qryGetSWWHours.FieldByName('BONUS_PERCENTAGE').AsString <> '') then
            BonusPercentage :=
              qryGetSWWHours.FieldByName('BONUS_PERCENTAGE').AsFloat;
          EarnedTime := qryGetSWWHours.FieldByName('SUMMINUTE').AsFloat;
          if not BonusInMoney then
          begin
            EarnedTime := Round(EarnedTime + EarnedTime *
              BonusPercentage / 100);
{$IFDEF DEBUG}
  WDebugLog('- BonusPerc=' + Format('%.0f', [BonusPercentage]));
{$ENDIF}
          end;
          TotEarnedTime := TotEarnedTime + EarnedTime;
          qryGetSWWHours.Next;
        end;
{$IFDEF DEBUG}
  WDebugLog('- EarnedTime=' + Format('%.0f', [TotEarnedTime]) +
    ' Year=' + IntToStr(Year));
{$ENDIF}
        qryGetSWWHours.Close;
      end; // if not qryGetSWWHours.Eof then

      Result := Result + TotEarnedTime;
      // Now update ABSENCETOTAL, field EARNED_SHORTWWEEK_MINUTE
      // RV071.6.
      if AUpdateBalance then
      begin
        BookingDay := EncodeDate(Year, 1, 1);
        UpdateAbsenceTotal('EARNED_SHORTWWEEK_MINUTE', BookingDay,
          AEmployeeNumber, TotEarnedTime, True);
      end;
      // Not year from year!
      if AMyDate then
        Break;
    end; // for Year := YearFrom to YearTo do
  end; // if (ADateFrom <> NullDate)
{$IFDEF DEBUG}
  WDebugLog('- RecalcEarnedSWWHours - END');
{$ENDIF}
end;

// RV075.2.
procedure TGlobalDM.RecalculateBalances(AEmployeeNumber: Integer;
  ADateFrom, ADateTo: TDateTime);
begin
  try
    RecalcEarnedTFTHours(AEmployeeNumber, ADateFrom, ADateTo);
    RecalcEarnedSWWHours(AEmployeeNumber, ADateFrom, ADateTo);
  except
    // Ignore error
  end;
end;

// RV075.4. This only updates PHEPT-record.
procedure TGlobalDM.UpdatePHEPT(AEmployeeData: TScannedIDCard;
  AProductionDate: TDateTime; AHourType, AMinutes: Integer; AManual: String;
  AInsertProdHrs: Boolean);
begin
  FillProdHourPerHourType(AEmployeeData, AProductionDate,
    AHourType, AMinutes, AManual, AInsertProdHrs);
end;

{ TBalanceCounters - START }

procedure TBalanceCounters.ComputeHoliday(var Remaining, Normal, Used,
  Planned, Available: String; RemainingVisible: Boolean);
begin
  Remaining := NullTime; Normal := NullTime; Used := NullTime;
  Planned := NullTime; Available := NullTime;
  with GlobalDM.qryBalanceCounters do
  begin
    if not IsEmpty then
    begin
      if RemainingVisible then
        Remaining := IntMin2StringTime(
          Round(FieldByName('LAST_YEAR_HOLIDAY_MINUTE').AsFloat),True)
      else
        Remaining := IntMin2StringTime(0,True);
      Normal := IntMin2StrDigTime(
        Round(FieldByName('NORM_HOLIDAY_MINUTE').AsFloat),3,True);
      Used := IntMin2StringTime(
        Round(FieldByName('USED_HOLIDAY_MINUTE').AsFloat),True);
    end;
    // Compute Planned
    Planned := IntMin2StringTime(ComputePlanned(GlobalDM.qryWork,
      EmployeeNumber, HOLIDAYAVAILABLETB, DateFrom), True);
    Available := IntMin2StringTime((StrTime2IntMin(Remaining) + StrTime2IntMin(Normal) -
      StrTime2IntMin(Used) - StrTime2IntMin(Planned)),True);
  end;
end;

procedure TBalanceCounters.ComputeIllness(var CurrentYear,
  PreviousYears: string);
begin
  // Previous years
  with GlobalDM.qryWork do
  begin
    // Previous years
    Close;
    SQL.Clear;
    SQL.Add(
      'SELECT ' + NL +
      '  SUM(ILLNESS_MINUTE) TOT ' + NL +
      'FROM ' + NL +
      '  ABSENCETOTAL' + NL +
      'WHERE ' + NL +
      '  EMPLOYEE_NUMBER = :EMPNO AND ABSENCE_YEAR <= :AYEAR'
      );
    ParamByName('EMPNO').AsInteger := EmployeeNumber;
    ParamByName('AYEAR').AsInteger := Year;
    Open;
    if not Eof then
      PreviousYears :=
        IntMin2StringTime(FieldByName('TOT').AsInteger,True);
    // Current year
    Close;
    SQL.Clear;
    SQL.Add(
      'SELECT ' + NL +
      '  ILLNESS_MINUTE ' + NL +
      'FROM ' + NL +
      '  ABSENCETOTAL ' + NL +
      'WHERE ' + NL +
      '  EMPLOYEE_NUMBER = :EMPNO AND ABSENCE_YEAR = :AYEAR'
      );
    ParamByName('EMPNO').AsInteger := EmployeeNumber;
    ParamByName('AYEAR').asInteger := Year;
    Open;
    if not Eof then
      CurrentYear := IntMin2StringTime(
        FieldByName('ILLNESS_MINUTE').AsInteger,True);
  end;
end;

procedure TBalanceCounters.ComputeTimeForTime(var Remaining, Earned, Used,
  Planned, Available: String);
begin
  Remaining := NullTime; Earned := NullTime; Used := NullTime;
  Planned := NullTime; Available := NullTime;
  with GlobalDM.qryBalanceCounters do
  begin
    if not IsEmpty then
    begin
      Remaining := IntMin2StringTime(
        Round(FieldByName('LAST_YEAR_TFT_MINUTE').AsFloat),True);
      Earned := IntMin2StringTime(
        Round(FieldByName('EARNED_TFT_MINUTE').AsFloat),True);
      Used := IntMin2StringTime(
        Round(FieldByName('USED_TFT_MINUTE').AsFloat),True);
    end;
    Planned := IntMin2StringTime(ComputePlanned(GlobalDM.qryWork,
      EmployeeNumber, TIMEFORTIMEAVAILABILITY, DateFrom),True);
    Available := IntMin2StringTime((StrTime2IntMin(Remaining) +
      StrTime2IntMin(Earned) - StrTime2IntMin(Used) -
        StrTime2IntMin(Planned)),True);
  end;
end;

procedure TBalanceCounters.ComputeWorkTime(var Remaining, Earned, Used,
  Planned, Available: String);
begin
  Remaining := NullTime; Earned := NullTime; Used := NullTime;
  Planned := NullTime; Available := NullTime;
  with GlobalDM.qryBalanceCounters do
  begin
    if not IsEmpty then
    begin
      Remaining := IntMin2StringTime(Round(
        FieldByName('LAST_YEAR_WTR_MINUTE').AsFloat),True);
      Earned := IntMin2StringTime(Round(
       FieldByName('EARNED_WTR_MINUTE').AsFloat),True);
      Used := IntMin2StringTime(Round(
        FieldByName('USED_WTR_MINUTE').AsFloat),True);
    end;
    Planned :=
      IntMin2StringTime(ComputePlanned(GlobalDM.qryWork, EmployeeNumber,
        WORKTIMEREDUCTIONAVAILABILITY, DateFrom),True);
    Available := IntMin2StringTime((StrTime2IntMin(Remaining) +
      StrTime2IntMin(Earned) - StrTime2IntMin(Used) -
        StrTime2IntMin(Planned)),True);
  end;
end;

procedure TBalanceCounters.ComputeHldPrevYear(var Remaining, Used, Planned,
  Available: String);
begin
  //Previous year holiday
  //'  LAST_YEAR_HLD_PREV_YEAR_MINUTE, USED_HOLIDAY_PREV_YEAR_MINUTE, '
  Remaining := NullTime; Used := NullTime;
  Planned := NullTime; Available := NullTime;
  with GlobalDM.qryBalanceCounters do
  begin
    if not IsEmpty then
    begin
      Remaining := IntMin2StringTime(Round(
        FieldByName('LAST_YEAR_HLD_PREV_YEAR_MINUTE').AsFloat),True);
      Used := IntMin2StringTime(Round(
        FieldByName('USED_HLD_PREV_YEAR_MINUTE').AsFloat),True);
    end;
    Planned :=
      IntMin2StringTime(ComputePlanned(GlobalDM.qryWork, EmployeeNumber,
        HOLIDAYPREVIOUSYEARAVAILABILITY, DateFrom),True);
    Available := IntMin2StringTime((StrTime2IntMin(Remaining)
      - StrTime2IntMin(Used) - StrTime2IntMin(Planned)),True);
  end;
end;

procedure TBalanceCounters.ComputeSeniorityHld(var Normal, Used, Planned,
  Available: String);
begin
  //Seniority holiday
  //'  NORM_SENIORITY_HOLIDAY_MINUTE, USED_SENIORITY_HOLIDAY_MINUTE, '
  Normal := NullTime; Used := NullTime; Planned := NullTime; Available := NullTime;
  with GlobalDM.qryBalanceCounters do
  begin
    if not IsEmpty then
    begin
      Normal := IntMin2StrDigTime(Round(
        FieldByName('NORM_SENIORITY_HOLIDAY_MINUTE').AsFloat), 3, True);
      Used := IntMin2StringTime(Round(
        FieldByName('USED_SENIORITY_HOLIDAY_MINUTE').AsFloat),True);
    end;
    Planned :=
      IntMin2StringTime(ComputePlanned(GlobalDM.qryWork, EmployeeNumber,
        SENIORITYHOLIDAYAVAILABILITY, DateFrom),True);
    Available := IntMin2StringTime((StrTime2IntMin(Normal)
      - StrTime2IntMin(Used) - StrTime2IntMin(Planned)),True);
  end;
end;

procedure TBalanceCounters.ComputeAddBnkHoliday(var Remaining, Used,
  Planned, Available: String);
begin
  //Add. bank holiday
  //'  LAST_YEAR_ADD_BANK_HOLIDAY_MINUTE, USED_ADD_BANK_HOLIDAY_MINUTE, '
  Remaining := NullTime; Used := NullTime;
  Planned := NullTime; Available := NullTime;
  with GlobalDM.qryBalanceCounters do
  begin
    if not IsEmpty then
    begin
      Remaining := IntMin2StringTime(Round(
        FieldByName('LAST_YEAR_ADD_BANK_HLD_MINUTE').AsFloat),True);
      Used := IntMin2StringTime(Round(
        FieldByName('USED_ADD_BANK_HLD_MINUTE').AsFloat),True);
    end;
    Planned :=
      IntMin2StringTime(ComputePlanned(GlobalDM.qryWork, EmployeeNumber,
        ADDBANKHOLIDAYAVAILABILITY, DateFrom),True);
    Available := IntMin2StringTime((StrTime2IntMin(Remaining) +
      - StrTime2IntMin(Used) - StrTime2IntMin(Planned)),True);
  end;
end;

procedure TBalanceCounters.ComputeSatCredit(var Remaining, Earned, Used,
  Planned, Available: String);
begin
  //Saturday credit
  //'  LAST_YEAR_SAT_CREDIT_MINUTE, USED_SAT_CREDIT_MINUTE, '
  Remaining := NullTime; Earned := NullTime; Used := NullTime;
  Planned := NullTime; Available := NullTime;
  with GlobalDM.qryBalanceCounters do
  begin
    if not IsEmpty then
    begin
      Earned := IntMin2StringTime(Round(
        FieldByName('EARNED_SAT_CREDIT_MINUTE').AsFloat),True);
      Used := IntMin2StringTime(Round(
        FieldByName('USED_SAT_CREDIT_MINUTE').AsFloat),True);
    end;
    Planned :=
      IntMin2StringTime(ComputePlanned(GlobalDM.qryWork, EmployeeNumber,
        SATCREDITAVAILABILITY, DateFrom),True);
    Available := IntMin2StringTime(
      (StrTime2IntMin(Earned) - StrTime2IntMin(Used) -
      StrTime2IntMin(Planned)),True);
  end;
end;

procedure TBalanceCounters.ComputeRsvBnkHoliday(var Normal, Used, Planned,
  Available: String);
begin
  //Bank holiday to reserve
  //'  NORM_BANK_HOLIDAY_RESERVE_MINUTE, USED_BANK_HOLIDAY_RESERVE_MINUTE, '
  Normal := NullTime; Used := NullTime; Planned := NullTime; Available := NullTime;
  with GlobalDM.qryBalanceCounters do
  begin
    if not IsEmpty then
    begin
      Normal := IntMin2StrDigTime(Round(
        FieldByName('NORM_BANK_HLD_RESERVE_MINUTE').asFloat), 3, True);
      Used := IntMin2StringTime(Round(
        FieldByName('USED_BANK_HLD_RESERVE_MINUTE').asFloat),True);
    end;
    Planned :=
      IntMin2StringTime(ComputePlanned(GlobalDM.qryWork, EmployeeNumber,
        BANKHOLIDAYRESERVEAVAILABILITY, DateFrom),True);
    Available := IntMin2StringTime((StrTime2IntMin(Normal)
      - StrTime2IntMin(Used) - StrTime2IntMin(Planned)),True);
  end;
end;

procedure TBalanceCounters.ComputeShorterWeek(var Remaining, Earned, Used,
  Planned, Available: String);
begin
  //Shorter week
  //'  LAST_YEAR_SHORTWWEEK_MINUTE, USED_SHORTWWEEK_MINUTE '
  // RV071.5. Addition of: EARNED_SHORTWWEEK_MINUTE
  Remaining := NullTime; Earned := NullTime; Used := NullTime;
  Planned := NullTime; Available := NullTime;
  with GlobalDM.qryBalanceCounters do
  begin
    if not IsEmpty then
    begin
      Remaining := IntMin2StringTime(Round(
        FieldByName('LAST_YEAR_SHORTWWEEK_MINUTE').AsFloat),True);
      Earned := IntMin2StrDigTime(Round(
        FieldByName('EARNED_SHORTWWEEK_MINUTE').AsFloat), 3, True);
      Used := IntMin2StringTime(Round(
        FieldByName('USED_SHORTWWEEK_MINUTE').AsFloat),True);
    end;
    Planned :=
      IntMin2StringTime(ComputePlanned(GlobalDM.qryWork, EmployeeNumber,
        SHORTERWORKWEEKAVAILABILITY, DateFrom),True);
    Available := IntMin2StringTime((StrTime2IntMin(Remaining) +
      StrTime2IntMin(Earned) - StrTime2IntMin(Used) -
        StrTime2IntMin(Planned)),True);
  end;
end;

function TBalanceCounters.ComputeMaxSalaryBalanceMinutes: String;
var
  Len: Integer;
begin
  Result := IntMin2StringTime(0, True);
  with GlobalDM.qryMaxSalaryBalance do
  begin
    Close;
    ParamByName('EMPLOYEE_NUMBER').AsInteger := EmployeeNumber;
    Open;
    if not IsEmpty then
    begin
      First;
      // MR:04-01-2005
      // If value is negative the length should be 3, otherwise it should
      // be 4! Otherwise the values will be converted wrong.
      if FieldByName('MAX_SALARY_BALANCE_MINUTES').AsInteger < 0 then
        Len := 3
      else
        Len := 4;
      Result := IntMin2StrDigTime(
        FieldByName('MAX_SALARY_BALANCE_MINUTES').AsInteger, Len, True);
    end;
    Close;
  end;
end;

function TBalanceCounters.DetermineCounterValue(
  const Fieldname: String): String;
begin
  Result := GlobalDM.qryBalanceCounters.FieldByName(FieldName).AsString;
end;

function TBalanceCounters.DetermineNormBankHldResMinManYN: String;
begin
  Result := DetermineCounterValue('NORM_BANK_HLD_RES_MIN_MAN_YN');
end;

procedure TBalanceCounters.GetAllCountersActivated;
var
  CountryId: Integer;
//  StrValue: String;
begin
  CountryId := SystemDM.GetDBValue(
  ' SELECT NVL(P.COUNTRY_ID, 0) FROM ' +
  ' EMPLOYEE E, PLANT P ' +
  ' WHERE ' +
  '   E.PLANT_CODE = P.PLANT_CODE AND ' +
  '   E.EMPLOYEE_NUMBER = ' +
  IntToStr(EmployeeNumber), 0
  );

  // Get all counters here for the given CountryID.
  with GlobalDM.qryActiveCounters do
  begin
    Close;
    ParamByName('COUNTRY_ID').AsInteger := CountryID;
    Open;
  end;

{
  Result := SystemDM.GetCounterActivated(CountryId, AAbsenceType);

  if Result then
  begin
    StrValue := SystemDM.GetDBValue(
      Format(
      'SELECT AC.COUNTER_DESCRIPTION FROM ABSENCETYPEPERCOUNTRY AC ' +
      'WHERE COUNTRY_ID = %d AND ABSENCETYPE_CODE = ''%s''',
      [CountryId, AAbsenceType]
      ), '*'
    );
    if StrValue <> '*' then
      ACounterDescription := StrValue;
  end;
}
end;

function TBalanceCounters.GetCounterActivated(AAbsenceType: String;
  var ACounterDescription: String): Boolean;
begin
  Result := False;
  with GlobalDM.qryActiveCounters do
  begin
    if not IsEmpty then
    begin
      if Locate('ABSENCETYPE_CODE', AAbsenceType, []) then
      begin
        ACounterDescription := FieldByName('COUNTER_DESCRIPTION').AsString;
        Result := True;
      end
      else
        ACounterDescription := '';
    end
    else
      ACounterDescription := '';
  end;
end;

procedure TBalanceCounters.GetCounters;
begin
  GetAllCountersActivated;
  with GlobalDM.qryBalanceCounters do
  begin
    Close;
    ParamByName('EMPLOYEE_NUMBER').AsInteger := EmployeeNumber;
    ParamByName('ABSENCE_YEAR').AsInteger := Year;
    Open;
  end;
end;

function TBalanceCounters.AbsenceTypePerCountryExist: Boolean;
var
  CountryId: Integer;
begin
  Result := False;
  CountryId := SystemDM.GetDBValue(
  ' SELECT NVL(P.COUNTRY_ID, 0) FROM ' +
  ' EMPLOYEE E, PLANT P ' +
  ' WHERE ' +
  '   E.PLANT_CODE = P.PLANT_CODE AND ' +
  '   E.EMPLOYEE_NUMBER = ' +
  IntToStr(EmployeeNumber), 0
  );
  if CountryID <> 0 then
  begin
    with GlobalDM.qryAbsenceTypePerCountryExist do
    begin
      Close;
      ParamByName('COUNTRY_ID').AsInteger :=
        CountryID;
      Open;
      Result := not Eof;
      Close;
    end;
  end;
end;

{ TBalanceCounters - END }

procedure TGlobalDM.DataModuleDestroy(Sender: TObject);
begin
  ABalanceCounters.Free;
end;

// RV085.15. Is there a Plant linked to Belgium?
function TGlobalDM.PlantIsBelgium: Boolean;
begin
  Result := False;
  with qryPlantCountryBelgium do
  begin
    Close;
    ParamByName('CODE').AsString := COUNTRYCODE_BELGIUM;
    ParamByName('USER_NAME').AsString := SystemDM.UserTeamLoginUser;
    Open;
    if not Eof then
      Result := True;
    Close;
  end;
end;

// RV100.1.
function TGlobalDM.FindWorkspotOverruledContractGroup(APlantCode,
  AWorkspotCode, ADefaultContractGroupCode: String): String;
begin
  Result := ADefaultContractGroupCode;
  with qryWorkspotContractGroup do
  begin
    Close;
    ParamByName('PLANT_CODE').AsString := APlantCode;
    ParamByName('WORKSPOT_CODE').AsString := AWorkspotCode;
    Open;
    if not Eof then
      Result := FieldByName('CONTRACTGROUP_CODE').AsString;
    Close;
  end;
end;

// RV100.1. Find contract group by employee, when empty in IDCard.
procedure TGlobalDM.FindContractGroup(var AIDCard: TScannedIDCard);
begin
  if AIDCard.ContractgroupCode = '' then
  begin
    with qryPopIDCard do
    begin
      Close;
      ParamByName('EMPNO').AsInteger := AIDCard.EmployeeCode;
      Open;
      if not Eof then
        AIDCard.ContractgroupCode :=
          FieldByName('CONTRACTGROUP_CODE').AsString;
      Close;
    end
  end;
end;

// RV103.1. Changed to global procedure.
// Change availability from <AAbsenceReason> to '*'.
procedure TGlobalDM.UpdateEMA(AAbsenceReason: String;
  AEmployeeIDCard: TScannedIDCard; AStartDate: TDateTime; AMore: Integer);
begin
  // AMore = 1: Multiple records are changed!
  // AMore = 0: Only 1 record is changed.
  with qryEmpAvailUpdate1 do
  begin
    Close;
    ParamByName('NEWAVCODE').AsString := '*';
    ParamByName('STDATE').AsDateTime := AStartDate;
    ParamByName('MUTATOR').AsString := SystemDM.CurrentProgramUser;
    ParamByName('EMPNO').AsInteger := AEmployeeIDCard.EmployeeCode;
    ParamByName('AVCODE').AsString := AAbsenceReason;
    ParamByName('MORE').AsInteger := AMore;
    ExecSql;
    Close;
  end;
  with qryEmpAvailUpdate2 do
  begin
    Close;
    ParamByName('NEWAVCODE').AsString := '*';
    ParamByName('STDATE').AsDateTime := AStartDate;
    ParamByName('MUTATOR').AsString := SystemDM.CurrentProgramUser;
    ParamByName('EMPNO').AsInteger := AEmployeeIDCard.EmployeeCode;
    ParamByName('AVCODE').AsString := AAbsenceReason;
    ParamByName('MORE').AsInteger := AMore;
    ExecSql;
    Close;
  end;
  with qryEmpAvailUpdate3 do
  begin
    Close;
    ParamByName('NEWAVCODE').AsString := '*';
    ParamByName('STDATE').AsDateTime := AStartDate;
    ParamByName('MUTATOR').AsString := SystemDM.CurrentProgramUser;
    ParamByName('EMPNO').AsInteger := AEmployeeIDCard.EmployeeCode;
    ParamByName('AVCODE').AsString := AAbsenceReason;
    ParamByName('MORE').AsInteger := AMore;
    ExecSql;
    Close;
  end;
  with qryEmpAvailUpdate4 do
  begin
    Close;
    ParamByName('NEWAVCODE').AsString := '*';
    ParamByName('STDATE').AsDateTime := AStartDate;
    ParamByName('MUTATOR').AsString := SystemDM.CurrentProgramUser;
    ParamByName('EMPNO').AsInteger := AEmployeeIDCard.EmployeeCode;
    ParamByName('AVCODE').AsString := AAbsenceReason;
    ParamByName('MORE').AsInteger := AMore;
    ExecSql;
    Close;
  end;
  if SystemDM.MaxTimeblocks > MIN_TBS then
  begin
    with qryEmpAvailUpdate5 do
    begin
      Close;
      ParamByName('NEWAVCODE').AsString := '*';
      ParamByName('STDATE').AsDateTime := AStartDate;
      ParamByName('MUTATOR').AsString := SystemDM.CurrentProgramUser;
      ParamByName('EMPNO').AsInteger := AEmployeeIDCard.EmployeeCode;
      ParamByName('AVCODE').AsString := AAbsenceReason;
      ParamByName('MORE').AsInteger := AMore;
      ExecSql;
      Close;
    end;
    with qryEmpAvailUpdate6 do
    begin
      Close;
      ParamByName('NEWAVCODE').AsString := '*';
      ParamByName('STDATE').AsDateTime := AStartDate;
      ParamByName('MUTATOR').AsString := SystemDM.CurrentProgramUser;
      ParamByName('EMPNO').AsInteger := AEmployeeIDCard.EmployeeCode;
      ParamByName('AVCODE').AsString := AAbsenceReason;
      ParamByName('MORE').AsInteger := AMore;
      ExecSql;
      Close;
    end;
    with qryEmpAvailUpdate7 do
    begin
      Close;
      ParamByName('NEWAVCODE').AsString := '*';
      ParamByName('STDATE').AsDateTime := AStartDate;
      ParamByName('MUTATOR').AsString := SystemDM.CurrentProgramUser;
      ParamByName('EMPNO').AsInteger := AEmployeeIDCard.EmployeeCode;
      ParamByName('AVCODE').AsString := AAbsenceReason;
      ParamByName('MORE').AsInteger := AMore;
      ExecSql;
      Close;
    end;
    with qryEmpAvailUpdate8 do
    begin
      Close;
      ParamByName('NEWAVCODE').AsString := '*';
      ParamByName('STDATE').AsDateTime := AStartDate;
      ParamByName('MUTATOR').AsString := SystemDM.CurrentProgramUser;
      ParamByName('EMPNO').AsInteger := AEmployeeIDCard.EmployeeCode;
      ParamByName('AVCODE').AsString := AAbsenceReason;
      ParamByName('MORE').AsInteger := AMore;
      ExecSql;
      Close;
    end;
    with qryEmpAvailUpdate9 do
    begin
      Close;
      ParamByName('NEWAVCODE').AsString := '*';
      ParamByName('STDATE').AsDateTime := AStartDate;
      ParamByName('MUTATOR').AsString := SystemDM.CurrentProgramUser;
      ParamByName('EMPNO').AsInteger := AEmployeeIDCard.EmployeeCode;
      ParamByName('AVCODE').AsString := AAbsenceReason;
      ParamByName('MORE').AsInteger := AMore;
      ExecSql;
      Close;
    end;
    with qryEmpAvailUpdate10 do
    begin
      Close;
      ParamByName('NEWAVCODE').AsString := '*';
      ParamByName('STDATE').AsDateTime := AStartDate;
      ParamByName('MUTATOR').AsString := SystemDM.CurrentProgramUser;
      ParamByName('EMPNO').AsInteger := AEmployeeIDCard.EmployeeCode;
      ParamByName('AVCODE').AsString := AAbsenceReason;
      ParamByName('MORE').AsInteger := AMore;
      ExecSql;
      Close;
    end;
  end; // if
end; // UpdateEMA

// RV103.1.
procedure TGlobalDM.DetermineTimeForTimeSettings(ADataSet: TDataSet;
  var ATimeForTimeYN, ABonusInMoneyYN, AShorterWorkingWeekYN: String);
begin
    if ContractGroupTFTExists then
    begin
      ATimeForTimeYN :=
        ADataSet.FieldByName('TIME_FOR_TIME_YN').AsString;
      ABonusInMoneyYN :=
        ADataSet.FieldByName('BONUS_IN_MONEY_YN').AsString;
      AShorterWorkingWeekYN :=
        ADataSet.FieldByName('HT_ADV_YN').AsString;
{$IFDEF DEBUG}
  WDebugLog('- CG TFTYN=' + ATimeForTimeYN + ' BIMYN=' + ABonusInMoneyYN);
{$ENDIF}
    end
    else
    begin
      if ADataSet.FieldByName('HT_OVERTIME_YN').AsString = CHECKEDVALUE then
      begin
        ATimeForTimeYN :=
          ADataSet.FieldByName('HT_TIME_FOR_TIME_YN').AsString;
        ABonusInMoneyYN :=
          ADataSet.FieldByName('HT_BONUS_IN_MONEY_YN').AsString;
        AShorterWorkingWeekYN :=
          ADataSet.FieldByName('HT_ADV_YN').AsString;
{$IFDEF DEBUG}
  WDebugLog('- HT TFTYN=' + ATimeForTimeYN + ' BIMYN=' + ABonusInMoneyYN);
{$ENDIF}
      end
      else
      begin
        ATimeForTimeYN := UNCHECKEDVALUE;
        ABonusInMoneyYN := UNCHECKEDVALUE;
        AShorterWorkingWeekYN := UNCHECKEDVALUE;
      end;
    end;
end;

// RV103.1.
function TGlobalDM.DetermineTimeForTime(ADataSet: TDataSet;
  AEarnedTimeTFT: Double; AEarnedMin: Integer): Double;
var
  Percentage: Double;
  TimeForTimeYN, BonusInMoneyYN, ShorterWorkingWeekYN: String;
begin
  DetermineTimeForTimeSettings(ADataSet, TimeForTimeYN, BonusInMoneyYN,
    ShorterWorkingWeekYN);
  if TimeForTimeYN = CHECKEDVALUE then
  begin
    Percentage := ADataSet.FieldByName('BONUS_PERCENTAGE').AsFloat;
    if BonusInMoneyYN = UNCHECKEDVALUE then
      AEarnedTimeTFT := AEarnedTimeTFT +
        Round(AEarnedMin + AEarnedMin * Percentage / 100)
    else
      AEarnedTimeTFT := AEarnedTimeTFT + AEarnedMin;
  end;
  Result := AEarnedTimeTFT;
end; // DetermineTimeForTime

// RV103.1.
function TGlobalDM.DetermineShorterWorkingWeek(ADataSet: TDataSet;
  AEarnedTimeSWW: Double; AEarnedMin: Integer): Double;
var
  Percentage: Double;
  TimeForTimeYN, BonusInMoneyYN, ShorterWorkingWeekYN: String;
begin
  DetermineTimeForTimeSettings(ADataSet, TimeForTimeYN, BonusInMoneyYN,
    ShorterWorkingWeekYN);
  if ShorterWorkingWeekYN = CHECKEDVALUE then
  begin
    Percentage := ADataSet.FieldByName('BONUS_PERCENTAGE').AsFloat;
    if BonusInMoneyYN = UNCHECKEDVALUE then
      AEarnedTimeSWW := AEarnedTimeSWW +
        Round(AEarnedMin + AEarnedMin * Percentage / 100)
    else
      AEarnedTimeSWW := AEarnedTimeSWW + AEarnedMin;
  end;
  Result := AEarnedTimeSWW;
end; // DetermineShorterWorkingWeek

// RV103.1.
procedure TGlobalDM.BooksTFT(ABookingDay: TDateTime;
  AEmployeeData: TScannedIDCard; AEarnedTimeTFT: Double);
begin
{$IFDEF DEBUG}
  WDebugLog('- EarnedTimeTFT=' + Format('%.f', [AEarnedTimeTFT]));
{$ENDIF}
  // booking Time for Time
  if AEarnedTimeTFT > 0 then
    UpdateAbsenceTotal('EARNED_TFT_MINUTE', ABookingDay,
      AEmployeeData.EmployeeCode, AEarnedTimeTFT, False);
end; // BooksTFT

// RV103.1.
procedure TGlobalDM.BooksSWW(ABookingDay: TDateTime;
  AEmployeeData: TScannedIDCard; AEarnedTimeSWW: Double);
begin
{$IFDEF DEBUG}
  WDebugLog('- EarnedTimeSWW=' + Format('%.f', [AEarnedTimeSWW]));
{$ENDIF}
  // RV071.6.
  // Booking Shorter Working Week (SWW)
  if AEarnedTimeSWW > 0 then
    UpdateAbsenceTotal('EARNED_SHORTWWEEK_MINUTE', ABookingDay,
      AEmployeeData.EmployeeCode, AEarnedTimeSWW, False);
end; // BooksSWW

// RV103.1.
// RV104.1. Add 'AUpdateYes' when called from 'process planned absence':
//          - This determines if PHPEPT should be updated:
//            - For 'process planned absence' this is not needed.
function TGlobalDM.BooksWorkedHoursOnBankHoliday(ABookingDay: TDateTime;
  AWorkedMin: Integer; AEmployeeData: TScannedIDcard;
  AManual: String;
  AUpdateYes: Boolean=True): Integer;
var
  HourtypeWorkedOnBankHoliday, AbsenceReasonID, AbsenceMinutes: Integer;
  AbsenceReasonCode, AbsenceTypeCode: String;
  AbsenceYear, Month, Day: Word;
  EarnedMin: Integer;
  EarnedTimeTFT, EarnedTimeSWW: Double;
  function DetermineHourtypeWorkedOnBankHoliday: Integer;
  begin
    Result := -1;
    with qryHTONBK do
    begin
      Close;
      ParamByName('EMPLOYEE_NUMBER').AsInteger := AEmployeeData.EmployeeCode;
      ParamByName('BANKHOL_DATE').AsDate := ABookingDay;
      Open;
      if not Eof then
      begin
        AbsenceReasonCode := FieldByName('ABSENCEREASON_CODE').AsString;
        AbsenceTypeCode := FieldByName('ABSENCETYPE_CODE').AsString;
        AbsenceReasonID := FieldByName('ABSENCEREASON_ID').AsInteger;
        if FieldByName('CG_HOURTYPE_WRK_ON_BANKHOL').AsString <> '' then
          Result := FieldByName('CG_HOURTYPE_WRK_ON_BANKHOL').AsInteger
        else
        begin
          if FieldByName('HOURTYPE_WRK_ON_BANKHOL').AsString <> '' then
            Result := FieldByName('HOURTYPE_WRK_ON_BANKHOL').AsInteger;
        end;
      end;
      Close;
    end;
  end; // DetermineHourtypeWorkedOnBankHoliday
begin
  Result := AWorkedMin;
  if Result = 0 then
    Exit;
  AbsenceReasonCode := '';
  AbsenceTypeCode := '';
  AbsenceReasonID := -1;
  EarnedTimeTFT := 0;
  EarnedTimeSWW := 0;
  FindContractGroup(AEmployeeData);
  HourtypeWorkedOnBankHoliday := DetermineHourtypeWorkedOnBankHoliday;
  if HourtypeWorkedOnBankHoliday <> -1 then
  begin
    qryOvertimeParams.Close;
    qryOvertimeParams.ParamByName('CONTRACTGROUP_CODE').AsString :=
      AEmployeeData.ContractgroupCode;
    qryOvertimeParams.ParamByName('HOURTYPE_NUMBER').AsInteger :=
      HourtypeWorkedOnBankHoliday;
    qryOvertimeParams.Open;
    if not qryOvertimeParams.Eof then
    begin
      // Change Employee Availability from <AbsenceReasonCode> to '*'
      UpdateEMA(AbsenceReasonCode, AEmployeeData, ABookingDay, 0);
      // Absence hours must be reprocessed!
      with qryAHE do
      begin
        Close;
        ParamByName('EMPLOYEE_NUMBER').AsInteger := AEmployeeData.EmployeeCode;
        ParamByName('ABSENCEHOUR_DATE').AsDate := ABookingDay;
        ParamByName('ABSENCEREASON_ID').AsInteger := AbsenceReasonID;
        ParamByName('MANUAL_YN').AsString := UNCHECKEDVALUE;
        Open;
        if not Eof then
        begin
          AbsenceMinutes := FieldByName('ABSENCE_MINUTE').AsInteger;
          // Subtract minutes from balance (when needed, based on absencetype)
          if AbsenceTypeCode <> '' then
          begin
            DecodeDate(ABookingDay, AbsenceYear, Month, Day);
            UpdateAbsenceTotalMode(qryWork, AEmployeeData.EmployeeCode,
              AbsenceYear, -1 * AbsenceMinutes, AbsenceTypeCode[1]);
          end;
        end;
        Close;
      end;
      // Now delete the absence hours.
      with qryAHEDelete do
      begin
        ParamByName('EMPLOYEE_NUMBER').AsInteger := AEmployeeData.EmployeeCode;
        ParamByName('ABSENCEHOUR_DATE').AsDate := ABookingDay;
        ParamByName('ABSENCEREASON_ID').AsInteger := AbsenceReasonID;
        ParamByName('MANUAL_YN').AsString := UNCHECKEDVALUE;
        ExecSQL;
      end;
      // Book hours made on bank holiday
      EarnedMin := UpdateSalaryHour(ABookingDay,
        AEmployeeData, HourtypeWorkedOnBankHoliday, AWorkedMin, AManual, False,
        AUpdateYes // RV104.1.
        );
      // Also book TFT/SWW if needed.
      EarnedTimeTFT := DetermineTimeForTime(qryOvertimeParams,
        EarnedTimeTFT, EarnedMin);
      EarnedTimeSWW := DetermineShorterWorkingWeek(qryOvertimeParams,
        EarnedTimeSWW, EarnedMin);
      BooksTFT(ABookingDay, AEmployeeData, EarnedTimeTFT);
      BooksSWW(ABookingDay, AEmployeeData, EarnedTimeSWW);
      Result := 0;
    end;
  end;
end; // BooksWorkedHoursOnBankHoliday

// RV103.2. This can be used to only determine SWW hours, without making
// changes to database.
function TGlobalDM.DetermineEarnedSWWHours(AEmployeeNumber: Integer;
  ADateFrom, ADateTo: TDateTime; AMyDate: Boolean): Double;
begin
  // Only determine earned SWW hours
  Result := RecalcEarnedSWWHours(AEmployeeNumber, ADateFrom, ADateTo, False,
    AMyDate);
end;

// 20013183
procedure TBalanceCounters.ComputeTravelTime(var Remaining, Earned, Used,
  Planned, Available: String);
begin
  Remaining := NullTime; Earned := NullTime; Used := NullTime;
  Planned := NullTime; Available := NullTime;
  with GlobalDM.qryBalanceCounters do
  begin
    if not IsEmpty then
    begin
      Remaining := IntMin2StringTime(Round(
        FieldByName('LAST_YEAR_TRAVELTIME_MINUTE').AsFloat),True);
      Earned := IntMin2StringTime(Round(
        FieldByName('EARNED_TRAVELTIME_MINUTE').AsFloat),True);
      Used := IntMin2StringTime(Round(
        FieldByName('USED_TRAVELTIME_MINUTE').AsFloat),True);
    end;
    Planned :=
      IntMin2StringTime(ComputePlanned(GlobalDM.qryWork, EmployeeNumber,
        TRAVELTIMEAVAILABILITY, DateFrom),True);
    Available := IntMin2StringTime((StrTime2IntMin(Remaining) +
      StrTime2IntMin(Earned) - StrTime2IntMin(Used) -
      StrTime2IntMin(Planned)),True);
  end;
end; // ComputeTravelTime

// 20013489 Overnight-Shift-System
procedure TGlobalDM.UpdateTimeRegScanning(AIDCard: TScannedIDCard;
  AShiftDate: TDateTime);
begin
  try
    with qryTRSUpdate do
    begin
      // Search arguments
      ParamByName('DATETIME_IN').AsDateTime := AIDCard.DateInOriginal; // 20015346
      ParamByName('EMPLOYEE_NUMBER').AsInteger := AIDCard.EmployeeCode;
      // Fields to change
      ParamByName('SHIFT_DATE').AsDateTime := AShiftDate;
      ParamByName('MUTATOR').AsString := SystemDM.CurrentProgramUser;
      ExecSQL;
    end;
  except
    on E: EDatabaseError do
    begin
      WErrorLog('UpdateTimeRegScanning:' + E.Message);
    end;
    on E: Exception do
    begin
      WErrorLog('UpdateTimeRegScanning:' + E.Message);
    end;
  end;
end;

// 20013489 Overnight-shift-system
// Use ShiftDate (based on shift) to store the production record.
// When this is about an overnight-shift it can mean it is stored
// on a different day (can be the day before or the current day).
// The ShiftDate indicates the start-of-the-shift.
function TGlobalDM.UpdateProductionHour(ProductionDate: TDateTime;
  EmployeeData: TScannedIDCard; Minutes, PayedBreaks: Integer;
  Manual: String): Integer;
var
  OldProdMin, OldPayedBreaks: integer;
  MyNow: TDateTime;
  StartDate, EndDate: TDateTime;
begin
  if SystemDM.UseShiftDateSystem then
  begin
    // 20013489. Determine production-date based on ShiftDate.
    // Only do this for non-manual-records!
    if Manual = UNCHECKEDVALUE then
    begin
      // 20013489 Assign ShiftDate to ProductionDate.
      if EmployeeData.ShiftDate = NullDate then
      begin
        // Find the shiftdate and assign to productiondate
        AProdMinClass.GetShiftDay(EmployeeData, StartDate, EndDate);
        ProductionDate := Trunc(StartDate);
      end
      else
        ProductionDate := EmployeeData.ShiftDate;
    end; // if
  end; // if UseShiftDateSystem
{$IFDEF DEBUG}
  WDebugLog('- UpdateProductionHour() ProductionDate=' + DateToStr(ProductionDate) +
    ' Emp=' + IntToStr(EmployeeData.EmployeeCode) +
    ' Shift=' + IntToStr(EmployeeData.ShiftNumber) +
    ' ShiftDate=' + DateToStr(EmployeeData.ShiftDate) +
    ' DateIn=' + DateTimeToStr(EmployeeData.DateIn) +
    ' DateOut=' + DateTimeToStr(EmployeeData.DateOut) +
    ' Min=' + IntMin2StringTime(Minutes, False) +
    ' PBrks=' + IntMin2StringTime(PayedBreaks, False)
  );
{$ENDIF}
//  Result := -1;
  MyNow := Now;
  qryProdHour.Close;
  qryProdHour.ParamByName('PDATE').AsDateTime := ProductionDate;
  qryProdHour.ParamByName('PCODE').AsString := EmployeeData.PlantCode;
  qryProdHour.ParamByName('SHNO').AsInteger := EmployeeData.ShiftNumber;
  qryProdHour.ParamByName('EMPNO').AsInteger := EmployeeData.EmployeeCode;
  qryProdHour.ParamByName('WCODE').AsString := EmployeeData.WorkSpotCode;
  qryProdHour.ParamByName('JCODE').AsString := EmployeeData.JobCode;
  qryProdHour.ParamByName('MAN').AsString := Manual;
  qryProdHour.Open;
  if not qryProdHour.IsEmpty then
  begin
    OldProdMin :=
      qryProdHour.FieldByName('PRODUCTION_MINUTE').AsInteger;
    OldPayedBreaks := qryProdHour.FieldByName('PAYED_BREAK_MINUTE').AsInteger;
    if (OldProdMin + Minutes) <= 0 then
    begin
      with qryProdHourDelete do
      begin
        Close;
        ParamByName('PDATE').AsDateTime := ProductionDate;
        ParamByName('PCODE').AsString := EmployeeData.PlantCode;
        ParamByName('SHNO').AsInteger := EmployeeData.ShiftNumber;
        ParamByName('EMPNO').AsInteger := EmployeeData.EmployeeCode;
        ParamByName('WCODE').AsString := EmployeeData.WorkSpotCode;
        ParamByName('JCODE').AsString := EmployeeData.JobCode;
        ParamByName('MAN').AsString := Manual;
        ExecSql;
        Close;
      end;
// delete all prodhourperemplperalltypes is were inserted before in extra table
//car 10.12.2002
      DeleteProdHourForAllTypes(EmployeeData, ProductionDate,
        ProductionDate, Manual);
//end car
    end
    else
    begin
      with qryProdHourUpdate do
      begin
        Close;
        ParamByName('PMIN').AsInteger := OldProdMin + Minutes;
        ParamByName('PAIDBREAK').AsInteger := PayedBreaks + OldPayedBreaks;
        ParamByName('PDATE').AsDateTime := ProductionDate;
        ParamByName('PCODE').AsString := EmployeeData.PlantCode;
        ParamByName('SHNO').AsInteger := EmployeeData.ShiftNumber;
        ParamByName('EMPNO').AsInteger := EmployeeData.EmployeeCode;
        ParamByName('WCODE').AsString := EmployeeData.WorkSpotCode;
        ParamByName('JCODE').AsString := EmployeeData.JobCode;
        ParamByName('MAN').AsString := Manual;
        ParamByName('MUTATOR').AsString := SystemDM.CurrentProgramUser;
        ParamByName('MUTATIONDATE').AsDateTime := MyNow;
        ExecSql;
        Close;
      end;
    end;
    Result := 0;
  end
  else
  begin
    with qryProdHourInsert do
    begin
      Close;
      ParamByName('PDATE').AsDateTime := ProductionDate;
      ParamByName('PCODE').AsString := EmployeeData.PlantCode;
      ParamByName('SHNO').AsInteger := EmployeeData.ShiftNumber;
      ParamByName('EMPNO').AsInteger := EmployeeData.EmployeeCode;
      ParamByName('WCODE').AsString := EmployeeData.WorkSpotCode;
      ParamByName('JCODE').AsString := EmployeeData.JobCode;
      ParamByName('PMIN').AsInteger := Minutes;
      ParamByName('MAN').AsString := Manual;
      ParamByName('PAYEDBREAK').AsInteger := PayedBreaks;
      ParamByName('CDATE').AsDateTime := MyNow;
      ParamByName('MDATE').AsDateTime := MyNow;
      ParamByName('MUTATOR').AsString := SystemDM.CurrentProgramUser;
      ExecSQL;
      Close;
    end;
    Result := 1;
  end;
  qryProdHour.Close;
  if Minutes >= 60 then
  begin
    // Changing the Employee level to C
    qryWSPerEmp.Close;
    qryWSPerEmp.ParamByName('EMPNO').AsInteger := EmployeeData.EmployeeCode;
    qryWSPerEmp.ParamByName('PCODE').AsString := EmployeeData.PlantCode;
    qryWSPerEmp.ParamByName('DCODE').AsString := EmployeeData.DepartmentCode;
    qryWSPerEmp.ParamByName('WCODE').AsString := EmployeeData.WorkSpotCode;
    qryWSPerEmp.Open;
    if (not qryWSPerEmp.IsEmpty) then
    begin
      if qryWSPerEmp.FieldByName('EMPLOYEE_LEVEL').IsNull then
      begin
        with qryWSPerEmpUpdate do
        begin
          Close;
          ParamByName('EMPLEVEL').AsString := LevelC;
          ParamByName('STLDATE').AsDateTime := MyNow;
          ParamByName('MUT').AsString := SystemDM.CurrentProgramUser;
          ParamByName('MUTDATE').AsDateTime := MyNow;
          ParamByName('EMPNO').AsInteger := EmployeeData.EmployeeCode;
          ParamByName('PCODE').AsString := EmployeeData.PlantCode;
          ParamByName('DCODE').AsString := EmployeeData.DepartmentCode;
          ParamByName('WCODE').AsString := EmployeeData.WorkSpotCode;
          ExecSQL;
          Close;
        end;
      end;
    end
    else
    begin
      with qryWSPerEmpInsert do
      begin
        Close;
        ParamByName('EMPLNO').AsInteger := EmployeeData.EmployeeCode;
        ParamByName('PCODE').AsString := EmployeeData.PlantCode;
        ParamByName('EMPLEVEL').AsString := LevelC;
        ParamByName('DCODE').AsString := EmployeeData.DepartmentCode;
        ParamByName('CDATE').AsDateTime := MyNow;
        ParamByName('WCODE').AsString := EmployeeData.WorkSpotCode;
        ParamByName('MDATE').AsDateTime := MyNow;
        ParamByName('MUT').AsString := SystemDM.CurrentProgramUser;
        ParamByName('STLEVDATE').AsDateTime := MyNow;
        ExecSQL;
        Close;
      end;
    end;
    qryWSPerEmp.Close;
  end;
end; // UpdateProductionHour

end.


