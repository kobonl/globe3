object GridBaseDM: TGridBaseDM
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Left = 268
  Top = 170
  Height = 480
  Width = 696
  object TableMaster: TTable
    BeforePost = DefaultBeforePost
    BeforeDelete = DefaultBeforeDelete
    OnDeleteError = DefaultDeleteError
    OnEditError = DefaultEditError
    OnNewRecord = DefaultNewRecord
    OnPostError = DefaultPostError
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    Left = 92
    Top = 48
  end
  object TableDetail: TTable
    BeforePost = DefaultBeforePost
    BeforeDelete = DefaultBeforeDelete
    OnDeleteError = DefaultDeleteError
    OnEditError = DefaultEditError
    OnNewRecord = DefaultNewRecord
    OnPostError = DefaultPostError
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    MasterSource = DataSourceMaster
    Left = 92
    Top = 140
  end
  object DataSourceMaster: TDataSource
    DataSet = TableMaster
    Left = 200
    Top = 48
  end
  object DataSourceDetail: TDataSource
    DataSet = TableDetail
    Left = 208
    Top = 132
  end
  object TableExport: TTable
    OnCalcFields = TableExportCalcFields
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    Left = 404
    Top = 324
  end
  object DataSourceExport: TDataSource
    Left = 488
    Top = 324
  end
end
