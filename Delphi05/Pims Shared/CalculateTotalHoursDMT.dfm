inherited CalculateTotalHoursDM: TCalculateTotalHoursDM
  OldCreateOrder = True
  Left = 296
  Top = 213
  Height = 531
  Width = 741
  object QuerySelector: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    Left = 344
    Top = 128
  end
  object QueryRequest: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  R.REQUESTED_EARLY_TIME,'
      '  R.REQUESTED_LATE_TIME'
      'FROM'
      '  REQUESTEARLYLATE R'
      'WHERE '
      '  ('
      '  R.REQUEST_DATE >= :DATEFROM AND'
      '  R.REQUEST_DATE <= :DATETO'
      '  )  AND'
      '  R.EMPLOYEE_NUMBER = :EMPNO'
      'ORDER BY'
      '  R.REQUESTED_EARLY_TIME,'
      '  R.REQUESTED_LATE_TIME'
      '')
    Left = 344
    Top = 16
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPNO'
        ParamType = ptUnknown
      end>
  end
  object ClientDataSetBShift: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'DataSetProviderBShift'
    Left = 56
    Top = 120
  end
  object ClientDataSetBDept: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'DataSetProviderBDept'
    Left = 56
    Top = 72
  end
  object ClientDataSetBEmpl: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'DataSetProviderBEmpl'
    Left = 56
    Top = 16
  end
  object DataSetProviderBEmpl: TDataSetProvider
    Constraints = True
    Left = 200
    Top = 16
  end
  object DataSetProviderBDept: TDataSetProvider
    Constraints = True
    Left = 200
    Top = 72
  end
  object DataSetProviderBShift: TDataSetProvider
    Constraints = True
    Left = 200
    Top = 128
  end
  object ClientDataSetShift: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'DataSetProviderShift'
    Left = 56
    Top = 344
  end
  object DataSetProviderShift: TDataSetProvider
    Constraints = True
    Left = 200
    Top = 344
  end
  object ClientDataSetTBShift: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'DataSetProviderTBShift'
    Left = 56
    Top = 288
  end
  object ClientDataSetTBDept: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'DataSetProviderTBDept'
    Left = 56
    Top = 232
  end
  object ClientDataSetTBEmpl: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'DataSetProviderTBEmpl'
    Left = 56
    Top = 168
  end
  object DataSetProviderTBEmpl: TDataSetProvider
    Constraints = True
    Left = 200
    Top = 176
  end
  object DataSetProviderTBDept: TDataSetProvider
    Constraints = True
    Left = 200
    Top = 232
  end
  object DataSetProviderTBShift: TDataSetProvider
    Constraints = True
    Left = 200
    Top = 288
  end
  object QueryShiftSchedule: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  S.EMPLOYEE_NUMBER,'
      '  S.SHIFT_SCHEDULE_DATE,'
      '  S.PLANT_CODE,'
      '  S.SHIFT_NUMBER'
      'FROM'
      '  SHIFTSCHEDULE S'
      'WHERE'
      '  S.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND'
      '  S.SHIFT_SCHEDULE_DATE = :SHIFT_SCHEDULE_DATE AND'
      '  S.SHIFT_NUMBER <> -1'
      'ORDER BY'
      '  S.EMPLOYEE_NUMBER,'
      '  S.SHIFT_SCHEDULE_DATE,'
      '  S.PLANT_CODE,'
      '  S.SHIFT_NUMBER'
      ' ')
    Left = 344
    Top = 72
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'SHIFT_SCHEDULE_DATE'
        ParamType = ptUnknown
      end>
  end
  object QueryContractgroup: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  C.ROUND_TRUNC_SALARY_HOURS,'
      '  C.ROUND_MINUTE'
      'FROM'
      '  CONTRACTGROUP C,'
      '  EMPLOYEE E'
      'WHERE'
      '  C.CONTRACTGROUP_CODE = E.CONTRACTGROUP_CODE AND'
      '  E.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER'
      ' ')
    Left = 344
    Top = 184
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end>
  end
  object QueryEmployee: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  PLANT_CODE, SHIFT_NUMBER'
      'FROM'
      '  EMPLOYEE'
      'WHERE'
      '  EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND'
      '  SHIFT_NUMBER IS NOT NULL')
    Left = 344
    Top = 240
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end>
  end
  object qryPrevScan1: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  DATETIME_IN, DATETIME_OUT'
      'FROM'
      '  TIMEREGSCANNING'
      'WHERE'
      '  EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND'
      '  DATETIME_OUT = :DATETIME_OUT AND'
      '  DATETIME_OUT <> :D_OUT')
    Left = 328
    Top = 296
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETIME_OUT'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'D_OUT'
        ParamType = ptUnknown
      end>
  end
  object qryPrevScan2: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  DATETIME_IN,DATETIME_OUT '
      'FROM '
      '  TIMEREGSCANNING '
      'WHERE '
      '  EMPLOYEE_NUMBER=:EMPLOYEE_NUMBER AND '
      '  DATETIME_OUT=:DATETIME_OUT'
      '')
    Left = 328
    Top = 352
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETIME_OUT'
        ParamType = ptUnknown
      end>
  end
  object qryNextScan1: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  DATETIME_IN, DATETIME_OUT '
      'FROM '
      '  TIMEREGSCANNING '
      'WHERE '
      '  EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND '
      '  DATETIME_IN = :DATETIME_OUT AND '
      '  DATETIME_IN <> :D_IN'
      '')
    Left = 416
    Top = 296
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETIME_OUT'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'D_IN'
        ParamType = ptUnknown
      end>
  end
  object qryNextScan2: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  DATETIME_IN, DATETIME_OUT '
      'FROM '
      '  TIMEREGSCANNING '
      'WHERE '
      '  EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND '
      '  DATETIME_IN = :DATETIME_OUT'
      '')
    Left = 416
    Top = 352
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETIME_OUT'
        ParamType = ptUnknown
      end>
  end
  object qryFindPlanningBlocks: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  NVL(ET.SCHEDULED_TIMEBLOCK_1,'#39'-'#39') SCHEDULED_TIMEBLOCK_1,'
      '  NVL(ET.SCHEDULED_TIMEBLOCK_2,'#39'-'#39') SCHEDULED_TIMEBLOCK_2,'
      '  NVL(ET.SCHEDULED_TIMEBLOCK_3,'#39'-'#39') SCHEDULED_TIMEBLOCK_3,'
      '  NVL(ET.SCHEDULED_TIMEBLOCK_4,'#39'-'#39') SCHEDULED_TIMEBLOCK_4,'
      '  NVL(ET.SCHEDULED_TIMEBLOCK_5,'#39'-'#39') SCHEDULED_TIMEBLOCK_5,'
      '  NVL(ET.SCHEDULED_TIMEBLOCK_6,'#39'-'#39') SCHEDULED_TIMEBLOCK_6,'
      '  NVL(ET.SCHEDULED_TIMEBLOCK_7,'#39'-'#39') SCHEDULED_TIMEBLOCK_7,'
      '  NVL(ET.SCHEDULED_TIMEBLOCK_8,'#39'-'#39') SCHEDULED_TIMEBLOCK_8,'
      '  NVL(ET.SCHEDULED_TIMEBLOCK_9,'#39'-'#39') SCHEDULED_TIMEBLOCK_9,'
      '  NVL(ET.SCHEDULED_TIMEBLOCK_10,'#39'-'#39') SCHEDULED_TIMEBLOCK_10'
      'FROM'
      '  EMPLOYEEPLANNING ET'
      'WHERE'
      '  ET.EMPLOYEEPLANNING_DATE = :EMPLOYEEPLANNING_DATE AND'
      '  ET.PLANT_CODE = :PLANT_CODE AND'
      '  ET.WORKSPOT_CODE = :WORKSPOT_CODE AND'
      '  ET.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND'
      '  ET.SHIFT_NUMBER = :SHIFT_NUMBER AND'
      '  ('
      '    ET.SCHEDULED_TIMEBLOCK_1 IN ('#39'A'#39', '#39'B'#39', '#39'C'#39') OR'
      '    ET.SCHEDULED_TIMEBLOCK_2 IN ('#39'A'#39', '#39'B'#39', '#39'C'#39') OR'
      '    ET.SCHEDULED_TIMEBLOCK_3 IN ('#39'A'#39', '#39'B'#39', '#39'C'#39') OR'
      '    ET.SCHEDULED_TIMEBLOCK_4 IN ('#39'A'#39', '#39'B'#39', '#39'C'#39') OR'
      '    ET.SCHEDULED_TIMEBLOCK_5 IN ('#39'A'#39', '#39'B'#39', '#39'C'#39') OR'
      '    ET.SCHEDULED_TIMEBLOCK_6 IN ('#39'A'#39', '#39'B'#39', '#39'C'#39') OR'
      '    ET.SCHEDULED_TIMEBLOCK_7 IN ('#39'A'#39', '#39'B'#39', '#39'C'#39') OR'
      '    ET.SCHEDULED_TIMEBLOCK_8 IN ('#39'A'#39', '#39'B'#39', '#39'C'#39') OR'
      '    ET.SCHEDULED_TIMEBLOCK_9 IN ('#39'A'#39', '#39'B'#39', '#39'C'#39') OR'
      '    ET.SCHEDULED_TIMEBLOCK_10 IN ('#39'A'#39', '#39'B'#39', '#39'C'#39')'
      '  )'
      'UNION ALL'
      'SELECT'
      '  NVL(SP.SCHEDULED_TIMEBLOCK_1,'#39'-'#39'),'
      '  NVL(SP.SCHEDULED_TIMEBLOCK_2,'#39'-'#39'),'
      '  NVL(SP.SCHEDULED_TIMEBLOCK_3,'#39'-'#39'),'
      '  NVL(SP.SCHEDULED_TIMEBLOCK_4,'#39'-'#39'),'
      '  NVL(SP.SCHEDULED_TIMEBLOCK_5,'#39'-'#39'),'
      '  NVL(SP.SCHEDULED_TIMEBLOCK_6,'#39'-'#39'),'
      '  NVL(SP.SCHEDULED_TIMEBLOCK_7,'#39'-'#39'),'
      '  NVL(SP.SCHEDULED_TIMEBLOCK_8,'#39'-'#39'),'
      '  NVL(SP.SCHEDULED_TIMEBLOCK_9,'#39'-'#39'),'
      '  NVL(SP.SCHEDULED_TIMEBLOCK_10,'#39'-'#39')'
      'FROM'
      '  STANDARDEMPLOYEEPLANNING  SP'
      'WHERE'
      '  SP.DAY_OF_WEEK = :DAY_OF_WEEK AND'
      '  SP.PLANT_CODE = :PLANT_CODE AND'
      '  SP.WORKSPOT_CODE = :WORKSPOT_CODE AND'
      '  SP.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND'
      '  SP.SHIFT_NUMBER = :SHIFT_NUMBER AND'
      '  ('
      '    SP.SCHEDULED_TIMEBLOCK_1 IN ('#39'A'#39','#39'B'#39','#39'C'#39') OR'
      '    SP.SCHEDULED_TIMEBLOCK_2 IN ('#39'A'#39','#39'B'#39','#39'C'#39') OR'
      '    SP.SCHEDULED_TIMEBLOCK_3 IN ('#39'A'#39','#39'B'#39','#39'C'#39') OR'
      '    SP.SCHEDULED_TIMEBLOCK_4 IN ('#39'A'#39','#39'B'#39','#39'C'#39') OR'
      '    SP.SCHEDULED_TIMEBLOCK_5 IN ('#39'A'#39','#39'B'#39','#39'C'#39') OR'
      '    SP.SCHEDULED_TIMEBLOCK_6 IN ('#39'A'#39','#39'B'#39','#39'C'#39') OR'
      '    SP.SCHEDULED_TIMEBLOCK_7 IN ('#39'A'#39','#39'B'#39','#39'C'#39') OR'
      '    SP.SCHEDULED_TIMEBLOCK_8 IN ('#39'A'#39','#39'B'#39','#39'C'#39') OR'
      '    SP.SCHEDULED_TIMEBLOCK_9 IN ('#39'A'#39','#39'B'#39','#39'C'#39') OR'
      '    SP.SCHEDULED_TIMEBLOCK_10 IN ('#39'A'#39','#39'B'#39','#39'C'#39')'
      '  )'
      'UNION ALL'
      'SELECT'
      '  NVL(EA.AVAILABLE_TIMEBLOCK_1,'#39'-'#39'),'
      '  NVL(EA.AVAILABLE_TIMEBLOCK_2,'#39'-'#39'),'
      '  NVL(EA.AVAILABLE_TIMEBLOCK_3,'#39'-'#39'),'
      '  NVL(EA.AVAILABLE_TIMEBLOCK_4,'#39'-'#39'),'
      '  NVL(EA.AVAILABLE_TIMEBLOCK_5,'#39'-'#39'),'
      '  NVL(EA.AVAILABLE_TIMEBLOCK_6,'#39'-'#39'),'
      '  NVL(EA.AVAILABLE_TIMEBLOCK_7,'#39'-'#39'),'
      '  NVL(EA.AVAILABLE_TIMEBLOCK_8,'#39'-'#39'),'
      '  NVL(EA.AVAILABLE_TIMEBLOCK_9,'#39'-'#39'),'
      '  NVL(EA.AVAILABLE_TIMEBLOCK_10,'#39'-'#39')'
      'FROM'
      '  EMPLOYEEAVAILABILITY EA'
      'WHERE'
      '  EA.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND'
      '  EA.PLANT_CODE = :PLANT_CODE AND'
      '  EA.EMPLOYEEAVAILABILITY_DATE = :EMPLOYEEPLANNING_DATE AND'
      '  EA.SHIFT_NUMBER = :SHIFT_NUMBER AND'
      '  ('
      '    EA.AVAILABLE_TIMEBLOCK_1 = '#39'*'#39' OR'
      '    EA.AVAILABLE_TIMEBLOCK_2 = '#39'*'#39' OR'
      '    EA.AVAILABLE_TIMEBLOCK_3 = '#39'*'#39' OR'
      '    EA.AVAILABLE_TIMEBLOCK_4 = '#39'*'#39' OR'
      '    EA.AVAILABLE_TIMEBLOCK_5 = '#39'*'#39' OR'
      '    EA.AVAILABLE_TIMEBLOCK_6 = '#39'*'#39' OR'
      '    EA.AVAILABLE_TIMEBLOCK_7 = '#39'*'#39' OR'
      '    EA.AVAILABLE_TIMEBLOCK_8 = '#39'*'#39' OR'
      '    EA.AVAILABLE_TIMEBLOCK_9 = '#39'*'#39' OR'
      '    EA.AVAILABLE_TIMEBLOCK_10 = '#39'*'#39
      '  )'
      'UNION ALL'
      'SELECT'
      '  NVL(SA.AVAILABLE_TIMEBLOCK_1,'#39'-'#39'),'
      '  NVL(SA.AVAILABLE_TIMEBLOCK_2,'#39'-'#39'),'
      '  NVL(SA.AVAILABLE_TIMEBLOCK_3,'#39'-'#39'),'
      '  NVL(SA.AVAILABLE_TIMEBLOCK_4,'#39'-'#39'),'
      '  NVL(SA.AVAILABLE_TIMEBLOCK_5,'#39'-'#39'),'
      '  NVL(SA.AVAILABLE_TIMEBLOCK_6,'#39'-'#39'),'
      '  NVL(SA.AVAILABLE_TIMEBLOCK_7,'#39'-'#39'),'
      '  NVL(SA.AVAILABLE_TIMEBLOCK_8,'#39'-'#39'),'
      '  NVL(SA.AVAILABLE_TIMEBLOCK_9,'#39'-'#39'),'
      '  NVL(SA.AVAILABLE_TIMEBLOCK_10,'#39'-'#39')'
      'FROM'
      '  STANDARDAVAILABILITY SA'
      'WHERE'
      '  SA.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND'
      '  SA.PLANT_CODE = :PLANT_CODE AND'
      '  SA.DAY_OF_WEEK = :DAY_OF_WEEK AND'
      '  SA.SHIFT_NUMBER = :SHIFT_NUMBER AND'
      '  ('
      '    SA.AVAILABLE_TIMEBLOCK_1 = '#39'*'#39' OR'
      '    SA.AVAILABLE_TIMEBLOCK_2 = '#39'*'#39' OR'
      '    SA.AVAILABLE_TIMEBLOCK_3 = '#39'*'#39' OR'
      '    SA.AVAILABLE_TIMEBLOCK_4 = '#39'*'#39' OR'
      '    SA.AVAILABLE_TIMEBLOCK_5 = '#39'*'#39' OR'
      '    SA.AVAILABLE_TIMEBLOCK_6 = '#39'*'#39' OR'
      '    SA.AVAILABLE_TIMEBLOCK_7 = '#39'*'#39' OR'
      '    SA.AVAILABLE_TIMEBLOCK_8 = '#39'*'#39' OR'
      '    SA.AVAILABLE_TIMEBLOCK_9 = '#39'*'#39' OR'
      '    SA.AVAILABLE_TIMEBLOCK_10 = '#39'*'#39
      '  )'
      ''
      ' ')
    Left = 56
    Top = 400
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'EMPLOYEEPLANNING_DATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'WORKSPOT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'SHIFT_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'DAY_OF_WEEK'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'WORKSPOT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'SHIFT_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'EMPLOYEEPLANNING_DATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'SHIFT_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'DAY_OF_WEEK'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'SHIFT_NUMBER'
        ParamType = ptUnknown
      end>
  end
  object qryIsFirstScan: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  CASE :ADELETEACTION'
      '    WHEN 0 THEN IsFirstScan(EMPLOYEE_NUMBER, DATETIME_IN, NULL)'
      '    ELSE IsFirstScan(EMPLOYEE_NUMBER, DATETIME_IN, DATETIME_IN)'
      '  END FIRSTSCAN'
      'FROM'
      '  TIMEREGSCANNING'
      'WHERE'
      '  EMPLOYEE_NUMBER = :AEMPLOYEENUMBER AND'
      '  DATETIME_IN = :ADATETIMEIN'
      ' ')
    Left = 520
    Top = 16
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ADELETEACTION'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'AEMPLOYEENUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'ADATETIMEIN'
        ParamType = ptUnknown
      end>
  end
  object qryIsLastScan: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  CASE :ADELETEACTION'
      '    WHEN 0 THEN IsLastScan(EMPLOYEE_NUMBER, DATETIME_OUT, NULL)'
      '    ELSE IsLastScan(EMPLOYEE_NUMBER, DATETIME_OUT, DATETIME_IN)'
      '  END LASTSCAN'
      'FROM'
      '  TIMEREGSCANNING'
      'WHERE'
      '  EMPLOYEE_NUMBER = :AEMPLOYEENUMBER AND'
      '  DATETIME_OUT = :ADATETIMEOUT'
      ''
      ' ')
    Left = 520
    Top = 80
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ADELETEACTION'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'AEMPLOYEENUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'ADATETIMEOUT'
        ParamType = ptUnknown
      end>
  end
  object qryFirstScan: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  MIN(DATETIME_IN) DATETIME_IN'
      'FROM'
      '  TIMEREGSCANNING'
      'WHERE'
      '  EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND'
      '  PROCESSED_YN = '#39'Y'#39' AND'
      '  DATETIME_IN BETWEEN :DATEFROM AND :DATETO'
      '  AND'
      '  ('
      
        '    (:CHECKDELETEDATE = 0) OR (:CHECKDELETEDATE = 1 AND DATETIME' +
        '_IN <> :DELETEDATE)'
      '  )'
      ''
      ''
      ' '
      ' '
      ' ')
    Left = 520
    Top = 152
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'CHECKDELETEDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'CHECKDELETEDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DELETEDATE'
        ParamType = ptUnknown
      end>
  end
  object qryLastScan: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  MAX(DATETIME_OUT) DATETIME_OUT'
      'FROM'
      '  TIMEREGSCANNING'
      'WHERE'
      '  EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND'
      '  PROCESSED_YN = '#39'Y'#39' AND'
      '  DATETIME_IN BETWEEN :DATEFROM and :DATETO'
      '  AND'
      '  ('
      
        '    (:CHECKDELETEDATE = 0) OR (:CHECKDELETEDATE = 1 AND DATETIME' +
        '_IN <> :DELETEDATE)'
      '  )'
      ''
      ''
      ' '
      ' '
      ' ')
    Left = 520
    Top = 208
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'CHECKDELETEDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'CHECKDELETEDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DELETEDATE'
        ParamType = ptUnknown
      end>
  end
end
