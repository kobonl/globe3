(*
   Changes:
     MRA:02-SEP-2008 RV9 (Revision 9)
       Changes for option to use an other database then PIMS/PIMS.
       This is read from an PIMSORA.INI-file in SystemDMT.
*)
unit DialogLoginDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DBTables, Db;

type

  TDialogLoginDM = class(TDataModule)
    LoginPims: TDatabase;
    TableUser: TTable;
    TableGroupMenu: TTable;
    TablePimsGroup: TTable;
    QueryCheckOpenApl: TQuery;
    QueryServerTime: TQuery;
    QueryServerTimeSERVERDATETIME: TDateTimeField;
    QueryServerTimeOrcl: TQuery;
    DateTimeField1: TDateTimeField;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure LoginPimsLogin(Database: TDatabase; LoginParams: TStrings);
  private
    { Private declarations }
    FApl_Menu_Login: Integer;
    FApl_Regroot_Path_Login: String;
  public
    { Public declarations }
    property Apl_Menu_Login: Integer read FApl_Menu_Login write FApl_Menu_Login;
    property Apl_Regroot_Path_Login: String read FApl_Regroot_Path_Login
      write FApl_Regroot_Path_Login;
    function Now: TDateTime;
  end;

var
  DialogLoginDM: TDialogLoginDM;

implementation
uses ULoginConst, SystemDMT, DialogLoginFRM, EncryptIt, UPimsConst;
{$R *.DFM}

procedure TDialogLoginDM.DataModuleCreate(Sender: TObject);
var
  I: Integer;
begin
  if (Application.Title = 'ADMINISTRATION - TOOL') or
    (Application.Title = 'ORCL - ADMINISTRATION - TOOL') then
  begin
    Apl_Menu_Login := APL_MENU_LOGIN_ADM;
    Apl_RegRoot_Path_Login := APL_REGROOT_PATH_LOGIN_ADM;
  end;
  if (Application.Title = 'PIMS - UNIT MAINTENANCE') or
    (Application.Title = 'ORCL - PIMS - UNIT MAINTENANCE') then
  begin
    Apl_Menu_Login := APL_MENU_LOGIN_PIMS;
    Apl_RegRoot_Path_Login := APL_REGROOT_PATH_LOGIN_PIMS;
  end;
  if Application.Title = 'PIMS - PRODUCTION SCREEN' then
  begin
    Apl_Menu_Login := APL_MENU_LOGIN_PRODSCREEN;
    Apl_RegRoot_Path_Login := APL_REGROOT_PATH_LOGIN_PRODSCREEN;
  end;

  // MRA:02-09-2008 Take params from Pims-database (SystemDMT), because
  //                database-login+password can be different then PIMS/PIMS.
  LoginPims.Connected := False;
  LoginPims.Params.Clear;
  for I := 0 to SystemDM.Pims.Params.Count - 1 do
    LoginPims.Params.Add(SystemDM.Pims.Params.Strings[I]);
  LoginPims.Connected := True;

  if TableUser.Exists then
    TableUser.Open;
  if TablePimsGroup.Exists then
    TablePIMSGroup.Open;
  if TableGroupMenu.Exists then
    TableGroupMenu.Open
  else
    Exit;

  QueryCheckOpenApl.Prepare;

  // set properties per applications

end;

procedure TDialogLoginDM.DataModuleDestroy(Sender: TObject);
begin
  QueryCheckOpenApl.Close;
  QueryCheckOpenApl.UnPrepare;
end;

function TDialogLoginDM.Now: TDateTime;
begin
(* ORACLE TEST *)
  // Pims -> Oracle
  QueryServerTimeOrcl.Open;
  Result := QueryServerTimeOrcl.FieldByName('SERVERDATETIME').AsDateTime;
  QueryServerTimeOrcl.Close;
end;

procedure TDialogLoginDM.LoginPimsLogin(Database: TDatabase;
  LoginParams: TStrings);
var
  I: Integer;
begin
  // MRA:02-09-2008 Take values from SystemDM, because they can be different
  //                then PIMS/PIMS !
  LoginParams.Clear;
  (* ORACLE TEST *)
  // Pims -> Oracle
  for I := 0 to SystemDM.Pims.Params.Count - 1 do
    LoginParams.Add(SystemDM.Pims.Params.Strings[I]);
end;

end.
