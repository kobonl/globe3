unit PimsFRM;
(*******************************************************************************
Unit     : FPims  TfrmPims = (TForm)
Function : Pims's parent form. All other forms are inherited from this
         : form.
Note     : -
Changes  : -
--------------------------------------------------------------------------------
Inheritance tree:
TForm
frmPims
  Changed design properties :
  + Font.Name := Tahoma (MicroSoft Outlook Style)
  + Position  := poScreenCenter
  + ShowHint  := True

  Added controls:
  -

  Events:
  + FormCreate: Update the progressbar on the splashscreen

  Methods:
  + procedure SetParent(AParent :TWinControl); override;
    : Allmost all Pims forms are placed on a panel in another form.
    : Only via this method it is posible to have multiple toolbars
    : and full control over a form within the other form.
*******************************************************************************)
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DBCtrls, Dblup1a, dxCntner, dxTL, dxDBELib,
  dxDBCtrl, dxDBGrid, StdCtrls, Dblup1b, dxEdLib, dbPicker, ComCtrls,
  dxExGrEd, dxExELib, dxDBEdtr, dxLayout;

type
  TPimsF = class(TForm)
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    procedure SetParent(AParent: TWinControl); override;
  public
    { Public declarations }
  end;

var
  PimsF: TPimsF;

implementation

uses
  SplashFRM, UPimsConst;

{$R *.DFM}
procedure SetColorComponents(FComponent: TComponent);
begin
  if not ((FComponent.ClassName = 'TEdit') or
    (FComponent.ClassName = 'TdxDBEdit') or
    (FComponent.ClassName = 'TDBLookupComboBox') or
    (FComponent.ClassName = 'TDBEdit') or
    (FComponent.ClassName = 'TDBComboBoxPlus') or
    (FComponent.ClassName = 'TdxDBDateEdit') or
    (FComponent.ClassName = 'TdxDBTimeEdit') or
    (FComponent.ClassName = 'TdxDBMaskEdit') or
    (FComponent.ClassName = 'TdxDBExtLookupEdit')) then
    Exit;

  if FComponent.Tag <> 0 then
    (FComponent as TWinControl).Brush.Color := clRequired;
  if not((FComponent as TWinControl).Enabled) then
    (FComponent as TWinControl).Brush.Color := clDisabled;
end;

procedure TPimsF.FormCreate(Sender: TObject);
var
  Counter: Integer;
  TempComponent: TComponent;
begin
  inherited;
  if Starting then
     SplashF.UpdateProgressBar(Self);
  { Set colours for edit components }
  { Save/Restore grid settings      }

   // TempComponent := Components[Counter];
  for Counter := 0 to (ComponentCount - 1) do
  begin
    TempComponent := Components[Counter];
    if (TempComponent is TDBEdit) then
    begin
      if (TempComponent as TDBEdit).Tag <> 0 then
        (TempComponent as TDBEdit).Color := clRequired;
      if not((TempComponent as TDBEdit).Enabled) then
        (TempComponent as TDBEdit).Color := clDisabled;
    end;
    if (TempComponent is TdxDBEdit) then
    begin
      if (TempComponent as TdxDBEdit).Tag <> 0 then
        (TempComponent as TdxDBEdit).Color := clRequired;
      if not((TempComponent as TdxDBEdit).Enabled) then
        (TempComponent as TdxDBEdit).Color := clDisabled;
    end;
    if (TempComponent is TdxEdit) then
    begin
      if (TempComponent as TdxEdit).Tag <> 0 then
        (TempComponent as TdxEdit).Color := clRequired;
      if not((TempComponent as TdxEdit).Enabled) then
        (TempComponent as TdxEdit).Color := clDisabled;
    end;
    if (TempComponent is TEdit) then
    begin
      if (TempComponent as TEdit).Tag <> 0 then
        (TempComponent as TEdit).Color := clRequired;
      if not((TempComponent as TEdit).Enabled) then
        (TempComponent as TEdit).Color := clDisabled;
    end;
     if (TempComponent is TComboBox) then
    begin
      if (TempComponent as TComboBox).Tag <> 0 then
        (TempComponent as TComboBox).Color := clRequired;
      if not((TempComponent as TComboBox).Enabled) then
        (TempComponent as TComboBox).Color := clDisabled;
    end;
    
    if (TempComponent is TDBLookupComboBox) then
    begin
      if (TempComponent as TDBLookupComboBox).Tag <> 0 then
      begin
        (TempComponent as TDBLookupComboBox).Color := clRequired;
      end;
      if not ((TempComponent as TDBLookupComboBox).Enabled) then
      begin
        (TempComponent as TDBLookupComboBox).Color := clDisabled;
      end;
    end;

    if (TempComponent is TDBComboBoxPlus) then
    begin
      if (TempComponent as TDBComboBoxPlus).Tag <> 0 then
      begin
        (TempComponent as TDBComboBoxPlus).Color := clRequired;
        (TempComponent as TDBComboBoxPlus).ListColor := clRequired;
      end;
    end;

    if (TempComponent is TDateTimePicker) then
    begin
      if (TempComponent as TDateTimePicker).Tag <> 0 then
      begin
        (TempComponent as TDateTimePicker).Color := clRequired;
        if not((TempComponent as TDateTimePicker).Enabled) then
        (TempComponent as TDateTimePicker).Color := clDisabled;
      end;
    end;

    if (TempComponent is TdxDBDateEdit) then
    begin
      if (TempComponent as TdxDBDateEdit).Tag <> 0 then
        (TempComponent as TdxDBDateEdit).Color := clRequired;
      if not((TempComponent as TdxDBDateEdit).Enabled) then
        (TempComponent as TdxDBDateEdit).Color := clDisabled;
    end;
     if (TempComponent is TdxDBMaskEdit) then
    begin
      if (TempComponent as TdxDBMaskEdit).Tag <> 0 then
        (TempComponent as TdxDBMaskEdit).Color := clRequired;
      if not((TempComponent as TdxDBMaskEdit).Enabled) then
        (TempComponent as TdxDBMaskEdit).Color := clDisabled;
    end;

    if (TempComponent is TdxDBTimeEdit) then
    begin
      if (TempComponent as TdxDBTimeEdit).Tag <> 0 then
        (TempComponent as TdxDBTimeEdit).Color := clRequired;
      if not((TempComponent as TdxDBTimeEdit).Enabled) then
        (TempComponent as TdxDBTimeEdit).Color := clDisabled;
    end;
    if (TempComponent is TdxDBLookupEdit) then
    begin
      if (TempComponent as TdxDBLookupEdit).Tag <> 0 then
        (TempComponent as TdxDBLookupEdit).Color := clRequired;
      if not((TempComponent as TdxDBLookupEdit).Enabled) then
        (TempComponent as TdxDBLookupEdit).Color := clDisabled;
    end;
    if (TempComponent is TdxDBTimeEdit) then
    begin
      if (TempComponent as TdxDBTimeEdit).Tag <> 0 then
        (TempComponent as TdxDBTimeEdit).Color := clRequired;
      if not((TempComponent as TdxDBTimeEdit).Enabled) then
        (TempComponent as TdxDBTimeEdit).Color := clDisabled;
    end;
    if (TempComponent is TdxDBSpinEdit) then
    begin
      if (TempComponent as TdxDBSpinEdit).Tag <> 0 then
        (TempComponent as TdxDBSpinEdit).Color := clRequired;
      if not((TempComponent as TdxDBSpinEdit).Enabled) then
        (TempComponent as TdxDBSpinEdit).Color := clDisabled;
    end;
     if (TempComponent is TdxSpinEdit) then
    begin
      if (TempComponent as TdxSpinEdit).Tag <> 0 then
        (TempComponent as TdxSpinEdit).Color := clRequired;
      if not((TempComponent as TdxSpinEdit).Enabled) then
        (TempComponent as TdxSpinEdit).Color := clDisabled;
    end;
    if (TempComponent is TDBPicker) then
    begin
      if (TempComponent as TDBPicker).Tag <> 0 then
        (TempComponent as TDBPicker).Color := clRequired;
      if not((TempComponent as TDBPicker).Enabled) then
        (TempComponent as TDBPicker).Color := clDisabled;
    end;
    if (TempComponent is TdxDBExtLookupEdit) then
    begin
      if (TempComponent as TdxDBExtLookupEdit).Tag <> 0 then
        (TempComponent as TdxDBExtLookupEdit).Color := clRequired;
      if not((TempComponent as TdxDBExtLookupEdit).Enabled) then
        (TempComponent as TdxDBExtLookupEdit).Color := clDisabled;
    end;
 // SetColorComponents(TempComponent);

    if (TempComponent is TdxDBGrid) then
    begin
    { Get registrypath for layoutsaving }
    { Save Design time settings for restore to factory }
      with (TempComponent as TdxDBGrid) do
      begin
        RegistryPath := PIMS_REGROOT_PATH_GRID
          + 'Design\'
          + (Sender as TForm).Name
          + '\' + (TempComponent as TdxDBGrid).Name;
        SaveToRegistry(RegistryPath);
      end;
  { Load User settings }
      with (TempComponent as TdxDBGrid) do
      begin
        try
          RegistryPath := PIMS_REGROOT_PATH_GRID
            + 'User\'
            + (Sender as TForm).Name
            + '\' + (TempComponent as TdxDBGrid).Name;
          LoadFromRegistry(RegistryPath);
        except
          // the last code was scrapped or invalid grouping
          RegistryPath := PIMS_REGROOT_PATH_GRID
            + 'Design\'
            + (Sender as TForm).Name
            + '\' + (TempComponent as TdxDBGrid).Name;
          LoadFromRegistry(RegistryPath);
        end;
      end;
     { with (TempComponent as TdxDBGrid) do
      begin
        RegistryPath := PIMS_REGROOT_PATH_GRID
          + 'Design\'
          + (Sender as TForm).Name
          + '\' + (TempComponent as TdxDBGrid).Name;
        SaveToRegistry(RegistryPath);
      end;}
    end;
  end;
end;

procedure TPimsF.SetParent(AParent: TWinControl);
begin
  inherited;
  if Assigned(AParent) then
  begin
    Position := poDefault;
    BorderIcons := [];
    BorderStyle := bsNone;
    SetBounds(0, 0, Parent.ClientWidth, Parent.ClientHeight);
  end;
end;

end.
