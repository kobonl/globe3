(*
  MRA:9-APR-2015 20016449
  - Time Zone Implementation
  - This allows a calculated TDateTimeField to be editable.
*)
unit UEditableCalcDateTimeField;

interface

uses
  db, classes;

type
  TEditableCalcDateTimeField = class(TDateTimeField)
  public
  protected
    function GetCanModify: Boolean; override;
  end;

implementation

function TEditableCalcDateTimeField.GetCanModify: Boolean;
begin
  Result := True;
end;

end.
