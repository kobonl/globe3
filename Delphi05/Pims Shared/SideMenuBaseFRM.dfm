inherited SideMenuBaseF: TSideMenuBaseF
  Left = 215
  Top = 138
  Width = 800
  Height = 600
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlImageBase: TPanel
    Width = 784
    TabOrder = 3
    inherited imgOrbit: TImage
      Left = 667
    end
  end
  inherited pnlInsertBase: TPanel
    Width = 784
    Height = 481
    Align = alClient
  end
  inherited stbarBase: TStatusBar
    Top = 542
    Width = 784
    Panels = <
      item
        Text = ' Gotli Labs'
        Width = 200
      end
      item
        Width = 450
      end
      item
        Width = 50
      end>
    SimplePanel = False
    SimpleText = '  Gotli Labs'
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        IsMainMenu = True
        ItemLinks = <
          item
            Item = dxBarSubItemFile
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarSubItemHelp
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 26
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 1
        UseOwnFont = False
        Visible = False
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    Left = 416
    Top = 128
    DockControlHeights = (
      0
      0
      24
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarButtonResetColumns: TdxBarButton
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFDFBF9FBFAF8FBFAF8FBFAF8FBFAF8FCFAF8FBFAF8FB
        FAF8FBFAF8FBFAF8FCFAF8FBFAF8FBFAF8FBFAF8FBFAF8FBFAF8946D31946D31
        946D31946D31946D31946D31946D31946D31946D31946D31946D31946D31946D
        31946D31946D31946D31946D31FEE8A8FEE8A8FEE8A8FEE8A8946D31FEE8A8FE
        E8A8FEE8A8FEE8A8946D31FEE8A8FEE8A8FEE8A8FEE8A8946D31946D31FFF0AF
        FFF0AFFFF0AFFFF0AF946D31FFF0AFFFF0AFFFF0AFFFF0AF946D31FFF0AFFFF0
        AFFFF0AFFFF0AF946D31946D31946D31946D31946D31946D31946D31946D3194
        6D31946D31946D31946D31946D31946D31946D31946D31946D31946D31FFF0AF
        FFF0AFFFF0AFFFF0AF946D31FFF0AFFFF0AFFFF0AFFFF0AF946D31FFF0AFFFF0
        AFFFF0AFFFF0AF946D31946D31FFF9D4FFF9D4FFF8D3FFFCD8946D31FFF9D4FF
        F9D4FFF8D3FFFCD7946D31FFF9D4FFF8D3FFF7D2FFFEDA946D31946D31946D31
        946D31946D31946D31946D31946D31946D31946D31946D31946D31946D31946D
        31946D31946D31946D31946D31F9F2D6F9F2D6F9F1D5F9F5DA946D31F9F2D6F9
        F2D6F9F1D5F9F5DA946D31F9F2D6F9F2D6F9F1D5F9F7DD946D31946D31FFFFF1
        FFFFF1FFFEF0FFFFF6946D31FFFFF1FFFFF2FFFFF0FFFFF6946D31FFFFF1FFFF
        F2FFFFF0FFFFFA946D31946D3100EA0000EA0000EA0000EA00FFFFF200EA0000
        EA0000EA0000EA00FFFFF200EA0000EA0000EA0000EA00946D31946D3100EA00
        00EA0000EA0000EA00FFFFF200EA0000EA0000EA0000EA00FFFFF200EA0000EA
        0000EA0000ED00946D31946D31946D31946D31946D31946D31946D31946D3194
        6D31946D31946D31946D31946D31946D31946D31946D31946D31FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
    end
  end
  inherited dxBarDBNavigator: TdxBarDBNavigator
    Left = 624
    Top = 136
  end
  object imgListBase: TImageList
    Height = 48
    Width = 48
    Left = 8
    Top = 144
  end
end
