object SystemDM: TSystemDM
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Left = 245
  Top = 97
  Height = 579
  Width = 696
  object SessionPims: TSession
    NetFileDir = 'C:\'
    SessionName = 'SessionPims'
    Left = 88
    Top = 24
  end
  object Pims: TDatabase
    AliasName = 'PimsDBOR'
    DatabaseName = 'Pims'
    LoginPrompt = False
    Params.Strings = (
      'USER NAME=PIMS'
      'PASSWORD=PIMS')
    SessionName = 'SessionPims'
    Left = 200
    Top = 24
  end
  object OnePims: TOneSolar
    Left = 304
    Top = 24
  end
  object TableDay: TTable
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    TableName = 'DAYTABLE'
    Left = 88
    Top = 88
    object TableDayDAY_NUMBER: TIntegerField
      FieldName = 'DAY_NUMBER'
    end
    object TableDayDAY_CODE: TStringField
      FieldName = 'DAY_CODE'
      Required = True
      Size = 2
    end
    object TableDayDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
    end
  end
  object DataSourceDay: TDataSource
    DataSet = TableDay
    Left = 200
    Top = 88
  end
  object TablePIMSettings: TTable
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    TableName = 'PIMSSETTING'
    Left = 304
    Top = 88
    object TablePIMSettingsSAVE_DAY_PRD_INFO: TIntegerField
      FieldName = 'SAVE_DAY_PRD_INFO'
    end
    object TablePIMSettingsSAVE_DETAIL_PRD_INFO: TIntegerField
      FieldName = 'SAVE_DETAIL_PRD_INFO'
    end
    object TablePIMSettingsUNIT_WEIGHT: TStringField
      FieldName = 'UNIT_WEIGHT'
      Required = True
      Size = 6
    end
    object TablePIMSettingsUNIT_PIECES: TStringField
      FieldName = 'UNIT_PIECES'
      Required = True
      Size = 6
    end
    object TablePIMSettingsWEEK_START_ON: TIntegerField
      FieldName = 'WEEK_START_ON'
    end
    object TablePIMSettingsSETTING_PASSWORD: TStringField
      FieldName = 'SETTING_PASSWORD'
      Required = True
      Size = 30
    end
    object TablePIMSettingsVERSION_NUMBER: TIntegerField
      FieldName = 'VERSION_NUMBER'
    end
    object TablePIMSettingsCHANGE_SCAN_YN: TStringField
      FieldName = 'CHANGE_SCAN_YN'
      Size = 1
    end
    object TablePIMSettingsREAD_PLANNING_ONCE_YN: TStringField
      FieldName = 'READ_PLANNING_ONCE_YN'
      Size = 1
    end
    object TablePIMSettingsYEARS_FOR_SICK_PAY: TIntegerField
      FieldName = 'YEARS_FOR_SICK_PAY'
    end
    object TablePIMSettingsHOURTYPE_SICK_PAY: TIntegerField
      FieldName = 'HOURTYPE_SICK_PAY'
    end
    object TablePIMSettingsMINUTES_SICK_PAY: TIntegerField
      FieldName = 'MINUTES_SICK_PAY'
    end
    object TablePIMSettingsMAX_MINUTES_SICK_PAY: TIntegerField
      FieldName = 'MAX_MINUTES_SICK_PAY'
    end
    object TablePIMSettingsFILLPRODUCTIONHOURS_YN: TStringField
      FieldName = 'FILLPRODUCTIONHOURS_YN'
      Size = 1
    end
    object TablePIMSettingsDATACOL_PATH: TStringField
      FieldName = 'DATACOL_PATH'
      Size = 128
    end
    object TablePIMSettingsDATACOL_READFILES_INTERVAL: TIntegerField
      FieldName = 'DATACOL_READFILES_INTERVAL'
    end
    object TablePIMSettingsDATACOL_PRESET_STARTTIME: TDateTimeField
      FieldName = 'DATACOL_PRESET_STARTTIME'
    end
    object TablePIMSettingsDATACOL_TIME_INTERVAL: TIntegerField
      FieldName = 'DATACOL_TIME_INTERVAL'
    end
    object TablePIMSettingsDELETE_WK_YN: TStringField
      FieldName = 'DELETE_WK_YN'
      Size = 1
    end
    object TablePIMSettingsFIRST_WEEK_OF_YEAR: TIntegerField
      FieldName = 'FIRST_WEEK_OF_YEAR'
      Required = True
    end
    object TablePIMSettingsEMPLOYEES_ACROSS_PLANTS_YN: TStringField
      FieldName = 'EMPLOYEES_ACROSS_PLANTS_YN'
      Size = 1
    end
    object TablePIMSettingsCODE: TStringField
      FieldName = 'CODE'
      Size = 3
    end
    object TablePIMSettingsEMBED_DIALOGS_YN: TStringField
      FieldName = 'EMBED_DIALOGS_YN'
      Size = 1
    end
    object TablePIMSettingsABSLOG_EMPHRS_YN: TStringField
      FieldName = 'ABSLOG_EMPHRS_YN'
      Size = 1
    end
    object TablePIMSettingsABSLOG_TRS_YN: TStringField
      FieldName = 'ABSLOG_TRS_YN'
      Size = 1
    end
    object TablePIMSettingsEFF_BO_QUANT_YN: TStringField
      FieldName = 'EFF_BO_QUANT_YN'
      Size = 1
    end
    object TablePIMSettingsPS_AUTO_CLOSE_SECS: TIntegerField
      FieldName = 'PS_AUTO_CLOSE_SECS'
    end
    object TablePIMSettingsUPDATE_EMP_YN: TStringField
      FieldName = 'UPDATE_EMP_YN'
      Size = 1
    end
    object TablePIMSettingsUPDATE_PLANT_YN: TStringField
      FieldName = 'UPDATE_PLANT_YN'
      Required = True
      Size = 1
    end
    object TablePIMSettingsUPDATE_WK_YN: TStringField
      FieldName = 'UPDATE_WK_YN'
      Size = 1
    end
    object TablePIMSettingsDELETE_PLANT_YN: TStringField
      FieldName = 'DELETE_PLANT_YN'
      Size = 1
    end
    object TablePIMSettingsDELETE_EMP_YN: TStringField
      FieldName = 'DELETE_EMP_YN'
      Size = 1
    end
    object TablePIMSettingsUPDATE_DELETE_EMP_YN: TStringField
      FieldName = 'UPDATE_DELETE_EMP_YN'
      Size = 1
    end
    object TablePIMSettingsUPDATE_DELETE_PLANT_YN: TStringField
      FieldName = 'UPDATE_DELETE_PLANT_YN'
      Size = 1
    end
    object TablePIMSettingsDATACOL_STORE_QTY_5MIN_PERIOD: TStringField
      FieldName = 'DATACOL_STORE_QTY_5MIN_PERIOD'
      Size = 1
    end
    object TablePIMSettingsSHOW_WEEKNUMBERS_YN: TStringField
      FieldName = 'SHOW_WEEKNUMBERS_YN'
      Size = 1
    end
    object TablePIMSettingsSHIFTDATESYSTEM_YN: TStringField
      FieldName = 'SHIFTDATESYSTEM_YN'
      Size = 1
    end
    object TablePIMSettingsFINAL_RUN_YN: TStringField
      FieldName = 'FINAL_RUN_YN'
      Size = 1
    end
    object TablePIMSettingsENABLE_MACHINETIMEREC_YN: TStringField
      FieldName = 'ENABLE_MACHINETIMEREC_YN'
      Size = 4
    end
    object TablePIMSettingsWORKSPOT_INCL_SEL_YN: TStringField
      FieldName = 'WORKSPOT_INCL_SEL_YN'
      Size = 4
    end
    object TablePIMSettingsDATETIME_SEL_YN: TStringField
      FieldName = 'DATETIME_SEL_YN'
      Size = 4
    end
    object TablePIMSettingsINCL_OPEN_SCANS_YN: TStringField
      FieldName = 'INCL_OPEN_SCANS_YN'
      Size = 4
    end
    object TablePIMSettingsERROR_LOG_TO_DB_YN: TStringField
      FieldName = 'ERROR_LOG_TO_DB_YN'
      Size = 4
    end
    object TablePIMSettingsTIMEREC_IN_SECONDS_YN: TStringField
      FieldName = 'TIMEREC_IN_SECONDS_YN'
      Size = 4
    end
    object TablePIMSettingsIGNORE_BREAKS_YN: TStringField
      FieldName = 'IGNORE_BREAKS_YN'
      Size = 4
    end
    object TablePIMSettingsTIMEZONE: TMemoField
      FieldName = 'TIMEZONE'
      BlobType = ftMemo
      Size = 400
    end
    object TablePIMSettingsPSSHOWEMPEFF_YN: TStringField
      FieldName = 'PSSHOWEMPEFF_YN'
      Size = 4
    end
    object TablePIMSettingsPSSHOWEMPINFO_YN: TStringField
      FieldName = 'PSSHOWEMPINFO_YN'
      Size = 4
    end
    object TablePIMSettingsJOBCOMMENT_YN: TStringField
      FieldName = 'JOBCOMMENT_YN'
      Size = 4
    end
    object TablePIMSettingsPACKAGE_HOUR_CALC_YN: TStringField
      FieldName = 'PACKAGE_HOUR_CALC_YN'
      Size = 4
    end
    object TablePIMSettingsBREAK_BTN_YN: TStringField
      FieldName = 'BREAK_BTN_YN'
      Size = 4
    end
    object TablePIMSettingsLUNCH_BTN_YN: TStringField
      FieldName = 'LUNCH_BTN_YN'
      Size = 4
    end
    object TablePIMSettingsTR_EOD_BTN_YN: TStringField
      FieldName = 'TR_EOD_BTN_YN'
      Size = 4
    end
    object TablePIMSettingsPS_EOD_BTN_YN: TStringField
      FieldName = 'PS_EOD_BTN_YN'
      Size = 4
    end
    object TablePIMSettingsMANREG_EOD_BTN_YN: TStringField
      FieldName = 'MANREG_EOD_BTN_YN'
      Size = 4
    end
    object TablePIMSettingsEXTERN_QTY_FROM_PQ_YN: TStringField
      FieldName = 'EXTERN_QTY_FROM_PQ_YN'
      Size = 4
    end
    object TablePIMSettingsLUNCH_BTN_SCANOUT_YN: TStringField
      FieldName = 'LUNCH_BTN_SCANOUT_YN'
      Size = 4
    end
    object TablePIMSettingsHYBRID: TIntegerField
      FieldName = 'HYBRID'
    end
    object TablePIMSettingsUPDATE_EMPNR_YN: TStringField
      FieldName = 'UPDATE_EMPNR_YN'
      Size = 4
    end
    object TablePIMSettingsPRODHRS_NOSUBTRACT_BREAKS: TStringField
      FieldName = 'PRODHRS_NOSUBTRACT_BREAKS'
      Size = 4
    end
    object TablePIMSettingsMAX_TIMEBLOCKS: TIntegerField
      FieldName = 'MAX_TIMEBLOCKS'
    end
    object TablePIMSettingsTIMERECCR_DONOTSCANOUT_YN: TStringField
      FieldName = 'TIMERECCR_DONOTSCANOUT_YN'
      Size = 4
    end
    object TablePIMSettingsOVERTIME_PERDAY_YN: TStringField
      FieldName = 'OVERTIME_PERDAY_YN'
      Size = 4
    end
    object TablePIMSettingsREPPLAN_STARTEND_TIMES_YN: TStringField
      FieldName = 'REPPLAN_STARTEND_TIMES_YN'
      Size = 4
    end
    object TablePIMSettingsOCCUP_HANDLING: TIntegerField
      FieldName = 'OCCUP_HANDLING'
    end
  end
  object TableWorkStation: TTable
    BeforePost = TableWorkStationBeforePost
    BeforeDelete = DefaultBeforeDelete
    OnPostError = DefaultPostError
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    TableName = 'WORKSTATION'
    Left = 88
    Top = 168
    object TableWorkStationCOMPUTER_NAME: TStringField
      FieldName = 'COMPUTER_NAME'
      Required = True
      Size = 32
    end
    object TableWorkStationLICENSEVERSION: TIntegerField
      FieldName = 'LICENSEVERSION'
    end
    object TableWorkStationTIMERECORDING_YN: TStringField
      FieldName = 'TIMERECORDING_YN'
      Size = 1
    end
    object TableWorkStationSHOW_HOUR_YN: TStringField
      FieldName = 'SHOW_HOUR_YN'
      Size = 1
    end
    object TableWorkStationHIDE_ID_YN: TStringField
      FieldName = 'HIDE_ID_YN'
      Size = 1
    end
    object TableWorkStationCARDREADER_DELAY: TFloatField
      FieldName = 'CARDREADER_DELAY'
    end
    object TableWorkStationMANREGS_MULTJOBS_YN: TStringField
      FieldName = 'MANREGS_MULTJOBS_YN'
      Size = 4
    end
    object TableWorkStationPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Size = 24
    end
  end
  object DataSourceWorkStation: TDataSource
    DataSet = TableWorkStation
    Left = 200
    Top = 168
  end
  object TableTmpPivot: TTable
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    Left = 88
    Top = 248
  end
  object TableTaylor: TTable
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    Left = 424
    Top = 88
  end
  object TableExportPayroll: TTable
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    Left = 424
    Top = 24
  end
  object QueryServerTime: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  CURRENT_TIMESTAMP AS SERVERDATETIME '
      'FROM '
      '  RDB$DATABASE')
    Left = 336
    Top = 176
    object QueryServerTimeSERVERDATETIME: TDateTimeField
      FieldName = 'SERVERDATETIME'
    end
  end
  object ClientDataSetDay: TClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <
      item
        Name = 'byDAYNUMBER'
        Fields = 'DAY_NUMBER'
      end>
    IndexName = 'byDAYNUMBER'
    Params = <>
    ProviderName = 'DataSetProviderDay'
    StoreDefs = True
    Left = 448
    Top = 176
  end
  object DataSetProviderDay: TDataSetProvider
    DataSet = TableDay
    Constraints = True
    Left = 448
    Top = 232
  end
  object QueryUpdatePims: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    ParamCheck = False
    Left = 208
    Top = 248
  end
  object QueryServerTimeOrcl: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  SYSDATE AS SERVERDATETIME '
      'FROM '
      '  DUAL'
      ' ')
    Left = 336
    Top = 232
    object DateTimeField1: TDateTimeField
      FieldName = 'SERVERDATETIME'
    end
  end
  object qryInitSession: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'ALTER SESSION SET NLS_SORT=BINARY')
    Left = 88
    Top = 312
  end
  object qryPlantDeptTeam: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT T.PLANT_CODE, T.DEPARTMENT_CODE, T.TEAM_CODE'
      'FROM DEPARTMENTPERTEAM T'
      'WHERE'
      '  ('
      '    (:USER_NAME = '#39'*'#39') OR'
      '    (T.TEAM_CODE IN'
      
        '      (SELECT TU.TEAM_CODE FROM TEAMPERUSER TU WHERE TU.USER_NAM' +
        'E = :USER_NAME))'
      '  )'
      'ORDER BY T.PLANT_CODE, T.DEPARTMENT_CODE'
      ' '
      ' ')
    Left = 336
    Top = 296
    ParamData = <
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end>
  end
  object qryTemp: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    Left = 168
    Top = 312
  end
  object qryPimsMenuGroup: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    Left = 448
    Top = 296
  end
  object qryPimsMenuGroup2: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    Left = 448
    Top = 344
  end
  object qryFinalRun: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  PLANT_CODE,'
      '  EXPORT_TYPE,'
      '  EXPORT_DATE'
      'FROM'
      '  FINALRUN'
      'WHERE'
      '  PLANT_CODE = :PLANT_CODE AND'
      '  EXPORT_TYPE = :EXPORT_TYPE')
    Left = 88
    Top = 360
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'EXPORT_TYPE'
        ParamType = ptUnknown
      end>
  end
  object qryFinalRunInsert: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'INSERT INTO FINALRUN'
      
        '(PLANT_CODE, EXPORT_TYPE, EXPORT_DATE, CREATIONDATE, MUTATIONDAT' +
        'E, MUTATOR)'
      'VALUES'
      
        '(:PLANT_CODE, :EXPORT_TYPE, :EXPORT_DATE, sysdate, sysdate, :MUT' +
        'ATOR)'
      ''
      '')
    Left = 168
    Top = 360
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'EXPORT_TYPE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'EXPORT_DATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MUTATOR'
        ParamType = ptUnknown
      end>
  end
  object qryFinalRunUpdate: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'UPDATE FINALRUN'
      'SET'
      '  EXPORT_DATE = :EXPORT_DATE,'
      '  MUTATIONDATE = SYSDATE,'
      '  MUTATOR = :MUTATOR'
      'WHERE'
      '  PLANT_CODE = :PLANT_CODE AND'
      '  EXPORT_TYPE = :EXPORT_TYPE'
      ' ')
    Left = 264
    Top = 360
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'EXPORT_DATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MUTATOR'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'EXPORT_TYPE'
        ParamType = ptUnknown
      end>
  end
  object qryPlantExportType: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  P.PLANT_CODE,'
      
        '  NVL(C.EXPORT_TYPE, (SELECT MIN(EP.EXPORT_TYPE) FROM EXPORTPAYR' +
        'OLL EP)) EXPORT_TYPE'
      'FROM PLANT P INNER JOIN COUNTRY C ON'
      '  P.COUNTRY_ID = C.COUNTRY_ID'
      'WHERE'
      '  P.PLANT_CODE = :PLANT_CODE'
      '')
    Left = 368
    Top = 360
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end>
  end
  object qryEmpExportType: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  P.PLANT_CODE,'
      
        '  NVL(C.EXPORT_TYPE, (SELECT MIN(EP.EXPORT_TYPE) FROM EXPORTPAYR' +
        'OLL EP)) EXPORT_TYPE'
      'FROM '
      '  EMPLOYEE E INNER JOIN PLANT P ON'
      '    E.PLANT_CODE = P.PLANT_CODE'
      '  INNER JOIN COUNTRY C ON'
      '    P.COUNTRY_ID = C.COUNTRY_ID'
      'WHERE'
      '  E.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER')
    Left = 368
    Top = 416
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end>
  end
  object qryABSLogInsert: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'INSERT INTO ABSLOG'
      '('
      '  COMPUTER_NAME,'
      '  ABSLOG_TIMESTAMP,'
      '  LOGSOURCE,'
      '  PRIORITY,'
      '  SYSTEMUSER,'
      '  LOGMESSAGE'
      ')'
      'VALUES'
      '('
      '  :COMPUTER_NAME,'
      '  SYSDATE,'
      '  :LOGSOURCE,'
      '  :PRIORITY,'
      '  :SYSTEMUSER,'
      '  :LOGMESSAGE'
      ')'
      ''
      ' '
      ' ')
    Left = 88
    Top = 424
    ParamData = <
      item
        DataType = ftString
        Name = 'COMPUTER_NAME'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'LOGSOURCE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'PRIORITY'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'SYSTEMUSER'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'LOGMESSAGE'
        ParamType = ptUnknown
      end>
  end
  object qryPlant: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT PLANT_CODE, TIMEZONE'
      'FROM PLANT')
    Left = 168
    Top = 424
  end
  object qryPlantUpdate: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'UPDATE PLANT'
      'SET TIMEZONEHRSDIFF = :TIMEZONEHRSDIFF,'
      '  MUTATIONDATE = SYSDATE'
      'WHERE PLANT_CODE = :PLANT_CODE'
      ''
      ' ')
    Left = 240
    Top = 424
    ParamData = <
      item
        DataType = ftInteger
        Name = 'TIMEZONEHRSDIFF'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end>
  end
  object cdsTimeZone: TClientDataSet
    Aggregates = <>
    IndexFieldNames = 'TIMEZONE'
    Params = <>
    Left = 472
    Top = 424
    object cdsTimeZoneTIMEZONE: TStringField
      FieldName = 'TIMEZONE'
      Size = 100
    end
  end
  object qryPlantFind: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      
        'SELECT PLANT_CODE, TIMEZONE, NVL(TIMEZONEHRSDIFF, 0) TIMEZONEHRS' +
        'DIFF'
      'FROM PLANT'
      'WHERE PLANT_CODE = :PLANT_CODE'
      ' ')
    Left = 304
    Top = 440
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end>
  end
end
