(*
   Changes:
     MRA:28-OCT-2008 RV13 (Revision 13)
       Changes for option to use an other database then PIMS/PIMS.
       This is read from an PIMSORA.INI-file in SystemDMT.
     MRA:2-MAR-2009 RV023. (Revision 23)
       Changes: property added for get/set of ExportPayrollType.
     MRA:6-MAY-2009 RV026. (Revision 26)
       Add export payroll for REINO.
     MRA:12-JAN-2010. RV050.8. 889955.
     - Restrict plants using teamsperuser.
     MRA:17-FEB-2010. ORA11. Oracle 11 Test.
     - Set default pims-database user/password to PIMS/PIMS,
       instead of PIMS/Pims.
       Oracle 11 is case-sentive!
     - Set NetFileDir earlier to current folder of application, to prevent
       it is using C:\ instead! This can give problems using Vista or Windows 7.
    SO: 08-JUN-2010 REV065.7
    - added two new queries to update the ilness messages and shift schedule
    tables when the employee end date is filled
    SO: 15-JUN-2010 RV066.1. 550491
    - tailor made non-scanners
    SO:08-JUL-2010 RV067.4. 550497
    SO:04-AUG-2010 RV067.5. 550497
      Export Attentia
    MRA:27-AUG-2010 RV067.MRA.11. Bugfix.
    - For certain DB-errors, only the error-code was shown, but try to also
      show the error-message.
    MRA:29-SEP-2010 RV069.1. 550494. User Rights bugfix.
    - Added filter-procedure to filter not only on Plant but also on
      Plant + Department.
    MRA:11-OCT-2010 RV071.13. 550497 Bugfixing.
    - Added sorting on hourtypes.
    MRA:21-OCT-2010. RV073.5. Access rights.
    - LoginUser-function added for teams-per-user use.
    MRA:1-NOV-2010. RV075.10. Tailor made non-scanners. RFL.
    - IsRFL function added to determine tailor-made for RFL.
    MRA:8-NOV-2010. RV077.5.
    - Addition of function that determines export type ATTENT.
    MRA:1-DEC-2010. RV082.5.
    - Store ContractgroupCode for selected employee in memory,
      to use in Contractgroup-related dialogs, so it can
      be looked up when showing these dialogs.
    MRA:28-JAN-2011 RV085.19. SC-50015401. SR-20011412. CVL (tailor made).
    - Add option to see if database is from CVL (PIMSSETTING.CODE = 'CVL').
    MRA:10-FEB-2011 RV086.2.
    - Add option to filter on PLANT + TEAM-combination.
    MRA:22-FEB-2011 RV087.2.
    - Addition of export payroll AFAS.
    MRA:24-FEB-2011 RV087.3. Dialog Login Change
    - Dialog Login
      - The Dialog Login is kept open all the time,
        because of 2 variables:
        - UserTeamSelectionYN
        - LoginUser
      - This must be changed by storing them in SystemDM.
  MRA:8-APR-2011 RV089.1. SC-20011561.10 - Remote Apps problem
  - Make it possible to embed dialogs in Pims, instead of
    open them as a modal dialog.
  - Added PIMSSETTING-field: EMBED_DIALOG and property EmbedDialog.
    When 'Y'/True then embed, otherwise not (default).
  JVL:14-APR-2011. RV089. SO-550532
  - Memorize selected plant
  MRA:21-APR-2011. RV089.1.
  - Addition of DisplayMessage4Buttons. This is a dialog that
    shows 4 buttons: Yes, No, All, No To All.
  MRA:27-APR-2011. RV091.
  - Renamed LoginUser to UserTeamLoginUser to prevent mistakes
    when it should be CurrentLoginUser.
  MRA:27-APR-2011. RV01.3.
  - Added FutureDate-function, that returns a fixed futuredate.
  MRA:4-MAY-2011. RV092.8. Addition.
  - Added IsTLB for this order.
  MRA:1-JUN-2011. RV093.2. SO-20011561.
  - Make 'embed dialogs' configurable using 'Pims Settings Dialog'.
  MRA:1-JUN-2011. RV093.3. SO-20011698.
  - Make 'log employee hours' and 'log timerecording scans' configurable
    using 'Pims Settings Dialog'.
  MRA:4-JUL-2011. RV094.4.
  - Workspots per Employee Dialog
    - During testing of VOS-database, this gave error:
      General SQL error
      ORA-00902 Invalid datatype
  - CAUSE: The TDatabase-settings were: pims / PIMS for
    database-user and database-password. This has been changed to:
    PIMS / PIMS. And this gave the error in 'workspots per employee'-dialog.
  MRA:10-APR-2012. 20012858.
  - Addition of pims-setting: EFF_BO_QUANT_YN: Eff. based on quantity (or time).
  MRA:28-JUN-2012. 20012858.
  - Addition of pims-setting: PSAutoCloseSecs.
  MRA:2-NOV-2012 20013723
  - Addition of IsCLF-property.
  MRA:19-NOV-2012 20013288
  - Addition of export payroll Easy Labor (ELA)
  MRA:4-FEB-2013 20013923
  - Make separate settings for update/delete for plant/employee/workspot
    - Existing properties are:
      - UpdatePlantYN
      - UpdateEmployeeYN
      - DeleteWKYN
    - Extra properties added:
      - DeletePlantYN
      - DeleteEmployeeYN
      - UpdateWKYN
  MRA:12-MAR-2013 20014036
  - Use Pims-login-account for storing scan-mutations (BCH-specific)
    - Addition of IsBCH that can be used for BCH-specific changes.
  MRA:1-MAY-2013 20012944
  - Show weeknumbers
    - Addition of extra PIMSSETTING named SHOW_WEEKNUMBERS_YN.
    - The following classes are overridden here:
      - TDateTimePicker
      - TMonthCalendar
      - TDBPicker
      - To use these in a form/dialog:
        - unit SystemDMT must be added as last unit in first uses-line.
  MRA:2-OCT-2013 TD-23247
  - Addition of new WORKSTATION-field CARDREADER_DELAY that can be
    used to slow done the reading of cards via contactless card
    readers in case the reading is not reliable. This value is in
    milliseconds.
  - Added Wait-procedure.
  MRA:10-OCT-2013 20014327
  - Make use of Overnight-Shift-System optional.
    - Addition of field SHIFTDATESYSTEM_YN to PIMSSETTING-table.
    - The contents of this field can only be changed by script.
  MRA:18-OCT-2013 20014714
  - Addition of export payroll for WMU.
  MRA:29-OCT-2013 20014715
  - Addition of 'IsWMU' to make 'report incentive program' only available for
    WMU.
  MRA:6-FEB-2014 20011800 Final Run System
  - Addition of PIMSSETTING: FINAL_RUN_YN and property UseFinalRun
  - Addition of IsAdmin to see if the current user of type System Administrator.
  - Addition of SysAdmin-property to indicate if a user is of type
    System Administrator.
  MRA:26-FEB-2014 20014442
  - Addition of IsVOS for custom-made-solutions.
  MRA:30-MAY-2014 20015178
  - Measure Performance without employees
    - Addition of PIMSSETTING: ENABLE_MACHINETIMEREC_YN
  MRA:6-JUN-2014 20015223
  - Productivity reports and grouping on workspots
    - Addition of PIMSSETTING: WORKSPOT_INCL_SEL_YN
  MRA:18-JUN-2014 20015520
  - Productivity reports and selection on time
  MRA:18-JUN-2014 20015521
  - Productivity reports and include open scans
  MRA:14-JUL-2014 20015302
  - Addition of workstation-setting named MANREGS_MULTJOBS_YN to make
    the manual registrations of multiple jobs optional.
  MRA:22-JUL-2014 TD-25352
  - Host name too long: Extend clientname/computer from 15 to 32 chars.
  MRA:31-OCT-2014 20011800.80
  - Disable final run system
  MRA:10-NOV-2014 20014826
  - Log error messages optionally to database.
  - Add Param 'BLOBS TO CACHE=-1' to prevent errors with memo-fields in queries,
    during scrolling. Which happens with Error-log-table.
  MRA:16-MAR-2015 20015346
  - For Personal Screen + Timerecording-application:
    - Store scans in seconds
    - Cut off time and breaks handling
    - Creation-date and Mutation-date handling for scans
  - For Pims:
    - Store manual scans without seconds (no changes needed)
    - Do not show scan-seconds in reports/dialogs
  - Addition of 2 Pims-Settings (add to System Settings-dialog)
    - TimerecordingInSecs
    - IgnoreBreaks
  MRA:1-APR-2015 SO-20016449
  - Time zone implementation
  MRA:6-MAY-2015 SO-20014450.50
  - Always calculate eff. based on time
  - Addition of 2 settings for Personal Screen:
    - PSShowEmpInfo
    - PSShowEmpEff
  MRA:21-JUL-2015 PIM-50
  - Addition of job comments (system setting).
  MRA:14-SEP-2015 PIM-87
  - Addition of pims-setting UsePackageHourCalc (boolean). When true then
    it will use a package that handles hour calculations.
  MRA:28-SEP-2015 PIM-12
  - Addition of system setings to optionally show Break, Lunch
    and End-Of-Day-buttons.
  - Disabled 'Date time selection' and 'Open scans' (always False).
  MRA:30-SEP-2015 PIM-53
  - Related to this order: bugfix for: DisplayMessage4Buttons
  MRA:9-OCT-2015 PIM-90
  - Adapt ABSSolute Script Quantities to PIMS for Personal Screen
  - Added pimssetting named ExternQtyFromPQ (PIMSSETTING.EXTERN_QTY_FROM_PQ_YN)
  MRA:27-JAN-2016 PIM-139
  - Lunch button optionally scan out.
  MRA:19-APR-2016 ABS-27052
  - Default printer handling
  MRA:19-MAY-2016 PIM-180
  - Addition of pims-setting-field HYBRID (0=none 1=hybrid 2=future mode)
  - Property is called HYBRIDMODE.
  - This is a read-only property. It can only be changed via a script.
  - When HYBRID=1 then a hybrid-version can be implemented/used for
    the COCKPIT-software.
  MRA:30-SEP-2016 PIM-227
  - Make change of employee number optional.
  MRA:23-OCT-2017 PIM-319
  - Do not subtract breaks from production hours.
  MRA:23-APR-2018 GLOB3-94
  - Problem when 2 users work in planning dialogs
  - Related to this: Add option to run Pims multiple times,
    but only for DEBUG-version (PimsORCL_DEBUG.exe).
  - Added try-except-line for record/key deleted.
  MRA:19-FEB-2018 GLOB3-72
  - Export Payroll NTS
  MRA:23-FEB-2018 GLOB3-86
  - Changes for hours per employee report
  - Addition of IsNTS-property to make this custom-made.
  MRA:15-JUN-2018 GLOB3-60
  - Extension 4 to 10 blocks for planning
  - Added pimssetting (MaxTimeblocks) that can be 4 or 10.
  MRA:31-AUG-2018 GLOB3-60
  - Extension 4 to 10 blocks for planning
  - It can happen the setting for MaxTimeblocks is back to 4 again!
  - There was a bug in 'AssignSettings' that did this, meaning each time you
    used Configuration-dialog, it changed this setting back to 4.
  MRA:4-JAN-2019 GLOB3-202
  - Scan via machine-card readers and do not scan out
  MRA:8-FEB-2019 GLOB3-234
  - Export Payroll Quickbooks
  MRA:22-FEB-2019 GLOB3-223
  - Restructure availability of functionality made for NTS
  MRA:1-APR-2019 PIM-377
  - Show start and end times in planning reports
  MRA:15-APR-2019 GLOB3-271
  - Export payroll to Paychex
  MRA:26-APR-2019 GLOB3-297
  - Export payroll Navision
  MRA:27-MAY-2019 GLOB3-284
  - Week start at Wednesday
  MRA:8-JUL-2019 GLOB3-330
  - Make functionality with 'occupied' optional
  MRA:6-SEP-2019 GLOB3-342
  - Issue scanningen dekenhal
  - When using team-filtering it goes wrong for report checklist scans.
  - This filters on plant of TR-record, which related to the plant of the
    workspot where the employee worked on, but this can be different from
    employee's plant. To solve it, filter on plant of the employee, which
    was also done for filter on department.
  MRA:26-JUL-2019 GLOB3-337
  - All programs should be startable via exe instead of batch, just like TR
  - NOTE: Project is changed: Conditional define PIMS is added.
  MRA:21-OCT-2019 GLOB3-370
  - Oracle client settings and check on folder
  MRA:20-DEC-2019 GLOB3-377
  - Export to payroll - BambooHR
*)
unit SystemDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DBTables, DB, OneSolar, Provider, DBClient, BDE, Registry, ComCtrls,
  absDateTimePicker;

const
  MaxScriptField = 100;
  DefaultMaxTimeblocks = 4;
  DefaultMaxTimeblocks10 = 10;

type
  THybridMode=(hmNone, hmHybrid);

type
  TOvertime=(OTNone, OTDay, OTWeek, OTPeriod, OTMonth);

// GLOB3-94
type
  TOneSolar = class(OneSolar.TOneSolar)
  public
    constructor Create(AOwner: TComponent); override;
  end;


type
  TFileNames = Array[1..MaxScriptField] of String;
  TPimsDatabase = (DB_UNKNOWN, DB_PARADOX, DB_INTERBASE, DB_ORACLE);

  // MR:09-07-2003
  // Date-format-class for use in setting filters that
  // use (BDE)-dates.
  // Usage:
  //   In SystemDM a variable 'ADateFmt' is created for this class.
  //   This variable has 2 public methods and 2 public strings.
  //   Use it in combination with 'FormatDateTime'-function.
  // Example of use:
  //   with SystemDM do
  //   begin
  //     ADateFmt.SwitchDateTimeFmtBDE;
  //     MyFormDM.TableDetail.Filter :=
  //       '(STARTDATE <= ' +
  //       ADateFmt.Quote +
  //       FormatDateTime(ADateFmt.DateTimeFmt, Frac(CurrentDate)) +
  //       ADateFmt.Quote + ')' +
  //       ' AND ' +
  //       '(ENDDATE >= ' +
  //       ADateFmt.Quote +
  //       FormatDateTime(ADateFmt.DateTimeFmt, Frac(CurrentDate)) +
  //       ADateFmt.Quote + ')';
  //     ADateFmt.SwitchDateTimeFmtWindows;
  //   end;
  //
  // Remark:
  // When using 'Abort' please put use syntax 'SysUtils.Abort'.
  // Otherwise it's confused with the BDE-Abort by the compiler.
  TDateFmt = class
  private
    FDateTimeFmt: String;
    FQuote: String;
    FSepBDE: String;
    FDateFmtBDE: String;
    FTimeFmtBDE: String;
    FSepWindows: String;
    FDateFmtWindows: String;
    FTimeFmtWindows: String;
    function CreateDateTimeFmt: String;
  public
    procedure AfterConstruction; override;
    procedure SwitchDateTimeFmtBDE;
    procedure SwitchDateTimeFmtWindows;
// Read only properties!
    property DateTimeFmt: String read FDateTimeFmt;
    property Quote: String read FQuote;
  end;

  //CAR 7-5-2003
// type class defined for remember values between forms
// timerecording scanning, hours per employee
  TSaveTimeRecScanning = class
    FLastSelectedPlant: String;
    AEmployeeNumber: Integer;
    ADateScanning: TDatetime;
    AShiftNumber: Integer;
    procedure SetEmployee(const Value: Integer);
    procedure SetShift(const Value: Integer);
    procedure SetDateScanning(const Value: TDateTime);
  private
    FContractGroupCode: String;
  public
    constructor Create;
   { procedure SetValue(EmployeeNumber, ShiftNumber: Integer;
      DateScanning: TDateTime);}
    property Plant: String read FLastSelectedPlant write FLastSelectedPlant;
    property Employee: Integer read AEmployeeNumber write SetEmployee;
    property Shift: Integer read AShiftNumber write SetShift;
    property DateScanning: TDateTime read ADateScanning write SetDateScanning;
    property ContractGroupCode: String read FContractGroupCode
      write FContractGroupCode; // RV082.5.
  end;
  //end

// 20012944
type
  TDateTimePicker = class(TabsDateTimePicker)
  public
    constructor Create(AOwner: TComponent); override;
  end;

type
  TMonthCalendar = class(ComCtrls.TMonthCalendar)
  public
    constructor Create(AOwner: TComponent); override;
  end;

type
  TDBPicker = class(TabsDBDateTimePicker)
  public
    constructor Create(AOwner: TComponent); override;
  end;

  TSystemDM = class(TDataModule)
    SessionPims: TSession;
    Pims: TDatabase;
    OnePims: TOneSolar;
    TableDay: TTable;
    DataSourceDay: TDataSource;
    TablePIMSettings: TTable;
    TableWorkStation: TTable;
    DataSourceWorkStation: TDataSource;
    TableDayDAY_NUMBER: TIntegerField;
    TableDayDAY_CODE: TStringField;
    TableDayDESCRIPTION: TStringField;
    TablePIMSettingsSAVE_DAY_PRD_INFO: TIntegerField;
    TablePIMSettingsSAVE_DETAIL_PRD_INFO: TIntegerField;
    TablePIMSettingsUNIT_WEIGHT: TStringField;
    TablePIMSettingsUNIT_PIECES: TStringField;
    TablePIMSettingsWEEK_START_ON: TIntegerField;
    TablePIMSettingsSETTING_PASSWORD: TStringField;
    TablePIMSettingsVERSION_NUMBER: TIntegerField;
    TableWorkStationCOMPUTER_NAME: TStringField;
    TableWorkStationLICENSEVERSION: TIntegerField;
    TablePIMSettingsCHANGE_SCAN_YN: TStringField;
    TableTmpPivot: TTable;
    TableTaylor: TTable;
    TableExportPayroll: TTable;
    TablePIMSettingsREAD_PLANNING_ONCE_YN: TStringField;
    TableWorkStationTIMERECORDING_YN: TStringField;
    TablePIMSettingsYEARS_FOR_SICK_PAY: TIntegerField;
    TablePIMSettingsHOURTYPE_SICK_PAY: TIntegerField;
    TablePIMSettingsMINUTES_SICK_PAY: TIntegerField;
    TablePIMSettingsMAX_MINUTES_SICK_PAY: TIntegerField;
    TablePIMSettingsFILLPRODUCTIONHOURS_YN: TStringField;
    QueryServerTime: TQuery;
    QueryServerTimeSERVERDATETIME: TDateTimeField;
    TablePIMSettingsDATACOL_PATH: TStringField;
    TablePIMSettingsDATACOL_READFILES_INTERVAL: TIntegerField;
    TablePIMSettingsDATACOL_PRESET_STARTTIME: TDateTimeField;
    TablePIMSettingsDATACOL_TIME_INTERVAL: TIntegerField;
    ClientDataSetDay: TClientDataSet;
    DataSetProviderDay: TDataSetProvider;
    TablePIMSettingsDELETE_WK_YN: TStringField;
    QueryUpdatePims: TQuery;
    TablePIMSettingsFIRST_WEEK_OF_YEAR: TIntegerField;
    TablePIMSettingsEMPLOYEES_ACROSS_PLANTS_YN: TStringField;
    TableWorkStationSHOW_HOUR_YN: TStringField;
    QueryServerTimeOrcl: TQuery;
    DateTimeField1: TDateTimeField;
    qryInitSession: TQuery;
    qryPlantDeptTeam: TQuery;
    TablePIMSettingsCODE: TStringField;
    qryTemp: TQuery;
    qryPimsMenuGroup: TQuery;
    qryPimsMenuGroup2: TQuery;
    TablePIMSettingsEMBED_DIALOGS_YN: TStringField;
    TablePIMSettingsABSLOG_EMPHRS_YN: TStringField;
    TablePIMSettingsABSLOG_TRS_YN: TStringField;
    TablePIMSettingsEFF_BO_QUANT_YN: TStringField;
    TablePIMSettingsPS_AUTO_CLOSE_SECS: TIntegerField;
    TablePIMSettingsUPDATE_EMP_YN: TStringField;
    TablePIMSettingsUPDATE_PLANT_YN: TStringField;
    TablePIMSettingsUPDATE_WK_YN: TStringField;
    TablePIMSettingsDELETE_PLANT_YN: TStringField;
    TablePIMSettingsDELETE_EMP_YN: TStringField;
    TablePIMSettingsUPDATE_DELETE_EMP_YN: TStringField;
    TablePIMSettingsUPDATE_DELETE_PLANT_YN: TStringField;
    TablePIMSettingsDATACOL_STORE_QTY_5MIN_PERIOD: TStringField;
    TablePIMSettingsSHOW_WEEKNUMBERS_YN: TStringField;
    TableWorkStationHIDE_ID_YN: TStringField;
    TableWorkStationCARDREADER_DELAY: TFloatField;
    TablePIMSettingsSHIFTDATESYSTEM_YN: TStringField;
    TablePIMSettingsFINAL_RUN_YN: TStringField;
    qryFinalRun: TQuery;
    qryFinalRunInsert: TQuery;
    qryFinalRunUpdate: TQuery;
    qryPlantExportType: TQuery;
    qryEmpExportType: TQuery;
    TablePIMSettingsENABLE_MACHINETIMEREC_YN: TStringField;
    TablePIMSettingsWORKSPOT_INCL_SEL_YN: TStringField;
    TablePIMSettingsDATETIME_SEL_YN: TStringField;
    TablePIMSettingsINCL_OPEN_SCANS_YN: TStringField;
    TableWorkStationMANREGS_MULTJOBS_YN: TStringField;
    TablePIMSettingsERROR_LOG_TO_DB_YN: TStringField;
    qryABSLogInsert: TQuery;
    qryPlant: TQuery;
    qryPlantUpdate: TQuery;
    TablePIMSettingsTIMEREC_IN_SECONDS_YN: TStringField;
    TablePIMSettingsIGNORE_BREAKS_YN: TStringField;
    TablePIMSettingsTIMEZONE: TMemoField;
    cdsTimeZone: TClientDataSet;
    cdsTimeZoneTIMEZONE: TStringField;
    qryPlantFind: TQuery;
    TableWorkStationPLANT_CODE: TStringField;
    TablePIMSettingsPSSHOWEMPEFF_YN: TStringField;
    TablePIMSettingsPSSHOWEMPINFO_YN: TStringField;
    TablePIMSettingsJOBCOMMENT_YN: TStringField;
    TablePIMSettingsPACKAGE_HOUR_CALC_YN: TStringField;
    TablePIMSettingsBREAK_BTN_YN: TStringField;
    TablePIMSettingsLUNCH_BTN_YN: TStringField;
    TablePIMSettingsTR_EOD_BTN_YN: TStringField;
    TablePIMSettingsPS_EOD_BTN_YN: TStringField;
    TablePIMSettingsMANREG_EOD_BTN_YN: TStringField;
    TablePIMSettingsEXTERN_QTY_FROM_PQ_YN: TStringField;
    TablePIMSettingsLUNCH_BTN_SCANOUT_YN: TStringField;
    TablePIMSettingsHYBRID: TIntegerField;
    TablePIMSettingsUPDATE_EMPNR_YN: TStringField;
    TablePIMSettingsPRODHRS_NOSUBTRACT_BREAKS: TStringField;
    TablePIMSettingsMAX_TIMEBLOCKS: TIntegerField;
    TablePIMSettingsTIMERECCR_DONOTSCANOUT_YN: TStringField;
    TablePIMSettingsOVERTIME_PERDAY_YN: TStringField;
    TablePIMSettingsREPPLAN_STARTEND_TIMES_YN: TStringField;
    TablePIMSettingsOCCUP_HANDLING: TIntegerField;
    procedure DefaultBeforeDelete(DataSet: TDataSet);
    procedure DefaultBeforePost(DataSet: TDataSet);
    procedure DefaultDeleteError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure DefaultEditError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure DefaultNewRecord(DataSet: TDataSet);
    procedure DefaultPostError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure DefaultNotEmptyValidate(Sender: TField);
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure TablePIMSettingsBeforeDelete(DataSet: TDataSet);
    procedure TablePIMSettingsBeforePost(DataSet: TDataSet);
    procedure TablePIMSettingsEditError(DataSet: TDataSet;
      E: EDatabaseError; var Action: TDataAction);
    procedure TablePIMSettingsNewRecord(DataSet: TDataSet);
    procedure TableWorkStationBeforePost(DataSet: TDataSet);
  private
    { Private declarations }
    FDetailedProd: Integer;
    FProcessDayQantity:  Integer;
    FUnitsWeigh: String;
    FUnitsPieces: String;
    FWeekStartsOn: Integer;
    //car 4-12-2003
    FFirstWeekOfYear: Integer;
    FSettingsPassword : String;
    FReadPlanningOnceYN: String;
    FUpdateEmployeeYN: String;
    FUpdatePlantYN: String;
//CAR 21-8-2003
    FDeleteWKYN: String;
    FVersionNumber: Integer;
    FChangeScan: String;
    FTimeRecordingStation: String;
    FEmployeesAcrossPlantsYN: String;
    //
    FExportPayrollType: String;
    FPlantTeamFilterOn: Boolean;
    FIsAttent: Boolean;
    FIsCVL: Boolean;
    FUserTeamSelectionYN: String;
    FCurrentLoginUser: String;
    FLoginGroup: String;
    FApl_Menu_Login: Integer;
    FRegistryLogin: String;
    FAplEditYN: String;
    FEmbedDialogs: Boolean;
    FIsLTB: Boolean;
    FABSLOG_EmpHrsYN: String;
    FABSLOG_TRSYN: String;
    FEmbedDialogsYN: String;
    FEffBasedOnQuantYN: String;
    FPSAutoCloseSecs: Integer;
    FIsCLF: Boolean;
    FDeletePlantYN: String;
    FDeleteEmployeeYN: String;
    FUpdateWKYN: String;
    FIsBCH: Boolean;
    FShowWeeknumbersYN: String;
    FUseShiftDateSystem: Boolean;
    FIsWMU: Boolean;
    FUseFinalRun: Boolean;
    FIsAdmin: Boolean;
    FSysAdmin: Boolean;
    FIsVOS: Boolean;
    FEnableMachineTimeRec: Boolean;
    FWorkspotInclSel: Boolean;
    FInclOpenScans: Boolean;
    FDateTimeSel: Boolean;
    FCurrentComputerName: String;
    FLogToDB: Boolean;
    FAppName: String;
    FIgnoreBreaks: Boolean;
    FTimerecordingInSecs: Boolean;
    FWSTimezonehrsdiff: Integer;
    FDatabaseTimeZone: String;
    FPSShowEmpEff: Boolean;
    FPSShowEmpInfo: Boolean;
    FAddJobComment: Boolean;
    FUsePackageHourCalc: Boolean;
    FBreakBtnVisible: Boolean;
    FLunchBtnVisible: Boolean;
    FTREndOfDayBtnVisible: Boolean;
    FPSEndOfDayBtnVisible: Boolean;
    FMANREGEndOfDayBtnVisible: Boolean;
    FExternQtyFromPQ: Boolean;
    FLunchBtnScanOut: Boolean;
    FHybridMode: THybridMode;
    FUpdateEmpNr: Boolean;
    FProdHrsDoNotSubtractBreaks: Boolean;
    FIsNTS: Boolean;
    FMaxTimeblocks: Integer;
    FTimeRecCRDoNotScanOut: Boolean;
    FOvertimePerDay: Boolean;
    FRepPlanStartEndTime: Boolean;
    FOccup_Handling: Integer;
    FEnvVarRead: Boolean;
    function GetCurrentProgramUser: String;
    function GetCurrentComputerName: String;
    function GetPimsDatabase: TPimsDatabase;
    function GetDatabaseServerName: String;
    function GetCurrentClientEnvName: String;
    function GetCurrentComputerEnvName: String;
    procedure DetermineDatabaseUser;
    function GetExportPayrollType: String;
    procedure SetExportPayrollType(const Value: String);
    procedure SetIsAttent(const Value: Boolean);
    procedure SetIsCVL(const Value: Boolean);
    procedure SetIsLTB(const Value: Boolean);
    procedure ABSLogEnable;
    procedure SetIsCLF(const Value: Boolean);
    procedure SetIsBCH(const Value: Boolean);
    procedure SetIsWMU(const Value: Boolean);
    function GetIsAdmin: Boolean;
    procedure SetIsVOS(const Value: Boolean);
    procedure SetCurrentComputerName(const Value: String);
    procedure AppNameSet(const Value: String);
    procedure cdsTimeZoneFillList;
    procedure SetIsNTS(const Value: Boolean);
  public
    { Public declarations }
    ADateFmt: TDateFmt;
    //Car 17-10-2003 - 550262
    ASaveTimeRecScanning: TSaveTimeRecScanning;
    // RV087.3.
    function GetMenuNumberOfApplication: Integer;
    function GetMenuTreeOfParentMenuMain(AWorkQuery: TQuery; Parent_Menu: Integer;
      Visible_YN, Expand_YN: String): TQuery;
    function GetMenuTreeOfParentMenu(Parent_Menu: Integer;
      Visible_YN, Expand_YN: String): TQuery;
    function GetMenuTreeOfParentMenu2(Parent_Menu: Integer;
      Visible_YN, Expand_YN: String): TQuery;
    function GetMenuNumberOfMenuOption(const PimsMenuOption: Integer): Integer;
    function CheckVisibleMenuOption(const PimsMenuOption: Integer): Boolean;
    //
    property ReadPlanningOnceYN: String read FReadPlanningOnceYN write FReadPlanningOnceYN;
    property CurrentComputerName: String read GetCurrentComputerName write SetCurrentComputerName;
    property DetailedProd: Integer read FDetailedProd write FDetailedProd;
    property ProcessDayQantity : Integer read FProcessDayQantity write FProcessDayQantity;
    property UnitsWeigh: String read FUnitsWeigh write FUnitsWeigh;
    property UnitsPieces: String read FUnitsPieces write FUnitsPieces;
    property WeekStartsOn: Integer read FWeekStartsOn write FWeekStartsOn;
    property SettingsPassword: String read FSettingsPassword write FSettingsPassword;
    property VersionNumber: Integer read FVersionNumber write FVersionNumber;
    property ChangeScan: String read FChangeScan write FChangeScan;
    property TimeRecordingStation: String read FTimeRecordingStation
       write FTimeRecordingStation;
    property CurrentProgramUser: String read GetCurrentProgramUser;
    property UpdatePlantYN: String read FUpdatePlantYN write FUpdatePlantYN;
    property UpdateEmployeeYN: String read FUpdateEmployeeYN write FUpdateEmployeeYN;
    property DeleteWKYN: String read FDeleteWKYN write FDeleteWKYN;
//car 4-12-2003
    property FirstWeekOfYear: Integer read FFirstWeekOfYear write FFirstWeekOfYear;
    //CAR 7-5-2003
   {
    property SaveTimeRecScanning: TSaveTimeRecScanning read FSaveTimeRecScanning
      write FSaveTimeRecScanning;}
  (* ORACLE TEST *)
    property PimsDatabase: TPimsDatabase read GetPimsDatabase;
    function GetFillProdHourPerHourTypeYN: String;
    function GetDayIndex(Index: Integer): Integer;
    function GetDayWCode(Index: Integer): String;
    function GetDayWDescription(Index: Integer): String;
    procedure SetSettings;
    procedure AssignSettings;
    procedure GetSettings;

    procedure SynchronizeWorkStation;
    procedure DeletePivotTableNotUsed(TableName: String);
    function GetQualityIncentiveConf: Boolean;
    //CAR: 550299
    function GetGradeYN: Boolean;
    function GetExportPayroll: Boolean;

    procedure OpenSystemTables;
    // procedures for updating database - begin
    function UpdatePimsBase(DatabaseName: String): Boolean;
//    procedure AlterTablePimsVersion(QueryUpdate: TQuery;
//      DB_Old_Version, DB_Version : String);
    procedure GetSqlFiles(var SQLFiles: TFileNames; PIMS_Func, Pims_DB: String);
    function ReadSQLFile(QueryUpdate: TQuery; FileName: String): Boolean;
    function GetDBVers(SQLFileNames: String): Integer;
    procedure GetPimsDBVersion(QueryUpdate: TQuery; var DB_Vers, FuncDB_Vers: String);
// end updating database
//  Car 6-5-2004
    function ReadRegistry(Str_OpenKey, Str_Read: String): String;
    procedure WriteRegistry(Str_OpenKey, Str_Read, Str_Write: String);
    function GetMonthGroupEfficiency: Boolean;
    procedure DetermineWorkstationTimezonehrsdiff;
    procedure PlantTimezonehrsdiffWriteToDB;

    property EmployeesAcrossPlantsYN: String read FEmployeesAcrossPlantsYN
      write FEmployeesAcrossPlantsYN;
    property DatabaseServerName: String read GetDatabaseServerName;
    property CurrentClientEnvName: String read GetCurrentClientEnvName;
    property CurrentComputerEnvName: String read GetCurrentComputerEnvName;
    property ExportPayrollType: String read GetExportPayrollType
      write SetExportPayrollType;
    procedure PlantTeamFilterEnable(DataSet: TDataSet);
    function PlantTeamFilter(DataSet: TDataSet): Boolean;
    function PlantTeamTeamFilter(DataSet: TDataSet): Boolean; overload; // GLOB3-342
    function PlantTeamTeamFilter(DataSet: TDataSet;
      APlantField, ATeamField: String): Boolean; overload; // GLOB3-342
    function PlantDeptTeamFilter(DataSet: TDataSet): Boolean; overload;
    function PlantDeptTeamFilter(DataSet: TDataSet;
      APlantField, ADeptField: String): Boolean; overload;
    property PlantTeamFilterOn: Boolean read FPlantTeamFilterOn
      write FPlantTeamFilterOn;
    function GetCode: String;

    function GetDBValue(ASql: String; DefaultValue: Variant): Variant;
    procedure ExecSql(ASql: String);

    procedure UpdateAbsenceReasonPerCountry(ACountryId: Integer; AQuery: TQuery;
      ASortField: String='DESCRIPTION');
    procedure UpdateHourTypePerCountry(ACountryId: Integer; AQuery: TQuery);
    function GetCounterActivated(ACountryId: Integer; AAbsenceType: Char): Boolean;
    function UserTeamLoginUser: String; // RV073.5. Access rights.
    function IsRFL: Boolean;
    function FutureDate: TDateTime;
    function FinalRunExportDate(APlantCode, AExportType: String): TDateTime;
    function FinalRunExportDateByPlant(APlantCode: String): TDateTime;
    function FinalRunExportDateByEmployee(AEmployeeNumber: Integer): TDateTime;
    procedure FinalRunStoreExportDate(APlantCode, AExportType: String; AExportDate: TDateTime);
    property IsAttent: Boolean read FIsAttent write SetIsAttent;
    property IsCVL: Boolean read FIsCVL write SetIsCVL;
    property UserTeamSelectionYN: String read FUserTeamSelectionYN
      write FUserTeamSelectionYN; // RV087.3.
    property CurrentLoginUser: String read FCurrentLoginUser
      write FCurrentLoginUser; // RV087.3.
    property Apl_Menu_Login: Integer read FApl_Menu_Login write FApl_Menu_Login;
    property LoginGroup: String  read FLoginGroup write FLoginGroup;
    property RegistryLogin: String read FRegistryLogin
      write FRegistryLogin;
    property AplEditYN: String read FAplEditYN write FAplEditYN;
    property EmbedDialogs: Boolean read FEmbedDialogs write FEmbedDialogs;
    property IsLTB: Boolean read FIsLTB write SetIsLTB;
    property EmbedDialogsYN: String read FEmbedDialogsYN write FEmbedDialogsYN;
    property ABSLOG_EmpHrsYN: String read FABSLOG_EmpHrsYN
      write FABSLOG_EmpHrsYN;
    property ABSLOG_TRSYN: String read FABSLOG_TRSYN write FABSLOG_TRSYN;
    procedure ABSLogEmpHrsEnable(AEnable: Boolean);
    procedure ABSLogTRSEnable(AEnable: Boolean);
    function ShowWeeknumbers: Boolean; // 20012944
    function DBLog(AMsg: String; APriority: Integer): Boolean; // 20014862
    property EffBasedOnQuantYN: String read FEffBasedOnQuantYN
      write FEffBasedOnQuantYN;
    property PSAutoCloseSecs: Integer read FPSAutoCloseSecs
      write FPSAutoCloseSecs;
    property IsCLF: Boolean read FIsCLF write SetIsCLF;
    property DeletePlantYN: String read FDeletePlantYN write FDeletePlantYN;
    property DeleteEmployeeYN: String read FDeleteEmployeeYN write FDeleteEmployeeYN;
    property UpdateWKYN: String read FUpdateWKYN write FUpdateWKYN;
    property IsBCH: Boolean read FIsBCH write SetIsBCH; // 20014036
    property ShowWeeknumbersYN: String read FShowWeeknumbersYN
      write FShowWeeknumbersYN; // 20012944
    property UseShiftDateSystem: Boolean read FUseShiftDateSystem
      write FUseShiftDateSystem; // 20014327
    property IsWMU: Boolean read FIsWMU write SetIsWMU; // 20014715
    property UseFinalRun: Boolean read FUseFinalRun write FUseFinalRun; // 20011800
    property IsAdmin: Boolean read GetIsAdmin write FIsAdmin; // 20011800
    property SysAdmin: Boolean read FSysAdmin write FSysAdmin; // 20011800
    property IsVOS: Boolean read FIsVOS write SetIsVOS; // 20014442
    property EnableMachineTimeRec: Boolean
      read FEnableMachineTimeRec write FEnableMachineTimeRec; // 20015178
    property WorkspotInclSel: Boolean read FWorkspotInclSel
      write FWorkspotInclSel; // 20015223
    property DateTimeSel: Boolean read FDateTimeSel write FDateTimeSel; // 20015220
    property InclOpenScans: Boolean read FInclOpenScans write FInclOpenScans; // 20015221
    property LogToDB: Boolean read FLogToDB write FLogToDB; // 20014826
    property AppName: String read FAppName write AppNameSet; // 20014826
    property TimerecordingInSecs: Boolean read FTimerecordingInSecs write FTimerecordingInSecs; // 20015346
    property IgnoreBreaks: Boolean read FIgnoreBreaks write FIgnoreBreaks; // 20015346
    property WSTimezonehrsdiff: Integer read FWSTimezonehrsdiff write FWSTimezonehrsdiff; // 20016449
    property DatabaseTimeZone: String read FDatabaseTimeZone write FDatabaseTimeZone; // 20016449
    property PSShowEmpEff: Boolean read FPSShowEmpEff write FPSShowEmpEff; // 20014450.50
    property PSShowEmpInfo: Boolean read FPSShowEmpInfo write FPSShowEmpInfo; // 20014450.50
    property AddJobComment: Boolean read FAddJobComment write FAddJobComment; // PIM-50
    property UsePackageHourCalc: Boolean read FUsePackageHourCalc write FUsePackageHourCalc; // PIM-87
    property BreakBtnVisible: Boolean read FBreakBtnVisible write FBreakBtnVisible; // PIM-12
    property LunchBtnVisible: Boolean read FLunchBtnVisible write FLunchBtnVisible; // PIM-12
    property TREndOfDayBtnVisible: Boolean read FTREndOfDayBtnVisible write FTREndOfDayBtnVisible; // PIM-12
    property PSEndOfDayBtnVisible: Boolean read FPSEndOfDayBtnVisible write FPSEndOfDayBtnVisible; // PIM-12
    property MANREGEndOfDayBtnVisible: Boolean read FMANREGEndOfDayBtnVisible write FMANREGEndOfDayBtnVisible; // PIM-12
    property ExternQtyFromPQ: Boolean read FExternQtyFromPQ write FExternQtyFromPQ; // PIM-90
    property LunchBtnScanOut: Boolean read FLunchBtnScanOut write FLunchBtnScanOut; // PIM-139
    property HybridMode: THybridMode read FHybridMode; // PIM-180
    property UpdateEmpNr: Boolean read FUpdateEmpNr write FUpdateEmpNr; // PIM-227
    property ProdHrsDoNotSubtractBreaks: Boolean read FProdHrsDoNotSubtractBreaks write FProdHrsDoNotSubtractBreaks; // PIM-319
    property IsNTS: Boolean read FIsNTS write SetIsNTS;
    property MaxTimeblocks: Integer read FMaxTimeblocks write FMaxTimeblocks; // GLOB3-60
    property TimeRecCRDoNotScanOut: Boolean read FTimeRecCRDoNotScanOut write FTimeRecCRDoNotScanOut; // GLOB3-202
    property OvertimePerDay: Boolean read FOvertimePerDay write FOvertimePerDay; // GLOB3-223
    property RepPlanStartEndTime: Boolean read FRepPlanStartEndTime write FRepPlanStartEndTime; // PIM-377
    property Occup_Handling: Integer read FOccup_Handling write FOccup_Handling; // GLOB3-330
    property EnvVarRead: Boolean read FEnvVarRead write FEnvVarRead; // GLOB3-337
  end;

function DisplayMessage(const Msg: string; DlgType: TMsgDlgType;
  Buttons: TMsgDlgButtons): Integer;
function DisplayMessageFmt(const Msg: string; const Args: array of const;
  DlgType: TMsgDlgType; Buttons: TMsgDlgButtons): Integer;
function DisplayMessage4Buttons(const Msg: String): Integer;
function DoubleQuote(Code: string): string;
function GetErrorMessage(const PimsError: EDataBaseError;
  const PimsActivity: Integer): String;
function DisplayErrorMessage(const PimsError: EDataBaseError;
  const PimsActivity: Integer):Integer;
function IsErrorFromException(ErrorMessage: String;
  var ErrorCode: Integer): Boolean;
procedure GetBuildInfo(var V1, V2, V3, V4: Word);
function DisplayVersionInfo: String;
procedure CheckValidNumber(var Key:Char);
procedure ActiveTables(FDataModule: TDataModule; ActiveYN: Boolean);
function BoolYN(BoolVal: Boolean): String;
function YNBool(BoolVal: String): Boolean;
procedure ExecSql(QueryTmp: TQuery; StrSelect: String);

//CAR: 19-9-2003
procedure OpenSql(QueryTmp: TQuery; StrSelect: String);

function GetStrValue(StrTmp: String): String;
function GetIntValue(StrIntTmp: String): Integer;
function CompareDate(Date_1, Date_2: TDateTime): Integer;
function GetDate(const DateConst: TDateTime): TDateTime;
//replace system functions -car 27.02.2003
function GetTime(const DateConst: TDateTime): TDateTime;
function Now: TDateTime;
function Date: TDateTime;
function Time: TDateTime;
function NewNextNow: TDateTime; // MR:10-02-2005 Order 550373

// 20016449
function DateTimeTimeZone(ADateTime: TDateTime): TDateTime;
function DateTimeWithoutTimeZone(ADateTime: TDateTime): TDateTime;
function FormatDateTime(const Format: string; DateTime: TDateTime): string; //overload;
function DateTimeToStr(const DateTime: TDateTime): string;
function WSDate: TDateTime;
function WSNow: TDateTime;

var
  SystemDM: TSystemDM;
// KME 23-08-2004 - Call Now function once a session because 'Now'is a select in
// Interbase system table
// Reselect from database when System time is changed
// Handle message WM_TIMECHANGE in MainForm;
var
  PimsTimeInitialised: Boolean = False;
  PimsNextNow: TDateTime = 0; // MR:10-02-2005 Order 550373

implementation

uses
  UPimsConst, UPimsMessageRes, UPimsVersion, Inifiles, FileCtrl,
  MessageDialogFRM, UGlobalFunctions, UTimeZone, DialogSelectPrinterFRM,
  ListProcsFRM;
// MR:19-05-2004 Disabled, is not used here?
//  DialogSettingsFRM;

{$R *.DFM}

// KME 23-08-2004 - Call Now function once a session because 'Now'is a select in
// Interbase system table
var
  PimsTimeDifference: TDateTime = 0;

//CAR 7-5-2003 : Save value between TimeRecScanning and HoursPerEmpl
//CAR 17-10-2003 : 550262
constructor TSaveTimeRecScanning.Create;
begin
  inherited Create;
  Employee := -1;
  Shift := -1;
  DateScanning := EncodeDate(1900,1,1);
  Plant := '';
end;

procedure TSaveTimeRecScanning.SetEmployee(Const Value: Integer);
begin
  AEmployeeNumber := Value;
end;

procedure TSaveTimeRecScanning.SetShift(Const Value: Integer);
begin
  AShiftNumber := Value;
end;

procedure TSaveTimeRecScanning.SetDateScanning(Const Value:  TDateTime);
begin
  ADateScanning := Value;
end;

function TSystemDM.ReadRegistry(Str_OpenKey, Str_Read: String): String;
var
  Reg: TRegistry;
begin
  Reg := TRegistry.Create;
  try
    Reg.RootKey := HKEY_LOCAL_MACHINE;
    if Reg.OpenKey(Str_OpenKey, True) then
      Result := Reg.ReadString(Str_Read);
  finally
    Reg.CloseKey;
    Reg.Free;
  end;
end;

procedure TSystemDM.WriteRegistry(Str_OpenKey, Str_Read, Str_Write: String);
var
  Reg: TRegistry;
begin
  Reg := TRegistry.Create;
  try
    Reg.RootKey := HKEY_LOCAL_MACHINE;
    if Reg.OpenKey(Str_OpenKey, True) then
      Reg.WriteString(Str_Read, Str_Write);
  finally
    Reg.CloseKey;
    Reg.Free;
  end;
end;

// functions which will get the server date time car 27.02.2003
// KME 23-08-2004 - Call Now function once a session because 'Now'is a select in Interbase
// system table
function Time: TDateTime;
begin
  Result := Frac(Now);
{
  SystemDM.QueryServerTime.Open;
  Result := GetTime(SystemDM.QueryServerTime.FieldByName('SERVERDATETIME').AsDateTime);
  SystemDM.QueryServerTime.Close;
}
end;

function Date: TDateTime;
begin
  Result := Trunc(Now);
{
  SystemDM.QueryServerTime.Open;
  Result := GetDate(SystemDM.QueryServerTime.FieldByName('SERVERDATETIME').AsDateTime);
  SystemDM.QueryServerTime.Close;
}
end;

// MR:10-02-2005 Order 550373
function NewNextNow: TDateTime;
begin
  Result := SysUtils.Now +
    (1 / 24); // 1/n part of a day
end;

function Now: TDateTime;
var
  PimsTime: TDateTime;
begin
  // MR:09-02-2005 - Order 550373
  // MR:09-02-2005 Set 'ServerTime' every xx hours.
  if SysUtils.Now > PimsNextNow then
    PimsTimeInitialised := False;
  if not PimsTimeInitialised then
  begin
  (* ORACLE TEST *)
    if (SystemDM.PimsDatabase = DB_ORACLE) then
    begin
      SystemDM.QueryServerTimeOrcl.Open;
      PimsTime := SystemDM.QueryServerTimeOrcl.FieldByName('SERVERDATETIME').AsDateTime;
      SystemDM.QueryServerTimeOrcl.Close;
    end
    else
    begin
      SystemDM.QueryServerTime.Open;
      PimsTime := SystemDM.QueryServerTime.FieldByName('SERVERDATETIME').AsDateTime;
      SystemDM.QueryServerTime.Close;
    end;
    PimsTimeDifference := SysUtils.Now - PimsTime;
    PimsTimeInitialised := True;
    PimsNextNow := NewNextNow;
  end;
  // MR:14-12-2004 Difference should be subtracted instead of added!
  Result := SysUtils.Now - PimsTimeDifference;
  Result := DateTimeTimezone(Result); // 20016449
end;
// end car

function GetDate(const DateConst: TDateTime): TDateTime;
var
  DateTmp: TDateTime;
begin
  DateTmp := DateConst;
  ReplaceTime(DateTmp, 0);
  Result := DateTmp;
end;

function GetTime(const DateConst: TDateTime): TDateTime;
var
  Hour,  Min,  Sec, MSec: Word;
begin
  DecodeTime(DateConst, Hour, Min, Sec, MSec);
  Result := EncodeTime(Hour, Min, Sec, MSec);
end;

// 0 if equal
// 1 if date_1 > date_2
//-1 date_1 < date_2
function CompareDate(Date_1, Date_2: TDateTime): Integer;
var
  Year_1, Month_1, Day_1: Word;
  Year_2, Month_2, Day_2: Word;
begin
  ReplaceTime(Date_1, 0);
  ReplaceTime(Date_2, 0);
  DecodeDate(Date_1, Year_1, Month_1, Day_1);
  DecodeDate(Date_2, Year_2, Month_2, Day_2);
  Result := 1;
  if (Year_1 = Year_2) and (Month_1 = Month_2) and (Day_1 = Day_2) then
    Result := 0;
  if (Year_1 < Year_2) or
   ((Year_1 = Year_2) and (Month_1 < Month_2)) or
   ((Year_1 = Year_2) and (Month_1 = Month_2) and (Day_1 < Day_2)) then
     Result := -1;
end;

function GetStrValue(StrTmp: String): String;
var
  P: Integer;
begin
  P := Pos(Str_Sep, StrTmp);
  if p <= 0 then
    Result := ''
  else
    Result := Copy(StrTmp, 0 , p-1);
end;

function GetIntValue(StrIntTmp: String): Integer;
var
  P: Integer;
begin
  P := Pos(Str_Sep, StrIntTmp);
  if p <= 0 then
    Result := 0
  else
    Result := StrToInt(Copy(StrIntTmp, 0 , p-1));
end;

procedure ExecSql(QueryTmp: TQuery; StrSelect: String);
begin
  try
    QueryTmp.Active := False;
    QueryTmp.SQL.Clear;
    QueryTmp.SQL.Add(StrSelect);
    QueryTmp.ExecSQL;
  except
  end;
end;

procedure OpenSql(QueryTmp: TQuery; StrSelect: String);
begin
  try
    QueryTmp.Close;
    QueryTmp.SQL.Clear;
    QueryTmp.SQL.Add(StrSelect);
    QueryTmp.Open;
  except
  end;
end;

function BoolYN(BoolVal: Boolean): String;
begin
  if BoolVal then
    Result := CHECKEDVALUE
  else
    Result := UNCHECKEDVALUE;
end;

function YNBool(BoolVal: String): Boolean;
begin
  if BoolVal = CHECKEDVALUE then
    Result := True
  else
    Result := False;
end;

function TSystemDM.GetQualityIncentiveConf: Boolean;
begin
  Result := False;
  TableTaylor.TableName := 'TAYLORMADE';
  TableTaylor.Open;
  TableTaylor.First;
  if (TableTaylor.FieldByName('QUALITY_INCENTIVE_YN').AsString = 'Y') and
    not TableTaylor.Eof then
    Result := True;
end;

//CAR: 550299
function TSystemDM.GetGradeYN: Boolean;
begin
  Result := False;
  TableTaylor.TableName := 'TAYLORMADE';
  TableTaylor.Open;
  TableTaylor.First;
  if (TableTaylor.FieldByName('GRADE_YN').AsString = 'Y') and
    not TableTaylor.Eof then
    Result := True;
end;

function TSystemDM.GetExportPayroll: Boolean;
begin
  Result := False;
  TableExportPayroll.TableName := 'EXPORTPAYROLL';
  TableExportPayroll.Open;
  TableExportPayroll.First;
  if ((TableExportPayroll.FieldByName('EXPORT_TYPE').AsString = 'ADP') or
      (TableExportPayroll.FieldByName('EXPORT_TYPE').AsString = 'ADPUS') or
      (TableExportPayroll.FieldByName('EXPORT_TYPE').AsString = 'PARIS') or
      (TableExportPayroll.FieldByName('EXPORT_TYPE').AsString = 'SR') or
      (TableExportPayroll.FieldByName('EXPORT_TYPE').AsString = 'REINO') or
      //RV067.5.
      (TableExportPayroll.FieldByName('EXPORT_TYPE').AsString = 'ATTENT') or
      // RV087.2.
      (TableExportPayroll.FieldByName('EXPORT_TYPE').AsString = 'AFAS') or
      // 20013288
      (TableExportPayroll.FieldByName('EXPORT_TYPE').AsString = 'ELA') or
      // 20014714
      (TableExportPayroll.FieldByName('EXPORT_TYPE').AsString = 'WMU') or
      // GLOB3-72
      (TableExportPayroll.FieldByName('EXPORT_TYPE').AsString = 'NTS') or
      // GLOB3-234
      (TableExportPayroll.FieldByName('EXPORT_TYPE').AsString = 'QBOOKS') or
      // GLOB3-271
      (TableExportPayroll.FieldByName('EXPORT_TYPE').AsString = 'PCHEX') or
      // GLOB3-297
      (TableExportPayroll.FieldByName('EXPORT_TYPE').AsString = 'NAVIS') or
      // GLOB3-377
      (TableExportPayroll.FieldByName('EXPORT_TYPE').AsString = 'BAMBOO')
      ) and
    not TableExportPayroll.Eof then
    Result := True;
end;

//Car 15-9-2003: update database procedures -
// make global functions outside the TSystemDM class:

// update column name of PimsVersion using system table interbase -
{
procedure TSystemDM.AlterTablePimsVersion(QueryUpdate: TQuery;
  DB_Old_Version, DB_Version : String);
begin
  ExecSql(QueryUpdate,
    'UPDATE RDB$RELATION_FIELDS ' +
    ' SET  RDB$FIELD_NAME = ''' + DB_Version +  '''' +
    ' WHERE RDB$RELATION_NAME = ''PIMSVERSION''' +
    ' AND RDB$FIELD_NAME = ''DB_' + DB_Old_Version + '''' );
end;
}
//fill only valid sql script into array SQLFiles
// valid <=> db_script_vers > DB_Vers (of table PIMSVERSION)
// AND PIMS_FUNC = FUNC_XX(of table PIMSVersion)
procedure TSystemDM.GetSqlFiles(var SQLFiles: TFileNames; PIMS_Func, Pims_DB: String);
var
  SearchRec: TSearchRec;
  Index: Integer;
  PathFile: String;
// insert filename string into SQLFiles array so they are ascending sorted
  procedure InsertFileName(FileName: String; Index: Integer);
  var
    I, J: Integer;
  begin
    I:= 1;
    while (I <= MaxScriptField) and
     (GetDBVers(SQLFiles[I]) < GetDBVers(FileName)) and (SQLFiles[I] <> '') do
      Inc(I);

    if (SQLFiles[I] = '') then
    begin
      SqlFiles[I] := FileName;
      Exit;
    end;
    for J:= (Index - 1) downto I do
      SqlFiles[J + 1] := SqlFiles[J];
    SqlFiles[I] := FileName;
  end;

begin
  for Index := 1 to MaxScriptField do
    SQLFiles[Index] := '';
  Index := 1;
// car 15-9-2003 - rights users ... - change name of scripts
  if ((Application.Title <> 'ADMINISTRATION - TOOL') and
    (Application.Title <> 'ORCL - ADMINISTRATION - TOOL')) then
    PathFile := GetCurrentDir + '\PIMS_*_' + PIMS_Func + '.SQL'
  else
  // use the same adms_**_xx.sql name only for adm tool application
    PathFile := GetCurrentDir + '\ADMS_*_' + PIMS_Func + '.SQL';

  if (FindFirst(PathFile, faAnyFile, SearchRec) = 0) then
  begin
    if GetDBVers(SearchRec.Name) > StrToInt(PIMS_DB) then
    begin
      SQLFiles[1] := SearchRec.Name;
      Index := 2;
    end;
  end
  else
  begin
    FindClose(SearchRec);
    Exit;
  end;

  while (FindNext(SearchRec) = 0) and (Index <= MaxScriptField) do
  begin
    if  (GetDBVers(SearchRec.Name) >  StrToInt(PIMS_DB)) then
    begin
      InsertFileName(SearchRec.Name, Index);
      Inc(Index);
    end;
  end;
  FindClose(SearchRec);
end;

function TSystemDM.GetDBVers(SQLFileNames: String): Integer;
var
  PosDelimitator: Integer;
  StrDB: String;
begin
  Result := 0;
  SqlFileNames := Copy(SqlFileNames, 6, length(SqlFileNames));
  PosDelimitator := Pos('_', SqlFileNames);
  if PosDelimitator > 0 then
  begin
    StrDB := Copy(SqlFileNames, 0, PosDelimitator - 1);
    if StrDB <> '' then
      Result := StrToInt(StrDB);
  end;
end;

// get database version and functionality number of pims from the PIMSVERSION table
// use interbase system table
procedure TSystemDM.GetPimsDBVersion(QueryUpdate: TQuery;
  var DB_Vers, FuncDB_Vers: String);
begin
  DB_Vers := '';
  FuncDB_Vers := '';
  try
    // Pims -> Oracle: Table 'Pimsdbversion' is used to store database-version.
    OpenSql(QueryUpdate,
      'SELECT DBVERSION ' +
      'FROM PIMSDBVERSION');
    if QueryUpdate.IsEmpty then
      Exit;
    DB_Vers := QueryUpdate.FieldByName('DBVERSION').AsString;
    QueryUpdate.Close;
  except
    // Ignore error.
  end;
end;

// sql script file any sql statements are separated through '//'
// if one sql statement does not work then
// exit from the execution and saved all the statements not executed
// from that point
function TSystemDM.ReadSqlFile(QueryUpdate: TQuery; FileName: String): Boolean;
var
  SQLFile, ErrorFile: Text;
  SqlStr, SaveSqlStr: String;
  VChar: String;
procedure ExecuteSQL(SqlStr: String);
begin
  QueryUpdate.Close;
  QueryUpdate.SQL.Clear;
  QueryUpdate.ParamCheck := False;
  QueryUpdate.SQL.Add(SqlStr);
  QueryUpdate.ExecSQL;
  if SystemDM <> Nil then
    if Pos ('RDB$', SqlStr) > 0 then
    begin
      SystemDM.Pims.Close;
      SystemDM.Pims.Open;
    end;
end;

begin
// use this error file if exist as script sql
  SqlStr:= '';
  if FileExists(GetCurrentDir + '\Error' + FileName) then
  begin
    AssignFile(SQLFile, GetCurrentDir + '\Error' + FileName);
// error file will be called tmperror + pims_2_1.sql     
    SqlStr := 'TMP';
  end
  else
    AssignFile(SQLFile, GetCurrentDir + '\' + FileName);

// read sql file
  Reset(SQLFile);
  AssignFile(ErrorFile, GetCurrentDir + '\' + SqlStr +'Error' + FileName);
// empty error file
  Rewrite(ErrorFile);
  SqlStr := '';
  Result := True;
  try
    while not Eof(SQLFile) do
    begin
      Readln(SQLFile, VChar);
      //car 5/12/2003 -
    //  VChar := Trim(VChar);
      if VChar <> '' then
        if Pos('//', VChar) > 0 then
        begin
          if SqlStr <> '' then
          begin
            SaveSqlStr := SqlStr;
            ExecuteSQL(SqlStr);
            SqlStr := '';
          end;
        end
        else
        //car 5/12/2003 - write on a new line each string
          SqlStr := SqlStr + #13 +  VChar;
    end;{while}
  if Eof(SqlFile) and (SqlStr <> '') then
  begin
    SaveSqlStr := SqlStr;
    ExecuteSQL(SqlStr);
  end;
  except
    on E:EdbEngineError do
    Begin
      Result := False;
      DisplayMessage(E.Message, mtError, [mbOK]);
      WErrorLog(E.Message);
      try
        ExecuteSQL('ROLLBACK');
      except
      end;
      Writeln(ErrorFile, SaveSqlStr);
      Writeln(ErrorFile, '//');
      while Not Eof(SQLFile) do
      begin
        Readln(SqlFile,SqlStr);
        Writeln(ErrorFile, SqlStr);
      end;
      Close(SQLFile);
      Close(ErrorFile);
      if FileExists(GetCurrentDir + '\TmpError' + FileName) then
      begin
        Erase(SQLFile);
        Rename(ErrorFile, GetCurrentDir + '\Error' + FileName);
      end;
      Exit;
    End;
  end;
  Close(SQLFile);
  Close(ErrorFile);
  if FileExists(GetCurrentDir + '\TmpError' + FileName) then
    Erase(SQLFile);
  Erase(ErrorFile);
end;

function TSystemDM.UpdatePimsBase(DatabaseName: String): Boolean;
var
  Pims_DB, Pims_Func_DB, SQL_DB_VERS: String;
  Index: Integer;
  SqlFileNames: TFileNames;
  QueryUpdate: TQuery;
begin
(* ORACLE TEST *)
  if (SystemDM.PimsDatabase = DB_ORACLE) then
  begin
    // soon comming to a theatre near you
    Result := True;
  end
  else
  begin
    // version of pims funct in database
    QueryUpdate := TQuery.Create(Nil);
    QueryUpdate.SessionName := 'SessionPims';
    QueryUpdate.DataBaseName := DatabaseName;

    GetPimsDBVersion(QueryUpdate, Pims_DB, Pims_Func_DB);
    Result := (PIMS_Func >= StrToInt(Pims_Func_DB));
  // test if version pims.exe is valid
    if Not Result then
    begin
      DisplayMessage(SPimsInvalidVersion, mtInformation, [mbOk]);
      QueryUpdate.Free;
      Exit;
    end;
  // find all valid sql scripts into SQLFileNames array of strings, they are stored
  // in ascending order of names
    GetSqlFiles(SQLFileNames, IntToStr(PIMS_Func), Pims_DB);
    Index := 1;
    while (Index <= MaxScriptField) and (SQLFileNames[Index] <> '') do
    begin
      GetPimsDBVersion(QueryUpdate, Pims_DB, Pims_Func_DB);
  // get database version according to pims sql script
      SQL_DB_VERS := IntToStr(GetDBVers(SQLFileNames[Index]));
  // confirm update of database
      if (DisplayMessage(SUpdateDatabaseFrom + Pims_DB + SUpdateDatabaseTo +
        SQL_DB_VERS, mtConfirmation, [mbYes, mbNo]) = mrYes) then
      begin
  // run script - update database from version of old database into a new XX taken
  // from the name of script
        try
          if ReadSqlFile(QueryUpdate, SQLFileNames[Index]) then
//            AlterTablePimsVersion(QueryUpdate, Pims_DB, 'DB_' + SQL_DB_VERS)
          else
          begin
            DisplayMessage(SPimsInvalidVersion, mtInformation, [mbOk]);
            Result := False;
            QueryUpdate.Free;
            Exit;
          end;
        except
          Result := False;
          QueryUpdate.Free;
          Exit;
        end;
      end
      else
      begin
        DisplayMessage(SPimsInvalidVersion, mtInformation, [mbOk]);
        Result := False;
        QueryUpdate.Free;
        Exit;
      end;
      Inc(Index);
    end;{while}
    QueryUpdate.Free;
  end; {DB_ORACLE}
  if Result and (DatabaseName = 'Pims') then
     OpenSystemTables;

end;

procedure TSystemDm.SynchronizeWorkStation;
begin
//  CurrentComputerName := 'ABC12345671234567890123456789012'; // For testing purposes // TD-25352
  TableWorkStation.Active := True;
  TimeRecordingStation := UNCHECKEDVALUE; // TD-25352
  if not TableWorkStation.Findkey([CurrentComputerName]) then
    TableWorkStation.InsertRecord([CurrentComputerName, 0]);
end;

// 20013923
procedure TSystemDM.AssignSettings;
begin
  TablePIMSettings.FieldByName('SAVE_DETAIL_PRD_INFO').AsInteger :=
    DetailedProd;
  TablePIMSettings.FieldByName('SAVE_DAY_PRD_INFO').AsInteger :=
    ProcessDayQantity;
  // Car 20.2.2004  Trims leading and trailing spaces
  TablePIMSettings.FieldByName('UNIT_WEIGHT').AsString := Trim(UnitsWeigh);
  TablePIMSettings.FieldByName('UNIT_PIECES').AsString := Trim(UnitsPieces);
  TablePIMSettings.FieldByName('WEEK_START_ON').AsInteger := WeekStartsOn;
  TablePIMSettings.FieldByName('SETTING_PASSWORD').AsString :=
    SettingsPassword;
  TablePIMSettings.FieldByName('VERSION_NUMBER').AsInteger := VersionNumber;
  TablePIMSettings.FieldByName('CHANGE_SCAN_YN').AsString := ChangeScan;
  TablePIMSettings.FieldByName('READ_PLANNING_ONCE_YN').AsString :=
    ReadPlanningOnceYN;
  // 20013923
  // This fields is not used anymore, but is left in database to make
  // it downwards-compatible.
//  TablePIMSettings.FieldByName('UPDATE_DELETE_EMP_YN').AsString := 'N';
  TablePIMSettings.FieldByName('UPDATE_EMP_YN').AsString :=
    UpdateEmployeeYN;
  // 20013923
  // This fields is not used anymore, but is left in database to make
  // it downwards-compatible.
//  TablePIMSettings.FieldByName('UPDATE_DELETE_PLANT_YN').AsString := 'N';
  TablePIMSettings.FieldByName('UPDATE_PLANT_YN').AsString :=
    UpdatePlantYN;
  TablePIMSettings.FieldByName('DELETE_WK_YN').AsString := DeleteWKYN;
  TablePIMSettings.FieldByName('FIRST_WEEK_OF_YEAR').AsInteger :=
    FirstWeekOfYear;
  TablePIMSettings.FieldByName('EMPLOYEES_ACROSS_PLANTS_YN').AsString :=
    EmployeesAcrossPlantsYN;
  TablePIMSettings.FieldByName('EMBED_DIALOGS_YN').AsString :=
    EmbedDialogsYN;
  TablePIMSettings.FieldByName('ABSLOG_EMPHRS_YN').AsString :=
    ABSLOG_EmpHrsYN;
  TablePIMSettings.FieldByName('ABSLOG_TRS_YN').AsString :=
    ABSLOG_TRSYN;
  TablePIMSettings.FieldByName('EFF_BO_QUANT_YN').AsString :=
    EffBasedOnQuantYN;
  TablePIMSettings.FieldByName('PS_AUTO_CLOSE_SECS').AsInteger :=
    PSAutoCloseSecs;
    // 20013923
  TablePIMSettings.FieldByName('DELETE_EMP_YN').AsString :=
    DeleteEmployeeYN;
  TablePIMSettings.FieldByName('DELETE_PLANT_YN').AsString :=
    DeletePlantYN;
  TablePIMSettings.FieldByName('UPDATE_WK_YN').AsString :=
    UpdateWKYN;
  TablePIMSettings.FieldByName('SHOW_WEEKNUMBERS_YN').AsString :=
    ShowWeeknumbersYN; // 20012944
  if UseFinalRun then
    TablePIMSettings.FieldByName('FINAL_RUN_YN').AsString := CHECKEDVALUE
  else
    TablePIMSettings.FieldByName('FINAL_RUN_YN').AsString := UNCHECKEDVALUE;
  if EnableMachineTimeRec then // 20015178
    TablePIMSettings.FieldByName('ENABLE_MACHINETIMEREC_YN').AsString := CHECKEDVALUE
  else
    TablePIMSettings.FieldByName('ENABLE_MACHINETIMEREC_YN').AsString := UNCHECKEDVALUE;
  if WorkspotInclSel then // 20015223
    TablePIMSettings.FieldByName('WORKSPOT_INCL_SEL_YN').AsString := CHECKEDVALUE
  else
    TablePIMSettings.FieldByName('WORKSPOT_INCL_SEL_YN').AsString := UNCHECKEDVALUE;
  if DateTimeSel then // 20015220
    TablePIMSettings.FieldByName('DATETIME_SEL_YN').AsString := CHECKEDVALUE
  else
    TablePIMSettings.FieldByName('DATETIME_SEL_YN').AsString := UNCHECKEDVALUE;
  if InclOpenScans then // 20015221
    TablePIMSettings.FieldByName('INCL_OPEN_SCANS_YN').AsString := CHECKEDVALUE
  else
    TablePIMSettings.FieldByName('INCL_OPEN_SCANS_YN').AsString := UNCHECKEDVALUE;
  if LogToDB then // 20014826
    TablePIMSettings.FieldByName('ERROR_LOG_TO_DB_YN').AsString := CHECKEDVALUE
  else
    TablePIMSettings.FieldByName('ERROR_LOG_TO_DB_YN').AsString := UNCHECKEDVALUE;
  if TimerecordingInSecs then // 20015346
    TablePIMSettings.FieldByName('TIMEREC_IN_SECONDS_YN').AsString := CHECKEDVALUE
  else
    TablePIMSettings.FieldByName('TIMEREC_IN_SECONDS_YN').AsString := UNCHECKEDVALUE;
  if IgnoreBreaks then // 20015346
    TablePIMSettings.FieldByName('IGNORE_BREAKS_YN').AsString := CHECKEDVALUE
  else
    TablePIMSettings.FieldByName('IGNORE_BREAKS_YN').AsString := UNCHECKEDVALUE;
  if PSShowEmpEff then // 20014450.50
    TablePIMSettings.FieldByName('PSSHOWEMPEFF_YN').AsString := CHECKEDVALUE
  else
    TablePIMSettings.FieldByName('PSSHOWEMPEFF_YN').AsString := UNCHECKEDVALUE;
  if PSShowEmpInfo then // 20014450.50
    TablePIMSettings.FieldByName('PSSHOWEMPINFO_YN').AsString := CHECKEDVALUE
  else
    TablePIMSettings.FieldByName('PSSHOWEMPINFO_YN').AsString := UNCHECKEDVALUE;
  if AddJobComment then // PIM-50
    TablePIMSettings.FieldByName('JOBCOMMENT_YN').AsString := CHECKEDVALUE
  else
    TablePIMSettings.FieldByName('JOBCOMMENT_YN').AsString := UNCHECKEDVALUE;
  if UsePackageHourCalc then // PIM-87
    TablePIMSettings.FieldByName('PACKAGE_HOUR_CALC_YN').AsString := CHECKEDVALUE
  else
    TablePIMSettings.FieldByName('PACKAGE_HOUR_CALC_YN').AsString := UNCHECKEDVALUE;
  if BreakBtnVisible then // PIM-12
    TablePIMSettings.FieldByName('BREAK_BTN_YN').AsString := CHECKEDVALUE
  else
    TablePIMSettings.FieldByName('BREAK_BTN_YN').AsString := UNCHECKEDVALUE;
  if LunchBtnVisible then // PIM-12
    TablePIMSettings.FieldByName('LUNCH_BTN_YN').AsString := CHECKEDVALUE
  else
    TablePIMSettings.FieldByName('LUNCH_BTN_YN').AsString := UNCHECKEDVALUE;
  if TREndOfDayBtnVisible then // PIM-12
    TablePIMSettings.FieldByName('TR_EOD_BTN_YN').AsString := CHECKEDVALUE
  else
    TablePIMSettings.FieldByName('TR_EOD_BTN_YN').AsString := UNCHECKEDVALUE;
  if PSEndOfDayBtnVisible then // PIM-12
    TablePIMSettings.FieldByName('PS_EOD_BTN_YN').AsString := CHECKEDVALUE
  else
    TablePIMSettings.FieldByName('PS_EOD_BTN_YN').AsString := UNCHECKEDVALUE;
  if MANREGEndOfDayBtnVisible then // PIM-12
    TablePIMSettings.FieldByName('MANREG_EOD_BTN_YN').AsString := CHECKEDVALUE
  else
    TablePIMSettings.FieldByName('MANREG_EOD_BTN_YN').AsString := UNCHECKEDVALUE;
  if ExternQtyFromPQ then // PIM-90
    TablePIMSettings.FieldByName('EXTERN_QTY_FROM_PQ_YN').AsString := CHECKEDVALUE
  else
    TablePIMSettings.FieldByName('EXTERN_QTY_FROM_PQ_YN').AsString := UNCHECKEDVALUE;
  if LunchBtnScanOut then // PIM-139
    TablePIMSettings.FieldByName('LUNCH_BTN_SCANOUT_YN').AsString := CHECKEDVALUE
  else
    TablePIMSettings.FieldByName('LUNCH_BTN_SCANOUT_YN').AsString := UNCHECKEDVALUE;
  if UpdateEmpNr then // PIM-227
    TablePIMSettings.FieldByName('UPDATE_EMPNR_YN').AsString := CHECKEDVALUE
  else
    TablePIMSettings.FieldByName('UPDATE_EMPNR_YN').AsString := UNCHECKEDVALUE;
  if ProdHrsDoNotSubtractBreaks then // PIM-319
    TablePIMSettings.FieldByName('PRODHRS_NOSUBTRACT_BREAKS').AsString := CHECKEDVALUE
  else
    TablePIMSettings.FieldByName('PRODHRS_NOSUBTRACT_BREAKS').AsString := UNCHECKEDVALUE;
  TablePIMSettings.FieldByName('MAX_TIMEBLOCKS').AsInteger := MaxTimeblocks; // GLOB3-60 Bugfix! Not the default!
  if TimeRecCRDoNotScanOut then // GLOB3-202
    TablePIMSettings.FieldByName('TIMERECCR_DONOTSCANOUT_YN').AsString := CHECKEDVALUE
  else
    TablePIMSettings.FieldByName('TIMERECCR_DONOTSCANOUT_YN').AsString := UNCHECKEDVALUE;
  if OvertimePerDay then // GLOB3-223
    TablePIMSettings.FieldByName('OVERTIME_PERDAY_YN').AsString := CHECKEDVALUE
  else
    TablePIMSettings.FieldByName('OVERTIME_PERDAY_YN').AsString := UNCHECKEDVALUE;
  if RepPlanStartEndTime then // PIM-377
    TablePIMSettings.FieldByName('REPPLAN_STARTEND_TIMES_YN').AsString := CHECKEDVALUE
  else
    TablePIMSettings.FieldByName('REPPLAN_STARTEND_TIMES_YN').AsString := CHECKEDVALUE;
  TablePIMSettings.FieldByName('OCCUP_HANDLING').AsInteger := Occup_Handling; // GLOB3-330
end; // AssignSettings;

procedure TSystemDM.SetSettings;
begin
  TablePIMSettings.First;
  TablePIMSettings.Edit;
  AssignSettings;
  TablePIMSettings.Post;
  if not TableWorkStation.Active then
    TableWorkStation.Open;
  if TableWorkStation.FindKey([CurrentComputerName]) then
  begin
    TableWorkStation.Edit;
    TableWorkStation.FieldByName('TIMERECORDING_YN').AsString :=
      TimeRecordingStation;
    TableWorkStation.Post;
  end;
  // RV093.2.
  EmbedDialogs := (EmbedDialogsYN = CHECKEDVALUE);
end;

procedure TSystemDM.GetSettings;
begin
  DatabaseTimezone := ''; // 20016449
  if TablePIMSettings.RecordCount <= 0 then
  begin
    ProcessDayQantity := DAYPRODINFODEFAULT;
    DetailedProd := DETAILEDPRODDEFAULT;
    UnitsWeigh := UNITWEIGHTDEFAULT;
    UnitsPieces := UNITPIECESDEFAULT;
    WeekStartsOn := WEEKDAYSDEFAULT;
    //car 4-12-2003
    FirstWeekOfYear := FIRSTWEEKOFYEARDEFAULT;
    SettingsPassword := PASSWORDDEFAULT;
    VersionNumber := VERSIONNUMBERDEFAULT;
    ChangeScan := CHANGESCANYNDEFAULT;
    ReadPlanningOnceYN := CHANGESCANYNDEFAULT;
    UpdateEmployeeYN := UNCHECKEDVALUE;
    DeleteWKYN := UNCHECKEDVALUE;
    EmployeesAcrossPlantsYN := UNCHECKEDVALUE;
    EmbedDialogsYN := UNCHECKEDVALUE;
    ABSLOG_EmpHrsYN := UNCHECKEDVALUE;
    ABSLOG_TRSYN := UNCHECKEDVALUE;
    // 20012858
    // 20014450.50 Always calculate eff. on time
    EffBasedOnQuantYN := UNCHECKEDVALUE;
    PSAutoCloseSecs := 10;
    // 20013923
    UpdatePlantYN := UNCHECKEDVALUE;
    DeletePlantYN := UNCHECKEDVALUE;
    DeleteEmployeeYN := UNCHECKEDVALUE;
    UpdateWKYN := UNCHECKEDVALUE;
    // 20013923
    ShowWeeknumbersYN := UNCHECKEDVALUE; // 20012944
    UseFinalRun := False; // 20011800
    EnableMachineTimeRec := False; // 20015178
    WorkspotInclSel := False; // 20015223
    DateTimeSel := False; // 20015220
    InclOpenScans := False; // 20015221
    LogToDB := False; // 20014826
    IgnoreBreaks := False; // 20015346
    TimerecordingInSecs := False; // 20015346
    PSShowEmpEff := True; // 20014450.50
    PSShowEmpInfo := True; // 20014450.50
    AddJobComment := False; // PIM-50
    UsePackageHourCalc := False; // PIM-87
    BreakBtnVisible := False; // PIM-12
    LunchBtnVisible := False; // PIM-12
    TREndOfDayBtnVisible := True; // PIM-12
    PSEndOfDayBtnVisible := True; // PIM-12
    MANREGEndOfDayBtnVisible := True; // PIM-12
    ExternQtyFromPQ := True; // PIM-90
    LunchBtnScanOut := False; // PIM-139
    FHybridMode := hmNone; // PIM-180
    UpdateEmpNr := True; // PIM-227
    ProdHrsDoNotSubtractBreaks := False; // PIM-319
    MaxTimeblocks := DefaultMaxTimeblocks; // GLOB3-60
    TimeRecCRDoNotScanOut := False; // GLOB3-202
    OvertimePerDay := False; // GLOB3-223
    RepPlanStartEndTime := False; // PIM-377
    Occup_Handling := 2; // GLOB3-330
    TablePIMSettings.Insert;
    AssignSettings;
    TablePIMSettings.Post;
  end
  else
  begin
    TablePIMSettings.First;
    DetailedProd :=
      TablePIMSettings.FieldByName('SAVE_DETAIL_PRD_INFO').AsInteger;
    ProcessDayQantity :=
      TablePIMSettings.FieldByName('SAVE_DAY_PRD_INFO').AsInteger;
    UnitsWeigh :=
      TablePIMSettings.FieldByName('UNIT_WEIGHT').AsString;
    UnitsPieces :=
      TablePIMSettings.FieldByName('UNIT_PIECES').AsString;
    WeekStartsOn :=
      TablePIMSettings.FieldByName('WEEK_START_ON').AsInteger;
    SettingsPassword :=
      TablePIMSettings.FieldByName('SETTING_PASSWORD').AsString;
    VersionNumber :=
    	 TablePIMSettings.FieldByName('VERSION_NUMBER').AsInteger;
    ChangeScan :=
    	 TablePIMSettings.FieldByName('CHANGE_SCAN_YN').AsString;
    ReadPlanningOnceYN :=
    	 TablePIMSettings.FieldByName('READ_PLANNING_ONCE_YN').AsString;
    // 20013923
    UpdateEmployeeYN :=
      TablePIMSettings.FieldByName('UPDATE_EMP_YN').AsString;
    // 20013923
    UpdatePlantYN :=
      TablePIMSettings.FieldByName('UPDATE_PLANT_YN').AsString;
    DeleteWKYN :=
      TablePIMSettings.FieldByName('DELETE_WK_YN').AsString;
    FirstWeekOfYear :=
      TablePIMSettings.FieldByName('FIRST_WEEK_OF_YEAR').AsInteger;
    EmployeesAcrossPlantsYN :=
      TablePIMSettings.FieldByName('EMPLOYEES_ACROSS_PLANTS_YN').AsString;
    EmbedDialogsYN :=
      TablePIMSettings.FieldByName('EMBED_DIALOGS_YN').AsString;
    ABSLOG_EmpHrsYN :=
      TablePIMSettings.FieldByName('ABSLOG_EMPHRS_YN').AsString;
    ABSLOG_TRSYN :=
      TablePIMSettings.FieldByName('ABSLOG_TRS_YN').AsString;
    // 20014450.50 Always calculate eff. based on time
    EffBasedOnQuantYN := UNCHECKEDVALUE;
//      TablePIMSettings.FieldByName('EFF_BO_QUANT_YN').AsString;
    PSAutoCloseSecs :=
      TablePIMSettings.FieldByName('PS_AUTO_CLOSE_SECS').AsInteger;
    // 20013923
    DeleteEmployeeYN := TablePIMSettings.FieldByName('DELETE_EMP_YN').AsString;
    DeletePlantYN := TablePIMSettings.FieldByName('DELETE_PLANT_YN').AsString;
    UpdateWKYN := TablePIMSettings.FieldByName('UPDATE_WK_YN').AsString;
    ShowWeeknumbersYN :=
      TablePIMSettings.FieldByName('SHOW_WEEKNUMBERS_YN').AsString; // 20012944
    UseShiftDateSystem :=
      (TablePIMSettings.FieldByName('SHIFTDATESYSTEM_YN').AsString = 'Y'); // 20014327
    // 20011800.80 Disable Final Run
{
    UseFinalRun :=
      (TablePIMSettings.FieldByName('FINAL_RUN_YN').AsString = CHECKEDVALUE); // 20011800
}
    EnableMachineTimeRec :=
      (TablePIMSettings.FieldByName('ENABLE_MACHINETIMEREC_YN').AsString = CHECKEDVALUE); // 20015178
    WorkspotInclSel :=
      (TablePIMSettings.FieldByName('WORKSPOT_INCL_SEL_YN').AsString = CHECKEDVALUE); // 20015223
    // PIM-12 Do not use this anymore!
//    DateTimeSel :=
//      (TablePIMSettings.FieldByName('DATETIME_SEL_YN').AsString = CHECKEDVALUE); // 20015220
//    InclOpenScans :=
//      (TablePIMSettings.FieldByName('INCL_OPEN_SCANS_YN').AsString = CHECKEDVALUE); // 20015221
    LogToDB :=
      (TablePIMSettings.FieldByName('ERROR_LOG_TO_DB_YN').AsString = CHECKEDVALUE); // 20014826
    TimerecordingInSecs :=
      (TablePIMSettings.FieldByName('TIMEREC_IN_SECONDS_YN').AsString = CHECKEDVALUE); // 20015346
    IgnoreBreaks :=
      (TablePIMSettings.FieldByName('IGNORE_BREAKS_YN').AsString = CHECKEDVALUE); // 20015346
    try
      DatabaseTimezone :=
        TablePIMSettings.FieldByName('TIMEZONE').AsString; // 20016449
    except
      DatabaseTimezone := '';
    end;
    PSShowEmpInfo :=
      (TablePIMSettings.FieldByName('PSSHOWEMPINFO_YN').AsString = CHECKEDVALUE); // 20014450.50
    if PSShowEmpInfo then
      PSShowEmpEff :=
        (TablePIMSettings.FieldByName('PSSHOWEMPEFF_YN').AsString = CHECKEDVALUE) // 20014450.50
    else
      PSShowEmpEff := False;
    AddJobComment :=
      (TablePIMSettings.FieldByName('JOBCOMMENT_YN').AsString = CHECKEDVALUE); // PIM-50
    UsePackageHourCalc :=
      (TablePIMSettings.FieldByName('PACKAGE_HOUR_CALC_YN').AsString = CHECKEDVALUE); // PIM-87
    BreakBtnVisible := TablePIMSettings.FieldByName('BREAK_BTN_YN').AsString = CHECKEDVALUE; // PIM-12
    LunchBtnVisible := TablePIMSettings.FieldByName('LUNCH_BTN_YN').AsString = CHECKEDVALUE; // PIM-12
    TREndOfDayBtnVisible := TablePIMSettings.FieldByName('TR_EOD_BTN_YN').AsString = CHECKEDVALUE; // PIM-12
    PSEndOfDayBtnVisible := TablePIMSettings.FieldByName('PS_EOD_BTN_YN').AsString = CHECKEDVALUE; // PIM-12
    MANREGEndOfDayBtnVisible := TablePIMSettings.FieldByName('MANREG_EOD_BTN_YN').AsString = CHECKEDVALUE; // PIM-12
    ExternQtyFromPQ := TablePIMSettings.FieldByName('EXTERN_QTY_FROM_PQ_YN').AsString = CHECKEDVALUE; // PIM-90
    LunchBtnScanOut := TablePIMSettings.FieldByName('LUNCH_BTN_SCANOUT_YN').AsString = CHECKEDVALUE; // PIM-139
    FHybridMode := hmNone; // PIM-180
    try // PIM-180
      if TablePIMSettings.FieldByName('HYBRID').AsString <> '' then
        FHybridMode := THybridMode(TablePIMSettings.FieldByName('HYBRID').AsInteger);
    except
      FHybridMode := hmNone;
    end;
    UpdateEmpNr := TablePIMSettings.FieldByName('UPDATE_EMPNR_YN').AsString = CHECKEDVALUE; // PIM-227
    ProdHrsDoNotSubtractBreaks := TablePIMSettings.FieldByName('PRODHRS_NOSUBTRACT_BREAKS').AsString = CHECKEDVALUE; // PIM-319
    try
      MaxTimeblocks := TablePIMSettings.FieldByName('MAX_TIMEBLOCKS').AsInteger;
      if not ((MaxTimeblocks = DefaultMaxTimeblocks) or (MaxTimeblocks = DefaultMaxTimeblocks10)) then
        MaxTimeblocks := DefaultMaxTimeblocks;
    except
      MaxTimeblocks := DefaultMaxTimeblocks;
    end;
    TimeRecCRDoNotScanOut := TablePIMSettings.FieldByName('TIMERECCR_DONOTSCANOUT_YN').AsString = CHECKEDVALUE; // GLOB3-202
    try
      OvertimePerDay := TablePIMSettings.FieldByName('OVERTIME_PERDAY_YN').AsString = CHECKEDVALUE; // GLOB3-223
    except
      OvertimePerDay := False;
    end;
    try
      RepPlanStartEndTime := TablePIMSettings.FieldByName('REPPLAN_STARTEND_TIMES_YN').AsString = CHECKEDVALUE; // PIM-377
    except
      RepPlanStartEndTime := False;
    end;
    try
      Occup_Handling := TablePIMSettings.FieldByName('OCCUP_HANDLING').AsInteger; // GLOB3-330
    except
      Occup_Handling := 2;
    end;
  end;
  TimeRecordingStation := TIMERECORDINGSTATIONDEFAULT;
  if not TableWorkStation.Active then
    TableWorkStation.Open;

  if TableWorkStation.FindKey([CurrentComputerName]) then
    TimeRecordingStation := TableWorkStation.FieldByName('TIMERECORDING_YN').
      AsString;
  // RV093.2.
  EmbedDialogs := (EmbedDialogsYN = CHECKEDVALUE);

  PlantTimezonehrsdiffWriteToDB; // 20016449
  DetermineWorkstationTimezonehrsdiff; // 20016449
end;

function TSystemDM.GetFillProdHourPerHourTypeYN: String;
begin
  if not TablePIMSettings.Active then
    TablePIMSettings.Open;
  Result := TablePIMSettings.FieldByName('FILLPRODUCTIONHOURS_YN').AsString;
end;

function MyGetEnvironmentVariable(AValue: String): String;
var
  NewLength, nSize: DWORD;
  lpBuffer: Array[1..256] of Char; { do not use a PChar for Win95 }
begin
  Result := '';
  nSize := 255;
  NewLength := GetEnvironmentVariable(PChar(AValue), @lpBuffer, nSize);
  if NewLength > 0 then
  begin
    Result := String(lpBuffer);
    SetLength(Result, NewLength);
  end;
end;

function TSystemDM.GetCurrentClientEnvName: String;
begin
  Result := MyGetEnvironmentVariable('CLIENTNAME');
end;

function TSystemDM.GetCurrentComputerEnvName: String;
begin
  Result := MyGetEnvironmentVariable('COMPUTERNAME');
end;

// MR:22-01-2008 RV002:
// Get ComputerName by:
// - First look for environment variable CLIENTNAME.
//   If empty then look for environment variable COMPUTERNAME.
//   If this is also empty then look for COMPUTERNAME using a Windows-function.
// This change was needed when Pims is used with Terminal Services, to
// prevent the COMPUTERNAME is the Server-name instead of the Client-name.
// MR:10-06-2008 RV007:
// - For CLIENTNAME: If contents is 'Console' then see this also as ''.
function TSystemDM.GetCurrentComputerName: String;
const
  NEW_MAX_COMPUTERNAME_LENGTH=32; // TD-25352
var
  LengthComputerName: DWORD;
  pComputerName     : Array[1..NEW_MAX_COMPUTERNAME_LENGTH] of Char; { do not use a PChar for Win95 } // TD-25352
begin
  if FCurrentComputerName <> '' then
  begin
    Result := FCurrentComputerName;
    Exit;
  end;
  Result := CurrentClientEnvName;
  if (Result = '') or (UpperCase(Result) = 'CONSOLE') then
  begin
    Result := CurrentComputerEnvName;
    if Result = '' then
    begin
      LengthComputerName := NEW_MAX_COMPUTERNAME_LENGTH; // MAX_COMPUTERNAME_LENGTH + 1; // TD-25352
      if GetComputerName(@pComputerName, LengthComputerName) then
      begin
        Result := String(pComputerName);
        // result GetComputerName is without ZeroTerminator
        SetLength(Result, LengthComputerName);
      end
      else
        Result := 'DEMOPC';
    end;
  end;
  FCurrentComputerName := Result;
end;

// RV089.1.
{
procedure DisableProcessWindowsGhosting;
var
  DisableProcessWindowsGhostingImp : procedure;
begin
  try
    @DisableProcessWindowsGhostingImp :=
      GetProcAddress(GetModuleHandle('user32.dll'),
      'DisableProcessWindowsGhosting');
  if @DisableProcessWindowsGhostingImp <> nil then
  DisableProcessWindowsGhostingImp;
  except
    on E:Exception do
    begin
      OutputDebugString(PChar('DisableProcessWindowsGhosting error:'+E.Message));
    end;
  end;
end;
}

// End PIMS Settings Tools.
// RV089.1. Changes made to get DisplayMessage always on top.
function DisplayMessage(const Msg: string; DlgType: TMsgDlgType;
  Buttons: TMsgDlgButtons): Integer;
var
  B: TMsgDlgBtn;
  MyBtn: Integer;
  MyDlgType: Integer;
  Count: Integer;
begin
// Map this (Delphi-Dialog):
//  TMsgDlgBtn = (mbYes, mbNo, mbOK, mbCancel, mbAbort, mbRetry, mbIgnore,
//    mbAll, mbNoToAll, mbYesToAll, mbHelp);
// REMARK: The windows-dialog does not have ALL buttons compared with Delphi-
//         dialog!
// To Windows-Dialog-type:
// MB_ABORTRETRYIGNORE, MB_CANCELTRYCONTINUE, MB_HELP, MB_OK, MB_OKCANCEL,
// MB_RETRYCANCEL, MB_YESNO, MB_YESNOCANCEL
  MyBtn := MB_OK;
  Count := 0;
  for B := Low(TMsgDlgBtn) to High(TMsgDlgBtn) do
    if B in Buttons then
    begin
      if B in [mbHelp] then
        MyBtn := MB_HELP;
      if B in [mbAbort, mbRetry, mbIgnore] then
        MyBtn := MB_ABORTRETRYIGNORE;
      if B in [mbYes, mbNo] then
        MyBtn := MB_YESNO;
      if B in [mbOK] then
        MyBtn := MB_OK;
      if B in [mbCancel] then
        MyBtn := MB_OKCANCEL;
      inc(Count);
    end;
  // Trap this: Sometimes only YES is used instead of OK.
  if (Count = 1) and (MyBtn = MB_YESNO) then
    MyBtn := MB_OK;

// Map DialogType to Windows-Dialog-Types.
// TMsgDlgType = (mtWarning, mtError, mtInformation, mtConfirmation, mtCustom);
  case DlgType of
  mtWarning:      MyDlgType := MB_ICONWARNING;
  mtError:        MyDlgType := MB_ICONERROR;
  mtInformation:  MyDlgType := MB_ICONINFORMATION;
  mtConfirmation: MyDlgType := MB_ICONQUESTION;
  else
    MyDlgType := MB_ICONINFORMATION;
  end;

  Result := Windows.MessageBox(Application.Handle,
     PChar(Msg),
     'PIMS',
     MyBtn or
     MB_SYSTEMMODAL or MB_SETFOREGROUND or MB_TOPMOST or MyDlgType);

//    Result := MessageDlg(Msg, DlgType, Buttons, 0);
end;

function DisplayMessageFmt(const Msg: string; const Args: array of const;
  DlgType: TMsgDlgType; Buttons: TMsgDlgButtons): Integer;
begin
  Result := DisplayMessage(Format(Msg, Args), DlgType, Buttons);
end;

function DoubleQuote(Code: string): string;
begin
  Result := StringReplace(Code,'''','''''',[rfReplaceAll]);
end;

function IsErrorFromException(ErrorMessage: String;
  var ErrorCode: Integer): Boolean;
var
  SubStr: String;
  PosStr: Integer;
begin
  ErrorCode := 0;
  PosStr := Pos('~',ErrorMessage);
  if (PosStr > 0) then
  begin
    SubStr := Copy(ErrorMessage, PosStr + 1, 2);
    PosStr := Pos('~', SubStr);
    if (PosStr > 0) then
      ErrorCode := StrToInt(Copy(SubStr, PosStr - 1, 1));
  end;
  Result := (ErrorCode > 0);
end;

function GetErrorMessage(const PimsError: EDataBaseError;
  const PimsActivity: Integer): String;
var
  iDBIErrorCode, EXErrorCode: Integer;
  iDBIErrorMessage: String;
begin
  iDBIErrorCode := 0;
  iDBIErrorMessage := '';
  if (PimsError is EDBEngineError) then
  begin
    iDBIErrorCode := (PimsError as EDBEngineError).Errors[0].Errorcode;
    iDBIErrorMessage := PimsError.Message;
  end;
  if IsErrorFromException(iDBIErrorMessage, EXErrorCode) then
    iDBIErrorCode := EXErrorCode;
  case iDBIErrorCode of
    PIMS_DBIERR_KEYVIOL            : Result := SPimsKeyViolation;
    PIMS_DBIERR_REQDERR            : Result := SPimsFieldRequired;
    PIMS_DBIERR_FORIEGNKEYERR      :
      if PimsActivity = PIMS_POSTING then
        Result := SPimsForeignKeyPost
      else
        Result := SPimsForeignKeyDel;
    PIMS_DBIERR_DETAILRECORDSEXIST : Result := SPimsDetailsExist;
    PIMS_DBIERR_LOCKED: Result := SPimsRecordLocked;
    PIMS_EX_DELETE_LAST_RECORD : Result := SPimsTBDeleteLastRecord;
    PIMS_EX_MAX_RECORD : Result := SPimsTBInsertRecord;
    PIMS_EX_STARTENDDATE : Result := SPimsStartEndDate;
    PIMS_EX_STARTENDTIME : Result := SPimsStartEndTime;
    PIMS_EX_DATEINPREVIOUSPERIOD : Result := SPimsDateinPreviousPeriod;
    //CAR 15-1-2004 - Add an exception message
    PIMS_EX_CHECKEMPLCONTRDATEPREVIOUS :
      Result := SPimsEmplContractInDatePrevious;
    PIMS_EX_UNIQUEFIELD : Result := SPimsKeyViolation;
    // CAR 27.02.2003 - ERROR IN ILLNESSMESG
    PIMS_EX_CHECKILLMSGCLOSE: Result := SPimsEXCheckIllMsgClose;
    PIMS_EX_CHECKILLMSGSTARTDATE: Result := SPimsEXCheckIllMsgStartDate;
    PIMS_DBIERR_GENERAL_SQL_ERROR: Result := SPimsGeneralSQLError +
      #13 + iDBIErrorMessage; // RV067.MRA.11.
    PIMS_RECORD_KEY_DELETED: Result := SPimsRecordKeyDeleted; // GLOB3-94
  else
    // RV067.MRA.11.
    if iDBIErrorMessage <> '' then
      Result := 'Error: ' + IntToStr(iDBIErrorCode) + ' - ' + iDBIErrorMessage
    else
      Result := 'Unknown Error: ' + IntToStr(iDBIErrorCode);
  end; { case }
end;

function DisplayErrorMessage(const PimsError: EDataBaseError;
  const PimsActivity: Integer):Integer;
begin
  MessageBeep(MB_ICONHAND);
  Result := DisplayMessage(GetErrorMessage(PimsError, PimsActivity), mtWarning,
    [mbOk]);
end;

function DisplayVersionInfo: String;
var
  V1,       // Major Version
  V2,       // Minor Version
  V3,       // Release
  V4: Word; // Build Number
  Msg: String;
begin
  GetBuildInfo(V1, V2, V3, V4);
  if SystemDM.EnvVarRead then
    Msg := 'EVAR=Y'
  else
    Msg := 'EVAR=N';
  Result := IntToStr(V1) + '.'  + IntToStr(V2) + '.'  + IntToStr(V3) + '.'
    + IntToStr(V4) + ' ' + Msg;
end;

procedure GetBuildInfo(var V1, V2, V3, V4: Word);
var
  VerInfoSize, VerValueSize, Dummy : DWORD;
  VerInfo : Pointer;
  VerValue : PVSFixedFileInfo;
begin
  VerInfoSize := GetFileVersionInfoSize(PChar(ParamStr(0)), Dummy);
  GetMem(VerInfo, VerInfoSize);
  GetFileVersionInfo(PChar(ParamStr(0)), 0, VerInfoSize, VerInfo);
  VerQueryValue(VerInfo, '\', Pointer(VerValue), VerValueSize);
  with VerValue^ do
  begin
    V1 := dwFileVersionMS shr 16;
    V2 := dwFileVersionMS and $FFFF;
    V3 := dwFileVersionLS shr 16;
    V4 := dwFileVersionLS and $FFFF;
  end;
  FreeMem(VerInfo, VerInfoSize);
end;

procedure CheckValidNumber(var Key:Char);
const
  ValidKeys  : set of Char = [#08, #13] + [#48..#57];
begin
  if not( Key in ValidKeys) then
     Key := #0;
end;

procedure ActiveTables(FDataModule: TDataModule; ActiveYN: Boolean);
var
  DataSetCounter: Integer;
  DS: TDataSource;
  Temp: TComponent;
begin
  if FDataModule <> nil then
  for DataSetCounter := 0 to FDataModule.ComponentCount - 1 do
  begin
    Temp := FDataModule.Components[DataSetCounter];
    if (Temp is TTable) then
    begin
      if ((Temp as TTable).TableName <> '') then
      begin
        DS := (Temp as TTable).MasterSource;
        if (DS <> nil) and (TTable(DS.DataSet) <> nil) and
        	(TTable(DS.DataSet).TableName <> '')  then
        try
          TTable(DS.DataSet).Active := ActiveYN;
        except
        end;
        // TESTING!!!
//        DisplayMessage((Temp as TTable).TableName, mtInformation, [mbOK]);
        (Temp as TTable).Active := ActiveYN;
      end;
    end;
  end;
end;

procedure TSystemDM.DefaultNotEmptyValidate(Sender: TField);
begin
  if Sender.AsString = '' then
  begin
    DisplayMessageFmt(SPimsFieldRequiredFmt,[(Sender as TField).FieldName],
      mtWarning, [mbOK]);
    SysUtils.Abort;
  end;
end;

function TSystemDM.GetCurrentProgramUser: String;
var
  LengthUserName: DWORD;
  pUserName     : Array[1..21] of Char; { do not use a PChar for Win95 }
begin
  // 20014036
  if IsBCH then
  begin
    Result := CurrentLoginUser;
  end
  else
  begin
    LengthUserName := 21; // maxusername = 20 chars + ZeroTerminator
    if GetUserName(@pUserName, LengthUserName) then
    begin
      Result := String(pUserName);
      // strip zeroterminator
      SetLength(Result, (LengthUserName - 1));
    end
    else
      Result := 'DEMOUSER';
  end;
end;

procedure TSystemDM.DefaultBeforeDelete(DataSet: TDataSet);
begin
  if DisplayMessage(SPimsConfirmDelete, mtConfirmation, [mbYes, mbNo])
    <> mrYes then
    SysUtils.Abort;
end;

procedure TSystemDM.DefaultBeforePost(DataSet: TDataSet);
var
  FieldCounter: Integer;
  Str: String;
begin
  for FieldCounter := 0 to (DataSet.FieldCount - 1) do
  begin
    if (DataSet.Fields[FieldCounter].Tag = 1) then
      DefaultNotEmptyValidate(DataSet.Fields[FieldCounter]);
    //Car 19.2.2004 - delete spaces before and after a string value
    if (DataSet.Fields[FieldCounter] is TStringField) then
        if DataSet.Fields[FieldCounter].Value <> '' then
        begin
          // MR:06-04-2004 Only do a trim if it's really needed...
          // Otherwise an 'OnChange' can be triggered without need.
          Str := DataSet.Fields[FieldCounter].AsString;
          if Length(Str) > 0 then
            if (Str[1] = ' ') or (Str[Length(Str)] = ' ') then
              DataSet.Fields[FieldCounter].AsString :=
                Trim(DataSet.Fields[FieldCounter].AsString);
        end;
  end;
  DataSet.FieldByName('MUTATIONDATE').Value := Now;
  DataSet.FieldByName('MUTATOR').Value      := CurrentProgramUser;
end;

procedure TSystemDM.DefaultDeleteError(DataSet: TDataSet;
  E: EDatabaseError; var Action: TDataAction);
begin
  DisplayErrorMessage(E, PIMS_DELETING);
  Action := daAbort;
end;

procedure TSystemDM.DefaultEditError(DataSet: TDataSet;
  E: EDatabaseError; var Action: TDataAction);
begin
  DisplayErrorMessage(E, PIMS_EDITING);
  Action := daAbort;
end;

procedure TSystemDM.DefaultNewRecord(DataSet: TDataSet);
var
  CurrentDate: TDateTime;
begin
//CAR 23-7-2003 - Call Now function once because 'Now'is a select in Interbase
// system table
  CurrentDate := Now;
  DataSet.FieldByName('CREATIONDATE').Value := CurrentDate;
  DataSet.FieldByName('MUTATIONDATE').Value := CurrentDate;
  DataSet.FieldByName('MUTATOR').Value      := CurrentProgramUser;
end;

procedure TSystemDM.DefaultPostError(DataSet: TDataSet;
  E: EDatabaseError; var Action: TDataAction);
begin
  DisplayErrorMessage(E, PIMS_POSTING);
  Action := daAbort;
end;

procedure TSystemDM.DeletePivotTableNotUsed(TableName: String);
begin
  TableTmpPivot.TableName := TableName + CurrentComputerName;
  if TableTmpPivot.Exists then
  begin
    TableTmpPivot.Close;
    try
      TableTmpPivot.DeleteTable;
    except
    end;
  end;
  TableTmpPivot.Close;
  TableTmpPivot.TableName := '';
end;

// MRA:02-SEP-2008 Multi database users option
procedure TSystemDM.DetermineDatabaseUser;
const
  PimINIFilename='PIMSORA.INI';
  PimsDatabaseUserName='PIMS';
  PimsDatabasePassword='PIMS';
var
  AppRootPath: String;
  MyDatabase: String;
  MyDatabaseUsername: String;
  MyDatabasePassword: String;
  ORASettingsFound: Boolean;
  function PathCheck(Path: String): String;
  begin
    if Path <> '' then
      if Path[length(Path)] <> '\' then
        Path := Path + '\';
    Result := Path;
  end;
  procedure ReadINIFile;
  var
    Ini: TIniFile;
    function SetEnvVarValue(const VarName,
      VarValue: String): Integer; // GLOB3-337
    begin
      // Simply call API function
      if Windows.SetEnvironmentVariable(PChar(VarName),
        PChar(VarValue)) then
        Result := 0
      else
        Result := GetLastError;
    end;
    function ReadMakeOracleSettings(ASection: String): Boolean; // GLOB3-337
    var
       OracleHomeEnv: String;
       PathEnv: String;
       TNSAdminEnv: String;
       NLSLangEnv: String;
    begin
      Result := False;
      // Also read/set environment variables here:
      {
        Set ORACLE_HOME=C:\PIMSROOT\OraClient32b
        Set PATH=C:\PIMSROOT\OraClient32b
        Set TNS_ADMIN=C:\PIMSROOT\OraClient32b\network\admin
        Set NLS_LANG=SIMPLIFIED CHINESE_CHINA.ZHS16GBK
      }
      Self.EnvVarRead := False;
      OracleHomeEnv := '';
      PathEnv := '';
      TNSAdminEnv := '';
      NLSLangEnv := '';
      if Ini.ValueExists(ASection, 'ORACLE_HOME') then
        OracleHomeEnv := Ini.ReadString(ASection, 'ORACLE_HOME', OracleHomeEnv);
      // GLOB3-370
      if (OracleHomeEnv <> '') and DirectoryExists(OracleHomeEnv) then
      begin
        if Ini.ValueExists(ASection, 'PATH') then
          PathEnv := Ini.ReadString(ASection, 'PATH', PathEnv);
        if Ini.ValueExists(ASection, 'TNS_ADMIN') then
          TNSAdminEnv := Ini.ReadString(ASection, 'TNS_ADMIN', TNSAdminEnv);
        if Ini.ValueExists(ASection, 'NLS_LANG') then
          NLSLangEnv := Ini.ReadString(ASection, 'NLS_LANG', NLSLangEnv);
        if OracleHomeEnv <> '' then
        begin
          SetEnvVarValue('ORACLE_HOME', OracleHomeEnv);
          Self.EnvVarRead := True;
        end;
        if PathEnv <> '' then
        begin
          SetEnvVarValue('PATH', PathEnv + ';' + MyGetEnvironmentVariable('PATH'));
          Self.EnvVarRead := True;
        end;
        if TNSAdminEnv <> '' then
        begin
          SetEnvVarValue('TNS_ADMIN', TNSAdminEnv);
          Self.EnvVarRead := True;
        end;
        if NLSLangEnv <> '' then
        begin
          SetEnvVarValue('NLS_LANG', NLSLangEnv);
          Self.EnvVarRead := True;
        end;
      end
      else
        OracleHomeEnv := '';
      Result := Self.EnvVarRead;
    end; // ReadMakeOracleSettings
  begin
    if FileExists(PathCheck(AppRootPath) + PimINIFilename) then
    begin
      Ini := TIniFile.Create(PathCheck(AppRootPath) + PimINIFilename);
      try
        MyDatabase := Ini.ReadString('PIMSORA', 'Database', MyDatabase);
        MyDatabaseUsername :=
          Ini.ReadString('PIMSORA', 'DatabaseUsername', MyDatabaseUsername);
        if MyDatabaseUsername = '' then
          MyDatabaseUsername := PimsDatabaseUserName;
        MyDatabasePassword :=
          Ini.ReadString('PIMSORA', 'DatabasePassword', MyDatabasePassword);
        if MyDatabasePassword = '' then
          MyDatabasePassword := PimsDatabasePassword;
        ORASettingsFound := ReadMakeOracleSettings('ORA'); // GLOB3-337
{$IFDEF PIMS}
        if not ORASettingsFound then
          ReadMakeOracleSettings('PIMS'); // GLOB3-337
{$ENDIF}
{$IFDEF ADMTOOL}
        if not ORASettingsFound then
          ReadMakeOracleSettings('ADMTOOL'); // GLOB3-337
{$ENDIF}
      finally
        Ini.Free;
      end;
    end;
  end;
begin
  AppRootPath := GetCurrentDir;
  // MyDatabase is set through the BDE-configuration,
  // so it cannot be changed from here!
  MyDatabase := 'ABS1';
  MyDatabaseUsername := PimsDatabaseUsername;
  MyDatabasePassword := PimsDatabasePassword;
  ReadINIFile;
  MyDatabaseUsername := UpperCase(MyDatabaseUsername);
  MyDatabasePassword := UpperCase(MyDatabasePassword);
  if not
    ((MyDatabaseUsername = PimsDatabaseUsername) and
     (MyDatabasePassword = PimsDatabasePassword)) then
  begin
    // Set parameters for database based on ini-file.
    Pims.Params.Clear;
    Pims.Params.Add('USER NAME=' + MyDatabaseUsername);
    Pims.Params.Add('PASSWORD=' + MyDatabasePassword);
    Pims.Params.Add('BLOBS TO CACHE=-1'); // 20014826
  end
  else
    Pims.Params.Add('BLOBS TO CACHE=-1'); // 20014826
end;

procedure TSystemDM.DataModuleCreate(Sender: TObject);
var
  PrivateDir: String;
  Buffer : Array[0..Max_path] of Char;
//  HaltNow: Boolean;
begin
  FCurrentComputerName := '';
  AppName := 'Pims'; // 20014826 Name of current application
  // ORA11.
  SessionPims.NetFileDir := ExtractFilePath(Application.EXEName);

  // MRA:28-OCT-2008 Multi database users option, read database user
  //                 and password from INI-file.
  DetermineDatabaseUser;

  // MR:18-10-2006 Init variables for Session, e.g. NLS_SORT=BINARY.
  qryInitSession.ExecSQL;

  // MR:10-02-2005 Order 550373
  PimsNextNow := NewNextNow;
  // Create a variable for 'TDateFmt'-class
  ADateFmt := TDateFmt.Create;
  // update baza only if the application is not Pims or Adm Tool
  // CAR 10-10-2003: Add condition for production screen also
  // SO-20014826 This is old stuff, skip it!
{
  if ((Application.Title <> 'PIMS - UNIT MAINTENANCE') and
    (Application.Title <> 'ORCL - PIMS - UNIT MAINTENANCE')) and
   ((Application.Title <> 'ADMINISTRATION - TOOL') and
   (Application.Title <> 'ORCL - ADMINISTRATION - TOOL')) and
   (Application.Title <> 'PIMS - PRODUCTION SCREEN') then
    UpdatePimsBase('Pims');
}
  //CAR 20-10-2003 :initialize saved values for timerecscanning
  ASaveTimeRecScanning:= TSaveTimeRecScanning.Create;
  ASaveTimeRecScanning.ContractGroupCode := ''; // RV082.5.

   // MR:19-05-2004 Why is this done here?
   // Also, this 'dialog' is not even created at this point.
//   ShowLevel := DialogSettingsF.RegistryShowLevel;
   // Read it in this way:
//   ShowLevel := ReadRegistryShowLevel;
//   ShowLevelC := ReadRegistryShowLevelC;
   // MR:12-01-2005 the whole 'Showlevel'-code is taken out here
   // and put in the place where it belongs: 'staffplanningfrm'
   // and related files.

// check 3-11-2003 for lock files
  if ((Application.Title = 'PIMS - UNIT MAINTENANCE') or
   (Application.Title = 'ORCL - PIMS - UNIT MAINTENANCE')) then
  begin
    SessionPims.NetFileDir := ExtractFilePath(Application.EXEName);
    // get a temporary path
    FillChar(Buffer,Max_Path + 1, 0);
    GetTempPath(Max_path, Buffer);
    PrivateDir := String(Buffer);
    if PrivateDir[Length(PrivateDir)] <> '\' then
      PrivateDir := PrivateDir + '\';
    // delete if lock files are already created
    if FileExists(PrivateDir + 'PARADOX.LCK') then
      DeleteFile(PrivateDir + 'PARADOX.LCK');
    if FileExists(PrivateDir + 'PDOXUSRS.LCK') then
      DeleteFile(PrivateDir + 'PDOXUSRS.LCK');

    //set private directory
    try
      SessionPims.PrivateDir :=  PrivateDir;
    except
    end;
  end;
  GetExportPayroll; (* RV023 *)
  SetExportPayrollType(
    TableExportPayroll.FieldByName('EXPORT_TYPE').AsString); (* RV023 *)
  // RV050.8.
  PlantTeamFilterOn := False;
  // RV077.5.
  IsAttent := False;
  // RV085.19.
  IsCVL := False;
  // RV089.1. Done during 'getsettings and setsettings'.
//  EmbedDialogs := (EmbedDialogsYN = CHECKEDVALUE);
  // RV092.8.
  IsLTB := False;
  // 20013723
  IsCLF := False;
  // 20014036
  IsBCH := False;
  // 20014327
  UseShiftDateSystem := False;
  // 20014715
  IsWMU := False;
  UseFinalRun := False; // 20011800
  SysAdmin := False; // 20011800
  // 20014442
  IsVOS := False;
  EnableMachineTimeRec := False; // 20015178
  WorkspotInclSel := False; // 20015223
  DateTimeSel := False; // 20015220
  InclOpenScans := False; // 20015221
  LogToDB := False; // 20014826
  IgnoreBreaks := False; // 20015346
  TimerecordingInSecs := False; // 20015346
  PSShowEmpEff := True; // 20014450.50
  PSShowEmpInfo := True; // 20014450.50
  AddJobComment := False; // PIM-50
  UsePackageHourCalc := False; // PIM-87
  BreakBtnVisible := False; // PIM-12
  LunchBtnVisible := False; // PIM-12
  TREndOfDayBtnVisible := True; // PIM-12
  PSEndOfDayBtnVisible := True; // PIM-12
  MANREGEndOfDayBtnVisible := True; // PIM-12
  ExternQtyFromPQ := True; // PIM-90
  LunchBtnScanOut := False; // PIM-139
  FHybridMode := hmNone; // PIM-180
  UpdateEmpNr := True; // PIM-227
  ProdHrsDoNotSubtractBreaks := False; // PIM-319
  IsNTS := False; // GLOB3-86
  MaxTimeblocks := DefaultMaxTimeblocks; // GLOB3-60
  TimeRecCRDoNotScanOut := False; // GLOB3-202
  OvertimePerDay := False; // GLOB3-223
  RepPlanStartEndTime := False; // PIM-377
  Occup_Handling := 2; // GLOB3-330
  // 20016449
  try
    cdsTimeZone.CreateDataSet;
    cdsTimeZone.LogChanges := False;
    cdsTimeZoneFillList;
  except
  end;
  // ABS-27052
  try
    DialogSelectPrinterF := TDialogSelectPrinterF.Create(Self);
    if not DialogSelectPrinterF.DefaultPrinterFound then
      DialogSelectPrinterF.ShowModal;
  finally
    DialogSelectPrinterF.Free;
  end;
end; // DataModuleCreate

// call this function into the UpdatePimsBase procedure if the update result
//is True
procedure TSystemDM.OpenSystemTables;
begin
  DeletePivotTableNotUsed(TABLETEMPPROD);
  DeletePivotTableNotUsed(TABLETEMPSAL);
  DeletePivotTableNotUsed(TABLETEMPABS);
  ActiveTables(SystemDM, True);
  //CAR 7-3-2003
  ClientDataSetDay.Open;
  ClientDataSetDay.LogChanges := False; // MR:03-04-2006 Performance.
  SynchronizeWorkStation;
  GetSettings;
  // RV093.3.
  ABSLogEnable;
end;

procedure TSystemDM.DataModuleDestroy(Sender: TObject);
begin
  ADateFmt.Free;
  ActiveTables(SystemDM, False);
  DeletePivotTableNotUsed(TABLETEMPPROD);
  DeletePivotTableNotUsed(TABLETEMPSAL);
  DeletePivotTableNotUsed(TABLETEMPABS);
  ASaveTimeRecScanning.Free;
  // RV050.8.
  PlantTeamFilterOn := False;
  qryPlantDeptTeam.Close;
end;

// GLOB3-284
function TSystemDM.GetDayIndex(Index: Integer): Integer;
begin
  if WeekStartsOn = 2 then // Monday
  begin
    if Index = 7 then
      Index := 1
    else
      Index := (Index + 1);
  end;
  if WeekStartsOn = 4 then // Wednesday
  begin
    Index := ListProcsF.PimsDayOfWeek(Index) - 1;
    if Index = 0 then
      Index := 7;
  end;
  if WeekStartsOn = 7 then // Saturday
  begin
    if Index = 1 then
      Index := 7
    else
      Index := (Index - 1);
  end;
  Result := Index;
end;

function TSystemDM.GetDayWDescription(Index: Integer): String;
begin
  Index := GetDayIndex(Index); // GLOB3-284
  Result := '';
  // CAR 7-3-2003
  if ClientDataSetDay.FindKey([Index]) then
    Result :=  ClientDataSetDay.FieldByName('DESCRIPTION').AsString;
end;

// CAR 3-7-2003
function TSystemDM.GetDayWCode(Index: Integer): String;
begin
  Index := GetDayIndex(Index); // GLOB3-284
  Result := '';
  if ClientDataSetDay.FindKey([Index]) then
    Result :=  ClientDataSetDay.FieldByName('DAY_CODE').AsString;
end;

procedure TSystemDM.TablePIMSettingsBeforeDelete(DataSet: TDataSet);
begin
  SystemDM.DefaultBeforeDelete(DataSet);
end;

procedure TSystemDM.TablePIMSettingsBeforePost(DataSet: TDataSet);
begin
  SystemDM.DefaultBeforePost(DataSet);
end;

procedure TSystemDM.TablePIMSettingsEditError(DataSet: TDataSet;
  E: EDatabaseError; var Action: TDataAction);
begin
  SystemDM.DefaultEditError(DataSet, E, Action);
end;

procedure TSystemDM.TablePIMSettingsNewRecord(DataSet: TDataSet);
begin
  SystemDM.DefaultNewRecord(DataSet);
end;

// TDateFmt Start
procedure TDateFmt.AfterConstruction;
begin
  inherited;
  FDateTimeFmt := CreateDateTimeFmt;
  FQuote := '''';
end;

// Get DateTime-formats from BDE-settings.
function TDateFmt.CreateDateTimeFmt: String;
var
  Fmt: Fmtdate;
  DatePart, TimePart: String;
  Sep: String;
begin
  Check(dbiGetDateFormat(Fmt));
  Sep := Fmt.szDateSeparator;
  // Defaults
  DatePart := 'dd'   + Sep + 'mm' + Sep + 'yyyy';
  TimePart := 'hh:nn'; // minutes are 'nn' instead of 'mm'
  // Now build the formatstring for datetime
  case Fmt.iDateMode of
  0: // MDY
    DatePart := 'mm'   + Sep + 'dd' + Sep + 'yyyy';
  1: // DMY
    DatePart := 'dd'   + Sep + 'mm' + Sep + 'yyyy';
  2: // YMD
    DatePart := 'yyyy' + Sep + 'mm' + Sep + 'dd';
  end;
  Result := DatePart + ' ' + TimePart; // MDY
  // Set locale format for date-format
  // Save original (Windows) formats for later use
  FSepWindows := DateSeparator;
  FDateFmtWindows := ShortDateFormat;
  FTimeFmtWindows := ShortTimeFormat;
  // Save also BDE's formats for later use
  FSepBDE := Sep;
  FDateFmtBDE := DatePart;
  FTimeFmtBDE := TimePart;
end;

// Set date/time-formats to Windows-formats
procedure TDateFmt.SwitchDateTimeFmtWindows;
begin
  DateSeparator := FSepWindows[1];
  ShortDateFormat := FDateFmtWindows;
  ShortTimeFormat := FTimeFmtWindows;
end;

// Set date/time-formats to BDE-formats, needed when comparing
// with dates that are first converted to a string.
procedure TDateFmt.SwitchDateTimeFmtBDE;
begin
  DateSeparator := FSepBDE[1];
  ShortDateFormat := FDateFmtBDE;
  ShortTimeFormat := FTimeFmtBDE;
end;
// TDateFmt End

// MR:05-10-2004
function TSystemDM.GetMonthGroupEfficiency: Boolean;
begin
  Result := False;
  TableExportPayroll.Close;
  TableExportPayroll.TableName := 'EXPORTPAYROLL';
  TableExportPayroll.Open;
  if not TableExportPayroll.IsEmpty then
  begin
    TableExportPayroll.First;
    // MR:12-03-2007 Order 550443 Only look for 'SR' and 'Y'
    if
      (TableExportPayroll.FieldByName('EXPORT_TYPE').AsString = 'SR') and
      (TableExportPayroll.
        FieldByName('MONTH_GROUP_EFFICIENCY_YN').AsString = 'Y') then
      Result := True;
  end;
end;
(* ORACLE TEST *)
function TSystemDM.GetPimsDatabase: TPimsDatabase;
var
  PimsDatabaseDriverName: String;
begin
  PimsDatabaseDriverName := SessionPims.GetAliasDriverName(
    SessionPims.Databases[0].AliasName);
  if PimsDatabaseDriverName = 'STANDARD' then
    Result := DB_PARADOX
  else
    if PimsDatabaseDriverName = 'INTRBASE' then
      Result := DB_INTERBASE
    else
      if PimsDatabaseDriverName = 'ORACLE' then
        Result := DB_ORACLE
      else
        Result := DB_UNKNOWN;
end;

// Get ORACLE Database Server Name as defined in BDE-driver.
function TSystemDM.GetDatabaseServerName: String;
var
  MyList: TStringList;
  I: Integer;
begin
  Result := '';
  MyList := TStringList.Create;
  Session.GetAliasParams(SessionPims.Databases[0].AliasName, MyList);
  for I := 0 to MyList.Count - 1 do
    if Pos('SERVER NAME', UpperCase(MyList.Strings[I])) > 0 then
    begin
      try
        Result :=
          Copy(MyList.Strings[I], Pos('=', MyList.Strings[I]) + 1, 128);
      except
        Result := 'ABS1';
      end;
      Break;
    end;
  MyList.Free;
end;

// RV023.
function TSystemDM.GetExportPayrollType: String;
begin
  Result := FExportPayrollType;
end;

// RV023.
procedure TSystemDM.SetExportPayrollType(const Value: String);
begin
  FExportPayrollType := Value;
end;

// RV050.8. Team filter: Filter on Plant.
function TSystemDM.PlantTeamFilter(DataSet: TDataSet): Boolean;
begin
  Result := True;
  if PlantTeamFilterOn then
  begin
    try
      if DataSet <> nil then
        if DataSet.FindField('PLANT_CODE') <> nil then
          if qryPlantDeptTeam.Active and DataSet.Active then
            Result := qryPlantDeptTeam.Locate('PLANT_CODE',
              DataSet.FieldByName('PLANT_CODE').AsString, []);
    except
      Result := True;
    end;
  end;
end;

// RV086.2. Team filter: Filter on Plant + Team
function TSystemDM.PlantTeamTeamFilter(DataSet: TDataSet;
  APlantField, ATeamField: String): Boolean; // GLOB3-342
begin
  Result := True;
  if PlantTeamFilterOn then
  begin
    try
      if DataSet <> nil then
        if qryPlantDeptTeam.Active and DataSet.Active then
          Result := qryPlantDeptTeam.Locate('PLANT_CODE;TEAM_CODE',
            VarArrayOf([DataSet.FieldByName(APlantField).AsString,
              DataSet.FieldByName(ATeamField).AsString]),
              []);
    except
      Result := True;
    end;
  end;
end;

function TSystemDM.PlantTeamTeamFilter(DataSet: TDataSet): Boolean; // GLOB3-342
begin
  Result := PlantTeamTeamFilter(DataSet, 'PLANT_CODE', 'TEAM_CODE');
end;

// RV069.1. 550494. Team filter: Filter on Plant + Department where fieldnames
//                  are arguments in case they are named different.
function TSystemDM.PlantDeptTeamFilter(DataSet: TDataSet; APlantField,
  ADeptField: String): Boolean;
begin
  Result := True;
  if PlantTeamFilterOn then
  begin
    try
      if DataSet <> nil then
        if (DataSet.FindField(APlantField) <> nil) and
          (DataSet.FindField(ADeptField) <> nil) then
            if qryPlantDeptTeam.Active and DataSet.Active then
              Result := qryPlantDeptTeam.Locate('PLANT_CODE;DEPARTMENT_CODE',
                VarArrayOf([DataSet.FieldByName(APlantField).AsString,
                  DataSet.FieldByName(ADeptField).AsString]),
                  []);
    except
      Result := True;
    end;
  end;
end;

// RV069.1. 550494. Team filter: Filter on Plant + Department.
function TSystemDM.PlantDeptTeamFilter(DataSet: TDataSet): Boolean;
begin
  Result := PlantDeptTeamFilter(DataSet, 'PLANT_CODE', 'DEPARTMENT_CODE');
end;

// RV050.8.
procedure TSystemDM.PlantTeamFilterEnable(DataSet: TDataSet);
begin
  if UserTeamSelectionYN = CHECKEDVALUE then
  begin
    qryPlantDeptTeam.Close;
    qryPlantDeptTeam.ParamByName('USER_NAME').AsString := UserTeamLoginUser;
    qryPlantDeptTeam.Open;
    DataSet.Filtered := True;
    PlantTeamFilterOn := True;
  end
  else
  begin
    PlantTeamFilterOn := False;
  end;
end;
//RV066.1.
function TSystemDM.GetCode: String;
begin
  if not TablePIMSettings.Active then
    TablePIMSettings.Open;
  Result := TablePIMSettings.FieldByName('CODE').AsString;
end;
//RV067.4.
function TSystemDM.GetDBValue(ASql: String; DefaultValue: Variant): Variant;
begin
  Result := NULL;
  qryTemp.Close;
  qryTemp.SQL.Text := ASql;
  qryTemp.Open;
  if not qryTemp.Eof then
    Result := qryTemp.Fields[0].Value;
  qryTemp.Close;
  if VarIsNull(Result) then
    Result := DefaultValue;
end;

// RV083.3.
procedure TSystemDM.UpdateAbsenceReasonPerCountry(ACountryId: Integer;
  AQuery: TQuery; ASortField: String='DESCRIPTION');
var
  DefinedAbsenceReasons: Integer;
begin
  if ASortField = 'DESCRIPTION' then
    ASortField := 'AR.DESCRIPTION'
  else
    if ASortField = 'CODE' then
      ASortField := 'AR.ABSENCEREASON_CODE'
    else
      ASortField := 'AR.DESCRIPTION';

  DefinedAbsenceReasons :=
    GetDBValue('SELECT COUNT(*) FROM ABSENCEREASONPERCOUNTRY WHERE COUNTRY_ID = ' +
      IntToStr(ACountryId), 0);
  AQuery.Close;
  if DefinedAbsenceReasons = 0 then
    AQuery.Sql.Text :=
      'SELECT ' +
      '  AR.ABSENCEREASON_ID, AR.ABSENCEREASON_CODE, AR.DESCRIPTION, ' +
      '  AR.ABSENCETYPE_CODE ' +
      'FROM ' +
      '  ABSENCEREASON AR ' +
      'ORDER BY ' +
      ASortField
  else
    AQuery.Sql.Text :=
      ' SELECT ' +
      '   AR.ABSENCEREASON_ID, AR.ABSENCEREASON_CODE, AR.DESCRIPTION, ' +
      '   AR.ABSENCETYPE_CODE ' +
      ' FROM ' +
      '   ABSENCEREASON AR INNER JOIN ABSENCEREASONPERCOUNTRY ARC ON ' +
      '     AR.ABSENCEREASON_ID = ARC.ABSENCEREASON_ID AND ' +
      '     ARC.COUNTRY_ID = ' + IntToStr(ACountryId) +
      ' ORDER BY ' +
      ASortField;
  AQuery.Open;
end;

procedure TSystemDM.UpdateHourTypePerCountry(ACountryId: Integer; AQuery: TQuery);
var
  DefinedHourTypes: Integer;
begin
  // RV071.13. 550497 Bugfixing.
  DefinedHourTypes :=
    GetDBValue(
      'SELECT COUNT(*) ' +
      '  FROM HOURTYPEPERCOUNTRY ' +
      '  WHERE COUNTRY_ID = ' + IntToStr(ACountryId), 0);
  AQuery.Close;
  if DefinedHourTypes = 0 then
    AQuery.Sql.Text :=
      'SELECT H.*, H.DESCRIPTION HDESCRIPTION ' +
      '  FROM HOURTYPE H ' +
      '  ORDER BY H.HOURTYPE_NUMBER'
  else
    AQuery.Sql.Text :=
      'SELECT H.*, NVL(HC.DESCRIPTION, H.DESCRIPTION) HDESCRIPTION ' +
      '  FROM HOURTYPE H, HOURTYPEPERCOUNTRY HC ' +
      '  WHERE H.HOURTYPE_NUMBER = HC.HOURTYPE_NUMBER AND HC.COUNTRY_ID = ' +
      IntToStr(ACountryId) +
      '  ORDER BY H.HOURTYPE_NUMBER';
  AQuery.Open;
end;

function TSystemDM.GetCounterActivated(ACountryId: Integer; AAbsenceType: Char): Boolean;
var
  StrValue: String;
begin
  StrValue := SystemDM.GetDBValue(
    Format(
    'select ac.counter_active_yn from absencetypepercountry ac ' +
    '  where country_id = %d and absencetype_code = ''%s''',
    [ACountryId, AAbsenceType]
    ), '*'
  );

  Result := StrValue = CHECKEDVALUE;
end;

procedure TSystemDM.ExecSql(ASql: String);
begin
  SystemDMT.ExecSql(qryTemp, ASql);
end;

// RV073.5. Access rights.
// RV091.
function TSystemDM.UserTeamLoginUser: String;
begin
  if UserTeamSelectionYN = CHECKEDVALUE then
    Result := CurrentLoginUser
  else
    Result := '*';
end;

function TSystemDM.IsRFL: Boolean;
begin
  Result := GetCode = 'RFL';
end;

// RV077.5.
procedure TSystemDM.SetIsAttent(const Value: Boolean);
begin
  FIsAttent := Value;
  try
    if not TableExportPayroll.Active then
      TableExportPayroll.Open;
    if TableExportPayroll.Locate('EXPORT_TYPE', 'ATTENT', []) then
      FIsAttent := True;
  except
    FIsAttent := False;
  end;
end;

// RV085.19.
procedure TSystemDM.SetIsCVL(const Value: Boolean);
begin
  FIsCVL := GetCode = 'CVL';
end;

// RV087.3.
// Main-function: Do not call this direct.
function TSystemDM.GetMenuTreeOfParentMenuMain(AWorkQuery: TQuery;
  Parent_Menu: Integer; Visible_YN, Expand_YN: String): TQuery;
var
  SelectStr: String;
begin
  with AWorkQuery do
  begin
    SelectStr :=
      'SELECT ' +
      '  MENU_NUMBER, SEQ_NUMBER, VISIBLE_YN, EDIT_YN, EXPAND_YN, ' +
      '  DESCRIPTION ' +
      'FROM ' +
      '  PIMSMENUGROUP ' +
      'WHERE ' +
      '  GROUP_NAME = ''' +
      DoubleQuote(LoginGroup) + ''' AND ' +
      ' PARENT_MENU_NUMBER = ' + IntToStr(Parent_Menu) + ' AND EXPAND_YN = ''' +
      Expand_YN + '''';
    if Visible_YN = 'Y' then
      SelectStr :=  SelectStr  + ' AND VISIBLE_YN = ''' + Visible_YN + '''';
    SelectStr := SelectStr + ' ' +
      'ORDER BY ' +
      '  SEQ_NUMBER ';
    Close;
    SQL.Clear;
    SQL.Add(SelectStr);
    Open;
  end;
  Result := AWorkQuery;
end;

// RV087.3.
// Use this for the first (or only) loop.
function TSystemDM.GetMenuTreeOfParentMenu(Parent_Menu: Integer;
  Visible_YN, Expand_YN: String): TQuery;
begin
  Result := GetMenuTreeOfParentMenuMain(qryPimsMenuGroup, Parent_Menu,
      Visible_YN, Expand_YN);
end;

// RV087.3.
// Use this for to call in a second loop. It uses a different TQuery-Component.
function TSystemDM.GetMenuTreeOfParentMenu2(Parent_Menu: Integer;
  Visible_YN, Expand_YN: String): TQuery;
begin
  Result := GetMenuTreeOfParentMenuMain(qryPimsMenuGroup2, Parent_Menu,
      Visible_YN, Expand_YN);
end;

// RV087.3.
function TSystemDM.GetMenuNumberOfMenuOption(
  const PimsMenuOption: Integer): Integer;
var
  Parent_Level: Integer;
begin
  Result := -1;
  Parent_Level := GetMenuNumberOfApplication;
  GetMenuTreeOfParentMenu(Parent_Level, '', 'Y');

  qryPimsMenuGroup.First;
  qryPimsMenuGroup.MoveBy(PimsMenuOption);
  if not qryPimsMenuGroup.Eof then
    Result := qryPimsMenuGroup.FieldByName('MENU_NUMBER').AsInteger;
end;

// RV087.3.
function TSystemDM.GetMenuNumberOfApplication: Integer;
begin
  Result := APL_MENU_LOGIN;
end;

// RV087.3.
function TSystemDM.CheckVisibleMenuOption(
  const PimsMenuOption: Integer): Boolean;
begin
  GetMenuTreeOfParentMenu(GetMenuNumberOfMenuOption(PimsMenuOption), 'Y', 'Y');
  Result := not qryPimsMenuGroup.IsEmpty;
end;

// RV089.1.
// RV093.2. Not needed anymore.
{
procedure TSystemDM.SetEmbedDialogs(Value: Boolean);
begin
  try
    with qryTemp do
    begin
      Close;
      SQL.Add('SELECT T.EMBED_DIALOGS_YN FROM PIMSSETTING T');
      Open;
      Value := False;
      if FieldByName('EMBED_DIALOGS_YN').AsString = 'Y' then
        Value := True;
      Close;
    end;
  except
    Value := False;
  end;
  FEmbedDialogs := Value;
end;
}

// RV089.1. Show a message with 4 buttons: Yes, No, All, NoToAll.
//          This should be used instead of 'DisplayMessage' when
//          these 4 buttons should be shown.
// PIM-53 Related to this order: It gave an access violation when called for
// a second time.
function DisplayMessage4Buttons(const Msg: String): Integer;
begin
  MessageDialogF := TMessageDialogF.Create(nil); // PIM-53 Use argument nil not Application
  try
    MessageDialogF.Msg := Msg;
    Result := MessageDialogF.ShowModal;
  finally
    MessageDialogF := nil; // PIM-53 Use this instead of Free
  end;
end;

// RV091.3. Returns a fixed futuredate.
function TSystemDM.FutureDate: TDateTime;
begin
  Result := EncodeDate(2099, 12, 31);
end;

procedure TSystemDM.SetIsLTB(const Value: Boolean);
begin
  FIsLTB := GetCode = 'LTB';
end;

// RV093.3. Enable or disable logging based on pims-settings.
procedure TSystemDM.ABSLogEmpHrsEnable(AEnable: Boolean);
var
  MyAction: String;
begin
  if AEnable then
    MyAction := 'ENABLE'
  else
    MyAction := 'DISABLE';
  try
    ExecSQL('ALTER TRIGGER AHE_ABSLOGAHE ' + MyAction);
    ExecSQL('ALTER TRIGGER PHEPT_ABSLOGPHEPT ' + MyAction);
    ExecSQL('ALTER TRIGGER PHE_ABSLOGPHE ' + MyAction);
    ExecSQL('ALTER TRIGGER SHE_ABSLOGSHE ' + MyAction);
  except
    // Ignore error
  end;
end; // ABSLogEmpHrsEnable

// RV093.3. Enable or disable logging based on pims-settings.
procedure TSystemDM.ABSLogTRSEnable(AEnable: Boolean);
var
  MyAction: String;
begin
  if AEnable then
    MyAction := 'ENABLE'
  else
    MyAction := 'DISABLE';
  try
    ExecSQL('ALTER TRIGGER TRS_ABSLOGTRS ' + MyAction);
  except
    // Ignore error
  end;
end; // ABSLogTRSEnable

// RV093.3. Enable or disable logging based on pims-settings.
procedure TSystemDM.ABSLogEnable;
begin
  ABSLogEmpHrsEnable(ABSLOG_EmpHrsYN = CHECKEDVALUE);
  ABSLogTRSEnable(ABSLOG_TRSYN = CHECKEDVALUE);
end; // ABSLogEnable

// 20013723
procedure TSystemDM.SetIsCLF(const Value: Boolean);
begin
  FIsCLF := GetCode = 'CLF';
end;

// 20014036
procedure TSystemDM.SetIsBCH(const Value: Boolean);
begin
  FIsBCH := GetCode = 'BCH';
end;

// 20012944
function TSystemDM.ShowWeeknumbers: Boolean;
begin
  Result := (ShowWeeknumbersYN = 'Y');
end;

// 20012944
// IMPORTANT: This overrides the existing class named TDateTimePicker
// To make this work, the unit SystemDMT must be the last unit in the
// first uses-line in the unit where the TDateTimePicker is used.
{ TDateTimePicker }
constructor TDateTimePicker.Create(AOwner: TComponent);
begin
  inherited;
  Self.ShowWeekNumbers := SystemDM.ShowWeeknumbers;
  if SystemDM.ShowWeeknumbers then
  begin
    case SystemDM.WeekStartsOn of
    1: Self.FirstDayOfWeek := Integer(dowSunday);
    2: Self.FirstDayOfWeek := Integer(dowMonday);
    3: Self.FirstDayOfWeek := Integer(dowTuesday);
    4: Self.FirstDayOfWeek := Integer(dowWednesday);
    5: Self.FirstDayOfWeek := Integer(dowThursday);
    6: Self.FirstDayOfWeek := Integer(dowFriday);
    7: Self.FirstDayOfWeek := Integer(dowSaturday);
    end;
  end;
end;

// 20012944
// IMPORTANT: This overrides the existing class named TDBPicker
// To make this work, the unit SystemDMT must be the last unit in the
// first uses-line in the unit where the TDateTimePicker is used.
{ TDBPicker }
constructor TDBPicker.Create(AOwner: TComponent);
begin
  inherited;
  Self.ShowWeekNumbers := SystemDM.ShowWeeknumbers;
  if SystemDM.ShowWeeknumbers then
  begin
    case SystemDM.WeekStartsOn of
    1: Self.FirstDayOfWeek := Integer(dowSunday);
    2: Self.FirstDayOfWeek := Integer(dowMonday);
    3: Self.FirstDayOfWeek := Integer(dowTuesday);
    4: Self.FirstDayOfWeek := Integer(dowWednesday);
    5: Self.FirstDayOfWeek := Integer(dowThursday);
    6: Self.FirstDayOfWeek := Integer(dowFriday);
    7: Self.FirstDayOfWeek := Integer(dowSaturday);
    end;
  end;
end;

// 20012944
// IMPORTANT: This overrides the existing class named TMonthCalendar
// To make this work, the unit SystemDMT must be the last unit in the
// first uses-line in the unit where the TMonthCalendar is used.
constructor TMonthCalendar.Create(AOwner: TComponent);
begin
  inherited;
  Self.WeekNumbers := SystemDM.ShowWeeknumbers;
  if SystemDM.ShowWeeknumbers then
  begin
    case SystemDM.WeekStartsOn of
    1: Self.FirstDayOfWeek := dowSunday;
    2: Self.FirstDayOfWeek := dowMonday;
    3: Self.FirstDayOfWeek := dowTuesday;
    4: Self.FirstDayOfWeek := dowWednesday;
    5: Self.FirstDayOfWeek := dowThursday;
    6: Self.FirstDayOfWeek := dowFriday;
    7: Self.FirstDayOfWeek := dowSaturday;
    end;
  end;
end;

// 20014715
procedure TSystemDM.SetIsWMU(const Value: Boolean);
begin
  FIsWMU := GetCode = 'WMU';
end;

// 20011800
function TSystemDM.GetIsAdmin: Boolean;
begin
  FIsAdmin := (LoginGroup = ADMINGROUP) or (SysAdmin);
  Result := FIsAdmin;
end;

// 20011800
function TSystemDM.FinalRunExportDate(APlantCode,
  AExportType: String): TDateTime;
begin
  Result := NullDate;
  with qryFinalRun do
  begin
    Close;
    ParamByName('PLANT_CODE').AsString := APlantCode;
    ParamByName('EXPORT_TYPE').AsString := AExportType;
    Open;
    if not Eof then
      Result := FieldByName('EXPORT_DATE').AsDateTime;
    Close;
  end;
end; // FinalRunExportDate

// 20011800
function TSystemDM.FinalRunExportDateByPlant(APlantCode: String): TDateTime;
var
  ExportType: String;
begin
  Result := NullDate;
  with qryPlantExportType do
  begin
    Close;
    ParamByName('PLANT_CODE').AsString := APlantCode;
    Open;
    if not Eof then
    begin
      ExportType := FieldByName('EXPORT_TYPE').AsString;
      if ExportType <> '' then
        Result := FinalRunExportDate(APlantCode, ExportType);
    end;
  end;
end;

// 20011800
function TSystemDM.FinalRunExportDateByEmployee(AEmployeeNumber: Integer): TDateTime;
var
  ExportType: String;
begin
  Result := NullDate;
  with qryEmpExportType do
  begin
    Close;
    ParamByName('EMPLOYEE_NUMBER').AsInteger := AEmployeeNumber;
    Open;
    if not Eof then
    begin
      ExportType := FieldByName('EXPORT_TYPE').AsString;
      if ExportType <> '' then
        Result := FinalRunExportDate(
          FieldByName('PLANT_CODE').AsString, ExportType);
    end;
  end;
end;

// 20011800
procedure TSystemDM.FinalRunStoreExportDate(APlantCode,
  AExportType: String; AExportDate: TDateTime);
begin
  if FinalRunExportDate(APlantCode, AExportType) = NullDate then
  begin
    with qryFinalRunInsert do
    begin
      ParamByName('PLANT_CODE').AsString := APlantCode;
      ParamByName('EXPORT_TYPE').AsString := AExportType;
      ParamByName('EXPORT_DATE').AsDateTime := AExportDate;
      ParamByName('MUTATOR').AsString := CurrentProgramUser;
      ExecSQL;
      if SystemDM.Pims.InTransaction then
        SystemDM.Pims.Commit;
    end;
  end
  else
  begin
    with qryFinalRunUpdate do
    begin
      ParamByName('PLANT_CODE').AsString := APlantCode;
      ParamByName('EXPORT_TYPE').AsString := AExportType;
      ParamByName('EXPORT_DATE').AsDateTime := AExportDate;
      ParamByName('MUTATOR').AsString := CurrentProgramUser;
      ExecSQL;
      if SystemDM.Pims.InTransaction then
        SystemDM.Pims.Commit;
    end;
  end;
end; // FinalRunStoreExportDate

// 20014442
procedure TSystemDM.SetIsVOS(const Value: Boolean);
begin
  FIsVOS := GetCode = 'VOS';
end;

// TD-25352 Related to this todo
procedure TSystemDM.SetCurrentComputerName(const Value: String);
begin
  FCurrentComputerName := Value;
end;

// TD-25352 Related to this todo
procedure TSystemDM.TableWorkStationBeforePost(DataSet: TDataSet);
begin
  // Sometimes this field is left empty, to be sure we assign it here.
  DataSet.FieldByName('TIMERECORDING_YN').AsString := TimeRecordingStation;
end;

// 20014826 Log to DB
function TSystemDM.DBLog(AMsg: String; APriority: Integer): Boolean;
begin
{
  APriority:
  1 = Message
  2 = Warning
  3 = Error
  4 = Stack trace
  5 = Debug info
}
  Result := LogToDB;
  if LogToDB then
  begin
    with qryABSLogInsert do
    begin
      try
        if AMsg <> '' then
        begin
          if Length(AMsg) > 255 then
            AMsg := Copy(AMsg, 1, 255);
          ParamByName('COMPUTER_NAME').AsString := CurrentComputerName;
          ParamByName('LOGSOURCE').AsString := AppName;
          ParamByName('PRIORITY').AsInteger := APriority;
          ParamByName('SYSTEMUSER').AsString := CurrentProgramUser;
          ParamByName('LOGMESSAGE').AsString := AMsg;
          ExecSQL;
        end;
      except
        Result := False;
      end;
    end; // with
  end; // if
end; // DBLog

// 20014826
type
  TEXEVersionData = record
    CompanyName,
    FileDescription,
    FileVersion,
    InternalName,
    LegalCopyright,
    LegalTrademarks,
    OriginalFileName,
    ProductName,
    ProductVersion,
    Comments,
    PrivateBuild,
    SpecialBuild: string;
  end;

// 20014826
function GetEXEVersionData(const FileName: string): TEXEVersionData;
type
  PLandCodepage = ^TLandCodepage;
  TLandCodepage = record
    wLanguage,
    wCodePage: word;
  end;
var
  dummy,
  len: cardinal;
  buf, pntr: pointer;
  lang: string;
begin
  len := GetFileVersionInfoSize(PChar(FileName), dummy);
  if len = 0 then
    Exit;
  GetMem(buf, len);
  try
    if not GetFileVersionInfo(PChar(FileName), 0, len, buf) then
      Exit;

    if not VerQueryValue(buf, '\VarFileInfo\Translation\', pntr, len) then
      Exit;

    lang := Format('%.4x%.4x', [PLandCodepage(pntr)^.wLanguage, PLandCodepage(pntr)^.wCodePage]);

    if VerQueryValue(buf, PChar('\StringFileInfo\' + lang + '\CompanyName'), pntr, len){ and (@len <> nil)} then
      result.CompanyName := PChar(pntr);
    if VerQueryValue(buf, PChar('\StringFileInfo\' + lang + '\FileDescription'), pntr, len){ and (@len <> nil)} then
      result.FileDescription := PChar(pntr);
    if VerQueryValue(buf, PChar('\StringFileInfo\' + lang + '\FileVersion'), pntr, len){ and (@len <> nil)} then
      result.FileVersion := PChar(pntr);
    if VerQueryValue(buf, PChar('\StringFileInfo\' + lang + '\InternalName'), pntr, len){ and (@len <> nil)} then
      result.InternalName := PChar(pntr);
    if VerQueryValue(buf, PChar('\StringFileInfo\' + lang + '\LegalCopyright'), pntr, len){ and (@len <> nil)} then
      result.LegalCopyright := PChar(pntr);
    if VerQueryValue(buf, PChar('\StringFileInfo\' + lang + '\LegalTrademarks'), pntr, len){ and (@len <> nil)} then
      result.LegalTrademarks := PChar(pntr);
    if VerQueryValue(buf, PChar('\StringFileInfo\' + lang + '\OriginalFileName'), pntr, len){ and (@len <> nil)} then
      result.OriginalFileName := PChar(pntr);
    if VerQueryValue(buf, PChar('\StringFileInfo\' + lang + '\ProductName'), pntr, len){ and (@len <> nil)} then
      result.ProductName := PChar(pntr);
    if VerQueryValue(buf, PChar('\StringFileInfo\' + lang + '\ProductVersion'), pntr, len){ and (@len <> nil)} then
      result.ProductVersion := PChar(pntr);
    if VerQueryValue(buf, PChar('\StringFileInfo\' + lang + '\Comments'), pntr, len){ and (@len <> nil)} then
      result.Comments := PChar(pntr);
    if VerQueryValue(buf, PChar('\StringFileInfo\' + lang + '\PrivateBuild'), pntr, len){ and (@len <> nil)} then
      result.PrivateBuild := PChar(pntr);
    if VerQueryValue(buf, PChar('\StringFileInfo\' + lang + '\SpecialBuild'), pntr, len){ and (@len <> nil)} then
      result.SpecialBuild := PChar(pntr);
  finally
    FreeMem(buf);
  end;
end;

procedure TSystemDM.AppNameSet(const Value: String);
var
  EXEVersionData: TEXEVersionData;
begin
  try
    EXEVersionData := GetEXEVersionData(Application.ExeName);
    if EXEVersionData.InternalName <> '' then
      FAppName := EXEVersionData.InternalName
    else
      FAppName := Value;
  except
    FAppName := Value;
  end;
  if (FAppName = APPNAME_PIMS) or (Pos('PimsORCL', FAppName) > 0) then
  begin
    UGLogFilenameSet('PIMSLOG');
    UGErrorLogFilenameSet('PIMSERRLOG');
  end
  else
  if (FAppName = APPNAME_AT) or (Pos('AdmTool', FAppName) > 0) then
  begin
    UGLogFilenameSet('ATLOG');
    UGErrorLogFilenameSet('ATERRLOG');
  end
  else
  if (FAppName = APPNAME_PS) or (Pos('PersonalScreen', FAppName) > 0) then
  begin
    UGLogFilenameSet('PSLOG');
    UGErrorLogFilenameSet('PSERRLOG');
  end
  else
  if (FAppName = APPNAME_PRS) or (Pos('ProductionScreen', FAppName) > 0) then
  begin
    UGLogFilenameSet('PRSLOG');
    UGErrorLogFilenameSet('PRSERRLOG');
  end
  else
  if (FAppName = APPNAME_MANREG) or (Pos('ManualRegistrations', FAppName) > 0) then
  begin
    UGLogFilenameSet('MANREGLOG');
    UGErrorLogFilenameSet('MANREGERRLOG');
  end
  else
  if (FAppName = APPNAME_TRS) or (Pos('TimeRecording', FAppName) > 0) then
  begin
    UGLogFilenameSet('TRSLOG');
    UGErrorLogFilenameSet('TRSERRLOG');
  end
  else
  if (FAppName = APPNAME_ADC1) or (Pos('AutoDataCol1', FAppName) > 0) then
  begin
    UGLogFilenameSet('ADC1LOG');
    UGErrorLogFilenameSet('ADC1ERRLOG');
  end
  else
  if (FAppName = APPNAME_ADC2) or (Pos('AutoDataCol2', FAppName) > 0) then
  begin
    UGLogFilenameSet('ADC2LOG');
    UGErrorLogFilenameSet('ADC2ERRLOG');
  end
  else
  if (FAppName = APPNAME_ADC3) or (Pos('AutoDataCol3', FAppName) > 0) then
  begin
    UGLogFilenameSet('ADC3LOG');
    UGErrorLogFilenameSet('ADC3ERRLOG');
  end
  else
  begin
    UGLogFilenameSet('PIMSLOG');
    UGErrorLogFilenameSet('PIMSERRLOG');
  end;
end; // AppNameSet

// 20016449
procedure TSystemDM.DetermineWorkstationTimezonehrsdiff;
var
  WorkstationTimezone: String;
  PlantCode: String;
  TestTimezonehrsdiff: Integer;
begin
  WorkstationTimezone := '';
  WSTimezonehrsdiff := 0;
  TestTimezonehrsdiff := 0;
  TableWorkStation.Refresh;
  if TableWorkStation.Findkey([CurrentComputerName]) then
  begin
    try
      // Determine timezone via linked plant
      PlantCode :=
        TableWorkstation.FieldByName('PLANT_CODE').AsString;
      if PlantCode <> '' then
      begin
        qryPlantFind.Close;
        qryPlantFind.ParamByName('PLANT_CODE').AsString := PlantCode;
        qryPlantFind.Open;
        if not qryPlantFind.Eof then
        begin
          WorkstationTimezone := qryPlantFind.FieldByName('TIMEZONE').AsString;
          if WorkstationTimezone = 'TEST' then
            TestTimezonehrsdiff := qryPlantFind.FieldByName('TIMEZONEHRSDIFF').AsInteger;
        end;
        qryPlantFind.Close;
      end;
    except
      WorkstationTimezone := '';
    end;
    if WorkstationTimezone = 'TEST' then
      WSTimezonehrsdiff := TestTimezonehrsdiff
    else
      WSTimezonehrsdiff :=
        DetermineTimezonehrsdiff(DatabaseTimezone, WorkstationTimezone);
  end;
end; // DetermineWorkstationTimezonehrsdiff

// 20016449
procedure TSystemDM.PlantTimezonehrsdiffWriteToDB;
var
  PlantTimezonehrsdiff: Integer;
begin
  try
    qryPlant.Close;
    qryPlant.Open;
    while not qryPlant.Eof do
    begin
      PlantTimezonehrsdiff := 0;
      if qryPlant.FieldByName('TIMEZONE').AsString <> '' then
        PlantTimezonehrsDiff :=
          DetermineTimezonehrsdiff(DatabaseTimezone,
            qryPlant.FieldByName('TIMEZONE').AsString);
      // For testing purposes
      if Pos('TEST', qryPlant.FieldByName('TIMEZONE').AsString) = 0 then
      begin
        qryPlantUpdate.Close;
        qryPlantUpdate.ParamByName('PLANT_CODE').AsString :=
          qryPlant.FieldByName('PLANT_CODE').AsString;
        qryPlantUpdate.ParamByName('TIMEZONEHRSDIFF').AsInteger :=
          PlantTimezonehrsdiff;
        qryPlantUpdate.ExecSQL;
      end;
      qryPlant.Next;
    end;
  finally
    if SystemDM.Pims.InTransaction then
      SystemDM.Pims.Commit;
  end;
end; // PlantTimezonehrsdiffWriteToDB

// 20016449
function DateTimeTimeZone(ADateTime: TDateTime): TDateTime;
begin
  Result := IncHour(ADateTime, SystemDM.WSTimezonehrsdiff);
end;

// 20016449 Return datetime without time zone
function DateTimeWithoutTimeZone(ADateTime: TDateTime): TDateTime;
begin
//  Result := IncHour(ADateTime, -1 * SystemDM.WSTimezonehrsdiff);
  Result := ADateTime - 1/24 * SystemDM.WSTimezonehrsdiff;
end;

// 20016449
function FormatDateTime(const Format: string; DateTime: TDateTime): string; //overload;
begin
  Result := SysUtils.FormatDateTime(Format, DateTime);
end;

// 20016449
function DateTimeToStr(const DateTime: TDateTime): string;
begin
  Result := SysUtils.DateTimeToStr(DateTime);
end;

// 20016449
function WSDate: TDateTime;
begin
  Result := SysUtils.Date;
end;

// 20016449
function WSNow: TDateTime;
begin
  Result := SysUtils.Now;
end;

// 20016449
procedure TSystemDM.cdsTimeZoneFillList;
var
  I: Integer;
begin
  cdsTimeZone.EmptyDataSet;
  for I := 0 to TimeZoneList.Count - 1 do
  begin
    cdsTimeZone.Insert;
    cdsTimeZone.FieldByName('TIMEZONE').AsString := TimeZoneList.Strings[I];
    cdsTimeZone.Post;
  end;
end;

procedure TSystemDM.SetIsNTS(const Value: Boolean);
begin
  FIsNTS := GetCode = 'NTS';
end;

{ TOneSolar }

// GLOB3-94
// Allow DEBUG-version to start multiple times
constructor TOneSolar.Create(AOwner: TComponent);
begin
{$IFNDEF DEBUG}
  inherited;
{$ENDIF}
//
end;

end.
