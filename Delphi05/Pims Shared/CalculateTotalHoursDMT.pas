(*
  Use this 'CalculateTotalHoursDMT'-file if you want to use procedures like:
  - procedure GetShiftDay
  - procedure ComputeBreaks
  - procedure EmployeeCutOfTime
  - procedure GetProdMin

  Include 'CalculateTotalHoursDMT' in uses-part, in DMT-file of
  the part where you want to use it.

  If you want to refresh the data that's stored in the ClientDataSets,
  use the procedure 'AProdMinClass.Refresh'.

  Note: 'CalculateTotalHoursDMT' is (after SystemDMT) always created.
        This means also the Class-variables can always be used.

  Changes:
    MRA:15-JAN-2009 RV020.
      Added GetProdMinOvernight-procedure. This was needed for CheckListScan-
      report, because this gave problems with overnight-scans!

  MRA:09-04-2009 RV025.
    Fix problem when end-time is set as 00:00.

  MRA:30-OCT-2009 RV040.
    - Do not use 'Prepare', this costs lots of memory!
  MRA:26-FEB-2010. RV055.1.
  - Procedure get round-parameters is changed, it first tries to get values
    from IDCard.
  MRA:14-JUL-2010. RV063.1. RV067.MRA.7.
  - Put back old method of rounding. New method (RV055.1) is wrong!
  MRA:4-MAY-2011. RV092.8. Addition. SO-20011705
  - Determination of 'cutoff'-time is always based on
    first and last timeblock.
    Instead of that it should first look at planned timeblocks.
    If planning was found, then base first/last timeblock
    on the planned timeblocks.
    If no planning was found, then use the old method.
  - Examples:
    1) Planned timeblocks are 1,2 (of 4 timeblocks).
       This means the first timeblock should be 1 and last timeblock 2,
       instead of using 1 and 4.
    2) Planned timeblocks are 3,4 (of 4 timeblocks).
       This means the first timeblock should be 3 and last timeblock 4,
       instead of using 1 and 4.
  MRA:20-MAY-2011. RV092.8. SO-20011705.
  - Addition: Also look at standard-planning, employee-availability and
              standard-employee-availability.
  MRA:5-OCT-2012 20013489 Overnight Shift.
  - GetShiftDay: Return boolean to indicate it is about an overnight-shift,
    for later use. Store it in EmployeeData-record.
  - GetShiftDay: EmployeeData-record must be a VAR-variable!
  - GetShiftDay:
    - When DateIn is lower then StartDateTime (Shiftdate) then
      the Shiftdate (StartDateTime) must be set to date of DateIn.
    - When shift-definition is a non-overnight shift (starttime < endtime
      of shift-day-definition), then SHIFT_DATE must be set
      to DATETIME_IN of scan.
  MRA:22-OCT-2012 20013489 Overnight Shift.
  - GetShiftDay:
    - Always store production hours on day the shift started.
  MRA:26-OCT-2012 20013489
  - We do not need variable AOvernightShift.
  MRA:21-NOV-2012 20013288
  - Added DetermineShiftStartEndTB for determining of start/end based on
    timeblock.
  MRA:10-OCT-2013 20014327
  - Make use of Overnight-Shift-System optional.
  MRA:5-NOV-2013 TD-23416 -> This is NOT needed for Overnight-Shift-System
  - Missing hours:
    - Sometimes salary hours are not booked. This can happen with a scan like:
      - Sunday -> DateIn=2:30 DateOut=9:00
      - When this should be booked on previous day (Saturday), then it can happen
        it is not booked at all, because it is part of the previous week-period,
        resulting in missing hours.
      - To solve it, book it on the same day of the scan.
  MRA:12-JAN-2016 PIM-23
  - Addition of DESCRIPTION when reading timeblocks/breaks to clientdatasets.
  MRA:29-JUN-2016 PIM-181 Bugfix:
  - In procedure FindPlanningBlocks:
    - Query qryFindPlanningBlocks had to be changed to prevent an access
      violation when a timeblock is null, which resulted in an empty string.
  MRA:10-NOV-2017 PIM-326
  - Determination of first-scan/last-scan goes wrong and can result
    in wrong hours. When there are scans with seconds then it can give
    problems.
  MRA:21-NOV-2017 PIM-326 Rework
  - When Request-Early/Late is used, be sure both are filled to prevent
    it does not use Margin-Early/Late any more.
  MRA:15-JAN-2018 PIM-348
  - Calculation of Early/Late Margins gives a problem
  - Problem has to do with PIM-326.
  - It should always determine the first/last scan to determine if the
    current scan should be rounded because of early/late in/out-margins!
  MRA:18-JUN-2018 GLOB3-60
  - Extension 4 to 10 blocks for planning.
  MRA:13-JUL-2018 PIM-295
  - Inscan margins and planned time-blocks
  - When determining blocks, filter on planned timeblocks only, as was
    done for LTB (see IsLTB), this must always be done. 
*)

unit CalculateTotalHoursDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ReportBaseDMT, Db, DBTables, UPimsConst, Provider, DBClient, UScannedIDCard,
  GlobalDMT;

// MR:16-07-2003
// Record, Class and Methods for
// determining the Shift-Start+End Dates.
// TShiftDay - declarations - Start
type
  TShiftDayRecord = record
    ADate: TDateTime;
    AStart, AEnd: TDateTime;
    AValid: Boolean;
    APlantCode: String;
    AShiftNumber: Integer;
  end;

type
  TShiftDay = class
  private
    FEmployeeNumber: Integer;
    FScanStart: TDateTime;
    FScanEnd: TDateTime;
    FShiftNumber: Integer;
    FPlantCode: String;
    FDepartmentCode: String;
    FFromTimeRecordingApp: Boolean;
    procedure SetEmployeeNumber(const Value: Integer);
    procedure SetScanStart(const Value: TDateTime);
    procedure SetScanEnd(const Value: TDateTime);
    procedure SetPlantCode(const Value: String);
    procedure SetShiftNumber(const Value: Integer);
    procedure SetDepartmentCode(const Value: String);
  public
    Prev, Curr, Next: TShiftDayRecord;
    procedure DetermineShift(ADate: TDateTime;
      VAR AShiftDayRecord: TShiftDayRecord);
    procedure DetermineShiftStartEnd(
      VAR AShiftDayRecord: TShiftDayRecord);
    procedure DetermineShiftStartEndTB(
      VAR AShiftDayRecord: TShiftDayRecord;
      ATimeBlockNumber: Integer);
    procedure DetermineNewStartEnd(VAR NewScanStart, NewScanEnd: TDateTime);
    procedure GetShiftDay(var EmployeeData: TScannedIDCard;
      VAR StartDateTime, EndDateTime: TDateTime);
    property AEmployeeNumber: Integer read FEmployeeNumber
      write SetEmployeeNumber;
    property AScanStart: TDateTime read FScanStart write SetScanStart;
    property AScanEnd: TDateTime read FScanEnd write SetScanEnd;
    property APlantCode: String read FPlantCode write SetPlantCode;
    property AShiftNumber: Integer read FShiftNumber write SetShiftNumber;
    property ADepartmentCode: String read FDepartmentCode
      write SetDepartmentCode;
    property AFromTimeRecordingApp: Boolean read FFromTimeRecordingApp
      write FFromTimeRecordingApp;
  end;
// TShiftDay - declarations - End

//CAR 21-7-2003
// class- TTBLengthClass - contains functions for :
// determining:
// length of a TimeBlock
// timeblock valid

// TProdMin - for calculation of prod minute - used in few reports
// function of UGlobalFunction unit redefined
//- later will be removed from this unit

// Changes in pims:
// report: on Create/ Destroy message of datamodule new variable of class type
// defined below is created / free
// forms: OnShow/OnClose message - new variable of Class type defined
// is created/ destroy

type
  TTBMinPerWeek =  Array[1..MAX_TBS,1..7] of Integer;
  TTBNumber = Array[1..MAX_TBS] of Integer;
//
  TTBLengthClass = class
  private
    FTBStart,
    FTBEnd,
    FBreak: TTBMinPerWeek;
    FTBValid: TTBNumber;

    FEmployeeNumber: Integer;
    FShiftNumber: Integer;
    FPlantCode: String;
    FDepartmentCode: String;
    FRemoveNotPaidBreaks: Boolean;

    procedure SetEmployeeNumber(const Value: Integer);
    procedure SetPlantCode(const Value: String);
    procedure SetShiftNumber(const Value: Integer);
    procedure SetDepartmentCode(const Value: String);
    procedure SetRemoveNotPaidBreaks(const Value: Boolean);

    procedure FillTBValues(TmpClientDataSet: TClientDataSet;
      FillMinutes: Boolean; var Index: integer);
    procedure GetTimeBreaks(BreaksStart, BreaksEnd: TDateTime;
      Index, IndexDay: Integer; var BreakTime: Integer);

    procedure FillTBArray_ClientDataSet(ClientDataSetTmp_TBEmpl,
      ClientDataSetTmp_TBDept, ClientDataSetTmp_TBShift: TClientDataSet;
      FillMinutes, SetEmptyArray: Boolean);
    procedure FillBreaks_ClientDataSet(
      ClientDataSetTmp_BEmpl, ClientDataSetTmp_BDept,
      ClientDataSetTmp_BShift: TClientDataSet);
    procedure OpenDataTBLength;
    procedure CloseDataTBLength;
  public
    constructor Create;
    procedure Free;

    function GetMinutes(DateTimeMinutes: TDateTime): Integer;
    property AEmployeeNumber: Integer read FEmployeeNumber
      write SetEmployeeNumber;
    property APlantCode: String read FPlantCode write SetPlantCode;
    property AShiftNumber: Integer read FShiftNumber write SetShiftNumber;
    property ADepartmentCode: String read FDepartmentCode
      write SetDepartmentCode;
    property ARemoveNotPaidBreaks: Boolean read FRemoveNotPaidBreaks
      write SetRemoveNotPaidBreaks;

    procedure FillTBArray(Employee, Shift: Integer; Plant, Department: String);
    procedure FillBreaks(Employee, Shift: Integer;
      Plant, Department: String; RemoveNotPaidBreaks: Boolean);
    procedure FillTBLength(Employee, Shift: Integer;
      Plant, Department: String; RemoveNotPaidBreaks: Boolean);

    procedure ValidTB(Employee, Shift: Integer; Plant, Department: String);

    function GetLengthTB(TB_Number, Day_Number: Integer): Integer;
    function ExistTB(TB_Number: Integer): Boolean;
    function GetStartTime(TB_Number, Day_Number: Integer): TDateTime;
    function GetEndTime(TB_Number, Day_Number: Integer): TDateTime;
    function GetValidTB(TB_Number: Integer): Integer;
  end;

  TProdMinClass = class
  private
    FEmployeeData: TScannedIDCard;
    FStartTime, FEndTime: TDateTime;
    FOperationMode: Integer;
    FContractRoundMinutes: Boolean;

    procedure SetEmployeeData(const Value: TScannedIDCard);
    procedure SetOperationMode(const Value: Integer);
    procedure SetStartTime(const Value: TDateTime);
    procedure SetEndTime(const Value: TDateTime);

    procedure ComputeBreaks_ClientDataSet(
      ClientDataSetBEmpl_Tmp, ClientDataSetBDept_Tmp,
      ClientDataSetBShift_Tmp: TClientDataSet;
      var ProdMin, BreaksMin, PayedBreaks: Integer);
    procedure GetShiftWStartEnd_ClientDataSet
      (ClientDataSetProdTBEmpl_Tmp, ClientDataSetProdTBDept_Tmp,
       ClientDataSetProdTBShift_Tmp, ClientDataSetShift_Tmp: TClientDataSet;
       var WeekStartDateTime, WeekEndDateTime: TWeekDates;
       var Ready: Boolean);
    procedure EmployeeCutOfTime_Query(
      FirstScan, LastScan: Boolean; VAR EmployeeData: TScannedIDCard;
      DeleteAction: Boolean; DeleteEmployeeData: TScannedIDCard;
      var StartDTInterval, EndDTInterval: TDateTime);
    procedure GetShiftWStartEnd(EmployeeData: TScannedIDCard;
      var WeekStartDateTime, WeekEndDateTime: TWeekDates;
      var Ready: Boolean);
    procedure OpenDataProdMin;
    procedure CloseDataProdMin;
    procedure SetContractRoundMinutes(const Value: Boolean);
{    function IsFirstScan(AEmployeeNumber: Integer; ADateTimeIn: TDateTime;
      ADeleteDateTimeIn: TDateTime=0): Boolean; }
{    function IsLastScan(AEmployeeNumber: Integer; ADateTimeOut: TDateTime;
      ADeleteDateTimeIn: TDateTime=0): Boolean; }
  public
    Constructor Create;
    procedure Free;

    property AEmployeeData: TScannedIDCard read FEmployeeData
      write SetEmployeeData;
    property AOperationMode: Integer read FOperationMode
      write SetOperationMode;
    property AStartTime: TDateTime read FStartTime
      write SetStartTime;
    property AEndTime: TDateTime read FEndTime
      write SetEndTime;
    property AContractRoundMinutes: Boolean read FContractRoundMinutes
      write SetContractRoundMinutes;

    procedure GetShiftDay(
      var EmployeeData: TScannedIDCard;
      var StartDateTime, EndDateTime: TDateTime);

    procedure GetShiftStartEnd(EmployeeData: TScannedIDCard;
      ShiftDate: TDateTime;
      var StartDateTime, EndDateTime: TDateTime);

    procedure ComputeBreaks(
      EmployeeData: TScannedIDCard;
      STime, ETime: TDateTime; OperationMode: Integer;
      var ProdMin, BreaksMin, PayedBreaks: Integer);

    procedure EmployeeCutOfTime(
      FirstScan, LastScan: Boolean; VAR EmployeeData: TScannedIDCard;
      DeleteAction: Boolean; DeleteEmployeeData: TScannedIDCard;
      var StartDTInterval, EndDTInterval:TDateTime);

    procedure GetProdMin(VAR EmployeeData: TScannedIDCard;
      FirstScan, LastScan: Boolean;
      DeleteAction: Boolean; DeleteEmployeeData: TScannedIDCard;
      var BookingDay: TDateTime;
      var Minutes, BreaksMin, PayedBreaks: integer);

    procedure GetProdMinOvernight(VAR EmployeeData: TScannedIDCard;
      FirstScan, LastScan: Boolean;
      DeleteAction: Boolean; DeleteEmployeeData: TScannedIDCard;
      var BookingDay: TDateTime;
      var Minutes, BreaksMin, PayedBreaks: integer);

    procedure ReadContractRoundMinutesParams(
      EmployeeData: TScannedIDCard;
      var RoundTruncSalaryHours, RoundMinute: Integer); // RV063.1.

    procedure Refresh;

    function FindPlanningBlocks(EmployeeData: TScannedIDCard;
      var FirstPlanTimeblock, LastPlanTimeblock: Integer): Boolean;
  end;

  TCalculateTotalHoursDM = class(TReportBaseDM)
    QuerySelector: TQuery;
    QueryRequest: TQuery;
    ClientDataSetBShift: TClientDataSet;
    ClientDataSetBDept: TClientDataSet;
    ClientDataSetBEmpl: TClientDataSet;
    DataSetProviderBEmpl: TDataSetProvider;
    DataSetProviderBDept: TDataSetProvider;
    DataSetProviderBShift: TDataSetProvider;
    ClientDataSetShift: TClientDataSet;
    DataSetProviderShift: TDataSetProvider;
    ClientDataSetTBShift: TClientDataSet;
    ClientDataSetTBDept: TClientDataSet;
    ClientDataSetTBEmpl: TClientDataSet;
    DataSetProviderTBEmpl: TDataSetProvider;
    DataSetProviderTBDept: TDataSetProvider;
    DataSetProviderTBShift: TDataSetProvider;
    QueryShiftSchedule: TQuery;
    QueryContractgroup: TQuery;
    QueryEmployee: TQuery;
    qryPrevScan1: TQuery;
    qryPrevScan2: TQuery;
    qryNextScan1: TQuery;
    qryNextScan2: TQuery;
    qryFindPlanningBlocks: TQuery;
    qryIsFirstScan: TQuery;
    qryIsLastScan: TQuery;
    qryFirstScan: TQuery;
    qryLastScan: TQuery;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);

  public
    { Public declarations }
    // procedures for filling the client data set used in the new classes
    procedure OpenClientDataSet(ClientDataSetTmp: TClientDataSet;
      DataSetProviderTmp: TDataSetProvider;
      QueryTmp: TQuery; SelectStr: String);
    procedure OpenTBEmpl(ClientDataSetTmp: TClientDataSet;
      DataSetProviderTmp: TDataSetProvider; QueryTmp: TQuery);
    procedure OpenTBDept(ClientDataSetTmp: TClientDataSet;
      DataSetProviderTmp: TDataSetProvider; QueryTmp: TQuery);
    procedure OpenTBShift(ClientDataSetTmp: TClientDataSet;
      DataSetProviderTmp: TDataSetProvider; QueryTmp: TQuery);

    procedure OpenBreaksEmpl(ClientDataSetTmp: TClientDataSet;
      DataSetProviderTmp: TDataSetProvider; QueryTmp: TQuery);
    procedure OpenBreaksDept(ClientDataSetTmp: TClientDataSet;
      DataSetProviderTmp: TDataSetProvider; QueryTmp: TQuery);
    procedure OpenBreaksShift(ClientDataSetTmp: TClientDataSet;
      DataSetProviderTmp: TDataSetProvider; QueryTmp: TQuery);

    procedure OpenShift(ClientDataSetTmp: TClientDataSet;
      DataSetProviderTmp: TDataSetProvider; QueryTmp: TQuery);

    function DecodeHrsMin(HrsMin: Integer):String;
    function FillZero(IntVal: Integer): String;
  end;

var
  CalculateTotalHoursDM: TCalculateTotalHoursDM;
  AShiftDay: TShiftDay;
  AProdMinClass: TProdMinClass;
  ATBLengthClass: TTBLengthClass;
{$IFDEF DEBUG}
  DebugLine: String;
  DebugOn: Boolean;
{$ENDIF}

implementation

{$R *.DFM}

uses
  SystemDMT, UGlobalFunctions;

// MR:16-07-2003
// Record, Class and Methods for
// determining the Shift-Start+End Dates.
// TShiftDay - Declarations - Start
procedure TShiftDay.DetermineShift(
  ADate: TDateTime;
  VAR AShiftDayRecord: TShiftDayRecord);
begin
  // Init the values
  AShiftDayRecord.APlantCode := APlantCode;
  AShiftDayRecord.AShiftNumber := AShiftNumber;
  AShiftDayRecord.ADate := ADate;

  // MR:26-10-2005 If 'Shift' comes from Pims, don't re-calculate.
  if AFromTimeRecordingApp then
  begin
    CalculateTotalHoursDM.QueryShiftSchedule.Close;
    CalculateTotalHoursDM.QueryShiftSchedule.
      ParamByName('EMPLOYEE_NUMBER').AsInteger := AEmployeeNumber;
    // MR:08-03-2005 Order 550387 Trunc added.
    CalculateTotalHoursDM.QueryShiftSchedule.
      ParamByName('SHIFT_SCHEDULE_DATE').AsDateTime := Trunc(ADate);
//    if not CalculateTotalHoursDM.QueryShiftSchedule.Prepared then // RV040.
//      CalculateTotalHoursDM.QueryShiftSchedule.Prepare;
    CalculateTotalHoursDM.QueryShiftSchedule.Open;
    if not CalculateTotalHoursDM.QueryShiftSchedule.IsEmpty then
    begin
      AShiftDayRecord.APlantCode :=
        CalculateTotalHoursDM.
          QueryShiftSchedule.FieldByName('PLANT_CODE').Value;
      AShiftDayRecord.AShiftNumber :=
        CalculateTotalHoursDM.
          QueryShiftSchedule.FieldByName('SHIFT_NUMBER').Value;
    end
    else
    begin
      // MR:08-03-2005 Order 550387
      CalculateTotalHoursDM.QueryEmployee.Close;
      CalculateTotalHoursDM.QueryEmployee.
        ParamByName('EMPLOYEE_NUMBER').AsInteger := AEmployeeNumber;
//      if not CalculateTotalHoursDM.QueryEmployee.Prepared then // RV040.
//        CalculateTotalHoursDM.QueryEmployee.Prepare;
      CalculateTotalHoursDM.QueryEmployee.Open;
      if not CalculateTotalHoursDM.QueryEmployee.IsEmpty then
      begin
        AShiftDayRecord.APlantCode :=
          CalculateTotalHoursDM.QueryEmployee.
            FieldByName('PLANT_CODE').AsString;
        AShiftDayRecord.AShiftNumber :=
          CalculateTotalHoursDM.QueryEmployee.
            FieldByName('SHIFT_NUMBER').AsInteger;
      end;
      CalculateTotalHoursDM.QueryEmployee.Close;
    end;
    CalculateTotalHoursDM.QueryShiftSchedule.Close;
  end;
end;

procedure TShiftDay.DetermineShiftStartEnd(
  VAR AShiftDayRecord: TShiftDayRecord);
var
  Day: Integer;
  SDay: String;
  Ready: Boolean;
  ADataSet: TDataSet;
begin
  Day:= DayInWeek(SystemDM.WeekStartsOn, AShiftDayRecord.ADate);
  SDay := IntToStr(Day);

  // Init the start and end
  AShiftDayRecord.AStart := Trunc(AShiftDayRecord.ADate);
  AShiftDayRecord.AEnd := Trunc(AShiftDayRecord.ADate);
  AShiftDayRecord.AValid := False;

  // Timeblocks Per Employee
  ADataSet := CalculateTotalHoursDM.ClientDataSetTBEmpl;
  ADataSet.Filtered := False;
  ADataSet.Filter :=
    'PLANT_CODE = ' + '''' + DoubleQuote(AShiftDayRecord.APlantCode) + '''' +
    ' AND ' +
    'EMPLOYEE_NUMBER = ' + IntToStr(AEmployeeNumber) + ' AND ' +
    'SHIFT_NUMBER = ' + IntToStr(AShiftDayRecord.AShiftNumber);
  ADataSet.Filtered := True;
  Ready := not ADataSet.IsEmpty;
{$IFDEF DEBUG}
  if Ready then
    if DebugLine = '' then
      DebugLine := '-- TB-EMP found.';
{$ENDIF}

  // Timeblocks Per Department
  if not Ready then
  begin
    // Timeblocks per Department
    ADataSet := CalculateTotalHoursDM.ClientDataSetTBDept;
    ADataSet.Filtered := False;
    ADataSet.Filter :=
      'PLANT_CODE = ' + '''' + DoubleQuote(AShiftDayRecord.APlantCode) +
      '''' + ' AND ' +
      'DEPARTMENT_CODE = ''' + DoubleQuote(ADepartmentCode) +
      '''' + ' AND ' +
      'SHIFT_NUMBER = ' + IntToStr(AShiftDayRecord.AShiftNumber);
    ADataSet.Filtered := True;
    Ready := not ADataSet.IsEmpty;
{$IFDEF DEBUG}
  if Ready then
    if DebugLine = '' then
      DebugLine := '-- TB-DEPT found.';
{$ENDIF}

    // Timeblocks Per Shift
    if not Ready then
    begin
      // Timeblocks Per Shift
      ADataSet := CalculateTotalHoursDM.ClientDataSetTBShift;
      ADataSet.Filtered := False;
      ADataSet.Filter :=
        'PLANT_CODE = ' + '''' + DoubleQuote(AShiftDayRecord.APlantCode) +
        '''' + ' AND ' +
        'SHIFT_NUMBER = ' + IntToStr(AShiftDayRecord.AShiftNumber);
      ADataSet.Filtered := True;
      Ready := not ADataSet.IsEmpty;
{$IFDEF DEBUG}
  if Ready then
    if DebugLine = '' then
      DebugLine := '-- TB-SHIFT found.';
{$ENDIF}
    end;
  end;

  if Ready then
  begin
    // Search and Set a valid STARTTIME
    // Start looking for FIRST valid timeblock to LAST.
    ADataSet.First;
    while not ADataSet.Eof do
    begin
      // If times are not the same, it's a valid time
      // otherwise they are not valid, like
      // Starttime='00:00' and endtime='00:00'
{$IFDEF DEBUG}
(*  if DebugOn then
    WDebugLog('-- StartTime=' +
      DateTimeToStr(ADataSet.FieldByName('STARTTIME' + SDay).AsDateTime) +
      ' EndTime=' +
      DateTimeToStr(ADataSet.FieldByName('ENDTIME' + SDay).AsDateTime)
      ); *)
{$ENDIF}
      if Frac(ADataSet.FieldByName('STARTTIME' + SDay).AsDateTime) <>
        Frac(ADataSet.FieldByName('ENDTIME' + SDay).AsDateTime) then
      begin
        AShiftDayRecord.AStart :=
          Trunc(AShiftDayRecord.ADate) +
          Frac(ADataSet.FieldByName('STARTTIME' + SDay).AsDateTime);
{$IFDEF DEBUG}
  if DebugOn then
    WDebugLog('-- AStart found=' + DateTimeToStr(AShiftDayRecord.AStart));
{$ENDIF}
        AShiftDayRecord.AValid := True;
        Break;
      end;
      ADataSet.Next;
    end;
    // Search and Set a valid ENDDTIME
    // Start looking from LAST valid timeblock to FIRST.
    ADataSet.Last;
    while not ADataSet.Bof do
    begin
      // If times are not the same, it's a valid time
      // otherwise they are not valid, like
      // Starttime='00:00' and endtime='00:00'
{$IFDEF DEBUG}
(*
  if DebugOn then
    WDebugLog('-- StartTime=' +
      DateTimeToStr(ADataSet.FieldByName('STARTTIME' + SDay).AsDateTime) +
      ' EndTime=' +
      DateTimeToStr(ADataSet.FieldByName('ENDTIME' + SDay).AsDateTime)
      );
*)      
{$ENDIF}
      if Frac(ADataSet.FieldByName('STARTTIME' + SDay).AsDateTime) <>
        Frac(ADataSet.FieldByName('ENDTIME' + SDay).AsDateTime) then
      begin
        AShiftDayRecord.AEnd :=
          Trunc(AShiftDayRecord.ADate) +
          Frac(ADataSet.FieldByName('ENDTIME' + SDay).AsDateTime);
{$IFDEF DEBUG}
  if DebugOn then
    WDebugLog('-- AEnd found=' + DateTimeToStr(AShiftDayRecord.AEnd));
{$ENDIF}
        // MR: 22-02-2005 Order 550384
        // If shift is overnight then end-day is next day.
        if AShiftDayRecord.AStart > AShiftDayRecord.AEnd then
        begin
          AShiftDayRecord.AEnd := AShiftDayRecord.AEnd + 1;
{$IFDEF DEBUG}
  if DebugOn then
    WDebugLog('-- Start is bigger than End. AStart=' +
      DateTimeToStr(AShiftDayRecord.AStart) +
      ' AEnd=' +
      DateTimeToStr(AShiftDayRecord.AEnd)
      );
{$ENDIF}
        end;
        AShiftDayRecord.AValid := True;
        Break;
      end;
      ADataSet.Prior;
    end;
  end;
end;

procedure TShiftDay.DetermineNewStartEnd(
  VAR NewScanStart, NewScanEnd: TDateTime);
var
  Ready: Boolean;
  SaveScanEnd: TDateTime;
begin
  Ready := False;

  NewScanStart := Curr.AStart;
  NewScanEnd := Curr.AEnd;

  // MR:08-01-2004 If there was an open-scan, 'ScanEnd' will be a nulldate,
  //               in that case set it to 'ScanStart'
  SaveScanEnd := AScanEnd;
  if AScanEnd = NullDate then
    AScanEnd := AScanStart;

  if (Prev.AValid)
     and
     //Car 15-10-2003
     (
     //
      (
       (Curr.AValid) and
       (AScanEnd < Curr.AStart) and
       (
         (AScanStart <= Prev.AEnd)
         or
         ((AScanStart - Prev.AEnd) < (Curr.AStart - AScanEnd))
       )
     )
     or
     (
       (not Curr.AValid) and
       (
         (not Next.AValid)
         or
         (
           (AScanStart <= Prev.AEnd) or
           (
             (AScanStart > Prev.AEnd) and
             (AScanEnd < Next.AStart) and
             ((AScanStart - Prev.AEnd) < (Next.AStart - AScanEnd))
           )
         )
       )
     )
    )then
  begin
    NewScanStart := Prev.AStart;
    NewScanEnd := Prev.AEnd;
    Ready := True;
  end;

  if not Ready then
  begin
    if (Next.AValid)
       and
      (
       (
         (Curr.AValid) and
         (AScanStart > Curr.AEnd) and
         (
           (AScanEnd >= Next.AStart)
           or
           ((Next.AStart - AScanEnd) < (AScanStart - Curr.AEnd))
         )
       )
       or
       (
         (not Curr.AValid) and
         (
           (not Prev.AValid)
           or
           (
             (AScanEnd >= Next.AStart) or
             (
               (AScanStart > Prev.AEnd) and
               (AScanEnd < Next.AStart) and
               ((AScanStart - Prev.AEnd) >= (Next.AStart - AScanEnd))
             )
           )
         )
       )
      )then
    begin
      NewScanStart := Next.AStart;
      NewScanEnd := Next.AEnd;
    end;
  end;
  AScanEnd := SaveScanEnd;
end;

// 20013489 EmployeeData must be a VAR-variable!
procedure TShiftDay.GetShiftDay(
  var EmployeeData: TScannedIDCard; var StartDateTime, EndDateTime: TDateTime);
var
  PeriodStartDateTime, PeriodEndDateTime: TDateTime;
begin
  AEmployeeNumber := EmployeeData.EmployeeCode;
  AScanStart := EmployeeData.DateIn;
  AScanEnd := EmployeeData.DateOut;
  APlantCode := EmployeeData.PlantCode;
  AShiftNumber := EmployeeData.ShiftNumber;
  ADepartmentCode := EmployeeData.DepartmentCode;
  // MR:27-10-2005 Use this to determine from what application
  // this is called. Only if it is from TimeRecording-application
  // this will be 'true'.
  AFromTimeRecordingApp := EmployeeData.FromTimeRecordingApp;


{$IFDEF DEBUG}
  DebugLine := '';
  DebugOn := True;
{$ENDIF}

  // MR:01-10-2003
  // If called from HoursPerEmployee and option 'Absence' or 'Salary' then
  // Shift must be '-1'.
  if AShiftNumber <> -1 then
  begin
{$IFDEF DEBUG}
  WDebugLog('- DetermineShiftStartEnd');
{$ENDIF}
    DetermineShift(AScanStart, Curr);
    DetermineShift(AScanStart + 1, Next);
    DetermineShift(AScanStart - 1, Prev);


    DetermineShiftStartEnd(Curr);
{$IFDEF DEBUG}
    DebugOn := False;
{$ENDIF}
    DetermineShiftStartEnd(Next);
    DetermineShiftStartEnd(Prev);

    DetermineNewStartEnd(StartDateTime, EndDateTime);

{$IFDEF DEBUG16}
  WDebugLog('- GetShiftDay' +
  ' DateIn=' + DateTimeToStr(EmployeeData.DateIn) +
  ' StartDateTime=' + DateTimeToStr(StartDateTime)
  );
{$ENDIF}

    // 20013489
    // When DateIn is lower then StartDateTime (Shiftdate) then
    // the Shiftdate (StartDateTime) must be set to date of DateIn.
    if SystemDM.UseShiftDateSystem then // 20014327
    begin
      if Trunc(EmployeeData.DateIn) < Trunc(StartDateTime) then
      begin
        StartDateTime := Trunc(EmployeeData.DateIn) + Frac(StartDateTime);
{$IFDEF DEBUG16}
  WDebugLog('- GetShiftDay - After corr. -' +
  ' StartDateTime=' + DateTimeToStr(StartDateTime)
  );
{$ENDIF}
      end;
    end
    else
    // TD-23416 Exception!
    // If dateIn is higher than period, then
    // only look at current. This prevents the salary hours are not booked
    // at all.
    begin
      // Check if the calculated period is the same as the period that is
      // processed. If not then the hours must be booked on the same day of
      // the datein, to prevent these are are lost!
      PeriodStartDateTime := StartDateTime;
      PeriodEndDateTime := EndDateTime;
      GlobalDM.ComputeOvertimePeriod(PeriodStartDateTime, PeriodEndDateTime,
        EmployeeData);

      // Are we in the wrong period?
      // Is DateIn higher then period that should be calculated?
      // Only look at date-part.
      if (Trunc(EmployeeData.DateIn) > Trunc(PeriodEndDateTime)) then
      begin
{$IFDEF DEBUG}
  WDebugLog('-- GetShiftDay - Exception! Only take current day.' +
    ' Emp=' + IntToStr(EmployeeData.EmployeeCode) +
    ' ' + DateTimeToStr(EmployeeData.DateIn) + ' > ' +
    DateTimeToStr(PeriodEndDateTime) +
    ' Period=' + DateToStr(PeriodStartDateTime) + ' to ' +
    DateTimeToStr(PeriodEndDateTime)
    );
{$ENDIF}
        Curr.AValid := True;
        Prev.AValid := False;
        Next.AValid := False;
        DetermineNewStartEnd(StartDateTime, EndDateTime);
      end;
    end; // TD-23416
  end
  else
  begin
    StartDateTime := AScanStart;
    EndDateTime := AScanEnd;
  end;
end;

procedure TShiftDay.SetEmployeeNumber(const Value: Integer);
begin
  FEmployeeNumber := Value;
end;

procedure TShiftDay.SetDepartmentCode(const Value: String);
begin
  FDepartmentCode := Value;
end;

procedure TShiftDay.SetScanStart(const Value: TDateTime);
begin
  FScanStart := Value;
end;

procedure TShiftDay.SetScanEnd(const Value: TDateTime);
begin
  FScanEnd := Value;
end;

procedure TShiftDay.SetPlantCode(const Value: String);
begin
  FPlantCode := Value;
end;

procedure TShiftDay.SetShiftNumber(const Value: Integer);
begin
  FShiftNumber := Value;
end;
// TShiftDay - Declarations - End

function TCalculateTotalHoursDM.FillZero(IntVal: Integer): String;
begin
  Result := IntToStr(IntVal);
  if (IntVal = 0) then
     Result := '00';
  if (length(IntToStr(IntVal)) = 1) then
    Result := '0' + IntToStr(IntVal);
end;

function TCalculateTotalHoursDM.DecodeHrsMin(HrsMin: Integer):String;
var
  Hrs, Min: Integer;
begin
  if HrsMin < 0 then
    Result := '-'
  else
    Result := '';
  HrsMin := Abs(HrsMin);
  Hrs := HrsMin div 60;
  Min := HrsMin mod 60;
  Result := Result +  FillZero(Hrs) + ':' +  FillZero(Min);
  if Length(Result)= 6 then
    Result := ' ' + Result;
  if Length(Result)= 5 then
    Result := '  ' + Result;
end;

procedure TCalculateTotalHoursDM.OpenClientDataSet(
  ClientDataSetTmp: TClientDataSet;
  DataSetProviderTmp: TDataSetProvider;
  QueryTmp: TQuery;
  SelectStr: String);
begin
  if ClientDataSetTmp.Active then
    Exit;
  QueryTmp.Close;
  QueryTmp.Sql.Clear;
  QueryTmp.Sql.Add(SelectStr);
  DataSetProviderTmp.DataSet := QueryTmp;
  ClientDataSetTmp.Open;
end;

// fill timeblocks per employee
// client data set will "locate" a record based on default index created by: ORDER BY
// it is not necessary to create this index on each Client Data Set ...

procedure TCalculateTotalHoursDM.OpenTBEmpl(ClientDataSetTmp: TClientDataSet;
  DataSetProviderTmp: TDataSetProvider; QueryTmp: TQuery);
var
  SelectStr: String;
begin
  SelectStr :=
    'SELECT ' +
    '  EMPLOYEE_NUMBER, PLANT_CODE, SHIFT_NUMBER, TIMEBLOCK_NUMBER, DESCRIPTION, ' +
    '  STARTTIME1, ENDTIME1, STARTTIME2, ENDTIME2, STARTTIME3, ENDTIME3, ' +
    '  STARTTIME4, ENDTIME4, STARTTIME5, ENDTIME5, STARTTIME6, ENDTIME6, ' +
    '  STARTTIME7, ENDTIME7 ' +
    'FROM ' +
    '  TIMEBLOCKPEREMPLOYEE ' +
    'ORDER BY ' +
    '  PLANT_CODE, EMPLOYEE_NUMBER, SHIFT_NUMBER, TIMEBLOCK_NUMBER ';
  OpenClientDataSet(ClientDataSetTmp, DataSetProviderTmp, QueryTmp, SelectStr);
end;

// fill timeblocks per department
procedure TCalculateTotalHoursDM.OpenTBDept(ClientDataSetTmp: TClientDataSet;
  DataSetProviderTmp: TDataSetProvider; QueryTmp: TQuery);
var
  SelectStr: String;
begin
  SelectStr :=
    'SELECT ' +
    '  PLANT_CODE, DEPARTMENT_CODE, SHIFT_NUMBER, TIMEBLOCK_NUMBER, DESCRIPTION, ' +
    '  STARTTIME1, ENDTIME1, STARTTIME2, ENDTIME2, STARTTIME3, ENDTIME3, ' +
    '  STARTTIME4, ENDTIME4, STARTTIME5, ENDTIME5, STARTTIME6, ENDTIME6, ' +
    '  STARTTIME7, ENDTIME7 ' +
    'FROM ' +
    '  TIMEBLOCKPERDEPARTMENT ' +
    'ORDER BY ' +
    '  PLANT_CODE, DEPARTMENT_CODE, SHIFT_NUMBER, TIMEBLOCK_NUMBER ';
  OpenClientDataSet(ClientDataSetTmp, DataSetProviderTmp, QueryTmp, SelectStr);
end;

procedure TCalculateTotalHoursDM.OpenTBShift(ClientDataSetTmp: TClientDataSet;
  DataSetProviderTmp: TDataSetProvider; QueryTmp: TQuery);
var
  SelectStr: String;
begin
  SelectStr :=
    'SELECT ' +
    '  PLANT_CODE, SHIFT_NUMBER, TIMEBLOCK_NUMBER, DESCRIPTION, ' +
    '  STARTTIME1, ENDTIME1, STARTTIME2, ENDTIME2, STARTTIME3, ENDTIME3, ' +
    '  STARTTIME4, ENDTIME4, STARTTIME5, ENDTIME5, STARTTIME6, ENDTIME6, ' +
    '  STARTTIME7, ENDTIME7 ' +
    'FROM ' +
    '  TIMEBLOCKPERSHIFT ' +
    'ORDER BY ' +
    '  PLANT_CODE, SHIFT_NUMBER, TIMEBLOCK_NUMBER ';
  OpenClientDataSet(ClientDataSetTmp, DataSetProviderTmp, QueryTmp, SelectStr);
end;

procedure TCalculateTotalHoursDM.OpenBreaksEmpl(
  ClientDataSetTmp: TClientDataSet;
  DataSetProviderTmp: TDataSetProvider; QueryTmp: TQuery);
var
  SelectStr: String;
begin
  SelectStr :=
    'SELECT ' +
    '  PLANT_CODE, EMPLOYEE_NUMBER, SHIFT_NUMBER, DESCRIPTION, BREAK_NUMBER, PAYED_YN, ' +
    '  STARTTIME1, ENDTIME1, STARTTIME2, ENDTIME2, STARTTIME3, ENDTIME3, ' +
    '  STARTTIME4, ENDTIME4, STARTTIME5, ENDTIME5, STARTTIME6, ENDTIME6, ' +
    '  STARTTIME7, ENDTIME7 ' +
    'FROM ' +
    '  BREAKPEREMPLOYEE ' +
    'ORDER BY ' +
    '  PLANT_CODE, EMPLOYEE_NUMBER, SHIFT_NUMBER, BREAK_NUMBER ';
  OpenClientDataSet(ClientDataSetTmp, DataSetProviderTmp, QueryTmp, SelectStr);
end;

procedure TCalculateTotalHoursDM.OpenBreaksDept(ClientDataSetTmp: TClientDataSet;
  DataSetProviderTmp: TDataSetProvider; QueryTmp: TQuery);
var
  SelectStr: String;
begin
  SelectStr :=
    'SELECT ' +
    '  PLANT_CODE, DEPARTMENT_CODE, SHIFT_NUMBER, BREAK_NUMBER, DESCRIPTION, PAYED_YN, ' +
    '  STARTTIME1, ENDTIME1, STARTTIME2, ENDTIME2, STARTTIME3, ENDTIME3, ' +
    '  STARTTIME4, ENDTIME4, STARTTIME5, ENDTIME5, STARTTIME6, ENDTIME6, ' +
    '  STARTTIME7, ENDTIME7 ' +
    'FROM ' +
    '  BREAKPERDEPARTMENT ' +
    'ORDER BY ' +
    '  PLANT_CODE, DEPARTMENT_CODE, SHIFT_NUMBER, BREAK_NUMBER';
  OpenClientDataSet(ClientDataSetTmp, DataSetProviderTmp, QueryTmp, SelectStr);
end;

procedure TCalculateTotalHoursDM.OpenBreaksShift(
  ClientDataSetTmp: TClientDataSet;
  DataSetProviderTmp: TDataSetProvider; QueryTmp: TQuery);
var
  SelectStr: String;
begin
  SelectStr :=
    'SELECT ' +
    '  PLANT_CODE, SHIFT_NUMBER, BREAK_NUMBER, DESCRIPTION, PAYED_YN, '  +
    '  STARTTIME1, ENDTIME1, STARTTIME2, ENDTIME2, STARTTIME3, ENDTIME3, ' +
    '  STARTTIME4, ENDTIME4, STARTTIME5, ENDTIME5, STARTTIME6, ENDTIME6, ' +
    '  STARTTIME7, ENDTIME7 ' +
    'FROM ' +
    '  BREAKPERSHIFT ' +
    'ORDER BY ' +
    '  PLANT_CODE, SHIFT_NUMBER, BREAK_NUMBER' ;
  OpenClientDataSet(ClientDataSetTmp, DataSetProviderTmp, QueryTmp, SelectStr);
end;

procedure TCalculateTotalHoursDM.OpenShift(ClientDataSetTmp: TClientDataSet;
  DataSetProviderTmp: TDataSetProvider; QueryTmp: TQuery);
var
  SelectStr: String;
begin
  SelectStr :=
    'SELECT ' +
    '  PLANT_CODE, SHIFT_NUMBER, ' +
    '  STARTTIME1, ENDTIME1, STARTTIME2, ENDTIME2, STARTTIME3, ENDTIME3, ' +
    '  STARTTIME4, ENDTIME4, STARTTIME5, ENDTIME5, STARTTIME6, ENDTIME6, ' +
    '  STARTTIME7, ENDTIME7 ' +
    'FROM ' +
    '  SHIFT ' +
    'ORDER BY ' +
    '  PLANT_CODE, SHIFT_NUMBER ';
  OpenClientDataSet(ClientDataSetTmp, DataSetProviderTmp, QueryTmp, SelectStr);
end;

// start functions of class TTBLengthClass
procedure TTBLengthClass.SetEmployeeNumber(const Value: Integer);
begin
  FEmployeeNumber := Value;
end;

procedure TTBLengthClass.SetDepartmentCode(const Value: String);
begin
  FDepartmentCode := Value;
end;

procedure TTBLengthClass.SetRemoveNotPaidBreaks(const Value: Boolean);
begin
  FRemoveNotPaidBreaks := Value;
end;

procedure TTBLengthClass.SetPlantCode(const Value: String);
begin
  FPlantCode := Value;
end;

procedure TTBLengthClass.SetShiftNumber(const Value: Integer);
begin
  FShiftNumber := Value;
end;

constructor TTBLengthClass.Create;
begin
  inherited Create;
  OpenDataTBLength;
end;

procedure TTBLengthClass.Free;
begin
  CloseDataTBLength;
  inherited Free;
end;

procedure TTBLengthClass.OpenDataTBLength;
begin
  with CalculateTotalHoursDM do
  begin
    OpenTBEmpl(ClientDataSetTBEmpl, DataSetProviderTBEmpl, QuerySelector);
    OpenTBDept(ClientDataSetTBDept, DataSetProviderTBDept, QuerySelector);
    OpenTBShift(ClientDataSetTBShift, DataSetProviderTBShift, QuerySelector);
    OpenBreaksEmpl(ClientDataSetBEmpl, DataSetProviderBEmpl, QuerySelector);
    OpenBreaksDept(ClientDataSetBDept, DataSetProviderBDept, QuerySelector);
    OpenBreaksShift(ClientDataSetBShift, DataSetProviderBShift, QuerySelector);
  end;
end;

procedure TTBLengthClass.CloseDataTBLength;
begin
  with CalculateTotalHoursDM do
  begin
    ClientDataSetTBEmpl.Close;
    ClientDataSetTBDept.Close;
    ClientDataSetTBShift.Close;
    ClientDataSetBEmpl.Close;
    ClientDataSetBDept.Close;
    ClientDataSetBShift.Close;
  end;
end;


function TTBLengthClass.GetMinutes(DateTimeMinutes: TDateTime): Integer;
var
  Hour, Min, Sec, MSec: Word;
begin
  DecodeTime(DateTimeMinutes, Hour, Min, Sec, MSec);
  Result := Hour * 60 + Min;
end;

procedure TTBLengthClass.FillTBValues(TmpClientDataSet: TClientDataSet;
 FillMinutes: Boolean; var Index: integer);
var
  IndexDay: Integer;
begin
  Index := Index + 1;
  FTBValid[Index] := Index;
  if FillMinutes then
    for IndexDay := 1 to 7 do
    begin
      FTBStart[Index, IndexDay] :=
        GetMinutes(TmpClientDataSet.FieldByName('STARTTIME' +
          IntToStr(IndexDay)).AsDateTime);
      FTBEnd[Index, IndexDay] :=
        GetMinutes(TmpClientDataSet.FieldByName('ENDTIME' +
          IntToStr(IndexDay)).AsDateTime);
      if FTBStart[Index, IndexDay] > FTBEnd[Index, IndexDay] then
        FTBEnd[Index, IndexDay] := FTBEnd[Index, IndexDay] + 24 * 60;
    end;
  TmpClientDataSet.Next;
end;

procedure TTBLengthClass.FillTBArray_ClientDataSet(ClientDataSetTmp_TBEmpl,
  ClientDataSetTmp_TBDept, ClientDataSetTmp_TBShift: TClientDataSet;
  FillMinutes, SetEmptyArray: Boolean);
var
  IndexTB, IndexDay: Integer;
begin
  for IndexTB := 1 to SystemDM.MaxTimeblocks do
  begin
    FTBValid[IndexTB] := 0;
    if SetEmptyArray then
      for IndexDay := 1 to 7 do
      begin
        FTBStart[IndexTB,IndexDay] := 0;
        FTBEnd[IndexTB,IndexDay] := 0;
        FBreak[IndexTB,IndexDay] := 0;
      end;
  end;

  IndexTB := 0;
  if (AEmployeeNumber <> -1) then
  begin
    ClientDataSetTmp_TBEmpl.Filtered := False;
    ClientDataSetTmp_TBEmpl.Filter :=
      'PLANT_CODE = ' + '''' + DoubleQuote(APlantCode) + '''' + ' AND ' +
      'EMPLOYEE_NUMBER = ' + IntToStr(AEmployeeNumber) + ' AND ' +
      'SHIFT_NUMBER = ' + IntToStr(AShiftNumber);
    ClientDataSetTmp_TBEmpl.Filtered := True;
    ClientDataSetTmp_TBEmpl.First;
    while not ClientDataSetTmp_TBEmpl.Eof do
    //!! it is inside the FillTB function : "next" function on clientdataset
      FillTBValues(ClientDataSetTmp_TBEmpl, FillMinutes, IndexTB);
    if IndexTB <> 0 then
      Exit;
  end;
  ClientDataSetTmp_TBDept.Filtered := False;
  ClientDataSetTmp_TBDept.Filter :=
    'PLANT_CODE = ' + '''' + DoubleQuote(APlantCode) + '''' + ' AND ' +
    'DEPARTMENT_CODE = ''' + DoubleQuote(ADepartmentCode) + ''' AND ' +
    'SHIFT_NUMBER = ' + IntToStr(AShiftNumber);
  ClientDataSetTmp_TBDept.Filtered := True;
  ClientDataSetTmp_TBDept.First;
  while not ClientDataSetTmp_TBDept.Eof do
      FillTBValues(ClientDataSetTmp_TBDept, FillMinutes, IndexTB);
  if IndexTB <> 0 then
    Exit;

  ClientDataSetTmp_TBShift.Filtered := False;
  ClientDataSetTmp_TBShift.Filter :=
    'PLANT_CODE = ' + '''' + DoubleQuote(APlantCode) + '''' + ' AND ' +
    'SHIFT_NUMBER = ' + IntToStr(AShiftNumber);
  ClientDataSetTmp_TBShift.Filtered := True;
  ClientDataSetTmp_TBShift.First;

  while not ClientDataSetTmp_TBShift.Eof do
    FillTBValues(ClientDataSetTmp_TBShift, FillMinutes, IndexTB);
end;

procedure TTBLengthClass.ValidTB(Employee, Shift: Integer;
  Plant, Department: String);
begin
// set properties
  AEmployeeNumber := Employee;
  AShiftNumber := Shift;
  APlantCode:= Plant;
  ADepartmentCode := Department;

  FillTBArray_ClientDataSet(CalculateTotalHoursDM.ClientDataSetTBEmpl,
    CalculateTotalHoursDM.ClientDataSetTBDept,
    CalculateTotalHoursDM.ClientDataSetTBShift, False, False);
end;

procedure TTBLengthClass.FillTBArray(Employee, Shift: Integer;
  Plant, Department: String);
begin
  AEmployeeNumber := Employee;
  AShiftNumber := Shift;
  APlantCode:= Plant;
  ADepartmentCode := Department;

  FillTBArray_ClientDataSet(CalculateTotalHoursDM.ClientDataSetTBEmpl,
    CalculateTotalHoursDM.ClientDataSetTBDept,
    CalculateTotalHoursDM.ClientDataSetTBShift, True, True);
end;

procedure TTBLengthClass.GetTimeBreaks(BreaksStart, BreaksEnd: TDateTime;
  Index, IndexDay: Integer; var BreakTime: Integer);
var
  BreaksStartMin, BreaksEndMin: Integer;
begin
  BreakTime := 0;
  BreaksStartMin := GetMinutes(BreaksStart);
  BreaksEndMin := GetMinutes(BreaksEnd);
  if BreaksStart > BreaksEnd then
    BreaksEndMin := GetMinutes(BreaksEnd) + 24 * 60;
  if (BreaksEndMin < FTBStart[Index, IndexDay]) or
    (BreaksStartMin > FTBEnd[Index, IndexDay]) then
    Exit;
  BreakTime := Min(BreaksEndMin,FTBEnd[Index, IndexDay]) -
    Max(BreaksStartMin,FTBStart[Index, IndexDay]);
end;

procedure TTBLengthClass.FillBreaks(Employee, Shift: Integer;
  Plant, Department: String; RemoveNotPaidBreaks: Boolean);
begin
  AEmployeeNumber := Employee;
  APlantCode := Plant;
  AShiftNumber := Shift;
  ADepartmentCode := Department;
  ARemoveNotPaidBreaks := RemoveNotPaidBreaks;

  FillBreaks_ClientDataSet(CalculateTotalHoursDM.ClientDataSetBEmpl,
    CalculateTotalHoursDM.ClientDataSetBDept,
    CalculateTotalHoursDM.ClientDataSetBShift);
end;

procedure TTBLengthClass.FillBreaks_ClientDataSet(
  ClientDataSetTmp_BEmpl, ClientDataSetTmp_BDept,
  ClientDataSetTmp_BShift: TClientDataSet);
var
  IndexTB, IndexDay: Integer;
  BreakTime: Integer;
  FillBreaks: Boolean;
begin
  FillBreaks := False;
  for IndexTB := 1 to SystemDM.MaxTimeblocks do
    for IndexDay := 1 to 7 do
       FBreak[IndexTB,IndexDay] := 0;
  if AEmployeeNumber <> -1 then
  begin
    ClientDataSetTmp_BEmpl.Filtered := False;
    ClientDataSetTmp_BEmpl.Filter :=
      'PLANT_CODE = ' + '''' + DoubleQuote(APlantCode) + '''' + ' AND ' +
      'EMPLOYEE_NUMBER = ' + IntToStr(AEmployeeNumber) + ' AND ' +
      'SHIFT_NUMBER = ' + IntToStr(AShiftNumber);
    ClientDataSetTmp_BEmpl.Filtered := True;
    ClientDataSetTmp_BEmpl.First;
    while not ClientDataSetTmp_BEmpl.Eof do
    begin
      if ((ClientDataSetTmp_BEmpl.FieldByName('PAYED_YN').AsString =
        UNCHECKEDVALUE) and ARemoveNotPaidBreaks) or
        (not ARemoveNotPaidBreaks) then
      begin
        for IndexTB := 1 to SystemDM.MaxTimeblocks do
          for IndexDay := 1 to 7 do
          begin
            GetTimeBreaks(
              ClientDataSetTmp_BEmpl.FieldByName('STARTTIME' +
                IntToStr(IndexDay)).AsDateTime,
              ClientDataSetTmp_BEmpl.FieldByName('ENDTIME' +
                IntToStr(IndexDay)).AsDateTime,
              IndexTB, IndexDay, BreakTime);
            FBreak[IndexTB, IndexDay] := FBreak[IndexTB, IndexDay] + BreakTime;
            FillBreaks := True;
          end;
      end;
      ClientDataSetTmp_BEmpl.Next;
    end;// while
  end;
  if FillBreaks then
    Exit;
  ClientDataSetTmp_BDept.Filtered := False;
  ClientDataSetTmp_BDept.Filter :=
    'PLANT_CODE = ' + '''' + DoubleQuote(APlantCode) + '''' + ' AND ' +
    'DEPARTMENT_CODE = ''' + DoubleQuote(ADepartmentCode) + ''' AND ' +
    'SHIFT_NUMBER = ' + IntToStr(AShiftNumber);
  ClientDataSetTmp_BDept.Filtered := True;
  ClientDataSetTmp_BDept.First;
  while not ClientDataSetTmp_BDept.Eof do
  begin
    if ((ClientDataSetTmp_BDept.FieldByName('PAYED_YN').AsString =
      UNCHECKEDVALUE) and (ARemoveNotPaidBreaks)) or
      (not ARemoveNotPaidBreaks) then
      for IndexTB := 1 to SystemDM.MaxTimeblocks do
        for IndexDay := 1 to 7 do
        begin
          GetTimeBreaks(
            ClientDataSetTmp_BDept.FieldByName('STARTTIME' +
              IntToStr(IndexDay)).AsDateTime,
            ClientDataSetTmp_BDept.FieldByName('ENDTIME' +
              IntToStr(IndexDay)).AsDateTime,
            IndexTB, IndexDay, BreakTime);
          FBreak[IndexTB, IndexDay] := FBreak[IndexTB, IndexDay] + BreakTime;
          FillBreaks := True;
        end;
     ClientDataSetTmp_BDept.Next;
  end; // while
  if FillBreaks then
    Exit;
  ClientDataSetTmp_BShift.Filtered := False;
  ClientDataSetTmp_BShift.Filter :=
    'PLANT_CODE = ' + '''' + DoubleQuote(APlantCode) + '''' + ' AND ' +
    'SHIFT_NUMBER = ' + IntToStr(AShiftNumber);
  ClientDataSetTmp_BShift.Filtered := True;
  ClientDataSetTmp_BShift.First;
  while not ClientDataSetTmp_BShift.Eof do
  begin
    if ((ClientDataSetTmp_BShift.FieldByName('PAYED_YN').AsString =
      UNCHECKEDVALUE) and (ARemoveNotPaidBreaks)) or
      (not ARemoveNotPaidBreaks) then
       for IndexTB := 1 to SystemDM.MaxTimeblocks do
         for IndexDay := 1 to 7 do
         begin
           GetTimeBreaks(
             ClientDataSetTmp_BShift.FieldByName('STARTTIME' +
               IntToStr(IndexDay)).AsDateTime,
             ClientDataSetTmp_BShift.FieldByName('ENDTIME' +
               IntToStr(IndexDay)).AsDateTime,
             IndexTB, IndexDay, BreakTime);
           FBreak[IndexTB, IndexDay] := FBreak[IndexTB, IndexDay] + BreakTime;
         end;
    ClientDataSetTmp_BShift.Next;
  end; // while
end;

procedure TTBLengthClass.FillTBLength(Employee, Shift: Integer;
  Plant, Department: String; RemoveNotPaidBreaks: Boolean);
begin
  FillTBArray(Employee, Shift, Plant, Department);
  FillBreaks(Employee, Shift, Plant, Department, RemoveNotPaidBreaks);
end;

function TTBLengthClass.GetValidTB(TB_Number: Integer): Integer;
begin
  Result := FTBValid[TB_Number];
end;

function TTBLengthClass.ExistTB(TB_Number: Integer): Boolean;
begin
  Result := (FTBValid[TB_Number] <> 0);
end;

function TTBLengthClass.GetLengthTB(TB_Number, Day_Number: Integer): Integer;
begin
  Result := FTBEnd[TB_Number, Day_Number] - FTBStart[TB_Number, Day_Number] -
    FBreak[TB_Number, Day_Number];
end;

function TTBLengthClass.GetStartTime(TB_Number,
  Day_Number: Integer): TDateTime;
begin
  // RV025. If time is set as 1440 (00:00) it will give an exception.
  //        Because it tries an EncodeTime with (24, 0, 0, 0)
  try
    Result := EncodeTime(FTBStart[TB_Number, Day_Number] div 60,
      FTBStart[TB_Number, Day_Number] mod 60, 0, 0);
  except
    Result := EncodeTime(0, 0, 0, 0);
  end;
end;

function TTBLengthClass.GetEndTime(TB_Number,
  Day_Number: Integer): TDateTime;
var
  Minutes: Integer;
begin
  // RV025. If time is set as 1440 (00:00) it will give an exception.
  //        Because it tries an EncodeTime with (24, 0, 0, 0)
  try
    // RV027. If time is higher than 1440 (24:00) , then subtact this first!
    //        Because then time of next day (1 day was added).
    //        Example 27:00 whould be 3:00.
    //        Also correct 1440 (24:00 -> 00:00) here.
    Minutes := FTBEnd[TB_Number, Day_Number];
    if Minutes > 1440 then
      Minutes := Minutes - 1440
    else
      if Minutes = 1440 then
        Minutes := 0;
    Result := EncodeTime(Minutes div 60, Minutes mod 60, 0, 0);
//    Result := EncodeTime(FTBEnd[TB_Number, Day_Number] div 60,
//      FTBEnd[TB_Number, Day_Number] mod 60, 0, 0);
  except
    Result := EncodeTime(0, 0, 0, 0);
  end;
end;
// end function of TTBLength class

// start function of call TProdMinClass
constructor TProdMinClass.Create;
begin
  inherited Create;
  OpenDataProdMin;
end;

procedure TProdMinClass.Free;
begin
  CloseDataProdMin;
  inherited Free;
end;

procedure TProdMinClass.OpenDataProdMin;
begin
  with CalculateTotalHoursDM do
  begin
    OpenTBEmpl(ClientDataSetTBEmpl, DataSetProviderTBEmpl, QuerySelector);
    OpenTBDept(ClientDataSetTBDept, DataSetProviderTBDept, QuerySelector);
    OpenTBShift(ClientDataSetTBShift, DataSetProviderTBShift, QuerySelector);
    OpenBreaksEmpl(ClientDataSetBEmpl, DataSetProviderBEmpl, QuerySelector);
    OpenBreaksDept(ClientDataSetBDept, DataSetProviderBDept, QuerySelector);
    OpenBreaksShift(ClientDataSetBShift, DataSetProviderBShift, QuerySelector);
    OpenShift(ClientDataSetShift, DataSetProviderShift, QuerySelector);
    // prepare for speed reason queries used in function EmployeeCutOfTime
//    QueryRequest.Prepare; // RV040.
  end;
end;

procedure TProdMinClass.CloseDataProdMin;
begin
  with CalculateTotalHoursDM do
  begin
    ClientDataSetTBEmpl.Close;
    ClientDataSetTBDept.Close;
    ClientDataSetTBShift.Close;
    ClientDataSetShift.Close;
    ClientDataSetBEmpl.Close;
    ClientDataSetBDept.Close;
    ClientDataSetBShift.Close;
    QueryRequest.Close;
//    QueryRequest.Unprepare; // RV040.
  end;
end;

// Reread all data
procedure TProdMinClass.Refresh;
begin
  CloseDataProdMin;
  OpenDataProdMin;
end;

procedure TProdMinClass.SetEmployeeData(const Value: TScannedIDCard);
begin
  FEmployeeData := Value;
end;

procedure TProdMinClass.SetOperationMode(const Value: Integer);
begin
  FOperationMode := Value;
end;

procedure TProdMinClass.SetStartTime(const Value: TDateTime);
begin
  FStartTime := Value;
end;

procedure TProdMinClass.SetContractRoundMinutes(const Value: Boolean);
begin
  FContractRoundMinutes := Value;
end;

procedure TProdMinClass.SetEndTime(const Value: TDateTime);
begin
  FEndTime := Value;
end;

// RV055.1.
  // MR:23-1-2004 Get some fields from Contractgroup-table for given employee
procedure TProdMinClass.ReadContractRoundMinutesParams(
  EmployeeData: TScannedIDCard;
  var RoundTruncSalaryHours, RoundMinute: Integer); // RV063.1.
begin
//  RoundTruncSalaryHours := -1;
//  RoundMinute := -1;
  // RV055.1. First get values from IDCard, if they are 0 then get them from query.
  RoundTruncSalaryHours := EmployeeData.RoundTruncSalaryHours;
  RoundMinute := EmployeeData.RoundMinute;
  if (RoundTruncSalaryHours = 0) and (RoundMinute = 0) then
  begin
    with CalculateTotalHoursDM do
    begin
      QueryContractGroup.Close;
      QueryContractGroup.ParamByName('EMPLOYEE_NUMBER').AsInteger :=
        EmployeeData.EmployeeCode;
      // MR:08-07-2005 'not' was missing in following line!
  //    if not QueryContractGroup.Prepared then // RV040.
  //      QueryContractGroup.Prepare;
      QueryContractGroup.Open;
      if not QueryContractGroup.IsEmpty then
      begin
        QueryContractGroup.First;
        RoundTruncSalaryHours :=
          QueryContractGroup.FieldByName('ROUND_TRUNC_SALARY_HOURS').AsInteger;
        RoundMinute :=
          QueryContractGroup.FieldByName('ROUND_MINUTE').AsInteger;
      end;
      QueryContractGroup.Close;
    end;
  end;
end;

// store all records of Time block per Empl/Time block perDept/ Time block PerShift
// SHIFT tables into a separate list - only first and last TimeBlock is necessary
// these list are used in GetShiftStartDate function
procedure TProdMinClass.ComputeBreaks(
  EmployeeData: TScannedIDCard;
  STime, ETime: TDateTime; OperationMode: Integer;
  var ProdMin, BreaksMin, PayedBreaks: Integer);
begin
  AEmployeeData := EmployeeData;
  AStartTime := STime;
  AEndTime := ETime;
  AOperationMode := OperationMode;
  ComputeBreaks_ClientDataSet(CalculateTotalHoursDM.ClientDataSetBEmpl,
    CalculateTotalHoursDM.ClientDataSetBDept,
    CalculateTotalHoursDM.ClientDataSetBShift,
    ProdMin, BreaksMin, PayedBreaks);
end;

procedure TProdMinClass.ComputeBreaks_ClientDataSet(
  ClientDataSetBEmpl_Tmp, ClientDataSetBDept_Tmp,
  ClientDataSetBShift_Tmp: TClientDataSet;
  var ProdMin, BreaksMin, PayedBreaks: Integer);
var
  WeekDay: Integer;
  Ready: Boolean;
  StartTime, EndTime : TDateTime;
{$IFDEF DEBUG}
  DebugStr: String;
{$ENDIF}
  function SearchTimeBlock(DataSet: TDataSet;
    AStartTime, AEndTime: TDateTime; AOperationMode: Integer;
    StartTime, EndTime: TDateTime; WeekDay: Integer;
    var ProdMin, BreaksMin, PayedBreaks: Integer): Boolean;
  var
    StartDateTime, EndDateTime: TDateTime;
    MinOfBreak: Integer;
  begin
    Result := False;
    while not DataSet.Eof  do
    begin
      StartDateTime := DataSet.
        FieldByName('STARTTIME' + IntToStr(WeekDay)).AsDateTime;
      EndDateTime := DataSet.
        FieldByName('ENDTIME' + IntToStr(WeekDay)).AsDateTime;
      ReplaceTime(StartTime, StartDateTime);
      ReplaceTime(EndTime, EndDateTime);
      if StartDateTime > EndDateTime then
      	 EndTime := EndTime + 1; //ends in next day
      MinOfBreak :=
        ComputeMinutesOfIntersection(AStartTime, AEndTime, StartTime, EndTime);
      BreaksMin := BreaksMin + MinOfBreak;
{$IFDEF DEBUG1}
(*  WDebugLog('SearchTimeBlock AStartTime=' + DateTimeToStr(AStartTime) +
    ' AEndTime=' + DateTimeToStr(AEndTime) +
    ' StartTime=' + DateTimeToStr(StartTime) +
    ' EndTime=' + DateTimeToStr(EndTime) +
    ' BreaksMin=' + IntToStr(BreaksMin) +
    ' MinOfBreak= ' + IntToStr(MinOfBreak)
    ); *)
{$ENDIF}
      if DataSet.FieldByName('PAYED_YN').AsString = CHECKEDVALUE then
        PayedBreaks := PayedBreaks + MinOfBreak;
      if AOperationMode = 1 then
        EndTime := EndTime + MinOfBreak / DayToMin;
      DataSet.Next;
      Result := True;
    end;
  end;
begin
//{$IFDEF DEBUG}
//  WDebugLog('- GetBreaks-Start');
//{$ENDIF}
  WeekDay := DayInWeek(SystemDM.WeekStartsOn, AStartTime);
  ProdMin := 0;
  BreaksMin := 0;
  PayedBreaks := 0;
  StartTime := AStartTime;
  EndTime := AStartTime;

  // Breaks per Employee
  ClientDataSetBEmpl_Tmp.Filtered := False;
  ClientDataSetBEmpl_Tmp.Filter :=
    'PLANT_CODE = ' + '''' +
      DoubleQuote(AEmployeeData.PlantCode) + '''' + ' AND ' +
    'EMPLOYEE_NUMBER = ' + IntToStr(AEmployeeData.EmployeeCode) + ' AND ' +
    'SHIFT_NUMBER = ' + IntToStr(AEmployeeData.ShiftNumber);
  ClientDataSetBEmpl_Tmp.Filtered := True;
  ClientDataSetBEmpl_Tmp.First;

  Ready := SearchTimeBlock(ClientDataSetBEmpl_Tmp,
    AStartTime, AEndTime, AOperationMode,
    StartTime, EndTime, WeekDay, ProdMin, BreaksMin, PayedBreaks);

{$IFDEF DEBUG}
  if Ready then
    WDebugLog('- GetBreaks-Emp found.');
{$ENDIF}

  // Breaks per Department
  if not Ready then
  begin
    ClientDataSetBDept_Tmp.Filtered := False;
    ClientDataSetBDept_Tmp.Filter :=
      'PLANT_CODE = ' + '''' +
        DoubleQuote(AEmployeeData.PlantCode) + '''' + ' AND ' +
      'DEPARTMENT_CODE = ''' +
        DoubleQuote(AEmployeeData.DepartmentCode) + '''' + ' AND ' +
      'SHIFT_NUMBER = ' + IntToStr(AEmployeeData.ShiftNumber);
    ClientDataSetBDept_Tmp.Filtered := True;
    ClientDataSetBDept_Tmp.First;

    Ready := SearchTimeBlock(ClientDataSetBDept_Tmp,
      AStartTime, AEndTime, AOperationMode,
      StartTime, EndTime, WeekDay, ProdMin, BreaksMin, PayedBreaks);
{$IFDEF DEBUG}
  if Ready then
    WDebugLog('- GetBreaks-Dept found.');
{$ENDIF}
  end;

  // Breaks per Shift
  if not Ready then
  begin
    ClientDataSetBShift_Tmp.Filtered := False;
    ClientDataSetBShift_Tmp.Filter :=
      'PLANT_CODE = ' + '''' +
        DoubleQuote(AEmployeeData.PlantCode) + '''' + ' AND ' +
      'SHIFT_NUMBER = ' + IntToStr(AEmployeeData.ShiftNumber);
    ClientDataSetBShift_Tmp.Filtered := True;
    ClientDataSetBShift_Tmp.First;

{$IFDEF DEBUG}
  Ready :=
{$ENDIF}
    SearchTimeBlock(ClientDataSetBShift_Tmp,
      AStartTime, AEndTime, AOperationMode,
      StartTime, EndTime, WeekDay, ProdMin, BreaksMin, PayedBreaks);
{$IFDEF DEBUG}
  if Ready then
    WDebugLog('- GetBreaks-Shift found.');
{$ENDIF}
  end;

{$IFDEF DEBUG}
  if not Ready then
    WDebugLog('- GetBreaks-Nothing found.');
{$ENDIF}

  ProdMin := Round((AEndTime - AStartTime) * DayToMin - BreaksMin);
//{$IFDEF DEBUG}
//  WDebugLog('- GetBreaks-End');
//{$ENDIF}
end;

// - read the information from the new lists created instead of queries
procedure TProdMinClass.GetShiftStartEnd(
  EmployeeData: TScannedIDCard;
  ShiftDate: TDateTime;
  var StartDateTime, EndDateTime: TDateTime);
var
  Current_Day : Integer;
  WeekStartDateTime, WeekEndDateTime: TWeekDates;
  Ready: Boolean;
begin
  Ready := False;
  StartDateTime := EmployeeData.DateIn;
  EndDateTime := EmployeeData.DateIn;
  Current_Day:= DayInWeek(SystemDM.WeekStartsOn, ShiftDate);

  GetShiftWStartEnd(EmployeeData, WeekStartDateTime, WeekEndDateTime, Ready);
  if Ready then
  begin
    StartDateTime := WeekStartDateTime[Current_Day];
    EndDateTime := WeekEndDateTime[Current_Day];
  end;
  ReplaceDate(StartDateTime, ShiftDate);
  ReplaceDate(EndDateTime, ShiftDate);
end;

procedure TProdMinClass.GetShiftWStartEnd(
  EmployeeData: TScannedIDCard;
  var WeekStartDateTime, WeekEndDateTime: TWeekDates;
  var Ready: Boolean);
begin
  AEmployeeData := EmployeeData;

  GetShiftWStartEnd_ClientDataSet(CalculateTotalHoursDM.ClientDataSetTBEmpl,
    CalculateTotalHoursDM.ClientDataSetTBDept,
    CalculateTotalHoursDM.ClientDataSetTBShift,
    CalculateTotalHoursDM.ClientDataSetShift,
    WeekStartDateTime, WeekEndDateTime,
    Ready);
end;

procedure TProdMinClass.GetShiftWStartEnd_ClientDataSet(
  ClientDataSetProdTBEmpl_Tmp, ClientDataSetProdTBDept_Tmp,
  ClientDataSetProdTBShift_Tmp, ClientDataSetShift_Tmp: TClientDataSet;
  var WeekStartDateTime, WeekEndDateTime: TWeekDates;
  var Ready: Boolean);
var
  Index: Integer;
  FirstPlanTimeblock, LastPlanTimeblock: Integer;
  PlanningBlocksFilter: String;
{$IFDEF DEBUG}
  DebugStr: String;
{$ENDIF}
begin
  Ready := False;

  // RV092.8.
  PlanningBlocksFilter := '';
  // PIM-295 Do this always!
//  if SystemDM.IsLTB then
    if FindPlanningBlocks(AEmployeeData, FirstPlanTimeblock,
      LastPlanTimeblock) then
      PlanningBlocksFilter := ' AND ' +
        '(' +
        '  TIMEBLOCK_NUMBER = ' + IntToStr(FirstPlanTimeBlock) +
        '  OR TIMEBLOCK_NUMBER = ' + IntToStr(LastPlanTimeblock) +
        ')';

{$IFDEF DEBUG}
  if SystemDM.IsLTB then
  begin
    if PlanningBlocksFilter <> '' then
      WDebugLog('Planning/Avail. found: ' + PlanningBlocksFilter)
    else
      WDebugLog('No planning/Avail. found.');
  end;
{$ENDIF}


  ClientDataSetProdTBEmpl_Tmp.Filtered := False;
  ClientDataSetProdTBEmpl_Tmp.Filter :=
    'PLANT_CODE = ' + '''' + DoubleQuote(AEmployeeData.PlantCode) +
    '''' + ' AND ' +
    'EMPLOYEE_NUMBER = ' + IntToStr(AEmployeeData.EmployeeCode) + ' AND ' +
    'SHIFT_NUMBER = ' + IntToStr(AEmployeeData.ShiftNumber) +
    PlanningBlocksFilter;
  ClientDataSetProdTBEmpl_Tmp.Filtered := True;
  if not ClientDataSetProdTBEmpl_Tmp.IsEmpty then
  begin
    ClientDataSetProdTBEmpl_Tmp.First;
    for Index := 1 to 7 do
      WeekStartDateTime[Index] := ClientDataSetProdTBEmpl_Tmp.
        FieldByName('STARTTIME' + IntToStr(Index)).AsDateTime;
    ClientDataSetProdTBEmpl_Tmp.Last;
    for Index := 1 to 7 do
      WeekEndDateTime[Index] := ClientDataSetProdTBEmpl_Tmp.
        FieldByName('ENDTIME' + IntToStr(Index)).AsDateTime;
{$IFDEF DEBUG}
  WDebugLog('- GetShift-TBEmp found.');
{$ENDIF}
    Ready := True;
  end;

  if not Ready then
  begin
    ClientDataSetProdTBDept_Tmp.Filtered := False;
    ClientDataSetProdTBDept_Tmp.Filter :=
      'PLANT_CODE = ' + '''' + DoubleQuote(AEmployeeData.PlantCode) +
      '''' + ' AND ' +
      'DEPARTMENT_CODE = ' + '''' + DoubleQuote(AEmployeeData.DepartmentCode) +
      '''' + ' AND ' +
      'SHIFT_NUMBER = ' + IntToStr(AEmployeeData.ShiftNumber) +
      PlanningBlocksFilter;
    ClientDataSetProdTBDept_Tmp.Filtered := True;
    if not ClientDataSetProdTBDept_Tmp.IsEmpty  then
    begin
      ClientDataSetProdTBDept_Tmp.First;
      for Index := 1 to 7 do
        WeekStartDateTime[Index] := ClientDataSetProdTBDept_Tmp.
          FieldByName('STARTTIME' + IntToStr(Index)).AsDateTime;
      ClientDataSetProdTBDept_Tmp.Last;
      for Index := 1 to 7 do
        WeekEndDateTime[Index] := ClientDataSetProdTBDept_Tmp.
          FieldByName('ENDTIME' + IntToStr(Index)).AsDateTime;
{$IFDEF DEBUG}
  WDebugLog('- GetShift-TBDept found.');
{$ENDIF}
      Ready := True;
    end;
  end;

  if not Ready then
  begin
    ClientDataSetProdTBShift_Tmp.Filtered := False;
    ClientDataSetProdTBShift_Tmp.Filter :=
      'PLANT_CODE = ' + '''' + DoubleQuote(AEmployeeData.PlantCode) +
      '''' + ' AND ' +
      'SHIFT_NUMBER = ' + IntToStr(AEmployeeData.ShiftNumber) +
      PlanningBlocksFilter;
    ClientDataSetProdTBShift_Tmp.Filtered := True;
    if not ClientDataSetProdTBShift_Tmp.IsEmpty then
    begin
      ClientDataSetProdTBShift_Tmp.First;
      for Index := 1 to 7 do
        WeekStartDateTime[Index] :=
          ClientDataSetProdTBShift_Tmp.
            FieldByName('STARTTIME' + IntToStr(Index)).AsDateTime;
      ClientDataSetProdTBShift_Tmp.Last;
      for Index := 1 to 7 do
        WeekEndDateTime[Index] :=
          ClientDataSetProdTBShift_Tmp.
            FieldByName('ENDTIME' + IntToStr(Index)).AsDateTime;
{$IFDEF DEBUG}
  WDebugLog('- GetShift-TBShift found.');
{$ENDIF}
      Ready := True;
    end;
  end;

  if not Ready then
  begin
    ClientDataSetShift_Tmp.Filtered := False;
    ClientDataSetShift_Tmp.Filter :=
      'PLANT_CODE = ' + '''' + DoubleQuote(AEmployeeData.PlantCode) +
      '''' + ' AND ' +
      'SHIFT_NUMBER = ' + IntToStr(AEmployeeData.ShiftNumber);
    ClientDataSetShift_Tmp.Filtered := False;
    if not ClientDataSetShift_Tmp.IsEmpty then
    begin
      for Index := 1 to 7 do
      begin
        WeekStartDateTime[Index] :=
          ClientDataSetShift_Tmp.FieldByName('STARTTIME' +
            IntToStr(Index)).AsDateTime;
        WeekEndDateTime[Index] :=
          ClientDataSetShift_Tmp.FieldByName('ENDTIME' +
            IntToStr(Index)).AsDateTime;
      end;
{$IFDEF DEBUG}
  WDebugLog('- GetShift-Shift found.');
{$ENDIF}
      Ready := True;
    end;
  end;
{$IFDEF DEBUG}
  if Ready then
  begin
    DebugStr := '- ';
    for Index := 1 to 7 do
    begin
      DebugStr := DebugStr + IntToStr(Index) + ':' +
        FormatDateTime('hh:mm', WeekStartDateTime[Index]) + '-' +
        FormatDateTime('hh:mm', WeekEndDateTime[Index]) + ' ';
    end;
    WDebugLog(DebugStr);
  end
  else
    WDebugLog('- GetShift-Nothing found!');
{$ENDIF}
end;

// this function is the same as function of UGlobalUnit -
// but it call the new defined function  GetShiftWStartEnd
// 20013489 EmployeeData must be a VAR-variable!
procedure TProdMinClass.GetShiftDay(
  var EmployeeData: TScannedIDCard;
  var StartDateTime, EndDateTime: TDateTime);
begin
// MR:18-07-2003 This replaces the old functionality
  AShiftDay.GetShiftDay(EmployeeData, StartDateTime, EndDateTime);
end;

// MR:03-12-2003 EmployeeData changed to 'VAR'-variable.
procedure TProdMinClass.EmployeeCutOfTime(
  FirstScan, LastScan: Boolean; VAR EmployeeData: TScannedIDCard;
  DeleteAction: Boolean; DeleteEmployeeData: TScannedIDCard;
  var StartDTInterval, EndDTInterval:TDateTime);
begin
  AEmployeeData := EmployeeData;
  EmployeeCutOfTime_Query(
    FirstScan, LastScan, EmployeeData,
    DeleteAction, DeleteEmployeeData, StartDTInterval, EndDTInterval);
end;

// MR:25-09-2003
// MR:03-12-2003 EmployeeData changed to 'VAR'-variable, so values for
//               StartDTInterval+EndDTInterfval can be stored in this var. for
//               later use in 'BooksExceptionalTime'.
// MR:23-1-2004 RoundMinutes-procedures added.
procedure TProdMinClass.EmployeeCutOfTime_Query(
  FirstScan, LastScan: Boolean; VAR EmployeeData: TScannedIDCard;
  DeleteAction: Boolean; DeleteEmployeeData: TScannedIDCard;
  var StartDTInterval, EndDTInterval: TDateTime);
var
//  Ready: Boolean;
  CutOffStart, CutOffEnd: Boolean;
  RoundTruncSalaryHours, RoundMinute: Integer;
  MyQueryPrevScans, MyQueryNextScans: TQuery;
  IsMarginInEarly, IsMarginOutEarly: Boolean;
  // PIM-326
(*
  procedure SearchPrevScan(var ADateTimeIn: TDateTime);
  begin
    repeat
      // MR:10-10-2003 If action is 'Delete' then don't look
      // at the scan that will be deleted.
      if DeleteAction then
      begin
        MyQueryPrevScans := CalculateTotalHoursDM.qryPrevScan1;
        MyQueryPrevScans.Close;
        MyQueryPrevScans.
          ParamByName('EMPLOYEE_NUMBER').AsInteger :=
            EmployeeData.EmployeeCode;
        MyQueryPrevScans.
          ParamByName('DATETIME_OUT').AsDateTime := ADateTimeIn;
        MyQueryPrevScans.
          ParamByName('D_OUT').AsDateTime := DeleteEmployeeData.DateOut;
      end
      else
      begin
        MyQueryPrevScans := CalculateTotalHoursDM.qryPrevScan2;
        MyQueryPrevScans.Close;
        MyQueryPrevScans.
          ParamByName('EMPLOYEE_NUMBER').AsInteger :=
            EmployeeData.EmployeeCode;
        MyQueryPrevScans.
          ParamByName('DATETIME_OUT').AsDateTime:= ADateTimeIn;
      end;
      MyQueryPrevScans.Open;
      if not MyQueryPrevScans.IsEmpty then
      begin
        ADateTimeIn := MyQueryPrevScans.
          FieldByName('DATETIME_IN').AsDateTime;
        Ready := (ADateTimeIn <
          (StartDTInterval - EmployeeData.InscanEarly / DayToMin));
        if EmployeeData.DateIn > StartDTInterval then
          Ready := True;
      end;
    until MyQueryPrevScans.IsEmpty or Ready;
  end; // SearchPrevScan
  procedure SearchNextScan(var ADateTimeOut: TDateTime);
  begin
    repeat
      // MR:10-10-2003 If action is 'Delete' then don't look
      // at the scan that will be deleted.
      if DeleteAction then
      begin
        MyQueryNextScans := CalculateTotalHoursDM.qryNextScan1;
        MyQueryNextScans.Close;
        MyQueryNextScans.
          ParamByName('EMPLOYEE_NUMBER').AsInteger :=
            EmployeeData.EmployeeCode;
        MyQueryNextScans.
          ParamByName('DATETIME_OUT').AsDateTime := ADateTimeOut;
        MyQueryNextScans.
          ParamByName('D_IN').AsDateTime := DeleteEmployeeData.DateIn;
      end
      else
      begin
        MyQueryNextScans := CalculateTotalHoursDM.qryNextScan2;
        MyQueryNextScans.Close;
        MyQueryNextScans.
          ParamByName('EMPLOYEE_NUMBER').AsInteger :=
            EmployeeData.EmployeeCode;
        MyQueryNextScans.
          ParamByName('DATETIME_OUT').AsDateTime:= ADateTimeOut;
      end;
      MyQueryNextScans.Open;
      if not MyQueryNextScans.IsEmpty then
      begin
        ADateTimeOut :=
          MyQueryNextScans.
            FieldByName('DATETIME_OUT').AsDateTime;
        // MR:09-10-2003 '+' instead of '-'
        Ready := (ADateTimeOut >
          (EndDTInterval + EmployeeData.OutScanLate / DayToMin));
        // MR:09-10-2003 '<' instead of '>'
        if EmployeeData.DateOut < EndDTInterval then
          Ready := True;
      end;
    until MyQueryNextScans.IsEmpty or Ready;
  end; // SearchNextScan
*)
  // MR:23-1-2004 If needed, round the minutes.
  procedure ActionContractRoundMinutes;
  // MR:12-10-2004 Not needed.
(*
  var
    {MyDateTimeIn, }MyDateTimeOut: TDateTime;
    function RoundMinutes(RoundTruncSalaryHours, RoundMinute: Integer;
      ADate: TDateTime): TDateTime;
    var
      Hours, Mins, Secs, MSecs: Word;
    begin
      DecodeTime(ADate, Hours, Mins, Secs, MSecs);
      if RoundTruncSalaryHours = 1 then // Round
        Mins := Round(Mins / RoundMinute) * RoundMinute
      else
        Mins := Trunc(Mins / RoundMinute) * RoundMinute;
      Result := Trunc(ADate) + EncodeTime(Hours, Mins, Secs, MSecs);
    end;
*)
  begin
    // MR:23-1-2004
    AContractRoundMinutes := False;
    if (RoundTruncSalaryHours <> -1) and (RoundMinute <> -1) then
    begin
      if (RoundMinute > 1) then
        AContractRoundMinutes := True;
  // MR:12-10-2004 Not needed.
(*
      begin
//        MyDateTimeIn := EmployeeData.DateIn;
//        SearchPrevScan(MyDateTimeIn);
        // If this is the first scan of the day, then round minutes for start
//        if (MyDateTimeIn = StartDTInterval) then
//          StartDTInterval := RoundMinutes(RoundTruncSalaryHours, RoundMinute,
//            StartDTInterval);
        // Look if this is the last scan.
        MyDateTimeOut := EmployeeData.DateOut;
        SearchNextScan(MyDateTimeOut);
        // If this is the last scan of the day, then round minutes for end
        if (MyDateTimeOut = EmployeeData.DateOut) then
          AContractRoundMinutes := True;
//          EndDTInterval := RoundMinutes(RoundTruncSalaryHours, RoundMinute,
//            EndDTInterval);
      end;
*)
    end;
  end;
  // PIM-326 Only call this when really needed, not for all scans!
  procedure DetermineRequestEarlyLate;
  var
    RequestDate, EarlyTime, LateTime: TDateTime;
    DateFrom, DateTo: TDateTime;
  begin
    CalculateTotalHoursDM.QueryRequest.Close;
    RequestDate := EmployeeData.DateIn;
    // MR:13-02-2003 Be sure date-compare will work.
    //               Because 'REQUEST_DATE' contains a time-part, it
    //               must be compared by using a from/to datetime.
    DateFrom := Trunc(RequestDate) + Frac(EncodeTime(0, 0, 0, 0));
    DateTo := Trunc(RequestDate) + Frac(EncodeTime(23, 59, 59, 99));
    CalculateTotalHoursDM.QueryRequest.ParamByName('DATEFROM').AsDateTime :=
      DateFrom;
    CalculateTotalHoursDM.QueryRequest.ParamByName('DATETO').AsDateTime :=
      DateTo;
    CalculateTotalHoursDM.QueryRequest.ParamByName('EMPNO').AsInteger:=
      EmployeeData.EmployeeCode;
    CalculateTotalHoursDM.QueryRequest.Open;
    CalculateTotalHoursDM.QueryRequest.First;
    if not CalculateTotalHoursDM.QueryRequest.IsEmpty then
    begin
      // PIM-326 Check to see if early and late was found.
      EarlyTime := 0;
      if CalculateTotalHoursDM.QueryRequest.
          FieldByName('REQUESTED_EARLY_TIME').AsString <> '' then
        EarlyTime :=
          CalculateTotalHoursDM.QueryRequest.
            FieldByName('REQUESTED_EARLY_TIME').AsDateTime;
      LateTime := 0;
      if CalculateTotalHoursDM.QueryRequest.
          FieldByName('REQUESTED_LATE_TIME').AsString <> '' then
        LateTime :=
          CalculateTotalHoursDM.QueryRequest.
            FieldByName('REQUESTED_LATE_TIME').AsDateTime;
{$IFDEF DEBUG}
  if EarlyTime = 0 then
    WDebugLog('- EmpCutOffTime. RequestedEarlyTime not defined.')
  else
    WDebugLog('- EmpCutOffTime. RequestedEarlyTime=' + FormatDateTime('hh:mm', EarlyTime));
  if LateTime = 0 then
    WDebugLog('- EmpCutOffTime. RequestedLateTime not defined.')
  else
    WDebugLog('- EmpCutOffTime. RequestedLateTime=' + FormatDateTime('hh:mm', LateTime));
{$ENDIF}
      // PIM-326 Only do this when early or late was found!
      if EarlyTime > 0 then
        ReplaceTime(StartDTInterval, EarlyTime);
      if LateTime > 0 then
        ReplaceTime(EndDTInterval, LateTime);
    end;
    CalculateTotalHoursDM.QueryRequest.Close;
  end; // DetermineRequestEarlyLate;
  // PIM-348
  // Check if first found scan (within 12 hours) for this employee falls in
  // the In-Margin.
  function MarginInCheck: Boolean;
  var
    FirstDateTimeIn: TDateTime;
  begin
    Result := False;
    with CalculateTotalHoursDM.qryFirstScan do
    begin
      Close;
      ParamByName('EMPLOYEE_NUMBER').AsInteger := EmployeeData.EmployeeCode;
      ParamByName('DATEFROM').AsDateTime := EmployeeData.DateInOriginal - 0.5;
      ParamByName('DATETO').AsDateTime := EmployeeData.DateInOriginal;
      if DeleteAction then
        ParamByName('CHECKDELETEDATE').AsInteger := 1
      else
        ParamByName('CHECKDELETEDATE').AsInteger := 0;
      ParamByName('DELETEDATE').AsDateTime := DeleteEmployeeData.DateInOriginal;
      Open;
      if not Eof then
      begin
        if FieldByName('DATETIME_IN').AsString <> '' then
        begin
          FirstDateTimeIn := FieldByName('DATETIME_IN').AsDateTime;
          if (FirstDateTimeIn >=
            (StartDTInterval - EmployeeData.InscanEarly / DayToMin)) and
            (FirstDateTimeIn <= StartDTInterval) then
          begin
            IsMarginInEarly := True;
            Result := True;
          end
          else
            if (FirstDateTimeIn >= StartDTInterval) and
              (FirstDateTimeIn <=
              (StartDTInterval + EmployeeData.InscanLate / DayToMin)) then
            begin
              IsMarginInEarly := False;
              if FirstDateTimeIn = EmployeeData.DateInOriginal then
                Result := True;
            end;
        end;
      end;
      Close;
    end;
  end; // MarginInCheck
  // PIM-348
  // Check if last found scan (within 12 hours) for this employee falls in
  // the Out-Margin.
  function MarginOutCheck: Boolean;
  var
    LastDateTimeOut: TDateTime;
  begin
    Result := False;
    with CalculateTotalHoursDM.qryLastScan do
    begin
      Close;
      ParamByName('EMPLOYEE_NUMBER').AsInteger := EmployeeData.EmployeeCode;
      ParamByName('DATEFROM').AsDateTime := EmployeeData.DateInOriginal;
      ParamByName('DATETO').AsDateTime := EmployeeData.DateInOriginal + 0.5;
      if DeleteAction then
        ParamByName('CHECKDELETEDATE').AsInteger := 1
      else
        ParamByName('CHECKDELETEDATE').AsInteger := 0;
      ParamByName('DELETEDATE').AsDateTime := DeleteEmployeeData.DateInOriginal;
      Open;
      if not Eof then
      begin
        if FieldByName('DATETIME_OUT').AsString <> '' then
        begin
          LastDateTimeOut := FieldByName('DATETIME_OUT').AsDateTime;
          if (LastDateTimeOut >=
            (EndDTInterval - EmployeeData.OutScanEarly / DayToMin)) and
            (LastDateTimeOut <= EndDTInterval) then
          begin
            IsMarginOutEarly := True;
            if LastDateTimeOut = EmployeeData.DateOutOriginal then
              Result := True;
          end
          else
            if (LastDateTimeOut >= EndDTInterval) and
              (LastDateTimeOut <=
              (EndDTInterval + EmployeeData.OutScanLate / DayToMin)) then
            begin
              IsMarginOutEarly := False;
              Result := True;
            end;
        end;
      end;
      Close;
    end;
  end; // MarginOutCheck
begin
// MR:05-09-2003
  // MR:23-1-2004
  ReadContractRoundMinutesParams(EmployeeData, RoundTruncSalaryHours,
    RoundMinute);

  IsMarginInEarly := False;
  IsMarginOutEarly := False;
  CutOffStart := False;
  CutOffEnd := False;
//  Ready := False;
  if EmployeeData.CutOfTime = CHECKEDVALUE then
  begin
    MyQueryPrevScans := nil;
    MyQueryNextScans := nil;

    // PIM-326
(*
    DateTimeIn := EmployeeData.DateIn;
    // Search backwards in scans
    if FirstScan and
      (DateTimeIn >=
        (StartDTInterval - EmployeeData.InscanEarly / DayToMin)) and
      (DateTimeIn <=
        (StartDTInterval + EmployeeData.InscanLate / DayToMin)) then
    begin
      SearchPrevScan(DateTimeIn);
    end
    else
      SearchPrevScan(DateTimeIn);
    //Cut of time
    if MyQueryPrevScans <> nil then
      if MyQueryPrevScans.IsEmpty then
        CutOffStart := True;
*)
    // PIM-326
    // Determine if FirstScan is within margin, if this is the case then any
    // following scan must be checked for margin-correction.
{
    if not DeleteAction then
      CutOffStart := IsFirstScan(EmployeeData.EmployeeCode, EmployeeData.DateInOriginal)
    else
      CutOffStart := IsFirstScan(EmployeeData.EmployeeCode, EmployeeData.DateInOriginal,
        DeleteEmployeeData.DateInOriginal);
}
    // PIM-348
    if (EmployeeData.DateIn >=
       (StartDTInterval - EmployeeData.InscanEarly / DayToMin)) and
       (EmployeeData.DateIn <=
       (StartDTInterval + EmployeeData.InscanLate / DayToMin)) then
      CutOffStart := MarginInCheck;

    // PIM-326
(*
    DateTimeOut := EmployeeData.DateOut;
    // Search forwards in scans
    if LastScan and
      (DateTimeOut >=
        (EndDTInterval - EmployeeData.OutScanEarly / DayToMin)) and
      (DateTimeOut <=
        (EndDTInterval + EmployeeData.OutScanLate / DayToMin)) then
    begin
      SearchNextScan(DateTimeOut);
    end // if LastScan and ...
    else
      SearchNextScan(DateTimeOut);
    // Cut of time
    if MyQueryNextScans <> nil then
      if MyQueryNextScans.IsEmpty then
        CutOffend := True;
*)
{
    // PIM-326
    if not DeleteAction then
      CutOffEnd := IsLastScan(EmployeeData.EmployeeCode, EmployeeData.DateOutOriginal)
    else
      CutOffEnd := IsLastScan(EmployeeData.EmployeeCode, EmployeeData.DateOutOriginal,
        DeleteEmployeeData.DateInOriginal);
}
    // PIM-348
    if (EmployeeData.DateOut >=
      (EndDTInterval - EmployeeData.OutScanEarly / DayToMin)) and
      (EmployeeData.DateOut <=
      (EndDTInterval + EmployeeData.OutScanLate / DayToMin)) then
      CutOffEnd := MarginOutCheck;

    if MyQueryPrevScans <> nil then
      MyQueryPrevScans.Close;
    if MyQueryNextScans <> nil then
      MyQueryNextScans.Close;
  end; // if EmployeeData.CutOfTime = CHECKEDVALUE then

{$IFDEF DEBUG}
  WDebugLog('First/Last-Scan Emp=' + IntToStr(EmployeeData.EmployeeCode) +
    ' DateIn=' + DateTimeToStr(EmployeeData.DateInOriginal) +
    ' DateOut=' + DateTimeToStr(EmployeeData.DateOutOriginal) +
    ' CutOffStart='  + Bool2Str(CutOffStart) +
    ' CutOffEnd=' + Bool2Str(CutOffEnd)
    );
{$ENDIF}

  // PIM-326
  if CutOffStart or CutOffEnd then
    DetermineRequestEarlyLate;

  if CutOffStart then
  begin
{    StartDTInterval := DateInInterval(StartDTInterval, EmployeeData.DateIn,
      EmployeeData.InscanEarly, EmployeeData.InscanLate); }
    if IsMarginInEarly then
      StartDTInterval := DateInInterval(StartDTInterval, EmployeeData.DateIn,
        EmployeeData.InscanEarly, 0)
    else
      StartDTInterval := DateInInterval(StartDTInterval, EmployeeData.DateIn,
        0, EmployeeData.InscanLate);
  end
  else
    StartDTInterval := EmployeeData.DateIn;
  if CutOffEnd then
  begin
{    EndDTInterval := DateInInterval(EndDTInterval, EmployeeData.DateOut,
      EmployeeData.OutscanEarly, EmployeeData.OutscanLate) }
    if IsMarginOutEarly then
      EndDTInterval := DateInInterval(EndDTInterval, EmployeeData.DateOut,
        EmployeeData.OutscanEarly, 0)
    else
      EndDTInterval := DateInInterval(EndDTInterval, EmployeeData.DateOut,
        0, EmployeeData.OutscanLate);
  end
  else
    EndDTInterval := EmployeeData.DateOut;
  // MR:23-1-2004
  ActionContractRoundMinutes;
  if StartDTInterval > EndDTInterval then
    EndDTInterval := StartDTInterval;
  // MR:03-12-2003 Store the values for later use.
  EmployeeData.DateInCutOff := StartDTInterval;
  EmployeeData.DateOutCutOff := EndDTInterval;
{$IFDEF DEBUG}
  WDebugLog('CutOff-Margins Emp=' + IntToStr(EmployeeData.EmployeeCode) +
    ' DateInCutOff=' + DateTimeToStr(EmployeeData.DateInCutOff) +
    ' DateOutCutOff=' + DateTimeToStr(EmployeeData.DateOutCutOff) +
    ' IsMarginInEarly=' + Bool2Str(IsMarginInEarly) +
    ' IsMarginOutEarly=' + Bool2Str(IsMarginOutEarly)
    );
{$ENDIF}
end;

// MR:03-12-2003 EmployeeData changed to 'VAR'-variable.
procedure TProdMinClass.GetProdMin(VAR EmployeeData: TScannedIDCard;
  FirstScan, LastScan: Boolean;
  DeleteAction: Boolean; DeleteEmployeeData: TScannedIDCard;
  var BookingDay: TDateTime;
  var Minutes, BreaksMin, PayedBreaks: integer);
var
  StartDate, EndDate: TDateTime;
begin
  GetShiftDay(EmployeeData, StartDate, EndDate);
  BookingDay := Trunc(StartDate);

  GetShiftStartEnd(EmployeeData, StartDate, StartDate, EndDate);

{$IFDEF DEBUG}
  WDebugLog('- GetShiftStartEnd. StartDate=' + FormatDateTime('dd-mm-yyyy hh:mm', StartDate) +
    ' EndDate=' + FormatDateTime('dd-mm-yyyy hh:mm', EndDate));
{$ENDIF}

  EmployeeCutOfTime(
    FirstScan, LastScan, EmployeeData,
    DeleteAction, DeleteEmployeeData,
    StartDate, EndDate);

{$IFDEF DEBUG}
  WDebugLog('- EmpCutOffTime. StartDate=' +
    FormatDateTime('dd-mm-yyyy hh:mm', StartDate) +
    ' EndDate=' + FormatDateTime('dd-mm-yyyy hh:mm', EndDate));
{$ENDIF}

  ComputeBreaks(EmployeeData, StartDate, EndDate, 0,
    Minutes, BreaksMin, PayedBreaks);
end;

// MR:03-12-2003 EmployeeData changed to 'VAR'-variable.
// MRA:15-JAN-2009 RV020.
// This must be used for scans that could be overnight-scans!
procedure TProdMinClass.GetProdMinOvernight(VAR EmployeeData: TScannedIDCard;
  FirstScan, LastScan: Boolean;
  DeleteAction: Boolean; DeleteEmployeeData: TScannedIDCard;
  var BookingDay: TDateTime;
  var Minutes, BreaksMin, PayedBreaks: integer);
var
  ProdMinTot, BreaksMinTot, PayedBreaksTot: Integer;
  SaveDate_Out, SaveDate_In, BookingDayDummy: TDateTime;
  IsOverNight: Boolean;
begin
  ProdMinTot := 0;
  BreaksMinTot := 0;
  PayedBreaksTot := 0;
  SaveDate_Out := EmployeeData.DateOut;
  SaveDate_In := EmployeeData.DateIn;

  IsOverNight := Trunc(EmployeeData.DateOut) > Trunc(EmployeeData.DateIn);
  if IsOverNight then
  begin
    EmployeeData.DateOut := Trunc(EmployeeData.DateOut);
    GetProdMin(EmployeeData, FirstScan, LastScan, DeleteAction,
      DeleteEmployeeData, BookingDay, Minutes, BreaksMin, PayedBreaks);
    ProdMinTot := Minutes;
    BreaksMinTot := BreaksMin;
    PayedBreaksTot := PayedBreaks;

    EmployeeData.DateIn := EmployeeData.DateOut;
    EmployeeData.DateOut := SaveDate_Out;
  end;
  GetProdMin(EmployeeData, FirstScan, LastScan, DeleteAction,
    DeleteEmployeeData, BookingDayDummy, Minutes, BreaksMin, PayedBreaks);
  ProdMinTot := ProdMinTot + Minutes;
  BreaksMinTot := BreaksMinTot + BreaksMin;
  PayedBreaksTot := PayedBreaksTot + PayedBreaks;

  EmployeeData.DateOut := SaveDate_Out;
  EmployeeData.DateIn := SaveDate_In;
  Minutes := ProdMinTot;
  BreaksMin := BreaksMinTot;
  PayedBreaks := PayedBreaksTot;
end;

procedure TCalculateTotalHoursDM.DataModuleCreate(Sender: TObject);
begin
//  inherited;
  AProdMinClass := TProdMinClass.Create;
  ATBLengthClass := TTBLengthClass.Create;
  AShiftDay := TShiftDay.Create;
  // MR:03-04-2006 ClientDataSets,
  //               set Logchanges to False for performance-reasons.
  try
    ClientDataSetBShift.LogChanges := False;
    ClientDataSetBDept.LogChanges := False;
    ClientDataSetBEmpl.LogChanges := False;
    ClientDataSetShift.LogChanges := False;
    ClientDataSetTBShift.LogChanges := False;
    ClientDataSetTBDept.LogChanges := False;
    ClientDataSetTBEmpl.LogChanges := False;
  finally
    // ignore error.
  end;
end;

procedure TCalculateTotalHoursDM.DataModuleDestroy(Sender: TObject);
begin
  inherited;
  AProdMinClass.Free;
  ATBLengthClass.Free;
  AShiftDay.Free;
end;

// RV092.8.
function TProdMinClass.FindPlanningBlocks(EmployeeData: TScannedIDCard;
  var FirstPlanTimeblock, LastPlanTimeblock: Integer): Boolean;
var
  TBNumber: Integer;
  DayOfWeek: Integer;
begin
  Result := False;
  // What about 'overnight' ?
  // PIM-181 Bugfix: This query has been changed to prevent an
  //         access violation when a timeblock is null!
  with CalculateTotalHoursDM.qryFindPlanningBlocks do
  begin
{$IFDEF DEBUG}
  WDebugLog('Find Planning/Avail.:' +
    ' D=' + FormatDateTime('dd-mm-yyyy', Trunc(EmployeeData.DateIn)) +
    ' P=' + EmployeeData.PlantCode +
    ' W=' + EmployeeData.WorkspotCode +
    ' E=' + IntToStr(EmployeeData.EmployeeCode) +
    ' S=' + IntToStr(EmployeeData.ShiftNumber)
    );
{$ENDIF}
    // Addition: Also look at standard-planning, employee-availability and
    //           standard-employee-availability.
    DayOfWeek := DayInWeek(SystemDM.WeekStartsOn, Trunc(EmployeeData.DateIn));
    Close;
    ParamByName('EMPLOYEEPLANNING_DATE').AsDateTime :=
      Trunc(EmployeeData.DateIn);
    ParamByName('PLANT_CODE').AsString := EmployeeData.PlantCode;
    ParamByName('WORKSPOT_CODE').AsString := EmployeeData.WorkSpotCode;
    ParamByName('EMPLOYEE_NUMBER').AsInteger := EmployeeData.EmployeeCode;
    ParamByName('SHIFT_NUMBER').AsInteger := EmployeeData.ShiftNumber;
    ParamByName('DAY_OF_WEEK').AsInteger := DayOfWeek;
    Open;
    if not Eof then
    begin
      Result := True;
      // Init
      FirstPlanTimeblock := 1;
      LastPlanTimeblock := SystemDM.MaxTimeblocks;
      // What is the first planned/available timeblock?
      for TBNumber := 1 to SystemDM.MaxTimeblocks do
        if FieldByName('SCHEDULED_TIMEBLOCK_' +
          IntToStr(TBNumber)).AsString[1] in ['A', 'B', 'C', '*'] then
        begin
          FirstPlanTimeblock := TBNumber;
          Break;
        end;
      // What is the last planned/available timeblock?
      for TBNumber := SystemDM.MaxTimeblocks downto 1 do
        if FieldByName('SCHEDULED_TIMEBLOCK_' +
          IntToStr(TBNumber)).AsString[1] in ['A', 'B', 'C', '*'] then
        begin
          LastPlanTimeblock := TBNumber;
          Break;
        end;
    end;
    Close;
  end;
end;

// 20013288
// Determine start/end of Timeblock.
procedure TShiftDay.DetermineShiftStartEndTB(
  var AShiftDayRecord: TShiftDayRecord; ATimeBlockNumber: Integer);
var
  Day: Integer;
  SDay: String;
  Ready: Boolean;
  ADataSet: TDataSet;
begin
  Day:= DayInWeek(SystemDM.WeekStartsOn, AShiftDayRecord.ADate);
  SDay := IntToStr(Day);

  // Init the start and end
  AShiftDayRecord.AStart := Trunc(AShiftDayRecord.ADate);
  AShiftDayRecord.AEnd := Trunc(AShiftDayRecord.ADate);
  AShiftDayRecord.AValid := False;

  // Timeblocks Per Employee
  ADataSet := CalculateTotalHoursDM.ClientDataSetTBEmpl;
  ADataSet.Filtered := False;
  ADataSet.Filter :=
    'PLANT_CODE = ' + '''' + DoubleQuote(AShiftDayRecord.APlantCode) + '''' +
    ' AND ' +
    'EMPLOYEE_NUMBER = ' + IntToStr(AEmployeeNumber) + ' AND ' +
    'SHIFT_NUMBER = ' + IntToStr(AShiftDayRecord.AShiftNumber) + ' AND ' +
    'TIMEBLOCK_NUMBER = ' + IntToStr(ATimeBlockNumber);
  ADataSet.Filtered := True;
  Ready := not ADataSet.IsEmpty;

  // Timeblocks Per Department
  if not Ready then
  begin
    // Timeblocks per Department
    ADataSet := CalculateTotalHoursDM.ClientDataSetTBDept;
    ADataSet.Filtered := False;
    ADataSet.Filter :=
      'PLANT_CODE = ' + '''' + DoubleQuote(AShiftDayRecord.APlantCode) +
      '''' + ' AND ' +
      'DEPARTMENT_CODE = ''' + DoubleQuote(ADepartmentCode) +
      '''' + ' AND ' +
      'SHIFT_NUMBER = ' + IntToStr(AShiftDayRecord.AShiftNumber) + ' AND ' +
      'TIMEBLOCK_NUMBER = ' + IntToStr(ATimeBlockNumber);
    ADataSet.Filtered := True;
    Ready := not ADataSet.IsEmpty;

    // Timeblocks Per Shift
    if not Ready then
    begin
      // Timeblocks Per Shift
      ADataSet := CalculateTotalHoursDM.ClientDataSetTBShift;
      ADataSet.Filtered := False;
      ADataSet.Filter :=
        'PLANT_CODE = ' + '''' + DoubleQuote(AShiftDayRecord.APlantCode) +
        '''' + ' AND ' +
        'SHIFT_NUMBER = ' + IntToStr(AShiftDayRecord.AShiftNumber) + ' AND ' +
        'TIMEBLOCK_NUMBER = ' + IntToStr(ATimeBlockNumber);
      ADataSet.Filtered := True;
      Ready := not ADataSet.IsEmpty;
    end;
  end;

  if Ready then
  begin
    // Search and Set a valid STARTTIME
    // Start looking for FIRST valid timeblock to LAST.
    ADataSet.First;
    while not ADataSet.Eof do
    begin
      // If times are not the same, it's a valid time
      // otherwise they are not valid, like
      // Starttime='00:00' and endtime='00:00'
      if Frac(ADataSet.FieldByName('STARTTIME' + SDay).AsDateTime) <>
        Frac(ADataSet.FieldByName('ENDTIME' + SDay).AsDateTime) then
      begin
        AShiftDayRecord.AStart :=
          Trunc(AShiftDayRecord.ADate) +
          Frac(ADataSet.FieldByName('STARTTIME' + SDay).AsDateTime);
        AShiftDayRecord.AValid := True;
        Break;
      end;
      ADataSet.Next;
    end;
    // Search and Set a valid ENDDTIME
    // Start looking from LAST valid timeblock to FIRST.
    ADataSet.Last;
    while not ADataSet.Bof do
    begin
      // If times are not the same, it's a valid time
      // otherwise they are not valid, like
      // Starttime='00:00' and endtime='00:00'
      if Frac(ADataSet.FieldByName('STARTTIME' + SDay).AsDateTime) <>
        Frac(ADataSet.FieldByName('ENDTIME' + SDay).AsDateTime) then
      begin
        AShiftDayRecord.AEnd :=
          Trunc(AShiftDayRecord.ADate) +
          Frac(ADataSet.FieldByName('ENDTIME' + SDay).AsDateTime);
        begin
          AShiftDayRecord.AValid := True;
          Break;
        end;
      end;
      ADataSet.Prior;
    end;
  end;
end; // DetermineShiftStartEndTB

(*
function TProdMinClass.IsFirstScan(AEmployeeNumber: Integer;
  ADateTimeIn: TDateTime; ADeleteDateTimeIn: TDateTime=0): Boolean;
begin
  Result := False;
  with CalculateTotalHoursDM.qryIsFirstScan do
  begin
    Close;
    ParamByName('AEMPLOYEENUMBER').AsInteger := AEmployeeNumber;
    ParamByName('ADATETIMEIN').AsDateTime := ADateTimeIn;
    if ADeleteDateTimeIn = 0 then
      ParamByName('ADELETEACTION').AsInteger := 0
    else
      ParamByName('ADELETEACTION').AsInteger := 1;
    Open;
    if not Eof then
      Result := FieldByName('FIRSTSCAN').AsString = 'TRUE';
    Close;
  end;
end; // IsFirstScan

function TProdMinClass.IsLastScan(AEmployeeNumber: Integer;
  ADateTimeOut: TDateTime; ADeleteDateTimeIn: TDateTime=0): Boolean;
begin
  Result := False;
  with CalculateTotalHoursDM.qryIsLastScan do
  begin
    Close;
    ParamByName('AEMPLOYEENUMBER').AsInteger := AEmployeeNumber;
    ParamByName('ADATETIMEOUT').AsDateTime := ADateTimeOut;
    if ADeleteDateTimeIn = 0 then
      ParamByName('ADELETEACTION').AsInteger := 0
    else
      ParamByName('ADELETEACTION').AsInteger := 1;
    Open;
    if not Eof then
      Result := FieldByName('LASTSCAN').AsString = 'TRUE';
    Close;
  end;
end; // IsLastScan
*)

end.
