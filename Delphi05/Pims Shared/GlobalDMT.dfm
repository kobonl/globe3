object GlobalDM: TGlobalDM
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Left = 260
  Top = 107
  Height = 758
  Width = 1045
  object qrySalMin: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  NVL(SUM(S.SALARY_MINUTE), 0) SUMSALARYMINUTE'
      'FROM '
      '  SALARYHOURPEREMPLOYEE S '
      'WHERE '
      '  (S.SALARY_DATE >=:STARTDATE) AND '
      '  (S.SALARY_DATE <=:ENDDATE) AND '
      '  S.EMPLOYEE_NUMBER=:EMPNO')
    Left = 48
    Top = 8
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'ENDDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPNO'
        ParamType = ptUnknown
      end>
  end
  object qryContractgroup: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  C.PERIOD_STARTS_IN_WEEK,'
      '  C.OVERTIME_PER_DAY_WEEK_PERIOD,'
      '  C.WEEKS_IN_PERIOD,'
      '  C.WORKDAY_MO_YN,'
      '  C.WORKDAY_TU_YN,'
      '  C.WORKDAY_WE_YN,'
      '  C.WORKDAY_TH_YN,'
      '  C.WORKDAY_FR_YN,'
      '  C.WORKDAY_SA_YN,'
      '  C.WORKDAY_SU_YN,'
      '  C.NORMALHOURSPERDAY'
      'FROM'
      '  CONTRACTGROUP C'
      'WHERE'
      '  C.CONTRACTGROUP_CODE = :CONTRACTGROUP_CODE'
      ''
      ''
      ' '
      ' '
      ' ')
    Left = 168
    Top = 8
    ParamData = <
      item
        DataType = ftString
        Name = 'CONTRACTGROUP_CODE'
        ParamType = ptUnknown
      end>
  end
  object qrySalaryMinXXX: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  NVL(SUM(S.SALARY_MINUTE), 0) SUMSALARYMINUTE'
      'FROM'
      '  SALARYHOURPEREMPLOYEE S, HOURTYPE H'
      'WHERE'
      '  S.HOURTYPE_NUMBER = H.HOURTYPE_NUMBER AND'
      '  S.SALARY_DATE >= :STARTDATE AND S.SALARY_DATE <= :ENDDATE AND'
      '  S.EMPLOYEE_NUMBER = :EMPNO AND'
      '  H.IGNORE_FOR_OVERTIME_YN = :IGNORE_FOR_OVERTIME_YN'
      ' ')
    Left = 272
    Top = 8
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'ENDDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPNO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'IGNORE_FOR_OVERTIME_YN'
        ParamType = ptUnknown
      end>
  end
  object qryAbsenceMinXXX: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  NVL(SUM(AE.ABSENCE_MINUTE), 0) SUMABSENCEMINUTE'
      'FROM'
      '  ABSENCEHOURPEREMPLOYEE AE,'
      '  ABSENCEREASON AR, HOURTYPE H'
      'WHERE'
      '  AE.ABSENCEREASON_ID = AR.ABSENCEREASON_ID AND'
      '  AR.HOURTYPE_NUMBER = H.HOURTYPE_NUMBER AND'
      '  AE.ABSENCEHOUR_DATE >= :STARTDATE AND'
      '  AE.ABSENCEHOUR_DATE <= :ENDDATE AND'
      '  H.IGNORE_FOR_OVERTIME_YN = :IGNORE_FOR_OVERTIME_YN AND'
      '  AE.EMPLOYEE_NUMBER = :EMPNO'
      ' ')
    Left = 368
    Top = 8
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'ENDDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'IGNORE_FOR_OVERTIME_YN'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPNO'
        ParamType = ptUnknown
      end>
  end
  object qryOvertimeDef: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  C.TIME_FOR_TIME_YN, C.BONUS_IN_MONEY_YN,'
      '  O.HOURTYPE_NUMBER, O.STARTTIME, O.ENDTIME,'
      '  H.BONUS_PERCENTAGE, O.LINE_NUMBER,'
      '  H.OVERTIME_YN HT_OVERTIME_YN,'
      '  H.TIME_FOR_TIME_YN HT_TIME_FOR_TIME_YN,'
      '  H.BONUS_IN_MONEY_YN HT_BONUS_IN_MONEY_YN,'
      '  H.ADV_YN HT_ADV_YN,'
      '  O.FROMTIME, O.TOTIME,'
      '  O.DAY_OF_WEEK'
      'FROM'
      '  CONTRACTGROUP C,'
      '  OVERTIMEDEFINITION O, HOURTYPE H'
      'WHERE'
      '  C.CONTRACTGROUP_CODE = :CONTRACTGROUP_CODE AND'
      '  C.CONTRACTGROUP_CODE = O.CONTRACTGROUP_CODE AND'
      '  O.HOURTYPE_NUMBER = H.HOURTYPE_NUMBER AND'
      '  ('
      '    (O.DAY_OF_WEEK IS NULL)'
      '    OR'
      '    (O.DAY_OF_WEEK = :DAY_OF_WEEK)'
      '    OR'
      '    (:DAY_OF_WEEK = 0)'
      '  )'
      'ORDER BY'
      '  O.LINE_NUMBER'
      ' '
      ' '
      ' '
      ' '
      ' ')
    Left = 472
    Top = 8
    ParamData = <
      item
        DataType = ftString
        Name = 'CONTRACTGROUP_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'DAY_OF_WEEK'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'DAY_OF_WEEK'
        ParamType = ptUnknown
      end>
  end
  object AQuery: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    Left = 584
    Top = 8
  end
  object qryAbsTotTFT: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  EARNED_TFT_MINUTE'
      'FROM'
      '  ABSENCETOTAL'
      'WHERE'
      '  EMPLOYEE_NUMBER = :EMPNO AND'
      '  ABSENCE_YEAR = :AYEAR'
      ' ')
    Left = 48
    Top = 56
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPNO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'AYEAR'
        ParamType = ptUnknown
      end>
  end
  object qryAbsTotTFTUpdate: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'UPDATE ABSENCETOTAL'
      'SET EARNED_TFT_MINUTE = :OTIME,'
      'MUTATIONDATE=:MDATE, MUTATOR = :MUTATOR'
      'WHERE EMPLOYEE_NUMBER = :EMPNO AND'
      'ABSENCE_YEAR = :AYEAR'
      ' ')
    Left = 160
    Top = 56
    ParamData = <
      item
        DataType = ftFloat
        Name = 'OTIME'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'MDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MUTATOR'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPNO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'AYEAR'
        ParamType = ptUnknown
      end>
  end
  object qryAbsTotTFTInsert: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'INSERT INTO ABSENCETOTAL('
      'EMPLOYEE_NUMBER,ABSENCE_YEAR,'
      'EARNED_TFT_MINUTE,CREATIONDATE,'
      'MUTATIONDATE,MUTATOR) VALUES('
      ':EMPNO,:AYEAR,:OTIME,:CDATE,:MDATE, '
      ':MUTATOR)'
      ' ')
    Left = 272
    Top = 56
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPNO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'AYEAR'
        ParamType = ptUnknown
      end
      item
        DataType = ftFloat
        Name = 'OTIME'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'CDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'MDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MUTATOR'
        ParamType = ptUnknown
      end>
  end
  object qryExceptHourDef: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT DISTINCT'
      '  EHD.DAY_OF_WEEK,'
      '  EHD.STARTTIME,'
      '  EHD.ENDTIME,'
      '  EHD.HOURTYPE_NUMBER,'
      '  (SELECT NVL(H1.IGNORE_FOR_OVERTIME_YN, '#39'N'#39') FROM HOURTYPE H1'
      
        '   WHERE H1.HOURTYPE_NUMBER = EHD.HOURTYPE_NUMBER) HT_IGNORE_FOR' +
        '_OVERTIME_YN,'
      '  EHD.OVERTIME_HOURTYPE_NUMBER,'
      '  CASE'
      '    WHEN EHD.OVERTIME_HOURTYPE_NUMBER IS NOT NULL THEN'
      
        '      (SELECT NVL(H2.IGNORE_FOR_OVERTIME_YN, '#39'N'#39') FROM HOURTYPE ' +
        'H2'
      '       WHERE H2.HOURTYPE_NUMBER = EHD.OVERTIME_HOURTYPE_NUMBER)'
      '    ELSE'
      '      NULL'
      '  END OHT_IGNORE_FOR_OVERTIME_YN'
      'FROM'
      '  EXCEPTIONALHOURDEF EHD'
      'WHERE'
      '  ('
      '    EHD.CONTRACTGROUP_CODE = :CONTRACTGROUP_CODE'
      '  )'
      '  AND'
      '  ('
      '    ('
      '      (TRUNC(:START_TIME) = TRUNC(:END_TIME)) AND'
      '      (EHD.DAY_OF_WEEK = :DAY_OF_WEEK)'
      '    )'
      '    OR'
      '    ('
      '      (TRUNC(:START_TIME) <> TRUNC(:END_TIME)) AND'
      '      ((EHD.DAY_OF_WEEK = :WEEKDAYSTART)'
      '       OR (EHD.DAY_OF_WEEK = :WEEKDAYEND))'
      '    )'
      '  )'
      'ORDER BY 1, 2'
      ''
      ' '
      ' '
      ' ')
    Left = 48
    Top = 104
    ParamData = <
      item
        DataType = ftString
        Name = 'CONTRACTGROUP_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'START_TIME'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'END_TIME'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'DAY_OF_WEEK'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'START_TIME'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'END_TIME'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'WEEKDAYSTART'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'WEEKDAYEND'
        ParamType = ptUnknown
      end>
  end
  object qrySalary: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  S.SALARY_MINUTE'
      'FROM'
      '  SALARYHOURPEREMPLOYEE S'
      'WHERE'
      '  S.SALARY_DATE = :SALARY_DATE AND'
      '  S.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND'
      '  S.HOURTYPE_NUMBER = :HOURTYPE_NUMBER AND'
      '  S.MANUAL_YN = :MANUAL_YN '
      ' ')
    Left = 48
    Top = 152
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'SALARY_DATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'HOURTYPE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MANUAL_YN'
        ParamType = ptUnknown
      end>
  end
  object qrySalaryUpdate: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'UPDATE SALARYHOURPEREMPLOYEE'
      'SET SALARY_MINUTE=:SALARY_MINUTE,'
      'MUTATIONDATE=:MDATE,'
      'MUTATOR=:MUTATOR,'
      'FROMMANUALPROD_YN=:FROMMANUALPROD_YN'
      'WHERE SALARY_DATE=:SALARY_DATE AND'
      'EMPLOYEE_NUMBER=:EMPLOYEE_NUMBER AND'
      'HOURTYPE_NUMBER=:HOURTYPE_NUMBER AND'
      'MANUAL_YN=:MANUAL_YN '
      ' ')
    Left = 144
    Top = 152
    ParamData = <
      item
        DataType = ftInteger
        Name = 'SALARY_MINUTE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'MDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MUTATOR'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'FROMMANUALPROD_YN'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'SALARY_DATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'HOURTYPE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MANUAL_YN'
        ParamType = ptUnknown
      end>
  end
  object qrySalaryDelete: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'DELETE FROM SALARYHOURPEREMPLOYEE '
      'WHERE SALARY_DATE=:SALARY_DATE AND'
      'EMPLOYEE_NUMBER=:EMPLOYEE_NUMBER AND'
      'HOURTYPE_NUMBER=:HOURTYPE_NUMBER AND'
      'MANUAL_YN=:MANUAL_YN'
      ' ')
    Left = 240
    Top = 152
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'SALARY_DATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'HOURTYPE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MANUAL_YN'
        ParamType = ptUnknown
      end>
  end
  object qrySalaryInsert: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'INSERT INTO SALARYHOURPEREMPLOYEE'
      '(SALARY_DATE,EMPLOYEE_NUMBER,HOURTYPE_NUMBER,'
      'SALARY_MINUTE,MANUAL_YN,CREATIONDATE,MUTATIONDATE,'
      'EXPORTED_YN,MUTATOR, FROMMANUALPROD_YN)'
      'VALUES(:SALARY_DATE,:EMPLOYEE_NUMBER,:HOURTYPE_NUMBER,'
      ':SALARY_MINUTE,:MANUAL_YN,:CREATIONDATE,:MUTATIONDATE,'
      ':EXPORTED_YN,:MUTATOR, :FROMMANUALPROD_YN)'
      ' ')
    Left = 336
    Top = 152
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'SALARY_DATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'HOURTYPE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'SALARY_MINUTE'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'MANUAL_YN'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'CREATIONDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'MUTATIONDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'EXPORTED_YN'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'MUTATOR'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'FROMMANUALPROD_YN'
        ParamType = ptUnknown
      end>
  end
  object qryWSHourtype: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  W.HOURTYPE_NUMBER'
      'FROM'
      '  WORKSPOT W'
      'WHERE'
      '  W.PLANT_CODE = :PCODE AND'
      '  W.WORKSPOT_CODE = :WCODE'
      ' ')
    Left = 48
    Top = 200
    ParamData = <
      item
        DataType = ftString
        Name = 'PCODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'WCODE'
        ParamType = ptUnknown
      end>
  end
  object qryProdHourPEPT: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  PRODUCTION_MINUTE,'
      '  PROD_MIN_EXCL_BREAK'
      'FROM'
      '  PRODHOURPEREMPLPERTYPE'
      'WHERE'
      '  PRODHOUREMPLOYEE_DATE=:PDATE AND'
      '  PLANT_CODE=:PCODE AND'
      '  SHIFT_NUMBER=:SHNO AND'
      '  EMPLOYEE_NUMBER=:EMPNO AND'
      '  WORKSPOT_CODE=:WCODE AND'
      '  JOB_CODE=:JCODE AND'
      '  MANUAL_YN=:MAN AND'
      '  HOURTYPE_NUMBER =:HOURTYPE'
      ''
      ''
      ''
      ' '
      ' ')
    Left = 48
    Top = 248
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'PDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PCODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'SHNO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPNO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'WCODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'JCODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MAN'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'HOURTYPE'
        ParamType = ptUnknown
      end>
  end
  object qryProdHourPEPTUpdate: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'UPDATE PRODHOURPEREMPLPERTYPE'
      'SET PRODUCTION_MINUTE = :PMIN,'
      'PROD_MIN_EXCL_BREAK = :PROD_MIN_EXCL_BREAK,'
      'MUTATIONDATE = :MDATE,'
      'MUTATOR = :MUTATOR'
      'WHERE PRODHOUREMPLOYEE_DATE=:PDATE AND'
      'PLANT_CODE=:PCODE AND'
      'SHIFT_NUMBER=:SHNO AND'
      'EMPLOYEE_NUMBER=:EMPNO AND'
      'WORKSPOT_CODE=:WCODE AND'
      'JOB_CODE=:JCODE AND'
      'MANUAL_YN=:MAN AND'
      'HOURTYPE_NUMBER =:HOURTYPE'
      ' '
      ' '
      ' '
      ' ')
    Left = 168
    Top = 248
    ParamData = <
      item
        DataType = ftInteger
        Name = 'PMIN'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'PROD_MIN_EXCL_BREAK'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'MDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MUTATOR'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'PDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PCODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'SHNO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPNO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'WCODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'JCODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MAN'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'HOURTYPE'
        ParamType = ptUnknown
      end>
  end
  object qryProdHourPEPTInsert: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'INSERT INTO PRODHOURPEREMPLPERTYPE('
      'PRODHOUREMPLOYEE_DATE,PLANT_CODE,'
      'SHIFT_NUMBER,EMPLOYEE_NUMBER,'
      'WORKSPOT_CODE,JOB_CODE,'
      'PRODUCTION_MINUTE, PROD_MIN_EXCL_BREAK,'
      'HOURTYPE_NUMBER, MANUAL_YN,'
      'CREATIONDATE,MUTATIONDATE,MUTATOR)'
      'VALUES(:PDATE,:PCODE,:SHNO,:EMPNO,'
      ':WCODE,:JCODE,:PMIN,:PROD_MIN_EXCL_BREAK,'
      ':HOURTYPE, :MAN,'
      ':CDATE,:MDATE,:MUTATOR)'
      ' '
      ' '
      ' '
      ' ')
    Left = 304
    Top = 248
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'PDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'PCODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'SHNO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPNO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'WCODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'JCODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'PMIN'
        ParamType = ptUnknown
      end
      item
        DataType = ftInterface
        Name = 'PROD_MIN_EXCL_BREAK'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'HOURTYPE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MAN'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'CDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'MDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MUTATOR'
        ParamType = ptUnknown
      end>
  end
  object qryProdHour: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  P.PRODUCTION_MINUTE,'
      '  P.PAYED_BREAK_MINUTE'
      'FROM'
      '  PRODHOURPEREMPLOYEE P'
      'WHERE'
      '  P.PRODHOUREMPLOYEE_DATE = :PDATE AND'
      '  P.PLANT_CODE = :PCODE AND'
      '  P.SHIFT_NUMBER = :SHNO AND'
      '  P.EMPLOYEE_NUMBER = :EMPNO AND'
      '  P.WORKSPOT_CODE = :WCODE AND'
      '  P.JOB_CODE = :JCODE AND'
      '  P.MANUAL_YN = :MAN'
      ' '
      ' '
      ' '
      ' '
      ' ')
    Left = 48
    Top = 296
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'PDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PCODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'SHNO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPNO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'WCODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'JCODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MAN'
        ParamType = ptUnknown
      end>
  end
  object qryProdHourDelete: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'DELETE FROM PRODHOURPEREMPLOYEE'
      'WHERE PRODHOUREMPLOYEE_DATE = :PDATE AND'
      'PLANT_CODE = :PCODE AND'
      'SHIFT_NUMBER = :SHNO AND'
      'EMPLOYEE_NUMBER = :EMPNO AND'
      'WORKSPOT_CODE = :WCODE AND'
      'JOB_CODE = :JCODE AND'
      'MANUAL_YN = :MAN'
      ' '
      ' '
      ' ')
    Left = 152
    Top = 296
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'PDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PCODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'SHNO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPNO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'WCODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'JCODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MAN'
        ParamType = ptUnknown
      end>
  end
  object qryProdHourUpdate: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'UPDATE PRODHOURPEREMPLOYEE'
      'SET PRODUCTION_MINUTE = :PMIN,'
      'PAYED_BREAK_MINUTE = :PAIDBREAK,'
      'MUTATOR = :MUTATOR,'
      'MUTATIONDATE = :MUTATIONDATE'
      'WHERE PRODHOUREMPLOYEE_DATE = :PDATE AND'
      'PLANT_CODE = :PCODE AND'
      'SHIFT_NUMBER = :SHNO AND'
      'EMPLOYEE_NUMBER = :EMPNO AND'
      'WORKSPOT_CODE = :WCODE AND'
      'JOB_CODE = :JCODE AND'
      'MANUAL_YN = :MAN '
      ' '
      ' '
      ' ')
    Left = 264
    Top = 296
    ParamData = <
      item
        DataType = ftInteger
        Name = 'PMIN'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'PAIDBREAK'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MUTATOR'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'MUTATIONDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'PDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PCODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'SHNO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPNO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'WCODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'JCODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MAN'
        ParamType = ptUnknown
      end>
  end
  object qryProdHourInsert: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'INSERT INTO PRODHOURPEREMPLOYEE('
      'PRODHOUREMPLOYEE_DATE,'
      'PLANT_CODE,'
      'SHIFT_NUMBER, EMPLOYEE_NUMBER,'
      'WORKSPOT_CODE, JOB_CODE,'
      'PRODUCTION_MINUTE,'
      'MANUAL_YN, PAYED_BREAK_MINUTE,'
      'CREATIONDATE, MUTATIONDATE, MUTATOR)'
      'VALUES(:PDATE, :PCODE, :SHNO, :EMPNO,'
      ':WCODE, :JCODE, :PMIN, :MAN, :PAYEDBREAK,'
      ':CDATE, :MDATE, :MUTATOR)'
      ' '
      ' '
      ' '
      ' ')
    Left = 368
    Top = 296
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'PDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PCODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'SHNO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPNO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'WCODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'JCODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'PMIN'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MAN'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'PAYEDBREAK'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'CDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'MDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'MUTATOR'
        ParamType = ptUnknown
      end>
  end
  object qryWSPerEmp: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  W.EMPLOYEE_LEVEL'
      'FROM'
      '  WORKSPOTSPEREMPLOYEE W'
      'WHERE'
      '  W.EMPLOYEE_NUMBER = :EMPNO AND W.PLANT_CODE = :PCODE AND'
      '  W.DEPARTMENT_CODE = :DCODE AND W.WORKSPOT_CODE = :WCODE'
      ' ')
    Left = 48
    Top = 344
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPNO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PCODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'DCODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'WCODE'
        ParamType = ptUnknown
      end>
  end
  object qryWSPerEmpUpdate: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'UPDATE WORKSPOTSPEREMPLOYEE'
      'SET EMPLOYEE_LEVEL=:EMPLEVEL,'
      'STARTDATE_LEVEL=:STLDATE, MUTATOR=:MUT,'
      'MUTATIONDATE=:MUTDATE'
      'WHERE EMPLOYEE_NUMBER=:EMPNO AND'
      'PLANT_CODE=:PCODE AND'
      'DEPARTMENT_CODE=:DCODE AND WORKSPOT_CODE=:WCODE'
      ' ')
    Left = 152
    Top = 344
    ParamData = <
      item
        DataType = ftString
        Name = 'EMPLEVEL'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STLDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MUT'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'MUTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPNO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'PCODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'DCODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'WCODE'
        ParamType = ptUnknown
      end>
  end
  object qryWSPerEmpInsert: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'INSERT INTO WORKSPOTSPEREMPLOYEE(EMPLOYEE_NUMBER,'
      'PLANT_CODE, EMPLOYEE_LEVEL, DEPARTMENT_CODE,'
      'CREATIONDATE, WORKSPOT_CODE, MUTATIONDATE, MUTATOR,'
      'STARTDATE_LEVEL) VALUES('
      ':EMPLNO, :PCODE, :EMPLEVEL, :DCODE, :CDATE, :WCODE,'
      ':MDATE, :MUT, :STLEVDATE)'
      ' ')
    Left = 264
    Top = 344
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLNO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PCODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'EMPLEVEL'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'DCODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'CDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'WCODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'MDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MUT'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STLEVDATE'
        ParamType = ptUnknown
      end>
  end
  object qryExcepBFOvertime: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  C.EXCEPTIONAL_BEFORE_OVERTIME'
      'FROM'
      '  EMPLOYEE E, CONTRACTGROUP C'
      'WHERE'
      '  E.CONTRACTGROUP_CODE = C.CONTRACTGROUP_CODE AND'
      '  E.EMPLOYEE_NUMBER = :EMPNO'
      ' ')
    Left = 48
    Top = 392
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPNO'
        ParamType = ptUnknown
      end>
  end
  object qryProdHourPEPTDelete: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'DELETE FROM PRODHOURPEREMPLPERTYPE'
      'WHERE PRODHOUREMPLOYEE_DATE>=:FROMDATE AND'
      'PRODHOUREMPLOYEE_DATE<=:TODATE AND'
      'EMPLOYEE_NUMBER=:EMPNO AND'
      'MANUAL_YN=:MAN '
      ' ')
    Left = 440
    Top = 248
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'FROMDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'TODATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPNO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MAN'
        ParamType = ptUnknown
      end>
  end
  object qryEmpAvailUpdate1: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'UPDATE EMPLOYEEAVAILABILITY'
      'SET AVAILABLE_TIMEBLOCK_1 = :NEWAVCODE,'
      'MUTATIONDATE = SYSDATE,'
      'MUTATOR = :MUTATOR'
      'WHERE'
      '('
      '  (EMPLOYEEAVAILABILITY_DATE >= :STDATE AND :MORE = 1) OR'
      '  (EMPLOYEEAVAILABILITY_DATE = :STDATE AND :MORE = 0)'
      ') AND'
      'EMPLOYEE_NUMBER = :EMPNO'
      'AND AVAILABLE_TIMEBLOCK_1 =:AVCODE'
      ' '
      ' ')
    Left = 216
    Top = 392
    ParamData = <
      item
        DataType = ftString
        Name = 'NEWAVCODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MUTATOR'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'MORE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'MORE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPNO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'AVCODE'
        ParamType = ptUnknown
      end>
  end
  object qryEmpAvailUpdate2: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'UPDATE EMPLOYEEAVAILABILITY'
      'SET AVAILABLE_TIMEBLOCK_2 = :NEWAVCODE,'
      'MUTATIONDATE = SYSDATE,'
      'MUTATOR = :MUTATOR'
      'WHERE'
      '('
      '  (EMPLOYEEAVAILABILITY_DATE >= :STDATE AND :MORE = 1) OR'
      '  (EMPLOYEEAVAILABILITY_DATE = :STDATE AND :MORE = 0)'
      ') AND'
      'EMPLOYEE_NUMBER = :EMPNO'
      'AND AVAILABLE_TIMEBLOCK_2 =:AVCODE'
      ' '
      ' ')
    Left = 312
    Top = 392
    ParamData = <
      item
        DataType = ftString
        Name = 'NEWAVCODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MUTATOR'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'MORE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'MORE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPNO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'AVCODE'
        ParamType = ptUnknown
      end>
  end
  object qryEmpAvailUpdate3: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'UPDATE EMPLOYEEAVAILABILITY'
      'SET AVAILABLE_TIMEBLOCK_3  = :NEWAVCODE,'
      'MUTATIONDATE = SYSDATE,'
      'MUTATOR = :MUTATOR'
      'WHERE'
      '('
      '  (EMPLOYEEAVAILABILITY_DATE >= :STDATE AND :MORE = 1) OR'
      '  (EMPLOYEEAVAILABILITY_DATE = :STDATE AND :MORE = 0)'
      ') AND'
      'EMPLOYEE_NUMBER = :EMPNO'
      'AND AVAILABLE_TIMEBLOCK_3 =:AVCODE'
      ' '
      ' ')
    Left = 424
    Top = 392
    ParamData = <
      item
        DataType = ftString
        Name = 'NEWAVCODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MUTATOR'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'MORE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'MORE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPNO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'AVCODE'
        ParamType = ptUnknown
      end>
  end
  object qryEmpAvailUpdate4: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'UPDATE EMPLOYEEAVAILABILITY'
      'SET AVAILABLE_TIMEBLOCK_4 = :NEWAVCODE,'
      'MUTATIONDATE = SYSDATE,'
      'MUTATOR = :MUTATOR'
      'WHERE'
      '('
      '  (EMPLOYEEAVAILABILITY_DATE >= :STDATE AND :MORE = 1) OR'
      '  (EMPLOYEEAVAILABILITY_DATE = :STDATE AND :MORE = 0)'
      ') AND'
      'EMPLOYEE_NUMBER = :EMPNO'
      'AND AVAILABLE_TIMEBLOCK_4 =:AVCODE'
      ' '
      ' ')
    Left = 528
    Top = 392
    ParamData = <
      item
        DataType = ftString
        Name = 'NEWAVCODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MUTATOR'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'MORE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'MORE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPNO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'AVCODE'
        ParamType = ptUnknown
      end>
  end
  object qryIllnessMsg: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  ILLNESSMESSAGE_STARTDATE, ABSENCEREASON_CODE'
      'FROM'
      '  ILLNESSMESSAGE'
      'WHERE'
      '  EMPLOYEE_NUMBER = :EMPNO'
      '  AND PROCESSED_YN = :MAN'
      ' ')
    Left = 48
    Top = 440
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPNO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MAN'
        ParamType = ptUnknown
      end>
  end
  object qryIllnessMsgUpdate: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'UPDATE ILLNESSMESSAGE'
      'SET ILLNESSMESSAGE_ENDDATE = :EDATE,'
      'PROCESSED_YN = :MAN,'
      'MUTATOR =:MUTATOR, MUTATIONDATE =:MUTATIONDATE'
      'WHERE EMPLOYEE_NUMBER = :EMPNO AND'
      'ILLNESSMESSAGE_STARTDATE = :SDATE '
      ' ')
    Left = 144
    Top = 440
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'EDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MAN'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MUTATOR'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'MUTATIONDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPNO'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'SDATE'
        ParamType = ptUnknown
      end>
  end
  object qryPopIDCard: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  E.DEPARTMENT_CODE, E.CUT_OF_TIME_YN,'
      '  P.INSCAN_MARGIN_EARLY, P.INSCAN_MARGIN_LATE,'
      '  P.OUTSCAN_MARGIN_EARLY, P.OUTSCAN_MARGIN_LATE,'
      '  E.CONTRACTGROUP_CODE, CG.EXCEPTIONAL_BEFORE_OVERTIME,'
      '  CG.ROUND_TRUNC_SALARY_HOURS, CG.ROUND_MINUTE'
      'FROM'
      '  EMPLOYEE E INNER JOIN PLANT P ON'
      '    E.PLANT_CODE = P.PLANT_CODE'
      '  INNER JOIN CONTRACTGROUP CG ON'
      '    E.CONTRACTGROUP_CODE = CG.CONTRACTGROUP_CODE'
      'WHERE'
      '  E.EMPLOYEE_NUMBER = :EMPNO'
      ' '
      ' '
      ' ')
    Left = 48
    Top = 488
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPNO'
        ParamType = ptUnknown
      end>
  end
  object qryProdHourPEPTDeleteOneRec: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'DELETE FROM PRODHOURPEREMPLPERTYPE'
      'WHERE PRODHOUREMPLOYEE_DATE=:PDATE AND'
      'PLANT_CODE=:PCODE AND'
      'SHIFT_NUMBER=:SHNO AND'
      'EMPLOYEE_NUMBER=:EMPNO AND'
      'WORKSPOT_CODE=:WCODE AND'
      'JOB_CODE=:JCODE AND'
      'MANUAL_YN=:MAN AND'
      'HOURTYPE_NUMBER =:HOURTYPE')
    Left = 544
    Top = 296
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'PDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PCODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'SHNO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPNO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'WCODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'JCODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MAN'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'HOURTYPE'
        ParamType = ptUnknown
      end>
  end
  object qrySalaryAbsenceMin: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '('
      '  SELECT'
      '    NVL(SUM(S.SALARY_MINUTE), 0) SAL_MINUTES'
      '  FROM'
      '    SALARYHOURPEREMPLOYEE S, HOURTYPE H'
      '  WHERE'
      '    S.HOURTYPE_NUMBER = H.HOURTYPE_NUMBER AND'
      '    S.SALARY_DATE >= :STARTDATE AND'
      '    S.SALARY_DATE <= :ENDDATE AND'
      '    S.EMPLOYEE_NUMBER = :EMPNO AND'
      '    H.IGNORE_FOR_OVERTIME_YN = '#39'N'#39
      ')'
      '+'
      '('
      '  SELECT'
      '    NVL(SUM(AE.ABSENCE_MINUTE), 0)'
      '  FROM'
      '    ABSENCEHOURPEREMPLOYEE AE, ABSENCEREASON AR, HOURTYPE H'
      '  WHERE'
      '    AE.ABSENCEREASON_ID = AR.ABSENCEREASON_ID AND'
      '    AR.HOURTYPE_NUMBER = H.HOURTYPE_NUMBER AND'
      '    AE.ABSENCEHOUR_DATE >= :STARTDATE AND'
      '    AE.ABSENCEHOUR_DATE <= :ENDDATE AND'
      '    AE.EMPLOYEE_NUMBER = :EMPNO AND'
      '    H.IGNORE_FOR_OVERTIME_YN = '#39'N'#39
      ') SUMSALARYMINUTE'
      'FROM DUAL'
      ''
      ' '
      ' ')
    Left = 472
    Top = 120
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'ENDDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPNO'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'ENDDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPNO'
        ParamType = ptUnknown
      end>
  end
  object qrySalaryOvertimeMin: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  NVL(SUM(S.SALARY_MINUTE), 0) SUMSALARYOVERTIMEMIN'
      'FROM'
      '  SALARYHOURPEREMPLOYEE S INNER JOIN HOURTYPE H ON'
      '    S.HOURTYPE_NUMBER = H.HOURTYPE_NUMBER'
      '  INNER JOIN EMPLOYEE E ON'
      '    E.EMPLOYEE_NUMBER = S.EMPLOYEE_NUMBER'
      '  INNER JOIN CONTRACTGROUP C ON'
      '    E.CONTRACTGROUP_CODE = C.CONTRACTGROUP_CODE'
      '  INNER JOIN OVERTIMEDEFINITION O ON'
      '    O.CONTRACTGROUP_CODE = C.CONTRACTGROUP_CODE AND'
      '    O.HOURTYPE_NUMBER = S.HOURTYPE_NUMBER'
      'WHERE'
      '  S.SALARY_DATE >= :STARTDATE AND S.SALARY_DATE <= :ENDDATE AND'
      '  S.EMPLOYEE_NUMBER = :EMPNO AND'
      '  H.IGNORE_FOR_OVERTIME_YN = '#39'N'#39
      ''
      ' '
      ' '
      ' '
      ' ')
    Left = 472
    Top = 168
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'ENDDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPNO'
        ParamType = ptUnknown
      end>
  end
  object qrySHE: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  SHE.SALARY_DATE,'
      '  SHE.HOURTYPE_NUMBER,'
      '  SHE.SALARY_MINUTE'
      'FROM'
      '  SALARYHOURPEREMPLOYEE SHE'
      'WHERE'
      '  SHE.SALARY_DATE = :SALARY_DATE AND'
      '  SHE.MANUAL_YN = '#39'N'#39' AND'
      '  SHE.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND'
      
        ' ((:HOURTYPE_NUMBER = -1) OR (SHE.HOURTYPE_NUMBER = :HOURTYPE_NU' +
        'MBER))'
      'ORDER BY'
      '  SHE.HOURTYPE_NUMBER DESC'
      ''
      ' '
      ' '
      ' ')
    Left = 144
    Top = 496
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'SALARY_DATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'HOURTYPE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'HOURTYPE_NUMBER'
        ParamType = ptUnknown
      end>
  end
  object qryPHEPT: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  P.PRODHOUREMPLOYEE_DATE,'
      '  P.PLANT_CODE,'
      '  P.SHIFT_NUMBER,'
      '  P.EMPLOYEE_NUMBER,'
      '  P.WORKSPOT_CODE,'
      '  P.JOB_CODE,'
      '  P.HOURTYPE_NUMBER,'
      '  P.MANUAL_YN,'
      '  P.PRODUCTION_MINUTE'
      'FROM'
      '  PRODHOURPEREMPLPERTYPE P'
      'WHERE'
      '  P.PRODHOUREMPLOYEE_DATE = :PRODHOUREMPLOYEE_DATE AND'
      '  P.MANUAL_YN = '#39'N'#39' AND'
      '  P.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER'
      ' '
      ' '
      ' '
      ' ')
    Left = 208
    Top = 496
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'PRODHOUREMPLOYEE_DATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end>
  end
  object qryPHEPTUpdate: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'UPDATE PRODHOURPEREMPLPERTYPE'
      'SET PRODUCTION_MINUTE = :PMIN,'
      'MUTATIONDATE = :MDATE,'
      'MUTATOR = :MUTATOR'
      'WHERE PRODHOUREMPLOYEE_DATE=:PDATE AND'
      'PLANT_CODE=:PCODE AND'
      'SHIFT_NUMBER=:SHNO AND'
      'EMPLOYEE_NUMBER=:EMPNO AND'
      'WORKSPOT_CODE=:WCODE AND'
      'JOB_CODE=:JCODE AND'
      'MANUAL_YN=:MAN AND'
      'HOURTYPE_NUMBER =:HOURTYPE'
      ''
      ' '
      ' '
      ' '
      ' '
      ' ')
    Left = 280
    Top = 496
    ParamData = <
      item
        DataType = ftInteger
        Name = 'PMIN'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'MDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MUTATOR'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'PDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PCODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'SHNO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPNO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'WCODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'JCODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MAN'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'HOURTYPE'
        ParamType = ptUnknown
      end>
  end
  object qryPEPT: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  P.PRODHOUREMPLOYEE_DATE,'
      '  P.PLANT_CODE,'
      '  P.SHIFT_NUMBER,'
      '  P.EMPLOYEE_NUMBER,'
      '  P.WORKSPOT_CODE,'
      '  P.JOB_CODE,'
      '  P.HOURTYPE_NUMBER,'
      '  P.MANUAL_YN,'
      '  P.PRODUCTION_MINUTE,'
      '  P.PROD_MIN_EXCL_BREAK'
      'FROM'
      '  PRODHOURPEREMPLPERTYPE P'
      'WHERE'
      '  P.PRODHOUREMPLOYEE_DATE = :PRODHOUREMPLOYEE_DATE AND'
      '  P.MANUAL_YN = :MANUAL_YN AND'
      '  P.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND'
      '  P.HOURTYPE_NUMBER = :HOURTYPE_NUMBER'
      'ORDER BY'
      '  P.PRODUCTION_MINUTE DESC'
      '  '
      ''
      ''
      ' '
      ' '
      ' '
      ' '
      ' '
      ' ')
    Left = 376
    Top = 496
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'PRODHOUREMPLOYEE_DATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MANUAL_YN'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'HOURTYPE_NUMBER'
        ParamType = ptUnknown
      end>
  end
  object qryPEPTEB: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  P.PRODHOUREMPLOYEE_DATE,'
      '  P.PLANT_CODE,'
      '  P.SHIFT_NUMBER,'
      '  P.EMPLOYEE_NUMBER,'
      '  P.WORKSPOT_CODE,'
      '  P.JOB_CODE,'
      '  P.HOURTYPE_NUMBER,'
      '  P.MANUAL_YN,'
      '  P.PRODUCTION_MINUTE,'
      '  P.PROD_MIN_EXCL_BREAK'
      'FROM'
      '  PRODHOURPEREMPLPERTYPE P'
      'WHERE'
      '  P.PRODHOUREMPLOYEE_DATE >= :DATEFROM AND'
      '  P.PRODHOUREMPLOYEE_DATE <= :DATETO AND'
      '  P.MANUAL_YN = :MANUAL_YN AND'
      '  P.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER'
      'ORDER BY'
      '  P.PRODHOUREMPLOYEE_DATE DESC'
      ' '
      ' ')
    Left = 480
    Top = 496
    ParamData = <
      item
        DataType = ftDate
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDate
        Name = 'DATETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MANUAL_YN'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end>
  end
  object qryPEPTEBUpdate: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'UPDATE PRODHOURPEREMPLPERTYPE'
      'SET PROD_MIN_EXCL_BREAK = :PMIN,'
      'MUTATIONDATE = :MDATE,'
      'MUTATOR = :MUTATOR'
      'WHERE PRODHOUREMPLOYEE_DATE=:PDATE AND'
      'PLANT_CODE=:PCODE AND'
      'SHIFT_NUMBER=:SHNO AND'
      'EMPLOYEE_NUMBER=:EMPNO AND'
      'WORKSPOT_CODE=:WCODE AND'
      'JOB_CODE=:JCODE AND'
      'MANUAL_YN=:MAN AND'
      'HOURTYPE_NUMBER =:HOURTYPE'
      ''
      ' '
      ' '
      ' '
      ' '
      ' '
      ' '
      ' ')
    Left = 568
    Top = 496
    ParamData = <
      item
        DataType = ftInteger
        Name = 'PMIN'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'MDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MUTATOR'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'PDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PCODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'SHNO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPNO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'WCODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'JCODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MAN'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'HOURTYPE'
        ParamType = ptUnknown
      end>
  end
  object qryGetTFTHours: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  :CONTRACTGROUPTFT AS CGTFT_YN, H.HOURTYPE_NUMBER,'
      '  H.BONUS_IN_MONEY_YN AS HT_BONUS_IN_MONEY_YN,'
      '  C.BONUS_IN_MONEY_YN,'
      '  H.BONUS_PERCENTAGE, SUM(SHE.SALARY_MINUTE) AS SUMMINUTE'
      'FROM'
      '  SALARYHOURPEREMPLOYEE SHE INNER JOIN HOURTYPE H ON'
      '  SHE.HOURTYPE_NUMBER = H.HOURTYPE_NUMBER'
      '  INNER JOIN EMPLOYEE E ON'
      '  SHE.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER'
      '  INNER JOIN CONTRACTGROUP C ON'
      '  E.CONTRACTGROUP_CODE = C.CONTRACTGROUP_CODE'
      'WHERE'
      '  SHE.EMPLOYEE_NUMBER = :EMPNO AND'
      '  SHE.SALARY_DATE >= :DATEFROM AND'
      '  SHE.SALARY_DATE <= :DATETO AND'
      '  (H.OVERTIME_YN = '#39'Y'#39') AND'
      '  ('
      '    (C.TIME_FOR_TIME_YN = '#39'Y'#39' AND :CONTRACTGROUPTFT = '#39'Y'#39') OR'
      '    (H.TIME_FOR_TIME_YN = '#39'Y'#39' AND :CONTRACTGROUPTFT = '#39'N'#39')'
      '   )'
      
        'GROUP BY :CONTRACTGROUPTFT, H.HOURTYPE_NUMBER, H.BONUS_IN_MONEY_' +
        'YN,'
      '  C.BONUS_IN_MONEY_YN, H.BONUS_PERCENTAGE'
      'ORDER BY :CONTRACTGROUPTFT, H.HOURTYPE_NUMBER'
      ''
      ' '
      ' '
      ' ')
    Left = 576
    Top = 120
    ParamData = <
      item
        DataType = ftString
        Name = 'CONTRACTGROUPTFT'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPNO'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'CONTRACTGROUPTFT'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'CONTRACTGROUPTFT'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'CONTRACTGROUPTFT'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'CONTRACTGROUPTFT'
        ParamType = ptUnknown
      end>
  end
  object qryCheckEmpTFT: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  COUNT(*) AS CSUM'
      'FROM'
      '  EMPLOYEE E INNER JOIN CONTRACTGROUP C ON'
      '    E.CONTRACTGROUP_CODE = C.CONTRACTGROUP_CODE'
      'WHERE'
      '  E.EMPLOYEE_NUMBER = :EMPNO AND'
      '  C.TIME_FOR_TIME_YN = :TFT_YN'
      ' '
      ' ')
    Left = 576
    Top = 176
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPNO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'TFT_YN'
        ParamType = ptUnknown
      end>
  end
  object qryWorkspot: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  W.DEPARTMENT_CODE, H.IGNORE_FOR_OVERTIME_YN, '
      '  NVL(W.HOURTYPE_NUMBER, -1) HOURTYPE_NUMBER'
      'FROM'
      '  WORKSPOT W LEFT OUTER JOIN HOURTYPE H ON'
      '    W.HOURTYPE_NUMBER = H.HOURTYPE_NUMBER'
      'WHERE'
      '  W.PLANT_CODE = :PLANT_CODE AND'
      '  W.WORKSPOT_CODE = :WORKSPOT_CODE'
      ''
      ' ')
    Left = 48
    Top = 552
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'WORKSPOT_CODE'
        ParamType = ptUnknown
      end>
  end
  object qryUPSGetTFTBonus: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  C.TIME_FOR_TIME_YN, C.BONUS_IN_MONEY_YN'
      'FROM'
      '  CONTRACTGROUP C, EMPLOYEE E'
      'WHERE'
      '  E.EMPLOYEE_NUMBER = :EMPNO AND'
      '  C.CONTRACTGROUP_CODE = E.CONTRACTGROUP_CODE')
    Left = 144
    Top = 552
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPNO'
        ParamType = ptUnknown
      end>
  end
  object qryUPSDeleteAbsenceHours: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  S.HOURTYPE_NUMBER, S.SALARY_MINUTE, S.SALARY_DATE,'
      '  H.OVERTIME_YN, H.TIME_FOR_TIME_YN, H.BONUS_IN_MONEY_YN,'
      '  H.BONUS_PERCENTAGE'
      'FROM'
      '  SALARYHOURPEREMPLOYEE S INNER JOIN HOURTYPE H ON'
      '    S.HOURTYPE_NUMBER = H.HOURTYPE_NUMBER'
      'WHERE'
      '  S.SALARY_DATE >= :FROMDATE AND'
      '  S.SALARY_DATE <= :TODATE AND'
      '  S.EMPLOYEE_NUMBER = :EMPNO AND'
      '  S.MANUAL_YN = :MANUAL AND'
      '  H.HOURTYPE_NUMBER <> 1'
      ''
      ' '
      ' ')
    Left = 272
    Top = 552
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'FROMDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'TODATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPNO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MANUAL'
        ParamType = ptUnknown
      end>
  end
  object qryUPSDeleteSalary: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'DELETE FROM SALARYHOURPEREMPLOYEE'
      'WHERE SALARY_DATE >=:FROMDATE AND'
      'SALARY_DATE <=:TODATE AND'
      'EMPLOYEE_NUMBER=:EMPNO AND'
      'MANUAL_YN=:MANUAL')
    Left = 496
    Top = 552
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'FROMDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'TODATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPNO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MANUAL'
        ParamType = ptUnknown
      end>
  end
  object qryUPSDeleteProduction: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'DELETE FROM PRODHOURPEREMPLOYEE'
      'WHERE PRODHOUREMPLOYEE_DATE >= :FROMDATE AND'
      'PRODHOUREMPLOYEE_DATE <= :TODATE AND'
      'EMPLOYEE_NUMBER = :EMPNO AND'
      'MANUAL_YN = :MANUAL'
      ' '
      ' '
      ' ')
    Left = 616
    Top = 552
    ParamData = <
      item
        DataType = ftDate
        Name = 'FROMDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDate
        Name = 'TODATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPNO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MANUAL'
        ParamType = ptUnknown
      end>
  end
  object qryWork: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    Left = 144
    Top = 608
  end
  object qryContractGroupTFT: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT CG.TIME_FOR_TIME_YN, CG.BONUS_IN_MONEY_YN'
      'FROM CONTRACTGROUP CG'
      'WHERE CG.TIME_FOR_TIME_YN = '#39'Y'#39' OR'
      'CG.BONUS_IN_MONEY_YN = '#39'Y'#39
      ''
      ' '
      ' ')
    Left = 48
    Top = 608
  end
  object cdsSHEprio: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 352
    Top = 608
    object cdsSHEprioSALARY_DATE: TDateTimeField
      FieldName = 'SALARY_DATE'
    end
    object cdsSHEprioEMPLOYEE_NUMBER: TIntegerField
      FieldName = 'EMPLOYEE_NUMBER'
    end
    object cdsSHEprioHOURTYPE_NUMBER: TIntegerField
      FieldName = 'HOURTYPE_NUMBER'
    end
    object cdsSHEprioMANUAL_YN: TStringField
      FieldName = 'MANUAL_YN'
    end
    object cdsSHEprioSALARY_MINUTE: TIntegerField
      FieldName = 'SALARY_MINUTE'
    end
    object cdsSHEprioPRIORITY: TIntegerField
      FieldName = 'PRIORITY'
    end
    object cdsSHEprioSALARY_MINUTE_ROUNDED: TIntegerField
      FieldName = 'SALARY_MINUTE_ROUNDED'
    end
    object cdsSHEprioMINUTE_MOD_PART: TIntegerField
      FieldName = 'MINUTE_MOD_PART'
    end
  end
  object qrySHEround: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  SHE.SALARY_DATE,'
      '  SHE.EMPLOYEE_NUMBER,'
      '  SHE.MANUAL_YN,'
      '  SHE.HOURTYPE_NUMBER,'
      '  SHE.SALARY_MINUTE,'
      '  A1.PRIO PRIORITY'
      '-- BIJZONDERE UREN'
      'FROM'
      '  SALARYHOURPEREMPLOYEE SHE INNER JOIN EMPLOYEE E ON'
      '    SHE.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER'
      '  INNER JOIN CONTRACTGROUP CG ON'
      '    CG.CONTRACTGROUP_CODE = E.CONTRACTGROUP_CODE'
      '  INNER JOIN (SELECT A.*'
      '    FROM (SELECT CASE'
      '                 WHEN :EXCEPTIONAL_BEFORE_OVERTIME = '#39'Y'#39' THEN'
      '                  100 - ROWNUM'
      '                 ELSE'
      '                  200 - ROWNUM'
      '               END PRIO,'
      '               W1.*'
      '          FROM (SELECT *'
      '                  FROM (SELECT HT.HOURTYPE_NUMBER,'
      '                               HT.OVERTIME_YN,'
      '                               HT.BONUS_PERCENTAGE'
      '                          FROM HOURTYPE HT'
      '                         WHERE '
      '                           EXISTS'
      '                         (SELECT E.HOURTYPE_NUMBER'
      '                                  FROM EXCEPTIONALHOURDEF E'
      
        '                                 WHERE E.HOURTYPE_NUMBER = HT.HO' +
        'URTYPE_NUMBER'
      '                                   AND E.CONTRACTGROUP_CODE ='
      '                                       :CONTRACTGROUP_CODE)'
      '                           AND NOT EXISTS'
      '                         (SELECT A.HOURTYPE_NUMBER'
      '                                  FROM ABSENCEREASON A'
      
        '                                 WHERE A.HOURTYPE_NUMBER = HT.HO' +
        'URTYPE_NUMBER)'
      '                         ORDER BY HT.BONUS_PERCENTAGE) W) W1'
      '        UNION'
      '        -- OVER UREN'
      '        SELECT CASE'
      '                 WHEN :EXCEPTIONAL_BEFORE_OVERTIME = '#39'Y'#39' THEN'
      '                  200 - ROWNUM'
      '                 ELSE'
      '                  100 - ROWNUM'
      '               END PRIO,'
      '               X1.*'
      '          FROM (SELECT *'
      '                  FROM (SELECT HT.HOURTYPE_NUMBER,'
      '                               HT.OVERTIME_YN,'
      '                               HT.BONUS_PERCENTAGE'
      '                          FROM HOURTYPE HT'
      '                         WHERE '
      '                           HT.OVERTIME_YN = '#39'Y'#39
      '                           AND NOT EXISTS'
      '                         (SELECT E.HOURTYPE_NUMBER'
      '                                  FROM EXCEPTIONALHOURDEF E'
      
        '                                 WHERE E.HOURTYPE_NUMBER = HT.HO' +
        'URTYPE_NUMBER'
      '                                   AND E.CONTRACTGROUP_CODE ='
      '                                       :CONTRACTGROUP_CODE)'
      '                           AND NOT EXISTS'
      '                         (SELECT A.HOURTYPE_NUMBER'
      '                                  FROM ABSENCEREASON A'
      
        '                                 WHERE A.HOURTYPE_NUMBER = HT.HO' +
        'URTYPE_NUMBER)'
      '                         ORDER BY HT.BONUS_PERCENTAGE) X) X1'
      '        UNION'
      '        -- NORMALE UREN'
      '        SELECT *'
      '          FROM (SELECT 300 - ROWNUM PRIO,'
      '                       HT.HOURTYPE_NUMBER,'
      '                       HT.OVERTIME_YN,'
      '                       HT.BONUS_PERCENTAGE'
      '                  FROM HOURTYPE HT'
      '                 WHERE '
      '                   HT.OVERTIME_YN = '#39'N'#39
      '                   AND NOT EXISTS'
      '                 (SELECT E.HOURTYPE_NUMBER'
      '                          FROM EXCEPTIONALHOURDEF E'
      
        '                         WHERE E.HOURTYPE_NUMBER = HT.HOURTYPE_N' +
        'UMBER'
      
        '                           AND E.CONTRACTGROUP_CODE = :CONTRACTG' +
        'ROUP_CODE)'
      '                   AND NOT EXISTS'
      '                 (SELECT A.HOURTYPE_NUMBER'
      '                          FROM ABSENCEREASON A'
      
        '                         WHERE A.HOURTYPE_NUMBER = HT.HOURTYPE_N' +
        'UMBER)'
      '                 ORDER BY HT.HOURTYPE_NUMBER DESC) Y'
      '        UNION'
      '        -- AFWEZIGHEIDS UREN'
      '        SELECT *'
      '          FROM (SELECT 400 - ROWNUM PRIO,'
      '                       HT.HOURTYPE_NUMBER,'
      '                       HT.OVERTIME_YN,'
      '                       HT.BONUS_PERCENTAGE'
      '                  FROM HOURTYPE HT'
      '                 WHERE '
      '                   HT.OVERTIME_YN = '#39'N'#39
      '                   AND NOT EXISTS'
      '                 (SELECT E.HOURTYPE_NUMBER'
      '                          FROM EXCEPTIONALHOURDEF E'
      
        '                         WHERE E.HOURTYPE_NUMBER = HT.HOURTYPE_N' +
        'UMBER'
      
        '                           AND E.CONTRACTGROUP_CODE = :CONTRACTG' +
        'ROUP_CODE)'
      '                   AND EXISTS'
      '                 (SELECT A.HOURTYPE_NUMBER'
      '                          FROM ABSENCEREASON A'
      
        '                         WHERE A.HOURTYPE_NUMBER = HT.HOURTYPE_N' +
        'UMBER)'
      '                 ORDER BY HT.HOURTYPE_NUMBER DESC) Z) A ) A1 ON'
      '    A1.HOURTYPE_NUMBER = SHE.HOURTYPE_NUMBER'
      'WHERE'
      '  SHE.SALARY_DATE = :SALARY_DATE AND'
      '  SHE.MANUAL_YN = '#39'N'#39' AND'
      '  SHE.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER'
      'ORDER BY'
      '  PRIO'
      ''
      ''
      ''
      '')
    Left = 272
    Top = 608
    ParamData = <
      item
        DataType = ftString
        Name = 'EXCEPTIONAL_BEFORE_OVERTIME'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'CONTRACTGROUP_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'EXCEPTIONAL_BEFORE_OVERTIME'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'CONTRACTGROUP_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'CONTRACTGROUP_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'CONTRACTGROUP_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'SALARY_DATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end>
  end
  object cdsPHEPTprio: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 568
    Top = 608
    object cdsPHEPTprioPRODHOUREMPLOYEE_DATE: TDateField
      FieldName = 'PRODHOUREMPLOYEE_DATE'
    end
    object cdsPHEPTprioPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Size = 6
    end
    object cdsPHEPTprioSHIFT_NUMBER: TIntegerField
      FieldName = 'SHIFT_NUMBER'
    end
    object cdsPHEPTprioEMPLOYEE_NUMBER: TIntegerField
      FieldName = 'EMPLOYEE_NUMBER'
    end
    object cdsPHEPTprioWORKSPOT_CODE: TStringField
      FieldName = 'WORKSPOT_CODE'
      Size = 6
    end
    object cdsPHEPTprioJOB_CODE: TStringField
      FieldName = 'JOB_CODE'
      Size = 6
    end
    object cdsPHEPTprioMANUAL_YN: TStringField
      FieldName = 'MANUAL_YN'
      Size = 1
    end
    object cdsPHEPTprioHOURTYPE_NUMBER: TIntegerField
      FieldName = 'HOURTYPE_NUMBER'
    end
    object cdsPHEPTprioPRODUCTION_MINUTE: TIntegerField
      FieldName = 'PRODUCTION_MINUTE'
    end
    object cdsPHEPTprioPROD_MIN_EXCL_BREAK: TIntegerField
      FieldName = 'PROD_MIN_EXCL_BREAK'
    end
    object cdsPHEPTprioMINUTE_MOD_PART: TIntegerField
      FieldName = 'MINUTE_MOD_PART'
    end
    object cdsPHEPTprioPRODUCTION_MINUTE_ROUNDED: TIntegerField
      FieldName = 'PRODUCTION_MINUTE_ROUNDED'
    end
    object cdsPHEPTprioPRIORITY: TIntegerField
      FieldName = 'PRIORITY'
    end
  end
  object qryPHEPTround: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  PHE.PRODHOUREMPLOYEE_DATE,'
      '  PHE.PLANT_CODE,'
      '  PHE.SHIFT_NUMBER,'
      '  PHE.EMPLOYEE_NUMBER,'
      '  PHE.WORKSPOT_CODE,'
      '  PHE.JOB_CODE,'
      '  PHE.HOURTYPE_NUMBER,'
      '  PHE.MANUAL_YN,'
      '  PHE.PRODUCTION_MINUTE,'
      '  PHE.PROD_MIN_EXCL_BREAK,'
      '  A1.PRIO PRIORITY'
      '-- BIJZONDERE UREN'
      'FROM'
      '  PRODHOURPEREMPLPERTYPE PHE INNER JOIN EMPLOYEE E ON'
      '    PHE.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER'
      '  INNER JOIN CONTRACTGROUP CG ON'
      '    CG.CONTRACTGROUP_CODE = E.CONTRACTGROUP_CODE'
      '  INNER JOIN (SELECT A.*'
      '    FROM (SELECT CASE'
      '                 WHEN :EXCEPTIONAL_BEFORE_OVERTIME = '#39'Y'#39' THEN'
      '                  100 - ROWNUM'
      '                 ELSE'
      '                  200 - ROWNUM'
      '               END PRIO,'
      '               W1.*'
      '          FROM (SELECT *'
      '                  FROM (SELECT HT.HOURTYPE_NUMBER,'
      '                               HT.OVERTIME_YN,'
      '                               HT.BONUS_PERCENTAGE'
      '                          FROM HOURTYPE HT'
      '                         WHERE '
      '                           EXISTS'
      '                         (SELECT E.HOURTYPE_NUMBER'
      '                                  FROM EXCEPTIONALHOURDEF E'
      
        '                                 WHERE E.HOURTYPE_NUMBER = HT.HO' +
        'URTYPE_NUMBER'
      '                                   AND E.CONTRACTGROUP_CODE ='
      '                                       :CONTRACTGROUP_CODE)'
      '                           AND NOT EXISTS'
      '                         (SELECT A.HOURTYPE_NUMBER'
      '                                  FROM ABSENCEREASON A'
      
        '                                 WHERE A.HOURTYPE_NUMBER = HT.HO' +
        'URTYPE_NUMBER)'
      '                         ORDER BY HT.BONUS_PERCENTAGE) W) W1'
      '        UNION'
      '        -- OVER UREN'
      '        SELECT CASE'
      '                 WHEN :EXCEPTIONAL_BEFORE_OVERTIME = '#39'Y'#39' THEN'
      '                  200 - ROWNUM'
      '                 ELSE'
      '                  100 - ROWNUM'
      '               END PRIO,'
      '               X1.*'
      '          FROM (SELECT *'
      '                  FROM (SELECT HT.HOURTYPE_NUMBER,'
      '                               HT.OVERTIME_YN,'
      '                               HT.BONUS_PERCENTAGE'
      '                          FROM HOURTYPE HT'
      '                         WHERE '
      '                           HT.OVERTIME_YN = '#39'Y'#39
      '                           AND NOT EXISTS'
      '                         (SELECT E.HOURTYPE_NUMBER'
      '                                  FROM EXCEPTIONALHOURDEF E'
      
        '                                 WHERE E.HOURTYPE_NUMBER = HT.HO' +
        'URTYPE_NUMBER'
      '                                   AND E.CONTRACTGROUP_CODE ='
      '                                       :CONTRACTGROUP_CODE)'
      '                           AND NOT EXISTS'
      '                         (SELECT A.HOURTYPE_NUMBER'
      '                                  FROM ABSENCEREASON A'
      
        '                                 WHERE A.HOURTYPE_NUMBER = HT.HO' +
        'URTYPE_NUMBER)'
      '                         ORDER BY HT.BONUS_PERCENTAGE) X) X1'
      '        UNION'
      '        -- NORMALE UREN'
      '        SELECT *'
      '          FROM (SELECT 300 - ROWNUM PRIO,'
      '                       HT.HOURTYPE_NUMBER,'
      '                       HT.OVERTIME_YN,'
      '                       HT.BONUS_PERCENTAGE'
      '                  FROM HOURTYPE HT'
      '                 WHERE '
      '                   HT.OVERTIME_YN = '#39'N'#39
      '                   AND NOT EXISTS'
      '                 (SELECT E.HOURTYPE_NUMBER'
      '                          FROM EXCEPTIONALHOURDEF E'
      
        '                         WHERE E.HOURTYPE_NUMBER = HT.HOURTYPE_N' +
        'UMBER'
      
        '                           AND E.CONTRACTGROUP_CODE = :CONTRACTG' +
        'ROUP_CODE)'
      '                   AND NOT EXISTS'
      '                 (SELECT A.HOURTYPE_NUMBER'
      '                          FROM ABSENCEREASON A'
      
        '                         WHERE A.HOURTYPE_NUMBER = HT.HOURTYPE_N' +
        'UMBER)'
      '                 ORDER BY HT.HOURTYPE_NUMBER DESC) Y'
      '        UNION'
      '        -- AFWEZIGHEIDS UREN'
      '        SELECT *'
      '          FROM (SELECT 400 - ROWNUM PRIO,'
      '                       HT.HOURTYPE_NUMBER,'
      '                       HT.OVERTIME_YN,'
      '                       HT.BONUS_PERCENTAGE'
      '                  FROM HOURTYPE HT'
      '                 WHERE '
      '                   HT.OVERTIME_YN = '#39'N'#39
      '                   AND NOT EXISTS'
      '                 (SELECT E.HOURTYPE_NUMBER'
      '                          FROM EXCEPTIONALHOURDEF E'
      
        '                         WHERE E.HOURTYPE_NUMBER = HT.HOURTYPE_N' +
        'UMBER'
      
        '                           AND E.CONTRACTGROUP_CODE = :CONTRACTG' +
        'ROUP_CODE)'
      '                   AND EXISTS'
      '                 (SELECT A.HOURTYPE_NUMBER'
      '                          FROM ABSENCEREASON A'
      
        '                         WHERE A.HOURTYPE_NUMBER = HT.HOURTYPE_N' +
        'UMBER)'
      '                 ORDER BY HT.HOURTYPE_NUMBER DESC) Z) A ) A1 ON'
      '    A1.HOURTYPE_NUMBER = PHE.HOURTYPE_NUMBER'
      'WHERE'
      '  PHE.PRODHOUREMPLOYEE_DATE = :PRODHOUREMPLOYEE_DATE AND'
      '  PHE.MANUAL_YN = '#39'N'#39' AND'
      '  PHE.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER'
      'ORDER BY'
      '  PRIO'
      ''
      ''
      ' ')
    Left = 480
    Top = 608
    ParamData = <
      item
        DataType = ftString
        Name = 'EXCEPTIONAL_BEFORE_OVERTIME'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'CONTRACTGROUP_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'EXCEPTIONAL_BEFORE_OVERTIME'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'CONTRACTGROUP_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'CONTRACTGROUP_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'CONTRACTGROUP_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDate
        Name = 'PRODHOUREMPLOYEE_DATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end>
  end
  object qryAbsenceTotal: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  EARNED_TFT_MINUTE'
      'FROM'
      '  ABSENCETOTAL'
      'WHERE'
      '  EMPLOYEE_NUMBER = :EMPNO AND'
      '  ABSENCE_YEAR = :AYEAR'
      ' ')
    Left = 368
    Top = 56
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPNO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'AYEAR'
        ParamType = ptUnknown
      end>
  end
  object qryGetSWWHours: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  H.HOURTYPE_NUMBER,'
      '  H.BONUS_IN_MONEY_YN AS HT_BONUS_IN_MONEY_YN,'
      '  C.BONUS_IN_MONEY_YN AS CG_BONUS_IN_MONEY_YN,'
      '  H.BONUS_PERCENTAGE, SUM(SHE.SALARY_MINUTE) AS SUMMINUTE'
      'FROM'
      '  SALARYHOURPEREMPLOYEE SHE INNER JOIN HOURTYPE H ON'
      '  SHE.HOURTYPE_NUMBER = H.HOURTYPE_NUMBER'
      '  INNER JOIN EMPLOYEE E ON'
      '  SHE.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER'
      '  INNER JOIN CONTRACTGROUP C ON'
      '  E.CONTRACTGROUP_CODE = C.CONTRACTGROUP_CODE'
      'WHERE'
      '  SHE.EMPLOYEE_NUMBER = :EMPNO AND'
      '  SHE.SALARY_DATE >= :DATEFROM AND'
      '  SHE.SALARY_DATE <= :DATETO AND'
      '  H.OVERTIME_YN = '#39'Y'#39' AND'
      '  H.ADV_YN = '#39'Y'#39
      'GROUP BY H.HOURTYPE_NUMBER, H.BONUS_IN_MONEY_YN,'
      '  C.BONUS_IN_MONEY_YN, H.BONUS_PERCENTAGE'
      'ORDER BY H.HOURTYPE_NUMBER'
      '')
    Left = 664
    Top = 120
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPNO'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end>
  end
  object qryBalanceCounters: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  NORM_HOLIDAY_MINUTE, USED_HOLIDAY_MINUTE,'
      '  LAST_YEAR_HOLIDAY_MINUTE,'
      '  LAST_YEAR_TFT_MINUTE, EARNED_TFT_MINUTE,'
      '  USED_TFT_MINUTE,'
      '  LAST_YEAR_WTR_MINUTE, EARNED_WTR_MINUTE,'
      '  USED_WTR_MINUTE,'
      '  LAST_YEAR_HLD_PREV_YEAR_MINUTE, USED_HLD_PREV_YEAR_MINUTE,'
      '  NORM_SENIORITY_HOLIDAY_MINUTE, USED_SENIORITY_HOLIDAY_MINUTE,'
      '  LAST_YEAR_ADD_BANK_HLD_MINUTE, USED_ADD_BANK_HLD_MINUTE,'
      '  LAST_YEAR_SAT_CREDIT_MINUTE, USED_SAT_CREDIT_MINUTE,'
      '  NORM_BANK_HLD_RESERVE_MINUTE, USED_BANK_HLD_RESERVE_MINUTE,'
      '  LAST_YEAR_SHORTWWEEK_MINUTE, USED_SHORTWWEEK_MINUTE,'
      '  EARNED_SHORTWWEEK_MINUTE,'
      '  NORM_BANK_HLD_RES_MIN_MAN_YN,'
      '  EARNED_SAT_CREDIT_MINUTE,'
      '  LAST_YEAR_TRAVELTIME_MINUTE,'
      '  EARNED_TRAVELTIME_MINUTE,'
      '  USED_TRAVELTIME_MINUTE'
      'FROM'
      '  ABSENCETOTAL'
      'WHERE'
      '  EMPLOYEE_NUMBER=:EMPLOYEE_NUMBER AND '
      '  ABSENCE_YEAR=:ABSENCE_YEAR'
      ' ')
    Left = 584
    Top = 64
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'ABSENCE_YEAR'
        ParamType = ptUnknown
      end>
  end
  object qryMaxSalaryBalance: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '   EMPLOYEE_NUMBER, MAX_SALARY_BALANCE_MINUTES'
      'FROM'
      '  MAXSALARYBALANCE'
      'WHERE'
      '  EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER')
    Left = 480
    Top = 64
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end>
  end
  object qryActiveCounters: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  AC.ABSENCETYPE_CODE, AC.COUNTER_DESCRIPTION'
      'FROM'
      '  ABSENCETYPEPERCOUNTRY AC'
      'WHERE'
      '  AC.COUNTER_ACTIVE_YN = '#39'Y'#39' AND'
      '  AC.COUNTRY_ID = :COUNTRY_ID'
      ''
      ' '
      ' ')
    Left = 664
    Top = 48
    ParamData = <
      item
        DataType = ftInteger
        Name = 'COUNTRY_ID'
        ParamType = ptUnknown
      end>
  end
  object qryAbsenceTypePerCountryExist: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT T.COUNTRY_ID'
      'FROM ABSENCETYPEPERCOUNTRY T'
      'WHERE T.COUNTRY_ID = :COUNTRY_ID'
      '')
    Left = 632
    Top = 232
    ParamData = <
      item
        DataType = ftInteger
        Name = 'COUNTRY_ID'
        ParamType = ptUnknown
      end>
  end
  object qryPlantCountryBelgium: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT DISTINCT P.COUNTRY_ID, C.CODE, C.DESCRIPTION'
      'FROM PLANT P LEFT JOIN COUNTRY C ON'
      '  P.COUNTRY_ID = C.COUNTRY_ID'
      'WHERE'
      '  C.CODE = :CODE AND'
      '  P.PLANT_CODE IN'
      '  (SELECT DISTINCT T.PLANT_CODE'
      '   FROM DEPARTMENTPERTEAM T'
      '   WHERE'
      '   ('
      '     (:USER_NAME = '#39'*'#39') OR'
      '     (T.TEAM_CODE IN'
      
        '       (SELECT TU.TEAM_CODE FROM TEAMPERUSER TU WHERE TU.USER_NA' +
        'ME = :USER_NAME))'
      '   )'
      '  )'
      'ORDER BY C.CODE')
    Left = 544
    Top = 344
    ParamData = <
      item
        DataType = ftString
        Name = 'CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end>
  end
  object qryWorkspotContractgroup: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT W.CONTRACTGROUP_CODE'
      'FROM WORKSPOT W'
      'WHERE W.PLANT_CODE = :PLANT_CODE AND'
      'W.WORKSPOT_CODE = :WORKSPOT_CODE AND'
      'W.CONTRACTGROUP_CODE IS NOT NULL'
      ' ')
    Left = 168
    Top = 104
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'WORKSPOT_CODE'
        ParamType = ptUnknown
      end>
  end
  object qryHTONBK: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  B.ABSENCEREASON_CODE,'
      '  AR.ABSENCETYPE_CODE,'
      '  AR.ABSENCEREASON_ID,'
      '  B.HOURTYPE_WRK_ON_BANKHOL,'
      '  CG.HOURTYPE_WRK_ON_BANKHOL AS CG_HOURTYPE_WRK_ON_BANKHOL'
      'FROM'
      '  BANKHOLIDAY B INNER JOIN COUNTRY C ON'
      '    B.COUNTRY_ID = C.COUNTRY_ID'
      '  INNER JOIN PLANT P ON'
      '    C.COUNTRY_ID = P.COUNTRY_ID'
      '  INNER JOIN EMPLOYEE E ON'
      '    P.PLANT_CODE = E.PLANT_CODE'
      '  INNER JOIN CONTRACTGROUP CG ON'
      '   E.CONTRACTGROUP_CODE = CG.CONTRACTGROUP_CODE'
      '  INNER JOIN ABSENCEREASON AR ON'
      '    B.ABSENCEREASON_CODE = AR.ABSENCEREASON_CODE'
      'WHERE'
      '  E.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND'
      '  B.BANK_HOLIDAY_DATE = :BANKHOL_DATE'
      ' '
      ' '
      ' '
      ' ')
    Left = 656
    Top = 608
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'BANKHOL_DATE'
        ParamType = ptUnknown
      end>
  end
  object qryOvertimeParams: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  C.TIME_FOR_TIME_YN,'
      '  C.BONUS_IN_MONEY_YN,'
      '  H.BONUS_PERCENTAGE BONUS_PERCENTAGE,'
      '  H.OVERTIME_YN HT_OVERTIME_YN,'
      '  H.TIME_FOR_TIME_YN HT_TIME_FOR_TIME_YN,'
      '  H.BONUS_IN_MONEY_YN HT_BONUS_IN_MONEY_YN,'
      '  H.ADV_YN HT_ADV_YN'
      'FROM'
      '  CONTRACTGROUP C, HOURTYPE H'
      'WHERE'
      '  C.CONTRACTGROUP_CODE = :CONTRACTGROUP_CODE AND'
      '  H.HOURTYPE_NUMBER = :HOURTYPE_NUMBER')
    Left = 664
    Top = 504
    ParamData = <
      item
        DataType = ftString
        Name = 'CONTRACTGROUP_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'HOURTYPE_NUMBER'
        ParamType = ptUnknown
      end>
  end
  object qryAHE: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT A.ABSENCE_MINUTE'
      'FROM ABSENCEHOURPEREMPLOYEE A'
      'WHERE A.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND'
      'A.ABSENCEHOUR_DATE = :ABSENCEHOUR_DATE AND'
      'A.ABSENCEREASON_ID = :ABSENCEREASON_ID AND'
      'A.MANUAL_YN = :MANUAL_YN'
      ' '
      ' ')
    Left = 664
    Top = 344
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'ABSENCEHOUR_DATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'ABSENCEREASON_ID'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MANUAL_YN'
        ParamType = ptUnknown
      end>
  end
  object qryAHEDelete: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'DELETE FROM ABSENCEHOURPEREMPLOYEE'
      'WHERE EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND'
      'ABSENCEHOUR_DATE = :ABSENCEHOUR_DATE AND'
      'ABSENCEREASON_ID = :ABSENCEREASON_ID AND'
      'MANUAL_YN = :MANUAL_YN'
      ' '
      ' '
      ' ')
    Left = 664
    Top = 288
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'ABSENCEHOUR_DATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'ABSENCEREASON_ID'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MANUAL_YN'
        ParamType = ptUnknown
      end>
  end
  object qryTRSUpdate: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'UPDATE TIMEREGSCANNING T'
      'SET '
      '  T.SHIFT_DATE = :SHIFT_DATE,'
      '  T.MUTATIONDATE = SYSDATE,'
      '  T.MUTATOR = :MUTATOR'
      'WHERE '
      '  T.DATETIME_IN = :DATETIME_IN AND'
      '  T.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER')
    Left = 392
    Top = 552
    ParamData = <
      item
        DataType = ftDate
        Name = 'SHIFT_DATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MUTATOR'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETIME_IN'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end>
  end
  object qryShiftHT: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT S.HOURTYPE_NUMBER, H.IGNORE_FOR_OVERTIME_YN,'
      '  NVL(S.HOURTYPE_NUMBER, -1) HOURTYPE_NUMBER'
      'FROM SHIFT S LEFT OUTER JOIN HOURTYPE H ON'
      '  S.HOURTYPE_NUMBER = H.HOURTYPE_NUMBER'
      'WHERE S.PLANT_CODE = :PLANT_CODE AND'
      '  S.SHIFT_NUMBER = :SHIFT_NUMBER AND'
      '  S.HOURTYPE_NUMBER IS NOT NULL'
      ' ')
    Left = 344
    Top = 344
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'SHIFT_NUMBER'
        ParamType = ptUnknown
      end>
  end
  object qryEmpAvailUpdate5: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'UPDATE EMPLOYEEAVAILABILITY'
      'SET AVAILABLE_TIMEBLOCK_5 = :NEWAVCODE,'
      'MUTATIONDATE = SYSDATE,'
      'MUTATOR = :MUTATOR'
      'WHERE'
      '('
      '  (EMPLOYEEAVAILABILITY_DATE >= :STDATE AND :MORE = 1) OR'
      '  (EMPLOYEEAVAILABILITY_DATE = :STDATE AND :MORE = 0)'
      ') AND'
      'EMPLOYEE_NUMBER = :EMPNO'
      'AND AVAILABLE_TIMEBLOCK_5 =:AVCODE'
      ' '
      ' ')
    Left = 648
    Top = 392
    ParamData = <
      item
        DataType = ftString
        Name = 'NEWAVCODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MUTATOR'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'MORE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'MORE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPNO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'AVCODE'
        ParamType = ptUnknown
      end>
  end
  object qryEmpAvailUpdate6: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'UPDATE EMPLOYEEAVAILABILITY'
      'SET AVAILABLE_TIMEBLOCK_6 = :NEWAVCODE,'
      'MUTATIONDATE = SYSDATE,'
      'MUTATOR = :MUTATOR'
      'WHERE'
      '('
      '  (EMPLOYEEAVAILABILITY_DATE >= :STDATE AND :MORE = 1) OR'
      '  (EMPLOYEEAVAILABILITY_DATE = :STDATE AND :MORE = 0)'
      ') AND'
      'EMPLOYEE_NUMBER = :EMPNO'
      'AND AVAILABLE_TIMEBLOCK_6 =:AVCODE'
      ' '
      ' ')
    Left = 232
    Top = 440
    ParamData = <
      item
        DataType = ftString
        Name = 'NEWAVCODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MUTATOR'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'MORE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'MORE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPNO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'AVCODE'
        ParamType = ptUnknown
      end>
  end
  object qryEmpAvailUpdate7: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'UPDATE EMPLOYEEAVAILABILITY'
      'SET AVAILABLE_TIMEBLOCK_7 = :NEWAVCODE,'
      'MUTATIONDATE = SYSDATE,'
      'MUTATOR = :MUTATOR'
      'WHERE'
      '('
      '  (EMPLOYEEAVAILABILITY_DATE >= :STDATE AND :MORE = 1) OR'
      '  (EMPLOYEEAVAILABILITY_DATE = :STDATE AND :MORE = 0)'
      ') AND'
      'EMPLOYEE_NUMBER = :EMPNO'
      'AND AVAILABLE_TIMEBLOCK_7 =:AVCODE'
      ' '
      ' ')
    Left = 344
    Top = 440
    ParamData = <
      item
        DataType = ftString
        Name = 'NEWAVCODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MUTATOR'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'MORE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'MORE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPNO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'AVCODE'
        ParamType = ptUnknown
      end>
  end
  object qryEmpAvailUpdate8: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'UPDATE EMPLOYEEAVAILABILITY'
      'SET AVAILABLE_TIMEBLOCK_8 = :NEWAVCODE,'
      'MUTATIONDATE = SYSDATE,'
      'MUTATOR = :MUTATOR'
      'WHERE'
      '('
      '  (EMPLOYEEAVAILABILITY_DATE >= :STDATE AND :MORE = 1) OR'
      '  (EMPLOYEEAVAILABILITY_DATE = :STDATE AND :MORE = 0)'
      ') AND'
      'EMPLOYEE_NUMBER = :EMPNO'
      'AND AVAILABLE_TIMEBLOCK_8 =:AVCODE'
      ' '
      ' ')
    Left = 440
    Top = 440
    ParamData = <
      item
        DataType = ftString
        Name = 'NEWAVCODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MUTATOR'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'MORE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'MORE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPNO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'AVCODE'
        ParamType = ptUnknown
      end>
  end
  object qryEmpAvailUpdate9: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'UPDATE EMPLOYEEAVAILABILITY'
      'SET AVAILABLE_TIMEBLOCK_9 = :NEWAVCODE,'
      'MUTATIONDATE = SYSDATE,'
      'MUTATOR = :MUTATOR'
      'WHERE'
      '('
      '  (EMPLOYEEAVAILABILITY_DATE >= :STDATE AND :MORE = 1) OR'
      '  (EMPLOYEEAVAILABILITY_DATE = :STDATE AND :MORE = 0)'
      ') AND'
      'EMPLOYEE_NUMBER = :EMPNO'
      'AND AVAILABLE_TIMEBLOCK_9 =:AVCODE'
      ' '
      ' ')
    Left = 544
    Top = 440
    ParamData = <
      item
        DataType = ftString
        Name = 'NEWAVCODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MUTATOR'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'MORE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'MORE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPNO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'AVCODE'
        ParamType = ptUnknown
      end>
  end
  object qryEmpAvailUpdate10: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'UPDATE EMPLOYEEAVAILABILITY'
      'SET AVAILABLE_TIMEBLOCK_10 = :NEWAVCODE,'
      'MUTATIONDATE = SYSDATE,'
      'MUTATOR = :MUTATOR'
      'WHERE'
      '('
      '  (EMPLOYEEAVAILABILITY_DATE >= :STDATE AND :MORE = 1) OR'
      '  (EMPLOYEEAVAILABILITY_DATE = :STDATE AND :MORE = 0)'
      ') AND'
      'EMPLOYEE_NUMBER = :EMPNO'
      'AND AVAILABLE_TIMEBLOCK_10 =:AVCODE'
      ' '
      ' ')
    Left = 648
    Top = 440
    ParamData = <
      item
        DataType = ftString
        Name = 'NEWAVCODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MUTATOR'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'MORE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'MORE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPNO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'AVCODE'
        ParamType = ptUnknown
      end>
  end
end
