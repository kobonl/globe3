inherited DialogBaseF: TDialogBaseF
  Left = 305
  Top = 172
  HorzScrollBar.Range = 0
  VertScrollBar.Range = 0
  BorderStyle = bsDialog
  Caption = 'Base form for dialog'
  ClientHeight = 474
  ClientWidth = 637
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlImageBase: TPanel
    Width = 637
    TabOrder = 3
    inherited imgOrbit: TImage
      Left = 520
    end
  end
  inherited pnlInsertBase: TPanel
    Width = 637
    Height = 353
    Align = alClient
  end
  inherited stbarBase: TStatusBar
    Top = 455
    Width = 637
  end
  object pnlBottom: TPanel [4]
    Left = 0
    Top = 414
    Width = 637
    Height = 41
    Align = alBottom
    Color = clGrayText
    TabOrder = 7
    object btnOk: TBitBtn
      Left = 200
      Top = 8
      Width = 105
      Height = 25
      Caption = '&Ok'
      Default = True
      ModalResult = 1
      TabOrder = 0
      Glyph.Data = {
        36060000424D3606000000000000360000002800000020000000100000000100
        1800000000000006000012170000121700000000000000000000C89900C89900
        C89900C89900C89900C89900C89900C89900C89900C89900C89900C89900C899
        00C89900C89900C89900FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC89900C89900
        C89900C89900C89900C89900C89900C89900C89900C89900C89900C89900C899
        00C89900C89900C89900FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC89900C89900
        C89900C89900C89900C89900C89900C89900C89900C89900C89900C89900C899
        00C89900C89900C89900FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC89900C89900
        C89900C89900C89900FEFEFEC89900C89900C89900C89900C89900C89900C899
        00C89900C89900C89900FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0C0C0FFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC89900C89900
        C89900C89900FEFEFEFEFEFEFEFEFEC89900C89900C89900C89900C89900C899
        00C89900C89900C89900FFFFFFFFFFFFFFFFFFFFFFFFC0C0C0C0C0C0C0C0C0FF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC89900C89900
        C89900FEFEFEFEFEFEFEFEFEFEFEFEFEFEFEC89900C89900C89900C89900C899
        00C89900C89900C89900FFFFFFFFFFFFFFFFFFC0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC89900C89900
        FEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEC89900C89900C89900C899
        00C89900C89900C89900FFFFFFFFFFFFC0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC89900FEFEFE
        FEFEFEFEFEFEFEFEFEC89900FEFEFEFEFEFEFEFEFEFEFEFEC89900C89900C899
        00C89900C89900C89900FFFFFFC0C0C0C0C0C0C0C0C0C0C0C0FFFFFFC0C0C0C0
        C0C0C0C0C0C0C0C0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC89900FEFEFE
        FEFEFEFEFEFEC89900C89900C89900FEFEFEFEFEFEFEFEFEFEFEFEC89900C899
        00C89900C89900C89900FFFFFFC0C0C0C0C0C0C0C0C0FFFFFFFFFFFFFFFFFFC0
        C0C0C0C0C0C0C0C0C0C0C0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC89900C89900
        FEFEFEC89900C89900C89900C89900C89900FEFEFEFEFEFEFEFEFEFEFEFEC899
        00C89900C89900C89900FFFFFFFFFFFFC0C0C0FFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFC0C0C0C0C0C0C0C0C0C0C0C0FFFFFFFFFFFFFFFFFFFFFFFFC89900C89900
        C89900C89900C89900C89900C89900C89900C89900FEFEFEFEFEFEFEFEFEFEFE
        FEC89900C89900C89900FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFC0C0C0C0C0C0C0C0C0C0C0C0FFFFFFFFFFFFFFFFFFC89900C89900
        C89900C89900C89900C89900C89900C89900C89900C89900FEFEFEFEFEFEFEFE
        FEFEFEFEC89900C89900FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFC0C0C0C0C0C0C0C0C0C0C0C0FFFFFFFFFFFFC89900C89900
        C89900C89900C89900C89900C89900C89900C89900C89900C89900FEFEFEFEFE
        FEFEFEFEFEFEFEC89900FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFC0C0C0C0C0C0C0C0C0C0C0C0FFFFFFC89900C89900
        C89900C89900C89900C89900C89900C89900C89900C89900C89900C89900FEFE
        FEFEFEFEC89900C89900FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFC0C0C0C0C0C0FFFFFFFFFFFFC89900C89900
        C89900C89900C89900C89900C89900C89900C89900C89900C89900C89900C899
        00C89900C89900C89900FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC89900C89900
        C89900C89900C89900C89900C89900C89900C89900C89900C89900C89900C899
        00C89900C89900C89900FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
      NumGlyphs = 2
    end
    object btnCancel: TBitBtn
      Left = 314
      Top = 8
      Width = 105
      Height = 25
      Cancel = True
      Caption = '&Cancel'
      ModalResult = 2
      TabOrder = 1
      Glyph.Data = {
        36060000424D3606000000000000360000002800000020000000100000000100
        18000000000000060000121700001217000000000000000000002525A72525A7
        2525A72525A72525A72525A72525A72525A72525A72525A72525A72525A72525
        A72525A72525A72525A72525A72525A72525A72525A72525A72525A72525A725
        25A72525A72525A72525A72525A72525A72525A72525A72525A72525A7FEFEFE
        FEFEFE2525A72525A72525A72525A72525A72525A72525A72525A72525A7FEFE
        FEFEFEFEFEFEFE2525A72525A72525A78080802525A72525A72525A72525A725
        25A72525A72525A72525A72525A78080808080802525A72525A72525A7FEFEFE
        FEFEFEFEFEFE2525A72525A72525A72525A72525A72525A72525A7FEFEFEFEFE
        FEFEFEFEFEFEFE2525A72525A78080808080808080802525A72525A72525A725
        25A72525A72525A72525A78080808080808080808080802525A72525A7FEFEFE
        FEFEFEFEFEFEFEFEFE2525A72525A72525A72525A72525A7FEFEFEFEFEFEFEFE
        FEFEFEFE2525A72525A72525A78080808080808080808080802525A72525A725
        25A72525A72525A78080808080808080808080802525A72525A72525A72525A7
        FEFEFEFEFEFEFEFEFEFEFEFE2525A72525A72525A7FEFEFEFEFEFEFEFEFEFEFE
        FE2525A72525A72525A72525A72525A78080808080808080808080802525A725
        25A72525A78080808080808080808080802525A72525A72525A72525A72525A7
        2525A7FEFEFEFEFEFEFEFEFEFEFEFE2525A7FEFEFEFEFEFEFEFEFEFEFEFE2525
        A72525A72525A72525A72525A72525A72525A780808080808080808080808025
        25A78080808080808080808080802525A72525A72525A72525A72525A72525A7
        2525A72525A7FEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFE2525A72525
        A72525A72525A72525A72525A72525A72525A72525A780808080808080808080
        80808080808080808080802525A72525A72525A72525A72525A72525A72525A7
        2525A72525A72525A7FEFEFEFEFEFEFEFEFEFEFEFEFEFEFE2525A72525A72525
        A72525A72525A72525A72525A72525A72525A72525A72525A780808080808080
        80808080808080802525A72525A72525A72525A72525A72525A72525A72525A7
        2525A72525A72525A7FEFEFEFEFEFEFEFEFEFEFEFEFEFEFE2525A72525A72525
        A72525A72525A72525A72525A72525A72525A72525A72525A780808080808080
        80808080808080802525A72525A72525A72525A72525A72525A72525A72525A7
        2525A72525A7FEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFE2525A72525
        A72525A72525A72525A72525A72525A72525A72525A780808080808080808080
        80808080808080808080802525A72525A72525A72525A72525A72525A72525A7
        2525A7FEFEFEFEFEFEFEFEFEFEFEFE2525A7FEFEFEFEFEFEFEFEFEFEFEFE2525
        A72525A72525A72525A72525A72525A72525A780808080808080808080808025
        25A78080808080808080808080802525A72525A72525A72525A72525A72525A7
        FEFEFEFEFEFEFEFEFEFEFEFE2525A72525A72525A7FEFEFEFEFEFEFEFEFEFEFE
        FE2525A72525A72525A72525A72525A78080808080808080808080802525A725
        25A72525A78080808080808080808080802525A72525A72525A72525A7FEFEFE
        FEFEFEFEFEFEFEFEFE2525A72525A72525A72525A72525A7FEFEFEFEFEFEFEFE
        FEFEFEFE2525A72525A72525A78080808080808080808080802525A72525A725
        25A72525A72525A78080808080808080808080802525A72525A72525A7FEFEFE
        FEFEFEFEFEFE2525A72525A72525A72525A72525A72525A72525A7FEFEFEFEFE
        FEFEFEFEFEFEFE2525A72525A78080808080808080802525A72525A72525A725
        25A72525A72525A72525A78080808080808080808080802525A72525A7FEFEFE
        FEFEFE2525A72525A72525A72525A72525A72525A72525A72525A72525A7FEFE
        FEFEFEFEFEFEFE2525A72525A72525A78080802525A72525A72525A72525A725
        25A72525A72525A72525A72525A78080808080802525A72525A72525A72525A7
        2525A72525A72525A72525A72525A72525A72525A72525A72525A72525A72525
        A72525A72525A72525A72525A72525A72525A72525A72525A72525A72525A725
        25A72525A72525A72525A72525A72525A72525A72525A72525A7}
      NumGlyphs = 2
    end
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        IsMainMenu = True
        ItemLinks = <
          item
            Item = dxBarSubItemFile
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarSubItemHelp
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 26
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 1
        UseOwnFont = False
        Visible = False
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    Left = 528
    Top = 128
    DockControlHeights = (
      0
      0
      24
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarButtonResetColumns: TdxBarButton
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFDFBF9FBFAF8FBFAF8FBFAF8FBFAF8FCFAF8FBFAF8FB
        FAF8FBFAF8FBFAF8FCFAF8FBFAF8FBFAF8FBFAF8FBFAF8FBFAF8946D31946D31
        946D31946D31946D31946D31946D31946D31946D31946D31946D31946D31946D
        31946D31946D31946D31946D31FEE8A8FEE8A8FEE8A8FEE8A8946D31FEE8A8FE
        E8A8FEE8A8FEE8A8946D31FEE8A8FEE8A8FEE8A8FEE8A8946D31946D31FFF0AF
        FFF0AFFFF0AFFFF0AF946D31FFF0AFFFF0AFFFF0AFFFF0AF946D31FFF0AFFFF0
        AFFFF0AFFFF0AF946D31946D31946D31946D31946D31946D31946D31946D3194
        6D31946D31946D31946D31946D31946D31946D31946D31946D31946D31FFF0AF
        FFF0AFFFF0AFFFF0AF946D31FFF0AFFFF0AFFFF0AFFFF0AF946D31FFF0AFFFF0
        AFFFF0AFFFF0AF946D31946D31FFF9D4FFF9D4FFF8D3FFFCD8946D31FFF9D4FF
        F9D4FFF8D3FFFCD7946D31FFF9D4FFF8D3FFF7D2FFFEDA946D31946D31946D31
        946D31946D31946D31946D31946D31946D31946D31946D31946D31946D31946D
        31946D31946D31946D31946D31F9F2D6F9F2D6F9F1D5F9F5DA946D31F9F2D6F9
        F2D6F9F1D5F9F5DA946D31F9F2D6F9F2D6F9F1D5F9F7DD946D31946D31FFFFF1
        FFFFF1FFFEF0FFFFF6946D31FFFFF1FFFFF2FFFFF0FFFFF6946D31FFFFF1FFFF
        F2FFFFF0FFFFFA946D31946D3100EA0000EA0000EA0000EA00FFFFF200EA0000
        EA0000EA0000EA00FFFFF200EA0000EA0000EA0000EA00946D31946D3100EA00
        00EA0000EA0000EA00FFFFF200EA0000EA0000EA0000EA00FFFFF200EA0000EA
        0000EA0000ED00946D31946D31946D31946D31946D31946D31946D31946D3194
        6D31946D31946D31946D31946D31946D31946D31946D31946D31FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
    end
  end
  inherited dxBarDBNavigator: TdxBarDBNavigator
    Left = 600
  end
  inherited StandardMenuActionList: TActionList
    Top = 64
  end
end
