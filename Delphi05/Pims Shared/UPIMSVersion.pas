(*
 * Title:        UPIMSVesrion
 * Description:  Unit which contain version number of PIMS functionality
 * Copyright:    Copyright (c) 2000
 * Company:      ABS
 * Author        Carmen Panturu
 * Version       1.0
*)
unit UPIMSVersion;

interface

const
  PIMS_Func = 1;
implementation

end.
