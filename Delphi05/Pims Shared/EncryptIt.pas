unit EncryptIt;

interface
USES
  SysUtils, Classes;
const
  C1 = 52845;
  C2 = 22719;
  Key = 143;
  HEX: ARRAY['A'..'F'] OF INTEGER = (10,11,12,13,14,15);

  Valid_Char = ['A'..'Z', 'a'..'z', '0'..'9'];

function Encrypt(const S: String; const Key: Word): String;
function Decrypt(const S: String; const Key: Word): String;
function GetStringFromHex(S:String): String;

implementation

function Encrypt(const S: String; const Key : Word): String;
var
  I: Integer;
  Key_Encrypt: Word;
begin
  Key_Encrypt := Key;
  Result := S;
  for I := 1 to Length(S) do
  begin
    Result[I] := char(byte(S[I]) xor (Key_Encrypt shr 8));
    Key_Encrypt := (byte(Result[I]) + Key_Encrypt) * C1 + C2;
  end;
end;

function Decrypt(const S: String; const Key: Word): String;
var
  I: Integer;
  StrHex: String;
  Key_Decrypt: Word;
begin
  Key_Decrypt := Key;
  StrHex := GetStringFromHex(S);
  Result := StrHex;
  for I := 1 to Length(StrHex) do
  begin
    if I <= Length(Result) then
    begin
      Result[I] := char(byte(StrHex[I]) xor (Key_Decrypt shr 8));
      Key_Decrypt := (byte(StrHex[I]) + Key_Decrypt) * C1 + C2;
    end;  
  end;
end;

function GetStringFromHex(S:String): String;
var
  i, j, IntVal, ExpHex :Integer;
  CStr: Char;
  CryptStr: String;
begin
  CryptStr := '';
  S := UpperCase(S);
  i := 1;
  while i <= Length(S) do
  begin
    IntVal := 0;
    for j := i to i+1 do
    begin
      if j <= Length(S) then
      begin
        CStr := S[j];
        if j = i then
          ExpHex := 16
        else
          ExpHex:= 1;
        if CStr < 'A' then
        // only numbers
          IntVal := IntVal  +  StrToInt(CStr) * ExpHex
        else
          IntVal := IntVal + HEX[CStr] * ExpHex;
      end;
    end;
    i := i + 2;
    CryptStr := CryptStr + Chr(Intval);
  end;
  Result := CryptStr;
end;

end.
