unit SideMenuBaseFRM;
(*******************************************************************************
Unit     : FSideMenuBase    TfrmSideMenuBase = class(TfrmTopPimsLogo)
Function : Parent form. All Pims main Menus descend from here.
Note     : -
Changes  : -
--------------------------------------------------------------------------------
Inheritance tree:
TForm
frmPims
  Changed design properties :
  + Font.Name := Tahoma (MicroSoft Outlook Style)
  + Position  := poScreenCenter
  + ShowHint  := True

  Added controls:
  -

  Events:
  + FormCreate: Update the progressbar on the splashscreen

  Methods:
  + procedure SetParent(AParent :TWinControl); override;
    : Allmost all Solar froms a placed on a panel in another form.
    : Only via this method it is posible to have multiple toolbars
    : and full control over a form within the other form.

frmTopMenuBase
  Changed design properties :
  -

  Added controls:
  + dxBarManBase: Menu and toolbar manager from Dev Express
  + the Pims Menu structure
  + StandardMenuActionList

  Events:
  -

  Methods:
  -

frmTopPimsLogo
  Changed design properties :
  -

  Added controls:
  + pnlImageBase : base for the screencaptions and the Solar logo
    imgBaseSolar : the Solar logo
    pnlInsertBase: base for the inserted forms
    stbarBase    : the status bar


  Events:
  -

  Methods:
  -

frmSideMenuBase
  Changed design properties :
  + Height := 600
  + Width  := 800

  Added controls:                       Alignment
  + imgListBase                         non-visual

  Events / Properties:
  + InsertForm
    : The InsertForm is another designtime form which is merged on a panel. It
    : creates the same effect as a tab/pagecontrol. The big difference is the
    : fact that you can use all the methods and events of a TForm instead of
    : only a few events of a TPageControl.Page. Furthermore you create a lot of
    : small forms which are easy in design and have controlable components. The
    : use of a TPageControl tends to lead to very complex forms with an enormous
    : amount of components.

  Methods:
  + procedure SetInsertForm(AForm: TForm);
    : This method is the write method of the InsertForm property. It is a bit
    : like the InsertComponent method, but much easier to use because you can
    : now use designtime created forms.

*******************************************************************************)
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  dxBarDBNav, dxBar, ComDrvN, ComCtrls, ExtCtrls, ImgList,
  dxsbar, ActnList, TopPimsLogoFRM, jpeg;

type
  TManualCloseEvent = TNotifyEvent;


  TSideMenuBaseF = class(TTopPimsLogoF)
    imgListBase: TImageList;
    procedure imgBasePimsClick(Sender: TObject);
  private
    { Private declarations }
    FInsertForm: TForm;
    procedure SetInsertForm(AForm: TForm);
  protected
//    procedure NilInsertForm;
  public
    { Public declarations }
    procedure NilInsertForm;
    property InsertForm: TForm read FInsertForm write SetInsertForm;
  end;

var
  SideMenuBaseF: TSideMenuBaseF;

implementation

uses
  UPimsMessageRes, GridBaseFRM;

{$R *.DFM}

{ TfrmSideMenuBase }


procedure TSideMenuBaseF.SetInsertForm(AForm: TForm);
begin
  pnlInsertBase.Caption := ''; // PIM-12
  if AForm <> FInsertForm then
  begin
    if Assigned(FInsertForm) then
    begin
      FInsertForm.Hide;
      FInsertForm.Close;
    end;
    if AForm = Nil then
    begin
      pnlImageBase.Caption  := '';
      FInsertForm := Nil;
      Exit;
    end;
    AForm.Parent := pnlInsertBase;
    AForm.Align  := alClient;
    pnlImageBase.Caption  := '  ' + AForm.Caption;
  {  pnlInsertBase.Caption := SPimsLoading + AForm.Caption;}
    // Get toolbar from insertform and place it in the menuform's toolbar
    if (AForm is TGridBaseF) then
    begin
      dxBarManBase.LockUpdate := True;
      dxBarManBase.Bars[1].Visible    := False;
      dxBarManBase.Bars[1].Hidden     := True;
      dxBarManBase.Bars[1].Assign((AForm as TGridBaseF).dxBarManBase.Bars[1]);
      dxBarManBase.Bars[1].IsMainMenu := False;
      dxBarManBase.Bars[0].Visible    := False;
      dxBarManBase.Bars[0].Hidden     := True;
      dxBarManBase.Bars[0].IsMainMenu := True;
      dxBarManBase.Bars[0].DockedDockingStyle := dsTop;
      dxBarManBase.Bars[0].Row        := 0;
      dxBarManBase.Bars[0].DockedTop  := 0;
      dxBarManBase.Bars[0].DockedLeft := -1;
      dxBarManBase.Bars[1].Row        := 1;
      dxBarManBase.Bars[0].Hidden     := False;
      dxBarManBase.Bars[0].Visible    := True;
      dxBarManBase.Bars[0].OneOnRow   := True;
      dxBarManBase.Bars[1].WholeRow   := True;
      dxBarManBase.Bars[1].Hidden     := False;
      dxBarManBase.Bars[1].Visible    := True;
      dxBarManBase.LockUpdate := False;
      // Hide insertform's toolbar
      (AForm as TGridBaseF).dxBarManBase.LockUpdate := True;
      (AForm as TGridBaseF).dxBarManBase.Bars[1].Hidden  := True;
      (AForm as TGridBaseF).dxBarManBase.Bars[1].Visible := False;
      (AForm as TGridBaseF).dxBarManBase.LockUpdate := False;
    end
    else
    begin
      dxBarManBase.LockUpdate := True;
      dxBarManBase.Bars[1].Visible    := False;
      dxBarManBase.Bars[1].Hidden     := True;
      dxBarManBase.LockUpdate := False;
    end;
    Update;
    //
    //CAR 16-9-2003 right user changes
    //AForm.Show;
    FInsertForm := AForm;
  end;
end;

procedure TSideMenuBaseF.imgBasePimsClick(Sender: TObject);
var
  UpRightImage: TPoint;
  FormWidth: Integer;
begin
  inherited;
  // Show Solars Main Screen dropping out from the logo
  UpRightImage :=
{    ClientToScreen(Point((imgBaseSolar.Left + imgBaseSolar.Width),
    imgBaseSolar.Top));  }
    ClientToScreen(Point(((Sender as TImage).Left + (Sender as TImage).Width),
    (Sender as TImage).Top));
  for FormWidth := 0 to 480 do
  begin
    if FormWidth mod 48 = 0 then
    begin
      Application.MainForm.ClientWidth  := FormWidth;
      Application.MainForm.ClientHeight := FormWidth div 4 * 3;
      Application.MainForm.Left := UpRightImage.X - Application.MainForm.Width;
      Application.MainForm.Top  := UpRightImage.Y;
      Application.MainForm.Show;
      Application.MainForm.Update;
    end; { if }
  end; { FormWidth }
end;

procedure TSideMenuBaseF.NilInsertForm;
begin
  if InsertForm <> nil then
    InsertForm.Close;
  FInsertForm := nil;
  pnlImageBase.Caption  := '';
end;

end.

