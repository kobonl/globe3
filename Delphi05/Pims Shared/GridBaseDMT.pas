(*
  MRA:15-JUN-2018 GLOB3-60
  - Extension 4 to 10 blocks for planning.
*)
unit GridBaseDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, DBTables, dxDBGrid, dxTL;

type

  TGridBaseDM = class(TDataModule)
    TableMaster: TTable;
    TableDetail: TTable;
    DataSourceMaster: TDataSource;
    DataSourceDetail: TDataSource;
    TableExport: TTable;
    DataSourceExport: TDataSource;
    procedure DefaultBeforeDelete(DataSet: TDataSet);
    procedure DefaultBeforePost(DataSet: TDataSet);
    procedure DefaultDeleteError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure DefaultEditError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure DefaultNewRecord(DataSet: TDataSet);
    procedure DefaultPostError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure DefaultNotEmptyValidate(Sender: TField);
    procedure DataModuleCreate(Sender: TObject);
   
    procedure TableExportCalcFields(DataSet: TDataSet);

  private
    { Private declarations }
  public
    { Public declarations }

     Handle_Grid: TdxDBGrid;

    procedure SetNullValues(DataSet: TDataSet);
    procedure InitializeTimeValues(DataSet, FTable: TDataSet);
    function ValidateBlocksPerShift(DataSet, TShift: TDataSet): Integer;
    function ValidateIntervalTime(DataSet, TShift:TDataSet;
      NameTable: String): Integer;
    function CheckIsNightShift(Index: Integer; TShift: TDataSet): Boolean;
    function CheckBlocks(Host : TdxDBGrid; TShift: TTable; Breaks: Boolean) : Integer;

    procedure CreateExportTable;
    function GetDataField(FieldData: TField): TField;
    procedure SetLookup(TmpFieldName, TmpKeyFields,
      TmpLookUpKeyFields, TmpLookUpResultField: String;
      TmpDataSet, TmpLookUpDataset: TDataSet;
      var TmpField: TField);
    function CheckNrOfTimeblocks(ANr: Integer): Boolean;
  end;

  TGridBaseClassDM= Class of TGridBaseDM;

var
  GridBaseDM: TGridBaseDM;

implementation

uses SystemDMT, UPimsMessageRes, GridBaseFRM, UPimsConst;
{$R *.DFM}

procedure TGridBaseDM.DefaultBeforeDelete(DataSet: TDataSet);
begin
  SystemDM.DefaultBeforeDelete(DataSet);
end;

procedure TGridBaseDM.DefaultBeforePost(DataSet: TDataSet);
begin
  SystemDM.DefaultBeforePost(DataSet);
end;

procedure TGridBaseDM.DefaultDeleteError(DataSet: TDataSet;
  E: EDatabaseError; var Action: TDataAction);
begin
  SystemDM.DefaultDeleteError(DataSet, E, Action);
end;

procedure TGridBaseDM.DefaultEditError(DataSet: TDataSet;
  E: EDatabaseError; var Action: TDataAction);
begin
  SystemDM.DefaultEditError(DataSet, E, Action);
end;

procedure TGridBaseDM.DefaultNewRecord(DataSet: TDataSet);
begin
  SystemDM.DefaultNewRecord(DataSet);
end;

procedure TGridBaseDM.DefaultNotEmptyValidate(Sender: TField);
begin
  SystemDM.DefaultNotEmptyValidate(Sender);
end;

procedure TGridBaseDM.DefaultPostError(DataSet: TDataSet;
  E: EDatabaseError; var Action: TDataAction);
begin
//CAR 5-5-2003 Old bug- do not display error message twice
  if GridBaseF <> Nil then
  begin
    if not GridBaseF.FGiveError then
    begin
      Action := daAbort;
      GridBaseF.FGiveError := True;
      Exit;
    end
  end;
  SystemDM.DefaultPostError(DataSet, E, Action);
end;

procedure TGridBaseDM.SetNullValues(DataSet: TDataSet);
var
  Index: Integer;
begin
  for Index := 1 to 7 do
  begin
    if (DataSet.FieldByName('STARTTIME' + IntToStr(Index)).Value = Null) then
       DataSet.FieldByName('STARTTIME' + IntToStr(Index)).Value :=
         StrToTime('00:00');
    if (DataSet.FieldByName('ENDTIME' + IntToStr(Index)).Value = Null) then
      DataSet.FieldByName('ENDTIME' + IntToStr(Index)).Value:=
        StrToTime('00:00');
  end;
end;

procedure TGridBaseDM.InitializeTimeValues(DataSet, FTable: TDataSet);
var
  Index: Integer;
begin
  for Index := 1 to 7 do
  begin
    DataSet.FieldByName('STARTTIME' + IntToStr(Index)).Value :=
       FTable.FieldByName('STARTTIME' + IntToStr(Index)).Value;
    DataSet.FieldByName('ENDTIME' + IntToStr(Index)).Value :=
       FTable.FieldByName('ENDTIME' + IntToStr(Index)).Value;;
  end;
end;

function TGridBaseDM.ValidateBlocksPerShift(DataSet,
  TShift: TDataSet): Integer;
var
  Index: Integer;
  StrIndex: String;
function TimeFromDate(Date: TDateTime): TDateTime;
begin
  Result := GetTime(Date);
end;
begin
  Result := 0;
  for Index := 1 to 7 do
  begin
    StrIndex := IntToStr(Index);
    if not CheckIsNightShift(Index, TShift) then
    begin
      if (TimeFromDate(DataSet.FieldByName('STARTTIME' + StrIndex).AsDateTime) <
        TimeFromDate(TShift.FieldByName('STARTTIME' + StrIndex).AsDateTime))
      then
      begin
        Result := Index;
        DisplayMessage(SPimsTBStartTime +
          SystemDM.GetDayWDescription(Result) + '!', mtInformation, [mbOK]);
        Abort;
      end
   end
   else
   begin
      if (TimeFromDate(DataSet.FieldByName('STARTTIME' + StrIndex).AsDateTime) <
        TimeFromDate(TShift.FieldByName('STARTTIME' + StrIndex).AsDateTime))
        and
        (TimeFromDate(DataSet.FieldByName('STARTTIME' + StrIndex).AsDateTime) <
        TimeFromDate(TShift.FieldByName('ENDTIME' + StrIndex).AsDateTime)) then
      begin
        Result := Index;
        DisplayMessage(SPimsTBStartTime +
          SystemDM.GetDayWDescription(Result) + '!', mtInformation, [mbOK]);
        Abort;
      end
   end;
  end;

  for Index := 1 to 7 do
  begin
    StrIndex := IntToStr(Index);
    if not CheckIsNightShift(Index, TShift) then
    begin
      if (TimeFromDate(DataSet.FieldByName('ENDTIME' + StrIndex).AsDateTime) >
        TimeFromDate(TShift.FieldByName('ENDTIME' + StrIndex).AsDateTime)) then
      begin
        Result := Index;
        DisplayMessage(SPimsTBEndTime +
          SystemDM.GetDayWDescription(Result) + '!', mtInformation, [mbOK]);
        Abort;
      end
    end
    else
    begin
      if (TimeFromDate(DataSet.FieldByName('ENDTIME' + StrIndex).AsDateTime) >
        TimeFromDate(TShift.FieldByName('ENDTIME' + StrIndex).AsDateTime)) and
        (TimeFromDate(DataSet.FieldByName('ENDTIME' + StrIndex).AsDateTime) <
        TimeFromDate(TShift.FieldByName('STARTTIME' + StrIndex).AsDateTime)) then
      begin
        Result := Index;
        DisplayMessage(SPimsTBEndTime +
          SystemDM.GetDayWDescription(Result) + '!', mtInformation, [mbOK]);
        Abort;
      end
    end;
  end;
end;

function TGridBaseDM.ValidateIntervalTime(DataSet, TShift:TDataSet;
  NameTable: String): Integer;
var
  Index: Integer;
  StartTime, EndTime, EndTimeShift: TDateTime;
begin
  Result := 0;
  for Index := 1 to 7 do
  begin
    StartTime := GetTime(DataSet.FieldByName('STARTTIME' +
      IntToStr(Index)).AsDateTime);
    EndTime := GetTime(DataSet.FieldByName('ENDTIME' +
      IntToStr(Index)).AsDateTime);
    EndTimeShift :=
      GetTime(TShift.FieldByName('ENDTIME' +
      IntToStr(Index)).AsDateTime);
    if (CheckIsNightShift(Index, TShift) and (StartTime <= EndTimeShift) and
      (EndTime <= EndTimeShift) and (StartTime > EndTime)) or
      (CheckIsNightShift(Index, TShift) and (StartTime > EndTimeShift) and
      (EndTime > EndTimeShift) and (StartTime > EndTime)) or
      (CheckIsNightShift(Index, TShift) and (StartTime <= EndTimeShift) and
      (EndTime > EndTimeShift)) or
      (not CheckIsNightShift(Index, TShift) and (StartTime > EndTime)) then
      begin
        Result := Index;
        DisplayMessage(SPimsEndTime + NameTable + SPimsStartTime + NameTable +
          SPimsOn + SystemDM.GetDayWDescription(Result) + '!', mtInformation, [mbOK]);
        Abort;
      end;
  end;
end;


function TGridBaseDM.CheckBlocks(Host : TdxDBGrid; TShift: TTable;
  Breaks: Boolean) : Integer;
var Blk : array [1..MAX_TBS,1..14] of TDateTime;
    i,j : Integer;
    Adate, EndTimeShift : TDateTime;
    TheNode : TdxTreeListNode;
    Records : Integer;
    NightShift: Boolean;
function LastNotNull(i, j:Integer): TDateTime;
begin
  if Breaks then
  begin
    while (i > 1) do
    begin
      if (Int(Blk[i,j]) = Blk[i,j]) then
        i := i - 1
      else
        Break;
    end;
  end;
  Result := Blk[i,j];
end;
begin
  Records := Host.Count;
  if Records > SystemDM.MaxTimeblocks then
    Records := SystemDM.MaxTimeblocks;
  // Read all times
  for i:=1 to Records do
    begin
      TheNode := Host.Items[i-1];
      for j:=1 to 14 do
        Blk[i,j] := TheNode.Values[j+1];
    end;
  // Fill all fields with NOW date.
  ADate := now();
  for i:=1 to Records do
    for j:=1 to 14 do
      ReplaceDate(Blk[i,j],ADate);
    // Check intervals
  Result := 0;

  for i:= 1 to (Records - 1) do
  begin
    for j:= 1 to 7 do
    begin
      NightShift := CheckIsNightShift(j, TShift);
      EndTimeShift := TShift.FieldByName('ENDTIME' + IntToStr(j)).AsDateTime;
      ADate := now();
      ReplaceDate(EndTimeShift,ADate);
      if (Breaks) and
         (Int(Blk[i + 1,(2*j)]) = Blk[i + 1,(2*j)]) and
         (Int(Blk[i + 1, (2*j - 1)]) = Blk[i + 1, (2*j - 1)]) then
      else
      if (NightShift and
        (Blk[i,(2*j)] <= EndTimeShift) and
        (
        (Blk[i+1,(2*j-1)]  > EndTimeShift) or
        ((Blk[i+1,(2*j-1)]  < EndTimeShift) and (Blk[i,(2*j)] > Blk[i+1,(2*j-1)])))
        or
        (NightShift and
        (Blk[i,(2*j)] > EndTimeShift) and
        (Blk[i+1,(2*j-1)]  > EndTimeShift) and
        (Blk[i,(2*j)] > Blk[i+1,(2*j-1)]))
        or
        (not NightShift and (Blk[i,(2*j)] > Blk[i+1,(2*j-1)]))) then
        //error
        begin
          Result := 1;
          Break;
        end;
    end;
  end;
end;

function TGridBaseDM.CheckIsNightShift(Index: Integer; TShift: TDataSet): Boolean;
begin
  Result :=
   (GetTime(TShift.FieldByName('STARTTIME' +
    IntToStr(Index)).AsDateTime) >
    GetTime(TShift.FieldByName('ENDTIME' +
    IntToStr(Index)).AsDateTime));
end;

procedure TGridBaseDM.DataModuleCreate(Sender: TObject);
begin
// Create lookup for master/detail on description of PK of master CAR 8-5-2003
   CreateExportTable;
end;

function TGridBaseDM.GetDataField(FieldData: TField): TField;
begin
  Result := Nil;
  if FieldData.DataType = ftString then
    Result := TStringField.Create(TableExport);
  if FieldData.DataType = ftInteger then
    Result := TIntegerField.Create(TableExport);
  if FieldData.DataType = ftDateTime then
    Result := TDateTimeField.Create(TableExport);
  if FieldData.DataType = ftFloat then
    Result := TFloatField.Create(TableExport);
end;

procedure TGridBaseDM.SetLookup(TmpFieldName, TmpKeyFields,
  TmpLookUpKeyFields, TmpLookUpResultField: String;
  TmpDataSet, TmpLookUpDataset: TDataSet;
  var TmpField: TField);
begin
  with TmpField do
  begin
    FieldName := TmpFieldName;
    FieldKind:= fkLookup;
    DataSet := TmpDataSet;
    Name := TmpDataSet.Name + TmpFieldName;
    KeyFields:=  TmpKeyFields;
    LookUpDataset:=  TmpLookUpDataset;
   // LookupCache := True;
    LookUpKeyFields:= TmpLookUpKeyFields;
    LookUpResultField:= TmpLookUpResultField;
  end;
end;


procedure TGridBaseDM.CreateExportTable;
var
  TmpField: TField;
  i: Integer;
begin
// only if form is master/detail
  if TableDetail.MasterFields = '' then
    Exit;
// create fields from detail table into table export

  for i := 0 to TableDetail.Fields.Count - 1 do
  begin
    TmpField := GetDataField(TableDetail.Fields[i]);
    if TmpField <> nil then
    begin
      if TableDetail.Fields[i].FieldKind = fkLookup then
        SetLookup(TableDetail.Fields[i].FieldName,
          TableDetail.Fields[i].KeyFields,
          TableDetail.Fields[i].LookUpKeyFields,
          TableDetail.Fields[i].LookUpResultField,
          TableExport, TableDetail.Fields[i].LookUpDataset, TmpField);

       if TableDetail.Fields[i].FieldKind = fkData then
        with TmpField do begin
          FieldName := TableDetail.Fields[i].FieldName;
          FieldKind:= fkData;
          DataSet := TableExport;
          Name := Dataset.Name + FieldName;
        end;   
      if TableDetail.Fields[i].Calculated = True then
        with TmpField do begin
          FieldName := TableDetail.Fields[i].FieldName;
          DataSet := TableExport;
          Calculated := True;
          Name := Dataset.Name + FieldName;
        end;
      if TmpField.Name <> '' then
        TableExport.FieldDefs.Add(TmpField.Name, TableDetail.Fields[i].DataType,
          TableDetail.Fields[i].Size, false);
    end;
  end;
  TableExport.TableName :=  TableDetail.TableName;

// check if exist EMPLOYEE_NUMBER INSIDE AND MAKE IT FLOAT TYPE - for exporting
// big values - employee_number  with the new table Export created
// car 18-12-2003 replace TableMaster with DataSourceMaster.DataSet -
  if (DataSourceMaster.DataSet.FieldList.IndexOf('FLOAT_EMP') >= 0) OR
    (TableDetail.FieldList.IndexOf('EMPLOYEE_NUMBER') >= 0) then
    with TFloatField.Create(TableExport) do
    begin
      FieldName := 'FLOAT_EMP';
      Calculated := True;
      DataSet := TableExport;
      Name := TableExport.Name + FieldName;
      TableExport.FieldDefs.Add(Name, ftFloat, 0, false);
    end;
// add Description lookup of master table - data set
// car 18-12-2003 replace TableMaster with DataSourceMaster.DataSet -
// because it can be replaced by a Query for some forms
  if DataSourceMaster.DataSet.FieldList.IndexOf('DESCRIPTION') >= 0 then
  begin
    TmpField := TStringField.Create(TableExport);
    SetLookup(ExportDescriptonLU, TableDetail.MasterFields,
      TableDetail.MasterFields, 'DESCRIPTION', TableExport,
      DataSourceMaster.DataSet, TmpField);
    TableExport.FieldDefs.Add(TmpField.Name, ftString, 30, false);
   end;
   DataSourceExport.DataSet := TableExport;
end;



// CAR : add to Export table calculated field  for big employee numbers
procedure TGridBaseDM.TableExportCalcFields(DataSet: TDataSet);
begin
  if DataSet.FieldList.IndexOf('FLOAT_EMP') >= 0 then
    DataSet.FieldByNAME('FLOAT_EMP').AsFLOAT :=
      DataSet.FieldByName('EMPLOYEE_NUMBER').AsInteger;
end;

function TGridBaseDM.CheckNrOfTimeblocks(ANr: Integer): Boolean;
begin
  Result := True;
  if ANr > SystemDM.MaxTimeblocks - 1 then
  begin
    DisplayMessage(Format(SPimsTBInsertRecord, [SystemDM.MaxTimeblocks]),
      mtWarning, [mbOk]);
    Result := False;
  end;
end;

end.
