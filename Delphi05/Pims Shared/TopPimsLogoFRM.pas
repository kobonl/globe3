(*
  MRA:28-MAY-2013 New look Pims
  - Some pictures/colors are changed.
*)
unit TopPimsLogoFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  TopMenuBaseFRM, ActnList, dxBarDBNav, dxBar, ExtCtrls, ComCtrls, jpeg;

type
  TTopPimsLogoF = class(TTopMenuBaseF)
    pnlImageBase: TPanel;
    pnlInsertBase: TPanel;
    stbarBase: TStatusBar;
    imgOrbit: TImage;
    dxBarButtonSettings: TdxBarButton;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  TopPimsLogoF: TTopPimsLogoF;

implementation

{$R *.DFM}

uses
  UPimsConst;

procedure TTopPimsLogoF.FormCreate(Sender: TObject);
begin
  inherited;
  pnlImageBase.Color := clWhite; // PIM-250 // clPimsBlue; // 20014289
  pnlImageBase.Font.Color := clDarkRed; // PIM-250
end;

end.
