(*
  MRA:7-MAR-2014 TD-24046
  - Translate issues: Use a different method to translate.
    - Do NOT do this in separate files where the translation is done
      hard-coded!
    - New method:
      - Use 1 resource-strings-file that contains the translation-strings.
      - Use 1 translate-unit that assigns these strings to the properties
        in the forms.
      - In this way there is only 1 form that can be translated in multiple
        languages when the application is run.
      - The project-file defines the language using a condition.
  MRA:10-OCT-2014 SO-20015569
  - Show extra buttons for Down/Break/Lunch
*)
unit UTranslateStringsManReg;

interface

resourceString

{$IFDEF PIMSDUTCH}
  // Calculator
  STransNrDecimals = '# Decimalen';
  // DialogScanEmployee
  STransScanEmployee = 'Scan Werknemer';
  STransEmployee = 'Werknemer';
  STransIDCard = 'ID kaart';
  STransAccept = 'Accoord';
  STransOK = '&OK';
  STransCancel = '&ANNULEER';
  // ManualRegistrations
  STransManReg = 'Handmatige registraties';
  STransWorkspot = 'Werkplek';
  STransCurrentJob = 'Huidige Job';
  STransJobcode = 'Jobcode';
  STransSince = 'Sinds';
  STransEmployees = 'Werknemers';
  STransDateTimeIn = 'Datum/Tijd in';
  STransScanAll = 'Scan alles naar huidige job';
  STransScanEmp = 'Scan werknemer';
  STransDateTime = 'Datum/Tijd';
  STransStoreProd = '+ opslaan productie-aantal';
  STransChangeJob = 'Wijzig job';
  // DialogChangeJob
  STransSelectJob = 'Kies Job';
  STransEndOfDay = '&EINDE DAG';
  STransDown = 'STORING';
  STransBreak = 'PAUZE';
  STransLunch = 'LUNCH';
  // DialogSelectDownType
  STransSelectDownType = 'Kies Type Storing';
  STransMechDown = 'Mechanische Storing';
  STransNoMerch = 'Geen Artikelen';
{$ELSE}
  {$IFDEF PIMSGERMAN}
  // Calculator
  STransNrDecimals = '# Decimals';
  // DialogScanEmployee
  STransScanEmployee = 'Scan Mitarbeiter';
  STransEmployee = 'Mitarbeiter';
  STransIDCard = 'ID Karte';
  STransAccept = 'Akzeptier';
  STransOK = '&OK';
  STransCancel = '&ANNULLIER';
  // ManualRegistrations
  STransManReg = 'Manuelle Registrierung';
  STransWorkspot = 'Arbeitsplatz';
  STransCurrentJob = 'Heutige Job';
  STransJobcode = 'Jobcode';
  STransSince = 'Seit';
  STransEmployees = 'Mitarbeiters';
  STransDateTimeIn = 'Datum/Zeit ein';
  STransScanAll = 'Scan Alle nach heutige Job';
  STransScanEmp = 'Scan Mitarbeiter';
  STransDateTime = 'Datum/Zeit';
  STransStoreProd = '+ speicher Prod. Anzahl';
  STransChangeJob = '膎dern Job';
  // DialogChangeJob
  STransSelectJob = 'Selektier Job';
  STransEndOfDay = '&TAGESENDE';
  STransDown = 'DOWN';
  STransBreak = 'BREAK';
  STransLunch = 'LUNCH';
  // DialogSelectDownType
  STransSelectDownType = 'Wahl Steurungstype';
  STransMechDown = 'Mechan. Steurung';
  STransNoMerch = 'Keine Artikel';
{$ELSE}
  {$IFDEF PIMSDANISH}
  // Calculator
  STransNrDecimals = '#Decimaler';
  // DialogScanEmployee
  STransScanEmployee = 'Skan Medarb.';
  STransEmployee = 'Medarb.';
  STransIDCard = 'ID Kort';
  STransAccept = 'Accept';
  STransOK = '&OK';
  STransCancel = '&SLETt';
  // ManualRegistrations
  STransManReg = 'Manuel Registrering';
  STransWorkspot = 'Arb.sted';
  STransCurrentJob = 'Nuv. Job';
  STransJobcode = 'Jobkode';
  STransSince = 'Siden';
  STransEmployees = 'Medarb.';
  STransDateTimeIn = 'Dato/Tid i';
  STransScanAll = 'Skan alle til nuv. job';
  STransScanEmp = 'Scan medarb.';
  STransDateTime = 'Date / Tid';
  STransStoreProd = '+ gem produktionsantal';
  STransChangeJob = '苙dre job';
  // DialogChangeJob
  STransSelectJob = 'V鎙g Job';
  STransEndOfDay = '&SLUT DAG';
  STransDown = 'NEDE';
  STransBreak = 'PAUSE';
  STransLunch = 'LUNCH';
  // DialogSelectDownType
  STransSelectDownType = 'V鎙g lukning';
  STransMechDown = 'Mekanisk';
  STransNoMerch = 'Ingen varer';
{$ELSE}
  {$IFDEF PIMSJAPANESE}
  // Calculator
  STransNrDecimals = '# Decimals';
  // DialogScanEmployee
  STransScanEmployee = 'Scan Employee';
  STransEmployee = 'Employee';
  STransIDCard = 'ID Card';
  STransAccept = 'Accept';
  STransOK = '&OK';
  STransCancel = '&CANCEL';
  // ManualRegistrations
  STransManReg = 'Manual Registrations';
  STransWorkspot = 'Workspot';
  STransCurrentJob = 'Current Job';
  STransJobcode = 'Jobcode';
  STransSince = 'Since';
  STransEmployees = 'Employees';
  STransDateTimeIn = 'Date/Time in';
  STransScanAll = 'Scan all to current job';
  STransScanEmp = 'Scan employee';
  STransDateTime = 'Date / Time';
  STransStoreProd = '+ store production quantity';
  STransChangeJob = 'Change job';
  // DialogChangeJob
  STransSelectJob = 'Select Job';
  STransEndOfDay = '&END OF DAY';
  STransDown = 'DOWN';
  STransBreak = 'BREAK';
  STransLunch = 'LUNCH';
  // DialogSelectDownType
  STransSelectDownType = 'Select Down Type';
  STransMechDown = 'Mechanical Down';
  STransNoMerch = 'No Merchandize';
{$ELSE}
  {$IFDEF PIMSCHINESE}
  // Calculator
  STransNrDecimals = '# 位数';
  // DialogScanEmployee
  STransScanEmployee = '扫描员工';
  STransEmployee = '员工';
  STransIDCard = 'ID卡';
  STransAccept = '接受';
  STransOK = '&OK';
  STransCancel = '&取消';
  // ManualRegistrations
  STransManReg = '手工登记';
  STransWorkspot = '工作点';
  STransCurrentJob = '当前工作';
  STransJobcode = '工号';
  STransSince = '从';
  STransEmployees = '员工';
  STransDateTimeIn = '日期/重新计时';
  STransScanAll = '所有扫描到当前工作';
  STransScanEmp = '扫描员工';
  STransDateTime = '日期 / 时间';
  STransStoreProd = '+ 储存生产量';
  STransChangeJob = '切换工作';
  // DialogChangeJob
  STransSelectJob = '选择工作';
  STransEndOfDay = '&当日结束';
  STransDown = '下降';
  STransBreak = '返回';
  STransLunch = '午餐';
  // DialogSelectDownType
  STransSelectDownType = '选择下降模式';
  STransMechDown = '机械下降';
  STransNoMerch = '无货物';
{$ELSE}
  {$IFDEF PIMSNORWEGIAN}
  // Calculator
  STransNrDecimals = '# Desimaler';
  // DialogScanEmployee
  STransScanEmployee = 'Skann ansatt';
  STransEmployee = 'Ansatt';
  STransIDCard = 'ID Kort';
  STransAccept = 'Akseptert';
  STransOK = '&OK';
  STransCancel = '&AVBRYT';
  // ManualRegistrations
  STransManReg = 'Manuell registrering';
  STransWorkspot = 'Arbeidsposisjon';
  STransCurrentJob = 'Aktuel jobb';
  STransJobcode = 'Jobb kode';
  STransSince = 'Siden';
  STransEmployees = 'Ansatt';
  STransDateTimeIn = 'Dato/Tid inn';
  STransScanAll = 'Scann alle til aktuel jobb';
  STransScanEmp = 'Skann ansatt';
  STransDateTime = 'Dato / Tid';
  STransStoreProd = '+ lagre produsjons resultat';
  STransChangeJob = 'Endre jobb';
  // DialogChangeJob
  STransSelectJob = 'Velg jobb';
  STransEndOfDay = '&SLUTT P� DAG';
  STransDown = 'NEDE';
  STransBreak = 'PAUSE';
  STransLunch = 'LUNSJ';
  // DialogSelectDownType
  STransSelectDownType = 'Velg nede type';
  STransMechDown = 'Mekanisk nedetid';
  STransNoMerch = 'Ingen produkter';
{$ELSE}
// ENGLISH (default language)
  // Calculator
  STransNrDecimals = '# Decimals';
  // DialogScanEmployee
  STransScanEmployee = 'Scan Employee';
  STransEmployee = 'Employee';
  STransIDCard = 'ID Card';
  STransAccept = 'Accept';
  STransOK = '&OK';
  STransCancel = '&CANCEL';
  // ManualRegistrations
  STransManReg = 'Manual Registrations';
  STransWorkspot = 'Workspot';
  STransCurrentJob = 'Current Job';
  STransJobcode = 'Jobcode';
  STransSince = 'Since';
  STransEmployees = 'Employees';
  STransDateTimeIn = 'Date/Time in';
  STransScanAll = 'Scan all to current job';
  STransScanEmp = 'Scan employee';
  STransDateTime = 'Date / Time';
  STransStoreProd = '+ store production quantity';
  STransChangeJob = 'Change job';
  // DialogChangeJob
  STransSelectJob = 'Select Job';
  STransEndOfDay = '&END OF DAY';
  STransDown = 'DOWN';
  STransBreak = 'BREAK';
  STransLunch = 'LUNCH';
  // DialogSelectDownType
  STransSelectDownType = 'Select Down Type';
  STransMechDown = 'Mechanical Down';
  STransNoMerch = 'No Merchandize';
          {$ENDIF}
        {$ENDIF}
      {$ENDIF}
    {$ENDIF}
  {$ENDIF}
{$ENDIF}

implementation

end.
