unit CalculatorFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ComCtrls, ExtCtrls;

type
  TCalculatorF = class(TForm)
    gBoxCalculatorDisplay: TGroupBox;
    gBoxCalculatorButtons: TGroupBox;
    btn7: TButton;
    btn8: TButton;
    btn9: TButton;
    btnMinus: TButton;
    btn4: TButton;
    btn5: TButton;
    btn6: TButton;
    btnPlus: TButton;
    btn1: TButton;
    btn2: TButton;
    btn3: TButton;
    btnAccept: TButton;
    btn0: TButton;
    btnPeriod: TButton;
    btnC: TButton;
    Button1: TButton;
    UpDown1: TUpDown;
    DisplayPanel: TPanel;
    Label1: TLabel;
    Display: TPanel;
    Edit1: TEdit;
    UpDown2: TUpDown;
    Button_MR: TButton;
    Button_MC: TButton;
    PrintDialog: TPrintDialog;
    ScreenTicket: TMemo;
    procedure FormCreate(Sender: TObject);
    procedure ButtonClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
    DispString, InputString: String;
    PrevTeller, CurTeller, CurTotal, MemTotal: Double;
    CalcAction : Char;
    MaxDigits, NumDigits: Integer;
    FUseThis: Double;
    procedure SetDispString(A: Double);
    procedure AddTicket(A: String; B: Boolean);
    procedure ClearMemory;
    procedure SetUseThis(const Value: Double);
  public
    { Public declarations }
    procedure ClearCurrent;
    // Value from the calculator
    property UseThis: Double read FUseThis write SetUseThis;
  end;

var
  CalculatorF: TCalculatorF;
  CloseType: Boolean;
  TheTicket: TextFile;
  LastKey, PrevKey: Char;
  DrawLine, FormatString: String;
  OptionStatus: (os_DISPLAY, os_PRECISION, os_LOGO, os_HELP);
  DecimalStatus: (ds_YES, ds_NO);
  DecimalTyped: Integer;

implementation

{$R *.DFM}

procedure TCalculatorF.FormCreate(Sender: TObject);
begin
  NumDigits := 2;
  MaxDigits := 14;
  DrawLine  := '-';
  ClearCurrent;
  ClearMemory;
  CloseType:= False;
  AddTicket(Caption, False);
  AddTicket(FormatString, true);
  LastKey := ' ';
  PrevKey := ' ';
  UpDown1.Width   := 45;
  OptionStatus    := os_DISPLAY;
  Button1.Visible := False;
  Edit1.Visible   := False;
  Display.Visible := True;
  UpDown1.Visible := False;
  Label1.Visible  := False;
  ActiveControl   := nil;
end;

procedure TCalculatorF.ClearCurrent;
begin
  DecimalStatus   := ds_NO;
  DecimalTyped    := 0;
  CalcAction      := ' ';
  UseThis         := 0;
  CurTotal        := 0;
  CurTeller       := 0;
  PrevTeller      := 0;
  SetDispString(CurTeller);
  InputString     := '';
  Display.Caption := DispString;
end;
//----------------------------------------------------------------------------
procedure TCalculatorF.ClearMemory;
begin
  MemTotal := 0;
  Button_MR.Visible := (MemTotal <> 0);
  Button_MC.Visible := (MemTotal <> 0);
end;

procedure TCalculatorF.ButtonClick(Sender: TObject);
var
  LastButton: Char;
begin
  if (Sender is TButton) then
  begin
    if TButton(Sender).Tag <> 0 then
    begin
      LastButton := Char(TButton(Sender).Tag); // convert longint to char
      FormKeyPress(Sender, LastButton);
    end;
  end; // if (Sender is TButton)
end;

procedure TCalculatorF.SetDispString(A: Double);
begin
  UseThis := A;
  DispString := FloatToStrF(A, ffNumber, MaxDigits, NumDigits) + ' ';  // change this
  Button_MR.Visible := (MemTotal <> 0);
  Button_MC.Visible := (MemTotal <> 0);
end;

procedure TCalculatorF.FormKeyPress(Sender: TObject; var Key: Char);
const
  KeyQUIT     = Char(27); //KME?? shouldn't this be either #27 or Chr(27)
  KeyUSE      = Char(1);
  KeyMEMCLEAR = Char(2);
  KeyMEMSHOW  = Char(3);
  KeyMEMADD   = Char(4);
  KeyMEMMIN   = Char(5);
  KeyPERCENT  = Char(37);
  KeyPRINT    = Char(7);
  KeyCLEAR    = Char(12);
  KeyOPTIONS  = Char(9);
  KeyADD      = Char(43);
  KeyMINUS    = Char(45);
  KeyMULTIPLY = Char(42);
  KeyDIV      = Char(47);
  KeyTOTAL    = Char(61);
  KeyPOINT    = Char(46);
  KeyBACK     = Char(8);
var
  IsError : boolean;
//  LineNr : integer;
begin
//  Memo1.Lines.Add('BEFORE LastKey=' + LastKey +
//    ' PrevKey= ' + PrevKey +
//    ' CurTeller=' + FloatToStr(CurTeller) +
//    ' PrevTeller=' + FloatToStr(PrevTeller) +
//    ' CurTotal= ' + FloatToStr(CurTotal));
  IsError := False;
  PrevKey := LastKey;
  LastKey := UpCase(Key);
  if LastKey = Char(13) then
    LastKey := KeyTOTAL;
  case LastKey of
    KeyPRINT :
      begin
{        if PrintDialog.Execute then
           begin
           AssignPrn(TheTicket);
           Rewrite(TheTicket);
           Printer.Canvas.Font := ScreenTicket.Font;
           for LineNr := 0 to ScreenTicket.Lines.Count - 1 do
               WriteLn(TheTicket, ScreenTicket.Lines[LineNr]);
           CloseFile(TheTicket);
           end; } // if PrintDialog1.Execute then
      end; // KeyPrint
    KeyOPTIONS :
      begin
        case OptionStatus of
          os_DISPLAY   :
            begin
              Edit1.Text := IntToStr(NumDigits);
              OptionStatus := os_PRECISION;
            end;
          os_PRECISION :
            begin
              NumDigits := StrToInt (Edit1.Text);
              OptionStatus := os_LOGO;
            end;
          os_LOGO :
              OptionStatus := os_HELP;
          os_HELP :
              OptionStatus := os_DISPLAY;
         end; // case OptionStatus
        Display.Visible := (OptionStatus = os_DISPLAY);
        Label1.Visible  := (OptionStatus = os_PRECISION);
        Edit1.Visible   := (OptionStatus = os_PRECISION);
        UpDown1.Visible := (OptionStatus = os_PRECISION);
        Button1.Visible := (OptionStatus = os_HELP);
      end;
    KeyQuit, KeyUSE :
      begin
        CloseType := (LastKey = KeyUSE);
        Close;
      end; // KeyUSE
    KeyCLEAR :
      begin
        DecimalStatus := ds_NO;
        DecimalTyped := 0;
        if PrevKey = KeyCLEAR then
        begin
          ClearCurrent;
          AddTicket('C', true);
        end
        else
        begin
          CurTeller := 0;
          InputString := '';
          SetDispString(CurTeller);
        end;
      end; // KeyCLEAR
    KeyMEMCLEAR :
      begin
        AddTicket('MC', false);
        ClearMemory;
      end; // KeyMEMCLEAR
    KeyMEMSHOW :
      begin
        SetDispString(MemTotal);
        AddTicket(DispString + 'MR', false);
      end; // KeyMEMSHOW
    KeyMEMADD, KeyMEMMIN :
      begin
        case LastKey of
          KeyMEMADD :
            begin
              MemTotal := MemTotal + UseThis;
              AddTicket(DispString + 'M+', false);
            end; // KeyMEMADD
          KeyMEMMIN :
            begin
              MemTotal := MemTotal - UseThis;
              AddTicket(DispString + 'M-', false);
            end; // KeyMEMMIN
          end; // case CurrentKey  ??
        CalcAction  := ' ';
        InputString := ''; // if continued with an operation value of
                           // curteller is kept
                           // if digit typed, value of curteller is reset
        Button_MR.Visible := (MemTotal <> 0);
        Button_MC.Visible := (MemTotal <> 0);
      end; // KeyMEMADD, KeyMEMMIN
    KeyADD, KeyMINUS, KeyMULTIPLY, KeyDIV, KeyTOTAL :
      begin
        DecimalStatus := ds_NO;
        DecimalTyped := 0;
        case PrevKey of
          KeyADD, KeyMINUS, KeyMULTIPLY, KeyDIV, KeyTOTAL :
            begin
              if (LastKey = KeyADD) or (LastKey = KeyMINUS) then
              begin
                CurTeller := CurTotal;
                SetDispString(CurTeller);
              end
              else
              begin
                CurTeller := PrevTeller;
                SetDispString(CurTeller);
              end;
            end; // KeyADD, KeyMINUS
        end; // case PrevKey
        case CalcAction of
          KeyADD      :
            begin
              AddTicket(DispString + ' +', false);
              CurTotal := CurTotal + CurTeller;
            end; // KeyAdd
          KeyMINUS    :
            begin
              AddTicket(DispString + ' -', false);
              CurTotal := CurTotal - CurTeller;
            end; // KeyMINUS
          KeyMULTIPLY :
            begin
              AddTicket(DispString + ' *', false);
              CurTotal := CurTotal * CurTeller;
            end; // KeyMULTIPLY
          KeyDIV      :
            begin
              AddTicket(DispString + ' /', false);
              if CurTeller = 0 then
              begin
                CurTotal   := 0;
                DispString := 'Error';
              end // if CurTeller = 0
              else
                CurTotal := CurTotal / CurTeller;
            end; // KeyDIV
        else {case}
          begin
            AddTicket(DispString + '  ', false);
            CurTotal := CurTeller;
          end; // new entry
        end; // Case CalcAction
        PrevTeller := CurTeller;
        CurTeller := 0;
        InputString := '';
        if LastKey = KeyTOTAL then
        begin
          CalcAction := ' ';
        end // if LastKey = KeyTOTAL
        else
          CalcAction := LastKey;
        SetDispString(CurTotal);
        if LastKey = KeyTOTAL then
          AddTicket(DispString + ' =', true);
      end; // KeyAdd, KeyMINUS, ...
    KeyPERCENT :
      begin
        DecimalStatus := ds_NO;
        DecimalTyped := 0;
        case CalcAction of
          KeyADD      :
            begin
              AddTicket(DispString + '%+', false);
              CurTotal := CurTotal + (CurTotal * CurTeller / 100);
            end; // KeyADD
          KeyMINUS    :
            begin
              AddTicket(DispString + '%-', false);
              CurTotal := CurTotal - (CurTotal * CurTeller / 100);
            end; // KeyMINUS
          KeyMULTIPLY :
            begin
              AddTicket(DispString + '%*', false);
              CurTotal := CurTotal * CurTeller / 100;
            end; // KeyMULTIPLY
          KeyDIV      :
            begin
              AddTicket(DispString + '%/', false);
              CurTotal := CurTotal / CurTeller / 100;
            end; // KeyDIV
        else
          begin
            AddTicket(DispString + '% ', false);
            CurTotal := CurTeller;
          end; // new entry
        end; // Case CalcAction
        CalcAction  := ' ';
        InputString := ' '; // if continued with an operation value of
                           // curteller is kept
                           // if digit typed, value of curteller is reset
        SetDispString(CurTotal);
        PrevTeller := CurTeller;
        CurTeller  := CurTotal;
        CurTotal   := 0;
        AddTicket(DispString + '%=', true);
      end; // KeyPERCENT  ...
    KeyPOINT :
      begin
        if DecimalStatus = ds_NO then
        begin
          DecimalStatus := ds_YES;
          DecimalTyped  := 0;
          InputString   := InputString + DecimalSeparator;
          CurTeller     := StrToFloat(InputString);
          SetDispString(CurTeller);
        end; // if DecimalStatus := ds_NO
      end;
    '0'..'9' :
      begin
        if DecimalStatus = ds_YES then
          Inc(DecimalTyped)
        else
          DecimalTyped := 0;
        if DecimalTyped <= NumDigits then
        begin
          if Length(InputString) <= MaxDigits then
            InputString := InputString + LastKey;
          CurTeller := StrToFloat(InputString);
          SetDispString(CurTeller);
        end;
      end; // '0'..'9'
    KeyBACK :
      begin
        if Length(InputString) > 0 then
        begin
          if InputString[Length(InputString)] = DecimalSeparator then
          begin
             SetLength(InputString, Length(InputString)- 2);
             DecimalStatus := ds_NO;
          end
          else
          begin
             SetLength(InputString, Length(InputString)- 1);
             if DecimalTyped > 0 then
               Dec(DecimalTyped);
          end;
        end; // if Length(Inputstring) > 0
        if Length(InputString) = 0 then
          CurTeller := 0
        else
          CurTeller := StrToFloat(InputString);
        SetDispString(CurTeller);
      end; // KeyBACK
  end; // case CurrentKey of
  if IsError then
    DispString := 'Err. ';
  Display.Caption := DispString;
  ActiveControl := nil;
{
  Memo1.Lines.Add('AFTER  LastKey=' + LastKey +
    ' PrevKey= ' + PrevKey +
    ' CurTeller=' + FloatToStr(CurTeller) +
    ' PrevTeller=' + FloatToStr(PrevTeller) +
    ' CurTotal= ' + FloatToStr(CurTotal));
}    
end;

procedure TCalculatorF.AddTicket(A: String; B: Boolean);
begin
  while Length(A) < MaxDigits do
    A := ' ' + A;
  ScreenTicket.Lines.Add(A);
  while Length(DrawLine) < MaxDigits do
    DrawLine := '-' + DrawLine;
  if B then
    ScreenTicket.Lines.Add(DrawLine);
end;

procedure TCalculatorF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
//  if CloseType then
//    ModalResult := IDOK;
//  Memo1.Lines.SaveToFile('D:\TEMP\CALC.TXT');
end;

procedure TCalculatorF.Button1Click(Sender: TObject);
begin
  Application.HelpCommand(HELP_CONTENTS, 0);
end;

procedure TCalculatorF.SetUseThis(const Value: Double);
begin
  FUseThis := Value;
end;

end.
