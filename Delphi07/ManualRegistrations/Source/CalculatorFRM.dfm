object CalculatorF: TCalculatorF
  Left = 262
  Top = 107
  BorderStyle = bsNone
  Caption = 'CalculatorF'
  ClientHeight = 383
  ClientWidth = 330
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  PixelsPerInch = 96
  TextHeight = 13
  object gBoxCalculatorDisplay: TGroupBox
    Left = 0
    Top = 0
    Width = 330
    Height = 90
    Align = alTop
    TabOrder = 0
    object Button1: TButton
      Left = 8
      Top = 56
      Width = 75
      Height = 25
      Caption = 'Button1'
      TabOrder = 0
      Visible = False
      OnClick = Button1Click
    end
    object UpDown1: TUpDown
      Left = 296
      Top = 32
      Width = 16
      Height = 24
      TabOrder = 1
    end
    object DisplayPanel: TPanel
      Left = 8
      Top = 11
      Width = 315
      Height = 75
      TabOrder = 2
      object Label1: TLabel
        Left = 56
        Top = 16
        Width = 53
        Height = 13
        Caption = '# Decimals'
      end
      object Display: TPanel
        Left = 4
        Top = 7
        Width = 305
        Height = 61
        Alignment = taRightJustify
        BevelOuter = bvLowered
        BevelWidth = 2
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -24
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
      end
      object Edit1: TEdit
        Left = 12
        Top = 17
        Width = 288
        Height = 45
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -32
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        Text = '0'
      end
      object UpDown2: TUpDown
        Left = 300
        Top = 17
        Width = 15
        Height = 45
        Associate = Edit1
        Max = 7
        TabOrder = 2
        Visible = False
      end
    end
  end
  object gBoxCalculatorButtons: TGroupBox
    Left = 0
    Top = 90
    Width = 330
    Height = 293
    Align = alClient
    TabOrder = 1
    object btn7: TButton
      Tag = 55
      Left = 8
      Top = 22
      Width = 56
      Height = 56
      Caption = '7'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      TabStop = False
      OnClick = ButtonClick
    end
    object btn8: TButton
      Tag = 56
      Left = 71
      Top = 22
      Width = 55
      Height = 56
      Caption = '8'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      TabStop = False
      OnClick = ButtonClick
    end
    object btn9: TButton
      Tag = 57
      Left = 133
      Top = 22
      Width = 55
      Height = 56
      Caption = '9'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
      TabStop = False
      OnClick = ButtonClick
    end
    object btnMinus: TButton
      Tag = 45
      Left = 195
      Top = 22
      Width = 127
      Height = 56
      Caption = '-'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 4
      TabStop = False
      OnClick = ButtonClick
    end
    object btn4: TButton
      Tag = 52
      Left = 8
      Top = 86
      Width = 56
      Height = 56
      Caption = '4'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 5
      TabStop = False
      OnClick = ButtonClick
    end
    object btn5: TButton
      Tag = 53
      Left = 71
      Top = 86
      Width = 56
      Height = 56
      Caption = '5'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 6
      TabStop = False
      OnClick = ButtonClick
    end
    object btn6: TButton
      Tag = 54
      Left = 133
      Top = 86
      Width = 56
      Height = 56
      Caption = '6'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 7
      TabStop = False
      OnClick = ButtonClick
    end
    object btnPlus: TButton
      Tag = 43
      Left = 195
      Top = 86
      Width = 127
      Height = 56
      Caption = '+'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 8
      TabStop = False
      OnClick = ButtonClick
    end
    object btn1: TButton
      Tag = 49
      Left = 8
      Top = 150
      Width = 56
      Height = 56
      Caption = '1'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 9
      TabStop = False
      OnClick = ButtonClick
    end
    object btn2: TButton
      Tag = 50
      Left = 71
      Top = 150
      Width = 56
      Height = 56
      Caption = '2'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 10
      TabStop = False
      OnClick = ButtonClick
    end
    object btn3: TButton
      Tag = 51
      Left = 133
      Top = 150
      Width = 56
      Height = 56
      Caption = '3'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 11
      TabStop = False
      OnClick = ButtonClick
    end
    object btnAccept: TButton
      Tag = 61
      Left = 195
      Top = 150
      Width = 127
      Height = 118
      Caption = '='
      Default = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      TabStop = False
      OnClick = ButtonClick
    end
    object btn0: TButton
      Tag = 48
      Left = 8
      Top = 212
      Width = 56
      Height = 56
      Caption = '0'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 12
      TabStop = False
      OnClick = ButtonClick
    end
    object btnPeriod: TButton
      Tag = 46
      Left = 71
      Top = 212
      Width = 56
      Height = 56
      Caption = '.'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 13
      TabStop = False
      OnClick = ButtonClick
    end
    object btnC: TButton
      Tag = 12
      Left = 133
      Top = 212
      Width = 56
      Height = 56
      Caption = 'C'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 14
      TabStop = False
      OnClick = ButtonClick
    end
    object Button_MR: TButton
      Tag = 3
      Left = 38
      Top = 5
      Width = 30
      Height = 25
      Caption = 'MR'
      DragCursor = crDefault
      TabOrder = 15
      Visible = False
      OnClick = ButtonClick
    end
    object Button_MC: TButton
      Tag = 2
      Left = 5
      Top = 5
      Width = 30
      Height = 25
      Caption = 'MC'
      TabOrder = 16
      Visible = False
      OnClick = ButtonClick
    end
    object ScreenTicket: TMemo
      Left = 0
      Top = -1
      Width = 33
      Height = 42
      Alignment = taRightJustify
      Color = clSilver
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Courier New'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      ScrollBars = ssVertical
      TabOrder = 17
      Visible = False
      WordWrap = False
    end
  end
  object PrintDialog: TPrintDialog
    Left = 208
    Top = 32
  end
end
