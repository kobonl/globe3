unit SelectorFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BasePimsFRM, StdCtrls, Buttons, Grids, DBGrids, DB, ExtCtrls, ImgList;

const
  ModalOK       = 1;
  ModalCancel   = 2;
  ModalEndofDay = 3;

type

  TFormPos = record
    Top, Left, Height, Width: Integer;
  end;

  TSelectorF = class(TBasePimsForm)
    PanelButtons: TPanel;
    PanelGrid: TPanel;
    DBGridSelector: TDBGrid;
    PanelControls: TPanel;
    BitBtnOk: TBitBtn;
    BitBtnCancel: TBitBtn;
    BitBtnEndOfDay: TBitBtn;
    PanelBFirst: TPanel;
    SpeedButtonFirst: TSpeedButton;
    PanelBLast: TPanel;
    PanelBPrior: TPanel;
    SpeedButtonPrior: TSpeedButton;
    PanelNext: TPanel;
    SpeedButtonNext: TSpeedButton;
    SpeedButtonLast: TSpeedButton;
    procedure BitBtnEndOfDayClick(Sender: TObject);
    procedure SpeedButtonFirstClick(Sender: TObject);
    procedure SpeedButtonPriorClick(Sender: TObject);
    procedure SpeedButtonNextClick(Sender: TObject);
    procedure SpeedButtonLastClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
    function EnableSelection(var AFormPos: TFormPos;
       const AColumnNames: array of String; ADataSource: TDataSource): Integer;
  end;

var
  SelectorF: TSelectorF;

implementation

uses
  UPimsMessageRes;

{$R *.DFM}

{ TSelectorF }

procedure TSelectorF.BitBtnEndOfDayClick(Sender: TObject);
begin
  inherited;
  ModalResult := ModalEndOfDay;
end;

function TSelectorF.EnableSelection(var AFormPos: TFormPos;
  const AColumnNames: array of String;
  ADataSource: TDataSource): Integer;
var
	ColIndex : integer;
begin
  // Structure of ColumnNames
  // 0. Selector form Caption
  // 1 - x grid column text
  // only 2 columns - set width based on this
  ModalResult := ModalCancel;
  DBGridSelector.Columns.Clear;
  if High(AColumnNames) > 0 then
    Caption := AColumnNames[0];
  for ColIndex := 1 to (High(AColumnNames)) do
    begin
      DBGridSelector.Columns.Add;
      DBGridSelector.Columns[ColIndex - 1].Field :=
      	ADataSource.DataSet.Fields[ColIndex - 1];
      DBGridSelector.Columns[ColIndex - 1].Title.Caption :=
        AColumnNames[ColIndex];
      if (AColumnNames[ColIndex] = SPimsColumnCode) or
         (AColumnNames[ColIndex] = SPimsColumnPlant) then
        DBGridSelector.Columns[ColIndex - 1].Width :=
          Round(DBGridSelector.Width / (High(AColumnNames) + 2))
      else
        DBGridSelector.Columns[ColIndex - 1].Width :=
       	  DBGridSelector.Width - DBGridSelector.Columns[ColIndex - 2].Width;
    end;
  DBGridSelector.DataSource := ADataSource;
  Top    := AFormPos.Top;
  Left   := AFormPos.Left;
  Height := AFormPos.Height;
  Width  := AFormPos.Width;
  Result := ShowModal;
end;

procedure TSelectorF.SpeedButtonFirstClick(Sender: TObject);
begin
  inherited;
  DBGridSelector.DataSource.DataSet.First;
end;

procedure TSelectorF.SpeedButtonPriorClick(Sender: TObject);
begin
  inherited;
  DBGridSelector.DataSource.DataSet.Prior;
end;

procedure TSelectorF.SpeedButtonNextClick(Sender: TObject);
begin
  inherited;
  DBGridSelector.DataSource.DataSet.Next;
end;

procedure TSelectorF.SpeedButtonLastClick(Sender: TObject);
begin
  inherited;
	DBGridSelector.DataSource.DataSet.Last;
end;

procedure TSelectorF.FormShow(Sender: TObject);
begin
  inherited;
  DBGridSelector.SetFocus;
end;


procedure TSelectorF.FormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  case Key of
    VK_F10		: begin
                  BitBtnEndOfDayClick(self);
                  Key := 0;
                end;
    VK_ESCAPE : begin
                  ModalResult := ModalCancel;
                  Key := 0;
                end;
    {VK_UP			: begin
                  SpeedButtonPriorClick(self);
                  Key := 0;
                end;
    VK_DOWN		: begin
                  SpeedButtonNextClick(self);
                  Key := 0;
                end;}
    VK_LEFT		: begin
                  SpeedButtonFirstClick(self);
                  Key := 0;
                end;
    VK_RIGHT	: begin
                  SpeedButtonLastClick(self);
                  Key := 0;
                end;
  end;
end;

end.

