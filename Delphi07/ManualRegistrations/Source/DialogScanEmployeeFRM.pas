(*
  MRA:8-NOV-2013 TD-23355
  - Update Manual Registrations
  MRA:8-NOV-2013 20013176 (TD-23355)
  - Check for any Smart Card Commands, if found
    then it should be possible to read an ID from a smart card
    using a smart card reader.
  MRA:14-JUL-2014 20015302.70
  - Based on workstation-setting (EnableManRegsMultipleJobs) register
    quantities for multiple jobs or not.
  - Extra: Center this form based on ownersform.
  MRA:10-OCT-2014 SO-20015569
  - Show extra buttons for Down/Break/Lunch
  MRA:8-JUL-2015 PIM-12
  - Real Time Efficiency (button/color changes)
  MRA:28-JUL-2015 PIM-60
  - End-Of-Day-button blink at end of shift
*)
unit DialogScanEmployeeFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BasePimsSerialFRM, ComDrvN, ComCtrls, ExtCtrls, Buttons, StdCtrls, ImgList,
  PCSCRaw, PCSCDef, Reader, UPCSCCardReader, TimeRecordingDMT,
  UGlobalFunctions, UPimsConst, UScannedIDCard, CalculateTotalHoursDMT;

type
  TErrorCode = (ecOK, ecIDCardExpired, ecIDCardNotFound, ecEmployeeExpired,
    ecEmployeeNotFound, ecScanExists, ecNonScanEmployee, ecUnKnown);

type
  TDialogScanEmployeeF = class(TBasePimsSerialForm)
    pnl1: TPanel;
    pnl2: TPanel;
    sbarMessage: TStatusBar;
    gboxEmployee: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    edtIDCard: TEdit;
    btnAccept: TButton;
    edtEmployeeNumber: TEdit;
    edtEmployeeDescription: TEdit;
    BitBtnOk: TBitBtn;
    BitBtnCancel: TBitBtn;
    ReaderListBox: TListBox;
    RichEdit: TRichEdit;
    CommandComboBox: TComboBox;
    pnl3: TPanel;
    procedure btnAcceptClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BitBtnOkClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }
    FEmployeeNumber: Integer;
    FDeviceSettinsFound: Boolean;
    FVerifyPosition: Integer;
    FValuePosition: Integer;
    FVerifyString: String;
    // 20013176
    SmartCard: Boolean;
    MyBuffer: String;
    MyHexBuffer: String;
    CardReader: TPCSCCardReader;
    FWorkspotCode: String;
    FPlantCode: String;
    FNewJobCode: String;
    FNewJobDescription: String;
    procedure CardReaderProcessScan;
    procedure ReadCardID_3a;
    procedure ReadCardID_3b;
    procedure AddErrorLog(AMsg: String);
    function ValueIDCardSmartCard(var ACode : String): TErrorCode;
    procedure SetEmployeeNumber(const Value: Integer);
    procedure DetermineSerialDeviceSettings;
    procedure SetDeviceSettingsFound(const Value: Boolean);
    procedure SetValuePosition(const Value: Integer);
    procedure SetVerifyPosition(const Value: Integer);
    procedure SetVerifyString(const Value: String);
    procedure ProcessCodeRead(const CodeRead: String);
    procedure StartScanning(ACode: String);
    procedure StartScanningPost(const Code: String);
    procedure ChangeJob;
    procedure DisplayMessage(AMsg: String);
    procedure WMSysCommand(var Msg: TWMSysCommand); message WM_SYSCOMMAND;
    property DeviceSettingsFound: Boolean read FDeviceSettinsFound write
      SetDeviceSettingsFound;
    property VerifyString: String read FVerifyString write SetVerifyString;
    property VerifyPosition: Integer read FVerifyPosition
      write SetVerifyPosition;
    property ValuePosition: Integer read FValuePosition write SetValuePosition;
  public
    { Public declarations }
    property EmployeeNumber: Integer read FEmployeeNumber
      write SetEmployeeNumber;
    property PlantCode: String read FPlantCode write FPlantCode;
    property WorkspotCode: String read FWorkspotCode write FWorkspotCode;
    property NewJobCode: String read FNewJobCode write FNewJobCode;
    property NewJobDescription: String read FNewJobDescription write FNewJobDescription;
  end;

var
  DialogScanEmployeeF: TDialogScanEmployeeF;

implementation

{$R *.DFM}

uses
  ORASystemDMT, ManualRegistrationsDMT, UPimsMessageRes, Oracle,
  DialogChangeJobFRM;

procedure TDialogScanEmployeeF.FormCreate(Sender: TObject);
begin
  inherited;
  edtIDCard.Text              := '';
  edtEmployeeNumber.Text      := '';
  edtEmployeeDescription.Text := '';
  DetermineSerialDeviceSettings;
  if DeviceSettingsFound then
  begin
    OpenScanner;
    OnCodeRead := ProcessCodeRead;
  end;
  // 20013176
  SmartCard := False;
  MyBuffer := '';
  MyHexBuffer := '';
  CardReader := nil;
  if TimeRecordingDM.SmartCardCommandsFound then
  begin
    // Initialise needed for smart card reading.
    CardReader := TPCSCCardReader.Create;
    CardReader.MyBuffer := MyBuffer;
    CardReader.MyHexBuffer := MyHexBuffer;
    CardReader.ReaderListBox := ReaderListBox;
    CardReader.CommandComboBox := CommandComboBox;
    CardReader.RichEdit := RichEdit;
    if UpperCase(TimeRecordingDM.SerialDeviceCode) <> 'ICLASS' then
      CardReader.MyReadCardID := ReadCardID_3a
    else
      CardReader.MyReadCardID := ReadCardID_3b;
    CardReader.MyAddErrorLog := AddErrorLog;
    CardReader.Init;
  end;
end; // FormCreate

procedure TDialogScanEmployeeF.StartScanning(ACode: String);
begin
  with ManualRegistrationsDM do
  begin
    if SmartCard then
      ValueIDCardSmartCard(ACode);
    oqEmployeeIDCard.ClearVariables;
    oqEmployeeIDCard.SetVariable('IDCARD_NUMBER', ACode);
    oqEmployeeIDCard.Execute;
    if not oqEmployeeIDCard.Eof then
    begin
      edtEmployeeNumber.Text :=
        oqEmployeeIDCard.FieldAsString('EMPLOYEE_NUMBER');
      edtEmployeeDescription.Text :=
        oqEmployeeIDCard.FieldAsString('DESCRIPTION');
      DisplayMessage('');
      //sbarMessage.SimpleText := '';
    end
    else
    begin
      edtEmployeeNumber.Text := '';
      edtEmployeeDescription.Text := '';
      DisplayMessage(SPimsIDCardNotFound);
      //sbarMessage.SimpleText := SPimsIDCardNotFound;
    end;
  end; {with ManualRegistrationsDM}
end; // StartScanning

procedure TDialogScanEmployeeF.btnAcceptClick(Sender: TObject);
begin
  inherited;
  SmartCard := False; // 20013176.4 When Accept-button is clicked, then it is no smart card.
  StartScanning(edtIDCard.Text);
  if ORASystemDM.EnableManRegsMultipleJobs then
    BitBtnOkClick(Sender);
end;

procedure TDialogScanEmployeeF.SetEmployeeNumber(const Value: Integer);
begin
  FEmployeeNumber := Value;
end;

procedure TDialogScanEmployeeF.DetermineSerialDeviceSettings;
begin
  DeviceSettingsFound := False;
  with ManualRegistrationsDM do
  begin
    // Set scanner by database-settings
//    oqComportConnection.Close;
    oqComportConnection.SetVariable('COMPUTER_NAME',
      ORASystemDM.CurrentComputerName);
    oqComportConnection.Execute;
    // Only One Serial-device setting is allowed!
    if not oqComportConnection.Eof then
    begin
//      oqComportConnection.First;
      try
        ScannerSettings(
          oqComportConnection.FieldAsInteger('COMPORT'),
          oqComportConnection.FieldAsInteger('DATABITS'),
          oqComportConnection.FieldAsString('PARITY')[1],
          oqComportConnection.FieldAsInteger('BAUDRATE'),
          oqComportConnection.FieldAsInteger('STOPBITS')
          );
        DeviceSettingsFound := True;
        try
          if not oqComportConnection.FieldIsNull('VERIFYSTRING') then
            VerifyString :=
              oqComportConnection.FieldAsString('VERIFYSTRING');
          if not oqComportConnection.FieldIsNull('VERIFYPOSITION') then
            VerifyPosition :=
              oqComportConnection.FieldAsInteger('VERIFYPOSITION');
        except
          VerifyString   := '';
          VerifyPosition := 0;
        end;
        try
          if not oqComportConnection.FieldIsNull('VALUEPOSITION') then
            ValuePosition :=
              oqComportConnection.FieldAsInteger('VALUEPOSITION');
        except
          ValuePosition := 0;
        end;
      except
        ScannerSettings(1, 8, 'N', 9600, 1);
        VerifyString   := '';
        VerifyPosition := 0;
        ValuePosition  := 0;
      end;
    end
    else
    begin
      ScannerSettings(1, 8, 'N', 9600, 1);
      VerifyString   := '';
      VerifyPosition := 0;
      ValuePosition  := 0;
    end;
// not for oq's    oqComportConnection.Close;
  end;
end; // DetermineSerialDeviceSettings

// Code has been read by Serial-port
procedure TDialogScanEmployeeF.ProcessCodeRead(const CodeRead: String);
var
  BracketPosition: Integer;
begin
  DisplayMessage('');
  //sbarMessage.SimpleText := '';
  FlushScanner;
//  CloseScanner;
  edtIDCard.Text := CodeRead;
  // Filter on VerifyString
  if (VerifyString <> '') and (CodeRead <> '') then
  begin
    try
      if (Length(CodeRead) > VerifyPosition) then
      begin
        if (Copy(CodeRead, VerifyPosition, Length(VerifyString)) =
          VerifyString) then
        begin
          // Now get ID-card-code
          BracketPosition := Pos(')', CodeRead);
          edtIDCard.Text := Copy(CodeRead, ValuePosition,
            BracketPosition - ValuePosition);
        end
        else
        begin
          edtIDCard.Text := '';
          if (edtIDCard.Text = '') then
          begin
//            FOnScanError(SPimsNoMatch);
            DisplayMessage(SPimsNoMatch);
            //sbarMessage.SimpleText := SPimsNoMatch;
          end;
        end;
      end
      else
        edtIDCard.Text := '';
    except
        edtIDCard.Text := '';
    end;
  end;
  if edtIDCard.Text <> '' then
    StartScanningPost(edtIDCard.Text);
//  OpenScanner;
end; // ProcessCodeRead

// Wrapper around 'StartScanning' to be able to close and open scanner.
procedure TDialogScanEmployeeF.StartScanningPost(const Code: String);
begin
  ORASystemDM.OracleSession.CheckConnection(True);
  if not ORASystemDM.OracleSession.Connected then
    WErrorLog(SPimsConnectionLost);
  if DeviceSettingsFound then
    CloseScanner;
  StartScanning(Code);
  if DeviceSettingsFound then
    OpenScanner;
  if ORASystemDM.EnableManRegsMultipleJobs then
    BitBtnOkClick(nil);
end; // StartScanningPost

procedure TDialogScanEmployeeF.BitBtnOkClick(Sender: TObject);
begin
  inherited;
  EmployeeNumber := -1;
  if edtEmployeeNumber.Text <> '' then
  begin
    EmployeeNumber := StrToInt(edtEmployeeNumber.Text);
    if ORASystemDM.EnableManRegsMultipleJobs then
    begin
      ChangeJob;
    end;
  end;
end;

procedure TDialogScanEmployeeF.SetDeviceSettingsFound(
  const Value: Boolean);
begin
  FDeviceSettinsFound := Value;
end;

procedure TDialogScanEmployeeF.SetValuePosition(const Value: Integer);
begin
  FValuePosition := Value;
end;

procedure TDialogScanEmployeeF.SetVerifyPosition(const Value: Integer);
begin
  FVerifyPosition := Value;
end;

procedure TDialogScanEmployeeF.SetVerifyString(const Value: String);
begin
  FVerifyString := Value;
end;

procedure TDialogScanEmployeeF.FormShow(Sender: TObject);
var
  R: TRect;
begin
  inherited;
  // 20015302.70 Center window based on owners form
  GetWindowRect(Screen.ActiveForm.Handle, R);
  Width := R.Right - R.Left;
  Left := R.Left + ((R.Right - R.Left) div 2) - (Width div 2);
  Top := R.Top + ((R.Bottom - R.Top) div 2) - (Height div 2);

  DisplayMessage('');
  if ORASystemDM.EnableManRegsMultipleJobs then
  begin
    BitBtnOK.ModalResult := mrNone;
    BitBtnOK.Enabled := True;
    BitBtnCancel.Enabled := True;
    btnAccept.Enabled := True;
  end
  else
  begin
    Height := Height - pnl3.Height;
  end;
  edtIDCard.Text              := '';
  edtEmployeeNumber.Text      := '';
  edtEmployeeDescription.Text := '';
  edtIDCard.SetFocus;
end;

procedure TDialogScanEmployeeF.FormDestroy(Sender: TObject);
begin
  inherited;
  if DeviceSettingsFound then
  begin
    CloseScanner;
    OnCodeRead := nil;
  end;
  // 20013176
  if Assigned(CardReader) then
    CardReader.Destroy;
end;

// 20013176
procedure TDialogScanEmployeeF.CardReaderProcessScan;
begin
  // CardID should be in MyBuffer.
  // Now process the scan.
  if CardReader.MyHexBuffer <> '' then
  begin
    edtIDCard.Text := CardReader.MyHexBuffer;
    StartScanningPost(edtIDCard.Text);
  end;
end; // CardReaderProcessScan

// 20013176
// HID OMNIKEY 5321 Card Reader - MIFARE Classic 1K Card
// TD-23247 Added some delays (Wait) between cardreader-commands.
procedure TDialogScanEmployeeF.ReadCardID_3a;
var
  I: Integer;
  CardReaderIsConnected, TransmitOK: Boolean;
begin
  if not Visible then
    Exit;
  SmartCard := True;
  CardReader.MyBuffer := '';
  CardReader.MyHexBuffer := '';
  // 20013176.2 Connect Shared instead of Exclusive!
  // If needed, we can switch to 'shared' or to 'exclusive'.
  ORASystemDM.Wait(ORASystemDM.CardReaderDelay);
  if ORASystemDM.CardReaderConnectExclusive then
  begin
    CardReader.AddLog('CardReader.ConnectExclusive:');
    CardReaderIsConnected := CardReader.ConnectExclusive;
  end
  else
  begin
    CardReader.AddLog('CardReader.ConnectShared:');
    CardReaderIsConnected := CardReader.ConnectShared;
  end;
  if CardReaderIsConnected then
  begin
    ORASystemDM.Wait(ORASystemDM.CardReaderDelay);
    // Use commands that were read from database.
    for I := 0 to TimeRecordingDM.SmartCardCommandList.Count - 1 do
    begin
      CommandComboBox.Text := TimeRecordingDM.SmartCardCommandList[I];
      TransmitOK := CardReader.Transmit;
      ORASystemDM.Wait(ORASystemDM.CardReaderDelay);
      if not TransmitOK then
        Break;
    end;
    CardReader.Disconnect;
    ORASystemDM.Wait(ORASystemDM.CardReaderDelay);
    // MRA:26-JUN-2012 Truncate the read key if needed.
    if VerifyPosition <> 0 then
      CardReader.MyHexBuffer := Copy(CardReader.MyHexBuffer, 1, VerifyPosition);
    CardReader.AddLog('MyHexBuffer=' + CardReader.MyHexBuffer);
    CardReaderProcessScan;
  end;
end; // ReadCardID_3a

// 20013176
// HID OMNIKEY 5321 Card Reader
// For use with ICard.
procedure TDialogScanEmployeeF.ReadCardID_3b;
var
  I: Integer;
  CardReaderIsConnected, TransmitOK: Boolean;
begin
  if not Visible then
    Exit;
  SmartCard := True;
  CardReader.MyBuffer := '';
  CardReader.MyHexBuffer := '';
  // 20013176.2 Connect Shared instead of Exclusive!
  // If needed, we can switch to 'shared' or to 'exclusive'.
  ORASystemDM.Wait(ORASystemDM.CardReaderDelay);
  if ORASystemDM.CardReaderConnectExclusive then
  begin
    CardReaderIsConnected := CardReader.ConnectExclusive;
  end
  else
  begin
    CardReaderIsConnected := CardReader.ConnectShared;
  end;
  if CardReaderIsConnected then
  begin
    ORASystemDM.Wait(ORASystemDM.CardReaderDelay);
    // Use commands that were read from database.
    for I := 0 to TimeRecordingDM.SmartCardCommandList.Count - 1 do
    begin
      CommandComboBox.Text := TimeRecordingDM.SmartCardCommandList[I];
      TransmitOK := CardReader.TransmitCLICC;
      ORASystemDM.Wait(ORASystemDM.CardReaderDelay);
      if not TransmitOK then
        Break;
    end;
    CardReader.Disconnect;
    ORASystemDM.Wait(ORASystemDM.CardReaderDelay);
    CardReader.AddLog('MyHexBuffer=' + CardReader.MyHexBuffer);
    CardReaderProcessScan;
  end;
end; // ReadCardID_3b

// 20013176
// For use with card reader
procedure TDialogScanEmployeeF.AddErrorLog(AMsg: String);
begin
  WErrorLog(AMsg);
end; // AddErrorLog

// 20013176
// For use with card reader
function TDialogScanEmployeeF.ValueIDCardSmartCard(
  var ACode: String): TErrorCode;
var
  ARawCode: String;
begin
  Result := ecUnknown;  // Unknown error
  ARawCode := ACode;
  with TimeRecordingDM do
  begin
    oqIDCardRaw.ClearVariables;
    oqIDCardRaw.SetVariable('IDCARD_RAW', ARawCode);
    oqIDCardRaw.Execute;
    if not oqIDCardRaw.Eof then
    begin
      ACode := oqIDCardRaw.FieldAsString('IDCARD_NUMBER');
      Result := ecOK;
    end;
  end;
end;

// 20015302
procedure TDialogScanEmployeeF.ChangeJob;
var
  SelectorPos: TFormPos;
  SelectorColumns: array of String;
  SelectRes: Integer;
  ALastIDCard: TScannedIDCard;
  StartDate, EndDate: TDateTime;
  procedure SetSelectorPos(var AFormPos: TFormPos);
  var
    RealHeight: Integer;
    TopPart: Integer;
  begin
    // Subtract the part that is shown at the top
//    AFormPos.Top    := Top + pnlBottom.Top +
//      (Height -
//        pnl11.Height - pnlBottom.Height - sbarMessage.Height);
    TopPart := Height -
      pnl3.Height;// - pnlBottom.Height - sbarMessage.Height;
    RealHeight := Height - TopPart;
    AFormPos.Top    := Top + Round(RealHeight / 2);
    AFormPos.Left   := Left; // pnlBottom.Left;
    AFormPos.Height := Round(RealHeight / 2) + TopPart; // pnlBottom.Height;
    AFormPos.Width  := Width; // pnlBottom.Width;
  end;
begin
  BitBtnOK.Enabled := False;
  BitBtnCancel.Enabled := False;
  btnAccept.Enabled := False;
  SetLength(SelectorColumns, 3);
  SelectorColumns[0] := SPimsJob;
  SelectorColumns[1] := SPimsColumnCode;
  SelectorColumns[2] := SPimsColumnDescription;
//  OldSinceDateTime   := SinceDateTime;
//  OldJobCode         := NewJobCode;

  // Search for previous job based on open scan for the selected employee:
  DialogChangeJobF.OldJobCode := '';
  DialogChangeJobF.DisableCancelTimer := True;
  with ManualRegistrationsDM do
  begin
    if odsTRS.Locate('EMPLOYEE_NUMBER', EmployeeNumber, []) then
      DialogChangeJobF.OldJobCode := odsTRS.FieldByName('JOB_CODE').AsString;
  end;

  // PIM-60 Check for End-Of-Shift - Start
  DialogChangeJobF.EndOfDayBlink := False;
  if DialogChangeJobF.OldJobCode <> '' then // when there was a previous job
  begin
    with ManualRegistrationsDM do
    begin
      EmptyIDCard(ALastIDCard);
      PopulateIDCard(ALastIDCard, odsTRS);
      ALastIDCard.DateIn := Now;
      if ALastIDCard.DateIn > NullDate then
      begin
        AProdMinClass.GetShiftDay(ALastIDCard, StartDate, EndDate);
        ALastIDCard.ShiftStartDateTime := StartDate;
        ALastIDCard.ShiftEndDateTime := EndDate;
      end
      else
      begin
        ALastIDCard.ShiftStartDateTime := NullDate;
        ALastIDCard.ShiftEndDateTime := NullDate;
      end;
      DialogChangeJobF.EndOfDayBlink := EndOfShiftTest(ALastIDCard);
    end;
  end;
  // PIM-60 Check for End-Of-Shift - End

  try
    with ManualRegistrationsDM.odsJobcode do
    begin
      Active := False;
      ClearVariables;
      SetVariable('PLANT_CODE',          PlantCode);
      SetVariable('WORKSPOT_CODE',       WorkSpotCode);
      SetVariable('SHOW_AT_SCANNING_YN', 'Y');
      SetVariable('DATE_INACTIVE',       Trunc(Now));
      Active := True;
      if not Eof then
      begin
        SetSelectorPos(SelectorPos);
        SelectRes := DialogChangeJobF.EnableSelection(SelectorPos, SelectorColumns,
          ManualRegistrationsDM.dsrcJobcode);
        Update;
        case SelectRes of
        ModalOk:
          begin
            NewJobCode         := FieldByName('JOB_CODE').AsString;
            NewJobDescription  := FieldByName('DESCRIPTION').AsString;
            ModalResult := mrOK;
          end;
        ModalEndOfDay: //SetEndOfDay(NextIDCard);
          begin
            NewJobCode         := '';
            NewJobDescription  := '';
            ModalResult := ModalEndOfDay;
          end;
        ModalCancel:
          begin
            ModalResult := mrCancel;
          end;
        ModalMechanicalDown:
          begin
            // Change job to Down-job: 'mechanical down'.
            NewJobCode := MECHANICAL_DOWN_JOB;
            NewJobDescription  := SPimsMechanicalDown;
            ModalResult := mrOK;
        end;
        ModalNoMerchandizeDown:
          begin
            // Change job to Down-job: 'no merchandize'.
            NewJobCode := NO_MERCHANDIZE_JOB;
            NewJobDescription  := SPimsNoMerchandize;
            ModalResult := mrOK;
          end;
        ModalBreak:
          begin
            // Change job to Down-job: 'no merchandize'.
            NewJobCode := BREAK_JOB;
            NewJobDescription  := SPimsBreakJobDescription;
            ModalResult := mrOK;
          end;
        end; // case
      end;
      Active := False;
    end;
  finally
  end;
end;  // ChangeJob

// 20015302
procedure TDialogScanEmployeeF.DisplayMessage(AMsg: String);
begin
  sbarMessage.SimpleText := AMsg;
  pnl3.Caption := AMsg;
end;

// 20015302 Prevent it is moved
procedure TDialogScanEmployeeF.WMSysCommand(var Msg: TWMSysCommand);
begin
  if ((Msg.CmdType and $FFF0) = SC_MOVE) or
    ((Msg.CmdType and $FFF0) = SC_SIZE) then
  begin
    Msg.Result := 0;
    Exit;
  end;
  inherited;
end;

end.
