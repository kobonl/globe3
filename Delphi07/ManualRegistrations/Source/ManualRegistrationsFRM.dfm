inherited ManualRegistrationsF: TManualRegistrationsF
  Left = 245
  Top = 219
  VertScrollBar.Range = 0
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = 'Manual Registrations'
  ClientHeight = 657
  ClientWidth = 788
  Font.Name = 'MS Sans Serif'
  OldCreateOrder = False
  OnClose = FormClose
  OnKeyPress = FormKeyPress
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlImageBase: TPanel
    Width = 788
    TabOrder = 4
    inherited imgBasePims: TImage
      Left = 671
    end
  end
  inherited stbarBase: TStatusBar
    Top = 445
    Width = 788
    Visible = False
  end
  inherited pnlInsertBase: TPanel
    Width = 458
    Height = 403
    TabOrder = 5
  end
  object pnl11: TPanel [3]
    Left = 0
    Top = 42
    Width = 458
    Height = 403
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object gBoxWorkspot: TGroupBox
      Left = 0
      Top = 0
      Width = 458
      Height = 58
      Align = alTop
      Caption = 'Workspot'
      TabOrder = 0
      object Label1: TLabel
        Left = 8
        Top = 18
        Width = 79
        Height = 24
        Caption = 'Workspot'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object pnlWorkspot: TPanel
        Left = 128
        Top = 15
        Width = 326
        Height = 30
        Alignment = taLeftJustify
        BevelOuter = bvNone
        BorderStyle = bsSingle
        Caption = 'pnlWorkspot'
        Color = clWindow
        Ctl3D = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 0
      end
    end
    object gBoxCurrentJob: TGroupBox
      Left = 0
      Top = 58
      Width = 458
      Height = 90
      Align = alTop
      Caption = 'Current Job'
      TabOrder = 1
      object Label2: TLabel
        Left = 8
        Top = 18
        Width = 74
        Height = 24
        Caption = 'Jobcode'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label3: TLabel
        Left = 8
        Top = 54
        Width = 48
        Height = 24
        Caption = 'Since'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lblSince: TLabel
        Left = 128
        Top = 55
        Width = 67
        Height = 24
        Caption = 'lblSince'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object pnlJobcode: TPanel
        Left = 128
        Top = 15
        Width = 326
        Height = 30
        Alignment = taLeftJustify
        BevelOuter = bvNone
        BorderStyle = bsSingle
        Caption = 'pnlJobcode'
        Color = clWindow
        Ctl3D = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 0
      end
    end
    object gBoxEmployees: TGroupBox
      Left = 0
      Top = 148
      Width = 458
      Height = 255
      Align = alClient
      Caption = 'Employees'
      TabOrder = 2
      object dxTLEmployee: TdxTreeList
        Left = 2
        Top = 15
        Width = 454
        Height = 189
        Bands = <
          item
          end>
        DefaultLayout = True
        HeaderPanelRowCount = 1
        Align = alClient
        TabOrder = 0
        TreeLineColor = clGrayText
        ShowGrid = True
        ShowRoot = False
        object dxTLEmployeeColumnEmployee: TdxTreeListColumn
          Caption = 'Employee'
          Width = 174
          BandIndex = 0
          RowIndex = 0
        end
        object dxTLEmployeeColumnDateTimeIn: TdxTreeListColumn
          Caption = 'Date/Time in'
          Width = 141
          BandIndex = 0
          RowIndex = 0
        end
        object dxTLEmployeeColumnJobcode: TdxTreeListColumn
          Caption = 'Jobcode'
          Width = 134
          BandIndex = 0
          RowIndex = 0
        end
      end
      object pnlScanButtons: TPanel
        Left = 2
        Top = 204
        Width = 454
        Height = 49
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 1
        object btnScanAllToCurJob: TButton
          Left = 6
          Top = 5
          Width = 225
          Height = 41
          Caption = 'Scan all to current job'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          OnClick = btnScanAllToCurJobClick
        end
        object btnScanEmp: TButton
          Left = 237
          Top = 6
          Width = 214
          Height = 39
          Caption = 'Scan employee'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
          OnClick = btnScanEmpClick
        end
      end
    end
  end
  object pnl12: TPanel [4]
    Left = 458
    Top = 42
    Width = 330
    Height = 403
    Align = alRight
    BevelOuter = bvNone
    TabOrder = 1
    object gBoxDateTime: TGroupBox
      Left = 0
      Top = 0
      Width = 330
      Height = 58
      Align = alTop
      Caption = 'Date / Time'
      TabOrder = 0
      object lblDateTime: TLabel
        Left = 8
        Top = 23
        Width = 86
        Height = 16
        Caption = 'lblDateTime'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
    end
    object pnlCalculator: TPanel
      Left = 0
      Top = 58
      Width = 330
      Height = 345
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
    end
  end
  object pnlBottom: TPanel [5]
    Left = 0
    Top = 464
    Width = 788
    Height = 174
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    object lblAddProduction: TLabel
      Left = 472
      Top = 15
      Width = 221
      Height = 24
      Caption = '+ store production quantity'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object btnChangeJob: TButton
      Left = 216
      Top = 7
      Width = 241
      Height = 41
      Caption = 'Change job'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -24
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      OnClick = btnChangeJobClick
    end
  end
  object sbarMessage: TStatusBar [6]
    Left = 0
    Top = 638
    Width = 788
    Height = 19
    Panels = <
      item
        Text = '  Gotli Labs'
        Width = 190
      end
      item
        Width = 480
      end
      item
        Width = 50
      end>
  end
  inherited pnlTopLine: TPanel
    Width = 788
  end
  inherited mmPims: TMainMenu
    inherited miFile1: TMenuItem
      inherited Settings1: TMenuItem
        Visible = False
      end
    end
  end
  object OneSolar1: TOneSolar
    Left = 648
    Top = 8
  end
  object tmrShowTime: TTimer
    OnTimer = tmrShowTimeTimer
    Left = 714
    Top = 16
  end
  object tmrRefreshScans: TTimer
    Enabled = False
    Interval = 30000
    OnTimer = tmrRefreshScansTimer
    Left = 746
    Top = 16
  end
end
