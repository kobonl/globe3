(*
  MRA:7-NOV-2013 TD-23355
  - Update Manual Registrations Application
  MRA:13-APR-2015 20016449
  - Related to this order: Add ClientName parameter
*)
program ManualRegistrations;

uses
  Forms,
  Windows,
  SysUtils,
  BasePimsFRM in '..\..\Pims Shared\BasePimsFRM.pas' {BasePimsForm},
  BasePimsSerialFRM in '..\..\Pims Shared\BasePimsSerialFRM.pas' {BasePimsSerialForm},
  BaseTopMenuFRM in '..\..\Pims Shared\BaseTopMenuFRM.pas' {BaseTopMenuForm},
  BaseTopLogoFRM in '..\..\Pims Shared\BaseTopLogoFRM.pas' {BaseTopLogoForm},
  BaseDialogFRM in '..\..\Pims Shared\BaseDialogFRM.pas' {BaseDialogForm},
  AboutFRM in '..\..\Pims Shared\AboutFRM.pas' {AboutForm},
  OrderInfoFRM in '..\..\Pims Shared\OrderInfoFRM.pas' {OrderInfoForm},
  UPimsConst in '..\..\Pims Shared\UPimsConst.pas',
  UPimsMessageRes in '..\..\Pims Shared\UPimsMessageRes.pas',
  UPimsVersion in '..\..\Pims Shared\UPimsVersion.pas',
  UScannedIDCard in '..\..\Pims Shared\UScannedIDCard.pas',
  SplashFRM in '..\..\Pims Shared\SplashFRM.pas' {SplashForm},
  ORASystemDMT in '..\..\Pims Shared\ORASystemDMT.pas' {ORASystemDM: TDataModule},
  CalculateTotalHoursDMT in '..\..\Pims Shared\CalculateTotalHoursDMT.pas' {CalculateTotalHoursDM, TDataModule},
  UGlobalFunctions in '..\..\Pims Shared\UGlobalFunctions.pas',
  ManualRegistrationsFRM in 'ManualRegistrationsFRM.pas' {ManualRegistrationsF},
  ManualRegistrationsDMT in 'ManualRegistrationsDMT.pas' {ManualRegistrationsDM: TDataModule},
  DialogScanEmployeeFRM in 'DialogScanEmployeeFRM.pas' {DialogScanEmployeeF},
  CalculatorFRM in 'CalculatorFRM.pas' {CalculatorF},
  GlobalDMT in '..\..\Pims Shared\GlobalDMT.pas' {GlobalDM: TDataModule},
  DialogChangeJobFRM in 'DialogChangeJobFRM.pas' {DialogChangeJobF},
  UPCSCCardReader in '..\..\TimeRecording\Source\UPCSCCardReader.pas',
  TimeRecordingDMT in '..\..\TimeRecording\Source\TimeRecordingDMT.pas' {TimeRecordingDM: TDataModule},
  UProductionScreenDefs in '..\..\Pims Shared\UProductionScreenDefs.pas',
  UTranslateManReg in 'UTranslateManReg.pas',
  UTranslateStringsManReg in 'UTranslateStringsManReg.pas',
  DialogSelectDownTypeFRM in '..\..\Pims Shared\DialogSelectDownTypeFRM.pas' {DialogSelectDownTypeF},
  UTimeZone in '..\..\Pims Shared\UTimeZone.pas',
  ColorButton in '..\..\Pims Shared\ColorButton.pas',
  RealTimeEffDMT in '..\..\Pims Shared\RealTimeEffDMT.pas' {RealTimeEffDM: TDataModule};

{$R *.RES}

// How to get an alternative CLIENTNAME based on param?
function NewClientName: String;
var
  I, IPos: Integer;
  Param1, PStr: String;
  function MySetEnvironmentVariable(AName, AValue: String): Boolean;
  begin
    Result := SetEnvironmentVariable(PChar(AName), PChar(AValue));
  end;
begin
  Result := '';
  Param1 := '-C:';
  for I := 1 to ParamCount do
  begin
    PStr := UpperCase(ParamStr(I));
    IPos := Pos(Param1, PStr);
    if IPos > 0 then
    begin
      Result := Copy(PStr, IPos + Length(Param1), Length(Pstr));
      Break;
    end;
  end;
  if Result <> '' then
    MySetEnvironmentVariable('CLIENTNAME', Result);
end;

begin
  Application.Initialize;
  NewClientName;
  Application.CreateForm(TORASystemDM, ORASystemDM);
  Application.CreateForm(TRealTimeEffDM, RealTimeEffDM);
  ORASystemDM.AppName := APPNAME_MANREG;
  Application.CreateForm(TCalculateTotalHoursDM, CalculateTotalHoursDM);
  Application.CreateForm(TGlobalDM, GlobalDM);
  Application.CreateForm(TTimeRecordingDM, TimeRecordingDM);
  Application.CreateForm(TManualRegistrationsDM, ManualRegistrationsDM);
  Application.CreateForm(TManualRegistrationsF, ManualRegistrationsF);
  Application.CreateForm(TDialogSelectDownTypeF, DialogSelectDownTypeF);
  DialogSelectDownTypeF.Visible := False;
  ATranslateHandlerManReg.TranslateAll;
  Application.Run;
end.
