(*
  MRA:7-NOV-2013 TD-23355
  - Update Manual Registrations Application
  - tblTRS is not used anymore for performance reasons
  MRA:18-JUL-2014 SO-20015302
  - Sort on default job (odsJobcode changed).
  MRA:03-OCT-2014 SC-50036915 TD-25865 RSS
  - Some employees cannot be scanned in and others can.
  - In log file there is a message about:
    - TOracleQuery: First only possible if scrollable is true
  - Try to solve this by not using 'First' in code.
  MRA:10-OCT-2014 SO-20015569
  - Show extra buttons for Down/Break/Lunch
  MRA:24-OCT-2014 TD-25984
  - Check on open scans issue
  - It does not check for any open scans before it is about to create a new
    open scan. This can result in two open scans when employee was already
    scanned in via Timerecording (but on a different workspot).
  - To solve this: Always look for any open scan for the employee who changes
    job and close that first before creating the new open scan.
  MRA:8-JUL-2015 PIM-45
  - When a qty is about to be stored, check if start- and end-date are the same.
    If this is the case then first add 1 minute to end-date before the qty
    is stored.
  - Also: oqIDCardInit did not always get all needed data like idcard and 
    department-code, because it used the plant of the employee as the 
    workspot-plant, but that can be different from the scan!
  MRA:28-JUL-2015 PIM-60
  - End-Of-Day-button blink at end of shift
  - Addition of 2 extra fields to odsTRS and to oqFindScan:
    - IDCARD_IN, SHIFT_DATE
  - Addition of 2 extra fields to ScanRecord. These are also filled.
    - IDCardIn, ShiftDate
  MRA:19-JUL-2016 PIM-209
  - Manual Registrations and qty not added.
  - When a qty is added for the same job within the same period, it does
    not add this to PQ-table, but replaces it with the last value.
    This gives a difference with BI-tables, because there it is added.
  - To solve it: oqPRMerge should add the value for the same job during update!
*)
unit ManualRegistrationsDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ORASystemDMT, Db, Provider, DBClient, UScannedIDCard, Variants,
  Oracle, OracleData, DateUtils;

type
  TScanRecord = record
    PlantCode: String;
    WorkspotCode: String;
    JobCode: String;
    EmployeeNumber: Integer;
    ShiftNumber: Integer;
    DateTimeIn: TDateTime;
    DateTimeOut: TDateTime;
    IDCardIn: String; // PIM-60
    ShiftDate: TDateTime; // PIM-60
end;

type
  TManualRegistrationsDM = class(TDataModule)
    oqComportConnection: TOracleQuery;
    oqEmployeeIDCard: TOracleQuery;
    odsJobcode: TOracleDataSet;
    dsrcJobcode: TDataSource;
    oqWorkspotPerWorkstation: TOracleQuery;
    oqPQInsert: TOracleQuery;
    oqIDCardInit: TOracleQuery;
    oqTRSInsert: TOracleQuery;
    odsTRS: TOracleDataSet;
    oqDeterminePlannedWorkspot: TOracleQuery;
    oqDetermineShiftFromShiftSchedule: TOracleQuery;
    oqDetermineShiftByPlant: TOracleQuery;
    oqTRSUpdateJob: TOracleQuery;
    oqTRSCloseScan: TOracleQuery;
    oqPQMerge: TOracleQuery;
    oqCheckJob: TOracleQuery;
    oqInsertJob: TOracleQuery;
    oqFindScan: TOracleQuery;
  private
    { Private declarations }
    Dates: array of TDateTime;
    procedure DetermineScheduledTimeblocks(
      AQuery: TOracleQuery;
      const AShiftNumber: Integer;
      const FillShiftNumber: Boolean;
      var APlannedWorkspot, APlannedPlant, APlannedDepartment: String;
      var APlannedShiftNumber: Integer;
      var ANextIDCard: TScannedIDCard;
      var TBEnd: TDateTime);
    function DetermineTBTimes(
      const APlantCode, ADepartmentCode: String;
      const AEmployeeNumber, AShiftNumber: Integer;
      const ADateIn: TDateTime;
      Where: String): Boolean;
    function DetermineTimeBlocks(
      const APlantCode, ADepartmentCode: String;
      const AEmployeeNumber, AShiftNumber: Integer;
      const Where: String;
      var ADataSet: TDataSet): Boolean;
  public
    { Public declarations }
    procedure FillIDCardInit(var AIDCard: TScannedIDCard;
      const APlantCode, AWorkspotCode, AJobCode: String; const AEmployeeNumber: Integer);
    procedure FillIDCard(var IDCard: TScannedIDCard);
    procedure SelectShift(var ShiftNumber: Integer;
      AIDcard: TScannedIDCard; EmployeeNumber: Integer; DateFrom: TDateTime);
    procedure UpdateTRSJob(AEmployeeNumber: Integer; ADateIn: TDateTime;
      AJobCode: String);
    procedure UpdateTRSCloseScan(AEmployeeNumber: Integer; ADateIn,
      ADateOut: TDateTime; AIDCardOut: String; AShiftDate: TDateTime);
    procedure ActionAddMissingJobForWorkspot(APlantCode, AWorkspotCode,
      AJobCode, AJobDescription: String);
    function FindScan(AEmployeeNumber: Integer): Boolean;
    procedure AssignScanRecord;
    function EndOfShiftTest(ANextIDCard: TScannedIDCard): Boolean;
  end;

var
  ManualRegistrationsDM: TManualRegistrationsDM;
  AScanRecord: TScanRecord;

implementation

{$R *.DFM}

uses
  CalculateTotalHoursDMT, UGlobalFunctions, UPimsConst;

procedure TManualRegistrationsDM.FillIDCardInit(var AIDCard: TScannedIDCard;
  const APlantCode, AWorkspotCode, AJobCode: String;
  const AEmployeeNumber: Integer);
begin
  AIDCard.EmployeeCode := AEmployeeNumber;
  AIDCard.WorkSpotCode := AWorkspotCode;
  AIDCard.PlantCode    := APlantCode;
  AIDCard.JobCode      := AJobCode;
  with oqIDCardInit do // use 1 query instead of 4 queries
  begin
    ClearVariables;
    SetVariable('EMPLOYEE_NUMBER', AEmployeeNumber);
    SetVariable('PLANT_CODE', APlantCode);
    SetVariable('WORKSPOT_CODE', AWorkspotCode);
    SetVariable('JOB_CODE', AJobCode);
    Execute;
    if not Eof then
    begin
      AIDCard.InscanEarly    := FieldAsInteger('INSCAN_MARGIN_EARLY');
      AIDCard.Inscanlate     := FieldAsInteger('INSCAN_MARGIN_LATE');
      AIDCard.OutscanEarly   := FieldAsInteger('OUTSCAN_MARGIN_EARLY');
      AIDCard.OutScanLate    := FieldAsInteger('OUTSCAN_MARGIN_LATE');
      AIDCard.CutOfTime      := FieldAsString('CUT_OF_TIME_YN');
      AIDCard.IDCard         := FieldAsString('IDCARD_NUMBER');
      AIDCard.DepartmentCode := FieldAsString('DEPARTMENT_CODE');
      AIDCard.NormProdLevel  := FieldAsFloat('NORM_PROD_LEVEL');
    end;
  end;{with oqIDCardInit}
end;

procedure TManualRegistrationsDM.FillIDCard(var IDCard: TScannedIDCard);
begin
  // TD-25984 In AScanRecord the found scan-info must be assigned.
  FillIDCardInit(IDCard,
    AScanrecord.PlantCode,
    AScanRecord.WorkspotCode,
    AScanRecord.JobCode,
    AScanRecord.EmployeeNumber
    );

  IDCard.ShiftNumber := AScanRecord.ShiftNumber;
  IDCard.JobCode     := AScanrecord.JobCode;
  IDCard.DateIn      := AScanRecord.DateTimeIn;
  IDCard.DateOut     := AScanRecord.DateTimeOut;
  IDCard.IDCard      := AScanRecord.IDCardIn;
  IDCard.ShiftDate   := AScanRecord.ShiftDate;
end;

procedure TManualRegistrationsDM.DetermineScheduledTimeblocks(
  AQuery: TOracleQuery;
  const AShiftNumber: Integer;
  const FillShiftNumber: Boolean;
  var APlannedWorkspot, APlannedPlant, APlannedDepartment: String;
  var APlannedShiftNumber: Integer;
  var ANextIDCard: TScannedIDCard;
  var TBEnd: TDateTime);
var
  Where: String;
  Index: Integer;
begin
  Where := '';
  for Index := 1 to MaxTimeBlocks do
  begin
    if AQuery.FieldAsString('SCHEDULED_TIMEBLOCK_' + IntToStr(Index))[1]
      in ['A','B','C'] then
    begin
      if Where = '' then
        Where := ' AND (TIMEBLOCK_NUMBER = ' + IntToStr(Index)
      else
        Where := Where + ' OR TIMEBLOCK_NUMBER = ' + IntToStr(Index);
    end;
  end;
  if Where <> '' then
    Where := Where + ')';
  // MR:09-09-2004 Optimising
  DetermineTBTimes(
    AQuery.FieldAsString('PLANT_CODE'),
    AQuery.FieldAsString('DEPARTMENT_CODE'),
    ANextIDCard.EmployeeCode,
    AShiftNumber,
    ANextIDCard.DateIn,
    Where);
  // Test for correct planning
  for Index := 0 to Length(Dates) - 1 do   // find min date
  begin
    if (Dates[Index] > ANextIDCard.DateIn) and
      ((Dates[Index] <= TBEnd) or (TBEnd = 0)) then
    begin
      APlannedWorkspot   := AQuery.FieldAsString('WORKSPOT_CODE');
      APlannedPlant      := AQuery.FieldAsString('PLANT_CODE');
      APlannedDepartment := AQuery.FieldAsString('DEPARTMENT_CODE');
      // MR:05-12-2002
      if FillShiftNumber then
        APlannedShiftNumber := AQuery.FieldAsInteger('SHIFT_NUMBER');
      TBEnd := Dates[Index];
    end;
  end;
end;

// MR:09-09-2004 Optimise
function TManualRegistrationsDM.DetermineTBTimes(
  const APlantCode, ADepartmentCode: String;
  const AEmployeeNumber, AShiftNumber: Integer;
  const ADateIn: TDateTime;
  Where: String): Boolean;
var
  ADataSet: TDataSet;
  Normal: Integer;
begin
  SetLength(Dates, 0);
  if DetermineTimeBlocks(
    APlantCode, ADepartmentCode,
    AEmployeeNumber, AShiftNumber,
    Where,
    ADataSet) then
  begin
    Result := True;
    Where := IntToStr(PimsDayOfWeek(ADateIn)); // NextIDCard.DateIn
//    ADataSet.First; // SC-50036915 TD-25865
    while not ADataSet.Eof do
    begin
      SetLength(Dates, Length(Dates) + 1);
      Normal := Length(Dates) - 1;
      Dates[Normal]:=
        ADataSet.FieldByName('ENDTIME' + Where).AsDateTime;
      if ADataSet.FieldByName('STARTTIME' + Where).AsDateTime >
        ADataSet.FieldByName('ENDTIME' + Where).AsDateTime then
        Dates[Normal] := Dates[Normal] + 1 // next day
      else
        ReplaceDate(Dates[Normal], ADateIn); // NextIDCard.DateIn
      ADataSet.Next;
    end;
  end
  else
  begin
    // No TB-Times could be found.
    Result := False;
  end;
end;
          
// MR:09-09-2004 Optimise
// Get TimeBlock-data from clientdatasets that are available
// in CalculateTotalHoursDMT, instead of retrieving the data
// here again.
function TManualRegistrationsDM.DetermineTimeBlocks(
  const APlantCode, ADepartmentCode: String;
  const AEmployeeNumber, AShiftNumber: Integer;
  const Where: String;
  var ADataSet: TDataSet): Boolean;
begin
  // Timeblocks Per Employee
  ADataSet := CalculateTotalHoursDM.ClientDataSetTBEmpl;
  ADataSet.Filtered := False;
  ADataSet.Filter :=
    'PLANT_CODE = ' + '''' + DoubleQuote(APlantCode) + '''' +
    ' AND ' +
    'EMPLOYEE_NUMBER = ' + IntToStr(AEmployeeNumber) + ' AND ' +
    'SHIFT_NUMBER = ' + IntToStr(AShiftNumber) +
    Where;
  ADataSet.Filtered := True;
  Result := not ADataSet.IsEmpty;

  // Timeblocks Per Department
  if not Result then
  begin
    // Timeblocks per Department
    ADataSet := CalculateTotalHoursDM.ClientDataSetTBDept;
    ADataSet.Filtered := False;
    ADataSet.Filter :=
      'PLANT_CODE = ' + '''' + DoubleQuote(APlantCode) +
      '''' + ' AND ' +
      'DEPARTMENT_CODE = ''' + DoubleQuote(ADepartmentCode) +
      '''' + ' AND ' +
      'SHIFT_NUMBER = ' + IntToStr(AShiftNumber) +
      Where;
    ADataSet.Filtered := True;
    Result := not ADataSet.IsEmpty;

    // Timeblocks Per Shift
    if not Result then
    begin
      // Timeblocks Per Shift
      ADataSet := CalculateTotalHoursDM.ClientDataSetTBShift;
      ADataSet.Filtered := False;
      ADataSet.Filter :=
        'PLANT_CODE = ' + '''' + DoubleQuote(APlantCode) +
        '''' + ' AND ' +
        'SHIFT_NUMBER = ' + IntToStr(AShiftNumber) +
        Where;
      ADataSet.Filtered := True;
      Result := not ADataSet.IsEmpty;
    end;
  end;
end;

procedure TManualRegistrationsDM.SelectShift(var ShiftNumber: Integer;
  AIDcard: TScannedIDCard; EmployeeNumber: Integer; DateFrom: TDateTime);
const
  MaxTimeBlocks=4;
var
  StartDate, EndDate, TBEnd: TDateTime;
  MyPlannedWorkSpot, MyPlannedPlant, MyPlannedDepartment: String;
  MyPlannedShiftNumber: Integer;
  ShiftFound: Boolean;
begin
  // Select Next WorkSpot
  // Find the planned WorkSpot.
  StartDate := DateFrom;
  GetStartEndDay(StartDate, EndDate);
  // get this from TimeRecording
  with oqDeterminePlannedWorkspot do
  begin
    ClearVariables;
    SetVariable('STARTDATE',       StartDate);
    SetVariable('ENDDATE',         EndDate);
    SetVariable('EMPLOYEE_NUMBER', EmployeeNumber);
    Execute;
    MyPlannedWorkSpot   := NullStr;
    MyPlannedPlant      := NullStr;
    MyPlannedDepartment := NullStr;
    // MR:05-12-2002
    MyPlannedShiftNumber := -1;
    TBEnd := 0;
    if not Eof then
    begin
//      First;  SC-50036915 TD-25865 // for all records ??? the last can overwrite all
      while not Eof do
      begin
        DetermineScheduledTimeblocks(oqDeterminePlannedWorkspot,
          FieldAsInteger('SHIFT_NUMBER'),
          True,
          MyPlannedWorkspot, MyPlannedPlant, MyPlannedDepartment,
          MyPlannedShiftNumber, AIDCard,
          TBEnd);
        Next;
      end;
    end;
  end; {with TimeRecordingDM.oqDeterminePlannedWorkspot}
  // Select SHIFT
  if MyPlannedShiftNumber <> -1 then
    ShiftNumber := MyPlannedShiftNumber
  else
  begin
    ShiftFound := False;
    // MR:03-01-2003 There is no planned-shiftnumber,
    // so get it from table SHIFTSCHEDULE
    with oqDetermineShiftFromShiftSchedule do
    begin
      ClearVariables;
      SetVariable('EMPLOYEE_NUMBER',     EmployeeNumber);
      SetVariable('SHIFT_SCHEDULE_DATE', Trunc(DateFrom));
      Execute;
      if not Eof then
      begin
        if not FieldIsNull('SHIFT_NUMBER') and
          (FieldAsInteger('SHIFT_NUMBER') <> -1) then
        begin
          ShiftNumber := FieldAsInteger('SHIFT_NUMBER');
          ShiftFound  := True;
        end;
      end;
    end; {with oqDetermineShiftFromShiftSchedule}
    if not ShiftFound then
    begin
      // Select SHIFT
      with oqDetermineShiftByPlant do
      begin
        ClearVariables;
        SetVariable('PLANT_CODE', AIDCard.PlantCode);
        Execute;
        if not Eof then
          ShiftNumber := FieldAsInteger('SHIFT_NUMBER');
      end; {with oqDetermineShiftByPlant}
    end; {not ShiftFound}
  end; {else MyPlannedShiftNumber <> -1 }
end;

procedure TManualRegistrationsDM.UpdateTRSJob(AEmployeeNumber: Integer;
  ADateIn: TDateTime; AJobCode: String);
begin
  with oqTRSUpdateJob do
  begin
    ClearVariables;
    SetVariable('EMPLOYEE_NUMBER', AEmployeeNumber);
    SetVariable('DATETIME_IN', ADateIn);
    SetVariable('JOB_CODE', AJobCode);
    SetVariable('MUTATOR', ORASystemDM.CurrentProgramUser);
    Execute;
  end;
end; // UpdateTRSJob

procedure TManualRegistrationsDM.UpdateTRSCloseScan(
  AEmployeeNumber: Integer; ADateIn, ADateOut: TDateTime;
  AIDCardOut: String;
  AShiftDate: TDateTime);
begin
  with oqTRSCloseScan do
  begin
    ClearVariables;
    SetVariable('EMPLOYEE_NUMBER', AEmployeeNumber);
    SetVariable('DATETIME_IN', ADateIn);
    SetVariable('DATETIME_OUT', ADateOut);
    SetVariable('PROCESSED_YN', CHECKEDVALUE);
    SetVariable('IDCARD_OUT', AIDCardOut);
    SetVariable('SHIFT_DATE', AShiftDate);
    SetVariable('MUTATOR', ORASystemDM.CurrentProgramUser);
    Execute;
  end;
end; // UpdateTRSCloseScan

procedure TManualRegistrationsDM.ActionAddMissingJobForWorkspot(APlantCode,
  AWorkspotCode, AJobCode, AJobDescription: String);
begin
  try
    oqCheckJob.ClearVariables;
    oqCheckJob.SetVariable('PLANT_CODE',    APlantCode);
    oqCheckJob.SetVariable('WORKSPOT_CODE', AWorkspotCode);
    oqCheckJob.SetVariable('JOB_CODE',      AJobCode);
    oqCheckJob.Execute;
    if oqCheckJob.Eof then
    begin
      // Add the job for the workspot.
      oqInsertJob.ClearVariables;
      oqInsertJob.SetVariable('PLANT_CODE',    APlantCode);
      oqInsertJob.SetVariable('WORKSPOT_CODE', AWorkspotCode);
      oqInsertJob.SetVariable('JOB_CODE',      AJobCode);
      oqInsertJob.SetVariable('DESCRIPTION',   AJobDescription);
      oqInsertJob.SetVariable('MUTATOR',       ORASystemDM.CurrentProgramUser);
      oqInsertJob.Execute;
      if ORASystemDM.OracleSession.InTransaction then
        ORASystemDM.OracleSession.Commit;
    end; // if
  except
    on E: Exception do
    begin
      if ORASystemDM.OracleSession.InTransaction then
        ORASystemDM.OracleSession.Rollback;
      WErrorLog(E.Message);
    end;
  end; // except
end; // ActionAddMissingJobForWorkspot

procedure TManualRegistrationsDM.AssignScanRecord;
begin
  with odsTRS do
  begin
    AScanRecord.PlantCode := FieldByName('PLANT_CODE').AsString;
    AScanRecord.WorkspotCode := FieldByName('WORKSPOT_CODE').AsString;
    AScanRecord.JobCode := FieldByName('JOB_CODE').AsString;
    AScanRecord.EmployeeNumber := FieldByName('EMPLOYEE_NUMBER').AsInteger;
    AScanRecord.ShiftNumber := FieldByName('SHIFT_NUMBER').AsInteger;
    AScanRecord.DateTimeIn := FieldByName('DATETIME_IN').AsDateTime;
    AScanRecord.DateTimeOut := FieldByName('DATETIME_OUT').AsDateTime;
    AScanRecord.IDCardIn := FieldByName('IDCARD_IN').AsString; // PIM-60
    AScanRecord.ShiftDate := FieldByName('SHIFT_DATE').AsDateTime; // PIM-60
  end;
end;

// TD-25984
function TManualRegistrationsDM.FindScan(
  AEmployeeNumber: Integer): Boolean;
var
  DateFrom, DateTo: TDateTime;
begin
  Result := False;
  // Search for any open scan here
  // For last 12 hours
  DateFrom := Now - 0.5;
  DateTo := IncMinute(Now, 5);
  with oqFindScan do
  begin
    ClearVariables;
    SetVariable('EMPLOYEE_NUMBER', AEmployeeNumber);
    SetVariable('DATEFROM', DateFrom);
    SetVariable('DATETO', DateTo);
    Execute;
    if not Eof then
    begin
      AScanRecord.PlantCode := FieldAsString('PLANT_CODE');
      AScanRecord.WorkspotCode := FieldAsString('WORKSPOT_CODE');
      AScanRecord.JobCode := FieldAsString('JOB_CODE');
      AScanRecord.EmployeeNumber := FieldAsInteger('EMPLOYEE_NUMBER');
      AScanRecord.ShiftNumber := FieldAsInteger('SHIFT_NUMBER');
      AScanRecord.DateTimeIn := FieldAsDate('DATETIME_IN');
      AScanRecord.DateTimeOut := FieldAsDate('DATETIME_OUT');
      AScanRecord.IDCardIn := FieldAsString('IDCARD_IN');
      AScanRecord.ShiftDate := FieldAsDate('SHIFT_DATE');
      Result := True;
    end;
  end;
end; // FindScan

// PIM-60
function TManualRegistrationsDM.EndOfShiftTest(
  ANextIDCard: TScannedIDCard): Boolean;
begin
  Result := False;
  if (ANextIDCard.ShiftStartDateTime > NullDate) then
    if (ANextIDCard.DateIn >= ANextIDCard.ShiftEndDateTime) and
      (ANextIDcard.DateIn <= (ANextIDCard.ShiftEndDateTime + 1/24)) then // Add 1 hour to end
        Result := True;
end;

end.
