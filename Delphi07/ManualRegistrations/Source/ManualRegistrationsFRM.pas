(*
  MRA:7-NOV-2013 TD-23355
  - Update Manual Registrations Application
  - Some technical notes:
    - The ClientDataSet (cdsTRS) gave problems, this has been removed!
    - It used a 'locate' to search for a scan. This is too slow!!! So
      it has been removed. This means also the updating of scan-records
      is now done using Update-statements.
  MRA:4-APR-2014 TD-24655
  - Manual Registrations does not store quantities when there is
    only 1 job defined.
  MRA:14-JUL-2014 20015302
  - Based on workstation-setting (EnableManRegsMultipleJobs) register
    quantities for multiple jobs or not.
  MRA:18-JUL-2014 20015302.70 Rework
  - Inherited this form from 'TBaseTopLogoForm' instead of TForm,
    to change the style of the form.
  MRA:03-OCT-2014 SC-50036915 RSS TD-25865
  - Some employees cannot be scanned in and others can.
  - In log file there is a message about:
    - TOracleQuery: First only possible if scrollable is true
  - Try to solve this by not using 'First' in code.
  MRA:10-OCT-2014 SO-20015569
  - Show extra buttons for Down/Break/Lunch
  - Single Job-Mode did not function correct:
    - The 'odsTRS' needed to be refreshed because First was not used anymore
      because of TD-25865 (see also below).
  MRA:14-OCT-2014 TD-25865-Rework
  - Refresh added because it did not do that after removing a First-statement.
  MRA:24-OCT-2014 TD-25984
  - Check on open scans issue
  - It does not check for any open scans before it is about to create a new
    open scan. This can result in two open scans when employee was already
    scanned in via Timerecording (but on a different workspot).
  - To solve this: Always look for any open scan for the employee who changes
    job and close that first before creating the new open scan.
  MRA:18-MAY-2015 PIM-10
  - Hours registered incorrectly
    - The production hours are wrong, they can be higher than they should be.
  MRA:8-JUL-2015 PIM-12
  - Real Time Efficiency (buttons/colors changed)
  MRA:8-JUL-2015 PIM-45
  - When a qty is about to be stored, check if start- and end-date are the same.
    If this is the case then first add 1 minute to end-date before the qty
    is stored.
  MRA:28-JUL-2015 PIM-60
  - End-Of-Day-button blink at end of shift
  MRA:28-SEP-2015 PIM-12
  - Optionally show Break/Lunch/EndOfDay-buttons.
  MRA:2-DEC-2015 ABS-18715
  - No employee: Sometimes it shows no-employee in production detail report
  - Cause: It did not determine/set the Shift-Date in production-quantity-table!
  - Note: For the scans it was set correct, but not for the quantities!
  MRA:14-MAR-2016 PIM-12 / PIM-156
  - Store quantities in BI-table (workspot-eff-per-minute)
  MRA:3-JUN-2016 PIM-156.2
  - Bugfix:
    - It did not always calculated the efficiency.
    - It must be done after a whole minute!
    - To solve it, it keeps track of the time and does it after a whole
      minute has past. Also it shows message when it is busy, and
      when user wants to close the application it will wait till
      all has been done because of calculating the efficiency.
  MRA:28-JUL-2016 PIM-209.2
  - Bugfix:
    - It must check if last scan could be close. Only then it should
      store the quantity that was entered.
*)
unit ManualRegistrationsFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  OneSolar, ORASystemDMT, ManualRegistrationsDMT, ExtCtrls, StdCtrls, dxTL,
  dxCntner, DB, ComCtrls, dxExEdtr, UScannedIDCard, Variants, DateUtils,
  CalculateTotalHoursDMT, BaseTopLogoFRM, Menus, StdActns, ActnList,
  ImgList, OracleData, Oracle, jpeg, UPimsConst;

type
  TManualRegistrationsF = class(TBaseTopLogoForm)
    OneSolar1: TOneSolar;
    pnl11: TPanel;
    pnl12: TPanel;
    pnlBottom: TPanel;
    btnChangeJob: TButton;
    gBoxWorkspot: TGroupBox;
    gBoxCurrentJob: TGroupBox;
    gBoxEmployees: TGroupBox;
    gBoxDateTime: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    lblSince: TLabel;
    lblDateTime: TLabel;
    dxTLEmployee: TdxTreeList;
    dxTLEmployeeColumnEmployee: TdxTreeListColumn;
    dxTLEmployeeColumnDateTimeIn: TdxTreeListColumn;
    dxTLEmployeeColumnJobcode: TdxTreeListColumn;
    tmrShowTime: TTimer;
    sbarMessage: TStatusBar;
    pnlCalculator: TPanel;
    lblAddProduction: TLabel;
    tmrRefreshScans: TTimer;
    pnlWorkspot: TPanel;
    pnlJobcode: TPanel;
    pnlScanButtons: TPanel;
    btnScanAllToCurJob: TButton;
    btnScanEmp: TButton;
    procedure FormCreate(Sender: TObject);
    procedure tmrShowTimeTimer(Sender: TObject);
    procedure btnChangeJobClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnScanAllToCurJobClick(Sender: TObject);
    procedure btnScanEmpClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure tmrRefreshScansTimer(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
  private
    FNewJobDescription: String;
    FWorkspotCode: String;
    FPlantCode: String;
    FSinceDateTime: TDateTime;
    FNewJobCode: String;
    FProduction: Double;
    FOldJobCode: String;
    FOldSinceDateTime: TDateTime;
    FCalculatorEnabled: Boolean;
    FScannedEmpCount: Integer;
    MyLastIDCard: TScannedIDCard;
    { Private declarations }
    function GetFirstWorkspot: Boolean;
    procedure GetScannedEmployees;
    procedure UpdateScannings(const AEndOfDay: Boolean;
      const AEmployeeNumber: Integer; ACreateProduction: Boolean;
      const AUpdateSince: Boolean);
    procedure ChangeJob;
    procedure ChangeEmpAndJob;
    function DetermineShiftDate(AIDCard: TScannedIDCard; AStartDate,
      AEndDate: TDateTime): TDateTime;
    procedure CreateProductionRecord(const AIDCard: TScannedIDCard;
      const ASameJob: Boolean);
    procedure UpdateWorkspotEffPerMinuteTable(const AIDCard: TScannedIDCard;
      const ASameJob: Boolean); // PIM-156
    procedure CreateScanRecord(AIDCard: TScannedIDCard);
    function CloseScanRecord(AIDCard: TScannedIDCard): Boolean;
    procedure CreateScanAction(const EmployeeNumber: Integer);
    procedure StatusInfo(AMsg: String);
    procedure EnableCalculator(const AOn: Boolean);
    procedure InitCalculator;
    procedure EnableTimers(const AOn: Boolean);
    procedure SetCalculatorEnabled(const Value: Boolean);
    procedure SetNewJobCode(const Value: String);
    procedure SetOldJobCode(const Value: String);
    procedure SetOldSinceDateTime(const Value: TDateTime);
    procedure SetPlantCode(const Value: String);
    procedure SetProduction(const Value: Double);
    procedure SetScannedEmpCount(const Value: Integer);
    procedure SetSinceDateTime(const Value: TDateTime);
    procedure SetWorkspotCode(const Value: String);
    procedure CurrentJobHandler;
    procedure UpdateScanAndProd(const AEndOfDay: Boolean;
      const AEmployeeNumber: Integer);
// they are all private  public
    { Public declarations }
    property CalculatorEnabled: Boolean read FCalculatorEnabled
      write SetCalculatorEnabled;
    property NewJobCode: String read FNewJobCode write SetNewJobCode;
    property NewJobDescription: String read FNewJobDescription
      write FNewJobDescription;
    property OldJobCode: String read FOldJobCode write SetOldJobCode;
    property OldSinceDateTime: TDateTime read FOldSinceDateTime
      write SetOldSinceDateTime;
    property PlantCode: String read FPlantCode write SetPlantCode;
    property Production: Double read FProduction write SetProduction;
    property ScannedEmpCount: Integer read FScannedEmpCount write
      SetScannedEmpCount;
    property SinceDateTime: TDateTime read FSinceDateTime
      write SetSinceDateTime;
    property WorkspotCode: String read FWorkspotCode write SetWorkspotCode;
  end;

var
  ManualRegistrationsF: TManualRegistrationsF;

implementation

{$R *.DFM}

uses
   GlobalDMT, UGlobalFunctions, UPimsMessageRes, DialogChangeJobFRM,
   DialogScanEmployeeFRM, CalculatorFRM, RealTimeEffDMT;

procedure TManualRegistrationsF.InitCalculator;
begin
  Production := 0;
  CalculatorF.ClearCurrent;
end;

procedure TManualRegistrationsF.StatusInfo(AMsg: String);
begin
  sBarMessage.Panels[1].Text := AMsg;
  Update;
  Application.ProcessMessages;
end;

// Get first workspot for this workstation, from 'workspots per workstation'.
function TManualRegistrationsF.GetFirstWorkspot: Boolean;
begin
  with ManualRegistrationsDM.oqWorkspotPerWorkstation do
  begin
    ClearVariables;
    SetVariable('COMPUTER_NAME', ORASystemDM.CurrentComputerName);
    Execute;
    Result := not Eof;
    if Result then
    begin
      PlantCode           := FieldAsString('PLANT_CODE');
      WorkspotCode        := FieldAsString('WORKSPOT_CODE');
      pnlWorkspot.Caption := WorkspotCode + ' ' + FieldAsString('DESCRIPTION');
      Result := True;
    end
    else
    begin
      // No workspot found for this workstation! This should be defined.
      DisplayMessage(SPimsNoWorkspot, mtInformation, [mbOk]);
      StatusInfo(SPimsNoWorkspot);
    end;
  end;
end;

// Get employees that are scanned in on workspot for this workstation
procedure TManualRegistrationsF.GetScannedEmployees;
var
  Item: TdxTreeListNode;
  DateFrom, DateTo: TDateTime;
  MySinceDateTime: TDateTime;
begin
  dxTLEmployee.ClearNodes;

  // For last 12 hours
  DateFrom := Now - 0.5;
  DateTo := IncMinute(Now, 5);

  // Count the employees that are scanned
  ScannedEmpCount := 0;
  EmptyIDCard(MyLastIDCard); // PIM-60
  MySinceDateTime := SinceDateTime;
  with ManualRegistrationsDM do
  begin
    odsTRS.Active := False;
    odsTRS.DeleteVariables;
    odsTRS.DeclareAndSet('PLANT_CODE',    otString, PlantCode);
    odsTRS.DeclareAndSet('WORKSPOT_CODE', otString, WorkspotCode);
    odsTRS.DeclareAndSet('DATEFROM',      otDate, DateFrom);
    odsTRS.DeclareAndSet('DATETO',        otDate, DateTo);
    odsTRS.Active := True;
    // Enabled / Disable the Change Job-Button.
    // Keep it 'enabled', so user can always change job.
//    btnChangeJob.Enabled := not qryTimeRegScan.IsEmpty;
    while not odsTRS.Eof do
    begin
      // PIM-60 Store info about the first found scan here
      if ScannedEmpCount = 0 then
        PopulateIDCard(MyLastIDCard, odsTRS); // PIM-60
      Item := dxTLEmployee.Add;
      Item.Values[0] :=
        odsTRS.FieldByName('EMPLOYEE_NUMBER').AsString + ' ' +
        odsTRS.FieldByName('DESCRIPTION').AsString;
      Item.Values[1] :=
        DateTimeToStr(odsTRS.FieldByName('DATETIME_IN').AsDateTime);
      if MySinceDateTime = 0 then
      begin
        if SinceDateTime = 0 then
          SinceDateTime := odsTRS.FieldByName('DATETIME_IN').AsDateTime
        else
          if odsTRS.FieldByName('DATETIME_IN').AsDateTime < SinceDateTime then
            SinceDateTime := odsTRS.FieldByName('DATETIME_IN').AsDateTime;
      end;
      Item.Values[2] :=
        odsTRS.FieldByName('JOB_CODE').AsString + ' ' +
        odsTRS.FieldByName('JOBDESCRIPTION').AsString;
      // If there was not a current job
      if pnlJobCode.Caption = '' then
      begin
        NewJobCode := odsTRS.FieldByName('JOB_CODE').AsString;
        NewJobDescription := odsTRS.FieldByName('JOBDESCRIPTION').AsString;
        CurrentJobHandler;
      end;
      ScannedEmpCount := ScannedEmpCount + 1;
      odsTRS.Next;
    end; // while
//    Active := False; // Keep it open!
  end; // with
  btnScanAllToCurJob.Enabled := (ScannedEmpCount > 0);
end; // GetScannedEmployees

// ABS-18715
function TManualRegistrationsF.DetermineShiftDate(AIDCard: TScannedIDCard;
  AStartDate, AEndDate: TDateTime): TDateTime;
var
  IDCard: TScannedIDCard;
  Startdatetime, Enddatetime: TDateTime;
begin
  IDCard.EmployeeCode := -1;
  IDCard.PlantCode := PlantCode;
  IDCard.ShiftNumber := AIDCard.ShiftNumber;
  IDCard.DepartmentCode := '';
  IDCard.DateIn := AStartDate;
  IDCard.DateOut := AEndDate;

  Startdatetime := AStartDate;
  Enddatetime := AEndDate;
  AProdMinClass.GetShiftDay(IDCard, Startdatetime, Enddatetime);
  Result := Trunc(Startdatetime);
end; // DetermineShiftDate

procedure TManualRegistrationsF.CreateProductionRecord(
  const AIDCard: TScannedIDCard; const ASameJob: Boolean);
var
  StartDate, EndDate: TDateTime; // TD-24655
  ShiftDate: TDateTime;
begin
  // TD-23355: What to do with SHIFT_DATE ?
  // TD-24655
  if ASameJob then
  begin
    StartDate := RoundTime(Now, 1); // current time
    EndDate := IncMinute(StartDate, 1); // 1 minute later
  end
  else
  begin
    if ORASystemDM.EnableManRegsMultipleJobs then
    begin
      StartDate := RoundTime(AIDCard.DateIn, 1); // PIM-45
      EndDate := RoundTime(Now, 1); // Current
    end
    else
    begin
      StartDate := RoundTime(OldSinceDateTime, 1); // PIM-45
      EndDate := RoundTime(SinceDateTime, 1); // PIM-45
    end;
  end;
  // PIM-45
  if (StartDate = EndDate) then
  begin
    EndDate := IncMinute(EndDate, 1);
  end;
  ShiftDate := DetermineShiftDate(AIDCard, StartDate, EndDate); // ABS-18715
  // TD-24566 Use merge (insert or update) instead of insert.
  with ManualRegistrationsDM, oqPQMerge do
  begin
    try
      ClearVariables;
      SetVariable('PLANT_CODE',          PlantCode);
      SetVariable('WORKSPOT_CODE',       WorkspotCode);
      SetVariable('JOB_CODE',            AIDCard.JobCode);
      SetVariable('SHIFT_NUMBER',        AIDCard.ShiftNumber);
      SetVariable('START_DATE',          StartDate);
      SetVariable('END_DATE',            EndDate);
      SetVariable('COUNTER_VALUE',       0);
      SetVariable('QUANTITY',            Production);
{$IFDEF TEST}
      SetVariable('AUTOMATIC_QUANTITY',  Production);
      SetVariable('MANUAL_YN',           'N');
{$ELSE}
      SetVariable('AUTOMATIC_QUANTITY',  0);
      SetVariable('MANUAL_YN',           'Y');
{$ENDIF}
      SetVariable('MUTATOR',             ORASystemDM.CurrentProgramUser);
      SetVariable('SHIFT_DATE',          ShiftDate); // ABS-18715
      Execute;
    except
      on E: Exception do
      begin
        if (E is EOracleError) then
        begin
          if ((E as EOracleError).Errorcode = PIMS_ORAERR_KEYVIOL)
            then   // IGNORE replaces if not findkey
          else
            WErrorLog(E.Message);
        end; {E is EOracleError}
      end; {E: Exception}
    end; {try }
  end; {with oqPQMerge}
  UpdateWorkspotEffPerMinuteTable(AIDCard, ASameJob); // PIM-156  
end; // CreateProductionRecord

// PIM-156
procedure TManualRegistrationsF.UpdateWorkspotEffPerMinuteTable(
  const AIDCard: TScannedIDCard; const ASameJob: Boolean);
var
  EndTime, StartTime, ShiftDate: TDateTime;
  NormLevel, SecondsNorm: Double;
begin
  // Look from the last whole minute till now.
  // Use RoundTime to see if the minute must be rounded
  try
    EndTime := RoundTime(Now, 1);
    StartTime := EndTime - 1/24/60; // 1 minute earlier
    ShiftDate := DetermineShiftDate(AIDCard, StartTime, EndTime);
    NormLevel := AIDCard.NormProdLevel;
    // The secondsnorm will be changed during update
    SecondsNorm := 0;
    if NormLevel > 0 then
      SecondsNorm := Production * 3600 / NormLevel;
    RealTimeEffDM.MergeWorkspotEffPerMinute(
      ShiftDate,
      AIDCard.ShiftNumber,
      PlantCode,
      WorkspotCode,
      AIDCard.JobCode,
      StartTime,
      EndTime,
      60,  // ?
      SecondsNorm,
      Production,
      NormLevel,
      StartTime,
      EndTime
      );
      // Must this be done here?
      // PIM-156.2
      // Do NOT Calculate in application!
//    RealTimeEffDM.CalcEfficiency(PlantCode);
//    RealTimeEffDM.RecalcEfficiencyForToday(PlantCode, WorkspotCode);
  except
    on E:EOracleError do
    begin
      WErrorLog(E.Message);
    end;
    on E:Exception do
    begin
      WErrorLog(E.Message);
    end;
  end;
end; // UpdateWorkspotEffPerMinuteTable

procedure TManualRegistrationsF.CreateScanRecord(AIDCard: TScannedIDCard);
var
  UpdateProdMinOut, UpdateProdMinIn: Boolean;
begin
  // TD23355 What to do with SHIFT_DATE ?
  with ManualRegistrationsDM, oqTRSInsert do
  begin
    try
    // Create New (open) Scan for another Job
      ClearVariables;
      SetVariable('DATETIME_IN',     AIDCard.DateIn);
      SetVariable('EMPLOYEE_NUMBER', AIDCard.EmployeeCode);
      SetVariable('PLANT_CODE',      AIDCard.PlantCode);
      SetVariable('WORKSPOT_CODE',   AIDCard.WorkSpotCode);
      SetVariable('JOB_CODE',        AIDCard.JobCode);
      SetVariable('SHIFT_NUMBER',    AIDCard.ShiftNumber);
      SetVariable('PROCESSED_YN',    UNCHECKEDVALUE);
      SetVariable('IDCARD_IN',       AIDCard.IDCard);
      SetVariable('IDCARD_OUT',      '');
      SetVariable('MUTATOR',         ORASystemDM.CurrentProgramUser);
      SetVariable('SHIFT_DATE',      Trunc(AIDCard.DateIn)); // TD-23355
      Execute;
    except
      on E: Exception do
      begin
        if (E is EOracleError) then
        begin
          if ((E as EOracleError).Errorcode = PIMS_ORAERR_KEYVIOL)
            then   // IGNORE replaces if not findkey
          else
            WErrorLog(E.Message);
        end; {E is EOracleError}
      end; {E: Exception}
    end; {try }
  end; {with oqTRSInsert}

  UpdateProdMinOut := True;
  UpdateProdMinIn := False;
  GlobalDM.ProcessTimeRecording(AIDCard, AIDCard,
    True, False, True, UpdateProdMinOut,
    UpdateProdMinIn, False, False);
end; // CreateScanRecord

procedure TManualRegistrationsF.UpdateScannings(const AEndOfDay: Boolean;
  const AEmployeeNumber: Integer; ACreateProduction: Boolean;
  const AUpdateSince: Boolean);
var
  LastIDCard, NextIDCard: TScannedIDCard;
  GoOn: Boolean;
begin
  if AUpdateSince then
  begin
    SinceDateTime := Now;
    SinceDateTime := RoundTime(SinceDateTime, 1);
  end;
  try
    with ManualRegistrationsDM do
    begin
      odsTRS.Close; // TD-25865-Rework - Refresh here!
      odsTRS.Open;
      while not odsTRS.Eof do
      begin
        GoOn := True;
        AssignScanRecord; // TD-25984
        EmptyIDCard(LastIDCard);
        FillIDCard(LastIDCard);
        if not AEndOfDay then
          // Is there an open scanning of same job?
          if odsTRS.FieldByName('JOB_CODE').AsString = NewJobCode then
          begin
            GoOn := False;
            // TD-24655 Same job, but this can only be the case when there
            //          is only 1 job defined!
            //          In that case also store the production quantity!
            // There can be more then 1 emp. working on it, but only
            // store the quantity once.
            if ACreateProduction then
            begin
              CreateProductionRecord(LastIDCard, True);
              ACreateProduction := False;
            end;
          end;
        if GoOn then
          if (AEmployeeNumber <> -1) then
            // Is there an open scanning for given employee?
            GoOn :=
              (odsTRS.FieldByName('EMPLOYEE_NUMBER').AsInteger =
              AEmployeeNumber);
        if GoOn then
        begin
          LastIDCard.DateOut := SinceDateTime;
          EmptyIDCard(NextIDCard);
          FillIDCard(NextIDCard);

          // If starttime of scan >= sincetime then only change the job
          // of this scan. Leave the scan open.
          if RoundTime(odsTRS.FieldByName('DATETIME_IN').AsDateTime, 1) >= // PIM-45
            SinceDateTime then
          begin
            // Change job of open scan, leave it open.
            UpdateTRSJob(
              odsTRS.FieldByName('EMPLOYEE_NUMBER').AsInteger,
              odsTRS.FieldByName('DATETIME_IN').AsDateTime,
              NewJobCode);
            // PIM-45 Do this also here!
            if ACreateProduction then
            begin
              CreateProductionRecord(LastIDCard, False);
              ACreateProduction := False;
            end;
          end
          else
          begin
            if CloseScanRecord(LastIDCard) then
            begin
              if not AEndOfDay then
              begin
                NextIDCard.JobCode := NewJobCode;
                NextIDCard.DateIn  := SinceDateTime;
                NextIDCard.DateOut := NullDate;
                CreateScanRecord(NextIDCard);
              end;
              // There can be more then 1 emp. working on it, but only
              // store the quantity once.
              if ACreateProduction then
              begin
                CreateProductionRecord(LastIDCard, False);
                ACreateProduction := False;
              end;
            end; // If SameDateTime...
          end; // if CloseScanRecord
        end; // if GoOn...
        odsTRS.Next;
      end; // while not odsTRS.Eof do
    end; // with ManualRegistrationsDM do
    if ORASystemDM.OracleSession.InTransaction then
      ORASystemDM.OracleSession.Commit;
  except
    on E: EOracleError do
    begin
      ORASystemDM.OracleSession.Rollback;
      WErrorLog(E.Message);
    end;
    on E: Exception do
    begin
      ORASystemDM.OracleSession.Rollback;
      WErrorLog(E.Message);
//      DisplayMessage(GetErrorMessage(E, PIMS_POSTING), mtInformation, [mbOk]);
    end;
  end;
end; // UpdateScannings

procedure TManualRegistrationsF.ChangeJob;
var
  SelectorPos: TFormPos;
  SelectorColumns: array of String;
  SelectRes: Integer;
  StartDate, EndDate: TDateTime;
  procedure SetSelectorPos(var AFormPos: TFormPos);
  var
    RealHeight: Integer;
    TopPart: Integer;
  begin
    TopPart := Height -
      pnl11.Height - pnlBottom.Height - sbarMessage.Height;
    RealHeight := Height - TopPart;
    AFormPos.Top    := Top + Round(RealHeight / 2);
    AFormPos.Left   := Left; // pnlBottom.Left;
    AFormPos.Height := Round(RealHeight / 2) + TopPart; // pnlBottom.Height;
    AFormPos.Width  := Width; // pnlBottom.Width;
  end;
  procedure ChangeJobHandler;
  begin
    try
      Screen.cursor := crHourglass;
      try
        UpdateScannings(False, -1, Production > 0, True);
        CurrentJobHandler;
        lblSince.Caption   := TimeToStr(SinceDateTime);
        btnScanAllToCurJob.Enabled := True;
        btnScanEmp.Enabled := True;
        EnableCalculator(True);
        InitCalculator;
        Update;
        GetScannedEmployees;
      except
      end;
    finally
      Screen.cursor := crDefault;
    end;
  end; // ChangeJobHandler
begin
  SetLength(SelectorColumns, 3);
  SelectorColumns[0] := SPimsJob;
  SelectorColumns[1] := SPimsColumnCode;
  SelectorColumns[2] := SPimsColumnDescription;
  OldSinceDateTime   := SinceDateTime;
  OldJobCode         := NewJobCode;
  Production         := CalculatorF.UseThis;
  with ManualRegistrationsDM.odsJobcode do
  begin
    Active := False;
    ClearVariables;
    SetVariable('PLANT_CODE',          PlantCode);
    SetVariable('WORKSPOT_CODE',       WorkSpotCode);
    SetVariable('SHOW_AT_SCANNING_YN', CHECKEDVALUE);
    SetVariable('DATE_INACTIVE',       Trunc(Now));
    Active := True;
    if not Eof then
    begin
      SetSelectorPos(SelectorPos);
      // 20015302 Use this to show the previous job during job-selection
      DialogChangeJobF.OldJobCode := OldJobCode; // 22015302
      DialogChangeJobF.DisableCancelTimer := True; // 20015302
      // PIM-60 Check for End-Of-Shift - Start
      DialogChangeJobF.EndOfDayBlink := False;
      if ScannedEmpCount > 0 then // when there was a previous scan/job?
      begin
        with ManualRegistrationsDM do
        begin
          // This contains info about one of the previous scans.
          MyLastIDCard.DateIn := Now;
          try
            AProdMinClass.GetShiftDay(MyLastIDCard, StartDate, EndDate);
            MyLastIDCard.ShiftStartDateTime := StartDate;
            MyLastIDCard.ShiftEndDateTime := EndDate;
            DialogChangeJobF.EndOfDayBlink := EndOfShiftTest(MyLastIDCard);
          except
            MyLastIDCard.ShiftStartDateTime := NullDate;
            MyLastIDCard.ShiftEndDateTime := Nulldate;
          end;
        end;
      end;
      // PIM-60 Check for End-Of-Shift - End
      SelectRes := DialogChangeJobF.EnableSelection(SelectorPos, SelectorColumns,
        ManualRegistrationsDM.dsrcJobcode);
      Update;
      case SelectRes of
      ModalOk:
        begin
          NewJobCode         := FieldByName('JOB_CODE').AsString;
          NewJobDescription  := FieldByName('DESCRIPTION').AsString;
          ChangeJobHandler;
        end;
      ModalEndOfDay: //SetEndOfDay(NextIDCard);
        begin
          NewJobCode         := '';
          NewJobDescription  := '';
          try
            Screen.cursor := crHourglass;
            UpdateScannings(True, -1, Production > 0, True);
            CurrentJobHandler;
            lblSince.Caption   := '';
            btnScanAllToCurJob.Enabled := False;
            btnScanEmp.Enabled := False;
            EnableCalculator(False);
            InitCalculator;
            Update;
            GetScannedEmployees;
          finally
            Screen.cursor := crDefault;
          end;
        end;
      ModalCancel:
        begin
        end;
      ModalMechanicalDown:
        begin
          // Change job to Down-job: 'mechanical down'.
          NewJobCode := MECHANICAL_DOWN_JOB;
          NewJobDescription  := SPimsMechanicalDown;
          ChangeJobHandler;
        end;
      ModalNoMerchandizeDown:
        begin
          // Change job to Down-job: 'no merchandize'.
          NewJobCode := NO_MERCHANDIZE_JOB;
          NewJobDescription  := SPimsNoMerchandize;
          ChangeJobHandler;
        end;
      ModalBreak:
        begin
          // Change job to Break-job
          NewJobCode := BREAK_JOB;
          NewJobDescription  := SPimsBreakJobDescription;
          ChangeJobHandler;
        end;
      end; // case
    end;
    Active := False;
  end;
end; // ChangeJob

// 20015302
// Based on system setting 'EnableManRegsMulitpleJobs this button
// is used for 'change job' or for 'change employee (and job)'.
procedure TManualRegistrationsF.btnChangeJobClick(Sender: TObject);
begin
  try
    EnableTimers(False);
    // 20015302 -> Change Emps. + Mult. Jobs
    if ORASystemDM.EnableManRegsMultipleJobs then
      ChangeEmpAndJob
    else
    begin
      // Change Emps. + Single Job.
      ChangeJob;
    end;
  finally
    EnableTimers(True);
  end;
end;

procedure TManualRegistrationsF.SetPlantCode(const Value: String);
begin
  FPlantCode := Value;
end;

procedure TManualRegistrationsF.SetWorkspotCode(const Value: String);
begin
  FWorkspotCode := Value;
end;

procedure TManualRegistrationsF.SetSinceDateTime(const Value: TDateTime);
begin
  FSinceDateTime := Value;
end;

procedure TManualRegistrationsF.SetOldJobCode(const Value: String);
begin
  FOldJobCode := Value;
end;

procedure TManualRegistrationsF.SetNewJobCode(const Value: String);
begin
  FNewJobCode := Value;
end;

procedure TManualRegistrationsF.SetProduction(const Value: Double);
begin
  FProduction := Value;
end;

procedure TManualRegistrationsF.SetOldSinceDateTime(
  const Value: TDateTime);
begin
  FOldSinceDateTime := Value;
end;

procedure TManualRegistrationsF.SetScannedEmpCount(const Value: Integer);
begin
  FScannedEmpCount := Value;
end;

procedure TManualRegistrationsF.tmrShowTimeTimer(Sender: TObject);
begin
  inherited;
  // Show time
  lblDateTime.Caption := FormatDateTime(LONGDATE, now);
  if CalculatorEnabled then
    lblAddProduction.Visible := (CalculatorF.UseThis > 0);
end;

procedure TManualRegistrationsF.FormCreate(Sender: TObject);
var
  MyCaption: String;
begin
//  pnlImageBase.Color := clPimsBlue; // PIM-12 // PIM-250
  MyCaption := Caption;
  lblMessage.Caption := Caption;
  // MR:23-01-2008 RV002: Also show ComputerName.
  Caption := 'ORCL - ' + ORASystemDM.OracleSession.LogonDatabase + ' - ' +
    MyCaption + ' - ' + DisplayVersionInfo + ' - ' +
    ORASystemDM.CurrentComputerName;

  // 20015302
  if ORASystemDM.EnableManRegsMultipleJobs then
  begin
    gBoxCurrentJob.Visible := False;
    pnlScanButtons.Visible := False;
  end;

  sbarMessage.Panels[2].Text := DisplayVersionInfo;

  CalculatorF := TCalculatorF.Create(Self);
  CalculatorF.Parent := pnlCalculator;
  CalculatorF.Left := 0;
  CalculatorF.Top  := 0;
  CalculatorF.btnAccept.Default := True;
  CalculatorF.Show;

  lblAddProduction.Visible := False;
  pnlWorkspot.Caption      := '';
  pnlJobcode.Caption       := '';
  lblSince.Caption         := '';
  lblDateTime.Caption      := '';
  tmrShowTimeTimer(Sender);
  OldJobCode               := '';
  NewJobCode               := '';
  OldSinceDateTime         := 0;
  SinceDateTime            := 0;
  InitCalculator;

  EnableCalculator(False);

  btnScanAllToCurJob.Enabled := False;
  btnScanEmp.Enabled         := False;

  DialogChangeJobF := TDialogChangeJobF.Create(Self);
  DialogChangeJobF.BreakBtnVisible := ORASystemDM.BreakBtnVisible; // PIM-12
  DialogChangeJobF.LunchBtnVisible := ORASystemDM.LunchBtnVisible; // PIM-12
  DialogChangeJobF.EndOfDayBtnVisible := ORASystemDM.MANREGEndOfDayBtnVisible; // PIM-12
  DialogScanEmployeeF := TDialogScanEmployeeF.Create(Self);

  with ManualRegistrationsDM do
  begin
    // tblTRS.Active    := True;
  end;

  if GetFirstWorkspot then
  begin
    with ManualRegistrationsDM do
    begin
      //
      // Add any missing jobs here! Or it will not be added to the variables.
      //
      ActionAddMissingJobForWorkspot(PlantCode,
        WorkspotCode, BREAK_JOB, SPimsBreakJobDescription);
      ActionAddMissingJobForWorkspot(PlantCode,
        WorkspotCode, MECHANICAL_DOWN_JOB, SPimsMechanicalDown);
      ActionAddMissingJobForWorkspot(PlantCode,
        WorkspotCode, NO_MERCHANDIZE_JOB, SPimsNoMerchandize);
    end;
    GetScannedEmployees;
  end;

  EnableTimers(True);
end;

procedure TManualRegistrationsF.FormDestroy(Sender: TObject);
begin
  DialogScanEmployeeF.Close;
  with ManualRegistrationsDM do
  begin
    // tblTRS.Active    := False;
  end;
  DialogChangeJobF.Free;
  CalculatorF.Free;
end;

procedure TManualRegistrationsF.btnScanAllToCurJobClick(Sender: TObject);
begin
  try
    EnableTimers(False);
    UpdateScannings(False, -1, False, False);
  finally
    GetScannedEmployees;
    EnableTimers(True);
  end;
end;

// After an employee has been scanned, create a new (open) TRS-record
// based on given employeenumber, current job and since-datetime.
procedure TManualRegistrationsF.CreateScanAction(const EmployeeNumber: Integer);
var
  NextIDCard: TScannedIDCard;
  ShiftNumber: Integer;
  DateIn: TDateTime;
begin
  DateIn := RoundTime(Now, 1); // This must be based on current time!
  EmptyIDCard(NextIDCard);
  with ManualRegistrationsDM do
  begin
    FillIDCardInit(NextIDCard, PlantCode, WorkspotCode, NewJobCode,
      EmployeeNumber);
    SelectShift(ShiftNumber, NextIDcard, EmployeeNumber, Trunc(DateIn)
      {Trunc(SinceDateTime)});
    NextIDCard.ShiftNumber := ShiftNumber;
    NextIDCard.JobCode     := NewJobCode;
    NextIDCard.DateIn      := DateIn; // SinceDateTime;
//    ORASystemDM.Pims.StartTransaction;
    try
      CreateScanRecord(NextIDCard);
      ORASystemDM.OracleSession.Commit;
{$IFDEF DEBUG}
  WDebugLog('New Open Scan:' +
    ' DateIn=' + DateTimeToStr(NextIDCard.DateIn) +
    ' Job=' + NextIDCard.JobCode
    );
{$ENDIF}
    except
      on E:EOracleError do
      begin
        ORASystemDM.OracleSession.Rollback;
        WErrorLog(E.Message);
      end;
      on E: Exception do
      begin
        ORASystemDM.OracleSession.Rollback;
        WErrorLog(E.Message);
//        DisplayMessage(GetErrorMessage(E, PIMS_POSTING), mtInformation, [mbOk]);
      end;
    end;
  end;
end; // CreateScanAction

procedure TManualRegistrationsF.btnScanEmpClick(Sender: TObject);
var
  MyEmployeeNumber: Integer;
  LastIDCard: TScannedIDCard;
  CloseOK: Boolean;
begin
  try
    EnableTimers(False);
    MyEmployeeNumber := -1;
    if DialogScanEmployeeF.ShowModal = mrOK then
      MyEmployeeNumber := DialogScanEmployeeF.EmployeeNumber;
    with ManualRegistrationsDM do
    begin
      if MyEmployeeNumber <> -1 then
      begin
         // Is there a scanning for this employee?
         if not odsTRS.Active then
           odsTRS.Active := True;
         if odsTRS.Locate('EMPLOYEE_NUMBER', MyEmployeeNumber, []) then
           UpdateScannings(False, MyEmployeeNumber, False, False)
         else
         begin
           // TD-25984 If here a scan was found, then it was
           //          from a different workspot, and we must close this scan
           //          before we continue!
           CloseOK := True;  // PIM-209.2
           if FindScan(MyEmployeeNumber) then
           begin
             EmptyIDCard(LastIDCard);
             FillIDCard(LastIDCard);
             LastIDCard.DateOut := RoundTime(Now, 1);
             CloseOK := CloseScanRecord(LastIDCard); // PIM-209.2
           end;
           // Just create a new scanning on current job + time
           if CloseOK then  // PIM-209.2
             CreateScanAction(MyEmployeeNumber);
         end;
         GetScannedEmployees;
      end;
    end;
  finally
    EnableTimers(True);
  end;
end;  // btnScanEmpClick

procedure TManualRegistrationsF.FormKeyPress(Sender: TObject;
  var Key: Char);
const                        {BS, CR}     {0..9}        {+,  -,    .}
  ValidKeys  : set of Char = [#08, #13] + [#48..#57] + [#43, #45, #46] +
    ['c', 'C'];
begin
  if not CalculatorEnabled then
    Exit;

  if (Key in ValidKeys) then
  begin
    case Key of
      'c', 'C': Key := Chr(12); // clear
    end;
    CalculatorF.FormKeyPress(Sender, Key);
  end;
  CalculatorF.btnAccept.SetFocus;
end;

procedure TManualRegistrationsF.EnableCalculator(const AOn: Boolean);
var
  AComponent: TComponent;
  I: Integer;
begin
  CalculatorEnabled := AOn;
  if not AOn then
    lblAddProduction.Visible := False;
  for I := 0 to CalculatorF.ComponentCount - 1 do
  begin
  //KME you can also simply enable/disable the groupbox
    AComponent := CalculatorF.Components[I];
    if (AComponent is TButton) then
      if (AComponent as TButton).Parent = CalculatorF.gBoxCalculatorButtons then
        (AComponent as TButton).Enabled := AOn;
  end;
  CalculatorF.DisplayPanel.Enabled := AOn;
end;

procedure TManualRegistrationsF.SetCalculatorEnabled(const Value: Boolean);
begin
  FCalculatorEnabled := Value;
end;

procedure TManualRegistrationsF.EnableTimers(const AOn: Boolean);
begin
  tmrShowTime.Enabled     := AOn;
  tmrRefreshScans.Enabled := AOn;
end;

procedure TManualRegistrationsF.tmrRefreshScansTimer(Sender: TObject);
begin
  GetScannedEmployees;
end;

procedure TManualRegistrationsF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
{
  // PIM-156.2
  if DoCalcEff then
  begin
    ShowMessageOn('About to close. Please wait...');
    Application.ProcessMessages;
    MyDoClose := True;
    Action := caNone;
    Exit;
  end;
  ShowMessageOff;
}
  EnableTimers(False);
end;

procedure TManualRegistrationsF.CurrentJobHandler;
begin
  if NewJobCode <> '' then
  begin
    pnlJobCode.Caption := NewJobCode + ' ' + NewJobDescription;
    lblSince.Caption   := TimeToStr(SinceDateTime);
    btnScanEmp.Enabled := True;
    EnableCalculator(True);
  end
  else
  begin
    pnlJobCode.Caption := '';
    lblSince.Caption := '';
    btnScanEmp.Enabled := False;
    EnableCalculator(False);
  end;
end; // CurrentJobHandler

procedure TManualRegistrationsF.FormShow(Sender: TObject);
begin
  ORASystemDM.PSAutoCloseSecs := 0; // Do not auto close.
  // 20015302
  if ORASystemDM.EnableManRegsMultipleJobs then
  begin
    btnChangeJob.Caption := btnScanEmp.Caption;
  end;
end;

// 20015302
// Change scan for a one specific employee and job
// Add quantity for the previous job of this employee
procedure TManualRegistrationsF.ChangeEmpAndJob;
var
  SelectRes: Integer;
  MyEmployeeNumber: Integer;
begin
  DialogScanEmployeeF.PlantCode := PlantCode;
  DialogScanEmployeeF.WorkspotCode := WorkspotCode;
  Production := CalculatorF.UseThis;
  SelectRes := DialogScanEmployeeF.ShowModal;
  MyEmployeeNumber := -1;
  if SelectRes <> mrCancel then
    MyEmployeeNumber := DialogScanEmployeeF.EmployeeNumber;
  if MyEmployeeNumber = -1 then
    Exit;
  NewJobCode := DialogScanEmployeeF.NewJobCode;
  NewJobDescription := DialogScanEmployeeF.NewJobDescription;
  Update;
  // Handle scan
  with ManualRegistrationsDM do
  begin
    if MyEmployeeNumber <> -1 then
    begin
      if SelectRes <> ModalCancel then
      begin
        // Is there a scanning for this employee?
        if not odsTRS.Active then
          odsTRS.Active := True;
      end;
    end;
  end;
  // Handle scans + production
  case SelectRes of
  ModalOK:
    begin
      UpdateScanAndProd(False, MyEmployeeNumber);
      CurrentJobHandler;
      EnableCalculator(True);
      InitCalculator;
      Update;
      GetScannedEmployees;
    end;
  ModalEndOfDay: //SetEndOfDay(NextIDCard);
    begin
      UpdateScanAndProd(True, MyEmployeeNumber);
      CurrentJobHandler;
      EnableCalculator(True);
      InitCalculator;
      Update;
      GetScannedEmployees;
    end;
  ModalCancel:
    begin
    end;
  end; // case
end; // ChangeEmpAndJob

// 20015302
function TManualRegistrationsF.CloseScanRecord(AIDCard: TScannedIDCard): Boolean;
var
  StartDate, EndDate: TDateTime;
  ProdDate2: TDateTime; // PIM-10
begin
  // TD-25984 Use only variables from AIDCard here, not from odsTRS !
  Result := False;
  with ManualRegistrationsDM do
  begin
    if SameDateTime(
      AIDCard.DateIn, // TD-25984
      SinceDateTime) then
      DisplayMessage(SPimsDateinPreviousPeriod, mtInformation, [mbOk])
    else
    begin
      // Use GetShiftDay to get the correct 'startdate'.
      AProdMinClass.GetShiftDay(AIDCard, StartDate, EndDate);
      AIDCard.ShiftDate := Trunc(StartDate);
       // Close Current Scan (for same job)
      UpdateTRSCloseScan(
        AIDCard.EmployeeCode, // TD-25984
        AIDCard.DateIn, // TD-25984
        AIDCard.DateOut,
        AIDCard.IDCard,
        AIDCard.ShiftDate);

       // PIM-10
       if Trunc(AIDCard.DateOut) <> Trunc(AIDCard.DateIn) then
         ProdDate2 := Trunc(AIDCard.DateIn)
       else
         ProdDate2 := NullDate;
       // PIM-10
       // This will only book the production hours for current scan!
       GlobalDM.ProcessTimeRecording(AIDCard, AIDCard,
         True, True,
         True,
         True, True,
         False,
         True {LastScanRecordForDay} );

      // Use GetShiftDay to get the correct 'startdate'.
      AProdMinClass.GetShiftDay(AIDCard, StartDate, EndDate);
        AIDCard.ShiftDate := Trunc(StartDate);

      // This is needed to recalculate the production hours.
      AIDCard.FromTimeRecordingApp := True; // False; PIM-10 Do not recalc. prod. hrs!

      GlobalDM.UnprocessProcessScans(AIDCard,
        Trunc(StartDate), // Trunc(LastIDCard.DateIn), // PIM-10
        Trunc(AIDCard.DateOut), ProdDate2, NullDate, NullDate, // PIM-10
        True, False,
        False); // True); // PIM-10
      Result := True;
    end; // if
  end; // with
end; // CloseScanRecord

// 20015302
// Update scan and prod for 1 employee at a time.
procedure TManualRegistrationsF.UpdateScanAndProd(const AEndOfDay: Boolean;
  const AEmployeeNumber: Integer);
var
  LastIDCard, NextIDCard: TScannedIDCard;
  CloseOK: Boolean;
  Found: Boolean;
begin
  SinceDateTime := Now;
  SinceDateTime := RoundTime(SinceDateTime, 1);
  CloseOK := True;
  try
    // Search for last open scan for this employee
    with ManualRegistrationsDM do
    begin
      // Look for open scan.
      Found := odsTRS.Locate('EMPLOYEE_NUMBER', AEmployeeNumber, []);
      if Found then
      begin
        AssignScanRecord; // TD-25984
        EmptyIDCard(LastIDCard);
        FillIDCard(LastIDCard);
        LastIDCard.DateOut := SinceDateTime;
        EmptyIDCard(NextIDCard);
        FillIDCard(NextIDCard);
        // Close open scan here
        CloseOK := CloseScanRecord(LastIDCard);
        // PIM-209.2
        if CloseOK and (Production > 0) then
        begin
          CreateProductionRecord(LastIDCard, False);
        end;
      end // if found
      else
      begin
        // TD-25984 If here a scan was found, then it was
        //          from a different workspot, and we must close this scan
        //          before we continue!
        if FindScan(AEmployeeNumber) then // TD-25984
        begin
          EmptyIDCard(LastIDCard);
          FillIDCard(LastIDCard);
          LastIDCard.DateOut := RoundTime(Now, 1);
          CloseOK := CloseScanRecord(LastIDCard);
          // PIM-209.2
          if CloseOK and (Production > 0) then
          begin
            CreateProductionRecord(LastIDCard, False);
          end;
        end;
      end; // else if found
    end; // with ManualRegistrationsDM
    // Create a new open scan here
    if (not AEndOfDay) and CloseOK then
    begin
      if not Found then // No previous scan found
        CreateScanAction(AEmployeeNumber)
      else
      begin
        NextIDCard.JobCode := NewJobCode;
        NextIDCard.DateIn  := SinceDateTime;
        NextIDCard.DateOut := NullDate;
        CreateScanRecord(NextIDCard);
      end;
    end; // if (not AEndOfDay)...
    if ORASystemDM.OracleSession.InTransaction then
      ORASystemDM.OracleSession.Commit;
  except
    on E:EOracleError do
    begin
      ORASystemDM.OracleSession.Rollback;
      WErrorLog(E.Message);
    end;
    on E: Exception do
    begin
      ORASystemDM.OracleSession.Rollback;
      WErrorLog(E.Message);
    end;
  end; // try
end; // UpdateScanAndProd

end.
