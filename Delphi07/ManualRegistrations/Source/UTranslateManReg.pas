(*
  MRA:7-MAR-2014 TD-24046
  - Translate issues: Use a different method to translate.
    - Do NOT do this in separate files where the translation is done
      hard-coded!
    - New method:
      - Use 1 resource-strings-file that contains the translation-strings.
      - Use 1 translate-unit that assigns these strings to the properties
        in the forms.
      - In this way there is only 1 form that can be translated in multiple
        languages when the application is run.
      - The project-file defines the language using a condition.
  MRA:10-OCT-2014 SO-20015569
  - Show extra buttons for Down/Break/Lunch
  - Changed SelectorFRM to DialogChangeJobFRM
*)
unit UTranslateManReg;

interface

type
  TTranslateHandlerManReg = class
  private
  public
    constructor Create; overload;
    destructor Destroy; override;
    procedure TranslateAll;
    procedure TranslateInit;
    procedure TranslateDialogSelectDownType;
  end;

var
  ATranslateHandlerManReg: TTranslateHandlerManReg;

implementation

uses
  UTranslateStringsManReg, CalculatorFRM, DialogScanEmployeeFRM,
  ManualRegistrationsFRM, DialogChangeJobFRM, DialogSelectDownTypeFRM;

{ TTranslateHandlerManReg }

constructor TTranslateHandlerManReg.Create;
begin
//
end;

destructor TTranslateHandlerManReg.Destroy;
begin
//
  inherited;
end;

procedure TTranslateHandlerManReg.TranslateAll;
begin
  if Assigned(CalculatorF) then
    with CalculatorF do
    begin
      Label1.Caption := STransNrDecimals;
    end;
  if Assigned(DialogScanEmployeeF) then
    with DialogScanEmployeeF do
    begin
      Caption := STransScanEmployee;
      gboxEmployee.Caption := STransEmployee;
      Label1.Caption :=  STransIDCard;
      Label2.Caption := STransEmployee;
      btnAccept.Caption := STransAccept;
      BitBtnOk.Caption := STransOK;
      BitBtnCancel.Caption := STransCancel;
    end;
  if Assigned(ManualRegistrationsF) then
    with ManualRegistrationsF do
    begin
      // Caption := STransManReg;
      gBoxWorkspot.Caption := STransWorkspot;
      Label1.Caption := STransWorkspot;
      gBoxCurrentJob.Caption := STransCurrentJob;
      Label2.Caption := STransJobcode;
      Label3.Caption := STransSince;
      gBoxEmployees.Caption := STransEmployees;
      dxTLEmployeeColumnEmployee.Caption := STransEmployee;
      dxTLEmployeeColumnDateTimeIn.Caption := STransDateTimeIn;
      dxTLEmployeeColumnJobcode.Caption := STransJobcode;
      btnScanAllToCurJob.Caption := STransScanAll;
      btnScanEmp.Caption := STransScanEmp;
      gBoxDateTime.Caption := STransDateTime;
      lblAddProduction.Caption := STransStoreProd;
      btnChangeJob.Caption := STransChangeJob;
    end;
  if Assigned(DialogChangeJobF) then
    with DialogChangeJobF do
    begin
      Caption := STransSelectJob;
      BitBtnOk.Caption := STransOK;
      BitBtnCancel.Caption := STransCancel;
      BitBtnEndOfDay.Caption := STransEndOfDay;
      btnDown.Caption := STransDown;
      BtnBreak.Caption := STransBreak;
      btnLunch.Caption := STransLunch;
    end;
  TranslateDialogSelectDownType;
end;

procedure TTranslateHandlerManReg.TranslateDialogSelectDownType;
begin
  if Assigned(DialogSelectDownTypeF) then
    with DialogSelectDownTypeF do
    begin
      Caption := STransSelectDownType;
      GroupBox1.Caption := STransSelectDownType;
      btnMechanicalDown.Caption := STransMechDown;
      btnNoMerchandize.Caption := STransNoMerch;
      btnOk.Caption := STransOK;
      btnCancel.Caption := STransCancel;
    end;
end;

procedure TTranslateHandlerManReg.TranslateInit;
begin
//
end;

initialization
  { Initialization section goes here }
  ATranslateHandlerManReg := TTranslateHandlerManReg.Create;

finalization
  { Finalization section goes here }
  ATranslateHandlerManReg.Free;

end.
