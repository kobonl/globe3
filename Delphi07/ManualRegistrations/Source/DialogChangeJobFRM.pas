(*
  MRA:2-SEP-2011 RV079.1. Update. 20012013.
  - Make it possible to maximize the dialog.
  - Make the buttons bigger (higher),
    but only when maximized, or by using
    a setting?
  MRA:19-SEP-2011 RV080.1. Update. 20012013.
  - Make font of grid larger when maximized.
  MRA:10-OCT-2014 SO-20015569
  - Show extra buttons for Down/Break/Lunch
  - Changed this form from SelectorFRM to DialogChangeJobFRM and it is
    noew stored local for this application.
  MRA:8-JUL-2015 PIM-12
  - Real Time Efficiency (button/color changes)
  MRA:28-JUL-2015 PIM-60
  - End-Of-Day-button blink at end of shift
  MRA:28-SEP-2015 PIM-12
  - Optionally show Break/Lunch/EndOfDay-buttons.
  MRA:27-JAN-2016 PIM-139
  - Lunch button optionally scan out.
*)
unit DialogChangeJobFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BasePimsFRM, StdCtrls, Buttons, Grids, DBGrids, DB, ExtCtrls, ImgList,
  UPimsConst, ORASystemDMT;

const
	ModalOK       = 1;
  ModalCancel   = 2;
  ModalEndofDay = 3;
  ModalMechanicalDown = 4;
  ModalNoMerchandizeDown = 5;
  ModalBreak    = 6;
  TimeOutSecs   = 60;
  BREAK_JOB = 'BREAK';


type
  TDBGrid = class(DBGrids.TDBGrid)
  private
    procedure WMWindowPosChanged(var Message: TWMWindowPosChanged);
      message WM_WINDOWPOSCHANGED;
  end;

type
  TFormPos = record
    Top, Left, Height, Width: Integer;
  end;

type
  TDialogChangeJobF = class(TBasePimsForm)
    PanelButtons: TPanel;
    PanelGrid: TPanel;
    DBGridSelector: TDBGrid;
    PanelControls: TPanel;
    BitBtnOk: TBitBtn;
    BitBtnCancel: TBitBtn;
    BitBtnEndOfDay: TBitBtn;
    tmrCancel: TTimer;
    Panel1: TPanel;
    btnFirst: TBitBtn;
    btnUp: TBitBtn;
    btnLast: TBitBtn;
    btnLow: TBitBtn;
    btnDown: TBitBtn;
    BtnBreak: TBitBtn;
    BtnLunch: TBitBtn;
    tmrEndOfDayBlink: TTimer;
    procedure BitBtnEndOfDayClick(Sender: TObject);
    procedure SpeedButtonFirstClick(Sender: TObject);
    procedure SpeedButtonPriorClick(Sender: TObject);
    procedure SpeedButtonNextClick(Sender: TObject);
    procedure SpeedButtonLastClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure tmrCancelTimer(Sender: TObject);
    procedure btnUpClick(Sender: TObject);
    procedure btnLowClick(Sender: TObject);
    procedure btnDownClick(Sender: TObject);
    procedure BtnBreakClick(Sender: TObject);
    procedure BtnLunchClick(Sender: TObject);
    procedure tmrEndOfDayBlinkTimer(Sender: TObject);
  private
    { Private declarations }
{    SpeedButtonFirstHeight, SpeedButtonPriorHeight, SpeedButtonNextHeight,
    SpeedButtonLastHeight, } BitBtnOkHeight, BitBtnCancelHeight,
    BitBtnEndOfDayHeight, PanelButtonsHeight: Integer;
{    SpeedButtonFirstWidth, SpeedButtonPriorWidth, SpeedButtonNextWidth,
    SpeedButtonLastWidth: Integer; }
{    PanelBFirstWidth, PanelBPriorWidth, PanelNextWidth,
    PanelBLastWidth,}  DBGridSelectorFontSize: Integer;
    FIsMaximized: Boolean;
    FOldJobCode: String;
    FDisableCancelTimer: Boolean;
    FEndOfDayBlink: Boolean;
    EndOfDayColor: TColor;
    FBreakBtnVisible: Boolean;
    FLunchBtnVisible: Boolean;
    FEndOfDayBtnVisible: Boolean;
    ButtonOkLeft: Integer;
    ButtonCancelLeft: Integer;
    ButtonEndOfDayLeft: Integer;
    ButtonOkNewLeft: Integer;
    ButtonCancelNewLeft: Integer;
    procedure HandleButtonSize;
    procedure WMSysCommand(var Msg: TWMSysCommand); message WM_SYSCOMMAND;
  public
    { Public declarations }
    function EnableSelection(var AFormPos: TFormPos;
       const AColumnNames: array of String; ADataSource: TDataSource): Integer;
    property IsMaximized: Boolean read FIsMaximized write FIsMaximized;
    property OldJobCode: String read FOldJobCode write FOldJobCode;
    property DisableCancelTimer: Boolean
      read FDisableCancelTimer write FDisableCancelTimer; // 20015302
    property EndOfDayBlink: Boolean read FEndOfDayBlink write FEndOfDayBlink; // PIM-60
    property BreakBtnVisible: Boolean read FBreakBtnVisible write FBreakBtnVisible; // PIM-12
    property LunchBtnVisible: Boolean read FLunchBtnVisible write FLunchBtnVisible; // PIM-12
    property EndOfDayBtnVisible: Boolean read FEndOfDayBtnVisible write FEndOfDayBtnVisible; // PIM-12
  end;

var
  DialogChangeJobF: TDialogChangeJobF;

implementation

uses
  UPimsMessageRes, DialogSelectDownTypeFRM, UTranslateManReg;

{$R *.DFM}

procedure TDBGrid.WMWindowPosChanged(var Message: TWMWindowPosChanged);
begin
  inherited;
  Windows.ShowScrollBar(Handle, SB_VERT, False);
  Windows.ShowScrollBar(Handle, SB_HORZ, False);
end;

{ TDialogChangJobF }

procedure TDialogChangeJobF.BitBtnEndOfDayClick(Sender: TObject);
begin
  inherited;
  ModalResult := ModalEndOfDay;
end;

function TDialogChangeJobF.EnableSelection(var AFormPos: TFormPos;
  const AColumnNames: array of String;
  ADataSource: TDataSource): Integer;
var
	ColIndex : Integer;
begin
  BtnBreak.Visible := BreakBtnVisible; // PIM-12
  BtnLunch.Visible := LunchBtnVisible; // PIM-12
  BitBtnEndOfDay.Visible := EndOfDayBtnVisible; // PIM-12
  // Structure of ColumnNames
  // 0. Selector form Caption
  // 1 - x grid column text
  // only 2 columns - set width based on this
  ModalResult := ModalCancel;
  // RV080.1. Do this here!
  Top    := AFormPos.Top;
  Left   := AFormPos.Left;
  Height := AFormPos.Height;
  Width  := AFormPos.Width;
  DBGridSelector.Columns.Clear;
  if High(AColumnNames) > 0 then
    Caption := AColumnNames[0];
  for ColIndex := 1 to (High(AColumnNames)) do
  begin
    DBGridSelector.Columns.Add;
    DBGridSelector.Columns[ColIndex - 1].Field :=
    	ADataSource.DataSet.Fields[ColIndex - 1];
    DBGridSelector.Columns[ColIndex - 1].Title.Caption :=
      AColumnNames[ColIndex];
    if (AColumnNames[ColIndex] = SPimsColumnCode) or
       (AColumnNames[ColIndex] = SPimsColumnPlant) then
      DBGridSelector.Columns[ColIndex - 1].Width :=
        Round(DBGridSelector.Width / (High(AColumnNames) + 2))
    else
      DBGridSelector.Columns[ColIndex - 1].Width :=
     	  DBGridSelector.Width - DBGridSelector.Columns[ColIndex - 2].Width;
  end;
  DBGridSelector.DataSource := ADataSource;
  if OldJobCode <> '' then
    DBGridSelector.DataSource.DataSet.Locate('JOB_CODE', OldJobCode, []);
  // RV080.1. Moved to higher pos.
{  Top    := AFormPos.Top;
  Left   := AFormPos.Left;
  Height := AFormPos.Height;
  Width  := AFormPos.Width; }
  // MR: 01-10-2007 F10-button was disabled? Why was not clear, so
  // we set all to enabled to be sure.
  BitBtnOK.Enabled := True;
  BitBtnCancel.Enabled := True;
  if BitBtnEndOfDay.Visible then // PIM-12
    BitBtnEndOfDay.Enabled := True;
  // Down Job
  btnDown.Enabled := not ((OldJobCode = MECHANICAL_DOWN_JOB) or
    (OldJobCOde = NO_MERCHANDIZE_JOB));
  if btnBreak.Visible then
    btnBreak.Enabled := not (OldJobCode = BREAK_JOB);
  // PIM-139
  if btnLunch.Visible then
    if not ORASystemDM.LunchBtnScanOut then
      btnLunch.Enabled := not (OldJobCode = BREAK_JOB);
  HandleButtonSize;
  Result := ShowModal;
end;

procedure TDialogChangeJobF.SpeedButtonFirstClick(Sender: TObject);
begin
  inherited;
  DBGridSelector.DataSource.DataSet.First;
end;

procedure TDialogChangeJobF.SpeedButtonPriorClick(Sender: TObject);
begin
  inherited;
  DBGridSelector.DataSource.DataSet.Prior;
end;

procedure TDialogChangeJobF.SpeedButtonNextClick(Sender: TObject);
begin
  inherited;
  DBGridSelector.DataSource.DataSet.Next;
end;

procedure TDialogChangeJobF.SpeedButtonLastClick(Sender: TObject);
begin
  inherited;
	DBGridSelector.DataSource.DataSet.Last;
end;

procedure TDialogChangeJobF.FormShow(Sender: TObject);
begin
  inherited;
  DBGridSelector.SetFocus;
  if not DisableCancelTimer then // 20015302
    tmrCancel.Enabled := True;
  tmrEndOfDayBlink.Enabled := EndOfDayBlink; // PIM-60
  // PIM-12 Reposition buttons in case End-Of-Day-button is not shown.
  if BitBtnEndOfDay.Visible then
  begin
    BitBtnOk.Left := ButtonOkLeft;
    BitBtnCancel.Left := ButtonCancelLeft;
    BitBtnEndOfDay.Left := ButtonEndOfDayleft;
  end
  else
  begin
    BitBtnOk.Left := ButtonOkNewLeft;
    BitBtnCancel.Left := ButtonCancelNewLeft;
  end;
end;

procedure TDialogChangeJobF.FormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  case Key of
    VK_F10		: begin
                  if BitBtnEndOfDay.Visible then // PIM-12
                    BitBtnEndOfDayClick(self);
                  Key := 0;
                end;
    VK_ESCAPE : begin
                  ModalResult := ModalCancel;
                  Key := 0;
                end;
    {VK_UP			: begin
                  SpeedButtonPriorClick(self);
                  Key := 0;
                end;
    VK_DOWN		: begin
                  SpeedButtonNextClick(self);
                  Key := 0;
                end;}
    VK_LEFT		: begin
                  SpeedButtonFirstClick(self);
                  Key := 0;
                end;
    VK_RIGHT	: begin
                  SpeedButtonLastClick(self);
                  Key := 0;
                end;
  end;
end;

procedure TDialogChangeJobF.FormCreate(Sender: TObject);
begin
  inherited;
  EndOfDayColor := BitBtnEndOfDay.Color;
  EndOfDayBlink := False;
  OldJobCode := ''; // 20015302
  DisableCancelTimer := False; // 20015302
  tmrCancel.Interval := TimeOutSecs * 1000;
  // RV079.1.
{  SpeedButtonFirstHeight := SpeedButtonFirst.Height;
  SpeedButtonPriorHeight := SpeedButtonPrior.Height;
  SpeedButtonNextHeight := SpeedButtonNext.Height;
  SpeedButtonLastHeight := SpeedButtonLast.Height;
  SpeedButtonFirstWidth := SpeedButtonFirst.Width;
  SpeedButtonPriorWidth := SpeedButtonPrior.Width;
  SpeedButtonNextWidth := SpeedButtonNext.Width;
  SpeedButtonLastWidth := SpeedButtonLast.Width; }
{  PanelBFirstWidth := PanelBFirst.Width;
  PanelBPriorWidth := PanelBPrior.Width;
  PanelNextWidth := PanelNext.Width;
  PanelBLastWidth := PanelBLast.Width; }
  BitBtnOkHeight := BitBtnOk.Height;
  BitBtnCancelHeight := BitBtnCancel.Height;
  BitBtnEndOfDayHeight := BitBtnEndOfDay.Height;
  PanelButtonsHeight := PanelButtons.Height;
  // RV080.1.
  DBGridSelectorFontSize := DBGridSelector.Font.Size;
  // This is already created!
//  DialogSelectDownTypeF := TDialogSelectDownTypeF.Create(Application);
//  ATranslateHandlerManReg.TranslateDialogSelectDownType;
//  DialogSelectDownTypeF.Visible := False;
  ButtonOkLeft := BitBtnOk.Left; // PIM-12
  ButtonCancelLeft := BitBtnCancel.Left; // PIM-12
  ButtonEndOfDayLeft := BitBtnEndOfDay.Left; // PIM-12
  ButtonOkNewLeft := BitBtnOk.Left + BitBtnEndOfDay.Width; // PIM-12
  ButtonCancelNewLeft := BitBtnCancel.Left + BitBtnEndOfDay.Width; // PIM-12
end;

procedure TDialogChangeJobF.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  tmrEndOfDayBlink.Enabled := False;
  tmrCancel.Enabled := False;
end;

// MR:01-NOV-2007 Version 2.0.139.14
// Build in a timer for a time-out of 60 seconds. This prevents the creation
// of a record if a user forgot to confirm his/her action. This can happen
// when using more than 1 PC for Timerecording and can lead to wrong
// timerecording-records with wrong hours (too many).
procedure TDialogChangeJobF.tmrCancelTimer(Sender: TObject);
begin
  inherited;
  ModalResult := ModalCancel;
  Close;
end;

// RV079.1.
procedure TDialogChangeJobF.HandleButtonSize;
{var
  m, n, o: Double; }
begin
// 20014289
// Skip this part, make button 1 (bigger) size as default
{
  m := 1.7;
  n := 1.8;
  o := 1.5;
  if IsMaximized then
  begin
    PanelButtons.Height := Round(PanelButtonsHeight * 1.5);
    SpeedButtonFirst.Height := Round(SpeedButtonFirstHeight * m);
    SpeedButtonPrior.Height := Round(SpeedButtonPriorHeight * m);
    SpeedButtonNext.Height := Round(SpeedButtonNextHeight * m);
    SpeedButtonLast.Height := Round(SpeedButtonLastHeight * m);
    SpeedButtonFirst.Width := Round(SpeedButtonFirstWidth * o);
    SpeedButtonPrior.Width := Round(SpeedButtonPriorWidth * o);
    SpeedButtonNext.Width := Round(SpeedButtonNextWidth * o);
    SpeedButtonLast.Width := Round(SpeedButtonLastWidth * o);
    PanelBFirst.Width := Round(PanelBFirstWidth * o);
    PanelBPrior.Width := Round(PanelBPriorWidth * o);
    PanelNext.Width := Round(PanelNextWidth * o);
    PanelBLast.Width := Round(PanelBLastWidth * o);
    BitBtnOk.Height := Round(BitBtnOkHeight * n);
    BitBtnCancel.Height := Round(BitBtnCancelHeight * n);
    BitBtnEndOfDay.Height := Round(BitBtnEndOfDayHeight * n);
    // RV080.1.
    DBGridSelector.Font.Size := Round(DBGridSelectorFontSize  * n);
  end
  else
  begin
    SpeedButtonFirst.Height := SpeedButtonFirstHeight;
    SpeedButtonPrior.Height := SpeedButtonPriorHeight;
    SpeedButtonNext.Height := SpeedButtonNextHeight;
    SpeedButtonLast.Height := SpeedButtonLastHeight;
    SpeedButtonFirst.Width := SpeedButtonFirstWidth;
    SpeedButtonPrior.Width := SpeedButtonPriorWidth;
    SpeedButtonNext.Width := SpeedButtonNextWidth;
    SpeedButtonLast.Width := SpeedButtonLastWidth;
    PanelBFirst.Width := PanelBFirstWidth;
    PanelBPrior.Width := PanelBPriorWidth;
    PanelNext.Width := PanelNextWidth;
    PanelBLast.Width := PanelBLastWidth;
    BitBtnOk.Height := BitBtnOkHeight;
    BitBtnCancel.Height := BitBtnCancelHeight;
    BitBtnEndOfDay.Height := BitBtnEndOfDayHeight;
    PanelButtons.Height := PanelButtonsHeight;
    // RV080.1.
    DBGridSelector.Font.Size := DBGridSelectorFontSize;
  end;
}  
end;

// 20015302 Prevent it is moved
procedure TDialogChangeJobF.WMSysCommand(var Msg: TWMSysCommand);
begin
  if ((Msg.CmdType and $FFF0) = SC_MOVE) or
    ((Msg.CmdType and $FFF0) = SC_SIZE) then
  begin
    Msg.Result := 0;
    Exit;
  end;
  inherited;
end;

procedure TDialogChangeJobF.btnUpClick(Sender: TObject);
begin
  inherited;
  SpeedButtonPriorClick(Sender);
  //DBGridSelector.DataSource.DataSet.Prior;
end;

procedure TDialogChangeJobF.btnLowClick(Sender: TObject);
begin
  inherited;
  SpeedButtonNextClick(Sender);
  //DBGridSelector.DataSource.DataSet.Next;
end;

procedure TDialogChangeJobF.btnDownClick(Sender: TObject);
begin
  inherited;
  ModalResult := ModalCancel;
  if DialogSelectDownTypeF.ShowModal = mrOK then
  begin
    case DialogSelectDownTypeF.DownType of
    1: ModalResult := ModalMechanicalDown;
    2: ModalResult := ModalNoMerchandizeDown;
    end;
  end;
end;

procedure TDialogChangeJobF.BtnBreakClick(Sender: TObject);
begin
  inherited;
  ModalResult := ModalBreak;
end;

procedure TDialogChangeJobF.BtnLunchClick(Sender: TObject);
begin
  inherited;
  if ORASystemDM.LunchBtnScanOut then
    ModalResult := ModalEndOfDay
  else
    ModalResult := ModalBreak;
end;

// PIM-60
procedure TDialogChangeJobF.tmrEndOfDayBlinkTimer(Sender: TObject);
begin
  inherited;
  if BitBtnEndOfDay.Visible then // PIM-12
  begin
    if BitBtnEndOfDay.Color = EndOfDayColor then
      BitBtnEndOfDay.Color := clMyRed
    else
      BitBtnEndOfDay.Color := EndOfDayColor;
  end;
end;

end.

