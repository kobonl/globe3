(*
  MRA:15-JUL-2016 PIM-203
  - Time Recording via Card Readers
  MRA:4-JAN-2017 PIM-203 Bugfix
  - When there were 2 cards read it gave an exception (list out of bounds),
    reason: It removes an element when it was done, so the for-loop
    must be done from last element to 0.
  MRA:20-JAN-2017 PIM-260
  - Process a scan when the ID was read via the socket, instead of doing this
    via a separate timer. Reason: Because we now have multiple timers, a scan
    can processed directly wihout holding up the other card-readers.
  MRA:1-DEC-2017 PIM-330
  - Because of problems with blocked session, it must be prevented a procedure
    is not fully done before another procedure is run via timers!
  MRA:20-DEC-2017 PIM-330
  - To prevent a blocked session, be sure there is either a commit or
    a rollback done!
  MRA:23-JAN-2018 PIM-346
  - Blocked sessions problem. Put a Commit after an update-command. In exception
    are the Rollbacks and in the Finally we also do a rollback when needed.
  MRA:28-MAR-2018 GLOB3-112
  - TimeRecordingCR Add option to read ID via RAW-field of IDCard-table
  - Related to this order, changed position where messsage was given about
    scan processed, because it gave this always.
  MRA:2-MAY-2018 GLOB3-120 Rework
  - If first character of Host is not numeric, then skip it.
  - This is the Host that can be entered per Workspot, and gives
    the option to skip it when it does not start with a number.
  MRA:4-JAN-2019 GLOB3-202
  - Scan via machine-card readers and do not scan out
*)
unit UTimeRecordingCR;

interface

uses
  Classes, SysUtils, DateUtils, Oracle, ORASystemDMT;

const
  HOURSAGO: Double = 0.5;

type
  TErrorLog = procedure(const msg: String) of Object;
  TLog = procedure(const msg: String) of Object;

type
  TWorkspotHostPort = class(TObject)
    PlantCode: String;
    WorkspotCode: String;
    Host: String;
    Port: Integer;
    Index: Integer;
  public
    constructor Create;
    destructor Destroy; override;
    function ToString: String;
  end;

type
  TWSID = class(TObject)
    PlantCode: String;
    WorkspotCode: String;
    ID: String;
    Timestamp: TDateTime;
  public
    constructor Create;
    destructor Destroy; override;
    function ToString: String;
  end;

type
  TWSLED = class(TObject)
    PlantCode: String;
    WorkspotCode: String;
    Host: String;
    Port: Integer;
    Index: Integer;
  public
    constructor Create;
    destructor Destroy; override;
    function ToString: String;
  end;

var
  WHostPortList: TList;
//  WSIDList: TList;
  WSLEDList: TList;
  AErrorLog: TErrorLog;
  ALog: TLog;

procedure ErrorLog(const AMsg: String);
procedure Log(const AMsg: String);
function FindWHostPortList(APlantCode, AWorkspotCode, AHost: String;
  APort: Integer): TWorkspotHostPort;
function FindWSHostPortListByPW(APlantCode, AWorkspotCode: String):
  TWorkspotHostPort;
procedure AddWHostPortList(APlantCode, AWorkspotCode, AHost: String;
  APort: Integer);
procedure AssignWHostPortList(APlantCode: String);
//procedure AssignWSIDList(AID, APlantCode, AWorkspotCode: String);
procedure ProcessScan(AWSID: TWSID);
procedure CheckProcessScan(AWSID: TWSID);
// procedure ProcessScans;
function CheckOpenScan(APlantCode, AWorkspotCode: String): Boolean;
procedure AddWSLedList(APlantCode, AWorkspotCode: String);
function FindWSLEDList(APlantCode, AWorkspotCode: String): TWSLED;
procedure ProcessOneScan(APlantCode, AWorkspotCode, AID: String; ATimestamp: TDateTime);

implementation

uses
  TimeRecordingCRDMT, TimeRecordingDMT, UScannedIDCard, UGlobalFunctions,
  UPimsConst, UPimsMessageRes;

procedure ErrorLog(const AMsg: String);
begin
  if Assigned(AErrorLog) then
    AErrorLog(AMsg);
end;

procedure Log(const AMsg: String);
begin
  if Assigned(ALog) then
    ALog(AMsg);
end;

{ TWorkspotHostPort }

constructor TWorkspotHostPort.Create;
begin
//
end;

destructor TWorkspotHostPort.Destroy;
begin
//
  inherited;
end;

function TWorkspotHostPort.ToString: String;
begin
  Result := PlantCode + ';' + WorkspotCode + ';' + Host + ';' + IntToStr(Port);
end; // ToString

function FindWHostPortList(APlantCode, AWorkspotCode, AHost: String; APort: Integer): TWorkspotHostPort;
var
  I: Integer;
  AWorkspotHostPort: TWorkspotHostPort;
begin
  Result := nil;
  for I := 0 to WHostPortList.Count - 1 do
  begin
    AWorkspotHostPort := WHostPortList.Items[I];
    if (AWorkspotHostPort.PlantCode = APlantCode) and
      (AWorkspotHostPort.WorkspotCode = AWorkspotCode) and
      (AWorkspotHostPort.Host = AHost) and
      (AWorkspotHostPort.Port = APort) then
    begin
      Result := AWorkspotHostPort;
      Break;
    end;
  end;
end; // FindWHostPortList

function FindWSHostPortListByPW(APlantCode, AWorkspotCode: String):
  TWorkspotHostPort;
var
  I: Integer;
  AWorkspotHostPort: TWorkspotHostPort;
begin
  Result := nil;
  for I := 0 to WHostPortList.Count - 1 do
  begin
    AWorkspotHostPort := WHostPortList.Items[I];
    if (AWorkspotHostPort.PlantCode = APlantCode) and
      (AWorkspotHostPort.WorkspotCode = AWorkspotCode) then
    begin
      Result := AWorkspotHostPort;
      Break;
    end;
  end;
end; // FindWSHostPortListByPW

procedure AddWHostPortList(APlantCode, AWorkspotCode, AHost: String; APort: Integer);
var
  AWorkspotHostPort: TWorkspotHostPort;
  // GLOB3-120
  function CheckHost: Boolean;
  begin
    Result := False;
    if AHost <> '' then
      if AHost[1] in ['0'..'9'] then
        Result := True;
  end;
begin
  if CheckHost then
  begin
    if not Assigned(FindWHostPortList(APlantCode, AWorkspotCode, AHost, APort)) then
    begin
      AWorkspotHostPort := TWorkspotHostPort.Create;
      AWorkspotHostPort.PlantCode := APlantCode;
      AWorkspotHostPort.WorkspotCode := AWorkspotCode;
      AWorkspotHostPort.Host := AHost;
      AWorkspotHostPort.Port := APort;
      AWorkspotHostPort.Index := WHostPortList.Count;
      WHostPortList.Add(AWorkspotHostPort);
    end;
  end;
end; // AddWHostPortList

procedure AssignWHostPortList(APlantCode: String);
begin
  with TimerecordingCRDM.oqWorkspotHostPort do
  begin
    ClearVariables;
    SetVariable('PLANT_CODE', APlantCode);
    SetVariable('BATCH', TimeRecordingCRDM.Batch);
    Execute;
    while not Eof do
    begin
      AddWHostPortList(
        FieldAsString('PLANT_CODE'),
        FieldAsString('WORKSPOT_CODE'),
        FieldAsString('HOST'),
        FieldAsInteger('PORT')
        );
      Next;
    end;
  end;
end; // AssignWHostPortList

(*
procedure AssignWSIDList(AID, APlantCode, AWorkspotCode: String);
var
  AWSID: TWSID;
begin
  AWSID := TWSID.Create;
  AWSID.PlantCode := APlantCode;
  AWSID.WorkspotCode := AWorkspotCode;
  AWSID.ID := AID;
  AWSID.Timestamp := Now;
  WSIDList.Add(AWSID);
end; // AssignWSIDList
*)

function CheckOpenScan(APlantCode, AWorkspotCode: String): Boolean;
var
  LastIDCard, NextIDCard: TScannedIDCard;
  EmployeeNumber: Integer;
  EmployeeName: String;
  CurrentNow: TDateTime;
begin
  Result := False;
  // Look 1 minute in the future, to prevent it does not find most
  // recent made scan.
  CurrentNow := Now + (1/24/60);
  EmployeeNumber := -1;
  EmployeeName := '';
  try
    try
      EmptyIDCard(NextIDCard);
      NextIDCard.DateIn := CurrentNow;
      TimeRecordingDM.PopulateIDCard(NextIDCard);
      EmptyIDCard(LastIDCard);
      LastIDCard.EmployeeCode := -1;
      LastIDCard.PlantCode := APlantCode;
      LastIDCard.WorkSpotCode := AWorkspotCode;
      if TimeRecordingDM.DeterminePreviousWorkSpot(
        NextIDCard.DateIn,
        LastIDCard,
        CurrentNow, False, True,
        EmployeeNumber, EmployeeName,
        HOURSAGO) then
      begin
        if not LastIDCard.Processed then
          Result := True;
      end;
      if ORASystemDM.OracleSession.InTransaction then
        ORASystemDM.OracleSession.Commit;
    except
      on E: EOracleError do
      begin
        if ORASystemDM.OracleSession.InTransaction then
          ORASystemDM.OracleSession.Rollback;
        ErrorLog(E.Message);
      end;
      on E: Exception do
      begin
        if ORASystemDM.OracleSession.InTransaction then
          ORASystemDM.OracleSession.Rollback;
        ErrorLog(E.Message);
      end;
    end;
  finally
    if ORASystemDM.OracleSession.InTransaction then
      ORASystemDM.OracleSession.Rollback;
  end;
end; // CheckOpenScan

procedure ProcessScan(AWSID: TWSID);
var
  LastIDCard, NextIDCard: TScannedIDCard;
  EmployeeNumberDummy: Integer;
  EmployeeNameDummy: String;
  PrePlantText: String;
  SavePrevPlant, SavePrevWK, SavePrevJob, SavePrevDate: String;
  LastNow: TDateTime;
  EditPrePlantText, EditPreDateFromText, EditPreWorkspotText,
  EditPreJobText: String;
begin
  try
    try
      EmptyIDCard(NextIDCard);
      NextIDCard.IDCard := AWSID.ID;
      NextIDCard.DateIn := RoundTimeConditional(AWSID.Timestamp, 1);
      TimeRecordingDM.PopulateIDCard(NextIDCard);
      LastNow := AWSID.Timestamp;

      // Determine previous open scan on workspot-level
      EmptyIDCard(LastIDCard);
      LastIDCard.EmployeeCode := -1;
      LastIDCard.PlantCode := AWSID.PlantCode;
      LastIDCard.WorkSpotCode := AWSID.WorkspotCode;
      if TimeRecordingDM.DeterminePreviousWorkSpot(NextIDCard.DateIn, LastIDCard,
        AWSID.Timestamp, False, True, EmployeeNumberDummy,
        EmployeeNameDummy,
        HOURSAGO) then
      begin
        // When found, close it when it was from a different employee.
        if (LastIDCard.DateIn > NullDate) and (not LastIDCard.Processed) then
        begin
          LastIDCard.EmployeeCode := EmployeeNumberDummy;
          if (LastIDCard.EmployeeCode <> NextIDCard.EmployeeCode) then
          begin
            PrePlantText := SPimsEndDay;
            LastIDCard.DateOut := AWSID.Timestamp; // GLOB3-202
            TimeRecordingDM.HandleScanAction(
              LastIDCard, NextIDCard,
              PrePlantText,
              True {AIsScanning},
              SavePrevPlant, SavePrevWK, SavePrevJob, SavePrevDate,
              AWSID.Timestamp {ACurrentNow},
              LastNow,
              EditPrePlantText, EditPreDateFromText,
              EditPreWorkspotText, EditPreJobText
              );
          end;
        end;
      end;

      // Determine previous open scan on employee-level
      EmptyIDCard(LastIDCard);
      LastIDCard.EmployeeCode := NextIDCard.EmployeeCode;
      LastIDCard.EmplName := NextIDCard.EmplName;
      LastIDCard.CutOfTime := NextIDCard.CutOfTime;
      TimeRecordingDM.DeterminePreviousWorkSpot(NextIDCard.DateIn, LastIDCard,
        AWSID.Timestamp, False, False, EmployeeNumberDummy,
        EmployeeNameDummy,
        HOURSAGO);

      if not LastIDCard.Processed then
        LastIDCard.DateOut := NextIDCard.DateIn;

      if (LastIDCard.DateIn > NullDate) and (not LastIDCard.Processed) then
        EditPreDateFromText := FormatDateTime(LONGDATE, LastIDCard.DateIn)
      else
        EditPreDateFromText := FormatDateTime(LONGDATE,
          RoundTimeConditional(AWSID.Timestamp, 1));

      PrePlantText := LastIDCard.Plant;
      EditPrePlantText := LastIDCard.Plant;
      EditPreWorkspotText := LastIDCard.WorkSpot;
      EditPreJobText := LastIDCard.Job;
      SavePrevPlant := EditPrePlantText;
      SavePrevWK := EditPreWorkspotText;
      SavePrevJob := EditPreJobText;
      SavePrevDate := EditPreDateFromText;

      NextIDCard.PlantCode := AWSID.PlantCode;
      NextIDCard.WorkSpotCode := AWSID.WorkspotCode;
      NextIDCard.JobCode := ATWORKJOB;
      LastNow := AWSID.Timestamp;

      // Check on:
      // - Was there a last-scan?
      // - If so, was this on same plant+workspot as next-scan?
      if (LastIDCard.DateIn > NullDate) and (not LastIDCard.Processed) then
      begin
        if (LastIDCard.PlantCode = NextIDCard.PlantCode) and
          (LastIDCard.WorkspotCode = NextIDCard.WorkSpotCode) then
          PrePlantText := SPimsEndDay;
      end;

      TimeRecordingDM.HandleScanAction(
        LastIDCard, NextIDCard,
        PrePlantText,
        True {AIsScanning},
        SavePrevPlant, SavePrevWK, SavePrevJob, SavePrevDate,
        AWSID.Timestamp {ACurrentNow},
        LastNow,
        EditPrePlantText, EditPreDateFromText,
        EditPreWorkspotText, EditPreJobText
        );

      AddWSLEDList(LastIDCard.PlantCode, LastIDCard.WorkSpotCode);
      AddWSLEDList(NextIDCard.PlantCode, NextIDCard.WorkSpotCode);

      if ORASystemDM.OracleSession.InTransaction then
        ORASystemDM.OracleSession.Commit;
    except
      on E: EOracleError do
      begin
        if ORASystemDM.OracleSession.InTransaction then
          ORASystemDM.OracleSession.Rollback;
        ErrorLog(E.Message);
      end;
      on E: Exception do
      begin
        if ORASystemDM.OracleSession.InTransaction then
          ORASystemDM.OracleSession.Rollback;
        ErrorLog(E.Message);
      end;
    end;
  finally
    if ORASystemDM.OracleSession.InTransaction then
      ORASystemDM.OracleSession.Rollback;
  end;
end; // ProcessScan

procedure CheckProcessScan(AWSID: TWSID);
var
  ErrorCode: String;
  OldID: String;
begin
  with TimeRecordingDM do
  begin
    try
      if TimeRecordingDM.Raw then
      begin
        OldID := AWSID.ID;
        AWSID.ID := TimeRecordingDM.DetermineIDCardRaw(AWSID.ID);
        if OldID = AWSID.ID then
          Log('ID not found!');
      end;
      ErrorCode := ErrorToString(ValueIDCard(AWSID.ID, AWSID.Timestamp));
      if ErrorCode <> '' then
        ErrorLog(ErrorCode)
      else
      begin
        if TimeRecordingDM.Raw then
          Log('IDCard is OK (' + AWSID.ID + ')')
        else
          Log('IDCard is OK');
        ProcessScan(AWSID);
        Log('Scan processed'); // Do this here, not in finally!
      end;
    finally
//      WSIDList.Remove(AWSID); // Afterwards remove it from the list.
    end;
  end; // with
end; // ProcessScan

(*
procedure ProcessScans;
var
  I: Integer;
  AWSID: TWSID;
begin
  // This must be done in reverse order! Because it removes the last element
  // during the process.
  for I := WSIDList.Count - 1 downto 0 do
  begin
    AWSID := WSIDList.Items[I];
    CheckProcessScan(AWSID);
  end;
end; // ProcessScans
*)

procedure ProcessOneScan(APlantCode, AWorkspotCode, AID: String; ATimestamp: TDateTime);
var
  AWSID: TWSID;
begin
  AWSID := TWSID.Create;
  try
    AWSID.PlantCode := APlantCode;
    AWSID.WorkspotCode := AWorkspotCode;
    AWSID.ID := AID;
    AWSID.Timestamp := ATimestamp;
    CheckProcessScan(AWSID);
  finally
    AWSID.Free;
  end;
end;

function FindWSLEDList(APlantCode, AWorkspotCode: String): TWSLED;
var
  I: Integer;
  AWSLED: TWSLED;
begin
  Result := nil;
  for I := 0 to WSLEDList.Count - 1 do
  begin
    AWSLED := WSLEDList.Items[I];
    if (AWSLED.PlantCode = APlantCode) and (AWSLED.WorkspotCode = AWorkspotCode) then
    begin
      Result := AWSLED;
      Break;
    end;
  end;
end; // FindWSLEDList

procedure AddWSLedList(APlantCode, AWorkspotCode: String);
var
  AWSLED: TWSLED;
  AWorkspotHostPort: TWorkspotHostPort;
begin
  if not Assigned(FindWSLEDList(APlantCode, AWorkspotCode)) then
  begin
    AWorkspotHostPort := FindWSHostPortListByPW(APlantCode, AWorkspotCode);
    if Assigned(AWorkspotHostPort) then
    begin
      AWSLED := TWSLED.Create;
      AWSLED.PlantCode := APlantCode;
      AWSLED.WorkspotCode := AWorkspotCode;
      AWSLED.Host := AWorkspotHostPort.Host;
      AWSLED.Port := AWorkspotHostPort.Port;
      AWSLED.Index := AWorkspotHostPort.Index;
      WSLEDList.Add(AWSLED);
    end;
  end;
end; // AddWSLedList

{ TWSID }

constructor TWSID.Create;
begin
  inherited;
//
end;

destructor TWSID.Destroy;
begin
//
  inherited;
end;

function TWSID.ToString: String;
begin
  Result := PlantCode + ';' + WorkspotCode + ';' +
    ID + ';' + DateTimeToStr(Timestamp);
end;

{ TWSLED }

constructor TWSLED.Create;
begin
  inherited;
//
end;

destructor TWSLED.Destroy;
begin
//
  inherited;
end;

function TWSLED.ToString: String;
begin
  Result := Host + ';' + IntToStr(Port);
end;

initialization
  WHostPortList := TList.Create;
//  WSIDList := TList.Create;
  WSLEDList := TList.Create;
  AErrorLog := nil;
  ALog := nil;

finalization
  WHostPortList.Free;
//  WSIDList.Free;
  WSLEDList.Free;

end.
