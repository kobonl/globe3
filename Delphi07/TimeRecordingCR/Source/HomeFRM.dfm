inherited HomeF: THomeF
  Left = 507
  Top = 243
  Caption = 'Time Recording Card Reader'
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlInsertBase: TPanel
    Font.Color = clBlack
    Font.Height = -11
    object Memo1: TMemo
      Left = 0
      Top = 0
      Width = 624
      Height = 360
      Align = alClient
      ScrollBars = ssBoth
      TabOrder = 0
    end
  end
  object tmrProcessScans: TTimer
    Enabled = False
    Interval = 5000
    OnTimer = tmrProcessScansTimer
    Left = 336
    Top = 117
  end
  object tmrHandleLED: TTimer
    Enabled = False
    OnTimer = tmrHandleLEDTimer
    Left = 336
    Top = 181
  end
  object tmrReadAllSockets: TTimer
    Enabled = False
    Interval = 100
    OnTimer = tmrReadAllSocketsTimer
    Left = 336
    Top = 58
  end
  object tmrCheckConnection: TTimer
    Enabled = False
    Interval = 60000
    OnTimer = tmrCheckConnectionTimer
    Left = 344
    Top = 250
  end
end
