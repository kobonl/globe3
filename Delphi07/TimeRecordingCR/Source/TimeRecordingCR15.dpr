program TimeRecordingCR15;

uses
  Forms,
  BasePimsFRM in '..\..\Pims Shared\BasePimsFRM.pas' {BasePimsForm},
  UPimsConst in '..\..\Pims Shared\UPimsConst.pas',
  ColorButton in '..\..\Pims Shared\ColorButton.pas',
  ORASystemDMT in '..\..\Pims Shared\ORASystemDMT.pas' {ORASystemDM: TDataModule},
  UPimsMessageRes in '..\..\Pims Shared\UPimsMessageRes.pas',
  UGlobalFunctions in '..\..\Pims Shared\UGlobalFunctions.pas',
  UTimeZone in '..\..\Pims Shared\UTimeZone.pas',
  UPIMSVersion in '..\..\Pims Shared\UPIMSVersion.pas',
  BaseTopMenuFRM in '..\..\Pims Shared\BaseTopMenuFRM.pas' {BaseTopMenuForm},
  BaseTopLogoFRM in '..\..\Pims Shared\BaseTopLogoFRM.pas' {BaseTopLogoForm},
  HomeFRM in 'HomeFRM.pas' {HomeF},
  AboutFRM in '..\..\Pims Shared\AboutFRM.pas' {AboutForm},
  OrderInfoFRM in '..\..\Pims Shared\OrderInfoFRM.pas' {OrderInfoForm},
  TimeRecordingCRDMT in 'TimeRecordingCRDMT.pas' {TimeRecordingCRDM: TDataModule},
  UTimeRecordingCR in 'UTimeRecordingCR.pas',
  URFIDeasSocketPort in 'URFIDeasSocketPort.pas',
  TimeRecordingDMT in '..\..\TimeRecording\Source\TimeRecordingDMT.pas' {TimeRecordingDM: TDataModule},
  BasePimsSerialFRM in '..\..\Pims Shared\BasePimsSerialFRM.pas' {BasePimsSerialForm},
  UScannedIDCard in '..\..\Pims Shared\UScannedIDCard.pas',
  GlobalDMT in '..\..\Pims Shared\GlobalDMT.pas' {GlobalDM: TDataModule},
  CalculateTotalHoursDMT in '..\..\Pims Shared\CalculateTotalHoursDMT.pas' {CalculateTotalHoursDM: TDataModule},
  BaseDialogFRM in '..\..\Pims Shared\BaseDialogFRM.pas' {BaseDialogForm},
  SplashFRM in '..\..\Pims Shared\SplashFRM.pas' {SplashForm},
  UProductionScreenDefs in '..\..\Pims Shared\UProductionScreenDefs.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TORASystemDM, ORASystemDM);
  Application.CreateForm(TGlobalDM, GlobalDM);
  ORASystemDM.AppName := APPNAME_TRSCR + '15';
  Application.CreateForm(TCalculateTotalHoursDM, CalculateTotalHoursDM);
  Application.CreateForm(TTimeRecordingCRDM, TimeRecordingCRDM);
  TimeRecordingCRDM.Batch := 15;
  Application.CreateForm(TTimeRecordingDM, TimeRecordingDM);
  Application.CreateForm(THomeF, HomeF);
  Application.Run;
end.
