(*
  MRA:15-JUL-2016 PIM-203
  - Time Recording via Card Readers
  MRA:9-DEC-2016 PIM-203
  - It must always do the PortConnect to assign the correct host + port!
  MRA:18-DEC-2016 PIM-203
  - Stop tmrReadSockets during handling of socket. Start it again afterwards.
  MRA:4-JAN-2016 PIM-203
  - Clear Memo-component when there are more than 32000 lines.
  - Add timestamp to log-lines.
  - Log 'Start' at start of application.
  MRA:4-JAN-2016 PIM-254
  - Read ID with 8 characters for Rosslare-cards.
  - This is done by reading it first in hex, divide it in 2 and
    convert the 2 parts to decimal and paste them together.
  - Be sure the reader itself gives the expected result in hex!
  MRA:9-JAN-2017 PIM-254
  - Log to DB.
  MRA:10-JAN-2017 PIM-254
  - Shorten time for the 2 processes: ReadSockets + ProcessScans.
  - The time in msecs can be changed via INI-file.
  MRA:12-JAN-2017 PIM-203
  - Bugfix for:
    - When a card-reader was turn off/on during run of the program, it
      did not do anything anymore with this card-reader. Opening the
      Port does not give an error during this.
    - To solve it an extra timer is added that handles the LED-light.
      Only then it will detect the port is not open anymore and it will
      reconnect when the card-reader is turned on again.
    - An extra timer is added: tmrHandleLED. The interval can be set in
      INI-file (HandleLEDInterval) with default 5000 msecs.
  MRA:16-JAN-2017 PIM-203
  - Add extra list to keep track of workspots that are involved during scannning.
    This list is then used to know which card-readers must be checked, to
    decide if a LED must be changed.
  MRA:17-JAN-2017 PIM-203
  - Use multiple timers, one for each socket. In that way they can run parallel.
  MRA:20-JAN-2017 PIM-260
  - Process a scan when the ID was read via the socket, instead of doing this
    via a separate timer. Reason: Because we now have multiple timers, a scan
    can processed directly wihout holding up the other card-readers.
  - Note: When a socket is not available, can this give a problem for the
    other sockets/prccesses?
  MRA:22-JAN-2017 PIM-260
  - Add option to use a single timer or multiple timers. Via INI this can be
    changed.
  MRA:23-JAN-2017 PIM-264
  - Decrease logging.
  - Socket-errors should only be logged when debug=1, because that can give
    lots of logging (when machines/card-readers are powered off).
  - Showing what leds are on, do this in 1 line.
  MRA:1-DEC-2017 PIM-?
  - Because of problems with blocked session, it must be prevented a procedure
    is not fully done before another procedure is run via timers!
  MRA:14-DEC-2017 PIM-330.2
  - Check Oracle-Connection and reconnect when needed.
  - Do this via a timer for each minute.
  MRA:28-MAR-2018 GLOB3-112
  - TimeRecordingCR Add option to read ID via RAW-field of IDCard-table
  - This is needed when reading Mifare-cards.
  - Use settings:
    - Reader=(Empty)
    - IDLength=8
    - Raw=1
  MRA:19-APR-2018 GLOB3-120
  - Do not use computername in INI-file but use fixed PIMSORA
  - Show message it has found or not found INI-settings.
  - Also: Assign ConnectTimeOut BEFORE ReadINIFile!
  MRA:13-SEP-2018 GLOB3-127
  - TimeRecordingCR - Improve handling LEDS
  - Use Multi-threading for handling of ID read via cardreader
  - Use also Mutli-threading for handling of LEDS based on open scans.
  - Combine the two threads.
  MRA:27-SEP-2018 GLOB3-127
  - TimeRecordingCR - Improve handling LEDS
  - When using a db-session for each thread, this does not work correct, because
    then the current session was assigned to the global used session, and this
    gives problems: It did not commit anything until the program was closed.
  - Now 1 session is used (ORASystemDM.OracleSession), via property
    ThreadSafe = True, this will handle the statements in a queue. This can
    slow down the performance on db-level.
  - When using 1 db-session per thread, then probably lots of code must
    be changed/added, because several datamodules are used with db-components
    linked to the global OracleSession.
  MRA:12-OCT-2018 GLOB3-127
  - TimeRecordingCR - Improve handling LEDS
  - Tested with 1 real card-reader and 1 dummy (IP of computer).
  - This gives already problems.
  - It looks like the THLEDThread interferes too much and also clears the
    readbuffer. This means an ID that was just read can be missed, because of
    this. This THELEDThread is now doing something each 5 seconds.
  - To solve the above:
    - Do not clear the readbuffer?
    - Do it each 60 seconds?
  MRA:14-NOV-2018 GLOB3-127.1
  - Force close of the application.
  MRA:20-NOV-2018 GLOB3-127.2
  - When ReadBuffer gets too full, clear it.
  MRA:22-NOV-2018 GLOB3-127.3
  - Prevent it keeps an error-message in the buffer.
  MRA:27-NOV-2018 GLOB3-127.4
  - Clear the buffer after 1 minute.
  MRA:4-JAN-2019 GLOB3-202
  - Scan via machine-card readers and do not scan out
*)
unit HomeFRM;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseTopLogoFRM, dxCntner, Menus, StdActns, ActnList, ImgList,
  ComCtrls, ExtCtrls, ORASystemDMT, TimeRecordingCRDMT, StdCtrls, IniFiles,
  UTimeRecordingCR, URFIDeasSocketPort, UGlobalFunctions, IdException, jpeg,
  TimeRecordingDMT, SyncObjs, DBClient, Oracle, DB, OracleData;

const
  INIFilename='TimeRecordingCR.INI';
  IDLengthDefault=8;
  ReadSocketsIntervalDefault=200;
  ProcessScansIntervalDefault=1000;
  HandleLEDIntervalDefault=60000;
  HandleLEDAtCounter=25;
  ReadBufferMaxLen=32;

type
  TCRThread = Class( TThread )
    private
      FID: Integer;
      FUse: Boolean;
      FOnDone: TNotifyEvent;
      FCRThreadRFSocketIndex: Integer;
//      procedure NotifyIsDone;
//      procedure CRThreadRFSocketPortHandler;
      procedure Init(AIndex: Integer);
    public
      procedure Execute; Override;
      Property UseCriticalSection: Boolean read FUse write FUse;
      Property OnDone: TNotifyEvent read FOnDone write FOnDone;
      property CRThreadRFSocketIndex: Integer read FCRThreadRFSocketIndex write FCRThreadRFSocketIndex;
    end;

type
  THLEDThread = Class( TThread )
    private
      FID: Integer;
      FUse: Boolean;
      FOnDone: TNotifyEvent;
      FCRThreadRFSocketIndex: Integer;
      FCounter: Integer;
//      procedure NotifyIsDone;
      procedure HDLEDHandler;
      procedure Init(AIndex: Integer);
    public
      procedure Execute; Override;
      Property UseCriticalSection: Boolean read FUse write FUse;
      Property OnDone: TNotifyEvent read FOnDone write FOnDone;
      property CRThreadRFSocketIndex: Integer read FCRThreadRFSocketIndex write FCRThreadRFSocketIndex;
      property Counter: Integer read FCounter write FCounter;
    end;

type
  THomeF = class(TBaseTopLogoForm)
    Memo1: TMemo;
    tmrProcessScans: TTimer; // NOT USED
    tmrHandleLED: TTimer;
    tmrReadAllSockets: TTimer;
    tmrCheckConnection: TTimer;
    procedure FormCreate(Sender: TObject);
    procedure tmrReadSocketsTimer(Sender: TObject);
    procedure tmrProcessScansTimer(Sender: TObject); // NOT USED
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure tmrHandleLEDTimer(Sender: TObject);
    procedure tmrReadAllSocketsTimer(Sender: TObject);
    procedure tmrCheckConnectionTimer(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  protected
    procedure WMQueryEndSession(var Message: TWMQueryEndSession); message WM_QUERYENDSESSION;
  private
    { Private declarations }
    AppRootPath: String;
    FPlantCode: String;
    RFSocketIndex: Integer;
    FIDLength: Integer;
    FInitLightsFlag: Boolean;
    FDebug: Boolean;
    FReaderRosslare: Boolean;
    FProcessScansInterval: Integer;
    FReadSocketsInterval: Integer;
    FCloseAtEndTime: Boolean;
    FEndTime: TDateTime;
    FEndTimeMins: Integer;
    FEndTimeHrs: Integer;
    FHandleLEDInterval: Integer;
    ReadSocketTimerList: TList;
    FSingleTimer: Boolean;
    FLogLine: String;
    FReadSocketBusy: Boolean;
    FHandleLEDBusy: Boolean;
    FConnectTimeOut: Integer;
    FRaw: Boolean;
    CRThreadList: TList;
    HLEDThreadList: TList;
    procedure WriteINIFile;
    procedure ReadINIFile;
    procedure AssignRFSocketPortList;
    function RFSocketPortHandler: Boolean;
    procedure SetEndTime(const Value: TDateTime);
    procedure HandleOneLED(AIndex: Integer);
(*    procedure HandleLEDOutsideScan(AIndex: Integer);*)
    procedure HandleAllLEDS;
    procedure HandleLEDS;
//    procedure CreateReadSocketTimers;
//    procedure EndCreateSocketTimers;
//    procedure DoOnDone( Sender: Tobject );
    procedure StartThreads;
    procedure StopThreads;
  public
    { Public declarations }
    procedure AddLog(AMsg: String);
    procedure ErrorLog(const AMsg: String);
    procedure Log(const AMsg: String);
    procedure LogLogLine(ALogLine: String);
    procedure ShowInfo;
    procedure SessionCommit;
    procedure CloseNow;
    property PlantCode: String read FPlantCode write FPlantCode;
    property IDLength: Integer read FIDLength write FIDLength;
    property InitLightsFlag: Boolean read FInitLightsFlag write FInitLightsFlag;
    property Debug: Boolean read FDebug write FDebug;
    property ReaderRosslare: Boolean read FReaderRosslare write FReaderRosslare;
    property ReadSocketsInterval: Integer read FReadSocketsInterval write FReadSocketsInterval;
    property ProcessScansInterval: Integer read FProcessScansInterval write FProcessScansInterval;
    property HandleLEDInterval: Integer read FHandleLEDInterval write FHandleLEDInterval;
    property EndTime: TDateTime read FEndTime write SetEndTime;
    property CloseAtEndTime: Boolean read FCloseAtEndTime write FCloseAtEndTime;
    property EndTimeHrs: Integer read FEndTimeHrs write FEndTimeHrs;
    property EndTimeMins: Integer read FEndTimeMins write FEndTimeMins;
    property SingleTimer: Boolean read FSingleTimer write FSingleTimer;
    property LogLine: String read FLogLine write FLogLine;
    property ReadSocketBusy: Boolean read FReadSocketBusy write FReadSocketBusy;
    property HandleLEDBusy: Boolean read FHandleLEDBusy write FHandleLEDBusy;
    property ConnectTimeOut: Integer read FConnectTimeOut write FConnectTimeOut Default 500;
    property Raw: Boolean read FRaw write FRaw;
  end;

function PathCheck(Path: String): String;
procedure WErrorLog(AMsg: String);
procedure WLog(AMsg: String);

var
  HomeF: THomeF;
  SharedCs: TCriticalSection;
  HLedCounter: Integer;

implementation

{$R *.dfm}

procedure WErrorLog(AMsg: String);
begin
  if Debug then
  begin
    UGlobalFunctions.WErrorlog(AMsg);
    HomeF.SessionCommit;
  end;
end;

procedure WLog(AMsg: String);
begin
  HomeF.AddLog(AMsg);
  UGlobalFunctions.WLog(AMsg);
  HomeF.SessionCommit;
end;

procedure THomeF.Log(const AMsg: String);
begin
  WLog(AMsg);
end;

procedure THomeF.FormCreate(Sender: TObject);
begin
  inherited;
  CRThreadList := TList.Create;
  HLEDThreadList := TList.Create;
  SharedCs := TCriticalSection.Create;
(*  SharedCsHLED := TCriticalSection.Create; *)

  AppRootPath := GetCurrentDir;
  ReadSocketBusy := False;
  HandleLEDBusy := False;
  ORASystemDM.LogErrors := True;
  ORASystemDM.LogToDB := True; // Log always to DB
  lblMessage.Caption := Caption;
  AErrorLog := ErrorLog;
  TimeRecordingDM.OnScanError := ErrorLog; // PIM-330.2 Any errors must be logged
  ALog := Log;
  InitLightsFlag := False;
  RFSocketIndex := 0;
  PlantCode := '1';
  IDLength := IDLengthDefault;
  Debug := False;
  ReaderRosslare := True;
  SingleTimer := False;
  ReadSocketsInterval := ReadsocketsIntervalDefault;
  ProcessScansInterval := ProcessScansIntervalDefault;
  HandleLEDInterval := HandleLEDIntervalDefault;
  CloseAtEndTime := True;
  EndTimeHrs := 03;
  EndTimeMins := 0;
  Raw := False;
  ConnectTimeOut := 500; // Bugfix
  Log('--- Start ---');
  ReadINIFile;
  EndTime := Now;
  tmrReadAllSockets.Interval := ReadSocketsInterval;
  tmrProcessScans.Interval := ProcessScansInterval;
  tmrHandleLED.Interval := HandleLEDInterval;
  URFIDeasSocketPort.Debug := Debug;
  URFIDeasSocketPort.ReaderRosslare := ReaderRosslare;
  AssignWHostPortList(PlantCode);
  AssignRFSocketPortList;
  ReadSocketTimerList := TList.Create;
  // GLOB3-202
  if ORASystemDM.TimeRecCRDoNotScanOut then
  begin
    TimeRecordingDM.DetermineRoamingWorkspot(PlantCode);
    if TimeRecordingDM.RoamingWorkspotCode = '' then
    begin
      Log('Warning: There is no roaming workspot defined! Changed to: do scan out.');
      ORASystemDM.TimeRecCRDoNotScanOut := False;
    end;
  end;
  ShowInfo;
  if WHostPortList.Count > 0 then
  begin
    tmrHandleLED.Enabled := False; // PIM-330.2 Optimize // GLOB3-127 Replaced by multi-threads
    try
//      HandleAllLEDS; // GLOB3-127 This must be done once?
    except
    end;
    if SingleTimer then
    begin
//      tmrReadAllSockets.Enabled := True;
    end
{    else
      CreateReadSocketTimers; }
  end;
  tmrCheckConnection.Enabled := True;
  StartThreads; // GLOB3-127
end; // FormCreate

procedure THomeF.AddLog(AMsg: String);
begin
  if Memo1.Lines.Count > 32000 then
    Memo1.Lines.Clear;
  AMsg := DateTimeToStr(Now) + ' - ' + AMsg;
  Memo1.Lines.Add(AMsg);
end; // AddLog

procedure THomeF.ShowInfo;
var
  I: Integer;
  AWorkspotHostPort: TWorkspotHostPort;
  function BoolToStr(ABoolean: Boolean): String;
  begin
    if ABoolean then
      Result := '1'
    else
      Result := '0';
  end;
begin
  Log(
    'TRCR-Threads ' +
    'Batch:' + IntToStr(TimeRecordingCRDM.Batch) + 
    ' Plant:' + PlantCode +
    ' ID-Length:' + IntToStr(IDLength) +
    ' Debug:' + BoolToStr(Debug) +
    ' Do not scan out: ' + BoolToStr(ORASystemDM.TimeRecCRDoNotScanOut));
  if ORASystemDM.TimeRecCRDoNotScanOut then
    Log('Roaming workspot-code: ' + TimeRecordingDM.RoamingWorkspotCode +
      ' Job-code: ' + TimeRecordingDM.RoamingJobCode);
//    ' SingleTimer:' + BoolToStr(SingleTimer));
  if ReaderRosslare then
    Log('Reader:Rosslare (read 8 positions in hex and convert this)');
//  Log('ReadSocketsInterval:' + IntToStr(ReadSocketsInterval) + ' msecs' +
//  Log('ProcessScansInterval:' + IntToStr(ProcessScansInterval) + ' msecs');
//    ' HandleLEDInterval:' + IntToStr(HandleLEDInterval) + ' msecs');
  Log('CloseAtEndTime:' + BoolToStr(CloseAtEndTime) +
    ' EndTimeHrs:' + IntToStr(EndTimeHrs) +
    ' EndTimeMins:' + IntToStr(EndTimeMins) +
    ' EndTime:' + DateTimeToStr(EndTime) +
    ' ConnectTimeOut:' + IntToStr(ConnectTimeOut) +
    ' Raw:' + BoolToStr(Raw)
    );
  LogLine := '';
  if WHostPortList.Count > 0 then
  begin
    Log('Workspot-list (plant;workspot;host;port): (' + IntToStr(WHostPortList.Count) + ')');
    for I := 0 to WHostPortList.Count - 1 do
    begin
      AWorkspotHostPort := WHostPortList.Items[I];
      LogLine := LogLine + AWorkspotHostPort.ToString + ';';
    end;
    LogLogLine(LogLine);
  end
  else
    ErrorLog('Error: No Host+Port definitions found!');
end; // ShowInfo

function PathCheck(Path: String): String;
begin
  if Path <> '' then
    if Path[length(Path)] <> '\' then
      Path := Path + '\';
  Result := Path;
end; // PathCheck

procedure THomeF.WriteINIFile;
var
  Ini: TIniFile;
  Section: String;
begin
  Ini := TIniFile.Create(PathCheck(AppRootPath) + INIFilename);
  try
//    Section := ORASystemDM.CurrentComputerName; // Bugfix
    Section := 'PIMSORA';
    Ini.WriteString(Section, 'PlantCode', PlantCode);
    Ini.WriteInteger(Section, 'IDLength', IDLength);
    Ini.WriteBool(Section, 'Debug', Debug);
    Ini.WriteString(Section, 'Reader', '');
    Ini.WriteInteger(Section, 'ReadSocketsInterval', ReadSocketsInterval);
    Ini.WriteInteger(Section, 'ProcessScansInterval', ProcessScansInterval);
    Ini.WriteInteger(Section, 'HandleLEDInterval', HandleLEDInterval);
    Ini.WriteBool(Section, 'CloseAtEndTime', CloseAtEndTime);
    Ini.WriteInteger(Section, 'EndTimeHrs', EndTimeHrs);
    Ini.WriteInteger(Section, 'EndTimeMins', EndTimeMins);
    Ini.WriteBool(Section, 'SingleTimer', SingleTimer);
    Ini.WriteInteger(Section, 'ConnectTimeOut', ConnectTimeOut);
    Ini.WriteBool(Section, 'Raw', Raw);
  finally
    Ini.Free;
  end;
end; // WriteINIFile

procedure THomeF.ReadINIFile;
var
  Ini: TIniFile;
  Section: String;
begin
  if not FileExists(PathCheck(AppRootPath) + INIFilename) then
    WriteINIFile;
  Ini := TIniFile.Create(PathCheck(AppRootPath) + INIFilename);
  try
//    Section :=  ORASystemDM.CurrentComputerName; // Bugfix
    Section := 'PIMSORA';
    if Ini.SectionExists(Section) then
      Log('INI-settings found, using INI-settings.')
    else
    begin
      Log('WARNING: No settings found, using defaults. Use header [PIMSORA] in TimeRecordingCR.INI-file for settings!');
      Exit;
    end;
    if Ini.ValueExists(Section, 'PlantCode') then
      PlantCode := Ini.ReadString(Section, 'PlantCode', PlantCode);
    if Ini.ValueExists(Section, 'IDLength') then
      IDLength := Ini.ReadInteger(Section, 'IDLength', IDLength);
    if Ini.ValueExists(Section, 'Debug') then
      Debug := Ini.ReadBool(Section, 'Debug', Debug);
    if Ini.ValueExists(Section, 'Reader') then
      ReaderRosslare :=
        UpperCase(Ini.ReadString(Section, 'Reader', '')) = 'ROSSLARE';
    if ReaderRosslare then
      IDLength := 8;
    if Ini.ValueExists(Section, 'ReadSocketsInterval') then
    begin
      ReadSocketsInterval := Ini.ReadInteger(Section, 'ReadSocketsInterval',
        ReadSocketsInterval);
      if ReadSocketsInterval <= 0 then
        ReadSocketsInterval := ReadSocketsIntervalDefault;
    end;
    if Ini.ValueExists(Section, 'ProcessScansInterval') then
    begin
      ProcessScansInterval := Ini.ReadInteger(Section, 'ProcessScansInterval',
        ProcessScansInterval);
      if ProcessScansInterval <= 0 then
        ProcessScansInterval := ProcessScansIntervalDefault;
    end;
    if Ini.ValueExists(Section, 'HandleLEDInterval') then
    begin
      HandleLEDInterval := Ini.ReadInteger(Section, 'HandleLEDInterval',
        HandleLEDInterval);
      if HandleLEDInterval <= 0 then
        HandleLEDInterval := HandleLEDIntervalDefault;
    end;
    if Ini.ValueExists(Section, 'CloseAtEndTime') then
      CloseAtEndTime := Ini.ReadBool(Section, 'CloseAtEndTime', CloseAtEndTime);
    if Ini.ValueExists(Section, 'EndTimeHrs') then
      EndTimeHrs := Ini.ReadInteger(Section, 'EndTimeHrs', EndTimeHrs);
    if Ini.ValueExists(Section, 'EndTimeMins') then
      EndTimeMins := Ini.ReadInteger(Section, 'EndTimeMins', EndTimeMins);
    if Ini.ValueExists(Section, 'SingleTimer') then
      SingleTimer := Ini.ReadBool(Section, 'SingleTimer', SingleTimer);
    if Ini.ValueExists(Section, 'ConnectTimeOut') then
    begin
      ConnectTimeOut := Ini.ReadInteger(Section, 'ConnectTimeOut', ConnectTimeOut);
      if ConnectTimeOut <= 0 then
        ConnectTimeOut := 500;
    end;
    if Ini.ValueExists(Section, 'Raw') then
    begin
      if Ini.ReadInteger(Section, 'Raw', 0) = 1 then
        Raw := True
      else
        Raw := False;
    end;
    TimeRecordingDM.Raw := Raw;
  finally
    Ini.Free;
  end;
end; // ReadINIFile

procedure THomeF.AssignRFSocketPortList;
var
  I: Integer;
  AWorkspotHostPort: TWorkspotHostPort;
  ARFSocketPort: TRFSocketPort;
  ACRThread: TCRThread;
  AHLEDThread: THLEDThread;
begin
  for I := 0 to WHostPortList.Count - 1 do
  begin
    AWorkspotHostPort := WHostPortList.Items[I];
    URFIDeasSocketPort.AssignSocketPortList(
      AWorkspotHostPort.Host, AWorkspotHostPort.Port, IDLength,
      AWorkspotHostPort.PlantCode, AWorkspotHostPort.WorkspotCode);
  end;
  for I := 0 to RFSocketPortList.Count - 1 do
  begin
    ARFSocketPort := RFSocketPortList[I];
    ARFSocketPort.MyID := I + 1;
    ARFSocketPort.MyErrorLog := WErrorLog;
    ARFSocketPort.MyLog := WLog;
    ARFSocketPort.OnProcessScan := ProcessOneScan;
    ARFSocketPort.OnHandleLEDS := HandleLEDS;
    ARFSocketPort.ConnectTimeOut := ConnectTimeOut;
    // Thread CR
    ACRThread := TCRThread.Create(True);
    ACRThread.Init(I);
    CRThreadList.Add(ACRThread);
//    ACRThread.Resume;

    // Thread HDLED
    AHLEDThread := THLEDThread.Create(True);
    AHLEDThread.Init(I);
    HLEDThreadList.Add(AHLEDThread);
//    AHLEDThread.Resume;

  end;
end; // AssignRFSocketPortList

function THomeF.RFSocketPortHandler: Boolean;
var
  ARFSocketPort: TRFSocketPort;
begin
  Result := False;
  try
    ARFSocketPort := RFSocketPortList[RFSocketIndex];
    // PortConnect must ALWAYS be done, to assign the correct host + port
{
    if not ARFSocketPort.Open then
    begin
      AddLog('Host;Port:' + ARFSocketPort.ToString + '. Opening port...');
      ARFSocketPort.PortConnect(ARFSocketPort.Host, ARFSocketPort.Port);
      AddLog('Host;Port:' + ARFSocketPort.ToString + '. Port opened');
    end;
    if ARFSocketPort.Open then
}
    // Troubleshooting: (11-JAN-2017)
    // - When a reader is not available anymore then PortConnect still gives
    //   True. To solve this, try to send a command to the reader every minute
    //   or every 5 minutes, to know if it is still available.
    if ARFSocketPort.PortConnect(ARFSocketPort.Host, ARFSocketPort.Port) then
    begin
      try
        if (ARFSocketPort.ReadBuffer <> '') and
          ARFSocketPort.IDRead or ARFSocketPort.ColorRead then
        begin
          if ARFSocketPort.IDRead then
          begin
            Result := True;
            // Buffer has already changed! Use ID
(*            AssignWSIDList(ARFSocketPort.ReadBuffer,
              ARFSocketPort.PlantCode, ARFSocketPort.WorkspotCode); *)
            // Do this somewhere else, it is already processed here.
//            ErrorLog('ID=' + ARFSocketPort.ID + ' (' + ARFSocketPort.ToString + ')');
//            tmrProcessScans.Enabled := True;
            ARFSocketPort.IDRead := False;
          end
          else
          begin
            if ARFSocketPort.ColorRead then
            begin
              Log('Color=' + ARFSocketPort.ReadBuffer);
              ARFSocketPort.ColorRead := False;
            end;
          end;
          ARFSocketPort.ReadBuffer := '';
        end; // if
        // Do not do this: This has the effect it does not access the
        // card-reader anymore.
//        ARFSocketPort.PortDisconnect;
//        Sleep(250); // Wait
      except
        on E:EAccessViolation do
        begin
          ErrorLog(E.Message);
        end;
        on E:EIdClosedSocket do
        begin
          ErrorLog(E.Message);
        end;
        on E:Exception do
        begin
          ErrorLog(E.Message);
        end;
      end;
    end
    else
    begin
      ErrorLog('Error: ' + ARFSocketPort.ToString +
        '. Could not open port!');
    end;
  except
    on E:EAccessViolation do
    begin
      ErrorLog(E.Message);
    end;
    on E:EIdClosedSocket do
    begin
      ErrorLog(E.Message);
    end;
    on E:Exception do
    begin
      ErrorLog(E.Message);
    end;
  end;
end; // RFSocketPortHandler

procedure THomeF.tmrReadSocketsTimer(Sender: TObject);
var
  ATimer: TTimer;
  Done: Boolean;
begin
  inherited;
  Done := False;
  // This does not work?
//  if HandleLEDBusy then Exit;
  ATimer := nil;
  if (Sender is TTimer) then
    ATimer := (Sender as TTimer);
  if Assigned(ATimer) then
  begin
    try
      ReadSocketBusy := True;
      ATimer.Enabled := False;
      RFSocketIndex := ATimer.Tag;
      Done := RFSocketPortHandler;
    finally
      // 330.2 Handle LEDS (for same workspot but for scans made
      // on other stations) here.
//      if not Done then
//        HandleLEDOutsideScan(RFSocketIndex); // PIM-330.2
      ATimer.Enabled := True;
      ReadSocketBusy := False;
    end;
    if CloseAtEndTime then
    begin
      if Now >= EndTime then
        CloseNow;
    end;
  end;
end; // tmrReadSocketsTimer

procedure THomeF.ErrorLog(const AMsg: String);
begin
  WErrorLog(AMsg);
end;

// Do not do this!
procedure THomeF.tmrProcessScansTimer(Sender: TObject);
begin
  inherited;
{
  try
    tmrProcessScans.Enabled := False;
    tmrHandleLED.Enabled := False;
    ProcessScans;
  finally
    HandleLEDS;
    tmrHandleLED.Enabled := True;
    InitLightsFlag := False;
  end;
}
end; // tmrProcessScansTimer

// Handle LED based on scan, when open, led=green, when closed, led=red.
procedure THomeF.HandleOneLED(AIndex: Integer);
var
  ARFSocketPort: TRFSocketPort;
begin
  ARFSocketPort := RFSocketPortList[AIndex];
  ARFSocketPort.ReadBuffer := '';
  if CheckOpenScan(ARFSocketPort.PlantCode, ARFSocketPort.WorkspotCode) then
    ARFSocketPort.LEDGreen
  else
    ARFSocketPort.LEDRed;
  LogLine := LogLine + ARFSocketPort.LedLogLine + '|';
end; // HandleLED

// PIM-330.2
// Handle LED that comes from 'outside', not via card-reader-attached-to-machine
(*
procedure THomeF.HandleLEDOutsideScan(AIndex: Integer);
var
  ARFSocketPort: TRFSocketPort;
begin
  ARFSocketPort := RFSocketPortList[AIndex];
  // Prevent this is done too many times?
//  if ARFSocketPort.LastLEDColorText <> ARFSocketPort.LEDColorText then
  begin
    Sleep(250); // Wait
    ARFSocketPort.ReadBuffer := '';
    LogLine := LogLine + ARFSocketPort.LedLogLine + ';';
    if CheckOpenScan(ARFSocketPort.PlantCode, ARFSocketPort.WorkspotCode) then
      ARFSocketPort.LEDGreen
    else
      ARFSocketPort.LEDRed;
  end;
  ARFSocketPort.LastLEDColorText := ARFSocketPort.LEDColorText;
end; // HandleLEDOutsideScan
*)
procedure THomeF.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  try
    try
//      HomeF.CloseAtEndTime := True;
//      HomeF.FEndTime := Now - 1/24/60;
//      StopThreads;
//      tmrCheckConnection.Enabled := False;
//      if SingleTimer then
//        tmrReadAllSockets.Enabled := False
//      else
//        EndCreateSocketTimers;
    //  tmrProcessScans.Enabled := False;
//      tmrHandleLED.Enabled := False;
      try
        SessionCommit;
      except
        //
      end;
      try
//        CRThreadList.Free; // Do this during Destroy
(*        HLEDThreadList.Free; *)
//        SharedCs.Free; // Do this during Destroy
(*        SharedCsHLED.Free; *)
      except
        //
      end;
    except
    end;
  finally
//    HomeF.CloseAtEndTime := True;
//    HomeF.FEndTime := Now - 1/24/60;
//    StopThreads;
    Log('--- Stop ---');
//    Application.Terminate; // GLOB3-127.1
  end;
end;

procedure THomeF.WMQueryEndSession(var Message: TWMQueryEndSession);
begin
  // Respond to shutdown message form the system
  SessionCommit;
  Message.Result := Integer(True);
//  HomeF.CloseAtEndTime := True;
//  HomeF.FEndTime := Now - 1/24/60; // 1 minute earlier
  Close;
end;

procedure THomeF.SetEndTime(const Value: TDateTime);
begin
  FEndTime := Value;
  if (EndTimeHrs >= 0) and (EndTimeHrs <= 23) then
    if (EndTimeMins >= 0) and (EndTimeMins <= 59) then
      FEndTime := Trunc(Now) + Frac(EnCodeTime(EndTimeHrs, EndTimeMins, 0, 0));
  if Frac(FEndTime) <= Frac(Now) then
    FEndTime := Trunc(Now + 1) + Frac(FEndTime)
  else
    FEndTime := Trunc(Now) + Frac(FEndTime);
end;

// PIM-330.2 Do not use this timer
procedure THomeF.tmrHandleLEDTimer(Sender: TObject);
begin
  inherited;
  // This does not work?
//  if ReadSocketBusy then    Exit;
  try
    HandleLEDBusy := True;
    tmrHandleLED.Enabled := False;
    HandleAllLEDS;
  finally
    tmrHandleLED.Enabled := True;
    HandleLEDBusy := False;
  end;
end;

// Handle ALL LEDS for all sockets.
procedure THomeF.HandleAllLEDS;
var
  I: Integer;
begin
  LogLine := '';
  for I := 0 to WHostPortList.Count - 1 do
    HandleOneLED(I);
  LogLogLine(LogLine);
end; // HandleAllLEDS

// Handle LEDS based on workspots were scans were made.
// This can be about 2 leds, when an employee swaps workspots,
// in which case the first led must become red and the other green.
procedure THomeF.HandleLEDS;
var
  I: Integer;
  AWSLED: TWSLED;
begin
//  Sleep(250); // Wait?
  LogLine := '';
  if WSLEDList.Count = 0 then
    HandleOneLED(RFSocketIndex)
  else
  begin
    for I := WSLEDList.Count - 1 downto 0 do
    begin
      AWSLED := WSLEDList.Items[I];
      HandleOneLED(AWSLED.Index);
      WSLEDList.Remove(AWSLED);
    end;
    WSLEDList.Clear;
  end;
  LogLogLine(LogLine);
end; // HandleLEDS

(*
procedure THomeF.CreateReadSocketTimers;
var
  I: Integer;
  ATimer: TTimer;
begin
  for I := 0 to WHostPortList.Count - 1 do
  begin
    ATimer := TTimer.Create(Self);
    ATimer.Enabled := False;
    ATimer.Interval := ReadSocketsInterval;
    ATimer.Tag := I;
    ATimer.OnTimer := tmrReadSocketsTimer;
    ATimer.Enabled := True;
    ReadSocketTimerList.Add(ATimer);
  end;
end; // CreateReadSocketTimers
*)
(*
procedure THomeF.EndCreateSocketTimers;
var
  I: Integer;
  ATimer: TTimer;
begin
  for I := ReadSocketTimerList.Count - 1 downto 0 do
  begin
    ATimer := ReadSocketTimerList.Items[I];
    ATimer.Enabled := False;
    ReadSocketTimerList.Remove(ATimer);
  end;
  ReadSocketTimerList.Clear;
  ReadSocketTimerList.Free;
end;
*)
procedure THomeF.tmrReadAllSocketsTimer(Sender: TObject);
begin
  inherited;
  // This does not work?
  if HandleLEDBusy then    Exit;
  try
    ReadSocketBusy := True;
    tmrReadAllSockets.Enabled := False;
    RFSocketPortHandler;
  finally
    if RFSocketIndex < RFSocketPortList.Count - 1 then
      inc(RFSocketIndex)
    else
      RFSocketIndex := 0;
    tmrReadAllSockets.Enabled := True;
    ReadSocketBusy := True;
  end;
  if CloseAtEndTime then
  begin
    if Now >= EndTime then
      CloseNow;
  end;
end; // tmrReadAllSocketsTimer

// Split line over 'maxlen' to prevent it does not fit in abslog.
procedure THomeF.LogLogLine(ALogLine: String);
const
  Maxlen=255;
var
  Line: String;
begin
  repeat
    Line := Copy(ALogLine, 1, Maxlen);
    Log(Line);
    if Length(ALogLine) > Maxlen then
      ALogLine := Copy(ALogLine, Maxlen + 1, Length(LogLine));
  until Length(Line) <= Maxlen;
end;

// PIM-330.2.
procedure THomeF.tmrCheckConnectionTimer(Sender: TObject);
begin
  inherited;
  // Check connection and reconnect if not connected.
  ORASystemDM.OracleSession.CheckConnection(True);
  if not ORASystemDM.OracleSession.Connected then
    ErrorLog('Error: Oracle Connection Problem!');
end;

(*
procedure THomeF.DoOnDone(Sender: Tobject);
begin
// Do something when done
  if (Sender is TCRThread) then
    Log('TCRTread done');
end;
*)
{ TCRThread }

(*
procedure TCRThread.CRThreadRFSocketPortHandler;
var
  Done: Boolean;
begin
  try
    Self.Suspend;
    SharedCs.Acquire;
{
    ORASystemDM.OracleSession := Self.FOracleSession;
    if FOracleSession.InTransaction then
    begin
      FOracleSession.Commit;
Log('FOracleSession.Commit');
    end;
}
    HomeF.RFSocketIndex := Self.CRThreadRFSocketIndex;
    Done := HomeF.RFSocketPortHandler;
    HomeF.SessionCommit;
    Sleep(250);
    Application.ProcessMessages;
{
    if not Done then
    begin
      if Self.FCounter >= HandleLEDAtCounter then
      begin
        try
          Application.ProcessMessages;
          HomeF.HandleOneLED(Self.CRThreadRFSocketIndex);
          Sleep(250); // Wait?
        finally
          Self.FCounter := 0;
        end;
      end;
    end;
}
  finally
    HomeF.SessionCommit;
    SharedCs.Release;
    Self.Resume;
  end;
end; // CRThreadRFSocketPortHandler
*)

procedure TCRThread.Execute;
begin
  inherited;
  while (not Self.Terminated) do
  begin
    try
//      Synchronize(CRThreadRFSocketPortHandler);
      Sleep(1000); // Do this each n seconds.
    finally
    end;
(*
    if HomeF.CloseAtEndTime then
    begin
      if Now >= HomeF.EndTime then
      begin
        Self.Suspend;
        Self.DoTerminate;
        HomeF.CloseNow;
      end;

    end;
*)    
  end;
end;

procedure TCRThread.Init(AIndex: Integer);
begin
//  Self.Priority := tpLower; // What priority is needed here?
  Self.CRThreadRFSocketIndex := AIndex;
  Self.FID := AIndex + 1;
(*
  Self.FOracleSession := TOracleSession.Create(Application);
  Self.FOracleSession.LogonDatabase := ORASystemDM.OracleSession.LogonDatabase;
  Self.FOracleSession.LogonPassword := ORASystemDM.OracleSession.LogonPassword;
  Self.FOracleSession.LogonUsername := ORASystemDM.OracleSession.LogonUsername;
  Self.FOracleSession.ThreadSafe := True;
  Self.FOracleSession.Connected := True;
*)
end;

(*
procedure TCRThread.NotifyIsDone;
begin
  If Assigned( FOnDone ) then
    FOnDone( Self );
end;
*)
{ THLEDThread }

procedure THLEDThread.HDLEDHandler;
var
  ARFSocketPort: TRFSocketPort;
begin
  try
    Self.Suspend;
    SharedCs.Acquire;
    ARFSocketPort := RFSocketPortList[Self.CRThreadRFSocketIndex];
    // GLOB3-127.2 Prevent the readbuffer gets too long.
    // GLOB3-127.3 Prevent it keeps an error-message in the buffer.
    if (Length(ARFSocketPort.ReadBuffer) >= ReadBufferMaxLen) or
      (Pos('error', LowerCase(ARFSocketPort.ReadBuffer)) > 0) then
      ARFSocketPort.ReadBuffer := '';
    if HLedCounter >= 6 then
      ARFSocketPort.ReadBuffer := '';
    if ARFSocketPort.ReadBuffer = '' then
      HomeF.HandleOneLED(Self.CRThreadRFSocketIndex);
  finally
    SharedCs.Release;
    Self.Resume;
  end;
end;

procedure THLEDThread.Execute;
begin
  inherited;
  while (not Self.Terminated) do
  begin
    try
      Self.Counter := Self.Counter + 1;
      if Self.Counter > 6 then
        Self.Counter := 0;
      HLedCounter := Self.Counter;
      Synchronize(HDLEDHandler);
      Sleep(10000); // Do this each n seconds.
    finally
    end;
(*
    if HomeF.CloseAtEndTime then
    begin
      if Now >= HomeF.EndTime then
      begin
        Self.Suspend;
        Self.DoTerminate;
        HomeF.CloseNow;
      end;
    end;
*)
  end;
end;

procedure THLEDThread.Init(AIndex: Integer);
begin
  Self.CRThreadRFSocketIndex := AIndex;
  Self.FID := AIndex + 1;
  Self.Counter := 0;
//  FOracleSession := TOracleSession.Create(Application);
//  FOracleSession.LogonDatabase := ORASystemDM.OracleSession.LogonDatabase;
//  FOracleSession.LogonPassword := ORASystemDM.OracleSession.LogonPassword;
//  FOracleSession.LogonUsername := ORASystemDM.OracleSession.LogonUsername;
//  FOracleSession.Connected := True;
end;
(*
procedure THLEDThread.NotifyIsDone;
begin
  If Assigned( FOnDone ) then
    FOnDone( Self );
end;
*)
procedure THomeF.StartThreads;
var
  ACRThread: TCRThread;
  AHLEDThread: THLEDThread;
  I: Integer;
begin
  for I := 0 to Self.CRThreadList.Count - 1 do
  begin
    ACRThread := Self.CRThreadList.Items[I];
    ACRThread.Resume;
  end;
  for I := 0 to Self.HLEDThreadList.Count - 1 do
  begin
    AHLEDThread := Self.HLEDThreadList.Items[I];
    AHLEDThread.Resume;
  end;
end; // StartThreads;

procedure THomeF.StopThreads;
var
  ACRThread: TCRThread;
  AHLEDThread: THLEDThread;
  ARFSocketPort: TRFSocketPort;
  I: Integer;
begin
  for I := 0 to Self.CRThreadList.Count - 1 do
  begin
    try
      ACRThread := Self.CRThreadList.Items[I];
      ACRThread.Suspend;
      ACRThread.DoTerminate;
    except
    end;
  end;
  for I := 0 to Self.HLEDThreadList.Count - 1 do
  begin
    try
      AHLEDThread := Self.HLEDThreadList.Items[I];
      AHLEDThread.Suspend;
      AHLEDThread.DoTerminate;
      ARFSocketPort := RFSocketPortList[AHLEDThread.CRThreadRFSocketIndex];
      ARFSocketPort.IdTelnet := nil;
    except
    end;
  end;
end; // StopThreads

procedure THomeF.FormDestroy(Sender: TObject);
begin
  inherited;
  try
    try
      StopThreads;
      CRThreadList.Free;
      HLEDThreadList.Free;
      SharedCs.Free;
    except
    end;
  finally
  end;
end;

procedure THomeF.SessionCommit;
begin
  if ORASystemDM.OracleSession.InTransaction then
    ORASystemDM.OracleSession.Commit;
end;

// GLOB3-127.1
procedure THomeF.CloseNow;
begin
  try
    try
      SessionCommit;
    except
      //
    end;
  finally
    Log('--- Stop ---');
    Close;
//    Application.Terminate;
  end;
end; // CloseNow

end.
