object TimeRecordingCRDM: TTimeRecordingCRDM
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Left = 550
  Top = 224
  Height = 375
  Width = 435
  object oqCopyThis: TOracleQuery
    Session = ORASystemDM.OracleSession
    Left = 56
    Top = 24
  end
  object oqWorkspotHostPort: TOracleQuery
    SQL.Strings = (
      'SELECT'
      '  W.PLANT_CODE, W.WORKSPOT_CODE, W.HOST, W.PORT, W.BATCH'
      'FROM'
      '  WORKSPOT W'
      'WHERE'
      '  W.PLANT_CODE = :PLANT_CODE AND'
      '  W.HOST IS NOT NULL AND W.PORT IS NOT NULL AND'
      
        '  ((W.DATE_INACTIVE IS NULL) OR W.DATE_INACTIVE < TRUNC(SYSDATE)' +
        ') AND'
      '  ((NVL(W.BATCH,0) = :BATCH) OR (:BATCH = 0))'
      'ORDER BY'
      '  W.PLANT_CODE, W.WORKSPOT_CODE')
    Session = ORASystemDM.OracleSession
    Variables.Data = {
      03000000020000000B0000003A504C414E545F434F4445050000000000000000
      000000060000003A4241544348030000000000000000000000}
    Left = 56
    Top = 96
  end
end
