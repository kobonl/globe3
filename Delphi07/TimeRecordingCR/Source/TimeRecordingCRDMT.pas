(*
  MRA:15-JUL-2016 PIM-203
  - Time Recording via Card Readers
  MRA:15-DEC-2017 PIM-330.2
  - Be sure messages are ony logged, not shown.
*)
unit TimeRecordingCRDMT;

interface

uses
  SysUtils, Classes, ORASystemDMT, Oracle;

type
  TTimeRecordingCRDM = class(TDataModule)
    oqCopyThis: TOracleQuery;
    oqWorkspotHostPort: TOracleQuery;
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
    FBatch: Integer;
  public
    { Public declarations }
    property Batch: Integer read FBatch write FBatch;
  end;

var
  TimeRecordingCRDM: TTimeRecordingCRDM;

implementation

{$R *.dfm}

procedure TTimeRecordingCRDM.DataModuleCreate(Sender: TObject);
begin
  Batch := 0;
  ORASystemDM.OnlyLogMessages := True;
end;

end.
