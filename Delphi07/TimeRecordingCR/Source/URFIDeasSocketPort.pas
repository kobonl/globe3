(*
  MRA:15-JUL-2016 PIM-203
  - Time Recording via Card Readers
  MRA:4-JAN-2016 PIM-254
  - Read ID with 8 characters for Rosslare-cards.
  - This is done by reading it first in hex, divide it in 2 and
    convert the 2 parts to decimal and paste them together.
  - Be sure the reader itself gives the expected result in hex!
  MRA:9-JAN-2017 PIM-254
  - Use FillZero to ensure a number starting with 0 will give no
    problems.
  MRA:20-JAN-2017 PIM-260
  - Process a scan when the ID was read via the socket, instead of doing this
    via a separate timer. Reason: Because we now have multiple timers, a scan
    can processed directly wihout holding up the other card-readers.
  MRA:23-JAN-2017 PIM-264
  - Decrease logging.
  - Socket-errors should only be logged when debug=1, because that can give
    lots of logging (when machines/card-readers are powered off).
  MRA:15-DEC-2017 PIM-330.2.
  - Use Indy 10.1.5-components.
  - This has a time-out-argument for Connect.
  MRA:11-JAN-2018 PIM-346
  - Blocked Sessions Problem
    - There was still 1 idTelNet.Connect without a time-out! This is fixed.
  - Note: The time-out seems to work, so this gives a better performance.
*)
unit URFIDeasSocketPort;

interface

uses
  IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient, IdTelnet,
  IdException, SysUtils, Classes;

const
  CR = #13;
  LF = #10;

type
  TMyLog = procedure(AMsg: String);
  TMyErrorLog = procedure(AMsg: String);

type
  TProcessScanEvent = procedure(APlantCode, AWorkspotCode, AID: String; ATimestamp: TDateTime);
  THandleLEDS = procedure of object;

type
  TRFSocketPort = class
  private
    FIdTelnet: TIdTelnet;
    FHost: String;
    FPort: Integer;
    FReadBuffer: String;
    FReadDelay: Integer;
    FMyLog: TMyLog;
    FIDRead: Boolean;
    FColorRead: Boolean;
    FIDLength: Integer;
    FWorkspotCode: String;
    FPlantCode: String;
    FOnProcessScan: TProcessScanEvent;
    FOnHandleLEDS: THandleLEDS;
    FID: String;
    FLedLogLine: String;
    FMyErrorLog: TMyErrorLog;
    FConnectTimeOut: Integer;
    FLEDColorText: String;
    FLastLEDColorText: String;
    FMyID: Integer;
    FCounter: Integer;
    procedure SetHost(const Value: String);
    procedure SetPort(const Value: Integer);
    function GetOpen: Boolean;
    procedure SetOpen(const Value: Boolean);
    procedure IdTelnetDataAvailable(Sender: TIdTelnet;
      const Buffer: String);
    function SendCommand(ACommand: String): Boolean;
    procedure LEDSetColor(AColor: Integer);
    function LEDGetColor: Integer;
    function GetIsRed: Boolean;
    function IDConvert(AID: String): String;
    procedure DoProcessScan(APlantCode, AWorkspotCode, AID: String; ATimestamp: TDateTime);
    procedure DoHandleLEDS;
  public
    constructor Create; overload;
    constructor Create(AIdTelnet: TIdTelnet); overload;
    destructor Destroy; override;
    procedure Wait(const MSecsToWait : Integer);
    procedure WMyErrorLog(AMsg: String);
    procedure WMyLog(AMsg: String);
    procedure AddLedLog(AMsg: String);
    function PortConnect(AHost: String; APort: Integer): Boolean;
    procedure PortDisconnect;
    function ToString: String;
    procedure LEDRed;
    procedure LEDGreen;
    procedure LEDOrange;
    procedure LEDOff;
    property IdTelnet: TIdTelnet read FIdTelnet write FIdTelnet;
    property MyID: Integer read FMyID write FMyID;
    property Host: String read FHost write SetHost;
    property Port: Integer read FPort write SetPort;
    property PlantCode: String read FPlantCode write FPlantCode;
    property WorkspotCode: String read FWorkspotCode write FWorkspotCode;
    property Open: Boolean read GetOpen write SetOpen;
    property ReadBuffer: String read FReadBuffer write FReadBuffer;
    property ReadDelay: Integer read FReadDelay write FReadDelay;
    property MyLog: TMyLog read FMyLog write FMyLog;
    property IDRead: Boolean read FIDRead write FIDRead;
    property ColorRead: Boolean read FColorRead write FColorRead;
    property IsRed: Boolean read GetIsRed;
    property IDLength: Integer read FIDLength write FIDLength;
    property OnProcessScan: TProcessScanEvent read FOnProcessScan write FOnProcessScan;
    property OnHandleLEDS: THandleLEDS read FOnHandleLEDS write FOnHandleLEDS;
    property ID: String read FID write FID;
    property LedLogLine: String read FLedLogLine write FLedLogLine;
    property MyErrorLog: TMyErrorLog read FMyErrorLog write FMyErrorLog;
    property ConnectTimeOut: Integer read FConnectTimeOut write FConnectTimeOut Default 500;
    property LEDColorText: String read FLEDColorText write FLEDColorText;
    property LastLEDColorText: String read FLastLEDColorText write FLastLEDColorText;
    property Counter: Integer read FCounter write FCounter;
  end;

procedure AssignSocketPortList(AHost: String; APort: Integer;
  AIDLength: Integer; APlantCode, AWorkspotCode: String);

var
  RFSocketPortList: TList;
  Debug: Boolean;
  ReaderRosslare: Boolean;

implementation

{ TRFSocketPort }

constructor TRFSocketPort.Create;
begin
  inherited;
  MyLog := nil;
  MyErrorLog := nil;
  ReadDelay := 10;
  ConnectTimeOut := 200;
  Counter := 0;
end;

constructor TRFSocketPort.Create(AIdTelnet: TIdTelnet);
begin
  Create;
  IdTelnet := AIdTelnet;
  IdTelnet.Host := '';
  IdTelnet.Port := 0;
  ReadBuffer := '';
  IdTelnet.OnDataAvailable := IdTelnetDataAvailable;
end;

destructor TRFSocketPort.Destroy;
begin
//
  inherited;
end;

function TRFSocketPort.GetOpen: Boolean;
begin
  Result := IdTelnet.Connected;
end;

procedure TRFSocketPort.SetOpen(const Value: Boolean);
begin
//
end;

procedure TRFSocketPort.SetHost(const Value: String);
begin
  FHost := Value;
end;

procedure TRFSocketPort.SetPort(const Value: Integer);
begin
  FPort := Value;
end;

procedure TRFSocketPort.Wait(const MSecsToWait: Integer);
var
  TimeToWait,
  TimeToSendRequest : TDateTime;
begin
  TimeToWait   := MSecsToWait / MSecsPerDay;
  TimeToSendRequest := TimeToWait + SysUtils.Now;
  while SysUtils.Now < TimeToSendRequest do
    Sleep(0);
end;

procedure TRFSocketPort.IdTelnetDataAvailable(Sender: TIdTelnet;
  const Buffer: String);
var
  Start, Stop: Integer;
  CRDetected: Boolean;
begin
  Start := 1;
  CRDetected := False;
  Stop  := Pos(CR, Buffer);
  Self.WMyErrorLog('Buffer:' + Buffer);
  if Stop = 0 then
    Stop := Length(Buffer) + 1;
  while Start <= Length(Buffer) do
  begin
    if Buffer[Stop] = CR then
    begin
      CRDetected := True;
    end;
    Start := Stop + 1;
    if Start > Length(Buffer) then
      Break;
    if Buffer[Start] = LF then
      Start := Start + 1;
    Stop := Start;
    while (Buffer[Stop] <> CR) and (Stop <= Length(Buffer)) do
      Stop := Stop + 1;
  end; // while
  // Add the buffer to readbuffer, because it can come in parts.
  ReadBuffer := ReadBuffer + Buffer;
  if ReadBuffer <> '' then
  begin
    if CRDetected then
    begin
      try
        // Get Color here
        Start := Pos('{', ReadBuffer);
        Stop := Pos('}', ReadBuffer);
        if (Start > 0) and (Stop > 0) then
        begin
          ReadBuffer := Copy(ReadBuffer, Start, Stop);
          ColorRead := True;
        end;
        // Get ID here
        // Remove CR at end of buffer
        ReadBuffer := Copy(ReadBuffer, 1, Pos(CR, ReadBuffer) - 1);
        Self.WMyErrorLog('ReadBuffer:' + ReadBuffer);

        // Check for expected len here for ID
        if (Length(ReadBuffer) = IDLength) then
        begin
          ReadBuffer := IDConvert(ReadBuffer);
          ID := ReadBuffer;
          Self.Counter := Self.Counter + 1;
          Self.WMyLog(IntToStr(Self.MyID) +
            ': ID=' + ID + ' (' +IntToStr(Self.Counter) + ')');
          IDRead := True;
          DoProcessScan(Self.PlantCode, Self.WorkspotCode, ID, Now);
          DoHandleLEDS;
        end;
        // Reset buffer here
        if (Pos(ReadBuffer, 'rfid:out.led=0') > 0) or
           (Pos(ReadBuffer, 'rfid:out.led=1') > 0) or
           (Pos(ReadBuffer, 'rfid:out.led=2') > 0) or
           (Pos(ReadBuffer, 'rfid:out.led=3') > 0) then
           ReadBuffer := '';
      except
        on E: EIdSocketError do
        begin
          WMyErrorLog(E.Message);
        end;
        on E: Exception do
        begin
          WMyErrorLog(E.Message);
        end;
      end;
    end;
  end; // if
end; // IdTelnetDataAvailable

function TRFSocketPort.SendCommand(ACommand: String): Boolean;
var
  i : integer;
  s : string;
begin
  Result := False;
  if Debug then
  begin
    if Pos('rfid:out.led=1', ACommand) > 0 then
      WMyErrorLog('SendCommand:' + ACommand + ':red')
    else
      if Pos('rfid:out.led=2', ACommand) > 0 then
        WMyErrorLog('SendCommand:' + ACommand + ':grn')
      else
        WMyErrorLog('SendCommand:' + ACommand);
  end
  else
  begin
    if Pos('rfid:out.led=1', ACommand) > 0 then
      AddLedLog('red')
    else
      if Pos('rfid:out.led=2', ACommand) > 0 then
        AddLedLog('grn');
  end;
  // It is possible because of a time-out it is not connected anymore.
  try
    if not IdTelnet.Connected then
      IdTelnet.Connect(ConnectTimeOut); // PIM-330.2 Time-out
    if IdTelnet.Connected then
    begin
      s := ACommand;
      for i := 1 to length(s) do
        IdTelnet.SendCh(s[i]);
      IdTelnet.SendCh(#13); // ? Must be #10 ?
      Result := True;
    end;
    ReadBuffer := '';
  except
    // For now: Ignore error, only log it.
    // Note: When it could not connect, then perhaps the next time
    // it tries to read the counter, it can connect.
    // Otherwise???
    on E: EIdSocketError do
    begin
      WMyErrorLog(E.Message);
    end;
    on E: EIdConnectTimeOut do
    begin
      WMyErrorLog(E.Message);
    end;
    on E: Exception do
    begin
      WMyErrorLog(E.Message);
    end;
  end;
end; // SendCommand

procedure TRFSocketPort.WMyErrorLog(AMsg: String);
begin
  if Debug then
    if Assigned(MyErrorLog) then
      MyErrorLog('(' + Self.FIdTelnet.Host + ';' + IntToStr(Self.FIdTelnet.Port) + ') ' + AMsg);
end; // WMyErrorLog

function TRFSocketPort.PortConnect(AHost: String; APort: Integer): Boolean;
  procedure OpenPort;
  begin
    try
      IdTelnet.Host := AHost;
      IdTelnet.Port := APort;
      if not IdTelnet.Connected then
        IdTelnet.Connect(ConnectTimeOut); // PIM-330.2 Time-out
      Result := True;
    except
      on E: EIdSocketError do
      begin
        WMyErrorLog(E.Message);
      end;
      on E: EIdConnectTimeOut do
      begin
        WMyErrorLog(E.Message);
      end;
      on E: EIdAlreadyConnected do
      begin
//        WMyErrorLog(E.Message); // Ignore this error
      end;
      on E: Exception do
      begin
        WMyErrorLog(E.Message);
      end;
    end;
  end;
  procedure ClosePort;
  begin
    try
      PortDisconnect;
    except
      on E:EAccessViolation do
      begin
        WMyErrorLog(E.Message);
      end;
      on E: EIdSocketError do
      begin
        WMyErrorLog(E.Message);
      end;
      on E: Exception do
      begin
        WMyErrorLog(E.Message);
      end;
    end;
  end;
begin
  Result := False;
  try
    try
      OpenPort;
    except
      on E: EIdSocketError do
      begin
        WMyErrorLog(E.Message);
      end;
      on E: Exception do
      begin
        WMyErrorLog(E.Message);
      end;
    end;
  finally
    if not IdTelnet.Connected then
      WMyErrorLog('Not connected!');
    Result := IdTelnet.Connected;
  end;
end; // PortConnect

procedure TRFSocketPort.PortDisconnect;
begin
  try
    try
      if IdTelnet.Host <> '' then
        if IdTelnet.Connected then
        begin
          IdTelnet.InputBuffer.Clear;
          IdTelnet.Disconnect;
        end;
    except
      on E:EAccessViolation do
      begin
        WMyErrorLog(E.Message);
      end;
      on E: EIdClosedSocket do
      begin
        WMyErrorLog(E.Message);
      end;
      on E: EIdSocketError do
      begin
        WMyErrorLog(E.Message);
      end;
      on E: Exception do
      begin
        WMyErrorLog(E.Message);
      end;
    end;
  finally
    Wait(ReadDelay);
  end;
end; // PortDisconnect

function TRFSocketPort.ToString: String;
begin
  Result := Host + ';' + IntToStr(Port);
end; // ToString

procedure TRFSocketPort.LEDSetColor(AColor: Integer);
begin
  try
    SendCommand('rfid:out.led=' + IntToStr(AColor));
  except
    // Invalid color?
  end;
end; // LEDSetColor

function TRFSocketPort.LEDGetColor: Integer;
begin
  Result := 0;
  try
    ReadBuffer := '';
    ColorRead := False;
    SendCommand('rfid:out.led?');
  except
    // Error?
  end;
end; // LEDGetColor

procedure TRFSocketPort.LEDRed;
begin
  LEDSetColor(1);
  LEDColorText := 'red';
end;

procedure TRFSocketPort.LEDGreen;
begin
  LEDSetColor(2);
  LEDColorText := 'grn';
end;

procedure TRFSocketPort.LEDOrange;
begin
  LEDSetColor(3);
  LEDColorText := 'ora';
end;

procedure TRFSocketPort.LEDOff;
begin
  LEDSetColor(0);
  LEDColorText := 'off';
end;

function TRFSocketPort.GetIsRed: Boolean;
begin
  Result := LEDGetColor = 1;
end;

procedure AssignSocketPortList(AHost: String; APort: Integer;
  AIDLength: Integer; APlantCode, AWorkspotCode: String);
var
  ARFSocketPort: TRFSocketPort;
  AIdTelNet: TIdTelNet;
begin
  AIdTelnet := TIdTelNet.Create(nil);
  ARFSocketPort := TRFSocketPort.Create(AIdTelnet);
  if Assigned(ARFSocketPort) then
  begin
    RFSocketPortList.Add(ARFSocketPort);
    ARFSocketPort.Host := AHost;
    // Also assign them here!
    AIdTelnet.Host := AHost;
    AIdTelnet.Port := APort;
    ARFSocketPort.Port := APort;
    ARFSocketPort.PlantCode := APlantCode;
    ARFSocketPort.WorkspotCode := AWorkspotCode;
    ARFSocketPort.IDLength := AIDLength;
  end;
end; // AssignSocketPortList

// PIM-254
function TRFSocketPort.IDConvert(AID: String): String;
var
  Part1, Part2: String;
  function FillZero(const AValue: String; const ALen: Integer): String;
  begin
    Result := AValue;
    while Length(Result) < ALen do
      Result := '0' + Result;
  end;
begin
  Result := AID;
  if ReaderRosslare then
  begin
    // Split first in 2 parts
    try
      Part1 := Copy(AID, 1, 4); // 3 positions
      Part2 := Copy(AID, 5, 4); // 5 positions
      Result :=
        FillZero(IntToStr(StrToInt('$' + Part1)), 3) +
        FillZero(IntToStr(StrToInt('$' + Part2)), 5);
    except
      on E:Exception do
        WMyErrorLog(E.Message);
    end;
  end;
end; // IDConvert

procedure TRFSocketPort.DoProcessScan(APlantCode, AWorkspotCode, AID: String;
  ATimestamp: TDateTime);
begin
  if Assigned(OnProcessScan) then
    OnProcessScan(APlantCode, AWorkspotCode, AID, ATimestamp);
end;

procedure TRFSocketPort.DoHandleLEDS;
begin
  if Assigned(OnHandleLEDS) then
    OnHandleLEDS;
end;

procedure TRFSocketPort.AddLedLog(AMsg: String);
begin
  LedLogLine := Self.WorkspotCode + ':' + AMsg;
end;

procedure TRFSocketPort.WMyLog(AMsg: String);
begin
  if Assigned(MyLog) then
    MyLog(Self.WorkspotCode + ':' + AMsg);
end;

initialization
  RFSocketPortList := TList.Create;

finalization
  RFSocketPortList.Free;

end.
