--
-- PIM-23 Report Employee Info and Deviations (per period)
--

-- Create table
create table PIVOTDAY
(
  DAYDATE DATE not null
)
tablespace ABS_DATA
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate primary, unique and foreign key constraints 
alter table PIVOTDAY
  add primary key (DAYDATE)
  using index 
  tablespace INDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

-- Add menu item
INSERT INTO PIMSMENUGROUP 
SELECT t.group_name, 126104, 'Y', 'Y', 'N', 126100, 'Report Employee Info and Deviations', 4, SYSDATE, SYSDATE, 'ABS' 
FROM pimsusergroup t;

commit;


