--
-- PIM-315 Add BATCH to define a number for multiple TimeRecordingCR-programs.
--

declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'alter table WORKSPOT add BATCH number';
    exception when column_exists then null;
end;
/

commit;
