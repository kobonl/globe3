--
-- PIM-326 Procedure that checks for frst/last scan gives problems
--

CREATE OR REPLACE FUNCTION IsFirstScan(AEmployeeNumber Integer, ADateTimeIn Date, ADeleteDateTimeIn Date) return varchar2 is
  IsEmpty Boolean;
  DateIn Date;
  DateOut Date;
begin
  IsEmpty := False;
  begin
    SELECT
      DATETIME_IN, DATETIME_OUT
    INTO
      DateIn, DateOut
    FROM
      TIMEREGSCANNING
    WHERE
      EMPLOYEE_NUMBER = AEmployeeNumber AND
      (DATETIME_IN > ADateTimeIn - 0.5 AND DATETIME_IN < ADateTimeIn) AND
      ((ADeleteDateTimeIn IS NULL) OR (ADeleteDateTimeIn IS NOT NULL AND DATETIME_IN <> ADeleteDateTimeIn)) AND
      ROWNUM=1;
  exception
    when no_data_found then IsEmpty := True;
  end;
  if IsEmpty then
    return 'TRUE';
  else
    return 'FALSE';
  end if;
end IsFirstScan;
/

CREATE OR REPLACE FUNCTION IsLastScan(AEmployeeNumber Integer, ADateTimeOut Date, ADeleteDateTimeIn Date) return varchar2 is
  IsEmpty Boolean;
  DateIn Date;
  DateOut Date;
begin
  IsEmpty := False;
  begin
    SELECT
      DATETIME_IN, DATETIME_OUT
    INTO
      DateIn, DateOut
    FROM
      TIMEREGSCANNING
    WHERE
      EMPLOYEE_NUMBER = AEmployeeNumber AND
      DATETIME_OUT IS NOT NULL AND
      (DATETIME_OUT > ADateTimeOut AND DATETIME_OUT < ADateTimeOut + 0.5) AND
      ((ADeleteDateTimeIn IS NULL) OR (ADeleteDateTimeIn IS NOT NULL AND DATETIME_IN <> ADeleteDateTimeIn)) AND
      ROWNUM=1;
  exception
    when no_data_found then IsEmpty := True;
  end;
  if IsEmpty then
    return 'TRUE';
  else
    return 'FALSE';
  end if;
end IsLastScan;
/

commit;



