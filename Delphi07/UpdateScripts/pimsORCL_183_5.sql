--
-- PIM-319 Do not subtract breaks from Production Hours
--

declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'alter table PIMSSETTING add PRODHRS_NOSUBTRACT_BREAKS VARCHAR2(1) default ''N''';
    exception when column_exists then null;
end;
/

CREATE OR REPLACE PACKAGE "PACKAGE_HOUR_CALC" AS

  TYPE TORASystemDM IS RECORD (
    IsCVL Boolean,
    IsLTB Boolean,
    IsRFL Boolean,
    CurrentProgramUser Varchar2(21),
    CurrentComputerName Varchar2(128),
    ADebug Integer
  );
  TYPE TPimsSetting IS RECORD (
    UseShiftDateSystem Boolean,
    FillProdHourPerHourType Boolean,
    WeekStartsOn Integer,
    TimerecordingInSecs Boolean,
    ProdHrsDoNotSubtractBreaks Boolean
  );
  TYPE TGlobalDM IS RECORD (
    ContractGroupTFTExists Boolean,
    PayedBreakSave Integer,
    StandardMinutesPerMonth Integer
  );
  TYPE TShiftDayRecord IS RECORD (
    ADate Timestamp,
    AStart Timestamp,
    AEnd Timestamp,
    AValid Boolean,
    APlantCode Varchar2(6),
    AShiftNumber Integer
  );
  TYPE TScannedIDCard IS RECORD (
    EmployeeCode Integer,
    ShiftNumber Integer,
    InscanEarly Integer,
    InscanLate Integer,
    OutScanEarly Integer,
    OutScanLate Integer,
    EmplName Varchar2(40),
    IDCard Varchar2(15),
    WorkSpotCode Varchar2(6),
    WorkSpot varchar2(30),
    JobCode Varchar2(6),
    Job Varchar2(30),
    PlantCode Varchar2(6),
    Plant Varchar2(30),
    DepartmentCode Varchar2(30),
    CutOfTime Varchar2(1),
    DateIn Date,
    DateOut Date,
    DateInCutOff Date,
    DateOutCutOff Date,
    FromTimeRecordingApp Boolean,
    IgnoreForOvertimeYN Varchar2(1),
    ContractgroupCode Varchar2(6),
    ExceptionalBeforeOvertime Varchar2(1),
    RoundTruncSalaryHours Integer,
    RoundMinute Integer,
    WSHourtypeNumber Integer,
    ShiftHourtypeNumber Integer,
    ShiftDate Date,
    Processed Boolean,
    DateInOriginal Date,
    DateOutOriginal Date,
    ShiftStartDateTime Date,
    ShiftEndDateTime Date
  );
  TYPE TScan IS RECORD (
    EmployeeNumber Number,
    ShiftNumber Integer,
    IDCardIn varchar2(15),
    WorkspotCode Varchar2(6),
    JobCode Varchar2(6),
    PlantCode Varchar2(6),
    DateTimeIn Date,
    DateTimeOut Date,
    ShiftDate Date,
    ShiftHourtypeNumber Integer,
    ShiftIgnoreForOvertimeYN Varchar2(1),
    IgnoreForOvertimeYN Varchar2(1),
    WorkspotHourtypeNumber Integer,
    WorkspotDepartmentCode Varchar2(6),
    DepartmentCode Varchar2(6)
  );
  TYPE TOvertimeParams IS RECORD (
    TimeForTimeYN Varchar2(1),
    BonusInMoneyYN Varchar2(1),
    BonusPercentage Number,
    HT_OvertimeYN Varchar2(1),
    HT_TimeForTimeYN Varchar2(1),
    HT_BonusInMoneyYN Varchar2(1),
    HT_ADVYN Varchar2(1)
  );
--  Type TWeekDates is Varray(7) of Date;
--  Type TWeekDaysTotal is Varray(7) of Integer;
--  Type TDailyTime is Varray(8) of Varchar2(60);
  PimsSetting TPimsSetting;
  ORASystemDM TORASystemDM;
  GlobalDM TGlobalDM;
  OvertimeParams TOvertimeParams;
  DayToMin CONSTANT Integer := 1440;

  PROCEDURE WLog(APriority Integer, ALogMessage Varchar2);
  PROCEDURE WDebugLog(ALogMessage Varchar2);
  PROCEDURE WErrorLog(ALogMessage Varchar2);
  FUNCTION Bool2Str(ABoolean Boolean) return Varchar2;
  FUNCTION DateToStr(ADate Date) return Varchar2;
  FUNCTION DateTimeToStr(ADate Date) return Varchar2;
  FUNCTION IntToStr(AValue Integer) return Varchar2;
  FUNCTION IntMin2StringTime(AValue Integer, ADummy Boolean) return Varchar2;
  FUNCTION ContractGroupTFTExists return Boolean;
  FUNCTION TruncFrac(ADate Date, AFrac Date) return date;
  FUNCTION DateInInterval(ADate Date, ATestDate Date, AMinTo Integer, AMinAfter Integer) return Date;
  FUNCTION ComputeMinutesOfIntersection(AStart1 Date, AEnd1 Date, AStart2 Date, AEnd2 Date) return Integer;
  FUNCTION RoundTime(ADateTime Date, ATarget Integer) return date;
  FUNCTION RoundTimeConditional(ADateTime Date, ATarget Integer) return date;
  PROCEDURE GetPimsSettings;
  PROCEDURE EmptyIDCard(AIDCard in out TScannedIDCard);
  FUNCTION FindWSOverruledContractGroup(APlantCode varchar2, AWorkspotCode varchar2, ADefaultContractGroupCode varchar2) return varchar2;
  PROCEDURE PopulateIDCard(AIDCard in out TScannedIDCard, ADataSet in TScan, ARoundMinutes in Boolean);
  FUNCTION EncodeDate(fp_year in number, fp_month in number, fp_day in number) return date;
  FUNCTION EncodeDateTime(fp_year in number, fp_month in number, fp_day in number, fp_hour in number, fp_min in number, fp_sec in number) return date;
  PROCEDURE DecodeDate(fp_date in date, fp_year in out number, fp_month in out number, fp_day in out number);
  PROCEDURE DecodeDateTime(fp_date in date, fp_year in out number, fp_month in out number, fp_day in out number, fp_hour in out number, fp_min in out number, fp_sec in out number);
  PROCEDURE DecodeTime(fp_date in date, fp_hour in out number, fp_min in out number, fp_sec in out number);
  FUNCTION StartOfYear(fp_year in number) return date;
  FUNCTION DayOfWeek(fp_date in date) return number;
  FUNCTION LastDayOfWeek(fp_date in date) return date;
  FUNCTION WeekOfYear(fp_date in date) return number;
  FUNCTION WeeksInYear(fp_year in number) return number;
  FUNCTION LastMonthDay(fp_year in number, fp_month in number) return number;
  PROCEDURE WeekFromDate(fp_date in date, fp_year in out number, fp_week in out number);
  FUNCTION MidnightCorrection(STime Date, ETime Date) return Date;
  PROCEDURE BooksTFT(ABookingDay Date, AIDCard TScannedIDCard, AEarnedTimeTFT Number);
  PROCEDURE BooksSWW(ABookingDay Date, AIDCard TScannedIDCard, AEarnedTimeSWW Number);
  PROCEDURE DetermineTimeForTimeSettings(AOvertimeParams TOvertimeParams, ATimeForTimeYN in out Varchar2, ABonusInMoneyYN in out Varchar2, AShorterWorkingWeekYN in out Varchar2);
  FUNCTION DetermineShorterWorkingWeek(AOvertimeParams TOvertimeParams, AEarnedTimeSWW in out Number, AEarnedMin in out Integer) return Number;
  FUNCTION DetermineTimeForTime(AOvertimeParams TOvertimeParams, AEarnedTimeTFT in out Number, AEarnedMin in out Integer) return Number;
  PROCEDURE DetermineShiftStartEnd(AEmployeeNumber Number, ADepartmentCode Varchar2, AShiftDayRecord in out TShiftDayRecord, APlanFilter Varchar2);
  PROCEDURE GetShiftDay(AIDCard TScannedIDCard, StartDateTime in out Timestamp, EndDateTime in out Timestamp);
  PROCEDURE ComputeOvertimePeriod(fp_employee_number in number, fp_startdate in out date, fp_enddate in out date);
  PROCEDURE FillProdHourPerHourType(AProductionDate Date, AIDCard TScannedIDCard, AHourTypeNumber Integer, AMinutes Integer, AManual Varchar2, AInsertProdHrs Boolean);
  PROCEDURE ReadContractRoundMinutesParams(AIDCard TScannedIDCard, ARoundTruncSalaryHours in out Integer, ARoundMinute in out Integer);
  FUNCTION ContractRoundMinutes(AIDCard TScannedIDCard, ABookingDay Date, AHourTypeNumber Integer) return Integer;
  PROCEDURE RoundMinutesPHEPT(ABookingDay Date, AIDCard TScannedIDCard, AHourTypeNumber Integer, AMinutes Integer, AManual Varchar2);
  PROCEDURE FindContractGroup(AIDCard in out TScannedIDCard);
  FUNCTION BooksExceptionalTimeMain(ABookingDay Date, AExceptRegularMin Integer, AIDCard in out TScannedIDcard, AManual Varchar2, ADuringOvertime Boolean) return Integer;
  FUNCTION BooksExceptionalTime(ABookingDay Date, AExceptRegularMin Integer, AIDCard in out TScannedIDcard, AManual Varchar2) return Integer;
  FUNCTION BooksExceptionalTimeDuringOT(ABookingDay Date, AExceptRegularMin Integer, AIDCard in out TScannedIDcard, AManual Varchar2) return Integer;
  PROCEDURE BooksRegularSalaryHours(ABookingDay Date, AIDCard in out TScannedIDcard, ABookMinutes Integer, AManual Varchar2);
  FUNCTION BooksWorkedHoursOnBankHoliday(ABookingDay Date, AWorkedMin Integer, AIDCard in out TScannedIDcard, AManual Varchar2) return Integer;
  FUNCTION BooksOverTimeMain(ABookingDay Date, AWorkedMin Integer, AIDCard in out TScannedIDcard, AManual Varchar2, ACombinedHourSystem Boolean, ADetect Boolean, ANonOvertimeMin in out Integer,
           AOvertimeMin in out Integer) return Integer;
  FUNCTION BooksOverTime(ABookingDay Date, AWorkedMin Integer, AIDCard in out TScannedIDcard, AManual Varchar2) return Integer;
  FUNCTION DetectExceptHourDefDuringOHT(AIDCard in out TScannedIDCard, AExceptRegularMin Integer) return Boolean;
  FUNCTION BooksOverTimeCombinedHours(ABookingDay Date, AWorkedMin Integer, AIDCard in out TScannedIDcard, AManual Varchar2) return Integer;
  FUNCTION OverTimeDetect(ABookingDay Date, AWorkedMin Integer, AIDCard in out TScannedIDcard, AManual Varchar2, ANonOvertimeMin in out Integer, AOvertimeMin in out Integer) return Integer;
  FUNCTION DetectOvertimeNonOvertime(ABookingDay Date, AWorkedMin Integer, AIDCard in out TScannedIDCard, AManual Varchar2,
    ANonOvertimeMin in out Integer, AOvertimeMin in out Integer) return boolean;
  PROCEDURE ProcessBookings(ABookingDay Date, AIDCard in out TScannedIDCard, AUpdateProdMin Boolean, ARoundMinutes Boolean,
                            AProdMin Integer, APayedBreaks Integer, AManual Varchar2);
  PROCEDURE DeleteProdHourForAllTypes(AIDCard TScannedIDCard, AStartDate Date, AEndDate Date,  AManual Varchar2);
  FUNCTION AbsenceTotalField(AAbsenceTypeCode Char) return Varchar2;
  PROCEDURE UpdateAbsenceTotalMode(AEmployeeNumber Integer, AAbsenceYear Integer, AMinutes Number, AAbsenceTypeCode Char, AAddToOldMinutes Boolean default True);
  PROCEDURE UpdateEMA(AAbsenceReason Varchar2, AIDCard TScannedIDCard, AStartDate Date, AMore Integer);
  PROCEDURE UpdateEmployeeLevel(AIDCard TScannedIDCard);
  FUNCTION UpdateProductionHour(AProductionDate Date, AIDCard TScannedIDCard, AMinutes Integer, APayedBreaks Integer, AManual Varchar2) return Integer;
  FUNCTION UpdateSalaryHour(ASalaryDate Date, AIDCard TScannedIDCard, AHourTypeNumber Integer, AMinutes Integer, AManual Varchar2, AReplace Boolean,
                            AUpdateYes Boolean, AFromManualProd Varchar2 default 'N', ARoundMinutes Boolean default True) return Integer;
  FUNCTION UpdateAbsenceTotal(AFieldName Varchar2, ABookingDay Date, AEmployeeNumber Integer, AMinutes Number, AOverwrite Boolean) return Integer;
  FUNCTION DayInWeek(ACurrentDate Date) return Integer;
  FUNCTION FindPlanningBlocks(AIDCard TScannedIDCard, FirstPlanTimeblock in out Integer, LastPlanTimeblock in out Integer) return Boolean;
  PROCEDURE EmployeeCutOfTime(AFirstScan Boolean, ALastScan Boolean, AIDCard in out TScannedIDCard, ADeleteAction Boolean,
                              ADeleteIDCard TScannedIDCard, AStartDTInterval in out Date, AEndDTInterval in out Date);
  PROCEDURE ComputeBreaks(AIDCard TScannedIDCard, ASTime Date, AETime Date, AOperationMode Integer,
                          AProdMin in out Integer, ABreaksMin in out Integer, APayedBreaks in out Integer);
  PROCEDURE GetShiftWStartEnd(AIDCard TScannedIDCard, AStartDateTime in out Date, AEndDateTime in out Date, AReady in out Boolean);
  PROCEDURE GetShiftStartEnd(AIDCard TScannedIDCard, AShiftDate Date, AStartDateTime in out Date, AEndDateTime in out Date);
  PROCEDURE GetProdMin(AIDCard in out TScannedIDCard, AFirstScan Boolean, ALastScan Boolean,
                       ADeleteAction Boolean, ADeleteIDCard TScannedIDCard, ABookingDay in out Date,
                       AMinutes in out Integer, ABreaksMin in out Integer, APayedBreaks in out Integer);
  PROCEDURE UpdateTimeRegScanning(AIDCard TScannedIDCard, AShiftDate Date);
  PROCEDURE ProcessTimeRecording(AIDCard in out TScannedIDCard, ADeleteIDCard TScannedIDCard, AFirstScan Boolean, ALastScan Boolean,
                                 AUpdateSalMin Boolean, AUpdateProdMinOut Boolean, AUpdateProdMinIn Boolean, ADeleteAction Boolean,
                                 ALastScanRecordForDay Boolean, ACurrentProgramUser Varchar2, ACurrentComputerName Varchar2, ADebug Integer);
  PROCEDURE UnprocessProcessScans(AIDCard TScannedIDCard, AFromDate Date, AProdDate1 in out Date, AProdDate2 in out Date, AProdDate3 in out Date, AProdDate4 in out Date,
                                  AProcessSalary Boolean, ADeleteAction Boolean, ARecalcProdHours in out Boolean, AFromProcessPlannedAbsence Boolean, AManual Boolean,
                                  ACurrentProgramUser Varchar2, ACurrentComputerName Varchar2, ADebug Integer);
  PROCEDURE ProcessScan(AEmployeeNumber Integer, AShiftNumber Integer, APlantCode Varchar2, AWorkspotCode Varchar2, AJobCode Varchar2,
                        ADateIn Date, ADateOut Date, ACurrentProgramUser Varchar2, ACurrentComputerName Varchar2, ADebug Integer);


END PACKAGE_HOUR_CALC;
/
CREATE OR REPLACE PACKAGE BODY "PACKAGE_HOUR_CALC" AS

/*
  Priority:
  1 = Message
  2 = Warning
  3 = Error
  4 = Stack trace
  5 = Debug info
*/
PROCEDURE WLog(APriority Integer, ALogMessage Varchar2)
as
begin
  begin
    insert into abslog
     (abslog_id,
      computer_name,
      abslog_timestamp,
      logsource,
      priority,
      systemuser,
      logmessage)
     values (
      0, -- determined by trigger
      ORASystemDM.CurrentComputerName, -- computer name
      sysdate,
      'PACKAGE_HOUR_CALC', -- logsource
      APriority,
      ORASystemDM.CurrentProgramUser, -- systemuser
      Substr(ALogMessage, 1, 255)
      );
    commit;
  exception
    when others then null;
  end;
end WLog;

PROCEDURE WDebugLog(ALogMessage Varchar2)
as
begin
  WLog(5, ALogMessage);
end WDebugLog;

PROCEDURE WErrorLog(ALogMessage Varchar2)
as
begin
  WLog(3, ALogMessage);
end WErrorLog;

FUNCTION Bool2Str(ABoolean Boolean) return Varchar2 is
begin
  if ABoolean then
    return '1';
  else
    return '0';
  end if;
end Bool2Str;

FUNCTION DateToStr(ADate Date) return Varchar2 is
begin
  if ADate is null then
    return null;
  else
    return TO_CHAR(ADate, 'dd-mm-yyyy');
  end if;
end DateToStr;

FUNCTION DateTimeToStr(ADate Date) return Varchar2 is
begin
  if ADate is null then
    return null;
  else
    return TO_CHAR(ADate, 'dd-mm-yyyy hh24:mi:ss');
  end if;
end DateTimeToStr;

FUNCTION IntToStr(AValue Integer) return Varchar2 is
begin
  return TO_CHAR(AValue);
end IntToStr;

FUNCTION IntMin2StringTime(AValue Integer, ADummy Boolean) return Varchar2 is
begin
  return TO_CHAR(Trunc(AValue / 60), '09') || ':' || TO_CHAR(abs(mod(AValue, 60)), '09'); -- div -> use trunc or it rounds!
end IntMin2StringTime;

FUNCTION ContractGroupTFTExists return Boolean is
  Result Boolean;
  lv_TimeForTimeYN Varchar2(1);
begin
  Result := False;
  begin
    SELECT CG.TIME_FOR_TIME_YN
    INTO lv_TimeForTimeYN
    FROM CONTRACTGROUP CG
    WHERE CG.TIME_FOR_TIME_YN = 'Y' OR
    CG.BONUS_IN_MONEY_YN = 'Y' AND
    ROWNUM < 2;
    if SQL%ROWCOUNT = 1 then
      Result := True;
    end if;
  exception
    when no_data_found then null;
    when others then null;
  end;
  return Result;
end ContractGroupTFTExists;

FUNCTION TruncFrac(ADate Date, AFrac Date) return date is
  gv_year1 number;
  gv_month1 number;
  gv_day1 number;
  gv_hour1 number;
  gv_min1 number;
  gv_sec1 number;
  gv_year2 number;
  gv_month2 number;
  gv_day2 number;
  gv_hour2 number;
  gv_min2 number;
  gv_sec2 number;
begin
  DecodeDateTime(ADate, gv_year1, gv_month1, gv_day1, gv_hour1, gv_min1, gv_sec1);
  DecodeDateTime(AFrac, gv_year2, gv_month2, gv_day2, gv_hour2, gv_min2, gv_sec2);
  return EncodeDateTime(gv_year1, gv_month1, gv_day1, gv_hour2, gv_min2, gv_sec2);
--    select ADate - trunc(ADate)
--    into AFrac
--    from dual;
end TruncFrac;

FUNCTION DateInInterval(ADate Date, ATestDate Date,
  AMinTo Integer, AMinAfter Integer) return Date is
  MinDate Date;
  MaxDate Date;
  Result Date;
begin
  Result := ATestDate;
  MinDate := ADate - AMinTo / DayToMin;
  MaxDate := ADate + AMinAfter / DayToMin;
  if (MinDate <= ATestDate) and (ATestDate <= MaxDate) then
    Result := ADate;
  end if;
  return Result;
end DateInInterval;

FUNCTION ComputeMinutesOfIntersection(AStart1 Date, AEnd1 Date, AStart2 Date, AEnd2 Date) return Integer is
  Result Integer;
begin
  Result := 0;
  if (AEnd1 < AStart1) or (AEnd2 < AStart2) then
    return 0;
  end if;
  Result := (least(AEnd1, AEnd2) - greatest(AStart1, AStart2)) * DayToMin; -- min() max()
  if Result < 0 then
    Result := 0;
  end if;
  return Result;
end ComputeMinutesOfIntersection;

FUNCTION RoundTime(ADateTime Date, ATarget Integer) return date is
  MinSecRounder Integer := 30;
  Year Integer;
  Month Integer;
  Day Integer;
  Hour Integer;
  Minute Integer;
  Sec Integer;
  Result Date;
begin
  Result := ADateTime;
  DecodeDateTime(ADateTime, Year, Month, Day, Hour, Minute, Sec);
  case ATarget
    when 0 then null; -- round to seconds: not used!
    when 1 then -- round to minutes
      begin
        if Sec > MinSecRounder then
          Minute := Minute + 1;
        end if;
        Sec := 0;
      end;
  end case;
  if Minute = 60 then
    Minute := 0;
    Hour := Hour + 1;
    if Hour = 24 then
      Hour := 0;
      Result := Result + 1;
      DecodeDate(Result, Year, Month, Day);
    end if;
  end if;
  Return EncodeDateTime(Year, Month, Day, Hour, Minute, Sec);
end RoundTime;

FUNCTION RoundTimeConditional(ADateTime Date, ATarget Integer) return date is
begin
  if PimsSetting.TimerecordingInSecs then
    return ADateTime;
  else
    return RoundTime(ADateTime, ATarget);
  end if;
end RoundTimeConditional;


PROCEDURE GetPimsSettings as
  gv_code varchar2(3);
  gv_shiftdatesystem_yn varchar2(1);
  gv_fillproductionhours_yn varchar2(1);
  gv_timerecInSecs_yn varchar2(1);
  gv_ProdHrsDoNotSubtrBrks_yn varchar2(1);
begin
  PimsSetting.UseShiftDateSystem := False;
  PimsSetting.FillProdHourPerHourType := False;
  PimsSetting.WeekStartsOn := 1;
  PimsSetting.TimerecordingInSecs := False;
  PimsSetting.ProdHrsDoNotSubtractBreaks := False;
  ORASystemDM.IsCVL := False;
  ORASystemDM.IsLTB := False;
  ORASystemDM.IsRFL := False;
  ORASystemDM.CurrentProgramUser := 'ABS';
  ORASystemDM.CurrentComputerName := 'ABS';
  GlobalDM.ContractGroupTFTExists := ContractGroupTFTExists;

  begin
    select
      CODE,
      SHIFTDATESYSTEM_YN,
      FILLPRODUCTIONHOURS_YN,
      WEEK_START_ON,
      TIMEREC_IN_SECONDS_YN,
      PRODHRS_NOSUBTRACT_BREAKS
    into
      gv_code,
      gv_shiftdatesystem_yn,
      gv_fillproductionhours_yn,
      PimsSetting.WeekStartsOn,
      gv_timerecInSecs_yn,
      gv_ProdHrsDoNotSubtrBrks_yn
    from PIMSSETTING;
    if SQL%ROWCOUNT = 1 then
      if gv_code = 'CVL' then
        ORASystemDM.IsCVL := True;
      else
        if gv_code = 'LTB' then
          ORASystemDM.IsLTB := True;
        else
          if gv_code = 'RFL' then
            ORASystemDM.IsRFL := True;
          end if;
        end if;
      end if;
      PimsSetting.UseShiftDateSystem := gv_shiftdatesystem_yn = 'Y';
      PimsSetting.FillProdHourPerHourType := gv_fillproductionhours_yn = 'Y';
      PimsSetting.TimerecordingInSecs :=  gv_timerecInSecs_yn = 'Y';
      PimsSetting.ProdHrsDoNotSubtractBreaks := gv_ProdHrsDoNotSubtrBrks_yn = 'Y';
    end if;
  exception
    when no_data_found then null;
    when others then null;
  end;
end GetPimsSettings;

PROCEDURE EmptyIDCard(AIDCard in out TScannedIDCard) as
begin
  AIDCard.EmployeeCode   := NULL;
  AIDCard.IDCard         := NULL;
  AIDCard.ShiftNumber    := 0;
  AIDCard.InscanEarly    := 0;
  AIDCard.InscanLate     := 0;
  AIDCard.OutscanEarly   := 0;
  AIDCard.OutScanLate    := 0;
  AIDCard.WorkSpotCode   := NULL;
  AIDCard.WorkSpot       := NULL;
  AIDCard.JobCode        := NULL;
  AIDCard.Job            := NULL;
  AIDCard.DateIn         := NULL;
  AIDCard.DateOut        := NULL;
  AIDCard.PlantCode      := NULL;
  AIDCard.Plant          := NULL;
  AIDCard.DepartmentCode := NULL;
  AIDCard.EmplName       := NULL;
  AIDCard.Plant          := NULL;
  AIDCard.DateInCutOff   := NULL;
  AIDCard.DateOutCutOff  := NULL;
  AIDCard.ContractgroupCode         := NULL;
  AIDCard.ExceptionalBeforeOvertime := NULL;
  AIDCard.RoundTruncSalaryHours     := 0;
  AIDCard.RoundMinute               := 0;
  AIDCard.FromTimeRecordingApp := True;
  AIDCard.IgnoreForOvertimeYN := NULL;
  AIDCard.WSHourtypeNumber := -1;
  AIDCard.ShiftHourtypeNumber := -1;
  AIDCard.ShiftDate := NULL;
  AIDCard.Processed := False;
  AIDCard.DateInOriginal := NULL;
  AIDCard.DateOutOriginal := NULL;
  AIDCard.ShiftStartDateTime := NULL;
  AIDCard.ShiftEndDateTime := NULL;
end EmptyIDCard;

FUNCTION FindWSOverruledContractGroup(APlantCode varchar2, AWorkspotCode varchar2, ADefaultContractGroupCode varchar2) return varchar2 is
  gv_result varchar2(6);
begin
  gv_result := ADefaultContractGroupCode;
  begin
    SELECT
      W.CONTRACTGROUP_CODE
    INTO
      gv_result
    FROM
      WORKSPOT W
    WHERE
      W.PLANT_CODE = APlantCode AND
      W.WORKSPOT_CODE = AWorkspotCode AND
      W.CONTRACTGROUP_CODE IS NOT NULL;
  exception
    when others then null;
  end;
  return gv_result;
end FindWSOverruledContractGroup;

PROCEDURE PopulateIDCard(AIDCard in out TScannedIDCard, ADataSet in TScan, ARoundMinutes in Boolean) as
  WSHourTypeChecked Boolean;
  gv_IgnoreForOvertimeYN Varchar2(1);
begin
  AIDCard.DepartmentCode := NULL;
  AIDCard.ShiftHourtypeNumber := -1;
  AIDCard.WSHourtypeNumber := -1;
  AIDCard.IgnoreForOvertimeYN := '';
  WSHourTypeChecked := False;
  if ADataSet.EmployeeNumber is not null then
    AIDCard.EmployeeCode := ADataSet.EmployeeNumber;
    AIDCard.ShiftNumber := ADataSet.ShiftNumber;
    AIDCard.IDCard := ADataSet.IDCardIn;
    AIDCard.WorkspotCode := ADataSet.WorkspotCode;
    AIDCard.JobCode := ADataSet.JobCode;
    AIDCard.PlantCode := ADataSet.PlantCode;
    if ARoundMinutes then
      AIDCard.DateIn := RoundTime(ADataSet.DateTimeIn,1);
      AIDCard.DateOut := RoundTime(ADataSet.DateTimeOut,1);
    else
      AIDCard.DateIn := RoundTimeConditional(ADataSet.DateTimeIn,1);
      AIDCard.DateOut := RoundTimeConditional(ADataSet.DateTimeOut,1);
    end if;
    AIDCard.DateInOriginal := ADataSet.DateTimeIn;
    AIDCard.DateOutOriginal := ADataSet.DateTimeOut;
    AIDCard.DateInCutOff := AIDCard.DateIn;
    AIDCard.DateOutCutOff := AIDCard.DateOut;
    AIDCard.ShiftDate := ADataSet.ShiftDate;
    AIDcard.ShiftHourtypeNumber := -1;
    AIDCard.IgnoreForOvertimeYN := NULL;
    begin
      if ADataSet.ShiftHourtypeNumber is not null then
        AIDCard.ShiftHourtypeNumber :=
          ADataSet.ShiftHourtypeNumber;
      end if;
    exception
      when others then null;
    end;
    begin
      if ADataSet.ShiftIgnoreForOvertimeYN is not null then
        WSHourTypeChecked := True;
        AIDCard.IgnoreForOvertimeYN :=
          ADataset.IgnoreForOvertimeYN;
      end if;
    exception
      when others then null;
    end;
    AIDCard.WSHourtypeNumber := -1;
    if AIDCard.ShiftHourtypeNumber = -1 then
      begin
        if ADataSet.WorkspotHourtypeNumber is not null then
          AIDCard.WSHourtypeNumber :=
            ADataSet.WorkspotHourtypeNumber;
          if ADataSet.IgnoreForOvertimeYN is not null then
            AIDCard.IgnoreForOvertimeYN :=
              ADataSet.IgnoreForOvertimeYN;
          end if;
        end if;
      exception
        when others then null;
      end;
    end if;
    begin
      if ADataSet.WorkspotDepartmentCode is not null then
        AIDCard.DepartmentCode := ADataSet.WorkspotDepartmentCode;
      else
        begin
          if ADataSet.DepartmentCode is not null then
            AIDCard.DepartmentCode := ADataSet.DepartmentCode;
          end if;
        exception
          when others then null;
        end;
      end if;
    exception
      when others then null;
    end;
  end if;
  begin
    SELECT
      E.CUT_OF_TIME_YN,
      P.INSCAN_MARGIN_EARLY, P.INSCAN_MARGIN_LATE,
      P.OUTSCAN_MARGIN_EARLY, P.OUTSCAN_MARGIN_LATE,
      E.CONTRACTGROUP_CODE, CG.EXCEPTIONAL_BEFORE_OVERTIME,
      CG.ROUND_TRUNC_SALARY_HOURS, CG.ROUND_MINUTE
    INTO AIDCard.CutOfTime,
      AIDCard.InscanEarly, AIDCard.InscanLate,
      AIDcard.OutscanEarly, AIDCard.OutscanLate,
      AIDCard.ContractgroupCode, AIDCard.ExceptionalBeforeOvertime,
      AIDcard.RoundTruncSalaryHours, AIDCard.RoundMinute
    FROM
      EMPLOYEE E INNER JOIN PLANT P ON
        E.PLANT_CODE = P.PLANT_CODE
      INNER JOIN CONTRACTGROUP CG ON
        E.CONTRACTGROUP_CODE = CG.CONTRACTGROUP_CODE
    WHERE
      E.EMPLOYEE_NUMBER = AIDCard.EmployeeCode;
  exception
    when no_data_found then null;
    when others then null;
  end;
  AIDCard.FromTimeRecordingApp := True;
  if ORASystemDM.IsCVL then
    AIDCard.ContractgroupCode := FINDWSOVERRULEDCONTRACTGROUP(
      AIDCard.PlantCode, AIDCard.WorkSpotCode, AIDCard.ContractgroupCode);
  end if;
  if AIDCard.DepartmentCode is null then
    begin
      SELECT
        W.DEPARTMENT_CODE,
        H.IGNORE_FOR_OVERTIME_YN,
        NVL(W.HOURTYPE_NUMBER, -1) HOURTYPE_NUMBER
      INTO
        AIDCard.DepartmentCode,
        gv_IgnoreForOvertimeYN,
        AIDCard.WSHourtypeNumber
      FROM
        WORKSPOT W LEFT OUTER JOIN HOURTYPE H ON
          W.HOURTYPE_NUMBER = H.HOURTYPE_NUMBER
      WHERE
        W.PLANT_CODE = AIDCard.PlantCode AND
        W.WORKSPOT_CODE = AIDcard.WorkspotCode;
      if SQL%ROWCOUNT = 1 then
        if AIDCard.WSHourtypeNumber <> -1 then
          AIDCard.IgnoreForOvertimeYN :=
            gv_IgnoreForOvertimeYN;
        end if;
      end if;
    exception
      when no_data_found then null;
      when others then null;
    end;
  end if;
  if AIDCard.WSHourTypeNumber = -1 then
    if AIDCard.ShiftHourtypeNumber = -1 then
      if not WSHourTypeChecked then
        begin
          SELECT
            S.HOURTYPE_NUMBER,
            H.IGNORE_FOR_OVERTIME_YN
          INTO
            AIDCard.ShiftHourtypeNumber,
            AIDCard.IgnoreForOvertimeYN
          FROM SHIFT S INNER JOIN HOURTYPE H ON
            S.HOURTYPE_NUMBER = H.HOURTYPE_NUMBER
          WHERE S.PLANT_CODE = AIDCard.PlantCode AND
            S.SHIFT_NUMBER = AIDCard.ShiftNumber AND
            S.HOURTYPE_NUMBER IS NOT NULL;
        exception
          when no_data_found then null;
          when others then null;
        end;
      end if;
    end if;
  end if;
end PopulateIDCard;

FUNCTION EncodeDate(fp_year in number,
                    fp_month in number,
                    fp_day in number) return date is
begin
  return to_date(to_char(fp_year) || '-' || to_char(fp_month) || '-' || to_char(fp_day), 'yyyy-mm-dd');
end EncodeDate;

FUNCTION EncodeDateTime(fp_year in number,
                        fp_month in number,
                        fp_day in number,
                        fp_hour in number,
                        fp_min in number,
                        fp_sec in number) return date is
begin
  return to_date(to_char(fp_year) || '-' || to_char(fp_month) || '-' || to_char(fp_day) || ' ' ||
                 to_char(fp_hour) || ':' || to_char(fp_min) || ':' || to_char(fp_sec), 'yyyy-mm-dd hh24:mi:ss');
end EncodeDateTime;

-- DecodeDate
PROCEDURE DecodeDate(fp_date in date,
                     fp_year in out number,
                     fp_month in out number,
                     fp_day in out number) as
begin
  fp_year := extract(year from fp_date);
  fp_month := extract(month from fp_date);
  fp_day := extract(day from fp_date);
end DecodeDate;

PROCEDURE DecodeDateTime(fp_date in date, fp_year in out number, fp_month in out number, fp_day in out number, fp_hour in out number, fp_min in out number, fp_sec in out number) as
  gv_date timestamp;
begin
  gv_date := fp_date;
  fp_year := extract(year from gv_date);
  fp_month := extract(month from gv_date);
  fp_day := extract(day from gv_date);
  fp_hour := extract(hour from gv_date);
  fp_min := extract(minute from gv_date);
  fp_sec := extract(second from gv_date);
end DecodeDateTime;

PROCEDURE DecodeTime(fp_date in date, fp_hour in out number, fp_min in out number, fp_sec in out number) as
  l_year number;
  l_month number;
  l_day number;
begin
  DecodeDateTime(fp_Date, l_year, l_month, l_day, fp_hour, fp_min, fp_sec);
end;

FUNCTION StartOfYear(fp_year in number) return date is
  gv_firstofyear date;
  gv_firstweekofyear number;
  gv_pimsdayofweek number;
begin
  gv_firstofyear := encodedate(fp_year, 1, 1);
  gv_firstweekofyear := 0;
  begin
    select nvl(first_week_of_year, 0)
    into gv_firstweekofyear
    from pimssetting;
  exception
    when no_data_found then null;
    when others then null;
  end;

  if gv_firstweekofyear <> 0 then
    gv_pimsdayofweek := pimsdayofweek(gv_firstofyear);

    if gv_pimsdayofweek <> 0 then
      case gv_firstweekofyear
        when 1 then
          begin
            case gv_pimsdayofweek
            when 2 then gv_firstofyear := gv_firstofyear - 1;
            when 3 then gv_firstofyear := gv_firstofyear - 2;
            when 4 then gv_firstofyear := gv_firstofyear - 3;
            when 5 then gv_firstofyear := gv_firstofyear - 4;
            when 6 then gv_firstofyear := gv_firstofyear - 5;
            when 7 then gv_firstofyear := gv_firstofyear - 6;
            end case;
          end;
        when 4 then
          begin
            case gv_pimsdayofweek
            when 2 then gv_firstofyear := gv_firstofyear - 1;
            when 3 then gv_firstofyear := gv_firstofyear - 2;
            when 4 then gv_firstofyear := gv_firstofyear - 3;
            when 5 then gv_firstofyear := gv_firstofyear + 3;
            when 6 then gv_firstofyear := gv_firstofyear + 2;
            when 7 then gv_firstofyear := gv_firstofyear + 1;
            end case;
          end;
        when 7 then
          begin
            case gv_pimsdayofweek
            when 2 then gv_firstofyear := gv_firstofyear + 6;
            when 3 then gv_firstofyear := gv_firstofyear + 5;
            when 4 then gv_firstofyear := gv_firstofyear + 4;
            when 5 then gv_firstofyear := gv_firstofyear + 3;
            when 6 then gv_firstofyear := gv_firstofyear + 2;
            when 7 then gv_firstofyear := gv_firstofyear + 1;
            end case;
          end;
      end case;
    end if;
  end if;
  return gv_firstofyear;
end StartOfYear;

-- DayOfWeek
FUNCTION DayOfWeek(fp_date in date) return number is
  gv_day number;
begin
  gv_day := 1;
  -- What is daynumber of date-parameter ?
  begin
    SELECT TO_CHAR(fp_date, 'D')
    INTO gv_day
    FROM DUAL;
  exception
    when no_data_found then null;
    when others then null;
  end;
  return gv_day;
end DayOfWeek;

-- LastDayOfWeek
FUNCTION LastDayOfWeek(fp_date in date) return date is
  gv_day number;
  gv_lastday number;
  gv_weekstarton number;
  gv_date date;
begin
  gv_date := fp_date;
  begin
    select week_start_on
    into gv_weekstarton
    from pimssetting;
  exception
    when no_data_found then null;
    when others then null;
  end;

  gv_lastday := 1;
  case gv_weekstarton
  when 1 then gv_lastday := 7;
  when 2 then gv_lastday := 1;
  when 3 then gv_lastday := 2;
  when 4 then gv_lastday := 3;
  when 5 then gv_lastday := 4;
  when 6 then gv_lastday := 5;
  when 7 then gv_lastday := 6;
  end case;
  gv_day := dayofweek(gv_date);
  while gv_day <> gv_lastday
  loop
    gv_date := gv_date + 1;
    gv_day := dayofweek(gv_date);
  end loop;
  return gv_date;
end LastDayOfWeek;

-- WeekOfYear
FUNCTION WeekOfYear(fp_date in date) return number is
  gv_year number;
  gv_month number;
  gv_day number;
  gv_startdate date;
  gv_nextdate date;
  gv_date date;
  gv_dayspast number;
  gv_calcweek number;
  gv_week number;
begin
  decodedate(fp_date, gv_year, gv_month, gv_day);
  gv_startdate := startofyear(gv_year);
  gv_nextdate := startofyear(gv_year + 1);
  gv_date := lastdayofweek(fp_date);

  if (gv_date >= gv_startdate) and
    (gv_date <= gv_nextdate) then
    gv_dayspast := gv_date - gv_startdate;
    gv_calcweek := gv_dayspast / 7;
    gv_week := trunc(gv_calcweek) + 1;
  else
    if (gv_date < gv_startdate) then
      gv_week := weeksinyear(gv_year - 1);
    else
      gv_week := 1;
    end if;
  end if;
  return gv_week;
end WeekOfYear;

-- WeeksInYear
FUNCTION WeeksInYear(fp_year in number) return number is
  gv_date date;
  gv_weeks number;
begin
  gv_date := encodedate(fp_year, 12, 31);
  gv_weeks := weekofyear(gv_date);
  return gv_weeks;
end WeeksInYear;

-- LastMonthDay
FUNCTION LastMonthDay(fp_year in number,
                      fp_month in number) return number is
  gv_date date;
begin
  gv_date := last_day(encodedate(fp_year, fp_month, 1));
  return extract(day from gv_date);
end LastMonthDay;

-- WeekFromDate (based on: WeekUitDat)
PROCEDURE WeekFromDate(fp_date in date,
                       fp_year in out number,
                       fp_week in out number) as
  gv_day number;
  gv_month number;
  gv_startdate date;
  gv_nextdate date;
  gv_date date;
  gv_dayspast number;
  gv_calcweek number;
begin
  gv_date := fp_date;
  decodedate(fp_date, fp_year, gv_month, gv_day);
  if fp_year >= 1930 then
    gv_startdate := startofyear(fp_year);
    gv_nextdate := startofyear(fp_year+1);
    gv_date := lastdayofweek(gv_date);
    if (gv_date >= gv_startdate) and
      (gv_date <= gv_nextdate) then
      gv_dayspast := gv_date - gv_startdate;
      gv_calcweek := gv_dayspast / 7;
      fp_week := trunc(gv_calcweek) + 1;
    else
      if (gv_date < gv_startdate) then
        fp_year := fp_year - 1;
        fp_week := weeksinyear(fp_year);
      else
        fp_year := fp_year + 1;
        fp_week := 1;
      end if;
    end if;
  else
    fp_year := 0;
    fp_week := 0;
  end if;
end WeekFromDate;

FUNCTION MidnightCorrection(STime Date, ETime Date) return Date is
  Hours Integer;
  Mins Integer;
  Secs Integer;
  Result Date;
begin
  Result := ETime;
  if Trunc(STime) = Trunc(ETime) then
    DecodeTime(Result, Hours, Mins, Secs);
    if (Hours = 0) and (Mins = 0) then
      Result := TruncFrac(Result + 1, Result);
    end if;
  end if;
  return Result;
end MidnightCorrection;

PROCEDURE BooksTFT(ABookingDay Date, AIDCard TScannedIDCard, AEarnedTimeTFT Number)
as
  Dummy Integer;
begin
  -- Booking Time for Time
  if AEarnedTimeTFT > 0 then
    Dummy := UpdateAbsenceTotal('EARNED_TFT_MINUTE', ABookingDay,
      AIDCard.EmployeeCode, AEarnedTimeTFT, False);
  end if;
end BooksTFT;

PROCEDURE BooksSWW(ABookingDay Date, AIDCard TScannedIDCard, AEarnedTimeSWW Number)
as
  Dummy Integer;
begin
  -- Booking Shorter Working Week (SWW)
  if AEarnedTimeSWW > 0 then
    Dummy := UpdateAbsenceTotal('EARNED_SHORTWWEEK_MINUTE', ABookingDay,
      AIDCard.EmployeeCode, AEarnedTimeSWW, False);
  end if;
end BooksSWW;

PROCEDURE DetermineTimeForTimeSettings(AOvertimeParams TOvertimeParams, ATimeForTimeYN in out Varchar2, ABonusInMoneyYN in out Varchar2, AShorterWorkingWeekYN in out Varchar2)
as
begin
  if ContractGroupTFTExists then
    ATimeForTimeYN := AOvertimeParams.TimeForTimeYN;
    ABonusInMoneyYN := AOvertimeParams.BonusInMoneyYN;
    AShorterWorkingWeekYN := AOvertimeParams.HT_ADVYN;
  else
    if AOvertimeParams.HT_OvertimeYN = 'Y' then
      ATimeForTimeYN := AOvertimeParams.HT_TimeForTimeYN;
      ABonusInMoneyYN := AOvertimeParams.HT_BonusInMoneyYN;
      AShorterWorkingWeekYN := AOvertimeParams.HT_ADVYN;
    else
      ATimeForTimeYN := 'N';
      ABonusInMoneyYN := 'N';
      AShorterWorkingWeekYN := 'N';
    end if;
  end if;
end DetermineTimeForTimeSettings;

FUNCTION DetermineShorterWorkingWeek(AOvertimeParams TOvertimeParams, AEarnedTimeSWW in out Number, AEarnedMin in out Integer) return Number is
  Percentage Number;
  TimeForTimeYN Varchar2(1);
  BonusInMoneyYN Varchar2(1);
  ShorterWorkingWeekYN Varchar2(1);
begin
  DetermineTimeForTimeSettings(AOvertimeParams, TimeForTimeYN, BonusInMoneyYN,
    ShorterWorkingWeekYN);
  if ShorterWorkingWeekYN = 'Y' then
    Percentage := AOvertimeParams.BonusPercentage;
    if BonusInMoneyYN = 'N' then
      AEarnedTimeSWW := AEarnedTimeSWW +
        Round(AEarnedMin + AEarnedMin * Percentage / 100);
    else
      AEarnedTimeSWW := AEarnedTimeSWW + AEarnedMin;
    end if;
  end if;
  return AEarnedTimeSWW;
end DetermineShorterWorkingWeek;

FUNCTION DetermineTimeForTime(AOvertimeParams TOvertimeParams, AEarnedTimeTFT in out Number, AEarnedMin in out Integer) return Number is
  Percentage Number;
  TimeForTimeYN Varchar2(1);
  BonusInMoneyYN Varchar2(1);
  ShorterWorkingWeekYN Varchar2(1);
begin
  DetermineTimeForTimeSettings(AOvertimeParams, TimeForTimeYN, BonusInMoneyYN,
    ShorterWorkingWeekYN);
  if TimeForTimeYN = 'Y' then
    Percentage := AOvertimeParams.BonusPercentage;
    if BonusInMoneyYN = 'N' then
      AEarnedTimeTFT := AEarnedTimeTFT +
        Round(AEarnedMin + AEarnedMin * Percentage / 100);
    else
      AEarnedTimeTFT := AEarnedTimeTFT + AEarnedMin;
    end if;
  end if;
  return AEarnedTimeTFT;
end DetermineTimeForTime;

-- Procedure that determines the first and last timeblocks for the day of the shift
PROCEDURE DetermineShiftStartEnd(
  AEmployeeNumber Number,
  ADepartmentCode Varchar2,
  AShiftDayRecord in out TShiftDayRecord,
  APlanFilter Varchar2
  ) IS
  Day Integer;
  Ready Boolean;
  Type TDay IS VARRAY(7) of Timestamp;
  StarttimeStart TDay := TDay();
  EndtimeStart TDay := TDay();
  StarttimeEnd TDay := TDay();
  EndtimeEnd TDay := TDay();
BEGIN
  Day := PimsDayOfWeek(AShiftDayRecord.ADate);
  Ready := False;

  StarttimeStart.extend(7);
  EndtimeStart.extend(7);
  StarttimeEnd.extend(7);
  EndtimeEnd.extend(7);

  FOR i in 1..7 LOOP
    StarttimeStart(i) := AShiftDayRecord.ADate;
    EndtimeStart(i) := AShiftDayRecord.ADate;
    StarttimeEnd(i) := AShiftDayRecord.ADate;
    EndtimeEnd(i) := AShiftDayRecord.ADate;
  END LOOP;

  BEGIN
    -- We must know the start and end of the timeblocks
    select starttime1, endtime1, starttime2, endtime2,
      starttime3, endtime3, starttime4, endtime4,
      starttime5, endtime5, starttime6, endtime6,
      starttime7, endtime7
    into StarttimeStart(1), EndtimeStart(1), StarttimeStart(2), EndtimeStart(2),
      StarttimeStart(3), EndtimeStart(3), StarttimeStart(4), EndtimeStart(4),
      StarttimeStart(5), EndtimeStart(5), StarttimeStart(6), EndtimeStart(6),
      StarttimeStart(7), EndtimeStart(7)
    from timeblockperemployee
    where plant_code = AShiftDayRecord.APlantCode and
    employee_number = AEmployeeNumber and
    shift_number = AShiftDayRecord.AShiftNumber and
    rownum < 2 || APlanFilter
    order by timeblock_number;
    select starttime1, endtime1, starttime2, endtime2,
      starttime3, endtime3, starttime4, endtime4,
      starttime5, endtime5, starttime6, endtime6,
      starttime7, endtime7
    into StarttimeEnd(1), EndtimeEnd(1), StarttimeEnd(2), EndtimeEnd(2),
      StarttimeEnd(3), EndtimeEnd(3), StarttimeEnd(4), EndtimeEnd(4),
      StarttimeEnd(5), EndtimeEnd(5), StarttimeEnd(6), EndtimeEnd(6),
      StarttimeEnd(7), EndtimeEnd(7)
    from timeblockperemployee
    where plant_code = AShiftDayRecord.APlantCode and
    employee_number = AEmployeeNumber and
    shift_number = AShiftDayRecord.AShiftNumber and
    rownum < 2 || APlanFilter
    order by timeblock_number desc;
    IF SQL%ROWCOUNT = 1 THEN
      Ready := True;
    END IF;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
    BEGIN
      select starttime1, endtime1, starttime2, endtime2,
        starttime3, endtime3, starttime4, endtime4,
        starttime5, endtime5, starttime6, endtime6,
        starttime7, endtime7
      into StarttimeStart(1), EndtimeStart(1), StarttimeStart(2), EndtimeStart(2),
        StarttimeStart(3), EndtimeStart(3), StarttimeStart(4), EndtimeStart(4),
        StarttimeStart(5), EndtimeStart(5), StarttimeStart(6), EndtimeStart(6),
        StarttimeStart(7), EndtimeStart(7)
      from timeblockperdepartment
      where plant_code = AShiftDayRecord.APlantCode and
      department_code = ADepartmentCode and
      shift_number = AShiftDayRecord.AShiftNumber and
      rownum < 2 || APlanFilter
      order by timeblock_number;
      select starttime1, endtime1, starttime2, endtime2,
        starttime3, endtime3, starttime4, endtime4,
        starttime5, endtime5, starttime6, endtime6,
        starttime7, endtime7
      into StarttimeEnd(1), EndtimeEnd(1), StarttimeEnd(2), EndtimeEnd(2),
        StarttimeEnd(3), EndtimeEnd(3), StarttimeEnd(4), EndtimeEnd(4),
        StarttimeEnd(5), EndtimeEnd(5), StarttimeEnd(6), EndtimeEnd(6),
        StarttimeEnd(7), EndtimeEnd(7)
      from timeblockperdepartment
      where plant_code = AShiftDayRecord.APlantCode and
      department_code = ADepartmentCode and
      shift_number = AShiftDayRecord.AShiftNumber and
      rownum < 2 || APlanFilter
      order by timeblock_number desc;
      IF SQL%ROWCOUNT = 1 THEN
        Ready := True;
      END IF;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
      BEGIN
        select starttime1, endtime1, starttime2, endtime2,
          starttime3, endtime3, starttime4, endtime4,
          starttime5, endtime5, starttime6, endtime6,
          starttime7, endtime7
        into StarttimeStart(1), EndtimeStart(1), StarttimeStart(2), EndtimeStart(2),
          StarttimeStart(3), EndtimeStart(3), StarttimeStart(4), EndtimeStart(4),
          StarttimeStart(5), EndtimeStart(5), StarttimeStart(6), EndtimeStart(6),
          StarttimeStart(7), EndtimeStart(7)
        from timeblockpershift
        where plant_code = AShiftDayRecord.APlantCode and
        shift_number = AShiftDayRecord.AShiftNumber and
        rownum < 2 || APlanFilter
        order by timeblock_number;
        select starttime1, endtime1, starttime2, endtime2,
          starttime3, endtime3, starttime4, endtime4,
          starttime5, endtime5, starttime6, endtime6,
          starttime7, endtime7
        into StarttimeEnd(1), EndtimeEnd(1), StarttimeEnd(2), EndtimeEnd(2),
          StarttimeEnd(3), EndtimeEnd(3), StarttimeEnd(4), EndtimeEnd(4),
          StarttimeEnd(5), EndtimeEnd(5), StarttimeEnd(6), EndtimeEnd(6),
          StarttimeEnd(7), EndtimeEnd(7)
        from timeblockpershift
        where plant_code = AShiftDayRecord.APlantCode and
        shift_number = AShiftDayRecord.AShiftNumber and
        rownum < 2 || APlanFilter
        order by timeblock_number desc;
        IF SQL%ROWCOUNT = 1 THEN
          Ready := True;
        END IF;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
        BEGIN
          select starttime1, endtime1, starttime2, endtime2,
            starttime3, endtime3, starttime4, endtime4,
            starttime5, endtime5, starttime6, endtime6,
            starttime7, endtime7
          into StarttimeStart(1), EndtimeEnd(1), StarttimeStart(2), EndtimeEnd(2),
            StarttimeStart(3), EndtimeEnd(3), StarttimeStart(4), EndtimeEnd(4),
            StarttimeStart(5), EndtimeEnd(5), StarttimeStart(6), EndtimeEnd(6),
            StarttimeStart(7), EndtimeEnd(7)
          from shift
          where plant_code = AShiftDayRecord.APlantCode and
          shift_number = AShiftDayRecord.AShiftNumber;
          IF SQL%ROWCOUNT = 1 THEN
            Ready := True;
          END IF;
        EXCEPTION
          WHEN NO_DATA_FOUND THEN
            NULL;
        END;
      END;
    END;
  END;
  IF Ready THEN
    -- Add date + time-part here
    AShiftDayRecord.AStart := Trunc(AShiftDayRecord.ADate) + (StarttimeStart(Day) - Trunc(StarttimeStart(Day)));
    AShiftDayRecord.AEnd := Trunc(AShiftDayRecord.ADate) + (EndtimeEnd(Day) - Trunc(EndtimeEnd(Day)));
    AShiftDayRecord.AValid := True;
    IF AShiftDayRecord.AStart > AShiftDayRecord.AEnd THEN
      AShiftDayRecord.AEnd := AShiftDayRecord.AEnd + 1;
    END IF;
  ELSE
    AShiftDayRecord.AStart := Trunc(AShiftDayRecord.ADate);
    AShiftDayRecord.AEnd := Trunc(AShiftDayRecord.ADate) + 1 - 1/86400; -- 1 second before the next day;
    AShiftDayRecord.AValid := True;
    WErrorLog('Warning: No timeblock or shift was found!');
  END IF;
END DetermineShiftStartEnd;

PROCEDURE GetShiftDay(
  AIDCard TScannedIDCard,
  StartDateTime in out Timestamp,
  EndDateTime in out Timestamp)
AS
  PeriodStartDateTime Date;
  PeriodEndDateTime Date;
  AEmployeeNumber Integer;
  APlantCode Varchar2(6);
  ADepartmentCode Varchar2(6);
  AShiftNumber Integer;
  AScanStart Timestamp;
  AScanEnd Timestamp;
  Prev TShiftDayRecord;
  Curr TShiftDayRecord;
  Next TShiftDayRecord;
  FirstPlanTimeblock Integer;
  LastPlanTimeblock Integer;
  PlanningBlocksFilter Varchar2(1024);
  PROCEDURE DetermineShift(
    ADate Timestamp,
    AShiftDayRecord IN OUT TShiftDayRecord
  ) IS
      VPlantCode varchar2(6);
      VShiftNumber integer;
  BEGIN
    -- Init the values
    AShiftDayRecord.APlantCode := APlantCode;
    AShiftDayRecord.AShiftNumber := AShiftNumber;
    AShiftDayRecord.ADate := ADate;
    AShiftDayRecord.AValid := False;
    IF (AIDCard.FromTimeRecordingApp) THEN
    BEGIN
      select plant_code, shift_number
      into VPlantCode, VShiftNumber
      from shiftschedule
      where employee_number = AEmployeeNumber and
      shift_schedule_date = trunc(ADate) and
      shift_number <> -1
      and ROWNUM < 2
      order by 1,2;
      IF SQL%ROWCOUNT = 1 THEN
        AShiftDayRecord.APlantCode := VPlantCode;
        AShiftDayRecord.AShiftNumber := VShiftNumber;
      END IF;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
      BEGIN
        select plant_code, shift_number
        into VPlantCode, VShiftNumber
        from employee
        where employee_number = AEmployeeNumber;
        IF SQL%ROWCOUNT = 1 THEN
          AShiftDayRecord.APlantCode := VPlantCode;
          AShiftDayRecord.AShiftNumber := VShiftNumber;
        END IF;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
        BEGIN
          NULL;
        END;
      END;
    END;
    END IF;
  END; -- DetermineShift
  PROCEDURE DetermineNewStartEnd(
    NewScanStart IN OUT Timestamp,
    NewScanEnd IN OUT Timestamp
    ) IS
    Ready Boolean;
    SaveScanEnd Timestamp;
  BEGIN
    Ready := False;
    NewScanStart := Curr.AStart;
    NewScanEnd := Curr.AEnd;

    SaveScanEnd := AScanEnd;
    IF AScanEnd = NULL THEN
      AScanEnd := AScanStart;
    END IF;

    IF (Prev.AValid)
       and
       (
        (
         (Curr.AValid) and
         (AScanEnd < Curr.AStart) and
         (
           (AScanStart <= Prev.AEnd)
           or
           ((AScanStart - Prev.AEnd) < (Curr.AStart - AScanEnd))
         )
       )
       or
       (
         (not Curr.AValid) and
         (
           (not Next.AValid)
           or
           (
             (AScanStart <= Prev.AEnd) or
             (
               (AScanStart > Prev.AEnd) and
               (AScanEnd < Next.AStart) and
               ((AScanStart - Prev.AEnd) < (Next.AStart - AScanEnd))
             )
           )
         )
       )
      ) THEN
      BEGIN
        NewScanStart := Prev.AStart;
        NewScanEnd := Prev.AEnd;
        Ready := True;
      END;
    END IF;

    IF not Ready THEN
    BEGIN
      IF (Next.AValid)
         and
        (
         (
           (Curr.AValid) and
           (AScanStart > Curr.AEnd) and
           (
             (AScanEnd >= Next.AStart)
             or
             ((Next.AStart - AScanEnd) < (AScanStart - Curr.AEnd))
           )
         )
         or
         (
           (not Curr.AValid) and
           (
             (not Prev.AValid)
             or
             (
               (AScanEnd >= Next.AStart) or
               (
                 (AScanStart > Prev.AEnd) and
                 (AScanEnd < Next.AStart) and
                 ((AScanStart - Prev.AEnd) >= (Next.AStart - AScanEnd))
               )
             )
           )
         )
        ) THEN
        BEGIN
          NewScanStart := Next.AStart;
          NewScanEnd := Next.AEnd;
         END;
      END IF;
    END;
    END IF;
    AScanEnd := SaveScanEnd;
  END; -- DetermineNewStartEnd
BEGIN
  AEmployeeNumber := AIDCard.EmployeeCode;
  AScanStart := AIDCard.DateIn;
  AScanEnd := AIDCard.DateOut;
  APlantCode := AIDCard.PlantCode;
  AShiftNumber := AIDCard.ShiftNumber;
  ADepartmentCode := AIDCard.DepartmentCode;

  PlanningBlocksFilter := null;
  if ORASystemDM.IsLTB then
    if FindPlanningBlocks(AIDCard, FirstPlanTimeblock,
      LastPlanTimeblock) then
      PlanningBlocksFilter := ' AND ' ||
        '(' ||
        '  TIMEBLOCK_NUMBER = ' || FirstPlanTimeBlock ||
        '  OR TIMEBLOCK_NUMBER = ' || LastPlanTimeblock ||
        ')';
    end if;
  end if;

  IF AShiftNumber <> -1 THEN
    -- Step 1
    DetermineShift(AScanStart, Curr);
    DetermineShift(AScanStart + 1, Next);
    DetermineShift(AScanStart - 1, Prev);

    -- Step 2
    DetermineShiftStartEnd(AIDCard.EmployeeCode, AIDCard.DepartmentCode, Curr, PlanningBlocksFilter);
    DetermineShiftStartEnd(AIDCard.EmployeeCode, AIDCard.DepartmentCode, Next, PlanningBlocksFilter);
    DetermineShiftStartEnd(AIDCard.EmployeeCode, AIDCard.DepartmentCode, Prev, PlanningBlocksFilter);

    -- Step 3
    DetermineNewStartEnd(StartDateTime, EndDateTime);

    if PimsSetting.UseShiftDateSystem then
      if Trunc(AIDCard.DateIn) < Trunc(StartDateTime) then
        StartDateTime := TruncFrac(AIDCard.DateIn, StartDateTime);
      end if;
    else
      PeriodStartDateTime := StartDateTime;
      PeriodEndDateTime := EndDateTime;
      ComputeOvertimePeriod(AIDCard.EmployeeCode, PeriodStartDateTime, PeriodEndDateTime);
      if Trunc(AIDCard.DateIn) > Trunc(PeriodEndDateTime) then
        Curr.AValid := True;
        Prev.AValid := False;
        Next.AValid := False;
        DetermineNewStartEnd(StartDateTime, EndDateTime);
      end if;
    end if;
  ELSE
    StartDateTime := AScanStart;
    EndDateTime := AScanEnd;
  END IF;
END GetShiftDay;

PROCEDURE ComputeOvertimePeriod(fp_employee_number in number,
                                fp_startdate in out date,
                                fp_enddate in out date) as
  gv_bookingdate date;
  gv_contractgroupcode varchar2(6);
  gv_overtimetype number;
  gv_periodstart number;
  gv_periodlength number;
  gv_dayinweek number;
  gv_year number;
  gv_month number;
  gv_day number;
  gv_weeknumber number;
  gv_periodnumber number;
  gv_startweek number;
  gv_last_error_message varchar2(2000);
  gv_logtimestamp timestamp;
  gv_abslog_rowid rowid;
---------------------------------------------------------
  procedure write_log is
  begin
    select sysdate
    into gv_logtimestamp
    from dual;
    insert into abslog
     (abslog_id,
      computer_name,
      abslog_timestamp,
      logsource,
      priority,
      systemuser,
      logmessage)
     values (
      0, -- determined by trigger
      '-', -- computer name
      gv_logtimestamp,
      'COMPUTEOVERTIMEPERIOD', -- logsource
      1,
      '-', -- systemuser
      'Start ')
      returning rowid into gv_abslog_rowid;
    commit;
  end write_log;
---------------------------------------------------------
begin -- main
  begin
    gv_bookingdate := fp_startdate;
    gv_contractgroupcode := '';
    -- find contractgroupcode of employee
    begin
      select contractgroup_code
      into gv_contractgroupcode
      from employee
      where employee_number = fp_employee_number;
    exception
      when no_data_found then null;
      when others then null;
    end;
    if gv_contractgroupcode is not null then
      -- find contractgroup details
      gv_overtimetype := 0;
      gv_periodstart := 0;
      gv_periodlength := 0;
      begin
        select
          nvl(cg.overtime_per_day_week_period, 0),
          nvl(cg.period_starts_in_week, 0),
          nvl(cg.weeks_in_period, 0)
        into gv_overtimetype, gv_periodstart, gv_periodlength
        from contractgroup cg
        where cg.contractgroup_code = gv_contractgroupcode;
      exception
        when no_data_found then null;
        when others then null;
      end;
      if gv_overtimetype <> 0 then
        -- determine overtime period
        case gv_overtimetype
        when 1 then -- day
          begin
            fp_startdate := gv_bookingdate;
            fp_enddate := gv_bookingdate;
          end;
        when 2 then -- week
          begin
            gv_dayinweek := pimsdayofweek(gv_bookingdate);
            fp_startdate := gv_bookingdate - gv_dayinweek + 1;
            fp_enddate := gv_bookingdate + 7 - gv_dayinweek;
          end;
        when 3 then -- period
          begin
            if (gv_periodstart <= 0) or (gv_periodlength <= 0) then
              -- incorrect values
              fp_startdate := gv_bookingdate;
              fp_enddate := gv_bookingdate;
            else
              weekfromdate(gv_bookingdate, gv_year, gv_weeknumber);
              gv_periodnumber :=
                trunc((gv_weeknumber - gv_periodstart) / gv_periodlength + 1);
              gv_startweek :=
                (gv_periodnumber - 1) * gv_periodlength + gv_periodstart;
              fp_startdate := startofyear(gv_year);
              fp_startdate := fp_startdate + (gv_startweek - 1) * 7;
              fp_enddate := fp_startdate + (gv_periodlength * 7) - 1;
            end if;
          end;
        when 4 then -- month
          begin
            decodedate(gv_bookingdate, gv_year, gv_month, gv_day);
            fp_startdate := encodedate(gv_year, gv_month, 1);
            fp_enddate := encodedate(gv_year, gv_month,
              lastmonthday(gv_year, gv_month));
          end;
        end case;
        fp_startdate := trunc(fp_startdate);
        fp_enddate := trunc(fp_enddate) + 1 - 1/86400; -- 1 second before the next day
      end if;
    end if;
  exception
     WHEN OTHERS THEN
      begin
        gv_last_error_message := SQLERRM;
        write_log;
        update abslog l
         set l.logmessage = Substr(' failed. ' || gv_last_error_message,1,512),
             l.priority = 3 -- error
         where l.rowid = gv_abslog_rowid;
        commit;
      end;
  end;
end ComputeOvertimePeriod;

PROCEDURE FillProdHourPerHourType(AProductionDate Date, AIDCard TScannedIDCard, AHourTypeNumber Integer,
  AMinutes Integer, AManual Varchar2, AInsertProdHrs Boolean)
as
  TotalMinutes Integer;
  TotalMinutesExBreaks Integer;
  Found Boolean;
  procedure DetermineMinExclBreaks as
  begin
    if (GlobalDM.PayedBreakSave > 0) and
      ((TotalMinutesExBreaks - GlobalDM.PayedBreakSave) < 0) then
      GlobalDM.PayedBreakSave := ABS(TotalMinutesExBreaks - GlobalDM.PayedBreakSave);
      TotalMinutesExBreaks := 0;
    else
      TotalMinutesExBreaks := TotalMinutesExBreaks - GlobalDM.PayedBreakSave;
      GlobalDM.PayedBreakSave := 0;
    end if;
  end DetermineMinExclBreaks;
begin
  if (not PimsSetting.FillProdHourPerHourType) then
    Return;
  end if;

  TotalMinutes := 0;
  TotalMinutesExBreaks := 0;
  Found := False;
  if AInsertProdHrs then
    begin
      SELECT
        PHP.PRODUCTION_MINUTE,
        PHP.PROD_MIN_EXCL_BREAK
      INTO
        TotalMinutes,
        TotalMinutesExBreaks
      FROM
        PRODHOURPEREMPLPERTYPE PHP
      WHERE
        PHP.PRODHOUREMPLOYEE_DATE = AProductionDate AND
        PHP.PLANT_CODE = AIDCard.PlantCode AND
        PHP.SHIFT_NUMBER = AIDCard.ShiftNumber AND
        PHP.EMPLOYEE_NUMBER = AIDCard.EmployeeCode AND
        PHP.WORKSPOT_CODE = AIDCard.WorkspotCode AND
        PHP.JOB_CODE = AIDCard.JobCode AND
        PHP.MANUAL_YN = AManual AND
        PHP.HOURTYPE_NUMBER  = AHourTypeNumber;
      if SQL%ROWCOUNT = 1 then
        Found := True;
      end if;
    exception
      when others then null;
    end;
    if Found then
      TotalMinutes := TotalMinutes + AMinutes;
      TotalMinutesExBreaks := TotalMinutesExBreaks + AMinutes;
      DetermineMinExclBreaks;
      begin
        UPDATE
          PRODHOURPEREMPLPERTYPE PHP
        SET
          PHP.PRODUCTION_MINUTE = TotalMinutes,
          PHP.PROD_MIN_EXCL_BREAK = TotalMinutesExBreaks,
          PHP.MUTATIONDATE = SYSDATE,
          PHP.MUTATOR = ORASystemDM.CurrentProgramUser
        WHERE
          PHP.PRODHOUREMPLOYEE_DATE = AProductionDate AND
          PHP.PLANT_CODE = AIDCard.PlantCode AND
          PHP.SHIFT_NUMBER = AIDCard.ShiftNumber AND
          PHP.EMPLOYEE_NUMBER = AIDCard.EmployeeCode AND
          PHP.WORKSPOT_CODE = AIDCard.WorkspotCode AND
          PHP.JOB_CODE = AIDCard.JobCode AND
          PHP.MANUAL_YN = AManual AND
          PHP.HOURTYPE_NUMBER = AHourTypeNumber;
      exception
        when others then null;
      end;
    else
      begin
        TotalMinutesExBreaks := AMinutes;
        DetermineMinExclBreaks;
        INSERT INTO PRODHOURPEREMPLPERTYPE(
          PRODHOUREMPLOYEE_DATE,
          PLANT_CODE,
          SHIFT_NUMBER,
          EMPLOYEE_NUMBER,
          WORKSPOT_CODE,
          JOB_CODE,
          MANUAL_YN,
          HOURTYPE_NUMBER,
          PRODUCTION_MINUTE,
          PROD_MIN_EXCL_BREAK,
          CREATIONDATE,
          MUTATIONDATE,
          MUTATOR
        )
        VALUES(
          AProductionDate,
          AIDCard.PlantCode,
          AIDCard.ShiftNumber,
          AIDCard.EmployeeCode,
          AIDCard.WorkspotCode,
          AIDCard.JobCode,
          AManual,
          AHourTypeNumber,
          AMinutes,
          TotalMinutesExBreaks,
          SYSDATE,
          SYSDATE,
          ORASystemDM.CurrentProgramUser
        );
      exception
        when others then null;
      end;
    end if; -- Found
  else
    begin
      DELETE FROM PRODHOURPEREMPLPERTYPE PHP
      WHERE
        PHP.PRODHOUREMPLOYEE_DATE = AProductionDate AND
        PHP.PLANT_CODE = AIDCard.PlantCode AND
        PHP.SHIFT_NUMBER = AIDCard.ShiftNumber AND
        PHP.EMPLOYEE_NUMBER = AIDCard.EmployeeCode AND
        PHP.WORKSPOT_CODE = AIDCard.WorkspotCode AND
        PHP.JOB_CODE = AIDCard.JobCode AND
        PHP.MANUAL_YN = AManual AND
        PHP.HOURTYPE_NUMBER = AHourTypeNumber;
    exception
      when others then null;
    end;
  end if; -- AInsertProdHrs
end FillProdHourPerHourType;

PROCEDURE ReadContractRoundMinutesParams(AIDCard TScannedIDCard, ARoundTruncSalaryHours in out Integer, ARoundMinute in out Integer)
as
begin
  ARoundTruncSalaryHours := AIDCard.RoundTruncSalaryHours;
  ARoundMinute := AIDCard.RoundMinute;
  if (ARoundTruncSalaryHours = 0) and (ARoundMinute = 0) then
    begin
      SELECT
        C.ROUND_TRUNC_SALARY_HOURS,
        C.ROUND_MINUTE
      INTO
        ARoundTruncSalaryHours,
        ARoundMinute
      FROM
        CONTRACTGROUP C,
        EMPLOYEE E
      WHERE
        C.CONTRACTGROUP_CODE = E.CONTRACTGROUP_CODE AND
        E.EMPLOYEE_NUMBER = AIDCard.EmployeeCode;
    exception
      when others then null;
    end;
  end if;
end ReadContractRoundMinutesParams;

FUNCTION ContractRoundMinutes(AIDCard TScannedIDCard, ABookingDay Date, AHourTypeNumber Integer) return Integer is
  NewSalMin Integer;
  OldSalMin Integer;
  DifferenceMin Integer;
  RoundTruncSalaryHours Integer;
  RoundMinute Integer;
  PreviousDifferenceMin Integer;
  LeftOverMin Integer;
  Result Integer;
  Dummy Integer;
  CURSOR C_SHE IS
    SELECT
      SHE.SALARY_DATE,
      SHE.HOURTYPE_NUMBER,
      SHE.SALARY_MINUTE
    FROM
      SALARYHOURPEREMPLOYEE SHE
    WHERE
      SHE.SALARY_DATE = ABookingDay AND
      SHE.MANUAL_YN = 'N' AND
      SHE.EMPLOYEE_NUMBER = AIDCard.EmployeeCode AND
      ((AHourTypeNumber = -1) OR (SHE.HOURTYPE_NUMBER = AHourTypeNumber))
    ORDER BY
      SHE.HOURTYPE_NUMBER;
  function RoundMinutes(RoundTruncSalaryHours Integer, RoundMinute Integer,
    Minutes Integer) return Integer is
    Hours Integer;
    Mins Number; -- Must be a number or trunc gives same result as round
  begin
    Hours := Trunc(Minutes / 60); -- div: Be sure to use trunc or it rounds automatically!
    Mins := Abs(mod(Minutes, 60));
    if RoundTruncSalaryHours = 1 then -- Round
      Mins := Round(Mins / RoundMinute) * RoundMinute;
    else
      Mins := Trunc(Mins / RoundMinute) * RoundMinute;
    end if;
    return Hours * 60 + Mins;
  end RoundMinutes;
begin
  Result := 0;
  ReadContractRoundMinutesParams(AIDCard,
    RoundTruncSalaryHours, RoundMinute);
  if RoundMinute <= 1 then
    return Result;
  end if;
  PreviousDifferenceMin := -999;
  begin
    for R_SHE IN C_SHE loop
      LeftOverMin := 0;
      if (RoundTruncSalaryHours = 0) then -- Truncate
        if (PreviousDifferenceMin <> -999) then
          if (PreviousDifferenceMin < 0) then
            LeftOverMin := ABS(PreviousDifferenceMin);
          else
            LeftOverMin := PreviousDifferenceMin * -1;
          end if;
          Dummy := UpdateSalaryHour(ABookingDay, AIDCard,
            R_SHE.HOURTYPE_NUMBER,
            LeftOverMin,
            'N', False,
            False);
        end if; -- PreviousDifferenceMin <> -999
      end if; -- RoundTruncSalary = 0
      OldSalMin := R_SHE.SALARY_MINUTE + LeftOverMin;
      NewSalMin :=
        RoundMinutes(RoundTruncSalaryHours, RoundMinute, OldSalMin);
      DifferenceMin := NewSalMin - OldSalMin;
      PreviousDifferenceMin := DifferenceMin;
      if (RoundTruncSalaryHours = 0) then -- Truncate
        Result := Result + DifferenceMin;
      end if;
      if DifferenceMin <> 0 then
        Dummy := UpdateSalaryHour(ABookingDay, AIDCard,
          R_SHE.HOURTYPE_NUMBER,
          DifferenceMin,
          'N', False,
          False);
        if ORASystemDM.ADebug = 2 then
          WDebugLog('SAL Round mins. Bookday=' || DateToStr(ABookingDay) ||
            ' OldSal=' || IntToStr(OldSalMin) ||
            ' NewSal=' || IntToStr(NewSalMin) ||
            ' Diff=' || IntToStr(DifferenceMin) ||
            ' HT=' || IntToStr(R_SHE.HOURTYPE_NUMBER) ||
            ' Emp=' || IntToStr(AIDCard.EmployeeCode) ||
            ' In=' || DateTimeToStr(AIDCard.DateIn) ||
            ' Out=' || DateTimeToStr(AIDCard.DateOut)
          );
        end if;
      end if;
    end loop;
  exception
    when others then null;
  end;
  return Result;
end ContractRoundMinutes;

-- RoundMinutesProdHourPerEmpPerType
PROCEDURE RoundMinutesPHEPT(ABookingDay Date, AIDCard TScannedIDCard, AHourTypeNumber Integer, AMinutes Integer, AManual Varchar2)
as
  OldProdMin Integer;
  NewProdMin Integer;
  ProdMinExclBreak Integer;
  CURSOR C_PHE1 IS
    SELECT
      P.PRODHOUREMPLOYEE_DATE,
      P.PLANT_CODE,
      P.SHIFT_NUMBER,
      P.EMPLOYEE_NUMBER,
      P.WORKSPOT_CODE,
      P.JOB_CODE,
      P.HOURTYPE_NUMBER,
      P.MANUAL_YN,
      P.PRODUCTION_MINUTE,
      P.PROD_MIN_EXCL_BREAK
    FROM
      PRODHOURPEREMPLPERTYPE P
    WHERE
      P.PRODHOUREMPLOYEE_DATE = ABookingDay AND
      P.MANUAL_YN = AManual AND
      P.EMPLOYEE_NUMBER = AIDCard.EmployeeCode AND
      P.HOURTYPE_NUMBER = AHourTypeNumber AND
      ROWNUM < 2
    ORDER BY
      P.PRODUCTION_MINUTE DESC;
  procedure StoreProdMinExclBreak
  as
    CURSOR C_PHE2 IS
      SELECT
        P.PRODHOUREMPLOYEE_DATE,
        P.PLANT_CODE,
        P.SHIFT_NUMBER,
        P.EMPLOYEE_NUMBER,
        P.WORKSPOT_CODE,
        P.JOB_CODE,
        P.HOURTYPE_NUMBER,
        P.MANUAL_YN,
        P.PRODUCTION_MINUTE,
        P.PROD_MIN_EXCL_BREAK
      FROM
        PRODHOURPEREMPLPERTYPE P
      WHERE
        P.PRODHOUREMPLOYEE_DATE >= ABookingDay - 5 AND
        P.PRODHOUREMPLOYEE_DATE <= ABookingDay AND
        P.MANUAL_YN = AManual AND
        P.EMPLOYEE_NUMBER = AIDCard.EmployeeCode AND
        ROWNUM < 2
      ORDER BY
        P.PRODHOUREMPLOYEE_DATE DESC;
  begin
    begin
      for R_PHE in C_PHE2 loop
        begin
          UPDATE PRODHOURPEREMPLPERTYPE
          SET PROD_MIN_EXCL_BREAK = R_PHE.PROD_MIN_EXCL_BREAK + ProdMinExclBreak,
          MUTATIONDATE = SYSDATE,
          MUTATOR = ORASystemDM.CurrentProgramUser
          WHERE PRODHOUREMPLOYEE_DATE = R_PHE.PRODHOUREMPLOYEE_DATE AND
          PLANT_CODE = R_PHE.PLANT_CODE AND
          SHIFT_NUMBER = R_PHE.SHIFT_NUMBER AND
          EMPLOYEE_NUMBER = R_PHE.EMPLOYEE_NUMBER AND
          WORKSPOT_CODE = R_PHE.WORKSPOT_CODE AND
          JOB_CODE = R_PHE.JOB_CODE AND
          MANUAL_YN = R_PHE.MANUAL_YN AND
          HOURTYPE_NUMBER = R_PHE.HOURTYPE_NUMBER;
        exception
          when others then null;
        end;
      end loop;
    exception
      when others then null;
    end;

  end StoreProdMinExclBreak;
begin
  if (not PimsSetting.FillProdHourPerHourType) then
    return;
  end if;
  begin
    for R_PHE in C_PHE1 loop
      ProdMinExclBreak := R_PHE.PROD_MIN_EXCL_BREAK;
      OldProdMin := R_PHE.PRODUCTION_MINUTE;
      NewProdMin := OldProdMin + AMinutes;
      if NewProdMin <> 0 then
        begin
          UPDATE PRODHOURPEREMPLPERTYPE
          SET PRODUCTION_MINUTE = NewProdMin,
          MUTATIONDATE = SYSDATE,
          MUTATOR = ORASystemDM.CurrentProgramUser
          WHERE PRODHOUREMPLOYEE_DATE = R_PHE.PRODHOUREMPLOYEE_DATE AND
          PLANT_CODE = R_PHE.PLANT_CODE AND
          SHIFT_NUMBER = R_PHE.SHIFT_NUMBER AND
          EMPLOYEE_NUMBER = R_PHE.EMPLOYEE_NUMBER AND
          WORKSPOT_CODE = R_PHE.WORKSPOT_CODE AND
          JOB_CODE = R_PHE.JOB_CODE AND
          MANUAL_YN = R_PHE.MANUAL_YN AND
          HOURTYPE_NUMBER = R_PHE.HOURTYPE_NUMBER;
        exception
          when others then null;
        end;
      else
        begin
          DELETE FROM PRODHOURPEREMPLPERTYPE
          WHERE PRODHOUREMPLOYEE_DATE = R_PHE.PRODHOUREMPLOYEE_DATE AND
          PLANT_CODE = R_PHE.PLANT_CODE AND
          SHIFT_NUMBER = R_PHE.SHIFT_NUMBER AND
          EMPLOYEE_NUMBER = R_PHE.EMPLOYEE_NUMBER AND
          WORKSPOT_CODE = R_PHE.WORKSPOT_CODE AND
          JOB_CODE = R_PHE.JOB_CODE AND
          MANUAL_YN = R_PHE.MANUAL_YN AND
          HOURTYPE_NUMBER = R_PHE.HOURTYPE_NUMBER;
          StoreProdMinExclBreak;
        exception
          when others then null;
        end;
      end if; -- NewProdMin <> 0;
    end loop;
  exception
    when others then null;
  end;
end RoundMinutesPHEPT;

PROCEDURE FindContractGroup(AIDCard in out TScannedIDCard)
as
begin
  if AIDCard.ContractGroupCode is null then
    begin
      SELECT
        E.CONTRACTGROUP_CODE
      INTO
        AIDCard.ContractGroupCode
      FROM
        EMPLOYEE E INNER JOIN PLANT P ON
          E.PLANT_CODE = P.PLANT_CODE
        INNER JOIN CONTRACTGROUP CG ON
          E.CONTRACTGROUP_CODE = CG.CONTRACTGROUP_CODE
      WHERE
        E.EMPLOYEE_NUMBER = AIDCard.EmployeeCode;
    exception
      when others then null;
    end;
  end if;
end FindContractGroup;

FUNCTION BooksExceptionalTimeMain(ABookingDay Date, AExceptRegularMin Integer, AIDCard in out TScannedIDcard, AManual Varchar2, ADuringOvertime Boolean) return Integer is
  StartTime Date;
  EndTime Date;
  STime Date;
  ETime Date;
  MyHourTypeNumber Integer;
  ProdMin Integer;
  BreaksMin Integer;
  PayedBreaks Integer;
  IntersectionMin Integer;
  DayOfWeek Integer;
  IgnoreForOvertimeYN Varchar2(1);
  Result Integer;
  TYPE cv_excepthourdef IS REF CURSOR;
  cv cv_excepthourdef;
  TYPE TCV_Rec IS RECORD (
    DayOfWeek Integer,
    StartTime Date,
    Endtime Date,
    HourtypeNumber Integer,
    HTIgnoreForOvertimeYN Varchar2(1),
    OvertimeHourtypeNumber Integer,
    OHTIgnoreForOvertimeYN Varchar2(1)
  );
  CV_Rec TCV_Rec;
  Dummy Integer;
  procedure DetermineBreaks(AEndTime Date, ABreaksMin in out Integer, APayedBreaks in out Integer)
  as
    BreaksMinTotal Integer;
    PayedBreaksTotal Integer;
    DateIn Date;
    DateOut Date;
  begin
    DateIn := AIDCard.DateIn;
    DateOut := AEndTime;
    BreaksMinTotal := 0;
    PayedBreaksTotal := 0;
    loop
      ComputeBreaks(AIDCard, DateIn, DateOut,
        1, ProdMin, ABreaksMin, APayedBreaks);
      BreaksMinTotal := BreaksMinTotal + ABreaksMin;
      PayedBreaksTotal := PayedBreaksTotal + APayedBreaks;
      if (ABreaksMin > 0) then
        DateIn := DateOut;
        DateOut := DateOut + (ABreaksMin / 60 / 24);
      end if;
      exit when (ABreaksMin <= 0);
    end loop;
    ABreaksMin := BreaksMinTotal;
    APayedBreaks := PayedBreaksTotal;
  end DetermineBreaks;
  procedure DetermineBreaksOvernightScan(AEndTime in out Date,
    ABreaksMin in out Integer, APayedBreaks in out Integer)
  as
    SaveDateIn Date;
    SaveDateOut Date;
    SaveEndTime Date;
    MyBreaksMinTotal Integer;
    MyPayedBreaksTotal Integer;
    OvernightScan Boolean;
  begin
    OvernightScan := Trunc(StartTime) <> Trunc(AEndTime);
    MyBreaksMinTotal := 0;
    MyPayedBreaksTotal := 0;
    SaveDateIn := AIDCard.DateIn;
    SaveDateOut := AIDCard.DateOut;
    SaveEndTime := AEndTime;
    if OvernightScan then
      AIDCard.DateOut := Trunc(AIDCard.DateOut);
      AEndTime := AIDCard.DateOut;
      DetermineBreaks(AEndTime, ABreaksMin, APayedBreaks);
      MyBreaksMinTotal := ABreaksMin;
      MyPayedBreaksTotal := APayedBreaks;
      AIDCard.DateIn := AIDCard.DateOut;
      AIDCard.DateOut := SaveDateOut;
      AEndTime := SaveEndTime;
    end if;
    DetermineBreaks(AEndTime, ABreaksMin, APayedBreaks);
    MyBreaksMinTotal := MyBreaksMinTotal + ABreaksMin;
    MyPayedBreaksTotal := MyPayedBreaksTotal + APayedBreaks;
    if OvernightScan then
      AIDCard.DateIn := SaveDateIn;
      AIDCard.DateOut := SaveDateOut;
    end if;
    ABreaksMin := MyBreaksMinTotal;
    APayedBreaks := MyPayedBreaksTotal;
  end DetermineBreaksOvernightScan;
begin
  Result := 0;
  StartTime := AIDCard.DateInCutOff;
  EndTime   := StartTime + AExceptRegularMin / DayToMin;

  if AIDCard.WSHourtypeNumber <> -1 then
    Result := AExceptRegularMin;
    return Result;
  end if;

  if AIDCard.ShiftHourtypeNumber <> -1 then
    Result := AExceptRegularMin;
    return Result;
  end if;

  if AIDCard.IgnoreForOvertimeYN = 'Y' then
    Result := AExceptRegularMin;
    return Result;
  end if;

  DetermineBreaksOvernightScan(EndTime, BreaksMin, PayedBreaks);
  EndTime := EndTime + (Breaksmin - PayedBreaks) / DayToMin;
  Result  := AExceptRegularMin;
  FindContractGroup(AIDCard);
  begin
    OPEN cv FOR
      'SELECT DISTINCT ' ||
      '  EHD.DAY_OF_WEEK, ' ||
      '  EHD.STARTTIME, ' ||
      '  EHD.ENDTIME, ' ||
      '  EHD.HOURTYPE_NUMBER, ' ||
      '  (SELECT NVL(H1.IGNORE_FOR_OVERTIME_YN, ''N'') FROM HOURTYPE H1 ' ||
      '   WHERE H1.HOURTYPE_NUMBER = EHD.HOURTYPE_NUMBER) HT_IGNORE_FOR_OVERTIME_YN, ' ||
      '  NVL(EHD.OVERTIME_HOURTYPE_NUMBER, -1) OVERTIME_HOURTYPE_NUMBER, ' ||
      '  CASE ' ||
      '    WHEN EHD.OVERTIME_HOURTYPE_NUMBER IS NOT NULL THEN ' ||
      '      (SELECT NVL(H2.IGNORE_FOR_OVERTIME_YN, ''N'') FROM HOURTYPE H2 ' ||
      '       WHERE H2.HOURTYPE_NUMBER = EHD.OVERTIME_HOURTYPE_NUMBER) ' ||
      '    ELSE ' ||
      '      NULL ' ||
      '  END OHT_IGNORE_FOR_OVERTIME_YN ' ||
      'FROM ' ||
      '  EXCEPTIONALHOURDEF EHD INNER JOIN CONTRACTGROUP CG ' ||
      '    ON EHD.CONTRACTGROUP_CODE = CG.CONTRACTGROUP_CODE ' ||
      'WHERE ' ||
      '  ( ' ||
      '    EHD.CONTRACTGROUP_CODE = :CONTRACTGROUP_CODE ' ||
      '  )  ' ||
      '  AND ' ||
      '  ( ' ||
      '    ( ' ||
      '      (TRUNC(:START_TIME) = TRUNC(:END_TIME)) AND ' ||
      '      (EHD.DAY_OF_WEEK = :DAY_OF_WEEK) ' ||
      '    ) ' ||
      '    OR ' ||
      '    ( ' ||
      '      (TRUNC(:START_TIME) <> TRUNC(:END_TIME)) AND ' ||
      '      ((EHD.DAY_OF_WEEK = :WEEKDAYSTART) ' ||
      '       OR (EHD.DAY_OF_WEEK = :WEEKDAYEND)) ' ||
      '    ) ' ||
      '  )     ' ||
      'ORDER BY 1,2'
    USING AIDCard.ContractgroupCode, StartTime, EndTime, DayInWeek(StartTime),
      StartTime, EndTime, DayInWeek(StartTime), DayInWeek(EndTime);
    loop
      fetch CV into CV_Rec;
      exit when CV%NOTFOUND;
      DayOfWeek := CV_Rec.DayOfWeek;
      if (ADuringOvertime) then
        if (CV_Rec.OvertimeHourtypeNumber <> -1) then
          MyHourTypeNumber := CV_Rec.OvertimeHourtypeNumber;
          IgnoreForOvertimeYN := CV_Rec.OHTIgnoreForOvertimeYN;
        else
          MyHourTypeNumber := CV_Rec.HourtypeNumber;
          IgnoreForOvertimeYN := CV_Rec.HTIgnoreForOvertimeYN;
        end if;
      else
        MyHourTypeNumber := CV_Rec.HourtypeNumber;
        IgnoreForOvertimeYN := CV_Rec.HTIgnoreForOvertimeYN;
      end if;
      STime := CV_Rec.StartTime;
      ETime := CV_Rec.EndTime;
      if DayOfWeek = DayInWeek(StartTime) then
        STime := TruncFrac(StartTime, STime); -- ReplaceDate(STime, StartTime)
        ETime := TruncFrac(StartTime, ETime); -- ReplaceDate(ETime, StartTime)
      else
        STime := TruncFrac(EndTime, STime); -- ReplaceDate(STime, EndTime)
        ETime := TruncFrac(EndTime, ETime); -- ReplaceDate(ETime, EndTime)
      end if;
      ETime := MidnightCorrection(STime, ETime);
      IntersectionMin := ComputeMinutesOfIntersection(StartTime, EndTime,
        STime, ETime);
      if IntersectionMin > 0 then
        if STime < StartTime then
          STime := StartTime;
        end if;
        if ETime > EndTime then
          ETime := EndTime;
        end if;
        if STime > ETime then
          STime := ETime;
        end if;
        ComputeBreaks(AIDCard, STime, ETime, 0, ProdMin,
          BreaksMin, PayedBreaks);
        IntersectionMin := IntersectionMin - BreaksMin + PayedBreaks;
        if IntersectionMin > 0 then
          Dummy := UpdateSalaryHour(ABookingDay, AIDCard,
            MyHourTypeNumber, IntersectionMin, AManual, False,
            True);
          Result := Result - IntersectionMin;
        end if; -- IntersectionMin > 0
      end if; -- IntersectionMin > 0
    end loop;
  exception
    when no_data_found then null;
    when others then null;
  end;
  close CV;
  return Result;
end BooksExceptionalTimeMain;

FUNCTION BooksExceptionalTime(ABookingDay Date, AExceptRegularMin Integer, AIDCard in out TScannedIDcard, AManual Varchar2) return Integer is
begin
  return BooksExceptionalTimeMain(
    ABookingDay, AExceptRegularMin, AIDCard,
    AManual,
    False -- DuringOvertime
    );
end BooksExceptionalTime;

FUNCTION BooksExceptionalTimeDuringOT(ABookingDay Date, AExceptRegularMin Integer, AIDCard in out TScannedIDcard, AManual Varchar2) return Integer is
begin
  return BooksExceptionalTimeMain(
    ABookingDay, AExceptRegularMin, AIDCard,
    AManual,
    True -- ADuringOvertime
    );
end BooksExceptionalTimeDuringOT;

PROCEDURE BooksRegularSalaryHours(ABookingDay Date, AIDCard in out TScannedIDcard, ABookMinutes Integer, AManual Varchar2) as
  MyHourTypeNumber Integer;
  Dummy Integer;
begin
  if
    (
      (AManual <> 'Y') and (ABookMinutes > 0) -- non-manual hours
      or
      (AManual = 'Y') and (ABookMinutes <> 0) -- manual hours
    ) then
    MyHourTypeNumber := 1;

    if AIDCard.WSHourtypeNumber <> -1 then
      MyHourTypeNumber := AIDCard.WSHourtypeNumber;
    end if;
    if AIDCard.ShiftHourtypeNumber <> -1 then
      MyHourTypeNumber := AIDCard.ShiftHourtypeNumber;
    end if;
    Dummy := UpdateSalaryHour(ABookingDay, AIDcard,
      MyHourTypeNumber, ABookMinutes, AManual, False,
      True);
  end if;
end BooksRegularSalaryHours;

FUNCTION BooksWorkedHoursOnBankHoliday(ABookingDay Date, AWorkedMin Integer, AIDCard in out TScannedIDcard, AManual Varchar2) return Integer is
  HourtypeWorkedOnBankHoliday Integer;
  AbsenceReasonID Integer;
  AbsenceMinutes Integer;
  AbsenceReasonCode Varchar2(1);
  AbsenceTypeCode Varchar2(1);
  AbsenceYear Integer;
  Month Integer;
  Day Integer;
  EarnedMin Integer;
  EarnedTimeTFT Number;
  EarnedTimeSWW Number;
  Result Integer;
  Found Boolean;
  function DetermineHTWorkedOnBankHoliday return Integer is
    Result Integer;
    CGHourtypeWorkedOnBankHoliday Integer;
  begin
    Result := -1;
    begin
      SELECT
        B.ABSENCEREASON_CODE,
        AR.ABSENCETYPE_CODE,
        NVL(AR.ABSENCEREASON_ID, -1),
        NVL(B.HOURTYPE_WRK_ON_BANKHOL, -1),
        NVL(CG.HOURTYPE_WRK_ON_BANKHOL, -1) AS CG_HOURTYPE_WRK_ON_BANKHOL
      INTO
        AbsenceReasonCode,
        AbsenceTypeCode,
        AbsenceReasonID,
        HourtypeWorkedOnBankHoliday,
        CGHourtypeWorkedOnBankHoliday
      FROM
        BANKHOLIDAY B INNER JOIN COUNTRY C ON
          B.COUNTRY_ID = C.COUNTRY_ID
        INNER JOIN PLANT P ON
          C.COUNTRY_ID = P.COUNTRY_ID
        INNER JOIN EMPLOYEE E ON
          P.PLANT_CODE = E.PLANT_CODE
        INNER JOIN CONTRACTGROUP CG ON
         E.CONTRACTGROUP_CODE = CG.CONTRACTGROUP_CODE
        INNER JOIN ABSENCEREASON AR ON
          B.ABSENCEREASON_CODE = AR.ABSENCEREASON_CODE
      WHERE
        E.EMPLOYEE_NUMBER = AIDCard.EmployeeCode AND
        B.BANK_HOLIDAY_DATE = ABookingDay;
      if SQL%ROWCOUNT = 1 then
        if CGHourtypeWorkedOnBankHoliday <> -1 then
          Result := CGHourtypeWorkedOnBankHoliday;
        else
          Result := HourtypeWorkedOnBankHoliday;
        end if;
      end if;
    exception
      when others then Result := -1;
    end;
    return Result;
  end DetermineHTWorkedOnBankHoliday;
begin
  Result := AWorkedMin;
  if Result = 0 then
    return Result;
  end if;
  AbsenceReasonCode := null;
  AbsenceTypeCode := null;
  AbsenceReasonID := -1;
  EarnedTimeTFT := 0;
  EarnedTimeSWW := 0;
  FindContractGroup(AIDCard);
  HourtypeWorkedOnBankHoliday := DetermineHTWorkedOnBankHoliday;
  if HourtypeWorkedOnBankHoliday <> -1 then
    Found := False;
    begin
      SELECT
        C.TIME_FOR_TIME_YN,
        C.BONUS_IN_MONEY_YN,
        H.BONUS_PERCENTAGE BONUS_PERCENTAGE,
        H.OVERTIME_YN HT_OVERTIME_YN,
        H.TIME_FOR_TIME_YN HT_TIME_FOR_TIME_YN,
        H.BONUS_IN_MONEY_YN HT_BONUS_IN_MONEY_YN,
        H.ADV_YN HT_ADV_YN
      INTO
        OvertimeParams.TimeForTimeYN,
        OvertimeParams.BonusInMoneyYN,
        OvertimeParams.BonusPercentage,
        OvertimeParams.HT_OvertimeYN,
        OvertimeParams.HT_TimeForTimeYN,
        OvertimeParams.HT_BonusInMoneyYN,
        OvertimeParams.HT_ADVYN
      FROM
        CONTRACTGROUP C, HOURTYPE H
      WHERE
        C.CONTRACTGROUP_CODE = AIDCard.ContractGroupCode AND
        H.HOURTYPE_NUMBER = HourtypeWorkedOnBankHoliday;
      if SQL%ROWCOUNT = 1 then
        Found := True;
      end if;
    exception
      when others then null;
    end;
    if Found then
      UpdateEMA(AbsenceReasonCode, AIDCard, ABookingDay, 0);
      begin
        SELECT
          A.ABSENCE_MINUTE
        INTO
          AbsenceMinutes
        FROM
          ABSENCEHOURPEREMPLOYEE A
        WHERE
          A.EMPLOYEE_NUMBER = AIDCard.EmployeeCode AND
          A.ABSENCEHOUR_DATE = ABookingDay AND
          A.ABSENCEREASON_ID = AbsenceReasonID AND
          A.MANUAL_YN = 'N';
        if SQL%ROWCOUNT = 1 then
          if AbsenceTypeCode is not null then
            DecodeDate(ABookingDay, AbsenceYear, Month, Day);
            UpdateAbsenceTotalMode(AIDCard.EmployeeCode,
              AbsenceYear, -1 * AbsenceMinutes, AbsenceTypeCode);
          end if;
        end if;
      exception
        when others then null;
      end;
      -- Now delete the absence hours.
      begin
        DELETE FROM ABSENCEHOURPEREMPLOYEE
        WHERE EMPLOYEE_NUMBER = AIDCard.EmployeeCode AND
        ABSENCEHOUR_DATE = ABookingDay AND
        ABSENCEREASON_ID = AbsenceReasonID AND
        MANUAL_YN = 'N';
      exception
        when others then null;
      end;
      -- Book hours on bank holiday
      EarnedMin := UpdateSalaryHour(ABookingDay,
        AIDCard, HourtypeWorkedOnBankHoliday, AWorkedMin, AManual, False,
        True,
        'N', True
        );
      EarnedTimeTFT := DetermineTimeForTime(OvertimeParams,
        EarnedTimeTFT, EarnedMin);
      EarnedTimeSWW := DetermineShorterWorkingWeek(OvertimeParams,
        EarnedTimeSWW, EarnedMin);
      BooksTFT(ABookingDay, AIDCard, EarnedTimeTFT);
      BooksSWW(ABookingDay, AIDCard, EarnedTimeSWW);
      Result := 0;
    end if; -- Found
  end if; -- HourtypeWorkedOnBankHoliday <> -1
  return Result;
end BooksWorkedHoursOnBankHoliday;

FUNCTION BooksOverTimeMain(ABookingDay Date, AWorkedMin Integer, AIDCard in out TScannedIDcard, AManual Varchar2, ACombinedHourSystem Boolean, ADetect Boolean, ANonOvertimeMin in out Integer,
         AOvertimeMin in out Integer) return Integer is
  MyStartMin Integer;
  MyEndMin Integer;
  MyHourTypeNumber Integer;
  MyEarnedMin Integer;
  MyRegisteredMin Integer;
  MyOverTimeMin Integer;
  MyTotOverTime Integer;
  MyStartDate Date;
  MyEndDate Date;
  MyBookedMin Integer;
  EarnedTimeTFT Number;
  EarnedTimeSWW Number;
  TYPE cv_overtimedef IS REF CURSOR;
  cv cv_overtimedef;
  Type TCV_Rec is record(
    LineNumber Integer,
    HourtypeNumber Integer,
    StartTime Integer,
    EndTime Integer,
    TimeForTimeYN Varchar2(1),
    BonusInMoneyYN Varchar2(1),
    BonusPercentage Number,
    HT_OvertimeYN Varchar2(1),
    HT_TimeForTimeYN Varchar2(1),
    HT_BonusInMoneyYN Varchar2(1),
    HT_ADVYN Varchar2(1)
  );
  CV_Rec TCV_Rec;
  OvertimeParams TOvertimeParams;
  Result Integer;
  function DetermineStandardMinPerMonth return Integer is
    Type TWorkDays is Varray(7) of Boolean;
    Type TWorkDaysYN is Varray(7) of Char;
    WorkDays TWorkDays := TWorkDays();
    WorkDaysYN TWorkDaysYN := TWorkDaysYN();
    NormalHoursPerDay Integer;
    LastDayMonth Integer;
    Year Integer;
    Month Integer;
    Day Integer;
    Result Integer;
  begin
    Result := 0;
    WorkDays.extend(7);
    WorkDaysYN.extend(7);
    FindContractGroup(AIDCard);
    begin
      SELECT
        C.WORKDAY_MO_YN,
        C.WORKDAY_TU_YN,
        C.WORKDAY_WE_YN,
        C.WORKDAY_TH_YN,
        C.WORKDAY_FR_YN,
        C.WORKDAY_SA_YN,
        C.WORKDAY_SU_YN,
        C.NORMALHOURSPERDAY
      INTO
        WorkDaysYN(1),
        WorkDaysYN(2),
        WorkDaysYN(3),
        WorkDaysYN(4),
        WorkDaysYN(5),
        WorkDaysYN(6),
        WorkDaysYN(7),
        NormalHoursPerDay
      FROM
        CONTRACTGROUP C
      WHERE
        (C.CONTRACTGROUP_CODE = AIDCard.ContractgroupCode) AND
        (C.OVERTIME_PER_DAY_WEEK_PERIOD = 4);
      if SQL%ROWCOUNT = 1 then
        for i in 1..7 loop
          WorkDays(i) := WorkDaysYN(i) = 'Y';
        end loop;
        DecodeDate(ABookingDay, Year, Month, Day);
        LastDayMonth := LastMonthDay(Year, Month);
        for iday in 1..LastDayMonth loop
          if WorkDays(DayOfWeek(EncodeDate(Year, Month, iday))) then
            Result := Result + NormalHoursPerDay;
          end if;
        end loop;
      end if;
    exception
      when no_data_found then null;
      when others then null;
    end;
    return Result;
  end DetermineStandardMinPerMonth;
begin
  MyRegisteredMin     := AWorkedMin;
  MyTotOverTime       := 0;
  EarnedTimeTFT       := 0;
  MyStartDate         := ABookingDay;
  MyEndDate           := ABookingDay;
  MyEarnedMin         := 0;
  EarnedTimeSWW       := 0;
  Result              := 0;
  if AIDCard.WSHourtypeNumber <> -1 then
    Result := AWorkedMin;
    return Result;
  end if;

  if AIDCard.ShiftHourtypeNumber <> -1 then
    Result := AWorkedMin;
    return Result;
  end if;

  if AIDCard.IgnoreForOvertimeYN = 'Y' then
    Result := AWorkedMin;
    return Result;
  end if;

  ComputeOvertimePeriod(AIDCard.EmployeeCode, MyStartDate, MyEndDate);
  GlobalDM.StandardMinutesPerMonth := DetermineStandardMinPerMonth;

  begin
    SELECT
    (
      SELECT
        NVL(SUM(S.SALARY_MINUTE), 0) SAL_MINUTES
      FROM
        SALARYHOURPEREMPLOYEE S, HOURTYPE H
      WHERE
        S.HOURTYPE_NUMBER = H.HOURTYPE_NUMBER AND
        S.SALARY_DATE >= MyStartDate AND
        S.SALARY_DATE <= MyEndDate AND
        S.EMPLOYEE_NUMBER = AIDCard.EmployeeCode AND
        H.IGNORE_FOR_OVERTIME_YN = 'N'
    )
    +
    (
      SELECT
        NVL(SUM(AE.ABSENCE_MINUTE), 0)
      FROM
        ABSENCEHOURPEREMPLOYEE AE, ABSENCEREASON AR, HOURTYPE H
      WHERE
        AE.ABSENCEREASON_ID = AR.ABSENCEREASON_ID AND
        AR.HOURTYPE_NUMBER = H.HOURTYPE_NUMBER AND
        AE.ABSENCEHOUR_DATE >= MyStartDate AND
        AE.ABSENCEHOUR_DATE <= MyEndDate AND
        AE.EMPLOYEE_NUMBER = AIDCard.EmployeeCode AND
        H.IGNORE_FOR_OVERTIME_YN = 'N'
    ) TOT_MINUTES
    INTO
      MyBookedMin
    FROM DUAL;
    if SQL%ROWCOUNT = 1 then
      MyRegisteredMin := MyRegisteredMin + MyBookedMin;
    end if;
  exception
    when no_data_found then null;
    when others then null;
  end;

  FindContractGroup(AIDCard);
  begin
    OPEN cv FOR
      'SELECT ' ||
      '  OD.LINE_NUMBER, ' ||
      '  OD.HOURTYPE_NUMBER, ' ||
      '  OD.STARTTIME, ' ||
      '  OD.ENDTIME,  ' ||
      '  CG.TIME_FOR_TIME_YN, ' ||
      '  CG.BONUS_IN_MONEY_YN,  ' ||
      '  H.BONUS_PERCENTAGE, ' ||
      '  H.OVERTIME_YN HT_OVERTIME_YN, ' ||
      '  H.TIME_FOR_TIME_YN HT_TIME_FOR_TIME_YN, ' ||
      '  H.BONUS_IN_MONEY_YN HT_BONUS_IN_MONEY_YN, ' ||
      '  H.ADV_YN HT_ADV_YN ' ||
      'FROM ' ||
      '  CONTRACTGROUP CG, ' ||
      '  OVERTIMEDEFINITION OD, HOURTYPE H ' ||
      'WHERE ' ||
      '  CG.CONTRACTGROUP_CODE = :CONTRACTGROUP_CODE AND ' ||
      '  CG.CONTRACTGROUP_CODE = OD.CONTRACTGROUP_CODE AND ' ||
      '  OD.HOURTYPE_NUMBER = H.HOURTYPE_NUMBER  ' ||
      'ORDER BY  ' ||
      '  OD.LINE_NUMBER'
    USING AIDCard.ContractgroupCode;
    loop
      fetch CV into CV_Rec;
      exit when CV%NOTFOUND;
      MyStartMin := CV_Rec.StartTime + GlobalDM.StandardMinutesPerMonth;
      MyEndMin := CV_Rec.EndTime + GlobalDM.StandardMinutesPerMonth;
      MyHourTypeNumber := CV_Rec.HourtypeNumber;
      MyOverTimeMin := 0;

      if (MyBookedMin > MyStartMin) then
        MyStartMin := MyBookedMin;
      end if;
      if ((MyRegisteredMin >= MyStartMin) and (least(MyRegisteredMin, MyEndMin) <= MyEndMin)) then
        MyOverTimeMin := least(MyRegisteredMin, MyEndMin) - MyStartMin;
      end if;
      if (MyOverTimeMin < 0) then
        MyOverTimeMin := 0;
      end if;
      exit when (MyStartMin >= MyRegisteredMin) and (MyOverTimeMin <= 0);
      MyTotOverTime := MyTotOverTime + MyOverTimeMin;
      if MyOverTimeMin > 0 then
        if not (ACombinedHourSystem and ADetect) then
            MyEarnedMin := UpdateSalaryHour(ABookingDay, AIDCard,
              MyHourTypeNumber, MyOverTimeMin, AManual,
              False,
              True);
        else
          MyEarnedMin := 0;
        end if;
        if not (ACombinedHourSystem and ADetect) then
          OvertimeParams.TimeForTimeYN := CV_Rec.TimeForTimeYN;
          OvertimeParams.BonusInMoneyYN := CV_Rec.BonusInMoneyYN;
          OvertimeParams.BonusPercentage := CV_Rec.BonusPercentage;
          OvertimeParams.HT_OvertimeYN := CV_Rec.HT_OvertimeYN;
          OvertimeParams.HT_TimeForTimeYN := CV_Rec.HT_TimeForTimeYN;
          OvertimeParams.HT_BonusInMoneyYN := CV_Rec.HT_BonusInMoneyYN;
          OvertimeParams.HT_ADVYN := CV_Rec.HT_ADVYN;
          EarnedTimeTFT := DetermineTimeForTime(OvertimeParams,
            EarnedTimeTFT, MyEarnedMin);
          EarnedTimeSWW := DetermineShorterWorkingWeek(OvertimeParams,
            EarnedTimeSWW, MyEarnedMin);
        end if;
      end if;
    end loop;
  exception
    when no_data_found then null;
    when others then null;
  end;
  close CV;
  if (ACombinedHourSystem and ADetect) then
    AOvertimeMin := MyTotOverTime;
    ANonOvertimeMin := AWorkedMin - MyTotOverTime;
    if (ANonOvertimeMin < 0) then
      ANonOvertimeMin := 0;
    end if;
    Result := 0;
    return Result;
  end if;
  BooksTFT(ABookingDay, AIDCard, EarnedTimeTFT);
  BooksSWW(ABookingDay, AIDCard, EarnedTimeSWW);
  Result := AWorkedMin - MyTotOverTime;

  return Result;
end BooksOverTimeMain;

FUNCTION BooksOverTime(ABookingDay Date, AWorkedMin Integer, AIDCard in out TScannedIDcard, AManual Varchar2) return Integer is
  NonOvertimeMinDummy Integer;
  OvertimeMinDummy Integer;
begin
  NonOvertimeMinDummy := 0;
  OvertimeMinDummy := 0;
  return BooksOvertimeMain(
    ABookingDay, AWorkedMin, AIDCard,
    AManual,
    False, -- ACombinedHourSystem
    False, -- Detect: Detection no
    NonOvertimeMinDummy, OvertimeMinDummy);
end BooksOverTime;

FUNCTION DetectExceptHourDefDuringOHT(AIDCard in out TScannedIDCard, AExceptRegularMin Integer) return Boolean is
  StartTime Date;
  EndTime Date;
  OvertimeHourtypeNumber Integer;
  Result Boolean;
begin
  Result := False;

  StartTime := AIDCard.DateInCutOff;
  EndTime := StartTime + AExceptRegularMin/DayToMin;
  FindContractGroup(AIDCard);
  OvertimeHourtypeNumber := -1;
  begin
    SELECT DISTINCT
      NVL(EHD.OVERTIME_HOURTYPE_NUMBER, -1)
    INTO
      OvertimeHourtypeNumber
    FROM
      EXCEPTIONALHOURDEF EHD INNER JOIN CONTRACTGROUP CG
        ON EHD.CONTRACTGROUP_CODE = CG.CONTRACTGROUP_CODE
    WHERE
      (
        EHD.CONTRACTGROUP_CODE = AIDCard.ContractgroupCode
      )
      AND
      (
        (
          (TRUNC(StartTime) = TRUNC(EndTime)) AND
          (EHD.DAY_OF_WEEK = DayInWeek(StartTime))
        )
        OR
        (
          (TRUNC(StartTime) <> TRUNC(EndTime)) AND
          ((EHD.DAY_OF_WEEK = DayInWeek(StartTime))
           OR (EHD.DAY_OF_WEEK = DayInWeek(EndTime)))
        )
      );
  exception
    when others then null;
  end;

  if OvertimeHourtypeNumber <> -1 then
    Result := True;
  end if;
  return Result;
end DetectExceptHourDefDuringOHT;

FUNCTION BooksOverTimeCombinedHours(ABookingDay Date, AWorkedMin Integer, AIDCard in out TScannedIDcard, AManual Varchar2) return Integer is
  NonOvertimeMinDummy Integer;
  OvertimeMinDummy Integer;
begin
  NonOvertimeMinDummy := 0;
  OvertimeMinDummy := 0;
  return BooksOvertimeMain(
    ABookingDay, AWorkedMin, AIDCard,
    AManual,
    True, -- CombinedHourSystem
    False, -- Detect: Detection no
    NonOvertimeMinDummy, OvertimeMinDummy);
end BooksOverTimeCombinedHours;

FUNCTION OverTimeDetect(ABookingDay Date, AWorkedMin Integer, AIDCard in out TScannedIDcard, AManual Varchar2, ANonOvertimeMin in out Integer, AOvertimeMin in out Integer) return Integer is
begin
  return BooksOvertimeMain(
    ABookingDay, AWorkedMin, AIDCard,
    AManual,
    True, -- CombinedHourSystem
    True, -- Detect: Detection yes
    ANonOvertimeMin,
    AOvertimeMin);
end;

FUNCTION DetectOvertimeNonOvertime(ABookingDay Date, AWorkedMin Integer, AIDCard in out TScannedIDCard, AManual Varchar2,
  ANonOvertimeMin in out Integer, AOvertimeMin in out Integer) return boolean is
  Result Boolean;
  Dummy Integer;
begin
  Result := DetectExceptHourDefDuringOHT(AIDCard, AWorkedMin);
  if Result then
    ANonOvertimeMin := 0;
    AOvertimeMin := 0;
    Dummy := OvertimeDetect(ABookingDay, AWorkedMin, AIDCard,
      AManual, ANonOvertimeMin, AOvertimeMin);
    Result := (AOvertimeMin <> 0);
  end if;
  return Result;
end DetectOvertimeNonOvertime;

PROCEDURE ProcessBookings(ABookingDay Date, AIDCard in out TScannedIDCard, AUpdateProdMin Boolean, ARoundMinutes Boolean,
                          AProdMin Integer, APayedBreaks Integer, AManual Varchar2)
as
  MyNonOverTime Integer;
  BookExceptionalBeforeOvertime Boolean;
  OvertimeMin Integer;
  NonOvertimeMin Integer;
  OldDateInCutOff Date;
  OldDateIn Date;
  MyOverTime Integer;
  function DetermineExceptionalBeforeOT(
    AIDCard TScannedIDCard) return Boolean is
    ExceptionalBeforeOvertimeYN Varchar2(1);
    Result Boolean;
  begin
    Result := False;
    begin
      SELECT
        CG.EXCEPTIONAL_BEFORE_OVERTIME
      INTO
        ExceptionalBeforeOvertimeYN
      FROM
        CONTRACTGROUP CG INNER JOIN EMPLOYEE E ON
          CG.CONTRACTGROUP_CODE = E.CONTRACTGROUP_CODE
      WHERE
        E.EMPLOYEE_NUMBER = AIDCard.EmployeeCode;
      if SQL%ROWCOUNT = 1 then
        Result := ExceptionalBeforeOvertimeYN = 'Y';
      end if;
    exception
      when others then null;
    end;
    return Result;
  end DetermineExceptionalBeforeOT;
begin
  BookExceptionalBeforeOvertime :=
    DetermineExceptionalBeforeOT(AIDCard);
  GlobalDM.PayedBreakSave := APayedBreaks;
  if (AProdMin+APayedBreaks > 0) or ARoundMinutes then
    if BooksWorkedHoursOnBankHoliday(ABookingDay, AProdMin + APayedBreaks,
      AIDCard, AManual) <> 0 then
      if DetectOvertimeNonOvertime(ABookingDay,
        AProdMin + APayedBreaks, AIDCard, AManual,
        NonOvertimeMin, OvertimeMin) then
        OldDateIn := AIDCard.DateIn;
        OldDateInCutOff := AIDCard.DateInCutOff;
        if NonOvertimeMin <> 0 then
          MyNonOverTime := BooksExceptionalTime(ABookingDay,
            NonOvertimeMin, AIDCard, AManual);
          BooksRegularSalaryHours(ABookingDay,
            AIDCard, MyNonOverTime, AManual);
          AIDCard.DateIn :=
            AIDCard.DateIn + (NonOvertimeMin / 60 / 24);
          AIDCard.DateInCutOff :=
            AIDCard.DateInCutOff + (NonOvertimeMin / 60 / 24);
        end if;
        if OvertimeMin <> 0 then
          MyOverTime := BooksExceptionalTimeDuringOT(
            ABookingDay, OvertimeMin, AIDCard, AManual);
          MyOverTime := BooksOverTimeCombinedHours(ABookingDay,
            MyOverTime, AIDCard, AManual);
          BooksRegularSalaryHours(ABookingDay,
            AIDCard, MyOverTime, AManual);
        end if;
        AIDCard.DateIn := OldDateIn;
        AIDCard.DateInCutOff := OldDateInCutOff;
      else
        if not BookExceptionalBeforeOvertime then
          -- Default: First book overtime, then exceptional
          MyNonOverTime := BooksOverTime(ABookingDay, AProdMin + APayedBreaks,
            AIDCard, AManual);
          MyNonOverTime := BooksExceptionalTime(ABookingDay, MyNonOverTime,
            AIDCard, AManual);
          BooksRegularSalaryHours(ABookingDay, AIDCard, MyNonOverTime, AManual);
        else
          -- First book exceptional then overtime
          MyNonOverTime := BooksExceptionalTime(ABookingDay, AProdMin + APayedBreaks,
            AIDCard, AManual);
          MyNonOverTime := BooksOverTime(ABookingDay, MyNonOverTime,
            AIDCard, AManual);
          BooksRegularSalaryHours(ABookingDay, AIDCard, MyNonOverTime, AManual);
        end if; -- if not BookExceptionalBeforeOvertime
      end if;
    end if;
  end if;
  GlobalDM.PayedBreakSave := 0;
end ProcessBookings;

PROCEDURE DeleteProdHourForAllTypes(AIDCard TScannedIDCard, AStartDate Date, AEndDate Date, AManual Varchar2)
as
begin
  if (not PimsSetting.FillProdHourPerHourType) then
    return;
  end if;
  begin
    DELETE FROM
      PRODHOURPEREMPLPERTYPE PHE
    WHERE
      PHE.PRODHOUREMPLOYEE_DATE >= AStartDate AND
      PHE.PRODHOUREMPLOYEE_DATE <= AEndDate AND
      PHE.EMPLOYEE_NUMBER = AIDCard.EmployeeCode AND
      PHE.MANUAL_YN = AManual;
  exception
    when others then null;
  end;
end DeleteProdHourForAllTypes;

FUNCTION AbsenceTotalField(AAbsenceTypeCode Char) return Varchar2 is
  Result Varchar2(80);
begin
  case AAbsenceTypeCode
    when 'H' then Result := 'USED_HOLIDAY_MINUTE';
    when 'I' then Result := 'ILLNESS_MINUTE';
    when 'T' then Result := 'USED_TFT_MINUTE';
    when 'W' then Result := 'USED_WTR_MINUTE';
    when 'R' then Result := 'USED_HLD_PREV_YEAR_MINUTE';
    when 'E' then Result := 'USED_SENIORITY_HOLIDAY_MINUTE';
    when 'F' then Result := 'USED_ADD_BANK_HLD_MINUTE';
    when 'S' then Result := 'USED_SAT_CREDIT_MINUTE';
    when 'C' then Result := 'USED_BANK_HLD_RESERVE_MINUTE';
    when 'D' then Result := 'USED_SHORTWWEEK_MINUTE';
    when 'X' then Result := 'NORM_BANK_HLD_RESERVE_MINUTE';
    when 'Y' then Result := 'EARNED_SAT_CREDIT_MINUTE';
    else
      Result := '';
  end case;
  return Result;
end AbsenceTotalField;

PROCEDURE UpdateAbsenceTotalMode(AEmployeeNumber Integer, AAbsenceYear Integer, AMinutes Number, AAbsenceTypeCode Char, AAddToOldMinutes Boolean default True)
as
  UpdateField Varchar2(80);
  OldMinutes Number;
  SelectFlag1 Varchar2(80);
  FlagFieldName1 Varchar2(80);
  Field2Result Varchar2(80);
  dml_str varchar2(32767);
  TYPE cv_absencetotal IS REF CURSOR;
  cv cv_absencetotal;
  Found Boolean;
  function TestFlag1(AValue Varchar2) return Boolean is
    Result Boolean;
  begin
    Result := False;
    if UpdateField = 'NORM_BANK_HLD_RESERVE_MINUTE' then
      if (AValue = 'Y') then
        Result := True;
      end if;
    end if;
    return Result;
  end TestFlag1;
begin
  UpdateField := AbsenceTotalField(AAbsenceTypeCode);
  if UpdateField is null then
    return;
  end if;

  SelectFlag1 := ', MUTATOR '; -- default dummy field
  FlagFieldName1 := '';
  if UpdateField = 'NORM_BANK_HLD_RESERVE_MINUTE' then
    FlagFieldName1 := 'NORM_BANK_HLD_RES_MIN_MAN_YN';
    SelectFlag1 := ', ' + FlagFieldName1 + ' ';
  end if;

  Found := False;
  Field2Result := '';
  begin
    OPEN cv FOR
    'SELECT ' || UpdateField || SelectFlag1 ||
    ' FROM ABSENCETOTAL WHERE EMPLOYEE_NUMBER = ' ||
    AEmployeeNumber || ' AND ABSENCE_YEAR = ' || AAbsenceYear;
    FETCH cv INTO OldMinutes, Field2Result;
    if CV%ROWCOUNT = 1 then
      Found := True;
    end if;
  exception
    when others then null;
  end;
  close cv;

  if Found then
    if not TestFlag1(Field2Result) then
      if not AAddToOldMinutes then
        OldMinutes := 0;
      end if;
      dml_str :=
        'BEGIN ' ||
        '  UPDATE ABSENCETOTAL ' ||
        '  SET ' || UpdateField || ' = :NEWMIN, ' ||
        '  MUTATIONDATE = SYSDATE, ' ||
        '  MUTATOR = :AMutator ' ||
        '  WHERE EMPLOYEE_NUMBER = :AEmployeeNumber AND ' ||
        '  ABSENCE_YEAR = :AYear; ' ||
        ' END;';
      begin
        EXECUTE IMMEDIATE dml_str
        USING OldMinutes + AMinutes, ORASystemDM.CurrentProgramUser, AEmployeeNumber, AAbsenceYear;
      exception
        when others then WErrorLog('UpdateAbsenceTotalMode: ' || SQLERRM);
      end;
    end if;
  else
    dml_str :=
      'BEGIN ' ||
      '  INSERT INTO ABSENCETOTAL( ' ||
      '  EMPLOYEE_NUMBER, ABSENCE_YEAR, CREATIONDATE, MUTATIONDATE, MUTATOR,' || UpdateField || ')' ||
      '  VALUES(:EMPNO, :AYEAR, SYSDATE, SYSDATE, :MUT, :NEWMIN); ' ||
      'END;';
    begin
      EXECUTE IMMEDIATE dml_str
      USING AEmployeeNumber, AAbsenceYear, ORASystemDM.CurrentProgramUser, AMinutes;
    exception
      when others then WErrorLog('UpdateAbsenceTotalMode: ' || SQLERRM);
    end;
  end if;
end UpdateAbsenceTotalMode;

PROCEDURE UpdateEMA(AAbsenceReason Varchar2, AIDCard TScannedIDCard, AStartDate Date, AMore Integer)
as
begin
  -- AMore = 1: Multiple records are changed!
  -- AMore = 0: Only 1 record is changed.
  begin
    UPDATE EMPLOYEEAVAILABILITY
    SET AVAILABLE_TIMEBLOCK_1 = '*',
    MUTATIONDATE = SYSDATE,
    MUTATOR = ORASystemDM.CurrentProgramUser
    WHERE
    (
      (EMPLOYEEAVAILABILITY_DATE >= AStartDate AND AMore = 1) OR
      (EMPLOYEEAVAILABILITY_DATE = AStartDate AND AMore = 0)
    ) AND
    EMPLOYEE_NUMBER = AIDCard.EmployeeCode
    AND AVAILABLE_TIMEBLOCK_1 = AAbsenceReason;
  exception
    when no_data_found then null;
    when others then WErrorLog('UpdateEMA: ' || SQLERRM);
  end;
  begin
    UPDATE EMPLOYEEAVAILABILITY
    SET AVAILABLE_TIMEBLOCK_2 = '*',
    MUTATIONDATE = SYSDATE,
    MUTATOR = ORASystemDM.CurrentProgramUser
    WHERE
    (
      (EMPLOYEEAVAILABILITY_DATE >= AStartDate AND AMore = 1) OR
      (EMPLOYEEAVAILABILITY_DATE = AStartDate AND AMore = 0)
    ) AND
    EMPLOYEE_NUMBER = AIDCard.EmployeeCode
    AND AVAILABLE_TIMEBLOCK_2 = AAbsenceReason;
  exception
    when no_data_found then null;
    when others then WErrorLog('UpdateEMA: ' || SQLERRM);
  end;
  begin
    UPDATE EMPLOYEEAVAILABILITY
    SET AVAILABLE_TIMEBLOCK_3 = '*',
    MUTATIONDATE = SYSDATE,
    MUTATOR = ORASystemDM.CurrentProgramUser
    WHERE
    (
      (EMPLOYEEAVAILABILITY_DATE >= AStartDate AND AMore = 1) OR
      (EMPLOYEEAVAILABILITY_DATE = AStartDate AND AMore = 0)
    ) AND
    EMPLOYEE_NUMBER = AIDCard.EmployeeCode
    AND AVAILABLE_TIMEBLOCK_3 = AAbsenceReason;
  exception
    when no_data_found then null;
    when others then WErrorLog('UpdateEMA: ' || SQLERRM);
  end;
  begin
    UPDATE EMPLOYEEAVAILABILITY
    SET AVAILABLE_TIMEBLOCK_4 = '*',
    MUTATIONDATE = SYSDATE,
    MUTATOR = ORASystemDM.CurrentProgramUser
    WHERE
    (
      (EMPLOYEEAVAILABILITY_DATE >= AStartDate AND AMore = 1) OR
      (EMPLOYEEAVAILABILITY_DATE = AStartDate AND AMore = 0)
    ) AND
    EMPLOYEE_NUMBER = AIDCard.EmployeeCode
    AND AVAILABLE_TIMEBLOCK_4 = AAbsenceReason;
  exception
    when no_data_found then null;
    when others then WErrorLog('UpdateEMA: ' || SQLERRM);
  end;
end UpdateEMA;

PROCEDURE UpdateEmployeeLevel(AIDCard TScannedIDCard)
as
  Found Boolean;
  EmpLevel Varchar2(1);
begin
  Found := False;
  begin
    SELECT
      WPE.EMPLOYEE_LEVEL
    INTO
      EmpLevel
    FROM
      WORKSPOTSPEREMPLOYEE WPE
    WHERE
      WPE.EMPLOYEE_NUMBER = AIDCard.EmployeeCode AND
      WPE.PLANT_CODE = AIDCard.PlantCode AND
      WPE.DEPARTMENT_CODE = AIDCard.DepartmentCode AND
      WPE.WORKSPOT_CODE = AIDCard.WorkspotCode;
    if SQL%ROWCOUNT = 1 then
      Found := True;
    end if;
  exception
    when no_data_found then Found := False;
  end;
  if Found then
    if (INSTR('ABC', EmpLevel) <= 0) then
      begin
        UPDATE WORKSPOTSPEREMPLOYEE WPE
        SET
          WPE.EMPLOYEE_LEVEL = 'C',
          WPE.STARTDATE_LEVEL = SYSDATE,
          WPE.MUTATIONDATE = SYSDATE,
          WPE.MUTATOR = ORASystemDM.CurrentProgramUser
        WHERE
          WPE.EMPLOYEE_LEVEL NOT IN ('A','B','C') AND
          WPE.EMPLOYEE_NUMBER = AIDCard.EmployeeCode AND
          WPE.PLANT_CODE = AIDCard.PlantCode AND
          WPE.DEPARTMENT_CODE = AIDCard.DepartmentCode AND
          WPE.WORKSPOT_CODE = AIDCard.WorkspotCode;
       exception
         when others then WErrorLog('UpdateEmployeeLevel: ' || SQLERRM);
       end;
    end if;
  else
    begin
      INSERT INTO WORKSPOTSPEREMPLOYEE(
        EMPLOYEE_NUMBER,
        PLANT_CODE,
        EMPLOYEE_LEVEL,
        DEPARTMENT_CODE,
        WORKSPOT_CODE,
        STARTDATE_LEVEL,
        CREATIONDATE,
        MUTATIONDATE,
        MUTATOR
      )
      VALUES(
        AIDCard.EmployeeCode,
        AIDCard.PlantCode,
        'C',
        AIDCard.DepartmentCode,
        AIDCard.WorkspotCode,
        SYSDATE,
        SYSDATE,
        SYSDATE,
        ORASystemDM.CurrentProgramUser
      );
    exception
      when others then WErrorLog('UpdateEmployeeLevel: ' || SQLERRM);
    end;
  end if;
end UpdateEmployeeLevel;

FUNCTION UpdateProductionHour(AProductionDate Date, AIDCard TScannedIDCard, AMinutes Integer, APayedBreaks Integer, AManual Varchar2) return Integer is
  OldProdMin Integer;
  OldPayedBreaks  Integer;
  StartDate Date;
  EndDate Date;
  Found Boolean;
  ProductionDate Date;
  Result Integer;
begin
  ProductionDate := AProductionDate;
  if PimsSetting.UseShiftDateSystem then
    if AManual = 'N' then
      if AIDCard.ShiftDate = null then
        GetShiftDay(AIDcard, StartDate, EndDate);
        ProductionDate := Trunc(StartDate);
      else
        ProductionDate := AIDCard.ShiftDate;
      end if;
    end if;
  end if;
  if ORASystemDM.ADebug = 1 then
    WDebugLog('UpdateProductionHour. Date=' || DateToStr(AProductionDate) ||
      ' Min=' || IntMin2StringTime(AMinutes, False)
      );
  end if;
  Found := False;
  begin
    SELECT
      P.PRODUCTION_MINUTE,
      P.PAYED_BREAK_MINUTE
    INTO
      OldProdMin,
      OldPayedBreaks
    FROM
      PRODHOURPEREMPLOYEE P
    WHERE
      P.PRODHOUREMPLOYEE_DATE = ProductionDate AND
      P.PLANT_CODE = AIDCard.PlantCode AND
      P.SHIFT_NUMBER = AIDCard.ShiftNumber AND
      P.EMPLOYEE_NUMBER = AIDCard.EmployeeCode AND
      P.WORKSPOT_CODE = AIDCard.WorkspotCode AND
      P.JOB_CODE = AIDCard.JobCode AND
      P.MANUAL_YN = AManual;
    if SQL%ROWCOUNT = 1 then
      Found := True;
    end if;
  exception
    when no_data_found then Found := False;
  end;
  if Found then
    if (OldProdMin + AMinutes) <= 0 then
      begin
        DELETE FROM
          PRODHOURPEREMPLOYEE P
        WHERE
          P.PRODHOUREMPLOYEE_DATE = ProductionDate AND
          P.PLANT_CODE = AIDCard.PlantCode AND
          P.SHIFT_NUMBER = AIDCard.ShiftNumber AND
          P.EMPLOYEE_NUMBER = AIDCard.EmployeeCode AND
          P.WORKSPOT_CODE = AIDCard.WorkspotCode AND
          P.JOB_CODE = AIDCard.JobCode AND
          P.MANUAL_YN = AManual;
      exception
        when no_data_found then null;
        when others then WErrorLog(SQLERRM);
      end;
      DeleteProdHourForAllTypes(AIDCard, ProductionDate,
        ProductionDate, AManual);
    else
      begin
        UPDATE
          PRODHOURPEREMPLOYEE P
        SET
          P.PRODUCTION_MINUTE = AMinutes + OldProdMin,
          P.PAYED_BREAK_MINUTE = APayedBreaks + OldPayedBreaks,
          P.MUTATIONDATE = SYSDATE,
          P.MUTATOR = ORASystemDM.CurrentProgramUser
        WHERE
          P.PRODHOUREMPLOYEE_DATE = ProductionDate AND
          P.PLANT_CODE = AIDCard.PlantCode AND
          P.SHIFT_NUMBER = AIDCard.ShiftNumber AND
          P.EMPLOYEE_NUMBER = AIDCard.EmployeeCode AND
          P.WORKSPOT_CODE = AIDCard.WorkspotCode AND
          P.JOB_CODE = AIDCard.JobCode AND
          P.MANUAL_YN = AManual;
      exception
        when no_data_found then null;
        when others then WErrorLog(SQLERRM);
      end;
    end if;
    Result := 0;
  else
    begin
      INSERT INTO PRODHOURPEREMPLOYEE(
        PRODHOUREMPLOYEE_DATE,
        PLANT_CODE,
        SHIFT_NUMBER,
        EMPLOYEE_NUMBER,
        JOB_CODE,
        WORKSPOT_CODE,
        PRODUCTION_MINUTE,
        PAYED_BREAK_MINUTE,
        MANUAL_YN,
        CREATIONDATE,
        MUTATIONDATE,
        MUTATOR
      )
      VALUES(
        ProductionDate,
        AIDCard.PlantCode,
        AIDCard.ShiftNumber,
        AIDCard.EmployeeCode,
        AIDCard.JobCode,
        AIDCard.WorkspotCode,
        AMinutes,
        APayedBreaks,
        AManual,
        SYSDATE,
        SYSDATE,
        ORASystemDM.CurrentProgramUser
      );
    exception
      when others then WErrorLog(SQLERRM);
    end;
    Result := 1;
  end if; -- if Found
  if AMinutes >= 60 then
    UpdateEmployeeLevel(AIDCard);
  end if;
  return Result;
end UpdateProductionHour;

FUNCTION UpdateSalaryHour(ASalaryDate Date, AIDCard TScannedIDCard, AHourTypeNumber Integer, AMinutes Integer, AManual Varchar2, AReplace Boolean,
                          AUpdateYes Boolean, AFromManualProd Varchar2 default 'N', ARoundMinutes Boolean default True) return Integer is
  TotMin Integer;
  InsertNewProdHrs Boolean;
  MinuteNewProdHrs Integer;
  NewMinutes Integer;
  Found Boolean;
  Result Integer;
begin
  Result := AMinutes;
  if AReplace and (AMinutes = 0) then
    return Result;
  end if;
  if ORASystemDM.ADebug = 1 then
    WDebugLog('UpdateSalaryHour. Date=' || DateToStr(ASalaryDate) ||
      ' HT=' || IntToStr(AHourTypeNumber) ||
      ' Min=' || IntMin2StringTime(AMinutes, False)
      );
  end if;
  TotMin := 0;
  InsertNewProdHrs := False;
  MinuteNewProdHrs := 0;
  Found := False;
  begin
    SELECT
      NVL(SPE.SALARY_MINUTE, 0)
    INTO
      TotMin
    FROM
      SALARYHOURPEREMPLOYEE SPE
    WHERE
      SPE.SALARY_DATE = ASalaryDate AND
      SPE.EMPLOYEE_NUMBER = AIDCard.EmployeeCode AND
      SPE.HOURTYPE_NUMBER = AHourTypeNumber AND
      SPE.MANUAL_YN = AManual;
    if SQL%ROWCOUNT = 1 then
      Found := True;
    end if;
  exception
    when no_data_found then null;
    when others then null;
  end;
  if Found then
    if ((TotMin + AMinutes) <> 0) then
      if AReplace then
        NewMinutes := AMinutes;
        Result := AMinutes - TotMin;
      else
        NewMinutes := TotMin + AMinutes;
      end if;
      begin
        UPDATE
          SALARYHOURPEREMPLOYEE SPE
        SET
          SPE.SALARY_MINUTE = NewMinutes,
          SPE.MUTATIONDATE = SYSDATE,
          SPE.MUTATOR= ORASystemDM.CurrentProgramUser
        WHERE
          SPE.SALARY_DATE = ASalaryDate AND
          SPE.EMPLOYEE_NUMBER = AIDCard.EmployeeCode AND
          SPE.HOURTYPE_NUMBER = AHourTypeNumber AND
          SPE.MANUAL_YN = AManual;
        InsertNewProdHrs := True;
        MinuteNewProdHrs := AMinutes;
      exception
        when no_data_found then null;
        when others then WErrorLog(SQLERRM);
      end;
    else
      begin
        DELETE FROM
          SALARYHOURPEREMPLOYEE SPE
        WHERE
          SPE.SALARY_DATE = ASalaryDate AND
          SPE.EMPLOYEE_NUMBER = AIDCard.EmployeeCode AND
          SPE.HOURTYPE_NUMBER = AHourTypeNumber AND
          SPE.MANUAL_YN = AManual;
        InsertNewProdHrs := False;
        MinuteNewProdHrs := 0;
      exception
        when no_data_found then null;
        when others then WErrorLog(SQLERRM);
      end;
    end if; -- ((TotMin + AMinutes) <> 0)
  else
    if AMinutes <> 0 then
      begin
        INSERT INTO SALARYHOURPEREMPLOYEE(
          SALARY_DATE,
          EMPLOYEE_NUMBER,
          HOURTYPE_NUMBER,
          SALARY_MINUTE,
          MANUAL_YN,
          CREATIONDATE,
          MUTATIONDATE,
          EXPORTED_YN,
          MUTATOR
        )
        VALUES(
          ASalaryDate,
          AIDCard.EmployeeCode,
          AHourTypeNumber,
          AMinutes,
          AManual,
          SYSDATE,
          SYSDATE,
          'N',
          ORASystemDM.CurrentProgramUser
        );
        Result := AMinutes;
        InsertNewProdHrs := True;
        MinuteNewProdHrs := AMinutes;
      exception
        when others then WErrorLog(SQLERRM);
      end;
    end if;
  end if;  -- Found
  if AUpdateYes then
    FillProdHourPerHourType(ASalaryDate, AIDCard,
      AHourTypeNumber, MinuteNewProdHrs, AManual, InsertNewProdHrs);
  else
    if not ORASystemDM.IsRFL then
      if ARoundMinutes then
        RoundMinutesPHEPT(ASalaryDate, AIDCard, AHourTypeNumber,
          AMinutes, AManual);
      end if;
    end if;
  end if;

  return Result;
end UpdateSalaryHour;

FUNCTION UpdateAbsenceTotal(AFieldName Varchar2, ABookingDay Date, AEmployeeNumber Integer, AMinutes Number, AOverwrite Boolean) return Integer is
  Year Integer;
  Month Integer;
  Day Integer;
  Found Boolean;
  TotalMinutes Number;
  ExistingMinutes Number;
  Result Integer;
  TYPE cv_absencetotal IS REF CURSOR;
  cv cv_absencetotal;
  dml_update_str varchar2(32767) :=
    'BEGIN ' ||
    '  UPDATE ABSENCETOTAL ' ||
    '  SET ' || AFieldName || ' = :AMinutes, ' ||
    '  MutationDate = SYSDATE, ' ||
    '  Mutator = :AMutator ' ||
    '  WHERE EMPLOYEE_NUMBER = :AEmployeeNumber AND ' ||
    '  ABSENCE_YEAR = :AYear; ' ||
    'END;';
  dml_insert_str varchar2(32767) :=
    'BEGIN ' ||
    '  INSERT INTO ABSENCETOTAL( ' ||
    '  EMPLOYEE_NUMBER, ' ||
    '  ABSENCE_YEAR, ' ||
    AFieldName || ', ' ||
    '  CREATIONDATE, ' ||
    '  MUTATIONDATE, ' ||
    '  MUTATOR) ' ||
    '  VALUES( ' ||
    '  :AEmployeeNumber, ' ||
    '  :Year, ' ||
    '  :AMinutes, ' ||
    '  SYSDATE, ' ||
    '  SYSDATE, ' ||
    '  :Mutator ' ||
    '  ); ' ||
    'END;';
begin
  Result := 0;
  TotalMinutes := 0;
  ExistingMinutes := 0;
  Found := False;
  DecodeDate(ABookingDay, Year, Month, Day);
  begin
    OPEN cv FOR
    'SELECT ' || AFieldName ||
    ' FROM ABSENCETOTAL WHERE EMPLOYEE_NUMBER = ' ||
    AEmployeeNumber || ' AND ABSENCE_YEAR = ' || Year;
    FETCH cv INTO ExistingMinutes;
    if cv%ROWCOUNT = 1 then
      Found := True;
    end if;
  exception
    when others then null;
  end;
  close cv;
  if Found then
    if AOverwrite then
      TotalMinutes := AMinutes;
    else
      TotalMinutes := ExistingMinutes + AMinutes;
    end if;
    if TotalMinutes <> ExistingMinutes then
      begin
        EXECUTE IMMEDIATE dml_update_str
        USING AMinutes, ORASystemDM.CurrentProgramUser, AEmployeeNumber, Year;
      exception
        when others then WErrorLog(SQLERRM);
      end;
    end if;
  else
    if AMinutes <> 0 then
      begin
        EXECUTE IMMEDIATE dml_insert_str
        USING AEmployeeNumber, Year, AMinutes, ORASystemDM.CurrentProgramUser;
      exception
        when others then WErrorLog(SQLERRM);
      end;
      Result := 1;
    end if;
  end if;
  return Result;
end UpdateAbsenceTotal;

FUNCTION DayInWeek(ACurrentDate Date) return Integer is
begin
  Return PimsDayOfWeek(ACurrentDate);
end DayInWeek;

FUNCTION FindPlanningBlocks(AIDCard TScannedIDCard, FirstPlanTimeblock in out Integer, LastPlanTimeblock in out Integer) return Boolean is
  DayOfWeek Integer;
  Result Boolean;
  Type CV_Planning is ref cursor;
  CV CV_Planning;
  Type TCV_Rec is record(
    ScheduledTimeBlock1 Varchar2(1),
    ScheduledTimeBlock2 Varchar2(1),
    ScheduledTimeBlock3 Varchar2(1),
    ScheduledTimeBlock4 Varchar2(1)
  );
  Type TTimeblock is Varray(4) of Varchar2(1);
  Timeblock TTimeblock := TTimeBlock();
  CV_Rec TCV_Rec;
  WorkSelect Varchar2(32000);
  EmployeePlanningDate Date;
begin
  Result := False;
  Timeblock.extend(4);
  DayOfWeek := DayInWeek(Trunc(AIDCard.DateIn));
  EmployeePlanningDate := Trunc(AIDCard.DateIn);
  WorkSelect :=
    'SELECT ' ||
    '  ET.SCHEDULED_TIMEBLOCK_1, ' ||
    '  ET.SCHEDULED_TIMEBLOCK_2, ' ||
    '  ET.SCHEDULED_TIMEBLOCK_3, ' ||
    '  ET.SCHEDULED_TIMEBLOCK_4 ' ||
    'FROM ' ||
    '  EMPLOYEEPLANNING ET ' ||
    'WHERE ' ||
    '  ET.EMPLOYEEPLANNING_DATE = ' || '''' || TO_CHAR(EmployeePlanningDate) || '''' || ' AND ' ||
    '  ET.PLANT_CODE = ' || '''' || AIDCard.PlantCode || '''' || ' AND ' ||
    '  ET.WORKSPOT_CODE = ' || '''' || AIDCard.WorkspotCode || '''' || ' AND ' ||
    '  ET.EMPLOYEE_NUMBER = ' || AIDCard.EmployeeCode || ' AND ' ||
    '  ET.SHIFT_NUMBER = :SHIFT_NUMBER AND ' ||
    '  ( ' ||
    '    ET.SCHEDULED_TIMEBLOCK_1 IN (''A'', ''B'', ''C'') OR ' ||
    '    ET.SCHEDULED_TIMEBLOCK_2 IN (''A'', ''B'', ''C'') OR ' ||
    '    ET.SCHEDULED_TIMEBLOCK_3 IN (''A'', ''B'', ''C'') OR ' ||
    '    ET.SCHEDULED_TIMEBLOCK_4 IN (''A'', ''B'', ''C'') ' ||
    '  ) ' ||
    'UNION ALL ' ||
    'SELECT ' ||
    '  SP.SCHEDULED_TIMEBLOCK_1, ' ||
    '  SP.SCHEDULED_TIMEBLOCK_2, ' ||
    '  SP.SCHEDULED_TIMEBLOCK_3, ' ||
    '  SP.SCHEDULED_TIMEBLOCK_4 ' ||
    'FROM ' ||
    '  STANDARDEMPLOYEEPLANNING  SP ' ||
    'WHERE ' ||
    '  SP.DAY_OF_WEEK = ' || DayOfWeek || ' AND ' ||
    '  SP.PLANT_CODE = ' || '''' || AIDCard.PlantCode || '''' || ' AND ' ||
    '  SP.WORKSPOT_CODE = ' || '''' || AIDCard.WorkspotCode || '''' || ' AND ' ||
    '  SP.EMPLOYEE_NUMBER = ' || AIDCard.EmployeeCode || ' AND ' ||
    '  SP.SHIFT_NUMBER = ' || AIDCard.ShiftNumber || ' AND ' ||
    '  ( ' ||
    '    SP.SCHEDULED_TIMEBLOCK_1 IN (''A'',''B'',''C'') OR ' ||
    '    SP.SCHEDULED_TIMEBLOCK_2 IN (''A'',''B'',''C'') OR ' ||
    '    SP.SCHEDULED_TIMEBLOCK_3 IN (''A'',''B'',''C'') OR ' ||
    '    SP.SCHEDULED_TIMEBLOCK_4 IN (''A'',''B'',''C'') ' ||
    '  ) ' ||
    'UNION ALL ' ||
    'SELECT ' ||
    '  EA.AVAILABLE_TIMEBLOCK_1, ' ||
    '  EA.AVAILABLE_TIMEBLOCK_2, ' ||
    '  EA.AVAILABLE_TIMEBLOCK_3, ' ||
    '  EA.AVAILABLE_TIMEBLOCK_4 ' ||
    'FROM ' ||
    '  EMPLOYEEAVAILABILITY EA ' ||
    'WHERE ' ||
    '  EA.EMPLOYEE_NUMBER = ' || AIDCard.EmployeeCode || ' AND ' ||
    '  EA.PLANT_CODE = ' || '''' || AIDCard.PlantCode || '''' || ' AND ' ||
    '  EA.EMPLOYEEAVAILABILITY_DATE = ' || '''' || TO_CHAR(EmployeePlanningDate) || '''' || ' AND ' ||
    '  EA.SHIFT_NUMBER = ' || AIDCard.ShiftNumber || ' AND ' ||
    '  ( ' ||
    '    EA.AVAILABLE_TIMEBLOCK_1 = ''*'' OR ' ||
    '    EA.AVAILABLE_TIMEBLOCK_2 = ''*'' OR ' ||
    '    EA.AVAILABLE_TIMEBLOCK_3 = ''*'' OR ' ||
    '    EA.AVAILABLE_TIMEBLOCK_4 = ''*'' ' ||
    '  ) ' ||
    'UNION ALL ' ||
    'SELECT ' ||
    '  SA.AVAILABLE_TIMEBLOCK_1, ' ||
    '  SA.AVAILABLE_TIMEBLOCK_2, ' ||
    '  SA.AVAILABLE_TIMEBLOCK_3, ' ||
    '  SA.AVAILABLE_TIMEBLOCK_4 ' ||
    'FROM ' ||
    '  STANDARDAVAILABILITY SA ' ||
    'WHERE ' ||
    '  SA.EMPLOYEE_NUMBER = ' || AIDCard.EmployeeCode || ' AND ' ||
    '  SA.PLANT_CODE = ' || '''' || AIDCard.PlantCode || '''' || ' AND ' ||
    '  SA.DAY_OF_WEEK = ' || DayOfWeek || ' AND ' ||
    '  SA.SHIFT_NUMBER = ' || AIDCard.ShiftNumber || ' AND ' ||
    '  ( ' ||
    '    SA.AVAILABLE_TIMEBLOCK_1 = ''*'' OR ' ||
    '    SA.AVAILABLE_TIMEBLOCK_2 = ''*'' OR ' ||
    '    SA.AVAILABLE_TIMEBLOCK_3 = ''*'' OR ' ||
    '    SA.AVAILABLE_TIMEBLOCK_4 = ''*'' ' ||
    '  ) ';
  begin
    open CV for WorkSelect;
    fetch CV into CV_Rec;
    if CV%ROWCOUNT >= 1 then
      Result := True;
    end if;
  exception
    when no_data_found then null;
    when others then null;
  end;
  close CV;
  if Result then
    Timeblock(1) := CV_Rec.ScheduledTimeBlock1;
    Timeblock(2) := CV_Rec.ScheduledTimeBlock2;
    Timeblock(3) := CV_Rec.ScheduledTimeBlock3;
    Timeblock(4) := CV_Rec.ScheduledTimeBlock4;
    FirstPlanTimeBlock := 1;
    LastPlanTimeBlock := 4;
    -- What is the first planned/available timeblock?
    for TBNumber in 1..4
    loop
      if INSTR('ABC*', Timeblock(TBNumber)) > 0 then
        FirstPlanTimeblock := TBNumber;
        exit;
      end if;
    end loop;
    -- What is the last planned/available timeblock?
    for TBNumber in reverse 1..4
    loop
      if INSTR('ABC*', Timeblock(TBNumber)) > 0 then
        LastPlanTimeblock := TBNumber;
        exit;
      end if;
    end loop;
  end if;
end FindPlanningBlocks;

PROCEDURE EmployeeCutOfTime(AFirstScan Boolean, ALastScan Boolean, AIDCard in out TScannedIDCard, ADeleteAction Boolean,
                            ADeleteIDCard TScannedIDCard, AStartDTInterval in out Date, AEndDTInterval in out Date)
as
  RequestDate Date;
  DateTimeIn Date;
  DateTimeOut Date;
  EarlyTime Date;
  LateTime Date;
  DateFrom Date;
  DateTo Date;
  CutOffStart Boolean;
  CutOffEnd Boolean;
  IsEmpty Boolean;
  RoundTruncSalaryHours Integer;
  RoundMinute Integer;
  function SearchPrevScan(ADateTimeIn in out Date) return Boolean is
    IsEmpty Boolean;
    Ready Boolean;
  begin
    Ready := False;
    loop
      IsEmpty := False;
      if ADeleteAction then
        begin
          SELECT DATETIME_IN
          INTO ADateTimeIn
          FROM TIMEREGSCANNING
          WHERE EMPLOYEE_NUMBER = AIDCard.EmployeeCode AND
          ROUND(DATETIME_OUT, 'MI') = ROUND(ADateTimeIn, 'MI') AND
          ROUND(DATETIME_OUT, 'MI') <> ROUND(ADeleteIDCard.Dateout, 'MI');
        exception
          when no_data_found then IsEmpty := True;
        end;
      else
        begin
          SELECT DATETIME_IN
          INTO ADateTimeIn
          FROM TIMEREGSCANNING
          WHERE EMPLOYEE_NUMBER = AIDCard.EmployeeCode AND
          ROUND(DATETIME_OUT, 'MI') = ROUND(ADateTimeIn, 'MI');
        exception
          when no_data_found then IsEmpty := True;
        end;
      end if;
      if not IsEmpty then
        Ready := (ADateTimeIn <
          (AStartDTInterval - AIDCard.InscanEarly / DayToMin));
        if AIDCard.DateIn > AStartDTInterval then
          Ready := True;
        end if;
      end if;
      EXIT when IsEmpty or Ready;
    end loop;
    return IsEmpty;
  end SearchPrevScan;
  function SearchNextScan(ADateTimeOut in out Date) return Boolean is
    IsEmpty Boolean;
    Ready Boolean;
  begin
    loop
      IsEmpty := False;
      if ADeleteAction then
        begin
          SELECT DATETIME_OUT
          INTO ADateTimeOut
          FROM TIMEREGSCANNING
          WHERE EMPLOYEE_NUMBER = AIDCard.EmployeeCode AND
          ROUND(DATETIME_IN, 'MI') = ROUND(ADateTimeOut, 'MI') AND
          ROUND(DATETIME_IN, 'MI') <> ROUND(AIDCard.DateIn, 'MI');
        exception
          when no_data_found then IsEmpty := True;
        end;
      else
        begin
          SELECT DATETIME_OUT
          INTO ADateTimeOut
          FROM TIMEREGSCANNING
          WHERE EMPLOYEE_NUMBER = AIDCard.EmployeeCode AND
          ROUND(DATETIME_IN, 'MI') = ROUND(ADateTimeOut, 'MI');
        exception
          when no_data_found then IsEmpty := True;
        end;
      end if;
      if not IsEmpty then
        Ready := (ADateTimeOut >
          (AEndDTInterval + AIDCard.OutScanLate / DayToMin));
        if AIDCard.DateOut < AEndDTInterval then
          Ready := True;
        end if;
      end if;
      EXIT when IsEmpty or Ready;
    end loop;
    return IsEmpty;
  end SearchNextScan;
begin
  CutOffStart := False;
  CutOffEnd := False;
  if AIDCard.CutOfTime = 'Y' then
    RequestDate := AIDCard.DateIn;
    DateFrom := Trunc(RequestDate);
    DateTo := Trunc(RequestDate) + 1 - 1/86400; -- 1 second before the next day
    begin
      SELECT
        R.REQUESTED_EARLY_TIME,
        R.REQUESTED_LATE_TIME
      INTO
        EarlyTime,
        LateTime
      FROM
        REQUESTEARLYLATE R
      WHERE
        (
        R.REQUEST_DATE >= DateFrom AND
        R.REQUEST_DATE <= DateTo
        )  AND
        R.EMPLOYEE_NUMBER = AIDCard.EmployeeCode
      ORDER BY
        R.REQUESTED_EARLY_TIME,
        R.REQUESTED_LATE_TIME;
       if SQL%ROWCOUNT = 1 then
         AStartDTInterval := TruncFrac(AStartDTInterval, EarlyTime);
         AEndDTInterval := TruncFrac(AEndDTInterval, LateTime);
       end if;
    exception
      when no_data_found then null;
    end;
    DateTimeIn := AIDCard.DateIn;
    if AFirstScan and
      (DateTimeIn >=
        (AStartDTInterval - AIDCard.InscanEarly / DayToMin)) and
      (DateTimeIn <=
        (AStartDTInterval + AIDCard.InscanLate / DayToMin)) then
      IsEmpty := SearchPrevScan(DateTimeIn);
    else
      IsEmpty := SearchPrevScan(DateTimeIn);
    end if;
    if IsEmpty then
      CutOffStart := True;
    end if;
    DateTimeOut := AIDCard.DateOut;
    if ALastScan and
      (DateTimeOut >=
        (AEndDTInterval - AIDCard.OutScanEarly / DayToMin)) and
      (DateTimeOut <=
        (AEndDTInterval + AIDCard.OutScanLate / DayToMin)) then
      IsEmpty := SearchNextScan(DateTimeout);
    else
      IsEmpty := SearchNextScan(DateTimeout);
    end if;
    if IsEmpty then
      CutOffEnd := True;
    end if;
  end if; -- if AIDCard.CutOfTime = 'Y';
  if CutOffStart then
    AStartDTInterval := DateInInterval(AStartDTInterval, AIDCard.DateIn,
      AIDCard.InscanEarly, AIDCard.InscanLate);
  else
    AStartDTInterval := AIDCard.DateIn;
  end if;
  if CutOffEnd then
    AEndDTInterval := DateInInterval(AEndDTInterval, AIDCard.DateOut,
      AIDCard.OutscanEarly, AIDCard.OutscanLate);
  else
    AEndDTInterval := AIDCard.DateOut;
  end if;
  if AStartDTInterval > AEndDTInterval then
    AEndDTInterval := AStartDTInterval;
  end if;
  AIDCard.DateInCutOff := AStartDTInterval;
  AIDCard.DateOutCutOff := AEndDTInterval;
end EmployeeCutOfTime;

PROCEDURE ComputeBreaks(AIDCard TScannedIDCard, ASTime Date, AETime Date, AOperationMode Integer,
                        AProdMin in out Integer, ABreaksMin in out Integer, APayedBreaks in out Integer)
as
  WeekDay Integer;
  Ready Boolean;

  CURSOR C_BPE IS
    SELECT PLANT_CODE, EMPLOYEE_NUMBER, SHIFT_NUMBER, BREAK_NUMBER, PAYED_YN,
           STARTTIME1, STARTTIME2, STARTTIME3, STARTTIME4, STARTTIME5, STARTTIME6, STARTTIME7,
           ENDTIME1, ENDTIME2, ENDTIME3, ENDTIME4, ENDTIME5, ENDTIME6, ENDTIME7
    FROM BREAKPEREMPLOYEE
    WHERE PLANT_CODE = AIDCard.PlantCode AND
          EMPLOYEE_NUMBER = AIDCard.EmployeeCode AND
          SHIFT_NUMBER = AIDCard.ShiftNumber
    ORDER BY PLANT_CODE, EMPLOYEE_NUMBER, SHIFT_NUMBER, BREAK_NUMBER;

  CURSOR C_BPD IS
    SELECT PLANT_CODE, DEPARTMENT_CODE, SHIFT_NUMBER, BREAK_NUMBER, PAYED_YN,
           STARTTIME1, STARTTIME2, STARTTIME3, STARTTIME4, STARTTIME5, STARTTIME6, STARTTIME7,
           ENDTIME1, ENDTIME2, ENDTIME3, ENDTIME4, ENDTIME5, ENDTIME6, ENDTIME7
    FROM BREAKPERDEPARTMENT
    WHERE PLANT_CODE = AIDCard.PlantCode AND
          DEPARTMENT_CODE = AIDCard.DepartmentCode AND
          SHIFT_NUMBER = AIDCard.ShiftNumber
    ORDER BY PLANT_CODE, DEPARTMENT_CODE, SHIFT_NUMBER, BREAK_NUMBER;

  CURSOR C_BPS IS
    SELECT PLANT_CODE, SHIFT_NUMBER, BREAK_NUMBER, PAYED_YN,
           STARTTIME1, STARTTIME2, STARTTIME3, STARTTIME4, STARTTIME5, STARTTIME6, STARTTIME7,
           ENDTIME1, ENDTIME2, ENDTIME3, ENDTIME4, ENDTIME5, ENDTIME6, ENDTIME7
    FROM BREAKPERSHIFT
    WHERE PLANT_CODE = AIDCard.PlantCode AND
          SHIFT_NUMBER = AIDCard.ShiftNumber
    ORDER BY PLANT_CODE, SHIFT_NUMBER, BREAK_NUMBER;

  procedure SearchTimeBlock(AStartTime1 Date, AEndTime1 Date, AStartTime2 Date, AEndTime2 Date,
    AStartTime3 Date, AEndTime3 Date, AStartTime4 Date, AEndTime4 Date,
    AStartTime5 Date, AEndTime5 Date, AStartTime6 Date, AEndTime6 Date,
    AStartTime7 Date, AEndTime7 Date, APayedYN Varchar2)
  as
    StartDateTime Date;
    EndDateTime Date;
    StartTime Date;
    EndTime Date;
    MinOfBreak Integer;
  begin
    case WeekDay
    when 1 then begin StartDateTime := AStartTime1; EndDateTime := AEndTime1; end;
    when 2 then begin StartDateTime := AStartTime2; EndDateTime := AEndTime2; end;
    when 3 then begin StartDateTime := AStartTime3; EndDateTime := AEndTime3; end;
    when 4 then begin StartDateTime := AStartTime4; EndDateTime := AEndTime4; end;
    when 5 then begin StartDateTime := AStartTime5; EndDateTime := AEndTime5; end;
    when 6 then begin StartDateTime := AStartTime6; EndDateTime := AEndTime6; end;
    when 7 then begin StartDateTime := AStartTime7; EndDateTime := AEndTime7; end;
    end case;
    StartTime := TruncFrac(ASTime, StartDateTime);
    EndTime := TruncFrac(AETime, EndDateTime);
    if StartDateTime > EndDateTime then
      EndTime := EndTime + 1; -- ends in next day
    end if;
    MinOfBreak :=
      ComputeMinutesOfIntersection(ASTime, AETime, StartTime, EndTime);
    ABreaksMin := ABreaksMin + MinOfBreak;
    if APayedYN = 'Y' then
      APayedBreaks := APayedBreaks + MinOfBreak;
    end if;
    if AOperationMode = 1 then
      EndTime := EndTime + MinOfBreak / DayToMin;
    end if;
  end SearchTimeBlock;
begin
  WeekDay := DayInWeek(ASTime);
  AProdMin := 0;
  ABreaksMin := 0;
  APayedBreaks := 0;

  -- Determine breaks here
  Ready := False;
  for R_BPE in C_BPE loop
    SearchTimeBlock(R_BPE.STARTTIME1, R_BPE.ENDTIME1, R_BPE.STARTTIME2, R_BPE.ENDTIME2,
      R_BPE.STARTTIME3, R_BPE.ENDTIME3, R_BPE.STARTTIME4, R_BPE.ENDTIME4,
      R_BPE.STARTTIME5, R_BPE.ENDTIME5, R_BPE.STARTTIME6, R_BPE.ENDTIME6,
      R_BPE.STARTTIME7, R_BPE.ENDTIME7, R_BPE.PAYED_YN);
    Ready := True;
  end loop;

  if not Ready then
    for R_BPD in C_BPD loop
      SearchTimeBlock(R_BPD.STARTTIME1, R_BPD.ENDTIME1, R_BPD.STARTTIME2, R_BPD.ENDTIME2,
        R_BPD.STARTTIME3, R_BPD.ENDTIME3, R_BPD.STARTTIME4, R_BPD.ENDTIME4,
        R_BPD.STARTTIME5, R_BPD.ENDTIME5, R_BPD.STARTTIME6, R_BPD.ENDTIME6,
        R_BPD.STARTTIME7, R_BPD.ENDTIME7, R_BPD.PAYED_YN);
      Ready := True;
    end loop;
  end if;

  if not Ready then
    for R_BPS in C_BPS loop
      SearchTimeBlock(R_BPS.STARTTIME1, R_BPS.ENDTIME1, R_BPS.STARTTIME2, R_BPS.ENDTIME2,
        R_BPS.STARTTIME3, R_BPS.ENDTIME3, R_BPS.STARTTIME4, R_BPS.ENDTIME4,
        R_BPS.STARTTIME5, R_BPS.ENDTIME5, R_BPS.STARTTIME6, R_BPS.ENDTIME6,
        R_BPS.STARTTIME7, R_BPS.ENDTIME7, R_BPS.PAYED_YN);
      Ready := True;
    end loop;
  end if;

  AProdMin := Round((AETime - ASTime) * DayToMin - ABreaksMin);
end ComputeBreaks;

PROCEDURE GetShiftWStartEnd(AIDCard TScannedIDCard, AStartDateTime in out Date, AEndDateTime in out Date, AReady in out Boolean)
as
  MyIndex Integer;
  FirstPlanTimeblock Integer;
  LastPlanTimeblock Integer;
  PlanningBlocksFilter Varchar2(1024);
  ShiftDayRecord TShiftDayRecord;
begin
  AReady := False;

  PlanningBlocksFilter := null;
  if ORASystemDM.IsLTB then
    if FindPlanningBlocks(AIDCard, FirstPlanTimeblock,
      LastPlanTimeblock) then
      PlanningBlocksFilter := ' AND ' ||
        '(' ||
        '  TIMEBLOCK_NUMBER = ' || FirstPlanTimeBlock ||
        '  OR TIMEBLOCK_NUMBER = ' || LastPlanTimeblock ||
        ')';
    end if;
  end if;

  ShiftDayRecord.APlantCode := AIDCard.PlantCode;
  ShiftDayRecord.AShiftNumber := AIDCard.ShiftNumber;
  ShiftDayRecord.ADate := AIDCard.ShiftDate;
  ShiftDayRecord.AValid := False;
  DetermineShiftStartEnd(AIDCard.EmployeeCode, AIDCard.DepartmentCode, ShiftDayRecord, PlanningBlocksFilter);
  AStartDateTime := ShiftDayRecord.AStart;
  AEndDateTime := ShiftDayRecord.AEnd;
  AReady := ShiftDayRecord.AValid;

end GetShiftWStartEnd;

PROCEDURE GetShiftStartEnd(AIDCard TScannedIDCard, AShiftDate Date, AStartDateTime in out Date, AEndDateTime in out Date)
as
  Current_Day Integer;
  StartDateTime Date;
  EndDateTime Date;
  Ready Boolean;
begin
  Ready := False;
  AStartDateTime := AIDCard.DateIn;
  AEndDateTime := AIDCard.DateIn;
  Current_Day := DayInWeek(AShiftDate);

  GetShiftWStartEnd(AIDCard, StartDateTime, EndDateTime, Ready);
  -- ReplaceDate
  if Ready then
    AStartDateTime := Trunc(AShiftDate) + (StartDateTime - Trunc(StartDateTime));
    AEndDateTime := Trunc(AShiftDate) + (EndDateTime - Trunc(EndDateTime));
  end if;
end GetShiftStartEnd;

PROCEDURE GetProdMin(AIDCard in out TScannedIDCard, AFirstScan Boolean, ALastScan Boolean,
                     ADeleteAction Boolean, ADeleteIDCard TScannedIDCard, ABookingDay in out Date,
                     AMinutes in out Integer, ABreaksMin in out Integer, APayedBreaks in out Integer)
as
  StartDate Date;
  EndDate Date;
begin
  GetShiftDay(AIDCard, StartDate, EndDate);
  ABookingDay := Trunc(StartDate);

  GetShiftStartEnd(AIDCard, StartDate, StartDate, EndDate);

  EmployeeCutOfTime(
    AFirstScan, ALastScan, AIDCard,
    ADeleteAction, ADeleteIDCard,
    StartDate, EndDate);

  ComputeBreaks(AIDCard, StartDate, EndDate, 0,
    AMinutes, ABreaksMin, APayedBreaks);
end GetProdMin;

PROCEDURE UpdateTimeRegScanning(AIDCard TScannedIDCard, AShiftDate Date)
as
begin
  begin
    UPDATE TIMEREGSCANNING T
    SET
      T.SHIFT_DATE = AShiftDate,
      T.MUTATIONDATE = SYSDATE,
      T.MUTATOR = ORASystemDM.CurrentProgramUser
    WHERE
      T.DATETIME_IN = AIDCard.DateInOriginal AND
      T.EMPLOYEE_NUMBER = AIDCard.EmployeeCode;
  exception
    when others then null;
  end;
end UpdateTimeRegScanning;

PROCEDURE ProcessTimeRecording(AIDCard in out TScannedIDCard, ADeleteIDCard TScannedIDCard, AFirstScan Boolean, ALastScan Boolean,
                               AUpdateSalMin Boolean, AUpdateProdMinOut Boolean, AUpdateProdMinIn Boolean, ADeleteAction Boolean,
                               ALastScanRecordForDay Boolean,
                               ACurrentProgramUser Varchar2, ACurrentComputerName Varchar2, ADebug Integer)
as
  BookingDay Date;
  SaveDate_Out Date;
  SaveDate_In Date;
  SaveDate_In_CutOff Date;
  ProdMin Integer;
  BreaksMin Integer;
  PayedBreaks Integer;
  PayedBreaksTot Integer;
  ProdMinTot Integer;
  IsOvernight Boolean;
  StartDate Date;
  EndDate Date;
  DateIn Date;
  DateOut Date;
  Dummy Integer;
  FirstScan Boolean;
  ProdHrsBreaksMin Integer; -- PIM-319
begin
  GetPimsSettings;
  ORASystemDM.CurrentProgramUser := ACurrentProgramUser;
  ORASystemDM.CurrentComputerName := ACurrentComputerName;
  ORASystemDM.ADebug := ADebug;

  FirstScan := AFirstScan;
  if PimsSetting.UseShiftDateSystem then
    GetShiftDay(AIDCard, StartDate, EndDate);
  else
    AIDCard.ShiftDate := Trunc(AIDCard.DateIn);
  end if;

  ProdMinTot := 0;
  PayedBreaksTot := 0;
  SaveDate_In_CutOff := null;
  SaveDate_Out := AIDCard.DateOut;
  SaveDate_In := AIDCard.DateIn;
  IsOvernight := Trunc(AIDCard.DateOut) > Trunc(AIDCard.DateIn);
  if IsOvernight then
    AIDCard.DateOut := Trunc(AIDCard.DateOut);
    GetProdMin(AIDCard, FirstScan, False,
      ADeleteAction, ADeleteIDCard,
      BookingDay, ProdMin, BreaksMin, PayedBreaks);
    SaveDate_In_CutOff := AIDCard.DateInCutOff;
    ProdMinTot := ProdMinTot + ProdMin;
    PayedBreaksTot := PayedBreaksTot + PayedBreaks;
    if not PimsSetting.UseShiftDateSystem then
      DateIn := Trunc(AIDCard.DateIn);
    else
      DateIn := AIDCard.ShiftDate;
    end if;
    if PimsSetting.ProdHrsDoNotSubtractBreaks then -- PIM-319
      ProdHrsBreaksMin := BreaksMin;
    else
      ProdHrsBreaksMin := 0;
    end if;
    if ((ProdMin + ProdHrsBreaksMin) > 0) and AUpdateProdMinIn then -- PIM-319
      Dummy := UpdateProductionHour(
        Trunc(DateIn),
        AIDCard,
        ProdMin + ProdHrsBreaksMin, PayedBreaks, 'N'); -- PIM-319
    end if;
    AIDCard.DateIn := AIDCard.DateOut;
    AIDCard.DateOut := SaveDate_Out;
  end if; -- IsOverNight
  if IsOverNight then
    FirstScan := False;
  end if;
  GetProdMin(
    AIDCard, FirstScan, ALastScan, ADeleteAction, ADeleteIDCard,
    BookingDay, ProdMin, BreaksMin, PayedBreaks);
  if IsOverNight then
    AIDCard.DateInCutOff := SaveDate_In_CutOff;
  end if;
  ProdMinTot := ProdMinTot + ProdMin;
  PayedBreaksTot := PayedBreaksTot + PayedBreaks;
  if not PimsSetting.UseShiftDateSystem then
    DateOut := Trunc(AIDCard.DateOut);
  else
    DateOut := AIDcard.ShiftDate;
  end if;
  if PimsSetting.ProdHrsDoNotSubtractBreaks then -- PIM-319
    ProdHrsBreaksMin := BreaksMin;
  else
    ProdHrsBreaksMin := 0;
  end if;
  if ((ProdMin + ProdHrsBreaksMin) > 0) and AUpdateProdMinOut then -- PIM-319
    Dummy := UpdateProductionHour(
      Trunc(DateOut),
      AIDCard,
      ProdMin + ProdHrsBreaksMin, PayedBreaks, 'N'); -- PIM-319
  end if;
  AIDcard.DateIn := SaveDate_In;
  AIDCard.DateOut := SaveDate_Out;
  if IsOverNight then
    GetShiftDay(AIDCard, SaveDate_In, SaveDate_Out);
    BookingDay := Trunc(SaveDate_In);
  end if;
  if AUpdateSalMin then
    if PimsSetting.UseShiftDateSystem then
      BookingDay := AIDcard.ShiftDate;
    end if;
    ProcessBookings(
      BookingDay,
      AIDCard,
      AUpdateProdMinOut or AUpdateProdMinIn, False,
      ProdMinTot, PayedBreaksTot, 'N');
    -- Note: AContractRoundMinutes must be set!!!
    -- Check on AIDCard.RoundMinute instead on AContractRoundMinute
    if AIDCard.RoundMinute > 1 and ALastScanRecordForDay then
      Dummy := ContractRoundMinutes(AIDcard, BookingDay, -1);
    end if;
  end if;
  commit;
end ProcessTimeRecording;

PROCEDURE UnprocessProcessScans(AIDCard TScannedIDCard,
  AFromDate Date, AProdDate1 in out Date, AProdDate2 in out Date, AProdDate3 in out Date, AProdDate4 in out Date,
  AProcessSalary Boolean, ADeleteAction Boolean,
  ARecalcProdHours in out Boolean,
  AFromProcessPlannedAbsence Boolean,
  AManual Boolean,
  ACurrentProgramUser Varchar2,
  ACurrentComputerName Varchar2,
  ADebug Integer)
as
  StartDate Date;
  EndDate Date;
  Type TProdDate IS VARRAY(4) OF Date;
  ProdDate TProdDate := TProdDate();
  CurrentProdDate1 Date;
  CurrentProdDate2 Date;
  CurrentProdDate3 Date;
  CurrentProdDate4 Date;
  GoOn Boolean;
  AddToProdListOut Boolean;
  AddToProdListIn Boolean;
  AddToSalList Boolean;
  CurrentIDCard TScannedIDCard;
  Scan TScan;
  CurrentStart Date;
  CurrentEnd Date;
  CurrentTFT Boolean;
  CurrentBonusInMoney Boolean;
  CurrentTFT_YN Varchar2(1);
  CurrentBonusInMoney_YN Varchar2(1);
  CurrentBookingDay Date;
  LastScanRecordForDay Boolean;
  BookingDayNext Date;
  BookingDayCurrent Date;
  Counter Integer;
  WhereSelect Varchar2(32000);
  WorkSelect Varchar2(32000);
  Type CV_Scans is ref cursor;
  CV CV_Scans;
  SalDateFound Boolean;
  ProdDateFound Boolean;
  Type TCV_Rec is record(
    DatetimeIn Date,
    EmployeeNumber Number,
    PlantCode Varchar2(6),
    WorkspotCode Varchar2(6),
    JobCode Varchar2(6),
    ShiftNumber Number,
    DateTimeOut Date,
    ProcessedYN Varchar2(1),
    IDCardIn Varchar2(15),
    IDCardOut Varchar2(15),
    DepartmentCode Varchar2(6),
    ShiftDate Date,
    ShiftHourtypeNumber Number,
    ShiftIgnoreForOvertimeYN Varchar2(1),
    WorkspotHourtypeNumber Number,
    IgnoreForOvertimeYN Varchar2(1)
  );
  CV_Rec TCV_Rec;
  Type TBookmarkRecord is record(
    CV_Rec TCV_Rec,
    AddToSalList Boolean, AddToProdListOut Boolean, AddToProdListIn Boolean,
    BookingDay Date
  );
  Type TRecordsForUpdate is Table of TBookmarkRecord;
  RecordsForUpdate TRecordsForUpdate := TRecordsForUpdate();
  BookmarkRecord TBookmarkRecord;
  procedure GetTFT_Bonus(AIDCard TScannedIDCard)
  as
  begin
    CurrentTFT          := False;
    CurrentBonusInMoney := False;
    begin
      SELECT CG.TIME_FOR_TIME_YN, CG.BONUS_IN_MONEY_YN
      INTO CurrentTFT_YN, CurrentBonusInMoney_YN
      FROM
        EMPLOYEE E, CONTRACTGROUP CG
      WHERE
        E.EMPLOYEE_NUMBER = AIDCard.EmployeeCode AND
        E.CONTRACTGROUP_CODE = CG.CONTRACTGROUP_CODE;
    if SQL%ROWCOUNT = 1 then
      CurrentTFT := CurrentTFT_YN = 'Y';
      CurrentBonusInMoney := CurrentBonusInMoney_YN = 'Y';
    end if;
    exception
      when others then null;
    end;
  end GetTFT_Bonus;

  procedure DeleteAbsenceHours(AIDCard TScannedIDCard, AStartDate Date, AEndDate Date)
  as
    TotEarnedMin Number;
    Result Integer;
    CURSOR C_DAH IS
      SELECT
        H.HOURTYPE_NUMBER,
        SPE.SALARY_MINUTE,
        SPE.SALARY_DATE,
        H.OVERTIME_YN, H.TIME_FOR_TIME_YN, H.BONUS_IN_MONEY_YN,
        H.BONUS_PERCENTAGE, H.ADV_YN
      FROM
        SALARYHOURPEREMPLOYEE SPE INNER JOIN HOURTYPE H ON
          SPE.HOURTYPE_NUMBER = H.HOURTYPE_NUMBER
      WHERE
        SPE.SALARY_DATE >= AStartDate AND
        SPE.SALARY_DATE <= AEndDate AND
        SPE.EMPLOYEE_NUMBER = AIDCard.EmployeeCode AND
        SPE.MANUAL_YN = 'N'  AND
        SPE.HOURTYPE_NUMBER <> 1;
    procedure DetermineTimeForTimeSettings(AOvertimeYN Varchar2, ATimeForTimeYN Varchar2, ABonusInMoneyYN Varchar2)
    as
    begin
      if not GlobalDM.ContractGroupTFTExists then
        if AOvertimeYN = 'Y' then
          CurrentTFT := ATimeForTimeYN = 'Y';
          CurrentBonusInMoney := ABonusInMoneyYN = 'Y';
        else
          CurrentTFT := False;
          CurrentBonusInMoney := False;
        end if;
      end if;
    end DetermineTimeForTimeSettings;
  begin
    TotEarnedMin := 0;
    for R_DAH in C_DAH loop
      DetermineTimeForTimeSettings(R_DAH.OVERTIME_YN, R_DAH.TIME_FOR_TIME_YN, R_DAH.BONUS_IN_MONEY_YN);
      if CurrentTFT then
        TotEarnedMin := R_DAH.SALARY_MINUTE;
        if (not CurrentBonusInMoney) then
          TotEarnedMin := Round(TotEarnedMin +
            TotEarnedMin * R_DAH.BONUS_PERCENTAGE / 100);
        end if;
        if TotEarnedMin > 0 then
          Result := UpdateAbsenceTotal('EARNED_TFT_MINUTE', R_DAH.SALARY_DATE,
            AIDCard.EmployeeCode, (-1) * TotEarnedMin,
              False);
        end if;
      end if;
      if (R_DAH.OVERTIME_YN = 'Y') and
        (R_DAH.ADV_YN = 'Y') then
        TotEarnedMin := R_DAH.SALARY_MINUTE;
        if (not CurrentBonusInMoney) then
          TotEarnedMin := Round(TotEarnedMin +
            TotEarnedMin * R_DAH.BONUS_PERCENTAGE / 100);
        end if;
        if TotEarnedMin > 0 then
          Result := UpdateAbsenceTotal('EARNED_SHORTWWEEK_MINUTE', R_DAH.SALARY_DATE,
            AIDCard.EmployeeCode, (-1) * TotEarnedMin,
              False);
        end if;
      end if;
    end loop;
  end DeleteAbsenceHours;

  procedure DeleteProdHourForAllTypes(AIDCard TScannedIDCard, AStartDate Date, AEndDate Date, AManual Varchar2)
  as
  begin
    if PimsSetting.FillProdHourPerHourType then
      begin
        DELETE FROM
          PRODHOURPEREMPLPERTYPE PHE
        WHERE
          PHE.PRODHOUREMPLOYEE_DATE >= AStartDate AND
          PHE.PRODHOUREMPLOYEE_DATE <= AEndDate AND
          PHE.EMPLOYEE_NUMBER = AIDCard.EmployeeCode AND
          PHE.MANUAL_YN = AManual;
      exception
        when others then null;
      end;
    end if;
  end DeleteProdHourForAllTypes;

  procedure DeleteProduction(AIDCard TScannedIDCard, AStartDate Date, AEndDate Date)
  as
  begin
    begin
      DELETE FROM
        PRODHOURPEREMPLOYEE P
      WHERE
        P.PRODHOUREMPLOYEE_DATE >= AStartDate AND
        P.PRODHOUREMPLOYEE_DATE <= AEndDate AND
        P.EMPLOYEE_NUMBER = AIDCard.EmployeeCode AND
        P.MANUAL_YN = 'N';
    exception
      when others then null;
    end;
  end DeleteProduction;

  procedure DeleteSalary(AIDCard TScannedIDCard, AStartDate Date, AEndDate Date)
  as
  begin
    begin
      DELETE FROM
        SALARYHOURPEREMPLOYEE SPE
      WHERE
        SPE.SALARY_DATE >= AStartDate AND
        SPE.SALARY_DATE <= AEndDate AND
        SPE.EMPLOYEE_NUMBER = AIDCard.EmployeeCode AND
        SPE.MANUAL_YN = 'N';
    exception
      when others then null;
    end;
    DeleteProdHourForAllTypes(AIDCard, AStartDate, AEndDate, 'N');
    if ARecalcProdHours then
      DeleteProduction(AIDCard, AStartDate, AEndDate);
    end if;

  end DeleteSalary;

  procedure ShiftDateCheckOnProdDates(AIDCard TScannedIDCard,
    AProdDate1 in out Date, AProdDate2 in out Date,
    AProdDate3 in out Date, AProdDate4 in out Date)
  as
  begin
    if AProdDate1 is not null then
      if AIDCard.ShiftDate <> AProdDate1 then
        AProdDate1 := AIDCard.ShiftDate;
      end if;
    end if;
    if AProdDate2 is not null then
      if AIDCard.ShiftDate <> AProdDate2 then
        AProdDate2 := AIDCard.ShiftDate;
      end if;
    end if;
    if AProdDate3 is not null then
      if AIDCard.ShiftDate <> AProdDate3 then
        AProdDate3 := AIDCard.ShiftDate;
      end if;
    end if;
    if AProdDate4 is not null then
      if AIDCard.ShiftDate <> AProdDate4 then
        AProdDate4 := AIDCard.ShiftDate;
      end if;
    end if;
  end ShiftDateCheckOnProdDates;
  procedure PopulateScan(Scan in out TScan, CV_Rec TCV_Rec) as
  begin
    Scan.EmployeeNumber := CV_Rec.EmployeeNumber;
    Scan.ShiftNumber := CV_Rec.ShiftNumber;
    Scan.IDCardIn := CV_Rec.IDCardIn;
    Scan.WorkspotCode := CV_Rec.WorkspotCode;
    Scan.JobCode := CV_Rec.JobCode;
    Scan.PlantCode := CV_Rec.PlantCode;
    Scan.DateTimeIn := CV_Rec.DateTimeIn;
    Scan.DateTimeOut := CV_Rec.DateTimeOut;
    Scan.ShiftDate := CV_Rec.ShiftDate;
    Scan.ShiftHourtypeNumber := CV_Rec.ShiftHourtypeNumber;
    Scan.ShiftIgnoreForOvertimeYN := CV_Rec.ShiftIgnoreForOvertimeYN;
    Scan.IgnoreForOvertimeYN := CV_Rec.IgnoreForOvertimeYN;
    Scan.WorkspotHourtypeNumber := CV_Rec.WorkspotHourtypeNumber;
    Scan.WorkspotDepartmentCode := CV_Rec.DepartmentCode;
    Scan.DepartmentCode := CV_Rec.DepartmentCode;
  end;
  function ProdDateCheck(ADateIn Date, ADateOut Date) return Boolean is
    Result Boolean;
  begin
    Result := False;
    if
      (
        ((AProdDate1 is not null) and (Trunc(ADateIn) = AProdDate1))
        or
        ((AProdDate2 is not null) and (Trunc(ADateIn) = AProdDate2))
        or
        ((AProdDate3 is not null) and (Trunc(ADateIn) = AProdDate3))
        or
        ((AProdDate4 is not null) and (Trunc(ADateIn) = AProdDate4))
        or
        ((AProdDate1 is not null) and (Trunc(ADateOut) = AProdDate1))
        or
        ((AProdDate2 is not null) and (Trunc(ADateOut) = AProdDate2))
        or
        ((AProdDate3 is not null) and (Trunc(ADateOut) = AProdDate3))
        or
        ((AProdDate4 is not null) and (Trunc(ADateOut) = AProdDate4))
      )
      then
      Result := True;
    end if;
    return Result;
  end ProdDateCheck;
begin
  GetPimsSettings;
  ORASystemDM.CurrentProgramUser := ACurrentProgramUser;
  ORASystemDM.CurrentComputerName := ACurrentComputerName;
  ORASystemDM.ADebug := ADebug;

  if ORASystemDM.ADebug = 1 then
    WDebugLog('UnProcessProcessScans.');
    WDebugLog('- EMP=' || TO_CHAR(AIDCard.EmployeeCode) ||
    ' PL=' || AIDCard.PlantCode ||
    ' SHFT=' || TO_CHAR(AIDCard.ShiftNumber) ||
    ' WC=' || AIDCard.WorkSpotCode ||
    ' JC=' || AIDCard.JobCode ||
    ' DEPT=' || AIDCard.DepartmentCode
    );
    WDebugLog('- DIN=' || DateTimeToStr(AIDCard.DateIn) ||
      ' DOUT=' || DateTimeToStr(AIDCard.DateOut) ||
      ' ShDate=' || DateToStr(AIDCard.ShiftDate)
      );
    WDebugLog('- Params' ||
      ' FromDate=' || DateToStr(AFromDate) ||
      ' PDate1=' || DateToStr(AProdDate1) ||
      ' PDate2=' || DateToStr(AProdDate2) ||
      ' PDate3=' || DateToStr(AProdDate3) ||
      ' PDate4=' || DateToStr(AProdDate4) ||
      ' ProcessSal=' || Bool2Str(AProcessSalary) ||
      ' DelAction=' || Bool2Str(ADeleteAction) ||
      ' RecalcProdHrs=' || Bool2Str(ARecalcProdHours) ||
      ' FromProcessPlanAbsence=' || Bool2Str(AFromProcessPlannedAbsence) ||
      ' Man=' || Bool2Str(AManual)
      );
  end if;

  if not PimsSetting.UseShiftDateSystem then
    if AFromProcessPlannedAbsence then
      StartDate := Trunc(AIDCard.DateIn);
      EndDate := Trunc(AIDCard.DateOut);
    else
      GetShiftDay(AIDCard, StartDate, EndDate);
    end if;
  else
    ARecalcProdHours := not AIDCard.FromTimeRecordingApp;
    if not AFromProcessPlannedAbsence then
      GetShiftDay(AIDCard, StartDate, EndDate);
      if AManual then
        StartDate := TruncFrac(AIDCard.DateIn, StartDate);
        EndDate := TruncFrac(AIDCard.DateIn, EndDate);
        if StartDate > EndDate then
          EndDate := EndDate + 1;
        end if;
      end if;
    end if;
    if not AFromProcessPlannedAbsence then
      ShiftDateCheckOnProdDates(AIDCard, AProdDate1, AProdDate2, AProdDate3, AProdDate4);
    end if;
  end if;

  ProdDate.extend(4);

  ProdDate(1) := AProdDate1;
  ProdDate(2) := AProdDate2;
  ProdDate(3) := AProdDate3;
  ProdDate(4) := AProdDate4;

  if PimsSetting.UseShiftDateSystem then
    if AFromProcessPlannedAbsence then
      StartDate := Trunc(AIDCard.DateIn);
      EndDate := Trunc(AIDCard.DateOut);
    end if;
  end if;

  ComputeOvertimePeriod(AIDCard.EmployeeCode, StartDate, EndDate);
  if ORASystemDM.ADebug = 1 then
    WDebugLog('- OvertimePeriod' ||
      ' StartDate=' || DateTimeToStr(StartDate) ||
      ' EndDate=' || DateTimeToStr(EndDate)
      );
  end if;

  if AProcessSalary then
    GetTFT_Bonus(AIDCard);
    if not PimsSetting.UseShiftDateSystem then
      if (AFromDate > StartDate) and (AFromDate < EndDate) then
        DeleteAbsenceHours(AIDCard, AFromDate, EndDate);
        DeleteSalary(AIDCard, AFromDate, EndDate);
      else
        DeleteAbsenceHours(AIDCard, StartDate, EndDate);
        DeleteSalary(AIDCard, StartDate, EndDate);
      end if;
    else
      DeleteAbsenceHours(AIDCard, StartDate, EndDate);
      DeleteSalary(AIDCard, StartDate, EndDate);
    end if;
  end if;

  if not ARecalcProdHours then
    for i in 1..4
    loop
      if ProdDate(i) is not null then
        DeleteProduction(AIDCard, ProdDate(i), ProdDate(i));
      end if;
    end loop;
  end if;

  if AProcessSalary then
    if not PimsSetting.UseShiftDateSystem then
      WhereSelect :=
        ' WHERE ' ||
        '   T.DATETIME_IN >= :STARTDATE AND ' ||
        '   T.DATETIME_IN <= :ENDDATE AND ';
    else
      WhereSelect :=
        ' WHERE ' ||
        '   T.SHIFT_DATE >= :STARTDATE AND ' ||
        '   T.SHIFT_DATE <  :ENDDATE AND ';
    end if;
  else
    WhereSelect :=
      'WHERE ' ||
      '  ( ' ||
      '   ((T.DATETIME_IN >= :STARTDATE1) AND ' ||
      '    (T.DATETIME_IN < :ENDDATE1)) OR ' ||
      '   ((T.DATETIME_IN >= :STARTDATE2) AND ' ||
      '    (T.DATETIME_IN < :ENDDATE2)) OR ' ||
      '   ((T.DATETIME_IN >= :STARTDATE3) AND ' ||
      '    (T.DATETIME_IN < :ENDDATE3)) OR ' ||
      '   ((T.DATETIME_IN >= :STARTDATE4) AND ' ||
      '    (T.DATETIME_IN < :ENDDATE4)) OR ' ||
      '   ((T.DATETIME_OUT >= :STARTDATE1) AND ' ||
      '    (T.DATETIME_OUT < :ENDDATE1)) OR ' ||
      '   ((T.DATETIME_OUT >= :STARTDATE2) AND ' ||
      '    (T.DATETIME_OUT < :ENDDATE2)) OR ' ||
      '   ((T.DATETIME_OUT >= :STARTDATE3) AND ' ||
      '    (T.DATETIME_OUT < :ENDDATE3)) OR ' ||
      '   ((T.DATETIME_OUT >= :STARTDATE4) AND ' ||
      '    (T.DATETIME_OUT < :ENDDATE4))' ||
      '  ) AND ';
  end if;

  WorkSelect :=
    'SELECT ' ||
    '  T.DATETIME_IN, T.EMPLOYEE_NUMBER, T.PLANT_CODE, ' ||
    '  T.WORKSPOT_CODE, T.JOB_CODE, T.SHIFT_NUMBER, ' ||
    '  T.DATETIME_OUT, T.PROCESSED_YN, T.IDCARD_IN, ' ||
    '  T.IDCARD_OUT, W.DEPARTMENT_CODE, ' ||
    '  T.SHIFT_DATE, ' ||
    '  S.HOURTYPE_NUMBER SHIFT_HOURTYPE_NUMBER, ' ||
    '  CASE ' ||
    '    WHEN S.HOURTYPE_NUMBER IS NOT NULL THEN ' ||
    '      (SELECT H.IGNORE_FOR_OVERTIME_YN FROM HOURTYPE H ' ||
    '       WHERE H.HOURTYPE_NUMBER = S.HOURTYPE_NUMBER) ' ||
    '    ELSE ' ||
    '      '''' ' ||
    '  END SHIFT_IGNORE_FOR_OVERTIME_YN, ' ||
    '  W.HOURTYPE_NUMBER WORKSPOT_HOURTYPE_NUMBER, ' ||
    '  CASE ' ||
    '    WHEN W.HOURTYPE_NUMBER IS NOT NULL THEN ' ||
    '      (SELECT H.IGNORE_FOR_OVERTIME_YN FROM HOURTYPE H ' ||
    '       WHERE H.HOURTYPE_NUMBER = W.HOURTYPE_NUMBER) ' ||
    '    ELSE ' ||
    '      '''' ' ||
    '  END IGNORE_FOR_OVERTIME_YN ' ||
    'FROM ' ||
    '  TIMEREGSCANNING T INNER JOIN WORKSPOT W ON ' ||
    '    T.PLANT_CODE = W.PLANT_CODE AND ' ||
    '    T.WORKSPOT_CODE = W.WORKSPOT_CODE ' ||
    '  INNER JOIN SHIFT S ON ' ||
    '    T.PLANT_CODE = S.PLANT_CODE AND ' ||
    '    T.SHIFT_NUMBER = S.SHIFT_NUMBER ' ||
    WhereSelect ||
    '  T.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND ' ||
    '  T.PROCESSED_YN = :PROCESSED_YN ' ||
    'ORDER BY ' ||
    '  T.DATETIME_IN ';

  if AProcessSalary then
    if not PimsSetting.UseShiftDateSystem then
      open CV for
        WorkSelect using StartDate - 1, EndDate + 1, AIDCard.EmployeeCode, 'Y';
    else
      open CV for
        WorkSelect using StartDate, Trunc(EndDate) + 1, AIDCard.EmployeeCode, 'Y';
    end if;
  else
    open CV for
      WorkSelect using Trunc(AProdDate1), Trunc(AProdDate2), Trunc(AProdDate3), Trunc(AProdDate4),
        Trunc(AProdDate1) + 1, Trunc(AProdDate2) + 1, Trunc(AProdDate3) + 1,
        Trunc(AProdDate4) + 1, AIDCard.EmployeeCode, 'Y';
  end if;
  Counter := 0;
  loop
    fetch CV into CV_Rec;
    exit when CV%NOTFOUND;
    if ORASystemDM.ADebug = 1 then
      WDebugLog(' CV_Rec.DateIn=' || DateTimeToStr(CV_Rec.DateTimeIn) ||
        ' DateOut=' || DateTimeToStr(CV_Rec.DateTimeOut));
    end if;
    if PimsSetting.UseShiftDateSystem then
      CurrentProdDate1 := Null;
      CurrentProdDate2 := Null;
      CurrentProdDate3 := Null;
      CurrentProdDate4 := Null;
    end if;
    GoOn := True;
    if (PimsSetting.UseShiftDateSystem and (not AProcessSalary))
      or
      (not PimsSetting.UseShiftDateSystem) then
      SalDateFound :=
        (
          (RoundTime(CV_rec.DateTimeIn,1) >= StartDate) and
          (RoundTime(CV_rec.DateTimeIn,1) <= EndDate)
         or
          (RoundTime(CV_rec.DateTimeOut,1) >= StartDate) and
          (RoundTime(CV_rec.DateTimeOut,1) <= EndDate)
        );
      ProdDateFound :=
        ProdDateCheck(RoundTime(CV_rec.DateTimeIn,1),
          RoundTime(CV_rec.DateTimeOut,1));
      if not
        (
          SalDateFound
         or
          ProdDateFound
        ) then
        if ORASystemDM.ADebug = 1 then
          WDebugLog('-> Skipped');
        end if;
        GoOn := False;
      end if;
    end if; -- if (not UseShiftDateSystem) or (not ProcessSalary)

/*
    if not AProcessSalary then
      if not
        (
          (RoundTime(CV_rec.DateTimeIn, 1) >= StartDate) and
          (RoundTime(CV_rec.DateTimeIn, 1) <= EndDate)
        or
          (RoundTime(CV_rec.DateTimeOut, 1) >= StartDate) and
          (RoundTime(CV_rec.DateTimeOut, 1) <= EndDate)
        ) then
        GoOn := False;
      end if;
    end if;
*/
    if GoOn then
      AddToProdListOut := False;
      AddToProdListIn  := False;
      AddToSalList     := False;
      EmptyIDCard(CurrentIDCard);
      PopulateScan(Scan, CV_Rec);
      PopulateIDCard(CurrentIDCard, Scan, True);
      CurrentBookingDay := Trunc(CurrentIDCard.DateIn);
      if AProcessSalary then
        GetShiftDay(CurrentIDCard, CurrentStart, CurrentEnd);
        CurrentBookingDay := Trunc(CurrentStart);
        if PimsSetting.UseShiftDateSystem then
          CurrentIDCard.ShiftDate := CurrentBookingDay;
        else
          CurrentIDCard.ShiftDate := Trunc(CurrentIDCard.DateIn);
        end if;
        -- This can give double prod-hours
/*
        if ARecalcProdHours then
          ProdDate(1) := Trunc(CurrentIDCard.DateIn);
          ProdDate(2) := Trunc(CurrentIDCard.DateOut);
          if PimsSetting.UseShiftDateSystem then
            ShiftDateCheckOnProdDates(CurrentIDCard, ProdDate(1),
              ProdDate(2), ProdDate(1), ProdDate(2));
          end if;
        end if;
*/
        ComputeOvertimePeriod(CurrentIDCard.EmployeeCode, CurrentStart, CurrentEnd);
        if ORASystemDM.ADebug = 1 then
          WDebugLog('- DateIn=' || DateTimeToStr(CurrentIDCard.DateIn) ||
           ' DateOut=' || DateTimeToStr(CurrentIDCard.DateOut) ||
           ' CurBookDay=' || DateToStr(CurrentBookingDay) ||
           ' CurStart=' || DateTimeToStr(CurrentStart) ||
           ' CurEnd=' || DateTimeToStr(CurrentEnd)
          );
        end if;
        if (not PimsSetting.UseShiftDateSystem) then
          GoOn := (Trunc(AFromDate) >= Trunc(CurrentStart)) and
            (Trunc(AFromDate) <= Trunc(CurrentEnd)) and
            (CurrentBookingDay >= Trunc(AFromDate));
        else
          GoOn := (Trunc(AFromDate) >= Trunc(CurrentStart)) and
            (Trunc(AFromDate) <= Trunc(CurrentEnd)) and
            (CurrentBookingDay >= Trunc(CurrentIDCard.ShiftDate));
        end if;
        if GoOn then
          GoOn := not (ADeleteAction and
            (
              (CurrentIDCard.DateIn = AIDCard.DateIn)
              or
              (ARecalcProdHours)
            ) and
            (CurrentIDCard.EmployeeCode = AIDCard.EmployeeCode));
          if GoOn then
            AddToSalList := True;
            if ARecalcProdHours then
              ProdDate(1) := Trunc(CurrentIDCard.DateIn);
              ProdDate(2) := Trunc(CurrentIDCard.DateOut);
              if PimsSetting.UseShiftDateSystem then
                ShiftDateCheckOnProdDates(CurrentIDCard, ProdDate(1),
                  ProdDate(2), ProdDate(1), ProdDate(2));
              end if;
            end if;
          end if;
        end if; -- if GoOn
        CurrentProdDate1 := Trunc(CurrentIDCard.DateOut);
        CurrentProdDate2 := Trunc(CurrentIDCard.DateIn);
        if PimsSetting.UseShiftDateSystem then
          ShiftDateCheckOnProdDates(CurrentIDCard, CurrentProdDate1,
            CurrentProdDate2, CurrentProdDate3, CurrentProdDate4);
        else
          if (CurrentIDCard.DateIn <= CurrentEnd) and
            (CurrentIDCard.DateOut > CurrentEnd) then
            if ORASystemDM.ADebug = 1 then
              WDebugLog(' -> Exception: Part of scan falls in next period!');
            end if;
            if ARecalcProdHours then
              ProdDate(2) := CurrentIDCard.DateOut;
            end if;
          end if;
        end if;
        -- Determine if add to Production
        for i in 1..4
        loop
          if (CurrentProdDate1 is not null) and (ProdDate(i) is not null) then
            if (Trunc(CurrentProdDate1) = Trunc(ProdDate(i))) then
              if not (ADeleteAction and
                (CurrentIDCard.DateIn = AIDCard.DateIn) and
                (CurrentIDCard.EmployeeCode = AIDCard.EmployeeCode)) then
                AddToProdListOut := True;
              end if;
            end if;
          end if;
        end loop;
        if (Trunc(CurrentIDCard.DateIn) <> Trunc(CurrentIDCard.DateOut)) then
          for i in 1..4
          loop
            if (CurrentProdDate2 is not null) and (ProdDate(i) is not null) then
              if (Trunc(CurrentProdDate2) = Trunc(ProdDate(i))) then
                if not (ADeleteAction and
                  (CurrentIDCard.DateIn = AIDCard.DateIn) and
                  (CurrentIDCard.EmployeeCode = AIDCard.EmployeeCode)) then
                  AddToProdListIn := True;
                end if;
              end if;
            end if;
          end loop;
        end if;
        if PimsSetting.UseShiftDateSystem then
          if ADeleteAction then
            if AddToProdListOut or AddToProdListIn then
              AddToSalList := True;
            end if;
          end if;
        end if;
        if ORASystemDM.ADebug = 1 then
          WDebugLog('- AddToProdListOut=' || Bool2Str(AddToProdListOut) ||
            ' AddToProdListIn=' || Bool2Str(AddToProdListIn) ||
            ' AddToSalList=' || Bool2Str(AddToSalList)
            );
        end if;
        -- Add Production and/or Salary
        if AddToProdListOut or AddToProdListIn or AddToSalList then
          -- Bookmarked
          if ORASystemDM.ADebug = 1 then
            WDebugLog(' -> Bookmarked.');
          end if;
          Counter := Counter + 1;
          RecordsForUpdate.extend;
          BookmarkRecord.CV_Rec := CV_Rec;
          BookmarkRecord.AddToSalList := AddToSalList;
          BookmarkRecord.AddToProdListOut := AddToProdListOut;
          BookmarkRecord.AddToProdListIn := AddToProdListIn;
          BookmarkRecord.BookingDay := CurrentBookingDay;
          RecordsForUpdate(Counter) := BookmarkRecord;
        end if;
        if ARecalcProdHours then
          ProdDate(1) := Null;
          ProdDate(2) := Null;
        end if;
      end if;
    end if;
  end loop;
  close CV;

  -- Recalculate hours
  for MyIndex in 1..RecordsForUpdate.count
  loop
    LastScanRecordForDay := False;
    BookingDayNext := Null;
    if MyIndex = RecordsForUpdate.count then
      LastScanRecordForDay := True;
    else
      BookmarkRecord := RecordsForUpdate(MyIndex + 1);
      CV_Rec := BookmarkRecord.CV_Rec;
      EmptyIDCard(CurrentIDCard);
      PopulateScan(Scan, CV_Rec);
      PopulateIDCard(CurrentIDCard, Scan, True);
      GetShiftDay(CurrentIDcard, StartDate, EndDate);
      BookingDayNext := StartDate;
    end if;
    BookmarkRecord := RecordsForUpdate(MyIndex);
    CV_Rec := BookmarkRecord.CV_Rec;
    EmptyIDCard(CurrentIDCard);
    PopulateScan(Scan, CV_Rec);
    PopulateIDCard(CurrentIDCard, Scan, True);
    if not LastScanRecordForDay then
      GetShiftDay(CurrentIDCard, StartDate, EndDate);
      BookingDayCurrent := StartDate;
      if Trunc(BookingDayCurrent) <> Trunc(BookingDayNext) then
        LastScanRecordForDay := True;
      end if;
    end if;
    ProcessTimeRecording(CurrentIDCard, AIDCard, True, True,
      BookmarkRecord.AddToSalList, Bookmarkrecord.AddToProdListOut,
      Bookmarkrecord.AddToProdListIn, ADeleteAction,
      LastScanRecordForDay, ACurrentProgramUser, ACurrentComputerName, ADebug);
    if PimsSetting.UseShiftDateSystem then
      if AFromProcessPlannedAbsence then
        GetShiftDay(CurrentIDCard, StartDate, EndDate);
        if ((Trunc(StartDate) <> CurrentIDCard.ShiftDate) or
          (CurrentIDCard.ShiftDate is null)) then
            UpdateTimeRegScanning(CurrentIDCard, Trunc(StartDate));
        end if;
      end if;
    end if;
  end loop;
  commit;
end UnprocessProcessScans;

PROCEDURE ProcessScan(AEmployeeNumber Integer, AShiftNumber Integer, APlantCode Varchar2, AWorkspotCode Varchar2, AJobCode Varchar2,
                      ADateIn Date, ADateOut Date, ACurrentProgramUser Varchar2, ACurrentComputerName Varchar2, ADebug Integer)
as
  -- Boolean parameters are translated from/to integers:
  -- 0/1/null <--> false/true/null
  afirstscan boolean := sys.diutil.int_to_bool(1);
  alastscan boolean := sys.diutil.int_to_bool(1);
  aupdatesalmin boolean := sys.diutil.int_to_bool(1);
  aupdateprodminout boolean := sys.diutil.int_to_bool(1);
  aupdateprodminin boolean := sys.diutil.int_to_bool(1);
  adeleteaction boolean := sys.diutil.int_to_bool(0);
  alastscanrecordforday boolean := sys.diutil.int_to_bool(0);
  -- Non-scalar parameters require additional processing
  aidcard tscannedidcard;
  adeleteidcard tscannedidcard;
  DateIn date;
  DateOut date;
  DepartmentCode varchar2(6);
  afromdate date;
  astartdate date;
  aenddate date;
  aproddate1 date;
  aproddate2 date;
  aproddate3 date;
  aproddate4 date;
  aprocesssalary boolean := sys.diutil.int_to_bool(1);
  arecalcprodhours boolean := sys.diutil.int_to_bool(1);
  afromprocessplannedabsence boolean := sys.diutil.int_to_bool(1);
  amanual boolean := sys.diutil.int_to_bool(0);
begin
  DepartmentCode := '';
  begin
    select department_code
    into DepartmentCode
    from workspot
    where plant_code = APlantCode and
    workspot_code = AWorkSpotCode;
  exception
    when no_data_found then null;
    when others then null;
  end;

  aidcard.EmployeeCode := AEmployeeNumber;
  aidcard.PlantCode := APlantCode;
  aidcard.ShiftNumber := AShiftNumber;
  aidcard.WorkSpotCode := AWorkspotCode;
  aidcard.JobCode := AJobCode;
  aidcard.DepartmentCode := DepartmentCode;
  aidcard.DateIn := ADateIn;
  aidcard.DateOut := ADateOut;
  aidcard.FromTimeRecordingApp := false;

  GetShiftDay(aidcard, astartdate, aenddate);
  afromdate := trunc(astartdate);
  aidcard.ShiftDate := afromdate;

  -- Call the procedure
  processtimerecording(aidcard,
                       adeleteidcard,
                       afirstscan,
                       alastscan,
                       aupdatesalmin,
                       aupdateprodminout,
                       aupdateprodminin,
                       adeleteaction,
                       alastscanrecordforday,
                       ACurrentprogramuser,
                       ACurrentcomputername,
                       ADebug);

  aproddate1 := trunc(aidcard.DateOut);
  if trunc(aidcard.DateOut) <> trunc(aidcard.DateIn) then
    aproddate2 := trunc(aidcard.DateIn);
  else
    aproddate2 := null;
  end if;
  aproddate3 := null;
  aproddate4 := null;

  unprocessprocessscans(aidcard,
                        afromdate,
                        aproddate1,
                        aproddate2,
                        aproddate3,
                        aproddate4,
                        aprocesssalary,
                        adeleteaction,
                        arecalcprodhours,
                        afromprocessplannedabsence,
                        amanual,
                        ACurrentprogramuser,
                        ACurrentcomputername,
                        ADebug);

end ProcessScan;

END PACKAGE_HOUR_CALC;
/

commit;
