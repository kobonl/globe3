-- 
-- PIM-227 Make change of employee number optional
--

declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'alter table PIMSSETTING add UPDATE_EMPNR_YN varchar2(1) default ''Y''';
    exception when column_exists then null;
end;
/

commit;

