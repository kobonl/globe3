--
-- GLOB3-202 Scan via machine-card readers and do not scan out
--

UPDATE PIMSSETTING set TIMERECCR_DONOTSCANOUT_YN = 'Y';

commit;

