--
-- PIM-12 Use BI-view to read prod hours per employee
--

declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'alter table EMPLOYEEEFFPERSHIFT add MANUAL_YN varchar2(1) default ''N''';
    exception when column_exists then null;
end;
/

create or replace view v_prodhourperemployee as
select ef.shift_date prodhouremployee_date,
  ef.plant_code, p.description pdescription,
  bu.businessunit_code, bu.description bdescription,
  d.department_code, d.description ddescription,
  ef.shift_number, s.description sdescription, e.team_code,
  w.workspot_code, w.description wdescription,
  ef.job_code, j.description jdescription,
  j.ignorequantitiesinreports_yn,
  j.comparation_job_yn,
  ef.employee_number, e.description edescription,
  ef.employeesecondsactual / 60 production_minute,
  ef.employeequantity,
  ef.employeesecondsnorm,
  ef.normlevel,
  ef.manual_yn,
  nvl(bu.bonus_factor, 0) bonus_factor,
  nvl(r.revenue_amount, 0) revenue_amount,
  ec.hourly_wage,
  p.average_wage,
  j.norm_output_level,
  (select nvl(sum(p.production_minute),0) from prodhourperemplpertype p where
   ef.shift_date = p.prodhouremployee_date and
   ef.employee_number = p.employee_number and
   ef.plant_code = p.plant_code and
   ef.workspot_code = p.workspot_code and
   ef.job_code = p.job_code and
   ef.shift_number = p.shift_number and
   p.manual_yn = 'N') prodhourmin,
  (select nvl(sum(p2.payed_break_minute),0) from prodhourperemployee p2 where 
   ef.shift_date = p2.prodhouremployee_date and
   ef.employee_number = p2.employee_number and
   ef.plant_code = p2.plant_code and
   ef.workspot_code = p2.workspot_code and
   ef.job_code = p2.job_code and
   ef.shift_number = p2.shift_number and
   p2.manual_yn = 'N') payed_break_minute
from employeeeffpershift ef inner join jobcode j on (ef.plant_code = j.plant_code and ef.workspot_code = j.workspot_code and ef.job_code = j.job_code)
  inner join plant p on (ef.plant_code = p.plant_code)
  left join businessunit bu on (j.plant_code = bu.plant_code and j.businessunit_code = bu.businessunit_code)
  inner join workspot w on (ef.plant_code = w.plant_code and ef.workspot_code = w.workspot_code)
  inner join department d on (w.plant_code = d.plant_code and w.department_code = d.department_code)
  inner join shift s on (ef.plant_code = s.plant_code and ef.shift_number = s.shift_number)
  inner join employee e on (ef.employee_number = e.employee_number)
  left join revenue r on (j.businessunit_code = r.businessunit_code and ef.shift_date = r.revenue_date)
  left join employeecontract ec on (e.employee_number = ec.employee_number and ec.startdate <= sysdate and ec.enddate >= sysdate)
/

-- Create/Recreate indexes 
drop index XUIEMPLOYEEEFFPERSHIFT;
create unique index XUIEMPLOYEEEFFPERSHIFT on EMPLOYEEEFFPERSHIFT (plant_code, shift_date, shift_number, employee_number, workspot_code, job_code, manual_yn)
  tablespace INDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

CREATE OR REPLACE PROCEDURE "CALC_EFFICIENCY"(fp_plant_code varchar2) as
-- This procedure populates tables workspoteffperminute employeeeffperminute
-- It has to be scheduled to run every 1 minute, at 30 seconds past the minute, using an oracle job scheduler
-- Logging is done in abslog table.
-- At the first run it loads all scans of the current date from midnight.
-- If the procedure did not run for a longer time (hours/days) it will load all scans since last load.
-- Revisions:
-- Date        By       Version   Change
-- 29/04/2015  ABS HVH  1.0       Created new.
gv_last_pims_timestamp   timestamp;
gv_curr_pims_timestamp   timestamp;
gv_inserted              number;
gv_last_error_message    varchar2(2000);
gv_logtimestamp          timestamp;
gv_abslog_rowid          rowid;
gv_section               varchar2(40);
gv_old_intervalstarttime timestamp;
gv_old_intervalendtime   timestamp;

---------------------------------------------------------
procedure write_log is
begin
  select sysdate into gv_logtimestamp from dual;
  insert into abslog
   (abslog_id,
    computer_name,
    abslog_timestamp,
    logsource,
    priority,
    systemuser,
    logmessage)
   values (
    0, -- determined by trigger
    '-', -- computer name
    gv_logtimestamp,
    'CALC_EFFICIENCY', -- logsource
    1,
    '-', -- systemuser
    'Start Load ' || gv_section)
    returning rowid into gv_abslog_rowid;
  commit;
end write_log;

---------------------------------------------------------
procedure update_log is
begin
  update abslog l
    set l.logmessage = gv_section || ' loaded from ' || to_char(gv_last_pims_timestamp,'YYYY-MM-DD HH24:MI') ||
                     ' to ' || to_char(gv_curr_pims_timestamp,'YYYY-MM-DD HH24:MI') ||
                        ' ' || to_char(gv_inserted) || ' rows inserted in ' ||
                    to_char(EXTRACT(DAY FROM(sysdate - gv_logtimestamp)) * 24 * 60 * 60 +
                            EXTRACT(HOUR FROM(sysdate - gv_logtimestamp)) * 60 * 60 +
                            EXTRACT(MINUTE FROM(sysdate - gv_logtimestamp)) * 60 +
                            EXTRACT(SECOND FROM(sysdate - gv_logtimestamp)))
                    || ' seconds.'
  where l.rowid = gv_abslog_rowid;
  commit;
end update_log;

---------------------------------------------------------
procedure Time_on_Workspot_Minute_Level is
begin
  gv_section := 'Time_on_Workspot_Minute_Level';
  write_log;

  merge into workspoteffperminute
  using
  (select v.plant_code,
          v.shift_date,
          v.shift_number,
          v.workspot_code,
          v.job_code,
          v.intervalstarttime intervalstarttime,
          max(v.intervalendtime) intervalendtime,
          min(v.start_timestamp) start_timestamp,
          max(v.end_timestamp) end_timestamp,
          sum(v.seconds) seconds,
          avg(v.norm_level) norm_level
     from v_timereg_by_minute v
    where v.plant_code = fp_plant_code
    group by v.plant_code,
             v.shift_date,
             v.shift_number,
             v.workspot_code,
             v.job_code,
             v.intervalstarttime
  )
    merge_subquery
on (workspoteffperminute.plant_code = merge_subquery.plant_code and
    workspoteffperminute.shift_date = merge_subquery.shift_date and
    workspoteffperminute.shift_number = merge_subquery.shift_number and
    workspoteffperminute.workspot_code = merge_subquery.workspot_code and
    workspoteffperminute.job_code = merge_subquery.job_code and
    workspoteffperminute.intervalstarttime = merge_subquery.intervalstarttime
   )
  when not matched then
    insert
       (workspoteffperminute_id,
        shift_date,
        shift_number,
        plant_code,
        workspot_code,
        job_code,
        intervalstarttime,
        intervalendtime,
        workspotsecondsactual,
        workspotsecondsnorm,
        workspotquantity,
        normlevel,
        start_timestamp,
        end_timestamp,
        creationdate,
        mutationdate,
        mutator)
    values
      (seq_workspoteffperminute.nextval,
       merge_subquery.shift_date,
       merge_subquery.shift_number,
       merge_subquery.plant_code,
       merge_subquery.workspot_code,
       merge_subquery.job_code,
       merge_subquery.intervalstarttime,
       merge_subquery.intervalendtime,
       merge_subquery.seconds,
       0, -- workspotsecondsnorm
       0, -- workspotquantity
       merge_subquery.norm_level,
       merge_subquery.start_timestamp,
       merge_subquery.end_timestamp,
       sysdate, -- creationdate
       sysdate, -- mutationdate
       'system' -- mutator
      )
  when matched then
    update
    set workspotsecondsactual = merge_subquery.seconds,
        normlevel = merge_subquery.norm_level,
        mutationdate = sysdate;

  gv_inserted := SQL%ROWCOUNT;
  commit;
  update_log;
end Time_on_Workspot_Minute_Level;

---------------------------------------------------------
procedure Time_on_Employee_Minute_Level is
begin
  gv_section := 'Time_on_Employee_Minute_Level';
  write_log;

  merge into employeeeffperminute
  using
    (select v.plant_code,
            v.shift_date,
            v.shift_number,
            v.workspot_code,
            v.job_code,
            v.employee_number,
            v.intervalstarttime intervalstarttime,
            max(v.intervalendtime) intervalendtime,
            min(v.start_timestamp) start_timestamp,
            max(v.end_timestamp) end_timestamp,
            sum(v.seconds) seconds,
            avg(v.norm_level) norm_level
       from v_timereg_by_minute v
      where v.plant_code = fp_plant_code
   group by v.plant_code,
            v.shift_date,
            v.shift_number,
            v.workspot_code,
            v.job_code,
            v.employee_number,
            v.intervalstarttime
    )
      merge_subquery
  on (employeeeffperminute.plant_code = merge_subquery.plant_code and
      employeeeffperminute.shift_date = merge_subquery.shift_date and
      employeeeffperminute.shift_number = merge_subquery.shift_number and
      employeeeffperminute.workspot_code = merge_subquery.workspot_code and
      employeeeffperminute.job_code = merge_subquery.job_code and
      employeeeffperminute.employee_number = merge_subquery.employee_number and
      employeeeffperminute.intervalstarttime = merge_subquery.intervalstarttime
     )
    when not matched then
      insert
         (employeeeffperminute_id,
          shift_date,
          shift_number,
          plant_code,
          workspot_code,
          job_code,
          employee_number,
          intervalstarttime,
          intervalendtime,
          employeesecondsactual,
          employeesecondsnorm,
          employeequantity,
          normlevel,
          start_timestamp,
          end_timestamp,
          creationdate,
          mutationdate,
          mutator)
      values
        (seq_employeeeffperminute.nextval,
         merge_subquery.shift_date,
         merge_subquery.shift_number,
         merge_subquery.plant_code,
         merge_subquery.workspot_code,
         merge_subquery.job_code,
         merge_subquery.employee_number,
         merge_subquery.intervalstarttime,
         merge_subquery.intervalendtime,
         merge_subquery.seconds,
         0, -- employeesecondsnorm
         0, -- employeequantity
         merge_subquery.norm_level,
         merge_subquery.start_timestamp,
         merge_subquery.end_timestamp,
         sysdate, -- creationdate
         sysdate, -- mutationdate
         'system' -- mutator
        )
    when matched then
      update
      set employeesecondsactual = merge_subquery.seconds,
          normlevel = merge_subquery.norm_level,
          mutationdate = sysdate;

  gv_inserted := SQL%ROWCOUNT;
  commit;
  update_log;
end Time_on_Employee_Minute_Level;

---------------------------------------------------------
procedure Update_Qty_Employee_Minute_Lvl is
begin
  gv_section := 'Update_Qty_Employee_Minute_Lvl';
  write_log;

  merge into employeeeffperminute
  using
    (select e.plant_code,
            e.shift_date,
            e.shift_number,
            e.workspot_code,
            e.job_code,
            e.employee_number,
            e.intervalstarttime,
            case when w.workspotsecondsactual = 0
             then 0
             else e.employeesecondsactual / w.workspotsecondsactual * w.workspotquantity
            end employeequantity, -- equal share
            case when w.normlevel = 0 or w.workspotsecondsactual  = 0
              then 0
              else e.employeesecondsactual / w.workspotsecondsactual * w.workspotsecondsnorm
            end employeesecondsnorm,
            w.normlevel
       from employeeeffperminute e, workspoteffperminute w
      where w.start_timestamp >= gv_last_pims_timestamp
        and w.start_timestamp < gv_curr_pims_timestamp
        and e.start_timestamp >= gv_last_pims_timestamp
        and e.start_timestamp < gv_curr_pims_timestamp
        and w.plant_code = e.plant_code
        and w.shift_date = e.shift_date
        and w.shift_number = e.shift_number
        and w.workspot_code = e.workspot_code
        and w.job_code = e.job_code
        and w.intervalstarttime = e.intervalstarttime
        and w.plant_code = fp_plant_code
    )
      merge_subquery
  on (employeeeffperminute.plant_code = merge_subquery.plant_code and
      employeeeffperminute.shift_date = merge_subquery.shift_date and
      employeeeffperminute.shift_number = merge_subquery.shift_number and
      employeeeffperminute.workspot_code = merge_subquery.workspot_code and
      employeeeffperminute.job_code = merge_subquery.job_code and
      employeeeffperminute.employee_number = merge_subquery.employee_number and
      employeeeffperminute.intervalstarttime = merge_subquery.intervalstarttime
     )
    when matched then
      update
      set employeequantity = merge_subquery.employeequantity,
          employeesecondsnorm = merge_subquery.employeesecondsnorm,
          normlevel = merge_subquery.normlevel,
          mutationdate = sysdate;

  gv_inserted := SQL%ROWCOUNT;
  commit;
  update_log;
end Update_Qty_Employee_Minute_Lvl;

---------------------------------------------------------
 procedure Update_Employee_Shift_Totals is
 begin
  gv_section := 'Update_Employee_Shift_Totals';
  write_log;

  merge into employeeeffpershift
  using
  (select w.plant_code,
          w.shift_date,
          w.shift_number,
          w.workspot_code,
          w.job_code,
          w.employee_number,
          sum(w.employeesecondsactual) employeesecondsactual,
          sum(w.employeesecondsnorm) employeesecondsnorm,
          sum(w.employeequantity) employeequantity,
          avg(w.normlevel) normlevel
     from employeeeffperminute w
    where w.shift_date between trunc(gv_last_pims_timestamp)-1 and trunc(gv_curr_pims_timestamp)
    group by w.plant_code,
             w.shift_date,
             w.shift_number,
             w.workspot_code,
             w.job_code,
             w.employee_number
  )
    merge_subquery
on (employeeeffpershift.plant_code = merge_subquery.plant_code and
    employeeeffpershift.shift_date = merge_subquery.shift_date and
    employeeeffpershift.shift_number = merge_subquery.shift_number and
    employeeeffpershift.workspot_code = merge_subquery.workspot_code and
    employeeeffpershift.job_code = merge_subquery.job_code and
    employeeeffpershift.employee_number = merge_subquery.employee_number and
    employeeeffpershift.manual_yn = 'N'
   )
  when not matched then
  insert (employeeeffpershift_id,
          shift_date,
          shift_number,
          plant_code,
          workspot_code,
          job_code,
          employee_number,
          employeesecondsactual,
          employeesecondsnorm,
          employeequantity,
          normlevel,
          creationdate,
          mutationdate,
          mutator)
  values
         (seq_employeeeffpershift.nextval,
          merge_subquery.shift_date,
          merge_subquery.shift_number,
          merge_subquery.plant_code,
          merge_subquery.workspot_code,
          merge_subquery.job_code,
          merge_subquery.employee_number,
          merge_subquery.employeesecondsactual,
          merge_subquery.employeesecondsnorm,
          merge_subquery.employeequantity,
          merge_subquery.normlevel,
          sysdate, -- creation date
          sysdate, -- mutation date
          1 -- mutator
          )
  when matched then
    update
    set employeesecondsactual = merge_subquery.employeesecondsactual,
        employeesecondsnorm = merge_subquery.employeesecondsnorm,
        employeequantity = merge_subquery.employeequantity,
        normlevel = merge_subquery.normlevel,
        mutationdate = sysdate;

  gv_inserted := SQL%ROWCOUNT;
  commit;
  update_log;
end Update_Employee_Shift_Totals;


---------------------------------------------------------
begin -- main
 BEGIN
  -- Initisalation
  select sysdate into gv_logtimestamp from dual;

  BEGIN
  select e.currintervalstarttime, e.currintervalendtime
    into gv_old_intervalstarttime, gv_old_intervalendtime
    from efficiencycalcperiod e
   where e.plant_code = fp_plant_code;
  EXCEPTION
     WHEN NO_DATA_FOUND THEN
      begin
        select trunc(sysdate), trunc(sysdate)
          into gv_old_intervalstarttime, gv_old_intervalendtime
          from dual;
        insert into efficiencycalcperiod
          (plant_code,currintervalstarttime,currintervalendtime,lastintervalendtime)
          (select fp_plant_code, gv_old_intervalstarttime, gv_old_intervalendtime, gv_old_intervalstarttime from dual);
        commit;
      end;
  END;

  gv_last_pims_timestamp := gv_old_intervalendtime;
  select to_date(to_char(sysdate,'YYYYMMDDHH24MI'),'YYYYMMDDHH24MI')
    into gv_curr_pims_timestamp -- current timestamp rounded down to 1 minute
    from dual;

---- TEMP to go back in time to 29/1/2013 for continuous execution
--  gv_curr_pims_timestamp :=  gv_curr_pims_timestamp  - 829;
--  gv_last_pims_timestamp  := gv_last_pims_timestamp;
---

  update efficiencycalcperiod e
     set e.currintervalstarttime = gv_last_pims_timestamp,
         e.currintervalendtime = gv_curr_pims_timestamp
   where e.plant_code = fp_plant_code;
  commit;

-- exit when less than a minute between last and current timestamp
  if gv_curr_pims_timestamp <= gv_last_pims_timestamp then
    gv_section := 'Aborted';
    write_log;
    return;
  end if;

-- delete detailed data older than 3 days

  delete from workspoteffperminute w
   where w.shift_date < trunc(sysdate) - 3;
  commit;

  delete from employeeeffperminute w
   where w.shift_date < trunc(sysdate) - 3;
  commit;

  Time_on_Workspot_Minute_Level;

  Time_on_Employee_Minute_Level;

  Update_Qty_Employee_Minute_Lvl;

  Update_Employee_Shift_Totals;

-- Update lastintervalendtime to indicate that until this timestamp data is calculated
  update efficiencycalcperiod e
     set e.lastintervalendtime = gv_curr_pims_timestamp
   where e.plant_code = fp_plant_code;
  commit;

  EXCEPTION
     WHEN OTHERS THEN
      begin
        gv_last_error_message := SQLERRM;
        ROLLBACK;
        update abslog l
         set l.logmessage = Substr(gv_section || ' load Failed. ' || gv_last_error_message,1,512),
             l.priority = 3 -- error
         where l.rowid = gv_abslog_rowid;
        commit;
        update efficiencycalcperiod e
           set e.currintervalstarttime = gv_old_intervalstarttime,
               e.currintervalendtime = gv_old_intervalendtime
         where e.plant_code = fp_plant_code;
        commit;
      end;
  END;
end; -- CALC_EFFICIENCY
/

CREATE OR REPLACE PROCEDURE "RECALC_EFFICIENCY"(fp_plant_code varchar2,
                                               fp_shift_date date,
                                               fp_shift_number number,
                                               fp_workspot_code varchar2,
                                               fp_job_code varchar2) as
-- This procedure recalculates the time in workspoteffperminute and employeeeffperminute.
-- This procedure is called by maintain timerecoring scans, after scans are manually adjusted or added or deleted.
-- Revisions:
-- Date        By       Version   Change
-- 11/06/2015  ABS HVH  1.0       Created new.
gv_inserted           number;
gv_last_error_message varchar2(2000);
gv_logtimestamp       timestamp;
gv_abslog_rowid       rowid;
gv_section            varchar2(40);

---------------------------------------------------------
procedure write_log is pragma autonomous_transaction;
begin 
  select sysdate into gv_logtimestamp from dual;
  insert into abslog
   (abslog_id, 
    computer_name, 
    abslog_timestamp, 
    logsource, 
    priority, 
    systemuser, 
    logmessage)
   values (
    0, -- determined by trigger
    '-', -- computer name
    gv_logtimestamp,
    'RECALC_EFFICIENCY', -- logsource 
    1,
    '-', -- systemuser
    'Start Load ' || gv_section)
    returning rowid into gv_abslog_rowid;
  commit;
end write_log;

---------------------------------------------------------
procedure update_log is pragma autonomous_transaction;
begin 
  update abslog l
    set l.logmessage = gv_section || ' updated ' || to_char(gv_inserted) || ' rows inserted in ' ||
                       to_char(EXTRACT(DAY FROM(sysdate - gv_logtimestamp)) * 24 * 60 * 60 +
                               EXTRACT(HOUR FROM(sysdate - gv_logtimestamp)) * 60 * 60 +
                               EXTRACT(MINUTE FROM(sysdate - gv_logtimestamp)) * 60 +
                               EXTRACT(SECOND FROM(sysdate - gv_logtimestamp))) || ' seconds.'                    
  where l.rowid = gv_abslog_rowid;
  commit;
end update_log;

---------------------------------------------------------
procedure Time_on_Workspot_Minute_Level is
begin
  gv_section := 'Time_on_Workspot_Minute_Level';
  write_log;

  update workspoteffperminute w
    set w.workspotsecondsactual = 0,
        w.mutationdate = sysdate
  where w.plant_code = fp_plant_code
    and w.shift_date = fp_shift_date
    and w.shift_number = fp_shift_number
    and w.workspot_code = fp_workspot_code
    and w.job_code = fp_job_code;
  
  merge into workspoteffperminute
  using
  (select v.plant_code,
          v.shift_date,
          v.shift_number,
          v.workspot_code,
          v.job_code,
          v.intervalstarttime,
          max(v.intervalendtime) intervalendtime,
          min(v.start_timestamp) start_timestamp,
          max(v.end_timestamp) end_timestamp,
          sum(v.seconds) seconds,
          avg(v.norm_level) norm_level
     from v_timereg_by_minute_corr v 
    where v.plant_code = fp_plant_code
      and v.shift_date = fp_shift_date
      and v.shift_number = fp_shift_number
      and v.workspot_code = fp_workspot_code
      and v.job_code = fp_job_code
    group by v.plant_code,
             v.shift_date,
             v.shift_number,
             v.workspot_code,
             v.job_code,
             v.intervalstarttime
  )
    merge_subquery
on (workspoteffperminute.plant_code = merge_subquery.plant_code and
    workspoteffperminute.shift_date = merge_subquery.shift_date and
    workspoteffperminute.shift_number = merge_subquery.shift_number and
    workspoteffperminute.workspot_code = merge_subquery.workspot_code and
    workspoteffperminute.job_code = merge_subquery.job_code and
    workspoteffperminute.intervalstarttime = merge_subquery.intervalstarttime
   )
  when not matched then
    insert
       (workspoteffperminute_id, 
        shift_date, 
        shift_number, 
        plant_code, 
        workspot_code, 
        job_code,
        intervalstarttime,
        intervalendtime,
        workspotsecondsactual, 
        workspotsecondsnorm, 
        workspotquantity, 
        normlevel, 
        start_timestamp, 
        end_timestamp, 
        creationdate, 
        mutationdate, 
        mutator)
    values
      (seq_workspoteffperminute.nextval,
       merge_subquery.shift_date,
       merge_subquery.shift_number,             
       merge_subquery.plant_code,
       merge_subquery.workspot_code,
       merge_subquery.job_code,
       merge_subquery.intervalstarttime,
       merge_subquery.intervalendtime,
       merge_subquery.seconds,
       0, -- workspotsecondsnorm
       0, -- workspotquantity
       merge_subquery.norm_level,
       merge_subquery.start_timestamp,
       merge_subquery.end_timestamp,
       sysdate, -- creationdate
       sysdate, -- mutationdate
       1 -- mutator       
      )
  when matched then
    update
    set workspotsecondsactual = merge_subquery.seconds,
        normlevel = merge_subquery.norm_level,    
        mutationdate = sysdate;
  
  gv_inserted := SQL%ROWCOUNT;
  commit;
  update_log;
end Time_on_Workspot_Minute_Level;

---------------------------------------------------------
procedure Time_on_Employee_Minute_Level is
begin
  gv_section := 'Time_on_Employee_Minute_Level';
  write_log;

  update employeeeffperminute w
    set w.employeesecondsactual = 0,
        w.mutationdate = sysdate
  where w.plant_code = fp_plant_code
    and w.shift_date = fp_shift_date
    and w.shift_number = fp_shift_number
    and w.workspot_code = fp_workspot_code
    and w.job_code = fp_job_code;

  merge into employeeeffperminute
  using
    (select v.plant_code,
            v.shift_date,
            v.shift_number,
            v.workspot_code,
            v.job_code,
            v.employee_number,
            v.intervalstarttime,
            max(v.intervalendtime) intervalendtime,
            min(v.start_timestamp) start_timestamp,
            max(v.end_timestamp) end_timestamp,            
            sum(v.seconds) seconds,
            avg(v.norm_level) norm_level
       from v_timereg_by_minute_corr v 
      where v.plant_code = fp_plant_code
        and v.shift_date = fp_shift_date
        and v.shift_number = fp_shift_number
        and v.workspot_code = fp_workspot_code
        and v.job_code = fp_job_code
    group by v.plant_code,
             v.shift_date,
             v.shift_number,
             v.workspot_code,
             v.job_code,
             v.employee_number,
             v.intervalstarttime
    )
      merge_subquery
  on (employeeeffperminute.plant_code = merge_subquery.plant_code and
      employeeeffperminute.shift_date = merge_subquery.shift_date and
      employeeeffperminute.shift_number = merge_subquery.shift_number and
      employeeeffperminute.workspot_code = merge_subquery.workspot_code and
      employeeeffperminute.job_code = merge_subquery.job_code and
      employeeeffperminute.employee_number = merge_subquery.employee_number and
      employeeeffperminute.intervalstarttime = merge_subquery.intervalstarttime
     )
    when not matched then
      insert
         (employeeeffperminute_id, 
          shift_date, 
          shift_number, 
          plant_code, 
          workspot_code, 
          job_code, 
          employee_number, 
          intervalstarttime,
          intervalendtime,
          employeesecondsactual, 
          employeesecondsnorm, 
          employeequantity, 
          normlevel, 
          start_timestamp, 
          end_timestamp, 
          creationdate, 
          mutationdate, 
          mutator)
      values
        (seq_employeeeffperminute.nextval,
         merge_subquery.shift_date,
         merge_subquery.shift_number,             
         merge_subquery.plant_code,
         merge_subquery.workspot_code,
         merge_subquery.job_code,
         merge_subquery.employee_number,
         merge_subquery.intervalstarttime,
         merge_subquery.intervalendtime,
         merge_subquery.seconds,
         0, -- employeesecondsnorm
         0, -- employeequantity
         merge_subquery.norm_level,
         merge_subquery.start_timestamp,
         merge_subquery.end_timestamp,
         sysdate, -- creationdate
         sysdate, -- mutationdate
         1 -- mutator       
        )
    when matched then
      update
      set employeesecondsactual = merge_subquery.seconds,
          normlevel = merge_subquery.norm_level,
          mutationdate = sysdate;

  gv_inserted := SQL%ROWCOUNT;
  commit;
  update_log;
end Time_on_Employee_Minute_Level;

---------------------------------------------------------
procedure Update_Qty_Employee_Minute_Lvl is
begin
  gv_section := 'Update_Qty_Employee_Minute_Lvl';
  write_log;

  merge into employeeeffperminute
  using
    (select e.plant_code,
            e.shift_date,
            e.shift_number,
            e.workspot_code,
            e.job_code,
            e.employee_number,
            e.intervalstarttime,
            case when w.workspotsecondsactual = 0
             then 0
             else e.employeesecondsactual / w.workspotsecondsactual * w.workspotquantity 
            end employeequantity, -- equal share
            case when w.normlevel = 0 or w.workspotsecondsactual  = 0
              then 0
              else e.employeesecondsactual / w.workspotsecondsactual * w.workspotsecondsnorm 
            end employeesecondsnorm,
            w.normlevel
       from employeeeffperminute e, workspoteffperminute w
      where w.plant_code = e.plant_code
        and w.shift_date = e.shift_date
        and w.shift_number = e.shift_number
        and w.workspot_code = e.workspot_code
        and w.job_code = e.job_code
        and w.intervalstarttime = e.intervalstarttime
        and w.plant_code = fp_plant_code
        and w.shift_date = fp_shift_date
        and w.shift_number = fp_shift_number
        and w.workspot_code = fp_workspot_code
        and w.job_code = fp_job_code
    )
      merge_subquery
  on (employeeeffperminute.plant_code = merge_subquery.plant_code and
      employeeeffperminute.shift_date = merge_subquery.shift_date and
      employeeeffperminute.shift_number = merge_subquery.shift_number and
      employeeeffperminute.workspot_code = merge_subquery.workspot_code and
      employeeeffperminute.job_code = merge_subquery.job_code and
      employeeeffperminute.employee_number = merge_subquery.employee_number and
      employeeeffperminute.intervalstarttime = merge_subquery.intervalstarttime
     )
    when matched then
      update
      set employeequantity = merge_subquery.employeequantity, 
          employeesecondsnorm = merge_subquery.employeesecondsnorm, 
          normlevel = merge_subquery.normlevel, 
          mutationdate = sysdate;

  gv_inserted := SQL%ROWCOUNT;
  commit;
  update_log;
end Update_Qty_Employee_Minute_Lvl;  

---------------------------------------------------------
 procedure Update_Employee_Shift_Totals is
 begin
  gv_section := 'Update_Employee_Shift_Totals';
  write_log;

  merge into employeeeffpershift
  using
  (select w.plant_code,
          w.shift_date,
          w.shift_number,
          w.workspot_code,
          w.job_code,
          w.employee_number,
          sum(w.employeesecondsactual) employeesecondsactual,
          sum(w.employeesecondsnorm) employeesecondsnorm,
          sum(w.employeequantity) employeequantity,
          avg(w.normlevel) normlevel
     from employeeeffperminute w
    where w.shift_date = fp_shift_date
      and w.plant_code = fp_plant_code
    group by w.plant_code,
             w.shift_date,
             w.shift_number,
             w.workspot_code,
             w.job_code,
             w.employee_number
  )
    merge_subquery
on (employeeeffpershift.plant_code = merge_subquery.plant_code and
    employeeeffpershift.shift_date = merge_subquery.shift_date and
    employeeeffpershift.shift_number = merge_subquery.shift_number and
    employeeeffpershift.workspot_code = merge_subquery.workspot_code and
    employeeeffpershift.job_code = merge_subquery.job_code and
    employeeeffpershift.employee_number = merge_subquery.employee_number and
    employeeeffpershift.manual_yn = 'N'
   )
  when not matched then
  insert (employeeeffpershift_id, 
          shift_date, 
          shift_number, 
          plant_code, 
          workspot_code, 
          job_code, 
          employee_number, 
          employeesecondsactual, 
          employeesecondsnorm, 
          employeequantity, 
          normlevel, 
          creationdate, 
          mutationdate, 
          mutator) 
  values
         (seq_employeeeffpershift.nextval,
          merge_subquery.shift_date,
          merge_subquery.shift_number,
          merge_subquery.plant_code,
          merge_subquery.workspot_code,
          merge_subquery.job_code,
          merge_subquery.employee_number,
          merge_subquery.employeesecondsactual,
          merge_subquery.employeesecondsnorm,
          merge_subquery.employeequantity,
          merge_subquery.normlevel,
          sysdate, -- creation date
          sysdate, -- mutation date
          1 -- mutator
          ) 
  when matched then
    update
    set employeesecondsactual = merge_subquery.employeesecondsactual, 
        employeesecondsnorm = merge_subquery.employeesecondsnorm, 
        employeequantity = merge_subquery.employeequantity,
        normlevel = merge_subquery.normlevel, 
        mutationdate = sysdate;

  gv_inserted := SQL%ROWCOUNT;
  commit;
  update_log;
end Update_Employee_Shift_Totals;

---------------------------------------------------------
begin -- main
 BEGIN
  Time_on_Workspot_Minute_Level;

  Time_on_Employee_Minute_Level;

  Update_Qty_Employee_Minute_Lvl;

  Update_Employee_Shift_Totals;

  EXCEPTION
     WHEN OTHERS THEN
      begin
        gv_last_error_message := SQLERRM;
        ROLLBACK;
         update abslog l
         set l.logmessage = Substr(gv_section || ' Recalc Failed. ' || gv_last_error_message,1,512),
             l.priority = 3 -- error
         where l.rowid = gv_abslog_rowid;         
        commit;
      end;
  END;
end; -- RECALC_EFFICIENCY
/

CREATE OR REPLACE PROCEDURE "RECALC_EFFICIENCY_SHIFT"(fp_plant_code varchar2,
                                               fp_shift_date date,
                                               fp_shift_number number) as
-- This procedure recalculates the time in workspoteffperminute and employeeeffperminute.
-- This procedure is called by maintain timerecoring scans, after scans are manually adjusted or added or deleted.
-- Revisions:
-- Date        By       Version   Change
-- 11/06/2015  ABS HVH  1.0       Created new.
gv_inserted           number;
gv_last_error_message varchar2(2000);
gv_logtimestamp       timestamp;
gv_abslog_rowid       rowid;
gv_section            varchar2(40);

---------------------------------------------------------
procedure write_log is pragma autonomous_transaction;
begin
  select sysdate into gv_logtimestamp from dual;
  insert into abslog
   (abslog_id,
    computer_name,
    abslog_timestamp,
    logsource,
    priority,
    systemuser,
    logmessage)
   values (
    0, -- determined by trigger
    '-', -- computer name
    gv_logtimestamp,
    'RECALC_EFFICIENCY_SHIFT', -- logsource
    1,
    '-', -- systemuser
    'Start Load ' || gv_section)
    returning rowid into gv_abslog_rowid;
  commit;
end write_log;

---------------------------------------------------------
procedure update_log is pragma autonomous_transaction;
begin
  update abslog l
    set l.logmessage = gv_section || ' updated ' || to_char(gv_inserted) || ' rows inserted in ' ||
                       to_char(EXTRACT(DAY FROM(sysdate - gv_logtimestamp)) * 24 * 60 * 60 +
                               EXTRACT(HOUR FROM(sysdate - gv_logtimestamp)) * 60 * 60 +
                               EXTRACT(MINUTE FROM(sysdate - gv_logtimestamp)) * 60 +
                               EXTRACT(SECOND FROM(sysdate - gv_logtimestamp))) || ' seconds.'
  where l.rowid = gv_abslog_rowid;
  commit;
end update_log;

---------------------------------------------------------
procedure PQ_to_Workspot_Minute_Level is
  l_loopdate date;
  l_diff number;
  l_intervalstarttime date;
  l_intervalendtime date;
  l_workspotsecondsnorm number;
  l_workspotquantity number;
begin
  gv_section := 'PQ_to_Workspot_Minute_Level-RES';
  gv_inserted := 0;

  update workspoteffperminute w
     set w.workspotsecondsnorm = 0,
        w.normlevel = 0,
        w.workspotquantity = 0,
        w.mutationdate = sysdate
  where w.plant_code = fp_plant_code
    and w.shift_date = fp_shift_date
    and w.shift_number = fp_shift_number;

  begin
    for r_pq in
      (select
         pq.start_date,
         pq.end_date,
         pq.plant_code,
         pq.workspot_code,
         pq.job_code,
         pq.shift_date,
         pq.shift_number,
         j.norm_prod_level,
         pq.quantity
       from productionquantity pq
         inner join jobcode j on
           pq.plant_code = j.plant_code and
           pq.workspot_code = j.workspot_code and
           pq.job_code = j.job_code
       where
         pq.plant_code = fp_plant_code and
         pq.shift_number = fp_shift_number and
         pq.shift_date = fp_shift_date
       order by pq.start_date)
    loop
      -- difference in minutes
      select TRUNC((r_pq.end_date - r_pq.start_date)*24*60)
      into l_diff
      from DUAL;
      -- store per minute
      l_loopdate := r_pq.start_date;
      while l_loopdate < r_pq.end_date loop
        l_intervalstarttime := l_loopdate;
        l_intervalendtime := l_loopdate + 1/1440; -- 1 minute later
        if r_pq.norm_prod_level <> 0 then
          l_workspotsecondsnorm := r_pq.quantity * 3600 / r_pq.norm_prod_level;
        else
          l_workspotsecondsnorm := 0;
        end if;
        l_workspotquantity := r_pq.quantity / l_diff;

        merge into workspoteffperminute
        using dual
        on (workspoteffperminute.plant_code = fp_plant_code and
          workspoteffperminute.shift_date = fp_shift_date and
          workspoteffperminute.shift_number = fp_shift_number and
          workspoteffperminute.workspot_code = r_pq.workspot_code and
          workspoteffperminute.job_code = r_pq.job_code and
          workspoteffperminute.intervalstarttime = l_intervalstarttime
         )
        when not matched then
          insert
             (workspoteffperminute_id,
              shift_date,
              shift_number,
              plant_code,
              workspot_code,
              job_code,
              intervalstarttime,
              intervalendtime,
              workspotsecondsactual,
              workspotsecondsnorm,
              workspotquantity,
              normlevel,
              start_timestamp,
              end_timestamp,
              creationdate,
              mutationdate,
              mutator)
          values
            (seq_workspoteffperminute.nextval,
             fp_shift_date,
             fp_shift_number,
             fp_plant_code,
             r_pq.workspot_code,
             r_pq.job_code,
             l_intervalstarttime,
             l_intervalendtime,
             0,
             l_workspotsecondsnorm,
             l_workspotquantity,
             r_pq.norm_prod_level,
             l_intervalstarttime,
             l_intervalendtime,
             sysdate, -- creationdate
             sysdate, -- mutationdate
             1 -- mutator
            )
        when matched then
          update
            set workspotsecondsnorm = l_workspotsecondsnorm,
              normlevel = r_pq.norm_prod_level,
              workspotquantity = l_workspotquantity,
              mutationdate = sysdate;

        gv_inserted := gv_inserted + SQL%ROWCOUNT;
        commit;

        l_loopdate := l_loopdate + 1/1440; -- 1 minute later
      end loop;
    end loop;
  end;
  update_log;

end PQ_to_Workspot_Minute_Level;

---------------------------------------------------------
procedure Time_on_Workspot_Minute_Level is
begin
  gv_section := 'Time_on_Workspot_Minute_Level-RES';
  write_log;

  update workspoteffperminute w
    set w.workspotsecondsactual = 0,
        w.mutationdate = sysdate
  where w.plant_code = fp_plant_code
    and w.shift_date = fp_shift_date
    and w.shift_number = fp_shift_number;

  merge into workspoteffperminute
  using
  (select v.plant_code,
          v.shift_date,
          v.shift_number,
          v.workspot_code,
          v.job_code,
          v.intervalstarttime,
          max(v.intervalendtime) intervalendtime,
          min(v.start_timestamp) start_timestamp,
          max(v.end_timestamp) end_timestamp,
          sum(v.seconds) seconds,
          avg(v.norm_level) norm_level
     from v_timereg_by_minute_corr v
    where v.plant_code = fp_plant_code
      and v.shift_date = fp_shift_date
      and v.shift_number = fp_shift_number
    group by v.plant_code,
             v.shift_date,
             v.shift_number,
             v.workspot_code,
             v.job_code,
             v.intervalstarttime
  )
    merge_subquery
on (workspoteffperminute.plant_code = merge_subquery.plant_code and
    workspoteffperminute.shift_date = merge_subquery.shift_date and
    workspoteffperminute.shift_number = merge_subquery.shift_number and
    workspoteffperminute.workspot_code = merge_subquery.workspot_code and
    workspoteffperminute.job_code = merge_subquery.job_code and
    workspoteffperminute.intervalstarttime = merge_subquery.intervalstarttime
   )
  when not matched then
    insert
       (workspoteffperminute_id,
        shift_date,
        shift_number,
        plant_code,
        workspot_code,
        job_code,
        intervalstarttime,
        intervalendtime,
        workspotsecondsactual,
        workspotsecondsnorm,
        workspotquantity,
        normlevel,
        start_timestamp,
        end_timestamp,
        creationdate,
        mutationdate,
        mutator)
    values
      (seq_workspoteffperminute.nextval,
       merge_subquery.shift_date,
       merge_subquery.shift_number,
       merge_subquery.plant_code,
       merge_subquery.workspot_code,
       merge_subquery.job_code,
       merge_subquery.intervalstarttime,
       merge_subquery.intervalendtime,
       merge_subquery.seconds,
       0, -- workspotsecondsnorm
       0, -- workspotquantity
       merge_subquery.norm_level,
       merge_subquery.start_timestamp,
       merge_subquery.end_timestamp,
       sysdate, -- creationdate
       sysdate, -- mutationdate
       1 -- mutator
      )
  when matched then
    update
    set workspotsecondsactual = merge_subquery.seconds,
        normlevel = merge_subquery.norm_level,
        mutationdate = sysdate;

  gv_inserted := SQL%ROWCOUNT;
  commit;
  update_log;
end Time_on_Workspot_Minute_Level;

---------------------------------------------------------
procedure Time_on_Employee_Minute_Level is
begin
  gv_section := 'Time_on_Employee_Minute_Level-RES';
  write_log;

  update employeeeffperminute w
    set w.employeesecondsactual = 0,
        w.mutationdate = sysdate
  where w.plant_code = fp_plant_code
    and w.shift_date = fp_shift_date
    and w.shift_number = fp_shift_number;

  merge into employeeeffperminute
  using
    (select v.plant_code,
            v.shift_date,
            v.shift_number,
            v.workspot_code,
            v.job_code,
            v.employee_number,
            v.intervalstarttime,
            max(v.intervalendtime) intervalendtime,
            min(v.start_timestamp) start_timestamp,
            max(v.end_timestamp) end_timestamp,
            sum(v.seconds) seconds,
            avg(v.norm_level) norm_level
       from v_timereg_by_minute_corr v
      where v.plant_code = fp_plant_code
        and v.shift_date = fp_shift_date
        and v.shift_number = fp_shift_number
    group by v.plant_code,
             v.shift_date,
             v.shift_number,
             v.workspot_code,
             v.job_code,
             v.employee_number,
             v.intervalstarttime
    )
      merge_subquery
  on (employeeeffperminute.plant_code = merge_subquery.plant_code and
      employeeeffperminute.shift_date = merge_subquery.shift_date and
      employeeeffperminute.shift_number = merge_subquery.shift_number and
      employeeeffperminute.workspot_code = merge_subquery.workspot_code and
      employeeeffperminute.job_code = merge_subquery.job_code and
      employeeeffperminute.employee_number = merge_subquery.employee_number and
      employeeeffperminute.intervalstarttime = merge_subquery.intervalstarttime
     )
    when not matched then
      insert
         (employeeeffperminute_id,
          shift_date,
          shift_number,
          plant_code,
          workspot_code,
          job_code,
          employee_number,
          intervalstarttime,
          intervalendtime,
          employeesecondsactual,
          employeesecondsnorm,
          employeequantity,
          normlevel,
          start_timestamp,
          end_timestamp,
          creationdate,
          mutationdate,
          mutator)
      values
        (seq_employeeeffperminute.nextval,
         merge_subquery.shift_date,
         merge_subquery.shift_number,
         merge_subquery.plant_code,
         merge_subquery.workspot_code,
         merge_subquery.job_code,
         merge_subquery.employee_number,
         merge_subquery.intervalstarttime,
         merge_subquery.intervalendtime,
         merge_subquery.seconds,
         0, -- employeesecondsnorm
         0, -- employeequantity
         merge_subquery.norm_level,
         merge_subquery.start_timestamp,
         merge_subquery.end_timestamp,
         sysdate, -- creationdate
         sysdate, -- mutationdate
         1 -- mutator
        )
    when matched then
      update
      set employeesecondsactual = merge_subquery.seconds,
          normlevel = merge_subquery.norm_level,
          mutationdate = sysdate;

  gv_inserted := SQL%ROWCOUNT;
  commit;
  update_log;
end Time_on_Employee_Minute_Level;

---------------------------------------------------------
procedure Update_Qty_Employee_Minute_Lvl is
begin
  gv_section := 'Update_Qty_Employee_Minute_Lvl-RES';
  write_log;

  merge into employeeeffperminute
  using
    (select e.plant_code,
            e.shift_date,
            e.shift_number,
            e.workspot_code,
            e.job_code,
            e.employee_number,
            e.intervalstarttime,
            case when w.workspotsecondsactual = 0
             then 0
             else e.employeesecondsactual / w.workspotsecondsactual * w.workspotquantity
            end employeequantity, -- equal share
            case when w.normlevel = 0 or w.workspotsecondsactual  = 0
              then 0
              else e.employeesecondsactual / w.workspotsecondsactual * w.workspotsecondsnorm
            end employeesecondsnorm,
            w.normlevel
       from employeeeffperminute e, workspoteffperminute w
      where w.plant_code = e.plant_code
        and w.shift_date = e.shift_date
        and w.shift_number = e.shift_number
        and w.workspot_code = e.workspot_code
        and w.job_code = e.job_code
        and w.intervalstarttime = e.intervalstarttime
        and w.plant_code = fp_plant_code
        and w.shift_date = fp_shift_date
        and w.shift_number = fp_shift_number
    )
      merge_subquery
  on (employeeeffperminute.plant_code = merge_subquery.plant_code and
      employeeeffperminute.shift_date = merge_subquery.shift_date and
      employeeeffperminute.shift_number = merge_subquery.shift_number and
      employeeeffperminute.workspot_code = merge_subquery.workspot_code and
      employeeeffperminute.job_code = merge_subquery.job_code and
      employeeeffperminute.employee_number = merge_subquery.employee_number and
      employeeeffperminute.intervalstarttime = merge_subquery.intervalstarttime
     )
    when matched then
      update
      set employeequantity = merge_subquery.employeequantity,
          employeesecondsnorm = merge_subquery.employeesecondsnorm,
          normlevel = merge_subquery.normlevel,
          mutationdate = sysdate;

  gv_inserted := SQL%ROWCOUNT;
  commit;
  update_log;
end Update_Qty_Employee_Minute_Lvl;

---------------------------------------------------------
 procedure Update_Employee_Shift_Totals is
 begin
  gv_section := 'Update_Employee_Shift_Totals-RES';
  write_log;

  merge into employeeeffpershift
  using
  (select w.plant_code,
          w.shift_date,
          w.shift_number,
          w.workspot_code,
          w.job_code,
          w.employee_number,
          sum(w.employeesecondsactual) employeesecondsactual,
          sum(w.employeesecondsnorm) employeesecondsnorm,
          sum(w.employeequantity) employeequantity,
          avg(w.normlevel) normlevel
     from employeeeffperminute w
    where w.shift_date = fp_shift_date
      and w.plant_code = fp_plant_code
      and w.shift_number = fp_shift_number
    group by w.plant_code,
             w.shift_date,
             w.shift_number,
             w.workspot_code,
             w.job_code,
             w.employee_number
  )
    merge_subquery
on (employeeeffpershift.plant_code = merge_subquery.plant_code and
    employeeeffpershift.shift_date = merge_subquery.shift_date and
    employeeeffpershift.shift_number = merge_subquery.shift_number and
    employeeeffpershift.workspot_code = merge_subquery.workspot_code and
    employeeeffpershift.job_code = merge_subquery.job_code and
    employeeeffpershift.employee_number = merge_subquery.employee_number and
    employeeeffpershift.manual_yn = 'N'
   )
  when not matched then
  insert (employeeeffpershift_id,
          shift_date,
          shift_number,
          plant_code,
          workspot_code,
          job_code,
          employee_number,
          employeesecondsactual,
          employeesecondsnorm,
          employeequantity,
          normlevel,
          creationdate,
          mutationdate,
          mutator)
  values
         (seq_employeeeffpershift.nextval,
          merge_subquery.shift_date,
          merge_subquery.shift_number,
          merge_subquery.plant_code,
          merge_subquery.workspot_code,
          merge_subquery.job_code,
          merge_subquery.employee_number,
          merge_subquery.employeesecondsactual,
          merge_subquery.employeesecondsnorm,
          merge_subquery.employeequantity,
          merge_subquery.normlevel,
          sysdate, -- creation date
          sysdate, -- mutation date
          1 -- mutator
          )
  when matched then
    update
    set employeesecondsactual = merge_subquery.employeesecondsactual,
        employeesecondsnorm = merge_subquery.employeesecondsnorm,
        employeequantity = merge_subquery.employeequantity,
        normlevel = merge_subquery.normlevel,
        mutationdate = sysdate;

  gv_inserted := SQL%ROWCOUNT;
  commit;
  update_log;
end Update_Employee_Shift_Totals;

---------------------------------------------------------
begin -- main
 BEGIN
  PQ_to_Workspot_Minute_Level;

  Time_on_Workspot_Minute_Level;

  Time_on_Employee_Minute_Level;

  Update_Qty_Employee_Minute_Lvl;

  Update_Employee_Shift_Totals;

  EXCEPTION
     WHEN OTHERS THEN
      begin
        gv_last_error_message := SQLERRM;
        ROLLBACK;
         update abslog l
         set l.logmessage = Substr(gv_section || ' Recalc Failed-RES. ' || gv_last_error_message,1,512),
             l.priority = 3 -- error
         where l.rowid = gv_abslog_rowid;
        commit;
      end;
  END;
end; -- RECALC_EFFICIENCY_SHIFT
/

commit;

