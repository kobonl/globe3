--
-- VEI001-7 Interface Cockpit <> Pims
--

create or replace view vc_i_workspots as
select /*vc_i_workspots version 0.2*/
 substr(to_char(m."ExtRef"), 1, 4) || to_char(u.counter) workspot_code,
 pt."PLANTCODE" plant_code,
 to_char(u.counter) || ' ' || substr(m."ShortDescription", 1, 17) short_name,
 nvl(substr(m."LongDescription", 1, 27), m."ShortDescription") || ' ' || to_char(u.counter) description,
 sysdate creation_date, sysdate mutation_date, 'Cockpit' mutator,
 substr(to_char(mg."ExtRef"), 1, 6) department_code, substr(to_char(m."ExtRef"), 1, 4) machine_code,
 null date_inactive, 'Y' use_jobcode_yn, 'Y' productive_hour_yn, 'Y' measure_productivity_yn,
 'Y' quant_piece_yn, 'Y' automatic_datacol_yn
from   dbo.v_tblmachines@gtlab m, dbo.tblmachinegroups@gtlab mg, dbo.viewplant@gtlab pt,
       (select rownum counter from user_tables) u
where  u.counter <= m."Positions"
and    mg."idJensen" = m."MachineGroup_idJensen"
and    substr(to_char(m."ExtRef"), 1, 4) is not null
and    substr(to_char(mg."ExtRef"), 1, 6) is not null
/

create or replace view vc_i_machine as
select /*vc_i_machine version 0.1*/
 substr(to_char(m."ExtRef"), 1, 4) machine_code, pt."PLANTCODE" plant_code,
 substr(m."ShortDescription", 1, 20) short_name, nvl(substr(m."LongDescription", 1, 30), m."ShortDescription") description,
 sysdate creation_date, sysdate mutation_date, 'Cockpit' mutator,
 substr(to_char(mg."ExtRef"), 1, 6) department_code
from   dbo.v_tblmachines@gtlab m, dbo.v_tblmachinegroups@gtlab mg, dbo.viewplant@gtlab pt
where  mg."idJensen" = m."MachineGroup_idJensen"
and    substr(to_char(m."ExtRef"), 1, 4) is not null
/

create or replace view vc_i_department as
select /*vc_i_department version 0.1*/
 substr(to_char(x."ExtRef"), 1, 6) department_code, pt."PLANTCODE" plant_code,
 substr(x."ShortDescription", 1, 20) description,
 (select min(bu.businessunit_code) from businessunit bu where bu.plant_code = pt."PLANTCODE") businessunit_code,
 sysdate creationdate, 'Y' direct_hour_yn, sysdate mutationdate, 'Cockpit' mutator,
 'Y' plan_on_workspot_yn
from   dbo.tblmachinegroups@gtlab x, dbo.viewplant@gtlab pt
where  substr(to_char(x."ExtRef"), 1, 6) is not null and x."idJensen" in (select y."MachineGroup_idJensen" from dbo.tblmachines@gtlab y)
/


create or replace package package_cockpit is
  --Version 0.4
  procedure operators_2_cockpit;
  procedure department_2_pims;
  procedure machine_2_pims;
  procedure workspot_2_pims;
  procedure scans_2_cockpit;
end package_cockpit;
/
create or replace package body package_cockpit is

  --internal procedures
  procedure write_log(lfp_logmessage    in abslog.logmessage%type,
                      lfp_computer_name in varchar2,
                      lfp_priority      in abslog.priority%type) is
    pragma autonomous_transaction;
  begin
    /*
    Write message in ABSLOG with these priorities
    1 = Message
    2 = Warning
    3 = Error
    4 = Stack trace
    5 = Debug info
    */
    dbms_output.put_line(lfp_logmessage);
    insert into abslog
      (abslog_id, computer_name, abslog_timestamp, logsource, priority, systemuser, logmessage)
    values
      (seq_abslog.nextval, lfp_computer_name, sysdate /*abslog_timestamp*/, 'package_cockpit'
       /*logsource*/, lfp_priority /*priority*/, 'Cockpit' /*systemuser*/,
       substr(lfp_logmessage, 1, 255));
    --commit log
    commit;
  end write_log;

  procedure operators_2_cockpit is
    lv_message      varchar2(1000);
    lv_computername varchar2(32);

    procedure initialize is
    begin
      lv_computername := 'operators_2_cockpit';
    end initialize;
  begin
    initialize;
    --loop all employees
    lv_message := 'loop employee PIMS';
    for x in (select e.employee_id, e.employee_number extref, e.short_name shortdescription,
                     e.description longdescription,
                     case
                       when e.enddate < sysdate then
                        1
                       else
                        0
                     end retired, e.mutationdate
              from   employee e) loop
      begin
        --update dbo.tbloperators
        lv_message := 'update dbo.tbloperators employee_id ' || to_char(x.employee_id);
        update dbo.tbloperators@gtlab op
        set    op."Retired" = x.retired, op."ShortDescription" = x.shortdescription,
               op."LongDescription" = x.longdescription, op."ExtRef" = x.extref
        where  op."idJensen" = x.employee_id;
        if sql%rowcount = 0 then
          begin
            --insert into dbo.tbloperators
            lv_message := 'insert into dbo.tbloperators employee_id ' || to_char(x.employee_id);
            insert into dbo.tbloperators@gtlab
              ("idJensen", "Retired", "ShortDescription", "LongDescription", "ExtRef")
            values
              (x.employee_id, x.retired, x.shortdescription, x.longdescription, x.extref);
          end;
        end if;
      exception
        when no_data_found then
          begin
            rollback;
            write_log('No data found ' || lv_message, lv_computername, 3);
          end;
        when others then
          begin
            rollback;
            write_log(lv_message || sqlerrm, lv_computername, 3);
          end;
      end;
    end loop; --x
    commit;
  exception
    when no_data_found then
      begin
        rollback;
        write_log('No data found ' || lv_message, lv_computername, 3);
      end;
    when others then
      begin
        rollback;
        write_log(lv_message || sqlerrm, lv_computername, 3);
      end;
  end operators_2_cockpit;

  procedure department_2_pims is
    lv_message      varchar2(1000);
    lv_computername varchar2(32);

    procedure initialize is
    begin
      lv_computername := 'department_2_pims';
    end initialize;
  begin
    initialize;
    for y in (select * from vc_i_department) loop
      begin
        --update department
        lv_message := 'update department ' || y.department_code || ' plant ' || y.plant_code;
        update department t
        set    t.description = y.description,
               t.direct_hour_yn = y.direct_hour_yn, t.mutationdate = y.mutationdate,
               t.mutator = y.mutator, t.plan_on_workspot_yn = y.plan_on_workspot_yn
        where  t.department_code = y.department_code
        and    t.plant_code = y.plant_code;
        if sql%rowcount = 0 then
          begin
            --insert into department
            lv_message := 'insert into department ' || y.department_code || ' plant ' ||
                          y.plant_code;
            insert into department
              (department_code, plant_code, description, businessunit_code, creationdate,
               direct_hour_yn, mutationdate, mutator, plan_on_workspot_yn)
            values
              (y.department_code, y.plant_code, y.description, y.businessunit_code, y.creationdate,
               y.direct_hour_yn, y.mutationdate, y.mutator, y.plan_on_workspot_yn);
          end;
        end if;
      exception
        when no_data_found then
          begin
            rollback;
            write_log('No data found ' || lv_message, lv_computername, 3);
          end;
        when others then
          begin
            rollback;
            write_log(lv_message || sqlerrm, lv_computername, 3);
          end;
      end;
    end loop; --y
    commit;
  exception
    when no_data_found then
      begin
        rollback;
        write_log('No data found ' || lv_message, lv_computername, 3);
      end;
    when others then
      begin
        rollback;
        write_log(lv_message || sqlerrm, lv_computername, 3);
      end;
  end department_2_pims;

  procedure machine_2_pims is
    lv_message      varchar2(1000);
    lv_computername varchar2(32);

    procedure initialize is
    begin
      lv_computername := 'machine_2_pims';
    end initialize;
  begin
    initialize;
    for z in (select vm.machine_code, vm.plant_code, vm.description, vm.short_name, vm.creation_date,
                     vm.mutation_date, vm.mutator, vm.department_code
              from   vc_i_machine vm) loop
      begin
        --update machine
        lv_message := 'update machine ' || z.machine_code || ' plant ' || z.plant_code;
        update machine m
        set    m.description = z.description, m.short_name = z.short_name,
               m.creationdate = z.creation_date, m.mutationdate = z.mutation_date,
               m.mutator = z.mutator, m.department_code = z.department_code
        where  m.machine_code = z.machine_code
        and    m.plant_code = z.plant_code;
        if sql%rowcount = 0 then
          begin
            --insert into machine
            lv_message := 'insert into machine ' || z.machine_code || ' plant ' || z.plant_code;
            insert into machine
              (plant_code, machine_code, description, short_name, creationdate, mutationdate,
               mutator, department_code)
            values
              (z.plant_code, z.machine_code, z.description, z.short_name, z.creation_date,
               z.mutation_date, z.mutator, z.department_code);
          end;
        end if;
      exception
        when no_data_found then
          begin
            rollback;
            write_log('No data found ' || lv_message, lv_computername, 3);
          end;
        when others then
          begin
            rollback;
            write_log(lv_message || sqlerrm, lv_computername, 3);
          end;
      end;
    end loop; --y
    commit;
  exception
    when no_data_found then
      begin
        rollback;
        write_log('No data found ' || lv_message, lv_computername, 3);
      end;
    when others then
      begin
        rollback;
        write_log(lv_message || sqlerrm, lv_computername, 3);
      end;
  end machine_2_pims;

  procedure workspot_2_pims is
    lv_message      varchar2(1000);
    lv_computername varchar2(32);

    procedure initialize is
    begin
      lv_computername := 'workspot_2_pims';
    end initialize;
  begin
    initialize;
    for y in (select * from vc_i_workspots) loop
      begin
        --update workspot
        lv_message := 'update workspots ' || y.workspot_code || ' plant ' || y.plant_code;
        update workspot t
        set    t.short_name = y.short_name, t.description = y.description,
               t.creationdate = y.creation_date, t.mutationdate = y.mutation_date,
               t.mutator = y.mutator, t.department_code = y.department_code,
               t.machine_code = y.machine_code, t.date_inactive = y.date_inactive,
               t.use_jobcode_yn = y.use_jobcode_yn, t.productive_hour_yn = y.productive_hour_yn,
               t.measure_productivity_yn = y.measure_productivity_yn, t.quant_piece_yn = y.quant_piece_yn,
               t.automatic_datacol_yn = y.automatic_datacol_yn
        where  t.workspot_code = y.workspot_code
        and    t.plant_code = y.plant_code;
        if sql%rowcount = 0 then
          begin
            --insert into workspots
            lv_message := 'insert into workspot ' || y.workspot_code || ' plant ' || y.plant_code;
            insert into workspot
              (workspot_code, plant_code, short_name, description, creationdate, mutationdate,
               mutator, department_code, machine_code, date_inactive, use_jobcode_yn,
               productive_hour_yn, measure_productivity_yn, quant_piece_yn, automatic_datacol_yn)
            values
              (y.workspot_code, y.plant_code, y.short_name, y.description, y.creation_date,
               y.mutation_date, y.mutator, y.department_code, y.machine_code, y.date_inactive,
               y.use_jobcode_yn, y.productive_hour_yn, y.measure_productivity_yn, y.quant_piece_yn,
               y.automatic_datacol_yn);
          end;
        end if;
      exception
        when no_data_found then
          begin
            rollback;
            write_log('No data found ' || lv_message, lv_computername, 3);
          end;
        when others then
          begin
            rollback;
            write_log(lv_message || sqlerrm, lv_computername, 3);
          end;
      end;
    end loop; --y
    commit;
  exception
    when no_data_found then
      begin
        rollback;
        write_log('No data found ' || lv_message, lv_computername, 3);
      end;
    when others then
      begin
        rollback;
        write_log(lv_message || sqlerrm, lv_computername, 3);
      end;
  end workspot_2_pims;

  -- Internal function
  function spOperatorLogPims(AMachineCode varchar2, AWorkspotCode varchar2,
    AEmployeeNumber Smallint, ALogDate Timestamp, APlanInfo Integer, AState Integer) return Boolean is
    Result Boolean;
    ret integer;
    lv_message varchar2(1000);
    lv_computername varchar2(32);
    lv_extRef varchar2(12);
    PRAGMA AUTONOMOUS_TRANSACTION;
  begin
    Result := False;
    lv_message := 'spPimsLogOnOff';
    lv_computername := 'scans_2_cockpit';
    lv_extRef := cast(AEmployeeNumber as varchar2);
    begin
      ret := DBMS_HS_PASSTHROUGH.EXECUTE_IMMEDIATE@GTLAB(
        'dbo.spPimsLogOnOffMain "' || lv_extRef || '", "' ||
        to_char(ALogDate, 'YYYY-MM-DD HH24:MI:SS') || '", "' || 
        AMachineCode || '", "' || 
        AWorkspotCode || '", "' || 
        AState || '", "' || 
        APlanInfo || '"');
--      write_log(lv_message || ' ret=' || ret, lv_computername, 3);
      if ret = 0 then
        begin
          commit;
          Result := True;
        end;
      end if;
    exception
      when others then 
        begin
          rollback;
          write_log(lv_message || sqlerrm, lv_computername, 3);
        end;
    end;
    rollback;
    return Result;
  end spOperatorLogPims;

  procedure scans_2_cockpit is
    lv_message      varchar2(1000);
    lv_computername varchar2(32);
    lv_logdate      timestamp;
    procedure initialize is
    begin
      lv_computername := 'scans_2_cockpit';
    end initialize;
  begin
    initialize;
    --loop all scans for shift date
    lv_message := 'loop scans PIMS';

/*
   @MachineCode
  ,@WorkSpotCode
  ,@EmployeeNumber
  ,@Logdate
  ,@PlanInfo
  ,@State
*/
    for x in (select w.machine_code MachineCode,
                     substr(t.workspot_code, length(w.machine_code)+1) WorkspotCode,
                     e.employee_id EmployeeID,
                     e.employee_number EmployeeNumber,
                     t.datetime_in DateTimeIn,
                     t.datetime_out DateTimeOut,
                     t.mutationdate LogDate,
                     0 PlanInfo,
                     case
                       when t.datetime_out is null then 1 -- log-in
                       else 0 -- log-out
                     end State,
                     t.job_code
              from timeregscanning t
                inner join employee e on
                  t.employee_number = e.employee_number
                inner join workspot w on
                  t.plant_code = w.plant_code and
                  t.workspot_code = w.workspot_code
              where t.shift_date = trunc(sysdate) and
                    t.job_code = 'ATWORK' and
                    t.workspot_code is not null and
                    w.machine_code is not null and
                    (
                      (t.in_yn = 'N' and t.processed_yn = 'N')
                      or 
                      (t.out_yn = 'N' and t.processed_yn = 'Y')
                    )
                    or              
                    (t.in_yn is null or t.out_yn is null)
              order by t.DateTime_In                    
                  ) loop
      begin
        begin
          -- insert log info
          if x.state = 1 then
            lv_logdate := x.datetimein;
          else
            lv_logdate := x.datetimeout;        
          end if;
          lv_message := 'insert operator log info for employee ' || to_char(x.employeeid) || ' log-date ' || to_char(lv_logdate);
          if spOperatorLogPims(x.machinecode, x.workspotcode, x.employeenumber, lv_logdate, x.planinfo, x.state) then -- x.logdate->systimestamp
            begin
              if x.State = 1 then -- Log-in
                update timeregscanning t
                set t.in_yn = 'Y'
                where t.employee_number = x.Employeenumber and
                    t.datetime_in = x.Datetimein;
              else -- Log-out
                update timeregscanning t
                set t.in_yn = 'Y',
                t.out_yn = 'Y'
                where t.employee_number = x.Employeenumber and
                    t.datetime_in = x.Datetimein;
              end if;
            end;
          end if;
        exception
          when others then
          begin
            write_log(lv_message || sqlerrm, lv_computername, 3);
          end;
        end;
      end;
    end loop; --x
    commit;
  exception
    when no_data_found then
      begin
        rollback;
        write_log('No data found ' || lv_message, lv_computername, 3);
      end;
    when others then
      begin
        rollback;
        write_log(lv_message || sqlerrm, lv_computername, 3);
      end;
  end scans_2_cockpit;

end package_cockpit;
/

commit;

