--
-- PIM-52 Work schedules
--

-- Create table
create table WORKSCHEDULE
(
  WORKSCHEDULE_ID         NUMBER not null,
  CODE                    VARCHAR2(6) not null,
  DESCRIPTION             VARCHAR2(30) not null,
  REPEATS                 NUMBER default 1 not null,
  STARTDATE               DATE not null,
  CREATIONDATE            DATE not null,
  MUTATIONDATE            DATE not null,
  MUTATOR                 VARCHAR2(20) not null
)
tablespace ABS_DATA
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate primary, unique and foreign key constraints 
alter table WORKSCHEDULE
  add constraint XPKWORKSCHEDULE primary key (WORKSCHEDULE_ID)
  using index 
  tablespace INDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
alter table WORKSCHEDULE
  add constraint SKWORKSCHEDULE unique (CODE)
  using index 
  tablespace INDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

-- Create sequence 
create sequence SEQ_WORKSCHEDULE
minvalue 1
maxvalue 999999999999999999999999
start with 1
increment by 1
nocache;

CREATE OR REPLACE TRIGGER "SETWORKSCHEDULEID"  BEFORE INSERT
ON WORKSCHEDULE FOR EACH ROW
DECLARE
  NEWID INTEGER;
BEGIN
  SELECT SEQ_WORKSCHEDULE.NEXTVAL INTO NEWID FROM DUAL;
  :NEW.WORKSCHEDULE_ID := NEWID;
END;
/
-- Create table
create table WORKSCHEDULELINE
(
  WORKSCHEDULE_ID         NUMBER not null,
  WORKSCHEDULELINE_ID     NUMBER not null,
  DAY1_ABSRSN_CODE        VARCHAR2(1) default '*' not null,
  DAY1_SHIFT_NUMBER       NUMBER default 1 not null,
  DAY1_TYPE               NUMBER default 1 not null,
  DAY1_REPEATS            NUMBER default 1 not null,
  DAY2_ABSRSN_CODE        VARCHAR2(1) default '*' not null,
  DAY2_SHIFT_NUMBER       NUMBER default 1 not null,
  DAY2_TYPE               NUMBER default 1 not null,
  DAY2_REPEATS            NUMBER default 1 not null,
  DAY3_ABSRSN_CODE        VARCHAR2(1) default '*' not null,
  DAY3_SHIFT_NUMBER       NUMBER default 1 not null,
  DAY3_TYPE               NUMBER default 1 not null,
  DAY3_REPEATS            NUMBER default 1 not null,
  DAY4_ABSRSN_CODE        VARCHAR2(1) default '*' not null,
  DAY4_SHIFT_NUMBER       NUMBER default 1 not null,
  DAY4_TYPE               NUMBER default 1 not null,
  DAY4_REPEATS            NUMBER default 1 not null,
  DAY5_ABSRSN_CODE        VARCHAR2(1) default '*' not null,
  DAY5_SHIFT_NUMBER       NUMBER default 1 not null,
  DAY5_TYPE               NUMBER default 1 not null,
  DAY5_REPEATS            NUMBER default 1 not null,
  DAY6_ABSRSN_CODE        VARCHAR2(1) default '*' not null,
  DAY6_SHIFT_NUMBER       NUMBER default 1 not null,
  DAY6_TYPE               NUMBER default 1 not null,
  DAY6_REPEATS            NUMBER default 1 not null,
  DAY7_ABSRSN_CODE        VARCHAR2(1) default '*' not null,
  DAY7_SHIFT_NUMBER       NUMBER default 1 not null,
  DAY7_TYPE               NUMBER default 1 not null,
  DAY7_REPEATS            NUMBER default 1 not null,
  CREATIONDATE            DATE not null,
  MUTATIONDATE            DATE not null,
  MUTATOR                 VARCHAR2(20) not null
)
tablespace ABS_DATA
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate primary, unique and foreign key constraints 
alter table WORKSCHEDULELINE
  add constraint XPKWORKSCHEDULELINE primary key (WORKSCHEDULE_ID, WORKSCHEDULELINE_ID)
  using index 
  tablespace INDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

-- Create sequence 
create sequence SEQ_WORKSCHEDULELINE
minvalue 1
maxvalue 999999999999999999999999
start with 1
increment by 1
nocache;

CREATE OR REPLACE TRIGGER "SETWORKSCHEDULELINEID"  BEFORE INSERT
ON WORKSCHEDULELINE FOR EACH ROW
DECLARE
  NEWID INTEGER;
BEGIN
  SELECT SEQ_WORKSCHEDULELINE.NEXTVAL INTO NEWID FROM DUAL;
  :NEW.WORKSCHEDULELINE_ID := NEWID;
END;
/

declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'alter table EMPLOYEECONTRACT add WORKSCHEDULE_ID NUMBER';
    exception when column_exists then null;
end;
/

-- Add menu item
INSERT INTO PIMSMENUGROUP 
SELECT t.group_name, 123107, 'Y', 'Y', 'N', 123100, 'Work Schedule', 7, SYSDATE, SYSDATE, 'ABS' 
FROM pimsusergroup t;

-- Add menu item
INSERT INTO PIMSMENUGROUP 
SELECT t.group_name, 123108, 'Y', 'Y', 'N', 123100, 'Work Schedule Details', 8, SYSDATE, SYSDATE, 'ABS' 
FROM pimsusergroup t;

UPDATE PIMSDBVERSION SET DBVERSION = '181';

commit;


