--
-- GLOB3-204 Personal Time Off USA
--

--
-- CONTRACTGROUP
--
-- Add/modify columns 
declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'alter table CONTRACTGROUP add MAX_PTO_MINUTE NUMBER(10) default 0';
    exception when column_exists then null;
end;
/

create table PTODEFINITION
(
  CONTRACTGROUP_CODE VARCHAR2(6) not null,
  LINE_NUMBER        NUMBER(10) default 0 not null,
  FROMEMPYEARS       NUMBER(10) default 0 not null,
  TILLEMPYEARS       NUMBER(10) default 0 not null,
  PTOMINUTE          NUMBER(10) default 0 not null,
  CREATIONDATE       DATE not null,
  MUTATIONDATE       DATE not null,
  MUTATOR            VARCHAR2(20) not null
)
tablespace ABS_DATA
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate primary, unique and foreign key constraints 
alter table PTODEFINITION
  add constraint XPKPTODEFINITION primary key (CONTRACTGROUP_CODE, LINE_NUMBER)
  using index 
  tablespace INDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
alter table PTODEFINITION
  add constraint PTODEF_CONTRACTGROUP foreign key (CONTRACTGROUP_CODE)
  references CONTRACTGROUP (CONTRACTGROUP_CODE);
/

INSERT INTO PIMSMENUGROUP 
SELECT t.group_name, 121304, 'Y', 'Y', 'N', 121300, 'PTO definitions', 4, SYSDATE, SYSDATE, 'ABS' 
FROM pimsusergroup t;

--
-- GLOB3-209 Employee Information Page
--

--
-- EMPLOTEE
--
-- Add/modify columns 
declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'alter table EMPLOYEE add ETHNICITY VARCHAR2(30)';
    exception when column_exists then null;
end;
/

--
-- EMPLOTEE
--
-- Add/modify columns 
declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'alter table EMPLOYEE add JOBTITLE VARCHAR2(30)';
    exception when column_exists then null;
end;
/

--
-- EMPLOTEE
--
-- Add/modify columns 
declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'alter table EMPLOYEE add MARITALSTATE VARCHAR2(30)';
    exception when column_exists then null;
end;
/

commit;

