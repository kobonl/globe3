--
-- PIM-322 TimeRegScan-dialog and readonly for one tab
--

-- Create/Recreate indexes 
create index XPMUTATIONDATE on REQUESTEARLYLATE (mutationdate)
  tablespace INDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

commit;
