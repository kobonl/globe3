-- 
-- PIM-213 Show workspot employee efficiency
--

create or replace view v_emp15mineff as
select /*+ ordered index(r XIE3EMPLOYEEEFFPERMINUTE)*/
       r.plant_code,
       r.workspot_code,
       r.employee_number,
       round(sum(r.employeequantity)) qty,
       sum(r.employeesecondsactual) secondsactual,
       sum(r.employeesecondsnorm) secondsnorm,
       case when sum(r.employeesecondsactual) = 0
         then 0
         else sum(r.employeesecondsnorm) / sum(r.employeesecondsactual) * 100
       end eff
  from v_realtimeeffperiod v
 inner join employeeeffperminute r on (r.plant_code = v.plant_code and
                                       r.shift_date = v.current_shiftdate and
                                       r.intervalstarttime >= v.intervalendtime - 15/24/60 and
                                       r.intervalstarttime <= v.intervalendtime)
 where r.normlevel > 0
 group by r.plant_code,
          r.workspot_code,
          r.employee_number
/

create or replace view v_emp30mineff as
select /*+ ordered index(r XIE3EMPLOYEEEFFPERMINUTE)*/
       r.plant_code,
       r.workspot_code,
       r.employee_number,
       round(sum(r.employeequantity)) qty,
       sum(r.employeesecondsactual) secondsactual,
       sum(r.employeesecondsnorm) secondsnorm,
       case when sum(r.employeesecondsactual) = 0
         then 0
         else sum(r.employeesecondsnorm) / sum(r.employeesecondsactual) * 100
       end eff
  from v_realtimeeffperiod v
 inner join employeeeffperminute r on (r.plant_code = v.plant_code and
                                       r.shift_date = v.current_shiftdate and
                                       r.intervalstarttime >= v.intervalendtime - 30/24/60 and
                                       r.intervalstarttime <= v.intervalendtime)
 where r.normlevel > 0
 group by r.plant_code,
          r.workspot_code,
          r.employee_number
/

create or replace view v_emp60mineff as
select /*+ ordered index(r XIE3EMPLOYEEEFFPERMINUTE)*/
       r.plant_code,
       r.workspot_code,
       r.employee_number,
       round(sum(r.employeequantity)) qty,
       sum(r.employeesecondsactual) secondsactual,
       sum(r.employeesecondsnorm) secondsnorm,
       case when sum(r.employeesecondsactual) = 0
         then 0
         else sum(r.employeesecondsnorm) / sum(r.employeesecondsactual) * 100
       end eff
  from v_realtimeeffperiod v
 inner join employeeeffperminute r on (r.plant_code = v.plant_code and
                                       r.shift_date = v.current_shiftdate and
                                       r.intervalstarttime >= v.intervalendtime - 60/24/60 and
                                       r.intervalstarttime <= v.intervalendtime)
 where r.normlevel > 0
 group by r.plant_code,
          r.workspot_code,
          r.employee_number
/

create or replace view v_emptodaymineff as
select /*+ ordered index(r XIE3EMPLOYEEEFFPERMINUTE)*/
       r.plant_code,
       r.workspot_code,
       r.employee_number,
       round(sum(r.employeequantity)) qty,
       sum(r.employeesecondsactual) secondsactual,
       sum(r.employeesecondsnorm) secondsnorm,
       case when sum(r.employeesecondsactual) = 0
         then 0
         else sum(r.employeesecondsnorm) / sum(r.employeesecondsactual) * 100
       end eff
  from v_realtimeeffperiod v
 inner join employeeeffperminute r on (r.plant_code = v.plant_code and
                                       r.shift_date = v.current_shiftdate and
                                       r.intervalstarttime <= v.intervalendtime)
 where r.normlevel > 0
 group by r.plant_code,
          r.workspot_code,
          r.employee_number
/

create or replace view v_emp1mineff as
select /*+ ordered index(r XIE3EMPLOYEEEFFPERMINUTE)*/
       r.plant_code,
       r.workspot_code,
       r.employee_number,
       round(sum(r.employeequantity)) qty,
       sum(r.employeesecondsactual) secondsactual,
       sum(r.employeesecondsnorm) secondsnorm,
       case when sum(r.employeesecondsactual) = 0
         then 0
         else sum(r.employeesecondsnorm) / sum(r.employeesecondsactual) * 100
       end eff
  from v_realtimeeffperiod v
 inner join employeeeffperminute r on (r.plant_code = v.plant_code and
                                       r.shift_date = v.current_shiftdate and
                                       r.intervalstarttime >= v.intervalendtime - 1/24/60 and
                                       r.intervalstarttime <= v.intervalendtime)
 where r.normlevel > 0
 group by r.plant_code,
          r.workspot_code,
          r.employee_number
/
          
UPDATE PIMSDBVERSION SET DBVERSION = '183';

commit;

