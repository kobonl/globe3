--
--GLOB3-284 Week start at Wednesday
--

-- Create table to copy original contents in
create table STANDARDOCCUPATION2
(
  PLANT_CODE         VARCHAR2(6) not null,
  DEPARTMENT_CODE    VARCHAR2(6) not null,
  WORKSPOT_CODE      VARCHAR2(6) not null,
  SHIFT_NUMBER       NUMBER(10) not null,
  DAY_OF_WEEK        NUMBER(10) not null,
  OCC_A_TIMEBLOCK_1  NUMBER(10),
  OCC_A_TIMEBLOCK_2  NUMBER(10),
  OCC_A_TIMEBLOCK_3  NUMBER(10),
  OCC_A_TIMEBLOCK_4  NUMBER(10),
  OCC_B_TIMEBLOCK_1  NUMBER(10),
  OCC_B_TIMEBLOCK_2  NUMBER(10),
  OCC_B_TIMEBLOCK_3  NUMBER(10),
  OCC_B_TIMEBLOCK_4  NUMBER(10),
  OCC_C_TIMEBLOCK_1  NUMBER(10),
  OCC_C_TIMEBLOCK_2  NUMBER(10),
  OCC_C_TIMEBLOCK_3  NUMBER(10),
  OCC_C_TIMEBLOCK_4  NUMBER(10),
  FIXED_YN           VARCHAR2(1 BYTE) default 'N',
  CREATIONDATE       DATE not null,
  MUTATIONDATE       DATE not null,
  MUTATOR            VARCHAR2(20) not null,
  OCC_A_TIMEBLOCK_5  NUMBER(10),
  OCC_A_TIMEBLOCK_6  NUMBER(10),
  OCC_A_TIMEBLOCK_7  NUMBER(10),
  OCC_A_TIMEBLOCK_8  NUMBER(10),
  OCC_A_TIMEBLOCK_9  NUMBER(10),
  OCC_A_TIMEBLOCK_10 NUMBER(10),
  OCC_B_TIMEBLOCK_5  NUMBER(10),
  OCC_B_TIMEBLOCK_6  NUMBER(10),
  OCC_B_TIMEBLOCK_7  NUMBER(10),
  OCC_B_TIMEBLOCK_8  NUMBER(10),
  OCC_B_TIMEBLOCK_9  NUMBER(10),
  OCC_B_TIMEBLOCK_10 NUMBER(10),
  OCC_C_TIMEBLOCK_5  NUMBER(10),
  OCC_C_TIMEBLOCK_6  NUMBER(10),
  OCC_C_TIMEBLOCK_7  NUMBER(10),
  OCC_C_TIMEBLOCK_8  NUMBER(10),
  OCC_C_TIMEBLOCK_9  NUMBER(10),
  OCC_C_TIMEBLOCK_10 NUMBER(10)
)
tablespace ABS_DATA
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate primary, unique and foreign key constraints 
alter table STANDARDOCCUPATION2
  add constraint XPKSTANDARDOCCUPATION2 primary key (PLANT_CODE, DEPARTMENT_CODE, WORKSPOT_CODE, SHIFT_NUMBER, DAY_OF_WEEK)
  using index 
  tablespace INDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
/

-- Create table
create table STANDARDAVAILABILITY2
(
  PLANT_CODE             VARCHAR2(6) not null,
  DAY_OF_WEEK            NUMBER(10) not null,
  SHIFT_NUMBER           NUMBER(10) not null,
  EMPLOYEE_NUMBER        NUMBER(10) not null,
  AVAILABLE_TIMEBLOCK_1  VARCHAR2(1 BYTE),
  AVAILABLE_TIMEBLOCK_2  VARCHAR2(1 BYTE),
  AVAILABLE_TIMEBLOCK_3  VARCHAR2(1 BYTE),
  AVAILABLE_TIMEBLOCK_4  VARCHAR2(1 BYTE),
  CREATIONDATE           DATE not null,
  MUTATIONDATE           DATE not null,
  MUTATOR                VARCHAR2(20) not null,
  AVAILABLE_TIMEBLOCK_5  VARCHAR2(1),
  AVAILABLE_TIMEBLOCK_6  VARCHAR2(1),
  AVAILABLE_TIMEBLOCK_7  VARCHAR2(1),
  AVAILABLE_TIMEBLOCK_8  VARCHAR2(1),
  AVAILABLE_TIMEBLOCK_9  VARCHAR2(1),
  AVAILABLE_TIMEBLOCK_10 VARCHAR2(1)
)
tablespace ABS_DATA
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate primary, unique and foreign key constraints 
alter table STANDARDAVAILABILITY2
  add constraint XPKSTANDARDAVAILABILITY2 primary key (PLANT_CODE, DAY_OF_WEEK, SHIFT_NUMBER, EMPLOYEE_NUMBER)
  using index 
  tablespace INDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
/

-- Create table
create table STANDARDEMPLOYEEPLANNING2
(
  PLANT_CODE             VARCHAR2(6) not null,
  DAY_OF_WEEK            NUMBER(10) not null,
  SHIFT_NUMBER           NUMBER(10) not null,
  DEPARTMENT_CODE        VARCHAR2(6) not null,
  WORKSPOT_CODE          VARCHAR2(6) not null,
  EMPLOYEE_NUMBER        NUMBER(10) not null,
  SCHEDULED_TIMEBLOCK_1  VARCHAR2(1 BYTE),
  SCHEDULED_TIMEBLOCK_2  VARCHAR2(1 BYTE),
  SCHEDULED_TIMEBLOCK_3  VARCHAR2(1 BYTE),
  SCHEDULED_TIMEBLOCK_4  VARCHAR2(1 BYTE),
  CREATIONDATE           DATE not null,
  MUTATIONDATE           DATE not null,
  MUTATOR                VARCHAR2(20) not null,
  SCHEDULED_TIMEBLOCK_5  VARCHAR2(1),
  SCHEDULED_TIMEBLOCK_6  VARCHAR2(1),
  SCHEDULED_TIMEBLOCK_7  VARCHAR2(1),
  SCHEDULED_TIMEBLOCK_8  VARCHAR2(1),
  SCHEDULED_TIMEBLOCK_9  VARCHAR2(1),
  SCHEDULED_TIMEBLOCK_10 VARCHAR2(1)
)
tablespace ABS_DATA
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate primary, unique and foreign key constraints 
alter table STANDARDEMPLOYEEPLANNING2
  add constraint XPKSTANDARDEMPLOYEEPLANNING2 primary key (PLANT_CODE, DAY_OF_WEEK, SHIFT_NUMBER, DEPARTMENT_CODE, WORKSPOT_CODE, EMPLOYEE_NUMBER)
  using index 
  tablespace INDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
/  

-- Create table
create table SHIFT2
(
  SHIFT_NUMBER    NUMBER(10) not null,
  PLANT_CODE      VARCHAR2(6) not null,
  DESCRIPTION     VARCHAR2(30) not null,
  CREATIONDATE    DATE not null,
  MUTATIONDATE    DATE not null,
  MUTATOR         VARCHAR2(20) not null,
  STARTTIME1      DATE,
  ENDTIME1        DATE,
  STARTTIME2      DATE,
  ENDTIME2        DATE,
  STARTTIME3      DATE,
  ENDTIME3        DATE,
  STARTTIME4      DATE,
  ENDTIME4        DATE,
  STARTTIME5      DATE,
  ENDTIME5        DATE,
  STARTTIME6      DATE,
  ENDTIME6        DATE,
  STARTTIME7      DATE,
  ENDTIME7        DATE,
  HOURTYPE_NUMBER NUMBER(10)
)
tablespace ABS_DATA
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate primary, unique and foreign key constraints 
alter table SHIFT2
  add constraint XPKSHIFT2 primary key (PLANT_CODE, SHIFT_NUMBER)
  using index 
  tablespace INDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
/

-- Create table
create table TIMEBLOCKPERSHIFT2
(
  SHIFT_NUMBER     NUMBER(10) not null,
  PLANT_CODE       VARCHAR2(6) not null,
  CREATIONDATE     DATE not null,
  TIMEBLOCK_NUMBER NUMBER(10) not null,
  MUTATIONDATE     DATE not null,
  DESCRIPTION      VARCHAR2(30) not null,
  MUTATOR          VARCHAR2(20) not null,
  STARTTIME1       DATE,
  ENDTIME1         DATE,
  STARTTIME2       DATE,
  ENDTIME2         DATE,
  STARTTIME3       DATE,
  ENDTIME3         DATE,
  STARTTIME4       DATE,
  ENDTIME4         DATE,
  STARTTIME5       DATE,
  ENDTIME5         DATE,
  STARTTIME6       DATE,
  ENDTIME6         DATE,
  STARTTIME7       DATE,
  ENDTIME7         DATE
)
tablespace ABS_DATA
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate primary, unique and foreign key constraints 
alter table TIMEBLOCKPERSHIFT2
  add constraint XPKTIMEBLOCKPERSHIFT2 primary key (PLANT_CODE, SHIFT_NUMBER, TIMEBLOCK_NUMBER)
  using index 
  tablespace INDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
/  

-- Create table
create table TIMEBLOCKPERDEPARTMENT2
(
  PLANT_CODE       VARCHAR2(6) not null,
  DEPARTMENT_CODE  VARCHAR2(6) not null,
  TIMEBLOCK_NUMBER NUMBER(10) not null,
  CREATIONDATE     DATE not null,
  SHIFT_NUMBER     NUMBER(10) not null,
  DESCRIPTION      VARCHAR2(30),
  MUTATIONDATE     DATE not null,
  MUTATOR          VARCHAR2(20) not null,
  STARTTIME1       DATE,
  ENDTIME1         DATE,
  STARTTIME2       DATE,
  ENDTIME2         DATE,
  STARTTIME3       DATE,
  ENDTIME3         DATE,
  STARTTIME4       DATE,
  ENDTIME4         DATE,
  STARTTIME5       DATE,
  ENDTIME5         DATE,
  STARTTIME6       DATE,
  ENDTIME6         DATE,
  STARTTIME7       DATE,
  ENDTIME7         DATE
)
tablespace ABS_DATA
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate primary, unique and foreign key constraints 
alter table TIMEBLOCKPERDEPARTMENT2
  add constraint XPKTIMEBLOCKPERDEPARTMENT2 primary key (PLANT_CODE, DEPARTMENT_CODE, SHIFT_NUMBER, TIMEBLOCK_NUMBER)
  using index 
  tablespace INDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
/  

-- Create table
create table TIMEBLOCKPEREMPLOYEE2
(
  PLANT_CODE       VARCHAR2(6) not null,
  EMPLOYEE_NUMBER  NUMBER(10) not null,
  SHIFT_NUMBER     NUMBER(10) not null,
  CREATIONDATE     DATE not null,
  TIMEBLOCK_NUMBER NUMBER(10) not null,
  MUTATIONDATE     DATE not null,
  DESCRIPTION      VARCHAR2(30),
  STARTTIME1       DATE,
  ENDTIME1         DATE,
  MUTATOR          VARCHAR2(20) not null,
  STARTTIME2       DATE,
  ENDTIME2         DATE,
  STARTTIME3       DATE,
  ENDTIME3         DATE,
  STARTTIME4       DATE,
  ENDTIME4         DATE,
  STARTTIME5       DATE,
  ENDTIME5         DATE,
  STARTTIME6       DATE,
  ENDTIME6         DATE,
  STARTTIME7       DATE,
  ENDTIME7         DATE
)
tablespace ABS_DATA
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate primary, unique and foreign key constraints 
alter table TIMEBLOCKPEREMPLOYEE2
  add constraint XPKTIMEBLOCKPEREMPLOYEE2 primary key (EMPLOYEE_NUMBER, PLANT_CODE, SHIFT_NUMBER, TIMEBLOCK_NUMBER)
  using index 
  tablespace INDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
/

-- Create table
create table BREAKPERSHIFT2
(
  SHIFT_NUMBER NUMBER(10) not null,
  PLANT_CODE   VARCHAR2(6) not null,
  CREATIONDATE DATE not null,
  BREAK_NUMBER NUMBER(10) not null,
  MUTATIONDATE DATE not null,
  PAYED_YN     VARCHAR2(1 BYTE),
  DESCRIPTION  VARCHAR2(30) not null,
  MUTATOR      VARCHAR2(20) not null,
  STARTTIME1   DATE,
  ENDTIME1     DATE,
  STARTTIME2   DATE,
  ENDTIME2     DATE,
  STARTTIME3   DATE,
  ENDTIME3     DATE,
  STARTTIME4   DATE,
  ENDTIME4     DATE,
  STARTTIME5   DATE,
  ENDTIME5     DATE,
  STARTTIME6   DATE,
  ENDTIME6     DATE,
  STARTTIME7   DATE,
  ENDTIME7     DATE
)
tablespace ABS_DATA
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate primary, unique and foreign key constraints 
alter table BREAKPERSHIFT2
  add constraint XPKBREAKPERSHIFT2 primary key (PLANT_CODE, SHIFT_NUMBER, BREAK_NUMBER)
  using index 
  tablespace INDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
/

-- Create table
create table BREAKPERDEPARTMENT2
(
  PLANT_CODE      VARCHAR2(6) not null,
  DEPARTMENT_CODE VARCHAR2(6) not null,
  SHIFT_NUMBER    NUMBER(10) not null,
  BREAK_NUMBER    NUMBER(10) not null,
  DESCRIPTION     VARCHAR2(30) not null,
  PAYED_YN        VARCHAR2(1 BYTE) default 'N',
  CREATIONDATE    DATE not null,
  MUTATIONDATE    DATE not null,
  MUTATOR         VARCHAR2(20) not null,
  STARTTIME1      DATE,
  ENDTIME1        DATE,
  STARTTIME2      DATE,
  ENDTIME2        DATE,
  STARTTIME3      DATE,
  ENDTIME3        DATE,
  STARTTIME4      DATE,
  ENDTIME4        DATE,
  STARTTIME5      DATE,
  ENDTIME5        DATE,
  STARTTIME6      DATE,
  ENDTIME6        DATE,
  STARTTIME7      DATE,
  ENDTIME7        DATE
)
tablespace ABS_DATA
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate primary, unique and foreign key constraints 
alter table BREAKPERDEPARTMENT2
  add constraint XPKBREAKPERDEPARTMENT2 primary key (PLANT_CODE, DEPARTMENT_CODE, SHIFT_NUMBER, BREAK_NUMBER)
  using index 
  tablespace INDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
/

-- Create table
create table BREAKPEREMPLOYEE2
(
  EMPLOYEE_NUMBER NUMBER(10) not null,
  PLANT_CODE      VARCHAR2(6) not null,
  SHIFT_NUMBER    NUMBER(10) not null,
  BREAK_NUMBER    NUMBER(10) not null,
  PAYED_YN        VARCHAR2(1 BYTE),
  DESCRIPTION     VARCHAR2(30),
  STARTTIME1      DATE,
  ENDTIME1        DATE,
  STARTTIME2      DATE,
  ENDTIME2        DATE,
  STARTTIME3      DATE,
  ENDTIME3        DATE,
  STARTTIME4      DATE,
  ENDTIME4        DATE,
  STARTTIME5      DATE,
  ENDTIME5        DATE,
  STARTTIME6      DATE,
  ENDTIME6        DATE,
  STARTTIME7      DATE,
  ENDTIME7        DATE,
  CREATIONDATE    DATE not null,
  MUTATIONDATE    DATE not null,
  MUTATOR         VARCHAR2(20) not null
)
tablespace ABS_DATA
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate primary, unique and foreign key constraints 
alter table BREAKPEREMPLOYEE2
  add constraint XPKBREAKPEREMPLOYEE2 primary key (EMPLOYEE_NUMBER, PLANT_CODE, SHIFT_NUMBER, BREAK_NUMBER)
  using index 
  tablespace INDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
/

create or replace procedure ConvertWeekDaysSUN2WED is
  -- Local variables here
  acount Integer;
  counter Integer;
  cursor cursor_so is
    select plant_code, department_code, workspot_code, shift_number,
            DAY_OF_WEEK,
            OCC_A_TIMEBLOCK_1,
            OCC_A_TIMEBLOCK_2,
            OCC_A_TIMEBLOCK_3,
            OCC_A_TIMEBLOCK_4,
            OCC_B_TIMEBLOCK_1,
            OCC_B_TIMEBLOCK_2,
            OCC_B_TIMEBLOCK_3,
            OCC_B_TIMEBLOCK_4,
            OCC_C_TIMEBLOCK_1,
            OCC_C_TIMEBLOCK_2,
            OCC_C_TIMEBLOCK_3,
            OCC_C_TIMEBLOCK_4,
            OCC_A_TIMEBLOCK_5,
            OCC_A_TIMEBLOCK_6,
            OCC_A_TIMEBLOCK_7,
            OCC_A_TIMEBLOCK_8,
            OCC_A_TIMEBLOCK_9,
            OCC_A_TIMEBLOCK_10,
            OCC_B_TIMEBLOCK_5,
            OCC_B_TIMEBLOCK_6,
            OCC_B_TIMEBLOCK_7,
            OCC_B_TIMEBLOCK_8,
            OCC_B_TIMEBLOCK_9,
            OCC_B_TIMEBLOCK_10,
            OCC_C_TIMEBLOCK_5,
            OCC_C_TIMEBLOCK_6,
            OCC_C_TIMEBLOCK_7,
            OCC_C_TIMEBLOCK_8,
            OCC_C_TIMEBLOCK_9,
            OCC_C_TIMEBLOCK_10,
            FIXED_YN,
            CREATIONDATE,
            MUTATIONDATE,
            MUTATOR
            from standardoccupation2
            order by plant_code, department_code, workspot_code, shift_number, day_of_week;
  rec_so cursor_so%ROWTYPE;

  cursor cursor_sa is
    select PLANT_CODE, SHIFT_NUMBER, EMPLOYEE_NUMBER, DAY_OF_WEEK,
      AVAILABLE_TIMEBLOCK_1,
      AVAILABLE_TIMEBLOCK_2,
      AVAILABLE_TIMEBLOCK_3,
      AVAILABLE_TIMEBLOCK_4,
      AVAILABLE_TIMEBLOCK_5,
      AVAILABLE_TIMEBLOCK_6,
      AVAILABLE_TIMEBLOCK_7,
      AVAILABLE_TIMEBLOCK_8,
      AVAILABLE_TIMEBLOCK_9,
      AVAILABLE_TIMEBLOCK_10,
      CREATIONDATE,
      MUTATIONDATE,
      MUTATOR
    from standardavailability2
    order by PLANT_CODE, SHIFT_NUMBER, EMPLOYEE_NUMBER, DAY_OF_WEEK;
  rec_sa cursor_sa%ROWTYPE;

  cursor cursor_sep is
    select
      PLANT_CODE,
      DAY_OF_WEEK,
      SHIFT_NUMBER,
      DEPARTMENT_CODE,
      WORKSPOT_CODE,
      EMPLOYEE_NUMBER,
      SCHEDULED_TIMEBLOCK_1,
      SCHEDULED_TIMEBLOCK_2,
      SCHEDULED_TIMEBLOCK_3,
      SCHEDULED_TIMEBLOCK_4,
      CREATIONDATE,
      MUTATIONDATE,
      MUTATOR,
      SCHEDULED_TIMEBLOCK_5,
      SCHEDULED_TIMEBLOCK_6,
      SCHEDULED_TIMEBLOCK_7,
      SCHEDULED_TIMEBLOCK_8,
      SCHEDULED_TIMEBLOCK_9,
      SCHEDULED_TIMEBLOCK_10
    from standardemployeeplanning2
    order by PLANT_CODE, DAY_OF_WEEK, SHIFT_NUMBER, DEPARTMENT_CODE, WORKSPOT_CODE, EMPLOYEE_NUMBER;
  rec_sep cursor_sep%ROWTYPE;

  cursor cursor_sft is
    select
      SHIFT_NUMBER,
      PLANT_CODE,
      DESCRIPTION,
      CREATIONDATE,
      MUTATIONDATE,
      MUTATOR,
      STARTTIME1,
      ENDTIME1,
      STARTTIME2,
      ENDTIME2,
      STARTTIME3,
      ENDTIME3,
      STARTTIME4,
      ENDTIME4,
      STARTTIME5,
      ENDTIME5,
      STARTTIME6,
      ENDTIME6,
      STARTTIME7,
      ENDTIME7,
      HOURTYPE_NUMBER
    from shift2
    order by PLANT_CODE, SHIFT_NUMBER;
  rec_sft cursor_sft%ROWTYPE;

  cursor cursor_tbs is
    select
      SHIFT_NUMBER,
      PLANT_CODE,
      CREATIONDATE,
      TIMEBLOCK_NUMBER,
      MUTATIONDATE,
      DESCRIPTION,
      MUTATOR,
      STARTTIME1,
      ENDTIME1,
      STARTTIME2,
      ENDTIME2,
      STARTTIME3,
      ENDTIME3,
      STARTTIME4,
      ENDTIME4,
      STARTTIME5,
      ENDTIME5,
      STARTTIME6,
      ENDTIME6,
      STARTTIME7,
      ENDTIME7
    from timeblockpershift2
    order by PLANT_CODE, SHIFT_NUMBER, TIMEBLOCK_NUMBER;
  rec_tbs cursor_tbs%ROWTYPE;

  cursor cursor_tbd is
    select
      PLANT_CODE,
      DEPARTMENT_CODE,
      TIMEBLOCK_NUMBER,
      CREATIONDATE,
      SHIFT_NUMBER,
      DESCRIPTION,
      MUTATIONDATE,
      MUTATOR,
      STARTTIME1,
      ENDTIME1,
      STARTTIME2,
      ENDTIME2,
      STARTTIME3,
      ENDTIME3,
      STARTTIME4,
      ENDTIME4,
      STARTTIME5,
      ENDTIME5,
      STARTTIME6,
      ENDTIME6,
      STARTTIME7,
      ENDTIME7
    from timeblockperdepartment2
    order by PLANT_CODE, DEPARTMENT_CODE, SHIFT_NUMBER, TIMEBLOCK_NUMBER;
  rec_tbd cursor_tbd%ROWTYPE;

  cursor cursor_tbe is
    select
      PLANT_CODE,
      EMPLOYEE_NUMBER,
      SHIFT_NUMBER,
      CREATIONDATE,
      TIMEBLOCK_NUMBER,
      MUTATIONDATE,
      DESCRIPTION,
      STARTTIME1,
      ENDTIME1,
      MUTATOR,
      STARTTIME2,
      ENDTIME2,
      STARTTIME3,
      ENDTIME3,
      STARTTIME4,
      ENDTIME4,
      STARTTIME5,
      ENDTIME5,
      STARTTIME6,
      ENDTIME6,
      STARTTIME7,
      ENDTIME7
    from timeblockperemployee2
    order by EMPLOYEE_NUMBER, PLANT_CODE, SHIFT_NUMBER, TIMEBLOCK_NUMBER;
  rec_tbe cursor_tbe%ROWTYPE;

  cursor cursor_bps is
    select
      SHIFT_NUMBER,
      PLANT_CODE,
      CREATIONDATE,
      BREAK_NUMBER,
      MUTATIONDATE,
      PAYED_YN,
      DESCRIPTION,
      MUTATOR,
      STARTTIME1,
      ENDTIME1,
      STARTTIME2,
      ENDTIME2,
      STARTTIME3,
      ENDTIME3,
      STARTTIME4,
      ENDTIME4,
      STARTTIME5,
      ENDTIME5,
      STARTTIME6,
      ENDTIME6,
      STARTTIME7,
      ENDTIME7
    from breakpershift2
    order by PLANT_CODE, SHIFT_NUMBER, BREAK_NUMBER;
  rec_bps cursor_bps%ROWTYPE;

  cursor cursor_bpd is
    select
      PLANT_CODE,
      DEPARTMENT_CODE,
      SHIFT_NUMBER,
      BREAK_NUMBER,
      DESCRIPTION,
      PAYED_YN,
      CREATIONDATE,
      MUTATIONDATE,
      MUTATOR,
      STARTTIME1,
      ENDTIME1,
      STARTTIME2,
      ENDTIME2,
      STARTTIME3,
      ENDTIME3,
      STARTTIME4,
      ENDTIME4,
      STARTTIME5,
      ENDTIME5,
      STARTTIME6,
      ENDTIME6,
      STARTTIME7,
      ENDTIME7
    from breakperdepartment2
    order by PLANT_CODE, DEPARTMENT_CODE, SHIFT_NUMBER, BREAK_NUMBER;
  rec_bpd cursor_bpd%ROWTYPE;

  cursor cursor_bpe is
    select
      EMPLOYEE_NUMBER,
      PLANT_CODE,
      SHIFT_NUMBER,
      BREAK_NUMBER,
      PAYED_YN,
      DESCRIPTION,
      STARTTIME1,
      ENDTIME1,
      STARTTIME2,
      ENDTIME2,
      STARTTIME3,
      ENDTIME3,
      STARTTIME4,
      ENDTIME4,
      STARTTIME5,
      ENDTIME5,
      STARTTIME6,
      ENDTIME6,
      STARTTIME7,
      ENDTIME7,
      CREATIONDATE,
      MUTATIONDATE,
      MUTATOR
    from breakperemployee2
    order by EMPLOYEE_NUMBER, PLANT_CODE, SHIFT_NUMBER, BREAK_NUMBER;
  rec_bpe cursor_bpe%ROWTYPE;

  -- 3, 1 Wednesday at 3 must go to 1
  procedure ConvertDaySO(atoday integer) is
  begin
    insert into standardoccupation
    (PLANT_CODE, DEPARTMENT_CODE, WORKSPOT_CODE, SHIFT_NUMBER,
     DAY_OF_WEEK,
     OCC_A_TIMEBLOCK_1, OCC_A_TIMEBLOCK_2, OCC_A_TIMEBLOCK_3, OCC_A_TIMEBLOCK_4,
     OCC_B_TIMEBLOCK_1, OCC_B_TIMEBLOCK_2, OCC_B_TIMEBLOCK_3, OCC_B_TIMEBLOCK_4,
     OCC_C_TIMEBLOCK_1, OCC_C_TIMEBLOCK_2, OCC_C_TIMEBLOCK_3, OCC_C_TIMEBLOCK_4,
     OCC_A_TIMEBLOCK_5, OCC_A_TIMEBLOCK_6, OCC_A_TIMEBLOCK_7, OCC_A_TIMEBLOCK_8,
     OCC_A_TIMEBLOCK_9, OCC_A_TIMEBLOCK_10, OCC_B_TIMEBLOCK_5, OCC_B_TIMEBLOCK_6,
     OCC_B_TIMEBLOCK_7, OCC_B_TIMEBLOCK_8, OCC_B_TIMEBLOCK_9, OCC_B_TIMEBLOCK_10,
     OCC_C_TIMEBLOCK_5, OCC_C_TIMEBLOCK_6, OCC_C_TIMEBLOCK_7, OCC_C_TIMEBLOCK_8,
     OCC_C_TIMEBLOCK_9, OCC_C_TIMEBLOCK_10, FIXED_YN, CREATIONDATE, MUTATIONDATE, MUTATOR)
    values
    (rec_so.PLANT_CODE, rec_so.DEPARTMENT_CODE, rec_so.WORKSPOT_CODE, rec_so.SHIFT_NUMBER,
     atoday,
     rec_so.OCC_A_TIMEBLOCK_1, rec_so.OCC_A_TIMEBLOCK_2, rec_so.OCC_A_TIMEBLOCK_3, rec_so.OCC_A_TIMEBLOCK_4,
     rec_so.OCC_B_TIMEBLOCK_1, rec_so.OCC_B_TIMEBLOCK_2, rec_so.OCC_B_TIMEBLOCK_3, rec_so.OCC_B_TIMEBLOCK_4,
     rec_so.OCC_C_TIMEBLOCK_1, rec_so.OCC_C_TIMEBLOCK_2, rec_so.OCC_C_TIMEBLOCK_3, rec_so.OCC_C_TIMEBLOCK_4,
     rec_so.OCC_A_TIMEBLOCK_5, rec_so.OCC_A_TIMEBLOCK_6, rec_so.OCC_A_TIMEBLOCK_7, rec_so.OCC_A_TIMEBLOCK_8,
     rec_so.OCC_A_TIMEBLOCK_9, rec_so.OCC_A_TIMEBLOCK_10, rec_so.OCC_B_TIMEBLOCK_5, rec_so.OCC_B_TIMEBLOCK_6,
     rec_so.OCC_B_TIMEBLOCK_7, rec_so.OCC_B_TIMEBLOCK_8, rec_so.OCC_B_TIMEBLOCK_9, rec_so.OCC_B_TIMEBLOCK_10,
     rec_so.OCC_C_TIMEBLOCK_5, rec_so.OCC_C_TIMEBLOCK_6, rec_so.OCC_C_TIMEBLOCK_7, rec_so.OCC_C_TIMEBLOCK_8,
     rec_so.OCC_C_TIMEBLOCK_9, rec_so.OCC_C_TIMEBLOCK_10, rec_so.FIXED_YN,
     rec_so.CREATIONDATE, rec_so.MUTATIONDATE, rec_so.MUTATOR);
    commit;
  end ConvertDaySO;

  procedure ConvertDaySA(atoday Integer) is
  begin
    update standardavailability
    set AVAILABLE_TIMEBLOCK_1 = rec_sa.AVAILABLE_TIMEBLOCK_1,
        AVAILABLE_TIMEBLOCK_2 = rec_sa.AVAILABLE_TIMEBLOCK_2,
        AVAILABLE_TIMEBLOCK_3 = rec_sa.AVAILABLE_TIMEBLOCK_3,
        AVAILABLE_TIMEBLOCK_4 = rec_sa.AVAILABLE_TIMEBLOCK_4,
        AVAILABLE_TIMEBLOCK_5 = rec_sa.AVAILABLE_TIMEBLOCK_5,
        AVAILABLE_TIMEBLOCK_6 = rec_sa.AVAILABLE_TIMEBLOCK_6,
        AVAILABLE_TIMEBLOCK_7 = rec_sa.AVAILABLE_TIMEBLOCK_7,
        AVAILABLE_TIMEBLOCK_8 = rec_sa.AVAILABLE_TIMEBLOCK_8,
        AVAILABLE_TIMEBLOCK_9 = rec_sa.AVAILABLE_TIMEBLOCK_9,
        AVAILABLE_TIMEBLOCK_10 = rec_sa.AVAILABLE_TIMEBLOCK_10,
        MUTATIONDATE = SYSDATE,
        MUTATOR = 'GLOBE'
    where PLANT_CODE = rec_sa.PLANT_CODE and SHIFT_NUMBER = rec_sa.SHIFT_NUMBER
    and EMPLOYEE_NUMBER = rec_sa.EMPLOYEE_NUMBER
    and DAY_OF_WEEK = atoday;
  end ConvertDaySA;

  procedure ConvertDaySEP(atoday Integer) is
  begin
    insert into standardemployeeplanning
    (
      PLANT_CODE,
      DAY_OF_WEEK,
      SHIFT_NUMBER, DEPARTMENT_CODE,
      WORKSPOT_CODE, EMPLOYEE_NUMBER,
      SCHEDULED_TIMEBLOCK_1,
      SCHEDULED_TIMEBLOCK_2,
      SCHEDULED_TIMEBLOCK_3,
      SCHEDULED_TIMEBLOCK_4,
      CREATIONDATE, MUTATIONDATE, MUTATOR,
      SCHEDULED_TIMEBLOCK_5,
      SCHEDULED_TIMEBLOCK_6,
      SCHEDULED_TIMEBLOCK_7,
      SCHEDULED_TIMEBLOCK_8,
      SCHEDULED_TIMEBLOCK_9,
      SCHEDULED_TIMEBLOCK_10
    )
    values
    (
      rec_sep.PLANT_CODE,
      atoday,
      rec_sep.SHIFT_NUMBER,
      rec_sep.DEPARTMENT_CODE,
      rec_sep.WORKSPOT_CODE,
      rec_sep.EMPLOYEE_NUMBER,
      rec_sep.SCHEDULED_TIMEBLOCK_1,
      rec_sep.SCHEDULED_TIMEBLOCK_2,
      rec_sep.SCHEDULED_TIMEBLOCK_3,
      rec_sep.SCHEDULED_TIMEBLOCK_4,
      rec_sep.CREATIONDATE,
      rec_sep.MUTATIONDATE,
      rec_sep.MUTATOR,
      rec_sep.SCHEDULED_TIMEBLOCK_5,
      rec_sep.SCHEDULED_TIMEBLOCK_6,
      rec_sep.SCHEDULED_TIMEBLOCK_7,
      rec_sep.SCHEDULED_TIMEBLOCK_8,
      rec_sep.SCHEDULED_TIMEBLOCK_9,
      rec_sep.SCHEDULED_TIMEBLOCK_10
    );
  end ConvertDaySEP;
begin

  -- Copy SO into SO2
  select count(*) into acount from STANDARDOCCUPATION2;
  --  delete from STANDARDOCCUPATION2;
  if acount = 0 then
    insert into STANDARDOCCUPATION2
    select * from STANDARDOCCUPATION;
    commit;
  end if;

  -- Copy SA into SA2
  select count(*) into acount from standardavailability2;
  -- delete from standardavailability2;
  if acount = 0 then
    insert into standardavailability2
    select * from standardavailability;
    commit;
  end if;

  -- Copy SEP into SEP2
  select count(*) into acount from standardemployeeplanning2;
  -- delete from standardemployeeplanning2;
  if acount = 0 then
    insert into standardemployeeplanning2
    select * from standardemployeeplanning;
    commit;
  end if;

  -- copy SFT into SFT2
  select count(*) into acount from shift2;
  -- delete from shift2;
  if acount = 0 then
    insert into shift2
    select * from shift;
    commit;
  end if;

  -- Copy TBS into TBS2
  select count(*) into acount from timeblockpershift2;
  -- delete from timeblockpershift2;
  if acount = 0 then
    insert into timeblockpershift2
    select * from timeblockpershift;
    commit;
  end if;

  -- Copy TBD into TBD2
  select count(*) into acount from timeblockperdepartment2;
  -- delete from timeblockperdepartment2;
  if acount = 0 then
    insert into timeblockperdepartment2
    select * from timeblockperdepartment;
    commit;
  end if;

  -- Copy TBE into TBE2
  select count(*) into acount from timeblockperemployee2;
  -- delete from timeblockperemployee2;
  if acount = 0 then
    insert into timeblockperemployee2
    select * from timeblockperemployee;
    commit;
  end if;

  -- Copy BPS into BPS2
  select count(*) into acount from breakpershift2;
  -- delete from breakpershift2;
  if acount = 0 then
    insert into breakpershift2
    select * from breakpershift;
    commit;
  end if;

  -- Copy BPD into BPD2
  select count(*) into acount from breakperdepartment2;
  -- delete from breakperdepartment2;
  if acount = 0 then
    insert into breakperdepartment2
    select * from breakperdepartment;
    commit;
  end if;

  -- Copy BPE into BPE2
  select count(*) into acount from breakperemployee2;
  -- delete from breakperemployee2;
  if acount = 0 then
    insert into breakperemployee2
    select * from breakperemployee;
    commit;
  end if;

  /* Delete all records from this table, because they are all inserted again later */
  /* with a new numbering for day_of_week */
  delete from standardoccupation;
  commit;

  /* Delete also here the records, they are inserted later */
  delete from standardemployeeplanning;
  commit;

  counter := 0;
  open cursor_so;
  loop
    begin
      fetch cursor_so into rec_so;
      exit when cursor_so%notfound;
      if rec_so.day_of_week = 4 then -- Wed
        ConvertDaySO(1);
      end if;
      if rec_so.day_of_week = 5 then -- Thu
        ConvertDaySO(2);
      end if;
      if rec_so.day_of_week = 6 then -- Fri
        ConvertDaySO(3);
      end if;
      if rec_so.day_of_week = 7 then -- Sat
        ConvertDaySO(4);
      end if;
      if rec_so.day_of_week = 1 then -- Sun
        ConvertDaySO(5);
      end if;
      if rec_so.day_of_week = 2 then -- Mon
        ConvertDaySO(6);
      end if;
      if rec_so.day_of_week = 3 then -- Tue
        ConvertDaySO(7);
      end if;
    end;
    counter := counter + 1;
  end loop;
  close cursor_so;
  commit;
--  dbms_output.put_line('counter=' || counter);

  counter := 0;
  open cursor_sa;
  loop
    begin
      fetch cursor_sa into rec_sa;
      exit when cursor_sa%notfound;
      if rec_sa.day_of_week = 4 then -- Wed
        ConvertDaySA(1);
      end if;
      if rec_sa.day_of_week = 5 then -- Thu
        ConvertDaySA(2);
      end if;
      if rec_sa.day_of_week = 6 then -- Fri
        ConvertDaySA(3);
      end if;
      if rec_sa.day_of_week = 7 then -- Sat
        ConvertDaySA(4);
      end if;
      if rec_sa.day_of_week = 1 then -- Sun
        ConvertDaySA(5);
      end if;
      if rec_sa.day_of_week = 2 then -- Mon
        ConvertDaySA(6);
      end if;
      if rec_sa.day_of_week = 3 then -- Tue
        ConvertDaySA(7);
      end if;
    end;
    counter := counter + 1;
  end loop;
  close cursor_sa;
  commit;
--  dbms_output.put_line('counter=' || counter);

  counter := 0;
  open cursor_sep;
  loop
    begin
      fetch cursor_sep into rec_sep;
      exit when cursor_sep%notfound;
      if rec_sep.day_of_week = 4 then -- Wed
        ConvertDaySEP(1);
      end if;
      if rec_sep.day_of_week = 5 then -- Thu
        ConvertDaySEP(2);
      end if;
      if rec_sep.day_of_week = 6 then -- Fri
        ConvertDaySEP(3);
      end if;
      if rec_sep.day_of_week = 7 then -- Sat
        ConvertDaySEP(4);
      end if;
      if rec_sep.day_of_week = 1 then -- Sun
        ConvertDaySEP(5);
      end if;
      if rec_sep.day_of_week = 2 then -- Mon
        ConvertDaySEP(6);
      end if;
      if rec_sep.day_of_week = 3 then -- Tue
        ConvertDaySEP(7);
      end if;
    end;
    counter := counter + 1;
  end loop;
  close cursor_sep;
  commit;
--  dbms_output.put_line('counter=' || counter);

  open cursor_sft;
  loop
    begin
      fetch cursor_sft into rec_sft;
      exit when cursor_sft%notfound;
      update shift t
      set t.starttime1 = rec_sft.starttime4,
      t.endtime1 = rec_sft.endtime4,
      t.starttime2 = rec_sft.starttime5,
      t.endtime2 = rec_sft.endtime5,
      t.starttime3 = rec_sft.starttime6,
      t.endtime3 = rec_sft.endtime6,
      t.starttime4 = rec_sft.starttime7,
      t.endtime4 = rec_sft.endtime7,
      t.starttime5 = rec_sft.starttime1,
      t.endtime5 = rec_sft.endtime1,
      t.starttime6 = rec_sft.starttime2,
      t.endtime6 = rec_sft.endtime2,
      t.starttime7 = rec_sft.starttime3,
      t.endtime7 = rec_sft.endtime3
      where t.shift_number = rec_sft.shift_number and
      t.plant_code = rec_sft.plant_code;
    end;
  end loop;
  close cursor_sft;
  commit;

  open cursor_tbs;
  loop
    begin
      fetch cursor_tbs into rec_tbs;
      exit when cursor_tbs%notfound;
      update timeblockpershift t
      set t.starttime1 = rec_tbs.starttime4,
      t.endtime1 = rec_tbs.endtime4,
      t.starttime2 = rec_tbs.starttime5,
      t.endtime2 = rec_tbs.endtime5,
      t.starttime3 = rec_tbs.starttime6,
      t.endtime3 = rec_tbs.endtime6,
      t.starttime4 = rec_tbs.starttime7,
      t.endtime4 = rec_tbs.endtime7,
      t.starttime5 = rec_tbs.starttime1,
      t.endtime5 = rec_tbs.endtime1,
      t.starttime6 = rec_tbs.starttime2,
      t.endtime6 = rec_tbs.endtime2,
      t.starttime7 = rec_tbs.starttime3,
      t.endtime7 = rec_tbs.endtime3
      where t.shift_number = rec_tbs.shift_number and
      t.plant_code = rec_tbs.plant_code and
      t.timeblock_number = rec_tbs.timeblock_number;
    end;
  end loop;
  close cursor_tbs;
  commit;

  open cursor_tbd;
  loop
    begin
      fetch cursor_tbd into rec_tbd;
      exit when cursor_tbd%notfound;
      update timeblockperdepartment t
      set t.starttime1 = rec_tbd.starttime4,
      t.endtime1 = rec_tbd.endtime4,
      t.starttime2 = rec_tbd.starttime5,
      t.endtime2 = rec_tbd.endtime5,
      t.starttime3 = rec_tbd.starttime6,
      t.endtime3 = rec_tbd.endtime6,
      t.starttime4 = rec_tbd.starttime7,
      t.endtime4 = rec_tbd.endtime7,
      t.starttime5 = rec_tbd.starttime1,
      t.endtime5 = rec_tbd.endtime1,
      t.starttime6 = rec_tbd.starttime2,
      t.endtime6 = rec_tbd.endtime2,
      t.starttime7 = rec_tbd.starttime3,
      t.endtime7 = rec_tbd.endtime3
      where t.shift_number = rec_tbd.shift_number and
      t.department_code = rec_tbd.department_code and
      t.plant_code = rec_tbd.plant_code and
      t.timeblock_number = rec_tbd.timeblock_number;
    end;
  end loop;
  close cursor_tbd;
  commit;

  open cursor_tbe;
  loop
    begin
      fetch cursor_tbe into rec_tbe;
      exit when cursor_tbe%notfound;
      update timeblockperemployee t
      set t.starttime1 = rec_tbe.starttime4,
      t.endtime1 = rec_tbe.endtime4,
      t.starttime2 = rec_tbe.starttime5,
      t.endtime2 = rec_tbe.endtime5,
      t.starttime3 = rec_tbe.starttime6,
      t.endtime3 = rec_tbe.endtime6,
      t.starttime4 = rec_tbe.starttime7,
      t.endtime4 = rec_tbe.endtime7,
      t.starttime5 = rec_tbe.starttime1,
      t.endtime5 = rec_tbe.endtime1,
      t.starttime6 = rec_tbe.starttime2,
      t.endtime6 = rec_tbe.endtime2,
      t.starttime7 = rec_tbe.starttime3,
      t.endtime7 = rec_tbe.endtime3
      where t.employee_number = rec_tbe.employee_number and
      t.plant_code = rec_tbe.plant_code and
      t.shift_number = rec_tbe.shift_number and
      t.timeblock_number = rec_tbe.timeblock_number;
    end;
  end loop;
  close cursor_tbe;
  commit;

  open cursor_bps;
  loop
    begin
      fetch cursor_bps into rec_bps;
      exit when cursor_bps%notfound;
      update breakpershift t
      set t.starttime1 = rec_bps.starttime1,
      t.endtime1 = rec_bps.endtime4,
      t.starttime2 = rec_bps.starttime5,
      t.endtime2 = rec_bps.endtime5,
      t.starttime3 = rec_bps.starttime6,
      t.endtime3 = rec_bps.endtime6,
      t.starttime4 = rec_bps.starttime7,
      t.endtime4 = rec_bps.endtime7,
      t.starttime5 = rec_bps.starttime1,
      t.endtime5 = rec_bps.endtime1,
      t.starttime6 = rec_bps.starttime2,
      t.endtime6 = rec_bps.endtime2,
      t.starttime7 = rec_bps.starttime3,
      t.endtime7 = rec_bps.endtime3
      where t.shift_number = rec_bps.shift_number and
      t.plant_code = rec_bps.plant_code and
      t.break_number = rec_bps.break_number;
    end;
  end loop;
  close cursor_bps;
  commit;

  open cursor_bpd;
  loop
    begin
      fetch cursor_bpd into rec_bpd;
      exit when cursor_bpd%notfound;
      update breakperdepartment t
      set t.starttime1 = rec_bpd.starttime1,
      t.endtime1 = rec_bpd.endtime4,
      t.starttime2 = rec_bpd.starttime5,
      t.endtime2 = rec_bpd.endtime5,
      t.starttime3 = rec_bpd.starttime6,
      t.endtime3 = rec_bpd.endtime6,
      t.starttime4 = rec_bpd.starttime7,
      t.endtime4 = rec_bpd.endtime7,
      t.starttime5 = rec_bpd.starttime1,
      t.endtime5 = rec_bpd.endtime1,
      t.starttime6 = rec_bpd.starttime2,
      t.endtime6 = rec_bpd.endtime2,
      t.starttime7 = rec_bpd.starttime3,
      t.endtime7 = rec_bpd.endtime3
      where t.plant_code = rec_bpd.plant_code and
      t.department_code = rec_bpd.department_code and
      t.shift_number = rec_bpd.shift_number and
      t.break_number = rec_bpd.break_number;
    end;
  end loop;
  close cursor_bpd;
  commit;

  open cursor_bpe;
  loop
    begin
      fetch cursor_bpe into rec_bpe;
      exit when cursor_bpe%notfound;
      update breakperemployee t
      set t.starttime1 = rec_bpe.starttime1,
      t.endtime1 = rec_bpe.endtime4,
      t.starttime2 = rec_bpe.starttime5,
      t.endtime2 = rec_bpe.endtime5,
      t.starttime3 = rec_bpe.starttime6,
      t.endtime3 = rec_bpe.endtime6,
      t.starttime4 = rec_bpe.starttime7,
      t.endtime4 = rec_bpe.endtime7,
      t.starttime5 = rec_bpe.starttime1,
      t.endtime5 = rec_bpe.endtime1,
      t.starttime6 = rec_bpe.starttime2,
      t.endtime6 = rec_bpe.endtime2,
      t.starttime7 = rec_bpe.starttime3,
      t.endtime7 = rec_bpe.endtime3
      where t.employee_number = rec_bpe.employee_number and
      t.plant_code = rec_bpe.plant_code and
      t.shift_number = rec_bpe.shift_number and
      t.break_number = rec_bpe.break_number;
    end;
  end loop;
  close cursor_bpe;
  commit;

end ConvertWeekDaysSUN2WED;
/

/* To restore the data, in case it went wrong */
/*
delete from standardavailability;
insert into standardavailability select * from standardavailability2;
commit;
delete from standardoccupation;
insert into standardoccupation select * from standardoccupation2;
commit;
delete standardemployeeplanning;
insert into standardemployeeplanning select from standardemployeeplanning2;
commit;
delete from timeblockpershift;
insert into timeblockpershift select * from timeblockpershift2;
commit;
delete from timeblockperdepartment;
insert into timeblockperdepartment select * from timeblockperdepartment2;
commit;
delete from timeblockperemployee;
insert into timeblockperemployee select * from timeblockperemployee2;
commit;
delete from breakpershift;
insert into breakpershift select * from breakpershift2;
commit;
delete from breakperdepartment;
insert into breakperdepartment select * from breakperdepartment2;
commit;
delete from breakperemployee;
insert into breakperemployee select * from breakperemployee2;
commit;
update shift t
set t.starttime1 = (select t2.starttime1 from shift2 t2 where t2.plant_code = t.plant_code and t2.shift_number = t.shift_number),
t.endtime1 = (select t2.endtime1 from shift2 t2 where t2.plant_code = t.plant_code and t2.shift_number = t.shift_number),
t.starttime2 = (select t2.starttime2 from shift2 t2 where t2.plant_code = t.plant_code and t2.shift_number = t.shift_number),
t.endtime2 = (select t2.endtime2 from shift2 t2 where t2.plant_code = t.plant_code and t2.shift_number = t.shift_number),
t.starttime3 = (select t2.starttime3 from shift2 t2 where t2.plant_code = t.plant_code and t2.shift_number = t.shift_number),
t.endtime3 = (select t2.endtime3 from shift2 t2 where t2.plant_code = t.plant_code and t2.shift_number = t.shift_number),
t.starttime4 = (select t2.starttime4 from shift2 t2 where t2.plant_code = t.plant_code and t2.shift_number = t.shift_number),
t.endtime4 = (select t2.endtime4 from shift2 t2 where t2.plant_code = t.plant_code and t2.shift_number = t.shift_number),
t.starttime5 = (select t2.starttime5 from shift2 t2 where t2.plant_code = t.plant_code and t2.shift_number = t.shift_number),
t.endtime5 = (select t2.endtime5 from shift2 t2 where t2.plant_code = t.plant_code and t2.shift_number = t.shift_number),
t.starttime6 = (select t2.starttime6 from shift2 t2 where t2.plant_code = t.plant_code and t2.shift_number = t.shift_number),
t.endtime6 = (select t2.endtime6 from shift2 t2 where t2.plant_code = t.plant_code and t2.shift_number = t.shift_number),
t.starttime7 = (select t2.starttime7 from shift2 t2 where t2.plant_code = t.plant_code and t2.shift_number = t.shift_number),
t.endtime7 = (select t2.endtime7 from shift2 t2 where t2.plant_code = t.plant_code and t2.shift_number = t.shift_number);
commit;
*/

commit;

