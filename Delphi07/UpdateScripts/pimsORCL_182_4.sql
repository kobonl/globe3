--
-- VEI001-7 Interface Cockpit <> Pims
--

-- Add/modify columns 
alter table EMPLOYEE add EMPLOYEE_ID number;

update EMPLOYEE
set EMPLOYEE_ID = ROWNUM;
commit;

alter table EMPLOYEE modify EMPLOYEE_ID NOT NULL;

-- Create/Recreate indexes 
create unique index XIE2EMPLOYEE on EMPLOYEE (EMPLOYEE_ID)
  tablespace INDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 128K
    next 1M
    minextents 1
    maxextents unlimited
  );

declare
 ex number;
begin
  ex := 1;
  select MAX(EMPLOYEE_ID) + 1 into ex from EMPLOYEE;
  if ex > 0 then
  begin
    execute immediate 'DROP SEQUENCE SEQ_EMPLOYEE';
    exception when others then
      null;
    end;
  execute immediate 'CREATE SEQUENCE SEQ_EMPLOYEE MINVALUE 1 MAXVALUE 1000000000000000000000000000 START WITH ' || ex || ' INCREMENT BY 1 CACHE 20';
  end if;
end;
/

--  Creating trigger 
CREATE OR REPLACE TRIGGER SETEMPLOYEEID BEFORE INSERT
ON EMPLOYEE FOR EACH ROW
DECLARE
  NEWID INTEGER;
BEGIN
  SELECT SEQ_EMPLOYEE.NEXTVAL INTO NEWID FROM DUAL;
  :NEW.EMPLOYEE_ID := NEWID;
END;
/

commit;


