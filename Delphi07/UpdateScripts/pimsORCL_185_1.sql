--
-- Glob3-60 Extension 4 to 10 blocks for planning
--

--
-- PIMSSETTING
--
-- Add/modify columns 
declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'alter table PIMSSETTING add MAX_TIMEBLOCKS NUMBER(10) DEFAULT 4';
    exception when column_exists then null;
end;
/

--
-- EMPLOYEEAVAILABILITY
--
-- Add/modify columns 
declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'alter table EMPLOYEEAVAILABILITY add AVAILABLE_TIMEBLOCK_5 VARCHAR2(1)';
    exception when column_exists then null;
end;
/
-- Add/modify columns 
declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'alter table EMPLOYEEAVAILABILITY add AVAILABLE_TIMEBLOCK_6 VARCHAR2(1)';
    exception when column_exists then null;
end;
/
-- Add/modify columns 
declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'alter table EMPLOYEEAVAILABILITY add AVAILABLE_TIMEBLOCK_7 VARCHAR2(1)';
    exception when column_exists then null;
end;
/
-- Add/modify columns 
declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'alter table EMPLOYEEAVAILABILITY add AVAILABLE_TIMEBLOCK_8 VARCHAR2(1)';
    exception when column_exists then null;
end;
/
-- Add/modify columns 
declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'alter table EMPLOYEEAVAILABILITY add AVAILABLE_TIMEBLOCK_9 VARCHAR2(1)';
    exception when column_exists then null;
end;
/
-- Add/modify columns 
declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'alter table EMPLOYEEAVAILABILITY add AVAILABLE_TIMEBLOCK_10 VARCHAR2(1)';
    exception when column_exists then null;
end;
/
--
-- EMPLOYEEAVAILABILITY / SAVE_
--
-- Add/modify columns 
declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'alter table EMPLOYEEAVAILABILITY add SAVE_AVAILABLE_TIMEBLOCK_5 VARCHAR2(1)';
    exception when column_exists then null;
end;
/
-- Add/modify columns 
declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'alter table EMPLOYEEAVAILABILITY add SAVE_AVAILABLE_TIMEBLOCK_6 VARCHAR2(1)';
    exception when column_exists then null;
end;
/
-- Add/modify columns 
declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'alter table EMPLOYEEAVAILABILITY add SAVE_AVAILABLE_TIMEBLOCK_7 VARCHAR2(1)';
    exception when column_exists then null;
end;
/
-- Add/modify columns 
declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'alter table EMPLOYEEAVAILABILITY add SAVE_AVAILABLE_TIMEBLOCK_8 VARCHAR2(1)';
    exception when column_exists then null;
end;
/
-- Add/modify columns 
declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'alter table EMPLOYEEAVAILABILITY add SAVE_AVAILABLE_TIMEBLOCK_9 VARCHAR2(1)';
    exception when column_exists then null;
end;
/
-- Add/modify columns 
declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'alter table EMPLOYEEAVAILABILITY add SAVE_AVAILABLE_TIMEBLOCK_10 VARCHAR2(1)';
    exception when column_exists then null;
end;
/
--
-- EMPLOYEEPLANNING
--
-- Add/modify columns 
declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'alter table EMPLOYEEPLANNING add SCHEDULED_TIMEBLOCK_5 VARCHAR2(1)';
    exception when column_exists then null;
end;
/
-- Add/modify columns 
declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'alter table EMPLOYEEPLANNING add SCHEDULED_TIMEBLOCK_6 VARCHAR2(1)';
    exception when column_exists then null;
end;
/
-- Add/modify columns 
declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'alter table EMPLOYEEPLANNING add SCHEDULED_TIMEBLOCK_7 VARCHAR2(1)';
    exception when column_exists then null;
end;
/
-- Add/modify columns 
declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'alter table EMPLOYEEPLANNING add SCHEDULED_TIMEBLOCK_8 VARCHAR2(1)';
    exception when column_exists then null;
end;
/
-- Add/modify columns 
declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'alter table EMPLOYEEPLANNING add SCHEDULED_TIMEBLOCK_9 VARCHAR2(1)';
    exception when column_exists then null;
end;
/
-- Add/modify columns 
declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'alter table EMPLOYEEPLANNING add SCHEDULED_TIMEBLOCK_10 VARCHAR2(1)';
    exception when column_exists then null;
end;
/
--
-- OCCUPATIONPLANNING
--
--
-- A
--
-- Add/modify columns 
declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'alter table OCCUPATIONPLANNING add OCC_A_TIMEBLOCK_5 Number(10)';
    exception when column_exists then null;
end;
/
-- Add/modify columns 
declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'alter table OCCUPATIONPLANNING add OCC_A_TIMEBLOCK_6 Number(10)';
    exception when column_exists then null;
end;
/
-- Add/modify columns 
declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'alter table OCCUPATIONPLANNING add OCC_A_TIMEBLOCK_7 Number(10)';
    exception when column_exists then null;
end;
/
-- Add/modify columns 
declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'alter table OCCUPATIONPLANNING add OCC_A_TIMEBLOCK_8 Number(10)';
    exception when column_exists then null;
end;
/
-- Add/modify columns 
declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'alter table OCCUPATIONPLANNING add OCC_A_TIMEBLOCK_9 Number(10)';
    exception when column_exists then null;
end;
/
-- Add/modify columns 
declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'alter table OCCUPATIONPLANNING add OCC_A_TIMEBLOCK_10 Number(10)';
    exception when column_exists then null;
end;
/
--
-- B
--
-- Add/modify columns 
declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'alter table OCCUPATIONPLANNING add OCC_B_TIMEBLOCK_5 Number(10)';
    exception when column_exists then null;
end;
/
-- Add/modify columns 
declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'alter table OCCUPATIONPLANNING add OCC_B_TIMEBLOCK_6 Number(10)';
    exception when column_exists then null;
end;
/
-- Add/modify columns 
declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'alter table OCCUPATIONPLANNING add OCC_B_TIMEBLOCK_7 Number(10)';
    exception when column_exists then null;
end;
/
-- Add/modify columns 
declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'alter table OCCUPATIONPLANNING add OCC_B_TIMEBLOCK_8 Number(10)';
    exception when column_exists then null;
end;
/
-- Add/modify columns 
declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'alter table OCCUPATIONPLANNING add OCC_B_TIMEBLOCK_9 Number(10)';
    exception when column_exists then null;
end;
/
-- Add/modify columns 
declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'alter table OCCUPATIONPLANNING add OCC_B_TIMEBLOCK_10 Number(10)';
    exception when column_exists then null;
end;
/
--
-- C
--
-- Add/modify columns 
declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'alter table OCCUPATIONPLANNING add OCC_C_TIMEBLOCK_5 Number(10)';
    exception when column_exists then null;
end;
/
-- Add/modify columns 
declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'alter table OCCUPATIONPLANNING add OCC_C_TIMEBLOCK_6 Number(10)';
    exception when column_exists then null;
end;
/
-- Add/modify columns 
declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'alter table OCCUPATIONPLANNING add OCC_C_TIMEBLOCK_7 Number(10)';
    exception when column_exists then null;
end;
/
-- Add/modify columns 
declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'alter table OCCUPATIONPLANNING add OCC_C_TIMEBLOCK_8 Number(10)';
    exception when column_exists then null;
end;
/
-- Add/modify columns 
declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'alter table OCCUPATIONPLANNING add OCC_C_TIMEBLOCK_9 Number(10)';
    exception when column_exists then null;
end;
/
-- Add/modify columns 
declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'alter table OCCUPATIONPLANNING add OCC_C_TIMEBLOCK_10 Number(10)';
    exception when column_exists then null;
end;
/
--
-- STANDARDAVAILABILITY
--
-- Add/modify columns 
declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'alter table STANDARDAVAILABILITY add AVAILABLE_TIMEBLOCK_5 VARCHAR2(1)';
    exception when column_exists then null;
end;
/
-- Add/modify columns 
declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'alter table STANDARDAVAILABILITY add AVAILABLE_TIMEBLOCK_6 VARCHAR2(1)';
    exception when column_exists then null;
end;
/
-- Add/modify columns 
declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'alter table STANDARDAVAILABILITY add AVAILABLE_TIMEBLOCK_7 VARCHAR2(1)';
    exception when column_exists then null;
end;
/
-- Add/modify columns 
declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'alter table STANDARDAVAILABILITY add AVAILABLE_TIMEBLOCK_8 VARCHAR2(1)';
    exception when column_exists then null;
end;
/
-- Add/modify columns 
declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'alter table STANDARDAVAILABILITY add AVAILABLE_TIMEBLOCK_9 VARCHAR2(1)';
    exception when column_exists then null;
end;
/
-- Add/modify columns 
declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'alter table STANDARDAVAILABILITY add AVAILABLE_TIMEBLOCK_10 VARCHAR2(1)';
    exception when column_exists then null;
end;
/

--
-- PLANT
--
-- Add/modify columns 
declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'alter table PLANT add STDOCC_A_YN VARCHAR2(1) default ''Y''';
    exception when column_exists then null;
end;
/
-- Add/modify columns 
declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'alter table PLANT add STDOCC_B_YN VARCHAR2(1) default ''Y''';
    exception when column_exists then null;
end;
/
-- Add/modify columns 
declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'alter table PLANT add STDOCC_C_YN VARCHAR2(1) default ''Y''';
    exception when column_exists then null;
end;
/
--
-- STANDARDOCCUPATION
--
-- Add/modify columns 
declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'alter table STANDARDOCCUPATION add OCC_A_TIMEBLOCK_5 Number(10)';
    exception when column_exists then null;
end;
/
-- Add/modify columns 
declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'alter table STANDARDOCCUPATION add OCC_A_TIMEBLOCK_6 Number(10)';
    exception when column_exists then null;
end;
/
-- Add/modify columns 
declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'alter table STANDARDOCCUPATION add OCC_A_TIMEBLOCK_7 Number(10)';
    exception when column_exists then null;
end;
/
-- Add/modify columns 
declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'alter table STANDARDOCCUPATION add OCC_A_TIMEBLOCK_8 Number(10)';
    exception when column_exists then null;
end;
/
-- Add/modify columns 
declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'alter table STANDARDOCCUPATION add OCC_A_TIMEBLOCK_9 Number(10)';
    exception when column_exists then null;
end;
/
-- Add/modify columns 
declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'alter table STANDARDOCCUPATION add OCC_A_TIMEBLOCK_10 Number(10)';
    exception when column_exists then null;
end;
/
--
-- B
--
-- Add/modify columns 
declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'alter table STANDARDOCCUPATION add OCC_B_TIMEBLOCK_5 Number(10)';
    exception when column_exists then null;
end;
/
-- Add/modify columns 
declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'alter table STANDARDOCCUPATION add OCC_B_TIMEBLOCK_6 Number(10)';
    exception when column_exists then null;
end;
/
-- Add/modify columns 
declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'alter table STANDARDOCCUPATION add OCC_B_TIMEBLOCK_7 Number(10)';
    exception when column_exists then null;
end;
/
-- Add/modify columns 
declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'alter table STANDARDOCCUPATION add OCC_B_TIMEBLOCK_8 Number(10)';
    exception when column_exists then null;
end;
/
-- Add/modify columns 
declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'alter table STANDARDOCCUPATION add OCC_B_TIMEBLOCK_9 Number(10)';
    exception when column_exists then null;
end;
/
-- Add/modify columns 
declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'alter table STANDARDOCCUPATION add OCC_B_TIMEBLOCK_10 Number(10)';
    exception when column_exists then null;
end;
/
--
-- C
--
-- Add/modify columns 
declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'alter table STANDARDOCCUPATION add OCC_C_TIMEBLOCK_5 Number(10)';
    exception when column_exists then null;
end;
/
-- Add/modify columns 
declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'alter table STANDARDOCCUPATION add OCC_C_TIMEBLOCK_6 Number(10)';
    exception when column_exists then null;
end;
/
-- Add/modify columns 
declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'alter table STANDARDOCCUPATION add OCC_C_TIMEBLOCK_7 Number(10)';
    exception when column_exists then null;
end;
/
-- Add/modify columns 
declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'alter table STANDARDOCCUPATION add OCC_C_TIMEBLOCK_8 Number(10)';
    exception when column_exists then null;
end;
/
-- Add/modify columns 
declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'alter table STANDARDOCCUPATION add OCC_C_TIMEBLOCK_9 Number(10)';
    exception when column_exists then null;
end;
/
-- Add/modify columns 
declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'alter table STANDARDOCCUPATION add OCC_C_TIMEBLOCK_10 Number(10)';
    exception when column_exists then null;
end;
/
--
-- STANDARDEMPLOYEEPLANNING
--
-- Add/modify columns 
declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'alter table STANDARDEMPLOYEEPLANNING add SCHEDULED_TIMEBLOCK_5 VARCHAR2(1)';
    exception when column_exists then null;
end;
/
-- Add/modify columns 
declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'alter table STANDARDEMPLOYEEPLANNING add SCHEDULED_TIMEBLOCK_6 VARCHAR2(1)';
    exception when column_exists then null;
end;
/
-- Add/modify columns 
declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'alter table STANDARDEMPLOYEEPLANNING add SCHEDULED_TIMEBLOCK_7 VARCHAR2(1)';
    exception when column_exists then null;
end;
/
-- Add/modify columns 
declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'alter table STANDARDEMPLOYEEPLANNING add SCHEDULED_TIMEBLOCK_8 VARCHAR2(1)';
    exception when column_exists then null;
end;
/
-- Add/modify columns 
declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'alter table STANDARDEMPLOYEEPLANNING add SCHEDULED_TIMEBLOCK_9 VARCHAR2(1)';
    exception when column_exists then null;
end;
/
-- Add/modify columns 
declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'alter table STANDARDEMPLOYEEPLANNING add SCHEDULED_TIMEBLOCK_10 VARCHAR2(1)';
    exception when column_exists then null;
end;
/
--
-- PIVOTEMPSTAFFPLANNING - Fields must be set to BYTE or the record-length is too long!
--
DECLARE
  sql_stmt VARCHAR2(200);
  varcol VARCHAR2(200);
BEGIN
  FOR x in (SELECT * FROM USER_TAB_COLUMNS WHERE column_name LIKE 'WK%' and data_type LIKE 'CHAR%' and char_length = 8 and table_name = 'PIVOTEMPSTAFFPLANNING')
  LOOP
    sql_stmt := 'ALTER TABLE '||x.table_name||' MODIFY "'||x.column_name||'" '||x.data_type||'('||20||' BYTE)';
    -- DBMS_OUTPUT.PUT_LINE ( sql_stmt ||';') ;
    EXECUTE IMMEDIATE sql_stmt;
  END LOOP;
END;
/
--
-- PIVOTOCISTAFFPLANNING - Fields must be set to BYTE or the record-length is too long!
--
DECLARE
  sql_stmt VARCHAR2(200);
  varcol VARCHAR2(200);
BEGIN
  FOR x in (SELECT * FROM USER_TAB_COLUMNS WHERE column_name LIKE 'WK%' and data_type LIKE 'CHAR%' and char_length = 24 and table_name = 'PIVOTOCISTAFFPLANNING')
  LOOP
    sql_stmt := 'ALTER TABLE '||x.table_name||' MODIFY "'||x.column_name||'" '||x.data_type||'('||60||' BYTE)';
    -- DBMS_OUTPUT.PUT_LINE ( sql_stmt ||';') ;
    EXECUTE IMMEDIATE sql_stmt;
  END LOOP;
END;
/
--
-- GF_GETTIMEBLOCKS
--
CREATE OR REPLACE PROCEDURE GF_GETTIMEBLOCKS (
    EMPLOYEE_NUMBER INTEGER,
    PLANT_CODE VARCHAR2,
    DEPARTMENT_CODE VARCHAR2,
    SHIFT_NUMBER INTEGER,
    TB1 OUT INTEGER,
    TB2 OUT INTEGER,
    TB3 OUT INTEGER,
    TB4 OUT INTEGER,
    TB5 OUT INTEGER,
    TB6 OUT INTEGER,
    TB7 OUT INTEGER,
    TB8 OUT INTEGER,
    TB9 OUT INTEGER,
    TB10 OUT INTEGER
    )
IS
  lvREADY INTEGER;
BEGIN
  /* Initialization */
  TB1 := 0; TB2 := 0;
  TB3 := 0; TB4 := 0;
  TB5 := 0; TB6 := 0;
  TB7 := 0; TB8 := 0;
  TB9 := 0; TB10 := 0;
  lvREADY := 0;
  /* TimeBlocks per Employee */
  FOR X IN 
  (
  SELECT TIMEBLOCK_NUMBER TB
  FROM TIMEBLOCKPEREMPLOYEE
  WHERE 
    EMPLOYEE_NUMBER = GF_GETTIMEBLOCKS.EMPLOYEE_NUMBER 
    AND PLANT_CODE = GF_GETTIMEBLOCKS.PLANT_CODE 
    AND SHIFT_NUMBER = GF_GETTIMEBLOCKS.SHIFT_NUMBER
  )
  LOOP
    BEGIN
      IF (X.TB=1) THEN TB1 := 1; END IF;
      IF (X.TB=2) THEN TB2 := 1; END IF;
      IF (X.TB=3) THEN TB3 := 1; END IF;
      IF (X.TB=4) THEN TB4 := 1; END IF;
      IF (X.TB=5) THEN TB5 := 1; END IF;
      IF (X.TB=6) THEN TB6 := 1; END IF;
      IF (X.TB=7) THEN TB7 := 1; END IF;
      IF (X.TB=8) THEN TB8 := 1; END IF;
      IF (X.TB=9) THEN TB9 := 1; END IF;
      IF (X.TB=10) THEN TB10 := 1; END IF;
      lvREADY := 1;
    END;
  END LOOP;
  IF (lvREADY <= 0) THEN 
    BEGIN
      /* TimeBlocs per Department */
      FOR X IN 
      (
      SELECT TIMEBLOCK_NUMBER TB
      FROM TIMEBLOCKPERDEPARTMENT
      WHERE 
        DEPARTMENT_CODE = GF_GETTIMEBLOCKS.DEPARTMENT_CODE 
        AND PLANT_CODE = GF_GETTIMEBLOCKS.PLANT_CODE 
        AND SHIFT_NUMBER = GF_GETTIMEBLOCKS.SHIFT_NUMBER
      )
      LOOP
        BEGIN
          IF (X.TB=1) THEN TB1 := 1; END IF;
          IF (X.TB=2) THEN TB2 := 1; END IF;
          IF (X.TB=3) THEN TB3 := 1; END IF;
          IF (X.TB=4) THEN TB4 := 1; END IF;
          IF (X.TB=5) THEN TB5 := 1; END IF;
          IF (X.TB=6) THEN TB6 := 1; END IF;
          IF (X.TB=7) THEN TB7 := 1; END IF;
          IF (X.TB=8) THEN TB8 := 1; END IF;
          IF (X.TB=9) THEN TB9 := 1; END IF;
          IF (X.TB=10) THEN TB10 := 1; END IF;
          lvREADY := 1;
        END;
      END LOOP;
      IF (lvREADY <= 0) THEN
        BEGIN
          /* Time blocks per Shift */
          FOR X IN 
          (
          SELECT TIMEBLOCK_NUMBER TB
          FROM TIMEBLOCKPERSHIFT
          WHERE 
            PLANT_CODE = GF_GETTIMEBLOCKS.PLANT_CODE 
            AND SHIFT_NUMBER = GF_GETTIMEBLOCKS.SHIFT_NUMBER
          )
          LOOP
            BEGIN
              IF (X.TB=1) THEN TB1 := 1; END IF;
              IF (X.TB=2) THEN TB2 := 1; END IF;
              IF (X.TB=3) THEN TB3 := 1; END IF;
              IF (X.TB=4) THEN TB4 := 1; END IF;
              IF (X.TB=5) THEN TB5 := 1; END IF;
              IF (X.TB=6) THEN TB6 := 1; END IF;
              IF (X.TB=7) THEN TB7 := 1; END IF;
              IF (X.TB=8) THEN TB8 := 1; END IF;
              IF (X.TB=9) THEN TB9 := 1; END IF;
              IF (X.TB=10) THEN TB10 := 1; END IF;
            END;
          END LOOP;
        END;
      END IF;
    END;
  END IF;
END GF_GETTIMEBLOCKS;
/
--
-- STAFFAVAIL_FILLSTAV
--
CREATE OR REPLACE PROCEDURE STAFFAVAIL_FILLSTAV (
    EMPLOYEE_NUMBER INTEGER,
    DEPARTMENT_CODE VARCHAR2,
    CURRENTDATE DATE,
    STARTDATE DATE,
    PLANT_CODE VARCHAR2,
    CREATIONDATE DATE,
    MUTATOR VARCHAR2,
    INSERTRECORD OUT INTEGER,
    AV1 OUT VARCHAR2,
    AV2 OUT VARCHAR2,
    AV3 OUT VARCHAR2,
    AV4 OUT VARCHAR2,
    AV5 OUT VARCHAR2,
    AV6 OUT VARCHAR2,
    AV7 OUT VARCHAR2,
    AV8 OUT VARCHAR2,
    AV9 OUT VARCHAR2,
    AV10 OUT VARCHAR2,
    AV11 OUT VARCHAR2,
    SPLANT OUT VARCHAR2,
    STATUS IN OUT INTEGER -- 20011800
    )
IS
  lvWEEKDAYINDEX INTEGER;
  lvPLNT VARCHAR2(6);
  lvSHNO INTEGER;
  lvTB1 INTEGER;
  lvTB2 INTEGER;
  lvTB3 INTEGER;
  lvTB4 INTEGER;
  lvTB5 INTEGER;
  lvTB6 INTEGER;
  lvTB7 INTEGER;
  lvTB8 INTEGER;
  lvTB9 INTEGER;
  lvTB10 INTEGER;
  lvILL VARCHAR2(1);
  lvUseFinalRunYN VARCHAR2(1);
  lvFinalRunExportDate DATE;
  lvGoOn BOOLEAN;
BEGIN
  lvSHNO := -2;
-- do not use '' in oracle  AV1 := ''; AV2 := ''; AV3 := ''; AV4 := ''; AV5 := ''; SPLANT := '';
-- use something which cannot be returned by the query
  AV1 := '~'; AV2 := '~'; AV3 := '~'; AV4 := '~'; AV5 := '~'; 
  AV6 := '~'; AV7 := '~'; AV8 := '~'; AV9 := '~'; AV10 := '~'; 
  AV11 := '~'; /* Used for Shift-number! */
  SPLANT := '';

  BEGIN
    INSERTRECORD := 0;
    SELECT MAX(PLANT_CODE), MAX(SHIFT_NUMBER)
    INTO lvPLNT, lvSHNO
    FROM SHIFTSCHEDULE
    WHERE
      (EMPLOYEE_NUMBER = STAFFAVAIL_FILLSTAV.EMPLOYEE_NUMBER)
      AND (SHIFT_SCHEDULE_DATE = STAFFAVAIL_FILLSTAV.CURRENTDATE);

    IF (lvSHNO = -1) THEN
      BEGIN
        AV1 := '~'; AV2 := '~'; AV3 := '~'; AV4 := '~'; AV5 := '-';
        AV6 := '~'; AV7 := '~'; AV8 := '~'; AV9 := '~'; AV10 := '-';
        AV11 := '-';
        INSERTRECORD := 1;
      END;
    END IF; --(lvSHNO = -1)

    IF (lvSHNO >= 0) THEN
    BEGIN
      -- 20011800 - Final Run System Check - BEGIN
      lvGoOn := TRUE;
      lvUseFinalRunYN := 'N';
      lvFinalRunExportDate := null;
      SELECT FINAL_RUN_YN
      INTO lvUseFinalRunYN
      FROM PIMSSETTING;
      IF lvUseFinalRunYN = 'Y' THEN
        BEGIN
          SELECT T.EXPORT_DATE
          INTO lvFinalRunExportDate
          FROM FINALRUN T
          WHERE T.PLANT_CODE = lvPLNT AND
          T.EXPORT_TYPE =
          (SELECT NVL(C.EXPORT_TYPE, NVL((SELECT MIN(EP.EXPORT_TYPE) FROM EXPORTPAYROLL EP),-1)) EXPORT_TYPE
          FROM PLANT P INNER JOIN COUNTRY C ON
          P.COUNTRY_ID = C.COUNTRY_ID
          WHERE P.PLANT_CODE = lvPLNT);
        EXCEPTION
          WHEN NO_DATA_FOUND
            THEN lvFinalRunExportDate := null;
        END; -- Exception
        IF lvFinalRunExportDate is not null THEN
          lvGoOn := STAFFAVAIL_FILLSTAV.CURRENTDATE > lvFinalRunExportDate;
          IF not lvGoOn THEN
            STATUS := STATUS + 1;
          END IF;
        END IF; -- (lvFinalRunExportDate)
      END IF; -- (lvUseFinalRunYN)
      -- 20011800 - Final Run System Check - END
      IF lvGoOn THEN
      BEGIN
        AV1 := '~'; AV2 := '~'; AV3 := '~'; AV4 := '~'; AV5 := '~'; 
        AV6 := '~'; AV7 := '~'; AV8 := '~'; AV9 := '~'; AV10 := '~'; 
        AV11 := TO_CHAR(lvSHNO);--CAST(SHNO AS VARCHAR(2));
        SPLANT := lvPLNT;
        INSERTRECORD := 1;
        lvWEEKDAYINDEX := STAFFAVAIL_FILLSTAV.CURRENTDATE - STAFFAVAIL_FILLSTAV.STARTDATE + 1;
        SELECT
          MAX(AVAILABLE_TIMEBLOCK_1), MAX(AVAILABLE_TIMEBLOCK_2),
          MAX(AVAILABLE_TIMEBLOCK_3), MAX(AVAILABLE_TIMEBLOCK_4),
          MAX(AVAILABLE_TIMEBLOCK_5), MAX(AVAILABLE_TIMEBLOCK_6),
          MAX(AVAILABLE_TIMEBLOCK_7), MAX(AVAILABLE_TIMEBLOCK_8),
          MAX(AVAILABLE_TIMEBLOCK_9), MAX(AVAILABLE_TIMEBLOCK_10)
        INTO AV1, AV2, AV3, AV4, AV5, AV6, AV7, AV8, AV9, AV10
        FROM  STANDARDAVAILABILITY
        WHERE
          (PLANT_CODE = lvPLNT)
          AND (DAY_OF_WEEK = lvWEEKDAYINDEX)
          AND (SHIFT_NUMBER = lvSHNO)
          AND (EMPLOYEE_NUMBER = STAFFAVAIL_FILLSTAV.EMPLOYEE_NUMBER);
    /*Available TimeBlocks*/
        /*EXECUTE PROCEDURE*/
        GF_GETTIMEBLOCKS(STAFFAVAIL_FILLSTAV.EMPLOYEE_NUMBER, lvPLNT,
          STAFFAVAIL_FILLSTAV.DEPARTMENT_CODE, lvSHNO,
        /*RETURNING_VALUES*/
          lvTB1, lvTB2, lvTB3, lvTB4, lvTB5, lvTB6, lvTB7, lvTB8, lvTB9, lvTB10);
        IF (lvTB1 = 0) THEN BEGIN AV1 := '~'; END; END IF;
        IF (lvTB2 = 0) THEN BEGIN AV2 := '~'; END; END IF;
        IF (lvTB3 = 0) THEN BEGIN AV3 := '~'; END; END IF;
        IF (lvTB4 = 0) THEN BEGIN AV4 := '~'; END; END IF;
        IF (lvTB5 = 0) THEN BEGIN AV5 := '~'; END; END IF;
        IF (lvTB6 = 0) THEN BEGIN AV6 := '~'; END; END IF;
        IF (lvTB7 = 0) THEN BEGIN AV7 := '~'; END; END IF;
        IF (lvTB8 = 0) THEN BEGIN AV8 := '~'; END; END IF;
        IF (lvTB9 = 0) THEN BEGIN AV9 := '~'; END; END IF;
        IF (lvTB10 = 0) THEN BEGIN AV10 := '~'; END; END IF;
    /*Illness Messages*/
        IF ((AV1 = '*') OR (AV2 = '*') OR (AV3 = '*') OR (AV4 = '*') 
             OR (AV5 = '*') OR (AV6 = '*') OR (AV7 = '*') OR (AV8 = '*')
             OR (AV9 = '*') OR (AV10 = '*')) THEN
          BEGIN
            lvILL := '#';
            SELECT MIN(ABSENCEREASON_CODE)
            INTO lvILL
            FROM ILLNESSMESSAGE
            WHERE
              (EMPLOYEE_NUMBER = STAFFAVAIL_FILLSTAV.EMPLOYEE_NUMBER)
              AND (ILLNESSMESSAGE_STARTDATE <= STAFFAVAIL_FILLSTAV.CURRENTDATE)
              AND ((ILLNESSMESSAGE_ENDDATE >= STAFFAVAIL_FILLSTAV.CURRENTDATE)
                OR (ILLNESSMESSAGE_ENDDATE IS NULL));
            IF (lvILL <> '#') THEN
              BEGIN
                IF ((lvTB1 > 0) AND (AV1 = '*')) THEN AV1 := lvILL; END IF;
                IF ((lvTB2 > 0) AND (AV2 = '*')) THEN AV2 := lvILL; END IF;
                IF ((lvTB3 > 0) AND (AV3 = '*')) THEN AV3 := lvILL; END IF;
                IF ((lvTB4 > 0) AND (AV4 = '*')) THEN AV4 := lvILL; END IF;
                IF ((lvTB5 > 0) AND (AV5 = '*')) THEN AV5 := lvILL; END IF;
                IF ((lvTB6 > 0) AND (AV6 = '*')) THEN AV6 := lvILL; END IF;
                IF ((lvTB7 > 0) AND (AV7 = '*')) THEN AV7 := lvILL; END IF;
                IF ((lvTB8 > 0) AND (AV8 = '*')) THEN AV8 := lvILL; END IF;
                IF ((lvTB9 > 0) AND (AV9 = '*')) THEN AV9 := lvILL; END IF;
                IF ((lvTB10 > 0) AND (AV10 = '*')) THEN AV10 := lvILL; END IF;
              END;
            END IF; --(ILL <> '#')
          END ;
        END IF; --((AV1 = '*') OR (AV2 = '*') OR (AV3 = '*') OR (AV4 = '*')) etc.
    /**/
        IF (((lvTB1 > 0) OR (lvTB2 > 0) OR (lvTB3 > 0) OR (lvTB4 > 0)) AND
          ((AV1 <> '~') OR (AV2 <> '~') OR (AV3 <> '~') OR (AV4 <> '~'))) THEN
          BEGIN
                IF (AV1 = '~') THEN AV1 := ''; END IF;
                IF (AV2 = '~') THEN AV2 := ''; END IF;
                IF (AV3 = '~') THEN AV3 := ''; END IF;
                IF (AV4 = '~') THEN AV4 := ''; END IF;
                IF (AV5 = '~') THEN AV5 := ''; END IF;
                IF (AV6 = '~') THEN AV6 := ''; END IF;
                IF (AV7 = '~') THEN AV7 := ''; END IF;
                IF (AV8 = '~') THEN AV8 := ''; END IF;
                IF (AV9 = '~') THEN AV9 := ''; END IF;
                IF (AV10 = '~') THEN AV10 := ''; END IF;
            begin
    /*              dbms_output.put_line(''||AV1||AV2||AV3||AV4
               || ' PLANT: ' ||   lvPLNT
               || ' DATA: ' ||   STAFFAVAIL_FILLSTAV.CURRENTDATE
               || ' SHIFT: ' ||   lvSHNO
               || ' EMP: ' ||   STAFFAVAIL_FILLSTAV.EMPLOYEE_NUMBER);*/
                INSERT INTO EMPLOYEEAVAILABILITY(
                  PLANT_CODE, EMPLOYEEAVAILABILITY_DATE,
                  SHIFT_NUMBER, EMPLOYEE_NUMBER,
                  AVAILABLE_TIMEBLOCK_1, AVAILABLE_TIMEBLOCK_2,
                  AVAILABLE_TIMEBLOCK_3, AVAILABLE_TIMEBLOCK_4,
                  AVAILABLE_TIMEBLOCK_5, AVAILABLE_TIMEBLOCK_6,
                  AVAILABLE_TIMEBLOCK_7, AVAILABLE_TIMEBLOCK_8,
                  AVAILABLE_TIMEBLOCK_9, AVAILABLE_TIMEBLOCK_10,
                  CREATIONDATE, MUTATIONDATE,MUTATOR)
                VALUES(
                  lvPLNT, STAFFAVAIL_FILLSTAV.CURRENTDATE,
                  lvSHNO, STAFFAVAIL_FILLSTAV.EMPLOYEE_NUMBER,
                  AV1, AV2, AV3, AV4, AV5, AV6, AV7, AV8, AV9, AV10,
                  STAFFAVAIL_FILLSTAV.CREATIONDATE, STAFFAVAIL_FILLSTAV.CREATIONDATE, STAFFAVAIL_FILLSTAV.MUTATOR);
            END;
          exception
            when dup_val_on_index then
              begin
        --          dbms_output.put_line('Error: ' || substr(sqlerrm, 1, 200));
    /*              dbms_output.put_line('DUPLICATE'||AV1||AV2||AV3||AV4||AV5||AV6||AV7||AV8||AV9||AV10
               || ' PLANT: ' ||   lvPLNT
               || ' DATA: ' ||   STAFFAVAIL_FILLSTAV.CURRENTDATE
               || ' SHIFT: ' ||   lvSHNO
               || ' EMP: ' ||   STAFFAVAIL_FILLSTAV.EMPLOYEE_NUMBER);*/
                INSERTRECORD := 0;
              end;
      end;
        END IF; --((TB1>0) OR (TB2>0) OR (TB3>0) OR (TB4>0) etc.
      END;
      END IF; -- (lvGoOn)
    END;
    END IF; --(lvSHNO >= 0)
  END;
END STAFFAVAIL_FILLSTAV;
/
--
-- STAFFPLANNING_FILLSTDEPL
--
CREATE OR REPLACE PROCEDURE STAFFPLANNING_FILLSTDEPL (
    PLANT VARCHAR2,
    EMPLOYEE INTEGER,
    SHIFT INTEGER,
    WORKSPOT VARCHAR2,
    DEPARTMENT VARCHAR2,
    SCHEDULED_TIMEBLOCK_1 VARCHAR2,
    SCHEDULED_TIMEBLOCK_2 VARCHAR2,
    SCHEDULED_TIMEBLOCK_3 VARCHAR2,
    SCHEDULED_TIMEBLOCK_4 VARCHAR2,
    SCHEDULED_TIMEBLOCK_5 VARCHAR2,
    SCHEDULED_TIMEBLOCK_6 VARCHAR2,
    SCHEDULED_TIMEBLOCK_7 VARCHAR2,
    SCHEDULED_TIMEBLOCK_8 VARCHAR2,
    SCHEDULED_TIMEBLOCK_9 VARCHAR2,
    SCHEDULED_TIMEBLOCK_10 VARCHAR2,
    DAYEPL INTEGER,
    DATENOW DATE,
    MUTATOR VARCHAR2)
IS
  lvRECCOUNT INTEGER;
BEGIN
  SELECT COUNT(*)
  INTO lvRECCOUNT
  FROM STANDARDEMPLOYEEPLANNING
  WHERE
    (EMPLOYEE_NUMBER = STAFFPLANNING_FILLSTDEPL.EMPLOYEE)
    AND (DAY_OF_WEEK = STAFFPLANNING_FILLSTDEPL.DAYEPL)
    AND (DEPARTMENT_CODE = STAFFPLANNING_FILLSTDEPL.DEPARTMENT)
    AND (WORKSPOT_CODE = STAFFPLANNING_FILLSTDEPL.WORKSPOT)
    AND (SHIFT_NUMBER = STAFFPLANNING_FILLSTDEPL.SHIFT)
    AND (PLANT_CODE = STAFFPLANNING_FILLSTDEPL.PLANT)
  ;
  IF (lvRECCOUNT >= 1)
  THEN
    BEGIN
      UPDATE STANDARDEMPLOYEEPLANNING
      SET
        SCHEDULED_TIMEBLOCK_1 = STAFFPLANNING_FILLSTDEPL.SCHEDULED_TIMEBLOCK_1,
        SCHEDULED_TIMEBLOCK_2 = STAFFPLANNING_FILLSTDEPL.SCHEDULED_TIMEBLOCK_2,
        SCHEDULED_TIMEBLOCK_3 = STAFFPLANNING_FILLSTDEPL.SCHEDULED_TIMEBLOCK_3,
        SCHEDULED_TIMEBLOCK_4 = STAFFPLANNING_FILLSTDEPL.SCHEDULED_TIMEBLOCK_4,
        SCHEDULED_TIMEBLOCK_5 = STAFFPLANNING_FILLSTDEPL.SCHEDULED_TIMEBLOCK_5,
        SCHEDULED_TIMEBLOCK_6 = STAFFPLANNING_FILLSTDEPL.SCHEDULED_TIMEBLOCK_6,
        SCHEDULED_TIMEBLOCK_7 = STAFFPLANNING_FILLSTDEPL.SCHEDULED_TIMEBLOCK_7,
        SCHEDULED_TIMEBLOCK_8 = STAFFPLANNING_FILLSTDEPL.SCHEDULED_TIMEBLOCK_8,
        SCHEDULED_TIMEBLOCK_9 = STAFFPLANNING_FILLSTDEPL.SCHEDULED_TIMEBLOCK_9,
        SCHEDULED_TIMEBLOCK_10 = STAFFPLANNING_FILLSTDEPL.SCHEDULED_TIMEBLOCK_10,
        CREATIONDATE = STAFFPLANNING_FILLSTDEPL.DATENOW,
        MUTATIONDATE = STAFFPLANNING_FILLSTDEPL.DATENOW,
        MUTATOR = STAFFPLANNING_FILLSTDEPL.MUTATOR
      WHERE
        (EMPLOYEE_NUMBER = STAFFPLANNING_FILLSTDEPL.EMPLOYEE)
        AND (DAY_OF_WEEK = STAFFPLANNING_FILLSTDEPL.DAYEPL)
        AND (DEPARTMENT_CODE = STAFFPLANNING_FILLSTDEPL.DEPARTMENT)
        AND (WORKSPOT_CODE = STAFFPLANNING_FILLSTDEPL.WORKSPOT)
        AND SHIFT_NUMBER = STAFFPLANNING_FILLSTDEPL.SHIFT
        AND PLANT_CODE = STAFFPLANNING_FILLSTDEPL.PLANT;
    END;
  ELSE
    BEGIN
      IF
        ((SCHEDULED_TIMEBLOCK_1 <> 'N')
        OR (SCHEDULED_TIMEBLOCK_2 <> 'N')
        OR (SCHEDULED_TIMEBLOCK_3 <> 'N')
        OR (SCHEDULED_TIMEBLOCK_4 <> 'N')
        OR (SCHEDULED_TIMEBLOCK_5 <> 'N')
        OR (SCHEDULED_TIMEBLOCK_6 <> 'N')
        OR (SCHEDULED_TIMEBLOCK_7 <> 'N')
        OR (SCHEDULED_TIMEBLOCK_8 <> 'N')
        OR (SCHEDULED_TIMEBLOCK_9 <> 'N')
        OR (SCHEDULED_TIMEBLOCK_10 <> 'N'))
      THEN
        BEGIN
          INSERT INTO STANDARDEMPLOYEEPLANNING (
            PLANT_CODE, DAY_OF_WEEK, 
            SHIFT_NUMBER, DEPARTMENT_CODE,
            WORKSPOT_CODE, EMPLOYEE_NUMBER,
            SCHEDULED_TIMEBLOCK_1, SCHEDULED_TIMEBLOCK_2,
            SCHEDULED_TIMEBLOCK_3, SCHEDULED_TIMEBLOCK_4,
            SCHEDULED_TIMEBLOCK_5, SCHEDULED_TIMEBLOCK_6,
            SCHEDULED_TIMEBLOCK_7, SCHEDULED_TIMEBLOCK_8,
            SCHEDULED_TIMEBLOCK_9, SCHEDULED_TIMEBLOCK_10,
            CREATIONDATE, MUTATIONDATE, MUTATOR)
          VALUES(
            STAFFPLANNING_FILLSTDEPL.PLANT, STAFFPLANNING_FILLSTDEPL.DAYEPL, 
            STAFFPLANNING_FILLSTDEPL.SHIFT, STAFFPLANNING_FILLSTDEPL.DEPARTMENT,
            STAFFPLANNING_FILLSTDEPL.WORKSPOT, STAFFPLANNING_FILLSTDEPL.EMPLOYEE,
            STAFFPLANNING_FILLSTDEPL.SCHEDULED_TIMEBLOCK_1, STAFFPLANNING_FILLSTDEPL.SCHEDULED_TIMEBLOCK_2,
            STAFFPLANNING_FILLSTDEPL.SCHEDULED_TIMEBLOCK_3, STAFFPLANNING_FILLSTDEPL.SCHEDULED_TIMEBLOCK_4,
            STAFFPLANNING_FILLSTDEPL.SCHEDULED_TIMEBLOCK_5, STAFFPLANNING_FILLSTDEPL.SCHEDULED_TIMEBLOCK_6,
            STAFFPLANNING_FILLSTDEPL.SCHEDULED_TIMEBLOCK_7, STAFFPLANNING_FILLSTDEPL.SCHEDULED_TIMEBLOCK_8,
            STAFFPLANNING_FILLSTDEPL.SCHEDULED_TIMEBLOCK_9, STAFFPLANNING_FILLSTDEPL.SCHEDULED_TIMEBLOCK_10,
            STAFFPLANNING_FILLSTDEPL.DATENOW, STAFFPLANNING_FILLSTDEPL.DATENOW, STAFFPLANNING_FILLSTDEPL.MUTATOR);
        END;   
      END IF;
    END;
  END IF;
  COMMIT;
END STAFFPLANNING_FILLSTDEPL;
/
--
-- STAFFPLANNING_FILLEPL
--
CREATE OR REPLACE PROCEDURE STAFFPLANNING_FILLEPL (
    PLANT VARCHAR2,
    EMPLOYEE INTEGER,
    SHIFT INTEGER,
    WORKSPOT VARCHAR2,
    DEPARTMENT VARCHAR2,
    SCHEDULED_TIMEBLOCK_1 VARCHAR2,
    SCHEDULED_TIMEBLOCK_2 VARCHAR2,
    SCHEDULED_TIMEBLOCK_3 VARCHAR2,
    SCHEDULED_TIMEBLOCK_4 VARCHAR2,
    SCHEDULED_TIMEBLOCK_5 VARCHAR2,
    SCHEDULED_TIMEBLOCK_6 VARCHAR2,
    SCHEDULED_TIMEBLOCK_7 VARCHAR2,
    SCHEDULED_TIMEBLOCK_8 VARCHAR2,
    SCHEDULED_TIMEBLOCK_9 VARCHAR2,
    SCHEDULED_TIMEBLOCK_10 VARCHAR2,
    DATEEPL DATE,
    DATENOW DATE,
    MUTATOR VARCHAR2)
IS
  lvRECCOUNT INTEGER;
BEGIN
  SELECT COUNT(*)
  INTO lvRECCOUNT
  FROM EMPLOYEEPLANNING
  WHERE
    (EMPLOYEE_NUMBER = STAFFPLANNING_FILLEPL.EMPLOYEE)
    AND (EMPLOYEEPLANNING_DATE = STAFFPLANNING_FILLEPL.DATEEPL)
    AND (DEPARTMENT_CODE = STAFFPLANNING_FILLEPL.DEPARTMENT)
    AND (WORKSPOT_CODE = STAFFPLANNING_FILLEPL.WORKSPOT)
    AND (SHIFT_NUMBER = STAFFPLANNING_FILLEPL.SHIFT)
    AND (PLANT_CODE = STAFFPLANNING_FILLEPL.PLANT)
  ;
  IF (lvRECCOUNT >= 1)
  THEN
    BEGIN
      UPDATE EMPLOYEEPLANNING
      SET
        SCHEDULED_TIMEBLOCK_1 = STAFFPLANNING_FILLEPL.SCHEDULED_TIMEBLOCK_1,
        SCHEDULED_TIMEBLOCK_2 = STAFFPLANNING_FILLEPL.SCHEDULED_TIMEBLOCK_2,
        SCHEDULED_TIMEBLOCK_3 = STAFFPLANNING_FILLEPL.SCHEDULED_TIMEBLOCK_3,
        SCHEDULED_TIMEBLOCK_4 = STAFFPLANNING_FILLEPL.SCHEDULED_TIMEBLOCK_4,
        SCHEDULED_TIMEBLOCK_5 = STAFFPLANNING_FILLEPL.SCHEDULED_TIMEBLOCK_5,
        SCHEDULED_TIMEBLOCK_6 = STAFFPLANNING_FILLEPL.SCHEDULED_TIMEBLOCK_6,
        SCHEDULED_TIMEBLOCK_7 = STAFFPLANNING_FILLEPL.SCHEDULED_TIMEBLOCK_7,
        SCHEDULED_TIMEBLOCK_8 = STAFFPLANNING_FILLEPL.SCHEDULED_TIMEBLOCK_8,
        SCHEDULED_TIMEBLOCK_9 = STAFFPLANNING_FILLEPL.SCHEDULED_TIMEBLOCK_9,
        SCHEDULED_TIMEBLOCK_10 = STAFFPLANNING_FILLEPL.SCHEDULED_TIMEBLOCK_10,
        CREATIONDATE = STAFFPLANNING_FILLEPL.DATENOW,
        MUTATIONDATE = STAFFPLANNING_FILLEPL.DATENOW,
        MUTATOR = STAFFPLANNING_FILLEPL.MUTATOR
      WHERE
        (EMPLOYEE_NUMBER = STAFFPLANNING_FILLEPL.EMPLOYEE)
        AND (EMPLOYEEPLANNING_DATE = STAFFPLANNING_FILLEPL.DATEEPL)
        AND (DEPARTMENT_CODE = STAFFPLANNING_FILLEPL.DEPARTMENT)
        AND (WORKSPOT_CODE = STAFFPLANNING_FILLEPL.WORKSPOT)
        AND SHIFT_NUMBER = STAFFPLANNING_FILLEPL.SHIFT
        AND PLANT_CODE = STAFFPLANNING_FILLEPL.PLANT;
    END;
  ELSE
    BEGIN
      IF
        ((SCHEDULED_TIMEBLOCK_1 <> 'N')
        OR (SCHEDULED_TIMEBLOCK_2 <> 'N')
        OR (SCHEDULED_TIMEBLOCK_3 <> 'N')
        OR (SCHEDULED_TIMEBLOCK_4 <> 'N')
        OR (SCHEDULED_TIMEBLOCK_5 <> 'N')
        OR (SCHEDULED_TIMEBLOCK_6 <> 'N')
        OR (SCHEDULED_TIMEBLOCK_7 <> 'N')
        OR (SCHEDULED_TIMEBLOCK_8 <> 'N')
        OR (SCHEDULED_TIMEBLOCK_9 <> 'N')
        OR (SCHEDULED_TIMEBLOCK_10 <> 'N'))
      THEN
        BEGIN
          INSERT INTO EMPLOYEEPLANNING (
            PLANT_CODE, EMPLOYEEPLANNING_DATE, 
            SHIFT_NUMBER, DEPARTMENT_CODE,
            WORKSPOT_CODE, EMPLOYEE_NUMBER,
            SCHEDULED_TIMEBLOCK_1, SCHEDULED_TIMEBLOCK_2,
            SCHEDULED_TIMEBLOCK_3, SCHEDULED_TIMEBLOCK_4,
            SCHEDULED_TIMEBLOCK_5, SCHEDULED_TIMEBLOCK_6,
            SCHEDULED_TIMEBLOCK_7, SCHEDULED_TIMEBLOCK_8,
            SCHEDULED_TIMEBLOCK_9, SCHEDULED_TIMEBLOCK_10,
            CREATIONDATE, MUTATIONDATE, MUTATOR)
          VALUES(
            STAFFPLANNING_FILLEPL.PLANT, STAFFPLANNING_FILLEPL.DATEEPL, 
            STAFFPLANNING_FILLEPL.SHIFT, STAFFPLANNING_FILLEPL.DEPARTMENT,
            STAFFPLANNING_FILLEPL.WORKSPOT, STAFFPLANNING_FILLEPL.EMPLOYEE,
            STAFFPLANNING_FILLEPL.SCHEDULED_TIMEBLOCK_1, STAFFPLANNING_FILLEPL.SCHEDULED_TIMEBLOCK_2,
            STAFFPLANNING_FILLEPL.SCHEDULED_TIMEBLOCK_3, STAFFPLANNING_FILLEPL.SCHEDULED_TIMEBLOCK_4,
            STAFFPLANNING_FILLEPL.SCHEDULED_TIMEBLOCK_5, STAFFPLANNING_FILLEPL.SCHEDULED_TIMEBLOCK_6,
            STAFFPLANNING_FILLEPL.SCHEDULED_TIMEBLOCK_7, STAFFPLANNING_FILLEPL.SCHEDULED_TIMEBLOCK_8,
            STAFFPLANNING_FILLEPL.SCHEDULED_TIMEBLOCK_9, STAFFPLANNING_FILLEPL.SCHEDULED_TIMEBLOCK_10,
            STAFFPLANNING_FILLEPL.DATENOW, STAFFPLANNING_FILLEPL.DATENOW, STAFFPLANNING_FILLEPL.MUTATOR) ;
        END;   
      END IF;
    END;
  END IF;
  COMMIT;
END STAFFPLANNING_FILLEPL;
/
--
-- STAFFPLANNING_INSERTPIVOTEMP
--
CREATE OR REPLACE PROCEDURE STAFFPLANNING_INSERTPIVOTEMP (
    PIVOTCOMPUTERNAME VARCHAR2,
    PLANT_CODE VARCHAR2,
    EMP_PLANT_CODE VARCHAR2,
    TEAMFROM VARCHAR2,
    TEAMTO VARCHAR2,
    USER_NAME VARCHAR2,
    CURRENTDATE DATE,
    ONLYEMPAVAIL INTEGER,
    TYPEPLANNING INTEGER,
    SHIFT INTEGER,
    DAY_WEEK INTEGER,
    DATESELECTION DATE)
IS
  lvSTR VARCHAR2(20); /* 8 (4x2) -> 20 (10x2) */
  lvINSERTOK INTEGER;
BEGIN
   DELETE FROM PIVOTEMPSTAFFPLANNING WHERE PIVOTCOMPUTERNAME = STAFFPLANNING_INSERTPIVOTEMP.PIVOTCOMPUTERNAME;
   lvSTR := '20202020202020202020';
   FOR X IN
   (
   SELECT
     E.EMPLOYEE_NUMBER,
     CASE
       WHEN E.PLANT_CODE = STAFFPLANNING_INSERTPIVOTEMP.PLANT_CODE THEN E.DESCRIPTION
       WHEN E.PLANT_CODE <> STAFFPLANNING_INSERTPIVOTEMP.PLANT_CODE THEN E.DESCRIPTION || ' (' || E.PLANT_CODE || ')'
     END DESCRIPTION,
     E.PLANT_CODE EMPLOYEE_PLANT_CODE
   FROM EMPLOYEE E
   WHERE
     (E.PLANT_CODE = STAFFPLANNING_INSERTPIVOTEMP.PLANT_CODE OR STAFFPLANNING_INSERTPIVOTEMP.EMP_PLANT_CODE = '*') AND
     (
       ((STAFFPLANNING_INSERTPIVOTEMP.USER_NAME = '*') OR
        (E.TEAM_CODE IN
          (SELECT U.TEAM_CODE FROM TEAMPERUSER U WHERE U.USER_NAME = STAFFPLANNING_INSERTPIVOTEMP.USER_NAME)))
       AND
       ((STAFFPLANNING_INSERTPIVOTEMP.TEAMFROM = '*') OR (STAFFPLANNING_INSERTPIVOTEMP.TEAMTO = '*')
         OR ((E.TEAM_CODE >= STAFFPLANNING_INSERTPIVOTEMP.TEAMFROM)
           AND (E.TEAM_CODE <= STAFFPLANNING_INSERTPIVOTEMP.TEAMTO)))
      )
     AND
    (E.STARTDATE <= STAFFPLANNING_INSERTPIVOTEMP.CURRENTDATE)
    AND ((E.ENDDATE >= STAFFPLANNING_INSERTPIVOTEMP.CURRENTDATE ) OR
    (E.ENDDATE IS NULL))
   )
--   ;
   LOOP --DO
     BEGIN
       lvINSERTOK := 1;
       IF ((ONLYEMPAVAIL = 1)  OR (STAFFPLANNING_INSERTPIVOTEMP.EMP_PLANT_CODE = '*')) THEN
         BEGIN lvINSERTOK  := -1;
           IF (TYPEPLANNING = 0)  THEN
             BEGIN
               SELECT COUNT(*)
               INTO lvINSERTOK
               FROM EMPLOYEEAVAILABILITY
               WHERE (PLANT_CODE = STAFFPLANNING_INSERTPIVOTEMP.PLANT_CODE) AND
                 SHIFT_NUMBER = STAFFPLANNING_INSERTPIVOTEMP.SHIFT AND
                 EMPLOYEEAVAILABILITY_DATE = STAFFPLANNING_INSERTPIVOTEMP.DATESELECTION AND
                 EMPLOYEE_NUMBER = X.EMPLOYEE_NUMBER AND
                 (((AVAILABLE_TIMEBLOCK_1 = '*') AND (AVAILABLE_TIMEBLOCK_1 IS NOT NULL)) OR
                  ((AVAILABLE_TIMEBLOCK_2 = '*') AND (AVAILABLE_TIMEBLOCK_2 IS NOT NULL)) OR
                  ((AVAILABLE_TIMEBLOCK_3 = '*') AND (AVAILABLE_TIMEBLOCK_3 IS NOT NULL)) OR
                  ((AVAILABLE_TIMEBLOCK_4 = '*') AND (AVAILABLE_TIMEBLOCK_4 IS NOT NULL)) OR
                  ((AVAILABLE_TIMEBLOCK_5 = '*') AND (AVAILABLE_TIMEBLOCK_5 IS NOT NULL)) OR
                  ((AVAILABLE_TIMEBLOCK_6 = '*') AND (AVAILABLE_TIMEBLOCK_6 IS NOT NULL)) OR
                  ((AVAILABLE_TIMEBLOCK_7 = '*') AND (AVAILABLE_TIMEBLOCK_7 IS NOT NULL)) OR
                  ((AVAILABLE_TIMEBLOCK_8 = '*') AND (AVAILABLE_TIMEBLOCK_8 IS NOT NULL)) OR
                  ((AVAILABLE_TIMEBLOCK_9 = '*') AND (AVAILABLE_TIMEBLOCK_9 IS NOT NULL)) OR
                  ((AVAILABLE_TIMEBLOCK_10 = '*') AND (AVAILABLE_TIMEBLOCK_10 IS NOT NULL)));
               IF (lvINSERTOK <> 1) THEN
                 BEGIN
                   SELECT COUNT(*)
                   INTO lvINSERTOK
                   FROM SHIFTSCHEDULE H, STANDARDAVAILABILITY S
                   WHERE H.EMPLOYEE_NUMBER = X.EMPLOYEE_NUMBER AND
                     H.SHIFT_SCHEDULE_DATE = STAFFPLANNING_INSERTPIVOTEMP.DATESELECTION
                     AND H.SHIFT_NUMBER = 0 AND
                     S.EMPLOYEE_NUMBER = H.EMPLOYEE_NUMBER
                     AND (S.PLANT_CODE = STAFFPLANNING_INSERTPIVOTEMP.PLANT_CODE) AND
                     S.SHIFT_NUMBER = STAFFPLANNING_INSERTPIVOTEMP.SHIFT
                     AND S.DAY_OF_WEEK = STAFFPLANNING_INSERTPIVOTEMP.DAY_WEEK;
                 END;
               END IF;--(INSERTOK <> 1)
             END;
           END IF;--(TYPEPLANNING = 0)
           IF (TYPEPLANNING = 1) THEN
             BEGIN
               SELECT COUNT(*)
               INTO lvINSERTOK
               FROM STANDARDAVAILABILITY SA WHERE
                 (SA.PLANT_CODE = STAFFPLANNING_INSERTPIVOTEMP.PLANT_CODE)
                 AND (SA.SHIFT_NUMBER = STAFFPLANNING_INSERTPIVOTEMP.SHIFT)
                 AND (SA.DAY_OF_WEEK = STAFFPLANNING_INSERTPIVOTEMP.DAY_WEEK)
                 AND  (SA.EMPLOYEE_NUMBER = X.EMPLOYEE_NUMBER) AND
                ((SA.AVAILABLE_TIMEBLOCK_1 = '*') OR 
                 (SA.AVAILABLE_TIMEBLOCK_2 = '*') OR
                 (SA.AVAILABLE_TIMEBLOCK_3 = '*') OR 
                 (SA.AVAILABLE_TIMEBLOCK_4 = '*') OR
                 (SA.AVAILABLE_TIMEBLOCK_5 = '*') OR 
                 (SA.AVAILABLE_TIMEBLOCK_6 = '*') OR
                 (SA.AVAILABLE_TIMEBLOCK_7 = '*') OR 
                 (SA.AVAILABLE_TIMEBLOCK_8 = '*') OR
                 (SA.AVAILABLE_TIMEBLOCK_9 = '*') OR 
                 (SA.AVAILABLE_TIMEBLOCK_10 = '*'));
             END;
           END IF;
         END;
       END IF;--(ONLYEMPAVAIL = 1)
       IF (STAFFPLANNING_INSERTPIVOTEMP.EMP_PLANT_CODE = '*') THEN
       BEGIN
         IF ((ONLYEMPAVAIL = 0) AND (lvINSERTOK <> 1) AND (PLANT_CODE = X.EMPLOYEE_PLANT_CODE)) THEN
           lvINSERTOK := 1;
         END IF;
       END; END IF;
       IF (lvINSERTOK = 1) THEN
       BEGIN
         IF (ONLYEMPAVAIL = 1) THEN lvSTR := ' 7 7 7 7 7 7 7 7 7 7'; END IF;
         /*EXECUTE PROCEDURE*/
         STAFFPLANNING_INSRECPIVOTEMP(STAFFPLANNING_INSERTPIVOTEMP.PIVOTCOMPUTERNAME,
           X.EMPLOYEE_NUMBER, X.DESCRIPTION, lvSTR);
       END; END IF;--(INSERTOK = 1)
    END;
  END LOOP;
  COMMIT;
END STAFFPLANNING_INSERTPIVOTEMP;
/
--
-- STAFFPLANNING_INSERTPIVOTOCI
--
CREATE OR REPLACE PROCEDURE STAFFPLANNING_INSERTPIVOTOCI (
    PIVOTCOMPUTERNAME VARCHAR2)
IS
  lvVAL VARCHAR2(60); /* 24 (4x6)-> 60 (10x6) */
BEGIN
  lvVAL := '     0     0     0     0     0     0     0     0     0     0';
  DELETE FROM PIVOTOCISTAFFPLANNING WHERE
    PIVOTCOMPUTERNAME = STAFFPLANNING_INSERTPIVOTOCI.PIVOTCOMPUTERNAME; 
  /*EXECUTE PROCEDURE*/
  STAFFPLANNING_INSERTPIVOTLEVEL(PIVOTCOMPUTERNAME , 'A1', lvVAL);
  STAFFPLANNING_INSERTPIVOTLEVEL(PIVOTCOMPUTERNAME , 'A2', lvVAL);
  STAFFPLANNING_INSERTPIVOTLEVEL(PIVOTCOMPUTERNAME , 'B1', lvVAL);
  STAFFPLANNING_INSERTPIVOTLEVEL(PIVOTCOMPUTERNAME , 'B2', lvVAL);
  STAFFPLANNING_INSERTPIVOTLEVEL(PIVOTCOMPUTERNAME , 'C1', lvVAL);
  STAFFPLANNING_INSERTPIVOTLEVEL(PIVOTCOMPUTERNAME , 'C2', lvVAL);
  STAFFPLANNING_INSERTPIVOTLEVEL(PIVOTCOMPUTERNAME , 'D1', lvVAL);
  STAFFPLANNING_INSERTPIVOTLEVEL(PIVOTCOMPUTERNAME , 'D2', lvVAL);
  COMMIT;
END STAFFPLANNING_INSERTPIVOTOCI;
/ 
--
-- STAFFAVAIL_QUERY
--
CREATE OR REPLACE PROCEDURE STAFFAVAIL_QUERY (
/*
This procedure is now only inserting records and not returning a result-set like in interbase.
On the client the procedure is called and folloed by a select statement to get the result-set
*/
    TEAMFROM VARCHAR2,
    TEAMTO VARCHAR2,
    EMP_PLANT_CODE VARCHAR2,
    PLANT_CODE VARCHAR2,
    USER_NAME VARCHAR2,
    STARTDATE DATE,
    CREATIONDATE DATE,
    MUTATOR VARCHAR2,
    INSERT_STAV VARCHAR2,
    RECNO OUT INTEGER,
    EMPLOYEE_NUMBER OUT INTEGER,
    DESCRIPTION OUT VARCHAR2,
    DEPARTMENT_CODE OUT VARCHAR2,
    EMPPLANT_CODE OUT VARCHAR2,
    EMPSTARTDATE OUT DATE,
    EMPENDDATE OUT DATE,
    D11 OUT VARCHAR2, D12 OUT VARCHAR2, D13 OUT VARCHAR2, D14 OUT VARCHAR2, D15 OUT VARCHAR2,
    D21 OUT VARCHAR2, D22 OUT VARCHAR2, D23 OUT VARCHAR2, D24 OUT VARCHAR2, D25 OUT VARCHAR2,
    D31 OUT VARCHAR2, D32 OUT VARCHAR2, D33 OUT VARCHAR2, D34 OUT VARCHAR2, D35 OUT VARCHAR2,
    D41 OUT VARCHAR2, D42 OUT VARCHAR2, D43 OUT VARCHAR2, D44 OUT VARCHAR2, D45 OUT VARCHAR2,
    D51 OUT VARCHAR2, D52 OUT VARCHAR2, D53 OUT VARCHAR2, D54 OUT VARCHAR2, D55 OUT VARCHAR2,
    D61 OUT VARCHAR2, D62 OUT VARCHAR2, D63 OUT VARCHAR2, D64 OUT VARCHAR2, D65 OUT VARCHAR2,
    D71 OUT VARCHAR2, D72 OUT VARCHAR2, D73 OUT VARCHAR2, D74 OUT VARCHAR2, D75 OUT VARCHAR2,
    P1 OUT VARCHAR2,
    P2 OUT VARCHAR2,
    P3 OUT VARCHAR2,
    P4 OUT VARCHAR2,
    P5 OUT VARCHAR2,
    P6 OUT VARCHAR2,
    P7 OUT VARCHAR2,
    STATUS IN OUT INTEGER -- 20011800
    )
IS
  AV1 VARCHAR2(1); AV2 VARCHAR2(1); AV3 VARCHAR2(1); AV4 VARCHAR2(1); 
  AV5 VARCHAR2(1); AV6 VARCHAR2(1); AV7 VARCHAR2(1); AV8 VARCHAR2(1); 
  AV9 VARCHAR2(1); AV10 VARCHAR2(1);
  AV11 VARCHAR2(2); -- Shift
  ENDDATE DATE;
  CURRENTDATE DATE;
  AVAILABILITYDATE DATE;
  DAYDIFF INTEGER;
  INSERTRECORD INTEGER;
  SHNO INTEGER;
  SHS_NO INTEGER;
  SH_COUNT INTEGER;
  PLNT VARCHAR2(6);
  SPLANT VARCHAR2(6);
BEGIN
  STATUS := 0;
--  L1 := 0; L2 := 0; L3 := 0; L4 := 0;
-- Output params set to prevent compiler messages
  D11 := ''; D12 := ''; D13 := ''; D14 := ''; D15 := '';
  D21 := ''; D22 := ''; D23 := ''; D24 := ''; D25 := '';
  D31 := ''; D32 := ''; D33 := ''; D34 := ''; D35 := '';
  D41 := ''; D42 := ''; D43 := ''; D44 := ''; D45 := '';
  D51 := ''; D52 := ''; D53 := ''; D54 := ''; D55 := '';
  D61 := ''; D62 := ''; D63 := ''; D64 := ''; D65 := '';
  D71 := ''; D72 := ''; D73 := ''; D74 := ''; D75 := '';
  P1 := ''; P2 := ''; P3 := ''; P4 := ''; P5 := ''; P6 := ''; P7 := '';
-- Output is not used real procedure starts here
  ENDDATE := STARTDATE + 6;
  SELECT COUNT(SHIFT_NUMBER)
  INTO SH_COUNT
  FROM SHIFT
  WHERE ((PLANT_CODE = STAFFAVAIL_QUERY.PLANT_CODE) OR (STAFFAVAIL_QUERY.PLANT_CODE = '*'));
  IF (SH_COUNT = 1) THEN
    BEGIN
      SELECT SHIFT_NUMBER
      INTO SH_COUNT
      FROM SHIFT
        WHERE ((PLANT_CODE = STAFFAVAIL_QUERY.PLANT_CODE) OR (STAFFAVAIL_QUERY.PLANT_CODE = '*'));
    END;
  ELSE
    BEGIN
      SH_COUNT := -1;
    END;
  END IF;
/*
  MRA:5-FEB-2010. RV053.3.
  - Set this value always to -1 to prevent creation of shift-schedule records.
*/
/*
  MRA:10-FEB-2010. RV053.3. This change is cancelled.
*/
/*  SH_COUNT := -1; */
  RECNO := 0;
  FOR X IN
  (
  SELECT
    E.PLANT_CODE PLNT, E.EMPLOYEE_NUMBER EMPLOYEE_NUMBER,
    E.DESCRIPTION DESCRIPTION, E.DEPARTMENT_CODE DEPARTMENT_CODE,
    E.STARTDATE EMPSTARTDATE, E.ENDDATE EMPENDDATE
--  INTO
--    PLNT, EMPLOYEE_NUMBER, DESCRIPTION, DEPARTMENT_CODE, EMPSTARTDATE, EMPENDDATE
  FROM
    EMPLOYEE E
  WHERE
    ((STAFFAVAIL_QUERY.EMP_PLANT_CODE = '*') OR (E.PLANT_CODE = STAFFAVAIL_QUERY.EMP_PLANT_CODE)) AND
    (
      (
         (STAFFAVAIL_QUERY.USER_NAME <> '*') AND
         E.TEAM_CODE IN
           (
             SELECT T.TEAM_CODE
             FROM TEAMPERUSER T
             WHERE
               T.USER_NAME = STAFFAVAIL_QUERY.USER_NAME AND
               (
                 (STAFFAVAIL_QUERY.TEAMFROM = '*') OR
                 (T.TEAM_CODE >= STAFFAVAIL_QUERY.TEAMFROM AND
                  T.TEAM_CODE <= STAFFAVAIL_QUERY.TEAMTO)
               )
           )
      )
      OR
      (
          (STAFFAVAIL_QUERY.USER_NAME = '*') AND
          (
            (STAFFAVAIL_QUERY.TEAMFROM = '*') OR
            (E.TEAM_CODE >= STAFFAVAIL_QUERY.TEAMFROM AND E.TEAM_CODE <= STAFFAVAIL_QUERY.TEAMTO)
          )
      )
    )
  ORDER BY E.EMPLOYEE_NUMBER
  )
  LOOP --DO
    BEGIN
      EMPPLANT_CODE := X.PLNT;
      IF (SH_COUNT >= 0) THEN
        BEGIN
          CURRENTDATE := STARTDATE;
          WHILE (CURRENTDATE <= ENDDATE)
          LOOP --DO
            BEGIN
              SELECT COUNT(SHIFT_NUMBER)
              INTO SHS_NO
              FROM SHIFTSCHEDULE
              WHERE
                (EMPLOYEE_NUMBER = X.EMPLOYEE_NUMBER) AND (SHIFT_SCHEDULE_DATE=CURRENTDATE);

              IF (SHS_NO = 0) THEN
                BEGIN
                  -- TD-22429
                  IF (X.EMPSTARTDATE <= CURRENTDATE) AND
                    ((X.EMPENDDATE >= CURRENTDATE) OR (X.EMPENDDATE IS NULL))
                  THEN
                    INSERT INTO SHIFTSCHEDULE(
                      PLANT_CODE, EMPLOYEE_NUMBER, SHIFT_NUMBER,
                      SHIFT_SCHEDULE_DATE, CREATIONDATE, MUTATIONDATE,MUTATOR)
                    VALUES(
                      X.PLNT, X.EMPLOYEE_NUMBER, SH_COUNT,
                      CURRENTDATE, CREATIONDATE, CREATIONDATE, MUTATOR);
                  END IF;
                END;
              END IF; --(SHS_NO = 0)
              CURRENTDATE := CURRENTDATE + 1;
            END;
          END LOOP;
        END;
      END IF; --(SH_COUNT >= 0)
      CURRENTDATE := STARTDATE;
      FOR Y IN
      (
      SELECT
        EMPLOYEEAVAILABILITY_DATE AVAILABILITYDATE,
        AVAILABLE_TIMEBLOCK_1 AV1, AVAILABLE_TIMEBLOCK_2 AV2,
        AVAILABLE_TIMEBLOCK_3 AV3, AVAILABLE_TIMEBLOCK_4 AV4,
        AVAILABLE_TIMEBLOCK_5 AV5, AVAILABLE_TIMEBLOCK_6 AV6,
        AVAILABLE_TIMEBLOCK_7 AV7, AVAILABLE_TIMEBLOCK_8 AV8,
        AVAILABLE_TIMEBLOCK_9 AV9, AVAILABLE_TIMEBLOCK_10 AV10,
        PLANT_CODE PLNT, SHIFT_NUMBER AV11
      FROM EMPLOYEEAVAILABILITY
      WHERE
        ((PLANT_CODE = STAFFAVAIL_QUERY.PLANT_CODE) OR (STAFFAVAIL_QUERY.PLANT_CODE = '*')) AND
        (EMPLOYEE_NUMBER = X.EMPLOYEE_NUMBER) AND
        (EMPLOYEEAVAILABILITY_DATE >= STARTDATE) AND
        (EMPLOYEEAVAILABILITY_DATE <= ENDDATE)
      ORDER BY EMPLOYEEAVAILABILITY_DATE
      )
      LOOP --DO
        BEGIN
--          L1 := L1 + 1;
--          dbms_output.put_line('0 '||currentdate);
          DAYDIFF := Y.AVAILABILITYDATE - STARTDATE;
          /* IF FILL AUTOMATICALLY STAFF AVAILABILITY*/
          IF (INSERT_STAV = 'Y')
          THEN
            BEGIN
--              L2 := L2 + 1;
              WHILE (CURRENTDATE < Y.AVAILABILITYDATE)
              LOOP --DO
                BEGIN
--                  L3 := L3 + 1;
--                  dbms_output.put_line('1 '||currentdate);
                  /*EXECUTE PROCEDURE*/
                  -- TD-22429
                  IF (X.EMPSTARTDATE <= CURRENTDATE) AND
                    ((X.EMPENDDATE >= CURRENTDATE) OR (X.EMPENDDATE IS NULL))
                  THEN
                    -- This procedure fills it, but not with these AV-args, they are just dummy-vars.
                    STAFFAVAIL_FILLSTAV(X.EMPLOYEE_NUMBER,
                      X.DEPARTMENT_CODE, CURRENTDATE,
                      STARTDATE, PLANT_CODE, CREATIONDATE, MUTATOR,
                      /*RETURNING_VALUES*/
                      INSERTRECORD, AV1, AV2, AV3, AV4, AV5, AV6, AV7, AV8, AV9, AV10, AV11, SPLANT,
                      STATUS);
                  END IF;
                  CURRENTDATE := CURRENTDATE + 1;
                END;
              END LOOP; -- while
              CURRENTDATE := Y.AVAILABILITYDATE + 1;
--              dbms_output.put_line('0b '||currentdate);
            END;
          END IF; --(INSERT_STAV = 'Y')
--          dbms_output.put_line('0c '||currentdate);
        END;
      END LOOP; --Y
      /*IF FILL AUTOMATICALLY STAFF AVAILABILITY*/
      IF (INSERT_STAV = 'Y') THEN
        BEGIN
--          dbms_output.put_line('2 '||currentdate);
          WHILE (CURRENTDATE <= ENDDATE)
          LOOP --DO
            BEGIN
--              dbms_output.put_line('2a '||currentdate);
              /*THE SAME PROCEDURE AS ABOVE */
              /*EXECUTE PROCEDURE*/
              -- TD-22429
              IF (X.EMPSTARTDATE <= CURRENTDATE) AND
                ((X.EMPENDDATE >= CURRENTDATE) OR (X.EMPENDDATE IS NULL))
              THEN
                -- This procedure fills it, but not with these AV-args, they are just dummy-vars.
                STAFFAVAIL_FILLSTAV(X.EMPLOYEE_NUMBER,
                  X.DEPARTMENT_CODE, CURRENTDATE,
                  STARTDATE, PLANT_CODE, CREATIONDATE, MUTATOR,
                  /*RETURNING_VALUES*/
                  INSERTRECORD, AV1, AV2, AV3, AV4, AV5, AV6, AV7, AV8, AV9, AV10, AV11, SPLANT,
                  STATUS);
              END IF;
              CURRENTDATE := CURRENTDATE + 1;
--              L4 := L4 + 1;
            END;
          END LOOP;
        END;
      END IF; --(INSERT_STAV = 'Y')
      RECNO := RECNO + 1;
      --SUSPEND;
    END;
  END LOOP; --X
  COMMIT;
END STAFFAVAIL_QUERY;
/

CREATE OR REPLACE PROCEDURE STAFFPLANNING_UPDATEOCPL (
    PLANT VARCHAR2,
    DATESELECTION DATE,
    DAYSELECTION INTEGER,
    SHIFT INTEGER,
    FDATE DATE,
    MUT VARCHAR2)
IS
-- OCC_A_TB1 INTEGER;
-- OCC_A_TB2 INTEGER;
-- OCC_A_TB3 INTEGER;
-- OCC_A_TB4 INTEGER;
-- OCC_B_TB1 INTEGER;
-- OCC_B_TB2 INTEGER;
-- OCC_B_TB3 INTEGER;
-- OCC_B_TB4 INTEGER;
-- OCC_C_TB1 INTEGER;
-- OCC_C_TB2 INTEGER;
-- OCC_C_TB3 INTEGER;
-- OCC_C_TB4 INTEGER;
-- DEPT VARCHAR(6);
-- WK VARCHAR(6);
BEGIN
  DELETE FROM OCCUPATIONPLANNING
  WHERE
    PLANT_CODE = STAFFPLANNING_UPDATEOCPL.PLANT
    AND SHIFT_NUMBER = STAFFPLANNING_UPDATEOCPL.SHIFT
    AND OCC_PLANNING_DATE = STAFFPLANNING_UPDATEOCPL.DATESELECTION;
  INSERT INTO OCCUPATIONPLANNING (
    PLANT_CODE, DEPARTMENT_CODE, WORKSPOT_CODE, SHIFT_NUMBER, OCC_PLANNING_DATE,
    OCC_A_TIMEBLOCK_1, OCC_A_TIMEBLOCK_2, OCC_A_TIMEBLOCK_3, OCC_A_TIMEBLOCK_4, OCC_A_TIMEBLOCK_5, 
    OCC_A_TIMEBLOCK_6, OCC_A_TIMEBLOCK_7, OCC_A_TIMEBLOCK_8, OCC_A_TIMEBLOCK_9, OCC_A_TIMEBLOCK_10,
    OCC_B_TIMEBLOCK_1, OCC_B_TIMEBLOCK_2, OCC_B_TIMEBLOCK_3, OCC_B_TIMEBLOCK_4, OCC_B_TIMEBLOCK_5, 
    OCC_B_TIMEBLOCK_6, OCC_B_TIMEBLOCK_7, OCC_B_TIMEBLOCK_8, OCC_B_TIMEBLOCK_9, OCC_B_TIMEBLOCK_10,
    OCC_C_TIMEBLOCK_1, OCC_C_TIMEBLOCK_2, OCC_C_TIMEBLOCK_3, OCC_C_TIMEBLOCK_4, OCC_C_TIMEBLOCK_5, 
    OCC_C_TIMEBLOCK_6, OCC_C_TIMEBLOCK_7, OCC_C_TIMEBLOCK_8, OCC_C_TIMEBLOCK_9, OCC_C_TIMEBLOCK_10,
    CREATIONDATE, MUTATIONDATE, MUTATOR)
  SELECT
    PLANT_CODE, DEPARTMENT_CODE, WORKSPOT_CODE, SHIFT_NUMBER, STAFFPLANNING_UPDATEOCPL.DATESELECTION,
    OCC_A_TIMEBLOCK_1, OCC_A_TIMEBLOCK_2, OCC_A_TIMEBLOCK_3, OCC_A_TIMEBLOCK_4, OCC_A_TIMEBLOCK_5, 
    OCC_A_TIMEBLOCK_6, OCC_A_TIMEBLOCK_7, OCC_A_TIMEBLOCK_8, OCC_A_TIMEBLOCK_9, OCC_A_TIMEBLOCK_10,
    OCC_B_TIMEBLOCK_1, OCC_B_TIMEBLOCK_2, OCC_B_TIMEBLOCK_3, OCC_B_TIMEBLOCK_4, OCC_B_TIMEBLOCK_5, 
    OCC_B_TIMEBLOCK_6, OCC_B_TIMEBLOCK_7, OCC_B_TIMEBLOCK_8, OCC_B_TIMEBLOCK_9, OCC_B_TIMEBLOCK_10,
    OCC_C_TIMEBLOCK_1, OCC_C_TIMEBLOCK_2, OCC_C_TIMEBLOCK_3, OCC_C_TIMEBLOCK_4, OCC_C_TIMEBLOCK_5, 
    OCC_C_TIMEBLOCK_6, OCC_C_TIMEBLOCK_7, OCC_C_TIMEBLOCK_8, OCC_C_TIMEBLOCK_9, OCC_C_TIMEBLOCK_10,
    STAFFPLANNING_UPDATEOCPL.FDATE, STAFFPLANNING_UPDATEOCPL.FDATE, STAFFPLANNING_UPDATEOCPL.MUT
  FROM STANDARDOCCUPATION
  WHERE
    PLANT_CODE = STAFFPLANNING_UPDATEOCPL.PLANT
    AND SHIFT_NUMBER = STAFFPLANNING_UPDATEOCPL.SHIFT
    AND DAY_OF_WEEK = STAFFPLANNING_UPDATEOCPL.DAYSELECTION;
  COMMIT;
END STAFFPLANNING_UPDATEOCPL;
/

CREATE OR REPLACE PROCEDURE BANKHOL_UPDATEEMA (
    DATE_BANK IN DATE,
    COUNTRYID IN INTEGER,
    CURRENTUSER IN VARCHAR2,
    USERNAME IN VARCHAR2,
    NEW_ABSENCE IN VARCHAR2,
    OLD_ABSENCE IN VARCHAR2,
    STATUS IN OUT integer -- 20011800
	)
IS
  lvDEPARTMENT_CODE VARCHAR2(6);
  TB INTEGER;
  lvUseFinalRunYN VARCHAR2(1);
  lvFinalRunExportDate DATE;
  lvGoOn BOOLEAN;
BEGIN
  STATUS := 0; -- 20011800
  FOR
  X IN
  (
  SELECT EA.EMPLOYEE_NUMBER ENUMBER, EA.PLANT_CODE PCODE, EA.SHIFT_NUMBER SNUMBER
  FROM EMPLOYEEAVAILABILITY EA INNER JOIN EMPLOYEE E ON
    EA.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER
  WHERE EA.EMPLOYEEAVAILABILITY_DATE = DATE_BANK
    AND EA.PLANT_CODE IN (SELECT PLANT_CODE FROM PLANT WHERE COUNTRY_ID = COUNTRYID)
    AND
       (
        (USERNAME = '*') OR
        (E.TEAM_CODE IN
          (SELECT U.TEAM_CODE FROM TEAMPERUSER U WHERE U.USER_NAME = USERNAME))
       )
  )
  LOOP
    -- 20011800 - Final Run System Check - BEGIN
    lvGoOn := TRUE;
    lvUseFinalRunYN := 'N';
    lvFinalRunExportDate := null;
    SELECT FINAL_RUN_YN
    INTO lvUseFinalRunYN
    FROM PIMSSETTING;
    IF lvUseFinalRunYN = 'Y' THEN
      BEGIN
        SELECT T.EXPORT_DATE
        INTO lvFinalRunExportDate
        FROM FINALRUN T
        WHERE T.PLANT_CODE = X.PCODE AND
        T.EXPORT_TYPE =
        (SELECT NVL(C.EXPORT_TYPE, NVL((SELECT MIN(EP.EXPORT_TYPE) FROM EXPORTPAYROLL EP),-1)) EXPORT_TYPE
        FROM PLANT P INNER JOIN COUNTRY C ON
        P.COUNTRY_ID = C.COUNTRY_ID
        WHERE P.PLANT_CODE = X.PCODE);
      EXCEPTION
        WHEN NO_DATA_FOUND
          THEN lvFinalRunExportDate := null;
      END; -- Exception
      IF lvFinalRunExportDate is not null THEN
        lvGoOn := DATE_BANK > lvFinalRunExportDate;
      END IF; -- (lvFinalRunExportDate)
    END IF; -- (lvUseFinalRunYN)
    -- 20011800 - Final Run System Check - END

    IF lvGoOn THEN
    BEGIN
      TB := 0;
      SELECT COUNT(*)
      INTO TB
      FROM TIMEBLOCKPEREMPLOYEE
      WHERE
        EMPLOYEE_NUMBER = X.ENUMBER
        AND PLANT_CODE = X.PCODE
        AND SHIFT_NUMBER = X.SNUMBER;
      IF (TB = 0)
      THEN
        BEGIN
          SELECT E.DEPARTMENT_CODE
          INTO lvDEPARTMENT_CODE
          FROM EMPLOYEE E
          WHERE
            E.EMPLOYEE_NUMBER = X.ENUMBER ;
          SELECT COUNT(*)
          INTO TB
          FROM TIMEBLOCKPERDEPARTMENT TBD
          WHERE
            TBD.DEPARTMENT_CODE = lvDEPARTMENT_CODE
            AND TBD.PLANT_CODE = X.PCODE
            AND TBD.SHIFT_NUMBER = X.SNUMBER;
        END;
      END IF; -- (TB = 0)
      IF (TB = 0)
      THEN
        BEGIN
          SELECT COUNT(*)
          INTO TB
          FROM TIMEBLOCKPERSHIFT TBS
          WHERE
            TBS.PLANT_CODE = X.PCODE
            AND TBS.SHIFT_NUMBER = X.SNUMBER;
        END;
      END IF; -- (TB = 0)
      IF (TB >= 1)
      THEN
        BEGIN
          UPDATE EMPLOYEEAVAILABILITY EA
          SET EA.AVAILABLE_TIMEBLOCK_1 = NEW_ABSENCE,
          EA.MUTATIONDATE = SYSDATE,
          EA.MUTATOR = CURRENTUSER
          WHERE
            (EA.EMPLOYEEAVAILABILITY_DATE = DATE_BANK)
            AND (EA.EMPLOYEE_NUMBER = X.ENUMBER)
            AND (EA.SHIFT_NUMBER = X.SNUMBER)
            AND (EA.PLANT_CODE = X.PCODE)
            AND (EA.AVAILABLE_TIMEBLOCK_1 = OLD_ABSENCE);
        END;
      END IF; -- (TB >= 1)
      IF (TB >= 2)
      THEN
        BEGIN
          UPDATE EMPLOYEEAVAILABILITY EA
          SET EA.AVAILABLE_TIMEBLOCK_2 = NEW_ABSENCE,
          EA.MUTATIONDATE = SYSDATE,
          EA.MUTATOR = CURRENTUSER
          WHERE
            (EA.EMPLOYEEAVAILABILITY_DATE = DATE_BANK)
            AND (EA.EMPLOYEE_NUMBER = X.ENUMBER)
            AND (EA.SHIFT_NUMBER = X.SNUMBER)
            AND (EA.PLANT_CODE = X.PCODE)
            AND (EA.AVAILABLE_TIMEBLOCK_2 = OLD_ABSENCE);
        END;
      END IF; -- (TB >= 2)
      IF (TB >= 3)
        THEN
        BEGIN
          UPDATE EMPLOYEEAVAILABILITY EA
          SET EA.AVAILABLE_TIMEBLOCK_3 = NEW_ABSENCE,
          EA.MUTATIONDATE = SYSDATE,
          EA.MUTATOR = CURRENTUSER
          WHERE
            (EA.EMPLOYEEAVAILABILITY_DATE = DATE_BANK)
            AND (EA.EMPLOYEE_NUMBER = X.ENUMBER)
            AND (EA.SHIFT_NUMBER = X.SNUMBER)
            AND (EA.PLANT_CODE = X.PCODE)
            AND (EA.AVAILABLE_TIMEBLOCK_3 = OLD_ABSENCE);
        END;
      END IF; -- (TB >= 3)
      IF (TB >= 4)
        THEN
        BEGIN
          UPDATE EMPLOYEEAVAILABILITY EA
          SET EA.AVAILABLE_TIMEBLOCK_4 = NEW_ABSENCE,
          EA.MUTATIONDATE = SYSDATE,
          EA.MUTATOR = CURRENTUSER
          WHERE
            (EA.EMPLOYEEAVAILABILITY_DATE = DATE_BANK)
            AND (EA.EMPLOYEE_NUMBER = X.ENUMBER)
            AND (EA.SHIFT_NUMBER = X.SNUMBER)
            AND (EA.PLANT_CODE = X.PCODE)
            AND (EA.AVAILABLE_TIMEBLOCK_4 = OLD_ABSENCE);
        END;
      END IF; -- (TB >= 4)
      IF (TB >= 5)
        THEN
        BEGIN
          UPDATE EMPLOYEEAVAILABILITY EA
          SET EA.AVAILABLE_TIMEBLOCK_5 = NEW_ABSENCE,
          EA.MUTATIONDATE = SYSDATE,
          EA.MUTATOR = CURRENTUSER
          WHERE
            (EA.EMPLOYEEAVAILABILITY_DATE = DATE_BANK)
            AND (EA.EMPLOYEE_NUMBER = X.ENUMBER)
            AND (EA.SHIFT_NUMBER = X.SNUMBER)
            AND (EA.PLANT_CODE = X.PCODE)
            AND (EA.AVAILABLE_TIMEBLOCK_5 = OLD_ABSENCE);
        END;
      END IF; -- (TB >= 5)
      IF (TB >= 6)
        THEN
        BEGIN
          UPDATE EMPLOYEEAVAILABILITY EA
          SET EA.AVAILABLE_TIMEBLOCK_6 = NEW_ABSENCE,
          EA.MUTATIONDATE = SYSDATE,
          EA.MUTATOR = CURRENTUSER
          WHERE
            (EA.EMPLOYEEAVAILABILITY_DATE = DATE_BANK)
            AND (EA.EMPLOYEE_NUMBER = X.ENUMBER)
            AND (EA.SHIFT_NUMBER = X.SNUMBER)
            AND (EA.PLANT_CODE = X.PCODE)
            AND (EA.AVAILABLE_TIMEBLOCK_6 = OLD_ABSENCE);
        END;
      END IF; -- (TB >= 6)
      IF (TB >= 7)
        THEN
        BEGIN
          UPDATE EMPLOYEEAVAILABILITY EA
          SET EA.AVAILABLE_TIMEBLOCK_7 = NEW_ABSENCE,
          EA.MUTATIONDATE = SYSDATE,
          EA.MUTATOR = CURRENTUSER
          WHERE
            (EA.EMPLOYEEAVAILABILITY_DATE = DATE_BANK)
            AND (EA.EMPLOYEE_NUMBER = X.ENUMBER)
            AND (EA.SHIFT_NUMBER = X.SNUMBER)
            AND (EA.PLANT_CODE = X.PCODE)
            AND (EA.AVAILABLE_TIMEBLOCK_7 = OLD_ABSENCE);
        END;
      END IF; -- (TB >= 7)
      IF (TB >= 8)
        THEN
        BEGIN
          UPDATE EMPLOYEEAVAILABILITY EA
          SET EA.AVAILABLE_TIMEBLOCK_8 = NEW_ABSENCE,
          EA.MUTATIONDATE = SYSDATE,
          EA.MUTATOR = CURRENTUSER
          WHERE
            (EA.EMPLOYEEAVAILABILITY_DATE = DATE_BANK)
            AND (EA.EMPLOYEE_NUMBER = X.ENUMBER)
            AND (EA.SHIFT_NUMBER = X.SNUMBER)
            AND (EA.PLANT_CODE = X.PCODE)
            AND (EA.AVAILABLE_TIMEBLOCK_8 = OLD_ABSENCE);
        END;
      END IF; -- (TB >= 8)
      IF (TB >= 9)
        THEN
        BEGIN
          UPDATE EMPLOYEEAVAILABILITY EA
          SET EA.AVAILABLE_TIMEBLOCK_9 = NEW_ABSENCE,
          EA.MUTATIONDATE = SYSDATE,
          EA.MUTATOR = CURRENTUSER
          WHERE
            (EA.EMPLOYEEAVAILABILITY_DATE = DATE_BANK)
            AND (EA.EMPLOYEE_NUMBER = X.ENUMBER)
            AND (EA.SHIFT_NUMBER = X.SNUMBER)
            AND (EA.PLANT_CODE = X.PCODE)
            AND (EA.AVAILABLE_TIMEBLOCK_9 = OLD_ABSENCE);
        END;
      END IF; -- (TB >= 9)
      IF (TB >= 10)
        THEN
        BEGIN                                                                                                                          
          UPDATE EMPLOYEEAVAILABILITY EA
          SET EA.AVAILABLE_TIMEBLOCK_10 = NEW_ABSENCE,
          EA.MUTATIONDATE = SYSDATE,
          EA.MUTATOR = CURRENTUSER
          WHERE
            (EA.EMPLOYEEAVAILABILITY_DATE = DATE_BANK)
            AND (EA.EMPLOYEE_NUMBER = X.ENUMBER)
            AND (EA.SHIFT_NUMBER = X.SNUMBER)
            AND (EA.PLANT_CODE = X.PCODE)
            AND (EA.AVAILABLE_TIMEBLOCK_10 = OLD_ABSENCE);
        END;
      END IF; -- (TB >= 10)
    END;
	ELSE
	  status := status + 1; -- 20011800
	END IF; -- (lvGoOn)
  END LOOP;
  COMMIT;
END BANKHOL_UPDATEEMA;
/

CREATE OR REPLACE PROCEDURE STAFFPLANNING_READINTODAYPLN (
    PLANTFROM VARCHAR2,
    PLANTTO VARCHAR2,
    TEAMFROM VARCHAR2,
    TEAMTO VARCHAR2,
    USER_NAME VARCHAR2,
    DATEFROM DATE,
    DATETO DATE,
    DATENOW DATE,
    DATESOURCEFROM DATE,
    DATESOURCETO DATE,
    MUTATOR VARCHAR2)
IS
--  DATEEPL TIMESTAMP;
  NEWDATEEPL TIMESTAMP;
  lvAVTO_TB1 VARCHAR2(1);
  lvAVTO_TB2 VARCHAR2(1);
  lvAVTO_TB3 VARCHAR2(1);
  lvAVTO_TB4 VARCHAR2(1);
  lvAVTO_TB5 VARCHAR2(1);
  lvAVTO_TB6 VARCHAR2(1);
  lvAVTO_TB7 VARCHAR2(1);
  lvAVTO_TB8 VARCHAR2(1);
  lvAVTO_TB9 VARCHAR2(1);
  lvAVTO_TB10 VARCHAR2(1);
  lvSCHTO_TB1 VARCHAR2(1);
  lvSCHTO_TB2 VARCHAR2(1);
  lvSCHTO_TB3 VARCHAR2(1);
  lvSCHTO_TB4 VARCHAR2(1);
  lvSCHTO_TB5 VARCHAR2(1);
  lvSCHTO_TB6 VARCHAR2(1);
  lvSCHTO_TB7 VARCHAR2(1);
  lvSCHTO_TB8 VARCHAR2(1);
  lvSCHTO_TB9 VARCHAR2(1);
  lvSCHTO_TB10 VARCHAR2(1);
  lvSCHFROM_TB1 VARCHAR2(1);
  lvSCHFROM_TB2 VARCHAR2(1);
  lvSCHFROM_TB3 VARCHAR2(1);
  lvSCHFROM_TB4 VARCHAR2(1);
  lvSCHFROM_TB5 VARCHAR2(1);
  lvSCHFROM_TB6 VARCHAR2(1);
  lvSCHFROM_TB7 VARCHAR2(1);
  lvSCHFROM_TB8 VARCHAR2(1);
  lvSCHFROM_TB9 VARCHAR2(1);
  lvSCHFROM_TB10 VARCHAR2(1);
  
  RECCOUNT INTEGER;
BEGIN
  /*EXECUTE PROCEDURE*/ 
  STAFFPLANNING_DELETEEPLNSTD(PLANTFROM, PLANTTO, TEAMFROM,
    TEAMTO, USER_NAME, DATEFROM, DATETO);

  FOR X IN 
  (
  SELECT
    P.PLANT_CODE PLANT_CODE, P.EMPLOYEEPLANNING_DATE DATEEPL, 
    P.SHIFT_NUMBER SHIFT_NUMBER, P.DEPARTMENT_CODE DEPARTMENT_CODE,
    P.WORKSPOT_CODE WORKSPOT_CODE, P.EMPLOYEE_NUMBER EMPLOYEE_NUMBER, 
    P.SCHEDULED_TIMEBLOCK_1 SCHFROM_TB1, P.SCHEDULED_TIMEBLOCK_2 SCHFROM_TB2,
    P.SCHEDULED_TIMEBLOCK_3 SCHFROM_TB3, P.SCHEDULED_TIMEBLOCK_4 SCHFROM_TB4,
    P.SCHEDULED_TIMEBLOCK_5 SCHFROM_TB5, P.SCHEDULED_TIMEBLOCK_6 SCHFROM_TB6,
    P.SCHEDULED_TIMEBLOCK_7 SCHFROM_TB7, P.SCHEDULED_TIMEBLOCK_8 SCHFROM_TB8,
    P.SCHEDULED_TIMEBLOCK_9 SCHFROM_TB9, P.SCHEDULED_TIMEBLOCK_10 SCHFROM_TB10,
    A.AVAILABLE_TIMEBLOCK_1 AVFROM_TB1, A.AVAILABLE_TIMEBLOCK_2 AVFROM_TB2,
    A.AVAILABLE_TIMEBLOCK_3 AVFROM_TB3, A.AVAILABLE_TIMEBLOCK_4 AVFROM_TB4,
    A.AVAILABLE_TIMEBLOCK_5 AVFROM_TB5, A.AVAILABLE_TIMEBLOCK_6 AVFROM_TB6,
    A.AVAILABLE_TIMEBLOCK_7 AVFROM_TB7, A.AVAILABLE_TIMEBLOCK_8 AVFROM_TB8,
    A.AVAILABLE_TIMEBLOCK_9 AVFROM_TB9, A.AVAILABLE_TIMEBLOCK_10 AVFROM_TB10
  FROM
    EMPLOYEEPLANNING P, DEPARTMENT D, WORKSPOT W, EMPLOYEE E,
    DEPARTMENTPERTEAM T, EMPLOYEEAVAILABILITY A
  WHERE 
    (P.PLANT_CODE >= STAFFPLANNING_READINTODAYPLN.PLANTFROM) AND 
    (P.PLANT_CODE <= STAFFPLANNING_READINTODAYPLN.PLANTTO) AND
    (P.EMPLOYEEPLANNING_DATE >= STAFFPLANNING_READINTODAYPLN.DATESOURCEFROM ) AND 
    (P.EMPLOYEEPLANNING_DATE <= STAFFPLANNING_READINTODAYPLN.DATESOURCETO) AND
    (D.PLANT_CODE = P.PLANT_CODE) AND (D.DEPARTMENT_CODE = P.DEPARTMENT_CODE) AND
    (W.PLANT_CODE = P.PLANT_CODE) AND (W.WORKSPOT_CODE = P.WORKSPOT_CODE) AND
    (E.EMPLOYEE_NUMBER = P.EMPLOYEE_NUMBER) AND
    (T.PLANT_CODE = P.PLANT_CODE) AND (T.DEPARTMENT_CODE = P.DEPARTMENT_CODE) AND
    ( (D.PLAN_ON_WORKSPOT_YN = 'N') OR
      (
        (D.PLAN_ON_WORKSPOT_YN = 'Y') AND
        (
          (W.DATE_INACTIVE IS NULL) OR (W.DATE_INACTIVE > DATENOW)
        )
      )
    )  AND
    ((E.TEAM_CODE >= STAFFPLANNING_READINTODAYPLN.TEAMFROM) OR (STAFFPLANNING_READINTODAYPLN.TEAMFROM = '*')) AND
    ((E.TEAM_CODE <= STAFFPLANNING_READINTODAYPLN.TEAMTO) OR (STAFFPLANNING_READINTODAYPLN.TEAMTO = '*')) AND
    ((T.TEAM_CODE >= STAFFPLANNING_READINTODAYPLN.TEAMFROM) OR (STAFFPLANNING_READINTODAYPLN.TEAMFROM = '*')) AND
    ((T.TEAM_CODE <= STAFFPLANNING_READINTODAYPLN.TEAMTO) OR (STAFFPLANNING_READINTODAYPLN.TEAMTO = '*')) AND
    (
      (STAFFPLANNING_READINTODAYPLN.USER_NAME = '*') OR
      (
       (STAFFPLANNING_READINTODAYPLN.USER_NAME <> '*') AND
       (E.TEAM_CODE IN
         (
           SELECT U.TEAM_CODE FROM TEAMPERUSER U
           WHERE E.TEAM_CODE = U.TEAM_CODE AND U.USER_NAME = STAFFPLANNING_READINTODAYPLN.USER_NAME
         )
       ) AND
       (T.TEAM_CODE IN
         (
           SELECT U.TEAM_CODE
           FROM TEAMPERUSER U
           WHERE T.TEAM_CODE = U.TEAM_CODE AND U.USER_NAME = STAFFPLANNING_READINTODAYPLN.USER_NAME
         )
       )
      )
    ) AND
    (A.PLANT_CODE = P.PLANT_CODE) AND (A.EMPLOYEE_NUMBER = P.EMPLOYEE_NUMBER) AND
    (A.SHIFT_NUMBER = P.SHIFT_NUMBER) AND
    (A.EMPLOYEEAVAILABILITY_DATE = P.EMPLOYEEPLANNING_DATE) AND
    (
      (A.AVAILABLE_TIMEBLOCK_1 = '*') OR (A.AVAILABLE_TIMEBLOCK_2 = '*') OR
      (A.AVAILABLE_TIMEBLOCK_3 = '*') OR (A.AVAILABLE_TIMEBLOCK_4 = '*') OR
      (A.AVAILABLE_TIMEBLOCK_5 = '*') OR (A.AVAILABLE_TIMEBLOCK_6 = '*') OR
      (A.AVAILABLE_TIMEBLOCK_7 = '*') OR (A.AVAILABLE_TIMEBLOCK_8 = '*') OR
      (A.AVAILABLE_TIMEBLOCK_9 = '*') OR (A.AVAILABLE_TIMEBLOCK_10 = '*')
    )
    ORDER BY 
      P.EMPLOYEEPLANNING_DATE, P.EMPLOYEE_NUMBER
  ) 
  LOOP --DO 
    BEGIN
      IF (STAFFPLANNING_READINTODAYPLN.DATESOURCEFROM > STAFFPLANNING_READINTODAYPLN.DATEFROM) THEN
        NEWDATEEPL := 
          X.DATEEPL - (STAFFPLANNING_READINTODAYPLN.DATESOURCEFROM - STAFFPLANNING_READINTODAYPLN.DATEFROM);
      ELSE
        NEWDATEEPL := 
          X.DATEEPL + (STAFFPLANNING_READINTODAYPLN.DATEFROM - STAFFPLANNING_READINTODAYPLN.DATESOURCEFROM);
      END IF;
      lvAVTO_TB1 := '-'; lvAVTO_TB2 := '-'; lvAVTO_TB3 := '-'; lvAVTO_TB4 := '-'; lvAVTO_TB5 := '-';
      lvAVTO_TB6 := '-'; lvAVTO_TB7 := '-'; lvAVTO_TB8 := '-'; lvAVTO_TB9 := '-'; lvAVTO_TB10 := '-';
      lvSCHFROM_TB1 := X.SCHFROM_TB1;
      lvSCHFROM_TB2 := X.SCHFROM_TB2;
      lvSCHFROM_TB3 := X.SCHFROM_TB3;
      lvSCHFROM_TB4 := X.SCHFROM_TB4;
      lvSCHFROM_TB5 := X.SCHFROM_TB5;
      lvSCHFROM_TB6 := X.SCHFROM_TB6;
      lvSCHFROM_TB7 := X.SCHFROM_TB7;
      lvSCHFROM_TB8 := X.SCHFROM_TB8;
      lvSCHFROM_TB8 := X.SCHFROM_TB9;
      lvSCHFROM_TB10 := X.SCHFROM_TB10;
      FOR Y IN 
      (
      SELECT 
        A.AVAILABLE_TIMEBLOCK_1 AVTO_TB1, A.AVAILABLE_TIMEBLOCK_2 AVTO_TB2,
        A.AVAILABLE_TIMEBLOCK_3 AVTO_TB3, A.AVAILABLE_TIMEBLOCK_4 AVTO_TB4,
        A.AVAILABLE_TIMEBLOCK_5 AVTO_TB5, A.AVAILABLE_TIMEBLOCK_6 AVTO_TB6,
        A.AVAILABLE_TIMEBLOCK_7 AVTO_TB7, A.AVAILABLE_TIMEBLOCK_8 AVTO_TB8,
        A.AVAILABLE_TIMEBLOCK_9 AVTO_TB9, A.AVAILABLE_TIMEBLOCK_10 AVTO_TB10
      FROM EMPLOYEEAVAILABILITY A, EMPLOYEE E 
      WHERE 
        (E.EMPLOYEE_NUMBER = X.EMPLOYEE_NUMBER  
        AND E.STARTDATE <= NEWDATEEPL  
        AND (E.ENDDATE >= NEWDATEEPL OR E.ENDDATE IS NULL)) 
        AND (A.PLANT_CODE = X.PLANT_CODE) AND (A.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER)  
        AND (A.SHIFT_NUMBER = X.SHIFT_NUMBER)  
        AND (A.EMPLOYEEAVAILABILITY_DATE = NEWDATEEPL)   
        AND 
        (
          (A.AVAILABLE_TIMEBLOCK_1 = '*') OR (A.AVAILABLE_TIMEBLOCK_2 = '*') OR
          (A.AVAILABLE_TIMEBLOCK_3 = '*') OR (A.AVAILABLE_TIMEBLOCK_4 = '*') OR
          (A.AVAILABLE_TIMEBLOCK_5 = '*') OR (A.AVAILABLE_TIMEBLOCK_6 = '*') OR
          (A.AVAILABLE_TIMEBLOCK_7 = '*') OR (A.AVAILABLE_TIMEBLOCK_8 = '*') OR
          (A.AVAILABLE_TIMEBLOCK_9 = '*') OR (A.AVAILABLE_TIMEBLOCK_10 = '*')
        )
      )
      LOOP --DO
        BEGIN
          lvAVTO_TB1 := Y.AVTO_TB1;
          lvAVTO_TB2 := Y.AVTO_TB2;
          lvAVTO_TB3 := Y.AVTO_TB3;
          lvAVTO_TB4 := Y.AVTO_TB4;
          lvAVTO_TB5 := Y.AVTO_TB5;
          lvAVTO_TB6 := Y.AVTO_TB6;
          lvAVTO_TB7 := Y.AVTO_TB7;
          lvAVTO_TB8 := Y.AVTO_TB8;
          lvAVTO_TB9 := Y.AVTO_TB9;
          lvAVTO_TB10 := Y.AVTO_TB10;
          IF (lvAVTO_TB1 <> '*')  THEN lvSCHFROM_TB1 := 'N'; END IF;
          IF (lvAVTO_TB2 <> '*')  THEN lvSCHFROM_TB2 := 'N'; END IF;
          IF (lvAVTO_TB3 <> '*')  THEN lvSCHFROM_TB3 := 'N'; END IF;
          IF (lvAVTO_TB4 <> '*')  THEN lvSCHFROM_TB4 := 'N'; END IF;
          IF (lvAVTO_TB5 <> '*')  THEN lvSCHFROM_TB5 := 'N'; END IF;
          IF (lvAVTO_TB6 <> '*')  THEN lvSCHFROM_TB6 := 'N'; END IF;
          IF (lvAVTO_TB7 <> '*')  THEN lvSCHFROM_TB7 := 'N'; END IF;
          IF (lvAVTO_TB8 <> '*')  THEN lvSCHFROM_TB8 := 'N'; END IF;
          IF (lvAVTO_TB9 <> '*')  THEN lvSCHFROM_TB9 := 'N'; END IF;
          IF (lvAVTO_TB10 <> '*')  THEN lvSCHFROM_TB10 := 'N'; END IF;
        END;
      END LOOP; --Y
      IF (lvAVTO_TB1 <> '*')  THEN lvSCHFROM_TB1 := 'N'; END IF;
      IF (lvAVTO_TB2 <> '*')  THEN lvSCHFROM_TB2 := 'N'; END IF;
      IF (lvAVTO_TB3 <> '*')  THEN lvSCHFROM_TB3 := 'N'; END IF;
      IF (lvAVTO_TB4 <> '*')  THEN lvSCHFROM_TB4 := 'N'; END IF;
      IF (lvAVTO_TB5 <> '*')  THEN lvSCHFROM_TB5 := 'N'; END IF;
      IF (lvAVTO_TB6 <> '*')  THEN lvSCHFROM_TB6 := 'N'; END IF;
      IF (lvAVTO_TB7 <> '*')  THEN lvSCHFROM_TB7 := 'N'; END IF;
      IF (lvAVTO_TB8 <> '*')  THEN lvSCHFROM_TB8 := 'N'; END IF;
      IF (lvAVTO_TB9 <> '*')  THEN lvSCHFROM_TB9 := 'N'; END IF;
      IF (lvAVTO_TB10 <> '*')  THEN lvSCHFROM_TB10 := 'N'; END IF;
      
      IF ( 
        (lvSCHFROM_TB1 <> 'N') OR (lvSCHFROM_TB2  <> 'N') OR
        (lvSCHFROM_TB3 <> 'N') OR (lvSCHFROM_TB4  <> 'N') OR
        (lvSCHFROM_TB5 <> 'N') OR (lvSCHFROM_TB6  <> 'N') OR
        (lvSCHFROM_TB7 <> 'N') OR (lvSCHFROM_TB8  <> 'N') OR
        (lvSCHFROM_TB9 <> 'N') OR (lvSCHFROM_TB10 <> 'N')
        ) THEN
        BEGIN
          lvSCHTO_TB1 := 'N'; lvSCHTO_TB2 := 'N'; lvSCHTO_TB3 := 'N'; lvSCHTO_TB4 := 'N'; lvSCHTO_TB5 := 'N';
          lvSCHTO_TB6 := 'N'; lvSCHTO_TB7 := 'N'; lvSCHTO_TB8 := 'N'; lvSCHTO_TB9 := 'N'; lvSCHTO_TB10 := 'N';
          IF (X.AVFROM_TB1 = '*') THEN lvSCHTO_TB1 := lvSCHFROM_TB1; END IF;
          IF (X.AVFROM_TB2 = '*') THEN lvSCHTO_TB2 := lvSCHFROM_TB2; END IF;
          IF (X.AVFROM_TB3 = '*') THEN lvSCHTO_TB3 := lvSCHFROM_TB3; END IF;
          IF (X.AVFROM_TB4 = '*') THEN lvSCHTO_TB4 := lvSCHFROM_TB4; END IF;
          IF (X.AVFROM_TB5 = '*') THEN lvSCHTO_TB5 := lvSCHFROM_TB5; END IF;
          IF (X.AVFROM_TB6 = '*') THEN lvSCHTO_TB6 := lvSCHFROM_TB6; END IF;
          IF (X.AVFROM_TB7 = '*') THEN lvSCHTO_TB7 := lvSCHFROM_TB7; END IF;
          IF (X.AVFROM_TB8 = '*') THEN lvSCHTO_TB8 := lvSCHFROM_TB8; END IF;
          IF (X.AVFROM_TB9 = '*') THEN lvSCHTO_TB9 := lvSCHFROM_TB9; END IF;
          IF (X.AVFROM_TB10 = '*') THEN lvSCHTO_TB10 := lvSCHFROM_TB10; END IF;
          SELECT COUNT(*)  
          INTO RECCOUNT
          FROM EMPLOYEEPLANNING
          WHERE 
            EMPLOYEE_NUMBER = X.EMPLOYEE_NUMBER 
            AND EMPLOYEEPLANNING_DATE = NEWDATEEPL 
            AND DEPARTMENT_CODE = X.DEPARTMENT_CODE 
            AND WORKSPOT_CODE = X.WORKSPOT_CODE 
            AND SHIFT_NUMBER = X.SHIFT_NUMBER 
            AND PLANT_CODE = X.PLANT_CODE;
          IF (RECCOUNT >= 1) THEN
            BEGIN 
              UPDATE EMPLOYEEPLANNING 
              SET 
                SCHEDULED_TIMEBLOCK_1 = lvSCHTO_TB1, 
                SCHEDULED_TIMEBLOCK_2 = lvSCHTO_TB2, 
                SCHEDULED_TIMEBLOCK_3 = lvSCHTO_TB3,
                SCHEDULED_TIMEBLOCK_4 = lvSCHTO_TB4, 
                SCHEDULED_TIMEBLOCK_5 = lvSCHTO_TB5, 
                SCHEDULED_TIMEBLOCK_6 = lvSCHTO_TB6, 
                SCHEDULED_TIMEBLOCK_7 = lvSCHTO_TB7,
                SCHEDULED_TIMEBLOCK_8 = lvSCHTO_TB8, 
                SCHEDULED_TIMEBLOCK_9 = lvSCHTO_TB9, 
                SCHEDULED_TIMEBLOCK_10 = lvSCHTO_TB10, 
                CREATIONDATE = DATENOW, MUTATIONDATE = DATENOW, MUTATOR = MUTATOR 
              WHERE
                EMPLOYEE_NUMBER = X.EMPLOYEE_NUMBER 
                AND EMPLOYEEPLANNING_DATE = NEWDATEEPL 
                AND DEPARTMENT_CODE = X.DEPARTMENT_CODE 
                AND WORKSPOT_CODE = X.WORKSPOT_CODE 
                AND SHIFT_NUMBER = X.SHIFT_NUMBER 
                AND PLANT_CODE = X.PLANT_CODE ;
            END;
          ELSE
            BEGIN
              INSERT INTO EMPLOYEEPLANNING (
                PLANT_CODE, EMPLOYEEPLANNING_DATE, SHIFT_NUMBER,
                DEPARTMENT_CODE, WORKSPOT_CODE, EMPLOYEE_NUMBER,
                SCHEDULED_TIMEBLOCK_1, SCHEDULED_TIMEBLOCK_2, 
                SCHEDULED_TIMEBLOCK_3, SCHEDULED_TIMEBLOCK_4,
                SCHEDULED_TIMEBLOCK_5, SCHEDULED_TIMEBLOCK_6, 
                SCHEDULED_TIMEBLOCK_7, SCHEDULED_TIMEBLOCK_8,
                SCHEDULED_TIMEBLOCK_9, SCHEDULED_TIMEBLOCK_10, 
                CREATIONDATE, MUTATIONDATE, MUTATOR) 
              VALUES(X.PLANT_CODE, NEWDATEEPL, X.SHIFT_NUMBER, 
                X.DEPARTMENT_CODE, X.WORKSPOT_CODE, X.EMPLOYEE_NUMBER, 
                lvSCHTO_TB1, lvSCHTO_TB2, 
                lvSCHTO_TB3, lvSCHTO_TB4, 
                lvSCHTO_TB5, lvSCHTO_TB6, 
                lvSCHTO_TB7, lvSCHTO_TB8, 
                lvSCHTO_TB9, lvSCHTO_TB10, 
                DATENOW, DATENOW, MUTATOR);
            END;
          END IF; --(RECCOUNT >= 1)
        END;
      END IF; --((lvSCHFROM_TB1 <> 'N') OR (lvSCHFROM_TB2  <> 'N') OR
    END;
  END LOOP;
  COMMIT;
END STAFFPLANNING_READINTODAYPLN;
/
--
-- PIVOTEMPAVAILABLE
--
DECLARE
  sql_stmt VARCHAR2(200);
  day INTEGER;
  tbindex INTEGER;
BEGIN
  BEGIN
    FOR day IN 1..7 LOOP
      sql_stmt := 'ALTER TABLE PIVOTEMPAVAILABLE DROP COLUMN DAY'||TO_CHAR(day)||TO_CHAR(5);
      EXECUTE IMMEDIATE sql_stmt;
      FOR tbindex IN 5..10 LOOP
        sql_stmt := 'ALTER TABLE PIVOTEMPAVAILABLE ADD DAY'||TO_CHAR(day)||TO_CHAR(tbindex)||' VARCHAR2(1)';
        EXECUTE IMMEDIATE sql_stmt;
      END LOOP;
      sql_stmt := 'ALTER TABLE PIVOTEMPAVAILABLE ADD DAY'||TO_CHAR(day)||TO_CHAR(11)||' VARCHAR2(2)';
      EXECUTE IMMEDIATE sql_stmt;
    END LOOP;           
    EXCEPTION WHEN OTHERS THEN NULL;
  END;
END;
/

CREATE OR REPLACE PROCEDURE EMPLAVAILABLE_QUERYPERDATE (
    PLANT_CODE VARCHAR2,
    DEPARTMENT_CODE VARCHAR2,
    EMPLOYEE_NUMBER INTEGER,
    DATE_EMP DATE,
    EMA1 OUT VARCHAR2,
    EMA2 OUT VARCHAR2,
    EMA3 OUT VARCHAR2,
    EMA4 OUT VARCHAR2,
    EMA5 OUT VARCHAR2,
    EMA6 OUT VARCHAR2,
    EMA7 OUT VARCHAR2,
    EMA8 OUT VARCHAR2,
    EMA9 OUT VARCHAR2,
    EMA10 OUT VARCHAR2,
    EMA11 OUT VARCHAR2,
    PLNT OUT VARCHAR2,
    DATEEXIST OUT INTEGER)
IS
  lvTBVALID INTEGER;
  lvRECCOUNT INTEGER;
  lvSHIFT_NUMBER INTEGER;
  MYlvDATEEMA DATE;
  lvDATEEMA DATE;
BEGIN
  EMA1 := ''; EMA2 := ''; EMA3 := ''; EMA4 := ''; EMA5 := ''; EMA6 := ''; EMA7 := ''; EMA8 := ''; EMA9 := ''; EMA10 := ''; EMA11 := ''; PLNT := '';
  DATEEXIST := 0;
  lvDATEEMA := DATE_EMP + 1;
  SELECT
    MAX(EMPLOYEEAVAILABILITY_DATE),
    MAX(AVAILABLE_TIMEBLOCK_1), MAX(AVAILABLE_TIMEBLOCK_2),
    MAX(AVAILABLE_TIMEBLOCK_3), MAX(AVAILABLE_TIMEBLOCK_4),
    MAX(AVAILABLE_TIMEBLOCK_5), MAX(AVAILABLE_TIMEBLOCK_6),
    MAX(AVAILABLE_TIMEBLOCK_7), MAX(AVAILABLE_TIMEBLOCK_8),
    MAX(AVAILABLE_TIMEBLOCK_9), MAX(AVAILABLE_TIMEBLOCK_10),
    MAX(SHIFT_NUMBER), MAX(PLANT_CODE)
  INTO MYlvDATEEMA, EMA1, EMA2, EMA3, EMA4, EMA5, EMA6, EMA7, EMA8, EMA9, EMA10, lvSHIFT_NUMBER, PLNT
  FROM EMPLOYEEAVAILABILITY
  WHERE
    ((PLANT_CODE = EMPLAVAILABLE_QUERYPERDATE.PLANT_CODE) OR (EMPLAVAILABLE_QUERYPERDATE.PLANT_CODE = '*'))
    AND (EMPLOYEE_NUMBER = EMPLAVAILABLE_QUERYPERDATE.EMPLOYEE_NUMBER)
    AND (EMPLOYEEAVAILABILITY_DATE = EMPLAVAILABLE_QUERYPERDATE.DATE_EMP);
  IF (MYlvDATEEMA IS NOT NULL) THEN BEGIN LVDATEEMA := MYlvDATEEMA; END; END IF;
  IF (lvDATEEMA <> DATE_EMP) THEN
    BEGIN
      DATEEXIST := 0;
      SELECT MAX(SHIFT_NUMBER), MAX(PLANT_CODE)
      INTO lvRECCOUNT, PLNT
      FROM SHIFTSCHEDULE
      WHERE
        EMPLOYEE_NUMBER = EMPLAVAILABLE_QUERYPERDATE.EMPLOYEE_NUMBER
        AND SHIFT_SCHEDULE_DATE = EMPLAVAILABLE_QUERYPERDATE.DATE_EMP
        AND ((PLANT_CODE = EMPLAVAILABLE_QUERYPERDATE.PLANT_CODE) OR (EMPLAVAILABLE_QUERYPERDATE.PLANT_CODE = '*'));
      IF (lvRECCOUNT = -1) THEN
        BEGIN
          EMA1 := ''; EMA2 := ''; EMA3 := ''; EMA4 := ''; EMA5 := ''; EMA6 := ''; EMA7 := ''; EMA8 := ''; EMA9 := ''; EMA10 := ''; EMA11 := '-'; PLNT := '';
          DATEEXIST := 1;
        END;
      END IF;
      IF (lvRECCOUNT > -1) THEN
        BEGIN
          EMA1 := ''; EMA2 := ''; EMA3 := ''; EMA4 := ''; EMA5 := ''; EMA6 := ''; EMA7 := ''; EMA8 := ''; EMA9 := ''; EMA10 := ''; EMA11 := TO_CHAR(lvRECCOUNT);-- CAST (RECCOUNT AS VARCHAR2(2));
          DATEEXIST := 1;
        END    ;
      END IF;
  --    SUSPEND;
    END;
  ELSE
    BEGIN
      DATEEXIST := 1;

      SELECT COUNT(SHIFT_NUMBER)
      INTO lvRECCOUNT
      FROM SHIFTSCHEDULE
      WHERE
        EMPLOYEE_NUMBER = EMPLAVAILABLE_QUERYPERDATE.EMPLOYEE_NUMBER
        AND SHIFT_SCHEDULE_DATE = lvDATEEMA
        AND PLANT_CODE = PLNT
        AND SHIFT_NUMBER = lvSHIFT_NUMBER;
      IF (lvRECCOUNT > 0) THEN
        BEGIN
          EMA11 := TO_CHAR(lvSHIFT_NUMBER);-- CAST (SHIFT_NUMBER AS VARCHAR(2));
          /*EXECUTE PROCEDURE */
          GF_TIMEBLOCKVALID(EMPLAVAILABLE_QUERYPERDATE.EMPLOYEE_NUMBER, PLNT,
            EMPLAVAILABLE_QUERYPERDATE.DEPARTMENT_CODE, lvSHIFT_NUMBER,
          /*RETURNING_VALUES */
            lvTBVALID);
          IF (lvTBVALID = 0) THEN BEGIN EMA1 := ''; EMA2 := ''; EMA3 := ''; EMA4 := ''; EMA5 := ''; EMA6 := ''; EMA7 := ''; EMA8 := ''; EMA9 := ''; EMA10 := ''; END; END IF;
          IF (lvTBVALID = 1) THEN BEGIN EMA2 := ''; EMA3 := ''; EMA4 := ''; EMA5 := ''; EMA6 := ''; EMA7 := ''; EMA8 := ''; EMA9 := ''; EMA10 := ''; END; END IF;
          IF (lvTBVALID = 2) THEN BEGIN EMA3 := ''; EMA4 := ''; EMA5 := ''; EMA6 := ''; EMA7 := ''; EMA8 := ''; EMA9 := ''; EMA10 := ''; END; END IF;
          IF (lvTBVALID = 3) THEN BEGIN EMA4 := ''; EMA5 := ''; EMA6 := ''; EMA7 := ''; EMA8 := ''; EMA9 := ''; EMA10 := ''; END; END IF;
          IF (lvTBVALID = 4) THEN BEGIN EMA5 := ''; EMA6 := ''; EMA7 := ''; EMA8 := ''; EMA9 := ''; EMA10 := ''; END; END IF;
          IF (lvTBVALID = 5) THEN BEGIN EMA6 := ''; EMA7 := ''; EMA8 := ''; EMA9 := ''; EMA10 := ''; END; END IF;
          IF (lvTBVALID = 6) THEN BEGIN EMA7 := ''; EMA8 := ''; EMA9 := ''; EMA10 := ''; END; END IF;
          IF (lvTBVALID = 7) THEN BEGIN EMA8 := ''; EMA9 := ''; EMA10 := ''; END; END IF;
          IF (lvTBVALID = 8) THEN BEGIN EMA9 := ''; EMA10 := ''; END; END IF;
          IF (lvTBVALID = 9) THEN BEGIN EMA10 := ''; END; END IF;
        END;
      ELSE
        BEGIN
          SELECT COUNT(SHIFT_NUMBER)
          INTO lvRECCOUNT
          FROM SHIFTSCHEDULE
          WHERE EMPLOYEE_NUMBER = EMPLAVAILABLE_QUERYPERDATE.EMPLOYEE_NUMBER
            AND SHIFT_SCHEDULE_DATE = lvDATEEMA
            AND PLANT_CODE = PLNT
            AND SHIFT_NUMBER = -1;
          IF (lvRECCOUNT > 0) THEN
            BEGIN
              EMA1 := ''; EMA2 := ''; EMA3 := ''; EMA4 := ''; EMA6 := ''; EMA7 := ''; EMA8 := ''; EMA9 := ''; EMA10 := ''; EMA11 := '-';
            END;
          ELSE
            BEGIN
-- SO-550517 Do not empty the values here! Or you lose the emp.avail-record that was found.
--              EMA1 := ''; EMA2 := ''; EMA3 := ''; EMA4 := '';EMA5 := '';
--              DATEEXIST := 0;
-- SO-550517 Fill with shiftnumber found from emp.avail-record.
                EMA11 := TO_CHAR(lvSHIFT_NUMBER);
            END;
          END IF;
        END;
      END IF;--(lvRECCOUNT > 0)
--      SUSPEND;
    END;
  END IF;--(DATEEMA <> DATE_EMP)
END EMPLAVAILABLE_QUERYPERDATE;
/

CREATE OR REPLACE PROCEDURE EMPLAVAILABLE_FILLPIVOTEMA (
    PIVOTCOMPUTERNAME VARCHAR2,
    PLANT_CODE VARCHAR2,
    DEPARTMENT_CODE VARCHAR2,
    EMPLOYEE_NUMBER INTEGER,
    DATEMIN DATE,
    DATEMAX DATE,
    YEAR_NR INTEGER)
IS
  WEEK_NR INTEGER;
  RECORDEXIST1 INTEGER; RECORDEXIST2 INTEGER; RECORDEXIST3 INTEGER; RECORDEXIST4 INTEGER;
  RECORDEXIST5 INTEGER; RECORDEXIST6 INTEGER; RECORDEXIST7 INTEGER;
  DATETMP DATE;
  DATEEMAMIN DATE; DATEEMAMAX DATE;
  D11 VARCHAR2(1); D12 VARCHAR2(1); D13 VARCHAR2(1); D14 VARCHAR2(1); D15 VARCHAR2(1); D16 VARCHAR2(1); D17 VARCHAR2(1); D18 VARCHAR2(1); D19 VARCHAR2(1); D110 VARCHAR2(1); D111 VARCHAR2(2);
  D21 VARCHAR2(1); D22 VARCHAR2(1); D23 VARCHAR2(1); D24 VARCHAR2(1); D25 VARCHAR2(1); D26 VARCHAR2(1); D27 VARCHAR2(1); D28 VARCHAR2(1); D29 VARCHAR2(1); D210 VARCHAR2(1); D211 VARCHAR2(2);
  D31 VARCHAR2(1); D32 VARCHAR2(1); D33 VARCHAR2(1); D34 VARCHAR2(1); D35 VARCHAR2(1); D36 VARCHAR2(1); D37 VARCHAR2(1); D38 VARCHAR2(1); D39 VARCHAR2(1); D310 VARCHAR2(1); D311 VARCHAR2(2);
  D41 VARCHAR2(1); D42 VARCHAR2(1); D43 VARCHAR2(1); D44 VARCHAR2(1); D45 VARCHAR2(1); D46 VARCHAR2(1); D47 VARCHAR2(1); D48 VARCHAR2(1); D49 VARCHAR2(1); D410 VARCHAR2(1); D411 VARCHAR2(2);
  D51 VARCHAR2(1); D52 VARCHAR2(1); D53 VARCHAR2(1); D54 VARCHAR2(1); D55 VARCHAR2(1); D56 VARCHAR2(1); D57 VARCHAR2(1); D58 VARCHAR2(1); D59 VARCHAR2(1); D510 VARCHAR2(1); D511 VARCHAR2(2);
  D61 VARCHAR2(1); D62 VARCHAR2(1); D63 VARCHAR2(1); D64 VARCHAR2(1); D65 VARCHAR2(1); D66 VARCHAR2(1); D67 VARCHAR2(1); D68 VARCHAR2(1); D69 VARCHAR2(1); D610 VARCHAR2(1); D611 VARCHAR2(2);
  D71 VARCHAR2(1); D72 VARCHAR2(1); D73 VARCHAR2(1); D74 VARCHAR2(1); D75 VARCHAR2(1); D76 VARCHAR2(1); D77 VARCHAR2(1); D78 VARCHAR2(1); D79 VARCHAR2(1); D710 VARCHAR2(1); D711 VARCHAR2(2); 
  P1 VARCHAR2(6); P2 VARCHAR2(6); P3 VARCHAR2(6); P4 VARCHAR2(6); 
  P5 VARCHAR2(6); P6 VARCHAR2(6); P7 VARCHAR2(6);
BEGIN
  /*EXECUTE PROCEDURE */
  EMPLAVAILABLE_GETDATERANGE(EMPLAVAILABLE_FILLPIVOTEMA.PLANT_CODE, EMPLAVAILABLE_FILLPIVOTEMA.EMPLOYEE_NUMBER,
    EMPLAVAILABLE_FILLPIVOTEMA.DATEMIN, EMPLAVAILABLE_FILLPIVOTEMA.DATEMAX,
  /*RETURNING_VALUES */
    DATEEMAMIN, DATEEMAMAX);
  WEEK_NR := 1;
  DATETMP := DATEMIN;
  WHILE (DATETMP <= DATEMAX) 
  LOOP --DO
    BEGIN
      RECORDEXIST1 := 0;
      D11 := ''; D12 := ''; D13 := ''; D14 := ''; D15 := ''; D16 := ''; D17 := ''; D18 := ''; D19 := ''; D110 := ''; D111 := ''; P1 := '';
      IF ((DATETMP >= DATEEMAMIN) AND (DATETMP <= DATEEMAMAX)) THEN
        /*EXECUTE PROCEDURE*/ 
        EMPLAVAILABLE_QUERYPERDATE(EMPLAVAILABLE_FILLPIVOTEMA.PLANT_CODE, 
        EMPLAVAILABLE_FILLPIVOTEMA.DEPARTMENT_CODE, EMPLAVAILABLE_FILLPIVOTEMA.EMPLOYEE_NUMBER, 
        DATETMP,
        /*RETURNING_VALUES*/
          D11, D12, D13, D14, D15, D16, D17, D18, D19, D110, D111, P1, RECORDEXIST1); 
      END IF;
      RECORDEXIST2 := 0;
      D21 := ''; D22 := ''; D23 := ''; D24 := ''; D25 := ''; D26 := ''; D27 := ''; D28 := ''; D29 := ''; D210 := ''; D211 := ''; P2 := '';
      DATETMP := DATETMP + 1;
      IF ((DATETMP >= DATEEMAMIN) AND (DATETMP <= DATEEMAMAX)) THEN
        /*EXECUTE PROCEDURE*/ 
        EMPLAVAILABLE_QUERYPERDATE(EMPLAVAILABLE_FILLPIVOTEMA.PLANT_CODE, 
        EMPLAVAILABLE_FILLPIVOTEMA.DEPARTMENT_CODE, EMPLAVAILABLE_FILLPIVOTEMA.EMPLOYEE_NUMBER, 
        DATETMP,
        /*RETURNING_VALUES*/
         D21, D22, D23, D24, D25, D26, D27, D28, D29, D210, D211, P2, RECORDEXIST2);
      END IF;
      RECORDEXIST3 := 0;
      D31 := ''; D32 := ''; D33 := ''; D34 := ''; D35 := ''; D36 := ''; D37 := ''; D38 := ''; D39 := ''; D310 := ''; D311 := ''; P3 := '';
      DATETMP := DATETMP + 1;
      IF ((DATETMP >= DATEEMAMIN) AND (DATETMP <= DATEEMAMAX)) THEN
        /*EXECUTE PROCEDURE*/ 
        EMPLAVAILABLE_QUERYPERDATE(EMPLAVAILABLE_FILLPIVOTEMA.PLANT_CODE, 
        EMPLAVAILABLE_FILLPIVOTEMA.DEPARTMENT_CODE, EMPLAVAILABLE_FILLPIVOTEMA.EMPLOYEE_NUMBER, 
        DATETMP,
        /*RETURNING_VALUES*/
          D31, D32, D33, D34, D35, D36, D37, D38, D39, D310, D311, P3, RECORDEXIST3);
      END IF;
      RECORDEXIST4 := 0;
      D41 := ''; D42 := ''; D43 := ''; D44 := ''; D45 := ''; D46 := ''; D47 := ''; D48 := ''; D49 := ''; D410 := ''; D411 := ''; P4 := '';
      DATETMP := DATETMP + 1;
      IF ((DATETMP >= DATEEMAMIN) AND (DATETMP <= DATEEMAMAX)) THEN
        /*EXECUTE PROCEDURE*/ 
        EMPLAVAILABLE_QUERYPERDATE(EMPLAVAILABLE_FILLPIVOTEMA.PLANT_CODE, 
        EMPLAVAILABLE_FILLPIVOTEMA.DEPARTMENT_CODE, EMPLAVAILABLE_FILLPIVOTEMA.EMPLOYEE_NUMBER, 
        DATETMP,
        /*RETURNING_VALUES*/
         D41, D42, D43, D44, D45, D46, D47, D48, D49, D410, D411, P4, RECORDEXIST4);
      END IF;
      RECORDEXIST5 := 0;
      D51 := ''; D52 := ''; D53 := ''; D54 := ''; D55 := ''; D56 := ''; D57 := ''; D58 := ''; D59 := ''; D510 := ''; D511 := ''; P5 := '';
      DATETMP := DATETMP + 1;
      IF ((DATETMP >= DATEEMAMIN) AND (DATETMP <= DATEEMAMAX)) THEN
        /*EXECUTE PROCEDURE*/ 
        EMPLAVAILABLE_QUERYPERDATE(EMPLAVAILABLE_FILLPIVOTEMA.PLANT_CODE, 
        EMPLAVAILABLE_FILLPIVOTEMA.DEPARTMENT_CODE, EMPLAVAILABLE_FILLPIVOTEMA.EMPLOYEE_NUMBER, 
        DATETMP,
        /*RETURNING_VALUES*/
          D51, D52, D53, D54, D55, D56, D57, D58, D59, D510, D511, P5, RECORDEXIST5);
      END IF;
      RECORDEXIST6 := 0;
      D61 := ''; D62 := ''; D63 := ''; D64 := ''; D65 := ''; D66 := ''; D67 := ''; D68 := ''; D69 := ''; D610 := ''; D611 := ''; P6 := '';
      DATETMP := DATETMP + 1; 
      IF ((DATETMP >= DATEEMAMIN) AND (DATETMP <= DATEEMAMAX)) THEN
        /*EXECUTE PROCEDURE*/ 
        EMPLAVAILABLE_QUERYPERDATE(EMPLAVAILABLE_FILLPIVOTEMA.PLANT_CODE, 
        EMPLAVAILABLE_FILLPIVOTEMA.DEPARTMENT_CODE, EMPLAVAILABLE_FILLPIVOTEMA.EMPLOYEE_NUMBER, 
        DATETMP,
        /*RETURNING_VALUES*/
          D61, D62, D63, D64, D65, D66, D67, D68, D69, D610, D611, P6, RECORDEXIST6);
      END IF;
      RECORDEXIST7 := 0;
      D71 := ''; D72 := ''; D73 := ''; D74 := ''; D75 := ''; D76 := ''; D77 := ''; D78 := ''; D79 := ''; D710 := ''; D711 := ''; P7 := '';
      DATETMP := DATETMP + 1;
      IF ((DATETMP >= DATEEMAMIN) AND (DATETMP <= DATEEMAMAX)) THEN
        /*EXECUTE PROCEDURE*/ 
        EMPLAVAILABLE_QUERYPERDATE(EMPLAVAILABLE_FILLPIVOTEMA.PLANT_CODE, 
        EMPLAVAILABLE_FILLPIVOTEMA.DEPARTMENT_CODE, EMPLAVAILABLE_FILLPIVOTEMA.EMPLOYEE_NUMBER, 
        DATETMP,
        /*RETURNING_VALUES*/
          D71, D72, D73, D74, D75, D76, D77, D78, D79, D710, D711, P7, RECORDEXIST7); 
      END IF;
      IF ((RECORDEXIST1 = 1) OR (RECORDEXIST2 = 1) OR (RECORDEXIST3 = 1) OR
          (RECORDEXIST4 = 1) OR (RECORDEXIST5 = 1) OR (RECORDEXIST6 = 1) OR
          (RECORDEXIST7 = 1)) THEN
        INSERT INTO PIVOTEMPAVAILABLE (
          PIVOTCOMPUTERNAME, EMPLOYEE_NR, 
          YEAR_NR, WEEK_NR,
          DAY11, DAY12, DAY13, DAY14, DAY15, DAY16, DAY17, DAY18, DAY19, DAY110, DAY111,
          DAY21, DAY22, DAY23, DAY24, DAY25, DAY26, DAY27, DAY28, DAY29, DAY210, DAY211,
          DAY31, DAY32, DAY33, DAY34, DAY35, DAY36, DAY37, DAY38, DAY39, DAY310, DAY311,
          DAY41, DAY42, DAY43, DAY44, DAY45, DAY46, DAY47, DAY48, DAY49, DAY410, DAY411,
          DAY51, DAY52, DAY53, DAY54, DAY55, DAY56, DAY57, DAY58, DAY59, DAY510, DAY511,
          DAY61, DAY62, DAY63, DAY64, DAY65, DAY66, DAY67, DAY68, DAY69, DAY610, DAY611,
          DAY71, DAY72, DAY73, DAY74, DAY75, DAY76, DAY77, DAY78, DAY79, DAY710, DAY711,
          PLANT1, PLANT2, PLANT3, PLANT4, PLANT5, PLANT6, PLANT7)
        VALUES (
          EMPLAVAILABLE_FILLPIVOTEMA.PIVOTCOMPUTERNAME, EMPLAVAILABLE_FILLPIVOTEMA.EMPLOYEE_NUMBER, 
          EMPLAVAILABLE_FILLPIVOTEMA.YEAR_NR, WEEK_NR, 
          D11, D12, D13, D14, D15, D16, D17, D18, D19, D110, D111,
          D21, D22, D23, D24, D25, D26, D27, D28, D29, D210, D211,
          D31, D32, D33, D34, D35, D36, D37, D38, D39, D310, D311,
          D41, D42, D43, D44, D45, D46, D47, D48, D49, D410, D411,
          D51, D52, D53, D54, D55, D56, D57, D58, D59, D510, D511,
          D61, D62, D63, D64, D65, D66, D67, D68, D69, D610, D611,
          D71, D72, D73, D74, D75, D76, D77, D78, D79, D710, D711,
          P1, P2, P3, P4, P5, P6, P7); 
      END IF;
      WEEK_NR := WEEK_NR + 1;
      DATETMP := DATETMP + 1;
    END;
  END LOOP; --WHILE (DATETMP <= DATEMAX) 
  COMMIT;
END EMPLAVAILABLE_FILLPIVOTEMA;
/

create or replace function PlanTimeblockAsString(AV1 varchar2, AV2 varchar2, AV3 varchar2, AV4 varchar2, AV5 varchar2, AV6 varchar2, AV7 varchar2, AV8 varchar2, AV9 varchar2, AV10 varchar2) return varchar2 is
  Result varchar2(30);
  Type TAV IS VARRAY(10) of Varchar2(1);
  AVList TAV := TAV();
begin
  Result := '';
  AVList.extend(10);
  AVList(1) := AV1;
  AVList(2) := AV2;
  AVList(3) := AV3;
  AVList(4) := AV4;
  AVList(5) := AV5;
  AVList(6) := AV6;
  AVList(7) := AV7;
  AVList(8) := AV8;
  AVList(9) := AV9;
  AVList(10) := AV10;
 
  FOR i in 1..10 LOOP
    IF AVList(i) IN ('A','B','C') THEN
      IF Result IS NOT NULL THEN
        Result := Result || ',';
      END IF;
      Result := Result || TO_CHAR(i);
    END IF;
  END LOOP;
  return(Result);
end PlanTimeblockAsString;
/

-- Create table
create table STAFFPLANLEVEL
(
  ID                NUMBER(10) not null,
  NUMLEVEL          NUMBER(10) not null,
  DESCRIPTION       VARCHAR2(32) not null
)
tablespace ABS_DATA
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate primary, unique and foreign key constraints 
alter table STAFFPLANLEVEL
  add constraint XPKSTAFFPLANLEVEL primary key (ID)
  using index 
  tablespace INDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index XIE2STAFFPLANLEVEL on STAFFPLANLEVEL (NUMLEVEL)
  tablespace ABS_DATA
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 4M
    next 1M
    minextents 1
    maxextents unlimited
  );
create index XIE3STAFFPLANLEVEL on STAFFPLANLEVEL (DESCRIPTION)
  tablespace ABS_DATA
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 4M
    next 1M
    minextents 1
    maxextents unlimited
  );
/

-- Created on 16-07-2018 by MARCEL.RAND 
declare 
  -- Local variables here
  i integer;
begin
  -- Test statements here
  i := 1;
  for rec in 
  (
    SELECT *
    FROM
    (
    with testdata as
     (select 1 as col from dual
      union
      select 2 from dual
      union
      select 3 from dual
      union
      select 4 from dual
      union
      select 5 from dual
      union
      select 6 from dual
      union
      select 7 from dual
      union
      select 8 from dual
      union
      select 9 from dual
      union
      select 10 from dual
      ),
     -- create child column
     testdata2 as
      (select t.col as col1, t.col as col2 from testdata t)
     
     select level, sys_connect_by_path(col1, ',') path
       from testdata2 t
     connect by prior col1 < col2
      order by level, sys_connect_by_path(col1, ',')
     )
   ) 
   loop
--     dbms_output.put_line(rec.level);
     INSERT INTO STAFFPLANLEVEL
     (
       ID, 
       NUMLEVEL,
       DESCRIPTION
     )
     VALUES
     (
       i,
       rec.LEVEL,
       SUBSTR(rec.PATH, 2, LENGTH(rec.PATH))
     );
     COMMIT;
     i := i + 1;
   end loop;  

end;
/


UPDATE PIMSDBVERSION SET DBVERSION = '185';

commit;

