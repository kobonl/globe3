--
-- VEI001-7 Interface Cockpit <> Pims
--

create or replace view vo_operators as
select 
  e.employee_number extref, 
  e.short_name shortdescription, 
  e.description longdescription,
  case
    when e.enddate < sysdate then 1 
    else 0
  end retired,
  e.mutationdate
from employee e
/

create or replace view vi_machinegroups as
select 
  d.department_code, 
  d.plant_code, 
  d.description,
  d.businessunit_code, 
  d.mutationdate
from department d
/

create or replace view vi_machine as
select 
  m.plant_code, 
  m.machine_code, 
  m.short_name, 
  m.description, 
  m.department_code, 
  m.mutationdate
from machine m
/

create or replace view vi_workspot as
select 
  w.plant_code,
  w.workspot_code,
  w.description,
  w.short_name,
  w.department_code,
  w.machine_code,
  w.mutationdate
from workspot w
/

commit;


