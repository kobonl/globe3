--
-- PIM-135 Show down-jobs (as salary hours)
--         Also: Do not filter out the TDOWN, PDOWN and BREAK in BI-tables.
--

create or replace view v_proddetails as
select ef.shift_date,
  ef.plant_code, p.description pdescription,
  bu.businessunit_code, bu.description bdescription,
  d.department_code, d.description ddescription,
  ef.shift_number, s.description sdescription, e.team_code,
  w.workspot_code, w.description wdescription,
  ef.job_code, j.description jdescription,
  j.ignorequantitiesinreports_yn,
  j.comparation_job_yn,
  ef.employee_number, e.description edescription,
  ef.employeesecondsactual,
  ef.employeequantity,
  ef.employeesecondsnorm,
  ef.normlevel,
  nvl(bu.bonus_factor, 0) bonus_factor,
  nvl(r.revenue_amount, 0) revenue_amount,
  ec.hourly_wage,
  p.average_wage,
  j.norm_output_level,
  (select nvl(sum(p.production_minute),0) from prodhourperemplpertype p where
   ef.shift_date = p.prodhouremployee_date and
   ef.employee_number = p.employee_number and
   ef.plant_code = p.plant_code and
   ef.workspot_code = p.workspot_code and
   ef.job_code = p.job_code and
   ef.shift_number = p.shift_number and
   p.manual_yn = 'N') prodhourmin
from employeeeffpershift ef inner join jobcode j on (ef.plant_code = j.plant_code and ef.workspot_code = j.workspot_code and ef.job_code = j.job_code)
  inner join plant p on (ef.plant_code = p.plant_code)
  left join businessunit bu on (j.plant_code = bu.plant_code and j.businessunit_code = bu.businessunit_code)
  inner join workspot w on (ef.plant_code = w.plant_code and ef.workspot_code = w.workspot_code)
  inner join department d on (w.plant_code = d.plant_code and w.department_code = d.department_code)
  inner join shift s on (ef.plant_code = s.plant_code and ef.shift_number = s.shift_number)
  inner join employee e on (ef.employee_number = e.employee_number)
  left join revenue r on (j.businessunit_code = r.businessunit_code and ef.shift_date = r.revenue_date)
  left join employeecontract ec on (e.employee_number = ec.employee_number and ec.startdate <= sysdate and ec.enddate >= sysdate)
/

