--
-- PIM-12 Real Time Efficiency - Only do this for norm > 0
--

create or replace view v_realtimecurremployeeeff as
select /*+ ordered index(r XIE3EMPLOYEEEFFPERMINUTE)*/
       r.plant_code,
       r.shift_date,
       r.shift_number,
       s.description shift,
       r.employee_number,
       e.short_name,
       e.description name,
       e.department_code employee_std_department_code,
       d.description employee_std_department,
       e.team_code,
       t.description team,
       round(sum(r.employeequantity)) qty,
       sum(r.employeesecondsactual) secondsactual,
       sum(r.employeesecondsnorm) secondsnorm,
--       round(sum(r.employeesecondsactual) / 60, 1) minutesactual,
--       round(sum(r.employeesecondsnorm) / 60, 1) minutesnorm,
       case when sum(r.employeesecondsactual) = 0
         then 0
         else sum(r.employeesecondsnorm) / sum(r.employeesecondsactual) * 100
       end eff
  from v_realtimeeffperiod v
 inner join employeeeffperminute r on (r.plant_code = v.plant_code and
                                       r.shift_date = v.current_shiftdate and
                                       r.intervalstarttime >= v.intervalstarttime and
                                       r.intervalstarttime < v.intervalendtime)
 inner join shift s on (s.plant_code = r.plant_code and  s.shift_number = r.shift_number)
 inner join employee e on (e.employee_number = r.employee_number)
 inner join department d on (d.plant_code = e.plant_code and d.department_code = e.department_code)
  left outer join team t on (t.team_code = e.team_code)
 where r.normlevel > 0
 group by r.plant_code,
          r.shift_date,
          r.shift_number,
          s.description,
          r.employee_number,
          e.short_name,
          e.description,
          e.department_code,
          d.description,
          e.team_code,
          t.description
/

create or replace view v_realtimeshiftemployeeeff as
select ------  /*+ ordered index(r XIE3EMPLOYEEEFFPERMINUTE)*/
       r.plant_code,
       r.shift_date,
       r.shift_number,
       s.description shift,
       r.employee_number,
       e.short_name,
       e.description name,
       e.department_code employee_std_department_code,
       d.description employee_std_department,
       e.team_code,
       t.description team,
       round(sum(r.employeequantity)) qty,
       sum(r.employeesecondsactual) secondsactual,
       sum(r.employeesecondsnorm) secondsnorm,
       round(sum(r.employeesecondsactual) / 60, 1) minutesactual,
       round(sum(r.employeesecondsnorm) / 60, 1) minutesnorm,
       case when sum(r.employeesecondsactual) = 0
         then 0
         else sum(r.employeesecondsnorm) / sum(r.employeesecondsactual) * 100
       end eff
  from v_realtimeeffperiod v
 inner join employeeeffperminute r on (r.plant_code = v.plant_code and
                                       r.shift_date = v.current_shiftdate and
                                       r.intervalstarttime < v.intervalendtime)
 inner join shift s on (s.plant_code = r.plant_code and  s.shift_number = r.shift_number)
 inner join employee e on (e.employee_number = r.employee_number)
 inner join department d on (d.plant_code = e.plant_code and d.department_code = e.department_code)
  left outer join team t on (t.team_code = e.team_code)
 where r.normlevel > 0
 group by r.plant_code,
          r.shift_date,
          r.shift_number,
          s.description,
          r.employee_number,
          e.short_name,
          e.description,
          e.department_code,
          d.description,
          e.team_code,
          t.description
/

--
-- PIM-151 For current ghost-counts, look for last x minutes based on plant.realtimeinterval
--         to ensure it shows something in Production Screen.
--

create or replace view v_realtimecurrghostcount as
select
       r.plant_code,
       p.realtimeinterval,
       r.shift_date,
       r.shift_number,
       s.description shift,
       nvl(m.machine_code,w.workspot_code) machine_code,
       nvl(m.description,w.description) machine,
       w.workspot_code,
       w.description workspot_description,
       j.job_code,
       j.description job_code_description,
       v.currintervalstarttime intervalstarttime,
       v.lastintervalendtime intervalendtime,
       round(sum(r.workspotquantity)) qty,
       (select round(nvl(sum(t2.employeequantity),0)) from employeeeffperminute t2 where
         t2.plant_code = r.plant_code and t2.workspot_code = w.workspot_code and
         t2.job_code = j.job_code and t2.shift_date = r.shift_date and
         t2.shift_number = r.shift_number and
         t2.intervalstarttime >= v.currintervalstarttime-1/24/60*nvl(p.realtimeinterval,5) and
         t2.intervalendtime <= v.lastintervalendtime) qty2,
       sum(r.workspotsecondsactual) secondsactual,
       sum(r.workspotsecondsnorm) secondsnorm,
       sum(r.workspotsecondsactual * r.normlevel) / 3600 normqty,
       round(sum(r.workspotsecondsactual) / 60, 1) minutesactual,
       round(sum(r.workspotsecondsnorm) / 60, 1) minutesnorm,
       case when sum(r.workspotsecondsactual) = 0
         then 0
         else sum(r.workspotsecondsnorm) / sum(r.workspotsecondsactual) * 100
       end eff
 from efficiencycalcperiod v
 inner join plant p on (p.plant_code = v.plant_code)
 inner join workspoteffperminute r on (r.plant_code = v.plant_code and
                                       r.shift_date = trunc(sysdate) and
                                       r.intervalstarttime >= v.currintervalstarttime-1/24/60*nvl(p.realtimeinterval,5) and
                                       r.intervalendtime <= v.lastintervalendtime)
 left outer join shift s on (s.plant_code = r.plant_code and  s.shift_number = r.shift_number)
 inner join workspot w on (w.plant_code = r.plant_code and w.workspot_code = r.workspot_code)
 inner join jobcode j on (j.plant_code = r.plant_code and j.workspot_code = r.workspot_code and j.job_code = r.job_code)
 left outer join machine m on (m.plant_code = r.plant_code and m.machine_code = w.machine_code)
 group by r.plant_code,
          p.realtimeinterval,
          r.shift_date,
          r.shift_number,
          s.description,
          m.machine_code,
          m.description,
          w.workspot_code,
          w.description,
          j.job_code,
          j.description,
          v.currintervalstarttime,
          v.lastintervalendtime
/

commit;

