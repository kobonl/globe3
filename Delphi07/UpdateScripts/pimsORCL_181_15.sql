--
-- PIM-91 Datacollection via procedure
--

CREATE OR REPLACE PACKAGE "PACKAGE_DATACOLLECTION" AS

  TYPE TORASystemDM IS RECORD (
    CurrentProgramUser Varchar2(21),
    CurrentComputerName Varchar2(128),
    Workfile Varchar2(128),
    ADebug Integer
  );
  TYPE TPimsSetting IS RECORD (
    DatacolPath Varchar2(128),
    DatacolReadFilesInterval Integer,
    DatacolStoreQty5MinPeriod Boolean
  );

  PimsSetting TPimsSetting;
  ORASystemDM TORASystemDM;

  PROCEDURE GetPimsSettings;
  PROCEDURE WLog(APriority Integer, ALogMessage Varchar2);
  PROCEDURE WDebugLog(ALogMessage Varchar2);
  PROCEDURE WErrorLog(ALogMessage Varchar2);
  PROCEDURE Sleep(seconds NUMBER);
  PROCEDURE Parse_delimited_string(P_INPUT_STRING IN OUT VARCHAR2, P_OUTPUT_VALUE OUT VARCHAR2, P_DELIMITOR IN VARCHAR2 DEFAULT ',');
  FUNCTION RoundMinute(ADateTime Date, AMinutes Integer) return Date;
--  FUNCTION Open_File(fp_dir in varchar2, fp_filename in varchar2, fp_openmode in varchar2) return utl_file.file_type;
  FUNCTION RenameFile(ASourceDirectory Varchar2, ASourceFilename Varchar2, ATargetDirectory Varchar2, ATargetFilename Varchar2) return Boolean;
  PROCEDURE StoreWERecord(APlantCode Varchar2, AWorkspotCode Varchar2, AJobCode Varchar2, ANormLevel Integer, AShiftNumber Integer,
    AShiftDate Date, AStartDate Date, AEndDate Date, AQuantity Number);
  PROCEDURE StorePQRecord(APlantCode Varchar2, AWorkspotCode Varchar2, AJobCode Varchar2, AShiftNumber Integer,
    AShiftDate Date, AStartDate Date, AEndDate Date, AQuantity Number, ACounterValue Number);
  FUNCTION FindLastPQ(APlantCode Varchar2, AWorkspotCode Varchar2, AJobCode Varchar2, ADateTo Date,
    ALastEndDate in out Date, ALastCounterValue in out Number) return Boolean;
  PROCEDURE ProcessToProductionQuantity(AInterfaceCode Varchar2, ANewCounterValue Number, ADateTime Timestamp);
  PROCEDURE ProcessLine(ALine Varchar2);
  FUNCTION ProcessFile(AFilename Varchar2, ATimestamp Timestamp) return Boolean;
  PROCEDURE ReadProcessFiles;

END PACKAGE_DATACOLLECTION;
/

CREATE OR REPLACE PACKAGE BODY "PACKAGE_DATACOLLECTION" AS

/*
  Priority:
  1 = Message
  2 = Warning
  3 = Error
  4 = Stack trace
  5 = Debug info
*/
PROCEDURE WLog(APriority Integer, ALogMessage Varchar2)
as
begin
  begin
    insert into abslog
     (abslog_id,
      computer_name,
      abslog_timestamp,
      logsource,
      priority,
      systemuser,
      logmessage)
     values (
      0, -- determined by trigger
      ORASystemDM.CurrentComputerName, -- computer name
      sysdate,
      'PACKAGE_DATACOLLECTION', -- logsource
      APriority,
      ORASystemDM.CurrentProgramUser, -- systemuser
      Substr(ALogMessage, 1, 255)
      );
    commit;
  exception
    when others then null;
  end;
end WLog;

PROCEDURE WDebugLog(ALogMessage Varchar2)
as
begin
  if ORASystemDM.ADebug = 1 then
    WLog(5, ALogMessage);
  end if;
end WDebugLog;

PROCEDURE Sleep(seconds NUMBER) AS
BEGIN
  DBMS_LOCK.SLEEP(seconds);
END;

PROCEDURE WErrorLog(ALogMessage Varchar2)
as
begin
  WLog(3, ALogMessage);
end WErrorLog;

PROCEDURE GetPimsSettings as
  gv_datacolpath Varchar2(128);
  gv_datacolreadfileinterval Integer;
  gv_datacolstoreqty5minperiod Varchar2(1);
begin
  PimsSetting.DatacolPath := '..\\IMPORT';
  PimsSetting.DatacolReadFilesInterval := 30;
  PimsSetting.DatacolStoreQty5MinPeriod := False;
  ORASystemDM.CurrentProgramUser := 'ABS';
  ORASystemDM.CurrentComputerName := 'ABS';
  ORASystemDM.Workfile := 'WORKFILE.WRK';
  ORASystemDM.ADebug := 1;

  begin
    select
      DATACOL_PATH,
      DATACOL_READFILES_INTERVAL,
      DATACOL_STORE_QTY_5MIN_PERIOD
    into
      gv_datacolpath,
      gv_datacolreadfileinterval,
      gv_datacolstoreqty5minperiod
    from PIMSSETTING;
    if SQL%ROWCOUNT = 1 then
      PimsSetting.DatacolPath := gv_datacolpath;
      PimsSetting.DatacolReadFilesInterval := gv_datacolreadfileinterval;
      PimsSetting.DatacolStoreQty5MinPeriod := gv_datacolstoreqty5minperiod = 'Y';
    end if;
  exception
    when no_data_found then null;
    when others then null;
  end;
end GetPimsSettings;

PROCEDURE Parse_delimited_string(P_INPUT_STRING IN OUT VARCHAR2,
                                 P_OUTPUT_VALUE OUT VARCHAR2,
                                 P_DELIMITOR IN VARCHAR2 DEFAULT ',')
is
 /*This Procedure will parse out the first field of a delimited string it will return
 the result and a the orginal string minus the parsed out string.
 the ideal would be to execute this procedure for each field you want to extract
 from string. If you don't know how many values you need to parse out you can just
 keep executing this until the p_input_strng equals the P_output_value
 */
begin

  IF (instr(P_INPUT_STRING, P_DELIMITOR)) > 0 THEN
    P_OUTPUT_VALUE := substr(P_INPUT_STRING, 1, (instr(P_INPUT_STRING, P_DELIMITOR)));
    P_INPUT_STRING := regexp_replace(P_INPUT_STRING, P_OUTPUT_VALUE, '',1,1);
    P_OUTPUT_VALUE := replace(P_OUTPUT_VALUE, P_DELIMITOR, '');
    IF NVL(P_INPUT_STRING, ' ') = ' ' THEN
        P_INPUT_STRING := P_OUTPUT_VALUE;
    END IF;
  ELSE
    P_OUTPUT_VALUE := P_INPUT_STRING;
  END IF;
end Parse_delimited_string;

FUNCTION RoundMinute(ADateTime Date, AMinutes Integer) return Date is
begin
  return trunc(ADateTime, 'mi') - mod(EXTRACT(minute FROM cast(ADateTime as timestamp)), AMinutes) / (24 * 60);
end RoundMinute;

PROCEDURE StoreWERecord(APlantCode Varchar2, AWorkspotCode Varchar2, AJobCode Varchar2, ANormLevel Integer, AShiftNumber Integer,
  AShiftDate Date, AStartDate Date, AEndDate Date, AQuantity Number) as
  SecondsNorm Number;
begin
  WDebugLog('StoreWE: StartDate=' || TO_CHAR(AStartDate, 'dd-mm-yyyy hh24:mi') || ' EndDate=' || TO_CHAR(AEndDate, 'dd-mm-yyyy hh24:mi') ||
    ' Qty=' || AQuantity);
  begin
    SecondsNorm := 0;
    if ANormLevel <> 0 then
      SecondsNorm := AQuantity * 3600 / ANormLevel;
    end if;
    MERGE INTO WORKSPOTEFFPERMINUTE W
    USING DUAL
    ON (W.PLANT_CODE = APlantCode AND
      W.SHIFT_DATE = AShiftDate AND
      W.SHIFT_NUMBER = AShiftNumber AND
      W.WORKSPOT_CODE = AWorkspotCode AND
      W.JOB_CODE = AJobCode AND
      W.INTERVALSTARTTIME = AStartDate)
    WHEN NOT MATCHED THEN
    INSERT VALUES (
    SEQ_WORKSPOTEFFPERMINUTE.NEXTVAL,
    AShiftDate,
    AShiftNumber,
    APlantCode,
    AWorkspotCode,
    AJobCode,
    AStartDate,
    AEndDate,
    0, -- :WORKSPOTSECONDSACTUAL
    SecondsNorm, -- :WORKSPOTSECONDSNORM,
    AQuantity, -- :WORKSPOTQUANTITY,
    ANormLevel, --:NORMLEVEL,
    SYSDATE,
    SYSDATE,
    ORASystemDM.CurrentComputerName,
    AStartDate, -- :START_TIMESTAMP,
    AEndDate) -- :END_TIMESTAMP)
    WHEN MATCHED THEN
      UPDATE SET
        W.WORKSPOTQUANTITY = W.WORKSPOTQUANTITY + AQuantity, -- :WORKSPOTQUANTITY,
        W.MUTATIONDATE = SYSDATE,
        W.MUTATOR = ORASystemDM.CurrentComputerName;
    commit;
  exception
    when others then
      WErrorLog(SQLERRM);
  end;
end StoreWERecord;

PROCEDURE StorePQRecord(APlantCode Varchar2, AWorkspotCode Varchar2, AJobCode Varchar2, AShiftNumber Integer,
  AShiftDate Date, AStartDate Date, AEndDate Date, AQuantity Number, ACounterValue Number) as
begin
  WDebugLog('StorePQ: StartDate=' || TO_CHAR(AStartDate, 'dd-mm-yyyy hh24:mi') || ' EndDate=' || TO_CHAR(AEndDate, 'dd-mm-yyyy hh24:mi') ||
    ' Qty=' || AQuantity || ' CV=' || ACounterValue);
  begin
    MERGE INTO PRODUCTIONQUANTITY PQ
    USING DUAL
    ON (PQ.START_DATE = AStartDate AND
      PQ.PLANT_CODE = APlantCode AND
      PQ.WORKSPOT_CODE = AWorkspotCode AND
      PQ.JOB_CODE = AJobCode AND
      PQ.MANUAL_YN = 'N')
    WHEN NOT MATCHED THEN
      INSERT VALUES (
        AStartDate,
        APlantCode,
        AWorkspotCode,
        AJobCode,
        AShiftNumber,
        'N',
        AEndDate,
        AQuantity,
        ACounterValue,
        AQuantity,
        SYSDATE,
        SYSDATE,
        ORASystemDM.CurrentComputerName,
        AShiftDate)
    WHEN MATCHED THEN
      UPDATE SET
        PQ.SHIFT_NUMBER = AShiftNumber,
        PQ.SHIFT_DATE = AShiftDate,
        PQ.QUANTITY = PQ.QUANTITY + AQuantity,
        PQ.COUNTER_VALUE = ACounterValue,
        PQ.AUTOMATIC_QUANTITY = PQ.AUTOMATIC_QUANTITY + AQuantity,
        PQ.MUTATIONDATE = SYSDATE,
        PQ.MUTATOR = ORASystemDM.CurrentComputerName;
    commit;
  exception
    when others then
      WErrorLog(SQLERRM);
  end;
end StorePQRecord;

FUNCTION FindLastPQ(APlantCode Varchar2, AWorkspotCode Varchar2, AJobCode Varchar2, ADateTo Date,
  ALastEndDate in out Date, ALastCounterValue in out Number) return Boolean is
  Found Boolean;
  SearchDate Date;
begin
  Found := False;
  SearchDate := RoundMinute(ADateTo + 1/24/60 * 5, 5);
  begin
    for R_PQ in
    (
    SELECT
      Y.END_DATE, Y.COUNTER_VALUE
    FROM
      PRODUCTIONQUANTITY Y
    WHERE
      Y.PLANT_CODE = APlantCode AND
      Y.WORKSPOT_CODE = AWorkspotCode AND
      Y.JOB_CODE = AJobCode AND
      Y.MANUAL_YN = 'N' AND
      Y.END_DATE =
      (
        SELECT
          MAX(X.END_DATE)
        FROM
          PRODUCTIONQUANTITY X
        WHERE
          X.PLANT_CODE = APlantCode AND
          X.WORKSPOT_CODE = AWorkspotCode AND
          X.JOB_CODE = AJobCode AND
          X.MANUAL_YN = 'N' AND
          X.END_DATE <= SearchDate
      )
    )
    loop
      Found := True;
      ALastEndDate := R_PQ.END_DATE;
      ALastCounterValue := R_PQ.COUNTER_VALUE;
      WDebugLog('PQ Found:' || TO_CHAR(R_PQ.END_DATE, 'dd-mm-yyyy hh24:mi') || ',' || R_PQ.COUNTER_VALUE);
    end loop;
  exception
    when no_data_found then Found := False;
    when others then Found := False;
  end;
  return Found;
end FindLastPQ;

PROCEDURE ProcessToProductionQuantity(AInterfaceCode Varchar2, ANewCounterValue Number, ADateTime Timestamp) as
  Found Boolean;
  PROCEDURE ProcessJob(APlantCode Varchar2, AWorkspotCode Varchar2, AJobCode Varchar2, ANormProdLevel Integer, AShiftNumber Integer, AShiftDate Date) as
    LastEndDate Date;
    LastCounterValue Number;
    NewStartDate Date;
    NewEndDate Date;
    NewWEStartDate Date;
    NewWEEndDate Date;
    NewQuantity Number;
  begin
    if FindLastPQ(APlantCode, AWorkspotCode, AJobCode, ADateTime, LastEndDate, LastCounterValue) then
      if PimsSetting.DatacolStoreQty5MinPeriod then
        NewEndDate := RoundMinute(ADateTime + 1/24/60 * 5, 5);
        NewStartDate := NewEndDate - 1/24/60 * 5; -- minus 5 minutes
      else
        NewEndDate := RoundMinute(ADateTime + 1/24/60 * 5, 5);
        NewStartDate := RoundMinute(LastEndDate, 5);
        if NewEndDate = NewStartDate then
          NewStartDate := NewEndDate - 1/24/60 * 5; -- minus 5 minutes
        end if;
      end if;
      NewWEEndDate := RoundMinute(ADateTime + 1/24/60 * 1, 1);
      NewWEStartDate := NewWEEndDate - 1/24/60 * 1; -- minus 1 minute
      NewQuantity := ANewCounterValue - LastCounterValue;
      if NewQuantity <> 0 then
        StorePQRecord(APlantCode, AWorkspotCode, AJobCode, AShiftNumber, AShiftDate,
          NewStartDate, NewEndDate, NewQuantity, ANewCounterValue);
        StoreWERecord(APlantCode, AWorkspotCode, AJobCode, ANormProdLevel, AShiftNumber, AShiftDate,
          NewWEStartDate, NewWEEndDate, NewQuantity);
      end if;
      WDebugLog('PQ found');
    else
      NewEndDate := RoundMinute(ADateTime + 1/24/60 * 5, 5);
      NewStartDate := NewEndDate - 1/24/60 * 5; -- minus 5 minutes
      NewWEEndDate := RoundMinute(ADateTime + 1/24/60 * 1, 1);
      NewWEStartDate := NewWEEndDate - 1/24/60 * 1; -- minus 1 minute
      NewQuantity := 0;
      StorePQRecord(APlantCode, AWorkspotCode, AJobCode, AShiftNumber, AShiftDate,
        NewStartDate, NewEndDate, NewQuantity, ANewCounterValue);
      StoreWERecord(APlantCode, AWorkspotCode, AJobCode, ANormProdLevel, AShiftNumber, AShiftDate,
        NewWEStartDate, NewWEEndDate, NewQuantity);
      WDebugLog('PQ NOT Found');
    end if;
  end ProcessJob;
begin
  Found := False;
  -- Find a job based on scan(s)
  begin
    for R_TRS in
     (SELECT
      J.PLANT_CODE,
      J.WORKSPOT_CODE,
      J.JOB_CODE,
      J.NORM_PROD_LEVEL,
      P.CUTOFFTIMESHIFTDATE,
      MAX(TRS.SHIFT_DATE) AS SHIFT_DATE,
      MAX(TRS.SHIFT_NUMBER) AS SHIFT_NUMBER,
      MAX(TRS.DATETIME_IN) AS DATETIME_IN
    FROM
      JOBCODE J LEFT OUTER JOIN TIMEREGSCANNING TRS ON
        J.PLANT_CODE = TRS.PLANT_CODE AND
        J.WORKSPOT_CODE = TRS.WORKSPOT_CODE AND
        J.JOB_CODE = TRS.JOB_CODE
      INNER JOIN PLANT P ON
        J.PLANT_CODE = P.PLANT_CODE
    WHERE
      J.IGNOREINTERFACECODE_YN = 'N' AND
      TRS.DATETIME_IN >= ADateTime - 0.5 AND
      (
        (TRS.DATETIME_IN <= ADateTime AND TRS.DATETIME_OUT >= ADateTime)
      OR
        (TRS.DATETIME_IN <= ADateTime AND TRS.DATETIME_OUT IS NULL)
      )
      AND
      J.INTERFACE_CODE = AInterfaceCode
      AND NOT EXISTS (select *
        from ignoreinterfacecode i
        where i.plant_code = trs.plant_code and
          i.workspot_code = trs.workspot_code and
          i.job_code = trs.job_code and
          i.interface_code = AInterfaceCode)
    GROUP BY
      J.PLANT_CODE,
      J.WORKSPOT_CODE,
      J.JOB_CODE,
      J.NORM_PROD_LEVEL,
      P.CUTOFFTIMESHIFTDATE
    ORDER BY
      J.JOB_CODE)
    loop
      Found := True;
      WDebugLog('TRS Found=' || R_TRS.PLANT_CODE || ',' || R_TRS.WORKSPOT_CODE || ',' ||
        R_TRS.JOB_CODE || ',' ||
        R_TRS.SHIFT_NUMBER || ',' ||
        TO_CHAR(R_TRS.DATETIME_IN, 'dd-mm-yyyy hh24:mi:ss') || ',' ||
        TO_CHAR(R_TRS.CUTOFFTIMESHIFTDATE, 'dd-mm-yyyy hh24:mi')
        );
      ProcessJob(R_TRS.PLANT_CODE, R_TRS.WORKSPOT_CODE, R_TRS.JOB_CODE, R_TRS.NORM_PROD_LEVEL, R_TRS.SHIFT_NUMBER, R_TRS.SHIFT_DATE);
    end loop;
    -- Find a job (take the first one found)
    if not found then
      FOR R_TRS IN
      (
        SELECT
        J.PLANT_CODE,
        J.WORKSPOT_CODE,
        J.JOB_CODE,
        J.NORM_PROD_LEVEL,
        1 AS SHIFT_NUMBER,
        TRUNC(ADateTime) AS SHIFT_DATE,
        P.CUTOFFTIMESHIFTDATE
        FROM JOBCODE J INNER JOIN PLANT P ON
          J.PLANT_CODE = P.PLANT_CODE
        WHERE
          J.IGNOREINTERFACECODE_YN = 'N' AND
          J.INTERFACE_CODE = AInterfaceCode
        ORDER BY J.JOB_CODE
      )
      LOOP
        Found := True;
        WDebugLog('JOB found=' || R_TRS.PLANT_CODE || ',' || R_TRS.WORKSPOT_CODE || ',' ||
          R_TRS.JOB_CODE || ',' || R_TRS.NORM_PROD_LEVEL || ',' || R_TRS.SHIFT_NUMBER);
        ProcessJob(R_TRS.PLANT_CODE, R_TRS.WORKSPOT_CODE, R_TRS.JOB_CODE, R_TRS.NORM_PROD_LEVEL, R_TRS.SHIFT_NUMBER, R_TRS.SHIFT_DATE);
        EXIT;
      END LOOP;
    end if;
  exception
    when no_data_found then Found := False;
    when others then Found := False;
  end;
  if Found then
    WDebugLog('Job(s) found');
  else
    WDebugLog('No job(s) found');
  end if;
end ProcessToProductionQuantity;

PROCEDURE ProcessLine(ALine Varchar2) as
  Line Varchar2(32767);
  Field Varchar2(80);
  InterfaceCode Varchar2(25);
  CounterValue Number;
  DateTime Timestamp;
begin
  Line := ALine;
  WDebugLog(Line);
  BEGIN
    if Line IS NOT NULL then
      -- Interface code
      Parse_delimited_string(Line, Field, ';');
      if Field is not null then
        InterfaceCode := Field;
        -- CounterValue
        Parse_delimited_string(Line, Field, ';');
        if Field is not null then
          CounterValue := TO_NUMBER(Field);
          -- DateTime
          Parse_delimited_string(Line, Field, ';');
          if Field is not null then
            DateTime := TO_TIMESTAMP(Field, 'YYYYMMDDHH24MISSFF');
            WDebugLog(InterfaceCode || ',' || CounterValue || ',' || TO_CHAR(DateTime, 'dd-mm-yyyy hh24:mi:ss'));
            ProcessToProductionQuantity(InterfaceCode, CounterValue, DateTime);
          end if;
        end if;
      end if;
    end if;
  EXCEPTION
    WHEN OTHERS THEN
      WErrorLog(SQLERRM);
  END;
end ProcessLine;
/*
FUNCTION Open_File(fp_dir in varchar2, fp_filename in varchar2, fp_openmode in varchar2) return utl_file.file_type is
begin
  begin
    --open file
    WDebugLog('Open_File ' || fp_dir || fp_filename);
    return utl_file.fopen(fp_dir, fp_filename, fp_openmode);
  exception
    when utl_file.invalid_path then
      WDebugLog('Invalid Path');
    when utl_file.invalid_mode then
      WDebugLog('Invalid Mode');
    when utl_file.invalid_operation then
      WDebugLog('Invalid Operation');
    when utl_file.invalid_filehandle then
      WDebugLog('Invalid FileHandle');
    when utl_file.invalid_filename then
      WDebugLog('Invalid Filename Error');
    when utl_file.access_denied then
      WDebugLog('Access denied Error');
    when utl_file.invalid_offset then
      WDebugLog('Invalid Offset Error');
    when others then
      WDebugLog(SQLERRM);
  end;
end open_file;
*/
FUNCTION RenameFile(ASourceDirectory Varchar2, ASourceFilename Varchar2, ATargetDirectory Varchar2, ATargetFilename Varchar2) return Boolean as
  F1 UTL_FILE.FILE_TYPE;
  Result Boolean;
begin
  WDebugLog('RenameFile');
  Result := False;
  BEGIN
    -- Try to open the file
    BEGIN
      WDebugLog('OpenFile');
      Sleep(3);
      F1 := UTL_FILE.FOPEN(ASourceDirectory, ASourceFilename, 'R');
      Sleep(1);
      Result := True;
    EXCEPTION
      WHEN OTHERS THEN WDebugLog(SQLERRM);
    END;
    -- Try to close the file
    BEGIN
      WDebugLog('CloseFile');
      UTL_FILE.FCLOSE(F1);
      Sleep(1);
      Result := True;
    EXCEPTION
      WHEN OTHERS THEN WDebugLog(SQLERRM);
    END;
    -- Try to rename the file
    IF Result THEN
      WDebugLog('Renaming');
      Result := False;
      UTL_FILE.FRENAME(ASourceDirectory, ASourceFilename, ATargetDirectory, ATargetFilename, True);
      Result := True;
      Sleep(1);
      WDebugLog('Renamed');
    END IF;
  EXCEPTION
    WHEN UTL_FILE.INVALID_PATH THEN WDebugLog('Invalid Path');
    WHEN UTL_FILE.INVALID_OPERATION THEN WDebugLog('Invalid Operation');
    WHEN OTHERS THEN WDebugLog(SQLERRM);
  END;
  return Result;
end RenameFile;

FUNCTION ProcessFile(AFilename Varchar2, ATimestamp Timestamp) return Boolean as
  Line VARCHAR2(32767);
  F1 UTL_FILE.FILE_TYPE;
  Result Boolean;
  Renamed Boolean;
  FromDate Date;
  ToDate Date;
begin
  Result := False;
  WDebugLog(AFilename);
  -- Compare with current time minus x seconds
  -- When ATimestamp is older than x seconds then continue
  for I in 1..10 loop
    ToDate := sysdate;
    FromDate := ToDate - 1/24/60/60 * 15;
    WDebugLog('TS=' || TO_CHAR(ATimestamp, 'dd-mm-yyyy hh24:mi:ss') ||
      ' F=' || TO_CHAR(FromDate, 'dd-mm-yyyy hh24:mi:ss') || ' T=' || TO_CHAR(ToDate, 'dd-mm-yyyy hh24:mi:ss'));
    if ATimestamp >= FromDate and ATimestamp <= ToDate then
      WDebugLog('-> Waiting...');
      sleep(1);
    else
      Exit;
    end if;
  end loop;
  BEGIN
    Renamed := False;
    BEGIN
      Renamed := RenameFile('PIMSIMP_DIR', AFilename, 'PIMSIMP_DIR', ORASystemDM.Workfile);
    EXCEPTION
      WHEN OTHERS THEN WErrorLog(SQLERRM);
    END;
    IF Renamed THEN
      BEGIN
        -- Then process the workfile
        WDebugLog('Opening file');
        F1 := UTL_FILE.FOPEN('PIMSIMP_DIR', ORASystemDM.Workfile, 'R');
        IF UTL_FILE.IS_OPEN(F1) THEN
          WDebugLog('Reading: ' || ORASystemDM.Workfile);
          LOOP
            BEGIN
              UTL_FILE.GET_LINE(F1, Line);
              ProcessLine(Line);
            EXCEPTION
              WHEN NO_DATA_FOUND THEN EXIT;
            END;
          END LOOP;
        END IF;
        UTL_FILE.FCLOSE(F1);
        BEGIN
          -- Move the file to backup-location (overwrite when it already exists)
          UTL_FILE.FRENAME('PIMSIMP_DIR', ORASystemDM.Workfile, 'PIMSIMPBK_DIR', AFilename, True);
          Result := True;
        EXCEPTION
          WHEN OTHERS THEN WErrorLog(SQLERRM);
        END;
      EXCEPTION
        WHEN UTL_FILE.INVALID_PATH THEN WErrorLog(SQLERRM);
        WHEN UTL_FILE.INVALID_OPERATION THEN WErrorLog(SQLERRM);
        WHEN OTHERS THEN WErrorLog(SQLERRM);
      END;
    END IF;
  EXCEPTION
    WHEN OTHERS THEN Result := False;
  END;
  return Result;
end ProcessFile;

PROCEDURE ReadProcessFiles as
  MyCount Integer;
begin
  BEGIN
    GetPimsSettings;
    -- Delete 'bad' files
    FOR r_files IN (select * from pimsimpfiles_xt t where file_name like '%.bad' or file_name like '%.dsc' or file_name like '%.log')
    LOOP
      BEGIN
        UTL_FILE.FREMOVE('PIMSIMP_DIR', r_files.file_name);
      EXCEPTION
        WHEN OTHERS THEN NULL;
      END;
    END LOOP;
    -- How many files are there? When there are more than 0 we continue.
    SELECT COUNT(*)
    INTO MyCount
    FROM pimsimpfiles_vxt;
    IF MyCount <= 0 THEN
      RETURN;
    END IF;
    -- Import routine
    FOR r_files IN (SELECT * FROM pimsimpfiles_vxt ORDER BY file_time)
    LOOP
      IF NOT ProcessFile(r_files.file_name, r_files.file_time) THEN
        EXIT;
      END IF;
    END LOOP;
    -- Delete any old workfile
    BEGIN
      UTL_FILE.FREMOVE('PIMSIMP_DIR', ORASystemDM.Workfile);
    EXCEPTION
      WHEN OTHERS THEN NULL;
    END;
  EXCEPTION
    WHEN OTHERS THEN NULL;
  END;
end ReadProcessFiles;

END PACKAGE_DATACOLLECTION;
/

commit;

