--
--GLOB3-284 Week start at Wednesday
--

create or replace function PimsDayOfWeek(ADate In DATE) return integer is
  Result integer;
  DateDayNumber INTEGER;
  ORAWeekStartOn INTEGER;
  PimsWeekStartOn INTEGER;
begin
  -- What daynumber has Sunday according to Oracle ?
  SELECT TO_CHAR(TO_DATE('2-1-2011','dd-mm-yyyy'), 'D')
  INTO ORAWeekStartOn
  FROM DUAL;
  -- What is PimsWeekStartOn ?
  SELECT week_start_on
  INTO PimsWeekStartOn
  FROM pimssetting;
  -- What is daynumber of date-parameter ?
  SELECT TO_CHAR(Adate, 'D')
  INTO DateDayNumber
  FROM DUAL;

-- Set this as default
  Result := DateDayNumber;

--  dbms_output.put_line('ORAWeekStartOn=' || ORAWeekStartOn || ' PimsWeekStartOn=' || PimsWeekStartOn || ' DateDayNumber=' || DateDayNumber);

  IF (ORAWeekStartOn = 1) AND  (PimsWeekStartOn = 2) THEN -- First day: Monday
    BEGIN
      CASE DateDayNumber
      WHEN 1 THEN Result := 7; -- sun
      WHEN 2 THEN Result := 1; -- mon
      WHEN 3 THEN Result := 2; -- tue
      WHEN 4 THEN Result := 3; -- wed
      WHEN 5 THEN Result := 4; -- thu
      WHEN 6 THEN Result := 5; -- fri
      WHEN 7 THEN Result := 6; -- sat
      END CASE;
    END;
  ELSE
    IF (ORAWeekStartOn = 1) AND  (PimsWeekStartOn = 4) THEN -- First day: Wednesday
      BEGIN
        CASE DateDayNumber
        WHEN 1 THEN Result := 5; -- sun
        WHEN 2 THEN Result := 6; -- mon
        WHEN 3 THEN Result := 7; -- tue
        WHEN 4 THEN Result := 1; -- wed
        WHEN 5 THEN Result := 2; -- thu
        WHEN 6 THEN Result := 3; -- fri
        WHEN 7 THEN Result := 4; -- sat
        END CASE;
      END;
  ELSE
    IF (ORAWeekStartOn = 1) AND  (PimsWeekStartOn = 7) THEN -- First day: Saturday
      BEGIN
        CASE DateDayNumber
        WHEN 1 THEN Result := 2; -- sun
        WHEN 2 THEN Result := 3; -- mon
        WHEN 3 THEN Result := 4; -- tue
        WHEN 4 THEN Result := 5; -- wed
        WHEN 5 THEN Result := 6; -- thu
        WHEN 6 THEN Result := 7; -- fri
        WHEN 7 THEN Result := 1; -- sat
        END CASE;
      END;
    ELSE
      IF (ORAWeekStartOn = 7) AND  (PimsWeekStartOn = 1) THEN -- First day: Sunday
        BEGIN
          CASE DateDayNumber
          WHEN 7 THEN Result := 1; -- sun
          WHEN 1 THEN Result := 2; -- mon
          WHEN 2 THEN Result := 3; -- tue
          WHEN 3 THEN Result := 4; -- wed
          WHEN 4 THEN Result := 5; -- thu
          WHEN 5 THEN Result := 6; -- fri
          WHEN 6 THEN Result := 7; -- sat
          END CASE;
        END;
      ELSE
        IF (ORAWeekStartOn = 7) AND  (PimsWeekStartOn = 7) THEN -- First day: Saturday
          BEGIN
            CASE DateDayNumber
            WHEN 7 THEN Result := 2; -- sun
            WHEN 1 THEN Result := 3; -- mon
            WHEN 2 THEN Result := 4; -- tue
            WHEN 3 THEN Result := 5; -- wed
            WHEN 4 THEN Result := 6; -- thu
            WHEN 5 THEN Result := 7; -- fri
            WHEN 6 THEN Result := 1; -- sat
            END CASE;
          END;
          END IF;
        END IF;
      END IF;
    END IF;
  END IF;

  return(Result);
end PimsDayOfWeek;
/

commit;

