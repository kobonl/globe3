--
-- PIM-151 Views for Personal/Production Screen + Ghost Counts.
--

create or replace view v_realtimecurrghostcount as
select
       r.plant_code,
       r.shift_date,
       r.shift_number,
       s.description shift,
       nvl(m.machine_code,w.workspot_code) machine_code,
       nvl(m.description,w.description) machine,
       w.workspot_code,
       w.description workspot_description,
       j.job_code,
       j.description job_code_description,
       v.currintervalstarttime intervalstarttime,
       v.lastintervalendtime intervalendtime,
       round(sum(r.workspotquantity)) qty,
       (select round(nvl(sum(t2.employeequantity),0)) from employeeeffperminute t2 where
         t2.plant_code = r.plant_code and t2.workspot_code = w.workspot_code and
         t2.job_code = j.job_code and t2.shift_date = r.shift_date and
         t2.shift_number = r.shift_number and
         t2.intervalstarttime >= v.currintervalstarttime and
         t2.intervalendtime <= v.lastintervalendtime) qty2,
       sum(r.workspotsecondsactual) secondsactual,
       sum(r.workspotsecondsnorm) secondsnorm,
       sum(r.workspotsecondsactual * r.normlevel) / 3600 normqty,
       round(sum(r.workspotsecondsactual) / 60, 1) minutesactual,
       round(sum(r.workspotsecondsnorm) / 60, 1) minutesnorm,
       case when sum(r.workspotsecondsactual) = 0
         then 0
         else sum(r.workspotsecondsnorm) / sum(r.workspotsecondsactual) * 100
       end eff
 from efficiencycalcperiod v
 inner join workspoteffperminute r on (r.plant_code = v.plant_code and
                                       r.shift_date = trunc(sysdate) and
                                       r.intervalstarttime >= v.currintervalstarttime and
                                       r.intervalendtime <= v.lastintervalendtime)
 left outer join shift s on (s.plant_code = r.plant_code and  s.shift_number = r.shift_number)
 inner join workspot w on (w.plant_code = r.plant_code and w.workspot_code = r.workspot_code)
 inner join jobcode j on (j.plant_code = r.plant_code and j.workspot_code = r.workspot_code and j.job_code = r.job_code)
  left outer join machine m on (m.plant_code = r.plant_code and m.machine_code = w.machine_code)
 group by r.plant_code,
          r.shift_date,
          r.shift_number,
          s.description,
          m.machine_code,
          m.description,
          w.workspot_code,
          w.description,
          j.job_code,
          j.description,
          v.currintervalstarttime,
          v.lastintervalendtime
/

create or replace view v_realtimeshiftghostcount as
select
       r.plant_code,
       r.shift_date,
       r.shift_number,
       s.description shift,
       nvl(m.machine_code,w.workspot_code) machine_code,
       nvl(m.description,w.description) machine,
       w.workspot_code,
       w.description workspot_description,
       j.job_code,
       j.description job_code_description,
       round(sum(r.workspotquantity)) qty,
       (select round(nvl(sum(t2.employeequantity),0)) from employeeeffperminute t2 where
         t2.plant_code = r.plant_code and t2.workspot_code = w.workspot_code and
         t2.job_code = j.job_code and t2.shift_date = r.shift_date and
         t2.shift_number = r.shift_number) qty2,
       sum(r.workspotsecondsactual) secondsactual,
       sum(r.workspotsecondsnorm) secondsnorm,
       sum(r.workspotsecondsactual * r.normlevel) / 3600 normqty,
       round(sum(r.workspotsecondsactual) / 60, 1) minutesactual,
       round(sum(r.workspotsecondsnorm) / 60, 1) minutesnorm,
       case when sum(r.workspotsecondsactual) = 0
         then 0
         else sum(r.workspotsecondsnorm) / sum(r.workspotsecondsactual) * 100
       end eff
  from efficiencycalcperiod v
 inner join workspoteffperminute r on (r.plant_code = v.plant_code and
                                       r.shift_date = trunc(sysdate) and
                                       r.intervalendtime <= v.lastintervalendtime)
 left outer join shift s on (s.plant_code = r.plant_code and  s.shift_number = r.shift_number)
 inner join workspot w on (w.plant_code = r.plant_code and w.workspot_code = r.workspot_code)
 inner join jobcode j on (j.plant_code = r.plant_code and j.workspot_code = r.workspot_code and j.job_code = r.job_code)
  left outer join machine m on (m.plant_code = r.plant_code and m.machine_code = w.machine_code)
 group by r.plant_code,
          r.shift_date,
          r.shift_number,
          s.description,
          m.machine_code,
          m.description,
          w.workspot_code,
          w.description,
          j.job_code,
          j.description
/

commit;

