--
-- PIM-319 Do not subtract breaks from Production Hours
--

UPDATE PIMSSETTING
set PRODHRS_NOSUBTRACT_BREAKS = 'Y';

commit;
