--
-- GLOB3-156 - Interface Syntegro en Globe
--

create or replace package PACKAGE_SYNTEGRO is

  -- Author  : MARCEL.RAND
  -- Created : 21-09-2018 15:05:10
  -- Purpose : Package Syntegro Interface
  
  procedure scans_2_syntegro;
  procedure employees_2_globe;

end PACKAGE_SYNTEGRO;
/

create or replace package body PACKAGE_SYNTEGRO is

  -- Private constant declarations
  SEP constant varchar2(1) := ';';

  --internal procedures
  procedure write_log(lfp_logmessage    in abslog.logmessage%type,
                      lfp_computer_name in varchar2,
                      lfp_priority      in abslog.priority%type) is
    pragma autonomous_transaction;
  begin
    /*
    Write message in ABSLOG with these priorities
    1 = Message
    2 = Warning
    3 = Error
    4 = Stack trace
    5 = Debug info
    */
    dbms_output.put_line(lfp_logmessage);
    insert into abslog
      (abslog_id, computer_name, abslog_timestamp, logsource, priority, systemuser, logmessage)
    values
      (seq_abslog.nextval, lfp_computer_name, sysdate /*abslog_timestamp*/, 'package_syntegro'
       /*logsource*/, lfp_priority /*priority*/, 'Globe3' /*systemuser*/,
       substr(lfp_logmessage, 1, 1000)); -- Change from 255 to 1000
    --commit log
    commit;
  end write_log;
  
  -- Function and procedure implementations
  function DateTime2Str(ADate Date) return varchar2 is
  begin
    return TO_CHAR(ADate, 'YYYY-MM-DD HH24:MI');
  end;
  
  function NewScansFilename(AName varchar2) return varchar2 is
  begin
    return AName || TO_CHAR(sysdate, 'YYYYMMDD_HH24_MI_SS') || '.csv';
  end;

  procedure scans_2_syntegro is
    lv_message      varchar2(1000);
    lv_computername varchar2(32);
    lv_file         utl_file.file_type;
    lv_line         varchar2(32767);
    lv_count        integer;
  begin
    begin
      lv_computername := 'scans_2_syntegro';
      lv_message := 'scans 2 syntegro';

      lv_count := 0;
      for x in 
        (select 
           s.scanevent_id, s.scanevent_timestamp, s.employee_number, 
           s.datetime_in, s.datetime_out, s.workspot_code, s.marker, 
           s.exported
           from scanevent s
           where s.exported = 0 and s.scanevent_timestamp >= trunc(sysdate)
           order by 1
         ) loop
         if lv_count = 0 then
           lv_file := utl_file.fopen('CLOCKINGS_DIR',NewScansFilename('Clockings'),'W',32767);
         end if;
         lv_line := x.marker || SEP || x.employee_number || SEP || DateTime2Str(x.datetime_in) || SEP ||
           DateTime2Str(x.datetime_out) || SEP || x.workspot_code;
  --       dbms_output.put_line(lv_line);
         utl_file.put_line(lv_file, lv_line);
         lv_count := lv_count + 1;
         update scanevent s
         set s.exported = 1
         where s.scanevent_id = x.scanevent_id;
      end loop;
      if lv_count > 0 then
        utl_file.fclose(lv_file);
        commit;
      end if;
    exception
      when no_data_found then
      begin
        null;
      end;
      when others then
      begin
        rollback;
        write_log(lv_message || sqlerrm, lv_computername, 3);
      end;
    end;
  end scans_2_syntegro;
  
  procedure transfer_employees_2_globe(alv_filename varchar2) as
    lv_message       varchar2(1000);
    lv_computername  varchar2(32);
    lv_file          utl_file.file_type;
    lv_line          varchar2(1000);
    lv_filename      varchar2(128);
    lv_filename_done varchar2(128);
    lv_error_count   integer;
    lv_ok            integer;
    TYPE temp_rec IS RECORD
    (
      employee_number number, 
      short_name varchar2(6),
      description varchar2(40),
      sex varchar2(1),
      contractgroup_code varchar2(6),
      language_code varchar2(3),
      team_code varchar2(6),
      plant_code varchar2(6),
      department_code varchar2(6),
      shift_number number,
      startdate date,
      enddate date,
      firstaid_yn varchar2(1),
      is_scanning_yn varchar2(1),
      cut_of_time_yn varchar2(1),
      firstaid_exp_date date
     );
     emp_rec temp_rec;    
     function get_field(alv_line varchar2, anr integer) return varchar2 as
     begin 
       return RTRIM (REGEXP_SUBSTR (alv_line, '[^;]*;', 1, anr), ';');
     end;
     function check_log_errors return integer is
       lv_errorline varchar2(1000);
       lv_dummy varchar2(100);
       lv_error_file utl_file.file_type;
     begin
       lv_errorline := '';
       begin
         select p.plant_code
         into lv_dummy
         from plant p
         where p.plant_code = emp_rec.plant_code;
       exception
         when no_data_found then
           lv_errorline := lv_errorline || 'Plant (' || emp_rec.plant_code || ') not found. ';
       end;       
       begin
         select cg.contractgroup_code
         into lv_dummy
         from contractgroup cg
         where cg.contractgroup_code = emp_rec.contractgroup_code;
       exception
         when no_data_found then
           lv_errorline := lv_errorline || 'Contract group (' || emp_rec.contractgroup_code || ') not found. ';
       end;
       begin
         select la.language_code
         into lv_dummy
         from language la
         where la.language_code = emp_rec.language_code;
       exception
         when no_data_found then
           lv_errorline := lv_errorline || 'Language (' || emp_rec.language_code || ') not found. ';
       end;
       begin
         select d.department_code
         into lv_dummy
         from department d
         where d.plant_code = emp_rec.plant_code and d.department_code = emp_rec.department_code;
       exception
         when no_data_found then
           lv_errorline := lv_errorline || 'Department (' || emp_rec.department_code || ') not found. ';
       end;
       begin
         select t.team_code
         into lv_dummy
         from team t
         where t.plant_code = emp_rec.plant_code and t.team_code = emp_rec.team_code;
       exception
         when no_data_found then
           lv_errorline := lv_errorline || 'Team (' || emp_rec.team_code || ') not found. ';
       end;
       begin
         select to_char(s.shift_number)
         into lv_dummy
         from shift s
         where s.plant_code = emp_rec.plant_code and s.shift_number = emp_rec.shift_number;
       exception
         when no_data_found then
           lv_errorline := lv_errorline || 'Shift (' || emp_rec.shift_number || ') not found. ';
       end;
       if lv_errorline is not null then
         lv_error_file := utl_file.fopen('EMPLOYEES_DIR','employees_error_log.txt','A',1000);
         utl_file.put_line(lv_error_file, TO_CHAR(sysdate, 'YYYY-MM-DD HH24:MI') || ' - Employee (' || emp_rec.employee_number || '): ' || lv_errorline);
         utl_file.fclose(lv_error_file);
         return 1;
       else 
         return 0;
       end if;
     end check_log_errors;
  begin
    begin
      lv_computername := 'employees_2_globe';
      lv_message := 'employees 2 globe';
      lv_filename := alv_filename;
      lv_filename_done := 'employees_done.csv';
      lv_error_count := 0;
      lv_file := UTL_FILE.FOPEN ('EMPLOYEES_DIR', lv_filename, 'R');
      IF UTL_FILE.IS_OPEN(lv_file) THEN
        LOOP
          BEGIN
            lv_ok := 0;
            UTL_FILE.GET_LINE(lv_file, lv_line, 1000);
            IF lv_line IS NULL THEN
              EXIT;
            END IF;
            emp_rec.employee_number := TO_NUMBER(get_field(lv_line, 1));
            emp_rec.short_name := get_field(lv_line, 2);
            emp_rec.description := get_field(lv_line, 3);
            emp_rec.sex := get_field(lv_line, 4);
            emp_rec.contractgroup_code := get_field(lv_line, 5);
            emp_rec.language_code := get_field(lv_line, 6);
            emp_rec.team_code := get_field(lv_line, 7);
            emp_rec.plant_code := get_field(lv_line, 8);
            emp_rec.department_code := get_field(lv_line, 9);
            emp_rec.shift_number := TO_NUMBER(get_field(lv_line, 10));
            begin
              emp_rec.startdate := TO_DATE(get_field(lv_line, 11), 'YYYY-MM-DD');
            exception
              when others then
                lv_error_count := lv_error_count + 1;
                write_log(lv_message || sqlerrm, lv_computername, 3);
            end;
            if get_field(lv_line, 12) IS NOT NULL THEN
            begin
              emp_rec.enddate := TO_DATE(get_field(lv_line, 12), 'YYYY-MM-DD');
            exception
              when others then
                lv_error_count := lv_error_count + 1;
                write_log(lv_message || sqlerrm, lv_computername, 3);
            end;
            else
              emp_rec.enddate := NULL;
            end if;
            emp_rec.firstaid_yn := get_field(lv_line, 13);
            emp_rec.is_scanning_yn := get_field(lv_line, 14);
            emp_rec.cut_of_time_yn := get_field(lv_line, 15);
            if emp_rec.firstaid_yn = 'Y' then
            begin
              emp_rec.firstaid_exp_date := TO_DATE(get_field(lv_line, 16), 'YYYY-MM-DD');
            exception
              when others then
                lv_error_count := lv_error_count + 1;
                write_log(lv_message || sqlerrm, lv_computername, 3);
            end;
            else
              emp_rec.firstaid_exp_date := NULL;
            end if;
            if check_log_errors = 0 then
            BEGIN
              INSERT INTO EMPLOYEE 
              (
                EMPLOYEE_NUMBER,
                SHORT_NAME,
                DESCRIPTION,
                SEX,
                CONTRACTGROUP_CODE,
                LANGUAGE_CODE,
                TEAM_CODE,
                PLANT_CODE,
                DEPARTMENT_CODE,
                SHIFT_NUMBER,
                STARTDATE,
                ENDDATE,
                FIRSTAID_YN,
                IS_SCANNING_YN,
                CUT_OF_TIME_YN,
                FIRSTAID_EXP_DATE,
                CREATIONDATE,
                MUTATIONDATE,
                MUTATOR
              )
              VALUES
              (
                emp_rec.employee_number,
                emp_rec.short_name,
                emp_rec.description,
                emp_rec.sex,
                emp_rec.contractgroup_code,
                emp_rec.language_code,
                emp_rec.team_code,
                emp_rec.plant_code,
                emp_rec.department_code,
                emp_rec.shift_number,
                emp_rec.startdate,
                emp_rec.enddate,
                emp_rec.firstaid_yn,
                emp_rec.is_scanning_yn,
                emp_rec.cut_of_time_yn,
                emp_rec.firstaid_exp_date,
                sysdate,
                sysdate,
                'SYNTEGRO'
              );
              commit;
              lv_ok := 1;
            EXCEPTION
              WHEN DUP_VAL_ON_INDEX THEN
              BEGIN
                UPDATE EMPLOYEE
                SET 
                  SHORT_NAME = emp_rec.short_name,
                  DESCRIPTION = emp_rec.description,
                  SEX = emp_rec.sex,
                  CONTRACTGROUP_CODE = emp_rec.contractgroup_code,
                  LANGUAGE_CODE = emp_rec.language_code,
                  TEAM_CODE = emp_rec.team_code,
                  PLANT_CODE = emp_rec.plant_code,
                  DEPARTMENT_CODE = emp_rec.department_code,
                  SHIFT_NUMBER = emp_rec.shift_number,
                  STARTDATE = emp_rec.startdate,
                  ENDDATE = emp_rec.enddate,
                  FIRSTAID_YN = emp_rec.firstaid_yn,
                  IS_SCANNING_YN = emp_rec.is_scanning_yn,
                  CUT_OF_TIME_YN = emp_rec.cut_of_time_yn,
                  FIRSTAID_EXP_DATE = emp_rec.firstaid_exp_date,
                  MUTATIONDATE = sysdate,
                  MUTATOR = 'SYNTEGRO'
                WHERE EMPLOYEE_NUMBER = emp_rec.employee_number;
                commit;
                lv_ok := 1;
              EXCEPTION
                WHEN OTHERS THEN
                  lv_error_count := lv_error_count + 1;
                  rollback;
                  write_log(lv_message || sqlerrm, lv_computername, 3);
              END;
              WHEN OTHERS THEN
                lv_error_count := lv_error_count + 1;
                write_log(lv_message || sqlerrm, lv_computername, 3);
            END;
            end if;
          EXCEPTION
          WHEN NO_DATA_FOUND THEN
            EXIT;
          END;
        END LOOP;
        if lv_error_count = 0 then
          UTL_FILE.FRENAME('EMPLOYEES_DIR', lv_filename, 'EMPLOYEES_DIR', lv_filename_done, TRUE);        
        end if;
      END IF;
      UTL_FILE.FCLOSE(lv_file);      

    exception
      when no_data_found then
        write_log(lv_message || sqlerrm, lv_computername, 3);
      when others then
        write_log(lv_message || sqlerrm, lv_computername, 3);
    end;
  end transfer_employees_2_globe;

  procedure employees_2_globe is
  begin
    transfer_employees_2_globe('employees.csv');
  end employees_2_globe;


end PACKAGE_SYNTEGRO;
/
