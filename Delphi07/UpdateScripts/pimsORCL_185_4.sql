--
-- GLOB3-223 - Restructure availability of functionality made for NTS
--

--
-- PIMSSETTING
--
-- Add/modify columns 
declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'alter table PIMSSETTING add OVERTIME_PERDAY_YN VARCHAR2(1) default ''N''';
    exception when column_exists then null;
end;
/

--
-- PLANT
--
-- Add/modify columns 
declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'alter table PLANT add STAFFPLAN_SHOWTOT_YN VARCHAR2(1) default ''N''';
    exception when column_exists then null;
end;
/


commit;

