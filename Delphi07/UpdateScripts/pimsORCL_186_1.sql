--
-- PIM-377 Show start and end times in planning reports 
--

--
-- PIMSSETTING
--
-- Add/modify columns 
declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'alter table PIMSSETTING add REPPLAN_STARTEND_TIMES_YN VARCHAR2(1) default ''N''';
    exception when column_exists then null;
end;
/

UPDATE PIMSDBVERSION SET DBVERSION = '186';


commit;

