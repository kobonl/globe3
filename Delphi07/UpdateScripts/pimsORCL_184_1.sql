--
-- Glob3-81 Change for overtime hours
--

-- Add/modify columns 
declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'alter table OVERTIMEDEFINITION add FROMTIME date';
    exception when column_exists then null;
end;
/

-- Add/modify columns 
declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'alter table OVERTIMEDEFINITION add TOTIME date';
    exception when column_exists then null;
end;
/

-- Add/modify columns 
declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'alter table OVERTIMEDEFINITION add DAY_OF_WEEK number(10)';
    exception when column_exists then null;
end;
/

-- Add/modify columns 
declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'alter table ABSENCEREASON add EXPORT_CODE varchar2(6)';
    exception when column_exists then null;
end;
/


UPDATE PIMSDBVERSION SET DBVERSION = '184';

commit;

