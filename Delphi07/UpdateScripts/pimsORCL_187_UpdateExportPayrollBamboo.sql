--
-- GLOB3-377 Export to payroll - BambooHR
--

update exportpayroll
set export_type = 'BAMBOO';

commit;
