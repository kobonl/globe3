--
-- PIM-203 Read ID's with no visual display
--

declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'alter table WORKSPOT add HOST varchar2(30)';
    exception when column_exists then null;
end;
/

declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'alter table WORKSPOT add PORT number';
    exception when column_exists then null;
end;
/

commit;


