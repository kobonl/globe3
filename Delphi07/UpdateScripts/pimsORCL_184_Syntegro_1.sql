--
-- GLOB3-156 - Interface Syntegro en Globe
--

-- Create table
create table SCANEVENT
(
  SCANEVENT_ID         NUMBER(10) not null,
  SCANEVENT_TIMESTAMP  DATE not null,
  EMPLOYEE_NUMBER      NUMBER(10) not null,
  DATETIME_IN          DATE not null,
  DATETIME_OUT         DATE,
  WORKSPOT_CODE        VARCHAR2(6) not null,
  MARKER               VARCHAR2(1) not null,
  EXPORTED             NUMBER(10) default 0 not null
)
tablespace ABS_DATA
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
  
-- Create/Recreate primary, unique and foreign key constraints 
alter table SCANEVENT
  add constraint XPSCANEVENT primary key (SCANEVENT_ID)
  using index 
  tablespace INDX
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
  
-- Create sequence 
create sequence SEQ_SCANEVENT
minvalue 1
maxvalue 1000000000000000000000000000
start with 1
increment by 1
cache 20;

CREATE OR REPLACE TRIGGER SETSCANEVENTID BEFORE INSERT
ON SCANEVENT FOR EACH ROW
DECLARE
  NEWID INTEGER;
BEGIN
  SELECT SEQ_SCANEVENT.NEXTVAL INTO NEWID FROM DUAL;
  :NEW.SCANEVENT_ID := NEWID;
END;
/
CREATE OR REPLACE TRIGGER TRS_SCANEVENT
  BEFORE INSERT OR UPDATE OR DELETE ON TIMEREGSCANNING
  FOR EACH ROW
DECLARE
  -- declare variables here
BEGIN
  IF INSERTING THEN
    INSERT INTO SCANEVENT (
      SCANEVENT_ID,
      SCANEVENT_TIMESTAMP,
      EMPLOYEE_NUMBER,
      DATETIME_IN,
      DATETIME_OUT,
      WORKSPOT_CODE,
      MARKER,
      EXPORTED
    )
    VALUES
    (
      0,
      SYSDATE,
      :NEW.EMPLOYEE_NUMBER,
      :NEW.DATETIME_IN,
      :NEW.DATETIME_OUT,
      :NEW.WORKSPOT_CODE,
      'C',      
      0
    );
  ELSIF UPDATING THEN
    INSERT INTO SCANEVENT (
      SCANEVENT_ID,
      SCANEVENT_TIMESTAMP,
      EMPLOYEE_NUMBER,
      DATETIME_IN,
      DATETIME_OUT,
      WORKSPOT_CODE,
      MARKER,
      EXPORTED
    )
    VALUES
    (
      0,
      SYSDATE,
      :OLD.EMPLOYEE_NUMBER,
      :OLD.DATETIME_IN,
      :OLD.DATETIME_OUT,
      :OLD.WORKSPOT_CODE,
      'D',      
      0
    );
    INSERT INTO SCANEVENT (
      SCANEVENT_ID,
      SCANEVENT_TIMESTAMP,
      EMPLOYEE_NUMBER,
      DATETIME_IN,
      DATETIME_OUT,
      WORKSPOT_CODE,
      MARKER,
      EXPORTED
    )
    VALUES
    (
      0,
      SYSDATE,
      :NEW.EMPLOYEE_NUMBER,
      :NEW.DATETIME_IN,
      :NEW.DATETIME_OUT,
      :NEW.WORKSPOT_CODE,
      'C',      
      0
    );
  ELSE
    INSERT INTO SCANEVENT (
      SCANEVENT_ID,
      SCANEVENT_TIMESTAMP,
      EMPLOYEE_NUMBER,
      DATETIME_IN,
      DATETIME_OUT,
      WORKSPOT_CODE,
      MARKER,
      EXPORTED
    )
    VALUES
    (
      0,
      SYSDATE,
      :OLD.EMPLOYEE_NUMBER,
      :OLD.DATETIME_IN,
      :OLD.DATETIME_OUT,
      :OLD.WORKSPOT_CODE,
      'D',      
      0
    );
  END IF;
END TRS_SCANEVENT;
/

-- Must by done by system user 

CREATE DIRECTORY CLOCKINGS_DIR AS 'C:\PIMSROOT\ABS\Pims Oracle\Import\Clockings';

GRANT READ, WRITE, EXECUTE ON DIRECTORY CLOCKINGS_DIR TO PIMS;

CREATE DIRECTORY EMPLOYEES_DIR AS 'C:\PIMSROOT\ABS\Pims Oracle\Import\Employees';

GRANT READ, WRITE, EXECUTE ON DIRECTORY EMPLOYEES_DIR TO PIMS;
/




