--
-- PIM-139 Lunch button optionally scan out
--

declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'alter table PIMSSETTING add LUNCH_BTN_SCANOUT_YN varchar2(1) default ''N''';
    exception when column_exists then null;
end;
/
