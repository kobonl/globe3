--
-- GLOB3-330 - Make functionality with 'occupied' optional
--

--
-- PIMSSETTING
--
-- Add/modify columns 
declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'alter table PIMSSETTING add OCCUP_HANDLING NUMBER(10) DEFAULT 2';
    exception when column_exists then null;
end;
/

commit;
