--
-- PIM-135 Do not filter out TDOWN, PDOWN and BREAK jobs
--
create or replace view v_quantities_without_time as
select we.plant_code,
       we.shift_number,
       we.workspot_code,
       w.description workspot,
       we.job_code,
       j.description job_description,
       we.workspotquantity qty,
       we.intervalstarttime,
       we.intervalendtime
  from workspoteffperminute we,
       workspot w,
       jobcode j
 where we.workspotsecondsactual = 0
   and we.workspotquantity > 0
   and we.job_code not in ('NOJOB')
   and w.plant_code = we.plant_code
   and w.workspot_code = we.workspot_code
   and j.plant_code = we.plant_code
   and j.workspot_code = we.workspot_code
   and j.job_code = we.job_code
   and we.shift_date = trunc(sysdate)
/

create or replace view v_timereg_by_minute as
select -- This view is used by produce calc_efficiency for expaning timereg records to a result set with rows per minute per scan
       /*+ index(r XIE4TIMEREGSCANNING) */
       r.plant_code,
       nvl(r.shift_date, trunc(r.datetime_in)) shift_date,
       r.shift_number,
       r.workspot_code,
       r.job_code,
       j.norm_prod_level norm_level,
       r.employee_number,
       r.datetime_in,
       nvl(r.datetime_out,e.currintervalendtime) datetime_out,
       to_date(to_char(r.datetime_in,'YYYYMMDDHH24MI'),'YYYYMMDDHH24MI') + t.startoffset  intervalstarttime,
       to_date(to_char(r.datetime_in,'YYYYMMDDHH24MI'),'YYYYMMDDHH24MI') + t.endoffset intervalendtime,
       case when r.datetime_in > to_date(to_char(r.datetime_in,'YYYYMMDDHH24MI'),'YYYYMMDDHH24MI') + t.startoffset
        then r.datetime_in
        else to_date(to_char(r.datetime_in,'YYYYMMDDHH24MI'),'YYYYMMDDHH24MI') + t.startoffset
       end start_timestamp,
       case when nvl(r.datetime_out,e.currintervalendtime) < to_date(to_char(r.datetime_in,'YYYYMMDDHH24MI'),'YYYYMMDDHH24MI') + t.endoffset
        then nvl(r.datetime_out,e.currintervalendtime)
        else to_date(to_char(r.datetime_in,'YYYYMMDDHH24MI'),'YYYYMMDDHH24MI') + t.endoffset
       end end_timestamp,
       case when to_date(to_char(r.datetime_in,'YYYYMMDDHH24MI'),'YYYYMMDDHH24MI') + t.startoffset >= r.datetime_in and
                 to_date(to_char(r.datetime_in,'YYYYMMDDHH24MI'),'YYYYMMDDHH24MI') + t.endoffset <= nvl(r.datetime_out,e.currintervalendtime)
          then 60
          else
              case when to_date(to_char(r.datetime_in,'YYYYMMDDHH24MI'),'YYYYMMDDHH24MI') + t.startoffset < r.datetime_in and
                        to_date(to_char(r.datetime_in,'YYYYMMDDHH24MI'),'YYYYMMDDHH24MI') + t.endoffset <= nvl(r.datetime_out,e.currintervalendtime)
                then 60 - (r.datetime_in - (to_date(to_char(r.datetime_in,'YYYYMMDDHH24MI'),'YYYYMMDDHH24MI') + t.startoffset)) * 24 * 3600
                else
                  case when to_date(to_char(r.datetime_in,'YYYYMMDDHH24MI'),'YYYYMMDDHH24MI') + t.startoffset >= r.datetime_in and
                            to_date(to_char(r.datetime_in,'YYYYMMDDHH24MI'),'YYYYMMDDHH24MI') + t.endoffset > nvl(r.datetime_out,e.currintervalendtime)
                     then (nvl(r.datetime_out,e.currintervalendtime) - (to_date(to_char(r.datetime_in,'YYYYMMDDHH24MI'),'YYYYMMDDHH24MI') + t.startoffset)) * 24 * 3600
                     else (nvl(r.datetime_out,e.currintervalendtime) - r.datetime_in) * 24 * 3600
                  end
              end
       end seconds
  from timeregscanning r, workspot w, jobcode j, timeofday t, efficiencycalcperiod e
 where to_date(to_char(r.datetime_in,'YYYYMMDDHH24MI'),'YYYYMMDDHH24MI') + t.startoffset < nvl(r.datetime_out,e.currintervalendtime)
   and r.job_code not in ('NOJOB')
   and (nvl(r.datetime_out, TO_DATE('2099-12-31 00:00:00', 'yyyy-mm-dd hh24:mi:ss')) = TO_DATE('2099-12-31 00:00:00', 'yyyy-mm-dd hh24:mi:ss')
    or nvl(r.datetime_out, TO_DATE('2099-12-31 00:00:00', 'yyyy-mm-dd hh24:mi:ss')) > e.currintervalstarttime)
   and r.datetime_in between e.currintervalstarttime - 1 /*look 1 day back*/ and e.currintervalendtime
   and to_date(to_char(r.datetime_in,'YYYYMMDDHH24MI'),'YYYYMMDDHH24MI') + t.startoffset >= e.currintervalstarttime
   and to_date(to_char(r.datetime_in,'YYYYMMDDHH24MI'),'YYYYMMDDHH24MI') + t.startoffset <  e.currintervalendtime
   and e.plant_code = r.plant_code
   and w.plant_code = r.plant_code
   and w.workspot_code = r.workspot_code
   and w.measure_productivity_yn = 'Y'
   and j.plant_code = r.plant_code
   and j.workspot_code = r.workspot_code
   and j.job_code = r.job_code
/

create or replace view v_timereg_by_minute_corr as
select -- This view is used by produce recalc_efficiency for expaning timereg records to a result set with rows per minute per scan
       /*+ index(r XSPSHIFTDATE) */
       r.plant_code,
       r.shift_date,
       r.shift_number,
       r.workspot_code,
       r.job_code,
       j.norm_prod_level norm_level,
       r.employee_number,
       r.datetime_in,
       nvl(r.datetime_out,e.currintervalendtime) datetime_out,
       to_date(to_char(r.datetime_in,'YYYYMMDDHH24MI'),'YYYYMMDDHH24MI') + t.startoffset  intervalstarttime,
       to_date(to_char(r.datetime_in,'YYYYMMDDHH24MI'),'YYYYMMDDHH24MI') + t.endoffset intervalendtime,
       case when r.datetime_in > to_date(to_char(r.datetime_in,'YYYYMMDDHH24MI'),'YYYYMMDDHH24MI') + t.startoffset
        then r.datetime_in
        else to_date(to_char(r.datetime_in,'YYYYMMDDHH24MI'),'YYYYMMDDHH24MI') + t.startoffset
       end start_timestamp,
       case when nvl(r.datetime_out,e.currintervalendtime) < to_date(to_char(r.datetime_in,'YYYYMMDDHH24MI'),'YYYYMMDDHH24MI') + t.endoffset
        then nvl(r.datetime_out,e.currintervalendtime)
        else to_date(to_char(r.datetime_in,'YYYYMMDDHH24MI'),'YYYYMMDDHH24MI') + t.endoffset
       end end_timestamp,
       case when to_date(to_char(r.datetime_in,'YYYYMMDDHH24MI'),'YYYYMMDDHH24MI') + t.startoffset >= r.datetime_in and
                 to_date(to_char(r.datetime_in,'YYYYMMDDHH24MI'),'YYYYMMDDHH24MI') + t.endoffset <= nvl(r.datetime_out,e.currintervalendtime)
          then 60
          else
              case when to_date(to_char(r.datetime_in,'YYYYMMDDHH24MI'),'YYYYMMDDHH24MI') + t.startoffset < r.datetime_in and
                        to_date(to_char(r.datetime_in,'YYYYMMDDHH24MI'),'YYYYMMDDHH24MI') + t.endoffset <= nvl(r.datetime_out,e.currintervalendtime)
                then 60 - (r.datetime_in - (to_date(to_char(r.datetime_in,'YYYYMMDDHH24MI'),'YYYYMMDDHH24MI') + t.startoffset)) * 24 * 3600
                else
                  case when to_date(to_char(r.datetime_in,'YYYYMMDDHH24MI'),'YYYYMMDDHH24MI') + t.startoffset >= r.datetime_in and
                            to_date(to_char(r.datetime_in,'YYYYMMDDHH24MI'),'YYYYMMDDHH24MI') + t.endoffset > nvl(r.datetime_out,e.currintervalendtime)
                     then (nvl(r.datetime_out,e.currintervalendtime) - (to_date(to_char(r.datetime_in,'YYYYMMDDHH24MI'),'YYYYMMDDHH24MI') + t.startoffset)) * 24 * 3600
                     else (nvl(r.datetime_out,e.currintervalendtime) - r.datetime_in) * 24 * 3600
                  end
              end
       end seconds
  from timeregscanning r, workspot w, jobcode j, timeofday t, efficiencycalcperiod e
 where to_date(to_char(r.datetime_in,'YYYYMMDDHH24MI'),'YYYYMMDDHH24MI') + t.startoffset < nvl(r.datetime_out,e.lastintervalendtime)
   and to_date(to_char(r.datetime_in,'YYYYMMDDHH24MI'),'YYYYMMDDHH24MI') + t.startoffset < e.lastintervalendtime
   and r.job_code not in ('NOJOB')
   and r.datetime_in between r.shift_date - 1 and  r.shift_date + 2
   and e.plant_code = r.plant_code
   and w.plant_code = r.plant_code
   and w.workspot_code = r.workspot_code
   and w.measure_productivity_yn = 'Y'
   and j.plant_code = r.plant_code
   and j.workspot_code = r.workspot_code
   and j.job_code = r.job_code
/

