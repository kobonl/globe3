--
-- PIM-179 Add setting to trigger a Hybrid-mode
--

declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'alter table PIMSSETTING add HYBRID number(10) default 0';
    exception when column_exists then null;
end;
/

-- Add comments to the columns 
comment on column PIMSSETTING.HYBRID
  is '0=none 1=hybrid';

UPDATE PIMSDBVERSION SET DBVERSION = '182';

commit;


