BEGIN
DBMS_SCHEDULER.CREATE_JOB (
   job_name             => 'EMPSIDS_2_GLOBE_JOB',
   job_type             => 'PLSQL_BLOCK',
   job_action           => 'BEGIN package_brolli.employees_ids_2_globe; END;',
   start_date           => sysdate,
   repeat_interval      => 'FREQ=HOURLY;INTERVAL=1;BYHOUR=05,17;BYMINUTE=00',
   enabled              =>  TRUE,
   comments             => 'Emps/Ids to Globe');
END;
/
