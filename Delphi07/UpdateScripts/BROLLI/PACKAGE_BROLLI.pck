create or replace package PACKAGE_BROLLI is

  -- Author  : MARCEL.RAND
  -- Created : 10-02-2021 14:58:27
  -- Purpose : Package BROLLI Interface
  DEBUG Integer;

  procedure employees_ids_2_globe;

end PACKAGE_BROLLI;
/
create or replace package body PACKAGE_BROLLI is

  --internal procedures
  procedure write_log(lfp_logmessage    in abslog.logmessage%type,
                      lfp_computer_name in varchar2,
                      lfp_priority      in abslog.priority%type) is
    pragma autonomous_transaction;
  begin
    /*
    Write message in ABSLOG with these priorities
    1 = Message
    2 = Warning
    3 = Error
    4 = Stack trace
    5 = Debug info
    */
    dbms_output.put_line(lfp_logmessage);
    insert into abslog
      (abslog_id, computer_name, abslog_timestamp, logsource, priority, systemuser, logmessage)
    values
      (seq_abslog.nextval, lfp_computer_name, sysdate /*abslog_timestamp*/, 'package_brolli'
       /*logsource*/, lfp_priority /*priority*/, 'Globe3' /*systemuser*/,
       substr(lfp_logmessage, 1, 1000)); -- Change from 255 to 1000
    --commit log
    commit;
  end write_log;

  procedure InitSettings is
  begin
    DEBUG := 0;
  end;

  PROCEDURE WDebugLog(ALogMessage Varchar2)
  as
  begin
    if DEBUG = 1 then
      write_log(ALogMessage, 'employees_2_globe', 5);
    end if;
  end WDebugLog;

  PROCEDURE Sleep(seconds NUMBER) AS
  BEGIN
    DBMS_LOCK.SLEEP(seconds);
  END;

  procedure transfer_employees_ids_2_globe as
    lv_message       varchar2(1000);
    lv_computername  varchar2(32);
    lv_error_count   integer;
    lv_ok            integer;
    lv_employee_number integer;
    lv_idcard_number varchar2(15);
    lv_employee_found boolean;
    lv_update_needed boolean;
    lv_idcard_found boolean;
    TYPE temp_rec IS RECORD
    (
      employee_number number,
      short_name varchar2(6),
      description varchar2(40),
      sex varchar2(1),
      contractgroup_code varchar2(6),
      language_code varchar2(3),
      team_code varchar2(6),
      plant_code varchar2(6),
      department_code varchar2(6),
      shift_number number,
      startdate date,
      enddate date,
      firstaid_yn varchar2(1),
      is_scanning_yn varchar2(1),
      calc_bonus_yn varchar2(1),
      cut_of_time_yn varchar2(1),
      idnumber varchar2(15),
      idemployee_number number,
      idstartdate date,
      idenddate date
     );
     emp_rec temp_rec;
     function check_log_errors return integer is
       lv_errorline varchar2(1000);
       lv_dummy varchar2(100);
     begin
       lv_errorline := '';
       begin
         select p.plant_code
         into lv_dummy
         from plant p
         where p.plant_code = emp_rec.plant_code;
       exception
         when no_data_found then
           lv_errorline := lv_errorline || 'Plant (' || emp_rec.plant_code || ') not found. ';
       end;
       begin
         select cg.contractgroup_code
         into lv_dummy
         from contractgroup cg
         where cg.contractgroup_code = emp_rec.contractgroup_code;
       exception
         when no_data_found then
           lv_errorline := lv_errorline || 'Contract group (' || emp_rec.contractgroup_code || ') not found. ';
       end;
       begin
         select la.language_code
         into lv_dummy
         from language la
         where la.language_code = emp_rec.language_code;
       exception
         when no_data_found then
           lv_errorline := lv_errorline || 'Language (' || emp_rec.language_code || ') not found. ';
       end;
       begin
         select d.department_code
         into lv_dummy
         from department d
         where d.plant_code = emp_rec.plant_code and d.department_code = emp_rec.department_code;
       exception
         when no_data_found then
           lv_errorline := lv_errorline || 'Department (' || emp_rec.department_code || ') not found. ';
       end;
       begin
         select t.team_code
         into lv_dummy
         from team t
         where t.plant_code = emp_rec.plant_code and t.team_code = emp_rec.team_code;
       exception
         when no_data_found then
           lv_errorline := lv_errorline || 'Team (' || emp_rec.team_code || ') not found. ';
       end;
       -- also check if department belong to team
       begin
         select dt.department_code
         into lv_dummy
         from departmentperteam dt 
         where dt.plant_code = emp_rec.plant_code and dt.team_code = emp_rec.team_code
               and dt.department_code = emp_rec.department_code;
       exception
         when no_data_found then
           lv_errorline := lv_errorline || 'Dept (' || emp_rec.department_code || ') has no link with Team (' || emp_rec.team_code || '). ';
       end;
       begin
         select to_char(s.shift_number)
         into lv_dummy
         from shift s
         where s.plant_code = emp_rec.plant_code and s.shift_number = emp_rec.shift_number;
       exception
         when no_data_found then
           lv_errorline := lv_errorline || 'Shift (' || emp_rec.shift_number || ') not found. ';
       end;
       if lv_errorline is not null then
         write_log(lv_errorline || ' For employee:' || to_char(emp_rec.employee_number), lv_computername, 3);
         return 1;
       else
         return 0;
       end if;
     end check_log_errors;
  begin
    InitSettings;
    begin
      lv_computername := 'employees_2_globe';
      lv_message := 'employees 2 globe';
      lv_error_count := 0;
      for x in (select * from  vc_i_empidcard) loop
      begin
        emp_rec.employee_number := x.nummer;
        emp_rec.short_name := x.shortname;
        emp_rec.description := x.name;
        emp_rec.sex := x.sex;
        emp_rec.contractgroup_code := x.contractgroup;
        emp_rec.language_code := x.language;
        emp_rec.team_code := x.team;
        emp_rec.plant_code := x.plant;
        emp_rec.department_code := x.department;
        emp_rec.shift_number := x.shift;
        emp_rec.startdate := x.empstart;
        emp_rec.enddate := x.empend;
        emp_rec.firstaid_yn := x.firstaid;
        emp_rec.is_scanning_yn := x.isscanning;
        emp_rec.calc_bonus_yn := x.efficencybonus;
        emp_rec.cut_of_time_yn := x.cutofftime;
        -- Needed for IDCard
        emp_rec.idnumber := x.idnumber;
        emp_rec.idemployee_number := x.nummer; -- Should be the one that is also used for employee
        emp_rec.idstartdate := x.dateactiv;
        emp_rec.idenddate := x.dateend;
          begin
            -- EMPLOYEES
            lv_employee_found := True;
            if check_log_errors = 0 then
            BEGIN
              begin
                select employee_number
                into lv_employee_number
                from employee
                where employee_number = emp_rec.employee_number;
              exception
                when no_data_found then
                  lv_employee_found := False;
              end;
              if lv_employee_found = False then
              begin
                INSERT INTO EMPLOYEE
                (
                  EMPLOYEE_NUMBER,
                  SHORT_NAME,
                  DESCRIPTION,
                  SEX,
                  CONTRACTGROUP_CODE,
                  LANGUAGE_CODE,
                  TEAM_CODE,
                  PLANT_CODE,
                  DEPARTMENT_CODE,
                  SHIFT_NUMBER,
                  STARTDATE,
                  ENDDATE,
                  FIRSTAID_YN,
                  IS_SCANNING_YN,
                  CALC_BONUS_YN,
                  CUT_OF_TIME_YN,
                  CREATIONDATE,
                  MUTATIONDATE,
                  MUTATOR
                )
                VALUES
                (
                  emp_rec.employee_number,
                  emp_rec.short_name,
                  emp_rec.description,
                  emp_rec.sex,
                  emp_rec.contractgroup_code,
                  emp_rec.language_code,
                  emp_rec.team_code,
                  emp_rec.plant_code,
                  emp_rec.department_code,
                  emp_rec.shift_number,
                  emp_rec.startdate,
                  emp_rec.enddate,
                  emp_rec.firstaid_yn,
                  emp_rec.is_scanning_yn,
                  emp_rec.calc_bonus_yn,
                  emp_rec.cut_of_time_yn,
                  sysdate,
                  sysdate,
                  'BROLLI'
                );
                commit;
                lv_ok := 1;
              EXCEPTION
                WHEN DUP_VAL_ON_INDEX THEN
                  NULL;
                WHEN OTHERS THEN
                  lv_error_count := lv_error_count + 1;
                  write_log(lv_message || sqlerrm, lv_computername, 3);
              END;
              else
                  -- First check if all fields are the same
                  lv_update_needed := False;
                  begin
                    select employee_number
                    into lv_employee_number
                    from employee
                    where EMPLOYEE_NUMBER = emp_rec.employee_number
                    and SHORT_NAME = emp_rec.short_name
                    and DESCRIPTION = emp_rec.description
                    and SEX = emp_rec.sex
                    and CONTRACTGROUP_CODE = emp_rec.contractgroup_code
                    and LANGUAGE_CODE = emp_rec.language_code
                    and TEAM_CODE = emp_rec.team_code
                    and PLANT_CODE = emp_rec.plant_code
                    and DEPARTMENT_CODE = emp_rec.department_code
                    and SHIFT_NUMBER = emp_rec.shift_number
                    and STARTDATE = emp_rec.startdate
                    and COALESCE(ENDDATE, trunc(sysdate)) = COALESCE(emp_rec.enddate, trunc(sysdate))
                    and FIRSTAID_YN = emp_rec.firstaid_yn
                    and IS_SCANNING_YN = emp_rec.is_scanning_yn
                    and CALC_BONUS_YN = emp_rec.calc_bonus_yn
                    and CUT_OF_TIME_YN = emp_rec.cut_of_time_yn;
                  exception
                    when no_data_found then
                      lv_update_needed := True;
                  end;
                  if lv_update_needed = True then
                  begin
                    UPDATE EMPLOYEE
                    SET
                      SHORT_NAME = emp_rec.short_name,
                      DESCRIPTION = emp_rec.description,
                      SEX = emp_rec.sex,
                      CONTRACTGROUP_CODE = emp_rec.contractgroup_code,
                      LANGUAGE_CODE = emp_rec.language_code,
                      TEAM_CODE = emp_rec.team_code,
                      PLANT_CODE = emp_rec.plant_code,
                      DEPARTMENT_CODE = emp_rec.department_code,
                      SHIFT_NUMBER = emp_rec.shift_number,
                      STARTDATE = emp_rec.startdate,
                      ENDDATE = emp_rec.enddate,
                      FIRSTAID_YN = emp_rec.firstaid_yn,
                      IS_SCANNING_YN = emp_rec.is_scanning_yn,
                      CALC_BONUS_YN = emp_rec.calc_bonus_yn,
                      CUT_OF_TIME_YN = emp_rec.cut_of_time_yn,
                      MUTATIONDATE = sysdate,
                      MUTATOR = 'BROLLI'
                    WHERE EMPLOYEE_NUMBER = emp_rec.employee_number;
                    commit;
                    lv_ok := 1;
                  EXCEPTION
                    WHEN OTHERS THEN
                      lv_error_count := lv_error_count + 1;
                      write_log(lv_message || sqlerrm, lv_computername, 3);
                  end;
                  end if;
              end if; -- if lv_employee_found = False then
            END;
              -- IDCard 
              begin
                lv_idcard_found := True;
                select idcard_number
                into lv_idcard_number
                from idcard
                where idcard_number = emp_rec.idnumber;
              exception
                when no_data_found then
                  lv_idcard_found := False;
              end;
              if lv_idcard_found = False then
              begin
                INSERT INTO IDCARD
                (
                  IDCARD_NUMBER,
                  EMPLOYEE_NUMBER,
                  STARTDATE,
                  ENDDATE,
                  CREATIONDATE,
                  MUTATIONDATE,
                  MUTATOR,
                  IDCARD_RAW
                )
                VALUES
                (
                  emp_rec.idnumber,
                  emp_rec.employee_number,
                  emp_rec.idstartdate,
                  emp_rec.idenddate,
                  sysdate,
                  sysdate,
                  'BROLLI',
                  NULL
                );
                commit;
                lv_ok := 1;
              EXCEPTION
                WHEN DUP_VAL_ON_INDEX THEN
                  NULL;
                WHEN OTHERS THEN
                  lv_error_count := lv_error_count + 1;
                  write_log(lv_message || sqlerrm, lv_computername, 3);
              end;
              else
                begin
                  -- First check if all fields are the same
                  lv_update_needed := False;
                  begin
                    select idcard_number
                    into lv_idcard_number
                    from idcard
                    where idcard_number = emp_rec.idnumber
                    and employee_number = emp_rec.employee_number
                    and startdate = emp_rec.idstartdate
                    and COALESCE(enddate, trunc(sysdate)) = COALESCE(emp_rec.idenddate, trunc(sysdate));
                  exception
                    when no_data_found then
                      lv_update_needed := True;
                  end;
                  if lv_update_needed = True then
                    begin
                      UPDATE IDCARD
                      SET EMPLOYEE_NUMBER = emp_rec.employee_number,
                      STARTDATE = emp_rec.idstartdate,
                      ENDDATE = emp_rec.idenddate,
                      MUTATIONDATE = sysdate,
                      MUTATOR = 'BROLLI'
                      WHERE IDCARD_NUMBER = emp_rec.idnumber;
                      commit;
                    exception
                      WHEN OTHERS THEN
                        lv_error_count := lv_error_count + 1;
                        write_log(lv_message || sqlerrm, lv_computername, 3);
                    end;
                    lv_ok := 1;
                  end if;
                end;
              end if;
            end if; -- if check_log_errors = 0 then
          EXCEPTION
          WHEN NO_DATA_FOUND THEN
            EXIT;
          END;
        END;
      end loop;
    exception
      when no_data_found then
        write_log(lv_message || sqlerrm, lv_computername, 3);
      when others then
        write_log(lv_message || sqlerrm, lv_computername, 3);
    end;
  end transfer_employees_ids_2_globe;

  procedure employees_ids_2_globe is
  begin
    InitSettings;
    transfer_employees_ids_2_globe;
    commit;
  end employees_ids_2_globe;

end PACKAGE_BROLLI;
/
