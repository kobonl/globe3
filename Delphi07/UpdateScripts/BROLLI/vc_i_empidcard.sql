create or replace view vc_i_empidcard as
select e."Nummer" as nummer, UPPER(e."ShortName") as shortname,
  e."Name" as name, UPPER(e."Sex") as sex, e."Contractgroup" as contractgroup,
  e."Language" as language, e."Team" as team, e."Plant" as plant,
  e."Department" as department, e."Shift" as shift,
  TO_DATE(e."EmpStart") as empstart, TO_DATE(e."EmpEnd") as empend,
  UPPER(e."FirstAid") as firstaid, UPPER(e."isscanning") as isscanning,
  UPPER(e."efficencybonus") as efficencybonus,
  UPPER(e."cutofftime") as cutofftime, e."IDNumber" as idnumber, e."Employee" as employee,
  TO_DATE(e."DateActiv") as dateactiv, TO_DATE(e."DateEnd") as dateend
from Mitarbeiterabgleich_Globe@gtlabbr e

