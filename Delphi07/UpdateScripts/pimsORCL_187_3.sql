--
-- GLOB3-411 - Add External Reference to Employee-dialog in PIMS
--

-- Add/modify columns 
declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'alter table EMPLOYEE add EXT_REF varchar2(20)';
    exception when column_exists then null;
end;
/


commit;
