--
-- GLOB3-202 Scan via machine-card readers and do not scan out
--

--
-- PIMSSETTING
--
-- Add/modify columns 
declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'alter table PIMSSETTING add TIMERECCR_DONOTSCANOUT_YN VARCHAR2(1) default ''N''';
    exception when column_exists then null;
end;
/

--
-- WORKSPOT
--
-- Add/modify columns 
declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'alter table WORKSPOT add ROAMING_YN VARCHAR2(1) default ''N''';
    exception when column_exists then null;
end;
/

commit;

