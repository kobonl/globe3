(*
  MRA:8-SEP-2016 PIM-213
  - New screens showing per workspot the efficiency and how the
    operator performs.
*)
unit WorkspotEmpEffDMT;

interface

uses
  SysUtils, Classes, ORASystemDMT, DB, OracleData, Variants, Oracle;

type
  TWorkspotEmpEffDM = class(TDataModule)
    odsWSEmp30MinEff: TOracleDataSet;
    odsWSEmp15MinEff: TOracleDataSet;
    odsWSEmp60MinEff: TOracleDataSet;
    odsWSEmpTodayMinEff: TOracleDataSet;
    odsWSQty: TOracleDataSet;
    oqCopyThis: TOracleQuery;
    odsCopyThis: TOracleDataSet;
    odsWSEmp1MinEff: TOracleDataSet;
  private
    { Private declarations }
  public
    { Public declarations }
    procedure WorkspotEmpEffRefresh;
    procedure WorkspotEmpEffGetValues(APlantCode, AWorkspotCode: String;
      AEmployeeNumber: Integer;
      var A15MinEff, A30MinEff, A60MinEff, ATodayEff: Integer);
    procedure WorkspotQtyGetValues(APlantCode, AWorkspotCode: String;
      var AWSQty: Integer);
    function FindCurrentEmp(APlantCode, AWorkspotCode: String;
      AEmployeeNumber: Integer): Boolean;
  end;

var
  WorkspotEmpEffDM: TWorkspotEmpEffDM;

implementation

{$R *.dfm}

{ TWorkspotEmpEffDM }

procedure TWorkspotEmpEffDM.WorkspotEmpEffRefresh;
begin
  with odsWSEmp1MinEff do
  begin
    Close;
    Open;
  end;
  with odsWSEmp15MinEff do
  begin
    Close;
    Open;
  end;
  with odsWSEmp30MinEff do
  begin
    Close;
    Open;
  end;
  with odsWSEmp60MinEff do
  begin
    Close;
    Open;
  end;
  with odsWSEmpTodayMinEff do
  begin
    Close;
    Open;
  end;
  with odsWSQty do
  begin
    Close;
    Open;
  end;
end; // WorkspotEmpEffRefresh

procedure TWorkspotEmpEffDM.WorkspotEmpEffGetValues(APlantCode,
  AWorkspotCode: String; AEmployeeNumber: Integer; var A15MinEff,
  A30MinEff, A60MinEff, ATodayEff: Integer);
begin
  A15MinEff := 0;
  A30MinEff := 0;
  A60MinEff := 0;
  ATodayEff := 0;
  if not odsWSEmp30MinEff.Active then
    Exit;
  with odsWSEmp15MinEff do
  begin
    if Locate('PLANT_CODE;WORKSPOT_CODE;EMPLOYEE_NUMBER',
      VarArrayOf([APlantCode, AWorkspotCode, AEmployeeNumber]), []) then
    begin
      A15MinEff := Round(FieldByName('EFF').AsFloat);
    end;
  end;
  with odsWSEmp30MinEff do
  begin
    if Locate('PLANT_CODE;WORKSPOT_CODE;EMPLOYEE_NUMBER',
      VarArrayOf([APlantCode, AWorkspotCode, AEmployeeNumber]), []) then
    begin
      A30MinEff := Round(FieldByName('EFF').AsFloat);
    end;
  end;
  with odsWSEmp60MinEff do
  begin
    if Locate('PLANT_CODE;WORKSPOT_CODE;EMPLOYEE_NUMBER',
      VarArrayOf([APlantCode, AWorkspotCode, AEmployeeNumber]), []) then
    begin
      A60MinEff := Round(FieldByName('EFF').AsFloat);
    end;
  end;
  with odsWSEmpTodayMinEff do
  begin
    if Locate('PLANT_CODE;WORKSPOT_CODE;EMPLOYEE_NUMBER',
      VarArrayOf([APlantCode, AWorkspotCode, AEmployeeNumber]), []) then
    begin
      ATodayEff := Round(FieldByName('EFF').AsFloat);
    end;
  end;
end; // WorkspotEmpEffGetValues

procedure TWorkspotEmpEffDM.WorkspotQtyGetValues(APlantCode,
  AWorkspotCode: String; var AWSQty: Integer);
begin
  AWSQty := 0;
  with odsWSQty do
  begin
    if Active then
      if Locate('PLANT_CODE;WORKSPOT_CODE',
        VarArrayOf([APlantCode, AWorkspotCode]), []) then
        AWSQty := Round(FieldByName('QTY').AsFloat);
  end;
end; // WorkspotQtyGetValues

function TWorkspotEmpEffDM.FindCurrentEmp(APlantCode,
  AWorkspotCode: String; AEmployeeNumber: Integer): Boolean;
begin
  Result := False;
  with odsWSEmp1MinEff do
  begin
    if Active then
      if Locate('PLANT_CODE;WORKSPOT_CODE;EMPLOYEE_NUMBER',
        VarArrayOf([APlantCode, AWorkspotCode, AEmployeeNumber]), []) then
         Result := True;
  end;
end; // FindCurrentEmp

end.
