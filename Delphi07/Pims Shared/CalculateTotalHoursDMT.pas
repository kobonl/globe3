(*
  Use this 'CalculateTotalHoursDMT'-file if you want to use procedures like:
  - procedure GetShiftDay
  - procedure ComputeBreaks
  - procedure EmployeeCutOfTime
  - procedure GetProdMin

  Also, if you want to use UGlobalFunctions-procedures like:
  - UnprocessProcessScans
  - ProcessTimeRecording

  Include 'CalculateTotalHoursDMT' in uses-part, in DMT-file of
  the part where you want to use it.

  If you want to refresh the data that's stored in the ClientDataSets,
  use the procedure 'AProdMinClass.Refresh'.

  Note: 'CalculateTotalHoursDMT' is (after SystemDMT) always created.
        This means also the Class-variables can always be used.

  Changes:
    RV005: (Revision 005). Oracle-10-fix.
    2.0.139.126 - MRA:13-MAR-2008. Oracle 10 test.
      - During testing for Oracle 10g 10.2.0.1 and error occured:
        ORA-24909 Call in progress. Current operation cancelled.
        This is a problem with the special Oracle DOA-components.
        (Direct Oracle Access).
        This can be solved as follows:
        - Set 'Optimize' to 'False' for TOracleQuery and TOracleDataSet-
          components. In most cases this can happen with
          Insert/Update/Delete-statements, but also with Select-statements.
       Note: In Pims-applications made in Delphi5 did error did not happen,
             but there the DOA-components are not used.
    RV013. (Revision 013). Round Minutes.
    2.0.139.12x  - Rounding minutes-procedure rewritten, because Except. Hours were not
                   included during rounding of the salary-hours.
    RV023. (Revision 023).
    MRA:09-MAR-2009 Round minutes fix. Salary hour and ProdHourPerEmpPerType
                    gave different results after rounding. This is fixed.
    RV039.1. MRA:21-OCT-2009.
      - Rounding problem. The rounding was done in such a way that
        the total minutes for 1 salary-date could be less then the orginal
        total of minutes, e.g. a difference of 30 minutes instead of 15 minutes
        at the most. A method must be used that ensures the total
        of minutes on 1 salary-date (that has multiple records for multiple
        hour-types) stays (almost) the same, so it is no more than 15 minutes
        difference, when rounding/truncating is set to 15 minutes.
    RV040.3. (D5-RV) MRA:4-NOV-2009.
      - Rounding problem. This gives wrong rounding for PCO. To solve this
        rounding-correction must only be done for 'truncate'.
  MRA:26-FEB-2010. RV055.1.
  - Procedure get round-parameters is changed, it first tries to get values
    from IDCard.
  MRA:14-JUL-2010. RV063.1.
  - Put back old method of rounding. New method (RV055.1) is wrong!
  MRA:3-NOV-2010. RV072.3.
  - Rounding after Book except.hours-routine.
    - Because of rounding after Book exc.hrs. routine,
      it is possible hours were found for exc.hrs. but
      they are removed during rounding.
      Example:
        Rounding is 'truncate 15 min.'.
        12 minutes was found, but after rounding this is 0.
      This means 12 minutes are lost, which can result in losing
      more minutes afterwards.
    - Solution: Solve this by returning the minutes that were left
      after rounding in Rounding-routine.
    - EXTRA: Only round for a given hourtype for 'lost minutes'!
  MRA:4-MAY-2011. RV076.1. Addition. SO-20011705
  - Determination of 'cutoff'-time is always based on
    first and last timeblock.
    Instead of that it should first look at planned timeblocks.
    If planning was found, then base first/last timeblock
    on the planned timeblocks.
    If no planning was found, then use the old method.
  - Examples:
    1) Planned timeblocks are 1,2 (of 4 timeblocks).
       This means the first timeblock should be 1 and last timeblock 2,
       instead of using 1 and 4.
    2) Planned timeblocks are 3,4 (of 4 timeblocks).
       This means the first timeblock should be 3 and last timeblock 4,
       instead of using 1 and 4.
  MRA:20-MAY-2011. RV076.1. SO-20011705.
  - Addition: Also look at standard-planning, employee-availability and
              standard-employee-availability.
  MRA:5-OCT-2012 20013489 Overnight Shift.
  - GetShiftDay: Return boolean to indicate it is about an overnight-shift,
    for later use. Store it in EmployeeData-record.
  - GetShiftDay: EmployeeData-record must be a VAR-variable!
  - GetShiftDay:
    - When DateIn is smaller then StartDateTime (Shiftdate) then
      the Shiftdate (StartDateTime) must be set to date of DateIn.
    - When shift-definition is a non-overnight shift (starttime < endtime
      of shift-day-definition), then SHIFT_DATE must be set
      to DATETIME_IN of scan.
  MRA:22-OCT-2012 20013489 Overnight Shift.
  - GetShiftDay:
    - Always store production hours on day the shift started.
  MRA:1-NOV-2012 20013489
  - OvernightShift-boolean-veriable is not needed.
  MRA:21-MAY-2013 20014050 Automated Export Easy Labor
  - Addition of DetermineShiftStartEndTB procedure
  MRA:10-NOV-2014 20014826
  - Log error messages optionally to database.
  MRA:23-SEP-2015 PIM-87
  - Related to this order
  - For DetermineShiftStartEnd + DetermineShiftStartEndTB:
    - When no timeblocks-per-xxx are defined then take the shift-start/end!
  ABS-19136 Hour Calculation Test
  - When rounding is set to truncate to 15 minutes it can give a wrong result
    when more than 1 hour-type is involved because it divides the missing minutes
    over other hour-types. This can give a result where overtime-time is
    15 minutes higher regular hours is 15 minutes lower then was expected,
    at the end it can give less regular hours then was expected.
  - To solve this it must use a different sequence for the hour-types.
  MRA:29-JUN-2016 PIM-181 Bugfix:
  - In procedure FindPlanningBlocks:
    - Query qryFindPlanningBlocks had to be changed to prevent an access
      violation when a timeblock is null, which resulted in an empty string.
  MRA:29-NOV-2016 PIM-242
  - Check Shift during In/Out Scan
  MRA:31-MAY-2017 PIM-300
  - Wrong overtime hours
  - When outscan-margin-late was used it gave less hours than expected when
    one of the last scans (not the last one), was resulting in 'too late'.
  - This had to do with scans that included seconds, the routines that checked
    for previous or next scan failed because of that. To solve it the
    scans must be rounded to minutes before comparing in a query.
    - odsPrevScan1, odsPrevScan2, odsNextScan1, odsNextScan2
      are changed because of this.
  MRA:10-NOV-2017 PIM-326
  - Determination of first-scan/last-scan goes wrong and can result
    in wrong hours. When there are scans with seconds then it can give
    problems.
  MRA:21-NOV-2017 PIM-326 Rework
  - When Request-Early/Late is used, be sure both are filled to prevent
    it does not use Margin-Early/Late any more.
  MRA:15-JAN-2018 PIM-348
  - Calculation of Early/Late Margins gives a problem
  - Problem has to do with PIM-326.
  - It should always determine the first/last scan to determine if the
    current scan should be rounded because of early/late in/out-margins!
  MRA:2-JUL-2018 GLOB3-60
  - Extension 4 to 10 blocks for planning
  - Also: Base early/late-margins on first/last found planning
    block (see: RV076.1.)
  MRA:2-JUL-2018 PIM-295
  - Inscan margins and planned time-blocks
  - When determining blocks, filter on planned timeblocks only, as was
    done for LTB (see IsLTB), this must always be done.
*)

unit CalculateTotalHoursDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, UPimsConst, Provider, DBClient,
  Oracle, OracleData, UScannedIDCard;

// MR:16-07-2003
// Record, Class and Methods for
// determining the Shift-Start+End Dates.
// TShiftDay - declarations - Start
type
  PTBookmarkRecord = ^TBookmarkRecord;
  TBookmarkRecord = record
    Bookmark: TBookmark;
    AddToSalList, AddToProdListOut, AddToProdListIn: Boolean;
  end;

type
  TShiftDayRecord = record
    ADate: TDateTime;
    AStart, AEnd: TDateTime;
    AValid: Boolean;
    APlantCode: String;
    AShiftNumber: Integer;
  end;

type
  TShiftDay = class
  private
    FEmployeeNumber: Integer;
    FScanStart: TDateTime;
    FScanEnd: TDateTime;
    FShiftNumber: Integer;
    FPlantCode: String;
    FDepartmentCode: String;
    FFromTimeRecordingApp: Boolean; // MRA:07-MAR-2008 RV004.
    procedure SetEmployeeNumber(const Value: Integer);
    procedure SetScanStart(const Value: TDateTime);
    procedure SetScanEnd(const Value: TDateTime);
    procedure SetPlantCode(const Value: String);
    procedure SetShiftNumber(const Value: Integer);
    procedure SetDepartmentCode(const Value: String);
  public
    Prev, Curr, Next: TShiftDayRecord;
    procedure DetermineShift(ADate: TDateTime;
      var AShiftDayRecord: TShiftDayRecord);
    procedure DetermineShiftStartEnd(
      var AShiftDayRecord: TShiftDayRecord);
    procedure DetermineShiftStartEndTB(
      var AShiftDayRecord: TShiftDayRecord;
      ATimeBlockNumber: Integer);
    procedure DetermineNewStartEnd(var NewScanStart, NewScanEnd: TDateTime);
    procedure GetShiftDay(var EmployeeData: TScannedIDCard;
      var StartDateTime, EndDateTime: TDateTime);
    property AEmployeeNumber: Integer read FEmployeeNumber
      write SetEmployeeNumber;
    property AScanStart: TDateTime read FScanStart write SetScanStart;
    property AScanEnd: TDateTime read FScanEnd write SetScanEnd;
    property APlantCode: String read FPlantCode write SetPlantCode;
    property AShiftNumber: Integer read FShiftNumber write SetShiftNumber;
    property ADepartmentCode: String read FDepartmentCode
      write SetDepartmentCode;
    property AFromTimeRecordingApp: Boolean read FFromTimeRecordingApp
      write FFromTimeRecordingApp;
  end;
// TShiftDay - declarations - End

//CAR 21-7-2003
// class- TTBLengthClass - contains functions for :
// determining:
// length of a TimeBlock
// timeblock valid

// TProdMin - for calculation of prod minute - used in few reports
// function of UGlobalFunction unit redefined
//- later will be removed from this unit

// Changes in pims:
// report: on Create/ Destroy message of datamodule new variable of class type
// defined below is created / free
// forms: OnShow/OnClose message - new variable of Class type defined
// is created/ destroy

type
  TTBMinPerWeek =  Array[1..MAX_TBS,1..7] of Integer;
  TTBNumber = Array[1..MAX_TBS] of Integer;
//
  TTBLengthClass = class
  private
    FTBStart,
    FTBEnd,
    FBreak: TTBMinPerWeek;
    FTBValid: TTBNumber;

    FEmployeeNumber: Integer;
    FShiftNumber: Integer;
    FPlantCode: String;
    FDepartmentCode: String;
    FRemoveNotPaidBreaks: Boolean;

    procedure SetEmployeeNumber(const Value: Integer);
    procedure SetPlantCode(const Value: String);
    procedure SetShiftNumber(const Value: Integer);
    procedure SetDepartmentCode(const Value: String);
    procedure SetRemoveNotPaidBreaks(const Value: Boolean);

    function GetMinutes(DateTimeMinutes: TDateTime): Integer;
    procedure FillTBValues(TmpClientDataSet: TClientDataSet;
      FillMinutes: Boolean; var Index: integer);
    procedure GetTimeBreaks(BreaksStart, BreaksEnd: TDateTime;
      Index, IndexDay: Integer; var BreakTime: Integer);

    procedure FillTBArray_ClientDataSet(ClientDataSetTmp_TBEmpl,
      ClientDataSetTmp_TBDept, ClientDataSetTmp_TBShift: TClientDataSet;
      FillMinutes, SetEmptyArray: Boolean);
    procedure FillBreaks_ClientDataSet(
      ClientDataSetTmp_BEmpl, ClientDataSetTmp_BDept,
      ClientDataSetTmp_BShift: TClientDataSet);
    procedure OpenDataTBLength;
    procedure CloseDataTBLength;
  public
    constructor Create;
    procedure Free;

    property AEmployeeNumber: Integer read FEmployeeNumber
      write SetEmployeeNumber;
    property APlantCode: String read FPlantCode write SetPlantCode;
    property AShiftNumber: Integer read FShiftNumber write SetShiftNumber;
    property ADepartmentCode: String read FDepartmentCode
      write SetDepartmentCode;
    property ARemoveNotPaidBreaks: Boolean read FRemoveNotPaidBreaks
      write SetRemoveNotPaidBreaks;

    procedure FillTBArray(Employee, Shift: Integer; Plant, Department: String);
    procedure FillBreaks(Employee, Shift: Integer;
      Plant, Department: String; RemoveNotPaidBreaks: Boolean);
    procedure FillTBLength(Employee, Shift: Integer;
      Plant, Department: String; RemoveNotPaidBreaks: Boolean);

    procedure ValidTB(Employee, Shift: Integer; Plant, Department: String);

    function GetLengthTB(TB_Number, Day_Number: Integer): Integer;
    function ExistTB(TB_Number: Integer): Boolean;
    function GetStartTime(TB_Number, Day_Number: Integer): TDateTime;
    function GetEndTime(TB_Number, Day_Number: Integer): TDateTime;
    function GetValidTB(TB_Number: Integer): Integer;
  end;

  TProdMinClass = class
  private
    FEmployeeData: TScannedIDCard;
    FStartTime, FEndTime: TDateTime;
    FOperationMode: Integer;
    FContractRoundMinutes: Boolean;
    procedure SetEmployeeData(const Value: TScannedIDCard);
    procedure SetOperationMode(const Value: Integer);
    procedure SetStartTime(const Value: TDateTime);
    procedure SetEndTime(const Value: TDateTime);
    procedure ComputeBreaks_ClientDataSet(
      ClientDataSetBEmpl_Tmp, ClientDataSetBDept_Tmp,
      ClientDataSetBShift_Tmp: TClientDataSet;
      var ProdMin, BreaksMin, PayedBreaks: Integer);
    procedure GetShiftWStartEnd_ClientDataSet
      (ClientDataSetProdTBEmpl_Tmp, ClientDataSetProdTBDept_Tmp,
       ClientDataSetProdTBShift_Tmp, ClientDataSetShift_Tmp: TClientDataSet;
       var WeekStartDateTime, WeekEndDateTime: TWeekDates;
       var Ready: Boolean);
    procedure EmployeeCutOfTime_Query(
      FirstScan, LastScan: Boolean; var EmployeeData: TScannedIDCard;
      DeleteAction: Boolean; DeleteEmployeeData: TScannedIDCard;
      var StartDTInterval, EndDTInterval: TDateTime);
    procedure GetShiftWStartEnd(EmployeeData: TScannedIDCard;
      var WeekStartDateTime, WeekEndDateTime: TWeekDates;
      var Ready: Boolean);
    procedure OpenDataProdMin;
    procedure CloseDataProdMin;
    procedure SetContractRoundMinutes(const Value: Boolean);
  public
    constructor Create;
    procedure Free;
    property AEmployeeData: TScannedIDCard read FEmployeeData
      write SetEmployeeData;
    property AOperationMode: Integer read FOperationMode
      write SetOperationMode;
    property AStartTime: TDateTime read FStartTime
      write SetStartTime;
    property AEndTime: TDateTime read FEndTime
      write SetEndTime;
    property AContractRoundMinutes: Boolean read FContractRoundMinutes
      write SetContractRoundMinutes;
    procedure GetShiftDay(
      var EmployeeData: TScannedIDCard;
      var StartDateTime, EndDateTime: TDateTime);
    procedure GetShiftStartEnd(EmployeeData: TScannedIDCard;
      ShiftDate: TDateTime;
      var StartDateTime, EndDateTime: TDateTime);
    procedure ComputeBreaks(
      EmployeeData: TScannedIDCard;
      STime, ETime: TDateTime; OperationMode: Integer;
      var ProdMin, BreaksMin, PayedBreaks: Integer);
    procedure EmployeeCutOfTime(
      FirstScan, LastScan: Boolean; var EmployeeData: TScannedIDCard;
      DeleteAction: Boolean; DeleteEmployeeData: TScannedIDCard;
      var StartDTInterval, EndDTInterval:TDateTime);
    procedure GetProdMin(var EmployeeData: TScannedIDCard;
      FirstScan, LastScan: Boolean;
      DeleteAction: Boolean; DeleteEmployeeData: TScannedIDCard;
      var BookingDay: TDateTime;
      var Minutes, BreaksMin, PayedBreaks: integer);
    procedure ReadContractRoundMinutesParams(
      EmployeeData: TScannedIDCard;
      var RoundTruncSalaryHours, RoundMinute: Integer); // RV063.1.
    procedure Refresh;
    function FindPlanningBlocks(EmployeeData: TScannedIDCard;
      var FirstPlanTimeblock, LastPlanTimeblock: Integer): Boolean;
  end;

  TCalculateTotalHoursDM = class(TDataModule)
    odsSelector: TOracleDataSet;
    odsPrevScan1: TOracleDataSet;
    ClientDataSetBShift: TClientDataSet;
    ClientDataSetBDept: TClientDataSet;
    ClientDataSetBEmpl: TClientDataSet;
    DataSetProviderBEmpl: TDataSetProvider;
    DataSetProviderBDept: TDataSetProvider;
    DataSetProviderBShift: TDataSetProvider;
    ClientDataSetShift: TClientDataSet;
    DataSetProviderShift: TDataSetProvider;
    ClientDataSetTBShift: TClientDataSet;
    ClientDataSetTBDept: TClientDataSet;
    ClientDataSetTBEmpl: TClientDataSet;
    DataSetProviderTBEmpl: TDataSetProvider;
    DataSetProviderTBDept: TDataSetProvider;
    DataSetProviderTBShift: TDataSetProvider;
    odsNextScan1: TOracleDataSet;
    oqShiftSchedule: TOracleQuery;
    oqEmployeeShift: TOracleQuery;
    oqRequest: TOracleQuery;
    oqContractgroup: TOracleQuery;
    odsSHE: TOracleDataSet;
    oqFindPlanningBlocks: TOracleQuery;
    oqWorkspotDept: TOracleQuery;
    odsPrevScan2: TOracleDataSet;
    odsNextScan2: TOracleDataSet;
    oqIsFirstScan: TOracleQuery;
    oqIsLastScan: TOracleQuery;
    oqFirstScan: TOracleQuery;
    oqLastScan: TOracleQuery;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  public
    { Public declarations }
    // procedures for filling the client data set used in the new classes
    procedure OpenClientDataSet(ClientDataSetTmp: TClientDataSet;
      DataSetProviderTmp: TDataSetProvider;
      odsTmp: TOracleDataSet; SelectStr: String);
    procedure OpenTBEmpl(ClientDataSetTmp: TClientDataSet;
      DataSetProviderTmp: TDataSetProvider; odsTmp: TOracleDataSet);
    procedure OpenTBDept(ClientDataSetTmp: TClientDataSet;
      DataSetProviderTmp: TDataSetProvider; odsTmp: TOracleDataSet);
    procedure OpenTBShift(ClientDataSetTmp: TClientDataSet;
      DataSetProviderTmp: TDataSetProvider; odsTmp: TOracleDataSet);
    procedure OpenBreaksEmpl(ClientDataSetTmp: TClientDataSet;
      DataSetProviderTmp: TDataSetProvider; odsTmp: TOracleDataSet);
    procedure OpenBreaksDept(ClientDataSetTmp: TClientDataSet;
      DataSetProviderTmp: TDataSetProvider; odsTmp: TOracleDataSet);
    procedure OpenBreaksShift(ClientDataSetTmp: TClientDataSet;
      DataSetProviderTmp: TDataSetProvider; odsTmp: TOracleDataSet);
    procedure OpenShift(ClientDataSetTmp: TClientDataSet;
      DataSetProviderTmp: TDataSetProvider; odsTmp: TOracleDataSet);
    function DecodeHrsMin(HrsMin: Integer):String;
    function FillZero(IntVal: Integer): String;
    // RV072.3.
    function ContractRoundMinutes(
      AEmployeeIDCard: TScannedIDCard; ABookingDay: TDateTime;
      AHourTypeNumber: Integer=-1): Integer;
    function DetermineWorkspotDept(APlantCode, AWorkspotCode: String): String;
  end;

var
  CalculateTotalHoursDM: TCalculateTotalHoursDM;
  AShiftDay: TShiftDay;
  AProdMinClass: TProdMinClass;
  ATBLengthClass: TTBLengthClass;
{$IFDEF DEBUG3}
  DebugLine: String;
  DebugOn: Boolean;
{$ENDIF}

implementation

{$R *.DFM}

uses
  ORASystemDMT, UGlobalFunctions, GlobalDMT;


// MR:16-07-2003
// Record, Class and Methods for
// determining the Shift-Start+End Dates.
// TShiftDay - Declarations - Start
procedure TShiftDay.DetermineShift(
  ADate: TDateTime;
  var AShiftDayRecord: TShiftDayRecord);
{var
  MyShiftScheduleDate: TDateTime; }
begin
  // Init the values
  AShiftDayRecord.APlantCode := APlantCode;
  AShiftDayRecord.AShiftNumber := AShiftNumber;
  AShiftDayRecord.ADate := ADate;

  // PIM-242 Always skip this part to prevent it takes the wrong shift!
(*
  // MRA:07-MAR-2008 Only do this when called from Timerecording!
  if AFromTimeRecordingApp then
  begin
    CalculateTotalHoursDM.oqShiftSchedule.ClearVariables;
    CalculateTotalHoursDM.oqShiftSchedule.
      SetVariable('EMPLOYEE_NUMBER',     AEmployeeNumber);
    // MR:08-03-2005 Order 550387 Trunc added.
    MyShiftScheduleDate := Trunc(ADate);
    CalculateTotalHoursDM.oqShiftSchedule.
      SetVariable('SHIFT_SCHEDULE_DATE', MyShiftScheduleDate);
    CalculateTotalHoursDM.oqShiftSchedule.Execute;
    if not CalculateTotalHoursDM.oqShiftSchedule.Eof then
    begin
      AShiftDayRecord.APlantCode :=
        CalculateTotalHoursDM.oqShiftSchedule.FieldAsString('PLANT_CODE');
      AShiftDayRecord.AShiftNumber :=
        CalculateTotalHoursDM.oqShiftSchedule.FieldAsInteger('SHIFT_NUMBER');
    end
    else
    begin
      // MR:08-03-2005 Order 550387
      CalculateTotalHoursDM.oqEmployeeShift.ClearVariables;
      CalculateTotalHoursDM.oqEmployeeShift.
        SetVariable('EMPLOYEE_NUMBER', AEmployeeNumber);
      CalculateTotalHoursDM.oqEmployeeShift.Execute;
      if not CalculateTotalHoursDM.oqEmployeeShift.Eof then
      begin
        AShiftDayRecord.APlantCode :=
          CalculateTotalHoursDM.oqEmployeeShift.FieldAsString('PLANT_CODE');
        AShiftDayRecord.AShiftNumber :=
          CalculateTotalHoursDM.oqEmployeeShift.FieldAsInteger('SHIFT_NUMBER');
      end;
    end;
  end;
*)
end;

procedure TShiftDay.DetermineShiftStartEnd(
  var AShiftDayRecord: TShiftDayRecord);
var
  Day: Integer;
  SDay: String;
  Ready: Boolean;
  ADataSet: TDataSet;
begin
  Day  := DayInWeek(ORASystemDM.WeekStartsOn, AShiftDayRecord.ADate);
  SDay := IntToStr(Day);

  // Init the start and end
  AShiftDayRecord.AStart := Trunc(AShiftDayRecord.ADate);
  AShiftDayRecord.AEnd := Trunc(AShiftDayRecord.ADate);
  AShiftDayRecord.AValid := False;

  // Timeblocks Per Employee
  ADataSet := CalculateTotalHoursDM.ClientDataSetTBEmpl;
  ADataSet.Filtered := False;
  ADataSet.Filter :=
    'PLANT_CODE = ' + '''' + DoubleQuote(AShiftDayRecord.APlantCode) + '''' +
    ' AND ' +
    'EMPLOYEE_NUMBER = ' + IntToStr(AEmployeeNumber) + ' AND ' +
    'SHIFT_NUMBER = ' + IntToStr(AShiftDayRecord.AShiftNumber);
  ADataSet.Filtered := True;
  Ready := not ADataSet.IsEmpty;
{$IFDEF DEBUG3}
  if Ready then
    if DebugLine = '' then
      DebugLine := '-- TB-EMP found.';
{$ENDIF}

  // Timeblocks Per Department
  if not Ready then
  begin
    // Timeblocks per Department
    ADataSet := CalculateTotalHoursDM.ClientDataSetTBDept;
    ADataSet.Filtered := False;
    ADataSet.Filter :=
      'PLANT_CODE = ' + '''' + DoubleQuote(AShiftDayRecord.APlantCode) +
      '''' + ' AND ' +
      'DEPARTMENT_CODE = ''' + DoubleQuote(ADepartmentCode) +
      '''' + ' AND ' +
      'SHIFT_NUMBER = ' + IntToStr(AShiftDayRecord.AShiftNumber);
    ADataSet.Filtered := True;
    Ready := not ADataSet.IsEmpty;
{$IFDEF DEBUG3}
  if Ready then
    if DebugLine = '' then
      DebugLine := '-- TB-DEPT found.';
{$ENDIF}

    // Timeblocks Per Shift
    if not Ready then
    begin
      // Timeblocks Per Shift
      ADataSet := CalculateTotalHoursDM.ClientDataSetTBShift;
      ADataSet.Filtered := False;
      ADataSet.Filter :=
        'PLANT_CODE = ' + '''' + DoubleQuote(AShiftDayRecord.APlantCode) +
        '''' + ' AND ' +
        'SHIFT_NUMBER = ' + IntToStr(AShiftDayRecord.AShiftNumber);
      ADataSet.Filtered := True;
      Ready := not ADataSet.IsEmpty;
{$IFDEF DEBUG3}
  if Ready then
    if DebugLine = '' then
      DebugLine := '-- TB-SHIFT found.';
{$ENDIF}
      // PIM-87 No timeblocks-per-xxx defined? Then look at the Shift
      if not Ready then
      begin
        ADataSet := CalculateTotalHoursDM.ClientDataSetShift;
        ADataSet.Filtered := False;
        ADataSet.Filter :=
          'PLANT_CODE = ' + '''' + DoubleQuote(AShiftDayRecord.APlantCode) +
          '''' + ' AND ' +
          'SHIFT_NUMBER = ' + IntToStr(AShiftDayRecord.AShiftNumber);
        ADataSet.Filtered := True;
        Ready := not ADataSet.IsEmpty;
{$IFDEF DEBUG3}
  if Ready then
    if DebugLine = '' then
      DebugLine := '-- SHIFT found.';
{$ENDIF}
      end;
    end;
  end;

  if Ready then
  begin
    // Search and Set a valid STARTTIME
    // Start looking for FIRST valid timeblock to LAST.
    ADataSet.First;
    while not ADataSet.Eof do
    begin
      // If times are not the same, it's a valid time
      // otherwise they are not valid, like
      // Starttime='00:00' and endtime='00:00'
      if Frac(ADataSet.FieldByName('STARTTIME' + SDay).AsDateTime) <>
        Frac(ADataSet.FieldByName('ENDTIME' + SDay).AsDateTime) then
      begin
        AShiftDayRecord.AStart :=
          Trunc(AShiftDayRecord.ADate) +
          Frac(ADataSet.FieldByName('STARTTIME' + SDay).AsDateTime);
        AShiftDayRecord.AValid := True;
        Break;
      end;
      ADataSet.Next;
    end;
    // Search and Set a valid ENDDTIME
    // Start looking from LAST valid timeblock to FIRST.
    ADataSet.Last;
    while not ADataSet.Bof do
    begin
      // If times are not the same, it's a valid time
      // otherwise they are not valid, like
      // Starttime='00:00' and endtime='00:00'
      if Frac(ADataSet.FieldByName('STARTTIME' + SDay).AsDateTime) <>
        Frac(ADataSet.FieldByName('ENDTIME' + SDay).AsDateTime) then
      begin
        AShiftDayRecord.AEnd :=
          Trunc(AShiftDayRecord.ADate) +
          Frac(ADataSet.FieldByName('ENDTIME' + SDay).AsDateTime);
        begin
          // MR: 22-02-2005 Order 550384
          // If shift is overnight then end-day is next day.
          if AShiftDayRecord.AStart > AShiftDayRecord.AEnd then
          begin
            AShiftDayRecord.AEnd := AShiftDayRecord.AEnd + 1;
          end;
          AShiftDayRecord.AValid := True;
          Break;
        end;
      end;
      ADataSet.Prior;
    end;
  end;
end; // DetermineShiftStartEnd

procedure TShiftDay.DetermineNewStartEnd(
  var NewScanStart, NewScanEnd: TDateTime);
var
  Ready: Boolean;
  SaveScanEnd: TDateTime;
begin
  Ready := False;

  NewScanStart := Curr.AStart;
  NewScanEnd := Curr.AEnd;

  // MR:08-01-2004 If there was an open-scan, 'ScanEnd' will be a nulldate,
  //               in that case set it to 'ScanStart'
  SaveScanEnd := AScanEnd;
  if AScanEnd = NullDate then
    AScanEnd := AScanStart;

  if (Prev.AValid)
     and
     //Car 15-10-2003
     (
     //
      (
       (Curr.AValid) and
       (AScanEnd < Curr.AStart) and
       (
         (AScanStart <= Prev.AEnd)
         or
         ((AScanStart - Prev.AEnd) < (Curr.AStart - AScanEnd))
       )
     )
     or
     (
       (not Curr.AValid) and
       (
         (not Next.AValid)
         or
         (
           (AScanStart <= Prev.AEnd) or
           (
             (AScanStart > Prev.AEnd) and
             (AScanEnd < Next.AStart) and
             ((AScanStart - Prev.AEnd) < (Next.AStart - AScanEnd))
           )
         )
       )
     )
    )then
  begin
    NewScanStart := Prev.AStart;
    NewScanEnd := Prev.AEnd;
    Ready := True;
  end;

  if not Ready then
  begin
    if (Next.AValid)
       and
      (
       (
         (Curr.AValid) and
         (AScanStart > Curr.AEnd) and
         (
           (AScanEnd >= Next.AStart)
           or
           ((Next.AStart - AScanEnd) < (AScanStart - Curr.AEnd))
         )
       )
       or
       (
         (not Curr.AValid) and
         (
           (not Prev.AValid)
           or
           (
             (AScanEnd >= Next.AStart) or
             (
               (AScanStart > Prev.AEnd) and
               (AScanEnd < Next.AStart) and
               ((AScanStart - Prev.AEnd) >= (Next.AStart - AScanEnd))
             )
           )
         )
       )
      )then
    begin
      NewScanStart := Next.AStart;
      NewScanEnd := Next.AEnd;
    end;
  end;
  AScanEnd := SaveScanEnd;
end; // DetermineNewStartEnd

// 20013489 EmployeeData must be a VAR-variable!
procedure TShiftDay.GetShiftDay(
  var EmployeeData: TScannedIDCard; var StartDateTime, EndDateTime: TDateTime);
var
  PeriodStartDateTime, PeriodEndDateTime: TDateTime;
begin
  AEmployeeNumber := EmployeeData.EmployeeCode;
  AScanStart := EmployeeData.DateIn;
  AScanEnd := EmployeeData.DateOut;
  APlantCode := EmployeeData.PlantCode;
  AShiftNumber := EmployeeData.ShiftNumber;
  ADepartmentCode := EmployeeData.DepartmentCode;

  // MR:07-MAR-2008 Use this to determine from what application
  // this is called. Only if it is from TimeRecording-application
  // this will be 'true'.
  AFromTimeRecordingApp := EmployeeData.FromTimeRecordingApp;

{$IFDEF DEBUG3}
  DebugLine := '';
  DebugOn := True;
{$ENDIF}

  // MR:01-10-2003
  // If called from HoursPerEmployee and option 'Absence' or 'Salary' then
  // Shift must be '-1'.
  if AShiftNumber <> -1 then
  begin
{$IFDEF DEBUG3}
  WDebugLog('- DetermineShiftStartEnd');
{$ENDIF}
    DetermineShift(AScanStart, Curr);
{$IFDEF DEBUG3}
    DebugOn := False;
{$ENDIF}
    DetermineShift(AScanStart + 1, Next);
    DetermineShift(AScanStart - 1, Prev);

    DetermineShiftStartEnd(Curr);
    DetermineShiftStartEnd(Next);
    DetermineShiftStartEnd(Prev);

    DetermineNewStartEnd(StartDateTime, EndDateTime);


    // 20013489
    // When DateIn is smaller then StartDateTime (Shiftdate) then
    // the Shiftdate (StartDateTime) must be set to date of DateIn.
    if ORASystemDM.UseShiftDateSystem then // 20014327
    begin
      if Trunc(EmployeeData.DateIn) < Trunc(StartDateTime) then
      begin
        StartDateTime := Trunc(EmployeeData.DateIn) + Frac(StartDateTime);
      end;
    end
    else
    // TD-23416 Exception!
    // If dateIn is higher than period, then
    // only look at current. This prevents the salary hours are not booked
    // at all.
    begin
      // Check if the calculated period is the same as the period that is
      // processed. If not then the hours must be booked on the same day of
      // the datein, to prevent these are are lost!
      PeriodStartDateTime := StartDateTime;
      PeriodEndDateTime := EndDateTime;
      GlobalDM.ComputeOvertimePeriod(EmployeeData,
        PeriodStartDateTime, PeriodEndDateTime);

      // Are we in the wrong period?
      // Is DateIn higher then period that should be calculated?
      // Only look at date-part.
      if (Trunc(EmployeeData.DateIn) > Trunc(PeriodEndDateTime)) then
      begin
{$IFDEF DEBUG3}
  WDebugLog('-- GetShiftDay - Exception! Only take current day.' +
    ' Emp=' + IntToStr(EmployeeData.EmployeeCode) +
    ' ' + DateTimeToStr(EmployeeData.DateIn) + ' > ' +
    DateTimeToStr(PeriodEndDateTime) +
    ' Period=' + DateToStr(PeriodStartDateTime) + ' to ' +
    DateTimeToStr(PeriodEndDateTime)
    );
{$ENDIF}
        Curr.AValid := True;
        Prev.AValid := False;
        Next.AValid := False;
        DetermineNewStartEnd(StartDateTime, EndDateTime);
      end;
    end; // TD-23416
  end
  else
  begin
    StartDateTime := AScanStart;
    EndDateTime := AScanEnd;
  end;
end; // GetShiftDay

procedure TShiftDay.SetEmployeeNumber(const Value: Integer);
begin
  FEmployeeNumber := Value;
end;

procedure TShiftDay.SetDepartmentCode(const Value: String);
begin
  FDepartmentCode := Value;
end;

procedure TShiftDay.SetScanStart(const Value: TDateTime);
begin
  FScanStart := Value;
end;

procedure TShiftDay.SetScanEnd(const Value: TDateTime);
begin
  FScanEnd := Value;
end;

procedure TShiftDay.SetPlantCode(const Value: String);
begin
  FPlantCode := Value;
end;

procedure TShiftDay.SetShiftNumber(const Value: Integer);
begin
  FShiftNumber := Value;
end;
// TShiftDay - Declarations - End

function TCalculateTotalHoursDM.FillZero(IntVal: Integer): String;
begin
  Result := IntToStr(IntVal);
  if (IntVal = 0) then
     Result := '00';
  if (length(IntToStr(IntVal)) = 1) then
    Result := '0' + IntToStr(IntVal);
end;

function TCalculateTotalHoursDM.DecodeHrsMin(HrsMin: Integer):String;
var
  Hrs, Min: Integer;
begin
  if HrsMin < 0 then
    Result := '-'
  else
    Result := '';
  HrsMin := Abs(HrsMin);
  Hrs := HrsMin div 60;
  Min := HrsMin mod 60;
  Result := Result +  FillZero(Hrs) + ':' +  FillZero(Min);
  if Length(Result)= 6 then
    Result := ' ' + Result;
  if Length(Result)= 5 then
    Result := '  ' + Result;
end;

procedure TCalculateTotalHoursDM.OpenClientDataSet(
  ClientDataSetTmp: TClientDataSet;
  DataSetProviderTmp: TDataSetProvider;
  odsTmp: TOracleDataSet;
  SelectStr: String);
begin
  if ClientDataSetTmp.Active then
    Exit;
  odsTmp.Close;
  odsTmp.Sql.Clear;
  odsTmp.Sql.Add(SelectStr);
  DataSetProviderTmp.DataSet := odsTmp;
  ClientDataSetTmp.Open;
end;

// fill timeblocks per employee
// client data set will "locate" a record based on default Index created by: ORDER BY
// it is not necessary to create this Index on each Client Data Set ...

procedure TCalculateTotalHoursDM.OpenTBEmpl(ClientDataSetTmp: TClientDataSet;
  DataSetProviderTmp: TDataSetProvider; odsTmp: TOracleDataSet);
var
  SelectStr: String;
  Index: Integer;
begin
  SelectStr :=
    'SELECT EMPLOYEE_NUMBER, PLANT_CODE, SHIFT_NUMBER, TIMEBLOCK_NUMBER ';
  for Index := 1 to 7 do
    SelectStr := SelectStr + ', STARTTIME' + IntToStr(Index) +
      ', ENDTIME' + IntToStr(Index);
  SelectStr := SelectStr + ' FROM TIMEBLOCKPEREMPLOYEE ' +
    ' ORDER BY PLANT_CODE, EMPLOYEE_NUMBER, SHIFT_NUMBER, TIMEBLOCK_NUMBER ';
  OpenClientDataSet(ClientDataSetTmp, DataSetProviderTmp, odsTmp, SelectStr);
end;

// fill timeblocks per department
procedure TCalculateTotalHoursDM.OpenTBDept(ClientDataSetTmp: TClientDataSet;
  DataSetProviderTmp: TDataSetProvider; odsTmp: TOracleDataSet);
var
  SelectStr: String;
  Index: Integer;
begin
  SelectStr := 'SELECT PLANT_CODE, DEPARTMENT_CODE, SHIFT_NUMBER, ' +
    ' TIMEBLOCK_NUMBER ';
  for Index := 1 to 7 do
    SelectStr := SelectStr + ', STARTTIME' + IntToStr(Index) +
      ', ENDTIME' + IntToStr(Index);
  SelectStr := SelectStr + ' FROM TIMEBLOCKPERDEPARTMENT ' +
    ' ORDER BY PLANT_CODE, DEPARTMENT_CODE, SHIFT_NUMBER, TIMEBLOCK_NUMBER ';
  OpenClientDataSet(ClientDataSetTmp, DataSetProviderTmp, odsTmp, SelectStr);
end;

procedure TCalculateTotalHoursDM.OpenTBShift(ClientDataSetTmp: TClientDataSet;
  DataSetProviderTmp: TDataSetProvider; odsTmp: TOracleDataSet);
var
  SelectStr: String;
  Index: Integer;
begin
  SelectStr := 'SELECT PLANT_CODE, SHIFT_NUMBER, TIMEBLOCK_NUMBER ';
  for Index := 1 to 7 do
    SelectStr := SelectStr + ', STARTTIME' + IntToStr(Index) +
      ', ENDTIME' + IntToStr(Index);
  SelectStr := SelectStr + ' FROM TIMEBLOCKPERSHIFT ' +
    ' ORDER BY PLANT_CODE, SHIFT_NUMBER, TIMEBLOCK_NUMBER ';
  OpenClientDataSet(ClientDataSetTmp, DataSetProviderTmp, odsTmp, SelectStr);
end;

procedure TCalculateTotalHoursDM.OpenBreaksEmpl(
  ClientDataSetTmp: TClientDataSet;
  DataSetProviderTmp: TDataSetProvider; odsTmp: TOracleDataSet);
var
  SelectStr: String;
  Index: Integer;
begin
  SelectStr :=
    'SELECT PLANT_CODE, EMPLOYEE_NUMBER, SHIFT_NUMBER, BREAK_NUMBER, ' +
    ' PAYED_YN ';
  for Index := 1 to 7 do
    SelectStr := SelectStr + ',STARTTIME' + IntToStr(Index) +
      ', ENDTIME' + IntToStr(Index);
  SelectStr := SelectStr + ' FROM BREAKPEREMPLOYEE ' +
    ' ORDER BY PLANT_CODE, EMPLOYEE_NUMBER, SHIFT_NUMBER, BREAK_NUMBER ';
  OpenClientDataSet(ClientDataSetTmp, DataSetProviderTmp, odsTmp, SelectStr);
end;

procedure TCalculateTotalHoursDM.OpenBreaksDept(ClientDataSetTmp: TClientDataSet;
  DataSetProviderTmp: TDataSetProvider; odsTmp: TOracleDataSet);
var
  SelectStr: String;
  Index: Integer;
begin
  SelectStr :=
    'SELECT PLANT_CODE, DEPARTMENT_CODE, SHIFT_NUMBER, BREAK_NUMBER, ' +
    ' PAYED_YN ';
  for Index := 1 to 7 do
    SelectStr := SelectStr + ', STARTTIME' + IntToStr(Index)+
      ', ENDTIME' + IntToStr(Index);
  SelectStr := SelectStr + ' FROM BREAKPERDEPARTMENT ' +
    ' ORDER BY PLANT_CODE, DEPARTMENT_CODE, SHIFT_NUMBER, BREAK_NUMBER';
  OpenClientDataSet(ClientDataSetTmp, DataSetProviderTmp, odsTmp, SelectStr);
end;

procedure TCalculateTotalHoursDM.OpenBreaksShift(
  ClientDataSetTmp: TClientDataSet;
  DataSetProviderTmp: TDataSetProvider; odsTmp: TOracleDataSet);
var
  SelectStr: String;
  Index: Integer;
begin
  SelectStr := 'SELECT PLANT_CODE, SHIFT_NUMBER, BREAK_NUMBER, PAYED_YN ' ;
  for Index := 1 to 7 do
    SelectStr := SelectStr + ', STARTTIME' + IntToStr(Index) +
       ', ENDTIME' + IntToStr(Index);
  SelectStr := SelectStr + ' FROM BREAKPERSHIFT' +
    ' ORDER BY PLANT_CODE, SHIFT_NUMBER, BREAK_NUMBER' ;
  OpenClientDataSet(ClientDataSetTmp, DataSetProviderTmp, odsTmp, SelectStr);
end;

procedure TCalculateTotalHoursDM.OpenShift(ClientDataSetTmp: TClientDataSet;
  DataSetProviderTmp: TDataSetProvider; odsTmp: TOracleDataSet);
var
  SelectStr: String;
  Index: Integer;
begin
  SelectStr := 'SELECT PLANT_CODE, SHIFT_NUMBER' ;
  for Index := 1 to 7 do
    SelectStr := SelectStr + ', STARTTIME' + IntToStr(Index) +
       ', ENDTIME' + IntToStr(Index);
  SelectStr := SelectStr + ' FROM SHIFT ORDER BY PLANT_CODE, SHIFT_NUMBER ';
  OpenClientDataSet(ClientDataSetTmp, DataSetProviderTmp, odsTmp, SelectStr);
end;

// start functions of class TTBLengthClass
procedure TTBLengthClass.SetEmployeeNumber(const Value: Integer);
begin
  FEmployeeNumber := Value;
end;

procedure TTBLengthClass.SetDepartmentCode(const Value: String);
begin
  FDepartmentCode := Value;
end;

procedure TTBLengthClass.SetRemoveNotPaidBreaks(const Value: Boolean);
begin
  FRemoveNotPaidBreaks := Value;
end;

procedure TTBLengthClass.SetPlantCode(const Value: String);
begin
  FPlantCode := Value;
end;

procedure TTBLengthClass.SetShiftNumber(const Value: Integer);
begin
  FShiftNumber := Value;
end;

constructor TTBLengthClass.Create;
begin
  inherited Create;
  OpenDataTBLength;
end;

procedure TTBLengthClass.Free;
begin
  CloseDataTBLength;
  inherited Free;
end;

procedure TTBLengthClass.OpenDataTBLength;
begin
  with CalculateTotalHoursDM do
  begin
    OpenTBEmpl(ClientDataSetTBEmpl, DataSetProviderTBEmpl, odsSelector);
    OpenTBDept(ClientDataSetTBDept, DataSetProviderTBDept, odsSelector);
    OpenTBShift(ClientDataSetTBShift, DataSetProviderTBShift, odsSelector);
    OpenBreaksEmpl(ClientDataSetBEmpl, DataSetProviderBEmpl, odsSelector);
    OpenBreaksDept(ClientDataSetBDept, DataSetProviderBDept, odsSelector);
    OpenBreaksShift(ClientDataSetBShift, DataSetProviderBShift, odsSelector);
  end;
end;

procedure TTBLengthClass.CloseDataTBLength;
begin
  with CalculateTotalHoursDM do
  begin
    ClientDataSetTBEmpl.Close;
    ClientDataSetTBDept.Close;
    ClientDataSetTBShift.Close;
    ClientDataSetBEmpl.Close;
    ClientDataSetBDept.Close;
    ClientDataSetBShift.Close;
  end;
end;


function TTBLengthClass.GetMinutes(DateTimeMinutes: TDateTime): Integer;
var
  Hour, Min, Sec, MSec: Word;
begin
  DecodeTime(DateTimeMinutes, Hour, Min, Sec, MSec);
  Result := Hour * 60 + Min;
end;

procedure TTBLengthClass.FillTBValues(TmpClientDataSet: TClientDataSet;
 FillMinutes: Boolean; var Index: integer);
var
  IndexDay: Integer;
begin
  Index := Index + 1;
  FTBValid[Index] := Index;
  if FillMinutes then
    for IndexDay := 1 to 7 do
    begin
      FTBStart[Index, IndexDay] :=
        GetMinutes(TmpClientDataSet.FieldByName('STARTTIME' +
          IntToStr(IndexDay)).AsDateTime);
      FTBEnd[Index, IndexDay] :=
        GetMinutes(TmpClientDataSet.FieldByName('ENDTIME' +
          IntToStr(IndexDay)).AsDateTime);
      if FTBStart[Index, IndexDay] > FTBEnd[Index, IndexDay] then
        FTBEnd[Index, IndexDay] := FTBEnd[Index, IndexDay] + 24 * 60;
    end;
  TmpClientDataSet.Next;
end;

procedure TTBLengthClass.FillTBArray_ClientDataSet(ClientDataSetTmp_TBEmpl,
  ClientDataSetTmp_TBDept, ClientDataSetTmp_TBShift: TClientDataSet;
  FillMinutes, SetEmptyArray: Boolean);
var
  IndexTB, IndexDay: Integer;
begin
  for IndexTB := 1 to ORASystemDM.MaxTimeblocks do
  begin
    FTBValid[IndexTB] := 0;
    if SetEmptyArray then
      for IndexDay := 1 to 7 do
      begin
        FTBStart[IndexTB,IndexDay] := 0;
        FTBEnd[IndexTB,IndexDay] := 0;
        FBreak[IndexTB,IndexDay] := 0;
      end;
  end;

  IndexTB := 0;
  if (AEmployeeNumber <> -1) then
  begin
    ClientDataSetTmp_TBEmpl.Filtered := False;
    ClientDataSetTmp_TBEmpl.Filter :=
      'PLANT_CODE = ' + '''' + DoubleQuote(APlantCode) + '''' + ' AND ' +
      'EMPLOYEE_NUMBER = ' + IntToStr(AEmployeeNumber) + ' AND ' +
      'SHIFT_NUMBER = ' + IntToStr(AShiftNumber);
    ClientDataSetTmp_TBEmpl.Filtered := True;
    ClientDataSetTmp_TBEmpl.First;
    while not ClientDataSetTmp_TBEmpl.Eof do
    //!! it is inside the FillTB function : "next" function on clientdataset
      FillTBValues(ClientDataSetTmp_TBEmpl, FillMinutes, IndexTB);
    if IndexTB <> 0 then
      Exit;
  end;
  ClientDataSetTmp_TBDept.Filtered := False;
  ClientDataSetTmp_TBDept.Filter :=
    'PLANT_CODE = ' + '''' + DoubleQuote(APlantCode) + '''' + ' AND ' +
    'DEPARTMENT_CODE = ''' + DoubleQuote(ADepartmentCode) + ''' AND ' +
    'SHIFT_NUMBER = ' + IntToStr(AShiftNumber);
  ClientDataSetTmp_TBDept.Filtered := True;
  ClientDataSetTmp_TBDept.First;
  while not ClientDataSetTmp_TBDept.Eof do
      FillTBValues(ClientDataSetTmp_TBDept, FillMinutes, IndexTB);
  if IndexTB <> 0 then
    Exit;

  ClientDataSetTmp_TBShift.Filtered := False;
  ClientDataSetTmp_TBShift.Filter :=
    'PLANT_CODE = ' + '''' + DoubleQuote(APlantCode) + '''' + ' AND ' +
    'SHIFT_NUMBER = ' + IntToStr(AShiftNumber);
  ClientDataSetTmp_TBShift.Filtered := True;
  ClientDataSetTmp_TBShift.First;

  while not ClientDataSetTmp_TBShift.Eof do
    FillTBValues(ClientDataSetTmp_TBShift, FillMinutes, IndexTB);
end;

procedure TTBLengthClass.ValidTB(Employee, Shift: Integer;
  Plant, Department: String);
begin
// set properties
  AEmployeeNumber := Employee;
  AShiftNumber := Shift;
  APlantCode:= Plant;
  ADepartmentCode := Department;

  FillTBArray_ClientDataSet(CalculateTotalHoursDM.ClientDataSetTBEmpl,
    CalculateTotalHoursDM.ClientDataSetTBDept,
    CalculateTotalHoursDM.ClientDataSetTBShift, False, False);
end;

procedure TTBLengthClass.FillTBArray(Employee, Shift: Integer;
  Plant, Department: String);
begin
  AEmployeeNumber := Employee;
  AShiftNumber := Shift;
  APlantCode:= Plant;
  ADepartmentCode := Department;

  FillTBArray_ClientDataSet(CalculateTotalHoursDM.ClientDataSetTBEmpl,
    CalculateTotalHoursDM.ClientDataSetTBDept,
    CalculateTotalHoursDM.ClientDataSetTBShift, True, True);
end;

procedure TTBLengthClass.GetTimeBreaks(BreaksStart, BreaksEnd: TDateTime;
  Index, IndexDay: Integer; var BreakTime: Integer);
var
  BreaksStartMin, BreaksEndMin: Integer;
begin
  BreakTime := 0;
  BreaksStartMin := GetMinutes(BreaksStart);
  BreaksEndMin := GetMinutes(BreaksEnd);
  if BreaksStart > BreaksEnd then
    BreaksEndMin := GetMinutes(BreaksEnd) + 24 * 60;
  if (BreaksEndMin < FTBStart[Index, IndexDay]) or
    (BreaksStartMin > FTBEnd[Index, IndexDay]) then
    Exit;
  BreakTime := Min(BreaksEndMin,FTBEnd[Index, IndexDay]) -
    Max(BreaksStartMin,FTBStart[Index, IndexDay]);
end;

procedure TTBLengthClass.FillBreaks(Employee, Shift: Integer;
  Plant, Department: String; RemoveNotPaidBreaks: Boolean);
begin
  AEmployeeNumber := Employee;
  APlantCode := Plant;
  AShiftNumber := Shift;
  ADepartmentCode := Department;
  ARemoveNotPaidBreaks := RemoveNotPaidBreaks;

  FillBreaks_ClientDataSet(CalculateTotalHoursDM.ClientDataSetBEmpl,
    CalculateTotalHoursDM.ClientDataSetBDept,
    CalculateTotalHoursDM.ClientDataSetBShift);
end;

procedure TTBLengthClass.FillBreaks_ClientDataSet(
  ClientDataSetTmp_BEmpl, ClientDataSetTmp_BDept,
  ClientDataSetTmp_BShift: TClientDataSet);
var
  IndexTB, IndexDay: Integer;
  BreakTime: Integer;
  FillBreaks: Boolean;
begin
  FillBreaks := False;
  for IndexTB := 1 to ORASystemDM.MaxTimeblocks do
    for IndexDay := 1 to 7 do
       FBreak[IndexTB,IndexDay] := 0;
  if AEmployeeNumber <> -1 then
  begin
    ClientDataSetTmp_BEmpl.Filtered := False;
    ClientDataSetTmp_BEmpl.Filter :=
      'PLANT_CODE = ' + '''' + DoubleQuote(APlantCode) + '''' + ' AND ' +
      'EMPLOYEE_NUMBER = ' + IntToStr(AEmployeeNumber) + ' AND ' +
      'SHIFT_NUMBER = ' + IntToStr(AShiftNumber);
    ClientDataSetTmp_BEmpl.Filtered := True;
    ClientDataSetTmp_BEmpl.First;
    while not ClientDataSetTmp_BEmpl.Eof do
    begin
      if ((ClientDataSetTmp_BEmpl.FieldByName('PAYED_YN').AsString =
        UNCHECKEDVALUE) and ARemoveNotPaidBreaks) or
        (not ARemoveNotPaidBreaks) then
      begin
        for IndexTB := 1 to ORASystemDM.MaxTimeblocks do
          for IndexDay := 1 to 7 do
          begin
            GetTimeBreaks(
              ClientDataSetTmp_BEmpl.FieldByName('STARTTIME' +
                IntToStr(IndexDay)).AsDateTime,
              ClientDataSetTmp_BEmpl.FieldByName('ENDTIME' +
                IntToStr(IndexDay)).AsDateTime,
              IndexTB, IndexDay, BreakTime);
            FBreak[IndexTB, IndexDay] := FBreak[IndexTB, IndexDay] + BreakTime;
            FillBreaks := True;
          end;
      end;
      ClientDataSetTmp_BEmpl.Next;
    end;// while
  end;
  if FillBreaks then
    Exit;
  ClientDataSetTmp_BDept.Filtered := False;
  ClientDataSetTmp_BDept.Filter :=
    'PLANT_CODE = ' + '''' + DoubleQuote(APlantCode) + '''' + ' AND ' +
    'DEPARTMENT_CODE = ''' + DoubleQuote(ADepartmentCode) + ''' AND ' +
    'SHIFT_NUMBER = ' + IntToStr(AShiftNumber);
  ClientDataSetTmp_BDept.Filtered := True;
  ClientDataSetTmp_BDept.First;
  while not ClientDataSetTmp_BDept.Eof do
  begin
    if ((ClientDataSetTmp_BDept.FieldByName('PAYED_YN').AsString =
      UNCHECKEDVALUE) and (ARemoveNotPaidBreaks)) or
      (not ARemoveNotPaidBreaks) then
      for IndexTB := 1 to ORASystemDM.MaxTimeblocks do
        for IndexDay := 1 to 7 do
        begin
          GetTimeBreaks(
            ClientDataSetTmp_BDept.FieldByName('STARTTIME' +
              IntToStr(IndexDay)).AsDateTime,
            ClientDataSetTmp_BDept.FieldByName('ENDTIME' +
              IntToStr(IndexDay)).AsDateTime,
            IndexTB, IndexDay, BreakTime);
          FBreak[IndexTB, IndexDay] := FBreak[IndexTB, IndexDay] + BreakTime;
          FillBreaks := True;
        end;
     ClientDataSetTmp_BDept.Next;
  end; // while
  if FillBreaks then
    Exit;
  ClientDataSetTmp_BShift.Filtered := False;
  ClientDataSetTmp_BShift.Filter :=
    'PLANT_CODE = ' + '''' + DoubleQuote(APlantCode) + '''' + ' AND ' +
    'SHIFT_NUMBER = ' + IntToStr(AShiftNumber);
  ClientDataSetTmp_BShift.Filtered := True;
  ClientDataSetTmp_BShift.First;
  while not ClientDataSetTmp_BShift.Eof do
  begin
    if ((ClientDataSetTmp_BShift.FieldByName('PAYED_YN').AsString =
      UNCHECKEDVALUE) and (ARemoveNotPaidBreaks)) or
      (not ARemoveNotPaidBreaks) then
       for IndexTB := 1 to ORASystemDM.MaxTimeblocks do
         for IndexDay := 1 to 7 do
         begin
           GetTimeBreaks(
             ClientDataSetTmp_BShift.FieldByName('STARTTIME' +
               IntToStr(IndexDay)).AsDateTime,
             ClientDataSetTmp_BShift.FieldByName('ENDTIME' +
               IntToStr(IndexDay)).AsDateTime,
             IndexTB, IndexDay, BreakTime);
           FBreak[IndexTB, IndexDay] := FBreak[IndexTB, IndexDay] + BreakTime;
         end;
    ClientDataSetTmp_BShift.Next;
  end; // while
end;

procedure TTBLengthClass.FillTBLength(Employee, Shift: Integer;
  Plant, Department: String; RemoveNotPaidBreaks: Boolean);
begin
  FillTBArray(Employee, Shift, Plant, Department);
  FillBreaks(Employee, Shift, Plant, Department, RemoveNotPaidBreaks);
end;

function TTBLengthClass.GetValidTB(TB_Number: Integer): Integer;
begin
  Result := FTBValid[TB_Number];
end;

function TTBLengthClass.ExistTB(TB_Number: Integer): Boolean;
begin
  Result := (FTBValid[TB_Number] <> 0);
end;

function TTBLengthClass.GetLengthTB(TB_Number, Day_Number: Integer): Integer;
begin
  Result := FTBEnd[TB_Number, Day_Number] - FTBStart[TB_Number, Day_Number] -
    FBreak[TB_Number, Day_Number];
end;

function TTBLengthClass.GetStartTime(TB_Number,
  Day_Number: Integer): TDateTime;
begin
  Result := EncodeTime(FTBStart[TB_Number, Day_Number] div 60,
    FTBStart[TB_Number, Day_Number] mod 60, 0, 0);
end;

function TTBLengthClass.GetEndTime(TB_Number,
  Day_Number: Integer): TDateTime;
begin
  Result := EncodeTime(FTBEnd[TB_Number, Day_Number] div 60,
    FTBEnd[TB_Number, Day_Number] mod 60, 0, 0);
end;
// end function of TTBLength class

// start function of call TProdMinClass
constructor TProdMinClass.Create;
begin
  inherited Create;
  OpenDataProdMin;
end;

procedure TProdMinClass.Free;
begin
  CloseDataProdMin;
  inherited Free;
end;

procedure TProdMinClass.OpenDataProdMin;
begin
  with CalculateTotalHoursDM do
  begin
    OpenTBEmpl(ClientDataSetTBEmpl, DataSetProviderTBEmpl, odsSelector);
    OpenTBDept(ClientDataSetTBDept, DataSetProviderTBDept, odsSelector);
    OpenTBShift(ClientDataSetTBShift, DataSetProviderTBShift, odsSelector);
    OpenBreaksEmpl(ClientDataSetBEmpl, DataSetProviderBEmpl, odsSelector);
    OpenBreaksDept(ClientDataSetBDept, DataSetProviderBDept, odsSelector);
    OpenBreaksShift(ClientDataSetBShift, DataSetProviderBShift, odsSelector);
    OpenShift(ClientDataSetShift, DataSetProviderShift, odsSelector);
    // prepare for speed reason queries used in function EmployeeCutOfTime
//    odsRequest.Prepare;
//    odsPrevScans.Prepare;
//    odsNextScans.Prepare;
  end;
end;

procedure TProdMinClass.CloseDataProdMin;
begin
  with CalculateTotalHoursDM do
  begin
    ClientDataSetTBEmpl.Close;
    ClientDataSetTBDept.Close;
    ClientDataSetTBShift.Close;
    ClientDataSetShift.Close;
    ClientDataSetBEmpl.Close;
    ClientDataSetBDept.Close;
    ClientDataSetBShift.Close;
    odsPrevScan1.Close;
    odsNextScan1.Close;
    odsPrevScan2.Close;
    odsNextScan2.Close;
    oqContractGroup.Close;
    oqRequest.Close;
    oqShiftSchedule.Close;
    oqEmployeeShift.Close;
  end;
end;

// Reread all data
procedure TProdMinClass.Refresh;
begin
  CloseDataProdMin;
  OpenDataProdMin;
end;

procedure TProdMinClass.SetEmployeeData(const Value: TScannedIDCard);
begin
  FEmployeeData := Value;
end;

procedure TProdMinClass.SetOperationMode(const Value: Integer);
begin
  FOperationMode := Value;
end;

procedure TProdMinClass.SetStartTime(const Value: TDateTime);
begin
  FStartTime := Value;
end;

procedure TProdMinClass.SetContractRoundMinutes(const Value: Boolean);
begin
  FContractRoundMinutes := Value;
end;

procedure TProdMinClass.SetEndTime(const Value: TDateTime);
begin
  FEndTime := Value;
end;

  // MR:23-1-2004 Get some fields from Contractgroup-table for given employee
procedure TProdMinClass.ReadContractRoundMinutesParams(
  EmployeeData: TScannedIDCard;
  var RoundTruncSalaryHours, RoundMinute: Integer); // RV063.1.
begin
//  RoundTruncSalaryHours := -1;
//  RoundMinute := -1;
  // RV055.1. First get values from IDCard, if they are 0 then get them from query.
  RoundTruncSalaryHours := EmployeeData.RoundTruncSalaryHours;
  RoundMinute := EmployeeData.RoundMinute;
  if (RoundTruncSalaryHours = 0) and (RoundMinute = 0) then
  begin
    with CalculateTotalHoursDM do
    begin
      oqContractGroup.ClearVariables;
      oqContractGroup.SetVariable('EMPLOYEE_NUMBER', EmployeeData.EmployeeCode);
      oqContractGroup.Execute;
      if not oqContractGroup.Eof then
      begin
        RoundTruncSalaryHours :=
          oqContractGroup.FieldAsInteger('ROUND_TRUNC_SALARY_HOURS');
        RoundMinute :=
          oqContractGroup.FieldAsInteger('ROUND_MINUTE');
      end;
    end;
  end;
end;

// store all records of Time block per Empl/Time block perDept/ Time block PerShift
// SHIFT tables into a separate list - only first and last TimeBlock is necessary
// these list are used in GetShiftStartDate function
procedure TProdMinClass.ComputeBreaks(
  EmployeeData: TScannedIDCard;
  STime, ETime: TDateTime; OperationMode: Integer;
  var ProdMin, BreaksMin, PayedBreaks: Integer);
begin
  AEmployeeData := EmployeeData;
  AStartTime := STime;
  AEndTime := ETime;
  AOperationMode := OperationMode;
{$IFDEF DEBUG3}
  WDebugLog('ComputeBreaks:' +
    ' E=' + IntToStr(AEmployeeData.EmployeeCode) +
    ' W=' + AEmployeeData.WorkSpotCode +
    ' D=' + AEmployeeData.DepartmentCode +
    ' S=' + IntToStr(AEmployeeData.ShiftNumber)
      );
{$ENDIF}
  ComputeBreaks_ClientDataSet(CalculateTotalHoursDM.ClientDataSetBEmpl,
    CalculateTotalHoursDM.ClientDataSetBDept,
    CalculateTotalHoursDM.ClientDataSetBShift,
    ProdMin, BreaksMin, PayedBreaks);
end;

procedure TProdMinClass.ComputeBreaks_ClientDataSet(
  ClientDataSetBEmpl_Tmp, ClientDataSetBDept_Tmp,
  ClientDataSetBShift_Tmp: TClientDataSet;
  var ProdMin, BreaksMin, PayedBreaks: Integer);
var
  WeekDay: Integer;
  Ready: Boolean;
  StartTime, EndTime : TDateTime;
  function SearchTimeBlock(DataSet: TDataSet;
    AStartTime, AEndTime: TDateTime; AOperationMode: Integer;
    StartTime, EndTime: TDateTime; WeekDay: Integer;
    var ProdMin, BreaksMin, PayedBreaks: Integer): Boolean;
  var
    StartDateTime, EndDateTime: TDateTime;
    MinOfBreak: Integer;
  begin
    Result := False;
    while not DataSet.Eof  do
    begin
      StartDateTime := DataSet.
        FieldByName('STARTTIME' + IntToStr(WeekDay)).AsDateTime;
      EndDateTime := DataSet.
        FieldByName('ENDTIME' + IntToStr(WeekDay)).AsDateTime;
      ReplaceTime(StartTime, StartDateTime);
      ReplaceTime(EndTime, EndDateTime);
      if StartDateTime > EndDateTime then
      	 EndTime := EndTime + 1; //ends in next day
      MinOfBreak :=
        ComputeMinutesOfIntersection(AStartTime, AEndTime, StartTime, EndTime);
      BreaksMin := BreaksMin + MinOfBreak;
      if DataSet.FieldByName('PAYED_YN').AsString = CHECKEDVALUE then
        PayedBreaks := PayedBreaks + MinOfBreak;
      if AOperationMode = 1 then
        EndTime := EndTime + MinOfBreak / DayToMin;
      DataSet.Next;
      Result := True;
    end;
  end;
begin
  WeekDay := DayInWeek(ORASystemDM.WeekStartsOn, AStartTime);
  ProdMin := 0;
  BreaksMin := 0;
  PayedBreaks := 0;
  StartTime := AStartTime;
  EndTime := AStartTime;

  // Breaks per Employee
  ClientDataSetBEmpl_Tmp.Filtered := False;
  ClientDataSetBEmpl_Tmp.Filter :=
    'PLANT_CODE = ' + '''' +
      DoubleQuote(AEmployeeData.PlantCode) + '''' + ' AND ' +
    'EMPLOYEE_NUMBER = ' + IntToStr(AEmployeeData.EmployeeCode) + ' AND ' +
    'SHIFT_NUMBER = ' + IntToStr(AEmployeeData.ShiftNumber);
  ClientDataSetBEmpl_Tmp.Filtered := True;
  ClientDataSetBEmpl_Tmp.First;

  Ready := SearchTimeBlock(ClientDataSetBEmpl_Tmp,
    AStartTime, AEndTime, AOperationMode,
    StartTime, EndTime, WeekDay, ProdMin, BreaksMin, PayedBreaks);

{$IFDEF DEBUG3}
  if Ready then
    WDebugLog('- GetBreaks-Emp found.');
{$ENDIF}

  // Breaks per Department
  if not Ready then
  begin
    ClientDataSetBDept_Tmp.Filtered := False;
    ClientDataSetBDept_Tmp.Filter :=
      'PLANT_CODE = ' + '''' +
        DoubleQuote(AEmployeeData.PlantCode) + '''' + ' AND ' +
      'DEPARTMENT_CODE = ''' +
        DoubleQuote(AEmployeeData.DepartmentCode) + '''' + ' AND ' +
      'SHIFT_NUMBER = ' + IntToStr(AEmployeeData.ShiftNumber);
    ClientDataSetBDept_Tmp.Filtered := True;
    ClientDataSetBDept_Tmp.First;

    Ready := SearchTimeBlock(ClientDataSetBDept_Tmp,
      AStartTime, AEndTime, AOperationMode,
      StartTime, EndTime, WeekDay, ProdMin, BreaksMin, PayedBreaks);
{$IFDEF DEBUG3}
  if Ready then
    WDebugLog('- GetBreaks-Dept found.');
{$ENDIF}
  end;

  // Breaks per Shift
  if not Ready then
  begin
    ClientDataSetBShift_Tmp.Filtered := False;
    ClientDataSetBShift_Tmp.Filter :=
      'PLANT_CODE = ' + '''' +
        DoubleQuote(AEmployeeData.PlantCode) + '''' + ' AND ' +
      'SHIFT_NUMBER = ' + IntToStr(AEmployeeData.ShiftNumber);
    ClientDataSetBShift_Tmp.Filtered := True;
    ClientDataSetBShift_Tmp.First;

{$IFDEF DEBUG3}
  Ready :=
{$ENDIF}
    SearchTimeBlock(ClientDataSetBShift_Tmp,
      AStartTime, AEndTime, AOperationMode,
      StartTime, EndTime, WeekDay, ProdMin, BreaksMin, PayedBreaks);
{$IFDEF DEBUG3}
  if Ready then
    WDebugLog('- GetBreaks-Shift found.');
{$ENDIF}
  end;

{$IFDEF DEBUG3}
  WDebugLog('StartTime=' + DateTimeToStr(AStartTime) +
    ' EndTime=' + DateTimeToStr(AEndTime) +
    ' BreaksMin=' + IntToStr(BreaksMin)
    );
{$ENDIF}
  ProdMin := Round((AEndTime - AStartTime) * DayToMin - BreaksMin);
end;

// - read the information from the new lists created instead of queries
procedure TProdMinClass.GetShiftStartEnd(
  EmployeeData: TScannedIDCard;
  ShiftDate: TDateTime;
  var StartDateTime, EndDateTime: TDateTime);
var
  Current_Day : Integer;
  WeekStartDateTime, WeekEndDateTime: TWeekDates;
  Ready: Boolean;
begin
  Ready := False;
  StartDateTime := EmployeeData.DateIn;
  EndDateTime := EmployeeData.DateIn;
  Current_Day:= DayInWeek(ORASystemDM.WeekStartsOn, ShiftDate);

  GetShiftWStartEnd(EmployeeData, WeekStartDateTime, WeekEndDateTime, Ready);
  if Ready then
  begin
    StartDateTime := WeekStartDateTime[Current_Day];
    EndDateTime := WeekEndDateTime[Current_Day];
  end;
  ReplaceDate(StartDateTime, ShiftDate);
  ReplaceDate(EndDateTime, ShiftDate);
end;

procedure TProdMinClass.GetShiftWStartEnd(
  EmployeeData: TScannedIDCard;
  var WeekStartDateTime, WeekEndDateTime: TWeekDates;
  var Ready: Boolean);
begin
  AEmployeeData := EmployeeData;

  GetShiftWStartEnd_ClientDataSet(CalculateTotalHoursDM.ClientDataSetTBEmpl,
    CalculateTotalHoursDM.ClientDataSetTBDept,
    CalculateTotalHoursDM.ClientDataSetTBShift,
    CalculateTotalHoursDM.ClientDataSetShift,
    WeekStartDateTime, WeekEndDateTime,
    Ready);
end;

procedure TProdMinClass.GetShiftWStartEnd_ClientDataSet(
  ClientDataSetProdTBEmpl_Tmp, ClientDataSetProdTBDept_Tmp,
  ClientDataSetProdTBShift_Tmp, ClientDataSetShift_Tmp: TClientDataSet;
  var WeekStartDateTime, WeekEndDateTime: TWeekDates;
  var Ready: Boolean);
var
  Index: Integer;
  FirstPlanTimeblock, LastPlanTimeblock: Integer;
  PlanningBlocksFilter: String;
begin
  Ready := False;

  // RV076.1.
  PlanningBlocksFilter := '';
//  if ORASystemDM.IsLTB then // PIM-295 Do this always!
    if FindPlanningBlocks(AEmployeeData, FirstPlanTimeblock,
      LastPlanTimeblock) then
      PlanningBlocksFilter := ' AND ' +
        '(' +
        '  TIMEBLOCK_NUMBER = ' + IntToStr(FirstPlanTimeBlock) +
        '  OR TIMEBLOCK_NUMBER = ' + IntToStr(LastPlanTimeblock) +
        ')';

{$IFDEF DEBUG3}
  if ORASystemDM.IsLTB then
  begin
    if PlanningBlocksFilter <> '' then
      WDebugLog('Planning/Avail. found: ' + PlanningBlocksFilter)
    else
      WDebugLog('No planning/avail. found.');
  end;
{$ENDIF}

  ClientDataSetProdTBEmpl_Tmp.Filtered := False;
  ClientDataSetProdTBEmpl_Tmp.Filter :=
    'PLANT_CODE = ' + '''' + DoubleQuote(AEmployeeData.PlantCode) +
    '''' + ' AND ' +
    'EMPLOYEE_NUMBER = ' + IntToStr(AEmployeeData.EmployeeCode) + ' AND ' +
    'SHIFT_NUMBER = ' + IntToStr(AEmployeeData.ShiftNumber) +
    PlanningBlocksFilter;
  ClientDataSetProdTBEmpl_Tmp.Filtered := True;
  if not ClientDataSetProdTBEmpl_Tmp.IsEmpty then
  begin
    ClientDataSetProdTBEmpl_Tmp.First;
    for Index := 1 to 7 do
      WeekStartDateTime[Index] := ClientDataSetProdTBEmpl_Tmp.
        FieldByName('STARTTIME' + IntToStr(Index)).AsDateTime;
    ClientDataSetProdTBEmpl_Tmp.Last;
    for Index := 1 to 7 do
      WeekEndDateTime[Index] := ClientDataSetProdTBEmpl_Tmp.
        FieldByName('ENDTIME' + IntToStr(Index)).AsDateTime;
    Ready := True;
  end;

  if not Ready then
  begin
    ClientDataSetProdTBDept_Tmp.Filtered := False;
    ClientDataSetProdTBDept_Tmp.Filter :=
      'PLANT_CODE = ' + '''' + DoubleQuote(AEmployeeData.PlantCode) +
      '''' + ' AND ' +
      'DEPARTMENT_CODE = ' + '''' + DoubleQuote(AEmployeeData.DepartmentCode) +
      '''' + ' AND ' +
      'SHIFT_NUMBER = ' + IntToStr(AEmployeeData.ShiftNumber) +
      PlanningBlocksFilter;
    ClientDataSetProdTBDept_Tmp.Filtered := True;
    if not ClientDataSetProdTBDept_Tmp.IsEmpty  then
    begin
      ClientDataSetProdTBDept_Tmp.First;
      for Index := 1 to 7 do
        WeekStartDateTime[Index] := ClientDataSetProdTBDept_Tmp.
          FieldByName('STARTTIME' + IntToStr(Index)).AsDateTime;
      ClientDataSetProdTBDept_Tmp.Last;
      for Index := 1 to 7 do
        WeekEndDateTime[Index] := ClientDataSetProdTBDept_Tmp.
          FieldByName('ENDTIME' + IntToStr(Index)).AsDateTime;
      Ready := True;
    end;
  end;

  if not Ready then
  begin
    ClientDataSetProdTBShift_Tmp.Filtered := False;
    ClientDataSetProdTBShift_Tmp.Filter :=
      'PLANT_CODE = ' + '''' + DoubleQuote(AEmployeeData.PlantCode) +
      '''' + ' AND ' +
      'SHIFT_NUMBER = ' + IntToStr(AEmployeeData.ShiftNumber) +
      PlanningBlocksFilter;
    ClientDataSetProdTBShift_Tmp.Filtered := True;
    if not ClientDataSetProdTBShift_Tmp.IsEmpty then
    begin
      ClientDataSetProdTBShift_Tmp.First;
      for Index := 1 to 7 do
        WeekStartDateTime[Index] :=
          ClientDataSetProdTBShift_Tmp.
            FieldByName('STARTTIME' + IntToStr(Index)).AsDateTime;
      ClientDataSetProdTBShift_Tmp.Last;
      for Index := 1 to 7 do
        WeekEndDateTime[Index] :=
          ClientDataSetProdTBShift_Tmp.
            FieldByName('ENDTIME' + IntToStr(Index)).AsDateTime;
      Ready := True;
    end;
  end;

  if not Ready then
  begin
    ClientDataSetShift_Tmp.Filtered := False;
    ClientDataSetShift_Tmp.Filter :=
      'PLANT_CODE = ' + '''' + DoubleQuote(AEmployeeData.PlantCode) +
      '''' + ' AND ' +
      'SHIFT_NUMBER = ' + IntToStr(AEmployeeData.ShiftNumber);
    ClientDataSetShift_Tmp.Filtered := False;
    if not ClientDataSetShift_Tmp.IsEmpty then
    begin
      for Index := 1 to 7 do
      begin
        WeekStartDateTime[Index] :=
          ClientDataSetShift_Tmp.FieldByName('STARTTIME' +
            IntToStr(Index)).AsDateTime;
        WeekEndDateTime[Index] :=
          ClientDataSetShift_Tmp.FieldByName('ENDTIME' +
            IntToStr(Index)).AsDateTime;
      end;
      Ready := True;
    end;
  end;
end;

// this function is the same as function of UGlobalUnit -
// but it call the new defined function  GetShiftWStartEnd
// 20013489 EmployeeData must be a VAR-variable!
procedure TProdMinClass.GetShiftDay(
  var EmployeeData: TScannedIDCard;
  var StartDateTime, EndDateTime: TDateTime);
begin
// MR:18-07-2003 This replaces the old functionality
  AShiftDay.GetShiftDay(EmployeeData, StartDateTime, EndDateTime);
end;

// MR:03-12-2003 EmployeeData changed to 'var'-variable.
procedure TProdMinClass.EmployeeCutOfTime(
  FirstScan, LastScan: Boolean; var EmployeeData: TScannedIDCard;
  DeleteAction: Boolean; DeleteEmployeeData: TScannedIDCard;
  var StartDTInterval, EndDTInterval:TDateTime);
begin
  AEmployeeData := EmployeeData;
  EmployeeCutOfTime_Query(
    FirstScan, LastScan, EmployeeData,
    DeleteAction, DeleteEmployeeData, StartDTInterval, EndDTInterval);
end;

// MR:25-09-2003
// MR:03-12-2003 EmployeeData changed to 'var'-variable, so values for
//               StartDTInterval+EndDTInterfval can be stored in this var. for
//               later use in 'BooksExceptionalTime'.
// MR:23-1-2004 RoundMinutes-procedures added.
procedure TProdMinClass.EmployeeCutOfTime_Query(
  FirstScan, LastScan: Boolean; var EmployeeData: TScannedIDCard;
  DeleteAction: Boolean; DeleteEmployeeData: TScannedIDCard;
  var StartDTInterval, EndDTInterval: TDateTime);
var
  CutOffStart, CutOffEnd: Boolean;
  RoundTruncSalaryHours, RoundMinute: Integer;
  IsMarginInEarly, IsMarginOutEarly: Boolean;
  procedure ActionContractRoundMinutes;
  begin
    AContractRoundMinutes := False;
    if (RoundTruncSalaryHours <> -1) and (RoundMinute <> -1) then
    begin
      if (RoundMinute > 1) then
        AContractRoundMinutes := True;
    end;
  end;
  // PIM-326 Only call this when really needed, not for all scans!
  procedure DetermineRequestEarlyLate;
  var
    RequestDate, EarlyTime, LateTime: TDateTime;
    DateFrom, DateTo: TDateTime;
  begin
    RequestDate := EmployeeData.DateIn;
    // MR:13-02-2003 Be sure date-compare will work.
    //               Because 'REQUEST_DATE' contains a time-part, it
    //               must be compared by using a from/to datetime.
    DateFrom := Trunc(RequestDate) + Frac(EncodeTime(0, 0, 0, 0));
    DateTo := Trunc(RequestDate) + Frac(EncodeTime(23, 59, 59, 99));
    CalculateTotalHoursDM.oqRequest.ClearVariables;
    CalculateTotalHoursDM.oqRequest.SetVariable('DATEFROM', DateFrom);
    CalculateTotalHoursDM.oqRequest.SetVariable('DATETO',   DateTo);
    CalculateTotalHoursDM.oqRequest.SetVariable('EMPLOYEE_NUMBER',
      EmployeeData.EmployeeCode);
    CalculateTotalHoursDM.oqRequest.Execute;
    if not CalculateTotalHoursDM.oqRequest.Eof then
    begin
      // PIM-326 Check to see if early and late was found.
      EarlyTime := 0;
      if CalculateTotalHoursDM.oqRequest.FieldAsString('REQUESTED_EARLY_TIME') <> '' then
        EarlyTime :=
          CalculateTotalHoursDM.oqRequest.FieldAsDate('REQUESTED_EARLY_TIME');
      LateTime := 0;
      if CalculateTotalHoursDM.oqRequest.FieldAsString('REQUESTED_LATE_TIME') <> '' then
        LateTime :=
          CalculateTotalHoursDM.oqRequest.FieldAsDate('REQUESTED_LATE_TIME');
      if EarlyTime > 0 then
        ReplaceTime(StartDTInterval, EarlyTime);
      if LateTime > 0 then
        ReplaceTime(EndDTInterval, LateTime);
    end;
  end; // DetermineRequestEarlyLate
  // PIM-348
  // Check if first found scan (within 12 hours) for this employee falls in
  // the In-Margin.
  function MarginInCheck: Boolean;
  var
    FirstDateTimeIn: TDateTime;
  begin
    Result := False;
    with CalculateTotalHoursDM.oqFirstScan do
    begin
      ClearVariables;
      SetVariable('EMPLOYEE_NUMBER', EmployeeData.EmployeeCode);
      SetVariable('DATEFROM', EmployeeData.DateInOriginal - 0.5);
      SetVariable('DATETO', EmployeeData.DateInOriginal);
      if DeleteAction then
        SetVariable('CHECKDELETEDATE', 1)
      else
        SetVariable('CHECKDELETEDATE', 0);
      SetVariable('DELETEDATE', DeleteEmployeeData.DateInOriginal);
      Execute;
      if not Eof then
      begin
        if FieldAsString('DATETIME_IN') <> '' then
        begin
          FirstDateTimeIn := FieldAsDate('DATETIME_IN');
          if (FirstDateTimeIn >=
            (StartDTInterval - EmployeeData.InscanEarly / DayToMin)) and
            (FirstDateTimeIn <= StartDTInterval) then
          begin
            IsMarginInEarly := True;
            Result := True;
          end
          else
            if (FirstDateTimeIn >= StartDTInterval) and
              (FirstDateTimeIn <=
              (StartDTInterval + EmployeeData.InscanLate / DayToMin)) then
            begin
              IsMarginInEarly := False;
              if FirstDateTimeIn = EmployeeData.DateInOriginal then
                Result := True;
            end;
        end;
      end;
    end;
  end; // MarginInCheck
  // PIM-348
  // Check if last found scan (within 12 hours) for this employee falls in
  // the Out-Margin.
  function MarginOutCheck: Boolean;
  var
    LastDateTimeOut: TDateTime;
  begin
    Result := False;
    with CalculateTotalHoursDM.oqLastScan do
    begin
      ClearVariables;
      SetVariable('EMPLOYEE_NUMBER', EmployeeData.EmployeeCode);
      SetVariable('DATEFROM', EmployeeData.DateInOriginal);
      SetVariable('DATETO', EmployeeData.DateInOriginal + 0.5);
      if DeleteAction then
        SetVariable('CHECKDELETEDATE', 1)
      else
        SetVariable('CHECKDELETEDATE', 0);
      SetVariable('DELETEDATE', DeleteEmployeeData.DateInOriginal);
      Execute;
      if not Eof then
      begin
        if FieldAsString('DATETIME_OUT') <> '' then
        begin
          LastDateTimeOut := FieldAsDate('DATETIME_OUT');
          if (LastDateTimeOut >=
            (EndDTInterval - EmployeeData.OutScanEarly / DayToMin)) and
            (LastDateTimeOut <= EndDTInterval) then
          begin
            IsMarginOutEarly := True;
            if LastDateTimeOut = EmployeeData.DateOutOriginal then
              Result := True;
          end
          else
            if (LastDateTimeOut >= EndDTInterval) and
              (LastDateTimeOut <=
              (EndDTInterval + EmployeeData.OutScanLate / DayToMin)) then
            begin
              IsMarginOutEarly := False;
              Result := True;
            end;
        end;
      end;
    end;
  end; // MarginOutCheck
begin
  ReadContractRoundMinutesParams(EmployeeData, RoundTruncSalaryHours,
    RoundMinute);
  IsMarginInEarly := False;
  IsMarginOutEarly := False;
  CutOffStart := False;
  CutOffEnd := False;
  if EmployeeData.CutOfTime = CHECKEDVALUE then
  begin
    // PIM-348
    if (EmployeeData.DateIn >=
       (StartDTInterval - EmployeeData.InscanEarly / DayToMin)) and
       (EmployeeData.DateIn <=
       (StartDTInterval + EmployeeData.InscanLate / DayToMin)) then
      CutOffStart := MarginInCheck;

    // PIM-348
    if (EmployeeData.DateOut >=
      (EndDTInterval - EmployeeData.OutScanEarly / DayToMin)) and
      (EmployeeData.DateOut <=
      (EndDTInterval + EmployeeData.OutScanLate / DayToMin)) then
      CutOffEnd := MarginOutCheck;
  end; // if EmployeeData.CutOfTime = CHECKEDVALUE then

  // PIM-326
  if CutOffStart or CutOffEnd then
    DetermineRequestEarlyLate;

  if CutOffStart then
  begin
{    StartDTInterval := DateInInterval(StartDTInterval, EmployeeData.DateIn,
      EmployeeData.InscanEarly, EmployeeData.InscanLate); }
    if IsMarginInEarly then
      StartDTInterval := DateInInterval(StartDTInterval, EmployeeData.DateIn,
        EmployeeData.InscanEarly, 0)
    else
      StartDTInterval := DateInInterval(StartDTInterval, EmployeeData.DateIn,
        0, EmployeeData.InscanLate);
  end
  else
    StartDTInterval := EmployeeData.DateIn;
  if CutOffEnd then
  begin
{    EndDTInterval := DateInInterval(EndDTInterval, EmployeeData.DateOut,
      EmployeeData.OutscanEarly, EmployeeData.OutscanLate); }
    if IsMarginOutEarly then
      EndDTInterval := DateInInterval(EndDTInterval, EmployeeData.DateOut,
        EmployeeData.OutscanEarly, 0)
    else
      EndDTInterval := DateInInterval(EndDTInterval, EmployeeData.DateOut,
        0, EmployeeData.OutscanLate);
  end
  else
    EndDTInterval := EmployeeData.DateOut;
  // MR:23-1-2004
  ActionContractRoundMinutes;
  if StartDTInterval > EndDTInterval then
    EndDTInterval := StartDTInterval;
  // MR:03-12-2003 Store the values for later use.
  EmployeeData.DateInCutOff  := StartDTInterval;
  EmployeeData.DateOutCutOff := EndDTInterval;
end;

// MR:03-12-2003 EmployeeData changed to 'var'-variable.
procedure TProdMinClass.GetProdMin(var EmployeeData: TScannedIDCard;
  FirstScan, LastScan: Boolean;
  DeleteAction: Boolean; DeleteEmployeeData: TScannedIDCard;
  var BookingDay: TDateTime;
  var Minutes, BreaksMin, PayedBreaks: integer);
var
  StartDate, EndDate: TDateTime;
begin
  GetShiftDay(EmployeeData, StartDate, EndDate);
  BookingDay := StartDate;
  ReplaceTime(BookingDay,EncodeTime(0,0,0,0));

  GetShiftStartEnd(EmployeeData, StartDate, StartDate, EndDate);

  EmployeeCutOfTime(
    FirstScan, LastScan, EmployeeData,
    DeleteAction, DeleteEmployeeData,
    StartDate, EndDate);

  ComputeBreaks(EmployeeData, StartDate, EndDate, 0,
    Minutes, BreaksMin, PayedBreaks);
end;

// MRA:3-DEC-2008. RV015. Round Minutes.
// Round minutes for each record found in SALARYHOURPEREMPLOYEE (SHE) for the
// BookingDay and Employee. The rounding-difference (positive or negative) when
// not equal to 0, must be changed in the record that was found,
// for each combination of
// SHE.SALARY_DATE, SHE.EMPLOYEE_NUMBER, SHE.HOURTYPE_NUMBER and SHE.MANUAL_YN.
// Fields that must be changed are SHE.SALARY_MINUTE,
// SHE.MUTATIONDATE and SHE.MUTATOR.
// RV072.3. Return minutes that were lost during rounding.
// RV072.3. Option added to only round for a given hourtypenumber!
//          When argument 'AHourTypeNumber' is not -1 then only
//          round for the given hourtypenumber.
function TCalculateTotalHoursDM.ContractRoundMinutes(
  AEmployeeIDCard: TScannedIDCard; ABookingDay: TDateTime;
  AHourTypeNumber: Integer=-1): Integer;
var
  NewSalMin, OldSalMin, DifferenceMin: Integer;
  RoundTruncSalaryHours, RoundMinute: Integer;
  PreviousDifferenceMin, LeftOverMin: Integer; // RV039.1.
  function RoundMinutes(RoundTruncSalaryHours, RoundMinute: Integer;
    Minutes: Integer): Integer;
  var
    Hours, Mins: Integer;
  begin
    Hours := Minutes DIV 60;
    Mins := Minutes MOD 60;
    if RoundTruncSalaryHours = 1 then // Round
      Mins := Round(Mins / RoundMinute) * RoundMinute
    else
      Mins := Trunc(Mins / RoundMinute) * RoundMinute;
    Result := Hours * 60 + Mins;
  end;
begin
  // RV072.3.
  Result := 0;
{$IFDEF DEBUG1}
begin
  WDebugLog('ContractRoundMinutes');
end;
{$ENDIF}
  AProdMinClass.ReadContractRoundMinutesParams(AEmployeeIDCard,
    RoundTruncSalaryHours, RoundMinute);
  if RoundMinute <= 1 then
    Exit;
  PreviousDifferenceMin := -999; // RV039.1.
  // RV039.1. Sort odsSHE on HOURTYPE (ASC).
  // ABS-19136 Sort on HOURTYPE DESC to ensure the regular hours are as
  //           expected.
  with odsSHE do
  begin
    Close;
    DeleteVariables;
    DeclareAndSet('EMPLOYEE_NUMBER', otInteger, AEmployeeIDCard.EmployeeCode);
    DeclareAndSet('SALARY_DATE', otDate, ABookingDay);
    DeclareAndSet('HOURTYPE_NUMBER', otInteger, AHourTypeNumber);
    Open;
    while not Eof do
    begin
      // RV039.1. If there was a previous difference, then first add it to
      //        the current record.
      LeftOverMin := 0;
      // RV040.3. Do this only for 'truncate' !
      if (RoundTruncSalaryHours = 0) then // Truncate
      begin
        if (PreviousDifferenceMin <> -999) then
        begin
          // If difference was negative, it must be added as a plus-value,
          // otherwise it must be subtracted.
          if (PreviousDifferenceMin <> 0) then
          begin
            // Positive must be negative and the other way round.
            if (PreviousDifferenceMin < 0) then
              LeftOverMin := ABS(PreviousDifferenceMin)
            else
              LeftOverMin := PreviousDifferenceMin * -1;
            // Store the LeftOverMin in Salary-record.
            GlobalDM.UpdateSalaryHour(ABookingDay, AEmployeeIDCard,
              FieldByName('HOURTYPE_NUMBER').AsInteger,
              LeftOverMin,
              UNCHECKEDVALUE, False,
              False);
          end;
        end;
      end; // if (RoundTruncateSalaryHours = 0)

      // Round minutes and if there was a difference, update the salary-record.
      OldSalMin := FieldByName('SALARY_MINUTE').AsInteger
        + LeftOverMin; // RV039.1.
      NewSalMin :=
        RoundMinutes(RoundTruncSalaryHours, RoundMinute, OldSalMin);
      DifferenceMin := NewSalMin - OldSalMin;
      PreviousDifferenceMin := DifferenceMin; // RV039.1.
      // RV072.3. Do this only for Truncate
      if (RoundTruncSalaryHours = 0) then // Truncate
        Result := Result + DifferenceMin; // RV072.3.
      if DifferenceMin <> 0 then
      begin
        GlobalDM.UpdateSalaryHour(ABookingDay, AEmployeeIDCard,
          FieldByName('HOURTYPE_NUMBER').AsInteger,
          DifferenceMin,
          UNCHECKEDVALUE, False,
          False (* RV023. Use other procedure for rounding PHEPT! *) );
{$IFDEF DEBUG1}
begin
  WDebugLog('Rounding minutes. Bookingday=' + DateToStr(ABookingDay) +
    ' OldSal= ' + IntToStr(OldSalMin) +
    ' NewSal= ' + IntToStr(NewSalMin) +
    ' Diff=' + IntToStr(DifferenceMin) +
    ' Emp=' + IntToStr(AEmployeeIDCard.EmployeeCode) +
    ' In=' + DateTimeToStr(AEmployeeIDCard.DateIn) +
    ' Out=' + DateTimeToStr(AEmployeeIDCard.DateOut)
    );
end;
{$ENDIF}
      end;
      Next;
    end;
    Close;
  end;
end; // ContractRoundMinutes

procedure TCalculateTotalHoursDM.DataModuleCreate(Sender: TObject);
begin
//  inherited;
  AProdMinClass := TProdMinClass.Create;
  ATBLengthClass := TTBLengthClass.Create;
  AShiftDay := TShiftDay.Create;
end;

procedure TCalculateTotalHoursDM.DataModuleDestroy(Sender: TObject);
begin
  inherited;
  AProdMinClass.Free;
  ATBLengthClass.Free;
  AShiftDay.Free;
end;

// RV076.1.
function TProdMinClass.FindPlanningBlocks(EmployeeData: TScannedIDCard;
  var FirstPlanTimeblock, LastPlanTimeblock: Integer): Boolean;
var
  TBNumber: Integer;
  DayOfWeek: Integer;
begin
  Result := False;
  // What about 'overnight' ?
  // PIM-181 Bugfix: This query has been changed to prevent an
  //         access violation when a timeblock is null!
  with CalculateTotalHoursDM.oqFindPlanningBlocks do
  begin
{$IFDEF DEBUG3}
  WDebugLog('FindPlanning/Avail.:' +
    ' D=' + FormatDateTime('dd-mm-yyyy', Trunc(EmployeeData.DateIn)) +
    ' P=' + EmployeeData.PlantCode +
    ' W=' + EmployeeData.WorkspotCode +
    ' E=' + IntToStr(EmployeeData.EmployeeCode) +
    ' S=' + IntToStr(EmployeeData.ShiftNumber)
    );
{$ENDIF}
    // Addition: Also look at standard-planning, employee-availability and
    //           standard-employee-availability.
    DayOfWeek := DayInWeek(ORASystemDM.WeekStartsOn, Trunc(EmployeeData.DateIn));
    ClearVariables;
    SetVariable('EMPLOYEEPLANNING_DATE', Trunc(EmployeeData.DateIn));
    SetVariable('PLANT_CODE', EmployeeData.PlantCode);
    SetVariable('WORKSPOT_CODE', EmployeeData.WorkSpotCode);
    SetVariable('EMPLOYEE_NUMBER', EmployeeData.EmployeeCode);
    SetVariable('SHIFT_NUMBER', EmployeeData.ShiftNumber);
    SetVariable('DAY_OF_WEEK', DayOfWeek);
    Execute;
    if not Eof then
    begin
      Result := True;
      // Init
      FirstPlanTimeblock := 1;
      LastPlanTimeblock := ORASystemDM.MaxTimeblocks;
      // What is the first planned/available timeblock?
      for TBNumber := 1 to ORASystemDM.MaxTimeblocks do
        if FieldAsString('SCHEDULED_TIMEBLOCK_' +
          IntToStr(TBNumber))[1] in ['A', 'B', 'C', '*'] then
        begin
          FirstPlanTimeblock := TBNumber;
          Break;
        end;
      // What is the last planned/available timeblock?
      for TBNumber := ORASystemDM.MaxTimeblocks downto 1 do
        if FieldAsString('SCHEDULED_TIMEBLOCK_' +
          IntToStr(TBNumber))[1] in ['A', 'B', 'C', '*'] then
        begin
          LastPlanTimeblock := TBNumber;
          Break;
        end;
    end;
  end;
end; // FindPlanningBlocks

function TCalculateTotalHoursDM.DetermineWorkspotDept(APlantCode,
  AWorkspotCode: String): String;
begin
  Result := '';
  with oqWorkspotDept do
  begin
    ClearVariables;
    SetVariable('PLANT_CODE',    APlantCode);
    SetVariable('WORKSPOT_CODE', AWorkspotCode);
    Execute;
    if not Eof then
      Result := FieldAsString('DEPARTMENT_CODE');
  end;
end; // DetermineWorkspotDept

procedure TShiftDay.DetermineShiftStartEndTB(
  var AShiftDayRecord: TShiftDayRecord; ATimeBlockNumber: Integer);
var
  Day: Integer;
  SDay: String;
  Ready: Boolean;
  ADataSet: TDataSet;
begin
  Day:= DayInWeek(ORASystemDM.WeekStartsOn, AShiftDayRecord.ADate);
  SDay := IntToStr(Day);

  // Init the start and end
  AShiftDayRecord.AStart := Trunc(AShiftDayRecord.ADate);
  AShiftDayRecord.AEnd := Trunc(AShiftDayRecord.ADate);
  AShiftDayRecord.AValid := False;

  // Timeblocks Per Employee
  ADataSet := CalculateTotalHoursDM.ClientDataSetTBEmpl;
  ADataSet.Filtered := False;
  ADataSet.Filter :=
    'PLANT_CODE = ' + '''' + DoubleQuote(AShiftDayRecord.APlantCode) + '''' +
    ' AND ' +
    'EMPLOYEE_NUMBER = ' + IntToStr(AEmployeeNumber) + ' AND ' +
    'SHIFT_NUMBER = ' + IntToStr(AShiftDayRecord.AShiftNumber) + ' AND ' +
    'TIMEBLOCK_NUMBER = ' + IntToStr(ATimeBlockNumber);
  ADataSet.Filtered := True;
  Ready := not ADataSet.IsEmpty;

  // Timeblocks Per Department
  if not Ready then
  begin
    // Timeblocks per Department
    ADataSet := CalculateTotalHoursDM.ClientDataSetTBDept;
    ADataSet.Filtered := False;
    ADataSet.Filter :=
      'PLANT_CODE = ' + '''' + DoubleQuote(AShiftDayRecord.APlantCode) +
      '''' + ' AND ' +
      'DEPARTMENT_CODE = ''' + DoubleQuote(ADepartmentCode) +
      '''' + ' AND ' +
      'SHIFT_NUMBER = ' + IntToStr(AShiftDayRecord.AShiftNumber) + ' AND ' +
      'TIMEBLOCK_NUMBER = ' + IntToStr(ATimeBlockNumber);
    ADataSet.Filtered := True;
    Ready := not ADataSet.IsEmpty;

    // Timeblocks Per Shift
    if not Ready then
    begin
      // Timeblocks Per Shift
      ADataSet := CalculateTotalHoursDM.ClientDataSetTBShift;
      ADataSet.Filtered := False;
      ADataSet.Filter :=
        'PLANT_CODE = ' + '''' + DoubleQuote(AShiftDayRecord.APlantCode) +
        '''' + ' AND ' +
        'SHIFT_NUMBER = ' + IntToStr(AShiftDayRecord.AShiftNumber) + ' AND ' +
        'TIMEBLOCK_NUMBER = ' + IntToStr(ATimeBlockNumber);
      ADataSet.Filtered := True;
      Ready := not ADataSet.IsEmpty;

      // PIM-87 No timeblocks-per-xxx defined? Then look at the Shift
      if not Ready then
      begin
        // Shift
        ADataSet := CalculateTotalHoursDM.ClientDataSetShift;
        ADataSet.Filtered := False;
        ADataSet.Filter :=
          'PLANT_CODE = ' + '''' + DoubleQuote(AShiftDayRecord.APlantCode) +
          '''' + ' AND ' +
          'SHIFT_NUMBER = ' + IntToStr(AShiftDayRecord.AShiftNumber);
        ADataSet.Filtered := True;
        Ready := not ADataSet.IsEmpty;
      end;
    end;
  end;

  if Ready then
  begin
    // Search and Set a valid STARTTIME
    // Start looking for FIRST valid timeblock to LAST.
    ADataSet.First;
    while not ADataSet.Eof do
    begin
      // If times are not the same, it's a valid time
      // otherwise they are not valid, like
      // Starttime='00:00' and endtime='00:00'
      if Frac(ADataSet.FieldByName('STARTTIME' + SDay).AsDateTime) <>
        Frac(ADataSet.FieldByName('ENDTIME' + SDay).AsDateTime) then
      begin
        AShiftDayRecord.AStart :=
          Trunc(AShiftDayRecord.ADate) +
          Frac(ADataSet.FieldByName('STARTTIME' + SDay).AsDateTime);
        AShiftDayRecord.AValid := True;
        Break;
      end;
      ADataSet.Next;
    end;
    // Search and Set a valid ENDDTIME
    // Start looking from LAST valid timeblock to FIRST.
    ADataSet.Last;
    while not ADataSet.Bof do
    begin
      // If times are not the same, it's a valid time
      // otherwise they are not valid, like
      // Starttime='00:00' and endtime='00:00'
      if Frac(ADataSet.FieldByName('STARTTIME' + SDay).AsDateTime) <>
        Frac(ADataSet.FieldByName('ENDTIME' + SDay).AsDateTime) then
      begin
        AShiftDayRecord.AEnd :=
          Trunc(AShiftDayRecord.ADate) +
          Frac(ADataSet.FieldByName('ENDTIME' + SDay).AsDateTime);
        begin
          AShiftDayRecord.AValid := True;
          Break;
        end;
      end;
      ADataSet.Prior;
    end;
  end;
end; // DetermineShiftStartEndTB

end.
