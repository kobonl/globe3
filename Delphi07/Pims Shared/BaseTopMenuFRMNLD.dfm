inherited BaseTopMenuForm: TBaseTopMenuForm
  Caption = '_BaseTopMenuForm'
  Menu = mmPims
  PixelsPerInch = 96
  TextHeight = 13
  object StandardMenuActionList: TActionList
    Left = 424
    Top = 64
    object HelpAboutABSAct: TAction
      Category = 'Help'
      Caption = 'Over ABS'
      OnExecute = HelpAboutABSActExecute
    end
    object HelpABSHomePageAct: TAction
      Category = 'Help'
      Caption = 'ABS Web Pagina'
      OnExecute = HelpABSHomePageActExecute
    end
    object HelpOrderingInfoAct: TAction
      Category = 'Help'
      Caption = 'Bestel Informatie'
      OnExecute = HelpOrderingInfoActExecute
    end
    object HelpPimsHomePageAct: TAction
      Category = 'Help'
      Caption = 'Pims Web Pagina'
      OnExecute = HelpPimsHomePageActExecute
    end
    object HelpContentsAct: TAction
      Category = 'Help'
      Caption = 'Inhoud'
      OnExecute = HelpContentsActExecute
    end
    object HelpIndexAct: TAction
      Category = 'Help'
      Caption = 'Index'
      OnExecute = HelpIndexActExecute
    end
    object FileExit1: TFileExit
      Category = 'File'
      Caption = 'E&inde'
      Hint = 'Einde|Einde applicatie'
      ImageIndex = 43
    end
    object FileSettingsAct: TAction
      Category = 'File'
      Caption = 'Instellingen'
    end
    object FilePrintAct: TAction
      Category = 'File'
      Caption = 'Print'
      Hint = 'Print'
    end
    object FileClose: TAction
      Category = 'File'
      Caption = 'Einde'
      Hint = 'Einde'
      OnExecute = FileCloseExecute
    end
  end
  object mmPims: TMainMenu
    Left = 456
    Top = 64
    object miFile1: TMenuItem
      Caption = 'Bestand'
      object Settings1: TMenuItem
        Action = FileSettingsAct
      end
      object miExit1: TMenuItem
        Action = FileClose
      end
    end
    object miHelp1: TMenuItem
      Caption = 'Help'
      object Contents1: TMenuItem
        Action = HelpContentsAct
      end
      object Index1: TMenuItem
        Action = HelpIndexAct
      end
      object HorMenuLine1: TMenuItem
        Caption = '-'
      end
      object ABSGroupHomePage1: TMenuItem
        Action = HelpABSHomePageAct
      end
      object HorMenuLine2: TMenuItem
        Caption = '-'
      end
      object AboutABSGroup1: TMenuItem
        Action = HelpAboutABSAct
      end
      object OrderingInformation1: TMenuItem
        Action = HelpOrderingInfoAct
      end
    end
  end
  object StyleController: TdxEditStyleController
    BorderColor = clBlack
    BorderStyle = xbsSingle
    ButtonTransparence = ebtAlways
    Left = 488
    Top = 64
  end
  object StyleControllerRequired: TdxEditStyleController
    BorderColor = clBlack
    BorderStyle = xbsSingle
    ButtonTransparence = ebtAlways
    HotTrack = True
    Left = 520
    Top = 64
  end
end
