(*
  MRA:1-MAY-2015 20014450.50 Part 2
  - Real Time Efficiency
  - Summary added: Be sure to turn on egoLoadAllRecord!
  MRA:20-JUN-2016 PIM-194
  - Use 3 colors for efficiency-bars.
*)
unit DialogEmpListViewFRM;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseDialogFRM, dxCntner, Menus, StdActns, ActnList, ImgList,
  StdCtrls, Buttons, ComCtrls, ExtCtrls, ORASystemDMT, dxExEdtr, dxTL, dxDBCtrl,
  dxDBGrid, DB, UPimsConst, jpeg;

type
  TDialogEmpListViewF = class(TBaseDialogForm)
    pnlTop: TPanel;
    pnlMain: TPanel;
    lblName: TLabel;
    lblDate: TLabel;
    TimerAutoClose: TTimer;
    dxDBGridEmpEff: TdxDBGrid;
    dsEmpEff: TDataSource;
    dxDBGridEmpEffColumnWORKSPOT: TdxDBGridColumn;
    dxDBGridEmpEffColumnJOB: TdxDBGridColumn;
    dxDBGridEmpEffColumnQACTUAL: TdxDBGridColumn;
    dxDBGridEmpEffColumnQNORM: TdxDBGridColumn;
    dxDBGridEmpEffColumnQDIFF: TdxDBGridColumn;
    dxDBGridEmpEffColumnTACTUAL: TdxDBGridColumn;
    dxDBGridEmpEffColumnTNORM: TdxDBGridColumn;
    dxDBGridEmpEffColumnTDIFF: TdxDBGridColumn;
    dxDBGridEmpEffColumnEFF: TdxDBGridColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure TimerAutoCloseTimer(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure dxDBGridEmpEffCustomDrawCell(Sender: TObject;
      ACanvas: TCanvas; ARect: TRect; ANode: TdxTreeListNode;
      AColumn: TdxTreeListColumn; ASelected, AFocused,
      ANewItemRow: Boolean; var AText: String; var AColor: TColor;
      AFont: TFont; var AAlignment: TAlignment; var ADone: Boolean);
    procedure dxDBGridEmpEffColumnTACTUALDrawSummaryFooter(Sender: TObject;
      ACanvas: TCanvas; ARect: TRect; var AText: String;
      var AAlignment: TAlignment; AFont: TFont; var AColor: TColor;
      var ADone: Boolean);
    procedure dxDBGridEmpEffColumnTNORMDrawSummaryFooter(Sender: TObject;
      ACanvas: TCanvas; ARect: TRect; var AText: String;
      var AAlignment: TAlignment; AFont: TFont; var AColor: TColor;
      var ADone: Boolean);
    procedure dxDBGridEmpEffColumnTDIFFDrawSummaryFooter(Sender: TObject;
      ACanvas: TCanvas; ARect: TRect; var AText: String;
      var AAlignment: TAlignment; AFont: TFont; var AColor: TColor;
      var ADone: Boolean);
    procedure dxDBGridEmpEffColumnQDIFFDrawSummaryFooter(Sender: TObject;
      ACanvas: TCanvas; ARect: TRect; var AText: String;
      var AAlignment: TAlignment; AFont: TFont; var AColor: TColor;
      var ADone: Boolean);
    procedure dxDBGridEmpEffColumnEFFDrawSummaryFooter(Sender: TObject;
      ACanvas: TCanvas; ARect: TRect; var AText: String;
      var AAlignment: TAlignment; AFont: TFont; var AColor: TColor;
      var ADone: Boolean);
  private
    { Private declarations }
    FEmployeeNumber: Integer;
    FEmployeeName: String;
    TActualSummary: Double;
    TNormSummary: Double;
    function TimeFormat(AText: String): String;
    function QtyDiffFormat(AText: String): String;
    function MinuteDiffFormat(AText: String): String;
  public
    { Public declarations }
    property EmployeeNumber: Integer read FEmployeeNumber write FEmployeeNumber;
    property EmployeeName: String read FEmployeeName write FEmployeeName;
  end;

var
  DialogEmpListViewF: TDialogEmpListViewF;

implementation

{$R *.dfm}

uses
  RealTimeEffDMT, UGlobalFunctions, UTranslateStringsRealTimeEff,
  UTranslateStringsShared;

procedure TDialogEmpListViewF.FormCreate(Sender: TObject);
begin
  inherited;
  btnCancel.Visible := False;
end;

procedure TDialogEmpListViewF.FormShow(Sender: TObject);
begin
  inherited;
  if not Assigned(dxDBGridEmpEff.DataSource) then
    dxDBGridEmpEff.DataSource := dsEmpEff;
  lblName.Caption := EmployeeName;
  lblDate.Caption := ''; // DateToStr(Date);
  with RealTimeEffDM.odsRTEmpEff do
  begin
    Close;
    ClearVariables;
    SetVariable('EMPLOYEE_NUMBER', EmployeeNumber);
    Open;
    if not Eof then
    begin
      Last;
      if lblDate.Caption = '' then
        lblDate.Caption := STransLastUpdate + ' ' +
          DateTimeToStr(FieldByName('INTERVALENDTIME').AsDateTime);
    end;
  end;
  TimerAutoClose.Interval := ORASystemDM.AutoCloseInterval;
  if TimerAutoClose.Interval > 0 then
    TimerAutoClose.Enabled := True;
  TNormSummary := 0;
  TActualSummary := 0;
end;

procedure TDialogEmpListViewF.TimerAutoCloseTimer(Sender: TObject);
begin
  inherited;
  TimerAutoClose.Enabled := False;
  Close;
end;

procedure TDialogEmpListViewF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  TimerAutoClose.Enabled := False;
end;

function TDialogEmpListViewF.TimeFormat(AText: String): String;
begin
  if AText <> '' then
    AText := IntMin2StringTime(Round(StrToInt(AText) / 60), True);
  Result := AText;
end;

function TDialogEmpListViewF.MinuteDiffFormat(AText: String): String;
begin
  if AText <> '' then
  begin
    AText := IntToStr(Round(StrToInt(AText) / 60)) + STransMin;
    if (AText[1] <> '-') and (AText[1] <> '0') then
      AText := '+' + AText;
  end;
  Result := AText;
end;

function TDialogEmpListViewF.QtyDiffFormat(AText: String): String;
begin
  if AText <> '' then
  begin
    if (AText[1] <> '-') and (AText[1] <> '0') then
      AText := '+' + AText;
  end;
  Result := AText;
end;

procedure TDialogEmpListViewF.dxDBGridEmpEffCustomDrawCell(Sender: TObject;
  ACanvas: TCanvas; ARect: TRect; ANode: TdxTreeListNode;
  AColumn: TdxTreeListColumn; ASelected, AFocused, ANewItemRow: Boolean;
  var AText: String; var AColor: TColor; AFont: TFont;
  var AAlignment: TAlignment; var ADone: Boolean);
var
  MyEff: Double;  
begin
  inherited;
  if (AColumn = dxDBGridEmpEffColumnTACTUAL) or
    (AColumn = dxDBGridEmpEffColumnTNORM) then
      AText := TimeFormat(AText);
  if (AColumn = dxDBGridEmpEffColumnQDIFF) then
    AText := QtyDiffFormat(AText);
  if (AColumn = dxDBGridEmpEffColumnTDIFF) then
    AText := MinuteDiffFormat(AText);
  if (AColumn = dxDBGridEmpEffColumnEFF) then
  begin
    if ORASystemDM.PSShowEmpInfo and ORASystemDM.PSShowEmpEff then
    begin
      try
        if AText <> '' then
          MyEff := StrToInt(AText)
        else
          MyEff := 0;
      except
        MyEff := 0;
      end;
      if MyEff = 0 then
      begin
        AColor := clWhite;
        AFont.Color := clBlack;
      end
      else
      begin
        // PIM-174
        AColor := ORASystemDM.NewEffColor(MyEff);
{        if MyEff < 100 then
          AColor := clMyRed
        else
          AColor := clMyGreen; }
        AFont.Color := clWhite;
      end;
      AText := AText + ' %';
    end;
  end;
end;

procedure TDialogEmpListViewF.dxDBGridEmpEffColumnTACTUALDrawSummaryFooter(
  Sender: TObject; ACanvas: TCanvas; ARect: TRect; var AText: String;
  var AAlignment: TAlignment; AFont: TFont; var AColor: TColor;
  var ADone: Boolean);
begin
  inherited;
  if AText <> '' then
    TActualSummary := StrToFloat(AText)
  else
    TActualSummary := 0;
  AText := TimeFormat(AText);
end;

procedure TDialogEmpListViewF.dxDBGridEmpEffColumnTNORMDrawSummaryFooter(
  Sender: TObject; ACanvas: TCanvas; ARect: TRect; var AText: String;
  var AAlignment: TAlignment; AFont: TFont; var AColor: TColor;
  var ADone: Boolean);
begin
  inherited;
  if AText <> '' then
    TNormSummary := StrToFloat(AText)
  else
    TNormSummary := 0;
  AText := TimeFormat(AText);
end;

procedure TDialogEmpListViewF.dxDBGridEmpEffColumnQDIFFDrawSummaryFooter(
  Sender: TObject; ACanvas: TCanvas; ARect: TRect; var AText: String;
  var AAlignment: TAlignment; AFont: TFont; var AColor: TColor;
  var ADone: Boolean);
begin
  inherited;
  AText := QtyDiffFormat(AText);
end;

procedure TDialogEmpListViewF.dxDBGridEmpEffColumnTDIFFDrawSummaryFooter(
  Sender: TObject; ACanvas: TCanvas; ARect: TRect; var AText: String;
  var AAlignment: TAlignment; AFont: TFont; var AColor: TColor;
  var ADone: Boolean);
begin
  inherited;
  AText := MinuteDiffFormat(AText);
end;

procedure TDialogEmpListViewF.dxDBGridEmpEffColumnEFFDrawSummaryFooter(
  Sender: TObject; ACanvas: TCanvas; ARect: TRect; var AText: String;
  var AAlignment: TAlignment; AFont: TFont; var AColor: TColor;
  var ADone: Boolean);
begin
  inherited;
  if TActualSummary > 0 then
    AText := IntToStr(Round(TNormSummary / TActualSummary * 100)) + ' %'
  else
    AText := '0 %';
end;

end.
