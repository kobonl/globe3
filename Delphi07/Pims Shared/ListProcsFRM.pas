unit ListProcsFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Grids, {DBGrids, DB, DBTables, UScannedIDCard, }
  {CalculateTotalHoursDMT, DBClient,} ComCtrls,
  dxGrClms, dxDBELib, dxEdLib, Variants, OracleData,
  dxCntner, dxEditor, dxExEdtr, dxExGrEd, dxExELib, Dblup1a, ExtCtrls,
  dxLayout;

{ moved to UGlobalFunctions
const
  vbUseSystem = 0;
  vbSunday    = 1;
  vbMonday    = 2;
  vbTuesday   = 3;
  vbWednesday = 4;
  vbThursday  = 5;
  vbFriday    = 6;
  vbSaturday  = 7;
  vbFirstJan1     = 1;
  vbFirstFourDays = 2;
  vbFirstFullWeek = 3;
}
type
  TListProcsF = class(TForm)
  private
    { Private declarations }
  public
    { Public declarations }
(*    procedure FillComboBox(
      DataSet: TDataSet;
      var Combo: TCustomdxPickEdit;
      MasterFieldValue1, MasterFieldValue2: String;
      ShowFirst: Boolean;
      FieldMaster1, FieldMaster2, Field1, Field2: String);
*)
    procedure FillComboBoxMaster(
      DataSet: TOracleDataSet;
      const ComboStr: String;
      const ShowFirst: Boolean;
      var Combo: TComboBoxPlus);
    procedure FillComboBoxPlant(
      DataSet: TOracleDataSet;
      const ShowFirst: Boolean;
      VAR Combo: TComboBoxPlus);
(*
     procedure FillComboBoxWorkSpot(
      DataSet: TDataSet;
      const ShowFirst: Boolean;
      var Combo: TCustomdxPickEdit);
    procedure FillComboBoxMasterPlant(
      DataSet: TDataSet;
      var Combo: TCustomdxPickEdit;
      const plantnumber: String;
      const ComboStr: String;
      const ShowFirst: boolean);
    procedure FillComboBoxEmployees(
      DataSet: TDataSet;
      var Combo: TCustomdxPickEdit;
      const plantnumber: String;
      const ShowFirst: boolean);
    procedure FillComboBoxJobCode(
      DataSet: TDataSet;
      var Combo: TCustomdxPickEdit;
      const plantnumber: String;
      const workspotcode: String;
      const ShowFirst: boolean);   *)
{ moved to UGlobalFunctions
    function DatePart(
      Interval: string;
      ADate: TDateTime;
      FirstDayOfWeek: Integer;
      FirstWeekOfYear: Integer): word;
    function WeekOfYear(ADate: TDateTime): Word;
    function WeeksInYear(year: Integer): Integer;
    function DateFromWeek(Year, Week, Day: word): TDate;
    function DateDifferenceDays(OlderDate, YoungerDate: TDateTime): Word;
    function YMDJulian(Year, Month, Day: Integer):  Longint;
    procedure WeekUitDat(Ldate: TDateTime; var Year, Week: Word);
//    function DayWFromDate(LDate: TDateTime): Integer;
    function DayWStartOnFromDate(LDate: TDateTime): Integer;
    function LastMounthDay(YearNr, MonthNr: Integer): Integer;
    function LastDayOfWeek(ADate: TDateTime): TDateTime;
    function PimsDayOfWeek(ADate: TDateTime): Word;
 moved to UGlobalFunctions }

(*    procedure InitQueryEmplTotalPieces(
      QueryPQ, QueryTRS: TQuery;
      DateFrom, DateTo: TDateTime;
      OnlyJob: Boolean);
    procedure DetermineEmplTotalPieces(
      AQuery, QueryPQ, QueryTRS: TQuery;
      DateFrom, DateTo: TDateTime;
      //CAR 29-10-2003 : 550109
      CurrentEmpl, ShiftFrom, ShiftTo: Integer;
      OnlyJob: Boolean; CheckDate: Boolean;
      Plant_Code, WK_Code, JOB_Code, Department_Code: String;
      VAR EmplPieces: Double);
    procedure InitQueryEmplTotalPiecesTR(
      QueryTRS : TQuery;
      PlantFrom, PlantTo: String;
      WKFrom, WKTo: String;
      DateFrom, DateTo: TDateTime);
    procedure InitQueryEmplTotalPiecesPQ(
      QueryPQ : TQuery;
      DateFrom, DateTo: TDateTime;
      PlantFrom, PlantTo, WKFrom, WKTo: String;
      OnlyJob: Boolean);
    procedure InitQueryTR(
      QueryTRS : TQuery;  Plant_Code, Workspot_Code, Job_Code: String;
      DateFrom, DateTo: TDateTime);
    procedure InitQueryTotalPiecesPQ(
      QueryPQ : TQuery;
      DateFrom, DateTo: TDateTime;
      PlantFrom, PlantTo, WKFrom, WKTo: String);  *)
{    procedure DetermineTotalPiecesForAllEmployees(
      AQuery, QueryPQ, QueryTRS: TQuery;
      cdsEmpPieces: TClientDataSet;
      DateFrom, DateTo: TDateTime;
      ShiftFrom, ShiftTo: Integer;
      CheckDate: Boolean;
      AProgressBar: TProgressBar);    }
  end;

var
  ListProcsF: TListProcsF;
{ moved to UGlobalFunctions
function StartOfYear(Year: Word): TDate;
}
implementation

uses
  ORASystemDMT, UPimsConst, UGlobalFunctions;

{$R *.DFM}
(* moved to UGlobalFunctions
// MR:03-12-2003
// Depending on 'WeekStartsOn', set the day-numbers.
function TListProcsF.PimsDayOfWeek(ADate: TDateTime): Word;
begin
  Result := DayOfWeek(ADate); // 1=sun 2=mon 3=tue 4=wed 5=thu 6=fri 7=sat

  // WeekStartsOn can be either 1, 2 or 7. (1=sunday, 2=monday, 7=saterday).
  case ORASystemDM.WeekStartsOn of
//  1: // sunday, no correction needed 1=sun 2=mon 3=tue... 7=sat
  2: // monday, correct to 1=mon 2=tue 3=wed 4=thu 5=fri 6=sat 7=sun
    case Result of
    1: Result := 7; // sun
    2: Result := 1; // mon
    3: Result := 2; // tue
    4: Result := 3; // wed
    5: Result := 4; // thu
    6: Result := 5; // fri
    7: Result := 6; // sat
    end;
  7: // saturday, correct to 1=sat 2=sun 3=mon 4=tue 5=wed 6=thu 7=fri
    case Result of
    1: Result := 2; // sun
    2: Result := 3; // mon
    3: Result := 4; // tue
    4: Result := 5; // wed
    5: Result := 6; // thu
    6: Result := 7; // fri
    7: Result := 1; // sat
    end;
  end;
end;

// MR:03-12-2003
{ Get first monday of the year }
{ And correct it to first (other) day depending on 'WeekStartsOn' }
{ Also look at setting of FirstWeekOfYear to decide where how the }
{ first week is defined. }
function StartOfYear(Year: Word): TDate;
var
  FirstOfYear: TDateTime;
begin
  { 1 jan of year }
  FirstOfYear := EnCodeDate(Year, 1, 1);

  // First week is week that contains 1 january.
  case ORASystemDM.FirstWeekOfYear of
  1:
    case ListProcsF.PimsDayOfWeek(FirstOfYear) of
    2: FirstOfYear := FirstOfYear - 1;
    3: FirstOfYear := FirstOfYear - 2;
    4: FirstOfYear := FirstOfYear - 3;
    5: FirstOfYear := FirstOfYear - 4;
    6: FirstOfYear := FirstOfYear - 5;
    7: FirstOfYear := FirstOfYear - 6;
    end;
  // First week is week that contains 4 january.
  4:
    case ListProcsF.PimsDayOfWeek(FirstOfYear) of
    2: FirstOfYear := FirstOfYear - 1; // Tue -> Mon
    3: FirstOfYear := FirstOfYear - 2; // Wed -> Mon
    4: FirstOfYear := FirstOfYear - 3; // Thu -> Mon
    5: FirstOfYear := FirstOfYear + 3; // Fri -> Mon
    6: FirstOfYear := FirstOfYear + 2; // Sat -> Mon
    7: FirstOfYear := FirstOfYear + 1; // Sun -> Mon
    end;
  // First week is week that contains 7 january.
  7:
    case ListProcsF.PimsDayOfWeek(FirstOfYear) of
    2: FirstOfYear := FirstOfYear + 6;
    3: FirstOfYear := FirstOfYear + 5;
    4: FirstOfYear := FirstOfYear + 4;
    5: FirstOfYear := FirstOfYear + 3;
    6: FirstOfYear := FirstOfYear + 2;
    7: FirstOfYear := FirstOfYear + 1;
    end;
  end;
  Result := FirstOfYear;
end;

function TListProcsF.LastMonthDay(YearNr, MonthNr: Integer): Integer;
begin
  Result := 30;
  case MonthNr of
    1, 3, 5, 7, 8, 10, 12: Result := 31;
    2:
      if (YearNR mod 4) = 0 then
        Result := 29
      else
        Result := 28;
    4, 6, 9, 11:  Result := 30;
  end;
end;

// MR:03-12-2003
// Changes, so it looks at 'WeekstartsOn'-setting.
{ Get last day of the week (result depends on 'weekstartson') }
function TListProcsF.LastDayOfWeek(ADate: TDateTime): TDateTime;
var
  Day: Integer;
  LastDay: Integer;
begin
  LastDay := 1;
  case ORASystemDM.WeekStartsOn of
  1: LastDay := 7; // sun
  2: LastDay := 1; // mon
  3: LastDay := 2; // tue
  4: LastDay := 3; // wed
  5: LastDay := 4; // thu
  6: LastDay := 5; // fri
  7: LastDay := 6; // sat
  end;
  Day := DayOfWeek(ADate);
  while (Day <> LastDay) do
  begin
    ADate := ADate + 1;
    Day := DayOfWeek(ADate);
  end;
  result := ADate;
end;

// MR:03-12-2003
// function changed after weekstartson
{ Get the date from the week, specifying which day from the week
Week starts with Mo end finish on SU}
function TListProcsF.DateFromWeek(Year, Week, Day: Word): TDate;
var
  StartDate: TDate;
  NextDate: TDate;
  Date: TDate;
  HDay{, SaveDay}: Integer;
begin
  Day := Day mod 7;
//  SaveDay := Day;
  { Determine start of current year }
  StartDate := StartOfYear(Year);
  NextDate := StartOfYear(Year + 1);

  HDay := Day - ListProcsF.PimsDayOfWeek(StartDate);
  if (HDay < 0) then
    HDay := HDay + 7;

  Date := StartDate + ((Week - 1) * 7) + HDay;

  if Date >= NextDate then
    Date := NextDate - 1;
//  if SaveDay = 1 then
//    Date := Date - 7;
  Result := Date;
end;

{ Get number of week of given year }
function TListProcsF.WeeksInYear(year: Integer): Integer;
var
  ADate: TDateTime;
  Weeks: Integer;
begin
  ADate := EnCodeDate(year, 12, 31);
  Weeks := ListProcsF.WeekOfYear(ADate);
  if Weeks = 1 then
  begin
    ADate := EnCodeDate(year, 12, 24);
    Weeks := ListProcsF.WeekOfYear(ADate);
  end;
  result := Weeks;
end;

// MR:03-12-2003
{ Get weeknumber of given date, also get year }
function TListProcsF.WeekOfYear(ADate: TDateTime): Word;
var
  {AYear, }Year, Month, Day: Word;
  StartDate: TDateTime;
  NextDate: TDateTime;
  LDate: TDateTime;
  DaysPast: TDateTime;
  CalcWeek: TDateTime;
  Week: Word;
begin
  DecodeDate(ADate, Year, Month, Day);
//  AYear := Year;
  StartDate := StartOfYear(Year);
  NextDate := StartOfYear(Year + 1);
  LDate := LastDayOfWeek(ADate);

  if (LDate >= StartDate) and
     (LDate <= NextDate) then
  begin
    DaysPast := LDate - StartDate;
    CalcWeek := DaysPast / 7;
    Week := Trunc(CalcWeek) + 1;
  end
  else
  begin
    if (LDate < StartDate) then
    begin
//      AYear := Year - 1;
      Week := WeeksInYear(Year - 1);
    end
    else
    begin
//      AYear := Year + 1;
      Week := 1;
    end;
  end;
  Result := Week;
end;

// Computes Julian day number for any Gregorian calendar date
// Returns :  Julian day number (astronomically for the day beginning at noon
// on the given day
function TListProcsF.YMDJulian(Year, Month, Day: Integer):  Longint;
var
  Tmp1, Tmp2, Tmp3, Tmp4, Tmp5: Longint;
begin
  if Month > 2 then
  begin
    Tmp1 := Month -3;
    Tmp2 := Year;
  end
  else
  begin
    Tmp1 := Month + 9;
    Tmp2 := Year - 1;
  end;
  Tmp3 := Tmp2 Div 100;
  Tmp4 := Tmp2 Mod 100;
  Tmp5 := Day;
  Result := (146097 * Tmp3) Div 4 + (1461 * Tmp4) Div 4 +
    (153 * Tmp1 + 2) Div 5 + Tmp5 + 1721119;
end;

function TListProcsF.DateDifferenceDays(OlderDate, YoungerDate: TDateTime): Word;
var
   OlderYear, OlderMonth, OlderDays,
   YoungerYear, YoungerMonth, YoungerDays: Word;
begin
   DecodeDate(OlderDate, OlderYear, OlderMonth, OlderDays);
   DecodeDate(YoungerDate, YoungerYear, YoungerMonth, YoungerDays);
   Result :=  YMDJulian(YoungerYear,YoungerMonth,YoungerDays) -
     YMDJulian(OlderYear, OlderMonth, OlderDays);
end;

// changed after weekstarton
procedure TListProcsF.WeekUitDat(Ldate: TDateTime; var Year, Week: Word);
var
   StartLDate,        {-Date of first day first week of year}
   NextLDate,         {-Date of first day first week of year}
   CalcWeek,          {-Used to calculate week number}
   DaysPast: Real;    {-Days past since start of year}
   Month, Day: Word;
//   InitialDate, DateMin, DateMax: Real;
begin
  {-Determine current year}
//  InitialDate := LDate;
  DecodeDate(Ldate, Year, Month, Day);
  {-Check if year bigger than base year(e.g. 1930)}
  if (Year >= 1930) then
  begin
    {-Determine start of current year, first monday}
    StartLdate:= StartOfYear(Year);
    NextLdate:= StartOfYear(Year+1);
    Ldate:= LastDayOfWeek(Ldate);
    {-If requested date not before the start of the current year and
      the requested date not past the start of the next year then calculate
      week else the week is last week of previous year or week one of the next
      year}
    if (Ldate >= StartLdate) and
       (Ldate <= NextLdate) then
    begin
      {-Determine days past since begin of current year}
      DaysPast:= DateDifferenceDays(StartLdate, Ldate);
      {-Calculate number of weeks}
      CalcWeek:= DaysPast / 7;
      Week := Trunc(CalcWeek)+1;
    end
    else
    begin
      if (Ldate < StartLdate) then
      begin
        Year:= Year-1;
        Week:= WeeksInYear(Year);
      end
      else
      begin
        Year:= Year+1;
        Week:= 1;
      end; {IF (Ldate < StartLdate) THEN}
    end; {IF (Ldate >= StartLdate) AND...}
  end
  else
  begin
    Year:= 0;
    Week:= 0;
  end; {if (Year >= 1930) then}
end; {PROCEDURE WeekUitDat}

// not changed function after weekstartson because it is based on fixed data of
// daytable
function TListProcsF.DayWStartOnFromDate(LDate: TDateTime): Integer;
{ Returns the Day-of-the-week (0 = Sunday) (Zeller's congruence) from an }
{ algorithm IZLR given in a remark on CACM Algorithm 398.                }
{1 = Sunday; monday = 2 ... 7}
//var
//  DayInWeek: Integer;
begin
  Result := ListProcsF.PimsDayOfWeek(LDate);
{
  DayInWeek := DayWFromDate(LDate);
  Result := DayInWeek;
  if ORASystemDM.WeekStartsOn = 2 then
  begin
    Result := (DayInWeek - 1);
    if DayInWeek = 1 then
      Result := 7;
  end;
  if ORASystemDM.WeekStartsOn = 7 then
    Result := (DayInWeek mod 7 + 1);
}
end;

function TListProcsF.DatePart(
  Interval: string;
  ADate: TDateTime;
  FirstDayOfWeek: Integer;
  FirstWeekOfYear: Integer): word;
var
  day: word;
  month: word;
  year: word;
begin
  { Year }
  if Interval = 'yyyy' then
  begin
    DecodeDate(ADate, year, month, day);
    result := year;
  end
  else
  { Month }
  if Interval = 'm' then
  begin
    DecodeDate(ADate, year, month, day);
    result := month;
  end
  else
  { Day }
  if Interval = 'd' then
  begin
    DecodeDate(ADate, year, month, day);
    result := day;
  end
  else
  { Week }
  if Interval = 'ww' then
  begin
    result := WeekOfYear(ADate);
  end
  else
    result := 0;
end;
//
moved to UGlobalFunctions
*)

// 550284 - improve display information
procedure TListProcsF.FillComboBoxMaster(
  DataSet: TOracleDataSet;
  const ComboStr: String;
  const ShowFirst: Boolean;
  var Combo: TComboBoxPlus);
  function FillSpaces(N: Integer): String;
  var
    i: integer;
  begin
    for i := 1 to n do
      Result := Result + ' ';
  end;
begin
//if combo.tag = 1 then sort numerical
  with DataSet do
  begin
    Active := True;
    Combo.ClearGridData;
    Combo.RowCount := RecordCount;
    Combo.ColCount := 3;
    Combo.ColWidths[0] := 0;
    Combo.ColWidths[1] := 70;
    Combo.ColWidths[2] := 180;
    First;
    while not EOF do
    begin
      Combo.AddRow(['',
        FieldByName(ComboStr).AsString + Str_Sep +
        FieldByName('DESCRIPTION').AsString,
        FieldByName(ComboStr).AsString,
        FieldByName('DESCRIPTION').AsString]);
      Next;
    end;
    Combo.SizeGridToData;
    if not IsEmpty then
    begin
      if ShowFirst then
        First
      else
        Last;
      Combo.DisplayValue := FieldByName(ComboStr).AsString +
        Str_Sep + FieldByName('DESCRIPTION').AsString;
    end
    else
      Combo.DisplayValue := '';
  end;
end;
(*
procedure TListProcsF.FillComboBox(
  DataSet: TDataSet;
  VAR Combo: TCustomdxPickEdit;
  MasterFieldValue1, MasterFieldValue2: String;
  ShowFirst: Boolean;
  FieldMaster1, FieldMaster2, Field1, Field2: String);
begin
  if (DataSet is TQuery) then
  begin
    with (DataSet as TQuery) do
    begin
      Active := False;
      Close;
      if (FieldMaster1 <> '') then
        Params.ParamByName(FieldMaster1).AsString := MasterFieldValue1;
      if (FieldMaster2 <> '') then
        Params.ParamByName(FieldMaster2).AsString := MasterFieldValue2;
      if not Prepared then
        Prepare;
      Open;
    end;
  end;

  with DataSet do
  begin
    Active := True;
    Combo.ClearGridData;
    Combo.RowCount := RecordCount;
    Combo.ColCount := 3;
    // 550284 - improve display information
    Combo.ColWidths[0] := 0;
    Combo.ColWidths[1] := 70;
    Combo.ColWidths[2] := 180;
    First;
    while not EOF do
    begin
      Combo.AddRow(['',
        FieldByName(Field1).AsString + Str_Sep +
        FieldByName(Field2).AsString,
        FieldByName(Field1).AsString,
        FieldByName(Field2).AsString]);
      Next;
    end;
    Combo.SizeGridToData;
    // CAR 25-8-2003 - speed reason
    if not IsEmpty then
    begin
      if ShowFirst then
        First
      else
        Last;
      Combo.DisplayValue := FieldByName(Field1).AsString + Str_Sep +
        FieldByName(Field2).AsString;
    end
    else
      Combo.DisplayValue := '';
  end;
end;
*)
procedure TListProcsF.FillComboBoxPlant(DataSet: TOracleDataSet;
  const ShowFirst: Boolean; var Combo: TComboBoxPlus);
begin
  FillComboBoxMaster(DataSet,'PLANT_CODE', ShowFirst, Combo);
end;
(*
procedure TListProcsF.FillComboBoxWorkSpot(DataSet: TDataSet;
  const ShowFirst: Boolean; var Combo: TCustomdxPickEdit);
begin
  FillComboBoxMaster(DataSet,'WORKSPOT_CODE', ShowFirst, Combo);
end;

procedure TListProcsF.FillComboBoxJobCode(
  DataSet: TDataSet;
  var Combo: TCustomdxPickEdit;
  const plantnumber: String;
  const workspotcode: String;
  const ShowFirst: boolean);
begin
  with (DataSet as TQuery) do
  begin
    Active := False;
    Close;
    Params.ParamByName('plant_code').AsString := plantnumber;
    Params.ParamByName('workspot_code').AsString := workspotcode;
    if not Prepared then
      Prepare;
    Open;
    Active := True;
    Combo.ClearGridData;
    Combo.RowCount := RecordCount;
    // 550284 - improve display information
    Combo.ColCount := 3;
    Combo.ColWidths[0] := 0;
    Combo.ColWidths[1] := 70;
    Combo.ColWidths[2] := 180;
    First;
    while not EOF do
    begin
      Combo.AddRow(['',
        FieldByName('Job_Code').AsString + Str_Sep +
        FieldByName('DESCRIPTION').AsString,
        FieldByName('Job_Code').AsString,
        FieldByName('DESCRIPTION').AsString]);
      Next;
    end;
    Combo.SizeGridToData;
    if Not IsEmpty then
    begin
      if ShowFirst then
        First
      else
        Last;
      Combo.DisplayValue := FieldByName('Job_Code').AsString +
        Str_Sep + FieldByName('description').AsString;
    end
    else
      Combo.DisplayValue := '';
  end;
end;

procedure TListProcsF.FillComboBoxMasterPlant(
  DataSet: TDataSet;
  var Combo: TCustomdxPickEdit;
  const plantnumber: String;
  const ComboStr: String;
  const ShowFirst: boolean);
begin
  with (DataSet as TQuery) do
  begin
    Active := False;
    Close;
    Params.ParamByName('plant_code').AsString := plantnumber;
    if not Prepared then
      Prepare;
    Open;
    Active := True;
    Combo.ClearGridData;
    Combo.RowCount := RecordCount;
    Combo.ColCount := 3;
    // 550284 - improve display information
    Combo.ColWidths[0] := 0;   //0
    Combo.ColWidths[1] := 70;
    Combo.ColWidths[2] := 180;
    First;
    while not EOF do
    begin
      Combo.AddRow([ '',
        //display selection string
        FieldByName(ComboStr).AsString + Str_Sep +
        FieldByName('DESCRIPTION').AsString,
        // combo box strings
        FieldByName(ComboStr).AsString ,
        FieldByName('DESCRIPTION').AsString]);
      Next;
    end;
    Combo.SizeGridToData;
    if Not IsEmpty then
    begin
      if ShowFirst then
        First
      else
        Last;
      Combo.DisplayValue := FieldByName(ComboStr).AsString +
        Str_Sep + FieldByName('description').AsString;
    end
    else
      Combo.DisplayValue := '';
  end;
end;

procedure TListProcsF.FillComboBoxEmployees(
  DataSet: TDataSet;
  VAR Combo: TCustomdxPickEdit;
  const plantnumber: String;
  const ShowFirst: boolean);
begin
  FillComboBoxMasterPlant(DataSet, Combo, Plantnumber, 'Employee_Number',
    ShowFirst);
end;
*)
(*
procedure TListProcsF.InitQueryEmplTotalPieces(
  QueryPQ, QueryTRS: TQuery;
  DateFrom, DateTo: TDateTime;
  OnlyJob: Boolean);
begin
  QueryPQ.Active := False;
  QueryPQ.UnPrepare;
  QueryPQ.Unidirectional := True;
  QueryPQ.SQL.Clear;
  QueryPQ.SQL.Add('SELECT PLANT_CODE, WORKSPOT_CODE, JOB_CODE,');
  QueryPQ.SQL.Add('START_DATE, END_DATE, QUANTITY AS PIECES');
  QueryPQ.SQL.Add('FROM PRODUCTIONQUANTITY');
  QueryPQ.SQL.Add('WHERE QUANTITY <> 0');
  QueryPQ.SQL.Add('AND (END_DATE > :FSTARTDATE AND START_DATE < :FENDDATE)');
  QueryPQ.SQL.Add('ORDER BY');
  if OnlyJob then
    QueryPQ.SQL.Add(' PLANT_CODE, JOB_CODE,')
  else
    QueryPQ.SQL.Add(' PLANT_CODE, WORKSPOT_CODE, JOB_CODE,');
  QueryPQ.SQL.Add(' START_DATE, END_DATE');
  QueryPQ.ParamByName('FSTARTDATE').AsDateTime :=
    Trunc(DateFrom) + EncodeTime(0, 0, 0, 1);
  QueryPQ.ParamByName('FENDDATE').AsDateTime := Trunc(DateTo) + 1;
  if not QueryPQ.Prepared then
    QueryPQ.Prepare;
  QueryPQ.Active := True;

  QueryTRS.Active := False;
  QueryTRS.UnPrepare;
  QueryTRS.Unidirectional := True;
  QueryTRS.SQL.Clear;
  QueryTRS.SQL.Add('SELECT T.PLANT_CODE, T.WORKSPOT_CODE, T.JOB_CODE,');
  QueryTRS.SQL.Add('T.EMPLOYEE_NUMBER, T.DATETIME_IN, T.DATETIME_OUT,');
  QueryTRS.SQL.Add('T.SHIFT_NUMBER, E.DEPARTMENT_CODE');
  QueryTRS.SQL.Add('FROM TIMEREGSCANNING T, EMPLOYEE E');
  QueryTRS.SQL.Add('WHERE T.DATETIME_OUT IS NOT NULL AND');
  QueryTRS.SQL.Add('T.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER AND');
  QueryTRS.SQL.Add('(T.DATETIME_OUT > :DATEFROM AND T.DATETIME_IN < :DATETO)');
  QueryTRS.SQL.Add('ORDER BY T.PLANT_CODE, T.WORKSPOT_CODE, T.JOB_CODE, ');
  QueryTRS.SQL.Add('T.DATETIME_IN');
  QueryTRS.ParamByName('DATEFROM').AsDateTime :=
    Trunc(DateFrom) + Frac(EncodeTime(0, 0, 0, 0)) - 1;
  QueryTRS.ParamByName('DATETO').AsDateTime :=
    Trunc(DateTo) + 2;

  if not QueryTRS.Prepared then
    QueryTRS.Prepare;
  QueryTRS.Active := True;
end;

procedure TListProcsF.InitQueryEmplTotalPiecesPQ(
  QueryPQ : TQuery;
  DateFrom, DateTo: TDateTime;
  PlantFrom, PlantTo, WKFrom, WKTo: String;
  OnlyJob: Boolean);
begin
  QueryPQ.Active := False;
  QueryPQ.UnPrepare;
  QueryPQ.Unidirectional := True;
  QueryPQ.SQL.Clear;
  QueryPQ.SQL.Add('SELECT PLANT_CODE, WORKSPOT_CODE, JOB_CODE,');
  QueryPQ.SQL.Add('START_DATE, END_DATE, QUANTITY AS PIECES');
  QueryPQ.SQL.Add('FROM PRODUCTIONQUANTITY  ');
  QueryPQ.SQL.Add('WHERE  (QUANTITY <> 0)   ');
  if PlantFrom = PlantTo then
  begin
    QueryPQ.SQL.Add(' AND  PLANT_CODE  = :PlantFrom ');
    QueryPQ.SQL.Add(' AND  WORKSPOT_CODE >= :WKFrom and  WORKSPOT_CODE <= :WKTO ');
  end;
  if PlantFrom <> PlantTo then
    QueryPQ.SQL.Add(' AND ( PLANT_CODE >= :PlantFrom and  PLANT_CODE <= :PLANTTO )');
  QueryPQ.SQL.Add(' AND (END_DATE > :FSTARTDATE AND START_DATE < :FENDDATE)');
  QueryPQ.SQL.Add(' ORDER BY');
  if OnlyJob then
    QueryPQ.SQL.Add('  PLANT_CODE,  JOB_CODE ')
  else
    QueryPQ.SQL.Add('  PLANT_CODE,  WORKSPOT_CODE,  JOB_CODE ');

  QueryPQ.ParamByName('FSTARTDATE').AsDateTime := DateFrom;
  QueryPQ.ParamByName('FENDDATE').AsDateTime :=  DateTo;
  QueryPQ.ParamByName('PLANTFROM').AsString :=  PlantFrom;
  if PlantFrom <> PlantTo then
    QueryPQ.ParamByName('PLANTTO').AsString :=  PlantTo;
  if PlantFrom = PlantTo then
  begin
    QueryPQ.ParamByName('WKFROM').AsString :=  WKFrom;
    QueryPQ.ParamByName('WKTO').AsString :=  WKTo;
  end;
  if not QueryPQ.Prepared then
    QueryPQ.Prepare;
  QueryPQ.Active := True;
end;

procedure TListProcsF.InitQueryTotalPiecesPQ(
  QueryPQ : TQuery;
  DateFrom, DateTo: TDateTime;
  PlantFrom, PlantTo, WKFrom, WKTo: String);
begin
  QueryPQ.Active := False;
  QueryPQ.UnPrepare;
  QueryPQ.Unidirectional := True;
  QueryPQ.SQL.Clear;
  QueryPQ.SQL.Add('SELECT PLANT_CODE, WORKSPOT_CODE, JOB_CODE,');
  QueryPQ.SQL.Add('QUANTITY ');
  QueryPQ.SQL.Add('FROM PRODUCTIONQUANTITY WHERE QUANTITY <> 0 AND ');
  if PlantFrom = PlantTo then
  begin
    QueryPQ.SQL.Add('PLANT_CODE  = :PlantFrom AND ');
    QueryPQ.SQL.Add('WORKSPOT_CODE >= :WKFrom AND WORKSPOT_CODE <= :WKTO AND');
  end;

  if PlantFrom <> PlantTo then
    QueryPQ.SQL.Add('( PLANT_CODE >= :PlantFrom AND  PLANT_CODE <= :PLANTTO ) AND');
  QueryPQ.SQL.Add('(END_DATE > :FSTARTDATE AND START_DATE < :FENDDATE)');
  QueryPQ.SQL.Add('ORDER BY ');
  QueryPQ.SQL.Add('PLANT_CODE, WORKSPOT_CODE, JOB_CODE ');

  QueryPQ.ParamByName('FSTARTDATE').AsDateTime := DateFrom;
  QueryPQ.ParamByName('FENDDATE').AsDateTime :=  DateTo;
  QueryPQ.ParamByName('PLANTFROM').AsString :=  PlantFrom;
  if PlantFrom <> PlantTo then
    QueryPQ.ParamByName('PLANTTO').AsString :=  PlantTo;
  if PlantFrom = PlantTo then
  begin
    QueryPQ.ParamByName('WKFROM').AsString :=  WKFrom;
    QueryPQ.ParamByName('WKTO').AsString :=  WKTo;
  end;
  if not QueryPQ.Prepared then
    QueryPQ.Prepare;
  QueryPQ.Active := True;
end;

procedure TListProcsF.InitQueryEmplTotalPiecesTR(
  QueryTRS : TQuery;
  PlantFrom, PlantTo: String;
  WKFrom, WKTo: String;
  DateFrom, DateTo: TDateTime);
begin
  // Be sure the DEPARTMENT_CODE is also selected, from 'WORKSPOT'!
  QueryTRS.Active := False;
  QueryTRS.UnPrepare;
  QueryTRS.Unidirectional := True;
  QueryTRS.SQL.Clear;
  QueryTRS.SQL.Add('SELECT T.PLANT_CODE, T.WORKSPOT_CODE, T.JOB_CODE,');
  QueryTRS.SQL.Add('T.EMPLOYEE_NUMBER, T.DATETIME_IN, T.DATETIME_OUT, ');
  QueryTRS.SQL.Add('T.SHIFT_NUMBER, W.DEPARTMENT_CODE');
  QueryTRS.SQL.Add('FROM TIMEREGSCANNING T, WORKSPOT W');
  QueryTRS.SQL.Add('WHERE T.DATETIME_OUT IS NOT NULL AND');
  QueryTRS.SQL.Add('T.PLANT_CODE = W.PLANT_CODE AND');
  QueryTRS.SQL.Add('T.WORKSPOT_CODE = W.WORKSPOT_CODE AND');
  if PlantFrom = PlantTo then
  begin
    QueryTRS.SQL.Add('T.PLANT_CODE  = :PLANTFROM AND');
    QueryTRS.SQL.Add('T.WORKSPOT_CODE >= :WKFROM AND ' +
      'T.WORKSPOT_CODE <= :WKTO AND');
  end
  else
    QueryTRS.SQL.Add('T.PLANT_CODE >= :PLANTFROM AND ' +
      'T.PLANT_CODE <= :PLANTTO AND');
  QueryTRS.SQL.Add('(T.DATETIME_OUT > :DATEFROM AND T.DATETIME_IN < :DATETO)');
  QueryTRS.SQL.Add('ORDER BY T.PLANT_CODE, T.WORKSPOT_CODE, T.JOB_CODE,');
  QueryTRS.SQL.Add('T.DATETIME_IN');
  if PlantFrom = PlantTo then
  begin
    QueryTRS.ParamByName('PLANTFROM').AsString := PlantFrom;
    QueryTRS.ParamByName('WKFROM').AsString := WKFrom;
    QueryTRS.ParamByName('WKTO').AsString := WKTo;
  end
  else
  begin
    QueryTRS.ParamByName('PLANTFROM').AsString := PlantFrom;
    QueryTRS.ParamByName('PLANTTO').AsString := PlantTo;
  end;
  QueryTRS.ParamByName('DATEFROM').AsDateTime := DateFrom  - 1;
  QueryTRS.ParamByName('DATETO').AsDateTime := DateTo + 1  ;
  if not QueryTRS.Prepared then
    QueryTRS.Prepare;
  QueryTRS.Active := True;
end;

procedure TListProcsF.InitQueryTR(
  QueryTRS : TQuery; Plant_Code, Workspot_Code, Job_Code: String;
  DateFrom, DateTo: TDateTime);
begin
  QueryTRS.Active := False;
  QueryTRS.UnPrepare;
  QueryTRS.Unidirectional := True;
  QueryTRS.SQL.Clear;
  QueryTRS.SQL.Add('SELECT EMPLOYEE_NUMBER, DATETIME_IN,  DATETIME_OUT');
  QueryTRS.SQL.Add('FROM TIMEREGSCANNING');
  QueryTRS.SQL.Add('WHERE  PLANT_CODE = :PLANT_CODE AND');
  QueryTRS.SQL.Add('WORKSPOT_CODE = :WORKSPOT_CODE AND');
  QueryTRS.SQL.Add('JOB_CODE = :JOB_CODE AND');
  QueryTRS.SQL.Add('(DATETIME_OUT > :DATEFROM AND DATETIME_IN < :DATETO)');
  QueryTRS.ParamByName('PLANT_CODE').AsString := Plant_Code;
  QueryTRS.ParamByName('WORKSPOT_CODE').AsString := Workspot_Code;
  QueryTRS.ParamByName('JOB_CODE').AsString := Job_Code;
  QueryTRS.ParamByName('DATEFROM').AsDateTime := DateFrom;
  QueryTRS.ParamByName('DATETO').AsDateTime := DateTo;
  if not QueryTRS.Prepared then
    QueryTRS.Prepare;
  QueryTRS.Active := True;
end;

// MR:20-01-2005 This will fill a clientdatasets with ALL
// values that are needed. This must be called only once,
// at the start of the report (Production Detail Report).
(*
procedure TListProcsF.DetermineTotalPiecesForAllEmployees(
  AQuery, QueryPQ, QueryTRS: TQuery;
  cdsEmpPieces: TClientDataSet;
  DateFrom, DateTo: TDateTime;
  ShiftFrom, ShiftTo: Integer;
  CheckDate: Boolean;
  AProgressBar: TProgressBar);
var
  CurrentEmplTotalMinutes, TotalMinutesPQ: Integer;
  CurrentEmplTotalBreakMinutes, TotalBreakMinutesPQ: Integer;
  PQMax, PQCount, TRSCount: Integer;
  EmplPieces: Double;
  // LOCAL PROCEDURE
  procedure DetermineProdMins(AQuery: TQuery;
    APlantCode, AWorkspotCode, AJobCode, ADeptCode: String;
    AShiftNumber, AEmployeeNumber, ACurrentEmpl, AShiftFrom, AShiftTo: Integer;
    AStartDate, AEndDate, ADateTimeIn, ADateTimeOut: TDateTime;
    var CurrentEmplTotalMinutes, CurrentEmplTotalBreakMinutes,
      TotalMinutesPQ, TotalBreakMinutesPQ: Integer);
  var
    ProdMin, BreaksMin, PayedBreaks: Integer;
    StartDate, EndDate, BookingDay: TDateTime;
    EmployeeIDCard: TScannedIDCard;
  begin
    EmployeeIDCard.EmployeeCode := AEmployeeNumber;
    EmployeeIDCard.PlantCode := APlantCode;
    EmployeeIDCard.ShiftNumber := AShiftNumber;
    EmployeeIDCard.WorkSpotCode := AWorkspotCode;
    EmployeeIDCard.JobCode := AJobCode;
    EmployeeIDCard.DateIn := AStartDate;
    EmployeeIDCard.DateOut := AEndDate;
    PopulateIDCard(AQuery, EmployeeIDCard, nil);
    EmployeeIDCard.DepartmentCode := ADeptCode;
    StartDate := AStartDate;
    EndDate := AEndDate;
    if ADateTimeIn > StartDate then
      StartDate := ADateTimeIn;
    if ADateTimeOut < EndDate then
      EndDate := ADateTimeOut;
    // MR: 25-09-2003
    // MR: 22-07-2004 BEGIN
    // Make use of 'GetProdMins' instead of 'ComputeBreaks', otherwise
    // not cutoff-time is determined.
    EmployeeIDCard.DateIn := StartDate;
    EmployeeIDCard.DateOut := EndDate;
    AProdMinClass.GetProdMin(EmployeeIDCard, True, True, False,
      EmployeeIDCard, BookingDay, ProdMin, BreaksMin, PayedBreaks);
{
    AProdMinClass.ComputeBreaks(EmployeeIDCard, StartDate, EndDate, 0,
      ProdMin, BreaksMin, PayedBreaks);
}
    // MR: 22-07-2004 END
    TotalMinutesPQ := TotalMinutesPQ + ProdMin;
    TotalBreakMinutesPQ := TotalBreakMinutesPQ + BreaksMin;
    if not((EndDate < DateFrom) or (StartDate > DateTo)) then
    begin
      if (StartDate < DateFrom) or (EndDate > DateTo) then
      begin
        if StartDate < DateFrom then
          StartDate := DateFrom;
        if EndDate > DateTo then
          EndDate := DateTo;
        // MR: 25-09-2003
        // MR: 22-07-2004 BEGIN
        // Make use of 'GetProdMins' instead of 'ComputeBreaks', otherwise
        // not cutoff-time is determined.
        EmployeeIDCard.DateIn := StartDate;
        EmployeeIDCard.DateOut := EndDate;
        AProdMinClass.GetProdMin(EmployeeIDCard, True, True, False,
          EmployeeIDCard, BookingDay, ProdMin, BreaksMin, PayedBreaks);
{
        AProdMinClass.ComputeBreaks(EmployeeIDCard, StartDate, EndDate, 0,
          ProdMin, BreaksMin, PayedBreaks);
}
        // MR: 22-07-2004 END
      end;
      //CAR 29-10-2003 : 550109
      if (AEmployeeNumber = ACurrentEmpl) and (AShiftNumber >= AShiftFrom) and
        (AShiftNumber <= AShiftTo) then
      begin
        CurrentEmplTotalMinutes := CurrentEmplTotalMinutes + ProdMin;
        CurrentEmplTotalBreakMinutes := CurrentEmplTotalBreakMinutes +
          BreaksMin;
      end;
    end;
    // MR:22-07-2004 Set values back to originals
    EmployeeIDCard.DateIn := AStartDate;
    EmployeeIDCard.DateOut := AEndDate;
  end;
  procedure DeterminePieces(
    CurrentEmplTotalMinutes, CurrentEmplTotalBreakMinutes,
    TotalMinutesPQ, TotalBreakMinutesPQ: Integer;
    Pieces: Double;
    var EmplPieces: Double);
  begin
    if (TotalMinutesPQ > 0) then
      EmplPieces := EmplPieces +
        (CurrentEmplTotalMinutes  / TotalMinutesPQ) * Pieces
    else
      if (TotalBreakMinutesPQ > 0) then
        EmplPieces := EmplPieces +
          (CurrentEmplTotalBreakMinutes/TotalBreakMinutesPQ) * Pieces;
  end;
begin
  EmplPieces := 0;
  PQCount := 0;
  cdsEmpPieces.EmptyDataSet;
  QueryPQ.First;
  if QueryPQ.Eof then
    Exit;
  PQMax := QueryPQ.RecordCount;
  AProgressBar.Position := 0;
  AProgressBar.Max := PQMax;
  AProgressBar.Min := 0;
//WLog('PQ.RecordCount=' + IntToStr(QueryPQ.RecordCount));
   // Find first match of Plantcode+WorkspotCode+JobCode
  QueryPQ.Filtered := False;
  DateFrom := Trunc(DateFrom) + Frac(EncodeTime(0, 0, 0, 0));
  DateTo := Trunc(DateTo) + 1;
  while not QueryPQ.Eof do
  begin
    if ( ((QueryPQ.FieldByName('START_DATE').AsDateTime < DateTo) and
        (QueryPQ.FieldByName('END_DATE').AsDateTime >= DateFrom) and
        CheckDate) or
        not CheckDate) then
    begin
      try
        ORASystemDM.Pims.StartTransaction;
        TRSCount := 0;
        CurrentEmplTotalMinutes := 0;
        CurrentEmplTotalBreakMinutes := 0;
        TotalMinutesPQ := 0;
        TotalBreakMinutesPQ := 0;
        // First goto the correct date-record for QueryPQ!
        ORASystemDM.ADateFmt.SwitchDateTimeFmtORA;
        QueryTRS.Filtered := False;
        QueryTRS.Filter :=
          ' PLANT_CODE = ' + QueryPQ.FieldByName('PLANT_CODE').AsString +
          ' AND WORKSPOT_CODE = ' +
          QueryPQ.FieldByName('WORKSPOT_CODE').AsString + ' AND ' +
          ' NOT (DATETIME_OUT < ' +
           ORASystemDM.ADateFmt.Quote +
           FormatDateTime(ORASystemDM.ADateFmt.DateTimeFmt,
             QueryPQ.FieldByName('START_DATE').AsDateTime) +
           ORASystemDM.ADateFmt.Quote + ')' + ' AND ' +
           ' NOT (DATETIME_IN > ' +
           ORASystemDM.ADateFmt.Quote +
           FormatDateTime(ORASystemDM.ADateFmt.DateTimeFmt,
             QueryPQ.FieldByName('END_DATE').AsDateTime) +
           ORASystemDM.ADateFmt.Quote + ')';
        QueryTRS.Filtered := True;
        ORASystemDM.ADateFmt.SwitchDateTimeFmtWindows;
//WLog('  TRS.RecordCount=' + IntToStr(QueryTRS.RecordCount) +
//  ' P=' + QueryPQ.FieldByName('PLANT_CODE').AsString +
//  ' W=' + QueryPQ.FieldByName('WORKSPOT_CODE').AsString);
        while not QueryTRS.Eof and
          (QueryTRS.FieldByName('DATETIME_IN').Value <=
            QueryPQ.FieldByName('END_DATE').AsDateTime) do
        begin
          if
            (
              ((QueryTRS.FieldByName('DATETIME_IN').Value <=
                QueryPQ.FieldByName('START_DATE').Value) and
               (QueryTRS.FieldByName('DATETIME_OUT').Value >
                QueryPQ.FieldByName('START_DATE').Value)) or
               ((QueryTRS.FieldByName('DATETIME_IN').Value >
                 QueryPQ.FieldByName('START_DATE').Value) and
               (QueryTRS.FieldByName('DATETIME_OUT').Value <
                 QueryPQ.FieldByName('END_DATE').Value)) or
               ((QueryTRS.FieldByName('DATETIME_OUT').Value >=
                 QueryPQ.FieldByName('END_DATE').Value) and
                (QueryTRS.FieldByName('DATETIME_IN').Value <
                 QueryPQ.FieldByName('END_DATE').Value))
               ) then
          begin
            CurrentEmplTotalMinutes := 0;
            CurrentEmplTotalBreakMinutes := 0;
            inc(TRSCount);
            DetermineProdMins(AQuery,
              QueryTRS.FieldByName('PLANT_CODE').AsString,
              QueryTRS.FieldByName('WORKSPOT_CODE').Value,
              QueryTRS.FieldByName('JOB_CODE').AsString,
              QueryTRS.FieldByName('DEPARTMENT_CODE').AsString,
              QueryTRS.FieldByName('SHIFT_NUMBER').Value,
              QueryTRS.FieldByName('EMPLOYEE_NUMBER').Value,
              QueryTRS.FieldByName('EMPLOYEE_NUMBER').Value,
              ShiftFrom, ShiftTo,
              QueryPQ.FieldByName('START_DATE').AsDateTime,
              QueryPQ.FieldByName('END_DATE').AsDateTime,
              QueryTRS.FieldByName('DATETIME_IN').Value,
              QueryTRS.FieldByName('DATETIME_OUT').Value,
              CurrentEmplTotalMinutes, CurrentEmplTotalBreakMinutes,
              TotalMinutesPQ, TotalBreakMinutesPQ);
            if not cdsEmpPieces.FindKey([
              QueryTRS.FieldByName('PLANT_CODE').AsString,
              QueryTRS.FieldByName('WORKSPOT_CODE').AsString,
              QueryTRS.FieldByName('JOB_CODE').AsString,
              QueryTRS.FieldByName('DEPARTMENT_CODE').AsString,
              QueryTRS.FieldByName('EMPLOYEE_NUMBER').AsString]) then
            begin
              cdsEmpPieces.Insert;
              cdsEmpPieces.FieldByName('PLANT_CODE').AsString :=
                QueryTRS.FieldByName('PLANT_CODE').AsString;
              cdsEmpPieces.FieldByName('WORKSPOT_CODE').AsString :=
                QueryTRS.FieldByName('WORKSPOT_CODE').AsString;
              cdsEmpPieces.FieldByName('JOB_CODE').AsString :=
                QueryTRS.FieldByName('JOB_CODE').AsString;
              cdsEmpPieces.FieldByName('DEPARTMENT_CODE').AsString :=
                QueryTRS.FieldByName('DEPARTMENT_CODE').AsString;
              cdsEmpPieces.FieldByName('SHIFT_NUMBER').AsInteger :=
                QueryTRS.FieldByName('SHIFT_NUMBER').AsInteger;
              cdsEmpPieces.FieldByName('EMPLOYEE_NUMBER').AsInteger :=
                QueryTRS.FieldByName('EMPLOYEE_NUMBER').AsInteger;
              cdsEmpPieces.FieldByName('EMPPIECES').AsFloat := 0;
              cdsEmpPieces.FieldByName('EMPTOTALMINUTES').AsInteger :=
                CurrentEmplTotalMinutes;
              cdsEmpPieces.FieldByName('EMPTOTALBREAKMINUTES').AsInteger :=
                CurrentEmplTotalBreakMinutes;
              cdsEmpPieces.FieldByName('FOUND').AsInteger := 1;
              cdsEmpPieces.Post;
            end
            else
            begin
              cdsEmpPieces.Edit;
              cdsEmpPieces.FieldByName('EMPTOTALMINUTES').AsInteger :=
                cdsEmpPieces.FieldByName('EMPTOTALMINUTES').AsInteger +
                  CurrentEmplTotalMinutes;
              cdsEmpPieces.FieldByName('EMPTOTALBREAKMINUTES').AsInteger :=
                cdsEmpPieces.FieldByName('EMPTOTALBREAKMINUTES').AsInteger +
                  CurrentEmplTotalBreakMinutes;
              cdsEmpPieces.FieldByName('FOUND').AsInteger := 1;
              cdsEmpPieces.Post;
            end;
          end; // if
          QueryTRS.Next;
        end; //while
        if TRSCount > 0 then
        begin
          cdsEmpPieces.Filtered := False;
          cdsEmpPieces.Filter := 'Found = 1';
          cdsEmpPieces.Filtered := True;
          if not cdsEmpPieces.IsEmpty then
          begin
            cdsEmpPieces.First;
            while not cdsEmpPieces.Eof do
            begin
              EmplPieces := 0;
              DeterminePieces(
                cdsEmpPieces.FieldByName('EMPTOTALMINUTES').AsInteger,
                cdsEmpPieces.FieldByName('EMPTOTALBREAKMINUTES').AsInteger,
                TotalMinutesPQ, TotalBreakMinutesPQ,
                QueryPQ.FieldByName('PIECES').Value,
                EmplPieces);
{  WLog(
  ' P=' + cdsEmpPieces.FieldByName('PLANT_CODE').AsString +
  ' W=' + cdsEmpPieces.FieldByName('WORKSPOT_CODE').AsString +
  ' E=' + cdsEmpPieces.FieldByName('EMPLOYEE_NUMBER').AsString +
  ' ETM=' + cdsEmpPieces.FieldByName('EMPTOTALMINUTES').AsString +
  ' ETBM=' + cdsEmpPieces.FieldByName('EMPTOTALBREAKMINUTES').AsString +
  ' TMPQ=' + IntToStr(TotalMinutesPQ) +
  ' TBMPQ=' + IntToStr(TotalBreakMinutesPQ) +
  ' EPCS=' + Format('%f', [EmplPieces])
  );
}
              cdsEmpPieces.Edit;
              // Clear the Totalminutes
              cdsEmpPieces.FieldByName('EMPTOTALMINUTES').AsInteger := 0;
              cdsEmpPieces.FieldByName('EMPTOTALBREAKMINUTES').AsInteger := 0;
              // Store the EmplPieces
              cdsEmpPieces.FieldByName('EMPPIECES').AsFloat :=
                cdsEmpPieces.FieldByName('EMPPIECES').AsFloat + EmplPieces;
              cdsEmpPieces.FieldByName('FOUND').AsInteger := 0;
              cdsEmpPieces.Post;
              // Because a filter is being used on 'FOUND=1' and we have
              // changed this to 0 we go to the first record again
              // untill there are no 'FOUND=1'-records anymore.
              cdsEmpPieces.First;
            end;
          end;
          cdsEmpPieces.Filter := '';
          cdsEmpPieces.Filtered := False;
        end;
        if ORASystemDM.Pims.InTransaction then
          ORASystemDM.Pims.Commit;
      except
        if ORASystemDM.Pims.InTransaction then
          ORASystemDM.Pims.Rollback;
      end;
{
      if TRSCount > 0 then
        DeterminePieces(CurrentEmplTotalMinutes,
          CurrentEmplTotalBreakMinutes, TotalMinutesPQ, TotalBreakMinutesPQ,
          QueryPQ.FieldByName('PIECES').Value, EmplPieces);
}
    end; // if
    QueryPQ.Next;
    inc(PQCount);
    if (PQCount MOD (Round(PQMax / 100)) = 0) then
    begin
      AProgressBar.Position := PQCount;
      Application.ProcessMessages;
    end;
  end;   //WHILE
  AProgressBar.Position := 0;
  Application.ProcessMessages;
  QueryPQ.Filtered := False;
  QueryPQ.Filter := '';
  QueryTRS.Filtered := False;
  QueryTRS.Filter := '';
end;

procedure TListProcsF.DetermineEmplTotalPieces(
  AQuery, QueryPQ, QueryTRS: TQuery;
  DateFrom, DateTo: TDateTime;
  //CAR 550109: 29-10-2003
  CurrentEmpl, ShiftFrom, ShiftTo: Integer;
  OnlyJob: Boolean;
  CheckDate: Boolean;
  Plant_Code, WK_Code, JOB_Code, Department_Code: String;
  VAR EmplPieces: Double);
var
  CurrentEmplTotalMinutes,   TotalMinutesPQ: Integer;
  CurrentEmplTotalBreakMinutes, TotalBreakMinutesPQ: Integer;
  TRSCount: Integer;
  // LOCAL PROCEDURE
  procedure DetermineProdMins(AQuery: TQuery;
    APlantCode, AWorkspotCode, AJobCode, ADeptCode: String;
    AShiftNumber, AEmployeeNumber, ACurrentEmpl, AShiftFrom, AShiftTo: Integer;
    AStartDate, AEndDate, ADateTimeIn, ADateTimeOut: TDateTime;
    var CurrentEmplTotalMinutes, CurrentEmplTotalBreakMinutes,
      TotalMinutesPQ, TotalBreakMinutesPQ: Integer);
  var
    ProdMin, BreaksMin, PayedBreaks: Integer;
    StartDate, EndDate, BookingDay: TDateTime;
    EmployeeIDCard: TScannedIDCard;
  begin
    EmployeeIDCard.EmployeeCode := AEmployeeNumber;
    EmployeeIDCard.PlantCode := APlantCode;
    EmployeeIDCard.ShiftNumber := AShiftNumber;
    EmployeeIDCard.WorkSpotCode := AWorkspotCode;
    EmployeeIDCard.JobCode := AJobCode;
    EmployeeIDCard.DateIn := AStartDate;
    EmployeeIDCard.DateOut := AEndDate;
    PopulateIDCard(AQuery, EmployeeIDCard, nil);
    EmployeeIDCard.DepartmentCode := ADeptCode;
    StartDate := AStartDate;
    EndDate := AEndDate;
    if ADateTimeIn > StartDate then
      StartDate := ADateTimeIn;
    if ADateTimeOut < EndDate then
      EndDate := ADateTimeOut;
    // MR: 25-09-2003
    // MR: 22-07-2004 BEGIN
    // Make use of 'GetProdMins' instead of 'ComputeBreaks', otherwise
    // not cutoff-time is determined.
    EmployeeIDCard.DateIn := StartDate;
    EmployeeIDCard.DateOut := EndDate;
    AProdMinClass.GetProdMin(EmployeeIDCard, True, True, False,
      EmployeeIDCard, BookingDay, ProdMin, BreaksMin, PayedBreaks);
{
    AProdMinClass.ComputeBreaks(EmployeeIDCard, StartDate, EndDate, 0,
      ProdMin, BreaksMin, PayedBreaks);
}
    // MR: 22-07-2004 END
    TotalMinutesPQ := TotalMinutesPQ + ProdMin;
    TotalBreakMinutesPQ := TotalBreakMinutesPQ + BreaksMin;
    if not((EndDate < DateFrom) or (StartDate > DateTo)) then
    begin
      if (StartDate < DateFrom) or (EndDate > DateTo) then
      begin
        if StartDate < DateFrom then
          StartDate := DateFrom;
        if EndDate > DateTo then
          EndDate := DateTo;
        // MR: 25-09-2003
        // MR: 22-07-2004 BEGIN
        // Make use of 'GetProdMins' instead of 'ComputeBreaks', otherwise
        // not cutoff-time is determined.
        EmployeeIDCard.DateIn := StartDate;
        EmployeeIDCard.DateOut := EndDate;
        AProdMinClass.GetProdMin(EmployeeIDCard, True, True, False,
          EmployeeIDCard, BookingDay, ProdMin, BreaksMin, PayedBreaks);
{
        AProdMinClass.ComputeBreaks(EmployeeIDCard, StartDate, EndDate, 0,
          ProdMin, BreaksMin, PayedBreaks);
}
        // MR: 22-07-2004 END
      end;
      //CAR 29-10-2003 : 550109
      if (AEmployeeNumber = ACurrentEmpl) and (AShiftNumber >= AShiftFrom) and
        (AShiftNumber <= AShiftTo) then
      begin
        CurrentEmplTotalMinutes := CurrentEmplTotalMinutes + ProdMin;
        CurrentEmplTotalBreakMinutes := CurrentEmplTotalBreakMinutes +
          BreaksMin;
      end;
    end;
    // MR:22-07-2004 Set values back to originals
    EmployeeIDCard.DateIn := AStartDate;
    EmployeeIDCard.DateOut := AEndDate;
  end;
  // LOCAL PROCEDURE
  procedure DeterminePieces(
    CurrentEmplTotalMinutes, CurrentEmplTotalBreakMinutes,
    TotalMinutesPQ, TotalBreakMinutesPQ: Integer;
    Pieces: Double;
    var EmplPieces: Double);
  begin
    if (TotalMinutesPQ > 0) then
      EmplPieces := EmplPieces +
        (CurrentEmplTotalMinutes  / TotalMinutesPQ) * Pieces
    else
      if (TotalBreakMinutesPQ > 0) then
        EmplPieces := EmplPieces +
          (CurrentEmplTotalBreakMinutes/TotalBreakMinutesPQ) * Pieces;
  end;
  // LOCAL FUNCTION
begin
  EmplPieces := 0;
  QueryPQ.First;
  if QueryPQ.Eof then
    exit;
   // Find first match of Plantcode+WorkspotCode+JobCode
  if OnlyJob then
    QueryPQ.Locate('PLANT_CODE;JOB_CODE',
      VarArrayOf([Plant_code, JOB_CODE]), [])
  else
    QueryPQ.Locate('PLANT_CODE;WORKSPOT_CODE;JOB_CODE',
      VarArrayOf([Plant_code,  WK_Code, JOB_CODE]), []);

  DateFrom := Trunc(DateFrom) + Frac(EncodeTime(0, 0, 0, 0));
  DateTo := Trunc(DateTo) + 1;
  while not QueryPQ.Eof and
    (QueryPQ.FieldByName('PLANT_CODE').Value = Plant_Code) and
    (
     ((QueryPQ.FieldByName('WORKSPOT_CODE').Value = WK_Code) and not OnlyJob) OR
      OnlyJob
     ) and
    (QueryPQ.FieldByName('JOB_CODE').Value = JOB_CODE)  do
  begin
    if ( ((QueryPQ.FieldByName('START_DATE').AsDateTime < DateTo) and
        (QueryPQ.FieldByName('END_DATE').AsDateTime >= DateFrom) and CheckDate) or
        Not CheckDate) then
    begin
      TRSCount := 0;
      CurrentEmplTotalMinutes := 0;
      CurrentEmplTotalBreakMinutes := 0;
      TotalMinutesPQ := 0;
      TotalBreakMinutesPQ := 0;
     // First goto the correct date-record for QueryPQ!

      QueryTRS.First;
      QueryTRS.Locate('PLANT_CODE;WORKSPOT_CODE;JOB_CODE',
        VarArrayOf([Plant_Code,
        QueryPQ.FieldByName('WORKSPOT_CODE').Value, JOB_CODE]), []);
      while not QueryTRS.Eof and
        (QueryTRS.FieldByName('PLANT_CODE').Value =  Plant_Code) and
        (QueryTRS.FieldByName('WORKSPOT_CODE').Value =
         QueryPQ.FieldByName('WORKSPOT_CODE').Value) and
        (QueryTRS.FieldByName('JOB_CODE').Value = JOB_CODE) and
        (QueryTRS.FieldByName('DATETIME_IN').Value <=
          QueryPQ.FieldByName('END_DATE').AsDateTime) do
      begin
        if
          (
            ((QueryTRS.FieldByName('DATETIME_IN').Value <=
              QueryPQ.FieldByName('START_DATE').Value) and
             (QueryTRS.FieldByName('DATETIME_OUT').Value >
              QueryPQ.FieldByName('START_DATE').Value)) or
             ((QueryTRS.FieldByName('DATETIME_IN').Value >
               QueryPQ.FieldByName('START_DATE').Value) and
             (QueryTRS.FieldByName('DATETIME_OUT').Value <
               QueryPQ.FieldByName('END_DATE').Value)) or
             ((QueryTRS.FieldByName('DATETIME_OUT').Value >=
               QueryPQ.FieldByName('END_DATE').Value) and
              (QueryTRS.FieldByName('DATETIME_IN').Value <
               QueryPQ.FieldByName('END_DATE').Value))
             ) then
        begin
          inc(TRSCount);
{
// !!!TEST!!!
Memo1.Lines.Add(
  'DIN=' + DateTimeToStr(QueryTRS.FieldByName('DATETIME_IN').Value) +
  ' DOUT=' + DateTimeToStr(QueryTRS.FieldByName('DATETIME_OUT').Value) +
  ' SIN=' + DateTimeToStr(QueryPQ.FieldByName('START_DATE').AsDateTime) +
  ' SOUT=' + DateTimeToStr(QueryPQ.FieldByName('END_DATE').AsDateTime)
  );
}
          DetermineProdMins(AQuery,
            Plant_Code, QueryTRS.FieldByName('WORKSPOT_CODE').Value, JOB_CODE,
            Department_code, QueryTRS.FieldByName('SHIFT_NUMBER').Value,
            QueryTRS.FieldByName('EMPLOYEE_NUMBER').Value, CurrentEmpl,
            ShiftFrom, ShiftTo,
            QueryPQ.FieldByName('START_DATE').AsDateTime,
            QueryPQ.FieldByName('END_DATE').AsDateTime,
            QueryTRS.FieldByName('DATETIME_IN').Value,
            QueryTRS.FieldByName('DATETIME_OUT').Value,
            CurrentEmplTotalMinutes, CurrentEmplTotalBreakMinutes,
            TotalMinutesPQ, TotalBreakMinutesPQ);
        end; // if
        QueryTRS.Next;
      end; //while
      if TRSCount > 0 then
        DeterminePieces(CurrentEmplTotalMinutes,
          CurrentEmplTotalBreakMinutes, TotalMinutesPQ, TotalBreakMinutesPQ,
          QueryPQ.FieldByName('PIECES').Value, EmplPieces);
    end;
{
// !!!TEST!!!
Memo1.Lines.Add(
  '   SIN=' + DateTimeToStr(QueryPQ.FieldByName('START_DATE').AsDateTime) +
  '   SOUT=' + DateTimeToStr(QueryPQ.FieldByName('END_DATE').AsDateTime)
  );
}
    QueryPQ.Next;
  end;   //WHILE
end;
*)
// MR:03-12-2003 Following functions/procedures have been changed/removed
//               since 03-12-2003.
(*
{ Get first Monday of the year }
function StartOfYear(Year: word): TDate;
var
  firstOfYear: TDateTime;
begin
  { 1 jan of year }
  firstOfYear := EnCodeDate(year, 1, 1);

  { Get first monday }
  case DayOfWeek(firstOfYear) of
//  2: { Monday }
  3: firstOfYear := firstOfYear - 1; { Tuesday }
  4: firstOfYear := firstOfYear - 2; { Wednesday }
  5: firstOfYear := firstOfYear - 3; { Thursday }
  6: firstOfYear := firstOfYear + 3; { Friday }
  7: firstOfYear := firstOfYear + 2; { Saturday }
  1: firstOfYear := firstOfYear + 1; { Sunday }
  end;
  result := firstOfYear;
end;
*)

// MR:03-12-2003
(*
{ Get last day of the week (sunday) }
function TListProcsF.LastDayOfWeek(ADate: TDateTime): TDateTime;
var
  day: Integer;
begin
  day := DayOfWeek(ADate);
  while (day <> 1) do
  begin
    ADate := ADate + 1;
    day := DayOfWeek(ADate);
  end;
  result := ADate;
end;
*)

// MR:03-12-2003
(*
// function changed after weekstartson
{ Get the date from the week, specifying which day from the week
Week starts with Mo end finish on SU}
function TListProcsF.DateFromWeek(Year, Week, Day: word): TDate;
var
  StartDate: TDate;
  NextDate: TDate;
  Date: TDate;
  HDay, SaveDay: Integer;
begin
  Day := Day mod 7;
  SaveDay := Day;
  { Determine start of current year }
  StartDate := StartOfYear(Year);
  NextDate := StartOfYear(Year + 1);

  HDay := Day - DayOfWeek(StartDate);
  if (HDay < 0) then
    HDay := HDay + 7;

  Date := StartDate + ((Week - 1) * 7) + HDay;

  if Date >= NextDate then
    Date := NextDate - 1;
  if SaveDay = 1 then
    Date := Date - 7;
  Result := Date;
// MONDAY
  if ORASystemDM.WeekStartsOn = 2 then
    Result := Date + 1;
// SATURDAY
  if ORASystemDM.WeekStartsOn = 7 then
    Result := Date - 1;
end;
*)

(*
{ Get weeknumber of given date }
function TListProcsF.WeekOfYear(ADate: TDateTime): word;
var
  day: word;
  month: word;
  year: word;
  StartDate: TDateTime;
  NextDate: TDateTime;
  LDate: TDateTime;
  DaysPast: TDateTime;
  CalcWeek: TDateTime;
  Week: word;
begin
  DecodeDate(ADate, year, month, day);
  StartDate := StartOfYear(year);
  NextDate := StartOfYear(year + 1);
  LDate := LastDayOfWeek(ADate);

  if (LDate >= StartDate) and
     (LDate <= NextDate) then
  begin
    DaysPast := LDate - StartDate;
    CalcWeek := DaysPast / 7;
    Week := Trunc(CalcWeek) + 1;
  end
  else
  begin
    if (LDate < StartDate) then
      Week := ListProcsF.WeeksInYear(year-1)
    else
      Week := 1;
  end;

  result := Week;
end;
*)

// MR:03-12-2003
(*
// changed after weekstarton
procedure TListProcsF.WeekUitDat(Ldate : Real;var Year, Week: Word);
var
   StartLDate,        {-Date of first day first week of year}
   NextLDate,         {-Date of first day first week of year}
   CalcWeek,          {-Used to calculate week number}
   DaysPast: Real;    {-Days past since start of year}
   Month, Day: Word;
   InitialDate, DateMin, DateMax: Real;
begin
  {-Determine current year}
  InitialDate := LDate;
  DecodeDate(Ldate, Year, Month, Day);
  {-Check if year bigger than base year(e.g. 1930)}
  if (Year >= 1930) then
  begin
    {-Determine start of current year, first monday}
    StartLdate:= StartOfYear(Year);
    NextLdate:= StartOfYear(Year+1);
    Ldate:= LastDayOfWeek(Ldate);
    {-If requested date not before the start of the current year and
      the requested date not past the start of the next year then calculate
      week else the week is last week of previous year or week one of the next
      year}
    if (Ldate >= StartLdate) and
       (Ldate <= NextLdate) then
    begin
      {-Determine days past since begin of current year}
      DaysPast:= DateDifferenceDays(StartLdate, Ldate);
      {-Calculate number of weeks}
      CalcWeek:= DaysPast / 7;
      Week := Trunc(CalcWeek)+1;
    end
    else
    begin
      if (Ldate < StartLdate) then
      begin
        Year:= Year-1;
        Week:= WeeksInYear(Year);
      end
      else
      begin
        Year:= Year+1;
        Week:= 1;
      end; {IF (Ldate < StartLdate) THEN}
    end; {IF (Ldate >= StartLdate) AND...}
  end
  else
  begin
    Year:= 0;
    Week:= 0;
  end; {IF (Year >= 1930) THEN}
// changes for weekstartson of ORASystemDM
  DateMin := DateFromWeek(Year, Week, 1);
  DateMax := DateFromWeek(Year, Week, 7);
  if (CompareDate(InitialDate, DateMin) = -1) then
//  if (StrToDate(DateToStr(InitialDate)) < StrToDate(DateToStr(DateMin))) then
  begin
    if (Week > 1) then
      Week := Week - 1
    else
    begin
      Year := Year - 1;
      Week := WeeksInYear(Year);
    end;
  end;
  if (CompareDate(InitialDate, DateMax) = 1) then
//  if (StrToDate(DateToStr(InitialDate)) > StrToDate(DateToStr(DateMax))) then
  begin
    if (Week >= WeeksInYear(Year)) then
    begin
      Week := 1;
      Year := Year + 1;
    end
    else
      Week := Week + 1;
  end;
end; {PROCEDURE WeekUitDat}
*)

// MR:03-12-2003
(*
// not changed function after weekstartson because it is based on fixed data of
// daytable
function TListProcsF.DayWFromDate(LDate: TDateTime): Integer;
{ Returns the Day-of-the-week (0 = Sunday) (Zeller's congruence) from an }
{ algorithm IZLR given in a remark on CACM Algorithm 398.                }
{1 = Sunday; monday = 2 ... 7}
var
  Tmp, Tmp1, Tmp2  : LongInt;
  yy, mm, dd  : Word;
begin
  DecodeDate(LDate, yy, mm, dd);
  Tmp1 := mm + 10;
  Tmp2 := yy + (mm - 14) DIV 12;
  Tmp :=  1+((13 *  (Tmp1 - Tmp1 DIV 13 * 12) - 1) DIV 5 +
                dd + 77 + 5 * (Tmp2 - Tmp2 DIV 100 * 100) DIV 4 +
                Tmp2 DIV 400 - Tmp2 DIV 100 * 2) MOD 7;
  Tmp := Tmp - 1;
  if Tmp = 0 then
    Tmp :=7;
  Result := Tmp mod 7 + 1;
end;
*)

// MR:03-12-2003
(*
// not changed function after weekstartson because it is based on fixed data of
// daytable
function TListProcsF.DayWStartOnFromDate(LDate: TDateTime): Integer;
{ Returns the Day-of-the-week (0 = Sunday) (Zeller's congruence) from an }
{ algorithm IZLR given in a remark on CACM Algorithm 398.                }
{1 = Sunday; monday = 2 ... 7}
var
  DayInWeek: Integer;
begin
  DayInWeek := DayWFromDate(LDate);
  Result := DayInWeek;
  if ORASystemDM.WeekStartsOn = 2 then
  begin
    Result := (DayInWeek - 1);
    if DayInWeek = 1 then
      Result := 7;
  end;
  if ORASystemDM.WeekStartsOn = 7 then
    Result := (DayInWeek mod 7 + 1);
end;
*)

initialization

end.

