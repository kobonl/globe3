(*
  MRA:1-APR-2015 SO-20016449
  - Time zone implementation
*)

unit UTimeZone;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Registry, DateUtils;

type
  TPartialTZI = record // a TZI registry value is 44 bytes in this order
    Bias, // 4 bytes
    StdBias, // 4 bytes
    DltBias: Integer; // 4 bytes
    StdDate, // 16 bytes
    DltDate: TSystemTime; // 16 bytes
end;

function DaylightSavings(TZInfo: TTimeZoneInformation; DT: TDateTime): Boolean;
function LocalTimeToUniversal(TZInfo: TTimeZoneInformation; LT: TDateTime): TDateTime;
function UniversalTimeToLocal(TZInfo: TTimeZoneInformation; UT: TDateTime): TDateTime;
function ReadTimeZoneFromRegistry(ATimezone: String; var ATZInfo: TTimeZoneInformation): Boolean;
function DetermineTimezonehrsdiff(ADatabaseTimezone, AWorkstationTimezone: String): Integer;
function DetermineTimeZoneList: TStringList;
function IncHour(AValue: TDateTime; AHours: Integer): TDateTime;

var
  TimeZoneList: TStringList;

implementation

function DaylightSavings(TZInfo: TTimeZoneInformation; DT: TDateTime): Boolean;
var
  D, M, Y, WeekNo: Word;
  DTBegins, STBegins: TDateTime;
begin
// Get Year/Month/Day of DateTime passed as parameter.
  DecodeDate(DT, Y, M, D);
// If TZInfo.DaylightDate.wMonth is zero,
// Daylight Time not implemented.
  if (TZInfo.DaylightDate.wMonth = 0) then
    Result := False
  else //Daylight Time is implemented.
  begin
// If wYear is zero, use relative SystemTime format.
    if (TZInfo.StandardDate.wYear = 0) then
// Relative SystemTime format.
// Calculate DateTime Daylight Time begins using
// relative format. wDay defines which wDayOfWeek
// is used for time change: wDay of 1 identifies
// the first occurrence of wDayOfWeek in the month;
// 2..4 identify the second through fourth
// occurrence. A value of 5 identifies the last
// occurrence in the month.
    begin
// Start at beginning of Daylight month.
      DTBegins :=
      EncodeDate(Y, TZInfo.DaylightDate.wMonth, 1);
      case TZInfo.DaylightDate.wDay of
      1, 2, 3, 4:
      begin
// Get to first occurrence of wDayOfWeek.
// Key point: SysUtils.DayOfWeek is
// unary-based; TZInfo.Daylight.wDay is
// zero-based
        while (SysUtils.DayOfWeek(DTBegins) - 1) <>
          TZInfo.DaylightDate.wDayOfWeek do
          DTBegins := DTBegins + 1;
          WeekNo := 1;
          if TZInfo.DaylightDate.wDay <> 1 then
            repeat
              DTBegins := DTBegins + 7;
              Inc(WeekNo);
            until WeekNo = TZInfo.DaylightDate.wDay;
// Encode time Daylight Time begins.
         with TZInfo.DaylightDate do
           DTBegins := DTBegins + EncodeTime(
           wHour, wMinute, 0, 0);
      end;
      5:
      begin
// Count down from end of month to day of
// week. Recall that we set DTBegins to the
// first day of the month; go to the first
// day of the next month and decrement.
        DTBegins := IncMonth(DTBegins, 1);
        DTBegins := DTBegins - 1;
// Find the last occurrence of
// the day of the week.
        while SysUtils.DayOfWeek(DTBegins) - 1 <>
          TZInfo.DaylightDate.wDayOfWeek do
        DTBegins := DTBegins - 1;
// Encode time Daylight Time begins.
        with TZInfo.DaylightDate do
          DTBegins := DTBegins + EncodeTime(
            wHour, wMinute, 0, 0);
      end;
    end; // case.
// Calculate DateTime Standard Time begins using
// relative format. Start at beginning of
// Standard month.
    STBegins :=
    EncodeDate(Y, TZInfo.StandardDate.wMonth, 1);
    case TZInfo.StandardDate.wDay of
    1, 2, 3, 4:
    begin
      while (SysUtils.DayOfWeek(STBegins) - 1) <>
        TZInfo.StandardDate.wDayOfWeek do
       STBegins := STBegins + 1;
      WeekNo := 1;
      if TZInfo.StandardDate.wDay <> 1 then
        repeat
          STBegins := STBegins + 7;
          Inc(WeekNo);
        until (WeekNo = TZInfo.StandardDate.wDay);
      // Encode time Standard Time begins.
      with TZInfo.StandardDate do
        STBegins := STBegins + EncodeTime(wHour, wMinute, 0, 0);
    end;
    5:
    begin
    // Count down from end of month to day of
    // week. Recall we set DTBegins to first
    // day of the month; go to the first day of
    // the next month and decrement.
      STBegins := IncMonth(STBegins, 1);
      STBegins := STBegins - 1;
      // Find last occurrence of day of the week.
      while SysUtils.DayOfWeek(STBegins) - 1 <>
        TZInfo.StandardDate.wDayOfWeek do
        STBegins := STBegins - 1;
        // Encode time at which Standard Time begins.
      with TZInfo.StandardDate do
        STBegins := STBegins + EncodeTime(
          wHour, wMinute, 0, 0);
    end;
  end; // case.
  end
  else
  begin // Absolute SystemTime format.
    with TZInfo.DaylightDate do
    begin
      DTBegins := EncodeDate(wYear, wMonth, wDay) +
        EncodeTime(wHour, wMinute, 0, 0);
    end;
    with TZInfo.StandardDate do
    begin
      STBegins := EncodeDate(wYear, wMonth, wDay) +
        EncodeTime(wHour, wMinute, 0, 0);
    end;
  end;
  // Finally! How does DT compare to DTBegins and
  // STBegins?
  if (TZInfo.DaylightDate.wMonth <
    TZInfo.StandardDate.wMonth) then
    // For Northern Hemisphere...
    Result := (DT >= DTBegins) and (DT < STBegins)
  else
    // For Southern Hemisphere...
    Result := (DT < STBegins) or (DT >= DTBegins);
  end;
end;

function LocalTimeToUniversal(TZInfo: TTimeZoneInformation; LT: TDateTime): TDateTime;
var
  UT: TDateTime;
  TZOffset: Integer;
// Offset in minutes.
begin
// Initialize UT to something,
// so compiler doesn't complain.
  UT := LT;
// Determine offset in effect for DateTime LT.
  if DaylightSavings(TZInfo, LT) then
    TZOffset := TZInfo.Bias + TZInfo.DaylightBias
  else
    TZOffset := TZInfo.Bias + TZInfo.StandardBias;
// Apply offset.
  if (TZOffset > 0) then
// Time zones west of Greenwich.
    UT := LT + EncodeTime(TZOffset div 60,
      TZOffset mod 60, 0, 0)
  else
    if (TZOffset = 0) then
// Time Zone = Greenwich.
      UT := LT
    else
      if (TZOffset < 0) then
// Time zones east of Greenwich.
        UT := LT - EncodeTime(Abs(TZOffset) div 60,
          Abs(TZOffset) mod 60, 0, 0);
// Return Universal Time.
  Result := UT;
end;

function UniversalTimeToLocal(TZInfo: TTimeZoneInformation; UT: TDateTime): TDateTime;
var
  LT: TDateTime;
  TZOffset: Integer;
begin
  LT := UT;
// Determine offset in effect for DateTime UT.
  if DaylightSavings(TZInfo, UT) then
    TZOffset := TZInfo.Bias + TZInfo.DaylightBias
  else
    TZOffset := TZInfo.Bias + TZInfo.StandardBias;
// Apply offset.
  if (TZOffset > 0) then
// Time zones west of Greenwich.
    LT := UT - EncodeTime(TZOffset div 60,
      TZOffset mod 60, 0, 0)
  else
    if (TZOffset = 0) then
// Time Zone = Greenwich.
      LT := UT
    else
      if (TZOffset < 0) then
// Time zones east of Greenwich.
        LT := UT + EncodeTime(Abs(TZOffset) div 60,
          Abs(TZOffset) mod 60, 0, 0);
// Return Local Time.
  Result := LT;
end;

// Read time zone info from registry based on the name of a timezone.
// Put the result in TZIInfo-variable.
function ReadTimeZoneFromRegistry(ATimezone: String;
  var ATZInfo: TTimeZoneInformation): Boolean;
var
  reg : TRegistry;
  keyname: String;
  PartialTZI: TPartialTZI;
begin
  Result := False;

  reg := TRegistry.Create;
  reg.RootKey := HKEY_LOCAL_MACHINE;

  if SysUtils.Win32Platform=VER_PLATFORM_WIN32_NT then // Windows NT, Windows 2000
    keyname:='SOFTWARE\Microsoft\Windows NT\CurrentVersion\Time Zones'
  else // Version Windows 95 OSR2, Windows 98
    keyname:='SOFTWARE\Microsoft\Windows\CurrentVersion\Time Zones';

  try
    if reg.OpenKey(keyname+'\'+ATimeZone, false) then
    begin
      if reg.ReadBinaryData('TZI', PartialTZI, sizeof(PartialTZI)) > 0 then
      begin
{
        AddLog('Timezone=' + ATimezone);
        AddLog('Bias=' + IntToStr(PartialTZI.Bias));
        AddLog('StdBias=' + IntToStr(PartialTZI.StdBias));
        AddLog('StdDate: ' +
          Format('Month=%d DayOfWeek=%d Day=%d Hr=%d Min=%d',
            [PartialTZI.StdDate.wMonth,
             PartialTZI.StdDate.wDayOfWeek,
             PartialTZI.StdDate.wDay,
             PartialTZI.StdDate.wHour,
             PartialTZI.StdDate.wMinute]));
        AddLog('DltBias=' + IntToStr(PartialTZI.DltBias));
        AddLog('DltDate: ' +
          Format('Month=%d DayOfWeek=%d Day=%d Hr=%d Min=%d',
            [PartialTZI.DltDate.wMonth,
             PartialTZI.DltDate.wDayOfWeek,
             PartialTZI.DltDate.wDay,
             PartialTZI.DltDate.wHour,
             PartialTZI.DltDate.wMinute]));
}
        // Store result in ATZInfo for later use
        ATZInfo.Bias := PartialTZI.Bias; // StrLCopy(C, PChar(S), High(C));
//        ATZInfo.StandardName := reg.ReadString('Std');
//        StrLCopy(PChar(@ATZInfo.StandardName[0]),
//          PChar(reg.ReadString('Std')), High(ATZInfo.StandardName));
        ATZInfo.StandardDate := PartialTZI.StdDate;
        ATZInfo.StandardBias := PartialTZI.StdBias;
//        ATZInfo.DaylightName := reg.ReadString('Dlt');
//        StrLCopy(PChar(@ATZInfo.DaylightName[0]),
//          PChar(reg.ReadString('Dlt')), High(ATZInfo.DaylightName));
        ATZInfo.DaylightDate := PartialTZI.DltDate;
        ATZInfo.DaylightBias := PartialTZI.DltBias;
        Result := True;
      end;
    end;
  finally
    reg.CloseKey;
    reg.Free;
  end;
end; // ReadTimeZoneFromRegistry

// Determine time zone hour difference between 2 given time zones given by name
function DetermineTimezonehrsdiff(ADatabaseTimezone,
  AWorkstationTimezone: String): Integer;
var
  DatabaseTZInfo, WorkstationTZInfo: TTimeZoneInformation;
  MyNow: TDateTime;
{  S1, SDB1, SDB2, SWS1, SWS2: String;
  DBUT, WSUT: TDateTime; }
//  Minutes: Integer;
//  Result1: Integer;
begin
  Result := 0;
  MyNow := SysUtils.Now; // Local time!
  if (ADatabaseTimezone <> '') and (AWorkstationTimezone <> '') then
    if ReadTimeZoneFromRegistry(ADatabaseTimezone, DatabaseTZInfo) then
      if ReadTimeZoneFromRegistry(AWorkstationTimezone, WorkstationTZInfo) then
      begin
        if DaylightSavings(DatabaseTZInfo, MyNow) then
          DatabaseTZInfo.Bias := DatabaseTZInfo.Bias + DatabaseTZInfo.DaylightBias
        else
          DatabaseTZInfo.Bias := DatabaseTZInfo.Bias + DatabaseTZInfo.StandardBias;
        if DaylightSavings(WorkstationTZInfo, MyNow) then
          WorkstationTZInfo.Bias := WorkstationTZInfo.Bias + WorkstationTZInfo.DaylightBias
        else
          WorkstationTZInfo.Bias := WorkstationTZInfo.Bias + WorkstationTZInfo.StandardBias;

        // Testing
{
        S1 := DateTimeToStr(MyNow);
        SDB1 := DateTimeToStr(UniversalTimeToLocal(DatabaseTZInfo, LocalTimeToUniversal(DatabaseTZInfo, MyNow)));
        DBUT := LocalTimeToUniversal(DatabaseTZInfo, MyNow);
        SDB2 := DateTimeToStr(DBUT);
        SWS1 := DateTimeToStr(UniversalTimeToLocal(WorkstationTZInfo, LocalTimeToUniversal(WorkstationTZInfo, MyNow)));
        WSUT := LocalTimeToUniversal(WorkstationTZInfo, MyNow);
        SWS2 := DateTimeToStr(WSUT);
}

        // m := MinutesBetween(vend,vstart);
        // yourHMStr := Format('%2.2d:%2.2d',[m div 60,m mod 60]);
        // Minutes := MinutesBetween(DBUT, WSUT);
        // Result := MinutesBetween(DBUT, WSUT) div 60;

//        if Positive then
//          Result := Abs(WorkstationTZInfo.Bias div -60) - Abs(DatabaseTZInfo.Bias div -60);
//        else
//          Result := Abs(DatabaseTZInfo.Bias div -60) - Abs(WorkstationTZInfo.Bias div -60);
        Result := (DatabaseTZInfo.Bias - WorkstationTZInfo.Bias) div 60; 

        // Testing
{
        ShowMessage('Now=' + S1 + #13 +
          'DB-UniversalTimeToLocal=' + SDB1 + #13 +
          'WS-UniversalTimeToLocal=' + SWS1 + #13 +
          'DB-LocalTimeToUniversal=' + SDB2 + #13 +
          'WS-LocalTimeToUniversal=' + SWS2 + #13 +
          'Result=' + IntToStr(Result)
          ) ;
}
      end;
end; // DetermineTimezonehrsdiff

function DetermineTimeZoneList: TStringList;
var
  reg : TRegistry;
  keyname: String;
begin
  TimeZoneList.Clear;
  TimeZoneList.Sorted := True;

  reg := TRegistry.Create;
  reg.RootKey := HKEY_LOCAL_MACHINE;

  if SysUtils.Win32Platform=VER_PLATFORM_WIN32_NT then // Windows NT, Windows 2000
    keyname:='SOFTWARE\Microsoft\Windows NT\CurrentVersion\Time Zones'
  else // Version Windows 95 OSR2, Windows 98
    keyname:='SOFTWARE\Microsoft\Windows\CurrentVersion\Time Zones';

  reg.OpenKey(keyname,false);
  try
    if reg.HasSubKeys then
    begin
      reg.GetKeyNames(TimeZoneList);
    end;
  finally
    reg.CloseKey;
    reg.Free;
  end;
  Result := TimeZoneList;
end; // DetermineTimeZoneList

function IncHour(AValue: TDateTime; AHours: Integer): TDateTime;
begin
  Result := AValue + 1 / 24 * AHours;
end;

initialization
  TimeZoneList := TStringList.Create;
  TimeZoneList := DetermineTimeZoneList;

finalization
  TimeZoneList.Clear;
  TimeZoneList.Free;

end.
