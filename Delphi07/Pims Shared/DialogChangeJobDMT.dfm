object DialogChangeJobDM: TDialogChangeJobDM
  OldCreateOrder = False
  OnDestroy = DataModuleDestroy
  Left = 230
  Top = 115
  Height = 150
  Width = 215
  object dsJobs: TDataSource
    DataSet = odsJobs
    Left = 128
    Top = 16
  end
  object odsJobs: TOracleDataSet
    SQL.Strings = (
      'SELECT J.WORKSPOT_CODE, J.JOB_CODE, J.DESCRIPTION'
      'FROM JOBCODE J')
    QBEDefinition.QBEFieldDefs = {
      04000000030000000D000000574F524B53504F545F434F444501000000000008
      0000004A4F425F434F44450100000000000B0000004445534352495054494F4E
      010000000000}
    Session = ORASystemDM.OracleSession
    OnFilterRecord = odsJobsFilterRecord
    Left = 24
    Top = 16
    object odsJobsWORKSPOT_CODE: TStringField
      FieldName = 'WORKSPOT_CODE'
      Required = True
      Size = 6
    end
    object odsJobsJOB_CODE: TStringField
      FieldName = 'JOB_CODE'
      Required = True
      Size = 6
    end
    object odsJobsDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 30
    end
  end
end
