inherited DialogChangeJobF: TDialogChangeJobF
  Left = 399
  Top = 283
  VertScrollBar.Range = 0
  BorderStyle = bsDialog
  Caption = 'Wijzig Job'
  ClientHeight = 419
  ClientWidth = 757
  Position = poDefault
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlImageBase: TPanel
    Width = 757
    Caption = ' '
    inherited imgBasePims: TImage
      Left = 459
    end
  end
  inherited stbarBase: TStatusBar
    Top = 400
    Width = 757
  end
  inherited pnlInsertBase: TPanel
    Width = 757
    Height = 322
    Font.Color = clBlack
    object Panel1: TPanel
      Left = 693
      Top = 0
      Width = 64
      Height = 322
      Align = alRight
      Color = clGrayText
      TabOrder = 0
      object btnFirst: TBitBtn
        Left = 3
        Top = 3
        Width = 61
        Height = 81
        TabOrder = 0
        Visible = False
        OnClick = btnFirstClick
        Glyph.Data = {
          76060000424D7606000000000000360400002800000018000000180000000100
          0800000000004002000000000000000000000001000000010000000000000034
          190000663100009A490000CC610001FF7A00001B3400000E3400000F6600000F
          9A00000FCC000114FF000035660000296600001C6600001B9A00001ECC000121
          FF0000519A0000419A0000369A00002A9A000029CC00012DFF00006BCC00005B
          CC000051CC000042CC000038CC00013AFF000186FF000179FF00016DFF000160
          FF000153FF000147FF00003434000034260000663D00009A590000CC710001FF
          870000283400333333001E7C4B0008C4620001FF7A0033FF9400004266001E4F
          7C001E387C000824C400011AFF003342FF00005C9A00086AC4000853C400083B
          C400012DFF003351FF00007ACC000186FF00016DFF00015AFF000147FF00335C
          FF000192FF00339EFF00338EFF003384FF003375FF00336BFF00006666000066
          570000664A00009A640000CC7B0001FF9300005B66001E7C7C001E7C620008C4
          790001FF930033FFA400004F66001E667C006666660051AF7E003BF7950067FF
          B000006B9A000881C4005182AF00516BAF003B57F7006776FF000084CC000199
          FF003B9DF7003B86F7003B6EF7006781FF00019FFF0033ADFF0067B6FF0067A7
          FF00679CFF006791FF00009A9A00009A8B00009A7F00009A700000CC8A0001FF
          A00000929A0008C4C40008C4A80008C4910001FFA60033FFAE0000829A0008B0
          C40051AFAF0051AF95003BF7AC0067FFBF0000779A000899C4005199AF009999
          990084E2B10099FFCA000093CC0001B2FF003BB5F70084B5E200849EE20099A8
          FF0001ACFF0033B7FF0067C2FF0099CEFF0099C2FF0099B5FF0000CCCC0000CC
          BD0000CCAE0000CCA30000CC940001FFAD0000C6CC0001FFFF0001FFE60001FF
          D30001FFB90033FFBD0000B7CC0001F1FF003BF7F7003BF7DB003BF7C40067FF
          CA0000ADCC0001D8FF003BE3F70084E2E20084E2C80099FFD600009DCC0001C5
          FF003BCCF70084CDE200CCCCCC00CDFFE50001B8FF0033C6FF0067D1FF0099DB
          FF00CDE7FF00CDDBFF0001FFFF0001FFEC0001FFDF0001FFD30001FFC60001FF
          B90001F8FF0033FFFF0033FFF00033FFE10033FFD60033FFC70001EBFF0033F9
          FF0067FFFF0067FFF00067FFE50067FFD50001DEFF0033EAFF0067F7FF0099FF
          FF0099FFF00099FFE30001D2FF0033E0FF0067E8FF0099F4FF00CDFFFF00CDFF
          F10001C5FF0033D0FF0067DCFF0099E8FF00CDF4FF00FFFFFF00000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000ACACACACACAC
          ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
          ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
          ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
          ACACAC0000000000ACACACACACACACACACACACACACACACACACACAC00C7C7C7C1
          ACACACACACACACACACACACACACACACACACACAC00C7CDD5C1ACACACACACACACAC
          ACACACACACACACACACACAC00C7CDD6C1ACACACACACACACACACACACACACACACAC
          ACACAC00C7CDD5C1ACACACACACACACACACACACACACACACACACACAC00C7CDD5C1
          ACACACACACACACACACACACACACACACACACACAC00C7CDD6C1ACACACACACACACAC
          ACACACACACACACACACACAC00C7CDD5C1ACACACACACACACACACACACACACACAC00
          00000000C7CDD60000000000ACACACACACACACACACACACAC00C7C7C7C7CDD5D5
          D5D6C1ACACACACACACACACACACACACACAC00C7CDCDCDCDD5D6C1ACACACACACAC
          ACACACACACACACACACAC00C7CDCDD5D6C1ACACACACACACACACACACACACACACAC
          ACACAC00C7D5D6C1ACACACACACACACACACACACACACACACACACACACAC00D6C1AC
          ACACACACACACACACACACACACACACACACACACACACACC1ACACACACACACACACACAC
          ACACACACACAC2BCDCDCDCDCDCDCDCDCDCDCDCDCDCDACACACACACACACACAC2BCD
          CDCDCDCDCDCDCDCDCDCDCDCDCDACACACACACACACACAC2B2B2B2B2B2B2B2B2B2B
          2B2B2B2B2BACACACACACACACACACACACACACACACACACACACACACACACACACACAC
          ACACACACACACACACACACACACACACACACACACACACACACACACACAC}
      end
      object btnUp: TBitBtn
        Left = 3
        Top = 3
        Width = 61
        Height = 81
        TabOrder = 1
        OnClick = btnUpClick
        Glyph.Data = {
          76060000424D7606000000000000360400002800000018000000180000000100
          0800000000004002000000000000000000000001000000010000000000000034
          190000663100009A490000CC610001FF7A00001B3400000E3400000F6600000F
          9A00000FCC000114FF000035660000296600001C6600001B9A00001ECC000121
          FF0000519A0000419A0000369A00002A9A000029CC00012DFF00006BCC00005B
          CC000051CC000042CC000038CC00013AFF000186FF000179FF00016DFF000160
          FF000153FF000147FF00003434000034260000663D00009A590000CC710001FF
          870000283400333333001E7C4B0008C4620001FF7A0033FF9400004266001E4F
          7C001E387C000824C400011AFF003342FF00005C9A00086AC4000853C400083B
          C400012DFF003351FF00007ACC000186FF00016DFF00015AFF000147FF00335C
          FF000192FF00339EFF00338EFF003384FF003375FF00336BFF00006666000066
          570000664A00009A640000CC7B0001FF9300005B66001E7C7C001E7C620008C4
          790001FF930033FFA400004F66001E667C006666660051AF7E003BF7950067FF
          B000006B9A000881C4005182AF00516BAF003B57F7006776FF000084CC000199
          FF003B9DF7003B86F7003B6EF7006781FF00019FFF0033ADFF0067B6FF0067A7
          FF00679CFF006791FF00009A9A00009A8B00009A7F00009A700000CC8A0001FF
          A00000929A0008C4C40008C4A80008C4910001FFA60033FFAE0000829A0008B0
          C40051AFAF0051AF95003BF7AC0067FFBF0000779A000899C4005199AF009999
          990084E2B10099FFCA000093CC0001B2FF003BB5F70084B5E200849EE20099A8
          FF0001ACFF0033B7FF0067C2FF0099CEFF0099C2FF0099B5FF0000CCCC0000CC
          BD0000CCAE0000CCA30000CC940001FFAD0000C6CC0001FFFF0001FFE60001FF
          D30001FFB90033FFBD0000B7CC0001F1FF003BF7F7003BF7DB003BF7C40067FF
          CA0000ADCC0001D8FF003BE3F70084E2E20084E2C80099FFD600009DCC0001C5
          FF003BCCF70084CDE200CCCCCC00CDFFE50001B8FF0033C6FF0067D1FF0099DB
          FF00CDE7FF00CDDBFF0001FFFF0001FFEC0001FFDF0001FFD30001FFC60001FF
          B90001F8FF0033FFFF0033FFF00033FFE10033FFD60033FFC70001EBFF0033F9
          FF0067FFFF0067FFF00067FFE50067FFD50001DEFF0033EAFF0067F7FF0099FF
          FF0099FFF00099FFE30001D2FF0033E0FF0067E8FF0099F4FF00CDFFFF00CDFF
          F10001C5FF0033D0FF0067DCFF0099E8FF00CDF4FF00FFFFFF00000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000ACACACACACAC
          ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
          ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
          ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
          ACACAC0000000000ACACACACACACACACACACACACACACACACACACAC00C7C7C7C1
          ACACACACACACACACACACACACACACACACACACAC00C7CDD5C1ACACACACACACACAC
          ACACACACACACACACACACAC00C7CDD6C1ACACACACACACACACACACACACACACACAC
          ACACAC00C7CDD5C1ACACACACACACACACACACACACACACACACACACAC00C7CDD5C1
          ACACACACACACACACACACACACACACACACACACAC00C7CDD6C1ACACACACACACACAC
          ACACACACACACACACACACAC00C7CDD5C1ACACACACACACACACACACACACACACAC00
          00000000C7CDD60000000000ACACACACACACACACACACACAC00C7C7C7C7CDD5D5
          D5D6C1ACACACACACACACACACACACACACAC00C7CDCDCDCDD5D6C1ACACACACACAC
          ACACACACACACACACACAC00C7CDCDD5D6C1ACACACACACACACACACACACACACACAC
          ACACAC00C7D5D6C1ACACACACACACACACACACACACACACACACACACACAC00D6C1AC
          ACACACACACACACACACACACACACACACACACACACACACC1ACACACACACACACACACAC
          ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
          ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
          ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
          ACACACACACACACACACACACACACACACACACACACACACACACACACAC}
      end
      object btnLast: TBitBtn
        Left = 3
        Top = 83
        Width = 61
        Height = 81
        TabOrder = 3
        Visible = False
        OnClick = btnLastClick
        Glyph.Data = {
          76060000424D7606000000000000360400002800000018000000180000000100
          0800000000004002000000000000000000000001000000010000000000000034
          190000663100009A490000CC610001FF7A00001B3400000E3400000F6600000F
          9A00000FCC000114FF000035660000296600001C6600001B9A00001ECC000121
          FF0000519A0000419A0000369A00002A9A000029CC00012DFF00006BCC00005B
          CC000051CC000042CC000038CC00013AFF000186FF000179FF00016DFF000160
          FF000153FF000147FF00003434000034260000663D00009A590000CC710001FF
          870000283400333333001E7C4B0008C4620001FF7A0033FF9400004266001E4F
          7C001E387C000824C400011AFF003342FF00005C9A00086AC4000853C400083B
          C400012DFF003351FF00007ACC000186FF00016DFF00015AFF000147FF00335C
          FF000192FF00339EFF00338EFF003384FF003375FF00336BFF00006666000066
          570000664A00009A640000CC7B0001FF9300005B66001E7C7C001E7C620008C4
          790001FF930033FFA400004F66001E667C006666660051AF7E003BF7950067FF
          B000006B9A000881C4005182AF00516BAF003B57F7006776FF000084CC000199
          FF003B9DF7003B86F7003B6EF7006781FF00019FFF0033ADFF0067B6FF0067A7
          FF00679CFF006791FF00009A9A00009A8B00009A7F00009A700000CC8A0001FF
          A00000929A0008C4C40008C4A80008C4910001FFA60033FFAE0000829A0008B0
          C40051AFAF0051AF95003BF7AC0067FFBF0000779A000899C4005199AF009999
          990084E2B10099FFCA000093CC0001B2FF003BB5F70084B5E200849EE20099A8
          FF0001ACFF0033B7FF0067C2FF0099CEFF0099C2FF0099B5FF0000CCCC0000CC
          BD0000CCAE0000CCA30000CC940001FFAD0000C6CC0001FFFF0001FFE60001FF
          D30001FFB90033FFBD0000B7CC0001F1FF003BF7F7003BF7DB003BF7C40067FF
          CA0000ADCC0001D8FF003BE3F70084E2E20084E2C80099FFD600009DCC0001C5
          FF003BCCF70084CDE200CCCCCC00CDFFE50001B8FF0033C6FF0067D1FF0099DB
          FF00CDE7FF00CDDBFF0001FFFF0001FFEC0001FFDF0001FFD30001FFC60001FF
          B90001F8FF0033FFFF0033FFF00033FFE10033FFD60033FFC70001EBFF0033F9
          FF0067FFFF0067FFF00067FFE50067FFD50001DEFF0033EAFF0067F7FF0099FF
          FF0099FFF00099FFE30001D2FF0033E0FF0067E8FF0099F4FF00CDFFFF00CDFF
          F10001C5FF0033D0FF0067DCFF0099E8FF00CDF4FF00FFFFFF00000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000ACACACACACAC
          ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
          ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
          ACACACACACAC2B2B2B2B2B2B2B2B2B2B2B2B2B2B2BACACACACACACACACAC2BCD
          CDCDCDCDCDCDCDCDCDCDCDCDCDACACACACACACACACAC2BCDCDCDCDCDCDCDCDCD
          CDCDCDCDCDACACACACACACACACACACACACACACACAC00ACACACACACACACACACAC
          ACACACACACACACACACACACAC00C100ACACACACACACACACACACACACACACACACAC
          ACACAC00C1CDCD00ACACACACACACACACACACACACACACACACACAC00C1CDCDD5CD
          00ACACACACACACACACACACACACACACACAC00C1CDCDCDCDD5CD00ACACACACACAC
          ACACACACACACACAC00C1C7C7C7CDD6D6D6CD00ACACACACACACACACACACACAC00
          00000000C7CDD6C1C1C1C1C1ACACACACACACACACACACACACACACAC00C7CDD6C1
          ACACACACACACACACACACACACACACACACACACAC00C7CDD6C1ACACACACACACACAC
          ACACACACACACACACACACAC00C7CDD6C1ACACACACACACACACACACACACACACACAC
          ACACAC00C7CDD6C1ACACACACACACACACACACACACACACACACACACAC00C7CDD6C1
          ACACACACACACACACACACACACACACACACACACAC00C7CDD6C1ACACACACACACACAC
          ACACACACACACACACACACAC00C7D6D6C1ACACACACACACACACACACACACACACACAC
          ACACAC00C1C1C1C1ACACACACACACACACACACACACACACACACACACACACACACACAC
          ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
          ACACACACACACACACACACACACACACACACACACACACACACACACACAC}
      end
      object btnLow: TBitBtn
        Left = 3
        Top = 83
        Width = 61
        Height = 81
        TabOrder = 2
        OnClick = btnLowClick
        Glyph.Data = {
          76060000424D7606000000000000360400002800000018000000180000000100
          0800000000004002000000000000000000000001000000010000000000000034
          190000663100009A490000CC610001FF7A00001B3400000E3400000F6600000F
          9A00000FCC000114FF000035660000296600001C6600001B9A00001ECC000121
          FF0000519A0000419A0000369A00002A9A000029CC00012DFF00006BCC00005B
          CC000051CC000042CC000038CC00013AFF000186FF000179FF00016DFF000160
          FF000153FF000147FF00003434000034260000663D00009A590000CC710001FF
          870000283400333333001E7C4B0008C4620001FF7A0033FF9400004266001E4F
          7C001E387C000824C400011AFF003342FF00005C9A00086AC4000853C400083B
          C400012DFF003351FF00007ACC000186FF00016DFF00015AFF000147FF00335C
          FF000192FF00339EFF00338EFF003384FF003375FF00336BFF00006666000066
          570000664A00009A640000CC7B0001FF9300005B66001E7C7C001E7C620008C4
          790001FF930033FFA400004F66001E667C006666660051AF7E003BF7950067FF
          B000006B9A000881C4005182AF00516BAF003B57F7006776FF000084CC000199
          FF003B9DF7003B86F7003B6EF7006781FF00019FFF0033ADFF0067B6FF0067A7
          FF00679CFF006791FF00009A9A00009A8B00009A7F00009A700000CC8A0001FF
          A00000929A0008C4C40008C4A80008C4910001FFA60033FFAE0000829A0008B0
          C40051AFAF0051AF95003BF7AC0067FFBF0000779A000899C4005199AF009999
          990084E2B10099FFCA000093CC0001B2FF003BB5F70084B5E200849EE20099A8
          FF0001ACFF0033B7FF0067C2FF0099CEFF0099C2FF0099B5FF0000CCCC0000CC
          BD0000CCAE0000CCA30000CC940001FFAD0000C6CC0001FFFF0001FFE60001FF
          D30001FFB90033FFBD0000B7CC0001F1FF003BF7F7003BF7DB003BF7C40067FF
          CA0000ADCC0001D8FF003BE3F70084E2E20084E2C80099FFD600009DCC0001C5
          FF003BCCF70084CDE200CCCCCC00CDFFE50001B8FF0033C6FF0067D1FF0099DB
          FF00CDE7FF00CDDBFF0001FFFF0001FFEC0001FFDF0001FFD30001FFC60001FF
          B90001F8FF0033FFFF0033FFF00033FFE10033FFD60033FFC70001EBFF0033F9
          FF0067FFFF0067FFF00067FFE50067FFD50001DEFF0033EAFF0067F7FF0099FF
          FF0099FFF00099FFE30001D2FF0033E0FF0067E8FF0099F4FF00CDFFFF00CDFF
          F10001C5FF0033D0FF0067DCFF0099E8FF00CDF4FF00FFFFFF00000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000ACACACACACAC
          ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
          ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
          ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
          ACACACACAC00ACACACACACACACACACACACACACACACACACACACACACAC00C100AC
          ACACACACACACACACACACACACACACACACACACAC00C1CDCD00ACACACACACACACAC
          ACACACACACACACACACAC00C1CDCDD5CD00ACACACACACACACACACACACACACACAC
          AC00C1CDCDCDCDD5CD00ACACACACACACACACACACACACACAC00C1C7C7C7CDD6D6
          D6CD00ACACACACACACACACACACACAC0000000000C7CDD6C1C1C1C1C1ACACACAC
          ACACACACACACACACACACAC00C7CDD6C1ACACACACACACACACACACACACACACACAC
          ACACAC00C7CDD6C1ACACACACACACACACACACACACACACACACACACAC00C7CDD6C1
          ACACACACACACACACACACACACACACACACACACAC00C7CDD6C1ACACACACACACACAC
          ACACACACACACACACACACAC00C7CDD6C1ACACACACACACACACACACACACACACACAC
          ACACAC00C7CDD6C1ACACACACACACACACACACACACACACACACACACAC00C7D6D6C1
          ACACACACACACACACACACACACACACACACACACAC00C1C1C1C1ACACACACACACACAC
          ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
          ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
          ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
          ACACACACACACACACACACACACACACACACACACACACACACACACACAC}
      end
    end
    object dxDBGrid1: TdxDBGrid
      Left = 0
      Top = 0
      Width = 693
      Height = 322
      Bands = <
        item
        end>
      DefaultLayout = True
      HeaderPanelRowCount = 1
      KeyField = 'JOB_CODE'
      SummaryGroups = <>
      SummarySeparator = ', '
      Align = alClient
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -29
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      OnClick = dxDBGrid1Click
      BandFont.Charset = DEFAULT_CHARSET
      BandFont.Color = clBlack
      BandFont.Height = -19
      BandFont.Name = 'Tahoma'
      BandFont.Style = []
      DataSource = DialogChangeJobDM.dsJobs
      Filter.Criteria = {00000000}
      HeaderFont.Charset = DEFAULT_CHARSET
      HeaderFont.Color = clBlack
      HeaderFont.Height = -19
      HeaderFont.Name = 'Tahoma'
      HeaderFont.Style = []
      OptionsBehavior = [edgoAutoSort, edgoDragScroll, edgoEnterShowEditor, edgoImmediateEditor, edgoTabThrough, edgoVertThrough]
      OptionsDB = [edgoCancelOnExit, edgoCanDelete, edgoCanNavigation, edgoConfirmDelete, edgoUseBookmarks]
      OptionsView = [edgoAutoWidth, edgoBandHeaderWidth, edgoRowSelect, edgoUseBitmap]
      PreviewFont.Charset = DEFAULT_CHARSET
      PreviewFont.Color = clBlue
      PreviewFont.Height = -19
      PreviewFont.Name = 'Tahoma'
      PreviewFont.Style = []
      ScrollBars = ssNone
      OnCustomDrawCell = dxDBGrid1CustomDrawCell
      object dxDBGrid1WORKSPOT_CODE: TdxDBGridMaskColumn
        Visible = False
        BandIndex = 0
        RowIndex = 0
        FieldName = 'WORKSPOT_CODE'
      end
      object dxDBGrid1JOB_CODE: TdxDBGridMaskColumn
        Visible = False
        BandIndex = 0
        RowIndex = 0
        FieldName = 'JOB_CODE'
      end
      object dxDBGrid1DESCRIPTION: TdxDBGridMaskColumn
        Caption = 'WIJZIG JOB'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'DESCRIPTION'
      end
    end
  end
  inherited pnlBottom: TPanel
    Top = 359
    Width = 757
    inherited btnOk: TBitBtn
      Left = 238
      Top = 2
      Height = 38
      Font.Height = -19
      ParentFont = False
      Visible = False
    end
    inherited btnCancel: TBitBtn
      Left = 345
      Top = 2
      Height = 38
      Font.Height = -19
      ParentFont = False
      Visible = False
    end
    object btnDown: TBitBtn
      Left = 1
      Top = 2
      Width = 112
      Height = 38
      Caption = 'Storing'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      OnClick = btnDownClick
    end
    object btnContinue: TBitBtn
      Left = 115
      Top = 2
      Width = 113
      Height = 38
      Caption = 'Doorgaan'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
      OnClick = btnContinueClick
    end
    object PanelControls: TPanel
      Left = 459
      Top = 1
      Width = 297
      Height = 39
      Align = alRight
      Anchors = []
      BevelInner = bvLowered
      TabOrder = 4
      object BitBtnOk: TBitBtn
        Left = 7
        Top = 5
        Width = 75
        Height = 32
        Caption = '&OK'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        OnClick = BitBtnOkClick
        Kind = bkOK
      end
      object BitBtnCancel: TBitBtn
        Left = 86
        Top = 5
        Width = 75
        Height = 32
        Caption = '&Annuleer'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        OnClick = BitBtnCancelClick
        Kind = bkCancel
      end
      object BitBtnEndOfDay: TBitBtn
        Left = 165
        Top = 5
        Width = 129
        Height = 32
        Caption = '&Einde dag [F10]'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
        OnClick = BitBtnEndOfDayClick
        Kind = bkIgnore
      end
    end
    object BtnBreak: TBitBtn
      Left = 230
      Top = 2
      Width = 112
      Height = 38
      Caption = 'Pauze'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 5
      OnClick = BtnBreakClick
    end
    object btnLunch: TBitBtn
      Left = 344
      Top = 2
      Width = 112
      Height = 38
      Caption = 'Lunch'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 6
      OnClick = btnLunchClick
    end
  end
  object TimerAutoClose: TTimer
    Enabled = False
    Interval = 10000
    OnTimer = TimerAutoCloseTimer
    Left = 560
    Top = 69
  end
end
