object DialogLoginDM: TDialogLoginDM
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Left = 259
  Top = 230
  Height = 188
  Width = 240
  object odsPimsUser: TOracleDataSet
    SQL.Strings = (
      'SELECT'
      '  PU.USER_NAME,'
      '  PU.GROUP_NAME,'
      '  PU.USER_PASSWORD,'
      '  PU.SET_TEAM_SELECTION_YN'
      'FROM'
      '  PIMSUSER PU'
      'ORDER BY'
      '  PU.USER_NAME')
    QueryAllRecords = False
    CountAllRecords = True
    Session = ORASystemDM.OracleSession
    Left = 40
    Top = 8
  end
  object oqCheckOpenApl: TOracleQuery
    SQL.Strings = (
      'SELECT'
      '  PMG.MENU_NUMBER,'
      '  PMG.VISIBLE_YN,'
      '  PMG.EDIT_YN'
      'FROM'
      '  PIMSMENUGROUP PMG'
      'WHERE'
      '  PMG.GROUP_NAME = :GROUP_NAME AND'
      '  PMG.MENU_NUMBER = :APL_MENU')
    Session = ORASystemDM.OracleSession
    ReadBuffer = 1
    Variables.Data = {
      03000000020000000B0000003A47524F55505F4E414D45050000000000000000
      000000090000003A41504C5F4D454E55030000000000000000000000}
    Left = 40
    Top = 80
  end
end
