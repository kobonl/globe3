(*
  Changes:
    RV001: (Revision 001). Order 550459.
    2.0.139.122 - Bug fix wrong calculation of Overtime-hours. This could
                  happen when also 'exceptional hours' are defined and
                  resulted in wrong overtime-hours (too less).
    RV004: (Revision 004). Overtime-fix.
    2.0.139.125 - Bug fix for Overtime-problem. The overtime is not
                  calculated afterwards, but at the moment it is
                  determined (procedure BooksOvertime).
    RV005: (Revision 005). Oracle-10-fix.
    2.0.139.126 - MRA:13-MAR-2008. Oracle 10 test.
      - During testing for Oracle 10g 10.2.0.1 and error occured:
        ORA-24909 Call in progress. Current operation cancelled.
        This is a problem with the special Oracle DOA-components.
        (Direct Oracle Access).
        This can be solved as follows:
        - Set 'Optimize' to 'False' for TOracleQuery and TOracleDataSet-
          components. In most cases this can happen with
          Insert/Update/Delete-statements, but also with Select-statements.
          components.
       Note: In Pims-applications made in Delphi5 did error did not happen,
             but there the DOA-components are not used.
    RV013. (Revision 013). Round Minutes.
    2.0.139.12x  - Rounding minutes-procedure rewritten, because Except. Hours were not
                   included during rounding of the salary-hours.
    RV016. (Revision 016). Exceptional + Regular hours + non-paid breaks.
    2.0.139.xxx - Non-paid breaks were sometimes subtracted again during calculation of
                  exceptional hours, resulting in too much regular hours when
                  exceptional hours should be calculated.
                  This had to do with overnight-scans that were not recognized
                  correct during determination of breaks, during exceptional hours
                  calculations. Now these scans are split to get the correct result.
    RV023. (Revision 023).
      Detect if exceptional hours are made during the period there are also
      Overtime hours made. This is needed for Paris + Export Paryoll.
      - Database-changes: Table PRODHOURPEREMPLPERTYPE is changed !
        Use script 40.
      Also changed:
      - oqUPHSelect gave an error because it was looking at combination of
        Plant and Shift to get the Hourtype, but when Shifts are defined
        wrong (no shift 1), then it can lead to Prim. Key-violations!
        Used in: UpdateProductionHour().
        The HOURTYPE_NUMBER is now removed from qryProdHour-query, because
        it was not used!
        NOTE: This was only a problem in D5-sources! Not in D7.
      Also changed:
      - Added missing part in FillProdHourPerHourType, that was in D5 but
        not in D7! This deletes 1 record if needed.
      Also changed:
      - Round minutes fix. Salary hour and ProdHourPerEmpPerType
        gave different results after rounding. This is fixed.
    RV024. (Revision 024).
      Problem when determining combination of Exceptional and Overtime.
      Only use the exception hours that were just found! Otherwise
      it gives too much combined hours.
    RV025. (Revision 025). MRA:1-APR-2009.
      An extra check must be made to determine exceptional hours made during
      overtime hours. For this purpose the EXCEPTIONALHOURDEF-table has an
      extra hour-type (OVERTIME_HOUR_TYPE) that can be used to book this type
      of hours. During BooksOvertime these combined hours must be handled.
      This CANCELS parts of RV023 en all of RV024.
    RV028. (Revision 028). MRA:26-MAY-2009. Order-502383.
      Store extra field 'prod_min_excl_break' in ProdHourPerEmplPerType-table.
    RV033.1. MRA:17-SEP-2009.
      - When scan is processed for hour-calculation, check for 'ignore overtime'
        that can be set for the workspot when it has an hourtype has been set.
    RV033.4. MRA:18-SEP-2009.
      - Recalculate ABSENCEHOUR-table for time-for-time hours made during
        overtime. Added a procedure that can do this.
    RV033.5. MRA:21-SEP-2009 Bugfix.
      - CurrentIDCard is not filled before UpdateAbsenceTotal!
        Use AIDCard instead.
    RV039.2. MRA:22-OCT-2009.
      - Corrections made for overtime-hour calculation. This went wrong
        when there were multiple overtime-hour definitions.
    RV039.3. MRA:23-OCT-2009.
      - When hour-type is setup as 'ignore overtime' also ignore
        exceptional hours.
    RV040.1. MRA:29-OCT-2009.
      - Determination of 'lastscanofday' was wrong which could give wrong
        results for rounding of minutes.
    RV040.4. (D5-RV) MRA:11-NOV-2009.
      - PayedBreaks problem.
      - Add the PayedBreaks in ProcessBookings! Or they are not
        included when a scans is equal to a payed break!
             Example: When scan is 8:00-8:10 and payed break is the same,
                      then ProdMin will be 0, so it will not be booked!
    RV044.1. MRA:18-NOV-2009.
      - Detect Overtime: All overtime minutes must be detected. When
        there are multiple overtime-definitions, it must look at all of
        these to detect the overtime!
    RV045.1. MRA:23-NOV-2009. SR-889942.
      - Addition of 2 fields per hourtype related to Overtime:
        - Time for Time
        - Bonus in Money
      This can be used instead of setting on Contract Group-level, but
      only if there is not any contract group with one of these settings = 'Y'.
    MRA:24-FEB-2010. RV055.1. Round Minutes Version 3.
    - Rounding minutes for Salary + ProdHourPerEmplPerType is done in a
      different way. See comments at the new procedures.
    MRA:23-MAR-2010. RV056.1. (D5-revision-number)
    - UnProcessProcessScans:
      - When called from 'ProcessPlannedAbsence' (or 'TimeRecScanning' ?)
        it should always recalculate the Production-hours (PRODHOURPEREMPLOYEE),
        instead of only Salary-hours (and PRODHOURPEREMPLPERTYPE).
        Reason: ProductionHours can be wrong when afterwards e.g. timeblocks,
                breaks are changed.
    MRA:8-APR-2010. RV060.1.
    - Problem with calculation of exceptional hours. There was a bug during
      calculation of breaks, resulting in less exceptional hours.
    MRA:14-JUL-2010. RV063.1.
    - Put back old method of rounding. New method (RV055.1) is wrong!
    MRA:6-OCT-2010. RV071.6. 550497.
    - When the hourtype is set as 'Overtime' and 'Shorter Working Week'
      (ADV in dutch), then all overtime hours that are calculated for that
      Hourtype will also be booked in the balance of the employee as
      earned shorter working week. Earned shorter working week is
      stored per year and per employee in table ABSENCETOTALS in
      the column EARNED_SHORTWWEEK_MINUTE.
    - Also: Delete the old minutes first for ADV. in balance, before the new
            minutes are calculated.
    - Also: Bugfix for part that was deleting TFT-minutes in balance.
    MRA:9-NOV-2010. RV072.2. Bugfix.
    - PHEPT-table
      - Because of rounding the PHEPT-table is not
        correct anymore.
        Rounding is turned off for RFL.
    MRA:3-NOV-2010. RV072.3.
    - Rounding after Book except.hours-routine.
      - Because of rounding after Book exc.hrs. routine,
        it is possible hours were found for exc.hrs. but
        they are removed during rounding.
        Example:
          Rounding is 'truncate 15 min.'.
          12 minutes was found, but after rounding this is 0.
        This means 12 minutes are lost, which can result in losing
        more minutes afterwards.
      - Solution: Solve this by returning the minutes that were left
        after rounding in Rounding-routine.
      - EXTRA: Only round for a given hourtype for 'lost minutes'!
    - Added extra parameters for UpdateSalaryHour.
  MRA:22-NOV-2010. RV073.1. (D5: RV080.4.)
  - Hour Calculation and overtime
    - When hour calculation is done and manual salary hours exists,
      then the calculation gives wrong results.
      Probable cause: When it processes the scans it already takes
      ALL manual hours for the complete overtime-period to determine
      if the overtime-period has been reached.
    - When determining overtime, only look for registered hours (manual
      and absence hours) from the start of the overtime-period till the
      bookingdate!
  MRA:24-NOV-2010. RV073.2. (D5:RV080.7.)
  - The revision RV072.3. (rounding of except. hrs. in-between
    hour calculations), should be disabled, because it gives more
    problems than that it solves anything.
  - Advice: Use 'round 15 minutes' instead of 'trunc 15 minutes',
            because this gives problems with lost minutes.
  MRA:24-NOV-2010. RV073.3. (D5:RV080.6.)
  - Hour Calculation and hourtype on workspot
    - When an hourtype is set to a workspot and a scan
      is processed that is linked to this workspot, then
      it does not always use this workspot-hourtype.
      Only when there were regular hours made, it uses
      this workspot-hourtype.
    - Solution: It should always use this workspot-hourtype!
  MRA:7-DEC-2010 RV074.1. RV073.3. (D5:RV080.4.) !!!CANCELLED!!!
  - Hour Calculation and overtime
  - REMARK:
    - This revision must be turned back to it's old state.
    - Reason: It does not calculate overtime when it should do this,
      for example, when there is absence booked on days after
      the scans.
    - Advice: When manual hours are entered which should be booked
      themselves overtime-hours, then the hourtype for these manual hours
      must be set to 'Ignore overtime'.
  MRA:18-MAY-2011 RV077.1. SO-20011250
  - Timerecording
    - Overtime hours and difference when scans are made manual
      compared with made by timerecording.
      When scans are made manual it gives a different value
      for TFT in balance-counter, when they are made by
      timerecording.
      It looks like the TFT is not rounded/truncated.
    - It does not recalculate the TFT hours, like in Pims (D5).
    - Note: It also does not recalculate SWW (ADV) hours.
    - Added some procedures from D5-GlobalDMT:
      - RecalcEarnedSWWHours
      - RecalculateBalances
  MRA:1-NOV-2011 RV082.1. (D5: RV100.1) 20012124. Tailor made CVL.
  - Hour calculation
    - When 'overruled contract group' is not null for the
      workspot of the scan that is about to be calculated,
      then use that overruled contract group instead of the
      employee-contract group to book the hours for
      exceptional, overtime and regular hour.
  - Changed queries (they now search on contractgroup instead by employee):
    - oqExceptHourDef
    - oqComputeOvertimePeriod
    - oqOvertimeDef (renamed from: oqBOTSelectOvertimeMin)
    - oqContractGroup
  MRA:5-DEC-2011 RV084.1./RV103.1. SO-20011799. Worked hours during bank holiday.
  - Modification calculation worked hours on bank holiday.
    When hours were made during a bank holiday, then these
    hours must be booked on an hourtype that is defined
    for that bank holiday or on the contract group.
    During this the availability must be changed from
    bank holiday to available and the hours that were booked
    for bank holiday must be reprocessed.
  MRA:1-OCT-2012 20013489 Overnight-Shift-System.
  - Changes for Overnight-shift-system.
  MRA:23-OCT-2012 20013489 Overnight-Shift-System.
  - Always store production on start of shift.
  MRA:3-JAN-2012 20013489.10. Overnight-Shift-System.
  - When called from Timerecording or Personal Screen, only add the
    production hours, do no recalculate them!
  MRA:10-OCT-2013 20014327
  - Make use of Overnight-Shift-System optional.
  MRA:5-NOV-2013 TD-23416 (PCO)
  - Missing hours:
    - Sometimes salary hours are not booked. This can happen with a scan like:
      - Sunday -> DateIn=2:30 DateOut=9:00
      - This should be booked on previous day (Saturday), but then it can happen
        it is not booked at all, because it is part of the previous week-period,
        resulting in missing hours.
      - To solve it, book it on the same day of the scan.
    - Also when there is a scan like:
      - Saturday -> DateIn=14:30 DateOut=2:30 (next day!)
      - Then this can result in no booking of production hours on the next day,
        because a part falls in one period and the last part falls in next
        period.
  MRA:13-NOV-2013 TD-23386
  - Hourtype on shift should not result in double hours.
    - PopulateIDCard: Also determine hourtype on shift
      - Related to this: Also determine hourtype_number of workspot
      - Related to this: Also determine Ignore_overtime_yn
    - See: BooksHoursForShift-procedure
      - This procedure is NOT used anymore. It is handled in a different way.
  MRA:13-DEC-2013 TD-23817  Time correction needs to be removed from system
  - Hour calculation
    - It can happen because of rounding a scan results in an extra salary line
      of -15 minutes, when there are several different hour type involved.
    - Cause: It books negative regular hours that are left afterwards. This
      must be prevented! These must not be booked at all.
      NOTE: Manual negative hours must still be accepted.
  MRA:10-NOV-2014 20014826
  - Log error messages optionally to database.
  MRA:17-MAR-2015 20015346
  - Store scans in seconds:
    - Round scans to minutes, before hours are calculated
  - Important:
    - In some parts we must NOT round scans to minutes, or we will not
      found a previous scan correctly.
      However, for hour-calculations we must round the scans before we
      start the calculations.
  MRA:23-JUN-2015 PIM-49
  - NOTE: When 'Shift Date System' is NOT used.
  - Not all scan / production time are included in report hours per employee.
  - When using Period=Day in Contract Group then production hours are not
    calculated correct! It gives less production hours when there are
    scans involved that go through the night.
  - Cause: It calculates the production hours based on overnight-scan and
           stores it in 2 days, but during calculation it deletes the second
           day, so that hours are missing later, resulting in less hours.
  - Note: The salary hours are correct.
  - After a fix was done:
    - Now it goes OK but only when you use Process Planned Absence and
      recalculate a week-period. When only 1 day is recalculated then it still
      can go wrong (via Process Planned Absence or via
      Time Reg. Scanning-dialog).
  - A function has been added: ProdDateCheck. This checks if a prod-date is
    part of a scan (that can start on one day and end on next day). This
    prevents hours are not calculated because of a scan that ends on next day,
    but the hours till midnight should be calculated as production hours.
  - Example of what went wrong:
            TU  (30-6)                  WE (1-7)           TH (2-7)        FR (3-7)
    |---------------------|--------------------------|-------------------|------------------|
    |Scan 22-02:30 |-------------|   22-02:30 |------------| 22-02:30|-----------|
    |--------------------------|--------------------------|--------------------------|
    |Prod.     2:00|-----|||-----|2:30    2:00|-----|||-----|2:30     2:00|----|||----|
    |--------------------------|--------------------------|--------------------------|
    |Sal.      4:30|-------------|         4:30|-------------|        4:30|-----------|
    |--------------------------|--------------------------|--------------------------|
    Here, when scan of 1-7 22:00-02:30 was changed to 22:00-01:30 it resulted
    in less prod. hours on 2-7 because the scan of 2-7 22:00-02:30 was not taken
    into account. Sal. hours were OK.
  MRA:17-SEP-2015 PIM-87
  - Personal Screen Slowness
  - Use optionally (based on ORASystemDM.UsePackageHourCalc) a package
    for hour-calculation to improve the performance.
  - Related to this order:
    - Changed qryExceptHourDef-query (sorting added).
  - Add Debug-variable to make it possible to turn this on/off from Personal
    Screen's INI-file.
  - Add extra exceptions when calling package
  - Add extra debug-info for params of package
  MRA:14-MAR-2016 PIM-12
  - Addition of NormProdLevel-field for ScannedIDCard, PopulateIDCard has
    been changed to fill this field.
  MRA:24-MAY-2016 PIM-181
  - Allow only single employee on workspot
    - Also get Measure productivity via PopulateIDCard.
  MRA:23-OCT-2017 PIM-319
  - Do not subtract breaks from production hours
  MRA:12-FEB-2018 GLOB3-81
  - Change for overtime hours (only used for NTS)
  - Add from-to-time as extra restriction for day-period
  - Add day-of-week as extra setting.
  MRA:23-MAR-2018 GLOB3-81 Rework
  - See above.
  - Because of new from-to-time-settings, we need to know at what time the
    overtime started! This is needed when there are multiple overtime lines
    made like:
    - 1: MO: 7:30 - 23:59 - From 0:00 to 7:00 Hourtype=25
    - 2: MO: 7:30 - 23:59 - From 7:00 to 15:00 Hourtype=26
    - 3: MO: 7:30 - 23:59 - From 15:00 to 23:59 Hourtype=27
  - We have to prevent it takes the first line because in that case there will
    no overtime yet, because it did not happen during that time.
  - Because of this, first check if the date-out-time is within the from-to
    that must be checked when it is in overtime. Only if that is the case,
    then check further, otherwise skip the overtime-line and try the
    next one.
  MRA:5-NOV-2018 PIM-406
  - Scans made via timerecording result all in overtime
  - Cause: The above change (GLOB3-81 Rework) was not done for D7-part.
  MRA:2-JUL-2018 GLOB3-60
  - Extension 4 to 10 blocks for planning
  MRA:9-NOV-2018 PIM-408
  - Problem with exceptional hour calculation
  MRA:3-DEC-2018 PIM-406
  - Overtime gives a wrong result.
  - Example:
    - 1: MO:  7:30 - 10:30 - From 00:00 to 23:59
    - 2: MO: 10:30 - 24:00 - From 00:00 to 23:59
    Result should be:
    When there are 12:00 hours made.
    Reg.Min | SMin  | EMin  | OTime
    12:00   | 7:30  | 10:30 | 3:00
    12:00   | 10:30 | 24:00 | 1:30
    Regular time: 7:30.
  MRA:1-MAR-2019 GLOB3-223
  - Restructure availability of functionality made for NTS
*)

unit GlobalDMT;

interface

uses
  SysUtils, Classes, DB, UScannedIDCard, Oracle, DateUtils,
  OracleData, Dialogs, DBClient, ORASystemDMT, UPimsConst;

type
  TTimeBlock = array [1..MAX_TBS] of Boolean;

type
  TGlobalDM = class(TDataModule)
    oqCopyThis: TOracleQuery;
    oqComputeOvertimePeriod: TOracleQuery;
    oqWork: TOracleQuery;
    oqUSHSelect: TOracleQuery;
    oqUSHUpdate: TOracleQuery;
    oqUSHInsert: TOracleQuery;
    oqUSHDelete: TOracleQuery;
    oqFPHSelect: TOracleQuery;
    oqFPHUpdate: TOracleQuery;
    oqFPHInsert: TOracleQuery;
    oqBHFSSelect: TOracleQuery;
    oqBRSHSelect: TOracleQuery;
    oqDEBOSelect: TOracleQuery;
    oqBOTSelectTotMinutes: TOracleQuery;
    oqOvertimeDef: TOracleQuery;
    oqUATSelect: TOracleQuery;
    oqUATUpdate: TOracleQuery;
    oqUATInsert: TOracleQuery;
    oqPBFCSelect: TOracleQuery;
    oqCRMSelect: TOracleQuery;
    oqDPHDelete: TOracleQuery;
    oqWorkspot: TOracleQuery;
    oqUPHSelect: TOracleQuery;
    oqUPHInsert: TOracleQuery;
    oqUPHDelete: TOracleQuery;
    oqUPHUpdate: TOracleQuery;
    oqUELSelect: TOracleQuery;
    oqUELInsert: TOracleQuery;
    oqUELUpdate: TOracleQuery;
    oqUPSGetTFT_Bonus: TOracleQuery;
    oqUPSDeleteSalary: TOracleQuery;
    oqUPSDeleteProduction: TOracleQuery;
    oqUPSDeleteAbsenceHours: TOracleQuery;
    odsWork: TOracleDataSet;
    oqContractgroup: TOracleQuery;
    oqSalaryOvertimeMin: TOracleQuery;
    oqPopIDCard: TOracleQuery;
    oqFPHDelOneRec: TOracleQuery;
    oqPEPTSelect: TOracleQuery;
    oqPEPTUpdate: TOracleQuery;
    oqPEPTDelete: TOracleQuery;
    oqExceptHourDef: TOracleQuery;
    oqPEPTEBSelect: TOracleQuery;
    oqPEPTEBUpdate: TOracleQuery;
    oqGetTFTHours: TOracleQuery;
    oqCheckEmpTFT: TOracleQuery;
    oqContractGroupTFT: TOracleQuery;
    oqSHEround: TOracleQuery;
    cdsSHEprio: TClientDataSet;
    cdsSHEprioSALARY_DATE: TDateField;
    cdsSHEprioEMPLOYEE_NUMBER: TIntegerField;
    cdsSHEprioHOURTYPE_NUMBER: TIntegerField;
    cdsSHEprioMANUAL_YN: TStringField;
    cdsSHEprioSALARY_MINUTE: TIntegerField;
    cdsSHEprioPRIORITY: TIntegerField;
    cdsSHEprioSALARY_MINUTE_ROUNDED: TIntegerField;
    cdsSHEprioMINUTE_MOD_PART: TIntegerField;
    cdsPHEPTprio: TClientDataSet;
    oqPHEPTround: TOracleQuery;
    cdsPHEPTprioPRODHOUREMPLOYEE_DATE: TDateField;
    cdsPHEPTprioPLANT_CODE: TStringField;
    cdsPHEPTprioSHIFT_NUMBER: TIntegerField;
    cdsPHEPTprioEMPLOYEE_NUMBER: TIntegerField;
    cdsPHEPTprioWORKSPOT_CODE: TStringField;
    cdsPHEPTprioJOB_CODE: TStringField;
    cdsPHEPTprioMANUAL_YN: TStringField;
    cdsPHEPTprioHOURTYPE_NUMBER: TIntegerField;
    cdsPHEPTprioPRODUCTION_MINUTE: TIntegerField;
    cdsPHEPTprioPROD_MIN_EXCL_BREAK: TIntegerField;
    cdsPHEPTprioMINUTE_MOD_PART: TIntegerField;
    cdsPHEPTprioPRODUCTION_MINUTE_ROUNDED: TIntegerField;
    cdsPHEPTprioPRIORITY: TIntegerField;
    oqAbsenceTotal: TOracleQuery;
    oqGetSWWHours: TOracleQuery;
    oqWorkspotContractGroup: TOracleQuery;
    oqHTONBK: TOracleQuery;
    oqOvertimeParams: TOracleQuery;
    oqAHE: TOracleQuery;
    oqAHEDelete: TOracleQuery;
    oqEmpAvUpd1: TOracleQuery;
    oqEmpAvUpd2: TOracleQuery;
    oqEmpAvUpd3: TOracleQuery;
    oqEmpAvUpd4: TOracleQuery;
    oqUATM: TOracleQuery;
    oqTRSUpdate: TOracleQuery;
    oqShiftHT: TOracleQuery;
    oqUnProcessProcessScans: TOracleQuery;
    oqProcessTimeRecording: TOracleQuery;
    oqEmpAvUpd5: TOracleQuery;
    oqEmpAvUpd6: TOracleQuery;
    oqEmpAvUpd7: TOracleQuery;
    oqEmpAvUpd8: TOracleQuery;
    oqEmpAvUpd9: TOracleQuery;
    oqEmpAvUpd10: TOracleQuery;
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
    FStandardMinutesPerMonth: Integer;
    FPayedBreakSave: Integer;
    FDebug: Boolean;
    FOvertimeType: TOvertime;
    function DetectExceptHourDefDuringOvertimeHourtype( {RV025}
      AIDCard: TScannedIDCard;
      AExceptRegularMin: Integer): boolean;
    function DetectOvertimeNonOvertime(ABookingDay: TDateTime; {RV025}
      AWorkedMin: Integer; AIDCard: TScannedIDCard;
      AManual: String;
      VAR ANonOvertimeMin: Integer;
      VAR AOvertimeMin: Integer): boolean;
    function BooksOverTimeMain(ABookingDay: TdateTime; {RV025}
      AWorkedMin: Integer; AIDCard: TScannedIDcard;
      AManual: String;
      ACombinedHourSystem: Boolean;
      ADetect: Boolean;
      VAR ANonOvertimeMin: Integer;
      VAR AOvertimeMin: Integer
      ): Integer;
    function BooksOverTime(ABookingDay: TdateTime;
      AWorkedMin: Integer; AIDCard: TScannedIDcard; AManual: String): Integer;
    function BooksOverTimeCombinedHours(ABookingDay: TdateTime; {RV025}
      AWorkedMin: Integer; AIDCard: TScannedIDcard;
      AManual: String
      ): Integer;
    function OverTimeDetect(ABookingDay: TdateTime; {RV025}
      AWorkedMin: Integer; AIDCard: TScannedIDcard;
      AManual: String;
      VAR ANonOvertimeMin: Integer;
      VAR AOvertimeMin: Integer
      ): Integer;
    function BooksExceptionalTimeMain( {RV025}
      ABookingDay: TDateTime;
      AExceptRegularMin: Integer;
      AIDCard: TScannedIDcard;
      AManual: String;
      ADuringOvertime: Boolean): Integer;
    function BooksExceptionalTime( {RV025}
      ABookingDay: TDateTime;
      AExceptRegularMin: Integer; AIDCard: TScannedIDcard;
      AManual: String
      ): Integer;
    function BooksExceptionalTimeDuringOvertime( {RV025}
      ABookingDay: TDateTime;
      AExceptRegularMin: Integer; AIDCard: TScannedIDcard;
      AManual: String
      ): Integer;
{    procedure BooksHoursForShift(ABookingDay: TDateTime;
      AIDCard: TScannedIDCard; AProdMinPlusPayedBreaks: Integer;
      AManual: String); }
    procedure BooksRegularSalaryHours(ABookingDay: TDateTime;
      AIDCard: TScannedIDcard; ABookMinutes: Integer; AManual: String);
    procedure DeleteProdHourForAllTypes(AIDCard: TScannedIDCard;
      AStartDate, AEndDate: TDateTime;  AManual: String);
    function DetermineExceptionalBeforeOvertime(AIDCard: TScannedIDCard): Boolean;
    procedure FillProdHourPerHourType(AProductionDate: TDateTime;
      AIDCard: TScannedIDCard;
      AHourTypeNumber, AMinutes: Integer; AManual: String;
      AInsertProdHrs: Boolean);
    procedure ProcessBookings(ABookingDay: TDateTime; AIDCard: TScannedIDCard;
      AUpdateProdMin: Boolean; ARoundMinutes: Boolean;
      AProdMin, APayedBreaks: Integer; AManual: String);
    function AbsenceTotalSelect(AFieldName: String; ABookingDay: TDateTime;
      AEmployeeNumber: Integer; var AMinutes: Double): Boolean;
    procedure AbsenceTotalInsert(AFieldName: String; ABookingDay: TDateTime;
      AEmployeeNumber: Integer; AMinutes: Double);
    procedure AbsenceTotalUpdate(AFieldName: String; ABookingDay: TDateTime;
      AEmployeeNumber: Integer; AMinutes: Double);
    function UpdateAbsenceTotal(AFieldName: String;
      ABookingDay: TDateTime; AEmployeeNumber: Integer;
      AMinutes: Double; AOverwrite: Boolean): Integer;
{    function UpdateAbsenceTotal(ABookingDay: TDateTime;
      AIDCard: TScannedIDCard; AMinutes: Double): Integer; }
    procedure UpdateEmployeeLevel(const AIDCard: TScannedIDCard);
{    function UpdateProductionHourXXX(AProductionDate: TDateTime;
      AIDCard: TScannedIDCard; AMinutes, APayedBreaks: Integer;
      AManual: String): Integer; }
    function UpdateProductionHour(AProductionDate: TDateTime;
      AIDCard: TScannedIDCard; AMinutes, APayedBreaks: Integer;
      AManual: String): Integer;
    // RV055.1. RV063.1.
(*
    procedure ContractRoundMinutesSalaryV3(
      AEmployeeIDCard: TScannedIDCard; ABookingDay: TDateTime);
    procedure ContractRoundMinutesPHEPT(AEmployeeIDCard: TScannedIDCard;
      ABookingDay: TDateTime);
*)
    function FindWorkspotOverruledContractGroup(APlantCode,
      AWorkspotCode, ADefaultContractGroupCode: String): String;
    procedure FindContractGroup(var AIDCard: TScannedIDCard);
  public
    { Public declarations }
    procedure ComputeOvertimePeriod(AIDCard: TScannedIDCard;
      var AStartDate, AEndDate: TDateTime);
    procedure PopulateIDCard(var AIDCard: TScannedIDCard; ADataSet: TDataSet;
      ARoundToMinutes: Boolean=False);
    procedure ProcessTimeRecording(AIDCard, ADeleteIDCard: TScannedIDCard;
      AFirstScan, ALastScan, AUpdateSalMin, AUpdateProdMinOut, AUpdateProdMinIn,
      ADeleteAction, ALastScanRecordForDay: Boolean);
    procedure UnprocessProcessScans(AIDCard: TScannedIDCard;
      AFromDate, AProdDate1, AProdDate2, AProdDate3, AProdDate4: TDateTime;
      AProcessSalary, ADeleteAction: Boolean;
      ARecalcProdHours: Boolean;
      AFromProcessPlannedAbsence: Boolean=False;
      AManual: Boolean=False); // 20013489 Call with True when it is about manual hours!
    function UpdateSalaryHour(ASalaryDate: TDateTime; AIDCard: TScannedIDCard;
      AHourTypeNumber, AMinutes: Integer; AManual: String;
      AReplace: Boolean;
      UpdateYes: Boolean; (* RV023 *)
      FromManualProd: String = 'N';
      ARoundMinutes: Boolean = True): Integer;
    procedure RoundMinutesProdHourPerEmpPerType( (* RV023 *) // RV055.1. // RV063.1.
      ABookingDay: TDateTime;
      AEmployeeData: TScannedIDCard;
      AHourTypeNumber, AMinutes: Integer;
      AManual: String);
    procedure RecalcEarnedTFTHours(AEmployeeNumber: Integer;
      ADateFrom: TDatetime; ADateTo: TDateTime);
    procedure RecalcEarnedSWWHours(AEmployeeNumber: Integer;
      ADateFrom: TDatetime; ADateTo: TDateTime);
    procedure RecalculateBalances(AEmployeeNumber: Integer;
      ADateFrom, ADateTo: TDateTime);
    function ContractGroupTFTExists: Boolean;
    procedure UpdateEMA(AAbsenceReason: String;
      AEmployeeIDCard: TScannedIDCard; AStartDate: TDateTime;  AMore: Integer);
    procedure DetermineTimeForTimeSettings(ADataSet: TOracleQuery;
      var ATimeForTimeYN, ABonusInMoneyYN,
      AShorterWorkingWeekYN: String); // RV084.1.
    function DetermineTimeForTime(ADataSet: TOracleQuery; AEarnedTimeTFT: Double;
      AEarnedMin: Integer): Double; // RV084.1.
    function DetermineShorterWorkingWeek(ADataSet: TOracleQuery;
      AEarnedTimeSWW: Double; AEarnedMin: Integer): Double; // RV084.1.
    procedure BooksTFT(ABookingDay: TDateTime;
      AEmployeeData: TScannedIDCard; AEarnedTimeTFT: Double); // RV084.1.
    procedure BooksSWW(ABookingDay: TDateTime;
      AEmployeeData: TScannedIDCard; AEarnedTimeSWW: Double); // RV084.1.
    function BooksWorkedHoursOnBankHoliday(ABookingDay: TDateTime;
      AWorkedMin: Integer; AEmployeeData: TScannedIDcard;
      AManual: String): Integer; // RV084.1.
    function AbsenceTotalField(const AAbsenceTypeCode: Char): String;
    procedure UpdateAbsenceTotalMode(AEmployeeNumber,
      AAbsenceYear: Integer; AMinutes: Double; AAbsenceTypeCode: Char;
      AAddToOldMinutes: Boolean = True);
    procedure UpdateTimeRegScanning(AIDCard: TScannedIDCard;
      AShiftDate: TDateTime);
    property StandardMinutesPerMonth: Integer read FStandardMinutesPerMonth
      write FStandardMinutesPerMonth;
    property PayedBreakSave: Integer read FPayedBreakSave
      write FPayedBreakSave; // RV028.
    property Debug: Boolean read FDebug write FDebug; // PIM-87
    property OvertimeType: TOvertime read FOvertimeType write FOvertimeType; // GLOB3-81
  end;

var
  GlobalDM: TGlobalDM;

implementation

uses
  CalculateTotalHoursDMT, UGlobalFunctions;

{$R *.dfm}

{ TGlobalDM }

// RV025.
// Detect if the Exceptional Hour Definition for the employee has
// an hour-type defined for OVERTIME_HOUR_TYPE.
function TGlobalDM.DetectExceptHourDefDuringOvertimeHourtype(
  AIDCard: TScannedIDCard;
  AExceptRegularMin: Integer): boolean;
var
  StartTime, EndTime: TDateTime;
begin
  Result := False;

  StartTime := AIDCard.DateInCutOff;
  EndTime := StartTime + AExceptRegularMin/DayToMin;
  FindContractGroup(AIDCard); // RV082.1.
  with oqExceptHourDef do
  begin
    // RV025. The query is now put in the component.
    DeleteVariables;
    // RV082.1. It now looks directly for contractgroup, not by employee.
    DeclareAndSet('CONTRACTGROUP_CODE', otString, AIDCard.ContractgroupCode);
//    DeclareAndSet('EMPLOYEE_NUMBER', otInteger, AIDCard.EmployeeCode);
    DeclareAndSet('START_TIME', otDate, StartTime);
    DeclareAndSet('END_TIME', otDate, EndTime);
    DeclareAndSet('DAY_OF_WEEK', otInteger,
      DayInWeek(ORASystemDM.WeekStartsOn, StartTime));
    DeclareAndSet('WEEKDAYSTART', otInteger,
      DayInWeek(ORASystemDM.WeekStartsOn, StartTime));
    DeclareAndSet('WEEKDAYEND', otInteger,
      DayInWeek(ORASystemDM.WeekStartsOn, EndTime));
    Execute;
    if not Eof then
    begin
      if FieldAsString('OVERTIME_HOURTYPE_NUMBER') <> '' then
        Result := True;
    end;
  end;
end;

// RV025.
// Detect if there are overtime/non-overtime minutes made.
// This means the point at which overtime is starting is detected.
function TGlobalDM.DetectOvertimeNonOvertime(ABookingDay: TDateTime;
  AWorkedMin: Integer; AIDCard: TScannedIDCard;
  AManual: String;
  VAR ANonOvertimeMin: Integer;
  VAR AOvertimeMin: Integer): boolean;
begin
  Result := DetectExceptHourDefDuringOvertimeHourtype(
    AIDCard, AWorkedMin);
  if Result then
  begin
    ANonOvertimeMin := 0;
    AOvertimeMin := 0;
    OvertimeDetect(ABookingDay, AWorkedMin, AIDCard,
      AManual, ANonOvertimeMin, AOvertimeMin);
    Result := (AOvertimeMin <> 0);
  end;
end;

function TGlobalDM.BooksExceptionalTimeMain(ABookingDay: TDateTime;
  AExceptRegularMin: Integer; AIDCard: TScannedIDcard;
  AManual: String;
  ADuringOvertime: Boolean): Integer;
var
  StartTime, EndTime, STime, ETime: TDateTime;
  MyHourTypeNumber, ProdMin, BreaksMin, PayedBreaks, IntersectionMin: Integer;
  DayOfWeek: Integer;
  // RV060.1. New procedure
  procedure DetermineBreaks(AEndTime: TDateTime;
    var ABreaksMin, APayedBreaks: Integer);
  var
    BreaksMinTotal, PayedBreaksTotal: Integer;
    DateIn, DateOut: TDateTime;
  begin
    DateIn := AIDCard.DateIn;
    DateOut := AEndTime;
    BreaksMinTotal := 0;
    PayedBreaksTotal := 0;
    repeat
      // Determine breaks for this scan.
      AProdMinClass.ComputeBreaks(AIDCard, DateIn, DateOut,
        1, ProdMin, ABreaksMin, APayedBreaks);
      BreaksMinTotal := BreaksMinTotal + ABreaksMin;
      PayedBreaksTotal := PayedBreaksTotal + APayedBreaks;
      // Add found breaks to end-time of scan.
      // Only check for breaksmin here!
      if (ABreaksMin > 0) then
      begin
        DateIn := DateOut;
        DateOut := DateOut + (ABreaksMin / 60 / 24);
      end;
    until (ABreaksMin <= 0);
    ABreaksMin := BreaksMinTotal;
    APayedBreaks := PayedBreaksTotal;
  end;
  procedure DetermineBreaksOvernightScan(AEndTime: TDateTime;
    var ABreaksMin, APayedBreaks: Integer);
  var
    SaveDateIn, SaveDateOut, SaveEndTime: TDateTime;
    MyBreaksMinTotal, MyPayedBreaksTotal: Integer;
    OvernightScan: Boolean;
  begin
    OvernightScan := Trunc(StartTime) <> Trunc(AEndTime);
    MyBreaksMinTotal := 0;
    MyPayedBreaksTotal := 0;
    SaveDateIn := AIDCard.DateIn;
    SaveDateOut := AIDCard.DateOut;
    SaveEndTime := AEndTime;
    if OvernightScan then
    begin
      AIDCard.DateOut := Trunc(AIDCard.DateOut);
      AEndTime := AIDCard.DateOut;
      DetermineBreaks(AEndTime, ABreaksMin, APayedBreaks);
      MyBreaksMinTotal := ABreaksMin;
      MyPayedBreaksTotal := APayedBreaks;
      AIDCard.DateIn := AIDCard.DateOut;
      AIDCard.DateOut := SaveDateOut;
      AEndTime := SaveEndTime;
    end;
    DetermineBreaks(AEndTime, ABreaksMin, APayedBreaks);
    MyBreaksMinTotal := MyBreaksMinTotal + ABreaksMin;
    MyPayedBreaksTotal := MyPayedBreaksTotal + APayedBreaks;
    if OvernightScan then
    begin
      AIDCard.DateIn := SaveDateIn;
      AIDCard.DateOut := SaveDateOut;
    end;
    ABreaksMin := MyBreaksMinTotal;
    APayedBreaks := MyPayedBreaksTotal;
  end;
begin
  // MR:03-12-2003 Use 'cutoff'-datein-time
  StartTime := AIDCard.DateInCutOff;
  EndTime   := StartTime + AExceptRegularMin / DayToMin;

  // RV073.3. (D5:RV080.6.)
  // When this scan has workspot with an hourtype assigned, then
  // do not check on overtime.
  if AIDCard.WSHourtypeNumber <> -1 then
  begin
    Result := AExceptRegularMin;
    Exit;
  end;

  // TD-23386 If there is an hourtype defined on shift-level, then
  //          use that for booking hours.
  if AIDCard.ShiftHourtypeNumber <> -1 then
  begin
    Result := AExceptRegularMin;
    Exit;
  end;

  // RV039.3. When hour-type is setup as 'ignore overtime' also ignore
  //          exceptional hours.
  // When this scan has workspot with hourtype and 'ignore overtime' is YES,
  // then there is not need to check for overtime.
  if AIDCard.IgnoreForOvertimeYN = 'Y' then
  begin
    Result := AExceptRegularMin;
    Exit;
  end;


  // MRA:16-JAN-2009 RV020. Scans that are overnight-scans must be
  //                        split in 2 !
  //                        If not, then it will not always find the
  //                        correct breaks !
  DetermineBreaksOvernightScan(EndTime, BreaksMin, PayedBreaks);

  EndTime := EndTime + (Breaksmin - PayedBreaks)/DayToMin; //Unpaid Breaks.
  // MR:15-09-2004 RoundTime is a function, so 'endtime' will never
  // be rounded + also second argument should be 1 to round to minutes.
  Result  := AExceptRegularMin;
  FindContractGroup(AIDCard); // RV082.1.
  with oqExceptHourDef do
  begin
    // RV025. The query is now put in the component.
    DeleteVariables;
    // RV082.1. It now looks directly for contractgroup, not by employee.
    DeclareAndSet('CONTRACTGROUP_CODE', otString, AIDCard.ContractgroupCode);
    DeclareAndSet('START_TIME', otDate, StartTime);
    DeclareAndSet('END_TIME', otDate, EndTime);
    DeclareAndSet('DAY_OF_WEEK', otInteger,
      DayInWeek(ORASystemDM.WeekStartsOn, StartTime));
    DeclareAndSet('WEEKDAYSTART', otInteger,
      DayInWeek(ORASystemDM.WeekStartsOn, StartTime));
    DeclareAndSet('WEEKDAYEND', otInteger,
      DayInWeek(ORASystemDM.WeekStartsOn, EndTime));
    Execute;
    while not Eof do
    begin
      DayOfWeek        := FieldAsInteger('DAY_OF_WEEK');
      if (ADuringOvertime) then {RV025}
      begin
        if FieldAsString('OVERTIME_HOURTYPE_NUMBER') <> '' then
          MyHourTypeNumber := FieldAsInteger('OVERTIME_HOURTYPE_NUMBER')
        else
          MyHourTypeNumber := FieldAsInteger('HOURTYPE_NUMBER');
      end
      else
        MyHourTypeNumber := FieldAsInteger('HOURTYPE_NUMBER');
      STime            := FieldAsDate('STARTTIME');
      ETime            := FieldAsDate('ENDTIME');
      // MR:05-11-2003
      // Because a scan can go from one day to another, here
      // we check which day we must use for comparing.
      if DayOfWeek = DayInWeek(ORASystemDM.WeekStartsOn, StartTime) then
      begin
        ReplaceDate(STime, StartTime);
        ReplaceDate(ETime, StartTime);
      end
      else
      begin
        // PIM-408 We need to know the date related to DayOfWeek
        //         Try next day, related to StartTime
        STime := Trunc(StartTime+1) + Frac(STime);
        ETime := Trunc(StartTime+1) + Frac(ETime);
        // PIM-408 This can give a wrong result!
//        ReplaceDate(STime, EndTime);
//        ReplaceDate(ETime, EndTime);
      end;
      // MR:07-11-2003 Correct midnight-enddate
      ETime := MidnightCorrection(STime, ETime);
      IntersectionMin := ComputeMinutesOfIntersection(StartTime, EndTime,
        STime, ETime);
      if IntersectionMin > 0 then
      begin
        // MR:13-06-2003 Start
        if STime < StartTime then
          STime := StartTime;
        if ETime > EndTime then
          ETime := EndTime;
        if STime > ETime then
          STime := ETime;
        // MR:13-06-2003 End
        // Compute the UnPayed breaks min in interval
        // MR: 25-09-2003
        AProdMinClass.ComputeBreaks(AIDCard, STime, ETime, 0, ProdMin,
          BreaksMin, PayedBreaks);
        IntersectionMin := IntersectionMin - BreaksMin + PayedBreaks;
        if IntersectionMin > 0 then
        begin
          UpdateSalaryHour(ABookingDay, AIDCard,
            MyHourTypeNumber, IntersectionMin, AManual, False,
            True);

          // RV073.2. Disabled.
{
          // RV072.3. Round only for this hourtype and keep lost minutes during
          //          rounding.
          SalaryStored := True;
          LostMinutes := LostMinutes +
            CalculateTotalHoursDM.ContractRoundMinutes(AIDCard, ABookingDay,
              MyHourTypeNumber);
}
          // MR:12-06-2003 Included in the 'if'.
          //               To prevent miscalculation if 'IntersectionMin' is
          //               negative.
          Result := Result - IntersectionMin;
        end;
      end;
      Next;
    end; {while not Eof}
  end; {with oqExceptHourDef}
  // RV073.2. Disabled.
{
  // RV072.3. Return any lost minutes.
  if SalaryStored then
  begin
    Result := Result + (-1 * LostMinutes);
  end;
}  
end; // BooksExceptionalTimeMain

// RV025. Book the exceptional hours (not made during overtime)
function TGlobalDM.BooksExceptionalTime(
  ABookingDay: TDateTime;
  AExceptRegularMin: Integer; AIDCard: TScannedIDcard;
  AManual: String
  ): Integer;
begin
  Result := BooksExceptionalTimeMain(
    ABookingDay, AExceptRegularMin, AIDCard,
    AManual,
    False // ADuringOvertime
    );
end;

// RV025. Book the exceptional hours (made during overtime)
// This must be booked using a different hourtype that is defined
// in EXCEPTIONALHOURDEF, field OVERTIME_HOUR_TYPE.
function TGlobalDM.BooksExceptionalTimeDuringOvertime(
  ABookingDay: TDateTime;
  AExceptRegularMin: Integer; AIDCard: TScannedIDcard;
  AManual: String
  ): Integer;
begin
  Result := BooksExceptionalTimeMain(
    ABookingDay, AExceptRegularMin, AIDCard,
    AManual,
    True // ADuringOvertime
    );
end;

// TD-23386 Do NOT use this anymore! It is handled in a different way.
(*
procedure TGlobalDM.BooksHoursForShift(ABookingDay: TDateTime;
  AIDCard: TScannedIDCard; AProdMinPlusPayedBreaks: Integer;
  AManual: String);
begin
  if AProdMinPlusPayedBreaks > 0 then
  begin
    with oqBHFSSelect do
    begin
      ClearVariables;
      SetVariable('PLANT_CODE',            AIDCard.PlantCode);
      SetVariable('SHIFT_NUMBER',          AIDCard.ShiftNumber);
      Execute;
      if not Eof then
      begin
        if not FieldIsNull('HOURTYPE_NUMBER') then
          UpdateSalaryHour(ABookingDay, AIDCard,
            FieldAsInteger('HOURTYPE_NUMBER'),
            AProdMinPlusPayedBreaks, AManual, False{ Don't replace! },
            True);
      end; {Eof}
    end; {with oqBHFSSelect}
  end; {AProdMinPlusPayedBreaks > 0}
end;
*)

function TGlobalDM.BooksOverTimeMain(ABookingDay: TdateTime;
  AWorkedMin: Integer;
  AIDCard: TScannedIDcard;
  AManual: String;
  ACombinedHourSystem: Boolean;
  ADetect: Boolean;
  VAR ANonOvertimeMin: Integer;
  VAR AOvertimeMin: Integer
  ): Integer;
var
  MyStartMin, MyEndMin, MyHourTypeNumber(*, MyTotalWorkedMin*),
  MyEarnedMin, MyRegisteredMin, MyOverTimeMin, MyTotOverTime: Integer;
  MyStartDate, MyEndDate: TDateTime;
//  MyPercentage: Double; // RV071.6.
// RV039.2. Do not use! Gives wrong result for overtime!
//  MySalaryOvertimeMin: Integer; // MRA:06-MAR-2008 RV004. Overtime-fix.
  MyBookedMin: Integer; {RV025}
  EarnedTimeTFT: Double; // RV071.6. TFT=Time for Time
  EarnedTimeSWW: Double; // RV071.6. SWW=ShorterWorkingWeek
  FromTime, ToTime: TDateTime; // GLOB3-81
  // RV084.1.
(*
  // RV045.1.
  procedure DetermineTimeForTimeSettings;
  begin
    with oqOvertimeDef do
    begin
      if ContractGroupTFTExists then
      begin
        TimeForTimeYN := FieldAsString('TIME_FOR_TIME_YN');
        BonusInMoneyYN := FieldAsString('BONUS_IN_MONEY_YN');
      end
      else
      begin
        if FieldAsString('HT_OVERTIME_YN') = CHECKEDVALUE then
        begin
          TimeForTimeYN := FieldAsString('HT_TIME_FOR_TIME_YN');
          BonusInMoneyYN := FieldAsString('HT_BONUS_IN_MONEY_YN');
        end
        else
        begin
          TimeForTimeYN := UNCHECKEDVALUE;
          BonusInMoneyYN := UNCHECKEDVALUE;
        end;
      end;
    end;
  end; // DetermineTimeForTimeSettings;
*)
  // MR: 02-01-2007 Order 550432. Added overtime-per-month calculation.
  function DetermineStandardMinutesPerMonth: Integer;
  var
    WorkDays: array[1..7] of Boolean;
    NormalHoursPerDay: Integer; // Is stored in minutes.
    IDay, LastDayMonth: Integer;
    Year, Month, Day: Word;
  begin
    Result := 0;
    with oqContractgroup do
    begin
      FindContractGroup(AIDCard); // RV082.1.
      ClearVariables;
      // RV082.1. Search on Contractgroup instead by Employee.
      SetVariable('CONTRACTGROUP_CODE', AIDCard.ContractgroupCode);
//      SetVariable('EMPNO', AIDCard.EmployeeCode);
      Execute;
      if not Eof then
      begin
        // Only period = 4 (month) is selected in query.
        // In the array 'workdays' the first day=sunday.
        WorkDays[1] := FieldAsString('WORKDAY_SU_YN') = 'Y';
        WorkDays[2] := FieldAsString('WORKDAY_MO_YN') = 'Y';
        WorkDays[3] := FieldAsString('WORKDAY_TU_YN') = 'Y';
        WorkDays[4] := FieldAsString('WORKDAY_WE_YN') = 'Y';
        WorkDays[5] := FieldAsString('WORKDAY_TH_YN') = 'Y';
        WorkDays[6] := FieldAsString('WORKDAY_FR_YN') = 'Y';
        WorkDays[7] := FieldAsString('WORKDAY_SA_YN') = 'Y';
        NormalHoursPerDay := FieldAsInteger('NORMALHOURSPERDAY');
        DecodeDate(ABookingDay, Year, Month, Day);
        LastDayMonth := LastMonthDay(Year, Month);
        // Determine total number of normal working hours for this month
        for IDay := 1 to LastDayMonth do
        begin
           // DayOfWeek returns: 1=sun 2=mon 3=tue ...
          if WorkDays[DayOfWeek(EncodeDate(Year, Month, IDay))] then
            Result := Result + NormalHoursPerDay;
        end;
      end;
    end;
  end; // DetermineStandardMinutesPerMonth
 // RV084.1.
(*
  // RV071.6.
  function DetermineTimeForTime(AEarnedTimeTFT: Double;
    AEarnedMin: Integer): Double;
  var
    Percentage: Double;
  begin
    DetermineTimeForTimeSettings; // RV045.1.
    if TimeForTimeYN = CHECKEDVALUE then
    begin
      Percentage := oqOvertimeDef.FieldAsFloat('BONUS_PERCENTAGE');
      if BonusInMoneyYN = UNCHECKEDVALUE then
        AEarnedTimeTFT := AEarnedTimeTFT +
          Round(AEarnedMin + AEarnedMin * Percentage / 100)
      else
        AEarnedTimeTFT := AEarnedTimeTFT + AEarnedMin;
    end;
    Result := AEarnedTimeTFT;
  end; // DetermineTimeForTime
  // RV071.6.
  function DetermineShorterWorkingWeek(AEarnedTimeSWW: Double;
    AEarnedMin: Integer): Double;
  var
    Percentage: Double;
  begin
    DetermineTimeForTimeSettings;
    if ShorterWorkingWeekYN = CHECKEDVALUE then
    begin
      Percentage := oqOvertimeDef.FieldAsFloat('BONUS_PERCENTAGE');
      if BonusInMoneyYN = UNCHECKEDVALUE then
        AEarnedTimeSWW := AEarnedTimeSWW +
          Round(AEarnedMin + AEarnedMin * Percentage / 100)
      else
        AEarnedTimeSWW := AEarnedTimeSWW + AEarnedMin;
    end;
    Result := AEarnedTimeSWW;
  end; // DetermineShorterWorkingWeek
*)
  // GLOB3-81
  function CheckFromToTime(AOvertimeMin: Integer;
    AFromTime, AToTime: TDateTime): Integer;
  var
    FromTime, MidnightTime, MorningTime, ToTime: TDateTime;
    OTDateIn, OTDateOut: TDateTime;
  begin
    Result := AOvertimeMin;
    if Self.OvertimeType = OTDay then
    begin
      if not ((AFromTime = 0) and (AToTime = 0)) then
      begin
        FromTime := Trunc(ABookingDay) + Frac(AFromTime);
        ToTime := Trunc(ABookingDay) + Frac(AToTime);
        OTDateOut := AIDCard.DateOut;
        OTDateIn := AIDCard.DateOut - (AOvertimeMin * (1/60/24));
        if FromTime > ToTime then
        begin
          MidnightTime := Trunc(ABookingDay + 1);
          MorningTime := Trunc(ABookingDay);
          Result := ComputeMinutesOfIntersection(
            OTDateIn, OTDateOut,
            FromTime, MidnightTime) +
            ComputeMinutesOfIntersection(
            OTDateIn, OTDateOut,
            MorningTime, ToTime);
        end
        else
        begin
          // Compare here with scan-times.
          Result := ComputeMinutesOfIntersection(
            OTDateIn, OTDateOut,
            FromTime, ToTime);
        end;
        if Result > AOvertimeMin then
          Result := AOvertimeMin;
      end;
    end;
  end; // CheckFromToTime
begin
  MyRegisteredMin     := AWorkedMin;
//  MyTotalWorkedMin    := AWorkedMin;
  MyTotOverTime       := 0;
  EarnedTimeTFT       := 0; // RV071.6.
  MyStartDate         := ABookingDay;
  MyEndDate           := ABookingDay;
  MyEarnedMin         := 0;
  EarnedTimeSWW       := 0; // RV071.6.

  // RV073.3. (D5:RV080.6.)
  // When this scan has workspot with an hourtype assigned, then
  // do not check on overtime.
  if AIDCard.WSHourtypeNumber <> -1 then
  begin
    Result := AWorkedMin;
    Exit;
  end;

  // TD-23386 If there is an hourtype defined on shift-level, then
  //          use that for booking hours.
  if AIDCard.ShiftHourtypeNumber <> -1 then
  begin
    Result := AWorkedMin;
    Exit;
  end;

  // RV033.1.
  // When this scan has workspot with hourtype and 'ignore overtime' is YES,
  // then there is not need to check for overtime.
  if AIDCard.IgnoreForOvertimeYN = 'Y' then
  begin
    Result := AWorkedMin;
    Exit;
  end;

  ComputeOvertimePeriod(AIDCard, MyStartDate, MyEndDate);
  StandardMinutesPerMonth := DetermineStandardMinutesPerMonth;

  // SALARYHOURPEREMPLOYEE + ABSENCEHOURPEREMPLOYEE
  // MR:20-11-2002 Also select on IGNORE_FOR_OVERTIME_YN for 'Hourtype on Shift'
  with oqBOTSelectTotMinutes do
  begin
    ClearVariables;
    SetVariable('STARTDATE',       MyStartDate);
    // RV074.1. RV073.1. (D5:RV080.4.) !!!CANCELLED!!!
    SetVariable('ENDDATE',         MyEndDate);
    // RV074.1. RV073.1. (D5:RV080.4.) !!!CANCELLED!!!
{
    // RV073.1. (RV080.4.)
    //          Do not take all hours for the complete overtime-period!
    //          But only till the BookingDay!
    SetVariable('ENDDATE',         ABookingDay); // MyEndDate;
}
    SetVariable('EMPLOYEE_NUMBER', AIDCard.EmployeeCode);
    Execute;
    MyBookedMin := FieldAsInteger('TOT_MINUTES');
    MyRegisteredMin := MyRegisteredMin + FieldAsInteger('TOT_MINUTES');
  end; {with oqBOTSelectTotMinutes}

(* RV039.2. Do not use! Gives wrong result for overtime.
  // MRA:06-MAR-2008 RV004. Overtime-fix. Start.
  // Determine from Salary-hours the overtime-minutes registered so far.
  if (not ACombinedHourSystem) then
  begin
    with oqSalaryOvertimeMin do
    begin
      ClearVariables;
      SetVariable('STARTDATE', MyStartDate);
      SetVariable('ENDDATE',   MyEndDate);
      SetVariable('EMPNO',     AIDCard.EmployeeCode);
      Execute;
      MySalaryOvertimeMin := FieldAsInteger('SUMSALARYOVERTIMEMIN');
    end;
    // Subtract minutes of overtime that are already calculated.
    MyRegisteredMin := MyRegisteredMin - MySalaryOvertimeMin;
  end;
  // MRA:06-MAR-2008 RV004. Overtime-fix. End.
*)
  // RV071.6. Added HT.ADV_YN to query.
  FindContractGroup(AIDCard); // RV082.1.
  with oqOvertimeDef do
  begin
    ClearVariables;
    // RV082.1. Search on contractgroup, instead by employee
    SetVariable('CONTRACTGROUP_CODE', AIDCard.ContractgroupCode);
//    SetVariable('EMPLOYEE_NUMBER', AIDCard.EmployeeCode);
    // GLOB3-81 GLOB3-223
    if ORASystemDM.OvertimePerDay and (OvertimeType = OTDay) then
      SetVariable('DAY_OF_WEEK', DayInWeek(ORASystemDM.WeekStartsOn, ABookingDay))
    else
      SetVariable('DAY_OF_WEEK', 0);
    Execute;
    while not Eof do
    begin
      MyStartMin       := FieldAsInteger('STARTTIME') +
        StandardMinutesPerMonth;
      MyEndMin         := FieldAsInteger('ENDTIME') +
        StandardMinutesPerMonth;
      MyHourTypeNumber := FieldAsInteger('HOURTYPE_NUMBER');
      // GLOB3-81 GLOB3-223
      if ORASystemDM.OvertimePerDay and (OvertimeType = OTDay) then
      begin
        FromTime := FieldAsDate('FROMTIME');
        ToTime := FieldAsDate('TOTIME');
      end
      else
      begin
        FromTime := 0;
        ToTime := 0;
      end;

      // RV071.6.
      // RV084.1. Not needed here.
{      ShorterWorkingWeekYN := FieldAsString('HT_ADV_YN'); }

      // MRA:06-MAR-2008 RV004. Overtime-fix. Start.
      // Use if instead of following statement to determine Overtime.
(*
      MyOverTimeMin    := Min(MyEndMin, MyRegisteredMin) -
        Max(MyStartMin, (MyRegisteredMin - AWorkedMin));
*)
      MyOverTimeMin := 0;

      // RV025.
      // If the already booked minutes are higher than the lower boundary
      // of the Overtime-definition, then make the lower boundar equal to this.
      // To prevent too much overtime-hours are calculated.
      // Example:
      //   BookedMin is 41:30
      //   StartMin is 40:00
      //   Then StartMin should also become 41:30
      // RV039.2. Do this ALWAYS! Not only for 'ACombinedHourSystem' !
//      if (ACombinedHourSystem) then
        if (MyBookedMin > MyStartMin) then
          MyStartMin := MyBookedMin;

      if ((MyRegisteredMin >= MyStartMin) and (Min(MyRegisteredMin, MyEndMin) <= MyEndMin)) then
        MyOverTimeMin := Min(MyRegisteredMin, MyEndMin) - MyStartMin;
      // MRA:06-MAR-2008 RV004. Overtime-fix. End.

      if (MyOverTimeMin < 0) then
        MyOverTimeMin := 0;
      // MRA:06-MAR-2008 RV004. Also check on 'MyOverTimeMin' in following if.
      if (MyStartMin >= MyRegisteredMin) and (MyOverTimeMin <= 0) then
      begin
        Last;
      end
  // MR:13-06-2003 End
      else
      begin
        // GLOB3-81 First optionally check this here. GLOB3-223
        if ORASystemDM.OvertimePerDay and (OvertimeType = OTDay) then
          if (MyOverTimeMin > 0) then
            MyOverTimeMin := CheckFromToTime(MyOvertimeMin, FromTime, ToTime);

        MyTotOverTime := MyTotOverTime + MyOverTimeMin;
        if MyOverTimeMin > 0 then
        begin
          // RV044.1. Skip when detecting overtime!
          if not (ACombinedHourSystem and ADetect) then
          begin
            MyEarnedMin := UpdateSalaryHour(ABookingDay, AIDCard,
              MyHourTypeNumber, MyOverTimeMin, AManual,
              False { MR:25-11-2002 Was True: Set to False },
              True);
          end;
        end
        else
          MyEarnedMin := 0;
        // RV040.2. Skip when detecting!
        if not (ACombinedHourSystem and ADetect) then
        begin
          // RV071.6.
          EarnedTimeTFT := DetermineTimeForTime(oqOvertimeDef,
            EarnedTimeTFT, MyEarnedMin); // RV084.1.
          // RV071.6.
          EarnedTimeSWW := DetermineShorterWorkingWeek(oqOvertimeDef,
            EarnedTimeSWW, MyEarnedMin); // RV084.1.
        end; // if not (ACombinedHourSystem and ADetect) then
      end;
      // GLOB3-81 Decrease with already calculated overtime

      // PIM-406 Do not do this to prevent it miscalculates the overtime.
{
      if ORASystemDM.IsNTS and (OvertimeType = OTDay) then
        if (MyOverTimeMin > 0) then
          MyRegisteredMin := MyRegisteredMin - MyOvertimeMin;
}          
      Next;
    end {while not Eof}
  end; {with oqOvertimeDef}
  // RV025. Only detect if there were overtime minutes made,
  // do not store the hours!
  // RV044.1. Detect all overtime! This part is moved to end of loop.
  if (ACombinedHourSystem and ADetect) then
  begin
    AOvertimeMin := MyTotOverTime;
    ANonOvertimeMin := AWorkedMin - MyTotOverTime;
    if (ANonOvertimeMin < 0) then
      ANonOvertimeMin := 0;
    Result := 0;
    Exit;
  end;

  // Booking Time for Time
  BooksTFT(ABookingDay, AIDCard, EarnedTimeTFT); // RV084.1.
  // Booking Shorter Working Week (SWW)
  BooksSWW(ABookingDay, AIDCard, EarnedTimeSWW); // RV084.1.

  Result := AWorkedMin - MyTotOverTime;
  // MRA:06-MAR-2008 RV004. Overtime-fix. Start.
  // Result can be negative!
(*
  if not ARoundMinutes then
    if Result < 0 then
      Result := 0;
*)
  // MRA:06-MAR-2008 RV004. Overtime-fix. End.
end; // BooksOverTimeMain

// RV025. Book the Overtime minutes. No combined hours (except+overtime).
function TGlobalDM.BooksOverTime(ABookingDay: TdateTime;
  AWorkedMin: Integer; AIDCard: TScannedIDcard;
  AManual: String
  ): Integer;
var
  NonOvertimeMinDummy, OvertimeMinDummy: Integer;
begin
  NonOvertimeMinDummy := 0;
  OvertimeMinDummy := 0;
  Result := BooksOvertimeMain(
    ABookingDay, AWorkedMin, AIDCard,
    AManual,
    False, // ACombinedHourSystem
    False, // Detect: Detection no
    NonOvertimeMinDummy, OvertimeMinDummy);
end;

// RV025. Book the Overtime minutes, for combined hours (exceptional+overtime).
function TGlobalDM.BooksOverTimeCombinedHours(ABookingDay: TdateTime;
  AWorkedMin: Integer; AIDCard: TScannedIDcard;
  AManual: String
  ): Integer;
var
  NonOvertimeMinDummy, OvertimeMinDummy: Integer;
begin
  NonOvertimeMinDummy := 0;
  OvertimeMinDummy := 0;
  Result := BooksOvertimeMain(
    ABookingDay, AWorkedMin, AIDCard,
    AManual,
    True, // CombinedHourSystem
    False, // Detect: Detection no
    NonOvertimeMinDummy, OvertimeMinDummy);
end;

// RV025. Detect the Overtime minutes/Non Overtime minutes, do not book them.
function TGlobalDM.OverTimeDetect(ABookingDay: TdateTime;
  AWorkedMin: Integer; AIDCard: TScannedIDcard;
  AManual: String;
  VAR ANonOvertimeMin: Integer;
  VAR AOvertimeMin: Integer
  ): Integer;
begin
  Result := BooksOvertimeMain(
    ABookingDay, AWorkedMin, AIDCard,
    AManual,
    True, // CombinedHourSystem
    True, // Detect: Detection yes
    ANonOvertimeMin,
    AOvertimeMin);
end;

procedure TGlobalDM.BooksRegularSalaryHours(ABookingDay: TDateTime;
  AIDCard: TScannedIDcard; ABookMinutes: Integer; AManual: String);
var
  MyHourTypeNumber: Integer;
begin
  // regular salary hours
  // Hour Type = ? for regular hours.
  // TD-23817 Do not book negative values here (when non-manual)
  if { (ABookMinutes <> 0) }
    (
      (AManual <> CHECKEDVALUE) and (ABookMinutes > 0) // non-manual hours
      or
      (AManual = CHECKEDVALUE) and (ABookMinutes <> 0) // manual hours
    ) then
  begin
    MyHourTypeNumber := 1;

    // TD-23386 Workspot hourtype is determined during PopulateIDCard.
    if AIDCard.WSHourtypeNumber <> -1 then
      MyHourTypeNumber := AIDCard.WSHourtypeNumber;
    // TD-23386
    if AIDCard.ShiftHourtypeNumber <> -1 then
      MyHourTypeNumber := AIDCard.ShiftHourtypeNumber;
    UpdateSalaryHour(ABookingDay, AIDcard,
      MyHourTypeNumber, ABookMinutes, AManual, False{ Don't replace! },
      True);
  end; {ABookMinutes <> 0 }
end; // BooksRegularSalaryHours

procedure TGlobalDM.ComputeOvertimePeriod(AIDCard: TScannedIDCard;
  var AStartDate, AEndDate: TDateTime);
var
  MyOverTimeType, MyDayInWeek,
  MyPeriodStart, MyPeriodLength, MyStartWeek, MyPeriodNumber: Integer;
  MyWeekNumber, MyYear, MyMonth, MyDay: Word;
  MyBookingDate: TDateTime;
begin
  MyBookingDate := AStartDate;
  with oqComputeOvertimePeriod do
  begin
    FindContractGroup(AIDCard); // RV082.1.
    ClearVariables;
    // RV082.1. Search on Contractgroup instead by Employee.
    SetVariable('CONTRACTGROUP_CODE', AIDCard.ContractgroupCode);
//    SetVariable('EMPLOYEE_NUMBER', AIDCard.EmployeeCode);
    Execute;
    if not Eof then
    begin
      MyOverTimeType := FieldAsInteger('OVERTIME_PER_DAY_WEEK_PERIOD');
      Self.OvertimeType := TOvertime(MyOverTimeType); // GLOB3-81
      case TOvertime(MyOverTimeType) of
        OTDay : begin  // period = day
              AStartDate := MyBookingDate;
              AEndDate   := MyBookingDate;
            end;
        OTWeek : begin  // period = week
              MyDayInWeek := DayInWeek(ORASystemDM.WeekStartsOn, MyBookingDate);
              AStartDate  := MyBookingDate - MyDayInWeek + 1;
              AEndDate    := MyBookingDate + 7 - MyDayInWeek;
            end;
        OTPeriod : begin  // period = weeks
              MyPeriodStart  := FieldAsInteger('PERIOD_STARTS_IN_WEEK');
              MyPeriodLength := FieldAsInteger('WEEKS_IN_PERIOD');
              if (MyPeriodStart <= 0) or (MyPeriodLength <= 0) then
              // incorrect values
              begin
                AStartDate := MyBookingDate;
                AEnddate   := MyBookingDate;
              end
              else
              begin
                // START MR:06-01-2004 Rewritten, to prevent errors with
                //                     wrong year...
                WeekUitDat(MyBookingDate, MyYear, MyWeekNumber);
                MyPeriodNumber :=
                  Trunc((MyWeekNumber - MyPeriodStart) / MyPeriodLength + 1);
                MyStartWeek :=
                  (MyPeriodNumber - 1) * MyPeriodLength + MyPeriodStart;
                AStartDate  := StartOfYear(MyYear);
                AStartDate  := AStartDate + (MyStartWeek - 1) * 7;
                AEndDate    := AStartDate + (MyPeriodLength * 7) - 1;
              end;
            end;
        OTMonth :
          begin // period = month
            // Determine month by bookdate and set start, end of month.
            DecodeDate(MyBookingDate, MyYear, MyMonth, MyDay);
            AStartDate := EncodeDate(MyYear, MyMonth, 1);
            AEndDate := EncodeDate(MyYear, MyMonth,
              LastMonthDay(MyYear, MyMonth));
          end;
      end; {case}
      // MR:17-09-2003 Use '0,0,0,0' istead of '0,0,0,1' for 'EncodeTime'
      // Otherwise comparisons can go wrong.
      ReplaceTime(AStartDate, EncodeTime(0, 0, 0, 0));
      ReplaceTime(AEndDate,   EncodeTime(23, 59, 59, 99));
    end; {not Eof}
  end; {with}
end;

(* RV013.
procedure TGlobalDM.ContractRoundMinutes(ABookingDay: TDateTime;
  AIDCard: TScannedIDCard; var AProdMin: Integer);
var
  MyNewTotalMinutes,
  MyTotalMinutes,
  MyRoundTruncSalaryHours,
  MyRoundMinute: Integer;

  function RoundMinutes(ARoundTruncSalaryHours, ARoundMinute: Integer;
    AMinutes: Integer): Integer;
  var
    Hours, Mins: Integer;
  begin
    Hours := AMinutes div 60;
    Mins  := AMinutes mod 60;
    if ARoundTruncSalaryHours = 1 then // Round
      Mins := Round(Mins / ARoundMinute) * ARoundMinute
    else
      Mins := Trunc(Mins / ARoundMinute) * ARoundMinute;
    Result := Hours * 60 + Mins;
  end;

begin
  AProdMin := 0;
  with oqCRMSelect do
  begin
    ClearVariables;
    SetVariable('SALARY_DATE',     ABookingDay);
    SetVariable('EMPLOYEE_NUMBER', AIDCard.EmployeeCode);
    Execute;
    MyTotalMinutes := FieldAsInteger('TOT_MINUTES');
    AProdMinClass.ReadContractRoundMinutesParams(AIDCard,
      MyRoundTruncSalaryHours, MyRoundMinute);
    MyNewTotalMinutes := RoundMinutes(MyRoundTruncSalaryHours, MyRoundMinute,
      MyTotalMinutes);
    AProdMin := MyNewTotalMinutes - MyTotalMinutes;
  end; {with oqCRMSelect}
end;
*)

procedure TGlobalDM.DeleteProdHourForAllTypes(AIDCard: TScannedIDCard;
  AStartDate, AEndDate: TDateTime; AManual: String);
begin
  if (ORASystemDM.GetFillProdHourPerHourTypeYN = UNCHECKEDVALUE) then
    Exit;
  // MR:11-10-2003 Only select on 'Date' and 'Employee'!
  with oqDPHDelete do
  begin
    ClearVariables;
    SetVariable('EMPLOYEE_NUMBER', AIDCard.EmployeeCode);
    SetVariable('STARTDATE',       AStartDate);
    SetVariable('ENDDATE',         AEndDate);
    SetVariable('MANUAL_YN',       AManual);
    Execute;
  end;
end;

function TGlobalDM.DetermineExceptionalBeforeOvertime(
  AIDCard: TScannedIDCard): Boolean;
begin
  Result := False;
  // get this from the IDCard in the future since it is Employee data
  with oqDEBOSelect do
  begin
    ClearVariables;
    SetVariable('EMPLOYEE_NUMBER', AIDCard.EmployeeCode);
    Execute;
    if not Eof then
      if not FieldIsNull('EXCEPTIONAL_BEFORE_OVERTIME') then
        Result := (FieldAsString('EXCEPTIONAL_BEFORE_OVERTIME') = 'Y');
  end; {oqDEBOSelect}
end;

procedure TGlobalDM.FillProdHourPerHourType(AProductionDate: TDateTime;
  AIDCard: TScannedIDCard; AHourTypeNumber, AMinutes: Integer;
  AManual: String; AInsertProdHrs: Boolean);
var
  MyTotalMinutes: Integer;
  TotalMinutesExBreaks: Integer; // RV028.
  procedure DetermineMinExclBreaks; // RV028.
  begin
    if (PayedBreakSave > 0) and
      ((TotalMinutesExBreaks - PayedBreakSave) < 0) then
    begin
      PayedBreakSave := abs(TotalMinutesExBreaks - PayedBreakSave);
      TotalMinutesExBreaks := 0;
    end
    else
    begin
      TotalMinutesExBreaks := TotalMinutesExBreaks - PayedBreakSave;
      PayedBreakSave := 0;
    end;
  end;
begin
  if (ORASystemDM.GetFillProdHourPerHourTypeYN = UNCHECKEDVALUE) then
    Exit;
  if AInsertProdHrs then
  begin
    with oqFPHSelect do
    begin
      ClearVariables;
      SetVariable('PRODHOUREMPLOYEE_DATE', AProductionDate);
      SetVariable('PLANT_CODE',            AIDCard.PlantCode);
      SetVariable('SHIFT_NUMBER',          AIDCard.ShiftNumber);
      SetVariable('EMPLOYEE_NUMBER',       AIDCard.EmployeeCode);
      SetVariable('WORKSPOT_CODE',         AIDCard.WorkSpotCode);
      SetVariable('JOB_CODE',              AIDCard.JobCode);
      SetVariable('MANUAL_YN',             AManual);
      SetVariable('HOURTYPE_NUMBER',       AHourTypeNumber);
      Execute;
    end; {with oqFPHSelect}
    if not oqFPHSelect.Eof then {not oqFPHSelect.Eof}
    begin
      // Update record
      MyTotalMinutes := oqFPHSelect.FieldAsInteger('PRODUCTION_MINUTE') +
        AMinutes;
      // RV028. Start
      TotalMinutesExBreaks :=
        oqFPHSelect.FieldAsInteger('PROD_MIN_EXCL_BREAK') +
          AMinutes;
      DetermineMinExclBreaks;
      // RV028. End
      with oqFPHUpdate do
      begin
        ClearVariables;
        SetVariable('PRODHOUREMPLOYEE_DATE', AProductionDate);
        SetVariable('PLANT_CODE',            AIDCard.PlantCode);
        SetVariable('SHIFT_NUMBER',          AIDCard.ShiftNumber);
        SetVariable('EMPLOYEE_NUMBER',       AIDCard.EmployeeCode);
        SetVariable('WORKSPOT_CODE',         AIDCard.WorkSpotCode);
        SetVariable('JOB_CODE',              AIDCard.JobCode);
        SetVariable('MANUAL_YN',             AManual);
        SetVariable('HOURTYPE_NUMBER',       AHourTypeNumber);
        SetVariable('PRODUCTION_MINUTE',     MyTotalMinutes);
        // RV028.
        SetVariable('PROD_MIN_EXCL_BREAK',   TotalMinutesExBreaks);
        SetVariable('MUTATOR',               ORASystemDM.CurrentProgramUser);
        Execute;
      end; {with oqFPHUpdate}
    end   {not oqFPHSelect.Eof}
    else
    begin {else not oqFPHSelect.Eof}
      with oqFPHInsert do
      begin
        ClearVariables;
        SetVariable('PRODHOUREMPLOYEE_DATE', AProductionDate);
        SetVariable('PLANT_CODE',            AIDCard.PlantCode);
        SetVariable('SHIFT_NUMBER',          AIDCard.ShiftNumber);
        SetVariable('EMPLOYEE_NUMBER',       AIDCard.EmployeeCode);
        SetVariable('WORKSPOT_CODE',         AIDCard.WorkSpotCode);
        SetVariable('JOB_CODE',              AIDCard.JobCode);
        SetVariable('MANUAL_YN',             AManual);
        SetVariable('HOURTYPE_NUMBER',       AHourTypeNumber);
        SetVariable('PRODUCTION_MINUTE',     AMinutes);
        // RV028. Start
        TotalMinutesExBreaks := AMinutes;
        DetermineMinExclBreaks;
        SetVariable('PROD_MIN_EXCL_BREAK',   TotalMinutesExBreaks);
        // RV028. End
        SetVariable('MUTATOR',               ORASystemDM.CurrentProgramUser);
        Execute;
      end; {with oqFPHInsert}
    end;  {else not oqFPHSelect.Eof}
  end {AInsertProdHrs}
  else
  begin {AInsertProdHrs}
    // MRA:04-MAR-2009 This was in D5-Source but missing in D7-Source !!!
    // MR:06-09-2005 Order 550407. Delete it, after 'ProcessBookingsForContractRoundMinutes'
    // was processed to compensate for rounded minutes.
    with oqFPHDelOneRec do
    begin
      ClearVariables;
      SetVariable('PRODHOUREMPLOYEE_DATE', AProductionDate);
      SetVariable('PLANT_CODE',            AIDCard.PlantCode);
      SetVariable('SHIFT_NUMBER',          AIDCard.ShiftNumber);
      SetVariable('EMPLOYEE_NUMBER',       AIDCard.EmployeeCode);
      SetVariable('WORKSPOT_CODE',         AIDCard.WorkSpotCode);
      SetVariable('JOB_CODE',              AIDCard.JobCode);
      SetVariable('MANUAL_YN',             AManual);
      SetVariable('HOURTYPE_NUMBER',       AHourTypeNumber);
      Execute;
    end;
(*
    D5-Part:

    with qryProdHourPEPTDeleteOneRec do
    begin
      Close;
      ParamByName('PDATE').AsDateTime := ProductionDate;
      ParamByName('PCODE').AsString := EmployeeData.PlantCode;
      ParamByName('SHNO').AsInteger := EmployeeData.ShiftNumber;
      ParamByName('EMPNO').AsInteger := EmployeeData.EmployeeCode;
      ParamByName('WCODE').AsString := EmployeeData.WorkSpotCode;
      ParamByName('JCODE').AsString := EmployeeData.JobCode;
      ParamByName('MAN').AsString := Manual;
      ParamByName('HOURTYPE').AsInteger := HourType;
      ExecSQL;
      Close;
    end;
*)
  end; {AInsertProdHrs}
end;

procedure TGlobalDM.ProcessBookings(ABookingDay: TDateTime;
  AIDCard: TScannedIDCard; AUpdateProdMin, ARoundMinutes: Boolean;
  AProdMin, APayedBreaks: Integer; AManual: String);
var
  MyNonOverTime: Integer;
  MyBookExceptionalBeforeOvertime: Boolean;
  OvertimeMin, NonOvertimeMin: Integer; {RV025}
  OldDateInCutOff, OldDateIn: TDateTime; {RV025}
  MyOverTime: Integer; {RV025}
begin
  MyBookExceptionalBeforeOvertime :=
    DetermineExceptionalBeforeOvertime(AIDCard);
  GlobalDM.PayedBreakSave := APayedBreaks; // RV028.
  // Production Hours
  // RV040.4. PayedBreaks problem.
  //          Add the PayedBreaks here to ProdMin in if() ! Or they are not
  //          included when a scans is equal to a payed break!
  //          Example: When scan is 8:00-8:10 and payed break is the same,
  //                   then ProdMin will be 0, so it will not be booked!
  if (AProdMin+APayedBreaks > 0) or ARoundMinutes then
  begin
    // RV084.1.
    // When worked hours are detected on bank holiday (with an hourtype for
    // 'worked on bank holiday'), then when the hours are booked on holiday,
    // this will return 0 minutes, in which case no further hours have to
    // be booked.
    if BooksWorkedHoursOnBankHoliday(ABookingDay, AProdMin + APayedBreaks,
      AIDCard, AManual) <> 0 then
    begin
      // RV025.
      // Detect Overtime and Non-overtime combination. This is needed
      // when combined hours (exceptional during overtime hours) must be stored.
      // For this purpose the scan that has non-overtime and overtime minutes,
      // must be handled in 2 parts:
      // - Part1: The scan-part before the overtime: Non-overtime minutes.
      // - Part2: The scan-part falling in overtime: Overtime minutes.
      if DetectOvertimeNonOvertime(ABookingDay,
        AProdMin + APayedBreaks, AIDCard, AManual,
        NonOvertimeMin, OvertimeMin) then
      begin
        OldDateIn := AIDCard.DateIn;
        OldDateInCutOff := AIDCard.DateInCutOff;
        // Non-overtime minutes:
        // - Book as Exceptional and Regular hours.
        if NonOvertimeMin <> 0 then
        begin
          MyNonOverTime := BooksExceptionalTime(ABookingDay,
            NonOvertimeMin, AIDCard, AManual);
          BooksRegularSalaryHours(ABookingDay,
            AIDCard, MyNonOverTime, AManual);
          // Add NonOvertimeMin temporary to the scan!
          // Because it is used later during 'Books'-functions
          AIDCard.DateIn :=
            AIDCard.DateIn + (NonOvertimeMin / 60 / 24);
          AIDCard.DateInCutOff :=
            AIDCard.DateInCutOff + (NonOvertimeMin / 60 / 24);
        end;
        // Overtime minutes:
        // - Book as Exceptional made during overtime,
        //   Overtime, and Regular hours.
        if OvertimeMin <> 0 then
        begin
          MyOverTime := BooksExceptionalTimeDuringOvertime(
            ABookingDay, OvertimeMin, AIDCard, AManual);
          MyOverTime := BooksOverTimeCombinedHours(ABookingDay,
            MyOverTime, AIDCard, AManual);
          BooksRegularSalaryHours(ABookingDay,
            AIDCard, MyOverTime, AManual);
        end;
        AIDCard.DateIn := OldDateIn;
        AIDCard.DateInCutOff := OldDateInCutOff;
      end
      else
      begin
        // MR:18-11-2002
        // Process salary hours per Employee
        if not MyBookExceptionalBeforeOvertime then
        begin
          // Default: First book overtime, then exceptional
          MyNonOverTime := BooksOverTime(ABookingDay, AProdMin + APayedBreaks,
            AIDCard, AManual);
          MyNonOverTime := BooksExceptionalTime(ABookingDay, MyNonOverTime,
            AIDCard, AManual);
          BooksRegularSalaryHours(ABookingDay, AIDCard, MyNonOverTime, AManual);
        end
        else
        begin
          // First book exceptional then overtime
          MyNonOverTime := BooksExceptionalTime(ABookingDay, AProdMin + APayedBreaks,
            AIDCard, AManual);
          MyNonOverTime := BooksOverTime(ABookingDay, MyNonOverTime,
            AIDCard, AManual);
          BooksRegularSalaryHours(ABookingDay, AIDCard, MyNonOverTime, AManual);
        end; // if not MyBookExceptionalBeforeOvertime
      end; // if DetectOvertimeNonOvertime
    end; // if BooksWorkedHoursOnBankHoliday
    // TD-23386 Do NOT use this anymore! It is handled in a different way.
{
    // MR:19-11-2002
    // Book hours for hourtype of Shift
    BooksHoursForShift(ABookingDay, AIDCard, AProdMin + APayedBreaks, AManual);
}    
  end; // if (AProdMin+APayedBreaks > 0)
  GlobalDM.PayedBreakSave := 0; // RV028.
end;

(* RV013.
procedure TGlobalDM.ProcessBookingsForContractRoundMinutes(
  ABookingDay: TDateTime; AIDCard: TScannedIDCard; AUpdateProdMin,
  ARoundMinutes: Boolean; AProdMin, APayedBreaks: Integer;
  AManual: String);
var
  MyNonOverTime, MySalaryMinutes: Integer;
begin
  // Look for overtimedefinitions for the employee's contractgroup
  // and look if there are salary-minutes for these.
  with oqPBFCSelect do
  begin
    ClearVariables;
    SetVariable('SALARY_DATE',     ABookingDay);
    SetVariable('EMPLOYEE_NUMBER', AIDCard.EmployeeCode);
    Execute;
    if not Eof then
    begin
      while (not Eof) and (AProdMin < 0) do
      begin
        // Correct salary with 'prodmin'.
        UpdateSalaryHour(ABookingDay, AIDCard,
          FieldAsInteger('HOURTYPE_NUMBER'),
          AProdMin, AManual, False);
        // Calculate how many minutes are left
        MySalaryMinutes := FieldAsInteger('SALARY_MINUTE') + AProdMin;
        if MySalaryMinutes < 0 then
          AProdMin := MySalaryMinutes
        else
          AProdMin := 0;
        Next;
      end; {while (not Eof) and (AProdMin < 0)}
    end {if not Eof}
  end; {with oqPBFCSelect}
  // If there are still negative minutes left, then book these on regular hours.
  if AProdMin < 0 then
  begin
    MyNonOverTime := BooksExceptionalTime(ABookingDay, AProdMin,
      AIDCard, AManual);
    BooksRegularSalaryHours(ABookingDay, AIDCard, MyNonOverTime,
      AManual);
  end;
end;
*)

procedure TGlobalDM.ProcessTimeRecording(AIDCard,
  ADeleteIDCard: TScannedIDCard; AFirstScan, ALastScan, AUpdateSalMin,
  AUpdateProdMinOut, AUpdateProdMinIn, ADeleteAction,
  ALastScanRecordForDay: Boolean);
var
  MyBookingDay, MySaveDate_Out, MySaveDate_In, MySaveDate_In_CutOff: TDateTime;
  MyProdMin, MyBreaksMin, MyPayedBreaks,
  MyPayedBreaksTot, MyProdMinTot: Integer;
  MyIsOvernight: Boolean;
  StartDate, EndDate: TDateTime;
  MyDateIn, MyDateOut: TDateTime;
  ProdHrsBreaksMin: Integer; // PIM-319
  // PIM-87
  procedure PackageCalcHourProcessTimeRecording;
  begin
    with oqProcessTimeRecording do
    begin
      ClearVariables;
      // AIDCcard
      SetVariable('EmployeeCode', AIDCard.EmployeeCode);
      SetVariable('PlantCode', AIDCard.PlantCode);
      SetVariable('ShiftNumber', AIDCard.ShiftNumber);
      SetVariable('WorkspotCode', AIDCard.WorkSpotCode);
      SetVariable('JobCode', AIDCard.JobCode);
      SetVariable('DepartmentCode', AIDCard.DepartmentCode);
      SetVariable('DateIn', AIDCard.DateIn);
      SetVariable('DateOut', AIDCard.DateOut);
      SetVariable('ShiftDate', AIDCard.ShiftDate);
      // ADeleteIDCcard
      SetVariable('DELEmployeeCode', ADeleteIDCard.EmployeeCode);
      SetVariable('DELPlantCode', ADeleteIDCard.PlantCode);
      SetVariable('DELShiftNumber', ADeleteIDCard.ShiftNumber);
      SetVariable('DELWorkspotCode', ADeleteIDCard.WorkSpotCode);
      SetVariable('DELJobCode', ADeleteIDCard.JobCode);
      SetVariable('DELDepartmentCode', ADeleteIDCard.DepartmentCode);
      SetVariable('DELDateIn', ADeleteIDCard.DateIn);
      SetVariable('DELDateOut', ADeleteIDCard.DateOut);
      SetVariable('DELShiftDate', ADeleteIDCard.ShiftDate);
      // Params - booleans (convert to int)
      SetVariable('AFirstScan', Bool2Int(AFirstScan));
      SetVariable('ALastScan', Bool2Int(ALastScan));
      SetVariable('AUpdateSalMin', Bool2Int(AUpdateSalMin));
      SetVariable('AUpdateProdMinOut', Bool2Int(AUpdateProdMinOut));
      SetVariable('AUpdateProdMinIn', Bool2Int(AUpdateProdMinIn));
      SetVariable('ADeleteAction', Bool2Int(ADeleteAction));
      SetVariable('ALastScanRecordForDay', Bool2Int(ALastScanRecordForDay));
      // Other params
      SetVariable('acurrentprogramuser', ORASystemDM.CurrentProgramUser);
      SetVariable('acurrentcomputername', ORASystemDM.CurrentComputerName);
{$IFDEF DEBUG3}
      SetVariable('adebug', 1);
{$ELSE}
      SetVariable('adebug', 0);
{$ENDIF}
      Execute;
    end;
  end; // PackageCalcHourProcessTimeRecording
  // PIM-87
  procedure PackageCalcHourProcessTimeRecordingDebugLog;
  begin
    try
      // AIDCcard
      WDebugLog('PackageCalcHour ProcessTimeRecording - Params');
      WDebugLog('EmployeeCode=' + IntToStr(AIDCard.EmployeeCode));
      WDebugLog('PlantCode=' + AIDCard.PlantCode);
      WDebugLog('ShiftNumber=' + IntToStr(AIDCard.ShiftNumber));
      WDebugLog('WorkspotCode=' + AIDCard.WorkSpotCode);
      WDebugLog('JobCode=' + AIDCard.JobCode);
      WDebugLog('DepartmentCode=' + AIDCard.DepartmentCode);
      WDebugLog('DateIn=' + DateTimeToStr(AIDCard.DateIn));
      WDebugLog('DateOut=' + DateTimeToStr(AIDCard.DateOut));
      WDebugLog('ShiftDate=' + DateToStr(AIDCard.ShiftDate));
      // ADeleteIDCcard
      WDebugLog('DELEmployeeCode=' + IntToStr(ADeleteIDCard.EmployeeCode));
      WDebugLog('DELPlantCode=' + ADeleteIDCard.PlantCode);
      WDebugLog('DELShiftNumber=' + IntToStr(ADeleteIDCard.ShiftNumber));
      WDebugLog('DELWorkspotCode=' + ADeleteIDCard.WorkSpotCode);
      WDebugLog('DELJobCode=' + ADeleteIDCard.JobCode);
      WDebugLog('DELDepartmentCode=' + ADeleteIDCard.DepartmentCode);
      WDebugLog('DELDateIn=' + DateTimeToStr(ADeleteIDCard.DateIn));
      WDebugLog('DELDateOut=' + DateTimeToStr(ADeleteIDCard.DateOut));
      WDebugLog('DELShiftDate=' + DateToStr(ADeleteIDCard.ShiftDate));
      // Params - booleans (convert to int)
      WDebugLog('AFirstScan=' + Bool2Str(AFirstScan));
      WDebugLog('ALastScan=' + Bool2Str(ALastScan));
      WDebugLog('AUpdateSalMin=' + Bool2Str(AUpdateSalMin));
      WDebugLog('AUpdateProdMinOut=' + Bool2Str(AUpdateProdMinOut));
      WDebugLog('AUpdateProdMinIn=' + Bool2Str(AUpdateProdMinIn));
      WDebugLog('ADeleteAction=' + Bool2Str(ADeleteAction));
      WDebugLog('ALastScanRecordForDay=' + Bool2Str(ALastScanRecordForDay));
      // Other params
      WDebugLog('acurrentprogramuser=' + ORASystemDM.CurrentProgramUser);
      WDebugLog('acurrentcomputername=' + ORASystemDM.CurrentComputerName);
    except
    end;
  end; // PackageCalcHourProcessTimeRecordingDebugLog
begin
  // PIM-87
  if ORASystemDM.UsePackageHourCalc then
  begin
    try
      PackageCalcHourProcessTimeRecording;
    except
      on E: EOracleError do
      begin
        WErrorLog('PackageCalcHourProcessTimeRecording: ' + E.Message);
        PackageCalcHourProcessTimeRecordingDebugLog;
      end;
      on E: Exception do
      begin
        WErrorLog('PackageCalcHourProcessTimeRecording: ' + E.Message);
        PackageCalcHourProcessTimeRecordingDebugLog;
      end;
    end;
    Exit;
  end;

{$IFDEF DEBUG3}
  WDebugLog('ProcessTimeRecording - Start');
  WDebugLog('AIDCard');
  WDebugLog('- EMP=' + IntToStr(AIDCard.EmployeeCode) +
    ' PL=' + AIDCard.PlantCode +
    ' SHFT=' + IntToStr(AIDCard.ShiftNumber) +
    ' WC=' + AIDCard.WorkSpotCode +
    ' JC=' + AIDCard.JobCode +
    ' DEPT=' + AIDCard.DepartmentCode
    );
  WDebugLog(
    '- DIN=' + DateTimeToStr(AIDCard.DateIn) +
    ' DOUT=' + DateTimeToStr(AIDCard.DateOut) +
    ' SHIFTDATE=' + DateToStr(AIDCard.ShiftDate)
    );
  WDebugLog('- Params AFirstScan=' + Bool2Str(AFirstScan) +
    ' ALastScan=' + Bool2Str(ALastScan) +
    ' AUpdateSalMin=' + Bool2Str(AUpdateSalMin) +
    ' AUpdateProdMinOut=' + Bool2Str(AUpdateProdMinOut) +
    ' AUpdateProdMinIn=' + Bool2Str(AUpdateProdMinIn) +
    ' ADeleteAction=' + Bool2Str(ADeleteAction) +
    ' ALastScanRecordForDay=' + Bool2Str(ALastScanRecordForDay)
    );
{$ENDIF}

  if ORASystemDM.UseShiftDateSystem then
  begin
    // 20013489.10
    AProdMinClass.GetShiftDay(AIDCard, StartDate, EndDate);
    AIDCard.ShiftDate := Trunc(StartDate);
  end
  else
    AIDCard.ShiftDate := Trunc(AIDCard.DateIn);

  MyProdMinTot         := 0;
  MyPayedBreaksTot     := 0;
  MySaveDate_In_CutOff := 0; // MR:06-01-2004
  MySaveDate_Out       := AIDCard.DateOut;
  MySaveDate_In        := AIDCard.DateIn;
  // MR:08-09-2003
  // Overnight scan?
  MyIsOverNight := Trunc(AIDCard.DateOut) > Trunc(AIDCard.DateIn);
  if MyIsOverNight then
  begin
    AIDCard.DateOut := Trunc(AIDCard.DateOut);
    // MR:10-10-2003 Added 'DeleteAction' and 'ADeleteIDCard
    AProdMinClass.GetProdMin(
      AIDCard, AFirstScan, False,
      ADeleteAction, ADeleteIDCard,
      MyBookingDay, MyProdMin, MyBreaksMin, MyPayedBreaks);
{$IFDEF DEBUG3}
  WDebugLog('GetProdMin. ProdMin=' + IntMin2StringTime(MyProdMin, False) +
    ' BreaksMin=' + IntMin2StringTime(MyBreaksMin, False) +
    ' PayedBreaks=' + IntMin2StringTime(MyPayedBreaks, False)
    );
{$ENDIF}
    // MR:03-12-2003 If overnight, the first value must be saved here.
    MySaveDate_In_CutOff := AIDCard.DateInCutOff;
    MyProdMinTot         := MyProdMinTot + MyProdMin;
    MyPayedBreaksTot     := MyPayedBreaksTot + MyPayedBreaks;
    if not ORASystemDM.UseShiftDateSystem then
      MyDateIn := Trunc(AIDCard.DateIn)
    else
      MyDateIn := AIDCard.ShiftDate; // 20013489.10
    if ORASystemDM.ProdHrsDoNotSubtractBreaks then // PIM-319
      ProdHrsBreaksMin := MyBreaksMin
    else
      ProdHrsBreaksMin := 0;
    if ((MyProdMin + ProdHrsBreaksMin) > 0) and AUpdateProdMinIn then // PIM-319
      UpdateProductionHour(
        Trunc(MyDateIn), {Trunc(AIDCard.DateIn),} {AIDCard.ShiftDate, } // 20013489.10
        AIDCard,
        MyProdMin + ProdHrsBreaksMin, MyPayedBreaks, UNCHECKEDVALUE); // PIM-319
    AIDCard.DateIn  := AIDCard.DateOut;
    AIDCard.DateOut := MySaveDate_Out;
  end; {MyIsOverNight}
  { FirstScan (if scan is overnight: false) }
  if MyIsOverNight then
    AFirstScan := False;
  // MR:10-10-2003 Added 'DeleteAction' and 'ADeleteIDCard
  AProdMinClass.GetProdMin(
    AIDCard, AFirstScan, ALastScan, ADeleteAction, ADeleteIDCard,
    MyBookingDay, MyProdMin, MyBreaksMin, MyPayedBreaks);
{$IFDEF DEBUG3}
  WDebugLog('GetProdMin. ProdMin=' + IntMin2StringTime(MyProdMin, False) +
    ' BreaksMin=' + IntMin2StringTime(MyBreaksMin, False) +
    ' PayedBreaks=' + IntMin2StringTime(MyPayedBreaks, False)
    );
{$ENDIF}
  // MR:03-12-2003 If overnight, the first value must be restored here.
  if MyIsOverNight then
    AIDCard.DateInCutOff := MySaveDate_In_CutOff;
  MyProdMinTot     := MyProdMinTot + MyProdMin;
  MyPayedBreaksTot := MyPayedBreaksTot + MyPayedBreaks;
  if not ORASystemDM.UseShiftDateSystem then
    MyDateOut := Trunc(AIDCard.DateOut)
  else
    MyDateOut := AIDCard.ShiftDate; // 20013489.10
  if ORASystemDM.ProdHrsDoNotSubtractBreaks then // PIM-319
    ProdHrsBreaksMin := MyBreaksMin
  else
    ProdHrsBreaksMin := 0;
  if ((MyProdMin + ProdHrsBreaksMin) > 0) and AUpdateProdMinOut then // PIM-319
    UpdateProductionHour(
      Trunc(MyDateOut), {Trunc(AIDCard.DateOut),} {AIDCard.ShiftDate,} // 20013489.10
      AIDCard,
      MyProdMin + ProdHrsBreaksMin, MyPayedBreaks, UNCHECKEDVALUE); // PIM-319
  AIDCard.DateIn  := MySaveDate_In;
  AIDCard.DateOut := MySaveDate_Out;
  if MyIsOverNight then
  begin
    AProdMinClass.GetShiftDay(AIDCard, MySaveDate_In, MySaveDate_Out);
    MyBookingDay := Trunc(MySaveDate_In);
  end;
  // The total amount of minute for write in
  // PRODHOURPEREMPLOYEE is: ProdMin + PayedBreaks
  // MR:09-09-2003
  if AUpdateSalMin then
  begin
    if ORASystemDM.UseShiftDateSystem then
      MyBookingDay := AIDCard.ShiftDate;
    ProcessBookings(
      MyBookingDay, {AIDCard.ShiftDate,} // 20013489.10
      AIDCard,
      AUpdateProdMinOut or AUpdateProdMinIn, False{RoundMinutes},
      MyProdMinTot, MyPayedBreaksTot, UNCHECKEDVALUE);
    if AProdMinClass.AContractRoundMinutes and ALastScanRecordForDay then
    begin
      // MRA:03-DEC-2008 RV015. Rounding Minutes.
      // Round in a different way!
      // Do it for each record found in SalaryHourPerEmployee for the
      // BookingDay and Employee. The rounding-difference (positive or negative)
      // must be changed in the record that was found, using the hour-type.

      // RV063.1. Use old method for rounding!
      // OLD METHOD:
      CalculateTotalHoursDM.ContractRoundMinutes(AIDCard,
        MyBookingDay {AIDCard.ShiftDate} ); // 20013489.10
      // NEW METHOD:
{
      // RV055.1.
      ContractRoundMinutesSalaryV3(AIDCard, MyBookingDay);
      // RV055.1.
      ContractRoundMinutesPHEPT(AIDCard, MyBookingDay);
}


(* RV015. Rounding Minutes.
      // Round minutes... for total of salary hours for this scan-day.
      ContractRoundMinutes(MyBookingDay, AIDCard, MyProdMin2);
      if MyProdMin2 > 0 then
        ProcessBookings(MyBookingDay, AIDCard,
          AUpdateProdMinOut or AUpdateProdMinIn, True{RoundMinutes},
          MyProdMin2, 0, {PayedBreaksTot} UNCHECKEDVALUE)
      else
        if MyProdMin2 < 0 then
          ProcessBookingsForContractRoundMinutes(MyBookingDay,
          AIDCard, AUpdateProdMinOut or AUpdateProdMinIn, True{ARoundMinutes},
          MyProdMin2, 0{PayedBreaksTot}, UNCHECKEDVALUE);
*)
    end;
  end;
{$IFDEF DEBUG3}
  WDebugLog('ProcessTimeRecording - End');
{$ENDIF}
end;

procedure TGlobalDM.UnprocessProcessScans(AIDCard: TScannedIDCard;
  AFromDate, AProdDate1, AProdDate2, AProdDate3, AProdDate4: TDateTime;
  AProcessSalary, ADeleteAction: Boolean;
  ARecalcProdHours: Boolean;
  AFromProcessPlannedAbsence: Boolean=False;
  AManual: Boolean=False); // 20013489 Call with True when it is about manual hours!
var
  StartDate, EndDate, CurrentBookingDay,
  CurrentStart, CurrentEnd: TDateTime;
  Index: Integer;
  TotEarnedMin: Double;
  CurrentIDCard: TScannedIDCard;
  CurrentTFT, CurrentBonusInMoney: Boolean;
  RecordsForUpdate: TList;
  WhereSelect: String;
  AddToProdListOut, AddToProdListIn, AddToSalList: Boolean;
  ABookmarkRecord: PTBookmarkRecord;
  ProdDate: array[1..4] of TDateTime;
  I: Integer;
  LastScanRecordForDay: Boolean;
  BookingDayCurrent, BookingDayNext: TDateTime;
  CurrentProdDate1, CurrentProdDate2, CurrentProdDate3,
  CurrentProdDate4: TDateTime;
  GoOn: Boolean;
  SalDateFound, ProdDateFound: Boolean;
  procedure GetTFT_Bonus(AIDCard: TScannedIDCard);
  begin
    with oqUPSGetTFT_Bonus do
    begin
      ClearVariables;
      SetVariable('EMPLOYEE_NUMBER', AIDCard.EmployeeCode);
      Execute;
      CurrentTFT          := False;
      CurrentBonusInMoney := False;
      if not Eof then
      begin
        CurrentTFT := (FieldAsString('TIME_FOR_TIME_YN') = CHECKEDVALUE);
        CurrentBonusInMoney :=
        	(FieldAsString('BONUS_IN_MONEY_YN') = CHECKEDVALUE);
      end; {if not Eof}
    end; {with oqUPSGetTFT_Bonus}
  end; {GetTFT_Bonus}

  procedure DeleteAbsenceHours(AIDCard: TScannedIDCard;
    AStartDate, AEndDate: TDateTime);
    // RV045.1.
    // When there are no Contract Group settings for TFT,
    // then get TFT-settings for hourtype used for absence hours,
    // else the settings that were determined earlier are used.
    procedure DetermineTimeForTimeSettings;
    begin
      if not ContractGroupTFTExists then
      begin
        with oqUPSDeleteAbsenceHours do
        begin
          if FieldAsString('OVERTIME_YN') = CHECKEDVALUE then
          begin
            CurrentTFT :=
              (FieldAsString('TIME_FOR_TIME_YN') = CHECKEDVALUE);
            CurrentBonusInMoney :=
              (FieldAsString('BONUS_IN_MONEY_YN') = CHECKEDVALUE);
          end
          else
          begin
            CurrentTFT := False;
            CurrentBonusInMoney := False;
          end;
        end;
      end;
    end; // procedure DetermineTimeForTimeSettings;
  begin
    // RV071.6. 550497.
    //   Addition of H.ADV_YN
    //   Bugfix: TotEarnedMin is NOT the total earned minutes, but per the
    //           EarnedMin per salary-date, so it should NOT replace the
    //           existing hours, but only subtract it.
    // Here first the old minutes are deleted from the ABSENCETOTAL-table
    // for TFT and ADV, before the new values are calculated.
    with oqUPSDeleteAbsenceHours do
    begin
      ClearVariables;
      SetVariable('STARTDATE',       AStartDate);
      SetVariable('ENDDATE',         AEndDate);
      SetVariable('EMPLOYEE_NUMBER', AIDCard.EmployeeCode);
      SetVariable('MANUAL_YN',       UNCHECKEDVALUE);
      Execute;
      while not Eof do
      begin
        DetermineTimeForTimeSettings; // RV045.1.
        if CurrentTFT then
        begin
          TotEarnedMin := FieldAsInteger('SALARY_MINUTE');
          if (not CurrentBonusInMoney) then
            TotEarnedMin := Round(TotEarnedMin +
              TotEarnedMin * FieldAsFloat('BONUS_PERCENTAGE') / 100);
        // MRA:21-SEP-2009 RV033.5. Bugfix. CurrentIDCard is not filled!
        // Use AIDCard instead!
          if TotEarnedMin > 0 then
            UpdateAbsenceTotal('EARNED_TFT_MINUTE', FieldAsDate('SALARY_DATE'),
              AIDCard.EmployeeCode, (-1) * TotEarnedMin,
                False, {True}); // RV071.6. Bugfix! Must be False!
        end;{if CurrentTFT}
        // RV071.6. 550497. Shorter Working Week handling (ADV)
        if (FieldAsString('OVERTIME_YN') = CHECKEDVALUE) and
          (FieldAsString('ADV_YN') = CHECKEDVALUE) then
        begin
          TotEarnedMin := FieldAsInteger('SALARY_MINUTE');
          if (not CurrentBonusInMoney) then
            TotEarnedMin := Round(TotEarnedMin +
              TotEarnedMin * FieldAsFloat('BONUS_PERCENTAGE') / 100);
          if TotEarnedMin > 0 then
            UpdateAbsenceTotal('EARNED_SHORTWWEEK_MINUTE',
              FieldAsDate('SALARY_DATE'), AIDCard.EmployeeCode,
              (-1) * TotEarnedMin, False);
        end;
        Next;
      end;{while not Eof}
    end; {with oqUPSDeleteAbsenceHours}
  end; {DeleteAbsenceHours}

  // RV056.1. Added From-To-Date.
  procedure DeleteProduction(AIDCard: TScannedIDCard;
    AStartDate, AEndDate: TDateTime);
  begin
{$IFDEF DEBUG3}
  WDebugLog('DeleteProduction. From=' + DateToStr(AStartDate) +
    ' To=' + DateToStr(AEndDate)
    );
{$ENDIF}
    // RV056.1. Query changed, so it uses a from-to-date.
    with oqUPSDeleteProduction do
    begin
      ClearVariables;
//      SetVariable('PRODHOUREMPLOYEE_DATE', ABookingDay);
      SetVariable('STARTDATE',             AStartDate);
      SetVariable('ENDDATE',               AEndDate);
      SetVariable('EMPLOYEE_NUMBER',       AIDCard.EmployeeCode);
      SetVariable('MANUAL_YN',             UNCHECKEDVALUE);
      Execute;
    end;
  end; {DeleteProduction}

  procedure DeleteSalary(AIDCard: TScannedIDCard;
    AStartDate, AEndDate: TDateTime);
  begin
{$IFDEF DEBUG3}
  WDebugLog('DeleteSalary. From=' + DateToStr(AStartDate) +
    ' To=' + DateToStr(AEndDate)
    );
{$ENDIF}
    with oqUPSDeleteSalary do
    begin
      ClearVariables;
      SetVariable('STARTDATE',       AStartDate);
      SetVariable('ENDDATE',         AEndDate);
      SetVariable('EMPLOYEE_NUMBER', AIDCard.EmployeeCode);
      SetVariable('MANUAL_YN',       UNCHECKEDVALUE);
      Execute;
    end;{with oqUPSDeleteSalary}
    // MR:07-11-2003 Changes because 'CurrentIDCard' is probably not filled.
    // Now 'EmployeeIDCard' is used and also a parameter.
    DeleteProdHourForAllTypes(AIDCard, AStartDate, AEndDate,
      UNCHECKEDVALUE);
    // RV056.1.
    if ARecalcProdHours then
      DeleteProduction(AIDCard, AStartDate, AEndDate);
  end; {DeleteSalary}

  // 20013489
  // It is possible a scan must be booked on a different date, indicated
  // by ShiftDate (which can be an overnight-shift).
  procedure ShiftDateCheckOnProdDates(AIDCard: TScannedIDCard;
    var AProdDate1, AProdDate2, AProdDate3, AProdDate4: TDateTime);
  begin
    // 20013489
    // Always store production on start of shift.
//    if AIDCard.OvernightShift then
    begin
      if AProdDate1 <> NullDate then
        if AIDCard.ShiftDate <> AProdDate1 then
          AProdDate1 := AIDCard.ShiftDate;
      if AProdDate2 <> NullDate then
        if AIDCard.ShiftDate <> AProdDate2 then
          AProdDate2 := AIDCard.ShiftDate;
      if AProdDate3 <> NullDate then
        if AIDCard.ShiftDate <> AProdDate3 then
          AProdDate3 := AIDCard.ShiftDate;
      if AProdDate4 <> NullDate then
        if AIDCard.ShiftDate <> AProdDate4 then
          AProdDate4 := AIDCard.ShiftDate;
    end;
  end; // ShiftDateCheckOnProdDates
  // PIM-49
  // Extra check to see if a scan-date is equal to a production-date.
  function ProdDateCheck(ADateIn, ADateOut: TDateTime): Boolean;
  begin
    Result := False;
    if
      (
        ((AProdDate1 <> NullDate) and (Trunc(ADateIn) = AProdDate1))
        or
        ((AProdDate2 <> NullDate) and (Trunc(ADateIn) = AProdDate2))
        or
        ((AProdDate3 <> NullDate) and (Trunc(ADateIn) = AProdDate3))
        or
        ((AProdDate4 <> NullDate) and (Trunc(ADateIn) = AProdDate4))
        or
        ((AProdDate1 <> NullDate) and (Trunc(ADateOut) = AProdDate1))
        or
        ((AProdDate2 <> NullDate) and (Trunc(ADateOut) = AProdDate2))
        or
        ((AProdDate3 <> NullDate) and (Trunc(ADateOut) = AProdDate3))
        or
        ((AProdDate4 <> NullDate) and (Trunc(ADateOut) = AProdDate4))
      )
      then
      Result := True;
  end; // ProdDateCheck
  // PIM-87
  procedure PackageCalcHourUnProcessProcessScans;
  begin
    with oqUnProcessProcessScans do
    begin
      ClearVariables;
      // Boolean params (convert to integer)
      SetVariable('aprocesssalary', Bool2Int(AProcessSalary));
      SetVariable('adeleteaction', Bool2Int(ADeleteAction));
      SetVariable('arecalcprodhours', Bool2Int(ARecalcProdHours));
      SetVariable('afromprocessplannedabsence', Bool2Int(AFromProcessPlannedAbsence));
      SetVariable('amanual', Bool2Int(AManual));
      // AIDCcard
      SetVariable('EmployeeCode', AIDCard.EmployeeCode);
      SetVariable('PlantCode', AIDCard.PlantCode);
      SetVariable('ShiftNumber', AIDCard.ShiftNumber);
      SetVariable('WorkspotCode', AIDCard.WorkSpotCode);
      SetVariable('JobCode', AIDCard.JobCode);
      SetVariable('DepartmentCode', AIDCard.DepartmentCode);
      SetVariable('DateIn', AIDCard.DateIn);
      SetVariable('DateOut', AIDCard.DateOut);
      SetVariable('ShiftDate', AIDCard.ShiftDate);
      // Other params
      SetVariable('afromdate', AFromDate);
      SetVariable('aproddate1', AProdDate1);
      SetVariable('aproddate2', AProdDate2);
      SetVariable('aproddate3', AProdDate3);
      SetVariable('aproddate4', AProdDate4);
      SetVariable('acurrentprogramuser', ORASystemDM.CurrentProgramUser);
      SetVariable('acurrentcomputername', ORASystemDM.CurrentComputerName);
{$IFDEF DEBUG3}
      SetVariable('adebug', 1);
{$ELSE}
      SetVariable('adebug', 0);
{$ENDIF}
      Execute;
    end;
  end; // PackageCalcHourUnProcessProcessScans
  // PIM-87
  procedure PackageCalcHourUnProcessProcessScansDebugLog;
  begin
    try
      WDebugLog('PackageCalcHour UnProcessProcessScans - Params');
      // AIDCcard
      WDebugLog('EmployeeCode=' + IntToStr(AIDCard.EmployeeCode));
      WDebugLog('PlantCode=' + AIDCard.PlantCode);
      WDebugLog('ShiftNumber=' + IntToStr(AIDCard.ShiftNumber));
      WDebugLog('WorkspotCode=' + AIDCard.WorkSpotCode);
      WDebugLog('JobCode=' + AIDCard.JobCode);
      WDebugLog('DepartmentCode=' + AIDCard.DepartmentCode);
      WDebugLog('DateIn=' + DateTimeToStr(AIDCard.DateIn));
      WDebugLog('DateOut=' + DateTimeToStr(AIDCard.DateOut));
      WDebugLog('ShiftDate=' + DateToStr(AIDCard.ShiftDate));
      // Other params
      WDebugLog('afromdate=' + DateToStr(AFromDate));
      WDebugLog('aproddate1=' + DateToStr(AProdDate1));
      WDebugLog('aproddate2=' + DateToStr(AProdDate2));
      WDebugLog('aproddate3=' + DateToStr(AProdDate3));
      WDebugLog('aproddate4=' + DateToStr(AProdDate4));
      WDebugLog('aprocesssalary=' + Bool2Str(AProcessSalary));
      WDebugLog('adeleteaction=' + Bool2Str(ADeleteAction));
      WDebugLog('arecalcprodhours=' + Bool2Str(ARecalcProdHours));
      WDebugLog('afromprocessplannedabsence=' + Bool2Str(AFromProcessPlannedAbsence));
      WDebugLog('amanual=' + Bool2Str(AManual));
      WDebugLog('acurrentprogramuser=' + ORASystemDM.CurrentProgramUser);
      WDebugLog('acurrentcomputername=' + ORASystemDM.CurrentComputerName);
    except
    end;
  end; // PackageCalcHourUnProcessProcessScansDebugLog
begin
{$IFDEF DEBUG3}
  WDebugLog('Start UnprocessProcessScans');
{$ENDIF}
  // PIM-87
  if ORASystemDM.UsePackageHourCalc then
  begin
    try
      PackageCalcHourUnProcessProcessScans;
    except
      on E: EOracleError do
      begin
        WErrorLog('PackageCalcHourUnProcessProcessScans: ' + E.Message);
        PackageCalcHourUnProcessProcessScansDebugLog;
      end;
      on E: Exception do
      begin
        WErrorLog('PackageCalcHourUnProcessProcessScans: ' + E.Message);
        PackageCalcHourUnProcessProcessScansDebugLog;
      end;
    end;
    Exit;
  end;
  
  if not ORASystemDM.UseShiftDateSystem then
  begin
    if AFromProcessPlannedAbsence then
    begin
      StartDate := Trunc(AIDCard.DateIn);
      EndDate := Trunc(AIDCard.DateOut);
    end
    else
      AProdMinClass.GetShiftDay(AIDCard, StartDate, EndDate);
    // Calculate the employe booking day
    // This variable is not used!
//    BookingDay := Trunc(StartDate);
  end // not UseShiftDateSystem
  else
  begin // UseShiftDateSystem
    // 20013489.
    // Always recalc production hours, to ensure it is OK with overnight-scans.
    // 20013489.10. When 'from timerecordingapp' then do not recalculate the prod. hours!
    // 20014327 Overnight-shift-system must used be optional.
    ARecalcProdHours := not AIDCard.FromTimeRecordingApp;

    // 20013489.
    if not AFromProcessPlannedAbsence then
    begin
      AProdMinClass.GetShiftDay(AIDCard, StartDate, EndDate);
      // 20013489.
      // When AManual = False, then it is about Scans or Process Planned Absence.
      // When AManual = True, then it is about manual hours, not a real scan or
      // Process Planned Absence: Then the DateIn and DateOut is not about a real
      // scan. This means the manual hours must always be booked on the same day,
      // never on a previous day. Also the booking-period must be based on that day,
      // not on previous day/week/period/month.
      if AManual then
      begin
        StartDate := Trunc(AIDCard.DateIn) + Frac(StartDate);
        EndDate := Trunc(AIDCard.DateIn) + Frac(EndDate);
        if StartDate > EndDate then
          EndDate := EndDate + 1;
      end;
    end;

    // 20013489.
    if not AFromProcessPlannedAbsence then
      ShiftDateCheckOnProdDates(AIDCard, AProdDate1, AProdDate2, AProdDate3,
        AProdDate4);
  end; // if UseShiftDateSystem

  // 20013489.
  // ProdDate stores the date for the production-record.
  // Originally this was the same as the dates from the scan,
  // but with overnight-shift-system it can be different!
  // ProdDate1 = Based on New-scan-date-out
  // ProdDate2 = Based on New-scan-date-in
  // ProdDate3 = Based on Old-scan-date-out
  // ProdDate4 = Based on Old-scan-date-in

  ProdDate[1] := AProdDate1;
  ProdDate[2] := AProdDate2;
  ProdDate[3] := AProdDate3;
  ProdDate[4] := AProdDate4;

{$IFDEF DEBUG3}
begin
  WDebugLog('AIDCard');
  WDebugLog('- EMP=' + IntToStr(AIDCard.EmployeeCode) +
    ' PL=' + AIDCard.PlantCode +
    ' SHFT=' + IntToStr(AIDCard.ShiftNumber) +
    ' WC=' + AIDCard.WorkSpotCode +
    ' JC=' + AIDCard.JobCode +
    ' DEPT=' + AIDCard.DepartmentCode
    );
  WDebugLog(
    '- DIN=' + DateTimeToStr(AIDCard.DateIn) +
    ' DOUT=' + DateTimeToStr(AIDCard.DateOut) +
    ' SHIFTDATE=' + DateToStr(AIDCard.ShiftDate)
    );
  WDebugLog('- Params' +
    ' AFromDate=' + DateTimeToStr(AFromDate) +
    ' AProdDate1=' + DateTimeToStr(AProdDate1) +
    ' AProdDate2=' + DateTimeToStr(AProdDate2) +
    ' AProdDate3=' + DateTimeToStr(AProdDate3) +
    ' AProdDate4=' + DateTimeToStr(AProdDate4) +
    ' AProcessSalary=' + Bool2Str(AProcessSalary) +
    ' ADeleteAction=' + Bool2Str(ADeleteAction) +
    ' ARecalcProdHours=' + Bool2Str(ARecalcProdHours) +
    ' AFromProcessPlannedAbsence=' + Bool2Str(AFromProcessPlannedAbsence) +
    ' AManual=' + Bool2Str(AManual)
    );
end;
{$ENDIF}

  // Booking date
  // Related to 20013489.
  // Always the complete overtime-period must be calculated!
  // IMPORTANT:
  // The StartDate and EndDate must be filled before 'ComputerOverTimePeriod'
  // is called. Otherwise this function can not know for what period it must
  // calculate this.
  if ORASystemDM.UseShiftDateSystem then
    if AFromProcessPlannedAbsence then
    begin
      StartDate := Trunc(AIDCard.DateIn);
      EndDate := Trunc(AIDCard.DateOut);
    end;

  // Over Time Period
  ComputeOvertimePeriod(AIDCard, StartDate, EndDate);

{$IFDEF DEBUG3}
  WDebugLog('- OvertimePeriod' +
    ' StartDate=' + DateTimeToStr(StartDate) +
    ' EndDate=' + DateTimeToStr(EndDate) +
    ' FromDate=' + DateToStr(AFromDate)
    );
{$ENDIF}

  if AProcessSalary then
  begin
    GetTFT_Bonus(AIDCard);
    // Related to 20013489
    // ALWAYS process all records within the overtime-period
    if not ORASystemDM.UseShiftDateSystem then
    begin
      if (AFromDate > StartDate) and (AFromDate < EndDate) then
      begin
        DeleteAbsenceHours(AIDCard, AFromDate, EndDate);
        DeleteSalary(AIDCard, AFromDate, EndDate);
      end
      else
      begin
        DeleteAbsenceHours(AIDCard, StartDate, EndDate);
        DeleteSalary(AIDCard, StartDate, EndDate);
      end;
    end // if not UseShiftDateSystem
    else
    begin // if UseShiftDateSystem
      DeleteAbsenceHours(AIDCard, StartDate, EndDate);
      DeleteSalary(AIDCard, StartDate, EndDate);
    end; // if UseShiftDateSystem
  end; // if AProcessSalary

  // RV056.1. Only recalc prodhours when 'ProdDate' is filled.
  // 20013489.10.
//  if not AIDCard.FromTimeRecordingApp then // 20014327 Do not do this!
    if not ARecalcProdHours then
    begin
      for I := 1 to 4 do
        if ProdDate[I] <> NullDate then
        begin
          DeleteProduction(AIDCard, ProdDate[I], ProdDate[I]);
        end;
    end;

  // Al data ready. Start write in Data_Base
  RecordsForUpdate := Tlist.Create;
  // Select all records from TIMEREGSCANNING that result booking date for
  // Salary hours in OverTime period:
  // StartDateTime - 1day and EndDateTime + 1day

  // MR:05-09-2003
  if AProcessSalary then
  begin
    if not ORASystemDM.UseShiftDateSystem then
      WhereSelect :=
        ' WHERE ' +
        '   T.DATETIME_IN >= :STARTDATE AND ' + #13#10 +
        '   T.DATETIME_IN <= :ENDDATE AND '
    else
      // 20013489 Filter on SHIFT_DATE or not
      WhereSelect :=
        ' WHERE ' +
        '   T.SHIFT_DATE >= :STARTDATE AND ' +
        '   T.SHIFT_DATE <  :ENDDATE AND ';
  end
  else
  begin
    WhereSelect :=
      'WHERE ' + #13#10 +
      '  ( ' + #13#10 +
      '   ((T.DATETIME_IN >= :STARTDATE1) AND ' + #13#10 +
      '    (T.DATETIME_IN < :ENDDATE1)) OR ' + #13#10 +
      '   ((T.DATETIME_IN >= :STARTDATE2) AND ' + #13#10 +
      '    (T.DATETIME_IN < :ENDDATE2)) OR ' + #13#10 +
      '   ((T.DATETIME_IN >= :STARTDATE3) AND ' + #13#10 +
      '    (T.DATETIME_IN < :ENDDATE3)) OR ' + #13#10 +
      '   ((T.DATETIME_IN >= :STARTDATE4) AND ' + #13#10 +
      '    (T.DATETIME_IN < :ENDDATE4)) OR ' + #13#10 +
      '   ((T.DATETIME_OUT >= :STARTDATE1) AND ' + #13#10 +
      '    (T.DATETIME_OUT < :ENDDATE1)) OR ' + #13#10 +
      '   ((T.DATETIME_OUT >= :STARTDATE2) AND ' + #13#10 +
      '    (T.DATETIME_OUT < :ENDDATE2)) OR ' + #13#10 +
      '   ((T.DATETIME_OUT >= :STARTDATE3) AND ' + #13#10 +
      '    (T.DATETIME_OUT < :ENDDATE3)) OR ' + #13#10 +
      '   ((T.DATETIME_OUT >= :STARTDATE4) AND ' + #13#10 +
      '    (T.DATETIME_OUT < :ENDDATE4))' + #13#10 +
      '  ) AND ' + #13#10;
  end;

  // MR:03-02-2005 Also get department from workspot!
  // TD-23386 Also get hourtype on shift: SHIFT_HOURTYPE_NUMBER
  // TD-23386 Related to this todo: Also get workspot-hourtype here.
  odsWork.Active := False;
  odsWork.DeleteVariables;
  odsWork.SQL.Clear;
  odsWork.SQL.Add(
    'SELECT ' +
    '  T.DATETIME_IN, T.EMPLOYEE_NUMBER, T.PLANT_CODE, ' +
    '  T.WORKSPOT_CODE, T.JOB_CODE, T.SHIFT_NUMBER, ' +
    '  T.DATETIME_OUT, T.PROCESSED_YN, T.IDCARD_IN, ' +
    '  T.IDCARD_OUT, W.DEPARTMENT_CODE, ' +
    '  T.SHIFT_DATE, ' +
    '  S.HOURTYPE_NUMBER SHIFT_HOURTYPE_NUMBER, ' +
    '  CASE ' +
    '    WHEN S.HOURTYPE_NUMBER IS NOT NULL THEN ' +
    '      (SELECT H.IGNORE_FOR_OVERTIME_YN FROM HOURTYPE H ' +
    '       WHERE H.HOURTYPE_NUMBER = S.HOURTYPE_NUMBER) ' +
    '    ELSE ' +
    '      '''' ' +
    '  END SHIFT_IGNORE_FOR_OVERTIME_YN, ' +
    '  W.HOURTYPE_NUMBER WORKSPOT_HOURTYPE_NUMBER, ' +
    '  CASE ' +
    '    WHEN W.HOURTYPE_NUMBER IS NOT NULL THEN ' +
    '      (SELECT H.IGNORE_FOR_OVERTIME_YN FROM HOURTYPE H ' +
    '       WHERE H.HOURTYPE_NUMBER = W.HOURTYPE_NUMBER) ' +
    '    ELSE ' +
    '      '''' ' +
    '  END IGNORE_FOR_OVERTIME_YN ' +
    'FROM ' +
    '  TIMEREGSCANNING T INNER JOIN WORKSPOT W ON ' +
    '    T.PLANT_CODE = W.PLANT_CODE AND ' +
    '    T.WORKSPOT_CODE = W.WORKSPOT_CODE ' +
    '  INNER JOIN SHIFT S ON ' +
    '    T.PLANT_CODE = S.PLANT_CODE AND ' +
    '    T.SHIFT_NUMBER = S.SHIFT_NUMBER ' +
    WhereSelect +
    '  T.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND ' +
    '  T.PROCESSED_YN = :PROCESSED_YN ' +
    'ORDER BY ' +
    '  T.DATETIME_IN '
  );
  if AProcessSalary then
  begin
    if not ORASystemDM.UseShiftDateSystem then
    begin
      odsWork.DeclareAndSet('STARTDATE', otdate, StartDate - 1);
      odsWork.DeclareAndSet('ENDDATE',   otDate, EndDate + 1);
    end
    else
    begin
      // 20013489 Compare with SHIFT_DATE
      odsWork.DeclareAndSet('STARTDATE', otdate, StartDate);
      odsWork.DeclareAndSet('ENDDATE',   otDate, Trunc(EndDate) + 1);
    end;
  end
  else
  begin
    odsWork.DeclareAndSet('STARTDATE1', otDate, Trunc(AProdDate1));
    odsWork.DeclareAndSet('STARTDATE2', otDate, Trunc(AProdDate2));
    odsWork.DeclareAndSet('STARTDATE3', otDate, Trunc(AProdDate3));
    odsWork.DeclareAndSet('STARTDATE4', otDate, Trunc(AProdDate4));
    odsWork.DeclareAndSet('ENDDATE1',   otDate, Trunc(AProdDate1) + 1);
    odsWork.DeclareAndSet('ENDDATE2',   otDate, Trunc(AProdDate2) + 1);
    odsWork.DeclareAndSet('ENDDATE3',   otDate, Trunc(AProdDate3) + 1);
    odsWork.DeclareAndSet('ENDDATE4',   otDate, Trunc(AProdDate4) + 1);
  end;
  odsWork.DeclareAndSet('EMPLOYEE_NUMBER', otInteger, AIDCard.EmployeeCode);
  odsWork.DeclareAndSet('PROCESSED_YN',    otString,  CHECKEDVALUE);
  odsWork.Active := True;
  odsWork.First;
// MR:05-09-2003 NEW while...
  // 20015346 Round scans to whole minutes (with RoundTime-function) before
  // hours are calculated.
  while not odsWork.Eof do
  begin
{$IFDEF DEBUG3}
  WDebugLog(
    ' odsWork.DateIn=' + DateTimeToStr(RoundTime(odsWork.FieldByName('DATETIME_IN').AsDateTime,1)) +
    '  DateOut=' + DateTimeToStr(RoundTime(odsWork.FieldByName('DATETIME_OUT').AsDateTime,1))
    );
{$ENDIF}
    if ORASystemDM.UseShiftDateSystem then
    begin
      CurrentProdDate1 := NullDate;
      CurrentProdDate2 := NullDate;
      CurrentProdDate3 := NullDate;
      CurrentProdDate4 := NullDate;
    end;

    // PIM-49
    if (ORASystemDM.UseShiftDateSystem and (not AProcessSalary))
      or
      (not ORASystemDM.UseShiftDateSystem) then
    begin
      SalDateFound :=
        (
          (RoundTime(odsWork.FieldByName('DATETIME_IN').AsDateTime,1) >= StartDate) and
          (RoundTime(odsWork.FieldByName('DATETIME_IN').AsDateTime,1) <= EndDate)
         or
          (RoundTime(odsWork.FieldByName('DATETIME_OUT').AsDateTime,1) >= StartDate) and
          (RoundTime(odsWork.FieldByName('DATETIME_OUT').AsDateTime,1) <= EndDate)
        );
      ProdDateFound :=
        ProdDateCheck(RoundTime(odsWork.FieldByName('DATETIME_IN').AsDateTime,1),
          RoundTime(odsWork.FieldByName('DATETIME_OUT').AsDateTime,1));
      if not
        (
          SalDateFound
         or
          ProdDateFound
        ) then
        begin
{$IFDEF DEBUG3}
  WDebugLog('-> Skipped');
{$ENDIF}
          odsWork.Next;
          Continue;
        end;
    end; // if (not UseShiftDateSystem) or (not ProcessSalary)

    AddToProdListOut := False;
    AddToProdListIn  := False;
    AddToSalList     := False;
    EmptyIDCard(CurrentIDCard);
    // do not refill static Plant values
    // MR:10-05-2005 Call this function with an 'oqWork' instead of 'nil'.
    PopulateIDCard(CurrentIDCard, odsWork, True);
    CurrentBookingDay := Trunc(CurrentIDCard.DateIn);
    if AProcessSalary then
    begin
      AProdMinClass.GetShiftDay(CurrentIDCard, CurrentStart, CurrentEnd);
      CurrentBookingDay := Trunc(CurrentStart);
      // Calculate the employee booking day
      if ORASystemDM.UseShiftDateSystem then
        CurrentIDCard.ShiftDate := CurrentBookingDay // 20013489
      else
        CurrentIDCard.ShiftDate := Trunc(CurrentIDCard.DateIn);
      // RV056.1. Fill Proddates here, so it will result in a recalculation
      //          of the productionhours.
      if ARecalcProdHours then
      begin
        ProdDate[1] := Trunc(CurrentIDCard.DateIn);
        ProdDate[2] := Trunc(CurrentIDCard.DateOut);
        if ORASystemDM.UseShiftDateSystem then
          ShiftDateCheckOnProdDates(CurrentIDCard, ProdDate[1],
            ProdDate[2], ProdDate[1], ProdDate[2]);
      end;
      ComputeOvertimePeriod(CurrentIDCard, CurrentStart, CurrentEnd);
{$IFDEF DEBUG3}
  WDebugLog('- DateIn=' + DateTimeToStr(CurrentIDCard.DateIn) +
    ' DateOut=' + DateTimeToStr(CurrentIDCard.DateOut) +
    ' CurrentBookingDay=' + DateToStr(CurrentBookingDay) +
    ' CurrentStart=' + DateTimeToStr(CurrentStart) +
    ' CurrentEnd=' + DateTimeToStr(CurrentEnd) +
    ' FromDate=' + DateTimeToStr(AFromDate)
    );
{$ENDIF}
      // if same overtimeperiod and ...
      if (not ORASystemDM.UseShiftDateSystem) then
        GoOn := (Trunc(AFromDate) >= Trunc(CurrentStart)) and
          (Trunc(AFromDate) <= Trunc(CurrentEnd)) and
          (CurrentBookingDay >= Trunc(AFromDate))
      else
        GoOn := (Trunc(AFromDate) >= Trunc(CurrentStart)) and
          (Trunc(AFromDate) <= Trunc(CurrentEnd)) and
          (CurrentBookingDay >= Trunc(CurrentIDCard.ShiftDate));
{
      if (Trunc(AFromDate) >= Trunc(CurrentStart)) and
        (Trunc(AFromDate) <= Trunc(CurrentEnd)) and
        (
          (CurrentBookingDay >= Trunc(AFromDate)) or
          (
            (
              ORASystemDM.UseShiftDateSystem and
              (CurrentBookingDay >= Trunc(CurrentIDCard.ShiftDate))
            )
            or
            (
              (not ORASystemDM.UseShiftDateSystem)
            )
          ) // 20013489
        ) then
}      
      if GoOn then
      begin
        if (not ORASystemDM.UseShiftDateSystem) then
          GoOn := not (ADeleteAction and
            (CurrentIDCard.DateIn = AIDCard.DateIn) and
            (CurrentIDCard.EmployeeCode = AIDCard.EmployeeCode))
        else
          GoOn := not (ADeleteAction and
            (
              (CurrentIDCard.DateIn = AIDCard.DateIn)
              or
              (ARecalcProdHours)
            ) and // 20013489
            (CurrentIDCard.EmployeeCode = AIDCard.EmployeeCode));
{
        if not (ADeleteAction and
          (
            (CurrentIDCard.DateIn = AIDCard.DateIn)
            or
            (
              (
                (ORASystemDM.UseShiftDateSystem and ARecalcProdHours) // 20013489
              )
              or
              (
                (not ORASystemDM.UseShiftDateSystem)
              )
            )
          )
          and
          (CurrentIDCard.EmployeeCode = AIDCard.EmployeeCode)) then
}
        if GoOn then
        begin
          AddToSalList := True;
{$IFDEF DEBUG3}
  WDebugLog(' -> AddToSalList=True');
{$ENDIF}
          // RV057.1. Moved to here!
          if ARecalcProdHours then
          begin
            ProdDate[1] := Trunc(CurrentIDCard.DateIn);
            ProdDate[2] := Trunc(CurrentIDCard.DateOut);
            if ORASystemDM.UseShiftDateSystem then
              ShiftDateCheckOnProdDates(CurrentIDCard, ProdDate[1],
                ProdDate[2], ProdDate[1], ProdDate[2]);
          end;
        end;
      end;
    end; // if ProcessSalary then
    // Determine if add to Production
    // 20013489 Overnight-Shift-System
    // Only comparing dates of scans is not enough to know if there is an
    // overnight-shift involved.
    // The shift itself indicates if scans must be booked on shift-date or not.
    CurrentProdDate1 := Trunc(CurrentIDCard.DateOut);
    CurrentProdDate2 := Trunc(CurrentIDCard.DateIn);
    if ORASystemDM.UseShiftDateSystem then
      ShiftDateCheckOnProdDates(CurrentIDCard, CurrentProdDate1,
        CurrentProdDate2, CurrentProdDate3, CurrentProdDate4)
    else
    begin
      // PIM-49
      //   Scan SA 14:13-2:30 falls in end of previous period, but part after
      //   midnight falls in next period!
      if (CurrentIDCard.DateIn <= CurrentEnd) and
        (CurrentIDCard.DateOut > CurrentEnd) then
      begin
{$IFDEF DEBUG3}
  WDebugLog(' -> Exception: Part of scan falls in next period!');
{$ENDIF}
        // Part of the scan falls in next period!
        if ARecalcProdHours then
          ProdDate[2] := CurrentIDCard.DateOut;
      end;
    end;
    // Determine if add to Production
    for I := 1 to 4 do
      if (CurrentProdDate1 <> NullDate) and (ProdDate[I] <> NullDate) then
        if (Trunc(CurrentProdDate1) = Trunc(ProdDate[I])) then
        begin
          if not (ADeleteAction and
            (CurrentIDCard.DateIn = AIDCard.DateIn) and
            (CurrentIDCard.EmployeeCode = AIDCard.EmployeeCode)) then
            // IS THIS CHECK NEEDED? (20014327)
//            if ORASystemDM.UseShiftDateSystem and ARecalcProdHours then // 20013489.10.
              AddToProdListOut := True;
        end;
    // If Overnight then determine if add to Production
    if (Trunc(CurrentIDCard.DateIn) <> Trunc(CurrentIDCard.DateOut)) then
    begin
      for I := 1 to 4 do
        if (CurrentProdDate2 <> NullDate) and (ProdDate[I] <> NullDate) then
          if (Trunc(CurrentProdDate2) = Trunc(ProdDate[I])) then
          begin
            if not (ADeleteAction and
              (CurrentIDCard.DateIn = AIDCard.DateIn) and
              (CurrentIDCard.EmployeeCode = AIDCard.EmployeeCode)) then
              // IS THIS CHECK NEEDED? (20014327)
//              if ORASystemDM.UseShiftDateSystem and ARecalcProdHours then // 20013489.10.
                AddToProdListIn := True;
          end;
    end;
    if ORASystemDM.UseShiftDateSystem then
    begin
      // 20013489 Overnight-Shift-System
      // When prod. should be recalculated, also recalculate salary!
      // Otherwise salary is missing!
      if ADeleteAction then
      begin
        if AddToProdListOut or AddToProdListIn then
          AddToSalList := True;
      end;
    end; // if UseShiftDateSystem
{$IFDEF DEBUG3}
  WDebugLog('- AddToProdListOut=' + Bool2Str(AddToProdListOut) +
    ' AddToProdListIn=' + Bool2Str(AddToProdListIn) +
    ' AddToSalList=' + Bool2Str(AddToSalList)
    );
{$ENDIF}
    // Add Production and/or Salary
    if AddToProdListOut or AddToProdListIn or AddToSalList then
    begin
{$IFDEF DEBUG3}
  WDebugLog(' -> Bookmarked.');
{$ENDIF}
      New(ABookmarkRecord);
      ABookmarkRecord.Bookmark         := odsWork.GetBookmark;
      ABookmarkRecord.AddToSalList     := AddToSalList;
      ABookmarkRecord.AddToProdListOut := AddToProdListOut;
      ABookmarkRecord.AddToProdListIn  := AddToProdListIn;
      ABookmarkRecord.BookingDay := CurrentBookingDay; // TD-23416 Also store bookingday here!
      RecordsForUpdate.Add(ABookmarkRecord);
    end;
    // RV057.1. Reset ProdDate-values here.
    if ARecalcProdHours then
    begin
      ProdDate[1] := NullDate;
      ProdDate[2] := NullDate;
    end;
    odsWork.Next;
  end; {while not odsWork.Eof}

// MR:05-09-2003 NEW
  // Recalculate all deleted records from Salary and Production Table
  // Compute Salary hours for deleted Records
  { Processed := False; }
  odsWork.First;
  for Index := 0 to RecordsForUpdate.Count - 1 do
  begin
    LastScanRecordForDay := False;
    BookingDayNext := 0;
    // MR:29-07-2004
    if Index = RecordsForUpdate.Count - 1 then
      LastScanRecordForDay := True
    else
    begin
      ABookmarkRecord := RecordsForUpdate.Items[Index + 1];
      odsWork.GotoBookmark(ABookmarkRecord.Bookmark);
      // Don't free the bookmark here.
      EmptyIDCard(CurrentIDCard);
      // do not refill static Plant values
      // MR:10-05-2005 Call this function with an 'oqWork' instead of 'nil'.
      PopulateIDCard(CurrentIDCard, odsWork, True);
      // Get Bookingday of Next Scan-record
      AProdMinClass.GetShiftDay(CurrentIDCard, StartDate, EndDate);
      BookingDayNext := StartDate;
    end;
    ABookmarkRecord := RecordsForUpdate.Items[Index];
    odsWork.GotoBookmark(ABookmarkRecord.Bookmark);
    odsWork.FreeBookmark(ABookmarkRecord.Bookmark);
    EmptyIDCard(CurrentIDCard);
    // do not refill static Plant values
    // MR:10-05-2005 Call this function with an 'oqWork' instead of 'nil'.
    PopulateIDCard(CurrentIDCard, odsWork, True);
    if not LastScanRecordForDay then
    begin
      // Get Bookingday of Current Scan-record
      AProdMinClass.GetShiftDay(CurrentIDCard, StartDate, EndDate);
      BookingDayCurrent := StartDate;
      // Is Bookingday of Current different from Next?
      // RV040.1. Only compare date-parts!!!
      if Trunc(BookingDayCurrent) <> Trunc(BookingDayNext) then
        LastScanRecordForDay := True;
    end;
    // MR:10-10-2003 Added 'DeleteAction' and 'AIDCard'
    // In case of a DeleteAction, the data about this scan-to-delete
    // must be known.
    ProcessTimeRecording(CurrentIDCard, AIDCard, True, True,
      ABookmarkRecord.AddToSalList, ABookmarkrecord.AddToProdListOut,
      ABookmarkrecord.AddToProdListIn, ADeleteAction,
      LastScanRecordForDay);
    if ORASystemDM.UseShiftDateSystem then
    begin
      // 20013489
      if AFromProcessPlannedAbsence then
      begin
        // When ShiftDate is not correct (or not set), then update TRS-record
        // Be sure there is a StartDate determined.
        AProdMinClass.GetShiftDay(CurrentIDCard, StartDate, EndDate);
        if ((Trunc(StartDate) <> CurrentIDCard.ShiftDate) or
          (CurrentIDCard.ShiftDate = 0)) then
          UpdateTimeRegScanning(CurrentIDCard, Trunc(StartDate));
      end;
    end; // if UseShiftDateSystem
  end; {Index := 0 to RecordsForUpdate.Count - 1}

  // RV077.1. Recalculate balances here (like TFT and SWW/ADV).
  if RecordsForUpdate.Count > 0 then
  begin
    RecalculateBalances(AIDCard.EmployeeCode, StartDate, EndDate);
  end;

  // MR:08-09-2003
  // Clear list
  for Index := RecordsForUpdate.Count - 1 downto 0 do
  begin
    ABookmarkRecord := RecordsForUpdate.Items[Index];
    // MR:10-10-2003 'dispose' record
    Dispose(ABookMarkRecord);
    RecordsForUpdate.Remove(ABookmarkRecord);
  end;
  RecordsForUpdate.Free;
{$IFDEF DEBUG3}
  WDebugLog('End UnprocessProcessScans');
{$ENDIF}
end; // UnprocessProcessScans

// RV071.6.
function TGlobalDM.AbsenceTotalSelect(AFieldName: String;
  ABookingDay: TDateTime; AEmployeeNumber: Integer;
  var AMinutes: Double): Boolean;
var
  Year, Month, Day: word;
begin
  Result := False;
  AMinutes := 0;
  DecodeDate(ABookingDay, Year, Month, Day);
  with oqAbsenceTotal do
  begin
    Close;
    SQL.Clear;
    SQL.Add(
      'SELECT ' + NL +
      '  ' + AFieldName + ' ' + NL +
      'FROM ' + NL +
      '  ABSENCETOTAL ' + NL +
      'WHERE ' + NL +
      '  EMPLOYEE_NUMBER = :EMPNO AND ' + NL +
      '  ABSENCE_YEAR = :AYEAR '
    );
    DeleteVariables;
    DeclareAndSet('EMPNO',   otInteger,  AEmployeeNumber);
    DeclareAndSet('AYEAR',   otInteger,  Year);
    Execute;
    if not Eof then
    begin
      AMinutes := FieldAsFloat(AFieldName);
      Result := True;
    end;
  end;
end; // AbsenceTotalSelect

// RV071.6.
procedure TGlobalDM.AbsenceTotalInsert(AFieldName: String;
  ABookingDay: TDateTime; AEmployeeNumber: Integer; AMinutes: Double);
var
  Year, Month, Day: word;
begin
  DecodeDate(ABookingDay, Year, Month, Day);
  with oqAbsenceTotal do
  begin
    Close;
    SQL.Clear;
    SQL.Add(
      'INSERT INTO ABSENCETOTAL( ' + NL +
      '  EMPLOYEE_NUMBER, ABSENCE_YEAR, ' + NL +
      '  ' + AFieldName + ',' + NL +
      '  CREATIONDATE, ' + NL +
      '  MUTATIONDATE, MUTATOR) ' + NL +
      '  VALUES( ' + NL +
      '  :EMPNO, :AYEAR, :OTIME, :CDATE, :MDATE, ' + NL +
      '  :MUTATOR) '
      );
    DeleteVariables;
    DeclareAndSet('EMPNO',   otInteger,  AEmployeeNumber);
    DeclareAndSet('AYEAR',   otInteger,  Year);
    DeclareAndSet('OTIME',   otFloat,    AMinutes);
    DeclareAndSet('CDATE',   otDate,     Now);
    DeclareAndSet('MDATE',   otDate,     Now);
    DeclareAndSet('MUTATOR', otString,   ORASystemDM.CurrentProgramUser);
    Execute;
  end;
end; // AbsenceTotalInsert

// RV071.6.
procedure TGlobalDM.AbsenceTotalUpdate(AFieldName: String;
  ABookingDay: TDateTime; AEmployeeNumber: Integer; AMinutes: Double);
var
  Year, Month, Day: word;
begin
  DecodeDate(ABookingDay, Year, Month, Day);
  with oqAbsenceTotal do
  begin
    Close;
    SQL.Clear;
    SQL.Add(
      'UPDATE ABSENCETOTAL ' + NL +
      '  SET ' +
      '  ' + AFieldName + ' = :OTIME, ' + NL +
      '  MUTATIONDATE = :MDATE, MUTATOR = :MUTATOR ' + NL +
      '  WHERE EMPLOYEE_NUMBER = :EMPNO AND ' + NL +
      '  ABSENCE_YEAR = :AYEAR '
      );
    DeleteVariables;
    DeclareAndSet('EMPNO',   otInteger,  AEmployeeNumber);
    DeclareAndSet('AYEAR',   otInteger,  Year);
    DeclareAndSet('OTIME',   otFloat,    AMinutes);
    DeclareAndSet('MDATE',   otDate,     Now);
    DeclareAndSet('MUTATOR', otString,   ORASystemDM.CurrentProgramUser);
    Execute;
  end;
end; // AbsenceTotalUpdate

// RV071.6.
function TGlobalDM.UpdateAbsenceTotal(AFieldName: String;
  ABookingDay: TDateTime; AEmployeeNumber: Integer;
  AMinutes: Double; AOverwrite: Boolean): Integer;
var
  TotalMinutes, ExistingMinutes: Double;
begin
  Result := 0;
  if AbsenceTotalSelect(AFieldName, ABookingDay, AEmployeeNumber,
    ExistingMinutes) then
  begin
    if AOverwrite then
      TotalMinutes := AMinutes
    else
      TotalMinutes := ExistingMinutes + AMinutes;
    // Only update if there is a difference
    if TotalMinutes <> ExistingMinutes then
      AbsenceTotalUpdate(AFieldName, ABookingDay, AEmployeeNumber, TotalMinutes)
  end
  else
  begin
    // Only insert new record if it is <> 0
    if AMinutes <> 0 then
      AbsenceTotalInsert(AFieldName, ABookingDay, AEmployeeNumber, AMinutes);
    Result := 1;
  end;
end; // UpdateAbsenceTotal

procedure TGlobalDM.UpdateEmployeeLevel(const AIDCard: TScannedIDCard);
begin
  with oqUELSelect do
  begin
    ClearVariables;
    SetVariable('PLANT_CODE',            AIDCard.PlantCode);
    SetVariable('EMPLOYEE_NUMBER',       AIDCard.EmployeeCode);
    SetVariable('WORKSPOT_CODE',         AIDCard.WorkSpotCode);
    SetVariable('DEPARTMENT_CODE',       AIDCard.DepartmentCode);
    Execute;
  end; {with oqUELSelect}
  if not oqUELSelect.Eof then
  begin
    // update
    with oqUELUpdate do
    begin
      ClearVariables;
      SetVariable('PLANT_CODE',            AIDCard.PlantCode);
      SetVariable('EMPLOYEE_NUMBER',       AIDCard.EmployeeCode);
      SetVariable('WORKSPOT_CODE',         AIDCard.WorkSpotCode);
      SetVariable('DEPARTMENT_CODE',       AIDCard.DepartmentCode);
      SetVariable('MUTATOR',               ORASystemDM.CurrentProgramUser);
      Execute;
    end; {with oqUELUpdate}
  end   {if not oqUELSelect.Eof}
  else
  begin {else not oqUELSelect.Eof}
    // insert
    with oqUELInsert do
    begin
      ClearVariables;
      SetVariable('PLANT_CODE',            AIDCard.PlantCode);
      SetVariable('EMPLOYEE_NUMBER',       AIDCard.EmployeeCode);
      SetVariable('WORKSPOT_CODE',         AIDCard.WorkSpotCode);
      SetVariable('DEPARTMENT_CODE',       AIDCard.DepartmentCode);
      SetVariable('MUTATOR',               ORASystemDM.CurrentProgramUser);
      Execute;
    end;{with oqUELInsert}
  end;  {else not oqUELSelect.Eof}
end;

function TGlobalDM.UpdateSalaryHour(ASalaryDate: TDateTime;
  AIDCard: TScannedIDCard; AHourTypeNumber, AMinutes: Integer;
  AManual: String; AReplace: Boolean;
  UpdateYes: Boolean; (* RV023 *)
  FromManualProd: String = 'N';
  ARoundMinutes: Boolean = True): Integer;
var
  MyTotMin: Integer;
  MyInsertNewProdHrs: Boolean;
  MyMinuteNewProdHrs: Integer;
begin
  Result := AMinutes;
  if AReplace and (AMinutes = 0) then
    Exit;
{$IFDEF DEBUG3}
  WDebugLog('UpdateSalaryHour. Date=' + DateToStr(ASalaryDate) +
    ' HT=' + IntToStr(AHourTypeNumber) +
    ' Min=' + IntMin2StringTime(AMinutes, False)
    );
{$ENDIF}
  MyInsertNewProdHrs := False;
  MyMinuteNewProdHrs := 0;
  with oqUSHSelect do
  begin
    ClearVariables;
    SetVariable('SALARY_DATE',     ASalaryDate);
    SetVariable('EMPLOYEE_NUMBER', AIDCard.EmployeeCode);
    SetVariable('HOURTYPE_NUMBER', AHourTypenumber);
    SetVariable('MANUAL_YN',       AManual);
    Execute;
  end; {with}
  if not oqUSHSelect.Eof then // Update existing record
  begin
    MyTotMin := oqUSHSelect.FieldAsInteger('SALARY_MINUTE');
    if ((MyTotMin + AMinutes) <> 0) then
    begin
      with oqUSHUpdate do
      begin
        ClearVariables;
        SetVariable('SALARY_DATE',     ASalaryDate);
        SetVariable('EMPLOYEE_NUMBER', AIDCard.EmployeeCode);
        SetVariable('HOURTYPE_NUMBER', AHourTypenumber);
        SetVariable('MANUAL_YN',       AManual);
        SetVariable('MUTATOR',         ORASystemDM.CurrentProgramUser);
        // RV072.3. Not needed here yet.
//        SetVariable('FROMMANUALPROD_YN', FromManualProd);
        if AReplace then
        begin
          SetVariable('SALARY_MINUTE', AMinutes);
          Result := AMinutes - MyTotMin;
        end
        else
          SetVariable('SALARY_MINUTE', MyTotMin + AMinutes);
        Execute;
      end; {with oqUSHUpdate}
      MyInsertNewProdHrs := True;
      // MR:02-01-2003 Use 'minutes' instead of 'odsWork...'
      // This is also to prevent that a sum of minutes is used.
      MyMinuteNewProdHrs := AMinutes;
    end   {((MyTotMin + AMinutes) <> 0)}
    else
    begin {else ((MyTotMin + AMinutes) <> 0)}
      with oqUSHDelete do
      begin
        ClearVariables;
        SetVariable('SALARY_DATE',     ASalaryDate);
        SetVariable('EMPLOYEE_NUMBER', AIDCard.EmployeeCode);
        SetVariable('HOURTYPE_NUMBER', AHourTypenumber);
        SetVariable('MANUAL_YN',       AManual);
        Execute;
      end; {with}
      MyInsertNewProdHrs := False;
      MyMinuteNewProdHrs := 0;
    end;  {else ((MyTotMin + AMinutes) <> 0)}
  end   {not oqUSHSelect.Eof}
  else // Insert new record in SalaryHourerEmployee
  begin {else not oqUSHSelect.Eof}
    if AMinutes <> 0 then
    begin
      with oqUSHInsert do
      begin
        ClearVariables;
        SetVariable('SALARY_DATE',     ASalaryDate);
        SetVariable('EMPLOYEE_NUMBER', AIDCard.EmployeeCode);
        SetVariable('HOURTYPE_NUMBER', AHourTypenumber);
        SetVariable('MANUAL_YN',       AManual);
        SetVariable('SALARY_MINUTE',   AMinutes);
        SetVariable('MUTATOR',         ORASystemDM.CurrentProgramUser);
        // RV072.3. Not needed here yet.
//        SetVariable('FROMMANUALPROD_YN', FromManualProd);
        Execute;
      end; {with}
      Result := AMinutes;
// fill the extra table prodhours per hourtype
      MyInsertNewProdHrs := True;
      // MR:02-01-2003
      // Use Minutes instead of 'odsWork...'
      MyMinuteNewProdHrs := AMinutes;
    end;
  end; {else not oqUSHSelect.Eof}
  if UpdateYes then (* RV023 *)
    FillProdHourPerHourType(ASalaryDate, AIDCard,
      AHourTypeNumber, MyMinuteNewProdHrs, AManual, MyInsertNewProdHrs)
  else (* RV023 *)
// RV055.1. // RV063.1. Use old method for rounding!
    if not ORASystemDM.IsRFL then // RV072.2.
      if ARoundMinutes then // RV072.3.
        RoundMinutesProdHourPerEmpPerType(ASalaryDate, AIDCard, AHourTypeNumber,
          AMinutes, AManual);
end;

// 20015346 Default do NOT round to minutes.
procedure TGlobalDM.PopulateIDCard(var AIDCard: TScannedIDCard;
  ADataSet: TDataSet; ARoundToMinutes: Boolean=False); // 20015346
var
  WSHourTypeChecked: Boolean;
begin
  // TD-23386 Related to this order. Initialise values here
  // 20015346 Round time to minutes, also store original DateIn/Out in 2
  //          extra vars.
  AIDCard.DepartmentCode := '';
  AIDCard.ShiftHourtypeNumber := -1;
  AIDCard.WSHourtypeNumber := -1;
  AIDCard.IgnoreForOvertimeYN := '';
  WSHourTypeChecked := False;
  if ADataSet <> nil then
  begin
    AIDCard.EmployeeCode  := ADataSet.FieldByName('EMPLOYEE_NUMBER').AsInteger;
    AIDCard.ShiftNumber   := ADataSet.FieldByName('SHIFT_NUMBER').AsInteger;
    AIDCard.IDCard        := ADataSet.FieldByName('IDCARD_IN').AsString;
    AIDCard.WorkSpotCode  := ADataSet.FieldByName('WORKSPOT_CODE').AsString;
    AIDCard.JobCode       := ADataSet.FieldByName('JOB_CODE').AsString;
    AIDCard.PlantCode     := ADataSet.FieldByName('PLANT_CODE').AsString;
    // 20015346
    if ARoundToMinutes then
    begin
      AIDCard.DateIn        :=
        RoundTime(ADataSet.FieldByName('DATETIME_IN').AsDateTime,1);
      AIDCard.DateOut       :=
        RoundTime(ADataSet.FieldByName('DATETIME_OUT').AsDateTime,1);
    end
    else
    begin
      AIDCard.DateIn        :=
        RoundTimeConditional(ADataSet.FieldByName('DATETIME_IN').AsDateTime,1); // 20015346
      AIDCard.DateOut       :=
        RoundTimeConditional(ADataSet.FieldByName('DATETIME_OUT').AsDateTime,1); // 20015346
    end;
    AIDCard.DateInOriginal  := ADataSet.FieldByName('DATETIME_IN').AsDateTime;
    AIDCard.DateOutOriginal := ADataSet.FieldByName('DATETIME_OUT').AsDateTime;
    AIDCard.DateInCutOff  := AIDCard.DateIn;
    AIDCard.DateOutCutOff := AIDCard.DateOut;
    // 20013489
    AIDCard.ShiftDate     := ADataSet.FieldByName('SHIFT_DATE').AsDateTime;
    // TD-23386 Try to determine Hourtype defined on Shift.
    AIDCard.ShiftHourtypeNumber := -1;
    AIDCard.IgnoreForOvertimeYN := '';
    if Assigned(ADataSet.FindField('SHIFT_HOURTYPE_NUMBER')) then
      if ADataSet.FieldByName('SHIFT_HOURTYPE_NUMBER').AsString <> '' then
      begin
        AIDCard.ShiftHourtypeNumber :=
          ADataSet.FieldByName('SHIFT_HOURTYPE_NUMBER').AsInteger;
        if Assigned(ADataSet.FindField('SHIFT_IGNORE_FOR_OVERTIME_YN')) then
          AIDCard.IgnoreForOvertimeYN :=
            ADataSet.FieldByName('SHIFT_IGNORE_FOR_OVERTIME_YN').AsString
        else
          if Assigned(ADataSet.FindField('IGNORE_FOR_OVERTIME_YN')) then
          begin
            WSHourTypeChecked := True;
            AIDCard.IgnoreForOvertimeYN :=
              ADataSet.FieldByName('IGNORE_FOR_OVERTIME_YN').AsString;
          end;
      end;
    // TD-23386 Related to this: Also get hourtype-of-workspot here.
    AIDCard.WSHourtypeNumber := -1;
    // TD-23386 Hourtype-on-shift has higher priority then Hourtype-on-workspot.
    if AIDCard.ShiftHourtypeNumber = -1 then
    begin
      if Assigned(ADataSet.FindField('WORKSPOT_HOURTYPE_NUMBER')) then
        if ADataSet.FieldByName('WORKSPOT_HOURTYPE_NUMBER').AsString <> '' then
        begin
          AIDCard.WSHourtypeNumber :=
            ADataSet.FieldByName('WORKSPOT_HOURTYPE_NUMBER').AsInteger;
          // TD-23386 Related to this: Also get IgnoreForOvertime based on
          //          hourtype-of-workspot
          if Assigned(ADataSet.FindField('IGNORE_FOR_OVERTIME_YN')) then
            AIDCard.IgnoreForOvertimeYN :=
              ADataSet.FieldByName('IGNORE_FOR_OVERTIME_YN').AsString;
        end;
    end;
    // TD-23386 Related to this: Also get DepartmentCode.
    if Assigned(ADataSet.FindField('WORKSPOT_DEPARTMENT_CODE')) then
      AIDCard.DepartmentCode :=
        ADataSet.FieldByName('WORKSPOT_DEPARTMENT_CODE').AsString
    else
      if Assigned(ADataSet.FindField('DEPARTMENT_CODE')) then
        AIDCard.DepartmentCode :=
          ADataSet.FieldByName('DEPARTMENT_CODE').AsString;

  end;
  // PIM-12 Also get NormProdLevel
  with oqPopIDCard do
  begin
    ClearVariables;
    SetVariable('EMPLOYEE_NUMBER', AIDCard.EmployeeCode);
    SetVariable('PLANT_CODE', AIDCard.PlantCode); // PIM-12
    SetVariable('WORKSPOT_CODE', AIDCard.WorkSpotCode); // PIM-12
    SetVariable('JOB_CODE', AIDCard.JobCode); // PIM-12
    Execute;
    if not Eof then
    begin
      // TD-23386 Related to this order.
      // Do not assign department here! Because it MUST be the department
      // based on workspot (based on scan), not based on employee.
//      AIDCard.DepartmentCode := FieldAsString('DEPARTMENT_CODE');
      AIDCard.InscanEarly    := FieldAsInteger('INSCAN_MARGIN_EARLY');
      AIDCard.InscanLate     := FieldAsInteger('INSCAN_MARGIN_LATE');
      AIDCard.OutScanEarly   := FieldAsInteger('OUTSCAN_MARGIN_EARLY');
      AIDCard.OutScanLate    := FieldAsInteger('OUTSCAN_MARGIN_LATE');
      AIDCard.CutOfTime      := FieldAsString('CUT_OF_TIME_YN');
      AIDCard.ContractgroupCode :=
        FieldAsString('CONTRACTGROUP_CODE'); // RV055.1.
      AIDCard.ExceptionalBeforeOvertime :=
        FieldAsString('EXCEPTIONAL_BEFORE_OVERTIME');  // RV055.1.
      AIDCard.RoundTruncSalaryHours :=
        FieldAsInteger('ROUND_TRUNC_SALARY_HOURS'); // RV055.1.
      AIDCard.RoundMinute := FieldAsInteger('ROUND_MINUTE'); // RV055.1.
      AIDCard.NormProdLevel := FieldAsFloat('NORM_PROD_LEVEL'); // PIM-12
      AIDCard.MeasureProductivity := FieldAsString('MEASURE_PRODUCTIVITY_YN') = 'Y'; // PIM-181
    end;
    Close;
  end;
  // MR:07-MAR-2008 RV004.
  // 20013489.10. This must be TRUE when called from PersonalScreen/Timerecording!
  AIDCard.FromTimeRecordingApp := True;
  // RV082.1.
  if ORASystemDM.IsCVL then
  begin
    // Use 'overruled contract group' when this is attached to the workspot.
    AIDCard.ContractgroupCode := FindWorkspotOverruledContractGroup(
      AIDCard.PlantCode, AIDCard.WorkSpotCode, AIDCard.ContractgroupCode);
  end;
  // TD-23386 Related to this order.
  // When some values are not filled, do it here!
  if AIDCard.DepartmentCode = '' then
  begin
    with oqWorkspot do
    begin
      ClearVariables;
      SetVariable('PLANT_CODE', AIDCard.PlantCode);
      SetVariable('WORKSPOT_CODE', AIDCard.WorkSpotCode);
      Execute;
      if not Eof then
      begin
        AIDCard.DepartmentCode :=
          FieldAsString('DEPARTMENT_CODE');
        AIDCard.WSHourtypeNumber :=
          FieldAsInteger('HOURTYPE_NUMBER');
        if AIDCard.WSHourTypeNumber <> -1 then
          AIDCard.IgnoreForOvertimeYN :=
            FieldAsString('IGNORE_FOR_OVERTIME_YN');
      end;
    end; // with
  end; // if
  // TD-23386 Related to this order.
  // When some values are not filled, do it here!
  // Shift-Hourtype has LOWER priority then Workspot-Hourtype.
  if AIDCard.WSHourTypeNumber = -1 then
    if AIDCard.ShiftHourtypeNumber = -1 then
    begin
      if not WSHourTypeChecked then
      begin
        with oqShiftHT do
        begin
          ClearVariables;
          SetVariable('PLANT_CODE', AIDCard.PlantCode);
          SetVariable('SHIFT_NUMBER', AIDCard.ShiftNumber);
          Execute;
          if not Eof then
          begin
            AIDCard.ShiftHourtypeNumber := FieldAsInteger('HOURTYPE_NUMBER');
            AIDCard.IgnoreForOvertimeYN :=
              FieldAsString('IGNORE_FOR_OVERTIME_YN');
          end;
        end; // with
      end; // if
    end; // if
end; // PopulateIDCard

// MRA:09-MAR-2009 RV023.
// Update the first found record for ProdHourPerEmpPerType for the given
// Employee, Date, Hourtype-combination.
// This is used to correct minutes during rounding.
// RV063.1.
procedure TGlobalDM.RoundMinutesProdHourPerEmpPerType(ABookingDay: TDateTime;
  AEmployeeData: TScannedIDCard;
  AHourTypeNumber, AMinutes: Integer;
  AManual: String);
var
  OldProdMin, NewProdMin: Integer;
  // RV028. Store left over minutes in one of last 5 records.
  ProdMinExclBreak: Integer;
  procedure StoreProdMinExclBreak;
  begin
    with oqPEPTEBSelect do
    begin
      ClearVariables;
      SetVariable('EMPLOYEE_NUMBER', AEmployeeData.EmployeeCode);
      SetVariable('DATEFROM',        ABookingDay - 5);
      SetVariable('DATETO',          ABookingDay);
      SetVariable('MANUAL_YN',       AManual);
      Execute;
    end;
    if not oqPEPTEBSelect.Eof then
    begin
      with oqPEPTEBUpdate do
      begin
        ClearVariables;
        // Set parameters
        SetVariable('PMIN',
          oqPEPTEBSelect.FieldAsInteger('PROD_MIN_EXCL_BREAK') +
            ProdMinExclBreak);
        SetVariable('MDATE', Now);
        SetVariable('MUTATOR', ORASystemDM.CurrentProgramUser);
        // Search parameters
        SetVariable('PDATE',
          oqPEPTEBSelect.FieldAsDate('PRODHOUREMPLOYEE_DATE'));
        SetVariable('PCODE',
          oqPEPTEBSelect.FieldAsString('PLANT_CODE'));
        SetVariable('SHNO',
          oqPEPTEBSelect.FieldAsInteger('SHIFT_NUMBER'));
        SetVariable('EMPNO',
          oqPEPTEBSelect.FieldAsInteger('EMPLOYEE_NUMBER'));
        SetVariable('WCODE',
          oqPEPTEBSelect.FieldAsString('WORKSPOT_CODE'));
        SetVariable('JCODE',
          oqPEPTEBSelect.FieldAsString('JOB_CODE'));
        SetVariable('MAN',
          oqPEPTEBSelect.FieldAsString('MANUAL_YN'));
        SetVariable('HOURTYPE',
          oqPEPTEBSelect.FieldAsInteger('HOURTYPE_NUMBER'));
        Execute;
      end;
    end;
  end;
begin
  if (ORASystemDM.GetFillProdHourPerHourTypeYN = UNCHECKEDVALUE) then
    Exit;
  with oqPEPTSelect do
  begin
    ClearVariables;
    SetVariable('EMPLOYEE_NUMBER',       AEmployeeData.EmployeeCode);
    SetVariable('PRODHOUREMPLOYEE_DATE', ABookingDay);
    SetVariable('MANUAL_YN',             AManual);
    SetVariable('HOURTYPE_NUMBER',       AHourTypeNumber);
    Execute;
  end;
  if not oqPEPTSelect.Eof then
  begin
    // Only update first found record!
    ProdMinExclBreak := oqPEPTSelect.FieldAsInteger('PROD_MIN_EXCL_BREAK'); // RV028.
    OldProdMin := oqPEPTSelect.FieldAsInteger('PRODUCTION_MINUTE');
    NewProdMin := OldProdMin + AMinutes;
    if NewProdMin <> 0 then
    begin
      // ProdMin has changed, so update record
      with oqPEPTUpdate do
      begin
        // Set parameters
        ClearVariables;
        SetVariable('PMIN',    NewProdMin);
        SetVariable('MDATE',   Now);
        SetVariable('MUTATOR', ORASystemDM.CurrentProgramUser);
        // Search parameters
        SetVariable('PDATE',
          oqPEPTSelect.FieldAsDate('PRODHOUREMPLOYEE_DATE'));
        SetVariable('PCODE',
          oqPEPTSelect.FieldAsString('PLANT_CODE'));
        SetVariable('SHNO',
          oqPEPTSelect.FieldAsInteger('SHIFT_NUMBER'));
        SetVariable('EMPNO',
          oqPEPTSelect.FieldAsInteger('EMPLOYEE_NUMBER'));
        SetVariable('WCODE',
          oqPEPTSelect.FieldAsString('WORKSPOT_CODE'));
        SetVariable('JCODE',
          oqPEPTSelect.FieldAsString('JOB_CODE'));
        SetVariable('MAN',
          oqPEPTSelect.FieldAsString('MANUAL_YN'));
        SetVariable('HOURTYPE',
          oqPEPTSelect.FieldAsInteger('HOURTYPE_NUMBER'));
        Execute;
      end;
    end
    else
    begin
      // ProdMin is 0, so delete record
      with oqPEPTDelete do
      begin
        ClearVariables;
        SetVariable('PDATE',
          oqPEPTSelect.FieldAsDate('PRODHOUREMPLOYEE_DATE'));
        SetVariable('PCODE',
          oqPEPTSelect.FieldAsString('PLANT_CODE'));
        SetVariable('SHNO',
          oqPEPTSelect.FieldAsInteger('SHIFT_NUMBER'));
        SetVariable('EMPNO',
          oqPEPTSelect.FieldAsInteger('EMPLOYEE_NUMBER'));
        SetVariable('WCODE',
          oqPEPTSelect.FieldAsString('WORKSPOT_CODE'));
        SetVariable('JCODE',
          oqPEPTSelect.FieldAsString('JOB_CODE'));
        SetVariable('MAN',
          oqPEPTSelect.FieldAsString('MANUAL_YN'));
        SetVariable('HOURTYPE',
          oqPEPTSelect.FieldAsInteger('HOURTYPE_NUMBER'));
        Execute;
        StoreProdMinExclBreak; // RV028.
      end;
    end;
  end;
end;

// RV033.4. MRA:18-SEP-2009.
// Procedure that can recalculate time-for-time hours made during overtime
// that is stored in ABSENCETOTAL table.
// This must be done when salary-hours are recalculated and must be
// done for the employee and the complete year.
// RV045.1. Also: Calculation BONUS_PERCENTAGE added, this was missing!
// RV045.1. When 0 earned minutes are found, then this must also be updated!
procedure TGlobalDM.RecalcEarnedTFTHours(AEmployeeNumber: Integer;
  ADateFrom: TDatetime; ADateTo: TDateTime);
var
  DateFrom: TDateTime;
  DateTo: TDateTime;
  YearFrom, YearTo, Year, Month, Day: Word;
  EarnedTime: Double;
  IsTFT: Boolean;
  ContractGroupTFT: Boolean; // RV045.1.
  TotEarnedTime: Double; // RV045.1.
  CGTFT: Boolean; // RV045.1.
  BonusInMoney: Boolean; // RV045.1.
  BonusPercentage: Double; // RV045.1.
begin
  // RV045.1. Also check on TFT-settings on Hourtype, when no
  //          TFT-settings were made on ContractGroup-level.
  IsTFT := False;
  ContractGroupTFT := False;
  if ContractGroupTFTExists then
  begin
    ContractGroupTFT := True;
    // First check if Employee is of type TFT.
    with oqCheckEmpTFT do
    begin
      ClearVariables;
      SetVariable('EMPNO',    AEmployeeNumber);
      SetVariable('TFT_YN',   CHECKEDVALUE);
      Execute;
      if not Eof then
        if (FIeldAsInteger('CSUM') = 1) then
          IsTFT := True;
    end;
  end;
  if (not ContractGroupTFT) or (IsTFT) and
    (ADateFrom <> NullDate) and (ADateFrom <> NullDate) then
  begin
    DecodeDate(ADateFrom, YearFrom, Month, Day);
    DecodeDate(ADateTo, YearTo, Month, Day);
    for Year := YearFrom to YearTo do
    begin
      DateFrom := EncodeDate(Year, 1, 1);
      DateTo := EncodeDate(Year, 12, 31);
      // RV045.1. Query changed.
      with oqGetTFTHours do
      begin
        // First check for TFT-minutes.
        ClearVariables;
        SetVariable('EMPNO',    AEmployeeNumber);
        SetVariable('DATEFROM', DateFrom);
        SetVariable('DATETO',   DateTo);
        // RV045.1. Added extra param for query.
        //          If ContractGroupTFT is true,
        //          then use contractgroup-TFT-settings,
        //          if false, then use hourtype-TFT-settings.
        if ContractGroupTFT then
          SetVariable('CONTRACTGROUPTFT', CHECKEDVALUE)
        else
          SetVariable('CONTRACTGROUPTFT', UNCHECKEDVALUE);
//        SetVariable('TFT_YN',   CHECKEDVALUE);
        Execute;
        TotEarnedTime := 0; // RV045.1. When 0, this must also be stored!
        if not Eof then
        begin
          // RV045.1. Addition of calculation for Bonus_percentage.
          while not Eof do
          begin
            // CGTFT_YN, H.HOURTYPE_NUMBER, H.BONUS_IN_MONEY_YN AS HT_BONUS_IN_MONEY,
            // C.BONUS_IN_MONEY_YN,
            // H.BONUS_PERCENTAGE
            CGTFT := (FieldAsString('CGTFT_YN') = 'Y');
            if CGTFT then
              BonusInMoney := (FieldAsString('BONUS_IN_MONEY_YN') = 'Y')
            else
              BonusInMoney := (FieldAsString('HT_BONUS_IN_MONEY_YN') = 'Y');
            BonusPercentage := FieldAsFloat('BONUS_PERCENTAGE');
            EarnedTime := FieldAsFloat('SUMMINUTE');
            if not BonusInMoney then
              EarnedTime := Round(EarnedTime + EarnedTime *
                BonusPercentage / 100);
            TotEarnedTime := TotEarnedTime + EarnedTime;
            Next;
          end; // while not qryGetTFTHours.Eof do
        end; // if not Eof then
        // Now update ABSENCETOTAL, field EARNED_TFT_MINUTE
        with oqUATSelect do
        begin
          ClearVariables;
          SetVariable('EMPLOYEE_NUMBER', AEmployeeNumber);
          SetVariable('ABSENCE_YEAR',    Year);
          Execute;
        end;
        if oqUATSelect.Eof then
        begin
          // Insert new record
          if TotEarnedTime <> 0 then
          begin
            with oqUATInsert do
            begin
              ClearVariables;
              SetVariable('EMPLOYEE_NUMBER',   AEmployeeNumber);
              SetVariable('ABSENCE_YEAR',      Year);
              SetVariable('EARNED_TFT_MINUTE', TotEarnedTime);
              SetVariable('MUTATOR',           ORASystemDM.CurrentProgramUser);
              Execute;
           end;
          end;
        end
        else
        begin
          // Update existing record, but only if it is different.
          if (oqUATSelect.FieldAsFloat('EARNED_TFT_MINUTE')
            <> TotEarnedTime) then
          begin
            with oqUATUpdate do
            begin
              ClearVariables;
              SetVariable('EMPLOYEE_NUMBER',   AEmployeeNumber);
              SetVariable('ABSENCE_YEAR',      Year);
              SetVariable('EARNED_TFT_MINUTE', TotEarnedTime);
              SetVariable('MUTATOR',           ORASystemDM.CurrentProgramUser);
              Execute;
            end;
          end;
        end;
      end;
    end;
  end;
end;

// RV045.1.
// Is there a contract group with settings for
// TimeForTime = Y ?
function TGlobalDM.ContractGroupTFTExists: Boolean;
begin
  Result := False;
  with oqContractGroupTFT do
  begin
    Execute;
    if not Eof then
      Result := True;
  end;
end;

procedure TGlobalDM.DataModuleCreate(Sender: TObject);
begin
  Debug := False; // PIM-87
end;

// RV077.1.
// RV071.6
// Procedure that can recalculate shorter-working-week hours made during
// overtime that is stored in ABSENCETOTAL table.
// This must be done when salary-hours are recalculated and must be
// done for the employee and the complete year.
// RV045.1. Also: Calculation BONUS_PERCENTAGE added, this was missing!
// RV045.1. When 0 earned minutes are found, then this must also be updated!
procedure TGlobalDM.RecalcEarnedSWWHours(AEmployeeNumber: Integer;
  ADateFrom, ADateTo: TDateTime);
var
  DateFrom, DateTo: TDateTime;
  YearFrom, YearTo, Year, Month, Day: Word;
  EarnedTime, TotEarnedTime: Double;
  BonusInMoney: Boolean;
  BonusPercentage: Double;
  BookingDay: TDateTime;
begin
  if (ADateFrom <> NullDate) and (ADateTo <> NullDate) then
  begin
    DecodeDate(ADateFrom, YearFrom, Month, Day);
    DecodeDate(ADateTo, YearTo, Month, Day);
    for Year := YearFrom to YearTo do
    begin
      DateFrom := EncodeDate(Year, 1, 1);
      DateTo := EncodeDate(Year, 12, 31);
      with oqGetSWWHours do
      begin
        // First check for SWW-minutes.
        ClearVariables;
        SetVariable('EMPNO', AEmployeeNumber);
        SetVariable('DATEFROM', DateFrom);
        SetVariable('DATETO', DateTo);
        Execute;
      end;
      TotEarnedTime := 0;
      if not oqGetSWWHours.Eof then
      begin
        while not oqGetSWWHours.Eof do
        begin
          // Bonus in Money checkbox can be set at Contract group-level
          // and at Hourtype-level, check both here.
          BonusInMoney :=
            (oqGetSWWHours.FieldAsString('CG_BONUS_IN_MONEY_YN') = 'Y');
          if not BonusInMoney then
            BonusInMoney :=
              (oqGetSWWHours.FieldAsString('HT_BONUS_IN_MONEY_YN') = 'Y');
          BonusPercentage := 0;
          if (oqGetSWWHours.FieldAsString('BONUS_PERCENTAGE') <> '') then
            BonusPercentage :=
              oqGetSWWHours.FieldAsFloat('BONUS_PERCENTAGE');
          EarnedTime := oqGetSWWHours.FieldAsFloat('SUMMINUTE');
          if not BonusInMoney then
          begin
            EarnedTime := Round(EarnedTime + EarnedTime *
              BonusPercentage / 100);
          end;
          TotEarnedTime := TotEarnedTime + EarnedTime;
          oqGetSWWHours.Next;
        end;
      end; // if not oqGetSWWHours.Eof then

      // Now update ABSENCETOTAL, field EARNED_SHORTWWEEK_MINUTE
      // RV071.6.
      BookingDay := EncodeDate(Year, 1, 1);
      UpdateAbsenceTotal('EARNED_SHORTWWEEK_MINUTE', BookingDay,
        AEmployeeNumber, TotEarnedTime, True);
    end; // for Year := YearFrom to YearTo do
  end; // if (ADateFrom <> NullDate)
end;

// RV077.1.
procedure TGlobalDM.RecalculateBalances(AEmployeeNumber: Integer;
  ADateFrom, ADateTo: TDateTime);
begin
  try
    RecalcEarnedTFTHours(AEmployeeNumber, ADateFrom, ADateTo);
    RecalcEarnedSWWHours(AEmployeeNumber, ADateFrom, ADateTo);
  except
    // Ignore error
  end;
end;

// RV082.1.
function TGlobalDM.FindWorkspotOverruledContractGroup(APlantCode,
  AWorkspotCode, ADefaultContractGroupCode: String): String;
begin
  Result := ADefaultContractGroupCode;
  with oqWorkspotContractGroup do
  begin
    ClearVariables;
    SetVariable('PLANT_CODE', APlantCode);
    SetVariable('WORKSPOT_CODE', AWorkspotCode);
    Execute;
    if not Eof then
      Result := FieldAsString('CONTRACTGROUP_CODE');
  end;
end;

// RV082.1. Find contract group by employee, when empty in IDCard.
procedure TGlobalDM.FindContractGroup(var AIDCard: TScannedIDCard);
begin
  if AIDCard.ContractgroupCode = '' then
  begin
    with oqPopIDCard do
    begin
      ClearVariables;
      SetVariable('EMPLOYEE_NUMBER', AIDCard.EmployeeCode);
      Execute;
      if not Eof then
        AIDCard.ContractgroupCode :=
          FieldAsString('CONTRACTGROUP_CODE');
    end;
  end;
end;

// RV084.1.
procedure TGlobalDM.UpdateEMA(AAbsenceReason: String;
  AEmployeeIDCard: TScannedIDCard; AStartDate: TDateTime; AMore: Integer);
begin
  // AMore = 1: Multiple records are changed!
  // AMore = 0: Only 1 record is changed.
  with oqEmpAvUpd1 do
  begin
    ClearVariables;
    SetVariable('NEWAVCODE', '*');
    SetVariable('STDATE', AStartDate);
    SetVariable('MUTATOR', ORASystemDM.CurrentProgramUser);
    SetVariable('EMPNO', AEmployeeIDCard.EmployeeCode);
    SetVariable('AVCODE', AAbsenceReason);
    SetVariable('MORE', AMore);
    Execute;
  end;
  with oqEmpAvUpd2 do
  begin
    ClearVariables;
    SetVariable('NEWAVCODE', '*');
    SetVariable('STDATE', AStartDate);
    SetVariable('MUTATOR', ORASystemDM.CurrentProgramUser);
    SetVariable('EMPNO', AEmployeeIDCard.EmployeeCode);
    SetVariable('AVCODE', AAbsenceReason);
    SetVariable('MORE', AMore);
    Execute;
  end;
  with oqEmpAvUpd3 do
  begin
    ClearVariables;
    SetVariable('NEWAVCODE', '*');
    SetVariable('STDATE', AStartDate);
    SetVariable('MUTATOR', ORASystemDM.CurrentProgramUser);
    SetVariable('EMPNO', AEmployeeIDCard.EmployeeCode);
    SetVariable('AVCODE', AAbsenceReason);
    SetVariable('MORE', AMore);
    Execute;
  end;
  with oqEmpAvUpd4 do
  begin
    ClearVariables;
    SetVariable('NEWAVCODE', '*');
    SetVariable('STDATE', AStartDate);
    SetVariable('MUTATOR', ORASystemDM.CurrentProgramUser);
    SetVariable('EMPNO', AEmployeeIDCard.EmployeeCode);
    SetVariable('AVCODE', AAbsenceReason);
    SetVariable('MORE', AMore);
    Execute;
  end;
  if ORASystemDM.MaxTimeblocks > MAX_TBS then
  begin
    with oqEmpAvUpd5 do
    begin
      ClearVariables;
      SetVariable('NEWAVCODE', '*');
      SetVariable('STDATE', AStartDate);
      SetVariable('MUTATOR', ORASystemDM.CurrentProgramUser);
      SetVariable('EMPNO', AEmployeeIDCard.EmployeeCode);
      SetVariable('AVCODE', AAbsenceReason);
      SetVariable('MORE', AMore);
      Execute;
    end;
    with oqEmpAvUpd6 do
    begin
      ClearVariables;
      SetVariable('NEWAVCODE', '*');
      SetVariable('STDATE', AStartDate);
      SetVariable('MUTATOR', ORASystemDM.CurrentProgramUser);
      SetVariable('EMPNO', AEmployeeIDCard.EmployeeCode);
      SetVariable('AVCODE', AAbsenceReason);
      SetVariable('MORE', AMore);
      Execute;
    end;
    with oqEmpAvUpd7 do
    begin
      ClearVariables;
      SetVariable('NEWAVCODE', '*');
      SetVariable('STDATE', AStartDate);
      SetVariable('MUTATOR', ORASystemDM.CurrentProgramUser);
      SetVariable('EMPNO', AEmployeeIDCard.EmployeeCode);
      SetVariable('AVCODE', AAbsenceReason);
      SetVariable('MORE', AMore);
      Execute;
    end;
    with oqEmpAvUpd8 do
    begin
      ClearVariables;
      SetVariable('NEWAVCODE', '*');
      SetVariable('STDATE', AStartDate);
      SetVariable('MUTATOR', ORASystemDM.CurrentProgramUser);
      SetVariable('EMPNO', AEmployeeIDCard.EmployeeCode);
      SetVariable('AVCODE', AAbsenceReason);
      SetVariable('MORE', AMore);
      Execute;
    end;
    with oqEmpAvUpd9 do
    begin
      ClearVariables;
      SetVariable('NEWAVCODE', '*');
      SetVariable('STDATE', AStartDate);
      SetVariable('MUTATOR', ORASystemDM.CurrentProgramUser);
      SetVariable('EMPNO', AEmployeeIDCard.EmployeeCode);
      SetVariable('AVCODE', AAbsenceReason);
      SetVariable('MORE', AMore);
      Execute;
    end;
    with oqEmpAvUpd10 do
    begin
      ClearVariables;
      SetVariable('NEWAVCODE', '*');
      SetVariable('STDATE', AStartDate);
      SetVariable('MUTATOR', ORASystemDM.CurrentProgramUser);
      SetVariable('EMPNO', AEmployeeIDCard.EmployeeCode);
      SetVariable('AVCODE', AAbsenceReason);
      SetVariable('MORE', AMore);
      Execute;
    end;
  end; // if
end; // UpdateEMA

// RV084.1.
procedure TGlobalDM.DetermineTimeForTimeSettings(ADataSet: TOracleQuery;
  var ATimeForTimeYN, ABonusInMoneyYN, AShorterWorkingWeekYN: String);
begin
  with ADataSet do
  begin
    if ContractGroupTFTExists then
    begin
      ATimeForTimeYN := FieldAsString('TIME_FOR_TIME_YN');
      ABonusInMoneyYN := FieldAsString('BONUS_IN_MONEY_YN');
      AShorterWorkingWeekYN := FieldAsString('HT_ADV_YN');
    end
    else
    begin
      if FieldAsString('HT_OVERTIME_YN') = CHECKEDVALUE then
      begin
        ATimeForTimeYN := FieldAsString('HT_TIME_FOR_TIME_YN');
        ABonusInMoneyYN := FieldAsString('HT_BONUS_IN_MONEY_YN');
        AShorterWorkingWeekYN := FieldAsString('HT_ADV_YN');
      end
      else
      begin
        ATimeForTimeYN := UNCHECKEDVALUE;
        ABonusInMoneyYN := UNCHECKEDVALUE;
        AShorterWorkingWeekYN := UNCHECKEDVALUE;
      end;
    end;
  end;
end; // DetermineTimeForTimeSettings

// RV084.1.
function TGlobalDM.DetermineTimeForTime(ADataSet: TOracleQuery;
  AEarnedTimeTFT: Double; AEarnedMin: Integer): Double;
var
  Percentage: Double;
  TimeForTimeYN, BonusInMoneyYN, ShorterWorkingWeekYN: String;
begin
  DetermineTimeForTimeSettings(ADataSet, TimeForTimeYN, BonusInMoneyYN,
    ShorterWorkingWeekYN);
  if TimeForTimeYN = CHECKEDVALUE then
  begin
    Percentage := ADataSet.FieldAsFloat('BONUS_PERCENTAGE');
    if BonusInMoneyYN = UNCHECKEDVALUE then
      AEarnedTimeTFT := AEarnedTimeTFT +
        Round(AEarnedMin + AEarnedMin * Percentage / 100)
    else
      AEarnedTimeTFT := AEarnedTimeTFT + AEarnedMin;
  end;
  Result := AEarnedTimeTFT;
end; // DetermineTimeForTime

// RV084.1.
function TGlobalDM.DetermineShorterWorkingWeek(ADataSet: TOracleQuery;
  AEarnedTimeSWW: Double; AEarnedMin: Integer): Double;
var
  Percentage: Double;
  TimeForTimeYN, BonusInMoneyYN, ShorterWorkingWeekYN: String;
begin
  DetermineTimeForTimeSettings(ADataSet, TimeForTimeYN, BonusInMoneyYN,
    ShorterWorkingWeekYN);
  if ShorterWorkingWeekYN = CHECKEDVALUE then
  begin
    Percentage := ADataSet.FieldAsFloat('BONUS_PERCENTAGE');
    if BonusInMoneyYN = UNCHECKEDVALUE then
      AEarnedTimeSWW := AEarnedTimeSWW +
        Round(AEarnedMin + AEarnedMin * Percentage / 100)
    else
      AEarnedTimeSWW := AEarnedTimeSWW + AEarnedMin;
  end;
  Result := AEarnedTimeSWW;
end; // DetermineShorterWorkingWeek

// RV084.1.
procedure TGlobalDM.BooksTFT(ABookingDay: TDateTime;
  AEmployeeData: TScannedIDCard; AEarnedTimeTFT: Double);
begin
  // Booking Time for Time
  if AEarnedTimeTFT > 0 then
    UpdateAbsenceTotal('EARNED_TFT_MINUTE', ABookingDay,
      AEmployeeData.EmployeeCode, AEarnedTimeTFT, False);
end; // BooksTFT

// RV084.1.
procedure TGlobalDM.BooksSWW(ABookingDay: TDateTime;
  AEmployeeData: TScannedIDCard; AEarnedTimeSWW: Double);
begin
  // Booking Shorter Working Week (SWW)
  if AEarnedTimeSWW > 0 then
    UpdateAbsenceTotal('EARNED_SHORTWWEEK_MINUTE', ABookingDay,
      AEmployeeData.EmployeeCode, AEarnedTimeSWW, False);
end; // BooksSWW

// RV084.1.
function TGlobalDM.AbsenceTotalField(const AAbsenceTypeCode: Char): String;
begin
  case AAbsenceTypeCode of
    'H':  Result := 'USED_HOLIDAY_MINUTE';
    'I':  Result := 'ILLNESS_MINUTE';
    'T':  Result := 'USED_TFT_MINUTE';
    'W':  Result := 'USED_WTR_MINUTE';
    //RV067.4.
    'R':  Result := 'USED_HLD_PREV_YEAR_MINUTE';
    'E':  Result := 'USED_SENIORITY_HOLIDAY_MINUTE';
    'F':  Result := 'USED_ADD_BANK_HLD_MINUTE';
    'S':  Result := 'USED_SAT_CREDIT_MINUTE';
    'C':  Result := 'USED_BANK_HLD_RESERVE_MINUTE';
    'D':  Result := 'USED_SHORTWWEEK_MINUTE';
    'X':  Result := 'NORM_BANK_HLD_RESERVE_MINUTE';
    // RV071.10.
    'Y':  Result := 'EARNED_SAT_CREDIT_MINUTE';
    else
      Result := '';
  end;
end; // AbsenceTotalField

// RV084.1.
procedure TGlobalDM.UpdateAbsenceTotalMode(AEmployeeNumber,
  AAbsenceYear: Integer; AMinutes: Double; AAbsenceTypeCode: Char;
  AAddToOldMinutes: Boolean);
var
  UpdateField: String;
  OldMinutes: Double;
  SelectFlag1, FlagFieldName1: String;
  function TestFlag1: Boolean;
  begin
    Result := False;
    if UpdateField = 'NORM_BANK_HLD_RESERVE_MINUTE' then
      if (oqUATM.FieldAsString(FlagFieldName1) = CHECKEDVALUE) then
        Result := True;
  end;
begin
  // RV067.MRA.31
  UpdateField := AbsenceTotalField(AAbsenceTypeCode);
  if UpdateField = '' then
    Exit;

  SelectFlag1 := '';
  FlagFieldName1 := '';
  if UpdateField = 'NORM_BANK_HLD_RESERVE_MINUTE' then
  begin
    FlagFieldName1 := 'NORM_BANK_HLD_RES_MIN_MAN_YN';
    SelectFlag1 := ', ' + FlagFieldName1 + ' ';
  end;

  // RV071.3. Check on field NORM_BANK_HLD_RES_MIN_MAN_YN.
  oqUATM.Close;
  oqUATM.SQL.Clear;
  oqUATM.SQL.Add(
    Format('SELECT %s ', [UpdateField]) +
    SelectFlag1 +
    'FROM ' +
    '  ABSENCETOTAL ' +
    'WHERE ' +
    '  EMPLOYEE_NUMBER = :EMPNO AND ' +
    '  ABSENCE_YEAR = :AYEAR');
  oqUATM.DeleteVariables;
  oqUATM.DeclareAndSet('EMPNO', otInteger, AEmployeeNumber);
  oqUATM.DeclareAndSet('AYEAR', otInteger, AAbsenceYear);
  oqUATM.Execute;
  if not oqUATM.Eof then
  begin
    // RV071.3. Only update under certain condition.
    if not TestFlag1 then
    begin
      //RV067.4.
      if AAddToOldMinutes then
       OldMinutes := oqUATM.FieldAsFloat(UpdateField)
      else
        OldMinutes := 0;
      oqUATM.Close;
      oqUATM.SQL.Clear;
      oqUATM.SQL.Add(
        'UPDATE ABSENCETOTAL ' +
        Format('SET %s = :NEWMIN ', [UpdateField]) +
        ', MUTATOR = :MUTATOR, MUTATIONDATE = :MUTATIONDATE ' +
        'WHERE EMPLOYEE_NUMBER = :EMPNO AND ' +
        'ABSENCE_YEAR = :AYEAR');
      oqUATM.DeleteVariables;
      oqUATM.DeclareAndSet('EMPNO',        otInteger,  AEmployeeNumber);
      oqUATM.DeclareAndSet('AYEAR',        otInteger,  AAbsenceYear);
      oqUATM.DeclareAndSet('NEWMIN',       otFloat,    OldMinutes + AMinutes);
      oqUATM.DeclareAndSet('MUTATOR',      otString,   ORASystemDM.CurrentProgramUser);
      oqUATM.DeclareAndSet('MUTATIONDATE', otDate,     Now);
      oqUATM.Execute;
    end;
  end
  else
  begin
    oqUATM.Close;
    oqUATM.SQL.Clear;
    oqUATM.SQL.Add(
      'INSERT INTO ABSENCETOTAL( ' +
      'EMPLOYEE_NUMBER,ABSENCE_YEAR,CREATIONDATE,MUTATIONDATE, ' +
      Format('MUTATOR,%s) ',[UpdateField]) +
      'VALUES(:EMPNO,:AYEAR,:ADATE,:ADATE,:MUT,:NEWMIN)');
    oqUATM.DeleteVariables;
    oqUATM.DeclareAndSet('EMPNO',  otInteger, AEmployeeNumber);
    oqUATM.DeclareAndSet('AYEAR',  otInteger, AAbsenceYear);
    oqUATM.DeclareAndSet('ADATE',  otDate,    Now);
    oqUATM.DeclareAndSet('MUT',    otString,  ORASystemDM.CurrentProgramUser);
    oqUATM.DeclareAndSet('NEWMIN', otFloat,   AMinutes);
    oqUATM.Execute;
  end;
end; // UpdateAbsenceTotalMode

// RV084.1.
function TGlobalDM.BooksWorkedHoursOnBankHoliday(ABookingDay: TDateTime;
  AWorkedMin: Integer; AEmployeeData: TScannedIDcard;
  AManual: String): Integer;
var
  HourtypeWorkedOnBankHoliday, AbsenceReasonID, AbsenceMinutes: Integer;
  AbsenceReasonCode, AbsenceTypeCode: String;
  AbsenceYear, Month, Day: Word;
  EarnedMin: Integer;
  EarnedTimeTFT, EarnedTimeSWW: Double;
  function DetermineHourtypeWorkedOnBankHoliday: Integer;
  begin
    Result := -1;
    with oqHTONBK do
    begin
      ClearVariables;
      SetVariable('EMPLOYEE_NUMBER', AEmployeeData.EmployeeCode);
      SetVariable('BANKHOL_DATE', ABookingDay);
      Execute;
      if not Eof then
      begin
        AbsenceReasonCode := FieldAsString('ABSENCEREASON_CODE');
        AbsenceTypeCode := FieldAsString('ABSENCETYPE_CODE');
        AbsenceReasonID := FieldAsInteger('ABSENCEREASON_ID');
        if FieldAsString('CG_HOURTYPE_WRK_ON_BANKHOL') <> '' then
          Result := FieldAsInteger('CG_HOURTYPE_WRK_ON_BANKHOL')
        else
        begin
          if FieldAsString('HOURTYPE_WRK_ON_BANKHOL') <> '' then
            Result := FieldAsInteger('HOURTYPE_WRK_ON_BANKHOL');
        end;
      end;
    end;
  end; // DetermineHourtypeWorkedOnBankHoliday
begin
  Result := AWorkedMin;
  if Result = 0 then
    Exit;
  AbsenceReasonCode := '';
  AbsenceTypeCode := '';
  AbsenceReasonID := -1;
  EarnedTimeTFT := 0;
  EarnedTimeSWW := 0;
  FindContractGroup(AEmployeeData);
  HourtypeWorkedOnBankHoliday := DetermineHourtypeWorkedOnBankHoliday;
  if HourtypeWorkedOnBankHoliday <> -1 then
  begin
    oqOvertimeParams.ClearVariables;
    oqOvertimeParams.SetVariable('CONTRACTGROUP_CODE',
      AEmployeeData.ContractgroupCode);
    oqOvertimeParams.SetVariable('HOURTYPE_NUMBER',
      HourtypeWorkedOnBankHoliday);
    oqOvertimeParams.Execute;
    if not oqOvertimeParams.Eof then
    begin
      // Change Employee Availability from <AbsenceReasonCode> to '*'
      UpdateEMA(AbsenceReasonCode, AEmployeeData, ABookingDay, 0);
      // Absence hours must be reprocessed!
      with oqAHE do
      begin
        ClearVariables;
        SetVariable('EMPLOYEE_NUMBER', AEmployeeData.EmployeeCode);
        SetVariable('ABSENCEHOUR_DATE', ABookingDay);
        SetVariable('ABSENCEREASON_ID', AbsenceReasonID);
        SetVariable('MANUAL_YN', UNCHECKEDVALUE);
        Execute;
        if not Eof then
        begin
          AbsenceMinutes := FieldAsInteger('ABSENCE_MINUTE');
          // Subtract minutes from balance (when needed, based on absencetype)
          if AbsenceTypeCode <> '' then
          begin
            DecodeDate(ABookingDay, AbsenceYear, Month, Day);
            UpdateAbsenceTotalMode(AEmployeeData.EmployeeCode,
              AbsenceYear, -1 * AbsenceMinutes, AbsenceTypeCode[1]);
          end;
        end;
      end;
      // Now delete the absence hours.
      with oqAHEDelete do
      begin
        ClearVariables;
        SetVariable('EMPLOYEE_NUMBER', AEmployeeData.EmployeeCode);
        SetVariable('ABSENCEHOUR_DATE', ABookingDay);
        SetVariable('ABSENCEREASON_ID', AbsenceReasonID);
        SetVariable('MANUAL_YN', UNCHECKEDVALUE);
        Execute;
      end;
      // Book hours made on bank holiday
      EarnedMin := UpdateSalaryHour(ABookingDay,
        AEmployeeData, HourtypeWorkedOnBankHoliday, AWorkedMin, AManual, False,
        True
        );
      // Also book TFT/SWW if needed.
      EarnedTimeTFT := DetermineTimeForTime(oqOvertimeParams,
        EarnedTimeTFT, EarnedMin);
      EarnedTimeSWW := DetermineShorterWorkingWeek(oqOvertimeParams,
        EarnedTimeSWW, EarnedMin);
      BooksTFT(ABookingDay, AEmployeeData, EarnedTimeTFT);
      BooksSWW(ABookingDay, AEmployeeData, EarnedTimeSWW);
      Result := 0;
    end;
  end;
end; // BooksWorkedHoursOnBankHoliday

// 20013489 Overnight-shift-system
// Use ShiftDate (based on shift) to store the production record.
// When this is about an overnight-shift it can mean it is stored
// on a different day (can be the day before or the current day).
// The ShiftDate indicates the start-of-the-shift.
function TGlobalDM.UpdateProductionHour(AProductionDate: TDateTime;
  AIDCard: TScannedIDCard; AMinutes, APayedBreaks: Integer;
  AManual: String): Integer;
var
  MyOldProdMin, MyOldPayedBreaks: Integer;
  StartDate, EndDate: TDateTime;
begin
  if ORASystemDM.UseShiftDateSystem then
  begin
    // 20013489. Determine production-date based on ShiftDate.
    // Only do this for non-manual-records!
    if AManual = UNCHECKEDVALUE then
    begin
      // 20013489 Assign ShiftDate to ProductionDate.
      if AIDCard.ShiftDate = NullDate then
      begin
        // Find the shiftdate and assign to productiondate
        AProdMinClass.GetShiftDay(AIDCard, StartDate, EndDate);
        AProductionDate := Trunc(StartDate);
      end
      else
        AProductionDate := AIDCard.ShiftDate;
    end; // if
  end;
{$IFDEF DEBUG3}
  WDebugLog('UpdateProductionHour. Date=' + DateToStr(AProductionDate) +
    ' Min=' + IntMin2StringTime(AMinutes, False)
    );
{$ENDIF}
  with oqUPHSelect do
  begin
    ClearVariables;
    SetVariable('PRODHOUREMPLOYEE_DATE', AProductionDate);
    SetVariable('PLANT_CODE',            AIDCard.PlantCode);
    SetVariable('SHIFT_NUMBER',          AIDCard.ShiftNumber);
    SetVariable('EMPLOYEE_NUMBER',       AIDCard.EmployeeCode);
    SetVariable('WORKSPOT_CODE',         AIDCard.WorkSpotCode);
    SetVariable('JOB_CODE',              AIDCard.JobCode);
    SetVariable('MANUAL_YN',             AManual);
    Execute;
  end; {with oqUPHSelect}
  if not oqUPHSelect.Eof then
  begin
    MyOldProdMin     := oqUPHSelect.FieldAsInteger('PRODUCTION_MINUTE');
    MyOldPayedBreaks := oqUPHSelect.FieldAsInteger('PAYED_BREAK_MINUTE');
    if (MyOldProdMin + AMinutes) <= 0 then
    begin
      // delete productionhours
      with oqUPHDelete do
      begin
        ClearVariables;
        SetVariable('PRODHOUREMPLOYEE_DATE', AProductionDate);
        SetVariable('PLANT_CODE',            AIDCard.PlantCode);
        SetVariable('SHIFT_NUMBER',          AIDCard.ShiftNumber);
        SetVariable('EMPLOYEE_NUMBER',       AIDCard.EmployeeCode);
        SetVariable('WORKSPOT_CODE',         AIDCard.WorkSpotCode);
        SetVariable('JOB_CODE',              AIDCard.JobCode);
        SetVariable('MANUAL_YN',             AManual);
        Execute;
      end;
      // delete all prodhourperemplperalltypes is were inserted before in extra table
      DeleteProdHourForAllTypes(AIDCard, AProductionDate,
        AProductionDate,  AManual);
    end   {if (MyOldProdMin + AMinutes) <= 0}
    else
    begin {else (MyOldProdMin + AMinutes) <= 0}
      // update productionhours
      with oqUPHUpdate do
      begin
        ClearVariables;
        SetVariable('PRODHOUREMPLOYEE_DATE', AProductionDate);
        SetVariable('PLANT_CODE',            AIDCard.PlantCode);
        SetVariable('SHIFT_NUMBER',          AIDCard.ShiftNumber);
        SetVariable('EMPLOYEE_NUMBER',       AIDCard.EmployeeCode);
        SetVariable('WORKSPOT_CODE',         AIDCard.WorkSpotCode);
        SetVariable('JOB_CODE',              AIDCard.JobCode);
        SetVariable('MANUAL_YN',             AManual);
        SetVariable('PRODUCTION_MINUTE',     AMinutes + MyOldProdMin);
        SetVariable('PAYED_BREAK_MINUTE',    APayedBreaks + MyOldPayedBreaks);
        SetVariable('MUTATOR',               ORASystemDM.CurrentProgramUser);
        Execute;
      end; {with oqUPHUpdate}
    end;  {else (MyOldProdMin + AMinutes) <= 0}
    Result := 0;
  end    {if not oqUPHSelect.Eof}
  else
  begin  {else not oqUPHSelect.Eof}
    // insert
    with oqUPHInsert do
    begin
      ClearVariables;
      SetVariable('PRODHOUREMPLOYEE_DATE', AProductionDate);
      SetVariable('PLANT_CODE',            AIDCard.PlantCode);
      SetVariable('SHIFT_NUMBER',          AIDCard.ShiftNumber);
      SetVariable('EMPLOYEE_NUMBER',       AIDCard.EmployeeCode);
      SetVariable('WORKSPOT_CODE',         AIDCard.WorkSpotCode);
      SetVariable('JOB_CODE',              AIDCard.JobCode);
      SetVariable('MANUAL_YN',             AManual);
      SetVariable('PRODUCTION_MINUTE',     AMinutes);
      SetVariable('PAYED_BREAK_MINUTE',    APayedBreaks);
      SetVariable('MUTATOR',               ORASystemDM.CurrentProgramUser);
      Execute;
    end; {with oqUPHInert}
    Result := 1;
  end;   {else not oqUPHSelect.Eof}
  if AMinutes >= 60 then
    UpdateEmployeeLevel(AIDCard);
end; // UpdateProductionHour

procedure TGlobalDM.UpdateTimeRegScanning(AIDCard: TScannedIDCard;
  AShiftDate: TDateTime);
begin
  try
    with oqTRSUpdate do
    begin
      ClearVariables;
      // Search arguments
      SetVariable('DATETIME_IN',     AIDCard.DateInOriginal); // 20015346
      SetVariable('EMPLOYEE_NUMBER', AIDCard.EmployeeCode);
      // Fields to change
      SetVariable('SHIFT_DATE',      AShiftDate);
      SetVariable('MUTATOR',         ORASystemDM.CurrentProgramUser);
      Execute;
    end;
  except
    on E: EOracleError do
    begin
      WErrorLog('UpdateTimeRegScanning:' + E.Message);
    end;
    on E: Exception do
    begin
      WErrorLog('UpdateTimeRegScanning:' + E.Message);
    end;
  end;
end;

end.
