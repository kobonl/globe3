(*
  MRA:10-NOV-2014 20014826
  - Log error messages optionally to database.
  MRA:1-JUN-2015 PIM-42
  - Trap errors when writing to a file.
  MRA:17-SEP-2015 PIM-87
  - Some extra functions added because of this order.
  MRA:4-MAR-2016 PIM-125
  - When no one is scanned in anymore, do not log errors.
  MRA:22-JUL-2019 GLOB3-284
  - Week start at Wednesday
*)
unit UGlobalFunctions;

interface

uses
  SysUtils, Dialogs, Classes, UPimsConst,
  Controls, FileCtrl, DB;

// moved from ListProcs
const
  vbUseSystem = 0;
  vbSunday    = 1;
  vbMonday    = 2;
  vbTuesday   = 3;
  vbWednesday = 4;
  vbThursday  = 5;
  vbFriday    = 6;
  vbSaturday  = 7;
  vbFirstJan1     = 1;
  vbFirstFourDays = 2;
  vbFirstFullWeek = 3;
  UGLOGFILENAME_DEFAULT ='UGLOG';
  UGERRORLOGFILENAME_DEFAULT = 'UGERRORLOG';

type
  PTBookmarkRecord = ^TBookmarkRecord;
  TBookmarkRecord = record
    Bookmark: TBookmark;
    AddToSalList, AddToProdListOut, AddToProdListIn: Boolean;
    BookingDay: TDateTime; // TD-23416
  end;
  
function DatePart(Interval: String; ADate: TDateTime; FirstDayOfWeek: Integer;
  FirstWeekOfYear: Integer): word;
function WeekOfYear(ADate: TDateTime): Word;
function WeeksInYear(year: Integer): Integer;
function DateFromWeek(Year, Week, Day: word): TDate;
function DateDifferenceDays(OlderDate, YoungerDate: TDateTime): Word;
function YMDJulian(Year, Month, Day: Integer):  Longint;
procedure WeekUitDat(Ldate: TDateTime; var Year, Week: Word);
function DayWStartOnFromDate(LDate: TDateTime): Integer;
function LastMonthDay(YearNr, MonthNr: Integer): Integer;
function LastDayOfWeek(ADate: TDateTime): TDateTime;
function StartOfYear(Year: Word): TDate;
function PimsDayOfWeek(ADate: TDateTime): Word;
// end moved from ListProcs

function Min(int1,int2: Variant): Variant;
function Max(int1,int2: Variant): Variant;

function Seconds2StringMinSec(Seconds: Integer): String;

function IntMin2TimeMin(Minutes: integer): TDateTime;
{ Convert Minute(int) into datetime }

function TimeMin2IntMin(ADate:TDateTime): Integer;
{ Convert The time from ADateTime in minutes (integer) }

function IntMin2StringTime(Minutes: Integer; ShowZero: Boolean): String;
{ Convert the Minute in string like hours:min
  If minute=0 and ShowZero=False then result='' else result='00:00' }

function IntMin2StrDigTime(Minutes,HrsDigits:Integer;ShowZero:Boolean): String;
{Convert the Minute in string like hours:min
  If minute=0 and ShowZero=False then result='' else result='00:00'
  The minutes will be displayed with HrsDigits}

function StrTime2IntMin(ATime: String): Integer;
{ Convert the ATime string hrs:min(06:12) to amount of minutes }

function RoundTime(const ADateTime: TDateTime; const ATarget: Integer): TDateTime;
{case target of
  0: round milisecond
	1: round second
  2: round minute	}
function RoundTimeConditional(const ADateTime: TDateTime; const ATarget: Integer): TDateTime; // 20015346
{ Conditional based on ORASystemDM-setting TimerecordingInSecs:
  false: Round time
  true: do not round time
{case target of
  0: round milisecond
	1: round second
  2: round minute	}
function BuildMinTime(AStrTime: String): Integer;
{ This function are useful for TdxTimeEdit component
  formation strings like: '__:_3' into '00:03' and after convert into minutes }

function DayInWeek (WeekStartOn: Integer; CurrentDate: TDateTime):Integer;
{	WeekStartOn any possibilities 1-7; Fixed week days: MO-TU-WE-T
  1 = Monday
  result = day from CurrenTDate in week}

function DateInInterval(ADate, TestDate: TDateTime;
				 	MinTo, MinAfter: Integer): TDateTime;
// function check if TestDate is in DateTime interval
// if ADate-MinTo <= TestDate <= ADate+MinAfter then result=ADate
// else result=TestDate

function ComputeMinutesOfIntersection(Start1, End1,
  Start2, End2: TDateTime): Integer;
// This function retrieve the intersection between datetime intervals
// start1 - end1 and start2 - end2 in minutes;

// MR:07-11-2003 Corrects dates if end-date is '00:00' (midnight)
function MidnightCorrection(STime, ETime: TDateTime): TDateTime;


procedure GetStartEndDay(var StartDateTime, EndDateTime: TDateTime);
// Considering the date from StartDateTime, procedure build start-end of day
// If StartDateTime='21/12/01' will result in
// StartDateTime='21/12/01 00:00:00:000'
// EndDateTime='21/12/01 23:59:59:999'

function CharFromStr(const AStr: String; const ACharIndex: Integer;
  const ABlankChar: Char): Char;
// this function check if str is not empty
// if empty return BlankChar
// else return the Char CharIndex from str
// charindex is starting from 1

function GetNoFromStr(MyStr: String): Integer;
// this function get the number part from a string
// exampl: GetNoFromStr('Edit23') = 23.

function DateTimeBefore(ADate: TDateTime; const ASeconds: Integer): TDateTime;
function DateTimeAfter(ADate: TDateTime; const ASeconds: Integer): TDateTime;
// Calculate next DateTime that comes after a given interval
// of x minutes.
// Example: ADate  := 21-5-2002 23:57:48
//          Interval = 5 minutes
//          Result := 22-5-2002 00:02:48
function DateTimeTruncToInterval(ADate: TDateTime;
  const ASeconds: Integer): TDateTime;
// Trunc Time to Interval
// Example:
//   Interval is 5 minutes
//   12:06 will be 12:05
//   12:34 will be 12:30
function IsRoundMidnight(const ADateTime: TDateTime;
  const AFPIMSTimeInterval: Integer): Boolean;
// Determine if given time is round midnight in reach of
// FPIMSTimeInterval * 2. For example in reach of 10 minutes,
// so from 00.00 to 00.10 hrs.

// MR:17-12-2003 Logging is a public procedure
procedure WLogMain(AMsg: String; APriority: Integer);
procedure WLog(AMsg: String);
procedure WErrorLog(AMsg: String);
procedure WDebugLog(AMsg: String);
procedure UGLogFilenameSet(AValue: String);
procedure UGErrorLogFilenameSet(AValue: String);

// PIM-87
function Bool2Str(AValue: Boolean): String;
function Bool2Int(AValue: Boolean): Integer;

var
  UGLogFilename: String;
  UGErrorLogFilename: String;
  UGlobalLogPath: String;
  UGlobalErrorLogPath: String;

implementation

uses
  ORASystemDMT;

procedure UGLogFilenameSet(AValue: String);
begin
  UGLogFilename := AValue;
end;

procedure UGErrorLogFilenameSet(AValue: String);
begin
  UGErrorLogFilename := AValue;
end;

// 'WLogMain' = WriteLogMain
// 20014826
procedure WLogMain(AMsg: String; APriority: Integer);
var
  TFile: TextFile;
  Exists: Boolean;
  UGLogPath: String;
  LogPath: String;
  Error1, Error2: Integer;

  function MyZeroFormat(Value: String; Len: Integer): String;
  begin
    while Length(Value) < Len do
      Value := '0' + Value;
    Result := Value;
  end;
  function PathCheck(Path: String): String;
  begin
    if Path <> '' then
      if Path[length(Path)] <> '\' then
        Path := Path + '\';
    Result := Path;
  end;
  function NewLogPath(ALogRootPath, ALogFileName: String): String;
  var
    Year, Month, Day: Word;
    DateString: String;
  begin
    try
      DecodeDate(Date, Year, Month, Day);
      DateString :=
        MyZeroFormat(IntToStr(Year), 4) +
        MyZeroFormat(IntToStr(Month), 2) +
        MyZeroFormat(IntToStr(Day), 2);
    except
      DateString := '20020601';
    end;
    Result := PathCheck(ALogRootPath) +
      ORASystemDM.CurrentComputerName + '_' +
        ALogFileName + '_' + DateString + '.TXT';
  end;
begin
  if ORASystemDM.DBLog(AMsg, APriority) then // 20014826
    Exit;
  // Switch off I/O error checking - PIM-42
  {$I-}
  Error1 := 0;
{$IFDEF DEBUG1}
  ; // do nothing
{$ELSE}
  AMsg := DateTimeToStr(Now) + ' - ' + AMsg;
{$ENDIF}
  UGLogPath := '..\Log';
  try
    if not DirectoryExists(UGLogPath) then
      ForceDirectories(UGLogPath);
  except
  end;

  if APriority = PRIORITY_ERROR then // 20014826
  begin
    LogPath := NewLogPath(UGLogPath, UGErrorLogFilename);
    UGlobalErrorLogPath := LogPath;
  end
  else
  begin
    LogPath := NewLogPath(UGLogPath, UGLogFilename);
    UGlobalLogPath := LogPath;
  end;
  try
    Exists := FileExists(LogPath);
    AssignFile(TFile, LogPath);
    Error1 := IOResult;
    if Error1 = 0 then
      try
        if not Exists then
          Rewrite(TFile)
        else
          Append(TFile);
        Error2 := IOResult;
        if Error2 = 0 then
          WriteLn(TFile, AMsg);
      except
        // We cannot log here because this is the write-log-routine - PIM-42
//        on E:EInOutError do
      end;
  finally
    if Error1 = 0 then
      CloseFile(TFile);
  end;
  // Switch IO checking back on - PIM-42
  {$I+}
end; // WLogMain

// 'WLog' = WriteLog
procedure WLog(AMsg: String);
begin
  WLogMain(AMsg, PRIORITY_MESSAGE); // 20014826
end;

// 'WErrorLog' = WriteErrorLog
procedure WErrorLog(AMsg: String);
begin
  if ORASystemDM.LogErrors then // PIM-125
    WLogMain(AMsg, PRIORITY_ERROR); // 20014826
end;

procedure WDebugLog(AMsg: String);
begin
  WLogMain(AMsg, PRIORITY_DEBUG); // 20014826
end;

// Depending on 'WeekStartsOn', set the day-numbers.
function PimsDayOfWeek(ADate: TDateTime): Word;
begin
  Result := DayOfWeek(ADate); // 1=sun 2=mon 3=tue 4=wed 5=thu 6=fri 7=sat

  // WeekStartsOn can be either 1, 2, 4 or 7. (1=sunday, 2=monday, 4=wednesday 7=saturday).
  case ORASystemDM.WeekStartsOn of
//  1: // sunday, no correction needed 1=sun 2=mon 3=tue... 7=sat
  2: // monday, correct to 1=mon 2=tue 3=wed 4=thu 5=fri 6=sat 7=sun
    case Result of
    1: Result := 7; // sun
    2: Result := 1; // mon
    3: Result := 2; // tue
    4: Result := 3; // wed
    5: Result := 4; // thu
    6: Result := 5; // fri
    7: Result := 6; // sat
    end;
  // GLOB3-284
  4: // wednesday, correct to 1=wed, 2=thu, 3=fri, 4=sat, 5=sun, 6=mo, 7=tue
    case Result of
    1: Result := 5; // sun
    2: Result := 6; // mon
    3: Result := 7; // tue
    4: Result := 1; // wed
    5: Result := 2; // thu
    6: Result := 3; // fri
    7: Result := 4; // sat
    end;
  7: // saturday, correct to 1=sat 2=sun 3=mon 4=tue 5=wed 6=thu 7=fri
    case Result of
    1: Result := 2; // sun
    2: Result := 3; // mon
    3: Result := 4; // tue
    4: Result := 5; // wed
    5: Result := 6; // thu
    6: Result := 7; // fri
    7: Result := 1; // sat
    end;
  end;
end;

// MR:03-12-2003
{ Get first monday of the year }
{ And correct it to first (other) day depending on 'WeekStartsOn' }
{ Also look at setting of FirstWeekOfYear to decide where how the }
{ first week is defined. }
function StartOfYear(Year: Word): TDate;
var
  FirstOfYear: TDateTime;
begin
  { 1 jan of year }
  FirstOfYear := EnCodeDate(Year, 1, 1);

  // First week is week that contains 1 january.
  case ORASystemDM.FirstWeekOfYear of
  1:
    case PimsDayOfWeek(FirstOfYear) of
    2: FirstOfYear := FirstOfYear - 1;
    3: FirstOfYear := FirstOfYear - 2;
    4: FirstOfYear := FirstOfYear - 3;
    5: FirstOfYear := FirstOfYear - 4;
    6: FirstOfYear := FirstOfYear - 5;
    7: FirstOfYear := FirstOfYear - 6;
    end;
  // First week is week that contains 4 january.
  4:
    case PimsDayOfWeek(FirstOfYear) of
    2: FirstOfYear := FirstOfYear - 1; // Tue -> Mon
    3: FirstOfYear := FirstOfYear - 2; // Wed -> Mon
    4: FirstOfYear := FirstOfYear - 3; // Thu -> Mon
    5: FirstOfYear := FirstOfYear + 3; // Fri -> Mon
    6: FirstOfYear := FirstOfYear + 2; // Sat -> Mon
    7: FirstOfYear := FirstOfYear + 1; // Sun -> Mon
    end;
  // First week is week that contains 7 january.
  7:
    case PimsDayOfWeek(FirstOfYear) of
    2: FirstOfYear := FirstOfYear + 6;
    3: FirstOfYear := FirstOfYear + 5;
    4: FirstOfYear := FirstOfYear + 4;
    5: FirstOfYear := FirstOfYear + 3;
    6: FirstOfYear := FirstOfYear + 2;
    7: FirstOfYear := FirstOfYear + 1;
    end;
  end;
  Result := FirstOfYear;
end;

function LastMonthDay(YearNr, MonthNr: Integer): Integer;
begin
  Result := 30;
  case MonthNr of
    1, 3, 5, 7, 8, 10, 12: Result := 31;
    2:
      if (YearNR mod 4) = 0 then
        Result := 29
      else
        Result := 28;
    4, 6, 9, 11:  Result := 30;
  end;
end;

// MR:03-12-2003
// Changes, so it looks at 'WeekstartsOn'-setting.
{ Get last day of the week (result depends on 'weekstartson') }
function LastDayOfWeek(ADate: TDateTime): TDateTime;
var
  Day: Integer;
  LastDay: Integer;
begin
  LastDay := 1;
  case ORASystemDM.WeekStartsOn of
  1: LastDay := 7; // sun
  2: LastDay := 1; // mon
  3: LastDay := 2; // tue
  4: LastDay := 3; // wed
  5: LastDay := 4; // thu
  6: LastDay := 5; // fri
  7: LastDay := 6; // sat
  end;
  Day := DayOfWeek(ADate);
  while (Day <> LastDay) do
  begin
    ADate := ADate + 1;
    Day := DayOfWeek(ADate);
  end;
  result := ADate;
end;

// MR:03-12-2003
// function changed after weekstartson
{ Get the date from the week, specifying which day from the week
Week starts with Mo end finish on SU}
function DateFromWeek(Year, Week, Day: Word): TDate;
var
  StartDate: TDate;
  NextDate: TDate;
  Date: TDate;
  HDay{, SaveDay}: Integer;
begin
  Day := Day mod 7;
//  SaveDay := Day;
  { Determine start of current year }
  StartDate := StartOfYear(Year);
  NextDate := StartOfYear(Year + 1);

  HDay := Day - PimsDayOfWeek(StartDate);
  if (HDay < 0) then
    HDay := HDay + 7;

  Date := StartDate + ((Week - 1) * 7) + HDay;

  if Date >= NextDate then
    Date := NextDate - 1;
//  if SaveDay = 1 then
//    Date := Date - 7;
  Result := Date;
end;

{ Get number of week of given year }
function WeeksInYear(year: Integer): Integer;
var
  ADate: TDateTime;
  Weeks: Integer;
begin
  ADate := EnCodeDate(year, 12, 31);
  Weeks := WeekOfYear(ADate);
  if Weeks = 1 then
  begin
    ADate := EnCodeDate(year, 12, 24);
    Weeks := WeekOfYear(ADate);
  end;
  result := Weeks;
end;

// MR:03-12-2003
{ Get weeknumber of given date, also get year }
function WeekOfYear(ADate: TDateTime): Word;
var
  {AYear, }Year, Month, Day: Word;
  StartDate: TDateTime;
  NextDate: TDateTime;
  LDate: TDateTime;
  DaysPast: TDateTime;
  CalcWeek: TDateTime;
  Week: Word;
begin
  DecodeDate(ADate, Year, Month, Day);
//  AYear := Year;
  StartDate := StartOfYear(Year);
  NextDate := StartOfYear(Year + 1);
  LDate := LastDayOfWeek(ADate);

  if (LDate >= StartDate) and
     (LDate <= NextDate) then
  begin
    DaysPast := LDate - StartDate;
    CalcWeek := DaysPast / 7;
    Week := Trunc(CalcWeek) + 1;
  end
  else
  begin
    if (LDate < StartDate) then
    begin
//      AYear := Year - 1;
      Week := WeeksInYear(Year - 1);
    end
    else
    begin
//      AYear := Year + 1;
      Week := 1;
    end;
  end;
  Result := Week;
end;

// Computes Julian day number for any Gregorian calendar date
// Returns :  Julian day number (astronomically for the day beginning at noon
// on the given day
function YMDJulian(Year, Month, Day: Integer):  Longint;
var
  Tmp1, Tmp2, Tmp3, Tmp4, Tmp5: Longint;
begin
  if Month > 2 then
  begin
    Tmp1 := Month -3;
    Tmp2 := Year;
  end
  else
  begin
    Tmp1 := Month + 9;
    Tmp2 := Year - 1;
  end;
  Tmp3 := Tmp2 Div 100;
  Tmp4 := Tmp2 Mod 100;
  Tmp5 := Day;
  Result := (146097 * Tmp3) Div 4 + (1461 * Tmp4) Div 4 +
    (153 * Tmp1 + 2) Div 5 + Tmp5 + 1721119;
end;

function DateDifferenceDays(OlderDate, YoungerDate: TDateTime): Word;
var
   OlderYear, OlderMonth, OlderDays,
   YoungerYear, YoungerMonth, YoungerDays: Word;
begin
   DecodeDate(OlderDate, OlderYear, OlderMonth, OlderDays);
   DecodeDate(YoungerDate, YoungerYear, YoungerMonth, YoungerDays);
   Result :=  YMDJulian(YoungerYear,YoungerMonth,YoungerDays) -
     YMDJulian(OlderYear, OlderMonth, OlderDays);
end;

// changed after weekstarton
procedure WeekUitDat(Ldate: TDateTime; var Year, Week: Word);
var
   StartLDate,        {-Date of first day first week of year}
   NextLDate,         {-Date of first day first week of year}
   CalcWeek,          {-Used to calculate week number}
   DaysPast: Real;    {-Days past since start of year}
   Month, Day: Word;
//   InitialDate, DateMin, DateMax: Real;
begin
  {-Determine current year}
//  InitialDate := LDate;
  DecodeDate(Ldate, Year, Month, Day);
  {-Check if year bigger than base year(e.g. 1930)}
  if (Year >= 1930) then
  begin
    {-Determine start of current year, first monday}
    StartLdate:= StartOfYear(Year);
    NextLdate:= StartOfYear(Year+1);
    Ldate:= LastDayOfWeek(Ldate);
    {-If requested date not before the start of the current year and
      the requested date not past the start of the next year then calculate
      week else the week is last week of previous year or week one of the next
      year}
    if (Ldate >= StartLdate) and
       (Ldate <= NextLdate) then
    begin
      {-Determine days past since begin of current year}
      DaysPast:= DateDifferenceDays(StartLdate, Ldate);
      {-Calculate number of weeks}
      CalcWeek:= DaysPast / 7;
      Week := Trunc(CalcWeek)+1;
    end
    else
    begin
      if (Ldate < StartLdate) then
      begin
        Year:= Year-1;
        Week:= WeeksInYear(Year);
      end
      else
      begin
        Year:= Year+1;
        Week:= 1;
      end; {IF (Ldate < StartLdate) THEN}
    end; {IF (Ldate >= StartLdate) AND...}
  end
  else
  begin
    Year:= 0;
    Week:= 0;
  end; {if (Year >= 1930) then}
end; {PROCEDURE WeekUitDat}

// not changed function after weekstartson because it is based on fixed data of
// daytable
function DayWStartOnFromDate(LDate: TDateTime): Integer;
{ Returns the Day-of-the-week (0 = Sunday) (Zeller's congruence) from an }
{ algorithm IZLR given in a remark on CACM Algorithm 398.                }
{1 = Sunday; monday = 2 ... 7}
//var
//  DayInWeek: Integer;
begin
  Result := PimsDayOfWeek(LDate);
{
  DayInWeek := DayWFromDate(LDate);
  Result := DayInWeek;
  if ORASystemDM.WeekStartsOn = 2 then
  begin
    Result := (DayInWeek - 1);
    if DayInWeek = 1 then
      Result := 7;
  end;
  if ORASystemDM.WeekStartsOn = 7 then
    Result := (DayInWeek mod 7 + 1);
}
end;

function DatePart(
  Interval: string;
  ADate: TDateTime;
  FirstDayOfWeek: Integer;
  FirstWeekOfYear: Integer): word;
var
  day: word;
  month: word;
  year: word;
begin
  { Year }
  if Interval = 'yyyy' then
  begin
    DecodeDate(ADate, year, month, day);
    result := year;
  end
  else
  { Month }
  if Interval = 'm' then
  begin
    DecodeDate(ADate, year, month, day);
    result := month;
  end
  else
  { Day }
  if Interval = 'd' then
  begin
    DecodeDate(ADate, year, month, day);
    result := day;
  end
  else
  { Week }
  if Interval = 'ww' then
  begin
    result := WeekOfYear(ADate);
  end
  else
    result := 0;
end;

function Min(int1,int2: Variant): Variant;
begin
  if int1 <= int2 then
    Result := int1
  else
    Result := int2;
end;

function Max(int1,int2: Variant): Variant;
begin
  if int1 >= int2 then
    Result := int1
  else
    Result := int2;
end;

function Seconds2StringMinSec(Seconds: Integer): String;
var
  Hrs, Min, Sec: Word;
begin
  if Seconds <= 0 then
  begin
    Hrs := 0;
    Min := 0;
    Sec := 0;
  end
  else
  begin
    Hrs := Seconds div 3600;
    Min := Seconds div 60 - Hrs * 60;
    Sec := Seconds - (Hrs * 3600 + Min * 60) ;
  end;
  Result := Format('%.2d:%.2d:%.2d', [Hrs, Min, Sec]);
end;

function IntMin2TimeMin(Minutes: Integer): TDateTime;
var
  Hrs, Min: word;
begin
  if Minutes <= 0 then
    ReplaceTime(Result, EncodeTime(0,0,0,0))
  else
  begin
    case Minutes of
      1..1439 :
        begin
          Hrs := Minutes div 60; Min := Minutes mod 60;
          ReplaceTime(Result, EncodeTime(Hrs,Min,0,0));
        end
      else
        ReplaceTime(Result, EncodeTime(23,59,59,999));
    end;
  end;
end;

function TimeMin2IntMin(ADate:TDateTime): Integer;
var
  hour,min,sec,msec: word;
begin
  DecodeTime(ADate,hour,min,sec,msec);
  Result := Hour*60 + min;
end;

function IntMin2StringTime(Minutes: Integer; ShowZero: Boolean): String;
var
  Negative: Boolean;
begin
  Negative := False;
  if Minutes = 0 then
  begin
    if ShowZero then
      Result:=NullTime
    else
      Result:='';
  end
  else
  begin
    if Minutes < 0 then
    begin
      Negative := True;
      Minutes := Minutes*(-1);
    end;
    Result := Format('%.2d:%.2d',[(Minutes div 60),(Minutes mod 60)]);
    if Negative then
      Result := '-' + Result;
  end;
end;

function IntMin2StrDigTime(Minutes,HrsDigits:Integer;ShowZero:Boolean): String;
var
  Negative: Boolean;
begin
  Negative := False;
  if Minutes = 0 then
  begin
    if ShowZero then
      Result:=NullTime
    else
      Result:='';
  end
  else
  begin
    if Minutes < 0 then
    begin
      Negative := True;
      Minutes := Minutes*(-1);
    end;
    Result := Format('%.'+IntToStr(HrsDigits)+'d:%.2d',
      [(Minutes div 60),(Minutes mod 60)]);
    if Negative then
      Result := '-' + Result;
  end;
end;

function StrTime2IntMin(ATime: String): Integer;
var
  index: Integer;
  hours, minutes, temp: string;
  negative,ready: Boolean;
begin
  result := 0;
  hours := ''; minutes := '';
  ready := False; negative := false;
  Temp := Trim(ATime);
  if (length(Temp)>=1) and (Temp[1]='-') then
  begin
    negative:=True;
    Delete(Temp,1,1);
  end;
  if length(temp)=0 then
    Exit;
  for index := 1 to length(Temp) do
  begin
    if Temp[index] = ':' then
    begin
      Ready := True;
      Continue;
    end;
    if not Ready then
      hours := hours + Temp[index]
    else
      minutes := minutes + Temp[index];
  end;
  while (length(hours)>0) and (hours[1]='0') do
    Delete(hours,1,1);
  while (length(minutes)>0) and (minutes[1]='0') do
    Delete(minutes,1,1);
  if hours = '' then
    hours := '0';
  if minutes = '' then
    minutes := '0';
  try
    result := (StrToInt(hours))*60 + StrToInt(minutes);
    if negative then
      result:=result*(-1);
  except
    result := 0;
  end;
end;

// 20015346
function RoundTimeConditional(const ADateTime: TDateTime; const ATarget: Integer): TDateTime;
begin
  if ORASystemDM.TimerecordingInSecs then
    Result := ADateTime
  else
    Result := RoundTime(ADateTime, ATarget);
end;

function RoundTime(const ADateTime: TDateTime; const ATarget: Integer): TDateTime;
const
  MinSecRounder = 30;
  MSecRounder = 500;
var
  Hour, Min, Sec, Msec: Word;
begin
  Result := ADateTime;
  DecodeTime(ADateTime, Hour, Min, Sec, Msec);
  case ATarget of
    // round to seconds
    0:
      begin
        if Msec > MSecRounder then
        begin
          Sec := Sec + 1;
          if Sec = 60 then
          begin
            Sec := 0;
            Min := Min + 1;
          end;
        end;
      end;
    // round to minutes
    1:
      begin
        if Sec > MinSecRounder then
          Min := Min + 1;
        Sec := 0;
      end;
  end; // case
  if Min = 60 then
  begin
    Min := 0;
    Hour := Hour + 1;
    if Hour = 24 then
    begin
      Hour := 0;
      Result := Result + 1;
    end;
  end;
  Result := Trunc(Result) + Frac(EncodeTime(Hour, Min, Sec, 0));
end;

// MR:03-12-2003
function DayInWeek(WeekStartOn: Integer; CurrentDate: TDateTime):Integer;
//type
//  TWeekDays = array [1..7] of Integer;
//var
//  CurrentDateInWeek: Integer;
begin
  Result := PimsDayOfWeek(CurrentDate);
//  CurrentDateInWeek := DayOfWeek(CurrentDate);
//  if WeekStartOn <= CurrentDateInWeek then
//    result := CurrentDateInWeek - WeekStartOn +1
//  else
//    result := 8 - WeekStartOn + CurrentDateInWeek;
end;

function DateInInterval(ADate, TestDate : TDateTime;
  MinTo, MinAfter: Integer): TDateTime;
var
  MinDate, MaxDate: TDateTime;
begin
  Result  := TestDate;
  MinDate := ADate - MinTo / DayToMin;
  MaxDate := ADate + MinAfter / DayToMin;
  if (MinDate <= TestDate) and (TestDate <= MaxDate) then
    Result := ADate;
end;

function ComputeMinutesOfIntersection(start1,end1,start2,
  end2: TDateTime): Integer;
begin
  result:= 0;
  if (end1 < start1) or (end2 < start2) then
    Exit;
  Result:= (min(end1, end2) - max(start1,start2)) * DayToMin;
  if Result < 0 then
    Result:= 0;
end;

// MR:07-11-2003 Corrects dates if end-date is '00:00' (midnight)
function MidnightCorrection(STime, ETime: TDateTime): TDateTime;
var
  Hours, Mins, Secs, MSecs: Word;
begin
  Result := ETime;
  if Trunc(STime) = Trunc(ETime) then
  begin
    DecodeTime(Result, Hours, Mins, Secs, MSecs);
    // If midnight then set 'datetime'-variable to next day
    if (Hours = 0) and (Mins = 0) then
      Result := Trunc(Result + 1) + Frac(Result);
  end;
end;

procedure GetStartEndDay(var StartDateTime, EndDateTime: TDateTime);
begin
  ReplaceTime(StartDateTime, EncodeTime(0,0,0,0));
  EndDateTime := StartDateTime;
  ReplaceTime(EndDateTime, EncodeTime(23,59,59,999));
end;

function CharFromStr(const AStr: String; const ACharIndex: Integer;
  const ABlankChar: Char): Char;
begin
  Result := ABlankChar;
  if Length(AStr) < ACharIndex then
    Exit;
  Result := AStr[ACharindex];
end;

function BuildMinTime(AStrTime: String): Integer;
begin
  try
    Result := StrTime2IntMin(Trim(AStrTime));
  except
    Result := 0;
  end;
end;

function GetNoFromStr(MyStr: String): Integer;
var
  Index: Integer;
  NoStr: String;
begin
  Nostr := '';
  for Index := 1 to Length(MyStr) do
    if MyStr[Index] in ['-','0'..'9'] then
      Nostr := NoStr + MyStr[Index]
    else
      if (Length(NoStr) > 0) then Break;
  try
    Result := StrToInt(NoStr);
  except
    Result := 0;
  end;
end;

function DateTimeAfter(ADate: TDateTime;
  const ASeconds: Integer): TDateTime;
begin
  Result := ADate + (ASeconds / SecsPerDay);
end;

function DateTimeBefore(ADate: TDateTime;
  const ASeconds: Integer): TDateTime;
begin
  Result := ADate - (ASeconds / SecsPerDay);
end;

function DateTimeTruncToInterval(ADate: TDateTime;
  const ASeconds: Integer): TDateTime;
var
  ATime: TDateTime;
  Hour, Min, Sec, MSec: Word;
  IHour, IMin, ISec{, IMSec}: Integer;
  Rest: Integer;
begin
  DecodeTime(ADate, Hour, Min, Sec, MSec);
  IHour := Hour;
  IMin := Min;
  ISec := Sec;
//  IMSec := MSec;
  Rest := IMin mod (ASeconds div 60);
  Rest := Rest * 60 + ASeconds mod 60;
  while Rest > 0 do
  begin
    dec(ISec);
    if ISec < 0 then
    begin
      ISec := 59;
      dec(IMin);
      if IMin < 0 then
      begin
        IMin := 59;
        dec(IHour);
        if IHour < 0 then
        begin
          IHour := 23;
          // Previous day
          ADate := ADate - 1;
        end;
      end;
    end;
    dec(Rest);
  end;
  Hour := IHour;
  Min := IMin;
  // Set Sec and MSec to 0
  Sec := 0;
  MSec := 0;
  ATime := EncodeTime(Hour, Min, Sec, MSec);
  ADate := Trunc(ADate) + Frac(ATime);
  Result := ADate;
end;

function IsRoundMidnight(const ADateTime: TDateTime;
  const AFPIMSTimeInterval: Integer): Boolean;
var
  Hours, Mins, Secs, MSecs: Word;
begin
  DecodeTime(ADateTime, Hours, Mins, Secs, MSecs);
  // FPIMSTimeInterval is in Seconds
  Result := (Hours = 0) and ((Mins * 60 + Secs) < (AFPIMSTimeInterval * 2));
end;

// PIM-87
function Bool2Str(AValue: Boolean): String;
begin
  if AValue then
    Result := 'T'
  else
    Result := 'F';
end;

// PIM-87
function Bool2Int(AValue: Boolean): Integer;
begin
  if AValue then
    Result := 1
  else
    Result := 0;
end;

initialization
  UGLogFilename := UGLOGFILENAME_DEFAULT;
  UGErrorLogFilename := UGERRORLOGFILENAME_DEFAULT;
  UGlobalLogPath := '';
  UGlobalErrorLogPath := '';

end.

