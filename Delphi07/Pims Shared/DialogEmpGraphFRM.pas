(*
  Changes:
    MRA:26-08-2008: RV009 (Revision 009)
      - The function to get and show a chart was very slow. This is probably a
        Oracle 10-problem. To solve it, the query that gets the values
        is divided over 2 queries. One only gets the number of records,
        the other one gets the data. Also a hint is used.
      - Also solved a rounding-problem during export of a chart.
    MRA:JUN-2010. RV063.4. Order 550478. Personal Screen.
    - Addition of Machine/Workspot + Time recording.
    MRA:8-NOV-2011 RV083.1. Research.
    - Test to see if employee-list can be added to graph and can also
      be printed.
    MRA:21-NOV-2011. RV083.2. 20012357. Bugfix.
    - Query 'oqTimeRegScanningJob' and 'oqTimeRegScanning' did not look at
      the Plant, this could result in showing employees from a different
      plant in case workspotcode and jobcode are the same.
      Solved by adding PLANT_CODE to the query.
    MRA:13-SEP-2012 SO-20013516 Related to this order
    - NOJOB is not ignored, resulting in large quantities.
    MRA:28-MAY-2013 20014289 New look Pims
    - Some pictures/colors are changed.
    MRA:4-APR-2014 TD-24100 Percentage not shown at workspot
    - Sometimes it shows 0 for percentage while it should be > 0.
    - Cause: During ShowAdditionalInfo it called ShowEfficiency again, but
             it must not do this when the form is just openened, or it shows
             0.
    MRA:11-JUN-2014 TD-25123
    - A small change is needed:
    - In Production Screen and 'show graph' the nr-of-employees must be
      shown in block-form. This prevents it is shown as a peak
      (gradually goes up and down over time).
    MRA:17-OCT-2014 TD-25881
    - Problems with show graph
    - When the ShowChartFRM is closed, it must uncheck the 'show dates' checkbox,
      or it will not show a graph when you open this the graph-form again!
    - Because the efficiency was wrong when using 'enter dates' checkbox, it now
      does a call to ActionEfficiency, to ensure it is calculated in the same
      way as in the main-screen.
    MRA:2-MAR-2015 TD-25881.50 Rework
    - Problems with show graph
    - When clicking in the graph it showed 0 as efficiency.
    - Solution: When clicking in graph do NOT calculate efficiency via
      HomeF.routine.
    - It found more than 1 record per period.
    - Also for ChartExport some changes were needed.
    MRA:10-APR-2015 SO-20016449
    - Time Zone Implementation
    MRA:22-MAY-2015 20014450.50 Part 2
    - Real Time Efficiency
*)

unit DialogEmpGraphFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ORASystemDMT, ActnList, ComCtrls, ExtCtrls, StdCtrls,
  Series, TeEngine, TeeProcs, Chart, dxTL, dxDBCtrl,
  dxCntner, dxEditor, dxExEdtr, dxEdLib, Buttons, BaseTopLogoFRM,
  Menus, StdActns, ImgList, Oracle, ProductionScreenDMT, UPimsConst, jpeg;

const
  MAX_HOR_POINTS = 24; // Max horizontal points to show on hor. axis

type
  TProductionData = record
    ADateTime: TDateTime;
    ATimeInterval: Integer;
    AProductivityPerHour: Double;
    ANormProductionTotal: Double;
    ATotalPercentage: Double;
    AEmployeeCount: Integer;
    AClicked: Boolean; // TD-25881.50
  end;

type
  PTPosition = ^TPosition;
  TPosition = record
    ADateTime: TDateTime;
  end;

type
  PTEmployee2 = ^TEmployee2;
  TEmployee2 = record
    AEmployeeNumber: Integer;
    AEmployeeName: String;
    AJobCode: String;
    AJobDescription: String;
    AShiftNumber: Integer;
    ADateTimeIn: TDateTime;
    ADateTimeOut: TDateTime;
    ANormProdLevel: Integer;
  end;

type
  PTWorkspot = ^TWorkspot;
  TWorkspot = record
    AMachineCode: String;
    AWorkspotCode: String;
    AJobCode: String;
  end;

type
  TDialogEmpGraphF = class(TBaseTopLogoForm)
    pnlAllTopInfo: TPanel;
    pnlChart: TPanel;
    Chart1: TChart;
    TimerProductionChart: TTimer;
    pnlTopInfo: TPanel;
    pnlEmployees: TPanel;
    Splitter1: TSplitter;
    pnlDateInput: TPanel;
    Panel1: TPanel;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    dxList: TdxTreeList;
    dxListColumnMACHINE: TdxTreeListColumn;
    dxListColumnWORKSPOT: TdxTreeListColumn;
    dxListColumnJOB: TdxTreeListColumn;
    FileExportAct: TAction;
    SaveDialog1: TSaveDialog;
    TimerStatus: TTimer;
    pnlInfo: TPanel;
    lblEfficiencyDescription: TLabel;
    lblEfficiency: TLabel;
    lblTotalProdDescription: TLabel;
    lblTotalProd: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    sbtnHorScrollRight: TBitBtn;
    sbtnHorScrollLeft: TBitBtn;
    lblHelp: TLabel;
    Series2: TAreaSeries;
    Print1: TMenuItem;
    ExportChart1: TMenuItem;
    Series3: TLineSeries;
    Series4: TAreaSeries;
    btnCloseGraph: TButton;
    Panel5: TPanel;
    Panel6: TPanel;
    lblEmpCnt: TLabel;
    Series1: TLineSeries;
    cBoxShowEmpl: TCheckBox;
    cBoxScale: TCheckBox;
    TimerAutoClose: TTimer;
    procedure FormShow(Sender: TObject);
    procedure TimerProductionChartTimer(Sender: TObject);
    procedure Chart1ClickSeries(Sender: TCustomChart; Series: TChartSeries;
      ValueIndex: Integer; Button: TMouseButton; Shift: TShiftState; X,
      Y: Integer);
    procedure FormCreate(Sender: TObject);
    procedure cBoxShowEmplClick(Sender: TObject);
    procedure btnAcceptClick(Sender: TObject);
    procedure cBoxEnterDatesClick(Sender: TObject);
    procedure cBoxScaleClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FilePrintActExecute(Sender: TObject);
    procedure FileExportActExecute(Sender: TObject);
    procedure FileSettingsActExecute(Sender: TObject);
    procedure TimerStatusTimer(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure sbtnHorScrollRightClick(Sender: TObject);
    procedure sbtnHorScrollLeftClick(Sender: TObject);
    procedure btnCloseGraphClick(Sender: TObject);
    procedure Chart1AfterDraw(Sender: TObject);
    procedure stbarBaseDrawPanel(StatusBar: TStatusBar;
      Panel: TStatusPanel; const Rect: TRect);
    procedure TimerAutoCloseTimer(Sender: TObject);
  private
    { Private declarations }
    EmployeeList: TList;
    EmployeeListCount: Integer;
    TimerOnTime: TDateTime;
    FPlantCode: String;
    FWorkspotCode: String;
    FWorkspotDescription: String;
    FExportPath: String;
    FExportSeparator: String;
    FDatacolTimeInterval: Integer;
    XPosList: TList;
    XPosListCount: Integer;
    // Export variables
    TExportFile: TextFile;
    ExportNow: Boolean;
    ExportFilename: String;
    FRefreshTimeInterval: Integer;
    FEfficiencyPeriodSince: Boolean;
    FTotalPercentage: Double;
    FEfficiencyPeriodSinceTime: TDateTime;
//    FEfficiencyPeriodMode: TEffPeriodMode;
    FEfficiencyPeriodShiftStart: TDateTime;
    FInit: Boolean;
    FDebug: Boolean;
    FEmployeeNumber: Integer;
    FEmployeeName: String;
    WorkspotList: TList;
    procedure SetPlantCode(const Value: String);
    procedure SetWorkspotCode(const Value: String);
    procedure SetWorkspotDescription(const Value: String);
    function DifferenceInMinutes(ADateFrom, ADateTo: TDateTime): Integer;
(*    procedure DetermineEmployeesForWorkspot(
      const DateFrom, DateTo: TDateTime;
      var EmployeeCount: Integer;
      var NormProdLevelTotal: Double;
      dxList: TdxTreeList); *)
{    procedure DetermineEmployeesForWorkspotCreateList(
      const APlantCode, AWorkspotCode, AJobCode: String;
      DateFrom: TDateTime;
      const DateTo: TDateTime;
      var EmployeeCount: Integer;
      var NormProdLevelTotal: Double); }
(*    procedure DetermineEmployeesForWorkspotNoJobCreateList(
      const APlantCode, AWorkspotCode: String;
      DateFrom: TDateTime;
      const DateTo: TDateTime;
      var EmployeeCount: Integer;
      var NormProdLevelTotal: Double); *)
(*    procedure ActionProductionGraphXXX(
      var ProductionData: TProductionData;
      APlantCode, AWorkspotCode: String;
      DateFrom, DateTo: TDateTime;
      UpdateChart: Boolean); *)
    procedure ActionProductionGraph(
      var ProductionData: TProductionData;
      APlantCode, AWorkspotCode: String;
      DateFrom, DateTo: TDateTime;
      UpdateChart: Boolean);
(*    procedure ActionProductionGraphExport(
      var ProductionData: TProductionData;
      APlantCode, AWorkspotCode: String;
      DateFrom, DateTo: TDateTime;
      UpdateChart: Boolean); *)
    procedure ShowAdditionalInfo(const AInit: Boolean;
      AProductionData: TProductionData);
//    procedure ShowAdditionalInfoOnStatusBar(ProductionData: TProductionData);
    procedure ShowEmployees;
    procedure ShowEnterDates(Sender: TObject);
    procedure ShowEndOfChart;
    procedure AutoScaleSwitch;
(*    procedure EmpListAdd(
      var dxList: TdxTreeList;
      AEmployeeNumber: Integer;
      AEmployeeName: String;
      AJobCode: String;
      AJobName: String;
      AShiftNumber: Integer;
      ADateTimeIn: TDateTime); *)
    function WorkspotListAdd(
      AMachineCode, AWorkspotCode, AJobCode: String): Boolean;
    procedure dxWorkspotListAdd(
      var dxList: TdxTreeList;
      AMachine: String;
      AWorkspot: String;
      AJob: String);
    procedure SetExportPath(const Value: String);
    procedure SaveExportFileAs;
    procedure SetExportSeparator(const Value: String);
    procedure SetRefreshTimeInterval(const Value: Integer);
    procedure SetDatacolTimeInterval(const Value: Integer);
    procedure EmployeeListClear;
    procedure ShowEfficiency(const AInit: Boolean;
      const AAtDateTime: TDateTime;
      const ATotalPercentage: Double);
//    function CalculatePercentage(AProductionData: TProductionData): Double;
//    function MyFormatTime(ADateTime: TDateTime): String;
//    function MyFormatDate(ADateTime: TDateTime): String;
    function MyFormatDateTime(ADateTime: TDateTime): String;
    procedure ScrollAxis(Axis: TChartAxis; const Percent: Double);
    procedure HorizScroll(const Percent: Double);
    function FormatPercentage(APercentage: Double): String;
    procedure WorkspotListClear;
  public
    { Public declarations }
    property PlantCode: String write SetPlantCode;
    property WorkspotCode: String write SetWorkspotCode;
    property WorkspotDescription: String write SetWorkspotDescription;
    property MyExportPath: String write SetExportPath;
    property MyExportSeparator: String read FExportSeparator
      write SetExportSeparator;
    property RefreshTimeInterval: Integer read FRefreshTimeInterval
      write SetRefreshTimeInterval;
    property DatacolTimeInterval: Integer read FDatacolTimeInterval
      write SetDatacolTimeInterval;
    property EfficiencyPeriodSince: Boolean read FEfficiencyPeriodSince
      write FEfficiencyPeriodSince;
    property EfficiencyPeriodSinceTime: TDateTime
      read FEfficiencyPeriodSinceTime write FEfficiencyPeriodSinceTime;
    property TotalPercentage: Double read FTotalPercentage
      write FTotalPercentage;
    property EfficiencyPeriodShiftStart: TDateTime
      read FEfficiencyPeriodShiftStart write FEfficiencyPeriodShiftStart;
(*    property EfficiencyPeriodMode: TEffPeriodMode read FEfficiencyPeriodMode
      write FEfficiencyPeriodMode; *)
    property Init: Boolean read FInit write FInit;
    property Debug: Boolean read FDebug write FDebug;
    property EmployeeNumber: Integer read FEmployeeNumber write FEmployeeNumber;
    property EmployeeName: String read FEmployeeName write FEmployeeName;
  end;

var
  DialogEmpGraphF: TDialogEmpGraphF;

implementation

uses
  DialogPrintChartFRM,
  DialogChartSettingsFRM,
  UGlobalFunctions,
  UPimsMessageRes,
  UProductionScreenDefs,
  RealTimeEffDMT;

{$R *.DFM}

{ TShowChartF }

procedure TDialogEmpGraphF.FormCreate(Sender: TObject);
begin
  inherited;
  lblMessage.Caption := ' ' + Caption; // 20014289

  // 20014289
//  stBarBase.Color := clPimsBlue; // PIM-250

  XPosListCount := 0;
  ExportNow := False;
  Init := True;

  Chart1.AddSeries(Series1);
  Chart1.AddSeries(Series2); // Background!
  Chart1.AddSeries(Series3);
  Chart1.AddSeries(Series4);

  // 20014450.50
  Series4.Title := 'Time (min x 100)';
  Series4.AreaColor := clPimsDarkBlue;

  EmployeeList := TList.Create;
  EmployeeListCount := 0;

  lblTotalProdDescription.Caption := SPimsTotalProduction + ':';
  lblTotalProd.Caption := '';

  WorkspotList := TList.Create;
end;

procedure TDialogEmpGraphF.ShowEfficiency(
  const AInit: Boolean;
  const AAtDateTime: TDateTime;
  const ATotalPercentage: Double);
//var
//  Hour, Min, Sec, MSec: Word;
begin
//  if Init and EfficiencyPeriodSince then
(*  if AInit and ((EfficiencyPeriodMode = epSince) or
    (EfficiencyPeriodMode = epShift)) then
  begin
    if (EfficiencyPeriodMode = epSince) then
    begin
      DecodeTime(EfficiencyPeriodSinceTime, Hour, Min, Sec, MSec);
      lblEfficiencyDescription.Caption :=
        SPimsEfficiencySince + ' ' + ZeroFormat(IntToStr(Hour), 2) + ':' +
          ZeroFormat(IntToStr(Min), 2) + ':';
    end
    else
    begin
      DecodeTime(EfficiencyPeriodShiftStart, Hour, Min, Sec, MSec);
      lblEfficiencyDescription.Caption :=
        SPimsEfficiencyShift + ' ' + ZeroFormat(IntToStr(Hour), 2) + ':' +
          ZeroFormat(IntToStr(Min), 2) + ':';
    end;
  end
  else
    lblEfficiencyDescription.Caption := SPimsEfficiencyAt + ':';
*)
(*
  if AInit then
  begin
//    if EfficiencyPeriodSince then
    if (EfficiencyPeriodMode = epSince) or
      (EfficiencyPeriodMode = epShift) then
      lblEfficiency.Caption :=
// TD-24100 This was 'TotalPercentage' but must problable be ATotalPercentage?
        FormatPercentage(ATotalPercentage) + ' %'
    else
      lblEfficiency.Caption :=
        MyFormatDateTime(dxDateTo.Date) + ' ' +
            FormatPercentage(ATotalPercentage) + ' %';
  end
  else
    lblEfficiency.Caption :=
      MyFormatDateTime(AAtDateTime) + ' ' +
          FormatPercentage(ATotalPercentage) + ' %';
*)
  lblEfficiencyDescription.Caption := SPimsEfficiencyAt + ':';
end;

procedure TDialogEmpGraphF.FormShow(Sender: TObject);
var
  MyNow: TDateTime; // 20016449
begin
  inherited;
  DatacolTimeInterval := 300; // 20014450.50 Set fixed to 5 minutes!

  sbtnHorScrollRight.Color := clBtnFace; // 20014450.50
  sbtnHorScrollLeft.Color := clBtnFace;  // 20014450.50
  
  lblMessage.Caption := EmployeeName;

  XPosListCount := 0;

  MyNow := Now;
  lblEmpCnt.Caption := '';

  // TD-25881-50 Moved to this place, to prevent it will show 0 as date-time.
//  dxDateFrom.Date := Trunc(MyNow) + Frac(EncodeTime(7, 0, 0, 0));
//  dxDateTo.Date := MyNow;

  // Init := True; // TD-25881 This is assigned before this form is called
  ShowEfficiency(True, MyNow, TotalPercentage); // 20016449

  // This interval is set by HomeFRM
  TimerProductionChart.Interval := RefreshTimeInterval * 1000;

  AutoScaleSwitch;

//  Caption := SPimsColumnPlant + ': ' + FPlantCode + ' ' +
//    SPimsColumnWorkspot + ': ' + FWorkspotCode + ' - ' + FWorkspotDescription;

  Chart1.Title.Text.Clear;
  Chart1.Title.Text.Add(SPimsProductionOutput{ + ' - ' + Caption});

//  lblHelp.Caption := '(' + SPimsChartHelp + ')';
  lblHelp.Caption := '';
  
  ShowEmployees;
  ShowEnterDates(Sender);

  TimerProductionChartTimer(Sender);
  TimerProductionChart.Enabled := False;
  TimerOnTime := Now; 
  TimerStatus.Enabled := True;
  
  TimerAutoClose.Interval := ORASystemDM.AutoCloseInterval;
  if TimerAutoClose.Interval > 0 then
    TimerAutoClose.Enabled := True;
end;

procedure TDialogEmpGraphF.ShowEndOfChart;
var
  Difer: Double;
begin
  if not Chart1.BottomAxis.Automatic then
  begin
    with Chart1.BottomAxis do
    begin
      if Chart1.MaxXValue(Chart1.BottomAxis) > MAX_HOR_POINTS then
      begin
        try
          Minimum := 0;
          Difer := Maximum - Minimum;
          Maximum := Chart1.MaxXValue(Chart1.BottomAxis);
          if (Maximum - Difer) < Maximum then
            Minimum := Maximum - Difer;
        except
          Minimum := 1;
          Maximum := MAX_HOR_POINTS;
        end;
      end;
    end;
  end;
end;

procedure TDialogEmpGraphF.AutoScaleSwitch;
begin
  Chart1.BottomAxis.Automatic := cBoxScale.Checked;
  Chart1.BottomAxis.AutomaticMaximum := cBoxScale.Checked;
  Chart1.BottomAxis.AutomaticMinimum := cBoxScale.Checked;
  with Chart1.BottomAxis do
  begin
    if Automatic then
      Chart1.AllowPanning := pmNone
    else
    begin
      Chart1.AllowPanning := pmHorizontal;
      try
        Minimum := 0;
        if MAX_HOR_POINTS > Minimum then
        begin
          Maximum := MAX_HOR_POINTS;
          Minimum := 1;
        end
        else
        begin
          Minimum := 1;
          Maximum := MAX_HOR_POINTS;
        end;
      except
        Minimum := 1;
        Maximum := MAX_HOR_POINTS;
      end;
      ShowEndOfChart;
    end;
  end;
end;

procedure TDialogEmpGraphF.SetPlantCode(const Value: String);
begin
  FPlantCode := Value;
end;

procedure TDialogEmpGraphF.SetWorkspotCode(const Value: String);
begin
  FWorkspotCode := Value;
end;

procedure TDialogEmpGraphF.SetWorkspotDescription(const Value: String);
begin
  FWorkspotDescription := Value;
end;

function TDialogEmpGraphF.DifferenceInMinutes(ADateFrom, ADateTo: TDateTime): Integer;
var
  Hour, Min, Sec, MSec: Word;
  DayDiff: Integer;
begin
  DayDiff := Trunc(ADateTo) - Trunc(ADateFrom);
  DecodeTime(ADateTo - ADateFrom, Hour, Min, Sec, MSec);
  Hour := Hour + DayDiff * 24;
  Result := Hour * 60 + Min;
  if ADateTo - ADateFrom < 0 then
    Result := Result * -1;
end;
(*
procedure TShowChartF.EmpListAdd(
  var dxList: TdxTreeList;
  AEmployeeNumber: Integer;
  AEmployeeName: String;
  AJobCode: String;
  AJobName: String;
  AShiftNumber: Integer;
  ADateTimeIn: TDateTime);
var
  Item: TdxTreeListNode;
begin
  if dxList <> nil then
  begin
    Item := dxList.Add;
    Item.Values[dxListColumnEMPNR.Index]      := AEmployeeNumber;
    Item.Values[dxListColumnEMPNAME.Index]     := AEmployeeName;
    Item.Values[dxListColumnJOBCODE.Index]     := AJobCode;
    Item.Values[dxListColumnJOBNAME.Index]     := AJobName;
    Item.Values[dxListColumnSHIFTNR.Index]     := AShiftNumber;
    Item.Values[dxListColumnDATETIME_IN.Index] := ADateTimeIn;
  end;
end;
*)
procedure TDialogEmpGraphF.EmployeeListClear;
var
  I: Integer;
  APTEmployee: PTEmployee2;
begin
  if EmployeeListCount > 0 then
  begin
    for I := EmployeeList.Count - 1 downto 0 do
    begin
      APTEmployee := EmployeeList.Items[I];
      Dispose(APTEmployee);
      EmployeeList.Remove(APTEmployee);
    end;
    EmployeeList.Clear;
  end;
  EmployeeListCount := 0;
end;
{
function TDialogEmpGraphF.CalculatePercentage(
  AProductionData: TProductionData): Double;
begin
  if AProductionData.ANormProductionTotal > 0 then
    Result :=
      AProductionData.AProductivityPerHour /
        AProductionData.ANormProductionTotal * 100
//      Trunc(AProductionData.AProductivityPerHour
//        / AProductionData.ANormProductionTotal * 100)
  else
    Result := 0;
end;
}
(*
// Determine prodction values for Export
procedure TShowChartF.ActionProductionGraphExport(
  var ProductionData: TProductionData;
  APlantCode, AWorkspotCode: String;
  DateFrom, DateTo: TDateTime;
  UpdateChart: Boolean);
var
  StartDateTime: TDateTime;
  EndDateTime: TDateTime;
  Quantity: Double;
  QuantityPerHour: Double;
  NormProdLevelTotal: Double;
  EmployeeCount: Integer;
  Period: Integer;
  Hours, Mins, Secs, MSecs: Word;
  Year, Month, Day: Word;
  ExportLine: String;
begin
  // Init some values
  NormProdLevelTotal := 0;
  QuantityPerHour    := 0;
  EmployeeCount      := 0;
  Period             := 0;
  ProductionData.ADateTime            := Now;
  ProductionData.ANormProductionTotal := NormProdLevelTotal;
  ProductionData.AProductivityPerHour := QuantityPerHour;
  ProductionData.ATotalPercentage     := 0;
  ProductionData.AEmployeeCount       := EmployeeCount;
  ProductionData.ATimeInterval        := Period;

  with ProductionScreenDM, oqProductionQuantityChartExport do
  begin
    // TD-25881.50 MyDateTo() is adding 5 minutes! Do not use it!
    SetVariable('DATEFROM',      DateFrom);
    SetVariable('DATETO',        DateTo); // MyDateTo(DateTo));
    SetVariable('PLANT_CODE',    APlantCode);
    SetVariable('WORKSPOT_CODE', AWorkspotCode);
    SetVariable('JOB_CODE',      NOJOB); // SO-20013516
    Execute;
//    if RecordCount > 0 then
    if not Eof then
    begin
// already true      First;
      // TD-25881.50 Use procedure with 'NoJob' ! 
      DetermineEmployeesForWorkspotNoJobCreateList(
        APlantCode, //FieldAsString('PLANT_CODE'),
        AWorkspotCode, //FieldAsString('WORKSPOT_CODE'),
        //FieldAsString('JOB_CODE'),  // so jobcode is now needed
        DateFrom,
        DateTo,
        EmployeeCount,
        NormProdLevelTotal);
      while not Eof do
      begin
        StartDateTime  := FieldAsDate('START_DATE');
        EndDateTime    := FieldAsDate('END_DATE');
        Quantity       := FieldAsFloat('PQQUANTITY');
        Period := DifferenceInMinutes(StartDateTime, EndDateTime);
        QuantityPerHour := 0;
        if Period > 0 then
          QuantityPerHour := Quantity * 60 / Period;

        DetermineEmployeesForWorkspot(
          StartDateTime, EndDateTime,
          EmployeeCount, NormProdLevelTotal,
          nil);
        // Fill Record with some values that can be used in
        // other views.
        ProductionData.ADateTime            := DateTo;
        ProductionData.ANormProductionTotal := NormProdLevelTotal;
        ProductionData.AProductivityPerHour := QuantityPerHour;
        ProductionData.ATotalPercentage     := CalculatePercentage(ProductionData);
        ProductionData.AEmployeeCount       := EmployeeCount;
        ProductionData.ATimeInterval        := Period;
        // Now do something with the data
        DecodeDate(StartDateTime, Year, Month, Day);
        DecodeTime(StartDateTime, Hours, Mins, Secs, MSecs);

        ExportLine :=
          IntToStr(Month) + '-' + IntToStr(Day) + '-' + IntToStr(Year) +
            MyExportSeparator +
          ZeroFormat(IntToStr(Hours), 2) + ':' +
          ZeroFormat(IntToStr(Mins), 2) +
            MyExportSeparator +
          APlantCode +  // FieldAsString('PLANT_CODE'),
            MyExportSeparator +
          AWorkspotCode +  // FieldAsString('WORKSPOT_CODE'),
            MyExportSeparator +
          FieldAsString('JOB_CODE') +
            MyExportSeparator +
// MRA:26-08-2008 Trunc value, to prevent a rounding problem, showing a float
//                like 55,40394912381459 instead of 55
//          FloatToStr(QuantityPerHour) +
          IntToStr(Trunc(QuantityPerHour)) +
            MyExportSeparator +
          FloatToStr(NormProdLevelTotal) +
            MyExportSeparator +
          IntToStr(EmployeeCount);
        WriteLn(TExportFile, ExportLine);
        Next;
      end; {whle not Eof}
// not for oq's      Close;
    end; {if not Eof}
  end; {with ProductionScreenDM}
end; // ActionProductionGraphExport
*)
procedure TDialogEmpGraphF.ShowAdditionalInfo(const AInit: Boolean;
  AProductionData: TProductionData);
var
  MyDateTime: TDateTime;
begin
  if AProductionData.ADateTime = 0 then
    MyDateTime := Now
  else
    MyDateTime := AProductionData.ADateTime;
  if not AInit then // TD-24100 Only do this when Init is False!
    ShowEfficiency(False, MyDateTime,
      AProductionData.ATotalPercentage);
end;
{
procedure TDialogEmpGraphF.ShowAdditionalInfoOnStatusBar(ProductionData: TProductionData);
begin
//
end;
}
procedure TDialogEmpGraphF.TimerProductionChartTimer(Sender: TObject);
var
  ProductionData: TProductionData;
  APlantCode, AWorkspotCode: String;
  DateFrom, DateTo: TDateTime;
  MyNow: TDateTime; // 20016449
begin
  inherited;
  MyNow := Now;
  DateFrom := Trunc(MyNow);
  DateTo := MyNow;
  ActionProductionGraph(ProductionData,
    APlantCode, AWorkspotCode, DateFrom, DateTo, True)

(*
  MyNow := Now; // 20016449
  TimerProductionChart.Enabled := False;
  try
    APlantCode := FPlantCode;
    AWorkspotCode := FWorkspotCode;

    if cBoxEnterDates.Checked then
    begin
      DateFrom := dxDateFrom.Date;
      DateTo := dxDateTo.Date;
//      HomeF.DateFromGraph := DateFrom;
//      HomeF.DateToGraph := DateTo;
    end
    else
    begin
      // MR:27-10-2004 Since-date should be used here, if this
      // has been set, but 'since' is always used.
//      DateFrom := Date; // Today
//      DateFrom := Trunc(DateFrom) + Frac(EncodeTime(7, 0, 0, 0));
//      DateTo := Now;
      case EfficiencyPeriodMode of
      epSince, epCurrent:
        begin
          HomeF.DetermineSinceDates(MyNow, DateFrom, DateTo); // 20016449
        end;
      epShift:
        begin
          HomeF.DetermineSinceDates(MyNow, DateFrom, DateTo); // 20016449
          DateFrom := EfficiencyPeriodShiftStart;
        end;
      end;
      DateFrom := RoundTime(DateFrom, 1);
      DateTo := RoundTime(DateTo, 1);
      dxDateFrom.Date := DateFrom;
      dxDateTo.Date := DateTo;
    end;
{ HomeF.WriteLog(DateTimeToStr(Now) + ': ' +
  DateTimeToStr(DateFrom) + ' - ' + DateTimeToStr(DateTo)); }
    // Get the Data, show it in the Chart
    if not ExportNow then
      ActionProductionGraph(ProductionData,
        APlantCode, AWorkspotCode, DateFrom, DateTo, True)
    else
      ActionProductionGraphExport(ProductionData,
        APlantCode, AWorkspotCode, DateFrom, DateTo, True);
    ShowAdditionalInfoOnStatusBar(ProductionData);
    if Init or cBoxEnterDates.Checked then // TD-25881
    begin
      ShowAdditionalInfo(Init, ProductionData);
      Init := False;
    end;
  finally
    TimerProductionChart.Enabled := True;
    TimerOnTime := Now;
  end;
*)  
end;

procedure TDialogEmpGraphF.Chart1ClickSeries(Sender: TCustomChart;
  Series: TChartSeries; ValueIndex: Integer; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
  ProductionData: TProductionData;
  APlantCode, AWorkspotCode: String;
  DateFrom, DateTo: TDateTime;
  Hours, Mins: Integer;
  TimeStr: String;
  APTPosition: PTPosition;
begin
  inherited;
  if Button = mbRight then
    Exit;

  APlantCode := FPlantCode;
  AWorkspotCode := FWorkspotCode;
  DateFrom := Date;
  DateTo := Date;
  // Get Time (on X-Axis) as a string in format hh:mm
//  ShowMessage(Series.ValueMarkText[ValueIndex]);
  TimeStr := Series.ValueMarkText[ValueIndex];
  if (ValueIndex >= 0) and (ValueIndex <= XPosListCount-1) then
  begin
    APTPosition := XPosList.Items[ValueIndex];
    DateFrom := APTPosition.ADateTime;
    DateTo := APTPosition.ADateTime;
  end;
  Hours := StrToInt(Copy(TimeStr, 1, 2));
  Mins := StrToInt(Copy(TimeStr, 4, 2));
  DateFrom := Trunc(DateFrom) + Frac(EncodeTime(Hours, Mins, 0, 0));
  Mins := Mins + Trunc(DatacolTimeInterval / 60);
  if Mins > 59 then
  begin
    Mins := Mins - 60;
    if Hours < 23 then
      Hours := Hours + 1;
  end;
  // some minutes later (DatacolTimeInterval)
  DateTo := Trunc(DateTo) + Frac(EncodeTime(Hours, Mins, 0, 0));
  // Get the Date, don't show in Chart
  ProductionData.AClicked := True; // TD-25881.50
  ActionProductionGraph(ProductionData, APlantCode, AWorkspotCode,
    DateFrom, DateTo, False);
  // MR:08-02-2005 If this is zero, then it can be data from a gap.
  // In that case the 'Dateto' should be assigned.
  if ProductionData.ADateTime = 0 then
    ProductionData.ADateTime := DateTo;
  ShowAdditionalInfo(False, ProductionData);
  ProductionData.AClicked := False; // TD-25881.50
end;

procedure TDialogEmpGraphF.ShowEmployees;
begin
  pnlEmployees.Visible := cBoxShowEmpl.Checked;
  if pnlEmployees.Visible then
    pnlAllTopInfo.Height := pnlTopInfo.Height + pnlEmployees.Height
  else
    pnlAllTopInfo.Height := pnlTopInfo.Height;
end;

procedure TDialogEmpGraphF.cBoxShowEmplClick(Sender: TObject);
begin
  inherited;
  ShowEmployees;
end;

procedure TDialogEmpGraphF.btnAcceptClick(Sender: TObject);
begin
  inherited;
  TimerProductionChartTimer(Sender);
end;

procedure TDialogEmpGraphF.ShowEnterDates(Sender: TObject);
begin
//
end;

procedure TDialogEmpGraphF.cBoxEnterDatesClick(Sender: TObject);
begin
  inherited;
  ShowEnterDates(Sender);
  AutoScaleSwitch;
end;

procedure TDialogEmpGraphF.cBoxScaleClick(Sender: TObject);
begin
  inherited;
  AutoScaleSwitch;
end;

procedure TDialogEmpGraphF.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  cBoxScale.Checked := False;
  cBoxShowEmpl.Checked := False;
  TimerProductionChart.Enabled := False;
  TimerStatus.Enabled := False;
end;

procedure TDialogEmpGraphF.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if Key = VK_F5 then { Refresh }
    TimerProductionChartTimer(Sender);
  // TD-24100 Easier way to close this dialog
  if Key = VK_ESCAPE then
    Close;
end;

procedure TDialogEmpGraphF.FilePrintActExecute(Sender: TObject);
begin
  inherited;
//  DialogPrintChartF := TDialogPrintChartF.Create(Application);
  if DialogPrintChartF.ShowModal = mrOK then
  begin
    if DialogPrintChartF.Portrait then
      Chart1.PrintPortrait
    else
      Chart1.PrintLandscape;
  end;
//  DialogPrintChartF.Free;
end;

procedure TDialogEmpGraphF.SaveExportFileAs;
var
  Path: String;
  SaveFilename: String;
begin
  inherited;
  Path := GetCurrentDir;
  try
    SaveDialog1.InitialDir := FExportPath;
    SaveDialog1.Filename   := ExportFilename;
    if SaveDialog1.Execute then
    begin
      SaveFilename := ExtractFileName(SaveDialog1.Filename);
      if ExportFilename <> SaveFilename then
        CopyFile(PChar(FExportPath + ExportFilename),
          PChar(FExportPath + SaveFilename), False);
    end;
  finally
    SetCurrentDir(Path);
  end;
end;

procedure TDialogEmpGraphF.FileExportActExecute(Sender: TObject);
begin
  inherited;
  ExportNow := True;
  ExportFilename := 'Pims.txt';
  try
    AssignFile(TExportFile, FExportPath + ExportFilename);
    Rewrite(TExportFile);
    TimerProductionChartTimer(Sender);
  finally
    CloseFile(TExportFile);
    ExportNow := False;
    SaveExportFileAs;
  end;
end;

procedure TDialogEmpGraphF.SetExportPath(const Value: String);
begin
  FExportPath := Value;
end;

procedure TDialogEmpGraphF.SetExportSeparator(const Value: String);
begin
  FExportSeparator := Value;
end;

procedure TDialogEmpGraphF.FileSettingsActExecute(Sender: TObject);
begin
  inherited;
//  DialogChartSettingsF := TDialogChartSettingsF.Create(Application);
  DialogChartSettingsF.MyExportSeparator := MyExportSeparator;
  if DialogChartSettingsF.ShowModal = mrOK then
  begin
    MyExportSeparator := DialogChartSettingsF.MyExportSeparator;
  end;
//  DialogChartSettingsF.Free;
end;

procedure TDialogEmpGraphF.SetRefreshTimeInterval(const Value: Integer);
begin
  FRefreshTimeInterval := Value;
end;

procedure TDialogEmpGraphF.TimerStatusTimer(Sender: TObject);
//const
//  SecsADay = 86400;
//var
//  DurationInSec: Cardinal;
begin
  inherited;
(*  if TimerProductionChart.Enabled then
  begin
    DurationInSec := Trunc((Now - TimerOnTime) * SecsADay);
    stBarBase.Panels[0].Text := 'ABS' +
      ' (' + IntToStr(DurationInSec) + '/' + IntToStr(RefreshTimeInterval) + ')';
  end
  else
    stBarBase.Panels[0].Text := 'ABS' +
      ' (' + '0/' + IntToStr(RefreshTimeInterval) + ')'; *)
end;

procedure TDialogEmpGraphF.SetDatacolTimeInterval(const Value: Integer);
begin
  FDatacolTimeInterval := Value;
end;

procedure TDialogEmpGraphF.FormDestroy(Sender: TObject);
begin
  inherited;
  EmployeeListClear;
  EmployeeList.Free;
  WorkspotListClear;
  WorkspotList.Free;
end;

procedure TDialogEmpGraphF.FormResize(Sender: TObject);
begin
  inherited;
  pnlInfo.Left := Width - pnlInfo.Width;
  pnlInfo.Top := Height div 6;
end;

{
function TDialogEmpGraphF.MyFormatTime(ADateTime: TDateTime): String;
begin
  Result := FormatDateTime(ShortTimeFormat, ADateTime);
end;
}
{
function TDialogEmpGraphF.MyFormatDate(ADateTime: TDateTime): String;
begin
  Result := FormatDateTime(ShortDateFormat, ADateTime);
end;
}
function TDialogEmpGraphF.MyFormatDateTime(ADateTime: TDateTime): String;
begin
  Result := FormatDateTime(
    ShortDateFormat + ' ' + ShortTimeFormat, ADateTime);
end;

procedure TDialogEmpGraphF.ScrollAxis(Axis: TChartAxis; const Percent: Double);
var Amount:Double;
begin
  With Axis do
  begin
    Amount:=-((Maximum-Minimum)/(100.0/Percent));
    SetMinMax(Minimum-Amount,Maximum-Amount);
  end;
end;

procedure TDialogEmpGraphF.HorizScroll(const Percent: Double);
begin
  ScrollAxis(Chart1.TopAxis,Percent);
  ScrollAxis(Chart1.BottomAxis,Percent);
end;

procedure TDialogEmpGraphF.sbtnHorScrollRightClick(Sender: TObject);
begin
  inherited;
  HorizScroll(10);
end;

procedure TDialogEmpGraphF.sbtnHorScrollLeftClick(Sender: TObject);
begin
  inherited;
  HorizScroll(-10);
end;

procedure TDialogEmpGraphF.btnCloseGraphClick(Sender: TObject);
begin
  inherited;
  Close;
end;

// RV083.1. !!!TESTING!!!
// Test with showing of text below the chart.
// This does not work correct.
procedure TDialogEmpGraphF.Chart1AfterDraw(Sender: TObject);
var
  I, YPos: Integer;
begin
  inherited;

Exit;

  with Chart1, Canvas do
  begin
    Font.Name := 'Arial';
    Font.Height := -24;

    YPos := 0;
    { Output some text... }
    for I := 1 to 4 do
    begin
      TextOut(ChartRect.Left + 20,
        ChartRect.Bottom + 20 + YPos,
        'This is some text ' + IntToStr(I)
        );
//      ChartRect.Bottom := ChartRect.Bottom + YPos;
      Chart1.Height := Chart1.Height + YPos;
      YPos := YPos + 20;
    end;
  end;
end;

// 20014289
{
  To use this, you need a TStatusBar and at least one Panel.
  Then change the style of StatusBar1.Panels[0] to
  psOwnerDraw and add the code below to the OnDrawPanel handler.
}
procedure TDialogEmpGraphF.stbarBaseDrawPanel(StatusBar: TStatusBar;
  Panel: TStatusPanel; const Rect: TRect);
var
  SomeText: String;
  I: Integer;
  MyLeft: Integer;
begin
  inherited;
  MyLeft := 0;
  for I := 0 to StatusBar.Panels.Count - 1 do
  begin
    if Panel = StatusBar.Panels[I] then
    begin
      SomeText := StatusBar.Panels[I].Text;
//      Panel.Width := Trunc(stbarBase.Canvas.TextWidth(SomeText) * 1.5);
      Panel.Width := StatusBar.Panels[I].Width;
      with StatusBar.Canvas do
      begin
        Brush.Color := clDarkRed; // clPimsBlue; PIM-250
        FillRect(Rect);
//      Font.Name  := 'Arial';
         Font.Color := clWhite;
//      Font.Style := Font.Style + [fsItalic, fsBold];
        TextRect(Rect, MyLeft + 5,
          {Rect.Left + 1, }Rect.Top, SomeText);
      end;
    end; // if
    MyLeft := MyLeft + StatusBar.Panels[I].Width; //Panel.Width;
  end; // for
end;

// TD-25881
function TDialogEmpGraphF.FormatPercentage(APercentage: Double): String;
begin
//  Result := Format('%.2f', [APercentage]);
  Result := Format('%d %%', [Round(APercentage)]);
end;

// 20014550.50
// Determine values for Graph
procedure TDialogEmpGraphF.ActionProductionGraph(
  var ProductionData: TProductionData;
  APlantCode, AWorkspotCode: String;
  DateFrom, DateTo: TDateTime;
  UpdateChart: Boolean);
var
  StartDateTime: TDateTime;
  EndDateTime: TDateTime;
//  Quantity: Double;
  QuantityPerHour: Double;
  NormProdLevelTotal: Double;
  EmployeeCount: Integer;
  Period: Integer;
  PQCount{, PQRecordCount}: Integer;
  TotalQuantity: Double;
  I: Integer;
  MaxValue: Double;
  GapI, GapMinutes, GapCount: Integer;
  GapStartDateTime: TDateTime;
  PrevStartDateTime: TDateTime;
  MinutesActual: Double;
  Eff, TotalEff: Double;
  PrevAStartDateTime: TDateTime;
//  TestLine: String; 
  // Local Procedure
  procedure FreeXPosList(var XPosList: TList; var XPosListCount: Integer);
  var
    I: Integer;
    APTPosition: PTPosition;
  begin
    if XPosListCount > 0 then
    begin
      for I := XPosListCount - 1 downto 0 do
      begin
        APTPosition := XPosList[I];
        Dispose(APTPosition);
      end;
      XPosList.Free;
      XposListCount := 0;
    end;
  end;

  // MR:04-02-2005 procedure added.
  procedure AddItemToChart(const AStartDateTime: TDateTime;
    const AQuantityPerHour, ANormProdLevelTotal, AMinutesActual: Double;
    var AXPosListCount: Integer);
  var
    Year, Month, Day: Word;
    Hours, Mins, Secs, MSecs: Word;
    TimeStr: String;
    APTPosition: PTPosition;
  begin
    DecodeDate(AStartDateTime, Year, Month, Day);
    DecodeTime(AStartDateTime, Hours, Mins, Secs, MSecs);
    TimeStr := ZeroFormat(IntToStr(Hours), 2) + ':' +
      ZeroFormat(IntToStr(Mins), 2) + #13 +
       IntToStr(Month) + '-' + IntToStr(Day);
    // Update Item
    if PrevAStartDateTime = AStartDateTime then
    begin
      Series1.YValues[AXPosListCount-1] :=
        Series1.YValues[AXPosListCount-1] + AQuantityPerHour;
      Series3.YValues[AXPosListCount-1] :=
        Series3.YValues[AXPosListCount-1] + ANormProdLevelTotal;
      Series4.YValues[AXPosListCount-1] :=
        Series4.YValues[AXPosListCount-1] + AMinutesActual;
    end
    else
    begin
      // Add Item
      // Output
      Series1.AddY(AQuantityPerHour, TimeStr, Series1.SeriesColor);
      // Background!
      Series2.AddY(0, TimeStr, Series2.SeriesColor);
      // Norm Production Level
      //        Series3.AddY(ANormProdLevelTotal, TimeStr, Series3.AreaColor);
      Series3.AddY(ANormProdLevelTotal, TimeStr, Series3.SeriesColor);
      // MinutesActual (instead of EmployeeCount)
      Series4.AddY(AMinutesActual, TimeStr, Series4.AreaColor);
    end;

    if not Chart1.BottomAxis.Automatic then
       ShowEndOfChart;

    if PrevAStartDateTime <> AStartDateTime then
    begin
      // Store DateTime in TList
      New(APTPosition);
      APTPosition.ADateTime := AStartDateTime;
      XPosList.Add(APTPosition);
      Inc(AXPosListCount);
    end;
    PrevAStartDateTime := AStartDateTime;
  end;
begin
  // 25881 This is used in HomeF when calculating efficiency
//  HomeF.DateFromGraph := DateFrom;
//  HomeF.DateToGraph := DateTo;

  // Init some values
  NormProdLevelTotal := 0;
  QuantityPerHour    := 0;
  EmployeeCount      := 0;
  Period             := 0;
  ProductionData.ADateTime            := Now;
  ProductionData.ANormProductionTotal := NormProdLevelTotal;
  ProductionData.AProductivityPerHour := QuantityPerHour;
  ProductionData.ATotalPercentage     := 0;
  ProductionData.AEmployeeCount       := EmployeeCount;
  ProductionData.ATimeInterval        := Period;
  PQCount       := 0;
  TotalQuantity := 0;
  PrevStartDateTime := 0;
  EndDateTime := 0;
  MinutesActual := 0;
//  Eff := 0;
  TotalEff := 0;
  WorkspotListClear;
  dxList.ClearNodes;
  PrevAStartDateTime := 0;
//  TestLine := '';

  if UpdateChart then
  begin
    FreeXPosList(XPosList, XPosListCount);
    XPosList := TList.Create;
    Series1.Clear;  // Output
    Series2.Clear;  // Background!
    Series3.Clear;  // Norm Production Level
    Series4.Clear;  // Employees
  end;
  // Get the data.
  with RealTimeEffDM, oqRTEmpEffGraph do
  begin
    SetVariable('EMPLOYEE_NUMBER', EmployeeNumber);
    SetVariable('SHIFT_DATE',    Trunc(DateFrom));
    if UpdateChart then
      SetVariable('GETSHIFT', 1)
    else
      SetVariable('GETSHIFT', 0);
    SetVariable('DATEFROM', DateFrom);
    SetVariable('DATETO', DateTo);
    Execute;
//    PQRecordCount := oqRTEmpEffGraph.RowCount;
    if not Eof then  {oq equal to empty since first row = always active}
    begin
(*      if UpdateChart then
        DetermineEmployeesForWorkspotNoJobCreateList(
          APlantCode,    // FieldAsString('PLANT_CODE'),
          AWorkspotCode, // FieldAsString('WORKSPOT_CODE'),
//no jobcode          '',
          DateFrom,
          DateTo,
          EmployeeCount,
          NormProdLevelTotal); *)
      while not Eof do
      begin
        StartDateTime   := FieldAsDate('STARTDATE');
        EndDateTime     := StartDateTime + (5/1440); // 5 minutes later
        MinutesActual   := FieldAsFloat('MINUTESACTUAL');
        QuantityPerHour := FieldAsFloat('QTYPERHOUR');
        TotalQuantity   := TotalQuantity + FieldAsFloat('QTY'); //TotalQuantity + QuantityPerHour;
        NormProdLevelTotal := FieldAsFloat('NORMQTYPERHOUR');
        Eff             := FieldAsFloat('EFF');
        TotalEff        := TotalEff + Eff;
        Period          := DifferenceInMinutes(StartDateTime, EndDateTime);

        // Calculate norm-levels

        // Now get Employees that are Scanned-In on the Workspot and Jobcode,
        // This can influence the Norm-levels.
        Inc(PQCount);

        if WorkspotListAdd(FieldAsString('MACHINE_CODE'),
          FieldAsString('WORKSPOT_CODE'),
          FieldAsString('JOB_CODE')) then
          dxWorkspotListAdd(dxList, FieldAsString('MACHINE'),
            FieldAsString('WORKSPOT_DESCRIPTION'),
            FieldAsString('JOB_CODE_DESCRIPTION'));
(*
        if PQCount < PQRecordCount then
          DetermineEmployeesForWorkspot(
            StartDateTime, EndDateTime,
            EmployeeCount, NormProdLevelTotal,
            nil) // don't fill dxList
        else
          DetermineEmployeesForWorkspot(
            StartDateTime, EndDateTime,
            EmployeeCount, NormProdLevelTotal,
            dxList); // fill dxList
*)
        // Fill Record with some values that can be used in
        // other views.
        ProductionData.ADateTime := DateTo;
        ProductionData.ANormProductionTotal := NormProdLevelTotal;
        ProductionData.AProductivityPerHour := QuantityPerHour;
        ProductionData.ATotalPercentage := Eff; // CalculatePercentage(ProductionData);
        ProductionData.AEmployeeCount := EmployeeCount;
        ProductionData.ATimeInterval := Period;

        if UpdateChart then
        begin
          // MR:04-02-2005
          // Determine if there is a gap in the ProductionQuantity-records.
          // If this is the case, fill the gap.
          if PrevStartDateTime <> 0 then
          begin
            GapCount := 0;
            GapMinutes := DifferenceInMinutes(PrevStartDateTime, StartDateTime);
            if (GapMinutes > Trunc(DatacolTimeInterval / 60)) and
              (DatacolTimeInterval > 0) then
              GapCount := Trunc(GapMinutes / Trunc(DatacolTimeInterval / 60));
            if GapCount > 0 then
            begin
              GapStartDateTime := PrevStartDateTime +
                (Trunc(DatacolTimeInterval / 60) / 24 / 60);
              for GapI := 1 to GapCount - 1 do
              begin
                // Fill Chart-Output-Series
                AddItemToChart(GapStartDateTime, 0, 0, 0, XPosListCount);
                // Add minutes to datetime
                GapStartDateTime := GapStartDateTime +
                  (Trunc(DatacolTimeInterval / 60) / 24 / 60);
              end;
            end;
          end;
          // Fill Chart-Output-Series
          AddItemToChart(StartDateTime, QuantityPerHour,
            NormProdLevelTotal, MinutesActual {EmployeeCount}, XPosListCount);

// TESTING
{TestLine := TestLine +
  DateTimeToStr(StartDateTime) + ' | ' + DateTimeToStr(EndDateTime) + ' | ' +
  FloatToStr(QuantityPerHour) +
  #13; }
        end; // if UpdateChart then
        PrevStartDateTime := StartDateTime;
        Next;
      end; { while not Eof }
      // MRA:14-JUL-2010 Also add last position!
      if UpdateChart then
        AddItemToChart(EndDateTime, QuantityPerHour,
          NormProdLevelTotal, MinutesActual {EmployeeCount}, XPosListCount);
// not for oq's      Close;
    end; { if not Eof }
  end; {with ProductionScreenDM}
  // MR:11-01-2005 Show total production
//  if UpdateChart then
//    lblTotalProd.Caption := Format('%d', [Round(TotalQuantity)]);

  // Now change the values for series2 (Background)
  if UpdateChart then
  begin
    MaxValue := Series1.MaxYValue;
    if Series3.MaxYValue > MaxValue then
      MaxValue := Series3.MaxYValue;
    if Series4.MaxYValue > MaxValue then
      MaxValue := Series4.MaxYValue;
    if MaxValue > 0 then
      for I := 0 to Series2.YValues.Count - 1 do
        Series2.YValues[I] := MaxValue;
  end;
  // TD-25881 Calculate efficiency using global function
  // TD-25881.50 Do not do this for when clicking in graph!
  if not ProductionData.AClicked then
  begin
//    HomeF.ActionEfficiency(ADrawObject); // TD-25881
//    ProductionData.ATotalPercentage := ADrawObject.AEfficiency; // TD-255881
  end;
// TESTING
//ShowMessage(TestLine);
  // Show Efficiency for the last moment in time
  if UpdateChart then
    lblEfficiency.Caption :=
      MyFormatDateTime(Now) + ' ' +
        FormatPercentage(ProductionData.ATotalPercentage)
  else
  begin  // Click-In-Chart-handling
    if PQCount > 1 then
      lblEfficiency.Caption :=
        MyFormatDateTime(DateFrom) + ' ' +
          FormatPercentage(TotalEff / PQCount)
    else
      lblEfficiency.Caption :=
        MyFormatDateTime(DateFrom) + ' ' +
          FormatPercentage(ProductionData.ATotalPercentage);
  end;
  // Show total production quantity
  lblTotalProd.Caption := Format('%d', [Round(TotalQuantity)]);
end; // ActionProductionGraph

procedure TDialogEmpGraphF.dxWorkspotListAdd(var dxList: TdxTreeList; AMachine,
  AWorkspot, AJob: String);
var
  Item: TdxTreeListNode;
begin
  // TODO: A check must be done for unique rows!
  if dxList <> nil then
  begin
    Item := dxList.Add;
    Item.Values[dxListColumnMACHINE.Index]  := AMachine;
    Item.Values[dxListColumnWORKSPOT.Index] := AWorkspot;
    Item.Values[dxListColumnJOB.Index]      := AJob;
  end;
end;

procedure TDialogEmpGraphF.WorkspotListClear;
var
  I: Integer;
  APTWorkspot: PTWorkspot;
begin
  for I := WorkspotList.Count - 1 downto 0 do
  begin
    APTWorkspot := WorkspotList.Items[I];
    Dispose(APTWorkspot);
    WorkspotList.Remove(APTWorkspot);
  end;
  WorkspotList.Clear;
end;

function TDialogEmpGraphF.WorkspotListAdd(AMachineCode, AWorkspotCode,
  AJobCode: String): Boolean;
var
  I: Integer;
  APTWorkspot: PTWorkspot;
begin
  Result := True;
  for I := 0 to WorkspotList.Count - 1 do
  begin
    APTWorkspot := WorkspotList.Items[I];
    if (APTWorkspot.AMachineCode = AMachineCode) and
      (APTWorkspot.AWorkspotCode = AWorkspotCode) and
      (APTWorkspot.AJobCode = AJobCode) then
    begin
      Result := False;
      Break;
    end;
  end;
  if Result then
  begin
    new(APTWorkspot);
    APTWorkspot.AMachineCode := AMachineCode;
    APTWorkspot.AWorkspotCode := AWorkspotCode;
    APTWorkspot.AJobCode := AJobCode;
    WorkspotList.Add(APTWorkspot);
  end;
end;

procedure TDialogEmpGraphF.TimerAutoCloseTimer(Sender: TObject);
begin
  inherited;
  TimerAutoClose.Enabled := False;
  Close;
end;

end.


