(*
  MRA:28-MAY-2013 20014289 New look Pims
  - Some pictures/colors are changed.
  MRA:23-JAN-2014 TD-24046
  - Translate issues: Use a different method to translate.
*)
unit BaseDialogFRM;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseTopLogoFRM, StdCtrls, Buttons, dxCntner, Menus, StdActns,
  ActnList, ImgList, ComCtrls, ExtCtrls, UPimsConst, jpeg;

type
  TBaseDialogForm = class(TBaseTopLogoForm)
    pnlBottom: TPanel;
    btnOKSmall: TBitBtn;
    btnCancelSmall: TBitBtn;
    btnOK: TBitBtn;
    btnCancel: TBitBtn;
    procedure pnlBottomResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  BaseDialogForm: TBaseDialogForm;

implementation

{$R *.DFM}

(*
{$IFDEF PIMSDUTCH}
{$R *NLD.DFM}
{$ELSE}
  {$IFDEF PIMSGERMAN}
  {$R *DEU.DFM}
  {$ELSE}
    {$IFDEF PIMSFRENCH}
    {$R *FRA.DFM}
    {$ELSE}
    {$R *.DFM}
    {$ENDIF}
  {$ENDIF}
{$ENDIF}
*)

procedure TBaseDialogForm.pnlBottomResize(Sender: TObject);
begin
  inherited;
  // centre buttons reset color otherwise XP sets color
  (Sender as TPanel).ParentBackground := False;
  (Sender as TPanel).ParentColor      := False;
  if btnCancel.Visible then
  begin
    btnOk.Left     := (Sender as TPanel).Width div 2 - btnOk.Width - 4;
    btnCancel.Left := (Sender as TPanel).Width div 2 + 4;
  end
  else
  begin
    btnOK.Left := Round(pnlBottom.Width / 2 - btnOK.Width / 2);
  end;
end;

procedure TBaseDialogForm.FormCreate(Sender: TObject);
begin
  inherited;
//  pnlBottom.Color := pnlImageBase.Color; // 20014289

  btnOK.Color := clDarkRed; // clPimsBlue; // 20014450.50 // PIM-250
  btnCancel.Color := clDarkRed; // clPimsBlue; // 20014450.50 // PIM-250
  // 20014450.50
(*
{$IFDEF PRODUCTIONSCREEN}
  pnlBottom.Height := 41;
  btnOk.Height := 25;
  btnCancel.Height := 25;
  btnOK.Glyph.Assign(btnOKSmall.Glyph);
  btnCancel.Glyph.Assign(btnCancelSmall.Glyph);
{$ENDIF}
*)
end;

procedure TBaseDialogForm.FormShow(Sender: TObject);
begin
  inherited;
  pnlBottomResize(pnlBottom);
end;

end.
