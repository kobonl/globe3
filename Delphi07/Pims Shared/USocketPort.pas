(*
  SocketPort:
  - Use this class to read machine-counters from blackboxes using
    Socket-method. Each counter is identified by:
    - Host (IP-Address of Adam-4570)
    - Port (Port of Adam-4570, it has 2 ports).
    - ModuleAddress (ModuleAddress that connects to Adam-4080)
    - CounterNumber (The counter to read (can be 1 or 2).
  - Usage for main-application:
    - Include this unit
    - Add a TIdTelnet-component on the main-form (part of Indy-components)
    - Create an object of the SocketPort-class, like:
      - ASocketPort (of type: TSocketPort)
      - ASocketPort := TSocketPort.Create(IdTelNet) (parameter is name of
        the TIdTelnet-component.
  MRA:11-JUN-2012
  - When reading the machine-counter:
  - Made changes for 'IdTelnetDataAvailable':
    - Only accept the input when CR is read.
    - Check for '>' and CR.
    - Add buffer to ReadBuffer, because the input can come in parts.
  MRA:12-JUN-2012
  - Added debug-boolean. When True, show extra info.
  MRA:21-JUN-2012 20012858.70 Rework
  - When a blackbox is not available, which can happen when the Adam-device
    is turned off, or when IP-address is wrong or something else,
    then disable this blackbox, so it will be skipped.
    This prevents the program freezes.
  MRA:28-AUG-2012 TODO 21191
  - Prevent it is reading a wrong counter-value.
  - Use an ID to know which counter should be read: ReadIDIn, ReadIDOut
  - Use boolean to know if it should read or not.
  - Some functions that read the counter must check also if the port can
    be opened, if not then return false.
  - REMARK: When another application is reading the same counters, it
            can result in wrong values; IdTelnetDataAvailable will then result
            in values from this other application, even when it is not
            triggered here.
*)
unit USocketPort;

interface

uses
  IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient, IdTelnet,
  IdException, SysUtils;

type
  TMyLog = procedure(AMsg: String);

type
  TSocketPort = class
  private
    FIdTelnet: TIdTelnet;
    FPort: Integer;
    FHost: String;
    FModuleAddress: Integer;
    FCounterNumber: Integer;
    FReadBuffer: String;
    FCounterValue: Integer;
    FCounter1Read: Boolean;
    FCounter0Read: Boolean;
    FKeepPortOpen: Boolean;
    FReadDelay: Integer;
    FMyLog: TMyLog;
    FDebug: Boolean;
    FMyReadCounterSwitch: Boolean;
    FReadIDIn: Integer;
    FReadIDOut: Integer;
    function GetModuleAddress: Integer;
    function GetOpen: Boolean;
    procedure SetModuleAddress(const Value: Integer);
    procedure SetOpen(const Value: Boolean);
    function SendCommand(ACommand: String): Boolean;
    function SendStringWithCR(const AInput: String): Boolean;
    procedure IdTelnetDataAvailable(Sender: TIdTelnet;
      const Buffer: String);
    procedure SetHost(const Value: String);
    procedure SetPort(const Value: Integer);
  public
    constructor Create; overload;
    constructor Create(AIdTelnet: TIdTelnet); overload;
    destructor Destroy; override;
    procedure Wait(const MSecsToWait : Integer);
    function PortConnect(AHost: String; APort: Integer;
      AModuleAddress: Integer): Boolean;
    procedure PortDisconnect;
    function GetAddress: String;
    function ReadCounterorFrequencyValue0: Boolean;
    function ReadCounterorFrequencyValue1: Boolean;
    procedure ReadDigitalOutputandAlarmStatus;
    procedure SetDigitalOutputValueHi0;
    procedure SetDigitalOutputValueHi1;
    procedure SetDigitalOutputValueLo0;
    procedure SetDigitalOutputValueLo1;
    procedure ResetDigitalOutputValue;
    procedure ClearCounter0;
    procedure ClearCounter1;
    function PortInfo: String;
    procedure WMyErrorLog(AMsg: String);
    property IdTelnet: TIdTelnet read FIdTelnet write FIdTelnet;
    property Host: String read FHost write SetHost;
    property Port: Integer read FPort write SetPort;
    property ModuleAddress: Integer read GetModuleAddress write SetModuleAddress;
    property CounterNumber: Integer read FCounterNumber write FCounterNumber;
    property ReadBuffer: String read FReadBuffer write FReadBuffer;
    property Open: Boolean read GetOpen write SetOpen;
    property CounterValue: Integer read FCounterValue write FCounterValue;
    property Counter0Read: Boolean read FCounter0Read write FCounter0Read;
    property Counter1Read: Boolean read FCounter1Read write FCounter1Read;
    property KeepPortOpen: Boolean read FKeepPortOpen write FKeepPortOpen;
    property ReadDelay: Integer read FReadDelay write FReadDelay;
    property MyLog: TMyLog read FMyLog write FMyLog;
    property Debug: Boolean read FDebug write FDebug;
    property MyReadCounterSwitch: Boolean read FMyReadCounterSwitch
      write FMyReadCounterSwitch;
    property ReadIDIn: Integer read FReadIDIn write FReadIDIn;
    property ReadIDOut: Integer read FReadIDOut write FReadIDOut;
end;

var
  DigitalOutHi0: Boolean = False;
  DigitalOutHi1: Boolean= False;

implementation

{ TSocketPort }

procedure TSocketPort.ClearCounter0;
begin
  SendStringWithCR('$' + GetAddress + '60');
end;

procedure TSocketPort.ClearCounter1;
begin
  SendStringWithCR('$' + GetAddress + '61');
end;

constructor TSocketPort.Create;
begin
  inherited;
  KeepPortOpen := True; // ???
  ReadDelay := 10;
  MyLog := nil;
  Debug := False;
  ReadIDIn := 0;
  ReadIDOut := 0;
  MyReadCounterSwitch := False;
end;

constructor TSocketPort.Create(AIdTelnet: TIdTelnet);
begin
  Create;
  IdTelnet := AIdTelnet;
  // Be sure to initialise lasthost and lastport, otherwise
  // Keep-port-open can go wrong!
  IdTelnet.Host := '';
  IdTelnet.Port := 0;
//  IdTelnet.ReadTimeout := 5000; // in ms. This does not help...
  IdTelnet.OnDataAvailable := IdTelnetDataAvailable;
end;

destructor TSocketPort.Destroy;
begin
//
  inherited;
end;

function TSocketPort.GetAddress: String;
begin
  Result := Format('%2.2X', [IdTelnet.Tag]);
end;

function TSocketPort.GetModuleAddress: Integer;
begin
  Result := IdTelnet.Tag;
end;

function TSocketPort.GetOpen: Boolean;
begin
  Result := IdTelnet.Connected;
end;

// MRA:11-JUN-2012
// Changes:
// - Only accept the input when CR is read.
// - Check for '>' and CR.
// - Add buffer to ReadBuffer, because the input can come in parts.
procedure TSocketPort.IdTelnetDataAvailable(Sender: TIdTelnet;
  const Buffer: String);
{This routine comes directly from the ICS TNDEMO code. Thanks to
Francois Piette
It updates the memo control when we get data}
const
  CR = #13;
  LF = #10;
  EXPECTEDLEN = 10; // Length of returnvalue + CR // TODO 21191
var
  Start, Stop: Integer;
  CRDetected: Boolean;
  ReadOK: Boolean;
  function HexToInt(HexStr: String): Integer;
  begin
    Result := StrToInt('$' + HexStr);
  end;
begin
  if not MyReadCounterSwitch then
    Exit;
if Debug then
  WMyErrorLog('IdTelnetDataAvailable - START');
//  if Memo1.Lines.Count = 0 then
//    Memo1.Lines.Add('');
if Debug then
  WMyErrorLog('Buffer=[' + Buffer + ']');
  Start := 1;
  CRDetected := False;
  Stop  := Pos(CR, Buffer);
  if Stop = 0 then
    Stop := Length(Buffer) + 1;
  while Start <= Length(Buffer) do
  begin
//    Memo1.Lines.Strings[Memo1.Lines.Count - 1] :=
//      Memo1.Lines.Strings[Memo1.Lines.Count - 1] +
//      Copy(Buffer, Start, Stop - Start);
    if Buffer[Stop] = CR then
    begin
      CRDetected := True;
if Debug then
  WMyErrorLog('CR detected');
//      Memo1.Lines.Add('');
//      {$IFNDEF Linux}
//      SendMessage(Memo1.Handle, WM_KEYDOWN, VK_UP, 1);
//      {$ENDIF}
    end;
    Start := Stop + 1;
    if Start > Length(Buffer) then
      Break;
    if Buffer[Start] = LF then
      Start := Start + 1;
    Stop := Start;
    while (Buffer[Stop] <> CR) and (Stop <= Length(Buffer)) do
      Stop := Stop + 1;
  end;
  // Add the buffer to readbuffer, because it can come in parts.
  ReadBuffer := ReadBuffer + Buffer;
  // Handle the blackbox-counter that is received.
  if ReadBuffer <> '' then
  begin
    // TODO 21191 Also test on length
    if (ReadBuffer[1] = '>') and (Length(ReadBuffer) = EXPECTEDLEN)
      and CRDetected then
    begin
if Debug then
  WMyErrorLog('ReadBuffer=[' + ReadBuffer + ']' +
    ' Len=' + IntToStr(Length(ReadBuffer)));
      // Sometimes there can be a $ in the result. This must be ignored.
      if not (Pos('$', ReadBuffer) > 0) then
      begin
        // When there is a read-error, do not return 0 for countervalue,
        // but try again next time, or it will result in wrong counter-values!
        ReadOK := True;
        try
          // Remove CR at end of buffer
          ReadBuffer := Copy(ReadBuffer, 1, Pos(CR, ReadBuffer) - 1);
          CounterValue := HexToInt(Copy(ReadBuffer, 2, Length(ReadBuffer)));
        except
          on E: EAccessViolation do
          begin
            WMyErrorLog(E.Message);
            ReadBuffer := '';
            ReadOK := False;
          end;
          on E: EIdSocketError do
          begin
            WMyErrorLog(E.Message);
            ReadBuffer := '';
            ReadOK := False;
          end;
          on E: Exception do
          begin
            WMyErrorLog(E.Message);
            ReadBuffer := '';
            ReadOK := False;
          end;
        end; // try
        if ReadOK then
        begin
          case CounterNumber of
          1:
            begin
              Counter0Read := True;
              ReadIDOut := ReadIDIn;
            end;
          2:
            begin
              Counter1Read := True;
              ReadIDOut := ReadIDIn;
            end;
          end; // case
        end; // if
      end; // if
    end; // if
  end; // if
if Debug then
  WMyErrorLog('IdTelnetDataAvailable - END');
end; // IdTelnetDataAvailable

(*
procedure TSocketPort.IdTelnetDataAvailable(Sender: TIdTelnet;
  const Buffer: String);
{This routine comes directly from the ICS TNDEMO code. Thanks to
Francois Piette
It updates the memo control when we get data}
const
  CR = #13;
  LF = #10;
var
  Start, Stop: Integer;
  ReadOK: Boolean;
  function HexToInt(HexStr: String): Integer;
  begin
    Result := StrToInt('$' + HexStr);
  end;
begin
//  if Memo1.Lines.Count = 0 then
//    Memo1.Lines.Add('');
  Start := 1;
  Stop  := Pos(CR, Buffer);
  if Stop = 0 then
    Stop := Length(Buffer) + 1;
  while Start <= Length(Buffer) do
  begin
//    Memo1.Lines.Strings[Memo1.Lines.Count - 1] :=
//      Memo1.Lines.Strings[Memo1.Lines.Count - 1] +
//      Copy(Buffer, Start, Stop - Start);
    if Buffer[Stop] = CR then
    begin
//      Memo1.Lines.Add('');
//      {$IFNDEF Linux}
//      SendMessage(Memo1.Handle, WM_KEYDOWN, VK_UP, 1);
//      {$ENDIF}
    end;
    Start := Stop + 1;
    if Start > Length(Buffer) then
      Break;
    if Buffer[Start] = LF then
      Start := Start + 1;
    Stop := Start;
    while (Buffer[Stop] <> CR) and (Stop <= Length(Buffer)) do
      Stop := Stop + 1;
  end;
  ReadBuffer := Buffer;
  // Handle the blackbox-counter that is received.
  if ReadBuffer <> '' then
  begin
    if ReadBuffer[1] = '>' then
    begin
      // Sometimes there can be a $ in the result. This must be ignored.
      if not (Pos('$', ReadBuffer) > 0) then
      begin
        // When there is a read-error, do not return 0 for countervalue,
        // but try again next time, or it will result in wrong counter-values!
        ReadOK := True;
        try
          // Remove CR at end of buffer
          ReadBuffer := Copy(ReadBuffer, 1, Pos(CR, ReadBuffer) - 1);
          CounterValue := HexToInt(Copy(ReadBuffer, 2, Length(ReadBuffer)));
        except
          on E: EIdSocketError do
          begin
            WMyErrorLog(E.Message);
            ReadOK := False;
          end;
          on E: Exception do
          begin
            WMyErrorLog(E.Message);
            ReadOK := False;
          end;
        end; // try
        if ReadOK then
        begin
          case CounterNumber of
          1:
            begin
              Counter0Read := True;
            end;
          2:
            begin
              Counter1Read := True;
            end;
          end; // case
        end; // if
      end; // if
    end; // if
  end; // if
end;
*)

function TSocketPort.ReadCounterorFrequencyValue0: Boolean;
begin
  ReadBuffer := '';
//  CRTrig := ApdPort.AddDataTrigger(String(Chr(13)), False);
  Result := SendStringWithCR('#' + GetAddress + '0');
end;

function TSocketPort.ReadCounterorFrequencyValue1: Boolean;
begin
  ReadBuffer := '';
//  CRTrig := ApdPort.AddDataTrigger(String(Chr(13)), False);
  Result := SendStringWithCR('#' + GetAddress + '1');
end;

procedure TSocketPort.ReadDigitalOutputandAlarmStatus;
begin
  ReadBuffer := '';
//  CRTrig := ApdPort.AddDataTrigger(String(Chr(13)), False);
  SendStringWithCR('@' + GetAddress + 'DI');
end;

procedure TSocketPort.ResetDigitalOutputValue;
begin
  SendStringWithCR('@' + GetAddress + 'DO00');// both off
  DigitalOutHi0 := False;
  DigitalOutHi1 := False;
end;

function TSocketPort.SendStringWithCR(const AInput: String): Boolean;
begin
//  ApdPort.PutString(AInput + #13);
  Result := SendCommand(AInput);
end;

procedure TSocketPort.SetDigitalOutputValueHi0;
begin
  if DigitalOutHi1 then
    SendStringWithCR('@' + GetAddress + 'DO03') // both on
  else
    SendStringWithCR('@' + GetAddress + 'DO01');// only bit 0 on
  DigitalOutHi0 := True;
end;

procedure TSocketPort.SetDigitalOutputValueHi1;
begin
  if DigitalOutHi0 then
    SendStringWithCR('@' + GetAddress + 'DO03') // both on
  else
    SendStringWithCR('@' + GetAddress + 'DO02');// only bit 1 on
  DigitalOutHi1 := True;
end;

procedure TSocketPort.SetDigitalOutputValueLo0;
begin
  if DigitalOutHi1 then
    SendStringWithCR('@' + GetAddress + 'DO02') // (keep)only bit 1 on
  else
    SendStringWithCR('@' + GetAddress + 'DO00');// both off
  DigitalOutHi0 := False;
end;

procedure TSocketPort.SetDigitalOutputValueLo1;
begin
  if DigitalOutHi0 then
    SendStringWithCR('@' + GetAddress + 'DO01') // (keep)only bit 0 on
  else
    SendStringWithCR('@' + GetAddress + 'DO00');// both off
   DigitalOutHi1 := False;
end;

procedure TSocketPort.SetHost(const Value: String);
begin
  FHost := Value;
  // Do not assign this here! But only before opening the port!
//  IdTelnet.Host := Value;
end;

procedure TSocketPort.SetPort(const Value: Integer);
begin
  FPort := Value;
  // Do not assign this here! But only before opening the port!
//  IdTelnet.Port := Value;
end;

procedure TSocketPort.SetModuleAddress(const Value: Integer);
begin
  IdTelnet.Tag := Value;
  FModuleAddress := Value;
end;

procedure TSocketPort.SetOpen(const Value: Boolean);
begin
//  ApdPort.Open := Value;
end;

procedure TSocketPort.Wait(const MSecsToWait: Integer);
var
  TimeToWait,
  TimeToSendRequest : TDateTime;
begin
  TimeToWait   := MSecsToWait / MSecsPerDay;
  TimeToSendRequest := TimeToWait + SysUtils.Now;
  while SysUtils.Now < TimeToSendRequest do
    Sleep(0);
end; // Wait

function TSocketPort.SendCommand(ACommand: String): Boolean;
var
  i : integer;
  s : string;
begin
  Result := False;
  // It is possible because of a time-out it is not connected anymore.
  try
    if not IdTelnet.Connected then
      IdTelnet.Connect;
    if IdTelnet.Connected then
    begin
      s := ACommand;
      for i := 1 to length(s) do
        IdTelnet.SendCh(s[i]);
      IdTelnet.SendCh(#13);
      Result := True;
    end;
  except
    // For now: Ignore error, only log it.
    // Note: When it could not connect, then perhaps the next time
    // it tries to read the counter, it can connect.
    // Otherwise???
    on E: EIdSocketError do
    begin
      WMyErrorLog(E.Message);
    end;
    on E: Exception do
    begin
      WMyErrorLog(E.Message);
    end;
  end;
end;

function TSocketPort.PortConnect(AHost: String; APort,
  AModuleAddress: Integer): Boolean;
  procedure OpenPort;
  begin
    try
      IdTelnet.Host := AHost;
      IdTelnet.Port := APort;
      if not IdTelnet.Connected then
        IdTelnet.Connect;
      Result := True;
    except
      on E: EIdSocketError do
      begin
        WMyErrorLog(E.Message);
      end;
      on E: Exception do
      begin
        WMyErrorLog(E.Message);
      end;
    end;
  end;
  procedure ClosePort;
  begin
    try
      PortDisconnect;
    except
      on E:EAccessViolation do
      begin
        WMyErrorLog(E.Message);
      end;
      on E: EIdSocketError do
      begin
        WMyErrorLog(E.Message);
      end;
      on E: Exception do
      begin
        WMyErrorLog(E.Message);
      end;
    end;
  end;
begin
  Result := False;
  try
    try
      // Multi
      // List of SocketPorts is used -> Ports can all be opened.
      // No need to close them in-between. Only at the end.
      OpenPort;
(*
      if KeepPortOpen then
      begin
       if not ((IdTelnet.Host = AHost) and
         (IdTelnet.Port = APort)) then
          ClosePort
      end
      else
        ClosePort;
      if KeepPortOpen then
      begin
        if not ((IdTelnet.Host = AHost) and
          (IdTelnet.Port = APort)) then
          OpenPort
        else // Be sure the port is open! Try it several times!
          if not IdTelnet.Connected then
          begin
            OpenPort;
{            if not IdTelnet.Connected then
            begin
              OpenPort;
            end;
            if not IdTelnet.Connected then
            begin
              OpenPort;
            end; }
          end;
      end
      else
        OpenPort;
*)
    except
      on E: EIdSocketError do
      begin
        WMyErrorLog(E.Message);
      end;
      on E: Exception do
      begin
        WMyErrorLog(E.Message);
      end;
    end;
  finally
    ModuleAddress := AModuleAddress;
    Result := IdTelnet.Connected;
  end;
end; // PortConnect

procedure TSocketPort.PortDisconnect;
begin
  try
    try
      if IdTelnet.Host <> '' then
        if IdTelnet.Connected then
        begin
          IdTelnet.InputBuffer.Clear;
          IdTelnet.Disconnect;
        end;
    except
      on E:EAccessViolation do
      begin
        WMyErrorLog(E.Message);
      end;
      on E: EIdClosedSocket do
      begin
        WMyErrorLog(E.Message);
      end;
      on E: EIdSocketError do
      begin
        WMyErrorLog(E.Message);
      end;
      on E: Exception do
      begin
        WMyErrorLog(E.Message);
      end;
    end;
  finally
    Wait(ReadDelay);
  end;
end; // PortDisconnect

// Return info about the current port
function TSocketPort.PortInfo: String;
begin
  Result := Host + '/' + IntToStr(Port) + '/' +
    IntToStr(ModuleAddress) + '/' + IntToStr(CounterNumber);
end; // PortInfo

procedure TSocketPort.WMyErrorLog(AMsg: String);
begin
  if Assigned(MyLog) then
    MyLog(AMsg);
end;

end.

