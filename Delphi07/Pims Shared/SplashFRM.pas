unit SplashFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BasePimsFRM, dxfProgressBar, StdCtrls, ExtCtrls;

type
  TSplashForm = class(TBasePimsForm)
    pnlBack: TPanel;
    imgSplash: TImage;
    lblInfo: TLabel;
    dxfProgressBar: TdxfProgressBar;
  private
    { Private declarations }
  public
    { Public declarations }
    procedure UpdateProgressBar(Sender: TObject);
    procedure UpdateProgressBarOnly;
  end;

var
  SplashForm: TSplashForm;
  Starting  : Boolean;

implementation

{$IFDEF PIMSDUTCH}
{$R *NLD.DFM}
{$ELSE}
  {$IFDEF PIMSGERMAN}
  {$R *DEU.DFM}
  {$ELSE}
    {$IFDEF PIMSFRENCH}
    {$R *FRA.DFM}
    {$ELSE}
    {$R *.DFM}
    {$ENDIF}
  {$ENDIF}
{$ENDIF}

procedure TSplashForm.UpdateProgressBar(Sender: TObject);
begin
  dxfProgressBar.StepIt;
(*
  if Sender is TForm then
    lblInfo.Caption := 'Creating ' + (Sender as TForm).Caption
  else
    lblInfo.Caption := 'Creating ' + (Sender as TComponent).Name;
*)
  Update;
end;

procedure TSplashForm.UpdateProgressBarOnly;
begin
  dxfProgressBar.StepIt;
  Update;
end;

initialization
  Starting := False;

end.
