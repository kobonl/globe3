(*
  Changes:
    MRA:25-04-2008 R006 (Revision 006). V2.0.139.10.
      Employee with timeblocks-per-employee were not shown in Productionscreen
      and list 'show employees not scanned in'.
      Reason: cdsTimeBlockPerEmployee had a wrong index-key set in the
      component.
      It was: EMPLOYEE_NUMBER;PLANT_CODE;SHIFT_NUMBER;TIMEBLOCK_NUMBER
      But it must be: PLANT_CODE;SHIFT_NUMBER;TIMEBLOCK_NUMBER;EMPLOYEE_NUMBER
      Because of this, the 'findkey' never gave a result.
    MRA:02-JAN-2009 RV015 (Revision 015). V2.0.139.14.
      There was a problem with blinking puppets with no reason.
      Cause: Some properties were not filled about 'scanned' and 'jobcode',
      which resulted in a wrong comparison for 'blinking-puppets'.
    MRA:29-JAN-2009 RV017. Revision 017.
      Because timeblocks can end in a following day, the day-part
      must be used when storing and comparing STARTDATE and ENDDATE.
      Otherwise - for example - planning info is wrong for night-shifts.
      This means the STARTDATE and ENDDATE must be frac'd before showing
      it in a list!
    MRA:02-FEB-2009 RV018. Revision 018.
      Timeblocks can exist that start in current day and end in next day.
      For this, determination of things like planning and availability must
      be changed.
      1. EmployeePlanning: Records that range over 2 days must be read. But
         result must put in a client-dataset, that has no
         double employee-records.
      2. Employee/Standard Availability: same as above.
      3. Breaks are added to the Timeblocks, to prevent the program shows
         no-one planned if the current time is within a break.
      Also changed:
      1. Use Employee.Enddate to see if employee's data must be excluded from
         queries like EmployeePlanning, Employee Availability and Standard
         Availability. For TimeRegScanning it is not needed, that depends on
         the scans of an employee; if the employee is not available, there
         also will be no scannings.
      2. Problem solved with Show-employees-absence-without reason.
         This gave wrong result because of wrong if-found logic.
    MRA:11-FEB-2009 RV019. Revision 019.
      Bugfix for determination of planning + night-shift. During a day-shift,
      it also used planning of a previous day and showed it as current planning.
    MRA:2-APR-2010. RV057.2. REI.
    - Does not show 'planned' for night-shift on Sunday-evening.
    - Example: Employee=26, Sunday, 28-MAR-2010.
    MRA:18-JUN-2012 TD-20502 Bugfix. (VEN)
    - Production screen is not showing employees not scanned in.
    - Probable cause: During determination of 'StandAvail' it uses
      a DayNrFrom and DayNrTo, where DayNrFrom is the previous daynr, but
      when current daynr is 1 (Monday) then previous daynr will be 7.
    - NOTE: There was an error reported about an employee who
            was planned but not scanned in, and this employee did not
            appear at 'Show employees not scanned in'. However: This list
            will show only employees who are not scanned and not planned,
            but are available.
    MRA:17-SEP-2012 SO-20013563 Head count
    - Addition of head count
    - For 'head count' a list of employees is needed with conditions:
      - Only for the shown workspots in the current scheme
      - Only employees who are currently scanned in
      - The department (description) linked to the workspot
        is needed to show and to sort on
    - It must result in a displayed table of departments with the number
      of employees currently scanned in per department.
  MRA:15-NOV-2012 TD-21210
  - Problem-solving for colors yellow and orange.
    - It did not take into account how many employees were really planned.
      - Example: When only 1 employee is planned on a workspot, then it
                 must show all additional employees in color red, not
                 in yellow or orange.
    MRA:8-FEB-2013 SO-20013910. Plan on departments issue
    - Employees who are planned on departments handling added:
      - Also look for employees planned on the department linked to the
        workspot.
  MRA:11-FEB-2013 TD-22082 Perfomance problems
  - Use a ClientDataSet instead of a TList for WorkspotsPerEmployee.
    - Reason: When this list is too big, it takes too much time to
              search something.
  - Added Init-property, to see if the lists are initialized or not.
  - Filter on teams-per-user to limit the number of employees read.
  - Addition of 'LogChanges := False' for ClientDataSets.
  - Addition of 'Application.ProcessMessages' in while-statements.
  MRA:11-JUL-2014 TD-25322
  - It gives sometimes an error about 'variant' when loading certain schemes.
  - Cause: When TEAM_CODE was empty it gave this error, to solve it the
    type must be used for fields (e.g. .AsString instead of .Value), see also
    comment below where it is changed.
  MRA:14-APR-2015 SO-20016449
  - Time Zone Implementation
  - Use SysUtils.FormatDateTime to prevent it uses the time-zone-versions
    defined in ORASystemDMT.
  MRA:4-JUN-2015 20014450.50 Part 2
  - Real Time Efficiency
  - Show Curr + Shift Efficiency in employee-overview
  MRA:25-NOV-2016 PIM-237
  - ProductionScreen and Employees too late result can be wrong.
  - Reason: It compared with current planned time-block. It must compare
    with first planned time-block.
  MRA:28-NOV-2016 PIM-237
  - It compares with current open scan to determine too late! Instead of that
    it must compare with the first scan of the current shift-date + shift!
  MRA:9-MAR-2017 PIM-275
  - Planning at Department is shown wrong at Production Screen
  MRA:24-AUG-2018 GLOB3-60
  - Extension 4 to 10 blocks for planning
  MRA:26-AUG-2018 PIM-237
  - ProductionScreen and Employees too late result can be wrong
  - Note: Because of a change made for PIM-275, it does not fill workspotcode
    when checking for Too Late. Because of that the check for too late goes
    wrong.
*)

unit PlanScanEmpDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ORASystemDMT, Db, Provider, DBClient, Oracle, OracleData, Variants,
  UScannedIDCard, UPimsConst;

const
  QUOTE = #39{'};

const
  clMyOrange   = $0073B4FF;
  clMyYellow   = $0000FFFF;
  clMyPink     = $00EFCFFF;
  clYellowDark = $0000D7CC;

type
  TShowMode = (
    ShowEmployeesOnWorkspot,
    ShowAllEmployees,
    ShowEmployeesOnWrongWorkspot,
    ShowEmployeesNotScannedIn,
    ShowEmployeesAbsentWithReason,
    ShowEmployeesAbsentWithoutReason,
    ShowEmployeesWithFirstAid,
    ShowEmployeesTooLate,
    ShowHeadCount // SO-20013563
  );

// TD-22082
{
// Workspot per employee
type
  PTWorkspotsPerEMP = ^TWorkspotsPerEMP;
  TWorkspotsPerEMP = record
    PlantCode: String;
    DepartmentCode: String; // 20013910
    WorkspotCode: String;
    EmployeeNumber: Integer;
    StartdateLevel: TDateTime;
    EmployeeLevel: String;
  end;
}

// Employee, for result-set of employee-list
type
  PTEmployee = ^TEmployee;
  TEmployee = record
    AEmployeeNumber: Integer;
    AEmployeeName: String;
    AEmployeeShortName: String;
    ATeamCode: String;
    AScanned: Boolean;
    APlantCode: String;
    APlanWorkspotCode: String;
    APlanDepartmentCode: String; // 20013910 Also store plan-dept.
    AScanWorkspotCode: String;
    AJobCode: String;
    AColor: TColor;
    AShiftNumber: Integer;
    APlanStartDate: TDateTime;
    APlanEndDate: TDateTime;
    AScanDatetimeIn: TDateTime;
    AScanDatetimeOut: TDateTime;
    AStandAvailStartDate: TDateTime;
    AStandAvailEndDate: TDateTime;
    AEmpAvailStartDate: TDateTime;
    AEmpAvailEndDate: TDateTime;
    AFirstAidYN: String;
    AFirstAidExpDate: TDateTime;
    APlanLevel: String;
    AAbsenceReasonCode: String;
    AAbsenceReasonDescription: String;
    AEmpLevel: String;
    AKeep: Boolean;
    ACurrEff: Integer;  // 20014450.50 rounded result of current eff
    AShiftEff: Integer; // 20014450.50 rounded result of shift eff (total eff)
    AFirstPlanStartDate: TDateTime; // PIM-237 To store first-planned-timeblock-start-date-time
    AFirstScan: TDateTime; // PIM-237 To store the first scan of the shift-date+shift.
  end;

type
  TPlanScanClass = class(TObject)
  private
    FInit: Boolean; // TD-22082
//    procedure ClearWorkspotsPerEMPList; // TD-22082
    procedure ClearEmployeeList;
  public
//    WorkspotsPerEMPList: TList; // TD-22082
    EmployeeList: TList;
    constructor Create;
    destructor Destroy; override;
    procedure OpenTimeBlocks;
    function OpenEmployeePlanning(ADate: TDateTime): Boolean;
    function OpenTimeRegScanning(ADate: TDateTime): Boolean;
    function OpenStandAvail(ADate: TDateTIme): Boolean;
    function OpenEmployeeAvail(ADate: TDateTime): Boolean;
    procedure OpenOnlyStandAvail;
    procedure OpenOnlyEmpAvail;
    function OpenAbsenceReason: Boolean;
    function OpenEmployee: Boolean;
    function CreateOnlyStandAvailClientDataSet: Boolean;
{    function CreateWorkspotsPerEmployeeList(
      AStartdateLevel: TDateTime): Boolean; } // TD-22082
    function CreateOnlyEmpAvailEmployeesClientDataSetWithReason: Boolean;
    function CreateOnlyEmpAvailEmployeesClientDataSetWithoutReason: Boolean;
    function DeterminePlannedEmployeesPerWorkspot(PlantCode: String;
      WorkspotCode: String; CurrentDate: TDateTime): Boolean;
    function DetermineScannedEmployeesPerWorkspot(PlantCode: String;
      WorkspotCode: String; CurrentDate: TDateTime): Boolean;
    function DetermineScannedEmployeesPerPlant(PlantCode: String;
      CurrentDate: TDateTime): Boolean;
    function DetermineWorkspotsPerEmployeeLevel(PlantCode, DepartmentCode,
      WorkspotCode: String;
      EmployeeNumber: Integer): String; // 20013910
    function DetermineStandAvailEmployees(CurrentDate: TDateTime): Boolean;
    function DetermineEmployeeAvailEmployees(CurrentDate: TDateTime): Boolean;
    function DetermineEmployeeWSListMain(APlantCode, AWorkspotCode: String;
      Plan_Scan_Employees, All_Employees, Stand_Avail_Employees,
      Emp_Avail_Employees, Absent_With_Reason: Boolean): Boolean;
    function DetermineEmployeeWSListPLAN_SCAN_EMP(APlantCode,
      AWorkspotCode: String): Boolean;
    function DetermineEmployeeWSListALL_EMP(APlantCode,
      AWorkspotCode: String): Boolean;
    function DetermineEmployeeWSListSTAND_AVAIL_EMP(APlantCode,
    AWorkspotCode: String): Boolean;
    function DetermineEmployeeWSListEMP_AVAIL_EMP_REASON(APlantCode,
      AWorkspotCode: String): Boolean;
    function DetermineEmployeeWSListEMP_AVAIL_EMP_NO_REASON(APlantCode,
      AWorkspotCode: String): Boolean;
    function SearchEmployeePlanning(AEmployeeNumber: Integer): Boolean;
    function SearchTimeRegScanning(AEmployeeNumber: Integer): Boolean;
    function SearchStandAvail(AEmployeeNumber: Integer): Boolean;
    function SearchEmployeeAvail(AEmployeeNumber: Integer): Boolean;
    function SearchOnlyStandAvail(AEmployeeNumber: Integer): Boolean;
    function SearchOnlyEmpAvail(AEmployeeNumber: Integer): Boolean;
    function EmployeeListExists(AEmployeeNumber: Integer): Boolean;
    procedure OpenWorkspot; // 20013910
    function DetermineWorkspotDepartment(APlantCode,
      AWorkspotCode: String): String; // 20013910
    procedure OpenWorkspotsPerEmployee; // TD-22082
    procedure OpenTimeBlockPerEmployee; // TD-22082
    function FindEmployee(AEmployeeNumber: Integer): PTEmployee;
    property Init: Boolean read FInit write FInit; // TD-22082
  end;

type
  TPlanScanEmpDM = class(TDataModule)
    odsEmployeePlanning: TOracleDataSet;
    cdsEmployeePlanning: TClientDataSet;
    dspEmployeePlanning: TDataSetProvider;
    odsEmployeePlanningPLANLEVEL: TStringField;
    odsEmployeePlanningSTARTDATE: TDateTimeField;
    odsEmployeePlanningENDDATE: TDateTimeField;
    odsTimeBlockPerEmployee: TOracleDataSet;
    odsTimeBlockPerDepartment: TOracleDataSet;
    odsTimeBlockPerShift: TOracleDataSet;
    cdsTimeBlockPerEmployee: TClientDataSet;
    cdsTimeBlockPerDepartment: TClientDataSet;
    cdsTimeBlockPerShift: TClientDataSet;
    dspTimeBlockPerEmployee: TDataSetProvider;
    dspTimeBlockPerDepartment: TDataSetProvider;
    dspTimeBlockPerShift: TDataSetProvider;
    odsEmployeePlanningPLANT_CODE: TStringField;
    odsEmployeePlanningEMPLOYEEPLANNING_DATE: TDateTimeField;
    odsEmployeePlanningSHIFT_NUMBER: TIntegerField;
    odsEmployeePlanningDEPARTMENT_CODE: TStringField;
    odsEmployeePlanningWORKSPOT_CODE: TStringField;
    odsEmployeePlanningEMPLOYEE_NUMBER: TIntegerField;
    odsEmployeePlanningSHORT_NAME: TStringField;
    odsEmployeePlanningDESCRIPTION: TStringField;
    odsEmployeePlanningTEAM_CODE: TStringField;
    odsEmployeePlanningFIRSTAID_YN: TStringField;
    odsEmployeePlanningFIRSTAID_EXP_DATE: TDateTimeField;
    odsEmployeePlanningSCHEDULED_TIMEBLOCK_1: TStringField;
    odsEmployeePlanningSCHEDULED_TIMEBLOCK_2: TStringField;
    odsEmployeePlanningSCHEDULED_TIMEBLOCK_3: TStringField;
    odsEmployeePlanningSCHEDULED_TIMEBLOCK_4: TStringField;
    odsTimeRegScanning: TOracleDataSet;
    dspTimeRegScanning: TDataSetProvider;
    cdsTimeRegScanning: TClientDataSet;
    odsTimeRegScanningMARK: TBooleanField;
    odsTimeRegScanningPLANT_CODE: TStringField;
    odsTimeRegScanningWORKSPOT_CODE: TStringField;
    odsTimeRegScanningJOB_CODE: TStringField;
    odsTimeRegScanningSHIFT_NUMBER: TIntegerField;
    odsTimeRegScanningEMPLOYEE_NUMBER: TIntegerField;
    odsTimeRegScanningSHORT_NAME: TStringField;
    odsTimeRegScanningDESCRIPTION: TStringField;
    odsTimeRegScanningTEAM_CODE: TStringField;
    odsTimeRegScanningFIRSTAID_YN: TStringField;
    odsTimeRegScanningFIRSTAID_EXP_DATE: TDateTimeField;
    odsTimeRegScanningDATETIME_IN: TDateTimeField;
    odsTimeRegScanningDATETIME_OUT: TDateTimeField;
    odsStandAvail: TOracleDataSet;
    dspStandAvail: TDataSetProvider;
    cdsStandAvail: TClientDataSet;
    odsStandAvailSTARTDATE: TDateTimeField;
    odsStandAvailENDDATE: TDateTimeField;
    odsStandAvailPLANT_CODE: TStringField;
    odsStandAvailDAY_OF_WEEK: TIntegerField;
    odsStandAvailSHIFT_NUMBER: TIntegerField;
    odsStandAvailEMPLOYEE_NUMBER: TIntegerField;
    odsStandAvailSHORT_NAME: TStringField;
    odsStandAvailDESCRIPTION: TStringField;
    odsStandAvailDEPARTMENT_CODE: TStringField;
    odsStandAvailTEAM_CODE: TStringField;
    odsStandAvailFIRSTAID_YN: TStringField;
    odsStandAvailFIRSTAID_EXP_DATE: TDateTimeField;
    odsStandAvailAVAILABLE_TIMEBLOCK_1: TStringField;
    odsStandAvailAVAILABLE_TIMEBLOCK_2: TStringField;
    odsStandAvailAVAILABLE_TIMEBLOCK_3: TStringField;
    odsStandAvailAVAILABLE_TIMEBLOCK_4: TStringField;
    odsStandAvailMARK: TBooleanField;
    odsEmployeeAvail: TOracleDataSet;
    dspEmployeeAvail: TDataSetProvider;
    cdsEmployeeAvail: TClientDataSet;
    odsEmployeeAvailSTARTDATE: TDateTimeField;
    odsEmployeeAvailENDDATE: TDateTimeField;
    odsEmployeeAvailMARK: TBooleanField;
    odsEmployeeAvailPLANT_CODE: TStringField;
    odsEmployeeAvailEMPLOYEEAVAILABILITY_DATE: TDateTimeField;
    odsEmployeeAvailSHIFT_NUMBER: TIntegerField;
    odsEmployeeAvailEMPLOYEE_NUMBER: TIntegerField;
    odsEmployeeAvailSHORT_NAME: TStringField;
    odsEmployeeAvailDESCRIPTION: TStringField;
    odsEmployeeAvailDEPARTMENT_CODE: TStringField;
    odsEmployeeAvailTEAM_CODE: TStringField;
    odsEmployeeAvailFIRSTAID_YN: TStringField;
    odsEmployeeAvailFIRSTAID_EXP_DATE: TDateTimeField;
    odsEmployeeAvailAVAILABLE_TIMEBLOCK_1: TStringField;
    odsEmployeeAvailAVAILABLE_TIMEBLOCK_2: TStringField;
    odsEmployeeAvailAVAILABLE_TIMEBLOCK_3: TStringField;
    odsEmployeeAvailAVAILABLE_TIMEBLOCK_4: TStringField;
    odsAbsenceReason: TOracleDataSet;
    dspAbsenceReason: TDataSetProvider;
    cdsAbsenceReason: TClientDataSet;
    odsAbsenceReasonABSENCEREASON_CODE: TStringField;
    odsAbsenceReasonDESCRIPTION: TStringField;
    odsEmployeeAvailABSENCEREASONCODE: TStringField;
    odsEmployeeAvailABSENCEREASONDESCRIPTION: TStringField;
    odsEmployee: TOracleDataSet;
    dspEmployee: TDataSetProvider;
    cdsEmployee: TClientDataSet;
    odsEmployeeEMPLOYEE_NUMBER: TIntegerField;
    odsEmployeePLANT_CODE: TStringField;
    odsEmployeeSHORT_NAME: TStringField;
    odsEmployeeDESCRIPTION: TStringField;
    odsEmployeeTEAM_CODE: TStringField;
    cdsOnlyStandavail: TClientDataSet;
    cdsOnlyStandavailEMPLOYEENUMBER: TIntegerField;
    cdsOnlyStandavailEMPLOYEESHORTNAME: TStringField;
    cdsOnlyStandavailEMPLOYEENAME: TStringField;
    cdsOnlyStandavailTEAMCODE: TStringField;
    cdsOnlyStandavailPLANTCODE: TStringField;
    cdsOnlyStandavailWORKSPOTCODE: TStringField;
    cdsOnlyStandavailDEPARTMENTCODE: TStringField;
    cdsOnlyStandavailSHIFTNUMBER: TIntegerField;
    cdsOnlyStandavailFIRSTAIDYN: TStringField;
    cdsOnlyStandavailFIRSTAIDEXPDATE: TDateTimeField;
    cdsOnlyStandavailEMPLEVEL: TStringField;
    cdsOnlyStandavailSTARTDATE: TDateTimeField;
    cdsOnlyStandavailENDDATE: TDateTimeField;
    cdsOnlyStandavailMARK: TBooleanField;
    cdsOnlyEmpAvail: TClientDataSet;
    cdsOnlyEmpAvailEMPLOYEENUMBER: TIntegerField;
    cdsOnlyEmpAvailEMPLOYEESHORTNAME: TStringField;
    cdsOnlyEmpAvailEMPLOYEENAME: TStringField;
    cdsOnlyEmpAvailTEAMCODE: TStringField;
    cdsOnlyEmpAvailPLANTCODE: TStringField;
    cdsOnlyEmpAvailWORKSPOTCODE: TStringField;
    cdsOnlyEmpAvailDEPARTMENTCODE: TStringField;
    cdsOnlyEmpAvailSHIFTNUMBER: TIntegerField;
    cdsOnlyEmpAvailFIRSTAIDYN: TStringField;
    cdsOnlyEmpAvailFIRSTAIDEXPDATE: TDateTimeField;
    cdsOnlyEmpAvailEMPLEVEL: TStringField;
    cdsOnlyEmpAvailMARK: TBooleanField;
    cdsOnlyEmpAvailABSENCEREASONCODE: TStringField;
    cdsOnlyEmpAvailABSENCEREASONDESCRIPTION: TStringField;
    cdsOnlyEmpAvailPLANWORKSPOTCODE: TStringField;
    cdsOnlyEmpAvailPLANSTARTDATE: TDateTimeField;
    cdsOnlyEmpAvailPLANENDDATE: TDateTimeField;
    cdsOnlyEmpAvailPLANLEVEL: TStringField;
    cdsOnlyEmpAvailSTANDAVAILSTARTDATE: TDateTimeField;
    cdsOnlyEmpAvailSTANDAVAILENDDATE: TDateTimeField;
    cdsOnlyEmpAvailEMPAVAILSTARTDATE: TDateTimeField;
    cdsOnlyEmpAvailEMPAVAILENDDATE: TDateTimeField;
    oqCreateWorkspotsPerEmployeeListXXX: TOracleQuery;
    odsEmployeePlanningMARK: TBooleanField;
    odsWorkspot: TOracleDataSet;
    dspWorkspot: TDataSetProvider;
    cdsWorkspot: TClientDataSet;
    odsWorkspotPLANT_CODE: TStringField;
    odsWorkspotWORKSPOT_CODE: TStringField;
    odsWorkspotDEPARTMENT_CODE: TStringField;
    odsWorkspotsPerEmployee: TOracleDataSet;
    dsWorkspotsPerEmployee: TDataSetProvider;
    cdsWorkspotsPerEmployee: TClientDataSet;
    odsWorkspotsPerEmployeePLANT_CODE: TStringField;
    odsWorkspotsPerEmployeeDEPARTMENT_CODE: TStringField;
    odsWorkspotsPerEmployeeWORKSPOT_CODE: TStringField;
    odsWorkspotsPerEmployeeEMPLOYEE_NUMBER: TIntegerField;
    odsWorkspotsPerEmployeeSTARTDATE_LEVEL: TDateTimeField;
    odsWorkspotsPerEmployeeEMPLOYEE_LEVEL: TStringField;
    odsTimeRegScanningCURREFF: TFloatField;
    odsTimeRegScanningSHIFTEFF: TFloatField;
    odsEmployeePlanningFIRSTSTARTDATE: TDateTimeField;
    odsEmployeeAvailFIRSTSTARTDATE: TDateField;
    cdsOnlyEmpAvailFIRSTSTARTDATE: TDateField;
    odsTimeRegScanningFIRSTSCAN: TDateTimeField;
    odsEmployeePlanningSCHEDULED_TIMEBLOCK_5: TStringField;
    odsEmployeePlanningSCHEDULED_TIMEBLOCK_6: TStringField;
    odsEmployeePlanningSCHEDULED_TIMEBLOCK_7: TStringField;
    odsEmployeePlanningSCHEDULED_TIMEBLOCK_8: TStringField;
    odsEmployeePlanningSCHEDULED_TIMEBLOCK_9: TStringField;
    odsEmployeePlanningSCHEDULED_TIMEBLOCK_10: TStringField;
    odsStandAvailAVAILABLE_TIMEBLOCK_5: TStringField;
    odsStandAvailAVAILABLE_TIMEBLOCK_6: TStringField;
    odsStandAvailAVAILABLE_TIMEBLOCK_7: TStringField;
    odsStandAvailAVAILABLE_TIMEBLOCK_8: TStringField;
    odsStandAvailAVAILABLE_TIMEBLOCK_9: TStringField;
    odsStandAvailAVAILABLE_TIMEBLOCK_10: TStringField;
    odsEmployeeAvailAVAILABLE_TIMEBLOCK_5: TStringField;
    odsEmployeeAvailAVAILABLE_TIMEBLOCK_6: TStringField;
    odsEmployeeAvailAVAILABLE_TIMEBLOCK_7: TStringField;
    odsEmployeeAvailAVAILABLE_TIMEBLOCK_8: TStringField;
    odsEmployeeAvailAVAILABLE_TIMEBLOCK_9: TStringField;
    odsEmployeeAvailAVAILABLE_TIMEBLOCK_10: TStringField;
    procedure odsEmployeePlanningCalcFields(DataSet: TDataSet);
    procedure DataModuleCreate(Sender: TObject);
    procedure odsTimeRegScanningCalcFields(DataSet: TDataSet);
    procedure odsStandAvailCalcFields(DataSet: TDataSet);
    procedure odsEmployeeAvailCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
    FCurrentNow: TDateTime;
    procedure SetCurrentNow(const Value: TDateTime);
    function DetermineTimeBlockStartEndDate(
      PlantCode: String; EmployeeNumber: Integer;
      ShiftNumber: Integer; DepartmentCode: String;
      DayNr: Integer;
      TimeBlock: Integer;
      var StartDate, EndDate: TDateTime): Boolean;
    procedure PlanScanEmp_GetShiftDay(
      AEmployeeNumber: Integer;
      AScanStart, AScanEnd: TDateTime;
      APlantCode: String;
      AShiftNumber: Integer;
      ADepartmentCode: String;
      var StartDateTime, EndDateTime: TDateTime);
  public
    { Public declarations }
    procedure AdjustStartEndDates(
      AShiftStartDate, AShiftEndDate, ARecordDate: TDateTime;
      var AStartDate, AEndDate: TDateTime);
    property CurrentNow: TDateTime read FCurrentNow write SetCurrentNow;
  end;

var
  PlanScanEmpDM: TPlanScanEmpDM;

implementation

uses
  ProductionScreenDMT, CalculateTotalHoursDMT, UGlobalFunctions;

{$R *.DFM}

constructor TPlanScanClass.Create;
//var
//  MyNow: TDateTime;
begin
  inherited Create;
//  MyNow := Now;
  OpenTimeBlocks;
  OpenAbsenceReason;
  OpenEmployee; // Always open this, because it's used also as lookup-table.
//  OpenEmployeePlanning(MyNow);
//  OpenStandAvail(MyNow);
//  OpenEmployeeAvail(MyNow);
  OpenOnlyStandAvail;
  OpenOnlyEmpAvail;
//  WorkspotsPerEMPList := TList.Create; // TD-22082
  EmployeeList := TList.Create;
  OpenWorkspot; // 20013910 This is used as lookup-table.
  Init := False;
end;

destructor TPlanScanClass.Destroy;
begin
//  ClearWorkspotsPerEMPList; // TD-22082
//  EmployeesEMPList.Free;
//  WorkspotsPerEMPList.Free; // TD-22082
  inherited Destroy;
end;

// Open ClientDataSets for 'TimeBlocks'.
// These are all timeblocks that can be found,
// per Employee/Shift and Department.
procedure TPlanScanClass.OpenTimeBlocks;
begin
  with PlanScanEmpDM do
  begin
    cdsTimeBlockPerDepartment.Open;
    cdsTimeBlockPerDepartment.LogChanges := False; // TD-22082

//    cdsTimeBlockPerEmployee.Open; // TD-22082 Opened later
    cdsTimeBlockPerShift.Open;
    cdsTimeBlockPerShift.LogChanges := False; // TD-22082
  end;
end;

// Open the ClientDataSet to get 'EmployeePlanning'-data for given date.
// This gets data for all employees that are planned on the given date,
// on ALL workspots.
// MRA:02-FEB-2009 RV018. Also look at previous day!
function TPlanScanClass.OpenEmployeePlanning(ADate: TDateTime): Boolean;
begin
  with PlanScanEmpDM do
  begin
    // No Filtering!
    cdsEmployeePlanning.Filtered := False;
    cdsEmployeePlanning.Close;
    odsEmployeePlanning.Close;
    // MR:13-05-2005 Trunc must be used!
//    odsEmployeePlanning.ClearVariables;
//    odsEmployeePlanning.SetVariable('EMPLOYEEPLANNING_DATE', Trunc(ADate));
    // MRA:02-FEB-2009 RV018. Also look at previous day!
    odsEmployeePlanning.DeleteVariables;
    odsEmployeePlanning.DeclareAndSet('DATEFROM', otDate, Trunc(ADate)-1);
    odsEmployeePlanning.DeclareAndSet('DATETO',   otDate, Trunc(ADate));

    // TD-22082 Filter on user to limit number of records.
    odsEmployeePlanning.DeclareAndSet('USER_NAME', otString,
      ORASystemDM.UserTeamLoginUser);

    // MR:13-05-2005 Open and refresh 'ods'-first! Then open 'cds'.
    // The same for other Open-procedures in this DataModule.
    odsEmployeePlanning.Open;
    // Also Refresh! Otherwise the query will not update because the filter
    // was set to 'False'.
    // odsEmployeePlanning.Refresh;
    cdsEmployeePlanning.Open;
    cdsEmployeePlanning.LogChanges := False; // TD-22082
    Result := (cdsEmployeePlanning.RecordCount > 0);
  end;
end;

// Open the ClientDataSet to get 'TimeRegScanning'-data for given date.
// This gets data for all employees that are scanned on the given date,
// on ALL workspots.
function TPlanScanClass.OpenTimeRegScanning(ADate: TDateTime): Boolean;
var
  DateFrom, DateTo: TDateTime;
begin
  with PlanScanEmpDM do
  begin
    // No Filtering!
    cdsTimeRegScanning.Filtered := False;
    DateFrom := ADate - 0.5; // 12 hours before
    DateTo := ADate;
    cdsTimeRegScanning.Close;
    odsTimeRegScanning.Close;
    odsTimeRegScanning.DeleteVariables;
    odsTimeRegScanning.DeclareAndSet('DATEFROM', otDate, DateFrom);
    odsTimeRegScanning.DeclareAndSet('DATETO',   otDate, DateTo);

    // TD-22082 Filter on user to limit number of records.
    odsTimeRegScanning.DeclareAndSet('USER_NAME', otString,
      ORASystemDM.UserTeamLoginUser);

    odsTimeRegScanning.Open;
    // odsTimeRegScanning.Refresh;
    cdsTimeRegScanning.Open;
    cdsTimeRegScanning.LogChanges := False; // // TD-22082
    Result := (cdsTimeRegScanning.RecordCount > 0);
  end;
end;

// Open the ClientDataSet to get 'StandardAvailability'-data for given date.
function TPlanScanClass.OpenStandAvail(ADate: TDateTime): Boolean;
var
  DayNrFrom, DayNrTo: Integer;
begin
  // TD-20502
  // This is wrong!
  // Here the previous day is selected as 'DayNrFrom', but when
  // current DayNr is 1 (Monday) then previous daynr will be 7,
  // resulting in no records found.
  // The Query must be changed: it must look for DayNrFrom OR DayNrTo
  // instead of >= DayNrFrom and <= DayNrTo

  DayNrFrom := GetDayListIndex(DayOfWeek(ADate - 1));
  DayNrTo := GetDayListIndex(DayOfWeek(ADate));
  with PlanScanEmpDM do
  begin
    // No Filtering!
    cdsStandAvail.Filtered := False;
    cdsStandAvail.Close;
    odsStandAvail.Close;
(*
    odsStandAvail.ClearVariables;
    odsStandAvail.SetVariable('DAY_OF_WEEK_FROM', DayNrFrom);
    odsStandAvail.SetVariable('DAY_OF_WEEK_TO',   DayNrTo);
*)
    odsStandAvail.DeleteVariables;
    odsStandAvail.DeclareAndSet('DAY_OF_WEEK_FROM', otInteger, DayNrFrom);
    odsStandAvail.DeclareAndSet('DAY_OF_WEEK_TO',   otInteger, DayNrTo);

    // TD-22082 Filter on user to limit number of records.
    odsStandAvail.DeclareAndSet('USER_NAME', otString,
      ORASystemDM.UserTeamLoginUser);

    odsStandAvail.Open;
    // odsStandAvail.Refresh;
    cdsStandAvail.Open;
    cdsStandAvail.LogChanges := False; // TD-22082
    Result := (cdsStandAvail.RecordCount > 0);
  end;
end;

function TPlanScanClass.OpenEmployeeAvail(ADate: TDateTime): Boolean;
begin
  with PlanScanEmpDM do
  begin
    // No Filtering!
    cdsEmployeeAvail.Filtered := False;
    cdsEmployeeAvail.Close;
    odsEmployeeAvail.Close;
(*
    odsEmployeeAvail.ClearVariables;
    odsEmployeeAvail.SetVariable('EMPLOYEEAVAILABILITY_DATE', Trunc(ADate));
*)
    // MRA:02-FEB-2009 RV018. Also look at previous day!
    odsEmployeeAvail.DeleteVariables;
    odsEmployeeAvail.DeclareAndSet('DATEFROM', otDate, Trunc(ADate)-1);
    odsEmployeeAvail.DeclareAndSet('DATETO',   otDate, Trunc(ADate));

    // TD-22082 Filter on user to limit number of records.
    odsEmployeeAvail.DeclareAndSet('USER_NAME', otString,
      ORASystemDM.UserTeamLoginUser);

    odsEmployeeAvail.Open;
    // odsEmployeeAvail.Refresh;
    cdsEmployeeAvail.Open;
    cdsEmployeeAvail.LogChanges := False; // TD-22082
    Result := (cdsEmployeeAvail.RecordCount > 0);
  end;
end;

procedure TPlanScanClass.OpenOnlyStandAvail;
begin
  with PlanScanEmpDM do
  begin
    cdsOnlyStandAvail.CreateDataSet;
    cdsOnlyStandAvail.LogChanges := False; // TD-22082
  end;
end;

procedure TPlanScanClass.OpenOnlyEmpAvail;
begin
  with PlanScanEmpDM do
  begin
    cdsOnlyEmpAvail.CreateDataSet;
    cdsOnlyEmpAvail.LogChanges := False; // TD-22082
  end;
end;

function TPlanScanClass.OpenAbsenceReason: Boolean;
begin
  with PlanScanEmpDM do
  begin
    cdsAbsenceReason.Close;
    odsAbsenceReason.Close;
    odsAbsenceReason.Open;
    // odsAbsenceReason.Refresh;
    cdsAbsenceReason.Open;
    cdsAbsenceReason.LogChanges := False; // TD-22082
    Result := (cdsAbsenceReason.RecordCount > 0);
  end;
end;

function TPlanScanClass.OpenEmployee: Boolean;
begin
  with PlanScanEmpDM do
  begin
    cdsEmployee.Close;
    odsEmployee.Close;

    // TD-22082 Filter on user to limit number of records.
    odsEmployee.DeleteVariables;
    odsEmployee.DeclareAndSet('USER_NAME', otString,
      ORASystemDM.UserTeamLoginUser);

    odsEmployee.Open;
    // odsEmployee.Refresh;
    cdsEmployee.Open;
    cdsEmployee.LogChanges := False; // TD-22082
    Result := (cdsEmployee.RecordCount > 0);

    OpenTimeBlockPerEmployee; // TD-22082

    Init := True; // TD-22082
  end;
end;

// TD-22082
{
procedure TPlanScanClass.ClearWorkspotsPerEMPList;
var
  I: Integer;
  AWorkspotsPerEMP: PTWorkspotsPerEMP;
begin
  for I := WorkspotsPerEMPList.Count - 1 downto 0 do
  begin
    AWorkspotsPerEMP := WorkspotsPerEMPList.Items[I];
    WorkspotsPerEMPList.Remove(AWorkspotsPerEMP);
  end;
  // Don't free the list!
end;
}
function TPlanScanClass.CreateOnlyStandAvailClientDataSet;
var
  AFound: Boolean;
begin
  with PlanScanEmpDM do
  begin
    cdsOnlyStandAvail.EmptyDataSet;
    cdsStandAvail.First;
    while not cdsStandAvail.Eof do
    begin
      Application.ProcessMessages;
      AFound := SearchEmployeePlanning(
        cdsStandAvail.FieldByName('EMPLOYEE_NUMBER').Value);
      if not AFound then
        AFound := SearchTimeRegScanning(
          cdsStandAvail.FieldByName('EMPLOYEE_NUMBER').Value);
      if not AFound then
      begin
        if not SearchOnlyStandAvail(
          cdsStandAvail.FieldByName('EMPLOYEE_NUMBER').Value) then
        begin
          cdsOnlyStandAvail.Append;
          cdsOnlyStandAvail.FieldByName('EMPLOYEENUMBER').Value :=
            cdsStandAvail.FieldByName('EMPLOYEE_NUMBER').Value;
          cdsOnlyStandAvail.FieldByName('PLANTCODE').Value :=
            cdsStandAvail.FieldByName('PLANT_CODE').Value;
          cdsOnlyStandAvail.FieldByName('EMPLOYEESHORTNAME').Value :=
            cdsStandAvail.FieldByName('SHORT_NAME').Value;
          cdsOnlyStandAvail.FieldByName('EMPLOYEENAME').Value :=
            cdsStandAvail.FieldByName('DESCRIPTION').Value;
          cdsOnlyStandAvail.FieldByName('TEAMCODE').Value :=
            cdsStandAvail.FieldByName('TEAM_CODE').Value;
          cdsOnlyStandAvail.FieldByName('WORKSPOTCODE').Value := '';
          cdsOnlyStandAvail.FieldByName('DEPARTMENTCODE').Value :=
            cdsStandAvail.FieldByName('DEPARTMENT_CODE').Value;
          cdsOnlyStandAvail.FieldByName('SHIFTNUMBER').Value :=
            cdsStandAvail.FieldByName('SHIFT_NUMBER').Value;
          cdsOnlyStandAvail.FieldByName('STARTDATE').AsDateTime :=
            cdsStandAvail.FieldByName('STARTDATE').AsDateTime;
          cdsOnlyStandAvail.FieldByName('ENDDATE').AsDateTime :=
            cdsStandAvail.FieldByName('ENDDATE').AsDateTime;
          cdsOnlyStandAvail.FieldByName('MARK').Value := True;
          cdsOnlyStandAvail.Post;
        end;
      end;
      cdsStandAvail.Next;
    end; // while
    Result := (cdsOnlyStandAvail.RecordCount > 0);
  end;
end;

function TPlanScanClass.CreateOnlyEmpAvailEmployeesClientDataSetWithReason: Boolean;
var
  AFound: Boolean;
begin
  with PlanScanEmpDM do
  begin
    cdsOnlyEmpAvail.EmptyDataSet;
    cdsEmployeeAvail.First;
    while not cdsEmployeeAvail.Eof do
    begin
      Application.ProcessMessages;
      AFound := SearchEmployeePlanning(
        cdsEmployeeAvail.FieldByName('EMPLOYEE_NUMBER').Value);
      if not AFound then
        AFound := SearchTimeRegScanning(
          cdsEmployeeAvail.FieldByName('EMPLOYEE_NUMBER').Value);
      if not AFound then
      begin
        if not SearchOnlyEmpAvail(
          cdsEmployeeAvail.FieldByName('EMPLOYEE_NUMBER').Value) then
        begin
          cdsOnlyEmpAvail.Append;
          cdsOnlyEmpAvail.FieldByName('EMPLOYEENUMBER').Value :=
            cdsEmployeeAvail.FieldByName('EMPLOYEE_NUMBER').Value;
          cdsOnlyEmpAvail.FieldByName('PLANTCODE').Value :=
            cdsEmployeeAvail.FieldByName('PLANT_CODE').Value;
          cdsOnlyEmpAvail.FieldByName('EMPLOYEESHORTNAME').Value :=
            cdsEmployeeAvail.FieldByName('SHORT_NAME').Value;
          cdsOnlyEmpAvail.FieldByName('EMPLOYEENAME').Value :=
            cdsEmployeeAvail.FieldByName('DESCRIPTION').Value;
          cdsOnlyEmpAvail.FieldByName('TEAMCODE').Value :=
            cdsEmployeeAvail.FieldByName('TEAM_CODE').Value;
          cdsOnlyEmpAvail.FieldByName('WORKSPOTCODE').Value := '';
          cdsOnlyEmpAvail.FieldByName('DEPARTMENTCODE').Value :=
            cdsEmployeeAvail.FieldByName('DEPARTMENT_CODE').Value;
          cdsOnlyEmpAvail.FieldByName('SHIFTNUMBER').Value :=
            cdsEmployeeAvail.FieldByName('SHIFT_NUMBER').Value;
          cdsOnlyEmpAvail.FieldByName('PLANWORKSPOTCODE').Value := '';
          cdsOnlyEmpAvail.FieldByName('PLANSTARTDATE').AsDateTime := 0;
          cdsOnlyEmpAvail.FieldByName('PLANENDDATE').AsDateTime := 0;
          cdsOnlyEmpAvail.FieldByName('FIRSTSTARTDATE').AsDateTime := 0;
          cdsOnlyEmpAvail.FieldByName('PLANLEVEL').Value := '';
          cdsOnlyEmpAvail.FieldByName('STANDAVAILSTARTDATE').AsDateTime := 0;
          cdsOnlyEmpAvail.FieldByName('STANDAVAILENDDATE').AsDateTime := 0;
          cdsOnlyEmpAvail.FieldByName('EMPAVAILSTARTDATE').AsDateTime :=
            cdsEmployeeAvail.FieldByName('STARTDATE').AsDateTime;
          cdsOnlyEmpAvail.FieldByName('EMPAVAILENDDATE').AsDateTime :=
            cdsEmployeeAvail.FieldByName('ENDDATE').AsDateTime;
          cdsOnlyEmpAvail.FieldByName('ABSENCEREASONCODE').Value :=
            cdsEmployeeAvail.FieldByName('ABSENCEREASONCODE').Value;
          cdsOnlyEmpAvail.FieldByName('ABSENCEREASONDESCRIPTION').Value :=
            cdsEmployeeAvail.FieldByName('ABSENCEREASONDESCRIPTION').Value;
          cdsOnlyEmpAvail.FieldByName('MARK').Value := True;
          // Now look-up employee in Only-Standard-Available-list
          if SearchOnlyStandAvail(
            cdsEmployeeAvail.FieldByName('EMPLOYEE_NUMBER').Value) then
          begin
            cdsOnlyEmpAvail.FieldByName('STANDAVAILSTARTDATE').AsDateTime :=
              cdsOnlyStandAvail.FieldByName('STARTDATE').AsDateTime;
            cdsOnlyEmpAvail.FieldByName('STANDAVAILENDDATE').AsDateTime :=
              cdsOnlyStandAvail.FieldByName('ENDDATE').AsDateTime;
          end;
          cdsOnlyEmpAvail.Post;
        end;
      end;
      cdsEmployeeAvail.Next;
    end; // While
    Result := (cdsOnlyEmpAvail.RecordCount > 0);
  end;
end;

// Employees who are planned but not scanned and who are absent without a reason
// MR:09-FEB-2009 Changed, after compare for 'SearchStandAvail', the rest must
//                be in a block-statement!
function TPlanScanClass.CreateOnlyEmpAvailEmployeesClientDataSetWithoutReason: Boolean;
var
  AFound: Boolean;
begin
  with PlanScanEmpDM do
  begin
    cdsOnlyEmpAvail.EmptyDataSet;
    cdsEmployeePlanning.First;
    while not cdsEmployeePlanning.Eof do
    begin
      Application.ProcessMessages;
      AFound := SearchTimeRegScanning(
        cdsEmployeePlanning.FieldByName('EMPLOYEE_NUMBER').Value);
      if not AFound then
      begin
        AFound := SearchStandAvail(
          cdsEmployeePlanning.FieldByName('EMPLOYEE_NUMBER').Value);
        if AFound then
        begin
          AFound := SearchEmployeeAvail(
            cdsEmployeePlanning.FieldByName('EMPLOYEE_NUMBER').Value);
          if not AFound then
          begin
            if not SearchOnlyEmpAvail(
              cdsEmployeePlanning.FieldByName('EMPLOYEE_NUMBER').Value) then
            begin
              cdsOnlyEmpAvail.Append;
              cdsOnlyEmpAvail.FieldByName('EMPLOYEENUMBER').Value :=
                cdsEmployeePlanning.FieldByName('EMPLOYEE_NUMBER').Value;
              cdsOnlyEmpAvail.FieldByName('PLANTCODE').Value :=
                cdsEmployeePlanning.FieldByName('PLANT_CODE').Value;
              cdsOnlyEmpAvail.FieldByName('EMPLOYEESHORTNAME').Value :=
                cdsEmployeePlanning.FieldByName('SHORT_NAME').Value;
              cdsOnlyEmpAvail.FieldByName('EMPLOYEENAME').Value :=
                cdsEmployeePlanning.FieldByName('DESCRIPTION').Value;
              cdsOnlyEmpAvail.FieldByName('TEAMCODE').Value :=
                cdsEmployeePlanning.FieldByName('TEAM_CODE').Value;
              cdsOnlyEmpAvail.FieldByName('WORKSPOTCODE').Value :=
                cdsEmployeePlanning.FieldByName('WORKSPOT_CODE').Value;
              cdsOnlyEmpAvail.FieldByName('DEPARTMENTCODE').Value :=
                cdsEmployeePlanning.FieldByName('DEPARTMENT_CODE').Value;
              cdsOnlyEmpAvail.FieldByName('SHIFTNUMBER').Value :=
                cdsEmployeePlanning.FieldByName('SHIFT_NUMBER').Value;
              cdsOnlyEmpAvail.FieldByName('PLANWORKSPOTCODE').Value :=
                cdsEmployeePlanning.FieldByName('WORKSPOT_CODE').Value;
              cdsOnlyEmpAvail.FieldByName('PLANSTARTDATE').AsDateTime :=
                cdsEmployeePlanning.FieldByName('STARTDATE').AsDateTime;
              cdsOnlyEmpAvail.FieldByName('PLANENDDATE').AsDateTime :=
                cdsEmployeePlanning.FieldByName('ENDDATE').AsDateTime;
              cdsOnlyEmpAvail.FieldByName('FIRSTTARTDATE').AsDateTime :=
                cdsEmployeePlanning.FieldByName('FIRSTSTARTDATE').AsDateTime; // PIM-237
              cdsOnlyEmpAvail.FieldByName('PLANLEVEL').Value :=
                cdsEmployeePlanning.FieldByName('PLANLEVEL').AsString;
              cdsOnlyEmpAvail.FieldByName('STANDAVAILSTARTDATE').AsDateTime :=
                cdsStandAvail.FieldByName('STARTDATE').AsDateTime;
              cdsOnlyEmpAvail.FieldByName('STANDAVAILENDDATE').AsDateTime :=
                cdsStandAvail.FieldByName('ENDDATE').AsDateTime;
              cdsOnlyEmpAvail.FieldByName('EMPAVAILSTARTDATE').AsDateTime := 0;
              cdsOnlyEmpAvail.FieldByName('EMPAVAILENDDATE').AsDateTime := 0;
              cdsOnlyEmpAvail.FieldByName('ABSENCEREASONCODE').Value := '';
              cdsOnlyEmpAvail.FieldByName('ABSENCEREASONDESCRIPTION').Value := '';
              cdsOnlyEmpAvail.FieldByName('MARK').Value := True;
              cdsOnlyEmpAvail.Post;
            end;
          end;
        end;
      end;
      cdsEmployeePlanning.Next;
    end; // while
    Result := (cdsOnlyEmpAvail.RecordCount > 0);
  end;
end;

// TD-22082
{
function TPlanScanClass.CreateWorkspotsPerEmployeeList(
  AStartdateLevel: TDateTime): Boolean;
var
  AWorkspotsPerEMP: PTWorkspotsPerEMP;
  // 20013910 Also look for department
  function ExistsRecord(PlantCode, DepartmentCode, WorkspotCode: String;
    EmployeeNumber: Integer): Boolean;
  var
    I: Integer;
    AWorkspotsPerEMP: PTWorkspotsPerEMP;
  begin
    Result := False;
    for I := 0 to WorkspotsPerEMPList.Count - 1 do
    begin
      AWorkspotsPerEMP := WorkspotsPerEMPList.Items[I];
      if (AWorkspotsPerEMP.PlantCode = PlantCode) and
        (AWOrkspotsPerEMP.DepartmentCode = DepartmentCode) and
        (AWorkspotsPerEMP.WorkspotCode = WorkspotCode) and
        (AWorkspotsPerEMP.EmployeeNumber = EmployeeNumber) then
      begin
        Result := True;
        Break;
      end;
    end;
  end;
begin
  with PlanScanEMPDM.oqCreateWorkspotsPerEmployeeList do
  begin
    ClearVariables;
    SetVariable('STARTDATE_LEVEL', AStartdateLevel);
    Execute;
    Result := not(Eof);
    if Result then
    begin
// allready true fo oq's      First;//Last;
      while not Eof do
      begin
        // 20013910 Also look for department
        if not ExistsRecord(FieldAsString('PLANT_CODE'),
          FieldAsString('DEPARTMENT_CODE'),
          FieldAsString('WORKSPOT_CODE'),
          FieldAsInteger('EMPLOYEE_NUMBER')) then
        begin
          New(AWorkspotsPerEMP);
          AWorkspotsPerEMP.PlantCode      := FieldAsString('PLANT_CODE');
          AWorkspotsPerEMP.DepartmentCode := FieldAsString('DEPARTMENT_CODE');
          AWorkspotsPerEMP.WorkspotCode   := FieldAsString('WORKSPOT_CODE');
          AWorkspotsPerEMP.EmployeeNumber := FieldAsInteger('EMPLOYEE_NUMBER');
          AWorkspotsPerEMP.StartdateLevel := FieldAsDate('STARTDATE_LEVEL');
          AWorkspotsPerEMP.EmployeeLevel  := FieldAsString('EMPLOYEE_LEVEL');
          WorkspotsPerEMPList.Add(AWorkspotsPerEMP);
        end;
        Next;//Prior;
      end;
    end;
// not for TOracleQuery    Close;
  end;
end;
}
function TPlanScanClass.DeterminePlannedEmployeesPerWorkspot(
  PlantCode: String;
  WorkspotCode: String;
  CurrentDate: TDateTime): Boolean;
var
  DateFilterStr: String;
  DepartmentCode: String;
begin
  // MRA:29-JAN-2009 RV017. Do not 'frac' the CurrentDate, StartDate and EndDate
  // contains now also the date-part, because of overnight-timeblocks!
  ORASystemDM.ADateFmt.SwitchDateTimeFmtORA;
  DateFilterStr :=
    '(STARTDATE <= ' +
    QUOTE + SysUtils.FormatDateTime(ORASystemDM.ADateFmt.DateTimeFmt, CurrentDate)
     + QUOTE + ')' +
    ' AND ' +
    '(ENDDATE >= ' +
    QUOTE + SysUtils.FormatDateTime(ORASystemDM.ADateFmt.DateTimeFmt, CurrentDate)
     + QUOTE + ')';
  // Set filter on given plantcode and workspotcode
  with PlanScanEmpDM do
  begin
    cdsEmployeePlanning.Filtered := False;
    // All plantcodes + workspots
    if (PlantCode = '') and (WorkspotCode = '') then
      cdsEmployeePlanning.Filter :=
        'MARK=' + QUOTE + 'True' + QUOTE + ' AND ' +
        DateFilterStr
    else
    begin
    // Just 1 plantcode + workspot
      // MR:21-10-2003 Use QUOTES !
      // 20013910 Also look for plant+department-combination for plan-on-dept.
      DepartmentCode := DetermineWorkspotDepartment(PlantCode, WorkspotCode);
      cdsEmployeePlanning.Filter :=
        'PLANT_CODE=' + QUOTE + PlantCode + QUOTE +
        ' AND ' +
        '( ' +
        '   (WORKSPOT_CODE=' + QUOTE + WorkspotCode + QUOTE + ')' +
        '  OR ' +
        '   (DEPARTMENT_CODE=' + QUOTE + DepartmentCode + QUOTE + ' AND ' +
        '    WORKSPOT_CODE=' + QUOTE + '0' + QUOTE + ')' +
        ') ' +
        ' AND ' +
        'MARK=' + QUOTE + 'True' + QUOTE +
        ' AND ' +
        DateFilterStr;
    end;
    cdsEmployeePlanning.Filtered := True;
    Result := (cdsEmployeePlanning.RecordCount > 0);

//ShowMessage(IntToStr(cdsEmployeePlanning.RecordCount));
{$IFDEF DEBUG3}
WLog('Planned:');
cdsEmployeePlanning.First;
while not cdsEmployeePlanning.Eof do
begin
  WLog(
    cdsEmployeePlanning.FieldByName('EMPLOYEE_NUMBER').AsString + ', ' +
    cdsEmployeePlanning.FieldByName('PLANT_CODE').AsString + ', ' +
    cdsEmployeePlanning.FieldByName('WORKSPOT_CODE').AsString + ', ' +
    DateTimeToStr(cdsEmployeePlanning.FieldByName('STARTDATE').AsDateTime) + ', ' +
    DateTimeToStr(cdsEmployeePlanning.FieldByName('ENDDATE').AsDateTime) + ', ' +
    cdsEmployeePlanning.FieldByName('PLANLEVEL').AsString + ', ' +
    cdsEmployeePlanning.FieldByName('MARK').AsString + ', ' +
    DateTimeToStr(cdsEmployeePlanning.FieldByName('FIRSTSTARTDATE').AsDateTime)
    );
  cdsEmployeePlanning.Next;
end;
cdsEmployeePlanning.First;
{$ENDIF}
  end;
  ORASystemDM.ADateFmt.SwitchDateTimeFmtWindows;
end;

function TPlanScanClass.DetermineScannedEmployeesPerPlant(PlantCode: String;
  CurrentDate: TDateTime): Boolean;
var
  DateFilterStr: String;
begin
  ORASystemDM.ADateFmt.SwitchDateTimeFmtORA;
  // Set filter on given plantcode + workspotcode + currentdate
  DateFilterStr :=
    '(DATETIME_IN <= ' +
    QUOTE + SysUtils.FormatDateTime(ORASystemDM.ADateFmt.DateTimeFmt, CurrentDate)
     + QUOTE + ')' +
    ' AND ' +
    '((DATETIME_OUT >= ' +
    QUOTE + SysUtils.FormatDateTime(ORASystemDM.ADateFmt.DateTimeFmt, CurrentDate)
     + QUOTE + ')' +
    ' OR ' +
    '(DATETIME_OUT IS NULL))';

  with PlanScanEmpDM do
  begin
    cdsTimeRegScanning.Filtered := False;
    if (PlantCode = '') then
      cdsTimeRegScanning.Filter := DateFilterStr
    else
      // MR:21-10-2003 Use QUOTES!
      cdsTimeRegScanning.Filter :=
        'PLANT_CODE=' + QUOTE + PlantCode + QUOTE + ' AND ' +
        DateFilterStr;
    cdsTimeRegScanning.Filtered := True;
    Result := (cdsTimeRegScanning.RecordCount > 0);
  end;
  ORASystemDM.ADateFmt.SwitchDateTimeFmtWindows;
end; // DetermineScannedEmployeesPerPlant

function TPlanScanClass.DetermineScannedEmployeesPerWorkspot(
  PlantCode: String;
  WorkspotCode: String;
  CurrentDate: TDateTime): Boolean;
var
  DateFilterStr: String;
begin
  ORASystemDM.ADateFmt.SwitchDateTimeFmtORA;
  // Set filter on given plantcode + workspotcode + currentdate
  DateFilterStr :=
    '(DATETIME_IN <= ' +
    QUOTE + SysUtils.FormatDateTime(ORASystemDM.ADateFmt.DateTimeFmt, CurrentDate)
     + QUOTE + ')' +
    ' AND ' +
    '((DATETIME_OUT >= ' +
    QUOTE + SysUtils.FormatDateTime(ORASystemDM.ADateFmt.DateTimeFmt, CurrentDate)
     + QUOTE + ')' +
    ' OR ' +
    '(DATETIME_OUT IS NULL))';

  with PlanScanEmpDM do
  begin
    cdsTimeRegScanning.Filtered := False;
    if (PlantCode = '') and (WorkspotCode = '') then
      cdsTimeRegScanning.Filter := DateFilterStr
    else
      // MR:21-10-2003 Use QUOTES!
      cdsTimeRegScanning.Filter :=
        'PLANT_CODE=' + QUOTE + PlantCode + QUOTE + ' AND ' +
        'WORKSPOT_CODE=' + QUOTE + WorkspotCode + QUOTE + ' AND ' +
        DateFilterStr;
    cdsTimeRegScanning.Filtered := True;
    Result := (cdsTimeRegScanning.RecordCount > 0);

{$IFDEF DEBUG3}
WLog('Scanned:');
cdsTimeRegScanning.First;
while not cdsTimeRegScanning.Eof do
begin
  WLog(
    cdsTimeRegScanning.FieldByName('EMPLOYEE_NUMBER').AsString + ', ' +
    cdsTimeRegScanning.FieldByName('PLANT_CODE').AsString + ', ' +
    cdsTimeRegScanning.FieldByName('WORKSPOT_CODE').AsString + ', ' +
    DateTimeToStr(cdsTimeRegScanning.FieldByName('DATETIME_IN').AsDateTime) + ', ' +
    DateTimeToStr(cdsTimeRegScanning.FieldByName('DATETIME_OUT').AsDateTime) + ', ' +
    cdsTimeRegScanning.FieldByName('MARK').AsString
    );
  cdsTimeRegScanning.Next;
end;
cdsTimeRegScanning.First;
{$ENDIF}
  end;
  ORASystemDM.ADateFmt.SwitchDateTimeFmtWindows;
end;

function TPlanScanClass.DetermineStandAvailEmployees(
  CurrentDate: TDateTime): Boolean;
var
  DateFilterStr: String;
begin
  // MRA:29-JAN-2009 RV017. Do not 'frac' the CurrentDate, StartDate and EndDate
  // contains now also the date-part, because of overnight-timeblocks!
  ORASystemDM.ADateFmt.SwitchDateTimeFmtORA;
  // Set filter on given currentdate
  DateFilterStr :=
    '(STARTDATE <= ' +
    QUOTE + SysUtils.FormatDateTime(ORASystemDM.ADateFmt.DateTimeFmt, CurrentDate)
     + QUOTE + ')' +
    ' AND ' +
    '(ENDDATE >= ' +
    QUOTE + SysUtils.FormatDateTime(ORASystemDM.ADateFmt.DateTimeFmt, CurrentDate)
     + QUOTE + ')';

  with PlanScanEmpDM do
  begin
    cdsStandAvail.Filtered := False;
    cdsStandAvail.Filter :=
      'MARK=' + QUOTE + 'True' + QUOTE + ' AND ' +
      DateFilterStr;
    cdsStandAvail.Filtered := True;
    Result := (cdsStandAvail.RecordCount > 0);
  end;
  ORASystemDM.ADateFmt.SwitchDateTimeFmtWindows;
end;

function TPlanScanClass.DetermineEmployeeAvailEmployees(
  CurrentDate: TDateTime): Boolean;
var
  DateFilterStr: String;
begin
  // MRA:29-JAN-2009 RV017. Do not 'frac' the CurrentDate, StartDate and EndDate
  // contains now also the date-part, because of overnight-timeblocks!
  ORASystemDM.ADateFmt.SwitchDateTimeFmtORA;
  // Set filter on given currentdate
  DateFilterStr :=
    '(STARTDATE <= ' +
    QUOTE + SysUtils.FormatDateTime(ORASystemDM.ADateFmt.DateTimeFmt, CurrentDate)
     + QUOTE + ')' +
    ' AND ' +
    '(ENDDATE >= ' +
    QUOTE + SysUtils.FormatDateTime(ORASystemDM.ADateFmt.DateTimeFmt, CurrentDate)
     + QUOTE + ')';

  with PlanScanEmpDM do
  begin
    cdsEmployeeAvail.Filtered := False;
    cdsEmployeeAvail.Filter :=
      'MARK=' + QUOTE + 'True' + QUOTE + ' AND ' +
      DateFilterStr;
    cdsEmployeeAvail.Filtered := True;
    Result := (cdsEmployeeAvail.RecordCount > 0);
  end;
  ORASystemDM.ADateFmt.SwitchDateTimeFmtWindows;
end;

// 20013910 Also handle plan-on-departments.
function TPlanScanClass.DetermineWorkspotsPerEmployeeLevel(
  PlantCode, DepartmentCode, WorkspotCode: String;
  EmployeeNumber: Integer): String;
var
{  AWorkspotsPerEMP: PTWorkspotsPerEMP;
  I: Integer; } // TD-22082
  Found: Boolean; // 20013910
begin
  Result := '';
  Found := False;
  // TD-22082
  if WorkspotCode <> '0' then // 20013910 Plan-on-workspot
    if PlanScanEmpDM.
      cdsWorkspotsPerEmployee.Locate('PLANT_CODE;WORKSPOT_CODE;EMPLOYEE_NUMBER',
        VarArrayOf([PlantCode, WorkspotCode, EmployeeNumber]), []) then
    begin
      Result := PlanScanEmpDM.
        cdsWorkspotsPerEmployee.FieldByName('EMPLOYEE_LEVEL').AsString;
      Found := True;
    end;
  if not Found then // 20013910 Plan-on-dept.
    if PlanScanEmpDM.
      cdsWorkspotsPerEmployee.
        Locate('PLANT_CODE;DEPARTMENT_CODE;WORKSPOT_CODE;EMPLOYEE_NUMBER',
          VarArrayOf([PlantCode, DepartmentCode, '0', EmployeeNumber]), []) then
        Result := PlanScanEmpDM.
          cdsWorkspotsPerEmployee.FieldByName('EMPLOYEE_LEVEL').AsString;
{
  for I := 0 to WorkspotsPerEMPList.Count - 1 do
  begin
    AWorkspotsPerEMP := WorkspotsPerEMPList.Items[I];
    if WorkspotCode <> '0' then // 20013910
      if (AWorkspotsPerEMP.PlantCode = PlantCode) and
        (AWorkspotsPerEMP.WorkspotCode = WorkspotCode) and
        (AWorkspotsPerEMP.EmployeeNumber = EmployeeNumber) then
      begin
        Result := AWorkspotsPerEMP.EmployeeLevel;
        Break;
      end;
    // 20013910 Plan-on-dept.
    if Result = '' then
      if (AWorkspotsPerEMP.PlantCode = PlantCode) and
        (AWorkspotsPerEMP.DepartmentCode = DepartmentCode) and
        (AWorkspotsPerEMP.WorkspotCode = '0') and
        (AWorkspotsPerEMP.EmployeeNumber = EmployeeNumber) then
      begin
        Result := AWorkspotsPerEMP.EmployeeLevel;
        Break;
      end;
  end;
}
end;

procedure TPlanScanClass.ClearEmployeeList;
var
  APTEmployee: PTEmployee;
  I: Integer;
begin
  if Assigned(EmployeeList) then
  begin
    for I := EmployeeList.Count - 1 downto 0 do
    begin
      APTEmployee := EmployeeList.Items[I];
      Dispose(APTEmployee);
      EmployeeList.Remove(APTEmployee);
    end;
    EmployeeList.Clear;
  end;
  // Don't free the list!
end;

// MRA:02-FEB-2009 RV018.
function TPlanScanClass.EmployeeListExists(AEmployeeNumber: Integer): Boolean;
var
  I: Integer;
  APTEmployee: PTEmployee;
begin
  Result := False;
  if Assigned(EmployeeList) then
    for I := 0 to EmployeeList.Count - 1 do
    begin
      APTEmployee := EmployeeList.Items[I];
      if AEmployeeNumber = APTEmployee.AEmployeeNumber then
      begin
        Result := True;
        Break;
       end;
    end;
end;

// MRA:02-FEB-2009 RV018. Using this, the frac() can be switched on/off, of an
//                        alternative date-format can be used.
function MyDate(ADate: TDateTime): TDateTime;
begin
//  Result := Frac(ADate);
  Result := ADate;
end;

// Create a list of employees that are working on a specific plant+workspot
// Store this in 'EmployeeList' (TList)
function TPlanScanClass.DetermineEmployeeWSListMain(
  APlantCode, AWorkspotCode: String;
  Plan_Scan_Employees, All_Employees,
  Stand_Avail_Employees, Emp_Avail_Employees,
  Absent_With_Reason: Boolean): Boolean;
var
  MyNow: TDateTime;
  I, J: Integer;
  APTEmployee, APTEmployee2: PTEmployee;
  AFound: Boolean;
  PlanCount: Integer;
  ScanCount: Integer;
begin
  MyNow := Now;

  // TD-21210
  PlanCount := 0;
  ScanCount := 0;

  ClearEmployeeList;

  // Set a Filter on Planned Employees
  // Do this ALWAYS, in case a specific list needs it.
  DeterminePlannedEmployeesPerWorkspot(
    APlantCode, AWorkspotCode, MyNow
    );
  // PIM-275
  // Set a Filter on Scanned Employee
  // Do this ALWAYS, in case a specific list needs it.
{  DetermineScannedEmployeesPerWorkspot(
    APlantCode, AWorkspotCode, MyNow
    ); }
  // PIM-275 Do not filter on workspot
  DetermineScannedEmployeesPerPlant(
    APlantCode, MyNow
    );

  if (Plan_Scan_Employees) then
  begin
    // First determine PLANNED
    with PlanScanEmpDM do
    begin
      cdsEmployeePlanning.First;
      while not cdsEmployeePlanning.Eof do
      begin
        Application.ProcessMessages;
        // Keep employees in list
        if not EmployeeListExists(cdsEmployeePlanning.
          FieldByName('EMPLOYEE_NUMBER').Value) then
        begin
          new(APTEmployee);
          APTEmployee.AEmployeeNumber :=
            cdsEmployeePlanning.FieldByName('EMPLOYEE_NUMBER').Value;
          APTEmployee.AEmployeeShortName :=
            cdsEmployeePlanning.FieldByName('SHORT_NAME').Value;
          APTEmployee.AEmployeeName :=
            cdsEmployeePlanning.FieldByName('DESCRIPTION').Value;
          APTEmployee.APlantCode :=
            cdsEmployeePlanning.FieldByName('PLANT_CODE').Value;
          APTEmployee.APlanWorkspotCode :=
            cdsEmployeePlanning.FieldByName('WORKSPOT_CODE').Value;
          APTEmployee.APlanDepartmentCode :=
            cdsEmployeePlanning.FieldByName('DEPARTMENT_CODE').Value; // 20013910
          APTEmployee.ATeamCode :=
            cdsEmployeePlanning.FieldByName('TEAM_CODE').Value;
          APTEmployee.AScanned := False;
          APTEmployee.AJobCode := '';
          APTEmployee.AColor := clGray;
          APTEmployee.AShiftNumber :=
            cdsEmployeePlanning.FieldByName('SHIFT_NUMBER').Value;
          APTEmployee.AFirstAidYN :=
            cdsEmployeePlanning.FieldByName('FIRSTAID_YN').AsString;
          APTEmployee.AFirstAidExpDate :=
            cdsEmployeePlanning.FieldByName('FIRSTAID_EXP_DATE').AsDateTime;
          APTEmployee.APlanLevel :=
            cdsEmployeePlanning.FieldByName('PLANLEVEL').AsString;
          APTEmployee.AEmpLevel := DetermineWorkspotsPerEmployeeLevel(
            cdsEmployeePlanning.FieldByName('PLANT_CODE').Value,
            cdsEmployeePlanning.FieldByName('DEPARTMENT_CODE').Value, // 20013910
            cdsEmployeePlanning.FieldByName('WORKSPOT_CODE').Value,
            cdsEmployeePlanning.FieldByName('EMPLOYEE_NUMBER').Value);

          // MRA:29-JAN-2009 RV017. Frac the STARTDATE and ENDDATE here,
          // because it now contains the date-part.
          APTEmployee.APlanStartDate :=
            MyDate(cdsEmployeePlanning.FieldByName('STARTDATE').AsDateTime);
          APTEmployee.APlanEndDate :=
            MyDate(cdsEmployeePlanning.FieldByName('ENDDATE').AsDateTime);
          APTEmployee.AFirstPlanStartDate :=
            MyDate(cdsEmployeePlanning.FieldByName('FIRSTSTARTDATE').AsDateTime); // PIM-237

          APTEmployee.AScanDatetimeIn := 0;
          APTEmployee.AScanDatetimeOut := 0;
          APTEmployee.AFirstScan := 0; // PIM-237
          APTEmployee.AStandAvailStartDate := 0;
          APTEmployee.AStandAvailEndDate := 0;
          APTEmployee.AEmpAvailStartDate := 0;
          APTEmployee.AEmpAvailEndDate := 0;
          APTEmployee.ACurrEff := 0;
          APTEmployee.AShiftEff := 0;
          APTEmployee.AKeep := True;
          EmployeeList.Add(APTEmployee);
          inc(PlanCount);
        end; // if not EmployeeListExists()
        cdsEmployeePlanning.Next;
      end;
    end;

    with PlanScanEmpDM do
    begin
      // Now determine PLANNED + SCANNED puppets
      for I := 0 to EmployeeList.Count - 1 do
      begin
        APTEmployee := EmployeeList.Items[I];
        if SearchTimeRegScanning(APTEmployee.AEmployeeNumber) then
        begin
          // PIM-275
          if
            All_Employees
            or
            (APTEmployee.APlanWorkspotCode = '0') // planned on department
            or
            (
              (APlantCode = '') or (AWorkspotCode = '') // PIM-237 This can be empty!
            )
            or
            (
              (APlantCode = cdsTimeRegScanning.FieldByName('PLANT_CODE').Value)
              and
              (AWorkspotCode = cdsTimeRegScanning.FieldByName('WORKSPOT_CODE').Value)
            )
            then
          begin
            APTEmployee.AScanDatetimeIn :=
              cdsTimeRegScanning.FieldByName('DATETIME_IN').AsDateTime;
            APTEmployee.AScanDatetimeOut :=
              cdsTimeRegScanning.FieldByName('DATETIME_OUT').AsDateTime;
            APTEmployee.AFirstScan :=
              cdsTimeRegScanning.FieldByName('FIRSTSCAN').AsDateTime; // PIM-237
            APTEmployee.AScanWorkspotCode :=
              cdsTimeRegScanning.FieldByName('WORKSPOT_CODE').Value;
            APTEmployee.AColor := clGreen;
            // MRA:02-JAN-2009 RV015. Also fill the following!
            APTEmployee.AScanned := True;
            APTEmployee.AJobCode :=
              cdsTimeRegScanning.FieldByName('JOB_CODE').AsString;
            APTEmployee.ACurrEff :=
              Round(cdsTimeRegScanning.FieldByName('CURREFF').AsFloat);
            APTEmployee.AShiftEff :=
              Round(cdsTimeRegScanning.FieldByName('SHIFTEFF').AsFloat);
            APTEmployee.AShiftNumber :=
              cdsTimeRegScanning.FieldByName('SHIFT_NUMBER').AsInteger; // PIM-275
          end;
        end;
      end;
    end;

    // PIM-275 Get scans filtered on plant + workspot
    DetermineScannedEmployeesPerWorkspot(
      APlantCode, AWorkspotCode, MyNow
      );
      
    // Now determine ONLY SCANNED employees (the rest that's still 'marked'
    // in 'scannedEMPList'
    with PlanScanEmpDM do
    begin
      cdsTimeRegScanning.First;
      while not cdsTimeRegScanning.Eof do
      begin
        Application.ProcessMessages;
        // Exists record already in EmployeeList?
        AFound := False;
        for I := 0 to EmployeeList.Count - 1 do
        begin
          APTEmployee := EmployeeList.Items[I];
          if cdsTimeRegScanning.FieldByName('EMPLOYEE_NUMBER').Value =
            APTEmployee.AEmployeeNumber then
          begin
            AFound := True;
            Break;
          end;
        end;
        // Not found, add as 'Only Scanned Employee'.
        if not AFound then
        begin
          new(APTEmployee);
          APTEmployee.AEmployeeNumber :=
            cdsTimeRegScanning.FieldByName('EMPLOYEE_NUMBER').Value;
          APTEmployee.AEmployeeShortName :=
            cdsTimeRegScanning.FieldByName('SHORT_NAME').Value;
          APTEmployee.AEmployeeName :=
            cdsTimeRegScanning.FieldByName('DESCRIPTION').Value;
          APTEmployee.APlantCode :=
            cdsTimeRegScanning.FieldByName('PLANT_CODE').Value;
          APTEmployee.AScanWorkspotCode :=
            cdsTimeRegScanning.FieldByName('WORKSPOT_CODE').Value;
          APTEmployee.ATeamCode :=
            cdsTimeRegScanning.FieldByName('TEAM_CODE').Value;
          APTEmployee.AScanned := True;
          APTEmployee.AJobCode :=
            cdsTimeRegScanning.FieldByName('JOB_CODE').AsString;
          APTEmployee.AColor := clRed;
          APTEmployee.AShiftNumber :=
            cdsTimeRegScanning.FieldByName('SHIFT_NUMBER').Value;
          APTEmployee.AFirstAidYN :=
            cdsTimeRegScanning.FieldByName('FIRSTAID_YN').Value;
          APTEmployee.AFirstAidExpDate :=
            cdsTimeRegScanning.FieldByName('FIRSTAID_EXP_DATE').AsDateTime;
          APTEmployee.AEmpLevel := DetermineWorkspotsPerEmployeeLevel(
            cdsTimeRegScanning.FieldByName('PLANT_CODE').Value,
            DetermineWorkspotDepartment(
              cdsTimeRegScanning.FieldByName('PLANT_CODE').Value,
              cdsTimeRegScanning.FieldByName('WORKSPOT_CODE').Value), // 20013910
            cdsTimeRegScanning.FieldByName('WORKSPOT_CODE').Value,
            cdsTimeRegScanning.FieldByName('EMPLOYEE_NUMBER').Value);
          APTEmployee.APlanStartDate := 0;
          APTEmployee.APlanEndDate := 0;
          APTEmployee.AFirstPlanStartDate := 0; // PIM-237
          APTEmployee.AScanDatetimeIn :=
            cdsTimeRegScanning.FieldByName('DATETIME_IN').AsDateTime;
          APTEmployee.AScanDatetimeOut :=
            cdsTimeRegScanning.FieldByName('DATETIME_OUT').AsDateTime;
          APTEmployee.AFirstScan :=
            cdsTimeRegScanning.FieldByName('FIRSTSCAN').AsDateTime; // PIM-237
          APTEmployee.AStandAvailStartDate := 0;
          APTEmployee.AStandAvailEndDate := 0;
          APTEmployee.AEmpAvailStartDate := 0;
          APTEmployee.AEmpAvailEndDate := 0;
          APTEmployee.ACurrEff :=
            Round(cdsTimeRegScanning.FieldByName('CURREFF').AsFloat);
          APTEmployee.AShiftEff :=
            Round(cdsTimeRegScanning.FieldByName('SHIFTEFF').AsFloat);
          APTEmployee.AKeep := True;
          EmployeeList.Add(APTEmployee);
        end;
        cdsTimeRegScanning.Next;
      end;
    end;

    // Change color for employee
    // Take 'Only Scanned'-employee
    // Check if there is a 'Only Planned'-employee with the same
    // level.
    for I := 0 to EmployeeList.Count - 1 do
    begin
      APTEmployee := EmployeeList.Items[I];
      if APTEmployee.AColor = clRed then // Only Scanned
      begin
        for J := 0 to EmployeeList.Count - 1 do
        begin
          APTEmployee2 := EmployeeList.Items[J];
          if APTEmployee2.AColor = clGray then // Only Planned
            if (APTEmployee2.AEmployeeNumber <> APTEmployee.AEmployeeNumber) and
              (APTEmployee2.APlantCode = APTEmployee.APlantCode) and
              (  // 20013910 Test on workspot or on department
                (
                  (APTEmployee2.APlanWorkspotCode <> '0') and
                  (APTEmployee2.APlanWorkspotCode = APTEmployee.AScanWorkspotCode)
                )
                or
                (
                  (APTEmployee2.APlanWorkspotCode = '0') and
                  (APTemployee2.APlanDepartmentCode =
                   DetermineWorkspotDepartment(APTEmployee.APlantCode,
                     APTEmployee.AScanWorkspotCode))
                )
              ) then
            begin
              inc(ScanCount);
              // Determine level of 'Only Scanned'-puppet
              // Compare it with level of 'Only Planned'-puppet
//              if DetermineWorkspotsPerEmployeeLevel(
//                APTEmployee.APlantCode,
//                APTEmployee.AScanWorkspotCode,
//                APTEmployee.AEmployeeNumber) = APTEmployee2.APlanLevel then
              // TD-21210 Do this only if the number of planned employees is
              //          not reached yet, otherwise assign Red as color.
              if ScanCount <= PlanCount then
              begin
                if APTEmployee.AEmpLevel = APTEmployee2.APlanLevel then
                  APTEmployee.AColor := clMyYellow
                else
                  APTEmployee.AColor := clMyOrange;
              end;
              Break;
            end;
        end;
      end;
    end;
  end; // if (Plan_Scan_Employees) then

  // Also add the rest of the employees (non-plan and non-scan)?
  if (All_Employees) then
  begin
    with PlanScanEmpDM do
    begin
      cdsEmployee.First;
      while not cdsEmployee.Eof do
      begin
        Application.ProcessMessages;
        AFound := SearchEmployeePlanning(
          cdsEmployee.FieldByName('EMPLOYEE_NUMBER').AsInteger);
        if not AFound then
          AFound := SearchTimeRegScanning(
            cdsEmployee.FieldByName('EMPLOYEE_NUMBER').AsInteger);
        if not AFound then
        begin
          new(APTEmployee);
          APTEmployee.AEmployeeNumber :=
            cdsEmployee.FieldByName('EMPLOYEE_NUMBER').AsInteger;
          APTEmployee.AEmployeeShortName :=
            cdsEmployee.FieldByName('SHORT_NAME').AsString;
          APTEmployee.AEmployeeName :=
            cdsEmployee.FieldByName('DESCRIPTION').AsString;
          APTEmployee.APlantCode :=
            cdsEmployee.FieldByName('PLANT_CODE').AsString;
          APTEmployee.APlanWorkspotCode := '';
          // TD-25322 Use the types instead of 'Value'! When field is empty
          //          it can give an error!
          APTEmployee.ATeamCode :=
            cdsEmployee.FieldByName('TEAM_CODE').AsString;
          APTEmployee.AScanned := False;
          APTEmployee.AJobCode := '';
          APTEmployee.AColor := clWhite;
          APTEmployee.AShiftNumber := 0;
          APTEmployee.AFirstAidYN := '';
          APTEmployee.AFirstAidExpDate := 0;
          APTEmployee.APlanLevel := '';
          APTEmployee.AEmpLevel := '';
          APTEmployee.APlanStartDate := 0;
          APTEmployee.APlanEndDate := 0;
          APTEmployee.AFirstPlanStartDate := 0; // PIM-237
          APTEmployee.AScanDatetimeIn := 0;
          APTEmployee.AScanDatetimeOut := 0;
          APTEmployee.AFirstScan := 0; // PIM-237
          APTEmployee.AStandAvailStartDate := 0;
          APTEmployee.AStandAvailEndDate := 0;
          APTEmployee.AEmpAvailStartDate := 0;
          APTEmployee.AEmpAvailEndDate := 0;
          APTEmployee.ACurrEff := 0;
          APTEmployee.AShiftEff := 0;
          APTEmployee.AKeep := True;
          EmployeeList.Add(APTEmployee);
        end;
        cdsEmployee.Next;
      end; // while
    end;
  end;

  // Add Only Standard Available Employees
  if (Stand_Avail_Employees) then
  begin
    DetermineStandAvailEmployees(MyNow);
    CreateOnlyStandAvailClientDataSet;
    with PlanScanEmpDM do
    begin
      cdsOnlyStandAvail.First;
      while not cdsOnlyStandAvail.Eof do
      begin
        Application.ProcessMessages;
        new(APTEmployee);
        APTEmployee.AEmployeeNumber :=
          cdsOnlyStandAvail.FieldByName('EMPLOYEENUMBER').Value;
        APTEmployee.AEmployeeShortName :=
          cdsOnlyStandAvail.FieldByName('EMPLOYEESHORTNAME').Value;
        APTEmployee.AEmployeeName :=
          cdsOnlyStandAvail.FieldByName('EMPLOYEENAME').Value;
        APTEmployee.APlanWorkspotCode := '';
        APTEmployee.ATeamCode :=
          cdsOnlyStandAvail.FieldByName('TEAMCODE').Value;
        APTEmployee.AScanned := False;
        APTEmployee.AJobCode := '';
        APTEmployee.AColor := clWhite;
        APTEmployee.AShiftNumber := 0;
        APTEmployee.AFirstAidYN := '';
        APTEmployee.AFirstAidExpDate := 0;
        APTEmployee.APlanLevel := '';
        APTEmployee.AEmpLevel := '';
        APTEmployee.APlanStartDate := 0;
        APTEmployee.APlanEndDate := 0;
        APTEmployee.AFirstPlanStartDate := 0; // PIM-237
        APTEmployee.AScanDatetimeIn := 0;
        APTEmployee.AScanDatetimeOut := 0;
        APTEmployee.AFirstScan := 0; // PIM-237
        // MRA:29-JAN-2009 RV017. Frac the STARTDATE and ENDDATE here,
        // because it now contains the date-part.
        APTEmployee.AStandAvailStartDate :=
          MyDate(cdsOnlyStandAvail.FieldByName('STARTDATE').AsDateTime);
        APTEmployee.AStandAvailEndDate :=
          MyDate(cdsOnlyStandAvail.FieldByName('ENDDATE').AsDateTime);
        APTEmployee.AEmpAvailStartDate := 0;
        APTEmployee.AEmpAvailEndDate := 0;
        APTEmployee.ACurrEff := 0;
        APTEmployee.AShiftEff := 0;
        APTEmployee.AKeep := True;
        EmployeeList.Add(APTEmployee);
        cdsOnlyStandAvail.Next;
      end; // while
    end;
  end; // if (Stand_Avail_Employees) then

  // Add Only Emp Available Employees
  if (Emp_Avail_Employees) then
  begin
    DetermineStandAvailEmployees(MyNow);
    CreateOnlyStandAvailClientDataSet;
    DetermineEmployeeAvailEmployees(MyNow);
    if (Absent_With_Reason) then
      CreateOnlyEmpAvailEmployeesClientDataSetWithReason
    else
      CreateOnlyEmpAvailEmployeesClientDataSetWithoutReason;
    with PlanScanEmpDM do
    begin
      cdsOnlyEmpAvail.First;
      while not cdsOnlyEmpAvail.Eof do
      begin
        Application.ProcessMessages;
        new(APTEmployee);
        APTEmployee.AEmployeeNumber :=
          cdsOnlyEmpAvail.FieldByName('EMPLOYEENUMBER').Value;
        APTEmployee.AEmployeeShortName :=
          cdsOnlyEmpAvail.FieldByName('EMPLOYEESHORTNAME').Value;
        APTEmployee.AEmployeeName :=
          cdsOnlyEmpAvail.FieldByName('EMPLOYEENAME').Value;
        APTEmployee.ATeamCode :=
          cdsOnlyEmpAvail.FieldByName('TEAMCODE').Value;
        APTEmployee.AScanned := False;
        APTEmployee.AJobCode := '';
        APTEmployee.AColor := clWhite;
        APTEmployee.AShiftNumber := 0;
        APTEmployee.AFirstAidYN := '';
        APTEmployee.AFirstAidExpDate := 0;
        APTEmployee.APlanLevel := '';
        APTEmployee.AEmpLevel := '';
        APTEmployee.APlanWorkspotCode :=
          cdsOnlyEmpAvail.FieldByName('PLANWORKSPOTCODE').Value;
        // MRA:29-JAN-2009 RV017. Frac the STARTDATE and ENDDATE here,
        // because it now contains the date-part.
        APTEmployee.APlanStartDate :=
          MyDate(cdsOnlyEmpAvail.FieldByName('PLANSTARTDATE').AsDateTime);
        APTEmployee.APlanEndDate :=
          MyDate(cdsOnlyEmpAvail.FieldByName('PLANENDDATE').AsDateTime);
        APTEmployee.AFirstPlanStartDate :=
          MyDate(cdsOnlyEmpAvail.FieldByName('FIRSTSTARTDATE').AsDateTime); // PIM-237
        APTEmployee.APlanLevel :=
          cdsOnlyEmpAvail.FieldByName('PLANLEVEL').AsString;
        APTEmployee.AScanDatetimeIn := 0;
        APTEmployee.AScanDatetimeOut := 0;
        APTEmployee.AFirstScan := 0; // PIM-237
        APTEmployee.AStandAvailStartDate :=
          MyDate(cdsOnlyEmpAvail.FieldByName('STANDAVAILSTARTDATE').AsDateTime);
        APTEmployee.AStandAvailEndDate :=
          MyDate(cdsOnlyEmpAvail.FieldByName('STANDAVAILENDDATE').AsDateTime);
        APTEmployee.AEmpAvailStartDate :=
          MyDate(cdsOnlyEmpAvail.FieldByName('EMPAVAILSTARTDATE').AsDateTime);
        APTEmployee.AEmpAvailEndDate :=
          MyDate(cdsOnlyEmpAvail.FieldByName('EMPAVAILENDDATE').AsDateTime);
        APTEmployee.AAbsenceReasonCode :=
          cdsOnlyEmpAvail.FieldByName('ABSENCEREASONCODE').Value;
        APTEmployee.AAbsenceReasonDescription :=
          cdsOnlyEmpAvail.FieldByName('ABSENCEREASONDESCRIPTION').Value;
        APTEmployee.ACurrEff := 0;
        APTEmployee.AShiftEff := 0;
        APTEmployee.AKeep := True;
        EmployeeList.Add(APTEmployee);
        cdsOnlyEmpAvail.Next;
      end; // while
    end;
  end; // if (Emp_Avail_Employees) then

  Result := (EmployeeList.Count > 0);
end;

function TPlanScanClass.DetermineEmployeeWSListPLAN_SCAN_EMP(
  APlantCode, AWorkspotCode: String): Boolean;
begin
  Result := DetermineEmployeeWSListMain(APlantCode, AWorkspotCode,
    True, False, False, False, False);
end;

function TPlanScanClass.DetermineEmployeeWSListALL_EMP(
  APlantCode, AWorkspotCode: String): Boolean;
begin
  Result := DetermineEmployeeWSListMain(APlantCode, AWorkspotCode,
    True, True, False, False, False);
end;

function TPlanScanClass.DetermineEmployeeWSListSTAND_AVAIL_EMP(
  APlantCode, AWorkspotCode: String): Boolean;
begin
  Result := DetermineEmployeeWSListMain(APlantCode, AWorkspotCode,
    False, False, True, False, False);
end;

function TPlanScanClass.DetermineEmployeeWSListEMP_AVAIL_EMP_REASON(
  APlantCode, AWorkspotCode: String): Boolean;
begin
  Result := DetermineEmployeeWSListMain(APlantCode, AWorkspotCode,
    False, False, False, True, True);
end;

function TPlanScanClass.DetermineEmployeeWSListEMP_AVAIL_EMP_NO_REASON(
  APlantCode, AWorkspotCode: String): Boolean;
begin
  Result := DetermineEmployeeWSListMain(APlantCode, AWorkspotCode,
    False, False, False, True, False);
end;

function TPlanScanClass.SearchEmployeePlanning(
  AEmployeeNumber: Integer): Boolean;
begin
  Result := PlanScanEmpDM.cdsEmployeePlanning.Locate('EMPLOYEE_NUMBER',
    VarArrayOf([AEmployeeNumber]), []);
end;

function TPlanScanClass.SearchTimeRegScanning(
  AEmployeeNumber: Integer): Boolean;
begin
  Result := PlanScanEmpDM.cdsTimeRegScanning.Locate('EMPLOYEE_NUMBER',
    VarArrayOf([AEmployeeNumber]), []);
end;

function TPlanScanClass.SearchStandAvail(
  AEmployeeNumber: Integer): Boolean;
begin
  Result := PlanScanEmpDM.cdsStandAvail.Locate('EMPLOYEE_NUMBER',
    VarArrayOf([AEmployeeNumber]), []);
end;

function TPlanScanClass.SearchEmployeeAvail(
  AEmployeeNumber: Integer): Boolean;
begin
  Result := PlanScanEmpDM.cdsEmployeeAvail.Locate('EMPLOYEE_NUMBER',
    VarArrayOf([AEmployeeNumber]), []);
end;

function TPlanScanClass.SearchOnlyStandAvail(
  AEmployeeNumber: Integer): Boolean;
begin
  Result := PlanScanEmpDM.cdsOnlyStandAvail.FindKey([AEmployeeNumber]);
end;

function TPlanScanClass.SearchOnlyEmpAvail(
  AEmployeeNumber: Integer): Boolean;
begin
  Result := PlanScanEmpDM.cdsOnlyEmpAvail.FindKey([AEmployeeNumber]);
end;

function TPlanScanEmpDM.DetermineTimeBlockStartEndDate(
  PlantCode: String; EmployeeNumber: Integer;
  ShiftNumber: Integer; DepartmentCode: String;
  DayNr: Integer;
  TimeBlock: Integer;
  var StartDate, EndDate: TDateTime): Boolean;
var
  Day: String;
  BreakStart, BreakEnd: TDateTime;
  BreakFound: Boolean;
  // Add breaks to timeblocks if needed.
  procedure CalcBreaks(ABreakStart, ABreakEnd: TDateTime;
    var AStartDate, AEndDate: TDateTime);
  var
    MyStartDate, MyEndDate: TDateTime;
    OriginalStartDate, OriginalEndDate: TDateTime;
  begin
    MyStartDate := AStartDate;
    MyEndDate := AEndDate;
    OriginalStartDate := AStartDate;
    OriginalEndDate := AEndDate;
    // Is it an overnight timeblock?
    if Frac(MyStartDate) > Frac(MyEndDate) then
    begin
      MyEndDate := Trunc(MyEndDate+1) + Frac(MyEndDate);
      // If the break is in the morning then make it the next day.
      if (Frac(ABreakStart) >= Frac(EncodeTime(0, 0, 0, 0))) and
        (Frac(ABreakStart) <= Frac(EncodeTime(12, 0, 0, 0))) then
        ABreakStart := Trunc(ABreakStart+1) + Frac(ABreakStart);
      if (Frac(ABreakEnd) >= Frac(EncodeTime(0, 0, 0, 0))) and
        (Frac(ABreakEnd) <= Frac(EncodeTime(12, 0, 0, 0))) then
        ABreakEnd := Trunc(ABreakEnd+1) + Frac(ABreakEnd);
    end;
    // Is the break part of the timeblock?
    if ((ABreakStart >= MyStartDate) and (ABreakEnd <= MyEndDate)) or
       ((ABreakStart >= MyStartDate) and (ABreakStart <= MyEndDate)) or
       ((ABreakEnd >= MyStartDate) and (ABreakEnd <= MyEndDate)) then
    begin
      // Try to change the timeblock based on breaks.
      try
        if (ABreakStart < MyStartDate) then
          AStartDate := ABreakStart;
      except
        AStartDate := OriginalStartDate;
      end;
      try
        if (ABreakEnd > MyEndDate) then
          AEndDate := ABreakEnd;
      except
        AEndDate := OriginalEndDate;
      end;
    end;
  end;
begin
  Result := False;
  Day := IntToStr(DayNr);
  if cdsTimeBlockPerEmployee.FindKey([
    PlantCode,ShiftNumber,EmployeeNumber,TimeBlock]) then
  begin
    Result := True;
    StartDate := cdsTimeBlockPerEmployee.FieldByName('STARTTIME'+Day).Value;
    EndDate := cdsTimeBlockPerEmployee.FieldByName('ENDTIME'+Day).Value;
  end
  else
  begin
    if cdsTimeBlockPerDepartment.FindKey([
      PlantCode, DepartmentCode, ShiftNumber, TimeBlock]) then
    begin
      Result := True;
      StartDate := cdsTimeBlockPerDepartment.FieldByName('STARTTIME'+Day).Value;
      EndDate := cdsTimeBlockPerDepartment.FieldByName('ENDTIME'+Day).Value;
    end
    else
    begin
      if cdsTimeBlockPerShift.FindKey([
        PlantCode, ShiftNumber, TimeBlock]) then
      begin
        Result := True;
        StartDate := cdsTimeBlockPerShift.FieldByName('STARTTIME'+Day).Value;
        EndDate := cdsTimeBlockPerShift.FieldByName('ENDTIME'+Day).Value;
      end;
    end;
  end;

  // MR: 09-FEB-2009 Adjust times by looking at breaks.
  BreakFound := False;
  BreakStart := 0;
  BreakEnd := 0;
  with CalculateTotalHoursDM do
  begin
    if ClientDataSetBEmpl.FindKey([
      PlantCode,ShiftNumber,EmployeeNumber,TimeBlock]) then
    begin
      BreakFound := True;
      BreakStart := ClientDataSetBEmpl.FieldByName('STARTTIME'+Day).Value;
      BreakEnd := ClientDataSetBEmpl.FieldByName('ENDTIME'+Day).Value;
    end
    else
    begin
      if ClientDataSetBDept.FindKey([
        PlantCode, DepartmentCode, ShiftNumber, TimeBlock]) then
      begin
        BreakFound := True;
        BreakStart := ClientDataSetBDept.FieldByName('STARTTIME'+Day).Value;
        BreakEnd := ClientDataSetBDept.FieldByName('ENDTIME'+Day).Value;
      end
      else
      begin
        if ClientDataSetBShift.FindKey([
          PlantCode, ShiftNumber, TimeBlock]) then
        begin
          BreakFound := True;
          BreakStart := ClientDataSetBShift.FieldByName('STARTTIME'+Day).Value;
          BreakEnd := ClientDataSetBShift.FieldByName('ENDTIME'+Day).Value;
        end;
      end;
    end;
  end;
  if Result then
    if BreakFound then
    begin
//WLog('Before TB:' + DateTimeToStr(StartDate) + ' - ' + DateTimeToStr(EndDate));
//WLog('        B:' + DateTimeToStr(BreakStart) + ' - ' + DateTimeToStr(BreakEnd));
      CalcBreaks(BreakStart, BreakEnd, StartDate, EndDate);
//WLog('After  TB:' + DateTimeToStr(StartDate) + ' - ' + DateTimeToStr(EndDate));
    end;
end;

// MRA:29-JAN-2009 RV018.
procedure TPlanScanEmpDM.AdjustStartEndDates(
  AShiftStartDate, AShiftEndDate, ARecordDate: TDateTime;
  var AStartDate, AEndDate: TDateTime);
var
  NightShift: Boolean;
  DateTemp: TDateTime;
begin
  // Adjust the dates of the Start and EndDate.
  // MR:11-FEB-2009 RV019. Always use RecordDate for AShiftStartDate!
  NightShift := (Trunc(AShiftStartDate) <> Trunc(AShiftEndDate));
  AShiftStartDate := Trunc(ARecordDate) + Frac(AShiftStartDate);
{$IFDEF DEBUG3}
if NightShift then
  AShiftEndDate := Trunc(ARecordDate + 1) + Frac(AShiftEndDate)
else
  AShiftEndDate := Trunc(ARecordDate) + Frac(AShiftEndDate);
WLog(' S=' + DateTimeToStr(AShiftStartDate) + ' - ' +
  DateTimeToStr(AShiftEndDate));
{$ENDIF}
  if NightShift then
  begin
    // RV057.2. Use 'DateTemp' during comparison, otherwise you can get
    //          a rounding-problem! Resulting in wrong values!
    DateTemp := Trunc(ARecordDate) + Frac(AStartDate);
    if AShiftStartDate <= DateTemp then
      AStartDate := DateTemp
    else
      AStartDate := Trunc(ARecordDate + 1) + Frac(AStartDate);
    DateTemp := Trunc(ARecordDate) + Frac(AEndDate);
    if AShiftStartDate <= DateTemp then
      AEndDate := DateTemp
    else
      AEndDate := Trunc(ARecordDate + 1) + Frac(AEndDate);
  end
  else
  begin
    AStartDate := Trunc(ARecordDate) + Frac(AStartDate);
    AEndDate := Trunc(ARecordDate) + Frac(AEndDate);
  end;
end;

// MRA:02-FEB-2009 RV018.
procedure TPlanScanEmpDM.odsEmployeePlanningCalcFields(DataSet: TDataSet);
var
  TBI: Integer;
  DayNr: Integer;
  StartDate, EndDate: TDateTime;
  ShiftStartDate, ShiftEndDate: TDateTime;
  EmpPlanDate: TDateTime;
begin
  with DataSet do
  begin
    // First get the shift-start and shift-end employee is working on.
    PlanScanEmp_GetShiftDay(
      FieldByName('EMPLOYEE_NUMBER').Value,
      CurrentNow, CurrentNow,
      FieldByName('PLANT_CODE').Value,
      FieldByName('SHIFT_NUMBER').Value,
      FieldByName('DEPARTMENT_CODE').Value,
      ShiftStartDate, ShiftEndDate);
{$IFDEF DEBUG4}
WLog('ShiftDay: E=' + FieldByName('EMPLOYEE_NUMBER').AsString +
  ' Now=' + DateTimeToStr(CurrentNow) +
  ' Sh=' + FieldByName('SHIFT_NUMBER').AsString +
  ' D=' + FieldByName('DEPARTMENT_CODE').AsString
  );
{$ENDIF}
    DayNr := GetDayListIndex(
      DayOfWeek(FieldByName('EMPLOYEEPLANNING_DATE').AsDateTime));
    EmpPlanDate := FieldByName('EMPLOYEEPLANNING_DATE').AsDateTime;
    FieldByName('MARK').Value := False;
    // PIM-237 We have to know the first planned time-block-start-date-time.
    FieldByName('FIRSTSTARTDATE').AsDateTime := 0;
    for TBI := 1 to ORASystemDM.MaxTimeblocks do
    begin
      if FieldByName('SCHEDULED_TIMEBLOCK_' +
        IntToStr(TBI)).AsString <> '' then
      begin
        if FieldByName('SCHEDULED_TIMEBLOCK_' +
          IntToStr(TBI)).AsString[1] in ['A', 'B', 'C'] then
        begin
          if DetermineTimeBlockStartEndDate(
            FieldByName('PLANT_CODE').Value,
            FieldByName('EMPLOYEE_NUMBER').Value,
            FieldByName('SHIFT_NUMBER').Value,
            FieldByName('DEPARTMENT_CODE').Value,
            DayNr,
            TBI,
            StartDate,
            EndDate) then
          begin
            // First adjust the dates of the Start and EndDate.
            AdjustStartEndDates(ShiftStartDate, ShiftEndDate,
              EmpPlanDate, StartDate, EndDate);
            if FieldByName('FIRSTSTARTDATE').AsDateTime = 0 then // PIM-237
              FieldByName('FIRSTSTARTDATE').AsDateTime := StartDate;
{$IFDEF DEBUG3}
WLog('->Plan.Sched.TB:' + IntToStr(TBI) +
  ' Employee  =' + FieldByName('EMPLOYEE_NUMBER').AsString + 
  ' FirstSDate=' + DateTimeToStr(FieldByName('FIRSTSTARTDATE').AsDateTime) + 
  ' Adj. SDate=' + DateTimeToStr(StartDate) +
  ' Adj. EDate=' + DateTimeToStr(EndDate)
  );
{$ENDIF}
            // Then look if this is the current plan-timeblock
            if (CurrentNow >= StartDate) and
              (CurrentNow <= EndDate) then
            begin
              FieldByName('MARK').Value := True;
              FieldByName('PLANLEVEL').Value :=
                FieldByName('SCHEDULED_TIMEBLOCK_' +
                  IntToStr(TBI)).AsString[1];
              FieldByName('STARTDATE').Value := StartDate;
              FieldByName('ENDDATE').Value := EndDate;
{$IFDEF DEBUG4}
WLog('-> Plan-block: ' + FieldByName('EMPLOYEE_NUMBER').AsString + ' - ' +
  DateToStr(EmpPlanDate) + ' - ' +
  IntToStr(TBI) + ' - ' +
  FieldByName('PLANLEVEL').AsString + ' - ' +
  DateTimeToStr(StartDate) + ' - ' + DateTimeToStr(EndDate));
{$ENDIF}
              Break;
            end;
          end;
        end;
      end;
    end;
  end;
end; // odsEmployeePlanningCalcFields

procedure TPlanScanEmpDM.odsTimeRegScanningCalcFields(DataSet: TDataSet);
begin
  with DataSet do
  begin
    FieldByName('MARK').Value := False;
  end;
end;

procedure TPlanScanEmpDM.SetCurrentNow(const Value: TDateTime);
begin
  FCurrentNow := Value;
end;

procedure TPlanScanEmpDM.DataModuleCreate(Sender: TObject);
begin
  CurrentNow := Now;
end;

// MRA:02-FEB-2009 RV018.
procedure TPlanScanEmpDM.odsStandAvailCalcFields(DataSet: TDataSet);
var
  TBI: Integer;
  DayNr: Integer;
  CurrentDayNr: Integer;
  StartDate, EndDate: TDateTime;
  ShiftStartDate, ShiftEndDate: TDateTime;
  StandAvailDate: TDateTime;
begin
  CurrentDayNr := GetDayListIndex(DayOfWeek(CurrentNow));
  with DataSet do
  begin
    // First get the shift-start and shift-end employee is working on.
    PlanScanEmp_GetShiftDay(
      FieldByName('EMPLOYEE_NUMBER').Value,
      CurrentNow, CurrentNow,
      FieldByName('PLANT_CODE').Value,
      FieldByName('SHIFT_NUMBER').Value,
      FieldByName('DEPARTMENT_CODE').Value,
      ShiftStartDate, ShiftEndDate);
    DayNr := FieldByName('DAY_OF_WEEK').AsInteger;
    if (DayNr = CurrentDayNr) then
      StandAvailDate := Trunc(CurrentNow)
    else
      StandAvailDate := Trunc(CurrentNow - 1);
    FieldByName('MARK').Value := False;
    for TBI := 1 to ORASystemDM.MaxTimeblocks do
    begin
      if FieldByName('AVAILABLE_TIMEBLOCK_' +
        IntToStr(TBI)).AsString <> '' then
      begin
        if FieldByName('AVAILABLE_TIMEBLOCK_' +
          IntToStr(TBI)).AsString[1] = '*' then
          if DetermineTimeBlockStartEndDate(
            FieldByName('PLANT_CODE').Value,
            FieldByName('EMPLOYEE_NUMBER').Value,
            FieldByName('SHIFT_NUMBER').Value,
            FieldByName('DEPARTMENT_CODE').Value,
            DayNr,
            TBI,
            StartDate,
            EndDate) then
          begin
            // First adjust the dates of the Start and EndDate.
            AdjustStartEndDates(ShiftStartDate, ShiftEndDate,
              StandAvailDate, StartDate, EndDate);
            // Then look if this is the current stand-avail-timeblock
            if (CurrentNow >= StartDate) and
              (CurrentNow <= EndDate) then
            begin
              FieldByName('STARTDATE').Value := StartDate;
              FieldByName('ENDDATE').Value := EndDate;
              FieldByName('MARK').Value := True;
              Break;
{$IFDEF DEBUG3}
WLog('-> StandAvail-block: ' + IntToStr(TBI) + ' - ' +
  DateTimeToStr(StartDate) + ' - ' + DateTimeToStr(EndDate));
{$ENDIF}
            end;
          end;
      end;
    end;
//    FieldByName('MARK').Value := False;
  end;
end;

// MRA:02-FEB-2009 RV018.
procedure TPlanScanEmpDM.odsEmployeeAvailCalcFields(DataSet: TDataSet);
var
  TBI: Integer;
  DayNr: Integer;
  StartDate, EndDate: TDateTime;
  ShiftStartDate, ShiftEndDate: TDateTime;
  EmpAvailDate: TDateTime;
begin
  with DataSet do
  begin
    // First get the shift-start and shift-end employee is working on.
    PlanScanEmp_GetShiftDay(
      FieldByName('EMPLOYEE_NUMBER').Value,
      CurrentNow, CurrentNow,
      FieldByName('PLANT_CODE').Value,
      FieldByName('SHIFT_NUMBER').Value,
      FieldByName('DEPARTMENT_CODE').Value,
      ShiftStartDate, ShiftEndDate);
    DayNr := GetDayListIndex(
      DayOfWeek(FieldByName('EMPLOYEEAVAILABILITY_DATE').AsDateTime));
    EmpAvailDate := FieldByName('EMPLOYEEAVAILABILITY_DATE').AsDateTime;
    FieldByName('MARK').Value := False;
//    FieldByName('FIRSTSTARTDATE').AsDateTime := 0; // PIM-237
    for TBI := 1 to ORASystemDM.MaxTimeblocks do
    begin
      if FieldByName('AVAILABLE_TIMEBLOCK_' +
        IntToStr(TBI)).AsString <> '' then
      begin
        if not (FieldByName('AVAILABLE_TIMEBLOCK_' +
          IntToStr(TBI)).AsString[1] in ['*','-']) then
          if DetermineTimeBlockStartEndDate(
            FieldByName('PLANT_CODE').Value,
            FieldByName('EMPLOYEE_NUMBER').Value,
            FieldByName('SHIFT_NUMBER').Value,
            FieldByName('DEPARTMENT_CODE').Value,
            DayNr,
            TBI,
            StartDate,
            EndDate) then
          begin
            // First adjust the dates of the Start and EndDate.
            AdjustStartEndDates(ShiftStartDate, ShiftEndDate,
              EmpAvailDate, StartDate, EndDate);
//            if FieldByName('FIRSTSTARTDATE').AsDateTime = 0 then // PIM-237
//              FieldByName('FIRSTSTARTDATE').AsDateTime := StartDate;
            // Then look if this is the current stand-avail-timeblock
            if (CurrentNow >= StartDate) and
              (CurrentNow <= EndDate) then
            begin
              FieldByName('ABSENCEREASONCODE').Value :=
                FieldByName('AVAILABLE_TIMEBLOCK_' +
                IntToStr(TBI)).AsString[1];
              if cdsAbsenceReason.FindKey([
                FieldByName('AVAILABLE_TIMEBLOCK_' +
                IntToStr(TBI)).AsString[1]]) then
                FieldByName('ABSENCEREASONDESCRIPTION').Value :=
                  cdsAbsenceReason.FieldByName('DESCRIPTION').Value
              else
                FieldByName('ABSENCEREASONDESCRIPTION').Value := '';
              FieldByName('STARTDATE').Value := StartDate;
              FieldByName('ENDDATE').Value := EndDate;
              FieldByName('MARK').Value := True;
              Break;
            end;
          end;
      end;
    end;
//    FieldByName('MARK').Value := False;
  end;
end;

procedure TPlanScanEmpDM.PlanScanEmp_GetShiftDay(
  AEmployeeNumber: Integer;
  AScanStart, AScanEnd: TDateTime;
  APlantCode: String;
  AShiftNumber: Integer;
  ADepartmentCode: String;
  var StartDateTime, EndDateTime: TDateTime);
var
  EmployeeData: TScannedIDCard;
begin
  EmployeeData.EmployeeCode := AEmployeeNumber;
  EmployeeData.DateIn := AScanStart;
  EmployeeData.DateOut := AScanEnd;
  EmployeeData.PlantCode := APlantCode;
  EmployeeData.ShiftNumber := AShiftNumber;
  EmployeeData.DepartmentCode := ADepartmentCode;

  AShiftDay.GetShiftDay(EmployeeData, StartDateTime, EndDateTime);
end;

// 20013910 Used as lookup-table.
procedure TPlanScanClass.OpenWorkspot;
begin
  with PlanScanEmpDM do
  begin
    cdsWorkspot.Close;
    odsWorkspot.Close;
    odsWorkspot.Open;
    cdsWorkspot.Open;
    cdsWorkspot.LogChanges := False; // TD-22082
  end;
end;

// 20013910
function TPlanScanClass.DetermineWorkspotDepartment(APlantCode,
  AWorkspotCode: String): String;
begin
  Result := '0';
  with PlanScanEmpDM do
  begin
    if cdsWorkspot.Locate('PLANT_CODE;WORKSPOT_CODE',
      VarArrayOf([APlantCode, AWorkspotCode]), []) then
      Result := cdsWorkspot.FieldByName('DEPARTMENT_CODE').AsString;
  end;
end;

// TD-22082
procedure TPlanScanClass.OpenWorkspotsPerEmployee;
begin
  with PlanScanEmpDM do
  begin
    cdsWorkspotsPerEmployee.Close;
    odsWorkspotsPerEmployee.Close;

    // TD-22082 Filter on user to limit number of records.
    odsWorkspotsPerEmployee.DeleteVariables;
    odsWorkspotsPerEmployee.DeclareAndSet('USER_NAME', otString,
      ORASystemDM.UserTeamLoginUser);

    odsWorkspotsPerEmployee.Open;
    cdsWorkspotsPerEmployee.Open;
    cdsWorkspotsPerEmployee.LogChanges := False; // TD-22082
  end;
end; // OpenWorkspotsPerEmployee

// TD-22082
procedure TPlanScanClass.OpenTimeBlockPerEmployee;
begin
  if not Init then
  begin
  with PlanScanEmpDM do
  begin
    cdsTimeBlockPerEmployee.Close;
    odsTimeBlockPerEmployee.Close;

    // TD-22082 Filter on user to limit number of records.
    odsTimeBlockPerEmployee.DeleteVariables;
    odsTimeBlockPerEmployee.DeclareAndSet('USER_NAME', otString,
      ORASystemDM.UserTeamLoginUser);

    odsTimeBlockPerEmployee.Open;
    cdsTimeBlockPerEmployee.Open;
    cdsTimeBlockPerEmployee.LogChanges := False; // TD-22082
  end;
  end;
end; // OpenTimeBlockPerEmployee

// 20014450.50
function TPlanScanClass.FindEmployee(AEmployeeNumber: Integer): PTEmployee;
var
  APTEmployee: PTEmployee;
  I: Integer;
begin
  Result := nil;
  if Assigned(EmployeeList) then
  begin
    for I := 0 to EmployeeList.Count - 1 do
    begin
      APTEmployee := EmployeeList.Items[I];
      if APTEmployee.AEmployeeNumber = AEmployeeNumber then
      begin
        Result := APTEmployee;
        Break;
      end;
    end;
  end;
end; // FindEmployee

end.


