inherited GridBaseF: TGridBaseF
  Left = 180
  Top = 1
  Width = 658
  Height = 598
  Caption = 'Grid Base'
  OnClose = FormClose
  OnDestroy = FormDestroy
  OnDeactivate = FormDeactivate
  OnHide = FormHide
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnlMasterGrid: TPanel [1]
    Left = 0
    Top = 26
    Width = 650
    Height = 129
    Align = alTop
    Caption = 'pnlMasterGrid'
    TabOrder = 2
    object spltMasterGrid: TSplitter
      Left = 1
      Top = 125
      Width = 648
      Height = 3
      Cursor = crVSplit
      Align = alBottom
      AutoSnap = False
      MinSize = 100
    end
    object dxMasterGrid: TdxDBGrid
      Left = 1
      Top = 1
      Width = 648
      Height = 124
      Bands = <
        item
        end>
      DefaultLayout = True
      HeaderPanelRowCount = 1
      SummaryGroups = <>
      SummarySeparator = ', '
      Align = alClient
      TabOrder = 0
      OnEnter = dxGridEnter
      HideSelectionTextColor = clWindowText
      LookAndFeel = lfFlat
      OptionsBehavior = [edgoAutoSort, edgoCaseInsensitive, edgoDragScroll, edgoEnterShowEditor, edgoImmediateEditor, edgoShowHourGlass, edgoTabThrough, edgoVertThrough]
      OptionsDB = [edgoCanNavigation, edgoUseBookmarks]
      OptionsView = [edgoAutoWidth, edgoBandHeaderWidth, edgoRowSelect, edgoUseBitmap]
      OnBackgroundDrawEvent = dxGridBackgroundDrawEvent
      OnCustomDrawBand = dxGridCustomDrawBand
      OnCustomDrawCell = dxGridCustomDrawCell
      OnCustomDrawColumnHeader = dxGridCustomDrawColumnHeader
      OnEndColumnsCustomizing = dxGridEndColumnsCustomizing
    end
  end
  object pnlDetail: TPanel [2]
    Left = 0
    Top = 406
    Width = 650
    Height = 165
    Align = alBottom
    TabOrder = 4
    OnExit = pnlDetailExit
  end
  object pnlDetailGrid: TPanel [3]
    Left = 0
    Top = 155
    Width = 650
    Height = 251
    Align = alClient
    Caption = 'Panel Detail Grid'
    TabOrder = 6
    object spltDetail: TSplitter
      Left = 1
      Top = 247
      Width = 648
      Height = 3
      Cursor = crVSplit
      Align = alBottom
      AutoSnap = False
      MinSize = 100
    end
    object dxDetailGrid: TdxDBGrid
      Left = 1
      Top = 1
      Width = 648
      Height = 246
      Bands = <
        item
        end>
      DefaultLayout = True
      HeaderPanelRowCount = 1
      SummaryGroups = <>
      SummarySeparator = ', '
      Align = alClient
      TabOrder = 0
      OnDblClick = dxDetailGridDblClick
      OnEnter = dxGridEnter
      HideSelectionTextColor = clWindowText
      LookAndFeel = lfFlat
      OptionsBehavior = [edgoAutoSort, edgoCaseInsensitive, edgoDragScroll, edgoEnterShowEditor, edgoImmediateEditor, edgoShowHourGlass, edgoTabThrough, edgoVertThrough]
      OptionsDB = [edgoCanNavigation, edgoUseBookmarks]
      OptionsView = [edgoAutoWidth, edgoBandHeaderWidth, edgoRowSelect, edgoUseBitmap]
      OnBackgroundDrawEvent = dxGridBackgroundDrawEvent
      OnCustomDrawBand = dxGridCustomDrawBand
      OnCustomDrawCell = dxGridCustomDrawCell
      OnCustomDrawColumnHeader = dxGridCustomDrawColumnHeader
      OnEndColumnsCustomizing = dxGridEndColumnsCustomizing
    end
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = False
        WholeRow = False
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <
          item
            Item = dxBarButtonFirst
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonPrior
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonNext
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonLast
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarBDBNavInsert
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavDelete
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavPost
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavCancel
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonEditMode
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSort
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonRecordDetails
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSearch
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCustCol
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonShowGroup
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExpand
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCollapse
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonResetColumns
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonExportAllHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportAllXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    Left = 560
    Top = 104
    DockControlHeights = (
      0
      0
      26
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarButtonEditMode: TdxBarButton
      OnClick = dxBarButtonEditModeClick
    end
    inherited dxBarButtonRecordDetails: TdxBarButton
      OnClick = dxBarButtonRecordDetailsClick
    end
    inherited dxBarButtonSort: TdxBarButton
      ButtonStyle = bsChecked
      OnClick = dxBarButtonSortClick
    end
    inherited dxBarButtonSearch: TdxBarButton
      ButtonStyle = bsChecked
      OnClick = dxBarButtonSearchClick
    end
    inherited dxBarButtonCustCol: TdxBarButton
      OnClick = dxBarButtonCustColClick
    end
    inherited dxBarButtonShowGroup: TdxBarButton
      OnClick = dxBarButtonShowGroupClick
    end
    inherited dxBarButtonExpand: TdxBarButton
      OnClick = dxBarButtonExpandClick
    end
    inherited dxBarBDBNavInsert: TdxBarDBNavButton
      OnClick = dxBarBDBNavInsertClick
    end
    inherited dxBarButtonExportAllHTML: TdxBarButton
      OnClick = dxBarButtonExportGridClick
    end
    inherited dxBarButtonExportSelectionHTML: TdxBarButton
      OnClick = dxBarButtonExportGridClick
    end
    inherited dxBarButtonExportAllXLS: TdxBarButton
      OnClick = dxBarButtonExportGridClick
    end
    inherited dxBarButtonExportSelectionXLS: TdxBarButton
      OnClick = dxBarButtonExportGridClick
    end
    inherited dxBarButtonFirst: TdxBarButton
      OnClick = dxBarButtonNavigationClick
    end
    inherited dxBarButtonPrior: TdxBarButton
      Tag = 1
      OnClick = dxBarButtonNavigationClick
    end
    inherited dxBarButtonNext: TdxBarButton
      Tag = 2
      OnClick = dxBarButtonNavigationClick
    end
    inherited dxBarButtonLast: TdxBarButton
      Tag = 3
      OnClick = dxBarButtonNavigationClick
    end
    inherited dxBarButtonCollapse: TdxBarButton
      OnClick = dxBarButtonCollapseClick
    end
    inherited dxBarButtonResetColumns: TdxBarButton
      OnClick = dxBarButtonResetColumnsClick
    end
  end
  inherited dxBarDBNavigator: TdxBarDBNavigator
    DataSource = dsrcActive
    Left = 448
    Top = 56
  end
  inherited StandardMenuActionList: TActionList
    Left = 560
    Top = 48
    inherited HelpOrderingInfoAct: TAction
      Caption = '&Order Information'
    end
    inherited HelpSolarHomePageAct: TAction
      Caption = 'Home Page'
    end
  end
  object dsrcActive: TDataSource
    Left = 16
    Top = 104
  end
end
