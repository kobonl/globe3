(*
  MRA:8-SEP-2016 PIM-213
  - New screens showing per workspot the efficiency and how the
    operator performs.
*)
unit UWorkspotEmpEff;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, ComCtrls, dxfProgressBar, ORASystemDMT,
  UPimsConst;

type
  TpnlBar = class(TPanel)
    pnlBarLabel: TPanel;
    lblBar: TLabel;
    pnlBarPrgBar: TPanel;
    dxfProgressBar: TdxfProgressBar;
  private
    FID: Integer;
  public
    constructor Create(AOwner: TComponent); override;
    constructor BarCreate(AOwner: TComponent; AID: Integer);
    destructor Destroy; override;
    property ID: Integer read FID write FID;
  end;

type
  TpnlEmpEff = class(TPanel)
    pnlCircle: TPanel;
    shpCircle: TShape;
    lblPerc: TLabel;
    pnlBars: TPanel;
    pnlBar1: TpnlBar;
    pnlBar2: TpnlBar;
    pnlBar3: TpnlBar;
    pnlBar4: TpnlBar;
    pnlEmpName: TPanel;
    pnlListButton: TPanel;
    pnlGraphButton: TPanel;
  private
    FEmpName: String;
    FEmpNumber: Integer;
    procedure AddBar(AID: Integer);
  public
    constructor Create(AOwner: TComponent); override;
    constructor EmpCreate(AOwner: TComponent; AEmpNumber: Integer;
      AEmpName: String; AListButton: TBitBtn; AGraphButton: TBitBtn);
    destructor Destroy; override;
    procedure AddBars;
    procedure AssignButtons(AListButton: TBitBtn; AGraphButton: TBitBtn);
    property EmpNumber: Integer read FEmpNumber write FEmpNumber;
    property EmpName: String read FEmpName write FEmpName;
  end;

type
  TpnlWSEmpEff = class(TPanel)
    pnlEmps: TPanel;
    pnlWSSummary: TPanel;
    pnlWSTime: TPanel;
    lblDate: TLabel;
    lblTime: TLabel;
    lblWorkspot: TLabel;
    lblPieces: TLabel;
    pnlJobButton: TPanel;
    pnlInButton: TPanel;
  private
    FWorkspotCode: String;
    FPlantCode: String;
    FDescription: String;
    FWorkspotScale: Integer;
    FFontScale: Integer;
    FPersonalScreen: Boolean;
    procedure ShowEmpButtons(AEmpNumber: Integer);
  public
    EmpList: TList;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    constructor WSCreate(AOwner: TComponent; APlantCode, AWorkspotCode,
      ADescription: String; AWorkspotScale, AFontScale: Integer;
      AOnClick: TNotifyEvent;
      AOnMouseDown: TMouseEvent; AOnMouseMove: TMouseMoveEvent;
      AOnMouseUp: TMouseEvent;
      ATag: Integer;
      APersonalScreen: Boolean=False);
    function FindEmployee(AEmpNumber: Integer): TpnlEmpEff;
    function FindEmployeeIndex(AEmpNumber: Integer): Integer;
    procedure AddEmployee(AEmpNumber: Integer; AEmpName: String;
      AOnClick: TNotifyEvent;
      AOnMouseDown: TMouseEvent; AOnMouseMove: TMouseMoveEvent;
      AOnMouseUp: TMouseEvent;
      ATag: Integer;
      AListButton: TBitBtn=nil;
      AGraphButton: TBitBtn=nil);
    procedure EmployeeAssignButtons(AEmpNumber: Integer;
      AListButton: TBitBtn; AGraphButton: TBitBtn);
    procedure DelEmployee(AEmpNumber: Integer);
    procedure DelEmployees;
    procedure RefreshEmployees;
    procedure ShowEff(AEmpNumber: Integer; A15Min, A30Min, A60Min, AToday: Integer);
    procedure ShowEmpName(AEmpNumber: Integer);
    procedure HideEmpName(AEmpNumber: Integer);
    procedure ShowTime;
    procedure ShowSummary(AQty: Integer);
    function FontSize(AFontSize: Integer): Integer;
    procedure Renumber(ATag: Integer);
    procedure EditMode(AOn: Boolean);
    property PlantCode: String read FPlantCode write FPlantCode;
    property WorkspotCode: String read FWorkspotCode write FWorkspotCode;
    property Description: String read FDescription write FDescription;
    property WorkspotScale: Integer read FWorkspotScale write FWorkspotScale default 100;
    property FontScale: Integer read FFontScale write FFontScale default 100;
    property PersonalScreen: Boolean read FPersonalScreen write FPersonalScreen;
  end;

var
  MyOnClick: TNotifyEvent;
  MyOnMouseDown: TMouseEvent;
  MyOnMouseMove: TMouseMoveEvent;
  MyOnMouseUp: TMouseEvent;
  MyTag: Integer;
  MyWorkspotScale: Integer;
  MyFontScale: Integer;
  MyPersonalScreen: Boolean;
  WSMinWidth: Integer = 160;
  WSMinHeight: Integer = 449;
  WSMinFontSize: Integer = 12;

implementation

uses
  UPimsMessageRes;

{ TpnlEmpEff }

procedure TpnlEmpEff.AddBar(AID: Integer);
begin
  case AID of
  1:
    begin
      Self.pnlBar1 := TpnlBar.BarCreate(Self, AID);
      Self.pnlBar1.lblBar.Caption := SPims15Min;
    end;
  2:
    begin
      Self.pnlBar2 := TpnlBar.BarCreate(Self, AID);
      Self.pnlBar2.lblBar.Caption := SPims30Min;
    end;
  3:
    begin
      Self.pnlBar3 := TpnlBar.BarCreate(Self, AID);
      Self.pnlBar3.lblBar.Caption := SPims60Min;
    end;
  4:
    begin
      Self.pnlBar4 := TpnlBar.BarCreate(Self, AID);
      Self.pnlBar4.lblBar.Caption := SPimsToday;
    end;
  end;
end; // AddBar

procedure TpnlEmpEff.AddBars;
begin
  AddBar(1);
  AddBar(2);
  AddBar(3);
  AddBar(4);
end;

constructor TpnlEmpEff.Create(AOwner: TComponent);
  function MyDimension: Integer;
  begin
    if (AOwner as TpnlWSEmpEff).PersonalScreen then
      Result := 8
    else
      Result := 7;
  end;
begin
  inherited Create(AOwner);

  Self.OnClick := MyOnClick;
  Self.OnMouseDown := MyOnMouseDown;
  Self.OnMouseMove := MyOnMouseMove;
  Self.OnMouseUp := MyOnMouseUp;
  Self.Tag := MyTag;

  Parent := (AOwner as TpnlWSEmpEff);
  Parent.Width := Round(((AOwner as TpnlWSEmpEff).EmpList.Count+1) * WSMinWidth);
  Top := 0;
  Left := Round((AOwner as TpnlWSEmpEff).EmpList.Count *
    (AOwner as TpnlWSEmpEff).pnlEmps.Width);
  Height := (AOwner as TpnlWSEmpEff).pnlEmps.Height;
  Width := (AOwner as TpnlWSEmpEff).pnlEmps.Width;

  // Circle
  pnlCircle := TPanel.Create(Self);
  with pnlCircle do
  begin
    Parent := Self;
    Visible := True;
    Top := 0;
    Left := 0;
    Width := Self.Width;
    Height := Round((AOwner as TpnlWSEmpEff).pnlEmps.Height * 3/MyDimension);
    OnClick := Self.OnClick;
    OnMouseDown := Self.OnMouseDown;
    OnMouseMove := Self.OnMouseMove;
    OnMouseUp := Self.OnMouseUp;
    Tag := Self.Tag;
  end;
  shpCircle := TShape.Create(pnlCircle);
  with shpCircle do
  begin
    Parent := pnlCircle;
    Visible := True;
    Top := 0;
    Left := 0;
    Height := pnlCircle.Height;
    Width := pnlCircle.Width;
    Shape := stCircle;
    Brush.Color := clRed;
    OnClick := Self.OnClick;
    OnMouseDown := Self.OnMouseDown;
    OnMouseMove := Self.OnMouseMove;
    OnMouseUp := Self.OnMouseUp;
    Tag := Self.Tag;
  end;
  lblPerc := TLabel.Create(pnlCircle);
  with lblPerc do
  begin
    Parent := pnlCircle;
    Visible := True;
    Height := Round(pnlCircle.Height / 3);
    Width := Round(pnlCircle.Width / 3);
    Top := Round(shpCircle.Height / 2 - lblPerc.Height / 2);
    Left := Round(shpCircle.Width / 2 - lblPerc.Width / 2);
    Font.Size := (AOwner as TpnlWSEmpEff).FontSize(25);
    Font.Style := [fsBold];
    Color := shpCircle.Brush.Color;
    Caption := '0 %';
    OnClick := Self.OnClick;
    OnMouseDown := Self.OnMouseDown;
    OnMouseMove := Self.OnMouseMove;
    OnMouseUp := Self.OnMouseUp;
    Tag := Self.Tag;
  end;

  // Bars
  pnlBars := TPanel.Create(Self);
  with pnlBars do
  begin
    Parent := Self;
    Visible := True;
    Top := pnlCircle.Height;
    Left := 0;
    Width := Self.Width;
    Height := pnlCircle.Height;
    OnClick := Self.OnClick;
    OnMouseDown := Self.OnMouseDown;
    OnMouseMove := Self.OnMouseMove;
    OnMouseUp := Self.OnMouseUp;
    Tag := Self.Tag;
  end;
  Self.AddBars;

  // Employee Name
  pnlEmpName := TPanel.Create(Self);
  with pnlEmpName do
  begin
    Parent := Self;
    Visible := True;
    Top := pnlCircle.Height + pnlBars.Height;
    Left := 0;
    Width := Self.Width;
    Height := Round((AOwner as TpnlWSEmpEff).pnlEmps.Height * 1/MyDimension);
    Font.Size := (AOwner as TpnlWSEmpEff).FontSize(12);
    Font.Style := [fsBold];
    OnClick := Self.OnClick;
    OnMouseDown := Self.OnMouseDown;
    OnMouseMove := Self.OnMouseMove;
    OnMouseUp := Self.OnMouseUp;
    Tag := Self.Tag;
  end;

  if (AOwner as TpnlWSEmpEff).PersonalScreen then
  begin
    pnlListButton := TPanel.Create(Self);
    with pnlListButton do
    begin
      Parent := Self;
      Visible := True;
      Top := pnlCircle.Height + pnlBars.Height + pnlEmpName.Height;
      Left := 0;
      Width := Round(Self.Width / 2);
      Height := Round((AOwner as TpnlWSEmpEff).pnlEmps.Height * 1/MyDimension);
      Font.Size := (AOwner as TpnlWSEmpEff).FontSize(12);
      Font.Style := [fsBold];
//      OnClick := Self.OnClick;
//      OnMouseDown := Self.OnMouseDown;
//      OnMouseMove := Self.OnMouseMove;
//      OnMouseUp := Self.OnMouseUp;
      Tag := Self.Tag;
    end;
    pnlGraphButton := TPanel.Create(Self);
    with pnlGraphButton do
    begin
      Parent := Self;
      Visible := True;
      Top := pnlCircle.Height + pnlBars.Height + pnlEmpName.Height;
      Left := Round(Self.Width / 2);
      Width := Round(Self.Width / 2);
      Height := Round((AOwner as TpnlWSEmpEff).pnlEmps.Height * 1/MyDimension);
      Font.Size := (AOwner as TpnlWSEmpEff).FontSize(12);
      Font.Style := [fsBold];
//      OnClick := Self.OnClick;
//      OnMouseDown := Self.OnMouseDown;
//      OnMouseMove := Self.OnMouseMove;
//      OnMouseUp := Self.OnMouseUp;
      Tag := Self.Tag;
    end;
  end;
end; // Create

destructor TpnlEmpEff.Destroy;
begin
  Self.lblPerc.Visible := False;
  Self.lblPerc.Free;
  Self.shpCircle.Visible := False;
  Self.shpCircle.Free;
  Self.pnlCircle.Visible := False;
  Self.pnlCircle.Free;
  Self.pnlEmpName.Visible := False;
  Self.pnlEmpName.Free;
  Self.pnlBar1.Visible := False;
  Self.pnlBar1.Free;
  Self.pnlBar2.Visible := False;
  Self.pnlBar2.Free;
  Self.pnlBar3.Visible := False;
  Self.pnlBar3.Free;
  Self.pnlBar4.Visible := False;
  Self.pnlBar4.Free;
  Self.pnlBars.Visible := False;
  Self.pnlBars.Free;
  if Assigned(Self.pnlListButton) then
  begin
//    Self.pnlListButton.Visible := False;
    Self.pnlListButton.Free;
  end;
  if Assigned(Self.pnlGraphButton) then
  begin
//    Self.pnlGraphButton.Visible := False;
    Self.pnlGraphButton.Free;
  end;
  inherited;
end; // Destroy

procedure TpnlEmpEff.AssignButtons(AListButton, AGraphButton: TBitBtn);
begin
  if Assigned(AListButton) then
  begin
    with AListButton do
    begin
      if Parent <> Self.pnlListButton then
      begin
        Parent := Self.pnlListButton;
        Top := 0;
        Left := 0;
        Width := Round(Self.Width / 2);
        Height := Self.pnlListButton.Height;
        Align := alClient;
//        Visible := True;
      end;
    end;
  end;
  if Assigned(AGraphButton) then
  begin
    with AGraphButton do
    begin
      if Parent <> Self.pnlGraphButton then
      begin
        Parent := Self.pnlGraphButton;
        Top := 0;
        Left := 0;
        Width := Round(Self.Width / 2);
        Height := Self.pnlGraphButton.Height;
        Align := alClient;
//        Visible := True;
      end;
    end;
  end;
end; // AssignButtons

constructor TpnlEmpEff.EmpCreate(AOwner: TComponent; AEmpNumber: Integer;
  AEmpName: String;  AListButton: TBitBtn; AGraphButton: TBitBtn);
begin
  Self.Create(AOwner);
  Self.EmpNumber := AEmpNumber;
  Self.EmpName := AEmpName;
  Self.pnlEmpName.Caption := AEmpName;
  AssignButtons(AListButton, AGraphButton);
end; // EmpCreate

{ TpnlWSEmpEff }

constructor TpnlWSEmpEff.Create(AOwner: TComponent);
  function MyDimension: Integer;
  begin
    if Self.PersonalScreen then
      Result := 12
    else
      Result := 10;
  end;
begin
  inherited Create(AOwner);
  Self.WorkspotScale := MyWorkspotScale;
  Self.FontScale := MyFontScale;
  Self.OnClick := MyOnClick;
  Self.OnMouseDown := MyOnMouseDown;
  Self.OnMouseMove := MyOnMouseMove;
  Self.OnMouseUp := MyOnMouseUp;
  Self.Tag := MyTag;
  Self.PersonalScreen := MyPersonalScreen;
  Constraints.MinWidth := WSMinWidth;
  EmpList := TList.Create;
  Top := 10;
  Left := 10;
  if Round(WSMinWidth * Self.WorkspotScale / 100) > WSMinWidth then
    Width := Round(WSMinWidth * Self.WorkspotScale / 100)
  else
    Width := WSMinWidth;
  if PersonalScreen then
    WSMinHeight := 572;
  if Round(WSMinHeight * Self.WorkspotScale / 100) > WSMinHeight then
    Height := Round(WSMinHeight * Self.WorkspotScale / 100)
  else
    Height := WSMinHeight;
  Visible := True;
  Parent := (AOwner as TPanel);
  pnlEmps := TPanel.Create(Self);
  with pnlEmps do
  begin
    Parent := Self;
    Top := 0;
    Left := 0;
    Width := Self.Width;
    Height := Round(Self.Height * 8/MyDimension);
    OnClick := Self.OnClick;
    OnMouseDown := Self.OnMouseDown;
    OnMouseMove := Self.OnMouseMove;
    OnMouseUp := Self.OnMouseUp;
    Tag := Self.Tag;
    Visible := True;
  end;
  pnlWSTime := TPanel.Create(Self);
  with pnlWSTime do
  begin
    Parent := Self;
    Top := 366;
    Left := 0;
    Width := Self.Width;
    Height := Round(Self.Height * 1/MyDimension);
    Visible := True;
    Font.Size := Self.FontSize(12);
    Font.Style := [fsBold];
    Caption := '';
    Align := alBottom;
    OnClick := Self.OnClick;
    OnMouseDown := Self.OnMouseDown;
    OnMouseMove := Self.OnMouseMove;
    OnMouseUp := Self.OnMouseUp;
    Tag := Self.Tag;
  end;
  lblDate := TLabel.Create(Self);
  with lblDate do
  begin
    Parent := pnlWSTime;
    Alignment := taCenter;
    Visible := True;
    Top := 1;
    Left := 6;
    Height := Round(pnlWSTime.Height / 2);
    Width := pnlWSTime.Width;
    Font.Size := Self.FontSize(10);
    OnClick := Self.OnClick;
    OnMouseDown := Self.OnMouseDown;
    OnMouseMove := Self.OnMouseMove;
    OnMouseUp := Self.OnMouseUp;
    Tag := Self.Tag;
    BringToFront;
  end;
  lblTime := TLabel.Create(Self);
  with lblTime do
  begin
    Parent := pnlWSTime;
    Alignment := taCenter;
    Visible := True;
    Top := Round(pnlWSTime.Height / 2);
    Left := 6;
    Height := Round(pnlWSTime.Height / 2);
    Width := pnlWSTime.Width;
    Font.Size := Self.FontSize(10);
    OnClick := Self.OnClick;
    OnMouseDown := Self.OnMouseDown;
    OnMouseMove := Self.OnMouseMove;
    OnMouseUp := Self.OnMouseUp;
    Tag := Self.Tag;
    BringToFront;
  end;
  pnlWSSummary := TPanel.Create(Self);
  with pnlWSSummary do
  begin
    Parent := Self;
    Top := 407;
    Left := 0;
    Width := Self.Width;
    Height := Round(Self.Height * 1/MyDimension);
    Font.Size := Self.FontSize(12);
    Font.Style := [fsBold];
    OnClick := Self.OnClick;
    OnMouseDown := Self.OnMouseDown;
    OnMouseMove := Self.OnMouseMove;
    OnMouseUp := Self.OnMouseUp;
    Tag := Self.Tag;
    Visible := True;
    Align := alBottom;
  end;
  lblWorkspot := TLabel.Create(Self);
  with lblWorkspot do
  begin
    Parent := pnlWSSummary;
    Alignment := taCenter;
    Visible := True;
    Top := 1;
    Left := 6;
    Height := Round(pnlWSSummary.Height / 2);
    Width := pnlWSSummary.Width;
    Font.Size := Self.FontSize(10);
    OnClick := Self.OnClick;
    OnMouseDown := Self.OnMouseDown;
    OnMouseMove := Self.OnMouseMove;
    OnMouseUp := Self.OnMouseUp;
    Tag := Self.Tag;
    BringToFront;
  end;
  lblPieces := TLabel.Create(Self);
  with lblPieces do
  begin
    Parent := pnlWSSummary;
    Alignment := taCenter;
    Visible := True;
    Top := Round(pnlWSSummary.Height / 2);
    Left := 6;
    Height := Round(pnlWSSummary.Height / 2);
    Width := pnlWSSummary.Width;
    Font.Size := Self.FontSize(10);
    OnClick := Self.OnClick;
    OnMouseDown := Self.OnMouseDown;
    OnMouseMove := Self.OnMouseMove;
    OnMouseUp := Self.OnMouseUp;
    Tag := Self.Tag;
    BringToFront;
  end;
  with pnlEmps do
  begin
//    Align := alClient;
  end;
  if Self.PersonalScreen then
  begin
    pnlJobButton := TPanel.Create(Self);
    with pnlJobButton do
    begin
      Parent := Self;
      Top := 448;
      Left := 0;
      Width := Self.Width;
      Height := Round(Self.Height * 1/MyDimension);
      Font.Size := Self.FontSize(12);
      Font.Style := [fsBold];
//      OnClick := Self.OnClick;
//      OnMouseDown := Self.OnMouseDown;
//      OnMouseMove := Self.OnMouseMove;
//      OnMouseUp := Self.OnMouseUp;
      Tag := Self.Tag;
      Visible := True;
      Align := alBottom;
    end;
    pnlInButton := TPanel.Create(Self);
    with pnlInButton do
    begin
      Parent := Self;
      Top := 489;
      Left := 0;
      Width := Self.Width;
      Height := Round(Self.Height * 1/MyDimension);
      Font.Size := Self.FontSize(12);
      Font.Style := [fsBold];
//      OnClick := Self.OnClick;
//      OnMouseDown := Self.OnMouseDown;
//      OnMouseMove := Self.OnMouseMove;
//      OnMouseUp := Self.OnMouseUp;
      Tag := Self.Tag;
      Visible := True;
      Align := alBottom;
    end;
  end;
  with pnlEmps do
  begin
//    Align := alClient;
  end;
end; // Create

constructor TpnlWSEmpEff.WSCreate(AOwner: TComponent; APlantCode,
  AWorkspotCode, ADescription: String; AWorkspotScale, AFontScale: Integer;
  AOnClick: TNotifyEvent;
  AOnMouseDown: TMouseEvent; AOnMouseMove: TMouseMoveEvent;
  AOnMouseUp: TMouseEvent;
  ATag: Integer;
  APersonalScreen: Boolean=False);
begin
  MyWorkspotScale := AWorkspotScale;
  MyFontScale := AFontScale;
  MyOnClick := AOnClick;
  MyOnMouseDown := AOnMouseDown;
  MyOnMouseMove := AOnMouseMove;
  MyOnMouseUp := AOnMouseUp;
  MyTag := ATag;
  MyPersonalScreen := APersonalScreen;
  Self.Create(AOwner);
  PlantCode := APlantCode;
  WorkspotCode := AWorkspotCode;
  Description := ADescription;
  Self.pnlWSSummary.Caption := Description;
end;

destructor TpnlWSEmpEff.Destroy;
begin
  Self.pnlEmps.Visible := False;
  Self.pnlEmps.Free;
  Self.lblWorkspot.Visible := False;
  Self.lblWorkspot.Free;
  Self.lblPieces.Visible := False;
  Self.lblPieces.Free;
  Self.pnlWSSummary.Visible := False;
  Self.pnlWSSummary.Free;
  Self.lblDate.Visible := False;
  Self.lblDate.Free;
  Self.lblTime.Visible := False;
  Self.lblTime.Free;
  Self.pnlWSTime.Visible := False;
  Self.pnlWSTime.Free;
  if Assigned(Self.pnlJobButton) then
  begin
    Self.pnlJobButton.Visible := False;
    Self.pnlJobButton.Free;
  end;
  if Assigned(Self.pnlInButton) then
  begin
    Self.pnlInButton.Visible := False;
    Self.pnlInButton.Free;
  end;
  EmpList.Clear;
  EmpList.Free;
  inherited;
end; // Destroy

function TpnlWSEmpEff.FindEmployee(AEmpNumber: Integer): TpnlEmpEff;
var
  ApnlEmpEff: TpnlEmpEff;
  I: Integer;
begin
  Result := nil;
  if not Assigned(EmpList) then
    Exit;
  for I := 0 to EmpList.Count - 1 do
  begin
    ApnlEmpEff := EmpList.Items[I];
    if ApnlEmpEff.EmpNumber = AEmpNumber then
    begin
      Result := ApnlEmpEff;
      Break;
    end;
  end;
end; // FindEmployee

function TpnlWSEmpEff.FindEmployeeIndex(AEmpNumber: Integer): Integer;
var
  ApnlEmpEff: TpnlEmpEff;
  I: Integer;
begin
  Result := -1;
  if not Assigned(EmpList) then
    Exit;
  for I := 0 to EmpList.Count - 1 do
  begin
    ApnlEmpEff := EmpList.Items[I];
    if ApnlEmpEff.EmpNumber = AEmpNumber then
    begin
      Result := I;
      Break;
    end;
  end;
end; // FindEmployeeIndex

procedure TpnlWSEmpEff.AddEmployee(AEmpNumber: Integer; AEmpName: String;
  AOnClick: TNotifyEvent;
  AOnMouseDown: TMouseEvent; AOnMouseMove: TMouseMoveEvent;
  AOnMouseUp: TMouseEvent;
  ATag: Integer;
  AListButton: TBitBtn=nil;
  AGraphButton: TBitBtn=nil);
var
  ApnlEmpEff: TpnlEmpEff;
begin
  if not Assigned(FindEmployee(AEmpNumber)) then
  begin
    MyOnClick := AOnClick;
    MyOnMouseDown := AOnMouseDown;
    MyOnMouseMove := AOnMouseMove;
    MyOnMouseUp := AOnMouseUp;
    MyTag := ATag;
    ApnlEmpEff := TpnlEmpEff.EmpCreate(Self, AEmpNumber, AEmpName,
      AListButton, AGraphButton);
    ApnlEmpEff.pnlEmpName.Caption := AEmpName;
    EmpList.Add(ApnlEmpEff);
  end;
end;

procedure TpnlWSEmpEff.DelEmployee(AEmpNumber: Integer);
var
  ApnlEmpEff: TpnlEmpEff;
begin
  ApnlEmpEff := FindEmployee(AEmpNumber);
  if Assigned(ApnlEmpEff) then
  begin
    ApnlEmpEff.shpCircle.Visible := False;
    ApnlEmpEff.pnlCircle.Visible := False;
    ApnlEmpEff.lblPerc.Visible := False;
    ApnlEmpEff.pnlBars.Visible := False;
    ApnlEmpEff.pnlEmpName.Visible := False;
    ApnlEmpEff.Visible := False;
    EmpList.Remove(ApnlEmpEff);
  end;
end;

procedure TpnlWSEmpEff.RefreshEmployees;
var
  ApnlEmpEff: TpnlEmpEff;
  I: Integer;
begin
  if not Assigned(EmpList) then
    Exit;
  if EmpList.Count > 0 then
    Self.Width := Round(EmpList.Count * WSMinWidth)
  else
    Self.Width := WSMinWidth;
  for I := 0 to EmpList.Count - 1 do
  begin
    ApnlEmpEff := EmpList.Items[I];
    ApnlEmpEff.Top := 0;
    ApnlEmpEff.Left := Round(I * ApnlEmpEff.pnlEmpName.Width {Self.pnlEmps.Width});
  end;
end; // RefreshEmployees

procedure TpnlWSEmpEff.ShowEff(AEmpNumber: Integer; A15Min, A30Min, A60Min,
  AToday: Integer);
var
  ApnlEmpEff: TpnlEmpEff;
  function MyColor(AValue: Integer): TColor;
  begin
    if (AValue <= ORASystemDM.RedBoundary) then
      Result := clMyRed
    else
      if (AValue <= ORASystemDM.OrangeBoundary) then
        Result := clMyOrange
      else
        Result := clMyGreen;
  end; // MyColor
begin
  if not Assigned(Self) then
    Exit;
  ApnlEmpEff := FindEmployee(AEmpNumber);
  if Assigned(ApnlEmpEff) then
  begin
    ApnlEmpEff.Top := 0;
    ApnlEmpEff.lblPerc.Caption := IntToStr(A15Min) + ' %';
    ApnlEmpEff.lblPerc.Color := MyColor(A15Min);
    ApnlEmpEff.lblPerc.Left :=
      Round(ApnlEmpEff.shpCircle.Width / 2 - ApnlEmpEff.lblPerc.Width / 2);
    ApnlEmpEff.shpCircle.Brush.Color := MyColor(A15Min);
    ApnlEmpEff.pnlBar1.dxfProgressBar.Position := A15Min;
    ApnlEmpEff.pnlBar1.dxfProgressBar.BeginColor := MyColor(A15Min);
    ApnlEmpEff.pnlBar1.dxfProgressBar.EndColor := MyColor(A15Min);
    ApnlEmpEff.pnlBar2.dxfProgressBar.Position := A30Min;
    ApnlEmpEff.pnlBar2.dxfProgressBar.BeginColor := MyColor(A30Min);
    ApnlEmpEff.pnlBar2.dxfProgressBar.EndColor := MyColor(A30Min);
    ApnlEmpEff.pnlBar3.dxfProgressBar.Position := A60Min;
    ApnlEmpEff.pnlBar3.dxfProgressBar.BeginColor := MyColor(A60Min);
    ApnlEmpEff.pnlBar3.dxfProgressBar.EndColor := MyColor(A60Min);
    ApnlEmpEff.pnlBar4.dxfProgressBar.Position := AToday;
    ApnlEmpEff.pnlBar4.dxfProgressBar.BeginColor := MyColor(AToday);
    ApnlEmpEff.pnlBar4.dxfProgressBar.EndColor := MyColor(AToday);
    ApnlEmpEff.Left := Round(FindEmployeeIndex(AEmpNumber) * Self.pnlEmps.Width);
  end;
  if Assigned(Self.EmpList) then
  begin
    if Self.EmpList.Count > 0 then
      Self.Width := Self.EmpList.Count * Self.pnlEmps.Width
    else
      Self.Width := WSMinWidth;
  end;
end; // ShowEff

procedure TpnlWSEmpEff.ShowEmpButtons(AEmpNumber: Integer);
var
  ApnlEmpEff: TpnlEmpEff;
begin
  if not Assigned(Self) then
    Exit;
  ApnlEmpEff := FindEmployee(AEmpNumber);
  if Assigned(ApnlEmpEff) then
  begin
    ApnlEmpEff.pnlListButton.Width := Round(ApnlEmpEff.pnlEmpName.Width / 2);
    ApnlEmpEff.pnlGraphButton.Width := Round(ApnlEmpEff.pnlEmpName.Width / 2);
  end;
end; // ShowEmpButtons

procedure TpnlWSEmpEff.ShowEmpName(AEmpNumber: Integer);
var
  ApnlEmpEff: TpnlEmpEff;
begin
  if not Assigned(Self) then
    Exit;
  ApnlEmpEff := FindEmployee(AEmpNumber);
  if Assigned(ApnlEmpEff) then
  begin
    ApnlEmpEff.pnlEmpName.Caption := ApnlEmpEff.EmpName;
  end;
  if Self.PersonalScreen then
    ShowEmpButtons(AEmpNumber);
end; // ShowEmpName

procedure TpnlWSEmpEff.HideEmpName(AEmpNumber: Integer);
var
  ApnlEmpEff: TpnlEmpEff;
begin
  if not Assigned(Self) then
    Exit;
  ApnlEmpEff := FindEmployee(AEmpNumber);
  if Assigned(ApnlEmpEff) then
  begin
    ApnlEmpEff.pnlEmpName.Caption := '-';
  end;
end; // ShowEmpName

procedure TpnlWSEmpEff.DelEmployees;
var
  ApnlEmpEff: TpnlEmpEff;
  I: Integer;
begin
  if not Assigned(EmpList) then
    Exit;
  // First delete the existing EmpList
  for I := EmpList.Count - 1 downto 0 do
  begin
    ApnlEmpEff := EmpList.Items[I];
    ApnlEmpEff.Visible := False;
    DelEmployee(ApnlEmpEff.EmpNumber);
    EmpList.Remove(ApnlEmpEff);
  end;
  EmpList.Clear;
end; // DelEmployees

procedure TpnlWSEmpEff.ShowTime;
begin
  if Assigned(Self) then
    if Assigned(Self.pnlWSTime) then
    begin
      if Self.EmpList.Count > 1 then
      begin
        if Assigned(lblDate) and Assigned(lblTime) then
        begin
          Self.lblDate.Visible := False;
          Self.lblTime.Visible := False;
          Self.pnlWSTime.Caption := DateTimeToStr(Now);
        end;
      end
      else
      begin
        if Assigned(lblDate) and Assigned(lblTime) then
        begin
          Self.pnlWSTime.Caption := '';
          Self.lblDate.Visible := True;
          Self.lblTime.Visible := True;
          Self.lblDate.Caption := DateToStr(Now);
          Self.lblDate.Height := Round(pnlWSTime.Height / 2);
          Self.lblDate.Width := pnlWSTime.Width;
          Self.lblTime.Caption := TimeToStr(Now);
          Self.lblTime.Top := Round(pnlWSTime.Height / 2);
          Self.lblTime.Height := Round(pnlWSTime.Height / 2);
          Self.lblTime.Width := pnlWSTime.Width;
        end;
      end;
    end;
end;

procedure TpnlWSEmpEff.ShowSummary(AQty: Integer);
begin
  if Assigned(Self) then
    if Assigned(Self.pnlWSSummary) then
    begin
      if Self.EmpList.Count > 1 then
      begin
        if Assigned(lblWorkspot) and Assigned(lblPieces) then
        begin
          Self.lblWorkspot.Visible := False;
          Self.lblPieces.Visible := False;
          if AQty > 0 then
            Self.pnlWSSummary.Caption :=
              Description + ' - ' + IntToStr(AQty) + ' ' + SPimsPcs
          else
            Self.pnlWSSummary.Caption := Description
        end;
      end
      else
      begin
        if AQty > 0 then
        begin
          if Assigned(lblWorkspot) and Assigned(lblPieces) then
          begin
            Self.pnlWSSummary.Caption := '';
            Self.lblWorkspot.Visible := True;
            Self.lblWorkspot.Height := Round(pnlWSSummary.Height / 2);
            Self.lblWorkspot.Width := pnlWSSummary.Width;
            Self.lblPieces.Visible := True;
            Self.lblWorkspot.Caption := Description;
            Self.lblPieces.Caption := IntToStr(AQty) + ' ' + SPimsPcs;
            Self.lblPieces.Top := Round(pnlWSSummary.Height / 2);
            Self.lblPieces.Height := Round(pnlWSSummary.Height / 2);
            Self.lblPieces.Width := pnlWSSummary.Width;
          end;
        end
        else
        begin
          if Assigned(lblWorkspot) and Assigned(lblPieces) then
          begin
            Self.lblWorkspot.Visible := False;
            Self.lblPieces.Visible := False;
            Self.pnlWSSummary.Caption := Description;
          end;
        end;
      end;
    end;
end;

function TpnlWSEmpEff.FontSize(AFontSize: Integer): Integer;
begin
  Result := Round(AFontSize * FontScale / 100);
  if Result < AFontSize then
    Result := AFontSize;
end;

procedure TpnlWSEmpEff.Renumber(ATag: Integer);
var
  I: Integer;
  ApnlEmpEff: TpnlEmpEff;
begin
  Self.Tag := ATag;
  Self.pnlEmps.Tag := ATag;
  Self.pnlWSSummary.Tag := ATag;
  Self.pnlWSTime.Tag := ATag;
  Self.lblDate.Tag := ATag;
  Self.lblTime.Tag := ATag;
  Self.lblWorkspot.Tag := ATag;
  Self.lblPieces.Tag := ATag;
  if Self.PersonalScreen then
  begin
    Self.pnlJobButton.Tag := ATag;
    Self.pnlInButton.Tag := ATag;
  end;
  if Assigned(Self.EmpList) then
    for I := 0 to Self.EmpList.Count - 1 do
    begin
      ApnlEmpEff := Self.EmpList.Items[I];
      ApnlEmpEff.pnlCircle.Tag := ATag;
      ApnlEmpEff.shpCircle.Tag := ATag;
      ApnlEmpEff.lblPerc.Tag := ATag;
      ApnlEmpEff.pnlEmpName.Tag := ATag;
    end;
end; // Renumber

procedure TpnlWSEmpEff.EditMode(AOn: Boolean);
var
  I: Integer;
  ApnlEmpEff: TpnlEmpEff;
begin
  Self.lblDate.Visible := not AOn;
  Self.lblTime.Visible := not AOn;
  Self.lblWorkspot.Visible := not AOn;
  Self.lblPieces.Visible := not AOn;
  if Assigned(Self.EmpList) then
    for I := 0 to Self.EmpList.Count - 1 do
    begin
      ApnlEmpEff := Self.EmpList.Items[I];
      ApnlEmpEff.shpCircle.Visible := not AOn;
      ApnlEmpEff.lblPerc.Visible := not AOn;
    end;
end; // EditMode

procedure TpnlWSEmpEff.EmployeeAssignButtons(AEmpNumber: Integer;
  AListButton, AGraphButton: TBitBtn);
var
  ApnlEmpEff: TpnlEmpEff;
begin
  if not Assigned(Self) then
    Exit;
  ApnlEmpEff := FindEmployee(AEmpNumber);
  if Assigned(ApnlEmpEff) then
  begin
    ApnlEmpEff.AssignButtons(AListButton, AGraphButton);
  end;
end; // EmployeeAssignButtons

{ TpnlBar }

constructor TpnlBar.BarCreate(AOwner: TComponent; AID: Integer);
begin
  Self.Create(AOwner);
  Self.ID := AID;
  Self.Top := Round((AID-1) * Self.Height);
  Self.lblBar.Caption := IntToStr(AID);
end;

constructor TpnlBar.Create(AOwner: TComponent);
begin
  inherited;
  Parent := (AOwner as TpnlEmpEff).pnlBars;
  Visible := True;
  Top := 0;
  Left := 0;
  Height := Round(Parent.Height / 4);
  Width := Parent.Width;
  pnlBarLabel := TPanel.Create(Self);
  with pnlBarLabel do
  begin
    Parent := Self;
    Visible := True;
    Top := 1;
    Left := 1;
    Height := Self.Height;
    Width := Round(Self.Width * 1/3);
    BevelOuter := TBevelCut(bvNone);
  end;
  pnlBarPrgBar := TPanel.Create(Self);
  with pnlBarPrgBar do
  begin
    Parent := Self;
    Visible := True;
    Top := 1;
    Left := Round(Self.Width * 1/3);
    Height := Self.Height;
    Width := Round(Self.Width * 2/3);
    BevelOuter := TBevelCut(bvNone);
  end;
  lblBar := TLabel.Create(Self);
  with lblBar do
  begin
    Parent := pnlBarLabel;
    Visible := True;
    Top := 11;
    Left := 6;
    Font.Style := [fsBold];
    Height := pnlBarLabel.Height;
    Width := pnlBarLabel.Width;
    Font.Size := ((AOwner as TpnlEmpEff).Parent as TpnlWSEmpEff).FontSize(10);
  end;
  dxfProgressBar := TdxfProgressBar.Create(Self);
  with dxfProgressBar do
  begin
    Parent := pnlBarPrgBar;
    Visible := True;
    Top := 0;
    Left := 0;
    Height := pnlBarPrgBar.Height;
    Width := pnlBarPrgBar.Width;
    Style := sExSolid;
    BeginColor := clGreen;
    EndColor := clGreen;
    ShowText := True;
    Step := 1;
    Font.Size := ((AOwner as TpnlEmpEff).Parent as TpnlWSEmpEff).FontSize(10);
    Font.Style := [fsBold];
  end;
end; // Create

destructor TpnlBar.Destroy;
begin
  Self.lblBar.Visible := False;
  Self.lblBar.Free;
  Self.pnlBarLabel.Visible := False;
  Self.pnlBarLabel.Free;
  Self.dxfProgressBar.Visible := False;
  Self.dxfProgressBar.Free;
  Self.pnlBarPrgBar.Visible := False;
  Self.pnlBarPrgBar.Free;
  inherited;
end;

end.
