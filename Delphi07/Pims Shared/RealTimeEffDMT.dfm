object RealTimeEffDM: TRealTimeEffDM
  OldCreateOrder = False
  Left = 507
  Top = 251
  Height = 434
  Width = 680
  object oqMergeWSEffPerMinute: TOracleQuery
    SQL.Strings = (
      'MERGE INTO WORKSPOTEFFPERMINUTE W'
      'USING DUAL'
      'ON (W.PLANT_CODE = :PLANT_CODE AND'
      '  W.SHIFT_DATE = :SHIFT_DATE AND'
      '  W.SHIFT_NUMBER = :SHIFT_NUMBER AND'
      '  W.WORKSPOT_CODE = :WORKSPOT_CODE AND'
      '  W.JOB_CODE = :JOB_CODE AND'
      '  W.INTERVALSTARTTIME = :INTERVALSTARTTIME)'
      'WHEN NOT MATCHED THEN'
      'INSERT VALUES ('
      'SEQ_WORKSPOTEFFPERMINUTE.NEXTVAL,'
      ':SHIFT_DATE,'
      ':SHIFT_NUMBER,'
      ':PLANT_CODE,'
      ':WORKSPOT_CODE,'
      ':JOB_CODE,'
      ':INTERVALSTARTTIME,'
      ':INTERVALENDTIME,'
      ':WORKSPOTSECONDSACTUAL,'
      ':WORKSPOTSECONDSNORM,'
      ':WORKSPOTQUANTITY,'
      ':NORMLEVEL,'
      'SYSDATE,'
      'SYSDATE,'
      ':MUTATOR,'
      ':START_TIMESTAMP,'
      ':END_TIMESTAMP)'
      'WHEN MATCHED THEN'
      '  UPDATE SET'
      '    W.WORKSPOTQUANTITY = W.WORKSPOTQUANTITY + :WORKSPOTQUANTITY,'
      '    W.WORKSPOTSECONDSNORM ='
      '      CASE WHEN :NORMLEVEL <> 0'
      
        '      THEN (W.WORKSPOTQUANTITY + :WORKSPOTQUANTITY) * 3600 / :NO' +
        'RMLEVEL ELSE 0 END,'
      '    W.MUTATIONDATE = SYSDATE,'
      '    W.MUTATOR = :MUTATOR'
      '')
    Session = ORASystemDM.OracleSession
    Variables.Data = {
      030000000E0000000B0000003A53484946545F444154450C0000000000000000
      0000000D0000003A53484946545F4E554D424552030000000000000000000000
      0B0000003A504C414E545F434F44450500000000000000000000000E0000003A
      574F524B53504F545F434F4445050000000000000000000000090000003A4A4F
      425F434F4445050000000000000000000000120000003A494E54455256414C53
      5441525454494D450C0000000000000000000000100000003A494E5445525641
      4C454E4454494D450C0000000000000000000000160000003A574F524B53504F
      545345434F4E445341435455414C040000000000000000000000140000003A57
      4F524B53504F545345434F4E44534E4F524D0400000000000000000000001100
      00003A574F524B53504F545155414E544954590400000000000000000000000A
      0000003A4E4F524D4C4556454C040000000000000000000000100000003A5354
      4152545F54494D455354414D500C00000000000000000000000E0000003A454E
      445F54494D455354414D500C0000000000000000000000080000003A4D555441
      544F52050000000000000000000000}
    Left = 56
    Top = 96
  end
  object oqRTEmpEff: TOracleQuery
    SQL.Strings = (
      'SELECT '
      '  T.WORKSPOT_CODE, T.JOB_CODE,'
      '  T.WORKSPOT WORKSPOT, T.JOB_CODE_DESCRIPTION JOB, '
      '  T.QTY QACTUAL, T.NORMLEVEL QNORM, T.QTY - T.NORMLEVEL QDIFF, '
      
        '  T.SECONDSACTUAL TACTUAL, T.SECONDSNORM TNORM, T.SECONDSACTUAL ' +
        '- T.SECONDSNORM TDIF,'
      '  T.EFF'
      'FROM '
      '  V_REALTIMEEMPLOYEEEFF T '
      'WHERE '
      '  T.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER'
      'ORDER BY '
      '  T.WORKSPOT_CODE, T.JOB_CODE'
      '')
    Session = ORASystemDM.OracleSession
    Variables.Data = {
      0300000001000000100000003A454D504C4F5945455F4E554D42455203000000
      0000000000000000}
    Left = 56
    Top = 152
  end
  object oqRTCurrEmpEff: TOracleQuery
    SQL.Strings = (
      'SELECT'
      '  T.EFF'
      'FROM'
      '  V_REALTIMECURREMPLOYEEEFF T'
      'WHERE'
      '  T.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER')
    Session = ORASystemDM.OracleSession
    Variables.Data = {
      0300000001000000100000003A454D504C4F5945455F4E554D42455203000000
      0000000000000000}
    Left = 56
    Top = 200
  end
  object oqRTShiftEmpEff: TOracleQuery
    SQL.Strings = (
      'SELECT '
      '  T.EFF'
      'FROM '
      '  V_REALTIMESHIFTEMPLOYEEEFF T'
      'WHERE'
      '  T.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND'
      '  T.PLANT_CODE = :PLANT_CODE AND'
      '  T.SHIFT_NUMBER = :SHIFT_NUMBER')
    Session = ORASystemDM.OracleSession
    Variables.Data = {
      0300000003000000100000003A454D504C4F5945455F4E554D42455203000000
      00000000000000000B0000003A504C414E545F434F4445050000000000000000
      0000000D0000003A53484946545F4E554D424552030000000000000000000000}
    Left = 56
    Top = 256
  end
  object odsRTEmpEff: TOracleDataSet
    SQL.Strings = (
      'SELECT'
      '  T.WORKSPOT_CODE || '#39'-'#39' || T.JOB_CODE PRIMKEY,'
      '  T.WORKSPOT_CODE, T.JOB_CODE,'
      '  T.WORKSPOT_DESCRIPTION WORKSPOT, T.JOB_CODE_DESCRIPTION JOB,'
      
        '  ROUND(T.QTY) QACTUAL, ROUND(T.NORMQTY) QNORM, ROUND(T.QTY - T.' +
        'NORMQTY) QDIFF,'
      
        '  ROUND(T.SECONDSACTUAL) TACTUAL, ROUND(T.SECONDSNORM) TNORM, RO' +
        'UND(T.SECONDSACTUAL - T.SECONDSNORM) TDIFF,'
      '  ROUND(T.EFF) EFF,'
      '  T.INTERVALENDTIME INTERVALENDTIME'
      'FROM'
      '  V_REALTIMESHIFTEMPLEFFPERWS T'
      'WHERE'
      '  T.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER'
      'ORDER BY 2,3  '
      '')
    Variables.Data = {
      0300000001000000100000003A454D504C4F5945455F4E554D42455203000000
      0000000000000000}
    QBEDefinition.QBEFieldDefs = {
      040000000D0000000D000000574F524B53504F545F434F444501000000000008
      0000004A4F425F434F444501000000000008000000574F524B53504F54010000
      000000030000004A4F42010000000000070000005141435455414C0100000000
      0005000000514E4F524D01000000000005000000514449464601000000000007
      0000005441435455414C01000000000005000000544E4F524D01000000000003
      0000004546460100000000000500000054444946460100000000000700000050
      52494D4B45590100000000000F000000494E54455256414C454E4454494D4501
      0000000000}
    AutoCalcFields = False
    Session = ORASystemDM.OracleSession
    Left = 160
    Top = 152
    object odsRTEmpEffPRIMKEY: TStringField
      FieldName = 'PRIMKEY'
      Size = 49
    end
    object odsRTEmpEffWORKSPOT_CODE: TStringField
      FieldName = 'WORKSPOT_CODE'
      Required = True
      Size = 24
    end
    object odsRTEmpEffJOB_CODE: TStringField
      FieldName = 'JOB_CODE'
      Required = True
      Size = 24
    end
    object odsRTEmpEffWORKSPOT: TStringField
      FieldName = 'WORKSPOT'
      Required = True
      Size = 120
    end
    object odsRTEmpEffJOB: TStringField
      FieldName = 'JOB'
      Required = True
      Size = 120
    end
    object odsRTEmpEffQACTUAL: TFloatField
      FieldName = 'QACTUAL'
    end
    object odsRTEmpEffQNORM: TFloatField
      FieldName = 'QNORM'
    end
    object odsRTEmpEffQDIFF: TFloatField
      FieldName = 'QDIFF'
    end
    object odsRTEmpEffTACTUAL: TFloatField
      FieldName = 'TACTUAL'
    end
    object odsRTEmpEffTNORM: TFloatField
      FieldName = 'TNORM'
    end
    object odsRTEmpEffTDIFF: TFloatField
      FieldName = 'TDIFF'
    end
    object odsRTEmpEffEFF: TFloatField
      FieldName = 'EFF'
    end
    object odsRTEmpEffINTERVALENDTIME: TDateTimeField
      FieldName = 'INTERVALENDTIME'
    end
  end
  object oqCopyThis: TOracleQuery
    Session = ORASystemDM.OracleSession
    Left = 24
    Top = 24
  end
  object oqRTEmpEffGraph: TOracleQuery
    SQL.Strings = (
      'SELECT '
      '  G.STARTDATE, G.MINUTESACTUAL, G.NORMQTYPERHOUR, G.QTYPERHOUR,'
      
        '  G.PLANT_CODE, G.SHIFT_DATE, G.SHIFT_NUMBER, G.SHIFT, G.EMPLOYE' +
        'E_NUMBER,     '
      '  G.SHORT_NAME, G.NAME, G.MACHINE_CODE,'
      
        '  G.MACHINE, G.WORKSPOT_CODE, G.WORKSPOT_DESCRIPTION, G.JOB_CODE' +
        ',   '
      '  G.JOB_CODE_DESCRIPTION, G.QTY,'
      '  G.EFF'
      'FROM '
      '  V_REALTIMESHIFTEMPLEFFGRAPH G'
      'WHERE '
      '  G.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND'
      '  ('
      '    (G.SHIFT_DATE = :SHIFT_DATE AND :GETSHIFT = 1) OR'
      
        '    (G.STARTDATE >= :DATEFROM AND G.STARTDATE < :DATETO AND :GET' +
        'SHIFT = 0)'
      '  )'
      'ORDER BY G.STARTDATE')
    Session = ORASystemDM.OracleSession
    Variables.Data = {
      0300000005000000100000003A454D504C4F5945455F4E554D42455203000000
      00000000000000000B0000003A53484946545F444154450C0000000000000000
      000000090000003A474554534849465403000000000000000000000009000000
      3A4441544546524F4D0C0000000000000000000000070000003A44415445544F
      0C0000000000000000000000}
    Left = 56
    Top = 312
  end
  object oqRTCurrWSEff: TOracleQuery
    SQL.Strings = (
      'SELECT '
      '  T.EMPLOYEE_NUMBER, '
      '  T.NAME,'
      '  MIN(T.JOB_CODE) JOB_CODE,'
      '  MIN(NVL(J.PRODLEVELMINPERC,0)) PRODLEVELMINPERC, '
      '  MIN(NVL(J.MACHOUTPUTMINPERC,0)) MACHOUTPUTMINPERC,'
      '  MIN(T.SHIFT_NUMBER) SHIFT_NUMBER,'
      '  ROUND(SUM(T.QTY)) QTY,'
      '  ROUND(SUM(T.NORMQTY)) NORMQTY,'
      '  ROUND(SUM(T.SECONDSACTUAL)) SECONDSACTUAL,'
      '  ROUND(SUM(T.SECONDSNORM)) SECONDSNORM,'
      '  ROUND(AVG(T.EFF)) EFF'
      'FROM V_REALTIMECURREMPLEFFPERWS T INNER JOIN JOBCODE J ON'
      '  T.plant_code = J.PLANT_CODE AND'
      '  T.workspot_code = J.WORKSPOT_CODE AND'
      '  T.job_code = J.JOB_CODE'
      'WHERE T.PLANT_CODE = :PLANT_CODE AND'
      '('
      '  (T.WORKSPOT_CODE = :WORKSPOT_CODE AND :BYWORKSPOT=1) OR'
      '  (T.MACHINE_CODE = :WORKSPOT_CODE AND :BYWORKSPOT=0)'
      ')'
      'GROUP BY T.EMPLOYEE_NUMBER, T.NAME'
      'ORDER BY 1'
      ''
      '')
    Session = ORASystemDM.OracleSession
    Variables.Data = {
      03000000030000000B0000003A504C414E545F434F4445050000000000000000
      0000000E0000003A574F524B53504F545F434F44450500000000000000000000
      000B0000003A4259574F524B53504F54030000000000000000000000}
    Scrollable = True
    Left = 240
    Top = 152
  end
  object oqRTShiftWSEff: TOracleQuery
    SQL.Strings = (
      'SELECT '
      '  T.EMPLOYEE_NUMBER, '
      '  T.NAME,'
      '  ROUND(SUM(T.QTY)) QTY,'
      '  ROUND(SUM(T.NORMQTY)) NORMQTY,'
      '  ROUND(SUM(T.SECONDSACTUAL)) SECONDSACTUAL,'
      '  ROUND(SUM(T.SECONDSNORM)) SECONDSNORM,'
      '  ROUND(AVG(T.EFF)) EFF'
      'FROM V_REALTIMESHIFTEMPLEFFPERWS T'
      'WHERE T.PLANT_CODE = :PLANT_CODE AND'
      '('
      '  (T.WORKSPOT_CODE = :WORKSPOT_CODE AND :BYWORKSPOT=1) OR'
      '  (T.MACHINE_CODE = :WORKSPOT_CODE AND :BYWORKSPOT=0)'
      ')'
      'AND'
      'T.SHIFT_NUMBER = :SHIFT_NUMBER'
      'GROUP BY T.EMPLOYEE_NUMBER, T.NAME'
      'ORDER BY 1'
      ''
      '')
    Session = ORASystemDM.OracleSession
    Variables.Data = {
      03000000040000000B0000003A504C414E545F434F4445050000000000000000
      0000000E0000003A574F524B53504F545F434F44450500000000000000000000
      000D0000003A53484946545F4E554D4245520300000000000000000000000B00
      00003A4259574F524B53504F54030000000000000000000000}
    Left = 240
    Top = 200
  end
  object odsRTCurrWSEffALL: TOracleDataSet
    SQL.Strings = (
      'SELECT'
      '  T.PLANT_CODE,'
      '  T.MACHINE_CODE,'
      '  T.WORKSPOT_CODE,'
      '  T.EMPLOYEE_NUMBER,'
      '  T.NAME,'
      '  MIN(T.JOB_CODE) JOB_CODE,'
      '  MIN(NVL(J.PRODLEVELMINPERC,0)) PRODLEVELMINPERC,'
      '  MIN(NVL(J.MACHOUTPUTMINPERC,0)) MACHOUTPUTMINPERC,'
      '  MIN(T.SHIFT_NUMBER) SHIFT_NUMBER,'
      '  ROUND(SUM(T.QTY)) QTY,'
      '  ROUND(SUM(T.NORMQTY)) NORMQTY,'
      '  ROUND(SUM(T.SECONDSACTUAL)) SECONDSACTUAL,'
      '  ROUND(SUM(T.SECONDSNORM)) SECONDSNORM,'
      '  ROUND(AVG(T.EFF)) EFF'
      'FROM V_REALTIMECURREMPLEFFPERWS T INNER JOIN JOBCODE J ON'
      '  T.PLANT_CODE = J.PLANT_CODE AND'
      '  T.WORKSPOT_CODE = J.WORKSPOT_CODE AND'
      '  T.JOB_CODE = J.JOB_CODE'
      'GROUP BY   '
      '  T.PLANT_CODE,'
      '  T.MACHINE_CODE,'
      '  T.WORKSPOT_CODE,'
      '  T.EMPLOYEE_NUMBER, '
      '  T.NAME'
      'ORDER BY 1, 2, 3, 4')
    Session = ORASystemDM.OracleSession
    Left = 344
    Top = 152
  end
  object odsRTShiftWSEffALL: TOracleDataSet
    SQL.Strings = (
      'SELECT '
      '  T.PLANT_CODE,'
      '  T.MACHINE_CODE,'
      '  T.WORKSPOT_CODE,'
      '  T.SHIFT_NUMBER,'
      '  T.EMPLOYEE_NUMBER, '
      '  T.NAME,'
      '  ROUND(SUM(T.QTY)) QTY,'
      '  ROUND(SUM(T.NORMQTY)) NORMQTY,'
      '  ROUND(SUM(T.SECONDSACTUAL)) SECONDSACTUAL,'
      '  ROUND(SUM(T.SECONDSNORM)) SECONDSNORM,'
      '  ROUND(AVG(T.EFF)) EFF'
      'FROM V_REALTIMESHIFTEMPLEFFPERWS T'
      'GROUP BY '
      '  T.PLANT_CODE,'
      '  T.MACHINE_CODE,'
      '  T.WORKSPOT_CODE,'
      '  T.SHIFT_NUMBER,'
      '  T.EMPLOYEE_NUMBER, '
      '  T.NAME'
      'ORDER BY '
      '  1,2,3,4,5')
    Session = ORASystemDM.OracleSession
    Left = 344
    Top = 200
  end
  object odsRTCurrGhostAll: TOracleDataSet
    SQL.Strings = (
      'select '
      '  t.plant_code,'
      '  t.shift_date,'
      '  t.shift_number,'
      '  t.shift,'
      '  t.machine_code,'
      '  t.machine,'
      '  t.workspot_code,'
      '  t.workspot_description,'
      '  t.job_code,'
      '  t.job_code_description,'
      '  t.qty,'
      '  t.qty2,'
      '  t.secondsactual,'
      '  t.secondsnorm,'
      '  t.normqty,'
      '  t.minutesactual,'
      '  t.minutesnorm,'
      '  t.eff'
      'from v_realtimecurrghostcount t'
      'order by '
      '  t.plant_code,'
      '  t.machine_code,'
      '  t.workspot_code,'
      '  t.job_code'
      '')
    Session = ORASystemDM.OracleSession
    Left = 344
    Top = 256
  end
  object odsRTShiftGhostAll: TOracleDataSet
    SQL.Strings = (
      'select '
      '  t.plant_code,'
      '  t.shift_date,'
      '  t.shift_number,'
      '  t.shift,'
      '  t.machine_code,'
      '  t.machine,'
      '  t.workspot_code,'
      '  t.workspot_description,'
      '  t.job_code,'
      '  t.job_code_description,'
      '  t.qty,'
      '  t.qty2,'
      '  t.secondsactual,'
      '  t.secondsnorm,'
      '  t.normqty,'
      '  t.minutesactual,'
      '  t.minutesnorm,'
      '  t.eff'
      'from v_realtimeshiftghostcount t'
      'order by '
      '  t.plant_code,'
      '  t.machine_code,'
      '  t.workspot_code,'
      '  t.job_code'
      '')
    Session = ORASystemDM.OracleSession
    Left = 344
    Top = 312
  end
  object oqTRS: TOracleQuery
    SQL.Strings = (
      'SELECT'
      '  T.JOB_CODE,'
      '  T.SHIFT_NUMBER,'
      '  T.SHIFT_DATE'
      'FROM'
      '  TIMEREGSCANNING T'
      'WHERE'
      '  T.PLANT_CODE = :PLANT_CODE AND'
      '  T.WORKSPOT_CODE = :WORKSPOT_CODE AND'
      '  T.SHIFT_DATE = :SHIFT_DATE'
      'GROUP BY'
      '  T.JOB_CODE,'
      '  T.SHIFT_NUMBER,'
      '  T.SHIFT_DATE'
      'ORDER BY'
      '  T.JOB_CODE,'
      '  T.SHIFT_NUMBER,'
      '  T.SHIFT_DATE')
    Session = ORASystemDM.OracleSession
    Variables.Data = {
      03000000030000000B0000003A504C414E545F434F4445050000000000000000
      0000000E0000003A574F524B53504F545F434F44450500000000000000000000
      000B0000003A53484946545F444154450C0000000000000000000000}
    Left = 448
    Top = 152
  end
  object oqRecalcEff: TOracleQuery
    SQL.Strings = (
      'begin'
      '  -- Call the procedure'
      '  recalc_efficiency(:PLANT_CODE,'
      '                    :SHIFT_DATE,'
      '                    :SHIFT_NUMBER,'
      '                    :WORKSPOT_CODE,'
      '                    :JOB_CODE);'
      'end;')
    Session = ORASystemDM.OracleSession
    Variables.Data = {
      03000000050000000B0000003A504C414E545F434F4445050000000000000000
      0000000D0000003A53484946545F4E554D424552030000000000000000000000
      0E0000003A574F524B53504F545F434F44450500000000000000000000000900
      00003A4A4F425F434F44450500000000000000000000000B0000003A53484946
      545F444154450C0000000000000000000000}
    Left = 392
    Top = 24
  end
  object oqCalcEff: TOracleQuery
    SQL.Strings = (
      'begin'
      '  -- Call the procedure'
      '  calc_efficiency(:PLANT_CODE);'
      'end;')
    Session = ORASystemDM.OracleSession
    Variables.Data = {
      03000000010000000B0000003A504C414E545F434F4445050000000000000000
      000000}
    Left = 312
    Top = 24
  end
  object odsCopyThis: TOracleDataSet
    Session = ORASystemDM.OracleSession
    Left = 104
    Top = 24
  end
end
