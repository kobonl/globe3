(*
  PIM-202
  - Test Pims with RFID-reader pcProx Plus
  - Allow a string read via com-port that only ends with CR instead of CR + LF
*)
unit BasePimsSerialFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BasePimsFRM, ComDrvN, StdCtrls, ImgList;

type

  TCodeReadEvent = procedure(const Code: String) of object;

  TBasePimsSerialForm = class(TBasePimsForm)
    Port1: TCommPortDriverN;
    procedure FormCreate(Sender: TObject);
    procedure Port1ReceiveData(Sender: TObject;
      DataPtr: Pointer; DataSize: Integer);
  private
    { Private declarations }
    FLastChip       : String;
    FLastComReceive : String;
    FOnCodeRead	    : TCodeReadEvent;
    procedure DoCodeRead(const Code: String);
  public
    { Public declarations }
    property OnCodeRead: TCodeReadEvent read FOnCodeRead write FOnCodeRead;
    procedure FlushScanner;
    procedure OpenScanner;
    procedure CloseScanner;
    procedure ScannerSettings(
      const ComPort: Integer;
      const ComPortDataBits: Integer;
      const ComPortParity: Char;
      const ComPortBaudRate: Integer;
      const ComPortStopBits: Integer);
  end;

var
  BasePimsSerialForm: TBasePimsSerialForm;

implementation

{$R *.DFM}

procedure TBasePimsSerialForm.OpenScanner;
begin
  if not Port1.Connected then
    Port1.Connect;
end;

procedure TBasePimsSerialForm.CloseScanner;
begin
  if Port1.Connected then
    Port1.Disconnect;
end;

procedure TBasePimsSerialForm.FlushScanner;
begin
  FLastChip        := '';
  FLastComReceive  := '';
end;

procedure TBasePimsSerialForm.FormCreate(Sender: TObject);
begin
  inherited;
  // Assign the ComPort number
(*
  case SolarSystemSettings.ComPort of
    1 : Port1.ComPort := pnCOM1;
    2 : Port1.ComPort := pnCOM2;
    3 : Port1.ComPort := pnCOM3;
    4 : Port1.ComPort := pnCOM4;
  end; { case }
*)
end;

procedure TBasePimsSerialForm.Port1ReceiveData(Sender: TObject;
  DataPtr: Pointer; DataSize: Integer);
const
{ Serial Communication }
  LIF = #10; // LF line feed
  CRT = #13; // CR carriage return
var
  S,
  NewRead : String;
  CheckCR,
  CheckLF    : Integer;
begin
  inherited;
  S := String(PChar(DataPtr));
  SetLength(S, DataSize);
  S := FLastComReceive + S;
  FLastComReceive := S;

  CheckCR := Pos(CRT, S);
  CheckLF := Pos(LIF, S);

  // PIM-202 Exception: Allow also when there is only CR at the end.
  if (CheckCr = CheckLF - 1) or
    ((CheckCR > 0) and (CheckLF = 0)) then // PIM-202
  begin
    if (CheckCr = CheckLF - 1) then // PIM-202
      NewRead := Copy(S, 1, (CheckLF - 2))
    else
      NewRead := Copy(S, 1, (CheckCR - 1)); // PIM-202

    // MR:26-11-2002 Begin
    // Allow double scan!
    // Because the code that will be scanned
    // is an Employee-ID-Card, so it's allowed to
    // read the same code again.
    // This part is remarked
(*
    if (NewRead <> FLastChip) then
    begin
      FLastChip := NewRead;
      DoCodeRead(NewRead);
    end;
*)
    // This pas is new
    FLastChip := NewRead;
    DoCodeRead(NewRead);
    // MR:26-11-2002 End

    FLastComReceive := '';
    Delete(S, 1, CheckLF);
  end;
end;

procedure TBasePimsSerialForm.DoCodeRead(const Code: String);
begin
  if Assigned(FOnCodeRead) then
  FOnCodeRead(Code);
end;

procedure TBasePimsSerialForm.ScannerSettings(
  const ComPort: Integer;
  const ComPortDataBits: Integer;
  const ComPortParity: Char;
  const ComPortBaudRate: Integer;
  const ComPortStopBits: Integer);
begin
  if Port1.Connected then
    Port1.Disconnect;

  case ComPort of
    1 : Port1.ComPort := pnCOM1;
    2 : Port1.ComPort := pnCOM2;
    3 : Port1.ComPort := pnCOM3;
    4 : Port1.ComPort := pnCOM4;
    else
      Port1.ComPort := pnCOM1;
  end;
  case ComPortDataBits of
    5: Port1.ComPortDataBits := db5BITS;
    6: Port1.ComPortDataBits := db6BITS;
    7: Port1.ComPortDataBits := db7BITS;
    8 : Port1.ComPortDataBits := db8BITS;
    else
      Port1.ComPortDataBits := db8BITS;
  end;
  case ComPortParity of
    'E', 'e': Port1.ComPortParity := ptEVEN;
    'M', 'm': Port1.ComPortParity := ptMARK;
    'N', 'n': Port1.ComPortParity := ptNONE;
    'O', 'o': Port1.ComPortParity := ptODD;
    'S', 's': Port1.ComPortParity := ptSPACE;
    else
       Port1.ComPortParity := ptNONE;
  end;
  case ComPortBaudRate of
    110   : Port1.ComPortSpeed := br110;
    300   : Port1.ComPortSpeed := br300;
    600   : Port1.ComPortSpeed := br600;
    1200  : Port1.ComPortSpeed := br1200;
    2400  : Port1.ComPortSpeed := br2400;
    4800  : Port1.ComPortSpeed := br4800;
    9600  : Port1.ComPortSpeed := br9600;
    14400 : Port1.ComPortSpeed := br14400;
    19200 : Port1.ComPortSpeed := br19200;
    38400 : Port1.ComPortSpeed := br38400;
    56000 : Port1.ComPortSpeed := br56000;
    57600 : Port1.ComPortSpeed := br57600;
    115200: Port1.ComPortSpeed := br115200;
    else
      Port1.ComPortSpeed := br9600;
  end;
  case ComPortStopBits of
    1 : Port1.ComPortStopBits := sb1BITS;
    2 : Port1.ComPortStopBits := sb2BITS;
    else
      Port1.ComPortStopBits := sb1BITS;
  end;
end;

end.
