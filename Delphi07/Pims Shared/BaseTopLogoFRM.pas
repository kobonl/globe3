(*
  MRA:28-MAY-2013 New look Pims
  - Some pictures/colors are changed.
*)
unit BaseTopLogoFRM;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseTopMenuFRM, ComCtrls, ExtCtrls, dxCntner, Menus, StdActns,
  ActnList, ImgList, jpeg, StdCtrls;

type
  TBaseTopLogoForm = class(TBaseTopMenuForm)
    pnlImageBase: TPanel;
    imgBasePims: TImage;
    stbarBase: TStatusBar;
    pnlInsertBase: TPanel;
    lblMessage: TLabel;
    pnlTopLine: TPanel;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  BaseTopLogoForm: TBaseTopLogoForm;

implementation

uses
  UPimsConst;

{$R *.DFM}

procedure TBaseTopLogoForm.FormCreate(Sender: TObject);
begin
  inherited;
//  pnlImageBase.Color := clPimsBlue; // 20014289 // PIM-250
  lblMessage.Caption := ''; // PIM-250
end;

end.
