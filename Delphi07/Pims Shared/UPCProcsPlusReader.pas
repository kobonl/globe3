(*
  MRA:6-JAN-2017 PIM-256
  - Read IDs from FOBs via pcProcs-Plus-Reader with USB-connection
  MRA:21-JUN-2019 GLOB3-324
  - Add device named pcProx for Timerecording-app.
  - This is used for pcProx Plus readers with cards that should be read
    without changing it. Now it uses Rosslare that triggers the fact it
    is a pcProx Plus-reader, but also converts the ID.
  - Add property 'Rosslare'. When this is true, it should convert the read
    ID from a special hex-value to a readable ID. When not true, it should
    read the ID and not convert it as output.
*)
unit UPCProcsPlusReader;

interface

uses
  SysUtils, PCPROXAPI;

type
  TPCProcsPlusReader = class
  private
    FID: String;
    FConnected: Boolean;
    FErrorMsg: String;
    FRosslare: Boolean;
  public
    constructor Create;
    procedure Connect;
    procedure DisConnect;
    procedure GetActiveID;
    function DLLLoadedCheck: Boolean;
    property ID: String read FID write FID;
    property Connected: Boolean read FConnected write FConnected;
    property ErrorMsg: String read FErrorMsg write FErrorMsg;
    property Rosslare: Boolean read FRosslare write FRosslare;
  end;

implementation

{ TPCProcsPlusReader }

constructor TPCProcsPlusReader.Create;
begin
  inherited;
//
end;

function TPCProcsPlusReader.DLLLoadedCheck: Boolean;
begin
  Result := PCPROXAPI.PCProxAPI_DLLLoaded;
  if not Result then
    ErrorMsg := 'ERROR: Cannot find or load pcProxAPI.dll!';
end;

procedure TPCProcsPlusReader.Connect;
var
  pfDID: Integer;
  ret: Integer;
begin
  if not DLLLoadedCheck then
    Exit;
  PCPROXAPI.SetDevTypeSrch(PCPROXAPI.PRXDEVTYP_USB);
  pfDID := 0;
  ret := PCPROXAPI.USBConnect(pfDID);
  Connected := ret > 0;
  if not Connected then
    ErrorMsg := 'ERROR: Cannot connect to pcProcs Plus Card-reader!';
end;

procedure TPCProcsPlusReader.DisConnect;
begin
  if not DLLLoadedCheck then
    Exit;
  USBDisConnect;
end;

procedure TPCProcsPlusReader.GetActiveID;
type
  TBuf = array[0..15] of BYTE;
  PTBuf = ^TBuf;
var
  brc: Integer;
  Len: Smallint;
  buf: PTBuf;
  str: String;
  MyID: Integer;
  function FillZero(const AValue: String; const ALen: Integer): String;
  begin
    Result := AValue;
    while Length(Result) < ALen do
      Result := '0' + Result;
  end;
begin
  if not DLLLoadedCheck then
    Exit;
  if not Connected then
    Connect;
  if not Connected then
    Exit;
  new(buf);
  try
    try
      Len := SizeOf(buf);
      brc := PCPROXAPI.GetActiveID(buf[0], Len);
      ID := '';
      if brc > 0  then
      begin
        str := '';
        if Rosslare then
        begin
          // Convert to correct ID:
          // ID 160 10328 will be read as:
          // 88 40 160 13 (last is CR)
          // 160 is first part
          // 40 88 is second part (reversed)
          MyID := buf[2];
          str := FillZero(IntToStr(MyID), 3);
          MyID := (buf[1] * 256) + buf[0];
          str := str + FillZero(IntToStr(MyID), 5);
          ID := str;
        end
        else
        begin
          // GLOB3-324
          // NOTE: What it receives is the ID in hex, in reverse order.
          // Always read 4 bytes (ID with length of 8)
          // Convert to correct ID:
          // ID 4666 3810 will be read as:
          // 82 08 C8 02
          // This must be reversed to:
          // 02 C8 08 82
          // This must be converted to dec.
          ID := IntToHex(buf[3], 2) + IntToHex(buf[2], 2) +
            IntToHex(buf[1], 2) + IntToHex(buf[0], 2);
          ID := IntToStr(StrToInt('$' + ID));
        end;
      end;
    except
      on E:Exception do
        ErrorMsg := E.Message;
    end;
  finally
    dispose(buf);
  end;
end;

end.
