(*
  Changes:
    MRA:17-NOV-2009 RV043.1.
    - Option added for auto login, by using a param during start.
  MRA:11-FEB-2013 TD-22082 Perfomance problems
  - Store some settings in ORASystem for Team-per-user handling.
  MRA:28-MAY-2013 New look Pims
  - Centre OK+Cancel-buttons.
  MRA:23-JAN-2014 TD-24046
  - Translate issues: Use a different method to translate.
*)

unit DialogLoginFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DialogSelectionFRM, ComCtrls, StdCtrls, Buttons, ExtCtrls,
  Registry;

type
  TDialogLoginF = class(TDialogSelectionForm)
    GroupBox1: TGroupBox;
    Label2: TLabel;
    EditPassword: TEdit;
    Label1: TLabel;
    EditUserName: TEdit;
    GroupBox2: TGroupBox;
    Label3: TLabel;
    procedure FormShow(Sender: TObject);
    procedure btnOkOnClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    FAplEditYN: String;
    FAplConnected: Boolean;
    FAplVisibleYN: String;
    FUserTeamSelectionYN: String;
    FLoginPassword: String;
    FLoginUser: String;
    FLoginGroup: String;
    FAutoLogin: Boolean;
    FAutoLoginUser: String;
    function ReadRegistryLogin: String;
    procedure WriteRegistryLogin(const Value: String);
    procedure SetAutoLoginUser(const Value: String);
  public
    { Public declarations }
    //Team selection
    property UserTeamSelectionYN: String read FUserTeamSelectionYN
      write FUserTeamSelectionYN;
    property AplEditYN: String read FAplEditYN write  FAplEditYN;
    property AplConnected: Boolean read FAplConnected write FAplConnected;
    property AplVisibleYN: String read FAplVisibleYN write FAplVisibleYN;
    property LoginUser: String  read FLoginUser write FLoginUser;
    property LoginPassword: String read FLoginPassword write FLoginPassword;
    property LoginGroup: String  read FLoginGroup write FLoginGroup;
    property RegistryLogin: String read ReadRegistryLogin
      write WriteRegistryLogin;
    function VerifyPassword: Boolean;
    procedure SetDefaultsLoginProperties;
    function CheckValidDate(const S: String): Boolean;
    property AutoLogin: Boolean read FAutoLogin write FAutoLogin;
    property AutoLoginUser: String read FAutoLoginUser write
      SetAutoLoginUser;
  end;

var
  DialogLoginF: TDialogLoginF;

implementation

uses
  ULoginConst, DialogLoginDMT, UPimsConst, UPimsMessageRes, ORASystemDMT,
  EncryptIt, Oracle;

{$R *.DFM}

procedure TDialogLoginF.SetDefaultsLoginProperties;
begin
  AplEditYN    := UNCHECKEDVALUE;
  AplVisibleYN := UNCHECKEDVALUE;
  AplConnected := False;

  LoginGroup    := '';
  LoginUser     := '';
  LoginPassword := '';
  //team selection
  UserTeamSelectionYN := UNCHECKEDVALUE;
  // RV043.1.
  if not AutoLogin then
    EditUserName.Text := RegistryLogin;
end;

procedure TDialogLoginF.FormShow(Sender: TObject);
begin
  inherited;
  // RV043.1.
  if not AutoLogin then
    EditPassword.SetFocus;
end;

function TDialogLoginF.VerifyPassword: Boolean;
begin
  SetDefaultsLoginProperties;
  repeat
    // RV043.1.
    if not AutoLogin then
      ShowModal
    else
    begin
      btnOkOnClick(nil);
      ModalResult := mrOK;
    end;
    if (ModalResult = mrOk) then
    begin
      if (not AplConnected) then
        MessageDlg(SAplErrorLogin, mtWarning,  [mbOk], 0)
      else
       if (AplVisibleYN = UNCHECKEDVALUE) then
         MessageDlg(SAplNotVisible, mtWarning,  [mbOk], 0);
    end;
    Result := ((ModalResult = mrOk) and
      (AplConnected ) and (AplVisibleYN = CHECKEDVALUE));
    // MR:23-12-2004
    // If user entered a wrong password, give a change to try again.
//    if not Result then
//      Exit;
  until (ModalResult = mrCancel) or Result or AutoLogin;
  if (ModalResult = mrCancel) or (not Result) then
    Exit;

  if AplConnected then
  begin
    LoginUser     := EditUserName.Text;
    LoginPassword := EditPassword.Text;
    RegistryLogin := LoginUser;
    // TD-22082
    // Store settings in SystemDM here.
    ORASystemDM.UserTeamSelectionYN := UserTeamSelectionYN;
    ORASystemDM.CurrentLoginUser := LoginUser;
  end;
// if connection is Ok then
// if application is PIMS
(* not for oracle
  if DialogLoginDM.Apl_Menu_Login <> APL_MENU_LOGIN_ADM then
  begin
    // close login database as SYSDBA
    DialogLoginDM.LoginPims.Connected := False;
  end
  else
  begin
 // close pims database
    ORASystemDM.Pims.Connected := False;
  // update database as SYSDBA user - use LoginPims database
    Result := ORASystemDM.UpdatePimsBase('LoginPims');
  end;
*)
end;

procedure TDialogLoginF.btnOkOnClick(Sender: TObject);
begin
  if (EditUserName.Text = ADMINUSER) and
    (EditPassword.Text = Decrypt(ADMINPASSWORD, Key)) then
  begin
    AplConnected := True;
    AplVisibleYN := CHECKEDVALUE;
    AplEditYN    := CHECKEDVALUE;
    LoginGroup   := ADMINGROUP;
    Exit;
  end;

  if (EditUserName.Text = ADMINUSER) then
    if CheckValidDate(EditPassword.Text) then
    begin
      AplConnected := True;
      AplVisibleYN := CHECKEDVALUE;
      AplEditYN    := CHECKEDVALUE;
      LoginGroup   := ADMINGROUP;
      Exit;
    end;
// all tables and queries used are connected as SYSDBA user
  with DialogLoginDM do
  begin
{ they do exist all    if (not TableUser.Exists) or (not TablePimsGroup.Exists) or
      (not TableGroupMenu.Exists) then
      Exit;   }
    // check if user & password are defined in User table
    odsPimsUser.Active := True;
    if odsPimsUser.Locate('USER_NAME', EditUserName.Text, []) then
      if (odsPimsUser.FieldByName('USER_PASSWORD').AsString = EditPassword.Text) then
      begin
        AplConnected := True;
        LoginGroup   := odsPimsUser.FieldByName('GROUP_NAME').AsString;
        UserTeamSelectionYN :=
          odsPimsUser.FieldByName('SET_TEAM_SELECTION_YN').AsString;
        // read record of  "Adm tool" menu item
        oqCheckOpenApl.SetVariable('GROUP_NAME', LoginGroup);
        oqCheckOpenApl.SetVariable('APL_MENU',   Apl_Menu_Login);
        oqCheckOpenApl.Execute;
        if not oqCheckOpenApl.Eof then
        begin
          AplVisibleYN := oqCheckOpenApl.FieldAsString('VISIBLE_YN');
          AplEditYN    := oqCheckOpenApl.FieldAsString('EDIT_YN');
        end;
      end;
    odsPimsUser.Active := False;
  end; {with}
end;

// store in registry the previous connected user
function TDialogLoginF.ReadRegistryLogin: String;
var
//Car 6-5-2004
  RegResult: String;
begin
  Result := ORASystemDM.OracleSession.LogonUsername;
//  DialogLoginDM.
//    LoginPims.Params.Values[DialogLoginDM.LoginPims.Params.Names[0]];
  RegResult := ORASystemDM.ReadRegistry(DialogLoginDM.APL_REGROOT_PATH_LOGIN,
    'ToolLogging');
  if RegResult <> '' then
    Result := RegResult;
end;

procedure TDialogLoginF.WriteRegistryLogin(const Value: String);
begin
//Car 5-6-2004
  ORASystemDM.WriteRegistry(DialogLoginDM.APL_REGROOT_PATH_LOGIN, 'ToolLogging',
    Value);
end;

// check if coded Password is valid - contains a valid date YYMMDD
function TDialogLoginF.CheckValidDate(const S: String): Boolean;
var
  StrDate, CurrentDateStr: String;
  StrMonth, StrDay: String;
  Year, Month, Day: Word;
  i: Integer;
begin
  Result := False;
  if Length(S) mod 2 <> 0 then
    Exit;
  for i := 1 to Length(S) do
    if not(S[i] in Valid_Char) then
      Exit;

  StrDate := Decrypt(S, Key);
  DecodeDate(Now, Year, Month, Day);
  StrMonth := IntToStr(Month);
  if Length(StrMonth) = 1 then
    StrMonth := '0' + StrMonth;
  StrDay :=IntToStr(Day);
  if Length(StrDay) = 1 then
    StrDay := '0' + StrDay;
  CurrentDateStr := Copy(IntToStr(Year), 3, 2) + StrMonth  + StrDay;
  Result := (CurrentDateStr = StrDate);
end;

// create dialoglogin data module
procedure TDialogLoginF.FormCreate(Sender: TObject);
begin
  inherited;
  DialogLoginDM := TDialogLoginDM.Create(Self);
  Label3.Caption := 'Database:            Pims - ' +
    ORASystemDM.OracleSession.LogonDatabase;
  AutoLogin := False;
  AutoLoginUser := '';
  // 20014289
{$IFDEF PRODUCTIONSCREEN}
  Height := 200;
{$ENDIF}
end;

// RV043.1. Auto login.
procedure TDialogLoginF.SetAutoLoginUser(const Value: String);
begin
  FAutoLoginUser := Value;
  EditUserName.Text := Value;
  try
    // Now get password
    with DialogLoginDM do
    begin
      odsPimsUser.Active := True;
      if odsPimsUser.Locate('USER_NAME', EditUserName.Text, []) then
        EditPassword.Text := odsPimsUser.FieldByName('USER_PASSWORD').AsString
      else
        EditPassword.Text := '';
      odsPimsUser.Active := False;
    end;
  except
    EditPassword.Text := '';
  end;
end;

end.
