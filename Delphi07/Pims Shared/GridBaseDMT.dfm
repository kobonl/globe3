object GridBaseDM: TGridBaseDM
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Left = 268
  Top = 170
  Height = 281
  Width = 578
  object TableMaster: TTable
    BeforePost = DefaultBeforePost
    BeforeDelete = DefaultBeforeDelete
    OnDeleteError = DefaultDeleteError
    OnEditError = DefaultEditError
    OnNewRecord = DefaultNewRecord
    OnPostError = DefaultPostError
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    Left = 40
    Top = 8
  end
  object TableDetail: TTable
    BeforePost = DefaultBeforePost
    BeforeDelete = DefaultBeforeDelete
    OnDeleteError = DefaultDeleteError
    OnEditError = DefaultEditError
    OnNewRecord = DefaultNewRecord
    OnPostError = DefaultPostError
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    MasterSource = DataSourceMaster
    Left = 40
    Top = 72
  end
  object DataSourceMaster: TDataSource
    DataSet = TableMaster
    Left = 160
    Top = 8
  end
  object DataSourceDetail: TDataSource
    DataSet = TableDetail
    Left = 160
    Top = 72
  end
  object TableExport: TTable
    OnCalcFields = TableExportCalcFields
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    Left = 40
    Top = 136
  end
  object DataSourceExport: TDataSource
    Left = 160
    Top = 136
  end
end
