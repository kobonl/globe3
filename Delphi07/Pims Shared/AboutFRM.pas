(*
  MRA:24-MAR-2014 TD-24046
  - Related to this order.
  - Center this based on calling form, not on screen.
*)
unit AboutFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BasePimsFRM, ExtCtrls, StdCtrls, ImgList, ShellAPI;

type
  TAboutForm = class(TBasePimsForm)
    pnlBack: TPanel;
    imgAbs: TImage;
    lblVersion: TLabel;
    lblWebSite: TLabel;
    procedure imgAbsClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure lblWebSiteClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  AboutForm: TAboutForm;

implementation

{$R *.DFM}
(*
{$IFDEF PIMSDUTCH}
{$R *NLD.DFM}
{$ELSE}
  {$IFDEF PIMSGERMAN}
  {$R *DEU.DFM}
  {$ELSE}
    {$IFDEF PIMSFRENCH}
    {$R *FRA.DFM}
    {$ELSE}
    {$R *.DFM}
    {$ENDIF}
  {$ENDIF}
{$ENDIF}
*)
uses
{$IFNDEF PIMSMAINTENANCE}
  ORASystemDMT;
{$ELSE}
  HomeDMT;
{$ENDIF}

procedure GetBuildInfo(var V1, V2, V3, V4: Word);
var
  VerInfoSize, VerValueSize, Dummy : DWORD;
  VerInfo : Pointer;
  VerValue : PVSFixedFileInfo;
begin
  VerInfoSize := GetFileVersionInfoSize(PChar(ParamStr(0)), Dummy);
  GetMem(VerInfo, VerInfoSize);
  GetFileVersionInfo(PChar(ParamStr(0)), 0, VerInfoSize, VerInfo);
  VerQueryValue(VerInfo, '\', Pointer(VerValue), VerValueSize);
  with VerValue^ do
  begin
    V1 := dwFileVersionMS shr 16;
    V2 := dwFileVersionMS and $FFFF;
    V3 := dwFileVersionLS shr 16;
    V4 := dwFileVersionLS and $FFFF;
  end;
  FreeMem(VerInfo, VerInfoSize);
end;

function DisplayVersionInfo: String;
var
  V1,       // Major Version
  V2,       // Minor Version
  V3,       // Release
  V4: Word; // Build Number
begin
  GetBuildInfo(V1, V2, V3, V4);
  Result := IntToStr(V1) + '.'  + IntToStr(V2) + '.'  + IntToStr(V3) + '.'
    + IntToStr(V4);
end;

procedure TAboutForm.imgAbsClick(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TAboutForm.FormKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  if Key in [#13, #32] then // Enter + Spacebar
    Close;
end;

procedure TAboutForm.FormCreate(Sender: TObject);
begin
  inherited;
  lblVersion.Caption := lblVersion.Caption + ' ' + DisplayVersionInfo + ' ' +
{$IFNDEF PIMSMAINTENANCE}
    ORASystemDM.OracleSession.LogonUsername;
{$ELSE}
    HomeDM.OracleSession.LogonUsername;
{$ENDIF}
end;

procedure TAboutForm.lblWebSiteClick(Sender: TObject);
begin
  inherited;
  if ShellExecute(Handle, PChar('OPEN'), // PIM-250
    PChar('http://www.gotlilabs.com'), nil, nil, SW_SHOWMAXIMIZED) <= 32 then
      ShowMessage('Cannot open (or have no permission to open) internet-browser!');
end;

end.
