inherited DialogEnterJobCommentF: TDialogEnterJobCommentF
  Left = 497
  Top = 275
  VertScrollBar.Range = 0
  BorderStyle = bsDialog
  Caption = 'Enter comment for down job'
  ClientHeight = 190
  Position = poDefault
  PixelsPerInch = 96
  TextHeight = 13
  inherited stbarBase: TStatusBar
    Top = 171
  end
  inherited pnlInsertBase: TPanel
    Height = 71
    Font.Color = clBlack
    Font.Height = -11
    object GroupBox1: TGroupBox
      Left = 0
      Top = 0
      Width = 624
      Height = 71
      Align = alClient
      Caption = 'Enter comment for down job'
      TabOrder = 0
      object Label1: TLabel
        Left = 8
        Top = 24
        Width = 90
        Height = 25
        Caption = 'Comment'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -21
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object Edit1: TEdit
        Left = 200
        Top = 22
        Width = 409
        Height = 31
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -19
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
      end
    end
  end
  inherited pnlBottom: TPanel
    Top = 108
    inherited btnOK: TBitBtn
      OnClick = btnOKClick
    end
  end
end
