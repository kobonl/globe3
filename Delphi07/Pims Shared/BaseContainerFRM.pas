unit BaseContainerFRM;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseDialogFRM, StdActns, ActnList, Menus, ComDrvN, ImgList,
  ComCtrls, ExtCtrls, StdCtrls, Buttons,  dxCntner, dxEditor, dxEdLib;

type
  TBaseContainerForm = class(TBaseDialogForm)
  private
    { Private declarations }
    FInsertForm: TForm;
  protected
    procedure NilInsertForm;
    procedure SetInsertForm(AForm: TForm); virtual;
  public
    { Public declarations }
    function IsShortCut(var Message: TWMKey): Boolean; override;
    property InsertForm: TForm read FInsertForm write SetInsertForm;
  end;

var
  BaseContainerForm: TBaseContainerForm;

implementation

uses
  SystemDM, BaseGridFRM, SolarStringsUnit, SimpleContainerFRM;

{$R *.dfm}

{ TBaseInsertFormContainerForm }

function TBaseContainerForm.IsShortCut(
  var Message: TWMKey): Boolean;
begin
  if FInsertForm = nil then
    Result := inherited IsShortCut(Message)
  else
    Result := inherited IsShortCut(Message) or
      InsertForm.IsShortCut(Message);
end;

procedure TBaseContainerForm.NilInsertForm;
begin
  if InsertForm <> nil then
    InsertForm.Close;
  FInsertForm := nil;
end;

procedure TBaseContainerForm.SetInsertForm(AForm: TForm);
begin
  if AForm <> FInsertForm then
  begin
    if Assigned(FInsertForm) then
    begin
      if (FInsertForm is TSimpleContainerForm) then
        if ((FInsertForm as TSimpleContainerForm).InsertForm <> nil) then
          (FInsertForm as TSimpleContainerForm).InsertForm.Hide;
//      FInsertForm.Parent := nil;
      FInsertForm.Hide;
      FInsertForm.Close;
    end;

    AForm.Parent := pnlInsertBase;
    AForm.Align  := alClient;
    pnlImageBase.Caption  := '  ' + AForm.Caption;
    pnlInsertBase.Caption :=
      TranslateString(SSolarLoading) + ' ' + AForm.Caption;
    // Get toolbar from insertform and place it in the menuform's toolbar
    if (AForm is TBaseGridForm) then
    begin
    // do nothing yet
//      (AForm as TBaseGridForm).tlbarGrid.Parent := Self;
//       Self.InsertControl((AForm as TBaseGridForm).tlbarGrid);
    end;
    Update;
    AForm.Show;
    if (AForm is TSimpleContainerForm) then
      if ((AForm as TSimpleContainerForm).InsertForm <> nil) then
        (AForm as TSimpleContainerForm).InsertForm.Show;
    FInsertForm := AForm;
  end;
end;

end.
