(*
  MRA:15-OCT-2012 20013516 Flash Blue - Rework
  - Added 2 extra color-consts: clLightBlue and clDarkBlue.
  MRA:28-MAY-2013 New look Pims
  - Some pictures/colors are changed.
  MRA:10-NOV-2014 20014826
  - Log error messages optionally to database.
  MRA:10-JUN-2015 20014550.50
  - Override TButton with TColorButton
  MRA:23-JUL-2015 PIM-50
  - Add comment for down job
  MRA:12-OCT-2015 PIM-90
  - Read data from external application seconds
  MRA:20-JUN-2016 PIM-194
  - Use 3 colors for efficiency-bars.
  MRA:12-JAN-2017 PIM-250
  - Improve GUI / Gotli experience of PIMS application
  MRA:2-JUL-2018 GLOB3-60
  - Extension 4 to 10 blocks for planning, added const MAX_TBS.
*)
unit UPimsConst;

interface

uses
  Graphics, Classes, ColorButton, ButtonWithColor, SysUtils, ExtCtrls;


const
  PIMS_ORAERR_KEYVIOL            = 00001;

  // DBIERR translated errors
  PIMS_POSTING                   = 1000;
  PIMS_DELETING                  = 1001;
  PIMS_EDITING                   = 1002;
  PIMS_DBIERR_LOCKED             = 10241;
  PIMS_DBIERR_GENERAL_SQL_ERROR  = 13059;
  PIMS_DBIERR_KEYVIOL            = 9729;
  PIMS_DBIERR_REQDERR            = 9732;
  PIMS_DBIERR_FORIEGNKEYERR      = 9733;
  PIMS_DBIERR_DETAILRECORDSEXIST = 9734;
//---------------------begin------exceptions--------------------------//
  PIMS_EX_UNIQUEFIELD            = 1;
  PIMS_EX_STARTENDDATE           = 2;
  PIMS_EX_STARTENDTIME           = 3;
  PIMS_EX_DELETE_LAST_RECORD     = 4;
  PIMS_EX_MAX_RECORD             = 5;
  PIMS_EX_DATEINPREVIOUSPERIOD   = 6;
  PIMS_EX_CHECKILLMSGCLOSE       = 7;
  PIMS_EX_CHECKILLMSGSTARTDATE   = 8;
  PIMS_EX_CHECKEMPLCONTRDATEPREVIOUS = 9;
//--------------------end---------exceptions--------------------------//
  // Stock related data { required fields }

  // Amount of characters to show for the barcode
  DISPLAYLENGTH = 15;

  // MR:18-02-2005 Conversion Interbase->Oracle. For use in select-statements.
  // NL = NewLine
  NL = #13#10;

  // PIMS Registry
  // MR: Changed for Pims 'ABS\Solar' to 'ABS\Pims'
  PIMS_REGROOT_PATH_GRID         = '\Software\ABS\Pims\Grids\';
  PIMS_REGROOT_PATH_SYS          = '\Software\ABS\Pims\System\';


  // grid colours
  clTreeParent                    = $00FFF0DD; { solar blue }
//  clSecondLine                    = clInfoBk;  { windows setting std: soft yellow }
//  clInfoBk is the Windows Hint color; too many custmers have their own scheme
//  so now its hardcoded
  clSecondLine                    = $00E1FFFF; { soft yellow }
//  clRequired                      = $00FFD9EC; { soft violet }
//  clDisabled                      = $00FFF0DD; { solar blue }
//  clDisabled                      = clInfoBk;  { windows setting std: soft yellow }
  clDisabled                      = $00E1FFFF; { soft yellow }
  clGridEdit                      = $00EECCFF; { soft red }
  clGridNoFocus                   = $00C4FFC4; { soft green }
  clFontReadOnly                  = clGrayText;{ windows settings std: gray }
  // scan colours
 // clScanOk                        = $00A5F7AD; { green }
   clScanError                     = $007474FD; { red }
//  clScanFlag                      = $0065F1E9; { yellow }
//  clScanIn                        = $00A4F4A8; { green }
//  clScanRepair                    = $00ECB85C; { blue }
//  clScanRewash                    = $0000CCFF; { orange }
   clScanRouteP1                   = $00DDDDDD; { light gray }
   clScanScrap                     = $009E9E9E; { gray }
//  clScanStockNew                  = $00FFD9EC; { soft violet }
//  clScanStockUsed                 = $00FFF0DD; { solar blue }
   clScanOut                       = $009E9CF4; { red }
   clScanPToAStock                 = $00C4FFC4; { soft green }
//  clRouteGridFocus                = $002020F0; { red }
//  clRouteArticleNoOrder           = $00E1FFFF; { soft yellow }
//  clRouteArticleNotStarted        = $00FFF0DD; { solar blue }
//  clRouteArticleBusy              = clScanRewash; //$0000CCFF; { orange }
//  clRouteArticleFinished          = clScanOk; //$00A5F7AD; { green }
   clOtherPlant                     = clScanError; // Order 550349
   clLightBlue                      = $00FFE4AE; // 20013516
   clDarkBlue                       = $00B37800; // 20013516

   // 20014289
   clPimsBlue                       = $00C89800; // $00996F58; // 20014450.50
   clRequired                       = $00EADED9; { soft violet }
   clPimsLBlue                      = $00FFEFDD;
   clPimsDarkBlue                   = $00977200; // 20014450.50
   clMyGreen                        = $0011AC23; // $0000FF00; // 20014450.50
   clMyRed                          = $002B2BC4; // $000505DB; // $000000FF; // 20014450.50 // PIM-250
   clMyOrange                       = $00277FFF;// $000057D9; // PIM-194
   clDarkRed                        = $002525A7; // PIM-250
   clLightRed                       = $002B2BC4; // PIM-250

  // Navigation in the grids
  NAV_FIRST  = 0;
  NAV_PRIOR  = 1;
  NAV_NEXT   = 2;
  NAV_LAST   = 3;


//PIMS
  CHECKEDVALUE            = 'Y';
  UNCHECKEDVALUE          = 'N';
  PERMANENT               = 1;
  TEMPORARY               = 2;
  PARTTIME                = 3;

// Settings
  DETAILEDPRODDEFAULT     = 52;
  DAYPRODINFODEFAULT      = 52;
  UNITWEIGHTDEFAULT       = 'Kg';
  UNITPIECESDEFAULT       = 'pcs';
  CURRENTUSER             = 'PimsUSR1';
  PASSWORDDEFAULT         = 'pimsabs';
  WEEKDAYSDEFAULT         = 2;
  FIRSTWEEKOFYEARDEFAULT  = 1;
  HOURSONSTARTDAYDEFAULT  = CHECKEDVALUE;
  VERSIONNUMBERDEFAULT    = 0;
  CHANGESCANYNDEFAULT     = UNCHECKEDVALUE;
  TIMERECORDINGSTATIONDEFAULT = UNCHECKEDVALUE;

  // MR:14-07-2004 Following is not used... it was also using 'UStrings', that
  // also isn't used anymore.
(*
  DAYSDESCRIPTIONWEEK: Array[1..7] of string  =
   (SPimsMonday, SPimsTuesday, SPimsWednesday, SPimsThursday, SPimsFriday,
   	SPimsSaturday, SPimsSunday);
  DAYSCODEWEEK: Array[1..7] of string =
   (SPimsMondayShort, SPimsTuesdayShort, SPimsWednesdayShort, SPimsThursdayShort,
    SPimsFridayShort, SPimsSaturdayShort, SPimsSundayShort);
*)

// Time Recording
  LONGDATE     	= 'dddddd - hh : nn : ss';
  DayToMin      = 24*60;
  MaxTimeBlocks = 4;
  DUMMYSTR      = '0';
  DummyWKStr    = 'Dummy Workspot';
  NullTime      = '00:00';
  LevelA        = 'A';
  LevelB        = 'B';
  LevelC        = 'C';


 // Hours Per Employee
  HOLIDAYAVAILABLETB            = 'H';
  TIMEFORTIMEAVAILABILITY       = 'T';
  WORKTIMEREDUCTIONAVAILABILITY = 'W';
 // Illness Messages
  DEFAULTAVAILABILITYCODE = '*';

 // Staff Availability
  ALLTEAMS      = 'All Teams';
  ALLPLANTS     = 'All Plants';
  ILLNESS       = 'I';
  PAID          = 'P';
  UNPAID        = 'U';
  RSNB          = 'B';
  HOLIDAY       = 'H';
  WTR           = 'W';
  LABOURTHERAPY = 'L';

  CHR_SEP = CHR(ORD(254));

  UNAVAILABLE = '-';
// name of temporary tables
  TMPSTAFFPLANNINGEMP     = 'PIVOTEMPSTAFFPLANNING';
  TMPSTAFFPLANNINGOCI     = 'PIVOTOCISTAFFPLANNING';
  TMPWKPEREMPL            = 'PIVOTWKPEREMP';
  TMPEMPLOYEEAVAILABILITY = 'PIVOTEMPAVAILABLE';
//
  TABLETEMPPROD = 'HREPROD';
  TABLETEMPSAL  = 'HRESALARY';
  TABLETEMPABS  = 'HREABSENCE';
// Char separator - string
  STR_SEP = ' | ';
// grid form
  TAG_RESTORECOLUMN = -1;
//
  EXPORTPAYROLL_WEEK   = 'W';
  EXPORTPAYROLL_PERIOD = 'P';
  EXPORTPAYROLL_MONTH  = 'M';
//CONST for export
  ExportDescriptonLU = 'EXP_DESCLU';
  ExportSortLU = 'EXP_SORTLU';
// separator char
  CHR_SEP1 = CHR(ORD(253));
// user rights
  ADMINUSER     = 'SYSDBA';
  ADMINPASSWORD = '61BC5D5D5C96EC';
  ADMINGROUP    = 'ABSADMIN';
  PIMSPASSWORD  = 'pims';

  ITEM_INVISIBIL    = '0';
  ITEM_VISIBIL      = '1';
  ITEM_VISIBIL_EDIT = '2';
  ITEM_DEFAULT      = '';

  // RV063.3.
  NO_SCAN = 'NOSCAN';

  // 20012085.3
  ALL_DAYS = 8;
  PERIODWEEK = 0;
  PERIODDAY = 1;

  // 20014826
  PRIORITY_MESSAGE = 1;
  PRIORITY_WARNING = 2;
  PRIORITY_ERROR = 3;
  PRIORITY_STACKTRACE = 4;
  PRIORITY_DEBUG = 5;
  // 20014826
  APPNAME_PIMS = 'Pims';
  APPNAME_AT = 'AdminTool';
  APPNAME_PS = 'PersonalScreen';
  APPNAME_PRS = 'ProductionScreen';
  APPNAME_TRS = 'TimeRecording';
  APPNAME_MANREG = 'ManualRegistrations';
  APPNAME_ADC1 = 'AutoDataCol1';
  APPNAME_ADC2 = 'AutoDataCol2';
  APPNAME_ADC3 = 'AutoDataCol3';
  APPNAME_TRSCR = 'TimeRecordingCR';

  // PIM-50
  MECHANICAL_DOWN_JOB='TDOWN';
  NO_MERCHANDIZE_JOB='PDOWN';

  // PIM-90
  DEFAULT_READ_EXT_DATA_AT_SECONDS = 30;

  // PIM-179
  ATWORKJOB     = 'ATWORK';
  ATWORKJOBDESC = 'At Work';
  OCCUPJOB      = 'OCCUP';
  OCCUPJOBDESC  = 'Occupied';

  // PIM-194
  RED_BOUNDARY = 85;
  ORANGE_BOUNDARY = 105;
  MIN_COLOR_BOUNDARY = 30;
  MAX_COLOR_BOUNDARY = 150;

  MAX_TBS = 10; // GLOB3-60
  MIN_TBS = 4; // GLOB3-60
  ISHIFT = 11; // GLOB3-60

type
 // Hours Per Employee
  TweekDates     = array[1..7] of TDateTime;
  TWeekDaysTotal = array[1..7] of Integer;
  TDailyTime     = array[1..8] of string;

// 20014550.50 Override TButton to get a colored button
type
  TButton = class( ColorButton.TColorButton )
  published
    constructor Create(AOwner: TComponent); override;
  end;

type
  TBitBtn = class( ButtonWithColor.TBitBtnWithColor )
  published
    constructor Create(AOwner: TComponent); override;
  private
    FMyTag: Integer;
    FInit: Boolean;
  public
    property Init: Boolean read FInit write FInit;
    property MyTag: Integer read FMyTag write FMyTag;
  end;

function Quotes(AValue: String): String;

implementation

function Quotes(AValue: String): String;
begin
  Result := '''' + Trim(AValue) + '''';
end;

{ TButton }

// 20014550.50
constructor TButton.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  Self.BackColor := clDarkRed; // PIM-250 // clPimsBlue; // Back color
  Self.ForeColor := clWhite; // Font color
  Self.HoverColor := clLightRed; // clPimsDarkBlue; //clNavy; // color during clicking
  Self.Font.Color := clWhite;
  Self.ParentFont := True;
end;

{ TBitBtn }

// 20014550.50
constructor TBitBtn.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  Self.Color := clDarkRed; // PIM-250 //clPimsBlue;
  Self.Font.Color := clWhite;
  Self.ParentFont := True;
end;

end.
