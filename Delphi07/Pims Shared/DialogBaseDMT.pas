unit DialogBaseDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, DBTables;

type
  TDialogBaseDM = class(TDataModule)
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure ActiveTables(Table_Active: Boolean);
  end;

var
  DialogBaseDM: TDialogBaseDM;

implementation

{$R *.DFM}

procedure TDialogBaseDM.ActiveTables(Table_Active: Boolean);
var
  DataSetCounter: Integer;
  Temp: TComponent;
begin
  if Self <> nil then
    for DataSetCounter := 0 to Self.ComponentCount - 1 do
    begin
      Temp := Self.Components[DataSetCounter];
      if (Temp is TTable) then
      begin
        if ((Temp as TTable).TableName <> '') then
          (Temp as TTable).Active := Table_Active;
      end;
    end;
end;


procedure TDialogBaseDM.DataModuleCreate(Sender: TObject);
begin
  ActiveTables(True);
end;

procedure TDialogBaseDM.DataModuleDestroy(Sender: TObject);
begin
  ActiveTables(False);
end;

end.
