object WorkspotEmpEffDM: TWorkspotEmpEffDM
  OldCreateOrder = False
  Left = 192
  Top = 125
  Height = 417
  Width = 323
  object odsWSEmp30MinEff: TOracleDataSet
    SQL.Strings = (
      
        'select t.plant_code, t.workspot_code, t.employee_number, t.qty, ' +
        't.secondsactual, t.secondsnorm, t.eff'
      'from v_emp30mineff t'
      'order by 1,2,3'
      '')
    Session = ORASystemDM.OracleSession
    Left = 200
    Top = 88
  end
  object odsWSEmp15MinEff: TOracleDataSet
    SQL.Strings = (
      
        'select t.plant_code, t.workspot_code, t.employee_number, t.qty, ' +
        't.secondsactual, t.secondsnorm, t.eff'
      'from v_emp15mineff t'
      'order by 1,2,3'
      '')
    Session = ORASystemDM.OracleSession
    Left = 200
    Top = 144
  end
  object odsWSEmp60MinEff: TOracleDataSet
    SQL.Strings = (
      
        'select t.plant_code, t.workspot_code, t.employee_number, t.qty, ' +
        't.secondsactual, t.secondsnorm, t.eff'
      'from v_emp60mineff t'
      'order by 1,2,3')
    Session = ORASystemDM.OracleSession
    Left = 200
    Top = 200
  end
  object odsWSEmpTodayMinEff: TOracleDataSet
    SQL.Strings = (
      
        'select t.plant_code, t.workspot_code, t.employee_number, t.qty, ' +
        't.secondsactual, t.secondsnorm, t.eff'
      'from v_emptodaymineff t'
      'order by 1,2,3')
    Session = ORASystemDM.OracleSession
    Left = 200
    Top = 256
  end
  object odsWSQty: TOracleDataSet
    SQL.Strings = (
      'select t.plant_code, t.workspot_code, sum(t.qty) qty'
      'from v_emptodaymineff t'
      'group by t.plant_code, t.workspot_code'
      'order by t.plant_code, t.workspot_code')
    Session = ORASystemDM.OracleSession
    Left = 200
    Top = 312
  end
  object oqCopyThis: TOracleQuery
    Session = ORASystemDM.OracleSession
    Left = 24
    Top = 24
  end
  object odsCopyThis: TOracleDataSet
    Session = ORASystemDM.OracleSession
    Left = 104
    Top = 24
  end
  object odsWSEmp1MinEff: TOracleDataSet
    SQL.Strings = (
      
        'select t.plant_code, t.workspot_code, t.employee_number, t.qty, ' +
        't.secondsactual, t.secondsnorm, t.eff'
      'from v_emp1mineff t'
      'order by 1,2,3'
      '')
    Session = ORASystemDM.OracleSession
    Left = 200
    Top = 32
  end
end
