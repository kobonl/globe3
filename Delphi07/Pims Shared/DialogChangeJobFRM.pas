(*
  MRA:23-JAN-2014 TD-24046
  - Translate issues: Use a different method to translate.
  MRA:12-JUN-2014 20014450
  - Machine hours registration
  MRA:1-MAY-2015 20014450.50 Part 2
  - Real Time Efficiency
  MRA:3-JUL-2015 20014450.50 / PIM12
  - Real Time Efficiency
  - End-Of-Day-button blink at end-of-shift (only made for Timerecording!)
  MRA:28-SEP-2015 PIM-12
  - Show optionally Break, Lunch, End-Of-Day-buttons, based on system settings.
  - End-Of-Day-button blink at end-of-shift:
    - Set original color back after blinking!
  MRA:27-JAN-2016 PIM-139
  - Lunch button optionally scan out.
*)
unit DialogChangeJobFRM;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseDialogFRM, dxCntner, Menus, StdActns, ActnList, ImgList,
  StdCtrls, Buttons, ComCtrls, ExtCtrls, dxExEdtr, dxTL, PersonalScreenDMT,
  dxDBCtrl, dxDBGrid,{UProductionScreen} {UPersonalScreen}
  UProductionScreenDefs, UTranslateShared, UPimsConst, jpeg;

const
	ModalOK                = 1;
  ModalCancel            = 2;
  ModalEndofDay          = 3;
  ModalMechanicalDown    = 4;
  ModalNoMerchandizeDown = 5;
  ModalContinue          = 6;

type
  TDialogChangeJobF = class(TBaseDialogForm)
    btnDown: TBitBtn;
    btnContinue: TBitBtn;
    Panel1: TPanel;
    btnFirst: TBitBtn;
    btnUp: TBitBtn;
    btnLow: TBitBtn;
    btnLast: TBitBtn;
    dxDBGrid1: TdxDBGrid;
    dxDBGrid1WORKSPOT_CODE: TdxDBGridMaskColumn;
    dxDBGrid1JOB_CODE: TdxDBGridMaskColumn;
    dxDBGrid1DESCRIPTION: TdxDBGridMaskColumn;
    PanelControls: TPanel;
    BitBtnOk: TBitBtn;
    BitBtnCancel: TBitBtn;
    BitBtnEndOfDay: TBitBtn;
    BtnBreak: TBitBtn;
    btnLunch: TBitBtn;
    tmrAutoClose: TTimer;
    tmrEndOfDayBlink: TTimer;
    procedure FormShow(Sender: TObject);
    procedure dxDBGrid1CustomDrawCell(Sender: TObject; ACanvas: TCanvas;
      ARect: TRect; ANode: TdxTreeListNode; AColumn: TdxTreeListColumn;
      ASelected, AFocused, ANewItemRow: Boolean; var AText: String;
      var AColor: TColor; AFont: TFont; var AAlignment: TAlignment;
      var ADone: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure btnFirstClick(Sender: TObject);
    procedure btnLastClick(Sender: TObject);
    procedure btnLowClick(Sender: TObject);
    procedure btnUpClick(Sender: TObject);
    procedure BitBtnOkClick(Sender: TObject);
    procedure BitBtnCancelClick(Sender: TObject);
    procedure BitBtnEndOfDayClick(Sender: TObject);
    procedure btnDownClick(Sender: TObject);
    procedure btnContinueClick(Sender: TObject);
    procedure dxDBGrid1Click(Sender: TObject);
    procedure BtnBreakClick(Sender: TObject);
    procedure btnLunchClick(Sender: TObject);
    procedure tmrAutoCloseTimer(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure tmrEndOfDayBlinkTimer(Sender: TObject);
  private
    { Private declarations }
    ThisCaption: String;
    ThisTop, ThisLeft, ThisHeight, ThisWidth: Integer;
    NewWorkspotCode: String;
    NewJobCode: String;
    NewJobDescription: String;
    FProdScreenType: TProdScreenType;
    FPlantCode: String;
    FWorkspotCode: String;
    FJobCode: String;
    FJobDescription: String;
    FIsDown: Boolean;
    FIsBreak: Boolean;
    ButtonBreakLeft: Integer;
    ButtonLunchLeft: Integer;
    FJobCount: Integer;
    FEndOfDayBlink: Boolean;
    EndOfDayColor: TColor;
    ButtonOkLeft: Integer;
    ButtonCancelLeft: Integer;
    ButtonEndOfDayLeft: Integer;
    ButtonOkNewLeft: Integer;
    ButtonCancelNewLeft: Integer;
    procedure WMSysCommand(var Msg: TWMSysCommand); message WM_SYSCOMMAND;
  public
    { Public declarations }
    procedure AssignPosition(ParentForm: TForm);
    procedure ResetPosition;
    procedure UseDownButton(AVisible: Boolean);
    procedure UseBreakButton(AVisible: Boolean);
    procedure UseContinueButton(AVisible: Boolean);
    procedure UseLunchButton(AVisible: Boolean);
    procedure UseEndOfDayButton(AVisible: Boolean);
    procedure BreakButtonEnable(AEnable: Boolean);
    procedure ContinueButtonEnable(AEnable: Boolean);
    property ProdScreenType: TProdScreenType read FProdScreenType
      write FProdScreenType;
    property PlantCode: String read FPlantCode write FPlantCode;
    property WorkspotCode: String read FWorkspotCode write FWorkspotCode;
    property JobCode: String read FJobCode write FJobCode;
    property JobDescription: String read FJobDescription write FJobDescription;
    property IsDown: Boolean read FIsDown write FIsDown;
    property IsBreak: Boolean read FIsBreak write FIsBreak;
    property JobCount: Integer read FJobCount write FJobCount; // 20014450
    property EndOfDayBlink: Boolean read FEndOfDayBlink write FEndOfDayBlink; // 20014450.50
  end;

var
  DialogChangeJobF: TDialogChangeJobF;

implementation

{$R *.DFM}

uses
  DialogChangeJobDMT, DialogSelectDownTypeFRM, ORASystemDMT;

procedure TDialogChangeJobF.FormCreate(Sender: TObject);
begin
  inherited;
  EndOfDayColor := BitBtnEndOfDay.Color;

//  PanelControls.Color := clPimsBlue; // 20014450.50
//  pnlBottom.Color := clPimsBlue; // 20014450.50

  // 20014450.50 Change selected row color in dxDBGrid
  dxDBGrid1.HighlightColor := clPimsDarkBlue;
  dxDBGrid1.HideSelectionColor := clPimsDarkBlue;

  // 20014450.50
  btnDown.Font.Color := clWhite;
  btnContinue.Font.Color := clWhite;
  btnBreak.Font.Color := clWhite;
  btnLunch.Font.Color := clWhite;
  BitBtnOk.Font.Color := clWhite;
  BitBtnCancel.Font.Color := clWhite;
  BitBtnEndOfDay.Font.Color := clWhite;

  ThisTop := Top;
  ThisLeft := Left;
  ThisWidth := Width;
  ThisHeight := Height;
  ThisCaption := '';
  IsDown := False;
  // Create DialogSelectDownType, so it is ready to use.
  DialogSelectDownTypeF := TDialogSelectDownTypeF.Create(Application);
  ATranslateHandlerShared.TranslateDialogSelectDownType;
  DialogSelectDownTypeF.Visible := False;
  UseBreakButton(False);
  UseLunchButton(False);
  UseEndOfDayButton(True); // PIM-12
  ButtonBreakLeft := btnBreak.Left;
  ButtonLunchLeft := btnLunch.Left;
  ButtonOkLeft := BitBtnOk.Left; // PIM-12
  ButtonCancelLeft := BitBtnCancel.Left; // PIM-12
  ButtonEndOfDayLeft := BitBtnEndOfDay.Left; // PIM-12
  ButtonOkNewLeft := BitBtnOk.Left + BitBtnEndOfDay.Width; // PIM-12
  ButtonCancelNewLeft := BitBtnCancel.Left + BitBtnEndOfDay.Width; // PIM-12
end;

procedure TDialogChangeJobF.FormShow(Sender: TObject);
begin
  inherited;
  if ThisCaption = '' then
    ThisCaption := Caption;
  // Reposition some buttons here.
  if btnDown.Visible then
  begin
    btnBreak.Left := ButtonBreakLeft;
    btnLunch.Left := ButtonLunchLeft;
  end
  else
  begin
    btnBreak.Left := btnDown.Left;
    btnLunch.Left := btnContinue.Left;
  end;
  // PIM-12 Reposition buttons in case End-Of-Day-button is not shown.
  if BitBtnEndOfDay.Visible then
  begin
    BitBtnOk.Left := ButtonOkLeft;
    BitBtnCancel.Left := ButtonCancelLeft;
    BitBtnEndOfDay.Left := ButtonEndOfDayleft;
  end
  else
  begin
    BitBtnOk.Left := ButtonOkNewLeft;
    BitBtnCancel.Left := ButtonCancelNewLeft;
  end;
  DialogChangeJobDM.JobsFound := False;
  if DialogChangeJobDM.MachineCode <> '' then
  begin
    Caption := ThisCaption + ' (' + DialogChangeJobDM.MachineCode + ' - ' +
      DialogChangeJobDM.MachineDescription + ')';
    pnlImageBase.Caption := ' ' + DialogChangeJobDM.MachineCode + ' - ' +
      DialogChangeJobDM.MachineDescription;
  end
  else
  begin
    Caption := ThisCaption + ' (' + DialogChangeJobDM.WorkspotCode + ' - ' +
      DialogChangeJobDM.WorkspotDescription + ')';
    pnlImageBase.Caption := ' ' + DialogChangeJobDM.WorkspotCode + ' - ' +
      DialogChangeJobDM.WorkspotDescription;
  end;
  NewWorkspotCode := '';
  NewJobCode := '';
  NewJobDescription := '';
  if btnDown.Visible then
  begin
    if IsDown then
    begin
      btnDown.Enabled := False;
      btnContinue.Enabled := True;
    end
    else
    begin
      btnDown.Enabled := True;
      btnContinue.Enabled := False;
    end;
  end; // if
  if btnBreak.Visible then
  begin
    btnBreak.Enabled := not IsBreak;
  end;
  if btnLunch.Visible then // PIM-139
  begin
    if not ORASystemDM.LunchBtnScanOut then
      btnLunch.Enabled := not IsBreak;
  end;
  DialogChangeJobDM.DetermineJobs;
  if DialogChangeJobDM.JobsFound then
    DialogChangeJobDM.odsJobs.Locate('JOB_CODE',
      DialogChangeJobDM.CurrentJobCode, []);
  // 20014450
  if ORASystemDM.RegisterMachineHours then
    if DialogChangeJobDM.MachineCode <> '' then
    begin
      dxDBGRid1.Visible := (JobCount = 1);
      dxDBGrid1.Enabled := (JobCount = 1);
      btnUp.Enabled := (JobCount = 1);
      btnLow.Enabled := (JobCount = 1);
      if btnBreak.Visible then // PIM-12
        btnBreak.Enabled := (JobCount = 1);
      if btnLunch.Visible then // PIM-12
        btnLunch.Enabled := (JobCount = 1);
      if BitBtnEndOfDay.Visible then // PIM-12
        BitBtnEndOfDay.Enabled := (JobCount = 1);
      BitBtnOK.Enabled := (JobCount = 1);
    end;
  // 20013176
  tmrAutoClose.Interval := ORASystemDM.AutoCloseInterval;
  if tmrAutoClose.Interval > 0 then
    tmrAutoClose.Enabled := True;
  if BitBtnEndOfDay.Visible then // PIM-12
  begin
    BitBtnEndOfDay.Color := EndOfDayColor; // PIM-12
    tmrEndOfDayBlink.Enabled := EndOfDayBlink;
  end;
end;

procedure TDialogChangeJobF.dxDBGrid1CustomDrawCell(Sender: TObject;
  ACanvas: TCanvas; ARect: TRect; ANode: TdxTreeListNode;
  AColumn: TdxTreeListColumn; ASelected, AFocused, ANewItemRow: Boolean;
  var AText: String; var AColor: TColor; AFont: TFont;
  var AAlignment: TAlignment; var ADone: Boolean);
begin
  inherited;
  if DialogChangeJobDM.JobsFound then
  begin
    if AFocused or ASelected then
    begin
      try
        NewWorkspotCode := ANode.Strings[dxDBGrid1WORKSPOT_CODE.Index];
        NewJobCode := ANode.Strings[dxDBGrid1JOB_CODE.Index];
        NewJobDescription := ANode.Strings[dxDBGrid1DESCRIPTION.Index];
{
        NewWorkspotCode := ANode.Values[dxDBGrid1WORKSPOT_CODE.Index];
        NewJobCode := ANode.Values[dxDBGrid1JOB_CODE.Index];
        NewJobDescription := ANode.Values[dxDBGrid1DESCRIPTION.Index];
}
      except
        NewWorkspotCode := '';
        NewJobCode := '';
        NewJobDescription := '';
      end;
    end;
  end;
end;

procedure TDialogChangeJobF.btnFirstClick(Sender: TObject);
begin
  inherited;
  DialogChangeJobDM.odsJobs.First;
end;

procedure TDialogChangeJobF.btnLastClick(Sender: TObject);
begin
  inherited;
  DialogChangeJobDM.odsJobs.Last;
end;

procedure TDialogChangeJobF.btnLowClick(Sender: TObject);
begin
  inherited;
  DialogChangeJobDM.odsJobs.Next;
end;

procedure TDialogChangeJobF.btnUpClick(Sender: TObject);
begin
  inherited;
  DialogChangeJobDM.odsJobs.Prior;
end;

procedure TDialogChangeJobF.BitBtnOkClick(Sender: TObject);
begin
  inherited;
  WorkspotCode := NewWorkspotCode;
  if DialogChangeJobDM.JobsFound then
  begin
    JobCode := NewJobCode;
    JobDescription := NewJobDescription;
  end
  else
  begin
    JobCode := '';
    JobDescription := '';
  end;
  ModalResult := ModalOK;
end;

procedure TDialogChangeJobF.BitBtnCancelClick(Sender: TObject);
begin
  inherited;
  ModalResult := ModalCancel;
end;

procedure TDialogChangeJobF.BitBtnEndOfDayClick(Sender: TObject);
begin
  inherited;
  ModalResult := ModalEndOfDay;
end;

procedure TDialogChangeJobF.AssignPosition(ParentForm: TForm);
begin
  Top := ParentForm.Top + Round(ParentForm.Height / 4);
  Left := ParentForm.Left + Round(ParentForm.Width / 4);
//  Position := poOwnerFormCenter;
  Height := ThisHeight;
  Width := ThisWidth;
end;

procedure TDialogChangeJobF.ResetPosition;
begin
//  Position := poDefault;
end;

procedure TDialogChangeJobF.UseDownButton(AVisible: Boolean);
begin
  btnDown.Visible := AVisible;
end;

procedure TDialogChangeJobF.btnDownClick(Sender: TObject);
begin
  inherited;
  ModalResult := ModalCancel;
  if DialogSelectDownTypeF.ShowModal = mrOK then
  begin
    case DialogSelectDownTypeF.DownType of
    1: ModalResult := ModalMechanicalDown;
    2: ModalResult := ModalNoMerchandizeDown;
    end;
  end;
end;

procedure TDialogChangeJobF.btnContinueClick(Sender: TObject);
begin
  inherited;
  ModalResult := ModalContinue;
end;

procedure TDialogChangeJobF.dxDBGrid1Click(Sender: TObject);
begin
  inherited;
  BitBtnOkClick(Sender);
//  ShowMessage(NewWorkspotCode + ' | ' + NewJobCode + ' | ' + NewJobDescription);
end;

procedure TDialogChangeJobF.BtnBreakClick(Sender: TObject);
begin
  inherited;
  ModalResult := ModalBreak;
end;

procedure TDialogChangeJobF.UseBreakButton(AVisible: Boolean);
begin
  btnBreak.Visible := AVisible;
end;

procedure TDialogChangeJobF.BreakButtonEnable(AEnable: Boolean);
begin
  if btnBreak.Visible then
    btnBreak.Enabled := AEnable;
end;

procedure TDialogChangeJobF.ContinueButtonEnable(AEnable: Boolean);
begin
  if btnContinue.Visible then
    btnContinue.Enabled := AEnable;
end;

procedure TDialogChangeJobF.UseLunchButton(AVisible: Boolean);
begin
  btnLunch.Visible := AVisible;
end;

procedure TDialogChangeJobF.btnLunchClick(Sender: TObject);
begin
  inherited;
  if ORASystemDM.LunchBtnScanOut then // PIM-139
    ModalResult := ModalLunch
  else
    ModalResult := ModalBreak;
end;

procedure TDialogChangeJobF.UseContinueButton(AVisible: Boolean);
begin
  btnContinue.Visible := AVisible;
end;

procedure TDialogChangeJobF.tmrAutoCloseTimer(Sender: TObject);
begin
  inherited;
  tmrAutoClose.Enabled := False;
  Close;
end;

procedure TDialogChangeJobF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  if BitBtnEndOfDay.Visible then // PIM-12
    BitBtnEndOfDay.Color := EndOfDayColor; // PIM-12
  tmrAutoClose.Enabled := False;
  tmrEndOfDayBlink.Enabled := False;
end;

// 20014450.50 Prevent it is being moved
procedure TDialogChangeJobF.WMSysCommand(var Msg: TWMSysCommand);
begin
  if ((Msg.CmdType and $FFF0) = SC_MOVE) or
    ((Msg.CmdType and $FFF0) = SC_SIZE) then
  begin
    Msg.Result := 0;
    Exit;
  end;
  inherited;
end;

// 20014450.50
procedure TDialogChangeJobF.tmrEndOfDayBlinkTimer(Sender: TObject);
begin
  inherited;
  if BitBtnEndOfDay.Visible then // PIM-12
  begin
    if BitBtnEndOfDay.Color = EndOfDayColor then
      BitBtnEndOfDay.Color := clMyRed
    else
      BitBtnEndOfDay.Color := EndOfDayColor;
  end;
end;

// PIM-12
procedure TDialogChangeJobF.UseEndOfDayButton(AVisible: Boolean);
begin
  BitBtnEndOfDay.Visible := AVisible;
end;

end.
