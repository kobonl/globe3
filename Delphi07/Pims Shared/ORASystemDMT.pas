(*
   Changes:
     MRA:2-MAR-2009 RV023. (Revision 23)
       Changes: property added for get/set of ExportPayrollType.
     MRA:3-JUN-2009 RV029.
       If ADC2 is used, do not read the CLIENTNAME when getting
       COMPUTERNAME.
     MRA:9-NOV-2010 RV072.1.
     - Added 'IsRFL' to detect if Pimssetting.code is set
       to 'RFL'.
     MRA:4-MAY-2011. RV076.1. Addition. SO-20011705
     - Added IsTLB for this order.
     MRA:14-JUL-2011 RV078.1. Bugfix.
    - When started at CLOVA it gives:
      - ORA-00001: Unique constraint (PIMS - XPKWORKSTATION) violated.
      - Use 'try-except' to trap this error during startup,
        when workstation is added.
    MRA:26-SEP-2011 RV081.1. Addition.
    - When there is no Oracle- or network-connection during startup, there
      will be a message about what was wrong before application will terminate,
      instead of only terminating.
    MRA:2-NOV-2011 RV082.1. (D5: RV100.1) 20012124. Tailor made CVL.
    - Added 'IsCVL'-property for Tailor-made-functionality.
    MRA:19-APR-2012 20012858
    - Added EffBoQuantYN (Efficiency based on Quantity (or Time)) in
      Pimssettings for use with Personal Screen, and also for some
      reports (Pims) that calculate efficiency.
    MRA:24-MAY-2012 20013176
    - Related to this software error:
      - Addition of error logging.
    MRA:21-JUN-2012 20012858. Rework.
    - Addition of PIMSSETTING PS_AUTO_CLOSE_SECS. This is used to define
      the seconds it takes to autom. close a sub-dialog in Personal Screen.
    MRA:11-FEB-2013 TD-22082 Perfomance problems
    - Added functionality related to:
      - Filter on teams-per-user to limit the number records read.
    MRA:1-MAR-2013 TD-22171 Datacol; store quantiy in last 5 minute-period
    - Addition of PIMSSETTING: DATACOL_STORE_QTY_5MIN_PERIOD
    - Variable: DatacolStoreQty5MinPeriod
      - Possible values: 'Y' or 'N'. Default 'N'.
    - Can only be set using a script.
    MRA:19-AUG-2013 TD-22699
    - Added timer of 15 minutes to make it possible to get the time again from
      database.
    MRA:27-SEP-2013 Card Reader Troubleshoot (CANCELLED).
    - Added INI-file-setting to make it possible to ConnectShared or
      ConnectExclusive: CardReaderConnectExclusive (default False)
    MRA:2-OCT-2013 TD-23247
    - Addition of new WORKSTATION-field CARDREADER_DELAY that can be
      used to slow done the reading of cards via contactless card
      readers in case the reading is not reliable. This value is in
      milliseconds.
    - Added Wait-procedure.
    MRA:10-OCT-2013 20014327
    - Make use of Overnight-Shift-System optional.
      - Addition of field SHIFTDATESYSTEM_YN to PIMSSETTING-table.
      - The contents of this field can only be changed by script.
    MRA:30-MAY-2014 20015178
    - Measure Performance without employees
    MRA:12-JUN-2014 20014450
    - Machine hours registration
    MRA:14-JUL-2014 20015302
    - Manual Registrations: Added workstation-setting for determination
      if multiple jobs must be used or not: EnableManRegsMultipleJobs.
    MRA:22-JUL-2014 TD-25352
    - Host name too long: Extend clientname/computer from 15 to 32 chars.
    MRA:10-NOV-2014 20014826
    - Log error messages optionally to database.
    MRA:27-MAR-2015 TD-26942
    - Machines + workspots + no jobs in dialog problem
    - Note: There was a hardcoded setting (RegisterMachineHours) active that
            was not in use yet! (20014550)
    MRA:16-MAR-2015 20015346
    - Store scans in seconds
    - Cut off time and breaks handling
    - Creation-date and Mutation-date handling for scans
    - Addition of 2 Pims-Settings:
      - TimerecordingInSecs
      - IgnoreBreaks
    MRA:1-APR-2015 SO-20016449
    - Time zone implementation
    MRA:8-MAY-2015 SO-20014450.50
    - Real Time Efficiency
    - Always calculate efficiency based on time
    - Use views to show data
    MRA:21-JUL-2015 PIM-50
    - Addition of job comments for down jobs (system settings)
    MRA:14-SEP-2015 PIM-87
    - Addition of pims-setting UsePackageHourCalc (boolean). When true then
      it will use a package that handles hour calculations.
    MRA:28-SEP-2015 PIM-12
    - Addition of system setings to optionally show Break, Lunch
      and End-Of-Day-buttons.
    MRA:9-OCT-2015 PIM-90
    - Adapt ABSSolute Script Quantities to PIMS for Personal Screen
    - Added pimssetting named ExternQtyFromPQ (PIMSSETTING.EXTERN_QTY_FROM_PQ_YN)
    MRA:14-OCT-2015 PIM-12
    - EffBoQuantYN - setting
    - Because of problems when showing eff. based on time, we use
      eff. based on quantity instead. This refers to setting EffBoQuantYN.
      - When using eff-based-on-time with 'current' it shows wrong values
        when first you have quantities received for job A and then go to
        job B after which you do not receive qty anymore. Then the eff. is not
        changing for a long time (until current-period has passed, but then it
        goes to 0), while it should change because of a different job-norm.
  MRA:22-DEC-2015 PIM-12
  - Use TOracleQuery for inserting workstation-record
  MRA:27-JAN-2016 PIM-139
  - Lunch button optionally scan out.
  MRA:26-FEB-2016 PIM-12
  - Settings must always be set to TRUE (non-configurable!):
    - TimerecordingInSecs
    - IgnoreBreaks
  MRA:4-MAR-2016 PIM-125
  - When no one is scanned in anymore, do not log errors.
  MRA:18-MAR-2016 PIM-155
  - The EnableMachineTimeRec-variable was set to False, so it did not work
    anymore.
  MRA:19-MAY-2016 PIM-180
  - Addition of pims-setting-field HYBRID (0=none 1=hybrid 2=future mode)
  - Property is called HYBRIDMODE.
  - This is a read-only property. It can only be changed via a script.
  - When HYBRID=1 then a hybrid-version can be implemented/used for
    the COCKPIT-software.
  MRA:7-JUN-2016 PIM-188
  - Wrong Shift Date. Because of a bug it first read the pims-settings and then
    assigned 'UseShiftDateSystem' to False.
  MRA:20-JUN-2016 PIM-194
  - Use 3 colors for efficiency-bars.
  - Addition of 2 variables that can be used for Personal Screen and
    Prod. Screen: RedBoundary and OrangeBoundary.
  MRA:11-JAN-2017 PIM-254
  - Set environment-variables in the program to make it possible to start
    it without using a batch-file.
  MRA:23-OCT-2017 PIM-319
  - Do not subtract breaks from production hours.
  MRA:15-DEC-2017 PIM-330.2
  - Add option to only log message, no display, in case a program is
    running via a task-scheduler, it does not display anything!
  MRA:27-DEC-2017 PIM-337
  - Use settings via PIMSORA.INI to make environment settings for Oracle, so
    it can be started without a batch-file.
  - Do not show a message when this program is started while it was already
    started.
  MRA:11-JAN-2018 PIM-337
  - TimeRecording
    - Change needed for getting DB-settings via PIMSORA.INI-file:
      - Read these from a different section [TIMERECORDING]
  MRA:26-FEB-2018 GLOB3-85
  - TimeRecording
  - Show non-occupied workspots
  - Only for NTS
  MRA:2-JUL-2018 GLOB3-60
  - Extension 4 to 10 blocks for planning
  - Added pimssetting (MaxTimeblocks) that can be 4 or 10.
  MRA:31-AUG-2018 GLOB3-60
  - Extension 4 to 10 blocks for planning
  - Bugfix: When using Configuration, it resets max-timeblocks to 4 again.
  - This has been fixed.
  MRA:27-SEP-2018 GLOB3-127
  - TimeRecordingCR Improve handling LEDS
  - Because of multi-threading, when only 1 session is used, set
    OracleSession.ThreadSave to True!
  MRA:4-JAN-2019 GLOB3-202
  - Scan via machine-card readers and do not scan out
  MRA:1-MAR-2019 GLOB3-223
  - Restructure availability of functionality made for NTS
  MRA:8-JUL-2019 GLOB3-330
  - Make functionality with 'occupied' optional
  MRA:22-JUL-2019 GLOB3-284
  - Week start at Wednesday
  MRA:26-JUL-2019 GLOB3-337
  - All programs should be startable via exe instead of batch, just like TR
  - Do this for PRODUCTIONSCREEN.
*)
unit ORASystemDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DB, OneSolar, Provider, DBClient, Registry, Oracle, Inifiles, FileCtrl,
  DateUtils, UPimsConst,
{$IFDEF ORAMONITOR}
  OracleMonitor,
{$ENDIF}
  OracleData, ExtCtrls;

const
  DefaultMaxTimeblocks = 4;
  DefaultMaxTimeblocks10 = 10;

type
  THybridMode=(hmNone, hmHybrid);

type
  TOvertime=(OTNone, OTDay, OTWeek, OTPeriod, OTMonth);

type
  // MR:09-07-2003
  // Date-format-class for use in setting filters that
  // use (BDE)-dates.
  // Usage:
  //   In SystemDM a variable 'ADateFmt' is created for this class.
  //   This variable has 2 public methods and 2 public strings.
  //   Use it in combination with 'FormatDateTime'-function.
  // Example of use:
  //   with SystemDM do
  //   begin
  //     ADateFmt.SwitchDateTimeFmtORA;
  //     MyFormDM.TableDetail.Filter :=
  //       '(STARTDATE <= ' +
  //       ADateFmt.Quote +
  //       FormatDateTime(ADateFmt.DateTimeFmt, Frac(CurrentDate)) +
  //       ADateFmt.Quote + ')' +
  //       ' AND ' +
  //       '(ENDDATE >= ' +
  //       ADateFmt.Quote +
  //       FormatDateTime(ADateFmt.DateTimeFmt, Frac(CurrentDate)) +
  //       ADateFmt.Quote + ')';
  //     ADateFmt.SwitchDateTimeFmtWindows;
  //   end;
  //
  // Remark:
  // When using 'Abort' please put use syntax 'SysUtils.Abort'.
  // Otherwise it's confused with the BDE-Abort by the compiler.
  TDateFmt = class
  private
    FDateTimeFmt: String;
    FQuote: String;
    FSepORA: String;
    FDateFmtORA: String;
    FTimeFmtORA: String;
    FSepWindows: String;
    FDateFmtWindows: String;
    FTimeFmtWindows: String;
    function CreateDateTimeFmt: String;
  public
    procedure AfterConstruction; override;
    procedure SwitchDateTimeFmtORA;
    procedure SwitchDateTimeFmtWindows;
// Read only properties!
    property DateTimeFmt: String read FDateTimeFmt;
    property Quote: String read FQuote;
  end;

  //CAR 7-5-2003
  // type class defined for remember values between forms
  // timerecording scanning, hours per employee
  TSaveTimeRecScanning = class
    AEmployeeNumber: Integer;
    ADateScanning: TDatetime;
    AShiftNumber: Integer;
    procedure SetEmployee(const Value: Integer);
    procedure SetShift(const Value: Integer);
    procedure SetDateScanning(const Value: TDateTime);
  public
    constructor Create;
   { procedure SetValue(EmployeeNumber, ShiftNumber: Integer;
      DateScanning: TDateTime);}
    property Employee: Integer read AEmployeeNumber write SetEmployee;
    property Shift: Integer read AShiftNumber write SetShift;
    property DateScanning: TDateTime read ADateScanning write SetDateScanning;
  end;
  //end

  TORASystemDM = class(TDataModule)
    OnePims: TOneSolar;
    OracleSession: TOracleSession;
    OracleLogon: TOracleLogon;
    odsWorkStation: TOracleDataSet;
    odsCopyThis: TOracleDataSet;
    odsPimsSettings: TOracleDataSet;
    odsPimsSettingsSAVE_DAY_PRD_INFO: TIntegerField;
    odsPimsSettingsSAVE_DETAIL_PRD_INFO: TIntegerField;
    odsPimsSettingsUNIT_WEIGHT: TStringField;
    odsPimsSettingsUNIT_PIECES: TStringField;
    odsPimsSettingsWEEK_START_ON: TIntegerField;
    odsPimsSettingsSETTING_PASSWORD: TStringField;
    odsPimsSettingsVERSION_NUMBER: TIntegerField;
    odsPimsSettingsCHANGE_SCAN_YN: TStringField;
    odsPimsSettingsDATACOL_PATH: TStringField;
    odsPimsSettingsDATACOL_READFILES_INTERVAL: TIntegerField;
    odsPimsSettingsDATACOL_PRESET_STARTTIME: TDateTimeField;
    odsPimsSettingsDATACOL_TIME_INTERVAL: TIntegerField;
    odsPimsSettingsREAD_PLANNING_ONCE_YN: TStringField;
    odsPimsSettingsYEARS_FOR_SICK_PAY: TIntegerField;
    odsPimsSettingsHOURTYPE_SICK_PAY: TIntegerField;
    odsPimsSettingsMINUTES_SICK_PAY: TIntegerField;
    odsPimsSettingsMAX_MINUTES_SICK_PAY: TIntegerField;
    odsPimsSettingsUPDATE_DELETE_EMP_YN: TStringField;
    odsPimsSettingsFILLPRODUCTIONHOURS_YN: TStringField;
    odsPimsSettingsUPDATE_DELETE_PLANT_YN: TStringField;
    odsPimsSettingsDELETE_WK_YN: TStringField;
    odsPimsSettingsFIRST_WEEK_OF_YEAR: TIntegerField;
    odsPimsSettingsEMPLOYEES_ACROSS_PLANTS_YN: TStringField;
    oqServerTime: TOracleQuery;
    ClientDataSetDay: TClientDataSet;
    DataSetProviderDay: TDataSetProvider;
    odsDay: TOracleDataSet;
    odsDayDAY_NUMBER: TIntegerField;
    odsDayDAY_CODE: TStringField;
    odsDayDESCRIPTION: TStringField;
    odsExportPayroll: TOracleDataSet;
    odsPimsSettingsCODE: TStringField;
    odsPimsSettingsEFF_BO_QUANT_YN: TStringField;
    odsPimsSettingsPS_AUTO_CLOSE_SECS: TIntegerField;
    odsPlantDeptTeam: TOracleDataSet;
    TimerResetTime: TTimer;
    odsPimsSettingsENABLE_MACHINETIMEREC_YN: TStringField;
    oqABSLogInsert: TOracleQuery;
    odsPimsSettingsERROR_LOG_TO_DB_YN: TStringField;
    odsPimsSettingsTIMEREC_IN_SECONDS_YN: TStringField;
    odsPimsSettingsIGNORE_BREAKS_YN: TStringField;
    odsPimsSettingsTIMEZONE: TStringField;
    oqPlant: TOracleQuery;
    oqPlantUpdate: TOracleQuery;
    oqPlantFind: TOracleQuery;
    odsPimsSettingsPSSHOWEMPEFF_YN: TStringField;
    odsPimsSettingsPSSHOWEMPINFO_YN: TStringField;
    odsPimsSettingsJOBCOMMENT_YN: TStringField;
    odsPimsSettingsPACKAGE_HOUR_CALC_YN: TStringField;
    odsPimsSettingsBREAK_BTN_YN: TStringField;
    odsPimsSettingsLUNCH_BTN_YN: TStringField;
    odsPimsSettingsTR_EOD_BTN_YN: TStringField;
    odsPimsSettingsPS_EOD_BTN_YN: TStringField;
    odsPimsSettingsMANREG_EOD_BTN_YN: TStringField;
    odsPimsSettingsEXTERN_QTY_FROM_PQ_YN: TStringField;
    oqWorkstationAdd: TOracleQuery;
    odsPimsSettingsLUNCH_BTN_SCANOUT_YN: TStringField;
    odsPimsSettingsHYBRID: TIntegerField;
    odsPimsSettingsPRODHRS_NOSUBTRACT_BREAKS: TStringField;
    odsPimsSettingsMAX_TIMEBLOCKS: TIntegerField;
    odsPimsSettingsTIMERECCR_DONOTSCANOUT_YN: TStringField;
    odsPimsSettingsOVERTIME_PERDAY_YN: TStringField;
    odsPimsSettingsOCCUP_HANDLING: TIntegerField;
    procedure DefaultBeforeDelete(DataSet: TDataSet);
    procedure DefaultBeforePost(DataSet: TDataSet);
    procedure DefaultDeleteError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure DefaultEditError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure DefaultNewRecord(DataSet: TDataSet);
    procedure DefaultPostError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure DefaultNotEmptyValidate(Sender: TField);
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure TablePIMSettingsNewRecord(DataSet: TDataSet);
    procedure TimerResetTimeTimer(Sender: TObject);
  private
    FDetailedProd: Integer;
    FProcessDayQantity:  Integer;
    FUnitsWeigh: String;
    FUnitsPieces: String;
    FWeekStartsOn: Integer;
    //car 4-12-2003
    FFirstWeekOfYear: Integer;
    FSettingsPassword : String;
    FReadPlanningOnceYN: String;
    FUpdateEmployeeYN: String;
    FUpdatePlantYN: String;
//CAR 21-8-2003
    FDeleteWKYN: String;
    FVersionNumber: Integer;
    FChangeScan: String;
    FTimeRecordingStation: String;
    FEmployeesAcrossPlantsYN: String;
    FExportPayrollType: String;
    FADC2: boolean;
    FProdScreen: boolean;
    FIsRFL: Boolean;
    FIsLTB: Boolean;
    FIsCVL: Boolean;
    FEffBoQuantYN: String;
    FPSAutoCloseSecs: Integer;
    FUserTeamSelectionYN: String; // TD-22082
    FCurrentLoginUser: String;
    FPlantTeamFilterOn: Boolean; // TD-22082
    FDatacolStoreQty5MinPeriod: String;
    FCardReaderConnectExclusive: Boolean;
    FCardReaderDelay: Integer;
    FUseShiftDateSystem: Boolean;
    FEnableMachineTimeRec: Boolean;
    FRegisterMachineHours: Boolean;
    FEnableManRegsMultipleJobs: Boolean;
    FCurrentComputerName: String;
    FLogToDB: Boolean;
    FAppName: String;
    FIgnoreBreaks: Boolean;
    FTimerecordingInSecs: Boolean;
    FWSTimezonehrsdiff: Integer;
    FDatabaseTimeZone: String;
    FRealTimeEff: Boolean;
    FPSShowEmpEff: Boolean;
    FPSShowEmpInfo: Boolean;
    FAddJobComment: Boolean;
    FUsePackageHourCalc: Boolean;
    FBreakBtnVisible: Boolean;
    FLunchBtnVisible: Boolean;
    FTREndOfDayBtnVisible: Boolean;
    FPSEndOfDayBtnVisible: Boolean;
    FMANREGEndOfDayBtnVisible: Boolean;
    FExternQtyFromPQ: Boolean;
    FLunchBtnScanOut: Boolean;
    FLogErrors: Boolean;
    FHybridMode: THybridMode;
    FTimewarp: Integer;
    FRedBoundary: Integer;
    FOrangeBoundary: Integer;
    FProdHrsDoNotSubtractBreaks: Boolean;
    FOnlyLogMessages: Boolean;
    FIsNTS: Boolean;
    FMaxTimeblocks: Integer;
    FTimeRecCRDoNotScanOut: Boolean;
    FOvertimePerDay: Boolean;
    FOccup_Handling: Integer;
    function GetCurrentProgramUser: String;
    function GetCurrentComputerName: String;
    function GetCurrentClientEnvName: String;
    function GetCurrentComputerEnvName: String;
    function GetExportPayrollType: String;
    procedure SetExportPayrollType(const Value: String);
    procedure SetIsRFL(const Value: Boolean);
    procedure SetIsLTB(const Value: Boolean);
    procedure SetIsCVL(const Value: Boolean);
    procedure SetCurrentComputerName(const Value: String);
    procedure AppNameSet(const Value: String);
    procedure SetOrangeBoundary(const Value: Integer);
    procedure SetRedBoundary(const Value: Integer);
    procedure SetIsNTS(const Value: Boolean);
    { Private declarations }
  public
    { Public declarations }
    ADateFmt: TDateFmt;
//    Car 17-10-2003 - 550262
//    ASaveTimeRecScanning: TSaveTimeRecScanning;
    //
    property ReadPlanningOnceYN: String read FReadPlanningOnceYN write FReadPlanningOnceYN;
    property CurrentComputerName: String read GetCurrentComputerName write SetCurrentComputerName;
    property DetailedProd: Integer read FDetailedProd write FDetailedProd;
    property ProcessDayQantity : Integer read FProcessDayQantity write FProcessDayQantity;
    property UnitsWeigh: String read FUnitsWeigh write FUnitsWeigh;
    property UnitsPieces: String read FUnitsPieces write FUnitsPieces;
    property WeekStartsOn: Integer read FWeekStartsOn write FWeekStartsOn;
    property SettingsPassword: String read FSettingsPassword write FSettingsPassword;
    property VersionNumber: Integer read FVersionNumber write FVersionNumber;
    property ChangeScan: String read FChangeScan write FChangeScan;
    property TimeRecordingStation: String read FTimeRecordingStation
       write FTimeRecordingStation;
    property CurrentProgramUser: String read GetCurrentProgramUser;
    property UpdatePlantYN: String read FUpdatePlantYN write FUpdatePlantYN;
    property UpdateEmployeeYN: String read FUpdateEmployeeYN write FUpdateEmployeeYN;
    property DeleteWKYN: String read FDeleteWKYN write FDeleteWKYN;
//car 4-12-2003
    property FirstWeekOfYear: Integer read FFirstWeekOfYear write FFirstWeekOfYear;
    //CAR 7-5-2003
   {
    property SaveTimeRecScanning: TSaveTimeRecScanning read FSaveTimeRecScanning
      write FSaveTimeRecScanning;}
    function GetFillProdHourPerHourTypeYN: String;
    function GetDayIndex(Index: Integer): Integer;
    function GetDayWCode(Index: Integer): String;
    function GetDayWDescription(Index: Integer): String;
    procedure AssignSettings;
    procedure SetSettings;
    procedure GetSettings;

    procedure SynchronizeWorkStation;
//    function GetQualityIncentiveConf: Boolean;
    //CAR: 550299
//    function GetGradeYN: Boolean;
    function GetExportPayroll: Boolean;

    procedure OpenSystemTables;
    // procedures for updating database - begin
    function UpdatePimsBase(DatabaseName: String): Boolean;
//  Car 6-5-2004
    function ReadRegistry(Str_OpenKey, Str_Read: String): String;
    procedure WriteRegistry(Str_OpenKey, Str_Read, Str_Write: String);
    function GetCode: String;
//    procedure WErrorLog(AMsg: String);
    function AutoCloseInterval: Integer;
    function UserTeamLoginUser: String; // TD-22082
    procedure PlantTeamFilterEnable(DataSet: TDataSet); // TD-22082
    function PlantTeamFilter(DataSet: TDataSet): Boolean; // TD-22082
//    function GetMonthGroupEfficiency: Boolean;
    procedure Wait(const MSecsToWait : Integer);
    function DBLog(AMsg: String; APriority: Integer): Boolean; // 20014862
    procedure DetermineWorkstationTimezonehrsdiff;
    procedure PlantTimezonehrsdiffWriteToDB;
    function PlantTimezonehrsdiff(APlantCode: String): Integer;
    function NewEffColor(AEff: Double): TColor;
    property EmployeesAcrossPlantsYN: String read FEmployeesAcrossPlantsYN
      write FEmployeesAcrossPlantsYN;
    property CurrentClientEnvName: String read GetCurrentClientEnvName;
    property CurrentComputerEnvName: String read GetCurrentComputerEnvName;
    property ExportPayrollType: String read GetExportPayrollType
      write SetExportPayrollType;
    property ADC2: boolean read FADC2 write FADC2;
    property ProdScreen: boolean read FProdScreen write FProdScreen;
    property IsRFL: Boolean read FIsRFL write SetIsRFL;
    property IsLTB: Boolean read FIsLTB write SetIsLTB;
    property IsCVL: Boolean read FIsCVL write SetIsCVL;
    property EffBoQuantYN: String read FEffBoQuantYN write FEffBoQuantYN;
    property PSAutoCloseSecs: Integer read FPSAutoCloseSecs
      write FPSAutoCloseSecs;
    property UserTeamSelectionYN: String read FUserTeamSelectionYN
      write FUserTeamSelectionYN; // TD-22082
    property CurrentLoginUser: String read FCurrentLoginUser
      write FCurrentLoginUser;  // TD-22082
    property PlantTeamFilterOn: Boolean read FPlantTeamFilterOn
      write FPlantTeamFilterOn; // TD-22082
    property DatacolStoreQty5MinPeriod: String read FDatacolStoreQty5MinPeriod
      write FDatacolStoreQty5MinPeriod; // TD-22171
    property CardReaderConnectExclusive: Boolean read FCardReaderConnectExclusive
      write FCardReaderConnectExclusive default False;
    property CardReaderDelay: Integer read FCardReaderDelay
      write FCardReaderDelay default 0; // TD-23247
    property UseShiftDateSystem: Boolean read FUseShiftDateSystem
      write FUseShiftDateSystem; // 20014327
    property EnableMachineTimeRec: Boolean
      read FEnableMachineTimeRec write FEnableMachineTimeRec; // 20015178
    property RegisterMachineHours: Boolean
      read FRegisterMachineHours write FRegisterMachineHours; // 20014450
    property EnableManRegsMultipleJobs: Boolean
      read FEnableManRegsMultipleJobs write FEnableManRegsMultipleJobs; // 20015302
    property LogToDB: Boolean read FLogToDB write FLogToDB; // 20014826
    property AppName: String read FAppName write AppNameSet; // 20014826
    property TimerecordingInSecs: Boolean read FTimerecordingInSecs write FTimerecordingInSecs; // 20015346
    property IgnoreBreaks: Boolean read FIgnoreBreaks write FIgnoreBreaks; // 20015346
    property WSTimezonehrsdiff: Integer read FWSTimezonehrsdiff write FWSTimezonehrsdiff; // 20016449
    property DatabaseTimeZone: String read FDatabaseTimeZone write FDatabaseTimeZone; // 20016449
    property RealTimeEff: Boolean read FRealTimeEff write FRealTimeEff; // 20014450.50
    property PSShowEmpEff: Boolean read FPSShowEmpEff write FPSShowEmpEff; // 20014450.50
    property PSShowEmpInfo: Boolean read FPSShowEmpInfo write FPSShowEmpInfo; // 20014450.50
    property AddJobComment: Boolean read FAddJobComment write FAddJobComment; // PIM-50
    property UsePackageHourCalc: Boolean read FUsePackageHourCalc write FUsePackageHourCalc; // PIM-87
    property BreakBtnVisible: Boolean read FBreakBtnVisible write FBreakBtnVisible; // PIM-12
    property LunchBtnVisible: Boolean read FLunchBtnVisible write FLunchBtnVisible; // PIM-12
    property TREndOfDayBtnVisible: Boolean read FTREndOfDayBtnVisible write FTREndOfDayBtnVisible; // PIM-12
    property PSEndOfDayBtnVisible: Boolean read FPSEndOfDayBtnVisible write FPSEndOfDayBtnVisible; // PIM-12
    property MANREGEndOfDayBtnVisible: Boolean read FMANREGEndOfDayBtnVisible write FMANREGEndOfDayBtnVisible; // PIM-12
    property ExternQtyFromPQ: Boolean read FExternQtyFromPQ write FExternQtyFromPQ; // PIM-90
    property LunchBtnScanOut: Boolean read FLunchBtnScanOut write FLunchBtnScanOut; // PIM-139
    property LogErrors: Boolean read FLogErrors write FLogErrors default True; // PIM-125
    property HybridMode: THybridMode read FHybridMode; // PIM-180
    property Timewarp: Integer read FTimewarp write FTimewarp default 0;
    property RedBoundary: Integer read FRedBoundary write SetRedBoundary default RED_BOUNDARY;
    property OrangeBoundary: Integer read FOrangeBoundary write SetOrangeBoundary default ORANGE_BOUNDARY;
    property ProdHrsDoNotSubtractBreaks: Boolean read FProdHrsDoNotSubtractBreaks write FProdHrsDoNotSubtractBreaks; // PIM-319
    property OnlyLogMessages: Boolean read FOnlyLogMessages write FOnlyLogMessages;
    property IsNTS: Boolean read FIsNTS write SetIsNTS;
    property MaxTimeblocks: Integer read FMaxTimeblocks write FMaxTimeblocks; // GLOB3-60
    property TimeRecCRDoNotScanOut: Boolean read FTimeRecCRDoNotScanOut write FTimeRecCRDoNotScanOut; // GLOB3-202
    property OvertimePerDay: Boolean read FOvertimePerDay write FOvertimePerDay; // GLOB3-223
    property Occup_Handling: Integer read FOccup_Handling write FOccup_Handling; // GLOB3-330
  end;

function DisplayMessage(const Msg: String; DlgType: TMsgDlgType;
  Buttons: TMsgDlgButtons): Integer;
function DisplayMessageFmt(const Msg: String; const Args: array of const;
  DlgType: TMsgDlgType; Buttons: TMsgDlgButtons): Integer;
function DoubleQuote(Code: String): String;
function GetErrorMessage(const PimsError: EDataBaseError;
  const PimsActivity: Integer): String;
function DisplayErrorMessage(const PimsError: EDataBaseError;
  const PimsActivity: Integer):Integer;
function IsErrorFromException(ErrorMessage: String;
  var ErrorCode: Integer): Boolean;
procedure GetBuildInfo(var V1, V2, V3, V4: Word);
function DisplayVersionInfo: String;
procedure CheckValidNumber(var Key:Char);
procedure ActiveTables(FDataModule: TDataModule; ActiveYN: Boolean);
function BoolYN(BoolVal: Boolean): String;
function YNBool(BoolVal: String): Boolean;

function GetStrValue(StrTmp: String): String;
function GetIntValue(StrIntTmp: String): Integer;
function CompareDate(Date_1, Date_2: TDateTime): Integer;
//replace system functions -car 27.02.2003
function Now: TDateTime;
function Date: TDateTime;
function Time: TDateTime;

// 20016449
function DateTimeTimeZone(ADateTime: TDateTime): TDateTime;
function DateTimeWithoutTimeZone(ADateTime: TDateTime): TDateTime;
function FormatDateTime(const Format: string; DateTime: TDateTime): string; //overload;
function DateTimeToStr(const DateTime: TDateTime): string;

var
  ORASystemDM: TORASystemDM;
// KME 23-08-2004 - Call Now function once a session because 'Now'is a select in
// Interbase system table
// Reselect from database when System time is changed
// Handle message WM_TIMECHANGE in MainForm;
var
  PimsTimeInitialised: Boolean = False;

implementation

uses
  UPimsMessageRes, UPimsVersion,
  UGlobalFunctions, UTimeZone;
// MR:19-05-2004 Disabled, is not used here?
//  DialogSettingsFRM;

{$R *.DFM}

// KME 23-08-2004 - Call Now function once a session because 'Now'is a select in
// Interbase system table
var
  PimsTimeDifference: TDateTime = 0;

// CAR 07-05-2003 : Save value between TimeRecScanning and HoursPerEmpl
// CAR 17-10-2003 : 550262
constructor TSaveTimeRecScanning.Create;
begin
  inherited Create;
  Employee     := -1;
  Shift        := -1;
  DateScanning := EncodeDate(1900, 1, 1);
end;

procedure TSaveTimeRecScanning.SetEmployee(const Value: Integer);
begin
  AEmployeeNumber := Value;
end;

procedure TSaveTimeRecScanning.SetShift(const Value: Integer);
begin
  AShiftNumber := Value;
end;

procedure TSaveTimeRecScanning.SetDateScanning(const Value:  TDateTime);
begin
  ADateScanning := Value;
end;

function TORASystemDM.ReadRegistry(Str_OpenKey, Str_Read: String): String;
var
  Reg: TRegistry;
begin
  Reg := TRegistry.Create;
  try
    Reg.RootKey := HKEY_LOCAL_MACHINE;
    if Reg.OpenKey(Str_OpenKey, True) then
      Result := Reg.ReadString(Str_Read);
  finally
    Reg.CloseKey;
    Reg.Free;
  end;
end;

procedure TORASystemDM.WriteRegistry(Str_OpenKey, Str_Read, Str_Write: String);
var
  Reg: TRegistry;
begin
  Reg := TRegistry.Create;
  try
    Reg.RootKey := HKEY_LOCAL_MACHINE;
    if Reg.OpenKey(Str_OpenKey, True) then
      Reg.WriteString(Str_Read, Str_Write);
  finally
    Reg.CloseKey;
    Reg.Free;
  end;
end;

// functions which will get the server date time car 27.02.2003
// KME 23-08-2004 - Call Now function once a session because 'Now'is a select in Interbase
// system table
function Time: TDateTime;
begin
  Result := Frac(Now);
end;

function Date: TDateTime;
begin
  Result := Trunc(Now);
end;

function Now: TDateTime;
var
  PimsTime: TDateTime;
begin
  if not PimsTimeInitialised then
  begin
  (* ORACLE TEST *)
    ORASystemDM.oqServerTime.Execute;
    PimsTime := ORASystemDM.oqServerTime.Field('SERVERDATETIME');
    PimsTimeDifference := SysUtils.Now - PimsTime;
    PimsTimeInitialised := True;
  end;
  // MR:14-12-2004 Difference should be subtracted instead of added!
  Result := SysUtils.Now - PimsTimeDifference;
  Result := DateTimeTimeZone(Result); // 20016449
{$IFDEF DEBUG}
  if ORASystemDM.Timewarp <> 0 then
    Result := Result + ORASystemDM.Timewarp; // Only for test-purposes
{$ENDIF}
end;
// end car

// 0 if equal
// 1 if date_1 > date_2
//-1 date_1 < date_2
function CompareDate(Date_1, Date_2: TDateTime): Integer;
var
  Year_1, Month_1, Day_1: Word;
  Year_2, Month_2, Day_2: Word;
begin
  ReplaceTime(Date_1, 0);
  ReplaceTime(Date_2, 0);
  DecodeDate(Date_1, Year_1, Month_1, Day_1);
  DecodeDate(Date_2, Year_2, Month_2, Day_2);
  Result := 1;
  if (Year_1 = Year_2) and (Month_1 = Month_2) and (Day_1 = Day_2) then
    Result := 0;
  if (Year_1 < Year_2) or
   ((Year_1 = Year_2) and (Month_1 < Month_2)) or
   ((Year_1 = Year_2) and (Month_1 = Month_2) and (Day_1 < Day_2)) then
     Result := -1;
end;

function GetStrValue(StrTmp: String): String;
var
  P: Integer;
begin
  P := Pos(Str_Sep, StrTmp);
  if p <= 0 then
    Result := ''
  else
    Result := Copy(StrTmp, 0 , p-1);
end;

function GetIntValue(StrIntTmp: String): Integer;
var
  P: Integer;
begin
  P := Pos(Str_Sep, StrIntTmp);
  if p <= 0 then
    Result := 0
  else
    Result := StrToInt(Copy(StrIntTmp, 0 , p-1));
end;

function BoolYN(BoolVal: Boolean): String;
begin
  if BoolVal then
    Result := CHECKEDVALUE
  else
    Result := UNCHECKEDVALUE;
end;

function YNBool(BoolVal: String): Boolean;
begin
  if BoolVal = CHECKEDVALUE then
    Result := True
  else
    Result := False;
end;
function TORASystemDM.UpdatePimsBase(DatabaseName: String): Boolean;
begin
  Result := True;
end;

procedure TORASystemDM.SynchronizeWorkStation;
begin
  // RV078.1. Use try-except to trap errors
  try
    odsWorkStation.Active := True;
    // RV078.1. Add all fields during 'insertrecord'.
    if not odsWorkStation.Locate('COMPUTER_NAME' ,CurrentComputerName, []) then
    begin
      // PIM-12 Use TOracleQuery for inserting workstation-record
      with oqWorkstationAdd do
      begin
        ClearVariables;
        SetVariable('COMPUTER_NAME', CurrentComputerName);
        Execute;
      end;
    end;
//      odsWorkStation.InsertRecord([CurrentComputerName, 0,
//        UNCHECKEDVALUE, UNCHECKEDVALUE]);
  except
    // RV078.1.
    on E:EOracleError do
    begin
      // Ignore error
    end;
    on E:Exception do
    begin
      // Ignore error
    end;
  end;
end;

procedure TORASystemDM.AssignSettings;
begin
  odsPimsSettings.FieldByName('SAVE_DETAIL_PRD_INFO').AsInteger := DetailedProd;
  odsPimsSettings.FieldByName('SAVE_DAY_PRD_INFO').AsInteger :=
    ProcessDayQantity;
  // Car 20.2.2004  Trims leading and trailing spaces
  odsPimsSettings.FieldByName('UNIT_WEIGHT').AsString := Trim(UnitsWeigh);
  odsPimsSettings.FieldByName('UNIT_PIECES').AsString := Trim(UnitsPieces);
  odsPimsSettings.FieldByName('WEEK_START_ON').AsInteger := WeekStartsOn;
  odsPimsSettings.FieldByName('SETTING_PASSWORD').AsString := SettingsPassword;
  odsPimsSettings.FieldByName('VERSION_NUMBER').AsInteger := VersionNumber;
  odsPimsSettings.FieldByName('CHANGE_SCAN_YN').AsString := ChangeScan;
  odsPimsSettings.FieldByName('READ_PLANNING_ONCE_YN').AsString :=
    ReadPlanningOnceYN;
  odsPimsSettings.FieldByName('UPDATE_DELETE_EMP_YN').AsString :=
    UpdateEmployeeYN;
  odsPimsSettings.FieldByName('UPDATE_DELETE_PLANT_YN').AsString := UpdatePlantYN;
  odsPimsSettings.FieldByName('DELETE_WK_YN').AsString := DeleteWKYN;
  //car 4-12-2003
  odsPimsSettings.FieldByName('FIRST_WEEK_OF_YEAR').AsInteger := FirstWeekOfYear;
  odsPimsSettings.FieldByName('EMPLOYEES_ACROSS_PLANTS_YN').AsString :=
    EmployeesAcrossPlantsYN;
  odsPimsSettings.FieldByName('EFF_BO_QUANT_YN').AsString := EffBoQuantYN;
  odsPimsSettings.FieldByName('PS_AUTO_CLOSE_SECS').AsInteger :=
    PSAutoCloseSecs;
  odsPimsSettings.FieldByName('DATACOL_STORE_QTY_5MIN_PERIOD').AsString :=
    DatacolStoreQty5MinPeriod; // TD-22171
  if LogToDB then // 20014826
    odsPimsSettings.FieldByName('ERROR_LOG_TO_DB_YN').AsString := 'Y'
  else
    odsPimsSettings.FieldByName('ERROR_LOG_TO_DB_YN').AsString := 'N';
  if TimerecordingInSecs then // 20015346
    odsPimsSettings.FieldByName('TIMEREC_IN_SECONDS_YN').AsString := 'Y'
  else
    odsPimsSettings.FieldByName('TIMEREC_IN_SECONDS_YN').AsString := 'N';
  if IgnoreBreaks then // 20015346
    odsPimsSettings.FieldByName('IGNORE_BREAKS_YN').AsString := 'Y'
  else
    odsPimsSettings.FieldByName('IGNORE_BREAKS_YN').AsString := 'N';
  if PSShowEmpEff then // 20014450.50
    odsPimsSettings.FieldByName('PSSHOWEMPEFF_YN').AsString := 'Y'
  else
    odsPimsSettings.FieldByName('PSSHOWEMPEFF_YN').AsString := 'N';
  if PSShowEmpInfo then // 20014450.50
    odsPimsSettings.FieldByName('PSSHOWEMPINFO_YN').AsString := 'Y'
  else
    odsPimsSettings.FieldByName('PSSHOWEMPINFO_YN').AsString := 'N';
  if AddJobComment then // PIM-50
    odsPimsSettings.FieldByName('JOBCOMMENT_YN').AsString := 'Y'
  else
    odsPimsSettings.FieldByName('JOBCOMMENT_YN').AsString := 'N';
  if UsePackageHourCalc then // PIM-87
    odsPimsSettings.FieldByName('PACKAGE_HOUR_CALC_YN').AsString := 'Y'
  else
    odsPimsSettings.FieldByName('PACKAGE_HOUR_CALC_YN').AsString := 'N';
  if BreakBtnVisible then // PIM-12
    odsPimsSettings.FieldByName('BREAK_BTN_YN').AsString := 'Y'
  else
    odsPimsSettings.FieldByName('BREAK_BTN_YN').AsString := 'N';
  if LunchBtnVisible then // PIM-12
    odsPimsSettings.FieldByName('LUNCH_BTN_YN').AsString := 'Y'
  else
    odsPimsSettings.FieldByName('LUNCH_BTN_YN').AsString := 'N';
  if TREndOfDayBtnVisible then // PIM-12
    odsPimsSettings.FieldByName('TR_EOD_BTN_YN').AsString := 'Y'
  else
    odsPimsSettings.FieldByName('TR_EOD_BTN_YN').AsString := 'N';
  if PSEndOfDayBtnVisible then // PIM-12
    odsPimsSettings.FieldByName('PS_EOD_BTN_YN').AsString := 'Y'
  else
    odsPimsSettings.FieldByName('PS_EOD_BTN_YN').AsString := 'N';
  if MANREGEndOfDayBtnVisible then // PIM-12
    odsPimsSettings.FieldByName('MANREG_EOD_BTN_YN').AsString := 'Y'
  else
    odsPimsSettings.FieldByName('MANREG_EOD_BTN_YN').AsString := 'N';
  if ExternQtyFromPQ then // PIM-90
    odsPimsSettings.FieldByName('EXTERN_QTY_FROM_PQ_YN').AsString := 'Y'
  else
    odsPimsSettings.FieldByName('EXTERN_QTY_FROM_PQ_YN').AsString := 'N';
  if LunchBtnScanOut then // PIM-139
    odsPimsSettings.FieldByName('LUNCH_BTN_SCANOUT_YN').AsString := 'Y'
  else
    odsPimsSettings.FieldByName('LUNCH_BTN_SCANOUT_YN').AsString := 'N';
  if ProdHrsDoNotSubtractBreaks then // PIM-319
    odsPimsSettings.FieldByName('PRODHRS_NOSUBTRACT_BREAKS').AsString := 'Y'
  else
    odsPimsSettings.FieldByName('PRODHRS_NOSUBTRACT_BREAKS').AsString := 'N';
  odsPimsSettings.FieldByName('MAX_TIMEBLOCKS').AsInteger := MaxTimeblocks; // GLOB3-60 Not the default!
  if TimeRecCRDoNotScanOut then // GLOB3-202
    odsPimsSettings.FieldByName('TIMERECCR_DONOTSCANOUT_YN').AsString := CHECKEDVALUE
  else
    odsPimsSettings.FieldByName('TIMERECCR_DONOTSCANOUT_YN').AsString := UNCHECKEDVALUE;
  if OvertimePerDay then // GLOB3-223
    odsPimsSettings.FieldByName('OVERTIME_PERDAY_YN').AsString := CHECKEDVALUE
  else
    odsPimsSettings.FieldByName('OVERTIME_PERDAY_YN').AsString := UNCHECKEDVALUE;
  odsPimsSettings.FieldByName('OCCUP_HANDLING').AsInteger := Occup_Handling; // GLOB3-330
end;

procedure TORASystemDM.SetSettings;
begin
  try
    if not odsPimsSettings.Active then
      odsPimsSettings.Active := True;
    odsPimsSettings.Edit;
    AssignSettings;
    odsPimsSettings.Post;
    if not odsWorkStation.Active then
      odsWorkStation.Active := True;
    if odsWorkStation.Locate('COMPUTER_NAME' ,CurrentComputerName, []) then
    begin
      odsWorkStation.Edit;
      odsWorkStation.FieldByName('TIMERECORDING_YN').AsString :=
        TimeRecordingStation;
      // TD-23247
      try
        odsWorkStation.FieldByName('CARDREADER_DELAY').AsInteger :=
          CardReaderDelay;
      except
      end;
      odsWorkStation.Post;
    end;
  except
    // ignore error
  end;
end;

procedure TORASystemDM.GetSettings;
begin
  DatabaseTimezone := ''; // 20016449
  try
    if not odsPimsSettings.Active then
      odsPimsSettings.Active := True;
    if odsPimsSettings.RecordCount <= 0 then
    begin
      ProcessDayQantity := DAYPRODINFODEFAULT;
      DetailedProd := DETAILEDPRODDEFAULT;
      UnitsWeigh := UNITWEIGHTDEFAULT;
      UnitsPieces := UNITPIECESDEFAULT;
      WeekStartsOn := WEEKDAYSDEFAULT;
      //car 4-12-2003
      FirstWeekOfYear := FIRSTWEEKOFYEARDEFAULT;
      SettingsPassword := PASSWORDDEFAULT;
      VersionNumber := VERSIONNUMBERDEFAULT;
      ChangeScan := CHANGESCANYNDEFAULT;
      ReadPlanningOnceYN := CHANGESCANYNDEFAULT;
      UpdateEmployeeYN := UNCHECKEDVALUE;
      DeleteWKYN := UNCHECKEDVALUE;
      EmployeesAcrossPlantsYN := UNCHECKEDVALUE;
      // PIM-12 Set to fixed value
      EffBoQuantYN := UNCHECKEDVALUE;
      PSAutoCloseSecs := 10;
      DatacolStoreQty5MinPeriod := 'N'; // TD-22171
      LogToDB := False; // 20014826
      TimerecordingInSecs := True; // 20015346 // PIM-12
      IgnoreBreaks := True; // 20015346 // PIM-12
      PSShowEmpEff := True; // 20014450.50
      PSShowEmpInfo := True; // 20014450.50
      AddJobComment := False; // PIM-50
      UsePackageHourCalc := False; // PIM-87
      BreakBtnVisible := False; // PIM-12
      LunchBtnVisible := False; // PIM-12
      TREndOfDayBtnVisible := True; // PIM-12
      PSEndOfDayBtnVisible := True; // PIM-12
      MANREGEndOfDayBtnVisible := True; // PIM-12
      LunchBtnScanOut := False; // PIM-139
      ExternQtyFromPQ := True; // PIM-90
      FHybridMode := hmNone; // PIM-180
      ProdHrsDoNotSubtractBreaks := False; // PIM-319
      MaxTimeblocks := DefaultMaxTimeblocks; // GLOB3-60
      TimeRecCRDoNotScanOut := False; // GLOB3-202
      OvertimePerDay := False; // GLOB3-223
      Occup_Handling := 2; // GLOB3-330
      odsPimsSettings.Insert;
      AssignSettings;
      odsPimsSettings.Post;
    end
    else
    begin
      odsPimsSettings.First;
      DetailedProd :=
        odsPimsSettings.FieldByName('SAVE_DETAIL_PRD_INFO').AsInteger;
      ProcessDayQantity :=
        odsPimsSettings.FieldByName('SAVE_DAY_PRD_INFO').AsInteger;
      UnitsWeigh :=
        odsPimsSettings.FieldByName('UNIT_WEIGHT').AsString;
      UnitsPieces :=
        odsPimsSettings.FieldByName('UNIT_PIECES').AsString;
      WeekStartsOn :=
        odsPimsSettings.FieldByName('WEEK_START_ON').AsInteger;
      SettingsPassword :=
        odsPimsSettings.FieldByName('SETTING_PASSWORD').AsString;
      VersionNumber :=
      	odsPimsSettings.FieldByName('VERSION_NUMBER').AsInteger;
      ChangeScan :=
      	odsPimsSettings.FieldByName('CHANGE_SCAN_YN').AsString;
      ReadPlanningOnceYN :=
      	odsPimsSettings.FieldByName('READ_PLANNING_ONCE_YN').AsString;
      UpdateEmployeeYN :=
      	odsPimsSettings.FieldByName('UPDATE_DELETE_EMP_YN').AsString;
      UpdatePlantYN :=
      	odsPimsSettings.FieldByName('UPDATE_DELETE_PLANT_YN').AsString;
      DeleteWKYN :=
        odsPimsSettings.FieldByName('DELETE_WK_YN').AsString;
      FirstWeekOfYear :=
        odsPimsSettings.FieldByName('FIRST_WEEK_OF_YEAR').AsInteger;
      EmployeesAcrossPlantsYN :=
        odsPimsSettings.FieldByName('EMPLOYEES_ACROSS_PLANTS_YN').AsString;
      IsRFL :=
        (odsPimsSettings.FieldByName('CODE').AsString = 'RFL');
      // PIM-12 Set to fixed value
      EffBoQuantYN := UNCHECKEDVALUE;
{        odsPimsSettings.FieldByName('EFF_BO_QUANT_YN').AsString; }
      PSAutoCloseSecs :=
        odsPimsSettings.FieldByName('PS_AUTO_CLOSE_SECS').AsInteger;
      DatacolStoreQty5MinPeriod :=
        odsPimsSettings.FieldByName('DATACOL_STORE_QTY_5MIN_PERIOD').AsString; // TD-22171
      UseShiftDateSystem :=
        (odsPimsSettings.FieldByName('SHIFTDATESYSTEM_YN').AsString = 'Y'); // 20014327
      EnableMachineTimeRec :=
        (odsPimsSettings.FieldByName('ENABLE_MACHINETIMEREC_YN').AsString = 'Y'); // 20015178
      // 20014550 This must be a pims-setting? THIS IS NOT USED YET!!! // TD-26942
      RegisterMachineHours := False; // Testing: Here we set this on or off.
      LogToDB :=
        (odsPimsSettings.FieldByName('ERROR_LOG_TO_DB_YN').AsString = 'Y'); // 20014826
      TimerecordingInSecs := True; // PIM-12
//        (odsPimsSettings.FieldByName('TIMEREC_IN_SECONDS_YN').AsString = 'Y'); // 20015346
      IgnoreBreaks := True; // PIM-12
//        (odsPimsSettings.FieldByName('IGNORE_BREAKS_YN').AsString = 'Y'); // 20015346
      try
        DatabaseTimezone :=
          odsPimsSettings.FieldByName('TIMEZONE').AsString; // 20016449
      except
        DatabaseTimezone := '';
      end;
      PSShowEmpInfo :=
        (odsPimsSettings.FieldByName('PSSHOWEMPINFO_YN').AsString = 'Y'); // 20014450.50
      if PSShowEmpInfo then
        PSShowEmpEff :=
          (odsPimsSettings.FieldByName('PSSHOWEMPEFF_YN').AsString = 'Y') // 20014450.50
      else
        PSShowEmpEff := False;
      AddJobComment :=
        (odsPimsSettings.FieldByName('JOBCOMMENT_YN').AsString = 'Y'); // PIM-50
      UsePackageHourCalc :=
        (odsPimsSettings.FieldByName('PACKAGE_HOUR_CALC_YN').AsString = 'Y'); // PIM-87
      BreakBtnVisible := odsPimsSettings.FieldByName('BREAK_BTN_YN').AsString = 'Y'; // PIM-12
      LunchBtnVisible := odsPimsSettings.FieldByName('LUNCH_BTN_YN').AsString = 'Y'; // PIM-12
      TREndOfDayBtnVisible := odsPimsSettings.FieldByName('TR_EOD_BTN_YN').AsString = 'Y'; // PIM-12
      PSEndOfDayBtnVisible := odsPimsSettings.FieldByName('PS_EOD_BTN_YN').AsString = 'Y'; // PIM-12
      MANREGEndOfDayBtnVisible := odsPimsSettings.FieldByName('MANREG_EOD_BTN_YN').AsString = 'Y'; // PIM-12
      ExternQtyFromPQ := odsPimsSettings.FieldByName('EXTERN_QTY_FROM_PQ_YN').AsString = 'Y'; // PIM-90
      LunchBtnScanOut := odsPimsSettings.FieldByName('LUNCH_BTN_SCANOUT_YN').AsString = 'Y'; // PIM-139
      FHybridMode := hmNone; // PIM-180
      try // PIM-180
        if odsPimsSettings.FieldByName('HYBRID').AsString <> '' then
          FHybridMode := THybridMode(odsPimsSettings.FieldByName('HYBRID').AsInteger);
      except
        FHybridMode := hmNone;
      end;
      ProdHrsDoNotSubtractBreaks := odsPimsSettings.FieldByName('PRODHRS_NOSUBTRACT_BREAKS').AsString = 'Y'; // PIM-319
      try
        MaxTimeblocks := odsPimsSettings.FieldByName('MAX_TIMEBLOCKS').AsInteger;
        if not ((MaxTimeblocks = DefaultMaxTimeblocks) or (MaxTimeblocks = DefaultMaxTimeblocks10)) then
          MaxTimeblocks := DefaultMaxTimeblocks;
      except
        MaxTimeblocks := DefaultMaxTimeblocks;
      end;
      TimeRecCRDoNotScanOut := odsPimsSettings.FieldByName('TIMERECCR_DONOTSCANOUT_YN').AsString = CHECKEDVALUE; // GLOB3-202
      try
        OvertimePerDay := odsPimsSettings.FieldByName('OVERTIME_PERDAY_YN').AsString = CHECKEDVALUE; // GLOB3-223
      except
        OvertimePerDay := False;
      end;
      try
        Occup_Handling := odsPimsSettings.FieldByName('OCCUP_HANDLING').AsInteger; // GLOB3-330
      except
        Occup_Handling := 2;
      end;
    end;
  except
    on E:EOracleError do
    begin
      WErrorLog(E.Message);
      if not Self.OnlyLogMessages then
        ShowMessage(E.Message);
    end;
    on E:Exception do
    begin
      WErrorLog(E.Message);
      if not Self.OnlyLogMessages then
        ShowMessage(E.Message);
    end;
  end;
  TimeRecordingStation := TIMERECORDINGSTATIONDEFAULT;
  if not odsWorkStation.Active then
    odsWorkStation.Active := True;

  if odsWorkStation.Locate('COMPUTER_NAME' ,CurrentComputerName, []) then
  begin
    TimeRecordingStation :=
      odsWorkStation.FieldByName('TIMERECORDING_YN').AsString;
    // TD-23247
    try
      CardReaderDelay :=
        odsWorkStation.FieldByName('CARDREADER_DELAY').AsInteger;
    except
      CardReaderDelay := 0;
    end;
    // 20015302
    try
      EnableManRegsMultipleJobs :=
        odsWorkStation.FieldByName('MANREGS_MULTJOBS_YN').AsString = 'Y';
    except
      EnableManRegsMultipleJobs := False;
    end;
  end;
{$IFDEF DEBUG}
WErrorLog('CurrentComputerName=[' + CurrentComputerName + ']' +
  ' TimeRecordingStation=[' + TimeRecordingStation + ']') ;
{$ENDIF}
  PlantTimezonehrsdiffWriteToDB; // 20016449
  DetermineWorkstationTimezonehrsdiff; // 20016449
end;

function TORASystemDM.GetFillProdHourPerHourTypeYN: String;
begin
  if not odsPimsSettings.Active then
    odsPimsSettings.Open;
  Result := odsPimsSettings.FieldByName('FILLPRODUCTIONHOURS_YN').AsString;
end;

function TORASystemDM.GetCurrentClientEnvName: String;
begin
  Result := GetEnvironmentVariable('CLIENTNAME');
end;

function TORASystemDM.GetCurrentComputerEnvName: String;
begin
  Result := GetEnvironmentVariable('COMPUTERNAME');
end;

// MR:22-01-2008 RV002:
// Get ComputerName by:
// - First look for environment variable CLIENTNAME.
//   If empty then look for environment variable COMPUTERNAME.
//   If this is also empty then look for COMPUTERNAME using a Windows-function.
// This change was needed when Pims is used with Terminal Services, to
// prevent the COMPUTERNAME is the Server-name instead of the Client-name.
// MR:10-06-2008 RV007:
// - For CLIENTNAME: If contents is 'Console' then see this also as ''.
function TORASystemDM.GetCurrentComputerName: String;
const
  NEW_MAX_COMPUTERNAME_LENGTH=32; // TD-25352
var
  LengthComputerName: DWORD;
  // TD-25352 Extend array-length from 16 to 32
  pComputerName     : array[1..NEW_MAX_COMPUTERNAME_LENGTH] of Char; { do not use a PChar for Win95 }
begin
  if FCurrentComputerName <> '' then // TD-25352
  begin
    Result := FCurrentComputerName;
    Exit;
  end;
  Result := CurrentClientEnvName;
  // RV029. If called by ADC2-application, do not get CLIENTNAME!
  // RV029. Also, if called by Prodscreen-application, do not get CLIENTNAME!
  if ADC2 or ProdScreen or (Result = '') or (UpperCase(Result) = 'CONSOLE') then
  begin
    Result := CurrentComputerEnvName;
    if Result = '' then
    begin
      LengthComputerName := NEW_MAX_COMPUTERNAME_LENGTH; // MAX_COMPUTERNAME_LENGTH + 1; // TD-25352
      if GetComputerName(@pComputerName, LengthComputerName) then
      begin
        Result := String(pComputerName);
        // result GetComputerName is without ZeroTerminator
        SetLength(Result, LengthComputerName);
      end
      else
        Result := 'DEMOPC';
    end;
  end;
  FCurrentComputerName := Result;
end;

// End PIMS Settings Tools.
function DisplayMessage(const Msg: String; DlgType: TMsgDlgType;
  Buttons: TMsgDlgButtons): Integer;
begin
  Result := 0;
  if ORASystemDM.OnlyLogMessages then
    WErrorLog(Msg)
  else
    Result := MessageDlg(Msg, DlgType, Buttons, 0);
end;

function DisplayMessageFmt(const Msg: String; const Args: array of const;
  DlgType: TMsgDlgType; Buttons: TMsgDlgButtons): Integer;
begin
  Result := 0;
  if ORASystemDM.OnlyLogMessages then
    WErrorLog(Msg)
  else
    Result := DisplayMessage(Format(Msg, Args), DlgType, Buttons);
end;

function DoubleQuote(Code: String): String;
begin
  Result := StringReplace(Code,'''','''''',[rfReplaceAll]);
end;

function IsErrorFromException(ErrorMessage: String;
  var ErrorCode: Integer): Boolean;
var
  SubStr: String;
  PosStr: Integer;
begin
  ErrorCode := 0;
  PosStr := Pos('~',ErrorMessage);
  if (PosStr > 0) then
  begin
    SubStr := Copy(ErrorMessage, PosStr + 1, 2);
    PosStr := Pos('~', SubStr);
    if (PosStr > 0) then
      ErrorCode := StrToInt(Copy(SubStr, PosStr - 1, 1));
  end;
  Result := (ErrorCode > 0);
end;

function GetErrorMessage(const PimsError: EDataBaseError;
  const PimsActivity: Integer): String;
var
  iDBIErrorCode, EXErrorCode: Integer;
  iDBIErrorMessage: String;
begin
  Result := PimsError.Message;
  iDBIErrorCode := 0;
{  if (PimsError is EDBEngineError) then
  begin
    iDBIErrorCode := (PimsError as EDBEngineError).Errors[0].Errorcode;
    iDBIErrorMessage := PimsError.Message;
  end; }
  if IsErrorFromException(iDBIErrorMessage, EXErrorCode) then
    iDBIErrorCode := EXErrorCode;
  case iDBIErrorCode of
    PIMS_DBIERR_KEYVIOL            : Result := SPimsKeyViolation;
    PIMS_DBIERR_REQDERR            : Result := SPimsFieldRequired;
    PIMS_DBIERR_FORIEGNKEYERR      :
      if PimsActivity = PIMS_POSTING then
        Result := SPimsForeignKeyPost
      else
        Result := SPimsForeignKeyDel;
    PIMS_DBIERR_DETAILRECORDSEXIST : Result := SPimsDetailsExist;
    PIMS_DBIERR_LOCKED: Result := SPimsRecordLocked;
    PIMS_EX_DELETE_LAST_RECORD : Result := SPimsTBDeleteLastRecord;
    PIMS_EX_MAX_RECORD : Result := SPimsTBInsertRecord;
    PIMS_EX_STARTENDDATE : Result := SPimsStartEndDate;
    PIMS_EX_STARTENDTIME : Result := SPimsStartEndTime;
    PIMS_EX_DATEINPREVIOUSPERIOD : Result := SPimsDateinPreviousPeriod;
    //CAR 15-1-2004 - Add an exception message
    PIMS_EX_CHECKEMPLCONTRDATEPREVIOUS :
      Result := SPimsEmplContractInDatePrevious;
    PIMS_EX_UNIQUEFIELD : Result := SPimsKeyViolation;
    // CAR 27.02.2003 - ERROR IN ILLNESSMESG
    PIMS_EX_CHECKILLMSGCLOSE: Result := SPimsEXCheckIllMsgClose;
    PIMS_EX_CHECKILLMSGSTARTDATE: Result := SPimsEXCheckIllMsgStartDate;
    PIMS_DBIERR_GENERAL_SQL_ERROR: Result := SPimsGeneralSQLError;
  else
    Result := 'Unknown Error: ' + IntToStr(iDBIErrorCode);
  end; { case }
end;

function DisplayErrorMessage(const PimsError: EDataBaseError;
  const PimsActivity: Integer):Integer;
begin
  Result := 0;
  if ORASystemDM.OnlyLogMessages then
    WErrorLog(GetErrorMessage(PimsError, PimsActivity))
  else
  begin
    MessageBeep(MB_ICONHAND);
    Result := DisplayMessage(GetErrorMessage(PimsError, PimsActivity), mtWarning,
      [mbOk]);
  end;
end;

function DisplayVersionInfo: String;
var
  V1,       // Major Version
  V2,       // Minor Version
  V3,       // Release
  V4: Word; // Build Number
begin
  GetBuildInfo(V1, V2, V3, V4);
  Result := IntToStr(V1) + '.'  + IntToStr(V2) + '.'  + IntToStr(V3) + '.'
    + IntToStr(V4);
end;

procedure GetBuildInfo(var V1, V2, V3, V4: Word);
var
  VerInfoSize, VerValueSize, Dummy : DWORD;
  VerInfo : Pointer;
  VerValue : PVSFixedFileInfo;
begin
  VerInfoSize := GetFileVersionInfoSize(PChar(ParamStr(0)), Dummy);
  GetMem(VerInfo, VerInfoSize);
  GetFileVersionInfo(PChar(ParamStr(0)), 0, VerInfoSize, VerInfo);
  VerQueryValue(VerInfo, '\', Pointer(VerValue), VerValueSize);
  with VerValue^ do
  begin
    V1 := dwFileVersionMS shr 16;
    V2 := dwFileVersionMS and $FFFF;
    V3 := dwFileVersionLS shr 16;
    V4 := dwFileVersionLS and $FFFF;
  end;
  FreeMem(VerInfo, VerInfoSize);
end;

procedure CheckValidNumber(var Key: Char);
const
  ValidKeys  : set of Char = [#08, #13] + [#48..#57];
begin
  if not(Key in ValidKeys) then
     Key := #0;
end;

procedure ActiveTables(FDataModule: TDataModule; ActiveYN: Boolean);
var
  DataSetCounter: Integer;
//  DS: TDataSource;
  Temp: TComponent;
  TempSQL: String;
begin
  // to be replaced or removed
  TempSQL := '';
  if FDataModule <> nil then
  begin
    try
      for DataSetCounter := 0 to FDataModule.ComponentCount - 1 do
      begin
        Temp := FDataModule.Components[DataSetCounter];
        if (Temp is TOracleDataset) then
        begin
          if ((Temp as TOracleDataset).SQL <> nil) then
          begin
            TempSQL := (Temp as TOracleDataset).SQL.Text;
            if TempSQL <> '' then
              (Temp as TOracleDataset).Active := ActiveYN;
          end;
        end;
      end;
    except
      // RV078.1.
      on E:EOracleError do
      begin
        WErrorLog(E.Message + ' SQL: ' + TempSQL);
      end;
      on E:Exception do
      begin
        WErrorLog(E.Message);
      end;
    end;
  end;
end;

procedure TORASystemDM.DefaultNotEmptyValidate(Sender: TField);
begin
  if Sender.AsString = '' then
  begin
    if ORASystemDM.OnlyLogMessages then
      WErrorLog(SPimsFieldRequiredFmt)
    else
    begin
      DisplayMessageFmt(SPimsFieldRequiredFmt,[(Sender as TField).FieldName],
        mtWarning, [mbOK]);
      SysUtils.Abort;
    end;
  end;
end;

function TORASystemDM.GetCurrentProgramUser: String;
var
  LengthUserName: DWORD;
  pUserName     : array[1..21] of Char; { do not use a PChar for Win95 }
begin
  LengthUserName := 21; // maxusername = 20 chars + ZeroTerminator
  if GetUserName(@pUserName, LengthUserName) then
  begin
    Result := String(pUserName);
    // strip zeroterminator
    SetLength(Result, (LengthUserName - 1));
  end
  else
    Result := 'DEMOUSER';
end;

procedure TORASystemDM.DefaultBeforeDelete(DataSet: TDataSet);
begin
  if ORASystemDM.OnlyLogMessages then
    WErrorLog(SPimsConfirmDelete)
  else
  begin
    if DisplayMessage(SPimsConfirmDelete, mtConfirmation, [mbYes, mbNo])
      <> mrYes then
    SysUtils.Abort;
  end;
end;

procedure TORASystemDM.DefaultBeforePost(DataSet: TDataSet);
var
  FieldCounter: Integer;
  Str: String;
begin
  for FieldCounter := 0 to (DataSet.FieldCount - 1) do
  begin
    if (DataSet.Fields[FieldCounter].Tag = 1) then
      DefaultNotEmptyValidate(DataSet.Fields[FieldCounter]);
    //Car 19.2.2004 - delete spaces before and after a String value
    if (DataSet.Fields[FieldCounter] is TStringField) then
        if DataSet.Fields[FieldCounter].Value <> '' then
        begin
          // MR:06-04-2004 Only do a trim if it's really needed...
          // Otherwise an 'OnChange' can be triggered without need.
          Str := DataSet.Fields[FieldCounter].AsString;
          if Length(Str) > 0 then
            if (Str[1] = ' ') or (Str[Length(Str)] = ' ') then
              DataSet.Fields[FieldCounter].AsString :=
                Trim(DataSet.Fields[FieldCounter].AsString);
        end;
  end;
  with DataSet do
  begin
    FieldByName('MUTATIONDATE').AsDateTime := Now;
    FieldByName('MUTATOR').AsString        := CurrentProgramUser;
  end;
end;

procedure TORASystemDM.DefaultDeleteError(DataSet: TDataSet;
  E: EDatabaseError; var Action: TDataAction);
begin
  DisplayErrorMessage(E, PIMS_DELETING);
  Action := daAbort;
end;

procedure TORASystemDM.DefaultEditError(DataSet: TDataSet;
  E: EDatabaseError; var Action: TDataAction);
begin
  DisplayErrorMessage(E, PIMS_EDITING);
  Action := daAbort;
end;

procedure TORASystemDM.DefaultNewRecord(DataSet: TDataSet);
begin
  with DataSet do
  begin
    FieldByName('CREATIONDATE').AsDateTime := Now;
    FieldByName('MUTATIONDATE').AsDateTime := Now;
    FieldByName('MUTATOR').AsString        := CurrentProgramUser;
  end;
end;

procedure TORASystemDM.DefaultPostError(DataSet: TDataSet;
  E: EDatabaseError; var Action: TDataAction);
begin
  DisplayErrorMessage(E, PIMS_POSTING);
  Action := daAbort;
end;

procedure TORASystemDM.DataModuleCreate(Sender: TObject);
// MR:27-07-2006 Use a PIMSORA.INI-file to get the database-name,
//               as a default this is ABS1.
const
  PimINIFilename='PIMSORA.INI';
var
  MyDatabase: String;
  MyDatabaseUsername: String;
  MyDatabasePassword: String;
  AppRootPath: String;
  HaltNow: Boolean;
  function PathCheck(Path: String): String;
  begin
    if Path <> '' then
      if Path[length(Path)] <> '\' then
        Path := Path + '\';
    Result := Path;
  end;
  function ReadINIFile: String;
  var
    Ini: TIniFile;
    function SetEnvVarValue(const VarName,
      VarValue: String): Integer;
    begin
      // Simply call API function
      if Windows.SetEnvironmentVariable(PChar(VarName),
        PChar(VarValue)) then
        Result := 0
      else
        Result := GetLastError;
    end;
    procedure ReadMakeOracleSettings(ASection: String); // PIM-337
    var
       OracleHomeEnv: String;
       PathEnv: String;
       TNSAdminEnv: String;
    begin
      // Also read environment variables here:
      {
        Set ORACLE_HOME=C:\PIMSROOT\OraClient32b
        Set PATH=C:\PIMSROOT\OraClient32b
        Set TNS_ADMIN=C:\PIMSROOT\OraClient32b\network\admin
      }
      OracleHomeEnv := '';
      PathEnv := '';
      TNSAdminEnv := '';
      if Ini.ValueExists(ASection, 'ORACLE_HOME') then
        OracleHomeEnv := Ini.ReadString(ASection, 'ORACLE_HOME', OracleHomeEnv);
      if Ini.ValueExists(ASection, 'PATH') then
        PathEnv := Ini.ReadString(ASection, 'PATH', PathEnv);
      if Ini.ValueExists(ASection, 'TNS_ADMIN') then
        TNSAdminenv := Ini.ReadString(ASection, 'TNS_ADMIN', TNSAdminEnv);
      if OracleHomeEnv <> '' then
      begin
        SetEnvVarValue('ORACLE_HOME', OracleHomeEnv);
      end;
      if PathEnv <> '' then
      begin
        SetEnvVarValue('PATH', PathEnv + ';' + GetEnvironmentVariable('PATH'));
      end;
      if TNSAdminEnv <> '' then
      begin
        SetEnvVarValue('TNS_ADMIN', TNSAdminEnv);
      end;
    end; // ReadMakeOracleSettings
  begin
    Result := '';
    Ini := TIniFile.Create(PathCheck(AppRootPath) + PimINIFilename);
    try
      Result := UpperCase(Ini.ReadString('PIMSORA', 'Database', Result));
      MyDatabaseUsername :=
        UpperCase(
          Ini.ReadString('PIMSORA', 'DatabaseUsername', MyDatabaseUsername));
      MyDatabasePassword :=
        UpperCase(
          Ini.ReadString('PIMSORA', 'DatabasePassword', MyDatabasePassword));
      // PIM-254 -> Only for certain application!
{$IFDEF TIMERECORDINGCR}
  ReadMakeOracleSettings('PIMSORA'); // PIM-337
{$ENDIF}
{$IFDEF TIMERECORDING}
  ReadMakeOracleSettings('TIMERECORDING'); // PIM-337
{$ENDIF}
{$IFDEF PRODUCTIONSCREEN}
  ReadMakeOracleSettings('PRODUCTIONSCREEN'); // GLOB3-337
{$ENDIF}
      // Set always to FALSE, meaning reading is done in Shared Mode.
      CardReaderConnectExclusive := False;
    finally
      Ini.Free;
    end;
  end;
  procedure WriteINIFile;
  var
    Ini: TIniFile;
  begin
    Ini := TIniFile.Create(PathCheck(AppRootPath) + PimINIFilename);
    try
      Ini.WriteString('PIMSORA', 'Database', MyDatabase);
      Ini.WriteString('PIMSORA', 'DatabaseUsername', MyDatabaseUsername);
      Ini.WriteString('PIMSORA', 'DatabasePassword', MyDatabasePassword);
    finally
      Ini.Free;
    end;
  end;
begin
  AppName := 'Pims'; // 20014826 Name of current application
  LogErrors := True; // PIM-337
  OnlyLogMessages := False;
  WSTimezonehrsdiff := 0; // 20016449
  FCurrentComputerName := ''; // TD-25352
  EnableManRegsMultipleJobs := False; // 20015302
  CardReaderConnectExclusive := False;
  DatacolStoreQty5MinPeriod := 'N'; // TD-22171
  ADC2 := False;
  IsRFL := False;
  ProdScreen := False;
  AppRootPath := GetCurrentDir;
  MyDatabase := 'ABS1';
  MyDatabaseUsername := 'PIMS';
  MyDatabasePassword := 'PIMS';
  ReadINIFile;
  HaltNow := False;
  // Logon as PIMS / PIMS on ABS1 (default)
  try
    MyDatabase := ReadINIFile;
    if (MyDatabase = '') then
    begin
      if OracleLogon.Execute then
      begin
        OracleSession.Connected := True;
        if OracleSession.Connected then
        begin
          MyDatabase := OracleSession.LogonDatabase;
          MyDatabaseUsername := OracleSession.LogonUsername;
          MyDatabasePassword := OracleSession.LogonPassword;
          WriteINIFile;
        end
        else
          HaltNow := True;
      end
      else
        HaltNow := True;
    end
    else
    begin
      OracleSession.LogonDatabase := MyDatabase;
      OracleSession.LogonUsername := MyDatabaseUsername;
      OracleSession.LogonPassword := MyDatabasePassword;
    end;

    if not HaltNow then
      if not OracleSession.Connected then
      begin
        OracleSession.Connected := True;
        if not OracleSession.Connected then
          HaltNow := True;
      end;
  except
    on E:EOracleError do
    begin
      DisplayMessage(E.Message + #13#13 + 'Application will be terminated.',
        mtError, [mbOK]);
      HaltNow := True;
    end;
    on E:Exception do
    begin
      DisplayMessage(E.Message + #13#13 + 'Application will be terminated.',
        mtError, [mbOK]);
      HaltNow := True;
    end;
  end;

  if HaltNow then
  begin
    Application.Terminate;
    Halt;
  end;

  // Create a variable for 'TDateFmt'-class
  ADateFmt := TDateFmt.Create;
  // MRA: 04-MAR-2009 RV023. Must be added here!
  GetExportPayroll; (* RV023 *)
  // RV076.1.
  IsLTB := False;
  // RV082.1.
  IsCVL := False;
  ProdHrsDoNotSubtractBreaks := False; // PIM-319
  // GLOB3-85
  IsNTS := False;
  // TD-22082
  PlantTeamFilterOn := False;
  // 20014327
  UseShiftDateSystem := False;
  // 20015178 // PIM-155 Why was this disabled here?
//  EnableMachineTimeRec := False;
  // 20014450
  RegisterMachineHours := False;
  MaxTimeblocks := DefaultMaxTimeblocks; // GLOB3-60
  TimeRecCRDoNotScanOut := False; // GLOB3-202
  OvertimePerDay := False; // GLOB3-223
  Occup_Handling := 2; // GLOB3-330

  // PIM-194
  RedBoundary := RED_BOUNDARY;
  OrangeBoundary := ORANGE_BOUNDARY;

  // PIM-188 This must be called here, it also gets the settings!
  OpenSystemTables;
end;

// call this function into the UpdatePimsBase procedure if the update result
//is True
procedure TORASystemDM.OpenSystemTables;
begin
  // MR:10-05-2005 This is already done in 'DataModuleCreate'
(*
   if OracleLogon.Execute then
     OracleSession.Connected := True;
*)
  // MR:10-05-2005 This gives an error, so it is disabled.
(*
  ActiveTables(ORASystemDM, True);
*)
  //CAR 7-3-2003
//  ClientDataSetDay.Open;
  SynchronizeWorkStation;
  GetSettings;
end;

procedure TORASystemDM.DataModuleDestroy(Sender: TObject);
begin
  try
    OracleSession.Commit;
    OracleSession.Connected := False;
    ADateFmt.Free;
    ActiveTables(ORASystemDM, False);
  except
    // PIM-337 Ignore error (when started again while it was already running)
  end;
//  ASaveTimeRecScanning.Free;
end;

// GLOB3-284
function TORASystemDM.GetDayIndex(Index: Integer): Integer;
begin
  if WeekStartsOn = 2 then
  begin
    if Index = 7 then
      Index := 1
    else
      Index := (Index + 1);
  end;
  if WeekStartsOn = 4 then
  begin
    Index := PimsDayOfWeek(Index) - 1;
    if Index = 0 then
      Index := 7;
  end;
  if WeekStartsOn = 7 then
  begin
    if Index = 1 then
      Index := 7
    else
      Index := (Index - 1);
  end;
  Result := Index;
end; // GetDayIndex

function TORASystemDM.GetDayWDescription(Index: Integer): String;
begin
  Index := GetDayIndex(Index); // GLOB3-284
  Result := '';
  // CAR 7-3-2003
  if ClientDataSetDay.FindKey([Index]) then
    Result :=  ClientDataSetDay.FieldByName('DESCRIPTION').AsString;
end;

// CAR 3-7-2003
function TORASystemDM.GetDayWCode(Index: Integer): String;
begin
  Index := GetDayIndex(Index); // GLOB3-284
  Result := '';
  if ClientDataSetDay.FindKey([Index]) then
    Result :=  ClientDataSetDay.FieldByName('DAY_CODE').AsString;
end;

procedure TORASystemDM.TablePIMSettingsNewRecord(DataSet: TDataSet);
begin
end;

// TDateFmt Start
procedure TDateFmt.AfterConstruction;
begin
  inherited;
  FDateTimeFmt := CreateDateTimeFmt;
  FQuote := '''';
end;

// Get DateTime-formats from BDE-settings.
function TDateFmt.CreateDateTimeFmt: String;
var
//  Fmt: Fmtdate;
  DatePart, TimePart: String;
  Sep: String;
begin
//  Check(dbiGetDateFormat(Fmt));
//  Sep := Fmt.szDateSeparator;
  Sep := '-';
  // Defaults
  DatePart := 'dd'   + Sep + 'mm' + Sep + 'yyyy';
  TimePart := 'hh:nn'; // mm is month not minutes BUG????
  // ORA (TRS.DATETIME_IN) >=to_date(&DATEFROM, 'dd-mon-yyyy hh24:mi') 
  // Now build the formatstring for datetime
{  case Fmt.iDateMode of
  0: // MDY
    DatePart := 'mm'   + Sep + 'dd' + Sep + 'yyyy';
  1: // DMY
    DatePart := 'dd'   + Sep + 'mm' + Sep + 'yyyy';
  2: // YMD
    DatePart := 'yyyy' + Sep + 'mm' + Sep + 'dd';
  end; }
  Result := DatePart + ' ' + TimePart; // DMY
  // Set locale format for date-format
  // Save original (Windows) formats for later use
  FSepWindows     := DateSeparator;
  FDateFmtWindows := ShortDateFormat;
  FTimeFmtWindows := ShortTimeFormat;
  // Save also BDE's formats for later use
  FSepORA     := Sep;
  FDateFmtORA := DatePart;
  FTimeFmtORA := TimePart;
end;

// Set date/time-formats to Windows-formats
procedure TDateFmt.SwitchDateTimeFmtWindows;
begin
  DateSeparator   := FSepWindows[1];
  ShortDateFormat := FDateFmtWindows;
  ShortTimeFormat := FTimeFmtWindows;
end;

// Set date/time-formats to BDE-formats, needed when comparing
// with dates that are first converted to a String.
procedure TDateFmt.SwitchDateTimeFmtORA;
begin
  DateSeparator   := FSepORA[1];
  ShortDateFormat := FDateFmtORA;
  ShortTimeFormat := FTimeFmtORA;
end;
// TDateFmt End

// RV023.
function TORASystemDM.GetExportPayrollType: String;
begin
  Result := FExportPayrollType;
end;

// RV023.
procedure TORASystemDM.SetExportPayrollType(const Value: String);
begin
  FExportPayrollType := Value;
end;

// RV023. This is only needed to set the value for ExportPayrollType.
function TORASystemDM.GetExportPayroll: Boolean;
begin
  odsExportPayroll.Open;
  if not odsExportPayroll.Eof then
    SetExportPayrollType(odsExportPayroll.FieldByName('EXPORT_TYPE').AsString)
  else
    SetExportPayrollType('');
  odsExportPayroll.Close;
  Result := True;
end;

// RV072.1.
procedure TORASystemDM.SetIsRFL(const Value: Boolean);
begin
  FIsRFL := Value;
end;

// RV076.1.
function TORASystemDM.GetCode: String;
begin
  if not odsPimsSettings.Active then
    odsPimsSettings.Open;
  Result := odsPimsSettings.FieldByName('CODE').AsString;
end;

// RV076.1.
procedure TORASystemDM.SetIsLTB(const Value: Boolean);
begin
  FIsLTB := GetCode = 'LTB';
end;

// RV082.1.
procedure TORASystemDM.SetIsCVL(const Value: Boolean);
begin
  FIsCVL := GetCode = 'CVL';
end;

// 20013176
(*
procedure TORASystemDM.WErrorLog(AMsg: String);
begin
  WErrorLog(AMsg); // 20014826
end;
*)

function TORASystemDM.AutoCloseInterval: Integer;
begin
  Result := PSAutoCloseSecs * 1000;
end;

// TD-22082
function TORASystemDM.UserTeamLoginUser: String;
begin
  if UserTeamSelectionYN = CHECKEDVALUE then
    Result := CurrentLoginUser
  else
    Result := '*';
end;

// TD-23247
procedure TORASystemDM.Wait(const MSecsToWait: Integer);
var
  TimeToWait,
  TimeToSendRequest: TDateTime;
begin
  TimeToWait   := MSecsToWait / MSecsPerDay;
  TimeToSendRequest := TimeToWait + SysUtils.Now;
  while SysUtils.Now < TimeToSendRequest do
    Sleep(0);
end; // Wait

// TD-22082
procedure TORASystemDM.PlantTeamFilterEnable(DataSet: TDataSet);
begin
  if UserTeamSelectionYN = CHECKEDVALUE then
  begin
    with odsPlantDeptTeam do
    begin
      ClearVariables;
      SetVariable('USER_NAME', UserTeamLoginUser);
      Open;
      DataSet.Filtered := True;
      PlantTeamFilterOn := True;
    end;
  end
  else
  begin
    PlantTeamFilterOn := False;
  end;
end; // PlantTeamFilterEnable

// TD-22082
function TORASystemDM.PlantTeamFilter(DataSet: TDataSet): Boolean;
begin
  Result := True;
  if PlantTeamFilterOn then
  begin
    try
      if DataSet <> nil then
        if DataSet.FindField('PLANT_CODE') <> nil then
          if (not odsPlantDeptTeam.Eof) and DataSet.Active then
            Result := odsPlantDeptTeam.Locate('PLANT_CODE',
              DataSet.FieldByName('PLANT_CODE').AsString, []);
    except
      Result := True;
    end;
  end;
end; // PlantTeamFilter

// TD-22699 Reset a boolean to make it possible to get the time again
//          from server.
procedure TORASystemDM.TimerResetTimeTimer(Sender: TObject);
begin
  PimsTimeInitialised := False;
end;

// TD-25352
procedure TORASystemDM.SetCurrentComputerName(const Value: String);
begin
  FCurrentComputerName := Value;
end;

// 20014826 Log to DB
function TORASystemDM.DBLog(AMsg: String; APriority: Integer): Boolean;
begin
{
  APriority:
  1 = Message
  2 = Warning
  3 = Error
  4 = Stack trace
  5 = Debug info
}
  Result := LogToDB;
  if LogToDB then
  begin
    with oqABSLogInsert do
    begin
      try
        if AMsg <> '' then
        begin
          if Length(AMsg) > 255 then
            AMsg := Copy(AMsg, 1, 255);
          ClearVariables;
          SetVariable('COMPUTER_NAME', CurrentComputerName);
          SetVariable('LOGSOURCE',     AppName);
          SetVariable('PRIORITY',      APriority);
          SetVariable('SYSTEMUSER',    CurrentProgramUser);
          SetVariable('LOGMESSAGE',    AMsg);
          Execute;
//          if OracleSession.InTransaction then
//              OracleSession.Commit;
        end;
      except
        Result := False;
      end;
    end; // with
  end; // if
end; // DBLog

// 20014826
type
  TEXEVersionData = record
    CompanyName,
    FileDescription,
    FileVersion,
    InternalName,
    LegalCopyright,
    LegalTrademarks,
    OriginalFileName,
    ProductName,
    ProductVersion,
    Comments,
    PrivateBuild,
    SpecialBuild: string;
  end;

// 20014826
function GetEXEVersionData(const FileName: string): TEXEVersionData;
type
  PLandCodepage = ^TLandCodepage;
  TLandCodepage = record
    wLanguage,
    wCodePage: word;
  end;
var
  dummy,
  len: cardinal;
  buf, pntr: pointer;
  lang: string;
begin
  len := GetFileVersionInfoSize(PChar(FileName), dummy);
  if len = 0 then
    Exit;
  GetMem(buf, len);
  try
    if not GetFileVersionInfo(PChar(FileName), 0, len, buf) then
      Exit;

    if not VerQueryValue(buf, '\VarFileInfo\Translation\', pntr, len) then
      Exit;

    lang := Format('%.4x%.4x', [PLandCodepage(pntr)^.wLanguage, PLandCodepage(pntr)^.wCodePage]);

    if VerQueryValue(buf, PChar('\StringFileInfo\' + lang + '\CompanyName'), pntr, len){ and (@len <> nil)} then
      result.CompanyName := PChar(pntr);
    if VerQueryValue(buf, PChar('\StringFileInfo\' + lang + '\FileDescription'), pntr, len){ and (@len <> nil)} then
      result.FileDescription := PChar(pntr);
    if VerQueryValue(buf, PChar('\StringFileInfo\' + lang + '\FileVersion'), pntr, len){ and (@len <> nil)} then
      result.FileVersion := PChar(pntr);
    if VerQueryValue(buf, PChar('\StringFileInfo\' + lang + '\InternalName'), pntr, len){ and (@len <> nil)} then
      result.InternalName := PChar(pntr);
    if VerQueryValue(buf, PChar('\StringFileInfo\' + lang + '\LegalCopyright'), pntr, len){ and (@len <> nil)} then
      result.LegalCopyright := PChar(pntr);
    if VerQueryValue(buf, PChar('\StringFileInfo\' + lang + '\LegalTrademarks'), pntr, len){ and (@len <> nil)} then
      result.LegalTrademarks := PChar(pntr);
    if VerQueryValue(buf, PChar('\StringFileInfo\' + lang + '\OriginalFileName'), pntr, len){ and (@len <> nil)} then
      result.OriginalFileName := PChar(pntr);
    if VerQueryValue(buf, PChar('\StringFileInfo\' + lang + '\ProductName'), pntr, len){ and (@len <> nil)} then
      result.ProductName := PChar(pntr);
    if VerQueryValue(buf, PChar('\StringFileInfo\' + lang + '\ProductVersion'), pntr, len){ and (@len <> nil)} then
      result.ProductVersion := PChar(pntr);
    if VerQueryValue(buf, PChar('\StringFileInfo\' + lang + '\Comments'), pntr, len){ and (@len <> nil)} then
      result.Comments := PChar(pntr);
    if VerQueryValue(buf, PChar('\StringFileInfo\' + lang + '\PrivateBuild'), pntr, len){ and (@len <> nil)} then
      result.PrivateBuild := PChar(pntr);
    if VerQueryValue(buf, PChar('\StringFileInfo\' + lang + '\SpecialBuild'), pntr, len){ and (@len <> nil)} then
      result.SpecialBuild := PChar(pntr);
  finally
    FreeMem(buf);
  end;
end;

// 20014826
procedure TORASystemDM.AppNameSet(const Value: String);
var
  EXEVersionData: TEXEVersionData;
begin
  try
    EXEVersionData := GetEXEVersionData(Application.ExeName);
    if EXEVersionData.InternalName <> '' then
      FAppName := EXEVersionData.InternalName
    else
      FAppName := Value;
  except
    FAppName := Value;
  end;
  if (FAppName = APPNAME_PIMS) or (Pos('PimsORCL', FAppName) > 0) then
  begin
    UGLogFilenameSet('PIMSLOG');
    UGErrorLogFilenameSet('PIMSERRLOG');
  end
  else
  if (FAppName = APPNAME_AT) or (Pos('AdmTool', FAppName) > 0) then
  begin
    UGLogFilenameSet('ATLOG');
    UGErrorLogFilenameSet('ATERRLOG');
  end
  else
  if (FAppName = APPNAME_PS) or (Pos('PersonalScreen', FAppName) > 0) then
  begin
    UGLogFilenameSet('PSLOG');
    UGErrorLogFilenameSet('PSERRLOG');
  end
  else
  if (FAppName = APPNAME_PRS) or (Pos('ProductionScreen', FAppName) > 0) then
  begin
    UGLogFilenameSet('PRSLOG');
    UGErrorLogFilenameSet('PRSERRLOG');
  end
  else
  if (FAppName = APPNAME_MANREG) or (Pos('ManualRegistrations', FAppName) > 0) then
  begin
    UGLogFilenameSet('MANREGLOG');
    UGErrorLogFilenameSet('MANREGERRLOG');
  end
  else
  if (FAppName = APPNAME_TRSCR) or (Pos('TimeRecordingCR', FAppName) > 0) then
  begin
    UGLogFilenameSet('TRSCRLOG');
    UGErrorLogFilenameSet('TRSCRERRLOG');
  end
  else
  if (FAppName = APPNAME_TRS) or (Pos('TimeRecording', FAppName) > 0) then
  begin
    UGLogFilenameSet('TRSLOG');
    UGErrorLogFilenameSet('TRSERRLOG');
  end
  else
  if (FAppName = APPNAME_ADC1) or (Pos('AutoDataCol1', FAppName) > 0) then
  begin
    UGLogFilenameSet('ADC1LOG');
    UGErrorLogFilenameSet('ADC1ERRLOG');
  end
  else
  if (FAppName = APPNAME_ADC2) or (Pos('AutoDataCol2', FAppName) > 0) then
  begin
    UGLogFilenameSet('ADC2LOG');
    UGErrorLogFilenameSet('ADC2ERRLOG');
  end
  else
  if (FAppName = APPNAME_ADC3) or (Pos('AutoDataCol3', FAppName) > 0) then
  begin
    UGLogFilenameSet('ADC3LOG');
    UGErrorLogFilenameSet('ADC3ERRLOG');
  end
  else
  begin
    UGLogFilenameSet('PIMSLOG');
    UGErrorLogFilenameSet('PIMSERRLOG');
  end;
end; // AppNameSet

// 20016449
procedure TORASystemDM.DetermineWorkstationTimezonehrsdiff;
var
  WorkstationTimezone: String;
  PlantCode: String;
  TestTimezonehrsdiff: Integer;
begin
  WorkstationTimezone := '';
  WSTimezonehrsdiff := 0;
  TestTimezonehrsdiff := 0;
  odsWorkStation.Refresh;
  if odsWorkStation.Locate('COMPUTER_NAME', CurrentComputerName, []) then
  begin
    try
      // Determine timezone via linked plant
      PlantCode :=
        odsWorkStation.FieldByName('PLANT_CODE').AsString;
      if PlantCode <> '' then
      begin
        oqPlantFind.ClearVariables;
        oqPlantFind.SetVariable('PLANT_CODE', PlantCode);
        oqPlantFind.Execute;
        if not oqPlantFind.Eof then
        begin
          WorkstationTimezone := oqPlantFind.FieldAsString('TIMEZONE');
          if WorkstationTimezone = 'TEST' then
            TestTimezonehrsdiff := oqPlantFind.FieldAsInteger('TIMEZONEHRSDIFF');
        end;
      end;
    except
      WorkstationTimezone := '';
    end;
    if WorkstationTimezone = 'TEST' then
      WSTimezonehrsdiff := TestTimezonehrsdiff
    else
      WSTimezonehrsdiff :=
        DetermineTimezonehrsdiff(DatabaseTimezone, WorkstationTimezone);
  end;
end; // DetermineWorkstationTimezonehrsdiff

// 20016449
procedure TORASystemDM.PlantTimezonehrsdiffWriteToDB;
var
  PlantTimezonehrsdiff: Integer;
begin
  try
    oqPlant.Execute;
    while not oqPlant.Eof do
    begin
      PlantTimezonehrsdiff := 0;
      if oqPlant.Field('TIMEZONE') <> '' then
        PlantTimezonehrsDiff :=
          DetermineTimezonehrsdiff(DatabaseTimezone, oqPlant.Field('TIMEZONE'));
      // 20016449 For testing purposes
      if Pos('TEST', oqPlant.Field('TIMEZONE')) = 0 then
      begin
        oqPlantUpdate.ClearVariables;
        oqPlantUpdate.SetVariable('PLANT_CODE', oqPlant.Field('PLANT_CODE'));
        oqPlantUpdate.SetVariable('TIMEZONEHRSDIFF', PlantTimezonehrsdiff);
        oqPlantUpdate.Execute;
      end;
      oqPlant.Next;
    end;
  finally
    if ORASystemDM.OracleSession.InTransaction then
      ORASystemDM.OracleSession.Commit;
  end;
end; // PlantTimezonehrsdiffWriteToDB

// 20016449
function TORASystemDM.PlantTimezonehrsdiff(APlantCode: String): Integer;
begin
  Result := 0;
  if APlantCode = '' then
    Exit;
  try
    oqPlantFind.ClearVariables;
    oqPlantFind.SetVariable('PLANT_CODE', APlantCode);
    oqPlantFind.Execute;
    if not oqPlantFind.Eof then
      if oqPlantFind.FieldAsString('TIMEZONEHRSDIFF') <> '' then
        Result := oqPlantFind.FieldAsInteger('TIMEZONEHRSDIFF');
  finally
  end;
end; // PlantTimezonehrsdiff

// 20016449
function DateTimeTimeZone(ADateTime: TDateTime): TDateTime;
begin
  // Add/subtract hours because of Timezone-correcction
  Result := IncHour(ADateTime, ORASystemDM.WSTimezonehrsdiff);
end;

// 20016449
function DateTimeWithoutTimeZone(ADateTime: TDateTime): TDateTime;
begin
  // Add/subtract hours because of Timezone-correcction
  Result := IncHour(ADateTime, -1 * ORASystemDM.WSTimezonehrsdiff);
end;

// 20016449
function FormatDateTime(const Format: string; DateTime: TDateTime): string; //overload;
begin
//  Result := SysUtils.FormatDateTime(Format, DateTimeTimeZone(DateTime));
  Result := SysUtils.FormatDateTime(Format, DateTime);
end;

// 20016449
function DateTimeToStr(const DateTime: TDateTime): string;
begin
//  Result := SysUtils.DateTimeToStr(DateTimeTimeZone(DateTime));
  Result := SysUtils.DateTimeToStr(DateTime);
end;

// PIM-194
function TORASystemDM.NewEffColor(AEff: Double): TColor;
begin
  if AEff < RedBoundary then
    Result := clMyRed
  else
    if AEff < OrangeBoundary then
      Result := clMyOrange
    else
      Result := clMyGreen;
end; // NewEffColor

// PIM-194
procedure TORASystemDM.SetOrangeBoundary(const Value: Integer);
begin
  if ((Value >= MIN_COLOR_BOUNDARY) and (Value <= MAX_COLOR_BOUNDARY)) then
    FOrangeBoundary := Value
  else
    FOrangeBoundary := ORANGE_BOUNDARY;
end;

// PIM-194
procedure TORASystemDM.SetRedBoundary(const Value: Integer);
begin
  if ((Value >= MIN_COLOR_BOUNDARY) and (Value <= MAX_COLOR_BOUNDARY)) then
    FRedBoundary := Value
  else
    FRedBoundary := RED_BOUNDARY;
end;

procedure TORASystemDM.SetIsNTS(const Value: Boolean);
begin
  FIsNTS := GetCode = 'NTS';
end;

end.
