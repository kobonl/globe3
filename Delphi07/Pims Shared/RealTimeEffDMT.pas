(*
  MRA:1-MAY-2015 20014450.50 Part 2
  - Real Time Efficiency
  MRA:14-MAR-2016 PIM-151
  - Show Ghost Counts
  - Added 2 TOracleDataSets:
    - odsRTCurrGhostAll
    - odsRTShiftGhostALL
  MRA:21-MAR-2016 PIM-156
  - Addition procedure to do a recalc for the workspot for the current day.
  - oqMergeWSEffPerMinute had to be changed to prevent division by 0.
*)
unit RealTimeEffDMT;

interface

uses
  SysUtils, Classes, ORASystemDMT, DB, OracleData, Oracle, SyncObjs, Variants;

type
  TRealTimeEffDM = class(TDataModule)
    oqMergeWSEffPerMinute: TOracleQuery;
    oqRTEmpEff: TOracleQuery;
    oqRTCurrEmpEff: TOracleQuery;
    oqRTShiftEmpEff: TOracleQuery;
    odsRTEmpEff: TOracleDataSet;
    oqCopyThis: TOracleQuery;
    oqRTEmpEffGraph: TOracleQuery;
    odsRTEmpEffPRIMKEY: TStringField;
    odsRTEmpEffWORKSPOT_CODE: TStringField;
    odsRTEmpEffJOB_CODE: TStringField;
    odsRTEmpEffWORKSPOT: TStringField;
    odsRTEmpEffJOB: TStringField;
    odsRTEmpEffQACTUAL: TFloatField;
    odsRTEmpEffQNORM: TFloatField;
    odsRTEmpEffQDIFF: TFloatField;
    odsRTEmpEffTACTUAL: TFloatField;
    odsRTEmpEffTNORM: TFloatField;
    odsRTEmpEffTDIFF: TFloatField;
    odsRTEmpEffEFF: TFloatField;
    oqRTCurrWSEff: TOracleQuery;
    oqRTShiftWSEff: TOracleQuery;
    odsRTEmpEffINTERVALENDTIME: TDateTimeField;
    odsRTCurrWSEffALL: TOracleDataSet;
    odsRTShiftWSEffALL: TOracleDataSet;
    odsRTCurrGhostAll: TOracleDataSet;
    odsRTShiftGhostAll: TOracleDataSet;
    oqTRS: TOracleQuery;
    oqRecalcEff: TOracleQuery;
    oqCalcEff: TOracleQuery;
    odsCopyThis: TOracleDataSet;
  private
    { Private declarations }
  public
    { Public declarations }
    procedure MergeWorkspotEffPerMinute(
      AShiftDate: TDateTime; AShiftNumber: Integer; APlantCode, AWorkspotCode,
      AJobCode: String; AIntervalStartTime, AIntervalEndTime: TDateTime;
      AWorkspotSecondsActual, AWorkspotSecondsNorm, AWorkspotQuantity,
      ANormLevel: Double; AStartTimestamp,
      AEndTimestamp: TDateTime); // 20014450.50
    procedure CalcEfficiency(APlantCode: String);
    procedure RecalcEfficiency(APlantCode, AWorkspotCode, AJobCode: String;
      AShiftDate: TDateTime; AShiftNumber: Integer);
    procedure RecalcEfficiencyForToday(APlantCode, AWorkspotCode: String);
  end;

var
  RealTimeEffDM: TRealTimeEffDM;

implementation

{$R *.dfm}

uses
  UGlobalFunctions, UPimsMessageRes;

{ TRealTimeEffDM }

// 20014450.50
procedure TRealTimeEffDM.CalcEfficiency(APlantCode: String);
begin
  try
    oqCalcEff.SetVariable('PLANT_CODE', APlantCode);
    oqCalcEff.Execute;
    if ORASystemDM.OracleSession.InTransaction then
      ORASystemDM.OracleSession.Commit;
  except
    on E:EOracleError do
    begin
      WErrorLog('CalcEfficiency: ' + E.Message);
    end;
    on E:Exception do
    begin
      WErrorLog('CalcEfficiency: ' + E.Message);
    end;
  end;
end;

// PIM-156
procedure TRealTimeEffDM.RecalcEfficiency(APlantCode, AWorkspotCode,
  AJobCode: String; AShiftDate: TDateTime; AShiftNumber: Integer);
begin
  try
    oqRecalcEff.SetVariable('PLANT_CODE', APlantCode);
    oqRecalcEff.SetVariable('WORKSPOT_CODE', AWorkspotCode);
    oqRecalcEff.SetVariable('JOB_CODE', AJobCode);
    oqRecalcEff.SetVariable('SHIFT_DATE', AShiftDate);
    oqRecalcEff.SetVariable('SHIFT_NUMBER', AShiftNumber);
    oqRecalcEff.Execute;
    if ORASystemDM.OracleSession.InTransaction then
      ORASystemDM.OracleSession.Commit;
  except
    on E:EOracleError do
    begin
      WErrorLog('RecalcEfficiency: ' + E.Message);
    end;
    on E:Exception do
    begin
      WErrorLog('RecalcEfficiency: ' + E.Message);
    end;
  end;
end; // RecalcEfficiency

// PIM-156
// Do a recalc for given Plant + Workspot for today, based
// on scans found.
procedure TRealTimeEffDM.RecalcEfficiencyForToday(APlantCode,
  AWorkspotCode: String);
begin
  try
    with oqTRS do
    begin
      ClearVariables;
      SetVariable('PLANT_CODE',            APlantCode);
      SetVariable('WORKSPOT_CODE',         AWorkspotCode);
      SetVariable('SHIFT_DATE',            Date);
      Execute;
      while not Eof do
      begin
        RecalcEfficiency(
          APlantCode, AWorkspotCode, FieldAsString('JOB_CODE'),
          FieldAsDate('SHIFT_DATE'), FieldAsInteger('SHIFT_NUMBER')
          );
        Next;
      end;
    end;
  except
    on E:EOracleError do
    begin
      WErrorLog('RecalcEfficiency: ' + E.Message);
    end;
    on E:Exception do
    begin
      WErrorLog('RecalcEfficiency: ' + E.Message);
    end;
  end;
end; // RecalcEfficiencyForToday

// 20014450.50
procedure TRealTimeEffDM.MergeWorkspotEffPerMinute(AShiftDate: TDateTime;
  AShiftNumber: Integer; APlantCode, AWorkspotCode, AJobCode: String;
  AIntervalStartTime, AIntervalEndTime: TDateTime; AWorkspotSecondsActual,
  AWorkspotSecondsNorm, AWorkspotQuantity, ANormLevel: Double;
  AStartTimestamp, AEndTimestamp: TDateTime);
begin
  try
    with oqMergeWSEffPerMinute do
    begin
      ClearVariables;
      SetVariable('SHIFT_DATE',            AShiftDate);
      SetVariable('SHIFT_NUMBER',          AShiftNumber);
      SetVariable('PLANT_CODE',            APlantCode);
      SetVariable('WORKSPOT_CODE',         AWorkspotCode);
      SetVariable('JOB_CODE',              AJobCode);
      SetVariable('INTERVALSTARTTIME',     AIntervalStartTime);
      SetVariable('INTERVALENDTIME',       AIntervalEndTime);
      SetVariable('WORKSPOTSECONDSACTUAL', AWorkspotSecondsActual);
      SetVariable('WORKSPOTSECONDSNORM',   AWorkspotSecondsNorm);
      SetVariable('WORKSPOTQUANTITY',      AWorkspotQuantity);
      SetVariable('NORMLEVEL',             ANormLevel);
      SetVariable('START_TIMESTAMP',       AStartTimestamp);
      SetVariable('END_TIMESTAMP',         AEndTimestamp);
      SetVariable('MUTATOR',               ORASystemDM.CurrentProgramUser);
      Execute;
      if ORASystemDM.OracleSession.InTransaction then
        ORASystemDM.OracleSession.Commit;
{$IFDEF DEBUG2}
  WLog('MergeWSEffMin P=' + APlantCode +
    ' W=' + AWorkspotCode +
    ' J=' + AJobCode +
    ' S=' + DateTimeToStr(AIntervalStartTime) +
    ' E=' + DateTimeToStr(AIntervalEndTime) +
    ' Q=' + IntToStr(Round(AWorkspotQuantity))
    );
{$ENDIF}
    end;
  except
    on E:EOracleError do
    begin
      WErrorLog('MergeWorkspotEffPerMinute: ' + E.Message);
    end;
    on E:Exception do
    begin
      WErrorLog('MergeWorkspotEffPerMinute: ' + E.Message);
    end;
  end;
end; // MergeWorkspotEffPerMinute

end.
