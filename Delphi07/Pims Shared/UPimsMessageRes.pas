(*
  MRA:29-JAN-2016 PIM-5
  - German translation error.
  - Abwesend mit/ohne Reden translate Reden with Grund.
*)
unit UPimsMessageRes;

interface

uses
  SysUtils, Dialogs;

function DisplayMessage(const Msg: string; DlgType: TMsgDlgType;
  Buttons: TMsgDlgButtons): Integer;

resourcestring

{$IFDEF PIMSDUTCH}
  // ORASystemDMT
  SPimsKeyViolation = 'Gebruik een unieke waarde voor het '+
    'eerste veld.';
  SPimsForeignKeyPost = 'Dit record heeft een connectie met andere records.' + #13 +
    'Uw verzoek is geannuleerd.';
  SPimsForeignKeyDel = 'Dit record heeft een connectie met andere records.' + #13 +
    'Uw verzoek is geannuleerd.';
  SPimsFieldRequiredFmt = 'Vul aub de gevraagde invoer';
  SPimsFieldRequired = 'Aub alle verplichte velden vullen.';
  SPimsDetailsExist = 'Invoer niet toegestaan';
  SPimsRecordLocked = 'Dit record wordt door een andere gebruiker gebruikt.';
  SPimsTBDeleteLastRecord = 'Alleen het laatste record kan verwijderd worden';
  SPimsTBInsertRecord = 'Het maximum is 4 tijdblokken';
  SPimsStartEndDate = 'Startdatum is later dan einddatum';
  SPimsStartEndTime = 'Starttijd is later dan eindtijd';
  SPimsStartEndDateTime = 'Startdatum/tijd ligt na einddatum/tijd';
  SPimsDateinPreviousPeriod = 'Er bestaat al een scan met deze tijd';
  SPimsEmplContractInDatePrevious =
   'Er is al een contract met deze datum';
  SPimsEXCheckIllMsgClose =
    'Er is al een openstaande ziektemelding, sluit deze eerst af';
  SPimsEXCheckIllMsgStartDate =
    'Startdatum moet groter zijn dan einddatum van laatst aangemaakte ziektemelding ';
  SPimsGeneralSQLError = 'BDE reported a general SQL Error';
  SPimsConfirmDelete = 'Wilt u dit record verwijderen?';
  // TimeRecordingDMT
  SPimsStartDay = 'Start  van de dag';
  SpimsEndDay = 'Einde van de dag';
  SPimsNoPrevScanningFound = 'Geen vorige scanning gevonden';
  SPimsIDCardNotFound = 'Pas niet gevonden';
  SPimsIDCardExpired = 'Pas is ongeldig';
  SPimsEmployeeNotFound = 'Werknemer niet gevonden';
  SPimsInactiveEmployee = 'Werknemer inactief';
  SPimsNonScanEmployee = 'Werknemer is geen scanner. Scannen is niet toegestaan.';
  // SelectorFRM
  SPImsColumnCode = 'Code';
  SPimsColumnPlant = 'Bedrijf';
  // PersonalScreenDMT
  SPimsNoJobDescription = 'Geen job';
  SPimsBreakJobDescription = 'Pauze';
  SPimsMechanicalDown = 'Mechan. Storing';
  SPimsNoMerchandize = 'Geen Artikelen';
  // UPersonalScreen
  SPimsMultiple = '<Meerdere>';
  SPimsInButton = 'In/Uit';
  SPimsOutButton = 'UIT';
  SPimsModeButtonShift = 'Ploeg';
  SPimsModeButtonCurrent = 'Huidig';
  SPimsModeButtonSince = 'Vandaag';
  SPimsActual = 'Huidig';
  SPimsTarget = 'Doel';
  SPimsExtra = 'Extra';
  // DialogEnterJobCommentFRM
  SPimsPleaseEnterComment = 'Geef a.u.b. een melding.';
  // HybridDMT
  SPimsWorkspotOccupied = 'Werkplek bezet';
  SPimsYes = 'JA';
  SPimsNo = 'NEE';
  SPimsWorkspotOccupiedQuestion = ' Deze werkplek is al bezet'#13' door werknemer [%s].'#13' Moet [%s] worden uitgescand en u in?';
  SPimsAtWorkDescription = 'Aan het werk';
  SPimsOccupiedDescription = 'Bezet';
  // TimeRecordingFRM
  SPimsWorkspot = 'Selecteer werkplek';
  SPimsColumnWorkspot = 'Werkplek';
  SPimsNoWorkspot = 'Geen werkplekken gedefinieerd voor deze PC';
  SPimsJob = 'Selecteer job';
  SPimsColumnDescription = 'Omschrijving';
  SPimsNoMatch = 'Geen match voor werknemer + pasje';
  SPimsNonScanningStation = 'Dit is geen tijdregistratie-station';
  SPimsLastScanNotClosed = 'Laatste scan is niet afgesloten:';
  SPimsConnectionLost = 'Error: Database-connectie verbroken. Probeer nogmaals.';
  SPimsScanDone = 'Scanning verwerkt';
  // DialogLoginFRM
  SAplErrorLogin = 'Ongeldige gebruikersnaam of wachtwoord!';
  SAplNotVisible = 'Deze gebruiker mag de applicatie niet gebruiken';
  // DialogEmpGraphFRM
  SPimsTotalProduction = 'Totaal produktie';
  SPimsEfficiencyAt = 'Eff. op';
  SPimsProductionOutput = 'Produktie uitvoer';
  // DialogSettingsFRM
  SPimsChangeSettingWarning = 'U moet de applicatie opnieuw starten vanwege deze nieuwe instelling.';
  // DialogSelectModeFRM
  SPimsEmployeesAll = 'Alle werknemers';
  SPimsEmployeesOnWrongWorkspot = 'Op verkeerde werkplek';
  SPimsEmployeesNotScannedIn = 'Niet ingescand';
  SPimsEmployeesAbsentWithReason = 'Ziek met reden';
  SPimsEmployeesAbsentWithoutReason = 'Ziek zonder reden';
  SPimsEmployeesWithFirstAid = 'Met BHV';
  SPimsEmployeesTooLate = 'Te laat';
  // HomeFRM (PersonalScreen)
  SPimsLoadingScheme = 'Inlezen schema... Even wachten a.u.b.';
  SPimsTestingBlackBoxes = 'Blackboxen worden getest...';
  SPimsSaveChanges = 'Opslaan? ';
  SPimsSavingScheme = 'Opslaan Schema... Even wachten a.u.b.';
  SPimsNoData = 'Geen data';
  SPimsNotScannedIn = '<Niet ingescand>';
  SPimsDeleteMachineWorkspots = 'Verwijder machine + alle gerelateerde werkplekken ?';
  SPimsJobDoesNotExistForWorkspot = 'Fout: Job bestaat niet voor werkplek!';
  SPimsSavingAndClosing = 'Opslaan + Einde... Even wachten a.u.b.';
  SPimsShort = 'Tekort';
  SPimsSavingLog = 'Opslaan Log... Even wachten a.u.b.';
  SPimsCounterOneNotRead = 'Teller 1 niet gelezen!';
  SPimsCounterTwoNotRead = 'Teller 2 niet gelezen!';
  SPimsBlackBoxDisabling = 'Wordt uitgeschakeld.';
  SPimsErrorDuringReadCounter = 'Fout tijdens lezen teller!';
  SPimsBlackBoxIsDisabled = 'Is uitgeschakeld.';
  SPimsNoCountersDefined = 'FOUT: Geen tellers gedefinieerd voor lezen blackboxen!';
  // UWorkspotEmpEff
  SPimsPcs = 'st.';
  SPims30Min = '30 min';
  SPims15Min = '15 min';
  SPims60Min = '60 min';
  SPimsToday = 'Vandaag';
  // ShowChartFRM
  SPimsEfficiencySince = 'Eff. sinds';
  SPimsEfficiencyShift = 'Efficientie ploeg';
  SPimsChartHelp = 'Kies tijd: klik op grafiek';
  // DialogSelectModeFRM
  SPimsEmployeesHeadCount = 'Koppen tellen';
  // HomeFRM (ProductionScreen)
  SPimsLinkedJob = 'Gelinkte job'; // 20013196
  SPimsTotalHeadCount = 'Totaal';
  SPimsCannotOpenHTML = 'Please install an Internet Browser on your machine.';
  SPimsSchemeNotFound = 'Waarschuwing: Schema niet gevonden:';
  SPimsStartToWork = 'Ik begin met werken';
  SPimsEndingWork = 'Ik stop met werken';
  SPimsSeeSR = 'Ga naar Supervisor';
  SPimsWorkspotOccupiedGoToSupervisor = ' Deze werkplek is bezet.'#13' Ga a.u.b. naar de supervisor';
  SPimsWorkspotOccupiedQuestion2 = ' Deze werkplek is al bezet'#13' door werknemer [%s].'#13' Moet [%s] worden uitgescand en u in?';
  SPimsOK = 'OK';
  SPimsCancel = 'Annuleer';

{$ELSE}
  {$IFDEF PIMSFRENCH}
  // ORASystemDMT
  SPimsKeyViolation = 'Please use a unique value for the first field.';
  SPimsForeignKeyPost = 'This record has a connection to other records. Your '+
    'request is denied.';
  SPimsForeignKeyDel = 'This record has a connection to other records. Your '+
    'request is denied.';
  SPimsFieldRequiredFmt = 'Please fill required field %s.';
  SPimsFieldRequired = 'Please fill all required fields.';
  SPimsDetailsExist = 'This record has a connection to other records. Please '+
    'remove those records first.';
  SPimsRecordLocked = 'This record is in use by another user';
  SPimsTBDeleteLastRecord = 'Only the last record can be deleted';
  SPimsTBInsertRecord = 'Only 4 timeblocks allowed';
  SPimsStartEndDate = 'Start date is bigger than end date';
  SPimsStartEndTime = 'Start time is bigger than end time';
  SPimsStartEndDateTime = 'Start date time is bigger than end date time';
  SPimsDateinPreviousPeriod =  'There already is a scan that includes this time';
  SPimsEmplContractInDatePrevious =
   'There already is a contract that includes this date';
  SPimsEXCheckIllMsgClose =
    'There already is one outstanding illness message, please close it before';
  SPimsEXCheckIllMsgStartDate =
    'Start date should be bigger than the end date of last created illness message ';
  SPimsGeneralSQLError = 'BDE reported a general SQL Error';
  SPimsConfirmDelete = 'Do you want to delete the current record?';
  // TimeRecordingDMT
  SPimsStartDay = 'Start of day';
  SpimsEndDay = 'End of day';
  SPimsNoPrevScanningFound = 'No previous scanning found';
  SPimsIDCardNotFound = 'ID Card not found';
  SPimsIDCardExpired	= 'ID Card has expired';
  SPimsEmployeeNotFound = 'Employee not found';
  SPimsInactiveEmployee = 'Employee is inactive';
  SPimsNonScanEmployee = 'Employee is not a scanner. Scanning is not allowed.';
  // SelectorFRM
  SPimsColumnCode = 'Code';
  SPimsColumnPlant 	= 'Plant';
  // PersonalScreenDMT
  SPimsNoJobDescription = 'No Job';
  SPimsBreakJobDescription = 'Break';
  SPimsMechanicalDown = 'Trouble mecanique';
  SPimsNoMerchandize = 'Pas d''articles';
  // UPersonalScreen
  SPimsMultiple = '<Multiple>';
  SPimsInButton = 'In/Out';
  SPimsOutButton = 'OUT';
  SPimsModeButtonShift = '�quipe';
  SPimsModeButtonCurrent = 'Actuel';
  SPimsModeButtonSince = 'Jour';
  SPimsActual = 'Actuel';
  SPimsTarget = 'But';
  SPimsExtra = 'Extra';
  // DialogEnterJobCommentFRM
  SPimsPleaseEnterComment = 'Please enter a comment.';
  // HybridDMT
  SPimsWorkspotOccupied = 'Workspot occupied';
  SPimsYes = 'YES';
  SPimsNo = 'NO';
  SPimsWorkspotOccupiedQuestion = ' This workspot is already'#13' occupied by employee [%s].'#13' Should we log [%s] out and you in?';
  SPimsAtWorkDescription = 'At work';
  SPimsOccupiedDescription = 'Occupied';
  // TimeRecordingFRM
  SPimsWorkspot = 'Select Workspot';
  SPimsColumnWorkspot = 'Workspot';
  SPimsNoWorkspot = 'No Workspot defined on current workstation';
  SPimsJob 	= 'Select Job';
  SPimsColumnDescription = 'Description';
  SPimsNoMatch = 'No match for Employee + ID-Card';
  SPimsNonScanningStation = 'This is not a time recording station';
  SPimsLastScanNotClosed = 'Last scan is not closed:';
  SPimsConnectionLost = 'Error: Database connection lost. Please try again.';
  SPimsScanDone = 'Scanning processed';
  // DialogLoginFRM
  SAplErrorLogin = 'Invalid user name or password!';
  SAplNotVisible = 'This user can not open the application';
  // DialogEmpGraphFRM
  SPimsTotalProduction = 'Total production';
  SPimsEfficiencyAt = 'Efficiency at';
  SPimsProductionOutput = 'Production Output';
  // DialogSettingsFRM
  SPimsChangeSettingWarning = 'You have to restart the application before this setting will take effect.';
  // DialogSelectModeFRM
  SPimsEmployeesAll = 'All employees';
  SPimsEmployeesOnWrongWorkspot = 'On wrong workspot';
  SPimsEmployeesNotScannedIn = 'Not scanned in';
  SPimsEmployeesAbsentWithReason = 'Absent with reason';
  SPimsEmployeesAbsentWithoutReason = 'Absent without reason';
  SPimsEmployeesWithFirstAid = 'With first aid';
  SPimsEmployeesTooLate = 'Too late';
  // HomeFRM (PersonalScreen)
  SPimsLoadingScheme = 'Loading Scheme... Please wait...';
  SPimsTestingBlackBoxes = 'Testing blackboxes...';
  SPimsSaveChanges = 'Save changes?';
  SPimsSavingScheme = 'Saving Scheme... Please wait...';
  SPimsNoData = 'No Data';
  SPimsNotScannedIn = '<Not Scanned In>';
  SPimsDeleteMachineWorkspots = 'Delete machine + all related workspots ?';
  SPimsJobDoesNotExistForWorkspot = 'Error: Job does not exist for workspot!';
  SPimsSavingAndClosing = 'Saving + Closing... Please wait...';
  SPimsShort = 'Insuf.';
  SPimsSavingLog = 'Saving Log... Please wait...';
  SPimsCounterOneNotRead = 'Counter 1 not read!';
  SPimsCounterTwoNotRead = 'Counter 2 not read!';
  SPimsBlackBoxDisabling = 'Will be disabled.';
  SPimsErrorDuringReadCounter = 'Error during read of counter!';
  SPimsBlackBoxIsDisabled = 'Is disabled.';
  SPimsNoCountersDefined = 'ERROR: No counters defined for reading blackboxes!';
  // UWorkspotEmpEff
  SPimsPcs = 'pcs.';
  SPims30Min = '30 min';
  SPims15Min = '15 min';
  SPims60Min = '60 min';
  SPimsToday = 'Today';
  // ShowChartFRM
  SPimsEfficiencySince = 'Efficiency since';
  SPimsEfficiencyShift = 'Efficiency shift';
  SPimsChartHelp = 'select time: click on graph';
  // DialogSelectModeFRM
  SPimsEmployeesHeadCount = 'Head count';
  // HomeFRM (ProductionScreen)
  SPimsLinkedJob = 'Linked job'; // 20013196
  SPimsTotalHeadCount = 'Total';
  SPimsCannotOpenHTML = 'Please install an Internet Browser on your machine.';
  SPimsSchemeNotFound = 'Warning: Scheme not found:';
  SPimsStartToWork = 'I am starting work';
  SPimsEndingWork = 'I am ending work';
  SPimsSeeSR = 'See HR';
  SPimsWorkspotOccupiedGoToSupervisor = ' This workspot is occupied.'#13' Please go to supervisor';
  SPimsWorkspotOccupiedQuestion2 = ' This workspot is already'#13' occupied by employee [%s].'#13' Should we log [%s] out and you in?';
 SPimsOK = 'OK';
  SPimsCancel = 'Cancel';

{$ELSE}
  {$IFDEF PIMSGERMAN}
  // ORASystemDMT
  SPimsKeyViolation = 'Bitte gebrauchen Sie einen einzigarten Wert f�r das '+
    'erste Feld.';
  SPimsForeignKeyPost = 'Eingabe nicht erlaubt';
  SPimsForeignKeyDel = 'Eingabe nicht erlaubt';
  SPimsFieldRequiredFmt = 'Bitte gib gefragte Eingabe';
  SPimsFieldRequired = 'F�llen Sie bitte alle verpflichteten Felder aus.';
  SPimsDetailsExist = 'Eingabe nicht erlaubt';
  SPimsRecordLocked = 'Dieses Record wird von einem anderen Benutzer benutzt.';
  SPimsTBDeleteLastRecord = 'Nur das letzte Record kan gel�scht werden';
  SPimsTBInsertRecord = 'Maximum ist 4 Zeitbl�cke';
  SPimsStartEndDate = 'Startdatum ist gr�sser als Enddatum';
  SPimsStartEndTime = 'Startzeit ist gr�sser als Endzeit';
  SPimsStartEndDateTime = 'Start Datum/Zeit ist sp�ter dann Ende Zeit';
  SPimsDateinPreviousPeriod = 'Konflikt zwischen eingabe Start / Enddatum der '+
    'Perioden';
  SPimsEmplContractInDatePrevious =
   'Ein Vertrag existiert schon mit dieser Datum';
  SPimsEXCheckIllMsgClose =
    'There already is one outstanding illness message, please close it before';
  SPimsEXCheckIllMsgStartDate =
    'Start date should be bigger than the end date of last created illness message ';
  SPimsGeneralSQLError = 'BDE reported a general SQL Error';
  SPimsConfirmDelete = 'M�chten Sie den aktuellen Record entfernen?';
  // TimeRecordingDMT
  SPimsStartDay = 'Start vom Tag';
  SpimsEndDay = 'Ende vom Tag';
  SPimsNoPrevScanningFound = 'No previous scan found';
  SPimsIDCardNotFound = 'Karte nicht gefunden';
  SPimsIDCardExpired = 'Karte ist ung�ltig';
  SPimsEmployeeNotFound = 'Mitarbeiter nicht gefunden';
  SPimsInactiveEmployee = 'Mitarbeiter inaktiv';
  SPimsNonScanEmployee = 'Employee is not a scanner. Scanning is not allowed.';
  // SelectorFRM
  SPImsColumnCode = 'Kode';
  SPimsColumnPlant = 'Betrieb';
  // PersonalScreenDMT
  SPimsNoJobDescription = 'Kein Job';
  SPimsBreakJobDescription = 'Break';
  SPimsMechanicalDown = 'Mechanisch Down';
  SPimsNoMerchandize = 'Kein Merchandize';
  // UPersonalScreen
  SPimsMultiple = '<Mehrere>';
  SPimsInButton = 'Ein/Aus';
  SPimsOutButton = 'AUS';
  SPimsModeButtonShift = 'Schicht';
  SPimsModeButtonCurrent = 'Aktuell';
  SPimsModeButtonSince = 'Heute';
  SPimsActual = 'Heute';
  SPimsTarget = 'Ziel';
  SPimsExtra = 'Extra';
  // DialogEnterJobCommentFRM
  SPimsPleaseEnterComment = 'Please enter a comment.';
  // HybridDMT
  SPimsWorkspotOccupied = 'Arbeitsplatz besetzt';
  SPimsYes = 'JA';
  SPimsNo = 'NEIN';
  SPimsWorkspotOccupiedQuestion = ' Dieser Arbeitsplatz ist bereits'#13' durch Mitarbeiter [%s] besetzt.'#13' Sollen wir [%s] aus-scannen und Sie in?';
  SPimsAtWorkDescription = 'Arbeiten';
  SPimsOccupiedDescription = 'Besetzt';
  // TimeRecordingFRM
  SPimsWorkspot = 'Selektiere Arbeitsplatz';
  SPimsColumnWorkspot = 'Arbeitsplatz';
  SPimsNoWorkspot = 'Keine Arbeitspl�tze definiert f�r diese PC';
  SPimsJob = 'Selektiere Job';
  SPimsColumnDescription = 'Beschreibung';
  SPimsNoMatch = 'No match found for employee + idcard';
  SPimsNonScanningStation = 'This is no timerecording-station';
  SPimsLastScanNotClosed = 'Letzte Scan ist nicht geschlossen:';
  SPimsConnectionLost = 'Error: Database connection lost. Please try again.';
  SPimsScanDone = 'Scannung verarbeitet';
  // DialogLoginFRM
  SAplErrorLogin = 'Invalid Benutzername oder Kennwort!';
  SAplNotVisible = 'This user cannot open the application';
  // DialogEmpGraphFRM
  SPimsTotalProduction = 'Total Produktion';
  SPimsEfficiencyAt = 'Effizienz am';
  SPimsProductionOutput = 'Produktion Leist.';
  // DialogSettingsFRM
  SPimsChangeSettingWarning = 'You have to restart the application before this setting will take effect.';
  // DialogSelectModeFRM
  SPimsEmployeesAll = 'Alle Mitarbeiter';
  SPimsEmployeesOnWrongWorkspot = 'Am falschen Arbeitsplatz';
  SPimsEmployeesNotScannedIn = 'Nicht eingescannt';
  SPimsEmployeesAbsentWithReason = 'Abwesend mit Grund';
  SPimsEmployeesAbsentWithoutReason = 'Abwesend ohne Grund';
  SPimsEmployeesWithFirstAid = 'Mit Erste Hilfe';
  SPimsEmployeesTooLate = 'Zu sp�t';
  // HomeFRM (PersonalScreen)
  SPimsLoadingScheme = 'Einlesen Schema... Bitte warten...';
  SPimsTestingBlackBoxes = 'Testing blackboxes...';
  SPimsSaveChanges = 'Speichern? ';
  SPimsSavingScheme = 'Speichern Schema... Bitte warten...';
  SPimsNoData = 'No Data';
  SPimsNotScannedIn = '<Nicht eingescannt>';
  SPimsDeleteMachineWorkspots = 'Entfernen Machine + alle verbunden Arbeitspl�tze ?';
  SPimsJobDoesNotExistForWorkspot = 'Fehler: Job existiert nicht f�r Arbeitsplatz!';
  SPimsSavingAndClosing = 'Speichern + Ende... Bitte warten...';
  SPimsShort = 'Kurz';
  SPimsSavingLog = 'Speichern Log... Bitte warten...';
  SPimsCounterOneNotRead = 'Counter 1 not read!';
  SPimsCounterTwoNotRead = 'Counter 2 not read!';
  SPimsBlackBoxDisabling = 'Will be disabled.';
  SPimsErrorDuringReadCounter = 'Error during read of counter!';
  SPimsBlackBoxIsDisabled = 'Is disabled.';
  SPimsNoCountersDefined = 'ERROR: No counters defined for reading blackboxes!';
  // UWorkspotEmpEff
  SPimsPcs = 'St.';
  SPims30Min = '30 Min';
  SPims15Min = '15 Min';
  SPims60Min = '60 Min';
  SPimsToday = 'Heute';
  // ShowChartFRM
  SPimsEfficiencySince = 'Effizienz zeit';
  SPimsEfficiencyShift = 'Effizienz Schicht';
  SPimsChartHelp = 'Selektier Zeit: Klick auf Graph'; //, move: right-click between graph-items';
  // DialogSelectModeFRM
  SPimsEmployeesHeadCount = 'Head count';
  // HomeFRM (ProductionScreen)
  SPimsLinkedJob = 'Linked job'; // 20013196
  SPimsTotalHeadCount = 'Total';
  SPimsCannotOpenHTML = 'Please install an Internet Browser on your machine.';
  SPimsSchemeNotFound = 'Warnung: Scheme nicht gefunden:';
  SPimsStartToWork = 'Ich fange an zu arbeiten';
  SPimsEndingWork = 'Ich beende die Arbeit';
  SPimsSeeSR = 'Bitte geh zum Vorgesetzten';
  SPimsWorkspotOccupiedGoToSupervisor = ' Dieser Arbeitsplatz ist belegt.'#13' Bitte gehen Sie zum Vorgesetzten';
  SPimsWorkspotOccupiedQuestion2 = ' Dieser Arbeitsplatz ist bereits'#13' durch Mitarbeiter [%s] besetzt.'#13' Sollen wir [%s] aus-scannen und Sie in?';
  SPimsOK = 'OK';
  SPimsCancel = 'Annulier';

{$ELSE}
  {$IFDEF PIMSDANISH}
  // ORASystemDMT
  SPimsKeyViolation = 'Linie eksisterer allerede';
  SPimsForeignKeyPost = 'Denne fil har forbindelse til andre filer. Dit '+
    '�nske er afvist.';
  SPimsForeignKeyDel = 'Denne fil har forbindelse til andre filer. Dit '+
    '�nske er afvist.';
  SPimsFieldRequiredFmt = 'Udfyld n�dvendige felter %s.';
  SPimsFieldRequired = 'Udfyld alle n�dvendige felter.';
  SPimsDetailsExist = 'Denne fil har forbindelse til andre filer.  '+
    'fjern f�rst disse filer.';
  SPimsRecordLocked = 'Denne fil bruges af anden bruger';
  SPimsTBDeleteLastRecord = 'Kun seneste fil kan slettes';
  SPimsTBInsertRecord = 'Kun 4 tidsblokke tilladt';
  SPimsStartEndDate = 'Startdato er senere end slutdato';
  SPimsStartEndTime = 'Starttid er senere end sluttid';
  SPimsStartEndDateTime = 'Startdato/tid er senere end slutdato/tid';
  SPimsDateinPreviousPeriod =  'Denne tid findes p� en eksisterende skanning';
  SPimsEmplContractInDatePrevious =
   'En kontrakt indeholder allerede denne dato';
  SPimsEXCheckIllMsgClose =
    'Der er allerede en �ben sygdomsmedd., luk denne f�r';
  SPimsEXCheckIllMsgStartDate =
    'Start dato b�r v�re st�rre end slut dato for sidst dannede sygdomsmedd. ';
  SPimsGeneralSQLError = 'BDE reported a general SQL Error';
  SPimsConfirmDelete = 'Do you want to delete the current record?';
  // TimeRecordingDMT
  SPimsStartDay = 'Start p� dag';
  SpimsEndDay = 'Slut dag';
  SPimsNoPrevScanningFound = 'Ikke fundet tidl. skanning';
  SPimsIDCardNotFound = 'ID Kort ej fundet';
  SPimsIDCardExpired	= 'ID Kort udl�bet';
  SPimsEmployeeNotFound = 'Medarb. ikke fundet';
  SPimsInactiveEmployee = 'Medarb. er inaktiv';
  SPimsNonScanEmployee = 'Medarbejder er ikke en scanner. Scanningen er ikke tilladt.';
  // SelectorFRM
  SPImsColumnCode = 'Kode';
  SPimsColumnPlant 	= 'Virks';
  // PersonalScreenDMT
  SPimsNoJobDescription = 'Ingen job';
  SPimsBreakJobDescription = 'Pause';
  SPimsMechanicalDown = 'Mekanisk ned';
  SPimsNoMerchandize = 'Ingen salgsartikler';
  // UPersonalScreen
  SPimsMultiple = '<Mangfoldig>';
  SPimsInButton = 'I/Ud';
  SPimsOutButton = 'UD';
  SPimsModeButtonShift = 'Skift';
  SPimsModeButtonCurrent = 'Aktuel';
  SPimsModeButtonSince = 'I dag';
  SPimsActual = 'Aktuel';
  SPimsTarget = 'M�l';
  SPimsExtra = 'Ekstra';
  // DialogEnterJobCommentFRM
  SPimsPleaseEnterComment = 'Please enter a comment.';
  // HybridDMT
  SPimsWorkspotOccupied = 'Arb. sted bes�tte';
  SPimsYes = 'JA';
  SPimsNo = 'INGEN';
  SPimsWorkspotOccupiedQuestion = ' Dette arb.sted er allerede'#13' besat af medarbejder [%s].'#13' Scan [%s] ud og dig i?';
  SPimsAtWorkDescription = 'Arbejder';
  SPimsOccupiedDescription = 'Bes�tte';
  // TimeRecordingFRM
  SPimsWorkspot = 'V�lg arb.sted';
  SPimsColumnWorkspot = 'Arb.sted';
  SPimsNoWorkspot = 'Arb.sted ikke defineret p� denne arb.station';
  SPimsJob 	= 'V�lg Job';
  SPimsColumnDescription = 'Beskrivelse';
  SPimsNoMatch = 'Ingen match for medarb.+ID-kort';
  SPimsNonScanningStation = 'Denne station er ikke til tidsreg.';
  SPimsLastScanNotClosed = 'Last scan is not closed:';
  SPimsConnectionLost = 'Fejl: Database forbindelse mistet. Genstart program';
  SPimsScanDone = 'Scanning gennemf.';
  // DialogLoginFRM
  SAplErrorLogin = 'Ugyldigt brugernavn eller PW!';
  SAplNotVisible = 'Bruger kan ikke �bne dette program';
  // DialogEmpGraphFRM
  SPimsTotalProduction = 'Total produktion';
  SPimsEfficiencyAt = 'Effektiv p�';
  SPimsProductionOutput = 'Produktion Output';
  // DialogSettingsFRM
  SPimsChangeSettingWarning = 'Du er n�dt til at genstarte programmet, f�r denne indstilling vil tr�de i kraft.';
  // DialogSelectModeFRM
  SPimsEmployeesAll = 'Alle medarb.';
  SPimsEmployeesOnWrongWorkspot = 'Forkert arb.sted';
  SPimsEmployeesNotScannedIn = 'Ikke indskann.';
  SPimsEmployeesAbsentWithReason = 'Absent with reason';
  SPimsEmployeesAbsentWithoutReason = 'Frav�r med grund';
  SPimsEmployeesWithFirstAid = 'Med f�rstehj.';
  SPimsEmployeesTooLate = 'For sent';
  // HomeFRM (PersonalScreen)
  SPimsLoadingScheme = 'loading skemaet... vent venligst...';
  SPimsTestingBlackBoxes = 'Test blackboxes...';
  SPimsSaveChanges = 'Gem �ndr.?';
  SPimsSavingScheme = 'besparelse skemaet... vent venligst...';
  SPimsNoData = '0 Data';
  SPimsNotScannedIn = '<Ikke scannes i>';
  SPimsDeleteMachineWorkspots = 'Slette maskinen og alle relaterede arbejdspladser ?';
  SPimsJobDoesNotExistForWorkspot = 'Fejl: Job eksisterer ikke for arbejde plads!';
  SPimsSavingAndClosing = 'Gemme og lukke... vent venligst...';
  SPimsShort = 'Kort';
  SPimsSavingLog = 'Gemme Log... vent venligst...';
  SPimsCounterOneNotRead = 'T�ller 1 ikke l�se!';
  SPimsCounterTwoNotRead = 'T�ller 1 ikke l�se!';
  SPimsBlackBoxDisabling = 'Bliver deaktiveret.';
  SPimsErrorDuringReadCounter = 'Fejl under l�sning af t�ller!';
  SPimsNoCountersDefined = 'FEJL: Ingen t�llere er defineret for at l�se blackboxes!';
  SPimsBlackBoxIsDisabled = 'Er deaktiveret.';
  // UWorkspotEmpEff
  SPimsPcs = 'st.';
  SPims30Min = '30 min';
  SPims15Min = '15 min';
  SPims60Min = '60 min';
  SPimsToday = 'I dag';
  // ShowChartFRM
  SPimsEfficiencySince = 'Effektiv siden';
  SPimsEfficiencyShift = 'Effektivitet skift';
  SPimsChartHelp = 'V�lg tid: Klik p� graf';
  // DialogSelectModeFRM
  SPimsEmployeesHeadCount = 'Opt�lling';
  // HomeFRM (ProductionScreen)
  SPimsLinkedJob = 'Linked job';
  SPimsTotalHeadCount = 'Total';
  SPimsCannotOpenHTML = 'Please install an Internet Browser on your machine.';
  SPimsSchemeNotFound = 'Advarsel: Ordningen er ikke fundet:';
  SPimsStartToWork = 'Jeg starter arbejdet';
  SPimsEndingWork = 'Jeg slutter med arbejdet';
  SPimsSeeSR = 'Venligst g� til vejleder';
  SPimsWorkspotOccupiedGoToSupervisor = ' Denne arbejdsplads er besat.'#13' Venligst g? til vejleder';
  SPimsWorkspotOccupiedQuestion2 = ' Dieser Arbeitsplatz ist bereits'#13' durch Mitarbeiter [%s] besetzt.'#13' Sollen wir [%s] aus-scannen und Sie in?';
  SPimsOK = 'OK';
  SPimsCancel = 'Annuller';

{$ELSE}
  {$IFDEF PIMSJAPANESE}
  // ORASystemDMT
  SPimsKeyViolation = 'Please use a unique value for the first field.';
  SPimsForeignKeyPost = 'This record has a connection to other records. Your '+
    'request is denied.';
  SPimsForeignKeyDel = 'This record has a connection to other records. Your '+
    'request is denied.';
  SPimsFieldRequiredFmt = 'Please fill required field %s.';
  SPimsFieldRequired = 'Please fill all required fields.';
  SPimsDetailsExist = 'This record has a connection to other records. Please '+
    'remove those records first.';
  SPimsRecordLocked = 'This record is in use by another user';
  SPimsTBDeleteLastRecord = 'Only the last record can be deleted';
  SPimsTBInsertRecord = 'Only 4 timeblocks allowed';
  SPimsStartEndDate = 'Start date is bigger than end date';
  SPimsStartEndTime = 'Start time is bigger than end time';
  SPimsStartEndDateTime = 'Start date time is bigger than end date time';
  SPimsDateinPreviousPeriod =  'There already is a scan that includes this time';
  SPimsEmplContractInDatePrevious =
   'There already is a contract that includes this date';
  SPimsEXCheckIllMsgClose =
    'There already is one outstanding illness message, please close it before';
  SPimsEXCheckIllMsgStartDate =
    'Start date should be bigger than the end date of last created illness message ';
  SPimsGeneralSQLError = 'BDE reported a general SQL Error';
  SPimsConfirmDelete = 'Do you want to delete the current record?';
  // TimeRecordingDMT
  SPimsStartDay = 'Start of day';
  SpimsEndDay = 'End of day';
  SPimsNoPrevScanningFound = 'No previous scanning found';
  SPimsIDCardNotFound = 'ID Card not found';
  SPimsIDCardExpired	= 'ID Card has expired';
  SPimsEmployeeNotFound = 'Employee not found';
  SPimsInactiveEmployee = 'Employee is inactive';
  SPimsNonScanEmployee = 'Employee is not a scanner. Scanning is not allowed.';
  // SelectorFRM
  SPimsColumnCode = 'Code';
  SPimsColumnPlant 	= 'Plant';
  // PersonalScreenDMT
  SPimsNoJobDescription = 'No Job';
  SPimsBreakJobDescription = 'Break';
  SPimsMechanicalDown = 'Mechanical Down';
  SPimsNoMerchandize = 'No Merchandize';
  // UPersonalScreen
  SPimsMultiple = '<Multiple>';
  SPimsInButton = 'In/Out';
  SPimsOutButton = 'OUT';
  SPimsModeButtonShift = 'Shift';
  SPimsModeButtonCurrent = 'Current';
  SPimsModeButtonSince = 'Today';
  SPimsActual = 'Actual';
  SPimsTarget = 'Target';
  SPimsExtra = 'Extra';
  // DialogEnterJobCommentFRM
  SPimsPleaseEnterComment = 'Please enter a comment.';
  // HybridDMT
  SPimsWorkspotOccupied = 'Workspot occupied';
  SPimsYes = 'YES';
  SPimsNo = 'NO';
  SPimsWorkspotOccupiedQuestion = ' This workspot is already'#13' occupied by employee [%s].'#13' Should we log [%s] out and you in?';
  SPimsAtWorkDescription = 'At work';
  SPimsOccupiedDescription = 'Occupied';
  // TimeRecordingFRM
  SPimsWorkspot = 'Select Workspot';
  SPimsColumnWorkspot = 'Workspot';
  SPimsNoWorkspot = 'No Workspot defined on current workstation';
  SPimsJob 	= 'Select Job';
  SPimsColumnDescription = 'Description';
  SPimsNoMatch = 'No match for Employee + ID-Card';
  SPimsNonScanningStation = 'This is not a time recording station';
  SPimsLastScanNotClosed = 'Last scan is not closed:';
  SPimsConnectionLost = 'Error: Database connection lost. Please try again.';
  SPimsScanDone = 'Scanning processed';
  // DialogLoginFRM
  SAplErrorLogin = 'Invalid user name or password!';
  SAplNotVisible = 'This user can not open the application';
  // DialogEmpGraphFRM
  SPimsTotalProduction = 'Total production';
  SPimsEfficiencyAt = 'Efficiency at';
  SPimsProductionOutput = 'Production Output';
  // DialogSettingsFRM
  SPimsChangeSettingWarning = 'You have to restart the application before this setting will take effect.';
  // DialogSelectModeFRM
  SPimsEmployeesAll = 'All employees';
  SPimsEmployeesOnWrongWorkspot = 'On wrong workspot';
  SPimsEmployeesNotScannedIn = 'Not scanned in';
  SPimsEmployeesAbsentWithReason = 'Absent with reason';
  SPimsEmployeesAbsentWithoutReason = 'Absent without reason';
  SPimsEmployeesWithFirstAid = 'With first aid';
  SPimsEmployeesTooLate = 'Too late';
  // HomeFRM (PersonalScreen)
  SPimsLoadingScheme = 'Loading Scheme... Please wait...';
  SPimsTestingBlackBoxes = 'Testing blackboxes...';
  SPimsSaveChanges = 'Save changes?';
  SPimsSavingScheme = 'Saving Scheme... Please wait...';
  SPimsNoData = 'No Data';
  SPimsNotScannedIn = '<Not Scanned In>';
  SPimsDeleteMachineWorkspots = 'Delete machine + all related workspots ?';
  SPimsJobDoesNotExistForWorkspot = 'Error: Job does not exist for workspot!';
  SPimsSavingAndClosing = 'Saving + Closing... Please wait...';
  SPimsShort = 'Short';
  SPimsSavingLog = 'Saving Log... Please wait...';
  SPimsCounterOneNotRead = 'Counter 1 not read!';
  SPimsCounterTwoNotRead = 'Counter 2 not read!';
  SPimsBlackBoxDisabling = 'Will be disabled.';
  SPimsErrorDuringReadCounter = 'Error during read of counter!';
  SPimsBlackBoxIsDisabled = 'Is disabled.';
  SPimsNoCountersDefined = 'ERROR: No counters defined for reading blackboxes!';
  // UWorkspotEmpEff
  SPimsPcs = 'pcs.';
  SPims30Min = '30 min';
  SPims15Min = '15 min';
  SPims60Min = '60 min';
  SPimsToday = 'Today';
  // ShowChartFRM
  SPimsEfficiencySince = 'Efficiency since';
  SPimsEfficiencyShift = 'Efficiency shift';
  SPimsChartHelp = 'select time: click on graph';
  // DialogSelectModeFRM
  SPimsEmployeesHeadCount = 'Head count';
  // HomeFRM (ProductionScreen)
  SPimsLinkedJob = 'Linked job'; // 20013196
  SPimsTotalHeadCount = 'Total';
  SPimsCannotOpenHTML = 'Please install an Internet Browser on your machine.';
  SPimsSchemeNotFound = 'Warning: Scheme not found:';
  SPimsStartToWork = 'I am starting work';
  SPimsEndingWork = 'I am ending work';
  SPimsSeeSR = 'See HR';
  SPimsWorkspotOccupiedGoToSupervisor = ' This workspot is occupied.'#13' Please go to supervisor';
  SPimsWorkspotOccupiedQuestion2 = ' This workspot is already'#13' occupied by employee [%s].'#13' Should we log [%s] out and you in?';
  SPimsOK = 'OK';
  SPimsCancel = 'Cancel';

{$ELSE}
  {$IFDEF PIMSCHINESE}

  // ORASystemDMT
  SPimsKeyViolation = '��Ϊ��һ������Ψһֵ��';
  SPimsForeignKeyPost = '�˼�¼����������¼�й����� ���� '+
    '�����޷�ִ�С�';
  SPimsForeignKeyDel = '�˼�¼����������¼�й����� ���� '+
    '�����޷�ִ�С�';
  SPimsFieldRequiredFmt = '��װ���������� %s.';
  SPimsFieldRequired = '��װ����������';
  SPimsDetailsExist = '�˼�¼����������¼����ϵ�� �� '+
    '���ƶ���Щ��¼��';
  SPimsRecordLocked = '�˼�¼���ڱ������û�ʹ��';
  SPimsTBDeleteLastRecord = 'ֻ��ɾ�����ļ�¼';
  SPimsTBInsertRecord = '����4��ʱ��ģ��ɹ�ʹ��';
  SPimsStartEndDate = '��ʼ�������ڽ�������';
  SPimsStartEndTime = '��ʼʱ�����ڽ���ʱ��';
  SPimsStartEndDateTime = '��ʼ����ʱ�����ڽ�������ʱ��';
  SPimsDateinPreviousPeriod =  '�Դ��ڰ�����ʱ���ɨ���';
  SPimsEmplContractInDatePrevious =
   '�Ѵ��ڰ�����ʱ��ĺ�ͬ';
  SPimsEXCheckIllMsgClose =
    '���ȹر��ѳ��ֵ����ش�����Ϣ��';
  SPimsEXCheckIllMsgStartDate =
    '��ʼ����Ӧ�ô������һ��������Ϣ������ʱ��';
  SPimsGeneralSQLError = 'BDE ������һ��ͨ�� SQL ����';
  SPimsConfirmDelete = 'ȷ��ɾ���ղŵļ�¼?';
  // TimeRecordingDMT
  SPimsStartDay = '��ʼ���������';
  SpimsEndDay = '�������������';
  SPimsNoPrevScanningFound = 'δ�ҵ���ǰ��ɨ���';
  SPimsIDCardNotFound = 'δ�ҵ�ID�� ';
  SPimsIDCardExpired = 'ID��ʧЧ';
  SPimsEmployeeNotFound = 'δ�ҵ�Ա��';
  SPimsInactiveEmployee = '��Ա��������';
  SPimsNonScanEmployee = 'Ա������ɨ��';
  // SelectorFRM
  SPimsColumnCode = '����';
  SPimsColumnPlant = '����λ��';
  // PersonalScreenDMT
  SPimsNoJobDescription = '�޹���';
  SPimsBreakJobDescription = '��Ϣ';
  SPimsMechanicalDown = '���²���';
  SPimsNoMerchandize = '����Ʒ';
  // UPersonalScreen
  SPimsMultiple = '<����>';
  SPimsInButton = '��/��';
  SPimsOutButton = '��';
  SPimsModeButtonShift = '�ƶ�';
  SPimsModeButtonCurrent = '����';
  SPimsModeButtonSince = '����';
  SPimsActual = 'ʵ��';
  SPimsTarget = 'Ŀ��';
  SPimsExtra = '׷��';
  // DialogEnterJobCommentFRM
  SPimsPleaseEnterComment = '����������';
  // HybridDMT
  SPimsWorkspotOccupied = '��������ռ��';
  SPimsYes = '��';
  SPimsNo = '��';
  SPimsWorkspotOccupiedQuestion = '����������ѱ�'#13'occupied Ա��ռ�� [%s].'#13'�Ƿ���Ҫ���ǵǳ� [%s] �����¼?';
  SPimsAtWorkDescription = '������';
  SPimsOccupiedDescription = '��ռ��';
  // TimeRecordingFRM
  SPimsWorkspot = 'ѡ������';
  SPimsColumnWorkspot = '������';
  SPimsNoWorkspot = '�˹���վ����ȷ������';
  SPimsJob 	= 'ѡ����';
  SPimsColumnDescription = '˵��';
  SPimsNoMatch = '��ƥ��Ա�� + ID��';
  SPimsNonScanningStation = '�ⲻ��һ��ʱ���¼��վ';
  SPimsLastScanNotClosed = '�ϴε�ɨ����δ�ر�:';
  SPimsConnectionLost = '����: ���ݿ������ж�. ������.';
  SPimsScanDone = 'ɨ�账��';
  // DialogLoginFRM
  SAplErrorLogin = '��Ч�û���������!';
  SAplNotVisible = '���û����ܴ�Ӧ��';
  // DialogEmpGraphFRM
  SPimsTotalProduction = '�ܲ���';
  SPimsEfficiencyAt = '��Ч';
  SPimsProductionOutput = '����';
  // DialogSettingsFRM
  SPimsChangeSettingWarning = '������ʹ��.';
  // DialogSelectModeFRM
  SPimsEmployeesAll = '����Ա��';
  SPimsEmployeesOnWrongWorkspot = '��������';
  SPimsEmployeesNotScannedIn = 'δɨ�赽';
  SPimsEmployeesAbsentWithReason = '��ԭ��ȱϯ';
  SPimsEmployeesAbsentWithoutReason = '�޹�ȱϯ';
  SPimsEmployeesWithFirstAid = '����';
  SPimsEmployeesTooLate = '̫��';
  // HomeFRM (PersonalScreen)
  SPimsLoadingScheme = '���ط���... ���Ժ�...';
  SPimsTestingBlackBoxes = '���ڲ��Ժں���...';
  SPimsSaveChanges = '�����޸�?';
  SPimsSavingScheme = '����������... ���Ժ�...';
  SPimsNoData = '������';
  SPimsNotScannedIn = '<δɨ����>';
  SPimsDeleteMachineWorkspots = 'ɾ������ + ������ع����� ?';
  SPimsJobDoesNotExistForWorkspot = '����: �����㲻���ڹ���!';
  SPimsSavingAndClosing = '���� + �ر�... ���Ժ�...';
  SPimsShort = '����';
  SPimsSavingLog = '�����¼... ���Ժ�...';
  SPimsCounterOneNotRead = '������1δ����!';
  SPimsCounterTwoNotRead = '������2δ����!';
  SPimsBlackBoxDisabling = '���޷���ת.';
  SPimsErrorDuringReadCounter = '�������Ķ�����!';
  SPimsBlackBoxIsDisabled = '�޷���ת.';
  SPimsNoCountersDefined = '����: No counters defined for reading blackboxes!';
  // UWorkspotEmpEff
  SPimsPcs = 'pcs.';
  SPims30Min = '30 ����';
  SPims15Min = '15 ����';
  SPims60Min = '60 ����';
  SPimsToday = '����';
  // ShowChartFRM
  SPimsEfficiencySince = 'Ч����';
  SPimsEfficiencyShift = 'Ч��ת��';
  SPimsChartHelp = 'ѡ��ʱ��: ��ͼ���е��';
  // DialogSelectModeFRM
  SPimsEmployeesHeadCount = '������';
  // HomeFRM (ProductionScreen)
  SPimsLinkedJob = '��ع���'; // 20013196
  SPimsTotalHeadCount = '�ϼ�';
  SPimsSchemeNotFound = 'Warning: Scheme not found:';
  SPimsStartToWork = '��ʼ����';
  SPimsEndingWork = '��������';
  SPimsSeeSR = '֪ͨ�鳤';
  SPimsWorkspotOccupiedGoToSupervisor = ' �˹������ѱ�ռ�ã�'#13' ����ѯ�鳤��';
  SPimsWorkspotOccupiedQuestion2 = ' ����������ѱ�'#13' occupied Ա��ռ�� [%s].'#13' �Ƿ���Ҫ���ǵǳ� [%s] �����¼?';
  SPimsOK = 'OK';
  SPimsCancel = 'Cancel';

{$ELSE}
  {$IFDEF PIMSNORWEGIAN}

  // ORASystemDMT
  SPimsKeyViolation = 'Vennligst bruk en unik verdi for det første feltet.';
  SPimsForeignKeyPost = 'Denne posten har en forbindelse til andre poster. Din '+
    'forespørsel er ikke tillatt.';
  SPimsForeignKeyDel = 'Denne posten har en forbindelse til andre poster. Din '+
    'forespørsel er ikke tillatt.';
  SPimsFieldRequiredFmt = 'Vennligst fyll ut obligatorisk felt %s.';
  SPimsFieldRequired = 'Vennligst fyll ut alle obligatoriske felt.';
  SPimsDetailsExist = 'Denne posten har en forbindelse til andre poster. Vær så snill '+
    'fjern disse postene først.';
  SPimsRecordLocked = 'Denne posten brukes av en annen bruker';
  SPimsTBDeleteLastRecord = 'Bare den siste posten kan slettes';
  SPimsTBInsertRecord = 'Kun 4 timeblocks tillatt';
  SPimsStartEndDate = 'Startdato er større enn sluttdato';
  SPimsStartEndTime = 'Starttid er større enn slutttid';
  SPimsStartEndDateTime = 'Startdatoen er større enn sluttdatoen';
  SPimsDateinPreviousPeriod =  'Det er allerede en skanning som inkluderer dette tidspunktet';
  SPimsEmplContractInDatePrevious =
   'Det er allerede en kontrakt som inkluderer denne datoen';
  SPimsEXCheckIllMsgClose =
    'Det er allerede en utestående sykdomsmelding, vær så snill å lukke den først';
  SPimsEXCheckIllMsgStartDate =
    'Startdato bør være større enn sluttdatoen for sist opprettet sykdomsmelding ';
  SPimsGeneralSQLError = 'BDE rapporterte en generell SQL-feil';
  SPimsConfirmDelete = 'Ønsker du å slette gjeldende post?';
  // TimeRecordingDMT
  SPimsStartDay = 'Start av dag';
  SpimsEndDay = 'Avslutting av dag';
  SPimsNoPrevScanningFound = 'Ingen tidligere skanning funnet';
  SPimsIDCardNotFound = 'ID-kort ikke funnet';
  SPimsIDCardExpired	= 'ID Card har utgått';
  SPimsEmployeeNotFound = 'Ansatt ikke funnet';
  SPimsInactiveEmployee = 'Employee er inaktiv';
  SPimsNonScanEmployee = 'Ansatt er ikke en skanner. Skanning er ikke tillatt.';
  // SelectorFRM
  SPimsColumnCode = 'Kode';
  SPimsColumnPlant 	= 'Anlegg';
  // PersonalScreenDMT
  SPimsNoJobDescription = 'Ingen jobb';
  SPimsBreakJobDescription = 'Pause';
  SPimsMechanicalDown = 'Mekanisk brudd';
  SPimsNoMerchandize = 'Ingen produkter';
  // UPersonalScreen
  SPimsMultiple = '<Flere>';
  SPimsInButton = 'In/Ut';
  SPimsOutButton = 'UT';
  SPimsModeButtonShift = 'Skift';
  SPimsModeButtonCurrent = 'Nåværende';
  SPimsModeButtonSince = 'I Dag';
  SPimsActual = 'Faktiske';
  SPimsTarget = 'Mål';
  SPimsExtra = 'Ekstra';
  // DialogEnterJobCommentFRM
  SPimsPleaseEnterComment = 'Vennligst skriv inn en kommentar.';
  // HybridDMT
  SPimsWorkspotOccupied = 'Arbeidsstasjon opptatt';
  SPimsYes = 'JA';
  SPimsNo = 'NEI';
  SPimsWorkspotOccupiedQuestion = ' Denne arbeidsposisjon er allerede'#13' tatt av ansatt [%s]. '#13' Skal vi logge [%s] ut og deg inn?';
  SPimsAtWorkDescription = 'På Jobb';
  SPimsOccupiedDescription = 'Opptatt';
  // TimeRecordingFRM
  SPimsWorkspot = 'Velg arbeidsposisjon';
  SPimsColumnWorkspot = 'Arbeidsposisjon';
  SPimsNoWorkspot = 'Ingen arbeidsposisjon definert på gjeldende arbeidsstasjon';
  SPimsJob 	= 'Velg jobb';
  SPimsColumnDescription = 'Beskrivelse';
  SPimsNoMatch = 'Ingen treff for Ansatt + ID-kort';
  SPimsNonScanningStation = 'Dette er ikke en tidsregistreringsstasjon';
  SPimsLastScanNotClosed = 'Siste skanning er ikke stengt:';
  SPimsConnectionLost = 'Feil: Databaseforbindelse mistet. Vær så snill, prøv på nytt.';
  SPimsScanDone = 'Skanning behandlet';
  // DialogLoginFRM
  SAplErrorLogin = 'Ugyldig brukernavn eller passord!';
  SAplNotVisible = 'Denne brukeren kan ikke åpne programmet';
  // DialogEmpGraphFRM
  SPimsTotalProduction = 'Totalt produsert';
  SPimsEfficiencyAt = 'Effektivitet på';
  SPimsProductionOutput = 'Produksjon ferdig stilt';
  // DialogSettingsFRM
  SPimsChangeSettingWarning = 'Du må starte programmet på nytt før denne innstillingen trer i kraft.';
  // DialogSelectModeFRM
  SPimsEmployeesAll = 'Alle ansatte';
  SPimsEmployeesOnWrongWorkspot = 'På feil arbeidsposisjon';
  SPimsEmployeesNotScannedIn = 'Ikke Skannet inn';
  SPimsEmployeesAbsentWithReason = 'Fraværende med grunn';
  SPimsEmployeesAbsentWithoutReason = 'Fraværende uten grunn';
  SPimsEmployeesWithFirstAid = 'Industrivern';
  SPimsEmployeesTooLate = 'For sent';
  // HomeFRM (PersonalScreen)
  SPimsLoadingScheme = 'Laster inn skjema ... Vennligst vent ...';
  SPimsTestingBlackBoxes = 'Tester blackboxes ...';
  SPimsSaveChanges = 'Lage endringene?';
  SPimsSavingScheme = 'Lagrer skjema ... Vennligst vent ...';
  SPimsNoData = 'Ingen Data';
  SPimsNotScannedIn = '<Ikke skannet inn>';
  SPimsDeleteMachineWorkspots = 'Slett maskin + alle relaterte arbeidsposisjoner?';
  SPimsJobDoesNotExistForWorkspot = 'Feil: Jobb eksisterer ikke for arbeidsposisjon!';
  SPimsSavingAndClosing = 'Lagrer og Lukker ... Vennligst vent ...';
  SPimsShort = 'Kort';
  SPimsSavingLog = 'Lagrer logg ... Vennligst vent ...';
  SPimsCounterOneNotRead = 'Teller 1 ikke lest!';
  SPimsCounterTwoNotRead = 'Teller 2 ikke lest!';
  SPimsBlackBoxDisabling = 'Vil bli deaktivert.';
  SPimsErrorDuringReadCounter = 'Feil under avlesning av teller!';
  SPimsBlackBoxIsDisabled = 'Er deaktivert.';
  SPimsNoCountersDefined = 'FEIL: Ingen tellere definert for å lesing av Blackboxes!';
  // UWorkspotEmpEff
  SPimsPcs = 'pcs.';
  SPims30Min = '30 min';
  SPims15Min = '15 min';
  SPims60Min = '60 min';
  SPimsToday = 'I dag';
  // ShowChartFRM
  SPimsEfficiencySince = 'Effektivitet siden';
  SPimsEfficiencyShift = 'Effektivitet skift';
  SPimsChartHelp = 'velg tid: klikk på grafen';
  // DialogSelectModeFRM
  SPimsEmployeesHeadCount = 'Hoved antall';
  // HomeFRM (ProductionScreen)
  SPimsLinkedJob = 'Linket jobb'; // 20013196
  SPimsTotalHeadCount = 'Totalen';
  SPimsCannotOpenHTML = 'Installer en nettleser på maskinen din.';
  SPimsSchemeNotFound = 'Advarsel: Ordningen ikke funnet:';
  SPimsStartToWork = 'Jeg starter arbeidet';
  SPimsEndingWork = 'Jeg avslutter arbeidet';
  SPimsSeeSR = 'Vennligst g� til veileder';
  SPimsWorkspotOccupiedGoToSupervisor = ' Dette arbeidsstedet er opptatt.'#13' Vennligst g? til veileder';
  SPimsWorkspotOccupiedQuestion2 = ' Denne arbeidsposisjon er allerede'#13' tatt av ansatt [%s]. '#13' Skal vi logge [%s] ut og deg inn?';
  SPimsOK = 'OK';
  SPimsCancel = 'Avbryt';

{$ELSE}
// ENGLISH - DEFAULT LANGUAGE
  // ORASystemDMT
  SPimsKeyViolation = 'Please use a unique value for the first field.';
  SPimsForeignKeyPost = 'This record has a connection to other records. Your '+
    'request is denied.';
  SPimsForeignKeyDel = 'This record has a connection to other records. Your '+
    'request is denied.';
  SPimsFieldRequiredFmt = 'Please fill required field %s.';
  SPimsFieldRequired = 'Please fill all required fields.';
  SPimsDetailsExist = 'This record has a connection to other records. Please '+
    'remove those records first.';
  SPimsRecordLocked = 'This record is in use by another user';
  SPimsTBDeleteLastRecord = 'Only the last record can be deleted';
  SPimsTBInsertRecord = 'Only 4 timeblocks allowed';
  SPimsStartEndDate = 'Start date is bigger than end date';
  SPimsStartEndTime = 'Start time is bigger than end time';
  SPimsStartEndDateTime = 'Start date time is bigger than end date time';
  SPimsDateinPreviousPeriod =  'There already is a scan that includes this time';
  SPimsEmplContractInDatePrevious =
   'There already is a contract that includes this date';
  SPimsEXCheckIllMsgClose =
    'There already is one outstanding illness message, please close it before';
  SPimsEXCheckIllMsgStartDate =
    'Start date should be bigger than the end date of last created illness message ';
  SPimsGeneralSQLError = 'BDE reported a general SQL Error';
  SPimsConfirmDelete = 'Do you want to delete the current record?';
  // TimeRecordingDMT
  SPimsStartDay = 'Start of day';
  SpimsEndDay = 'End of day';
  SPimsNoPrevScanningFound = 'No previous scanning found';
  SPimsIDCardNotFound = 'ID Card not found';
  SPimsIDCardExpired	= 'ID Card has expired';
  SPimsEmployeeNotFound = 'Employee not found';
  SPimsInactiveEmployee = 'Employee is inactive';
  SPimsNonScanEmployee = 'Employee is not a scanner. Scanning is not allowed.';
  // SelectorFRM
  SPimsColumnCode = 'Code';
  SPimsColumnPlant 	= 'Plant';
  // PersonalScreenDMT
  SPimsNoJobDescription = 'No Job';
  SPimsBreakJobDescription = 'Break';
  SPimsMechanicalDown = 'Mechanical Down';
  SPimsNoMerchandize = 'No Merchandize';
  // UPersonalScreen
  SPimsMultiple = '<Multiple>';
  SPimsInButton = 'In/Out';
  SPimsOutButton = 'OUT';
  SPimsModeButtonShift = 'Shift';
  SPimsModeButtonCurrent = 'Current';
  SPimsModeButtonSince = 'Today';
  SPimsActual = 'Actual';
  SPimsTarget = 'Target';
  SPimsExtra = 'Extra';
  // DialogEnterJobCommentFRM
  SPimsPleaseEnterComment = 'Please enter a comment.';
  // HybridDMT
  SPimsWorkspotOccupied = 'Workspot occupied';
  SPimsYes = 'YES';
  SPimsNo = 'NO';
  SPimsWorkspotOccupiedQuestion = ' This workspot is already'#13' occupied by employee [%s].'#13' Should we log [%s] out and you in?';
  SPimsAtWorkDescription = 'At work';
  SPimsOccupiedDescription = 'Occupied';
  // TimeRecordingFRM
  SPimsWorkspot = 'Select Workspot';
  SPimsColumnWorkspot = 'Workspot';
  SPimsNoWorkspot = 'No Workspot defined on current workstation';
  SPimsJob 	= 'Select Job';
  SPimsColumnDescription = 'Description';
  SPimsNoMatch = 'No match for Employee + ID-Card';
  SPimsNonScanningStation = 'This is not a time recording station';
  SPimsLastScanNotClosed = 'Last scan is not closed:';
  SPimsConnectionLost = 'Error: Database connection lost. Please try again.';
  SPimsScanDone = 'Scanning processed';
  // DialogLoginFRM
  SAplErrorLogin = 'Invalid user name or password!';
  SAplNotVisible = 'This user can not open the application';
  // DialogEmpGraphFRM
  SPimsTotalProduction = 'Total production';
  SPimsEfficiencyAt = 'Efficiency at';
  SPimsProductionOutput = 'Production Output';
  // DialogSettingsFRM
  SPimsChangeSettingWarning = 'You have to restart the application before this setting will take effect.';
  // DialogSelectModeFRM
  SPimsEmployeesAll = 'All employees';
  SPimsEmployeesOnWrongWorkspot = 'On wrong workspot';
  SPimsEmployeesNotScannedIn = 'Not scanned in';
  SPimsEmployeesAbsentWithReason = 'Absent with reason';
  SPimsEmployeesAbsentWithoutReason = 'Absent without reason';
  SPimsEmployeesWithFirstAid = 'With first aid';
  SPimsEmployeesTooLate = 'Too late';
  // HomeFRM (PersonalScreen)
  SPimsLoadingScheme = 'Loading Scheme... Please wait...';
  SPimsTestingBlackBoxes = 'Testing blackboxes...';
  SPimsSaveChanges = 'Save changes?';
  SPimsSavingScheme = 'Saving Scheme... Please wait...';
  SPimsNoData = 'No Data';
  SPimsNotScannedIn = '<Not Scanned In>';
  SPimsDeleteMachineWorkspots = 'Delete machine + all related workspots ?';
  SPimsJobDoesNotExistForWorkspot = 'Error: Job does not exist for workspot!';
  SPimsSavingAndClosing = 'Saving + Closing... Please wait...';
  SPimsShort = 'Short';
  SPimsSavingLog = 'Saving Log... Please wait...';
  SPimsCounterOneNotRead = 'Counter 1 not read!';
  SPimsCounterTwoNotRead = 'Counter 2 not read!';
  SPimsBlackBoxDisabling = 'Will be disabled.';
  SPimsErrorDuringReadCounter = 'Error during read of counter!';
  SPimsBlackBoxIsDisabled = 'Is disabled.';
  SPimsNoCountersDefined = 'ERROR: No counters defined for reading blackboxes!';
  // UWorkspotEmpEff
  SPimsPcs = 'pcs.';
  SPims30Min = '30 min';
  SPims15Min = '15 min';
  SPims60Min = '60 min';
  SPimsToday = 'Today';
  // ShowChartFRM
  SPimsEfficiencySince = 'Efficiency since';
  SPimsEfficiencyShift = 'Efficiency shift';
  SPimsChartHelp = 'select time: click on graph';
  // DialogSelectModeFRM
  SPimsEmployeesHeadCount = 'Head count';
  // HomeFRM (ProductionScreen)
  SPimsLinkedJob = 'Linked job'; // 20013196
  SPimsTotalHeadCount = 'Total';
  SPimsCannotOpenHTML = 'Please install an Internet Browser on your machine.';
  SPimsSchemeNotFound = 'Warning: Scheme not found:';
  SPimsStartToWork = 'I am starting work';
  SPimsEndingWork = 'I am ending work';
  SPimsSeeSR = 'See HR';
  SPimsWorkspotOccupiedGoToSupervisor = ' This workspot is occupied.'#13' Please go to supervisor';
  SPimsWorkspotOccupiedQuestion2 = ' This workspot is already'#13' occupied by employee [%s].'#13' Should we log [%s] out and you in?';
  SPimsOK = 'OK';
  SPimsCancel = 'Cancel';

            {$ENDIF}
          {$ENDIF}
        {$ENDIF}
      {$ENDIF}
    {$ENDIF}
  {$ENDIF}
{$ENDIF}

implementation

function DisplayMessage(const Msg: string; DlgType: TMsgDlgType;
  Buttons: TMsgDlgButtons): Integer;
begin
  Result := MessageDlg(Msg, DlgType, Buttons, 0);
end;

end.
