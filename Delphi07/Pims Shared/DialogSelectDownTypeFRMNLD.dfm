inherited DialogSelectDownTypeF: TDialogSelectDownTypeF
  Left = 403
  Top = 235
  VertScrollBar.Range = 0
  BorderStyle = bsDialog
  Caption = 'Kies Type Storing'
  ClientHeight = 248
  ClientWidth = 384
  Position = poOwnerFormCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlImageBase: TPanel
    Width = 384
    inherited imgBasePims: TImage
      Left = 86
    end
  end
  inherited stbarBase: TStatusBar
    Top = 229
    Width = 384
  end
  inherited pnlInsertBase: TPanel
    Width = 384
    Height = 151
    Font.Color = clBlack
    object GroupBox1: TGroupBox
      Left = 0
      Top = 0
      Width = 384
      Height = 151
      Align = alClient
      Caption = 'Kies Type Storing'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -19
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object btnMechanicalDown: TButton
        Left = 8
        Top = 27
        Width = 367
        Height = 50
        Caption = 'Mechanische Storing'
        TabOrder = 0
        OnClick = btnMechanicalDownClick
      end
      object btnNoMerchandize: TButton
        Left = 8
        Top = 88
        Width = 368
        Height = 49
        Caption = 'Geen Artikelen'
        TabOrder = 1
        OnClick = btnNoMerchandizeClick
      end
    end
  end
  inherited pnlBottom: TPanel
    Top = 188
    Width = 384
    inherited btnOk: TBitBtn
      Left = 144
      Visible = False
    end
    inherited btnCancel: TBitBtn
      Left = 146
      Top = 3
      Height = 35
      Font.Height = -19
      ParentFont = False
      OnClick = btnCancelClick
    end
  end
  object TimerAutoClose: TTimer
    OnTimer = TimerAutoCloseTimer
    Left = 312
    Top = 53
  end
end
