unit DialogBaseFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ActnList, dxBarDBNav, dxBar, ComCtrls, ExtCtrls, StdCtrls,
  Buttons, TopPimsLogoFRM;

type
  TDialogBaseF = class(TTopPimsLogoF)
    pnlBottom: TPanel;
    btnOk: TBitBtn;
    btnCancel: TBitBtn;
  private
    { Private declarations }
  public

  end;
  
  TDialogBaseClassF= Class of TDialogBaseF;
var
  DialogBaseF: TDialogBaseF;

implementation

{$IFDEF PIMSDUTCH}
{$R *NLD.DFM}
{$ELSE}
  {$IFDEF PIMSGERMAN}
  {$R *DEU.DFM}
  {$ELSE}
    {$IFDEF PIMSFRENCH}
    {$R *FRA.DFM}
    {$ELSE}
    {$R *.DFM}
    {$ENDIF}
  {$ENDIF}
{$ENDIF}

end.
