unit DialogLoginDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  OracleData, Oracle, DB;

type

  TDialogLoginDM = class(TDataModule)
    odsPimsUser: TOracleDataSet;
    oqCheckOpenApl: TOracleQuery;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
    FApl_Menu_Login: Integer;
    FApl_Regroot_Path_Login: String;
   // FApl_Description_Login_Pims: String;
  public
    { Public declarations }
    property Apl_Menu_Login: Integer read FApl_Menu_Login write FApl_Menu_Login;
    property Apl_Regroot_Path_Login: String read FApl_Regroot_Path_Login
      write FApl_Regroot_Path_Login;

(* only used in Pims.exe not in other programs so not converted to oracle yet
    function GetMenuNumberOfApplication: Integer;
    function GetMenuNumberOfMenuOption(const PimsMenuOption: Integer): Integer;
    function GetMenuTreeOfParentMenu(Parent_Menu: Integer;
      Visible_YN, Expand_YN: String): TQuery;
    function CheckVisibleMenuOption(const PimsMenuOption: Integer): Boolean;
*)
  end;

var
  DialogLoginDM: TDialogLoginDM;

implementation

uses
  ULoginConst, ORASystemDMT, DialogLoginFRM, EncryptIt, UPimsConst;

{$R *.DFM}

procedure TDialogLoginDM.DataModuleCreate(Sender: TObject);
begin
  if (Application.Title = 'ADMINISTRATION - TOOL') or
    (Application.Title = 'ORCL - ADMINISTRATION - TOOL') then
  begin
    Apl_Menu_Login := APL_MENU_LOGIN_ADM;
    Apl_RegRoot_Path_Login := APL_REGROOT_PATH_LOGIN_ADM;
  end;
  if (Application.Title = 'PIMS - UNIT MAINTENANCE') or
    (Application.Title = 'ORCL - PIMS - UNIT MAINTENANCE') then
  begin
    Apl_Menu_Login := APL_MENU_LOGIN_PIMS;
    Apl_RegRoot_Path_Login := APL_REGROOT_PATH_LOGIN_PIMS;
  end;
  if (Application.Title = 'PIMS - PRODUCTION SCREEN') or
    (Application.Title = 'ORCL - PIMS - PRODUCTION SCREEN') or
    (Application.Title = 'ORCL - PIMS - PERSONAL SCREEN') then
  begin
    Apl_Menu_Login := APL_MENU_LOGIN_PRODSCREEN;
    Apl_RegRoot_Path_Login := APL_REGROOT_PATH_LOGIN_PRODSCREEN;
  end;
{ they do exist all  if TableUser.Exists then
    TableUser.Open;
  if TablePimsGroup.Exists then
    TablePIMSGroup.Open;
  if TableGroupMenu.Exists then
    TableGroupMenu.Open
  else
    Exit;  }
  // set properties per applications
end;

procedure TDialogLoginDM.DataModuleDestroy(Sender: TObject);
begin
  oqCheckOpenApl.Close;
end;
(*
function TDialogLoginDM.GetMenuNumberOfApplication: Integer;
begin
  Result := Apl_Menu_Login;
end;

function TDialogLoginDM.GetMenuTreeOfParentMenu(Parent_Menu: Integer;
  Visible_YN, Expand_YN: String): TQuery;
var
  SelectStr: String;
  WorkQuery: TQuery;
begin
  WorkQuery := TQuery.Create(nil);
  WorkQuery.SessionName := ORASystemDM.SessionPims.Name;
  WorkQuery.DataBaseName := ORASystemDM.Pims.Name;
  SelectStr :=
    'SELECT MENU_NUMBER, SEQ_NUMBER, VISIBLE_YN, EDIT_YN, EXPAND_YN, ' +
    ' DESCRIPTION FROM PIMSMENUGROUP WHERE GROUP_NAME = ''' +
    DoubleQuote(DialogLoginF.LoginGroup) + ''' AND ' +
    ' PARENT_MENU_NUMBER = ' + IntToStr(Parent_Menu) + ' AND EXPAND_YN = ''' +
    Expand_YN + '''';
  if Visible_YN = 'Y' then
    SelectStr :=  SelectStr  + ' AND VISIBLE_YN = ''' + Visible_YN + '''';
  SelectStr :=  SelectStr + ' ORDER BY SEQ_NUMBER ';

  WorkQuery.Close;
  WorkQuery.Sql.Clear;
  WorkQuery.Sql.Add(SelectStr);
  WorkQuery.Open;
  Result := WorkQuery;
end;

function TDialogLoginDM.GetMenuNumberOfMenuOption(
  const PimsMenuOption: Integer): Integer;
var
  Parent_Level: Integer;
  WorkQuery: TQuery;
begin
  Result := -1;
  Parent_Level := GetMenuNumberOfApplication;
  WorkQuery := GetMenuTreeOfParentMenu(Parent_Level, '', 'Y');

  WorkQuery.First;
  WorkQuery.MoveBy(PimsMenuOption);
  if not WorkQuery.Eof then
    Result := WorkQuery.FieldByName('MENU_NUMBER').AsInteger;
  WorkQuery.Free;
end;

function TDialogLoginDM.CheckVisibleMenuOption(
  const PimsMenuOption: Integer): Boolean;
begin
  Result := not GetMenuTreeOfParentMenu(
    GetMenuNumberOfMenuOption(PimsMenuOption), 'Y', 'Y').IsEmpty;
end;
*)
end.
