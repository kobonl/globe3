(*
  MRA:JUN-2010. RV063.4. Order 550478. Personal Screen.
  - Addition of Machine/Workspot + Time recording.
  MRA:16-MAR-2012 20012858. New ProductionScreen combined with datacol.
  - Based on ProductionScreen a new project is made named: PersonalScreen.
  - All that is not needed is left out here or disabled.
  MRA:3-JUL-2012 20012858.80
  - Addition of routines to test blackboxes.
  MRA:5-JUL-2012 20012858.90
  - Because 1 field was added, oqInsertJob had to be changed!
    - Extra field: EMPS_SCAN_IN_YN  (default: 'N').
  MRA:17-JUL-2012 20012858.160.
  - It should be possible to only read employees who scanned in.
    For this ReadAll should have an option to only look for employees (scans).
    - Handling:
      - First empty the existing list of employees.
      - After that read the scans again and update the employee-list.
  MRA:3-AUG-2012 20013478. Related to this order.
  - For this order a field was added to JOBCODE-table:
    Field: IGNOREQUANTITIESINREPORTS_YN
    This field must be added when jobs are autom. added like the BREAK-job.
  - Changes made to oqInsertJob.
  MRA:28-AUG-2012 TODO 21191
  - Sometimes it stores wrong quantities.
    - Why is not clear yet.
    - To solve it using a work-around:
      - When a quantity is higher than 10000, then do not store this value as
        PQ-record.
      - Log some variables during this.
      - The 'adam-test-tool' must not be used during this, because this can
        give read problems when 2 apps are reading the same blackboxes.
  MRA:10-SEP-2012 TODO-21191
  - Use check-digits for reading blackboxes.
  - An extra field 'Checkdigits' must be added for this purpose. It can
    be left empty. It must be in the range from '10' till
    '99' (and always 2 positions).
  MRA:11-SEP-2012 Bugfix related to 20013516. It did not read the 'Down'-jobs.
  - Reasons: It only read trs-records for 'show_at_productionscreen_yn'.
    This is wrong, it should read all trs-records here.
  MRA:8-OCT-2012 20013489 Overnight-shift-system.
  - Store SHIFT_DATE in ProdutionQuantity-record.
  MRA:22-NOV-2012 200134549 Related to this order.
  - When an INSERT-statement is used, always use the structure:
    - INSERT xxx (field1, field2, ...) values (field1, field2, ...)
    - And be sure the fieldnames are all added, except for:
      - Fields that will be added later do not have to be included,
        when they are not-null and have a default value set on db-level.
  MRA:4-JAN-2013 TD-21738
  - Refresh emps gives wrong target for today.
  MRA:21-NOV-2013 20013196
  - Multiple counters on one workspot
  MRA:11-APR-2014 TD-24736
  - Troubleshoot DFD about date-format! Because of this it did NOT store
    ProductionQuantity-records, only CounterValuePS-records.
  MRA:17-APR-2014 TD-24736 Rework
  - BCH had the same problem, but there the change for DFD did NOT work and
    it did not store quantities at all as a result.
  - To solve this:
    - Do not use the DFD-solution (see above).
    - Use more try/except and error-logging to see what goes wrong before
      updating the quantities to database.
    - A check must be done if the Date/Time-format that is shown in Personal
      Screen is the same as shown at right/bottom part on Desktop (by Windows).
      If that differs, then the Date/Time-format is not taken over correctly
      from Windows-system by Personal Screen. This must be fixed first.
      For Windows 7/8 this can be a problem, see on Internet at:
      http://wiki.timemaker.org/index.php/Problem_with_Date_format
  MRA:23-APR-2014 TD-24736.60 Rework
  - Make solution to change format of date/time optional based on
    setting named DateTimeFormatPatch. Default False.
  MRA:6-MAY-2014 TD-24819
  - Personal Screen and Comparison Jobs must use NOJOB for initial value
  MRA:7-MAY-2014 TD-24814
  - Since and Shift Time on Personal Screen is not from the start of the
    day scan.
  - REASON: Query oqTRS gave double records, because a link\ with
    EMPLOYEEPLANNING was wrong (workspot-link). Also: When planning on
    department instead of workspot it probably also goes wrong!
  - NOTE: Because EMPLOYEEPLANNING-info is not used at all, it has been removed
    from qryTRS. The original query is saved in oqTRS_OLD.
    Also the link with WORKSPOTPEREMPLOYEE is removed from qryTRS, because
    level is not used at all.
  MRA:12-MAY-2014 SO-20015330
  - Automatic reset of counters.
  MRA:12-MAY-2014 SO-20015331
  - Ignore Interface Codes handling
  MRA:26-MAY-2014 TD-25016
  - Personal Screen + link jobs combined with ignore interface codes
    gives problems
  - IMPORTANT: cdsQtyToStore can have more than 1 plant/workspot/job-record
    within 1 5-minute-period, so also use STARTTIME when looking for a record.
  - Use a second cdsQtyToStoreWork for while-loops and the cdsQtyToStore when
    something must be updated during the loop.
  MRA:30-MAY-2014 20015178
  - Measure Performance without employees
  MRA:12-JUN-2014 20014450
  - Machine hours registration
  MRA:17-JUN-2014 20015178.70 Rework
  - Related to this order:
  - When using 'Refresh employees'-option (settings) then it resets the
    efficiency-bar.
  - When the fake-employee was scanned out (no open scan anymore) then
    it is not possible to open a new job again: Look only for open scans!
  MRA:18-AUG-2014 20015178.90 Rework
  - When there is no job yet and the first job is selected, it is not always
    created.
  - Cause: It looked till 'Now' for scans, but when a scan is created at
           e.g. 50 seconds after, then it will find nothing because the scan
           was rounded to the upper minute, and the current time (Now)
           is then BEFORE the scan-date.
           Example: Now=13:40:50 DateIn=13:41:00. This will not be found!
   - Solution: Add 30 seconds to current time and then round it, use
               that as 'till' time.
  MRA:8-SEP-2014 20013476
  - Make current variable, based on job-field SAMPLE_TIME_MINS.
  MRA:6-OCT-2014 20015376
  - Link Job Code Workspot - Show actual quantities
  MRA:23-DEC-2014 SO-20016016
  - Make it possible on workspot-level to read data (refresh) from database
    instead of from blackboxes.
  MRA:23-JAN-2015 SO-20016016.70 Rework
  - Make it possible on workspot-level to read data (refresh) from database
    instead of from blackboxes.
  - Rework: It did not show the quantity correct for Shift, only for
    Today and Current.
  MRA:10-MAR-2015 SO-20015376.60
  - Link Job Code Work spot - Show actual quantities
  - It did not refresh the employee-info for a workspot that functions as a
    link-job.
  MRA:16-MAR-2015 20015346
  - Store scans in seconds
  - Cut off time and breaks handling
  - Creation-date and Mutation-date handling for scans
  - Addition of 2 Pims-Settings:
    - TimerecordingInSecs
    - IgnoreBreaks
  MRA:1-MAY-2015 20014450.50 Part 2
  - Real time efficiency
  - Bugfix for theo-values. They were not calculated correctly because
    it first read scans and after that quantities and during read of
    quantities it did not calculate the theo-values.
  MRA:8-JUN-2015 ABS-8116
  - Do not show/log error when workspot is defined as 'receive qty from
    external app'.
  MRA:13-JUL-2015 PIM-12
  - Use a global setting for EfficiencyPeriodSinceTime that is assigned
    during reading of all data and based on plant-level of the first
    DrawObject that is read.
  - Be sure to recreate the empl-list when 'Refresh Employees' is used,
    or it will not show the employee anymore after 1 refresh.
  MRA:5-OCT-2015 PIM-90
  - Adapt ABSSolute Script Quantities to PIMS for Personal Screen
  - Added pimssetting named ExternQtyFromPQ (PIMSSETTING.EXTERN_QTY_FROM_PQ_YN)
  - Related to this order: Use CurrentTimeWindow instead of const!
  MRA:13-OCT-2015 PIM-12
  - Related to this order:
    - Keep track if 'ReadPlantInfo' has been done already with boolean.
    - Only look for scans older than max. 6 hours before?
      NO: When someone works for 8 hours and only made 1 scan at the start,
      we will not find it when we only look 6 hours before!
      Look for 'ReadDateTo - 0.5' not for 'ReadDateFrom - 0.5'!!!
  MRA:16-OCT-2015 PIM-91
  - Add time for current when using 'receive data from external application'.
  MRA:23-OCT-2015 PIM-12.2
  - Bugfix for linked jobs.
  MRA:30-DEC-2015 PIM-114
  - Determine shift-date before PQ is stored.
  MRA:24-FEB-2016 PIM-12
  - Bugfix: The real-time-eff-table (workspoteffperminute) was not filled
    correct! It resulted in too high qty-values!
    To solve it, it is now done in a similar way as WriteToDB, by using the
    qty-to-store-list to store the quantities.
  MRA:7-MAR-2016 PIM-111
  - Bugfix: Also read Down-jobs for job-list, to prevent it does show
    not-active when last job was a down-job and PS was restarted.
  MRA:17-MAR-2016 PIM-12.3
  - Bugfix for linked-jobs.
  - It cleared the linked-jobs-list when 'refresh employees' was used.
  - Only clear the linked-jobs-list when it is really needed.
  MRA:18-MAR-2016 PIM-155
  - Fake Employee-handling rework:
  - It gave an error when first scan was made, reason: When no scan was made
    yet for the day, it tried to add this twice.
  MRA:30-MAR-2016 PIM-159
  - When using a workspot for comparison it shows a double value when
    employee scans out from the workspot that links to this comparison worskpot.
  MRA:6-APR-2016 PIM-151
  - Show ghost counts in Personal/Production Screen.
  MRA:6-MAY-2016 PIM-151 Related to this issue
  - For oqPQ:
  - It checked on automatic_quantity <> 0
  - But when a PQ is added via Pims itself then quantity is filled
  - So check on quantity <> 0.
  MRA:22-JUN-2016 PIM-195
  - Problem with reset of efficiency for Shift and Today.
  - When Refresh Employee-option is used it resets the efficiency to 0 when
    using Shift/Today-mode.
*)

unit PersonalScreenDMT;

interface

uses
  SysUtils, Classes, ORASystemDMT, Dialogs, Oracle, DB, DBClient, DateUtils,
  Variants, UPersonalScreen, UScannedIDCard, Windows, UProductionScreenDefs,
  OracleData;

const
  CJ_CHANGE_JOB=0;
  CJ_END_OF_DAY=1;
  CJ_DOWN=2;
  CJ_CONTINUE=3;
  CJ_BREAK=4;
  CJ_LUNCH=5;

type
  PTMachineWorkspot = ^TMachineWorkspot;
  TMachineWorkspot = record
    APlantCode: String;
    AMachineCode: String;
    AWorkspotCode: String;
    ATag: Integer;
  end;

type
  TPersonalScreenDM = class(TDataModule)
    oqCopyThis: TOracleQuery;
    oqJobs: TOracleQuery;
    oqTRS: TOracleQuery;
    oqPQ: TOracleQuery;
    oqBlackBox: TOracleQuery;
    oqCV: TOracleQuery;
    cdsQtyToStore: TClientDataSet;
    cdsQtyToStorePLANT_CODE: TStringField;
    cdsQtyToStoreWORKSPOT_CODE: TStringField;
    cdsQtyToStoreJOB_CODE: TStringField;
    cdsQtyToStoreSHIFT_NUMBER: TIntegerField;
    cdsQtyToStoreQUANTITY: TFloatField;
    cdsQtyToStoreCOUNTER_VALUE: TFloatField;
    cdsQtyToStoreSTARTTIME: TDateTimeField;
    cdsQtyToStoreENDTIME: TDateTimeField;
    oqMergePQ: TOracleQuery;
    oqDeleteCV: TOracleQuery;
    oqInsertCV: TOracleQuery;
    oqLastPQ: TOracleQuery;
    oqCheckJob: TOracleQuery;
    oqInsertJob: TOracleQuery;
    oqMergeAddPQ: TOracleQuery;
    oqCheckPQ: TOracleQuery;
    oqCheckCompareJobsXXX: TOracleQuery;
    oqFirstCompareJobXXX: TOracleQuery;
    oqScan: TOracleQuery;
    oqInsertScan: TOracleQuery;
    oqScanFakeEmp: TOracleQuery;
    oqInsertFakeEmp: TOracleQuery;
    oqFakeEmp: TOracleQuery;
    oqCheckCompareJob: TOracleQuery;
    cdsQtyToStoreSHIFT_DATE: TDateTimeField;
    oqIsLinkJob: TOracleQuery;
    oqCompareJobs: TOracleQuery;
    oqTRS_OLD: TOracleQuery;
    oqIgnoreInterfaceCodes: TOracleQuery;
    cdsQtyToStoreWork: TClientDataSet;
    cdsQtyToStoreWorkSTARTTIME: TDateTimeField;
    cdsQtyToStoreWorkPLANT_CODE: TStringField;
    cdsQtyToStoreWorkWORKSPOT_CODE: TStringField;
    cdsQtyToStoreWorkJOB_CODE: TStringField;
    cdsQtyToStoreWorkSHIFT_NUMBER: TIntegerField;
    cdsQtyToStoreWorkQUANTITY: TFloatField;
    cdsQtyToStoreWorkCOUNTER_VALUE: TFloatField;
    cdsQtyToStoreWorkENDTIME: TDateTimeField;
    cdsQtyToStoreWorkSHIFT_DATE: TDateTimeField;
    cdsQtyToStoreWorkUPDATE_YN: TStringField;
    oqWorkspot: TOracleQuery;
    oqScanXXX: TOracleQuery;
    oqMergeMachHrs: TOracleQuery;
    oqMachine: TOracleQuery;
    oqMergeMachDownTime: TOracleQuery;
    oqPlant: TOracleQuery;
    odsCopyThis: TOracleDataSet;
    oqWSEffPerMinute: TOracleQuery;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
    FDateFrom: TDateTime;
    FDateTo: TDateTime;
    FDateTimeFormatPatch: Boolean;
    FPIMSDatacolTimeInterval: Integer;
    FPIMSMultiplier: Integer;
    FMachHrsMaxIdleTime: Integer;
    FReadPlantInfoDone: Boolean;
    FReceiveQtyFromExternalAppInitDone: Boolean;
    procedure ReadTimeRegScanning(var ADrawObject: PDrawObject);
    function DetermineShiftDate(APSList: TList;
      APlantCode, AWorkspotCode: String;
      AShiftNumber: Integer; AStartDate, AEndDate: TDateTime): TDateTime;
    procedure UpdateProductionQuantityTable(APSList: TList); // PIM-114
    procedure UpdateWorkspotEffPerMinuteTable(APSList: TList); // PIM-12
  public
    { Public declarations }
    MachineWorkspotList: TList;
    procedure ClearMachineWorkspotList;
    function MachineWorkspotExists(APlantCode, AMachineCode,
      AWorkspotCode: String): Boolean;
    procedure AddMachineWorkspotList(APlantCode, AMachineCode,
      AWorkspotCode: String; ATag: Integer);
    procedure DataInit(var APSList: TList; AAll: Boolean=True);
    procedure DataReadAll(var APSList: TList; var AReadCounterList: TList;
      AOnlyUpdateEmployeeList: Boolean=False;
      ARecreateReadCounterList: Boolean=False;
      AOneDrawObject: PDrawObject=nil;
      AReadAllForOneDrawObject: Boolean=False);
    procedure RefreshEmployeeList(var APSList: TList;
      var AReadCounterList: TList);
    procedure CreateQtyToStoreTempList(APSList: TList);
    procedure UpdateLinkJobs(APSList: TList);
    procedure UpdateCounterValueTable(APSList: TList);
    procedure WriteQtyToStoreList(APSList: TList; ALogPath: String);
    procedure InitQtyToStoreList(APSList: TList);
    procedure ActionWriteToFile(APSList: TList; ALogPath: String);
    procedure ActionWriteToDB(APSList: TList; ALogPath: String);
    function ActionUpdateAfterScan(APSList: TList;
      ALastIDCard, ANextIDCard: TScannedIDCard): Boolean;
    procedure ActionAddMissingJobForWorkspot(APlantCode, AWorkspotCode,
      AJobCode, AJobDescription: String);
{    function MergePQ(AStartDate: TDateTime;
      APlantCode, AWorkspotCode, AJobCode: String;
      AShiftNumber: Integer;
      AEndDate: TDateTime;
      AQuantity, ACounterValue: Double): Boolean; }
    function MergeCheckPQ(AStartDate: TDateTime;
      APlantCode, AWorkspotCode, AJobCode: String;
      AShiftNumber: Integer;
      AEndDate: TDateTime;
      AQuantity, ACounterValue: Double;
      AShiftDate: TDateTime): Boolean;
    function DetectCompareJob(APlantCode, AWorkspotCode: String;
      var AJobCode, AJobDescription: String;
      var AEmpsScanIn: Boolean): Boolean;
    procedure ReassignCompareJob(var APSList: TList);
    procedure ActionHandleLastQtyToStoreFile(ALogPath: String);
    procedure DeleteQtyToStoreListFile(ALogPath: String);
    procedure ActionDeleteFile(AFilename: String);
    procedure IgnoreInterfaceCodeHandling(APSList: TList);
    procedure DecreaseQtyByInterfaceCode(APSList: TList;
      AStartTime: TDateTime;
      AInterfaceCode: String; AQty: Double);
    procedure cdsQtyToStoreUpdate(AStartTime: TDateTime;
      APlantCode, AWorkspotCode, AJobCode: String;
      AQty: Double; ADecrease: Boolean; AInit: Boolean);
    procedure cdsQtyToStoreUpdateLinkJob(
      APlantCode, AWorkspotCode, AJobCode: String;
      AQty: Double; ADecrease: Boolean; AInit: Boolean);
    procedure cdsQtyToStoreCopyToWork;
    procedure CheckCreateFakeEmployeeInDB(APlantCode, AWorkspotCode: String;
      AFakeEmployeeNumber: Integer);
{    function CheckAddScanFakeEmployeeInDB(APlantCode, AWorkspotCode,
      AJobCode: String; ADatetimeIn: TDateTime; AFakeEmployeeNumber: Integer): Boolean; }
    function FakeEmployeeNumber(APlantCode, AWorkspotCode: String): Integer;
    procedure MachineHoursMerge(APlantCode, AMachineCode: String;
      AStartTimestamp: TDateTime; AShiftDate: TDateTime;
      AShiftNumber: Integer);
    procedure MachineDownTimeMerge(APlantCode, AMachineCode: String;
      AStartTimestamp: TDateTime; AShiftDate: TDateTime;
      AShiftNumber: Integer;
      AReasonCode: String);
    function MachineDepartment(APlantCode, AMachineCode: String): String;
    procedure UpdateMachineHours(APSList: TList);
    procedure UpdateMachineDownTime(var ADrawObject: PDrawObject);
    procedure UpdateMachineDownTimeHours(var APSList: TList);
    property ReadDateFrom: TDateTime read FDateFrom write FDateFrom;
    property ReadDateTo: TDateTime read FDateTo write FDateTo;
    property DateTimeFormatPatch: Boolean read FDateTimeFormatPatch
      write FDateTimeFormatPatch;
    property PIMSDatacolTimeInterval: Integer read FPIMSDatacolTimeInterval
      write FPIMSDatacolTimeInterval; // 20015178
    property PIMSMultiplier: Integer read FPIMSMultiplier write FPIMSMultiplier; // 20015178
    property MachHrsMaxIdleTime: Integer read FMachHrsMaxIdleTime
      write FMachHrsMaxIdleTime; // 20014450
    property ReadPlantInfoDone: Boolean read FReadPlantInfoDone write FReadPlantInfoDone;
    property ReceiveQtyFromExternalAppInitDone: Boolean
      read FReceiveQtyFromExternalAppInitDone write FReceiveQtyFromExternalAppInitDone;
  end;

function DateCompare(Item1, Item2: Pointer): Integer;

var
  PersonalScreenDM: TPersonalScreenDM;

implementation

{$R *.dfm}

uses
  CalculateTotalHoursDMT, UPimsConst, UGlobalFunctions, UPimsMessageRes,
  RealTimeEffDMT;

procedure TPersonalScreenDM.DataModuleCreate(Sender: TObject);
begin
  ReadPlantInfoDone := False; // PIM-12
  ReceiveQtyFromExternalAppInitDone := False; // PIM-12
  MachineWorkspotList := TList.Create;
  cdsQtyToStore.CreateDataSet;
  cdsQtyToStoreWork.CreateDataSet;
end;

procedure TPersonalScreenDM.ClearMachineWorkspotList;
var
  I: Integer;
  AMachineWorkspot: PTMachineWorkspot;
begin
  for I := MachineWorkspotList.Count - 1 downto 0 do
  begin
    AMachineWorkspot := MachineWorkspotList.Items[I];
    MachineWorkspotList.Remove(AMachineWorkspot);
    Dispose(AMachineWorkspot);
  end;
end;

function TPersonalScreenDM.MachineWorkspotExists(APlantCode,
  AMachineCode, AWorkspotCode: String): Boolean;
var
  I: Integer;
  AMachineWorkspot: PTMachineWorkspot;
begin
  Result := False;
  for I := 0 to MachineWorkspotList.Count - 1 do
  begin
    AMachineWorkspot := MachineWorkspotList.Items[I];
    if (AMachineWorkspot.APlantCode = APlantCode) and
      (AMachineWorkspot.AMachineCode = AMachineCode) and
      (AMachineWorkspot.AWorkspotCode = AWorkspotCode) then
    begin
      Result := True;
      Break;
    end;
  end;
end;

procedure TPersonalScreenDM.AddMachineWorkspotList(APlantCode,
  AMachineCode, AWorkspotCode: String; ATag: Integer);
var
  AMachineWorkspot: PTMachineWorkspot;
begin
  new(AMachineWorkspot);
  AMachineWorkspot.APlantCode := APlantCode;
  AMachineWorkspot.AMachineCode := AMachineCode;
  AMachineWorkspot.AWorkspotCode := AWorkspotCode;
  AMachineWorkspot.ATag := ATag;
  MachineWorkspotList.Add(AMachineWorkspot);
end;

procedure TPersonalScreenDM.DataModuleDestroy(Sender: TObject);
begin
  ClearMachineWorkspotList;
  MachineWorkspotList.Free;
  cdsQtyToStore.EmptyDataSet;
  cdsQtyToStoreWork.EmptyDataSet;
end;

{ New Personal Screen }

// Data Initialisation, this is done when scheme is already read.
// TODO 21191 Handle ReadCounterList somewhere else.
procedure TPersonalScreenDM.DataInit(var APSList: TList; AAll: Boolean=True);
var
  I: Integer;
  ADrawObject: PDrawObject;
begin
  // Init all workspots/jobs and employees
  for I := 0 to APSList.Count - 1 do
  begin
    ADrawObject := APSList.Items[I];
    ADrawObject.Init(AAll);
  end;
end; // DataInit

// Fill the employee-list with 1 or more employee, always for the whole day.
// The last/current scan must be shown at the top.
// REMARK: This list must be filled with the employees (1 record for each
//         employee). Because there can be multiple scans for 1 day,
//         it must calculate the time for 'actual-time-day' and
//         'actual-time-shift' when this list is filled.
procedure TPersonalScreenDM.ReadTimeRegScanning(var ADrawObject: PDrawObject);
begin
  try
    // Employees / Levels / Teams / Scans / Planning
    // First scan is newest record.
    // Bugfix related to 20013516. It did not read the 'Down'-jobs.
    //   Reasons: It only read trs-records for 'show_at_productionscreen_yn'.
    //   This is wrong, it should read all trs-records here.
    // TD-24814 IMPORTANT:
    // - This query inks TIMEREGSCANNING (TR) with WORKSPOTPEREMPLOYEE (WE)
    //   and EMPLOYEEPLANNING (EP)!
    //   But it missed the link with workspots for TR + EP. This resulted in
    //   double records, giving double minutes.
    //   ALSO: When planning on departments is used, it probable also
    //   goes wrong!
    //   NOTE: The part about EmployeePlanning has been removed from this
    //         oqTRS-query because it was not used at all.
    //         Also the link with WORKSPOTPEREMPLOYEE is removed, because
    //         level is not used at all.
    // PIM-12 Related to this order: Why look 12 hours before???
    //                               Only look since 'since-date'
    //        We still have to look 12 hours before to prevent it will find
    //        too many scans between 00:00 and ... (start-of-day).
    // PIM-12 Look for 'ReadDateTo - 0.5' not for 'ReadDateFrom - 0.5'!!!
    with oqTRS do
    begin
      ClearVariables;
      SetVariable('PLANT_CODE',      ADrawObject.APlantCode);
      SetVariable('WORKSPOT_CODE',   ADrawObject.AWorkspotCode);
      SetVariable('HALFADAY_BEFORE', ReadDateTo - 0.5);
      SetVariable('DATEFROM',        ReadDateFrom);
      SetVariable('DATETO',          ReadDateTo);
{$IFDEF DEBUG3}
{
WDebugLog('ReadTimeRegScanning:' +
  ' PlantCode=' + ADrawObject.APlantCode +
  ' WorkspotCode=' + ADrawObject.AWorkspotCode +
  ' HalfADayBefore=' + DateTimeToStr(ReadDateFrom - 0.25);
  ' DateFrom=' + DateTimeToStr(ReadDateFrom) +
  ' DateTo=' + DateTimeToStr(ReadDateTo));
}
{$ENDIF}
      Execute;
      while not Eof do
      begin
      // 20013379 No scans for compare job
(*
        // Compare Jobs
        SkipEntry := False;
        if (ADrawObject.AWorkspotTimeRec.ACompareJobCode <> '') and
          (ADrawObject.AWorkspotTimeRec.ACompareJobCode =
          FieldAsString('JOB_CODE')) then
        begin
          // Scan is too old! Just skip it
          if Now - FieldAsDate('DATETIME_IN') > 0.5 then
            // Scan is from yesterday!
            if Trunc(FieldAsDate('DATETIME_IN')) < Trunc(Now) then
              SkipEntry := True;
       end;
       if not SkipEntry then
*)
          ADrawObject.AddUpdateEmployeeList(
            FieldAsInteger('EMPLOYEE_NUMBER'),
            FieldAsString('DESCRIPTION'),
            FieldAsInteger('SHIFT_NUMBER'),
            FieldAsString('TEAM_CODE'),
            '', // FieldAsString('EMPLOYEE_LEVEL'), // TD-24814 Level is not used!
            FieldAsDate('DATETIME_IN'),
            FieldAsDate('DATETIME_OUT'),
            FieldAsString('PROCESSED_YN'),
            FieldAsString('JOB_CODE'),
            FieldAsString('JDESCRIPTION'),
            True
            );
        Next;
      end; // while
    end; // with
  except
    // todo: trap errors here
  end;
end; // ReadTimeRegScanning

// Read all workspots, jobs, employees, scans and quantities
// For all workspots in the scheme.
procedure TPersonalScreenDM.DataReadAll(var APSList: TList;
  var AReadCounterList: TList;
  AOnlyUpdateEmployeeList: Boolean=False;
  ARecreateReadCounterList: Boolean=False;
  AOneDrawObject: PDrawObject=nil;
  AReadAllForOneDrawObject: Boolean=False);
var
  I: Integer;
  ADrawObject: PDrawObject;
  AJobCodeRec: PJobCodeRec;
  // 20015331
  procedure ReadIgnoreInterfaceCodes;
  var
    AIgnoreInterfaceCodeRec: PIgnoreInterfaceCodeRec;
  begin
    InitIgnoreInterfaceCodeList(False);
    with oqIgnoreInterfaceCodes do
    begin
      Execute;
      while not Eof do
      begin
        // TD-25016 Only add items when the workspots are used in this scheme!
        if Assigned(FindDrawObject(APSList, FieldAsString('PLANT_CODE'),
          FieldAsString('WORKSPOT_CODE'))) then
        begin
          new(AIgnoreInterfaceCodeRec);
          AIgnoreInterfaceCodeRec.PlantCode := FieldAsString('PLANT_CODE');
          AIgnoreInterfaceCodeRec.WorkspotCode := FieldAsString('WORKSPOT_CODE');
          AIgnoreInterfaceCodeRec.JobCode := FieldAsString('JOB_CODE');
          AIgnoreInterfaceCodeRec.InterfaceCode :=
            FieldAsString('INTERFACE_CODE');
          IgnoreInterfaceCodeList.Add(AIgnoreInterfaceCodeRec);
        end;
        Next;
      end; // while
    end; // with
  end; // ReadIgnoreInterfaceCodes
  procedure ReadPlantInfo; // 20014450.50
  begin
    if ReadPlantInfoDone then // Only do this once! // PIM-12
      Exit;
    try
      with oqPlant do
      begin
        ClearVariables;
        SetVariable('PLANT_CODE', ADrawObject.APlantCode);
        Execute;
        if not Eof then
        begin
          ADrawObject.ACutOffTimeShiftDate := FieldAsDate('CUTOFFTIMESHIFTDATE');
          if ADrawObject.ARealTimeIntervalMinutes = -1 then
            ADrawObject.ARealTimeIntervalMinutes := FieldAsInteger('REALTIMEINTERVAL');
          if GlobalEfficiencyPeriodSinceTime = NullDate then
          begin
            GlobalEfficiencyPeriodSinceTime := ADrawObject.ACutOffTimeShiftDate;
            // Also change ReadDateFrom, that is used during ReadDateAll!
            ReadDateFrom :=
              Trunc(ReadDateFrom) + Frac(GlobalEfficiencyPeriodSinceTime);
          end;
        end;
      end;
    except
      // trap errors here
    end;
  end; // ReadPlantInfo
  procedure ReadJobs;
  var
    ALinkJobDrawObject: PDrawObject;
  begin
    try
      // Workspot / jobs / datacolconnection
      // Create job list
      // ShowMessage(ADrawObject.APlantCode + ', ' + ADrawObject.AWorkspotCode);
      // ATTENTION: Always include the NOJOB! This is used to store quantities
      //            during startup that are not really counted yet.
      //            ALSO: Include BREAKJOB! This is used to book/show the
      //                  correct time when employee has scanned to the
      //                  break-job (fixed job without a norm).
      with oqJobs do
      begin
        ClearVariables;
        SetVariable('PLANT_CODE',    ADrawObject.APlantCode);
        SetVariable('WORKSPOT_CODE', ADrawObject.AWorkspotCode);
        SetVariable('JOB_CODE',      NOJOB);
        SetVariable('BREAKJOB',      BREAKJOB);
        // PIM-111 Related to this order: We must also read the down-jobs, or
        //         it will not show them as current job (after a restart)!
        SetVariable('DOWN1',         MECHANICAL_DOWN_JOB); // PIM-111
        SetVariable('DOWN2',         NO_MERCHANDIZE_JOB); // PIM-111

        Execute;
        while not Eof do
        begin
          if not Assigned(ADrawObject.AJobCodeList) then
            ADrawObject.AJobCodeList := TList.Create;
          AJobCodeRec := PJobCodeRec.Create;
          AJobCodeRec.JobCode := FieldAsString('JOB_CODE');
          AJobCodeRec.JobCodeDescription := FieldAsString('JDESCRIPTION');
          AJobCodeRec.InterfaceCode := FieldAsString('INTERFACE_CODE');
          AJobCodeRec.Norm_prod_level := FieldAsInteger('NORM_PROD_LEVEL');
          // 20014450.50 Do not sample time mins
//          AJobCodeRec.Sample_time_mins := FieldAsInteger('SAMPLE_TIME_MINS'); // 20013476
          AJobCodeRec.Sample_time_mins := 0; // 20014450.50
          AJobCodeRec.ARealTimeIntervalMinutes :=
            ADrawObject.ARealTimeIntervalMinutes; // 20014450.50
          AJobCodeRec.Link_WorkspotCode :=
            FieldAsString('LINK_WORKSPOT_CODE'); // 20013196
          AJobCodeRec.Link_JobCode :=
            FieldAsString('LINK_JOB_CODE'); // 20013196
          // 20013196
          if (AJobCodeRec.Link_WorkspotCode <> '') and
            (AJobCodeRec.Link_JobCode <> '') then
          begin
            // 20015376.60 Determine what DrawObject is used as LinkJob-Workspot
            ALinkJobDrawObject := FindDrawObject(APSList, ADrawObject.APlantCode,
              AJobCodeRec.Link_WorkspotCode);
            if Assigned(ALinkJobDrawObject) then
            begin
              if Assigned(ALinkJobDrawObject.AWorkspotTimeRec) then
                ALinkJobDrawObject.AWorkspotTimeRec.AIsLinkJobWorkspot := True;
              // PIM-159 Related to this: Only do this when it was assigned.
              AddLinkJobList(ADrawObject.APlantCode,
                AJobCodeRec.Link_WorkspotCode,
                AJobCodeRec.Link_JobCode,
                ADrawObject.APlantCode,
                ADrawObject.AWorkspotCode,
                AJobCodeRec.JobCode
                );
            end;
          end;
          ADrawObject.AJobCodeList.Add(AJobCodeRec);
          Next;
        end; // while
      end; // with
    except
      // trap errors here
    end;
  end; // ReadJobs
  procedure ReadBlackBoxes;
  var
    ABlackBoxRec: PBlackBoxRec;
//    AReadCounterRec: PReadCounterRec;
  begin
    try
      with oqBlackBox do
      begin
        ClearVariables;
        SetVariable('PLANT_CODE',    ADrawObject.APlantCode);
        SetVariable('WORKSPOT_CODE', ADrawObject.AWorkspotCode);
        SetVariable('COMPUTER_NAME', ORASystemDM.CurrentComputerName);
        Execute;
        while not Eof do
        begin
          if not Assigned(ADrawObject.ABlackBoxList) then
            ADrawObject.ABlackBoxList := TList.Create;
          new(ABlackBoxRec);
          ABlackBoxRec.InterfaceCode := FieldAsString('INTERFACE_CODE');
          ABlackBoxRec.IPAddress := FieldAsString('IPADDRESS');
          ABlackBoxRec.Port := FieldAsString('PORT');
          ABlackBoxRec.ModuleAddress := FieldAsString('ADDRESS');
          ABlackBoxRec.CounterNumber := FieldAsInteger('COUNTER');
          ABlackBoxRec.ActualCounterValue := 0;
          ABlackBoxRec.ActualCounterDateTime := 0;
          ABlackBoxRec.Disabled := False;
          ABlackBoxRec.CheckDigits := FieldAsString('CHECKDIGITS'); // TD-21191
{$IFDEF TESTXXX}
          ABlackBoxRec.IPAddress := '';
          ABlackBoxRec.Port := '';
          ABlackBoxRec.CheckDigits := '';
{$ENDIF}
{$IFDEF TEST2XXX}
          ABlackBoxRec.IPAddress := '';
          ABlackBoxRec.Port := '';
          ABlackBoxRec.CheckDigits := '';
{$ENDIF}
          ABlackBoxRec.TestCounter := 0; // For TEST-version.
          ABlackBoxRec.Reset := False; // SO-20015330
          ADrawObject.ABlackBoxList.Add(ABlackBoxRec);

          // TODO 21191 Do this somewhere else!
{
          // Also add this to ReadCounterList
          new(AReadCounterRec);
          AReadCounterRec.DrawObject := ADrawObject;
          AReadCounterRec.BlackBoxRec := ABlackBoxRec;
          AReadCounterList.Add(AReadCounterRec);
}
{
          try
            if not IPExists(ABlackBoxRec.IPAddress) then
              ABlackBoxRec.Disabled := True;
          except
            //
          end;
}
          Next;
        end; // while
      end; // with
    except
      // todo: trap errors here
    end;
  end; // ReadBlackBoxes;
  procedure TestBlackBoxes;
  var
    I, J: Integer;
    ABlackBoxRec: PBlackBoxRec;
    AReadCounterRec: PReadCounterRec;
    IPList: TStringList;
  begin
    IPList := TStringList.Create;
    try
      try
        if Assigned(AReadCounterList) then
        begin
          for I := 0 to AReadCounterList.Count - 1 do
          begin
            AReadCounterRec := AReadCounterList.Items[I];
            ABlackBoxRec := AReadCounterRec.BlackBoxRec;
            if IPList.IndexOf(ABlackBoxRec.IPAddress) = -1 then
              IPList.Add(ABlackBoxRec.IPAddress);
          end;
          for I := 0 to IPList.Count - 1 do
          begin
            if not IPExists(IPList.Strings[I]) then
            begin
              // Disable 1 or more blackboxes with same IP-address
              for J := 0 to AReadCounterList.Count - 1 do
              begin
                AReadCounterRec := AReadCounterList.Items[J];
                ABlackBoxRec := AReadCounterRec.BlackBoxRec;
                if ABlackBoxRec.IPAddress = IPList.Strings[I] then
                  ABlackBoxRec.Disabled := True;
              end;
            end;
          end;
        end;
      except
        //
      end;
    finally
      IPList.Free;
    end;
  end; // TestBlackBoxes
  // Fill the hours for the employees that have processed scans by
  // reading production hours per employee table.
  // Is this needed ???
  procedure ReadProdHoursPerEmployee;
{  var
    AEmployeeRec: PTEmployeeRec;
    I2: Integer; }
  begin
    try
{      for I2 := 0 to ADrawObject.AEmployeeList.Count - 1 do
      begin
        AEmployeeRec := ADrawObject.AEmployeeList.Items[I2];
      end; }
    except
      // todo: trap errors here
    end;
  end; // ReadHoursPerEmployee
  procedure ReadProductionQuantities;
  var
    AJobCodeRec: PJobCodeRec;
  begin
{$IFDEF DEBUG2}
WDebugLog('ReadProductionQuantities');
{$ENDIF}
    try
      // For TODAY and SHIFT
      // Get production quantities per shift and job
      // PIM-151 Related to this order: It checked on automatic_quantity <> 0
      //         But when a PQ is added via Pims itself then quantity is filled
      //         So check on quantity <> 0.
      with oqPQ do
      begin
        ClearVariables;
        SetVariable('PLANT_CODE',    ADrawObject.APlantCode);
        SetVariable('WORKSPOT_CODE', ADrawObject.AWorkspotCode);
        SetVariable('START_DATE',    ReadDateFrom);
        SetVariable('CURR', 0); // 20016016
        Execute;
        while not Eof do
        begin
          // Per job
          AJobCodeRec :=
            ADrawObject.FindJobCodeListRec(FieldAsString('JOB_CODE'));
          if Assigned(AJobCodeRec) then
          begin
//            if AJobCodeRec.JobCode <> NOJOB then // PIM-151 Include NOJOB ?
            begin
              // Prod qty per day for job
              AJobCodeRec.AddQtyDay(FieldAsInteger('QTY'));
              // Prod qty per shift per job
              if ADrawObject.AWorkspotTimeRec.ACurrentShiftNumber =
                FieldAsInteger('SHIFT_NUMBER') then
                AJobCodeRec.AddQtyShift(FieldAsInteger('QTY'));
              if AJobCodeRec.JobCode <> NOJOB then // PIM-151
              begin
                // Per workspot
                // Prod qty per day for workspot
                ADrawObject.AddQtyDay(FieldAsInteger('QTY'));
                if ADrawObject.AWorkspotTimeRec.ACurrentShiftNumber =
                  FieldAsInteger('SHIFT_NUMBER') then
                  ADrawObject.AddQtyShift(FieldAsInteger('QTY'));

                // Theo qty per day for workspot for all jobs
                // Act_Time_xxx is in secs!
                // Do not do this here, but during read of employee-scans!
                // 20014450.50 Add Theo-values here, because we have already read
                //             the employee-scans.
                ADrawObject.AddTheoTimeDay(AJobCodeRec);
                ADrawObject.AddTheoQtyDay(AJobCodeRec);
{$IFDEF DEBUG2}
WDebugLog(
  'P=' + ADrawObject.APlantCode +
  ' WS=' + ADrawObject.AWorkspotCode +
  ' Job=' + AJobCodeRec.JobCode + ' (' + AJobCodeRec.JobCodeDescription + ')' +
  ' Norm=' + IntToStr(AJobCodeRec.Norm_prod_level) +
  ' Act_prodqty_day=' + IntToStr(AJobCodeRec.Act_prodqty_day) +
  ' Act_time_day=' + FloatToStr(AJobCodeRec.Act_time_day) +
  ' Theo_prodqty_day=' + IntToStr(ADrawObject.Theo_prodqty_day)
  );
{$ENDIF}
                // Theo qty per shift for workspot for all jobs
                if ADrawObject.AWorkspotTimeRec.ACurrentShiftNumber =
                  FieldAsInteger('SHIFT_NUMBER') then
                begin
                  // Do not do this here, but during read of employee-scans!
                  // 20014450.50 Add Theo-values here, because we have already
                  //             read the employee-scans.
                  ADrawObject.AddTheoTimeShift(AJobCodeRec);
                  ADrawObject.AddTheoQtyShift(AJobCodeRec);
                end;
              end; // if <> NOJOB
            end; // if
          end; // if
          Next;
        end; // while
        // REMARK: The prodqty for current that is stored here will be
        //         overwritten as soon as the first qty is read, so
        //         it has no use to get them here.
        // 20016016 This is needed for this order, where qty are from an
        //          external application, not from blackbox.
        if Assigned(ADrawObject.AWorkspotTimeRec) then
          if ADrawObject.AWorkspotTimeRec.AReceiveQtyFromExternalApp then
          begin
            // PIM-90 Read from PQ or from WorkspoteffPerMinute based on pimssetting
            if not ORASystemDM.ExternQtyFromPQ then
            begin
              // Read qty From WorkspotEffPerMinute
              with oqWSEffPerMinute do
              begin
                ClearVariables;
                SetVariable('PLANT_CODE',    ADrawObject.APlantCode);
                SetVariable('WORKSPOT_CODE', ADrawObject.AWorkspotCode);
                SetVariable('INTERVALSTARTTIME', IncSecond(Now, -CurrentTimeWindow));
                Execute;
                while not Eof do
                begin
                  // Per job
                  AJobCodeRec :=
                    ADrawObject.FindJobCodeListRec(FieldAsString('JOB_CODE'));
                  if Assigned(AJobCodeRec) then
                  begin
                    if AJobCodeRec.JobCode <> NOJOB then // PIM-151 Include NOJOB ?
                    begin
                      // Prod qty current for job
                      AJobCodeRec.AddQtyCurrent(FieldAsInteger('QTY'));
                      // Add to workspot
                      ADrawObject.AddQtyCurrent(FieldAsInteger('QTY'));
                      // Also add time: PIM-91
                      AJobCodeRec.Act_time_current := CurrentTimeWindow;
                      ADrawObject.Act_time_current := CurrentTimeWindow;
                    end; // if
                  end; // if
                  Next;
                end; // while
              end; // with
            end
            else
            begin
              // Read from ProductionQuantity
              // For CURRENT -> Only take records for last 5 minutes (CURRENT_TIME_WINDOW)
              // Get production quantities per shift and job
              ClearVariables;
              SetVariable('PLANT_CODE',    ADrawObject.APlantCode);
              SetVariable('WORKSPOT_CODE', ADrawObject.AWorkspotCode);
              SetVariable('START_DATE',    IncSecond(Now, -CurrentTimeWindow)); // PIM-90 Use CurrentTimeWindow!
              SetVariable('CURR', 1); // 20016016
              Execute;
              while not Eof do
              begin
                // Per job
                AJobCodeRec :=
                  ADrawObject.FindJobCodeListRec(FieldAsString('JOB_CODE'));
                if Assigned(AJobCodeRec) then
                begin
                  if AJobCodeRec.JobCode <> NOJOB then // PIM-151 Include NOJOB ?
                  begin
                    // Prod qty current for job
                    AJobCodeRec.AddQtyCurrent(FieldAsInteger('QTY'));
                    // Add to workspot
                    ADrawObject.AddQtyCurrent(FieldAsInteger('QTY'));
                    // Also add time: PIM-91
                    AJobCodeRec.Act_time_current := CurrentTimeWindow;
                    ADrawObject.Act_time_current := CurrentTimeWindow;
                  end; // if
                end; // if
                Next;
              end; // while
            end; // if
          end;
      end; // with
    except
      // todo: trap errors here
    end;
  end; // ReadProductionQuantities
  // Read counter values based on counter value-table
  // Version 1.
  // REMARK: An alternative table is used to store the counter-values.
  //         Instead of COUNTERVALUE-table, a new table named
  //         COUNTERVALUEPS is used.
  function ReadLastCounterValues: Boolean;
  var
    Jobs: String;
    AJobCodeRec: PJobCodeRec;
    SelectStr: String;
    I, CVIndex: Integer;
    LastCounterValue: Integer;
    procedure UnDoJobs;
    var
      I: Integer;
    begin
      for I := 0 to ADrawObject.AJobCodeList.Count - 1 do
      begin
        AJobCodeRec := ADrawObject.AJobCodeList[I];
        AJobCoderec.Done := False;
      end;
    end;
    // Assign the found counter value also to other jobs,
    // so next time there is a counting, it takes this value
    // as last counter value for the job.
    procedure AssignCounterValueToOtherJobs;
    var
      I: Integer;
    begin
      if LastCounterValue <> 0 then
        for I := 0 to ADrawObject.AJobCodeList.Count - 1 do
        begin
          AJobCodeRec := ADrawObject.AJobCodeList.Items[I];
          if not AJobCodeRec.Done then
          begin
            AJobCodeRec.Last_Counter_Value := LastCounterValue;
          end; // if
        end; // for
    end; // AssignCounterValueToOtherJobs
    // There can be more than 1 record found for job, only memorize
    // the latest (last) counter value.
    // IMPORTANT: Store the LastCounterValue for the CURRENTJOB, even if the
    //            CV-record is not equal to the currentjob, which can happen
    //            when the job has changed!
    //            Reason: Otherwise LastCounterValue will be 0 and calculation
    //                    Qty will go wrong!
    procedure MemorizeCounterValueForJob;
    var
      I: Integer;
      JobCode: String;
    begin
      JobCode := oqCV.FieldAsString('JOB_CODE');
      if Assigned(ADrawObject.AWorkspotTimeRec) then
        for I := 0 to ADrawObject.AJobCodeList.Count - 1 do
        begin
          AJobCodeRec := ADrawObject.AJobCodeList[I];
          if AJobCodeRec.JobCode =
            ADrawObject.AWorkspotTimeRec.ACurrentJobCode then
          begin
            if not AJobCodeRec.Done then
            begin
              AJobCodeRec.Last_Counter_Value :=
                oqCV.FieldAsInteger('COUNTER_VALUE');
              AJobCodeRec.Done := True;
{$IFDEF DEBUG2}
WDebugLog(
  'P=' + ADrawObject.APlantCode +
  ' WS=' + ADrawObject.AWorkspotCode +
  ' LastCounterReadTime=' + DateTimeToStr(ADrawObject.ALastCounterReadTime) +
  ' Job=' + AJobCodeRec.JobCode + ' (' + AJobCodeRec.JobCodeDescription + ')' +
  ' Job.Last_Counter_Value=' + IntToStr(AJobCodeRec.Last_Counter_Value)
  );
{$ENDIF}
            end;
            Break;
          end;
      end;
    end; // MemorizeCounterValueForJob;
  begin
{$IFDEF DEBUG2}
WDebugLog('ReadLastCounterValues');
{$ENDIF}
    Result := False;
    try
      Jobs := '';
      if ADrawObject.AJobCodeList.Count > 0 then
      begin
        Jobs := '  AND CV.JOB_CODE IN (';
        for I := 0 to ADrawObject.AJobCodeList.Count - 1 do
        begin
          AJobCodeRec := ADrawObject.AJobCodeList[I];
          Jobs := Jobs + '''' +
            AJobCodeRec.JobCode + '''';
          if I < ADrawObject.AJobCodeList.Count - 1 then
            Jobs := Jobs + ',';
        end; // for
        Jobs := Jobs + ')';
      end; // if
      SelectStr :=
        'SELECT ' + NL +
        '  CV.COUNTER_DATETIME, CV.JOB_CODE, CV.COUNTER_VALUE, ' + NL +
        '  CV.IGNORE_YN ' + NL +
        'FROM ' + NL +
        '  COUNTERVALUEPS CV ' + NL +
        'WHERE ' + NL +
        '  CV.PLANT_CODE = :PLANT_CODE AND ' + NL +
        '  CV.WORKSPOT_CODE = :WORKSPOT_CODE ' + NL +
        Jobs + NL +
        'ORDER BY ' + NL +
        '  CV.COUNTER_DATETIME DESC ';
      with oqCV do
      begin
        SQL.Clear;
        SQL.Add(SelectStr);
        ClearVariables;
        SetVariable('PLANT_CODE',    ADrawObject.APlantCode);
        SetVariable('WORKSPOT_CODE', ADrawObject.AWorkspotCode);
        Execute;
        CVIndex := 0;
        UnDoJobs;
        LastCounterValue := 0;
        ADrawObject.Last_Counter_Value := 0;
        if not Eof then
          Result := True;
        while not Eof do
        begin
          // Memorize last counter datetime for workspot
          if CVIndex = 0 then
          begin
            ADrawObject.ALastCounterReadTime := FieldAsDate('COUNTER_DATETIME');
            LastCounterValue := FieldAsInteger('COUNTER_VALUE');
            ADrawObject.Last_Counter_Value := LastCounterValue;
            // Test if LastCounterReadtime is not older than 12 hours
            // If so, then set it to 0 (not used).
            if (Now - ADrawObject.ALastCounterReadTime) > 0.5 then
              ADrawObject.ALastCounterReadTime := 0;
            // Always set it to 0!
            // Set it not to 0, but to 'now', or it goes wrong with
            // determination of last counter value!
            ADrawObject.ALastCounterReadTime := Now;
          end;
          // Memorize countervalue for job
          MemorizeCounterValueForJob;
          inc(CVIndex);
          Next;
        end; // while
        AssignCounterValueToOtherJobs;
      end; // with
    except
      // todo: trap errors here
    end;
  end; // ReadLastCounterValues
  // Read last Counter Values based on ProductionQuantity-table.
  // Look for last record per workspot, for all jobs.
  // Assign this to all jobs! To be sure what last counter value was.
  procedure ReadLastCounterValuesPQ;
  var
    AJobCodeRec: PJobCodeRec;
    LastCounterValue: Integer;
    procedure UndoJobs;
    var
      I: Integer;
    begin
      for I := 0 to ADrawObject.AJobCodeList.Count - 1 do
      begin
        AJobCodeRec := ADrawObject.AJobCodeList[I];
        AJobCoderec.Done := False;
      end; // for
    end; // UndoJobs
    // Assign the found counter value also to other jobs,
    // so next time there is a counting, it takes this value
    // as last counter value for the job.
    procedure AssignCounterValueToOtherJobs;
    var
      I: Integer;
    begin
      if LastCounterValue <> 0 then
        for I := 0 to ADrawObject.AJobCodeList.Count - 1 do
        begin
          AJobCodeRec := ADrawObject.AJobCodeList.Items[I];
          if not AJobCodeRec.Done then
          begin
            AJobCodeRec.Last_Counter_Value := LastCounterValue;
          end; // if
        end; // for
    end; // AssignCounterValueToOtherJobs
  begin
{$IFDEF DEBUG2}
WDebugLog('ReadLastCounterValuesPQ');
{$ENDIF}
    try
      with oqLastPQ do
      begin
        // Find last counter value for all jobs of 1 workspot
        ClearVariables;
        SetVariable('PLANT_CODE',    ADrawObject.APlantCode);
        SetVariable('WORKSPOT_CODE', ADrawObject.AWorkspotCode);
        Execute;
        UndoJobs;
        LastCounterValue := 0;
        while not Eof do
        begin
          AJobCodeRec :=
            ADrawObject.FindJobCodeListRec(FieldAsString('JOB_CODE'));
          if Assigned(AJobCodeRec) then
          begin
            AJobCodeRec.Last_Counter_Value := FieldAsInteger('COUNTER_VALUE');
            LastCounterValue := FieldAsInteger('COUNTER_VALUE');

            AJobCodeRec.Done := True;
            ADrawObject.ALastCounterReadTime := FieldAsDate('END_DATE');

{$IFDEF DEBUG2}
WDebugLog(
  'P=' + ADrawObject.APlantCode +
  ' WS=' + ADrawObject.AWorkspotCode +
  ' LastCounterReadTime=' + DateTimeToStr(ADrawObject.ALastCounterReadTime) +
  ' Job=' + AJobCodeRec.JobCode + ' (' + AJobCodeRec.JobCodeDescription + ')' +
  ' Job.Last_Counter_Value=' + IntToStr(AJobCodeRec.Last_Counter_Value)
  );
{$ENDIF}
            // Test if LastCounterReadtime is not older than 12 hours
            // If so, then set it to 0 (not used).
            if (Now - ADrawObject.ALastCounterReadTime) > 0.5 then
              ADrawObject.ALastCounterReadTime := Now;
            // Always set it to 0!
            //
            // MRA:4-APR-2012:
            // When this is not done, then a TimeElapsed will
            // be calculated and added to the time of the employee.
            // This can give too many hours!
//            ADrawObject.ALastCounterReadTime := 0;
            // Set it not to 0, but to 'now', or it goes wrong with
            // determination of last counter value!
            ADrawObject.ALastCounterReadTime := Now;
          end; // if
          Next;
        end; // while
        AssignCounterValueToOtherJobs;
      end; // with
    except
      // todo: trap error here
    end;
  end; // ReadLastCounterValuesPQ
  // 20013379
  function CompareJobsInit: Boolean;
  var
    ACompareJobRec: PCompareJobRec;
  begin
    Result := False;
    try
      // Compare Jobs Handling.
      Result := DetectCompareJob(ADrawObject.APlantCode,
        ADrawObject.AWorkspotCode,
        ADrawObject.AWorkspotTimeRec.ACompareJobCode,
        ADrawObject.AWorkspotTimeRec.ACompareJobDescription,
        ADrawObject.AWorkspotTimeRec.AEmpsScanIn);
      // TD-24819 Also init a list of compare jobs
      if Result and (not ADrawObject.AWorkspotTimeRec.AEmpsScanIn) then
      begin
        // Disable these 2 buttons, because no-one should scan-in on this workspot
        ADrawObject.AWorkspotTimeRec.AJobButton.Enabled := False;
        ADrawObject.AWorkspotTimeRec.AInButton.Enabled := False;
        // Create/Re-create a list of compare-jobs.
        ADrawObject.InitCompareJobList(False);
        with oqCompareJobs do
        begin
          ClearVariables;
          SetVariable('PLANT_CODE',    ADrawObject.APlantCode);
          SetVariable('WORKSPOT_CODE', ADrawObject.AWorkspotCode);
          Execute;
          while not Eof do
          begin
            new(ACompareJobRec);
            ACompareJobRec.WorkspotCode := FieldAsString('COMPARE_WORKSPOT_CODE');
            ACompareJobRec.JobCode := FieldAsString('COMPARE_JOB_CODE');
            ADrawObject.ACompareJobList.Add(ACompareJobRec);
            Next;
          end; // while
        end; // with
      end; // if
    except
    end;
  end; // CompareJobsInit;
  // 20013379
  procedure CompareJobsHandler;
  var
    EmployeeNumberFake: Integer;
    function ActionFakeEmployee: Boolean;
    var
      FakeEmpStr: String;
    begin
      Result := True;
      Randomize;
      FakeEmpStr := IntToStr(FAKE_EMPLOYEE_START_NUMBER);
      EmployeeNumberFake := StrToInt(FakeEmpStr + IntToStr(Random(999)));
    end; // ActionFakeEmployee
  begin
    try
      // There is a compare job detected. Use it as current job
      if (ADrawObject.AWorkspotTimeRec.ACompareJobCode <> '') and
        (not ADrawObject.AWorkspotTimeRec.AEmpsScanIn) then
      begin
        if ADrawObject.AWorkspotTimeRec.ACurrentJobCode = '' then
        begin
          ADrawObject.AWorkspotTimeRec.ACurrentJobCode :=
            ADrawObject.AWorkspotTimeRec.ACompareJobCode;
          ADrawObject.AWorkspotTimeRec.ACurrentJobDescription :=
            ADrawObject.AWorkspotTimeRec.ACompareJobDescription;
          // Create a fake employee
          if Assigned(ADrawObject.AEmployeeList) then
            if ADrawObject.AEmployeeList.Count = 0 then
            begin
              if ActionFakeEmployee then
                ADrawObject.AddUpdateEmployeeList(EmployeeNumberFake,
                  'Compare Employee',
                  1, '1', 'A',
                  RoundTime(Now, 1), 0, 'N',
                  ADrawObject.AWorkspotTimeRec.ACompareJobCode,
                  ADrawObject.AWorkspotTimeRec.ACompareJobDescription,
                  True,
                  False // Visible (20013379)
                  );
            end;
        end;
      end;
    except
      // todo: trap error here
    end;
  end; // CompareJobsHandler
  // 20015178
  function TimeRecByMachineInit: Boolean;
  begin
    Result := False;
    with oqWorkspot do
    begin
      ClearVariables;
      SetVariable('PLANT_CODE',    ADrawObject.APlantCode);
      SetVariable('WORKSPOT_CODE', ADrawObject.AWorkspotCode);
      Execute;
      if not Eof then
        Result := FieldAsString('TIMEREC_BY_MACHINE_YN') = 'Y';
    end;
    if Result then
    begin
      // Disable/Hide In-button, because no-one should scan-in on this workspot
      ADrawObject.AWorkspotTimeRec.AInButton.Enabled := False;
//      ADrawObject.AWorkspotTimeRec.AInButton.Visible := False;
      // Enable JobButton: Should always be enabled.
      ADrawObject.AWorkspotTimeRec.AJobButton.Enabled := True;
      // Employees do not scan in!
      ADrawObject.AWorkspotTimeRec.AEmpsScanIn := False;
      ADrawObject.AWorkspotTimeRec.AFakeEmployeeNumber :=
        oqWorkspot.FieldAsInteger('FAKE_EMPLOYEE_NUMBER');
      if ADrawObject.AWorkspotTimeRec.AFakeEmployeeNumber = -1 then
        ADrawObject.AWorkspotTimeRec.AFakeEmployeeNumber :=
          FakeEmployeeNumber(ADrawObject.APlantCode, ADrawObject.AWorkspotCode);
    end;
  end; // TimeRecByMachineInit
  // 20016016
  procedure ReceiveQtyFromExternalAppInit;
  begin
    if ReceiveQtyFromExternalAppInitDone then // PIM-12
      Exit;
    with oqWorkspot do
    begin
      ClearVariables;
      SetVariable('PLANT_CODE',    ADrawObject.APlantCode);
      SetVariable('WORKSPOT_CODE', ADrawObject.AWorkspotCode);
      Execute;
      if not Eof then
        ADrawObject.AWorkspotTimeRec.AReceiveQtyFromExternalApp :=
          FieldAsString('PS_QTYVIA_EXTERNAL_YN') = 'Y';
      // ABS-8116
      if ADrawObject.AWorkspotTimeRec.AReceiveQtyFromExternalApp then
        ReadExternalData := True // PIM-90
      else
        inc(BBCount);
    end;
  end; // ReceiveQtyFromExternalAppInit
  // 20016016 Read all for ONE ADrawObject
  procedure ReadAllForDrawObject(ADrawObject: PDrawObject);
  begin
    ADrawObject.ARecreateEmployeeDisplayList := True; // 20014450.50
    ReadPlantInfo; // 20014450.50
    ReadJobs;
    // TODO 21191 Do this on a condition. Not during Refresh!
    if ARecreateReadCounterList then
      ReadBlackBoxes;
    // 20015376 Related to this order: First read prodqty and then scans? It was the other way round.
    // 20016016.70 We must FIRST read time-recording and after that quantities,
    //             or we do not know what the current shift is (needed for
    //             knowing the actual quantity per shift).
    ReadTimeRegScanning(ADrawObject);
    ReadProductionQuantities;
    // PIM-12 This must be done here! Because at this point it knows the
    //        time + quantities.
    // PIM-12 Bugfix:
    //        - We do this before we show efficiency! 
//    ADrawObject.DetermineTheoProdQtyCurrent;
//    ADrawObject.DetermineTheoTimeCurrent;
    if not ReadLastCounterValues then
      ReadLastCounterValuesPQ;
    CompareJobsHandler;
    ADrawObject.ARefreshTimeStamp := Now;
  end; // ReadAllForDrawObject
begin
  try
    ReadIgnoreInterfaceCodes; // 20015331
    // PIM-12.3 Do not always clear the link job list! Because
    //          when 'refresh employees' is used, it is not recreated!
    //          Only clear it when ALL must be read.
    if not (AOnlyUpdateEmployeeList or AReadAllForOneDrawObject) then
      ClearLinkJobList; // 20015376
    // 20016016
    if AReadAllForOneDrawObject then
    begin
      ADrawObject := AOneDrawObject;
      ADrawObject.Init(False);
      ReadAllForDrawObject(AOneDrawObject);
      Exit;
    end;
    BBCount := 0; // ABS-8116
    // Read all Workspots, Jobs, Employees, Scans and Quantities
    for I := 0 to APSList.Count - 1 do
    begin
      ADrawObject := APSList.Items[I];
      if Assigned(ADrawObject.AWorkspotTimeRec) then
      begin
        ReceiveQtyFromExternalAppInit; // 20016016
        if AOnlyUpdateEmployeeList then
        begin
          // 20015376.60 Exclude Compare-workspots: No employees are used there.
          if not ADrawObject.AWorkspotTimeRec.AIsCompareWorkspot then
          begin
            // Only refresh the list of employees, which
            // can happen when they scan-in from another
            // workstation.
            // ADrawObject.InitTime; // 20015178.70
            // 20015178.70 Init this except for Theo_time_current.
            ADrawObject.Act_time_day := 0;
            ADrawObject.Act_time_shift := 0;
            ADrawObject.Act_time_current := 0;
            ADrawObject.Theo_time_day := 0;
            ADrawObject.Theo_time_shift := 0;
            // ADrawObject.Theo_time_current := 0;
            ADrawObject.InitQty; // TD-21738
            ADrawObject.InitEmployeeList(False);
            // PIM-195 This must be done, otherwise it shows 0 for efficiency,
            //         for Shift/Today-mode.
            ADrawObject.ResetJobsTheoTime;
//            ADrawObject.InitEmployeeDisplayList(False); // 20014450.50
            ADrawObject.ARecreateEmployeeDisplayList := True; // 20014450.50
            ReadTimeRegScanning(ADrawObject);
          end;
        end
        else
        begin
          //
          // Add any missing jobs here! Or it will not be added to the variables.
          //
          ActionAddMissingJobForWorkspot(ADrawObject.APlantCode,
            ADrawObject.AWorkspotCode, NOJOB, SPimsNoJobDescription);
          ActionAddMissingJobForWorkspot(ADrawObject.APlantCode,
            ADrawObject.AWorkspotCode, BREAKJOB, SPimsBreakJobDescription);
          ActionAddMissingJobForWorkspot(ADrawObject.APlantCode,
            ADrawObject.AWorkspotCode, MECHANICAL_DOWN_JOB, SPimsMechanicalDown);
          ActionAddMissingJobForWorkspot(ADrawObject.APlantCode,
            ADrawObject.AWorkspotCode, NO_MERCHANDIZE_JOB, SPimsNoMerchandize);
          ADrawObject.AWorkspotTimeRec.AIsCompareWorkspot :=
            CompareJobsInit; // TD-24819
          if ORASystemDM.EnableMachineTimeRec then
            ADrawObject.AWorkspotTimeRec.ATimeRecByMachine :=
              TimeRecByMachineInit; // 20015178
          ReadAllForDrawObject(ADrawObject); // 20016016
        end;
      end; // if
    end; // for
    ReassignCompareJob(APSList); // TD-24819
    // TODO 21191 Do this on a condition. Not during Refresh!
    if ARecreateReadCounterList then
      RecreateReadCounterList(APSList, AReadCounterList);
    if not AOnlyUpdateEmployeeList then
    begin
      // 20012858.80
      try
        if (BBCount > 0) then
          TestBlackBoxes;
      except
      end;
    end;
  except
  end;
  // PIM-12
  if not AReadAllForOneDrawObject then
  begin
    ReadPlantInfoDone := True;
    ReceiveQtyFromExternalAppInitDone := True;    
  end;
end; // DataReadAll

// Function used for sorting on date
function DateCompare(Item1, Item2: Pointer): Integer;
var
  AQtyToStoreRec1, AQtyToStoreRec2: PQtyToStoreRec;
begin
  Result := 0;
  AQtyToStoreRec1 := Item1;
  AQtyToStoreRec2 := Item2;
  if (AQtyToStoreRec1.Timestamp < AQtyToStoreRec2.Timestamp) then
    Result := -1
  else
    if (AQtyToStoreRec1.Timestamp > AQtyToStoreRec2.Timestamp) then
      Result := 1;
end; // DateCompare

procedure TPersonalScreenDM.WriteQtyToStoreList(APSList: TList;
  ALogPath: String);
var
  I: Integer;
  ADrawObject: PDrawObject;
begin
  try
    for I := 0 to APSList.Count - 1 do
    begin
      ADrawObject := APSList.Items[I];
      ADrawObject.WriteQtyToStoreListToFile(ALogPath);
    end;
  except
    on E:Exception do
    begin
      WErrorLog(E.Message);
    end;
  end;
end; // WriteQtyToStoreList

// Clear the QtyToStoreList when data is written to DB.
procedure TPersonalScreenDM.InitQtyToStoreList(APSList: TList);
var
  I: Integer;
  ADrawObject: PDrawObject;
begin
  try
    for I := 0 to APSList.Count - 1 do
    begin
      ADrawObject := APSList.Items[I];
      ADrawObject.InitQtyToStoreList(False);
    end;
  except
    on E:Exception do
    begin
      WErrorLog(E.Message);
    end;
  end;
end; // InitQtyToStoreList

procedure TPersonalScreenDM.ActionWriteToFile(APSList: TList;
  ALogPath: String);
begin
  WriteQtyToStoreList(APSList, ALogPath);
end; // ActionWriteToFile

// Create a QtyToStoreTempList that can be used for writing
// to database.
procedure TPersonalScreenDM.CreateQtyToStoreTempList(APSList: TList);
var
  I, J: Integer;
  ADrawObject: PDrawObject;
  AQtyToStoreRec: PQtyToStoreRec;
  AJobCodeRec: PJobCodeRec;
  // 20013196 Addition of 2 parameters, for use with link-job.
  procedure AddUpdateTempList(AWorkspotCode, AJobCode: String);
  var
    NewStartTime: TDateTime;
    EndTime: TDateTime;
    Found: Boolean;
  begin
    try
      NewStartTime :=
        DateTimeTruncToInterval(AQtyToStoreRec.Timestamp, PIMS_TIME_INTERVAL);
      EndTime := IncMinute(NewStartTime, 5);
    except
      on E:Exception do
      begin
        WErrorLog('CreateQtyToStoreTempList.DateTimeTruncToInterval: ' + E.Message);
        Exit;
      end;
    end;
    // 20013489
{    if AQtyToStoreRec.ShiftDate = NullDate then
      AQtyToStoreRec.ShiftDate := Trunc(NewStartTime); }
    // 20013489 Use ADrawObect.LastShiftDate, to ensure the ShiftDate is known,
    //          even when no-one is scanned in anymore.
    if ADrawObject.LastShiftDate = NullDate then
      ADrawObject.LastShiftDate := Trunc(NewStartTime);
{$IFDEF DEBUG5}
  if ADrawObject.LastShiftDate <> NullDate then
    WDebugLog('-> AddUpdateTempList. ADrawObject.LastShiftDate=' + DateToStr(ADrawObject.LastShiftDate))
  else
    WDebugLog('-> AddUpdateTempList. ADrawObject.LastShiftDate=NULL');
{$ENDIF}
    // MRA:11-APR-2014 TD-24736 Troubleshoot DFD about date-format!
    // TD-24736 Rework
    // Use more try-except error-logging to see what goes wrong before quantities are
    // stored to database. Otherwise you do not have a clue what went wrong.
    try
      if DateTimeFormatPatch then // TD-24736.60 Use this optional
        ORASystemDM.ADateFmt.SwitchDateTimeFmtORA; // TD-24736 Do not do this!
      try
        try
          Found := cdsQtyToStore.
            Locate('STARTTIME;PLANT_CODE;WORKSPOT_CODE;JOB_CODE;SHIFT_NUMBER;ENDTIME',
              VarArrayOf([NewStartTime,
              ADrawObject.APlantCode,
              AWorkspotCode, // 20013196
              AJobCode, // 20013196
              AQtyToStoreRec.ShiftNumber,
              EndTime]), []);
        except
          on E:EOracleError do
          begin
            WErrorLog('CreateQtyToStoreTempList.Locate: ' + E.Message);
            Exit;
          end;
          on E:Exception do
          begin
            WErrorLog('CreateQtyToStoreTempList.Locate: ' + E.Message);
            Exit;
          end;
        end;
        if not Found then
        begin
          // Insert
          with cdsQtyToStore do
          begin
            try
              Insert;
              FieldByName('STARTTIME').Value := NewStartTime;
              FieldByName('PLANT_CODE').Value := ADrawObject.APlantCode;
              FieldByName('WORKSPOT_CODE').Value := AWorkspotCode; // 20013196
              FieldByName('JOB_CODE').Value := AJobCode; // 20013196
              FieldByName('SHIFT_NUMBER').Value := AQtyToStoreRec.ShiftNumber;
              FieldByName('QUANTITY').Value := AQtyToStoreRec.Quantity;
              FieldByName('COUNTER_VALUE').Value := AQtyToStoreRec.Countervalue;
              FieldByName('ENDTIME').Value := EndTime;
              // 20013489
              FieldByName('SHIFT_DATE').Value := ADrawObject.LastShiftDate; // AQtyToStoreRec.ShiftDate; //
              Post;
            except
              on E:EOracleError do
              begin
                WErrorLog('CreateQtyToStoreTempList.Insert: ' + E.Message);
              end;
              on E:Exception do
              begin
                WErrorLog('CreateQtyToStoreTempList.Insert: ' + E.Message);
              end;
            end;
          end; // with
        end // if
        else
        begin
          // Update
          with cdsQtyToStore do
          begin
            try
              Edit;
              FieldByName('QUANTITY').Value :=
                FieldByName('QUANTITY').Value + AQtyToStoreRec.Quantity;
              FieldByName('COUNTER_VALUE').Value := AQtyToStoreRec.Countervalue;
              Post;
            except
              on E:EOracleError do
              begin
                WErrorLog('CreateQtyToStoreTempList.Update: ' + E.Message);
              end;
              on E:Exception do
              begin
                WErrorLog('CreateQtyToStoreTempList.Update: ' + E.Message);
              end;
            end;
          end; // with
        end; // if
      except
        on E:EOracleError do
        begin
          WErrorLog('CreateQtyToStoreTempList.Other: ' + E.Message);
        end;
        on E:Exception do
        begin
          WErrorLog('CreateQtyToStoreTempList.Other: ' + E.Message);
        end;
      end;
    finally
      if DateTimeFormatPatch then // TD-24736.60 Use this optional
        ORASystemDM.ADateFmt.SwitchDateTimeFmtWindows; // TD-24736 Do not do this!
    end;
  end; // AddUpdateTempList
begin
// TESTING
{$IFDEF DEBUG2}
  WDebugLog('CreateQtyToStoreTempList - Start');
{$ENDIF}
  cdsQtyToStore.EmptyDataSet;
  try
    try
      for I := 0 to APSList.Count - 1 do
      begin
        ADrawObject := APSList.Items[I];
        // First sort list to date
        if Assigned(ADrawObject.QtyToStoreList) then
        begin
          if ADrawObject.QtyToStoreList.Count > 0 then
          begin
            ADrawObject.QtyToStoreList.Sort(DateCompare);
            for J := 0 to ADrawObject.QtyToStoreList.Count - 1 do
            begin
              AQtyToStoreRec := ADrawObject.QtyToStoreList.Items[J];
              // Only add/update when jobcode <> '', which can happen
              // when there is no current job (no employee who scanned in).
              if AQtyToStoreRec.JobCode <> '' then
              begin
                // 20013196 Handling of link-job-quantities.
                AddUpdateTempList(ADrawObject.AWorkspotCode,
                  AQtyToStoreRec.JobCode);
                // When there is a link-job, then store the same quantity for
                // the link-job.
                AJobCodeRec :=
                  ADrawObject.FindJobCodeListRec(AQtyToStoreRec.JobCode);
                if Assigned(AJobCodeRec) then
                  if (AJobCodeRec.Link_WorkspotCode <> '') and
                    (AJobCodeRec.Link_JobCode <> '') then
                  begin
// TESTING
{$IFDEF DEBUG2}
  WDebugLog('LinkJob: ' +
    AJobCodeRec.Link_WorkspotCode +
    '/' +
    AJobCodeRec.Link_JobCode);
{$ENDIF}
                    AddUpdateTempList(AJobCodeRec.Link_WorkspotCode,
                      AJobCodeRec.Link_JobCode);
                  end;
              end;
            end; // for
          end; // if
        end; // if
      end; // for
    except
      on E:EOracleError do
      begin
        WErrorLog('CreateQtyToStoreTempList.for: ' + E.Message);
      end;
      on E:Exception do
      begin
        WErrorLog('CreateQtyToStoreTempList.for: ' + E.Message);
      end;
    end;
  finally
    // Make a copy for later use
    cdsQtyToStoreCopyToWork;
// TESTING
{$IFDEF DEBUG3}
    with cdsQtyToStore do
    begin
      First;
      while not Eof do
      begin
        WLog(
          FieldByName('STARTTIME').AsString + ';' +
          FieldByName('PLANT_CODE').AsString + ';' +
          FieldByName('WORKSPOT_CODE').AsString + ';' +
          FieldByName('JOB_CODE').AsString + ';' +
          FieldByName('SHIFT_NUMBER').AsString + ';' +
          FieldByName('QUANTITY').AsString + ';' +
          FieldByName('COUNTER_VALUE').AsString + ';' +
          FieldByName('ENDTIME').AsString + ';' +
          FieldByName('SHIFT_DATE').AsString
          );
        Next;
      end;
    end; // try
  WDebugLog('CreateQtyToStoreTempList - End');
{$ENDIF}
  end; // try
end; // CreateQtyToStoreTempList

// PIM-12
function TPersonalScreenDM.DetermineShiftDate(APSList: TList; APlantCode,
  AWorkspotCode: String; AShiftNumber: Integer; AStartDate,
  AEndDate: TDateTime): TDateTime;
var
  DrawObjectTemp: PDrawObject;
  IDCard: TScannedIDCard;
  Startdatetime, Enddatetime: TDateTime;
begin
  DrawObjectTemp := FindDrawObject(APSList, APlantCode, AWorkspotCode);
  IDCard.EmployeeCode := -1;
  IDCard.PlantCode := APlantCode;
  IDCard.ShiftNumber := AShiftNumber;
  IDCard.DepartmentCode := '';
  if Assigned(DrawObjectTemp) then
    if Assigned(DrawObjectTemp.AWorkspotTimeRec) then
      IDCard.DepartmentCode := DrawObjectTemp.AWorkspotTimeRec.ADepartmentCode;
  IDCard.DateIn := AStartDate;
  IDCard.DateOut := AEndDate;

  Startdatetime := AStartDate;
  Enddatetime := AEndDate;
  AProdMinClass.GetShiftDay(IDCard, Startdatetime, Enddatetime);
  Result := Trunc(Startdatetime);
end; // DetermineShiftDate

// Write records from clientdataset to ProductionQuantity-Table.
procedure TPersonalScreenDM.UpdateProductionQuantityTable(APSList: TList); // PIM-114
var
  ShiftDate: TDateTime; // PIM-114
begin
  try
    try
      with cdsQtyToStore do
      begin
        First;
        while not Eof do
        begin
          ShiftDate := DetermineShiftDate(
            APSList,
            FieldByName('PLANT_CODE').AsString,
            FieldByName('WORKSPOT_CODE').AsString,
            FieldByName('SHIFT_NUMBER').AsInteger,
            FieldByName('STARTTIME').AsDateTime,
            FieldByName('ENDTIME').AsDateTime
            ); // PIM-114
          MergeCheckPQ(
            FieldByName('STARTTIME').AsDateTime,
            FieldByName('PLANT_CODE').AsString,
            FieldByName('WORKSPOT_CODE').AsString,
            FieldByName('JOB_CODE').AsString,
            FieldByName('SHIFT_NUMBER').AsInteger,
            FieldByName('ENDTIME').AsDateTime,
            FieldByName('QUANTITY').AsFloat,
            FieldByName('COUNTER_VALUE').AsFloat,
            ShiftDate // FieldByName('SHIFT_DATE').AsDateTime // PIM-114
            );
          Next;
        end;
      end;
    except
      on E:EOracleError do
      begin
        WErrorLog(E.Message);
      end;
      on E:Exception do
      begin
        WErrorLog(E.Message);
      end;
    end;
  finally
  end;
end; // UpdateProductionQuantityTable

// PIM-12
// Write data based on clientdataset to BI-table
procedure TPersonalScreenDM.UpdateWorkspotEffPerMinuteTable(
  APSList: TList);
var
  ADrawObject: PDrawObject;
  AJobCodeRec: PJobCodeRec;
  ShiftDate: TDateTime;
  StartTime, EndTime: TDateTime;
  NormLevel, SecondsNorm: Double;
begin
  // Look from the last whole minute till now.
  // Use RoundTime to see if the minute must be rounded
  EndTime := RoundTime(Now, 1);
  StartTime := EndTime - 1/24/60; // 1 minute earlier
  try
    try
      with cdsQtyToStore do
      begin
        First;
        while not Eof do
        begin
          ShiftDate := DetermineShiftDate(
            APSList,
            FieldByName('PLANT_CODE').AsString,
            FieldByName('WORKSPOT_CODE').AsString,
            FieldByName('SHIFT_NUMBER').AsInteger,
            StartTime,
            EndTime
            );
          ADrawObject := FindDrawObject(APSList,
            FieldByName('PLANT_CODE').AsString,
            FieldByName('WORKSPOT_CODE').AsString);
          // PIM-159 Related to this: Be sure it is assigned!
          if Assigned(ADrawObject) then
          begin
            AJobCodeRec :=
              ADrawObject.FindJobCodeListRec(FieldByName('JOB_CODE').AsString);
            NormLevel := AJobCodeRec.Norm_prod_level;
            // PIM-151 Allow to store quantities with norm = 0
            if (FieldByName('QUANTITY').AsFloat > 0) then
            begin
              // The secondsnorm will be changed during update
              if NormLevel > 0 then
                SecondsNorm :=
                  FieldByName('QUANTITY').AsFloat * 3600 / NormLevel
              else
                SecondsNorm := 0;
              RealTimeEffDM.MergeWorkspotEffPerMinute(
                ShiftDate,
                FieldByName('SHIFT_NUMBER').AsInteger,
                FieldByName('PLANT_CODE').AsString,
                FieldByName('WORKSPOT_CODE').AsString,
                FieldByName('JOB_CODE').AsString,
                StartTime,
                EndTime,
                0,
                SecondsNorm,
                FieldByName('QUANTITY').AsFloat,
                NormLevel,
                StartTime,
                EndTime
                );
            end; // if
          end; // if
          Next;
        end;
      end;
    except
      on E:EOracleError do
      begin
        WErrorLog(E.Message);
      end;
      on E:Exception do
      begin
        WErrorLog(E.Message);
      end;
    end;
  finally
    LastTimeInsertWorkspotPerMinute := EndTime;
  end;
end; // UpdateWorkspotEffPerMinuteTable

// Update Counter Value Table
procedure TPersonalScreenDM.UpdateCounterValueTable(APSList: TList);
var
  I, J: Integer;
  ADrawObject: PDrawObject;
  AQtyToStoreRec: PQtyToStoreRec;
  procedure DeleteCVRecords;
  begin
    try
      with oqDeleteCV do
      begin
        ClearVariables;
        SetVariable('PLANT_CODE',    ADrawObject.APlantCode);
        SetVariable('WORKSPOT_CODE', ADrawObject.AWorkspotCode);
        Execute;
      end;
    except
      on E:EOracleError do
      begin
        WErrorLog(E.Message);
      end;
      on E:Exception do
      begin
        WErrorLog(E.Message);
      end;
    end;
  end; // DeleteCVRecords
  procedure InsertCVRecord;
  begin
    try
      with oqInsertCV do
      begin
        ClearVariables;
        SetVariable('PLANT_CODE',       ADrawObject.APlantCode);
        SetVariable('COUNTER_DATETIME', AQtyToStoreRec.Timestamp);
        SetVariable('WORKSPOT_CODE',    ADrawObject.AWorkspotCode);
        SetVariable('COUNTER_VALUE',    AQtyToStoreRec.Countervalue);
        SetVariable('JOB_CODE',         AQtyToStoreRec.JobCode);
        SetVariable('MUTATOR',          ORASystemDM.CurrentProgramUser);
        SetVariable('IGNORE_YN',        'N');
        SetVariable('SHIFT_NUMBER',     AQtyToStoreRec.ShiftNumber);
        Execute;
      end;
    except
      on E:EOracleError do
      begin
        WErrorLog(E.Message);
      end;
      on E:Exception do
      begin
        WErrorLog(E.Message);
      end;
    end;
  end; // InsertCVRecord
begin
  try
    try
      // For each plant+workspot...
      for I := 0 to APSList.Count - 1 do
      begin
        ADrawObject := APSList.Items[I];
        if Assigned(ADrawObject.QtyToStoreList) then
        begin
          if ADrawObject.QtyToStoreList.Count > 0 then
          begin
            ADrawObject.QtyToStoreList.Sort(DateCompare);
            for J := 0 to ADrawObject.QtyToStoreList.Count - 1 do
            begin
              AQtyToStoreRec := ADrawObject.QtyToStoreList.Items[J];
              // Only add/update when jobcode <> '', which can happen
              // when there is no current job (no employee who scanned in).
              if AQtyToStoreRec.JobCode <> '' then
              begin
                DeleteCVRecords;
                InsertCVRecord;
              end; // if
            end; // for
(*
          with cdsQtyToStore do
          begin
            Filtered := False;
            Filter := 'PLANT_CODE = ' +
              '''' + ADrawObject.APlantCode + '''' +
              ' AND WORKSPOT_CODE = ' +
              '''' + ADrawObject.AWorkspotCode + '''';
            Filtered := True;
            DeleteCVRecords;
            // Goto Last Element for last timestamp
            Last;
*)
          end; // if
        end; // if
      end; // for
    except
      on E:EOracleError do
      begin
        WErrorLog(E.Message);
      end;
      on E:Exception do
      begin
        WErrorLog(E.Message);
      end;
    end; // try
  finally
  end; // try
end; // UpdateCounterValueTable

// Action for writing all data to Database, for all workspots and jobs.
procedure TPersonalScreenDM.ActionWriteToDB(APSList: TList; ALogPath: String);
begin
  try
    CreateQtyToStoreTempList(APSList);
    IgnoreInterfaceCodeHandling(APSList); // SO-20015331
    UpdateLinkJobs(APSList); // TD-25016
    UpdateProductionQuantityTable(APSList); // PIM-114
    UpdateWorkspotEffPerMinuteTable(APSList); // PIM-12
    UpdateCounterValueTable(APSList);
    UpdateMachineHours(APSList); // SO-20014450
    UpdateMachineDownTimeHours(APSList); // SO-20014450
    if ORASystemDM.OracleSession.InTransaction then
      ORASystemDM.OracleSession.Commit;
    InitQtyToStoreList(APSList);
    DeleteQtyToStoreListFile(ALogPath);
  except
    on E:EOracleError do
    begin
      WErrorLog(E.Message);
    end;
    on E:Exception do
    begin
      WErrorLog(E.Message);
    end;
  end;
end; // ActionWriteToDB

// Action Update After Scan:
// - When an employee scans (in/out or end-of-day), then
//   the following must be trapped:
//   - The employee scans in to a job
//   - The employee scans out from a previous job and in to a new job
//   - The employee scans out from the job (end-of-day)
// - During this it is possible:
//   - The job is for the same workspot as 'drawobject'.
//   - The job is for a different workspot (as 'drawobject').
// Examples:
// - Employee 1 scans in on job A for workspot W1 (new-day)
// - Employee 1 scans from job A for workspot W1 to job B for workspot W1
// - Employee 1 scans from job B for workspot W1 to job C for workspot W2
// - Employee 1 scans out from job C for workspot W3 (end-of-day)
// Variables:
// - LastIDCard = previous job (plant/workspot/job are empty when no previous job)
// - NextIDCard = next (or new) job
function TPersonalScreenDM.ActionUpdateAfterScan(APSList: TList;
  ALastIDCard, ANextIDCard: TScannedIDCard): Boolean;
var
  ADrawObject: PDrawObject;
  ALastDrawObject, ANextDrawObject: PDrawObject;
  ALastJobCodeRec, ANextJobCodeRec: PJobCodeRec;
  AJobCodeRec: PJobCodeRec;
{  function FindDrawObject(APlantCode, AWorkspotCode: String): PDrawObject;
  var
    I: Integer;
  begin
    Result := nil;
    for I := 0 to APSList.Count - 1 do
    begin
      ADrawObject := APSList.Items[I];
      if Assigned(ADrawObject.AWorkspotTimeRec) then
      begin
        if (ADrawObject.APlantCode = APlantCode) and
          (ADrawObject.AWorkspotCode = AWorkSpotCode) then
        begin
          Result := ADrawObject;
          Break;
        end;
      end;
    end;
  end; }
  function FindLastCurrentJobCodeRec(ALastDrawObject: PDrawObject;
    ALastCurrentJobCode: String): PJobCodeRec;
  var
    I: Integer;
    AJobCodeRec: PJobCodeRec;
  begin
    Result := nil;
    if Assigned(ALastDrawObject) then
    begin
      if Assigned(ALastDrawObject.AJobCodeList) then
      begin
        for I := 0 to ALastDrawObject.AJobCodeList.Count - 1 do
        begin
          AJobCodeRec := ALastDrawObject.AJobCodeList[I];
          if (AJobCodeRec.JobCode = ALastCurrentJobCode) then
          begin
            Result := AJobCodeRec;
            Break;
          end; // if
        end; // for
        // If nothing was found, then use the NOJOB-job.
        // This can happen when there are quantities coming in
        // but no-one is scanned in at the moment.
        if not Assigned(Result) then
          Result := ALastDrawObject.FindJobCodeListRec(NOJOB);
      end; // if
    end; // if
  end; // FindLastCurrentCodeJobRec
  function FindJobDescription(AJobCode: String): String;
  var
    AJobCodeRec: PJobCodeRec;
  begin
    Result := '';
    AJobCodeRec := ADrawObject.FindJobCodeListRec(AJobCode);
    if Assigned(AJobCodeRec) then
      Result := AJobCodeRec.JobCodeDescription;
  end; // FindJobDescription
  function FindAnyEmployeeScannedIn(ADrawObject: PDrawObject): Boolean;
  var
    I: Integer;
    AEmployeeRec: PEmployeeRec;
  begin
    Result := False;
    if Assigned(ADrawObject) then
    begin
      if Assigned(ADrawObject.AEmployeeList) then
      begin
        for I := 0 to ADrawObject.AEmployeeList.Count - 1 do
        begin
          AEmployeeRec := ADrawObject.AEmployeeList[I];
          if AEmployeeRec.CurrentlyScannedIn then
          begin
            Result := True;
            Break;
          end; // if
        end; // for
      end; // if
    end; // if
  end; // FindAnyEmployeeScannedIn
  procedure ActionChangeCurrentJob;
  begin
    //
  end; // ActionChangeCurrentJob
begin
  Result := False; // PIM-50
  ALastDrawObject := nil;
  ADrawObject := nil;
  ALastJobCodeRec := nil;
  // First check if ALastIDCard and ANextIDCard are both empty,
  // if so, then exit.
  if (ALastIDCard.PlantCode = '') and (ALastIDCard.WorkSpotCode = '') and
    (ALastIDCard.JobCode = '') and
    (ANextIDCard.PlantCode = '') and (ANextIDCard.WorkSpotCode = '') and
    (ANextIDCard.JobCode = '') then
    Exit;
  //
  // Handle previous job (if exists) OR last scan (end-of-day)
  //
  // NOTE: This can also be the last scan (end-of-day) !
  //
  if not ((ALastIDCard.PlantCode = '') and (ALastIDCard.WorkSpotCode = '') and
    (ALastIDCard.JobCode = '')) then
  begin
    // Previous job
    // Find workspot belonging to last (previous) job
    // This can be from a different workspot!
    ADrawObject := FindDrawObject(APSList, ALastIDCard.PlantCode,
      ALastIDCard.WorkSpotCode);
    if Assigned(ADrawObject) then
    begin
      if Assigned(ADrawObject.AWorkspotTimeRec) then // PIM-159
        ADrawObject.AWorkspotTimeRec.ALastJobCode :=
          ADrawObject.AWorkspotTimerec.ACurrentJobCode;
      ADrawObject.AJobChanged := False; // PIM-155
      ALastDrawObject := ADrawObject;
      // Find last current job code record for later use (previous job)
      // Store it in a deep copy.
      ALastJobCodeRec := PJobCodeRec.Create;
      ALastJobCodeRec.Copy(FindLastCurrentJobCodeRec(
        ALastDrawObject, ALastDrawObject.AWorkspotTimeRec.ACurrentJobCode));
      ALastIDCard.Job := FindJobDescription(ALastIDCard.JobCode);
      // Add or update employeelist - this also updates job-records.
      ADrawObject.AddUpdateEmployeeList(ALastIDCard.EmployeeCode,
        ALastIDCard.EmplName, ALastIDCard.ShiftNumber,
        '', // TeamCode
        '', // Level
        ALastIDCard.DateIn, ALastIDCard.DateOut,
        'Y', // ProcessedYN
        ALastIDCard.JobCode,
        ALastIDCard.Job,
        False);
      // If no-one is scanned-in now on this workspot, then
      // assign the last job's last-counter-value to nojob.
      if not FindAnyEmployeeScannedIn(ADrawObject) then
      begin
        AJobCodeRec := ADrawObject.FindJobCodeListRec(NOJOB);
        if Assigned(AJobCodeRec) and Assigned(ALastJobCodeRec) then
          AJobCodeRec.Last_Counter_Value :=
            ALastJobCodeRec.Last_Counter_Value;
        ADrawObject.AJobChanged := True;
        Result := True;
      end;
    end; // if
  end; // if
  //
  // Handle next/new or last job (end-of-day)
  //
  if ANextIDCard.Job = SpimsEndDay then
  begin
    //
    // End of day (last scan)
    //
    // NOTE: When end-of-day, then there is NO next plantcode / workspotcode!
    //       Then NextIDCard is empty for plantcode and workspotcode!
    //       Only LastIDCard will be filled, and this is handled already.
    //
    // There is NO next job!
{
    ADrawObject := FindDrawObject(APSList, ANextIDCard.PlantCode,
      ANextIDCard.WorkSpotCode);
}
    if Assigned(ADrawObject) then
    begin
(*
      // Find last current job code record (previous job)
      // This is determined earlier to prevent something is changed in between.
{      ALastJobCodeRec := FindLastCurrentJobCodeRec(
        ALastDrawObject, ALastDrawObject.AWorkspotTimeRec.ACurrentJobCode); }
      ANextIDCard.Job := FindJobDescription(ANextIDCard.JobCode);
      // Add or update employeelist - this also updates job-records.
      ADrawObject.AddUpdateEmployeeList(ANextIDCard.EmployeeCode,
        ANextIDCard.EmplName, ANextIDCard.ShiftNumber,
        '', // TeamCode
        '', // Level
        ANextIDCard.DateIn, ANextIDCard.DateOut,
        'Y', // ProcessedYN
        ANextIDCard.JobCode,
        ANextIDCard.Job,
        False);
*)
      // Change current job to last current job because this
      // employee scanned out.
      // Also check if there is still an other employee scanned in.
      if Assigned(ALastJobCodeRec) and
        FindAnyEmployeeScannedIn(ADrawObject) then
      begin
        ADrawObject.AWorkspotTimeRec.ACurrentJobCode := ALastJobCodeRec.JobCode;
        ADrawObject.AWorkspotTimeRec.ACurrentJobDescription := ALastJobCodeRec.JobCodeDescription;
        // Set this to True, which means it will adjust
        // the job.last_counter_value to blackbox.last_counter_value
        // when reading the counters.
        ADrawObject.AJobChanged := True;
        Result := True;
//        ADrawObject.ALastCounterReadTime := 0;
      end
      else
      begin
        // There is no current job anymore: Assign to NOJOB
//        ADrawObject.AWorkspotTimeRec.ACurrentJobCode := NOJOB;
//        ADrawObject.AWorkspotTimeRec.ACurrentJobDescription :=
//          SPimsNoJobDescription;
        // Be sure this job gets the last counter value of previous job,
        // or the adding of the quantity will go wrong!
        AJobCodeRec := ADrawObject.FindJobCodeListRec(NOJOB);
        if Assigned(AJobCodeRec) and Assigned(ALastJobCodeRec) then
          AJobCodeRec.Last_Counter_Value :=
            ALastJobCodeRec.Last_Counter_Value;
        // Assign '' now, because there is no current job.
        ADrawObject.AWorkspotTimeRec.ACurrentJobCode := '';
        ADrawObject.AWorkspotTimeRec.ACurrentJobDescription := '';
        //
        ADrawObject.AJobChanged := True;
        Result := True;
//        ADrawObject.ALastCounterReadTime := 0;
      end;
    end;
  end // if end-of-day
  else
  begin // next/new job
    //
    // Next/New job - Can be a job of SAME or DIFFERENT workspot!
    //
    // Find workspot belonging to next/new job
    ADrawObject := FindDrawObject(APSList, ANextIDCard.PlantCode,
      ANextIDCard.WorkSpotCode);
    if Assigned(ADrawObject) then
    begin
      ANextDrawObject := ADrawObject;
      //
      // New job for same  or different workspot?
      //
      // First get next job
      ANextJobCodeRec :=
        ANextDrawObject.FindJobCodeListRec(ANextIDCard.JobCode);
      // Find last current job code record, when there was a previous/last job
      if Assigned(ALastDrawObject) then // Was there a previous job?
      begin
         // This is done earlier! To prevent something is changed in-between.
{        ALastJobCodeRec := FindLastCurrentJobCodeRec(
          ALastDrawObject, ALastDrawObject.AWorkspotTimeRec.ACurrentJobCode); }
        if Assigned(ALastJobCodeRec) then
        begin
          if Assigned(ANextJobCodeRec) then
          begin
            // Do this only when workspot is the same
            if (ALastDrawObject.APlantCode = ANextDrawObject.APlantCode) and
              (ALastDrawObject.AWorkspotCode = ANextDrawObject.AWorkspotCode) then
            begin
              // Next job gets last counter value from previous current job
              ANextJobCodeRec.Last_Counter_Value :=
                ALastJobCodeRec.Last_Counter_Value;
            end
            else
            begin
              // Different/other workspot
              // Find last current job for next workspot
              AJobCodeRec := FindLastCurrentJobCodeRec(
                ANextDrawObject, ANextDrawObject.AWorkspotTimeRec.ACurrentJobCode);
              if Assigned(AJobCodeRec) then
              begin
                // Next job gets last counter value from previous current job
                ANextJobCodeRec.Last_Counter_Value :=
                  AJobCodeRec.Last_Counter_Value;
              end; // if
            end; // if
          end; // if
        end; // if
      end // if
      else
      begin
        // There was no previous job
        // Find last current job for next workspot
        if Assigned(ANextJobCodeRec) then
        begin
          AJobCodeRec := FindLastCurrentJobCodeRec(
            ANextDrawObject, ANextDrawObject.AWorkspotTimeRec.ACurrentJobCode);
          if Assigned(AJobCodeRec) then
          begin
            // Next job gets last counter value from previous current job
            ANextJobCodeRec.Last_Counter_Value :=
              AJobCodeRec.Last_Counter_Value;
          end; // if
        end; // if
      end; // if

      // Change current job
      //
      // PIM-147 Do not do this here! It is done during 'AddUpdateEmployeeList'
//      ADrawObject.AWorkspotTimeRec.ACurrentJobCode := ANextIDCard.JobCode;
//      ADrawObject.AWorkspotTimeRec.ACurrentJobDescription := ANextIDCard.Job;
//      ADrawObject.AJobChanged := True;
      Result := True;
//      ADrawObject.ALastCounterReadTime := 0;
      // Change current shift
      // Action when shift changes
      if ADrawObject.AWorkspotTimeRec.ACurrentShiftNumber <>
        ANextIDCard.ShiftNumber then
      begin
        // Reset all shift counters
        ADrawObject.ResetShiftCounters;
        ADrawObject.AWorkspotTimeRec.ACurrentShiftStart := ANextIDCard.DateIn;
      end;
      ADrawObject.AWorkspotTimeRec.ACurrentShiftNumber := ANextIDCard.ShiftNumber;

      ANextIDCard.Job := FindJobDescription(ANextIDCard.JobCode);
      // Add or update employeelist - this also updates job-records.
      ADrawObject.AddUpdateEmployeeList(ANextIDCard.EmployeeCode,
        ANextIDCard.EmplName, ANextIDCard.ShiftNumber,
        '', // TeamCode
        '', // Level
        ANextIDCard.DateIn, ANextIDCard.DateOut,
        'N', // ProcessedYN
        ANextIDCard.JobCode,
        ANextIDCard.Job,
        False);
      ADrawObject.AJobChanged := ALastIDCard.JobCode <> ANextIDCard.JobCode; // PIM-155
    end; // if
  end; // next/new job
  //
  // NOTE: When there are no employees anymore currently scanned in
  //       on this workspot, then reset the 'shift'-counters!
  //
  // DO NOT DO THIS: When an employee was scanned in on a shift
  //                 and temporary scans out for lunch, and later continues
  //                 on the same shift, then we need to know the total time
  //                 of employee(s) on that shift.
  //                 By restting this, it will not be known anymore!
{
  if Assigned(ADrawObject) then
    if ADrawObject.ACurrentNumberOfEmployees = 0 then
    begin
      ResetShiftCounters;
      if Assigned(ADrawObject.AWorkspotTimeRec) then
      begin
        ADrawObject.AWorkspotTimeRec.ACurrentShiftNumber := 0;
        ADrawObject.AWorkspotTimeRec.ACurrentShiftStart := 0;
      end;
    end;
}
{$IFDEF DEBUG3}
  WDebugLog('LastIDCard=' +
    ' Emp=' + IntToStr(ALastIDCard.EmployeeCode) + ';' +
    ' P=' + ALastIDCard.PlantCode + ';' +
    ' W=' + ALastIDCard.WorkSpotCode + ';' + ALastIDCard.WorkSpot + ';' +
    ' J=' + ALastIDCard.JobCode + '; (' + ALastIDCard.Job + ');' +
    ' Sh=' + IntToStr(ALastIDCard.ShiftNumber)
    );
  WDebugLog('NextIDCard=' +
    ' Emp=' + IntToStr(ANextIDCard.EmployeeCode) + ';' +
    ' P=' + ANextIDCard.PlantCode + ';' +
    ' W=' + ANextIDCard.WorkSpotCode + ';' + ANextIDCard.WorkSpot + ';' +
    ' J=' + ANextIDCard.JobCode + '; (' + ANextIDCard.Job + ');' +
    ' Sh=' + IntToStr(ANextIDCard.ShiftNumber)
    );
  if Assigned(ADrawObject) then
    if Assigned(ADrawObject.AWorkspotTimeRec) then
      WDebugLog('CurrentJob=' + ADrawObject.AWorkspotTimeRec.ACurrentJobCode +
        ' (' + ADrawObject.AWorkspotTimeRec.ACurrentJobDescription + ')'
        );
{$ENDIF}
  // PIM-12 Reset after job-changed
  if Assigned(ADrawObject) then
    if ADrawObject.AJobChanged then
    begin
      ADrawObject.InitFifoList(False);
      ADrawObject.CompareJobInitFifoList(APSList); // PIM-155
    end;
  // This was used to get a deep copy, so free it here.
  if Assigned(ALastJobCodeRec) then
    ALastJobCodeRec.Free;
end; // ActionUpdateAfterScan

// Add missing job for workspot
procedure TPersonalScreenDM.ActionAddMissingJobForWorkspot(APlantCode,
  AWorkspotCode, AJobCode, AJobDescription: String);
begin
  try
    oqCheckJob.ClearVariables;
    oqCheckJob.SetVariable('PLANT_CODE',    APlantCode);
    oqCheckJob.SetVariable('WORKSPOT_CODE', AWorkspotCode);
    oqCheckJob.SetVariable('JOB_CODE',      AJobCode);
    oqCheckJob.Execute;
    if oqCheckJob.Eof then
    begin
      // Add the job for the workspot.
      oqInsertJob.ClearVariables;
      oqInsertJob.SetVariable('PLANT_CODE',    APlantCode);
      oqInsertJob.SetVariable('WORKSPOT_CODE', AWorkspotCode);
      oqInsertJob.SetVariable('JOB_CODE',      AJobCode);
      oqInsertJob.SetVariable('DESCRIPTION',   AJobDescription);
      oqInsertJob.SetVariable('MUTATOR',       ORASystemDM.CurrentProgramUser);
      oqInsertJob.Execute;
      if ORASystemDM.OracleSession.InTransaction then
        ORASystemDM.OracleSession.Commit;
    end; // if
  except
    on E: Exception do
    begin
      if ORASystemDM.OracleSession.InTransaction then
        ORASystemDM.OracleSession.Rollback;
      WErrorLog(E.Message);
    end;
  end; // except
end; // ActionAddMissingJobForWorkspot

// Handle Last Qty To Store file:
// - Read the last qty-to-store-file and
//   write the contents to PQ-table.
// - Afterwards delete the qty-to-store-file.
procedure TPersonalScreenDM.ActionHandleLastQtyToStoreFile(ALogPath: String);
{procedure TForm3.Button1Click(Sender: TObject);
var
  MySettings: TFormatSettings;
  s: string;
  d: TDateTime;
begin
  GetLocaleFormatSettings(GetUserDefaultLCID, MySettings);
  MySettings.DateSeparator := '-';
  MySettings.TimeSeparator := ':';
  MySettings.ShortDateFormat := 'mm-dd-yyyy';
  MySettings.ShortTimeFormat := 'hh:nn:ss';

  s := DateTimeToStr(Now, MySettings);
  ShowMessage(s);
  d := StrToDateTime(s, MySettings);
  ShowMessage(DateTimeToStr(d, MySettings));
end;}
var
  MyFileName, Line, Field: String;
  MySettings: TFormatSettings;
  TimeStamp, StartDate, EndDate: TDateTime;
  PlantCode, WorkspotCode, JobCode: String;
  ShiftNumber: Integer;
  Quantity, CounterValue: Integer;
  ShiftDate: TDateTime;
  TempList: TStringList;
  LineI: Integer;
  OK: Boolean;
  // Extract a field from a given line.
  // The line will be truncated, the part that has been
  // found is taken from the line.
  function ExtractFieldSep(VAR ALine: String; ASeparator: String): String;
  var
    i: Integer;
    Part: String;
  begin
    i := Pos(ASeparator, ALine);
    if i > 0 then
    begin
      Part := Copy(ALine, 1, i-1);
      ALine := Copy(ALine, i+1, Length(ALine));
    end
    else
    begin
      Part := ALine;
      ALine := '';
    end;
    Result := Part;
  end;
begin
{$IFDEF DEBUG3}
WDebugLog('ActionHandleLastQtyToStoreFile');
{$ENDIF}
  OK := False;
  MyFileName := ALogPath +
    ORASystemDM.CurrentComputerName + '_' + QTYTOSTOREFILENAME;
  if FileExists(MyFileName) then
  begin
    GetLocaleFormatSettings(LOCALE_SYSTEM_DEFAULT, MySettings);
    MySettings.DateSeparator := '-';
    MySettings.TimeSeparator := ':';
    MySettings.ShortDateFormat := 'dd-mm-yyyy';
    MySettings.ShortTimeFormat := 'hh:nn:ss';
    TempList := TStringList.Create;
    try
      TempList.LoadFromFile(MyFileName);
      for LineI := 0 to TempList.Count - 1 do
      begin
        Line := TempList.Strings[LineI];
        if Line <> '' then
        begin
          try
            // Get all the fields and put them in variables.
            Field := ExtractFieldSep(Line, ';');
            TimeStamp := StrToDateTime(Field, MySettings);
            PlantCode := ExtractFieldSep(Line, ';');
            WorkspotCode := ExtractFieldSep(Line, ';');
            JobCode := ExtractFieldSep(Line, ';');
            if JobCode = '' then
              JobCode := NOJOB;
            ShiftNumber := StrToInt(ExtractFieldSep(Line, ';'));
            Quantity := StrToInt(ExtractFieldSep(Line, ';'));
            CounterValue := StrToInt(ExtractFieldSep(Line, ';'));
            StartDate := DateTimeTruncToInterval(Timestamp, PIMS_TIME_INTERVAL);
            EndDate := IncMinute(StartDate, 5);
            if ORASystemDM.UseShiftDateSystem then
            begin
              // 20013489
              try
                Field := ExtractFieldSep(Line, ';');
                ShiftDate := Trunc(StrToDate(Field, MySettings));
              except
                ShiftDate := NullDate;
              end;
            end
            else
              ShiftDate := Trunc(StartDate);
            // 20013489
            if ShiftDate = NullDate then
              ShiftDate := Trunc(StartDate);
{$IFDEF DEBUG3}
WDebugLog('-> ShiftDate=' + DateToStr(ShiftDate));
{$ENDIF}
            if not MergeCheckPQ(StartDate, PlantCode, WorkspotCode, JobCode,
              ShiftNumber, EndDate, Quantity, CounterValue, ShiftDate) then
            begin
              OK := False;
              Break;
            end;
            OK := True;
          except
            on E:EOracleError do
            begin
              WErrorLog(E.Message);
            end;
            on E:Exception do
            begin
              WErrorLog(E.Message);
            end;
          end; // except
        end; // if
      end; // for
      if OK then
        ActionDeleteFile(MyFileName);
    finally
      TempList.Free;
    end;
  end;
end; // ActionHandleLastQtyToStoreFile

{
// Here quantity is NOT added to existing records!
function TPersonalScreenDM.MergePQ(AStartDate: TDateTime;
  APlantCode, AWorkspotCode, AJobCode: String;
  AShiftNumber: Integer;
  AEndDate: TDateTime;
  AQuantity, ACounterValue: Double): Boolean;
begin
  Result := False;
  try
    with oqMergePQ do
    begin
      ClearVariables;
      SetVariable('START_DATE',         AStartDate);
      SetVariable('PLANT_CODE',         APlantCode);
      SetVariable('WORKSPOT_CODE',      AWorkspotCode);
      SetVariable('JOB_CODE',           AJobCode);
      SetVariable('SHIFT_NUMBER',       AShiftNumber);
      SetVariable('MANUAL_YN',          'N');
      SetVariable('END_DATE',           AEndDate);
      SetVariable('QUANTITY',           AQuantity);
      SetVariable('COUNTER_VALUE',      ACounterValue);
      SetVariable('AUTOMATIC_QUANTITY', AQuantity);
      SetVariable('MUTATOR',            ORASystemDM.CurrentProgramUser);
      Execute;
      Result := True;
    end; // with
  except
    on E:EOracleError do
    begin
      WErrorLog(E.Message);
    end;
    on E:Exception do
    begin
      WErrorLog(E.Message);
    end;
  end; // except
end; // MergePQ
}

// Here quantity only merged when countervalue is different from existing
// PQ-record.
function TPersonalScreenDM.MergeCheckPQ(AStartDate: TDateTime; APlantCode,
  AWorkspotCode, AJobCode: String; AShiftNumber: Integer;
  AEndDate: TDateTime; AQuantity, ACounterValue: Double;
  AShiftDate: TDateTime): Boolean;
var
  IsLinkJob: Boolean; // 20013196
begin
  Result := False;
  IsLinkJob := Assigned(FindLinkJobList(APlantCode, AWorkspotCode, AJobCode)); // 20013196
  try
    // Check if the record already exists, which can happen
    // when existing 'countervalue' is the same or higher (>=).
    // This is needed to prevent it will change the record again, but
    // with a lower value.
    // Only if the new countervalue is higher, then it can update
    // the PQ-record.
    // 20013196 When it is a link-job, do not compare with highest counter value!
    //          Reason: The countervalue can come from multiple jobs that are
    //                  linked to link-job. But the total of the quantities for
    //                  these multiple jobs must be stored here!
    with oqCheckPQ do
    begin
      ClearVariables;
      SetVariable('START_DATE',         AStartDate);
      SetVariable('PLANT_CODE',         APlantCode);
      SetVariable('WORKSPOT_CODE',      AWorkspotCode);
      SetVariable('JOB_CODE',           AJobCode);
      SetVariable('SHIFT_NUMBER',       AShiftNumber);
      SetVariable('COUNTER_VALUE',      ACounterValue);
      SetVariable('LINK_JOB',           IsLinkJob); // 20013196
      Execute;
      if not Eof then
      begin
{$IFDEF DEBUG2}
WDebugLog('MergePQ: StartDate=' + DateTimeToStr(AStartDate) +
  ' P=' + APlantCode + ' W=' + AWorkspotCode + ' J=' + AJobCode +
  ' Qty= ' + IntToStr(Round(AQuantity)) +
  ' CV=' + IntToStr(Round(ACounterValue)) + ' - NO Merge');
{$ENDIF}
        Result := True;
        Exit; // Do NOT merge!
      end;
    end;
{$IFDEF DEBUG2}
WDebugLog('MergePQ: StartDate=' + DateTimeToStr(AStartDate) +
  ' P=' + APlantCode + ' W=' + AWorkspotCode + ' J=' + AJobCode +
  ' Qty=' + IntToStr(Round(AQuantity)) +
  ' CV=' + IntToStr(Round(ACounterValue)) + ' - Merge');
{$ENDIF}
    with oqMergeAddPQ do
    begin
      // TODO 21191. Skip earlier.
(*
      // TODO 21191. All bigger than 10000: skip.
      if AQuantity > 10000 then
      begin
        // Do not store this value in PQ-record.
        // Only log something.
        WLog('PQ-Skip: SD=' + DateTimeToStr(AStartDate) +
          ' ED=' + DateTimeToStr(AEndDate) +
          ' P=' + APlantCode + ' W=' + AWorkspotCode + ' J=' + AJobCode +
          ' Q=' + IntToStr(Round(AQuantity)) +
          ' CV=' + IntToStr(Round(ACounterValue))
         );
      end
      else
*)
      begin
        ClearVariables;
        SetVariable('START_DATE',         AStartDate);
        SetVariable('PLANT_CODE',         APlantCode);
        SetVariable('WORKSPOT_CODE',      AWorkspotCode);
        SetVariable('JOB_CODE',           AJobCode);
        SetVariable('SHIFT_NUMBER',       AShiftNumber);
        SetVariable('MANUAL_YN',          'N');
        SetVariable('END_DATE',           AEndDate);
        SetVariable('QUANTITY',           AQuantity);
        SetVariable('COUNTER_VALUE',      ACounterValue);
        SetVariable('AUTOMATIC_QUANTITY', AQuantity);
        SetVariable('MUTATOR',            ORASystemDM.CurrentProgramUser);
        SetVariable('SHIFT_DATE',         AShiftDate);
        Execute;
      end;
      Result := True;
    end; // with
  except
    on E:EOracleError do
    begin
      WErrorLog(E.Message);
    end;
    on E:Exception do
    begin
      WErrorLog(E.Message);
    end;
  end; // except
end; // MergeCheckPQ

procedure TPersonalScreenDM.ActionDeleteFile(AFilename: String);
begin
  try
    SysUtils.DeleteFile(AFilename);
  except
    on E:Exception do
    begin
      WErrorLog(E.Message);
    end;
  end; // except
end; // ActionDeleteFile

procedure TPersonalScreenDM.DeleteQtyToStoreListFile(ALogPath: String);
var
  MyFilename: String;
begin
  MyFilename := ALogPath +
    ORASystemDM.CurrentComputerName + '_' + QTYTOSTOREFILENAME;
  try
    if FileExists(MyFileName) then
      ActionDeleteFile(MyFilename);
  except
    // Ignore error
  end;
end; // DeleteQtyToStoreListFile

// Detect if there are any compare jobs.
// If so, the get the first job with a norm as default job.
function TPersonalScreenDM.DetectCompareJob(APlantCode,
  AWorkspotCode: String; var AJobCode, AJobDescription: String;
  var AEmpsScanIn: Boolean): Boolean;
begin
  Result := False;
  AJobCode := '';
  AEmpsScanIn := False;
  try
    // TD-24819 Only look for jobs with an interface code!
    with oqCheckCompareJob do
    begin
      ClearVariables;
      SetVariable('PLANT_CODE',         APlantCode);
      SetVariable('WORKSPOT_CODE',      AWorkspotCode);
      Execute;
      if not Eof then
      begin
        AJobCode := FieldAsString('JOB_CODE');
        AJobDescription := FieldAsString('DESCRIPTION');
        AEmpsScanIn := FieldAsString('EMPS_SCAN_IN_YN') = 'Y';
        Result := True;
      end;
    end;
  except
  end;
end; // DetectCompareJob

procedure TPersonalScreenDM.RefreshEmployeeList(var APSList,
  AReadCounterList: TList);
begin
  DataReadAll(APSList, AReadCounterList, True);
end; // RefreshEmployeeList

// TD-24819
// Check for compare jobs and reassign the compare job based on
// employees who scanned on the jobs that are compared.
// When no-one is scanned in yet, then assign NOJOB.
procedure TPersonalScreenDM.ReassignCompareJob(var APSList: TList);
var
  ADrawObject, ADrawObject2: PDrawObject;
  I, J: Integer;
  ACompareJobRec: PCompareJobRec;
  NewJobCode, NewJobDescription: String;
begin
  for I := 0 to APSList.Count - 1 do
  begin
    ADrawObject := APSList.Items[I];
    if Assigned(ADrawObject.AWorkspotTimeRec) then
    begin
      if ADrawObject.AWorkspotTimeRec.AIsCompareWorkspot and
        (not ADrawObject.AWorkspotTimeRec.AEmpsScanIn) then
      begin
        if Assigned(ADrawObject.ACompareJobList) then
        begin
          NewJobCode := NOJOB;
          NewJobDescription := SPimsNoJobDescription;
          for J := 0 to ADrawObject.ACompareJobList.Count - 1 do
          begin
            ACompareJobRec := ADrawObject.ACompareJobList.Items[J];
            ADrawObject2 := FindDrawObject(APSList, ADrawObject.APlantCode,
              ACompareJobRec.WorkspotCode);
            if Assigned(ADrawObject2) then
            begin
              if Assigned(ADrawObject2.AWorkspotTimeRec) then
              begin
                if (ADrawObject2.APlantCode = ADrawObject.APlantCode) and
                  (ADrawObject2.AWorkspotCode = ACompareJobRec.WorkspotCode) then
                begin
                  if (ADrawObject2.AWorkspotTimeRec.ACurrentJobCode =
                    ACompareJobRec.JobCode) then
                  begin
                    // Assign to compare job
                    NewJobCode := ADrawObject.AWorkspotTimeRec.ACompareJobCode;
                    NewJobDescription := ADrawObject.AWorkspotTimeRec.ACompareJobDescription;
                    Break; // Don't look further!
                  end;
                end; // if
              end; // if
            end; // if
          end; // for J
          // Assign found job here
          if NewJobCode = NOJOB then
          begin
            ADrawObject.AWorkspotTimeRec.ACurrentJobCode := '';
            ADrawObject.AWorkspotTimeRec.ACurrentJobDescription := '';
            ADrawObject.AWorkspotTimeRec.AJobButton.Caption := '';
          end
          else
          begin
            ADrawObject.AWorkspotTimeRec.ACurrentJobCode := NewJobCode;
            ADrawObject.AWorkspotTimeRec.ACurrentJobDescription := NewJobDescription;
          end;
        end; // with
      end; // if
    end; // if
  end; // for I
end; // ReassignCompareJob

// SO-20015331
procedure TPersonalScreenDM.DecreaseQtyByInterfaceCode(APSList: TList;
  AStartTime: TDateTime;
  AInterfaceCode: String; AQty: Double);
var
  ADrawObject{, ADrawObjectLink}: PDrawObject;
  AJobCodeRec{, AJobCodeRecLink}: PJobCodeRec;
  I, J: Integer;
begin
  for I := 0 to APSList.Count - 1 do
  begin
    ADrawObject := APSList.Items[I];
    if Assigned(ADrawObject.AJobCodeList) then
    begin
      for J := 0 to ADrawObject.AJobCodeList.Count - 1 do
      begin
        AJobCodeRec := ADrawObject.AJobCodeList[J];
        if AJobCodeRec.InterfaceCode = AInterfaceCode then
        begin
          // Decrease the qty
          cdsQtyToStoreUpdate(AStartTime,
            ADrawObject.APlantCode, ADrawObject.AWorkspotCode,
            AJobCodeRec.JobCode, AQty,
            True, False);
        end; // if
      end; // for
    end; // if
  end; // for
end; // DecreaseQtyByInterfaceCode

// SO-20015331
procedure TPersonalScreenDM.IgnoreInterfaceCodeHandling(APSList: TList);
var
  ADrawObject: PDrawObject;
  AIgnoreInterfaceCodeRec: PIgnoreInterfaceCodeRec;
  I: Integer;
  MyQuantity: Double;
  BookMarkStr: String;
begin
  // Look if there are any jobs with an ignore-interfacecode-list. If so,
  // then decrease the quantity for the jobs that must be ignored.
  try
    if Assigned(IgnoreInterfaceCodeList) then
      if IgnoreInterfaceCodeList.Count > 0 then
        for I := 0 to IgnoreInterfaceCodeList.Count - 1 do
        begin
          AIgnoreInterfaceCodeRec := IgnoreInterfaceCodeList.Items[I];
          ADrawObject := FindDrawObject(APSList,
            AIgnoreInterfaceCodeRec.PlantCode,
            AIgnoreInterfaceCodeRec.WorkspotCode);
          if Assigned(ADrawObject) then
          begin
//            MyQuantity := 0;
            // First find the qty of this job
            // TD-25016 There can be more than 1 record here!
            // So, use a filter to look them up.
            // ALSO: Use a work-copy for the loop, to prevent is looses the
            // last position because it has to update in-between.
            cdsQtyToStoreWork.Filtered := False;
            cdsQtyToStoreWork.Filter :=
              'PLANT_CODE = ' + '''' + ADrawObject.APlantCode + '''' +
              ' AND ' +
              'WORKSPOT_CODE = ' + '''' + ADrawObject.AWorkspotCode + '''' +
              ' AND ' +
              'JOB_CODE = ' + '''' + AIgnoreInterfaceCodeRec.JobCode + '''';
            cdsQtyToStoreWork.Filtered := True;
            cdsQtyToStoreWork.First;
            while not cdsQtyToStoreWork.Eof do
            begin
              MyQuantity := cdsQtyToStoreWork.FieldByName('QUANTITY').Value;
{$IFDEF DEBUG3}
WDebugLog('IgnoreIC: ' +
  'P/W/J=' + ADrawObject.APlantCode + ';' + ADrawObject.AWorkspotCode + ';' +
  AIgnoreInterfaceCodeRec.JobCode + ':' +
  ' MyQuantity=' + IntToStr(Round(MyQuantity))
  );
{$ENDIF}
            // Then subtract the quantity for the Interface codes that must be ignored
              if MyQuantity > 0 then
              begin
                BookMarkStr := cdsQtyToStoreWork.BookMark;
                DecreaseQtyByInterfaceCode(APSList,
                  cdsQtyToStoreWork.FieldByName('STARTTIME').AsDateTime,
                  AIgnoreInterfaceCodeRec.InterfaceCode, MyQuantity);
                 // Return to original record from while
                cdsQtyToStoreWork.BookMark := BookMarkStr;
              end;
              cdsQtyToStoreWork.Next;
            end; // while
            cdsQtyToStoreWork.Filtered := False;
          end; // if
        end; // for
  except
    on E:EOracleError do
    begin
      WErrorLog('IgnoreInterfaceCodeHandling: ' + E.Message);
    end;
    on E:Exception do
    begin
      WErrorLog('IgnoreInterfaceCodeHandling: ' + E.Message);
    end;
  end; // except
end; // IgnoreInterfaceCodeHandling

// TD-25016 In case there are ignore jobs used:
// Store the records for the link jobs again here, because
// it can be different when using ignore-jobs.
procedure TPersonalScreenDM.UpdateLinkJobs(APSList: TList);
var
  ADrawObject: PDrawObject;
  AJobCodeRec: PJobCodeRec;
  I: Integer;
  ALinkJobRec: PLinkJobRec;
  BookmarkStr: String;
  procedure RefreshTimeRecScanning; // 20015376.60
  var
    I: Integer;
  begin
    // Also read scans again for the link-job-workspot
    for I := 0 to APSList.Count - 1 do
    begin
      ADrawObject := APSList.Items[I];
      if Assigned(ADrawObject.AWorkspotTimeRec) then
        if ADrawObject.AWorkspotTimeRec.AIsLinkJobWorkspot then
        begin
          ADrawObject.Act_time_day := 0;
          ADrawObject.Act_time_shift := 0;
          ADrawObject.Act_time_current := 0;
          ADrawObject.Theo_time_day := 0;
          ADrawObject.Theo_time_shift := 0;
          ADrawObject.InitEmployeeList(False);
//          ADrawObject.InitEmployeeDisplayList(False); // 20014450.50 // PIM-12.2
          ADrawObject.ARecreateEmployeeDisplayList := True; // PIM-12.2
          ReadTimeRegScanning(ADrawObject);
        end;
    end;
  end; // RefreshTimeRecScanning
begin
  // When there are NO link jobs, just exit.
  if not Assigned(LinkJobList) then
    Exit;
  if Assigned(LinkJobList) then
    if LinkJobList.Count = 0 then
      Exit;
  RefreshTimeRecScanning; // 20015376.60
  // When there are NO ignore jobs, just exit.
  if not Assigned(IgnoreInterfaceCodeList) then
    Exit;
  if Assigned(IgnoreInterfaceCodeList) then
    if IgnoreInterfaceCodeList.Count = 0 then
      Exit;
  // First init quantities for link job(s) !
  for I := 0 to LinkJobList.Count - 1 do
  begin
    ALinkJobRec := LinkJobList.Items[I];
    cdsQtyToStoreUpdateLinkJob(ALinkJobRec.PlantCode,
      ALinkJobRec.WorkspotCode, ALinkJobRec.JobCode,
      0,
      False,
      True);
  end;
  with cdsQtyToStore do
  begin
    First;
    while not Eof do
    begin
      ADrawObject := FindDrawObject(APSList,
        FieldByName('PLANT_CODE').AsString,
        FieldByName('WORKSPOT_CODE').AsString);
      if Assigned(ADrawObject) then
      begin
         AJobCodeRec :=
            ADrawObject.FindJobCodeListRec(FieldByName('JOB_CODE').AsString);
         if Assigned(AJobCodeRec) then
         begin
            if (AJobCodeRec.Link_WorkspotCode <> '') and
               (AJobCodeRec.Link_JobCode <> '') then
             begin
{$IFDEF DEBUG3}
  WDebugLog('LinkJob: ' +
    AJobCodeRec.Link_WorkspotCode + '/' + AJobCodeRec.Link_JobCode +
    ' for: ' +
    ADrawObject.AWorkspotCode + '/' +  AJobCodeRec.JobCode +
    ' Qty=' + IntToStr(Round(FieldByName('QUANTITY').Value)));
{$ENDIF}
               // A different record will be updated, so be sure we
               // return to the original record afterwards.
               BookMarkStr := cdsQtyToStore.BookMark;

               // TD-25016 Here we update the quantities
               cdsQtyToStoreUpdate(FieldByName('STARTTIME').AsDateTime,
                 FieldByName('PLANT_CODE').AsString,
                 AJobCodeRec.Link_WorkspotCode, AJobCodeRec.Link_JobCode,
                 FieldByName('QUANTITY').Value,
                 False,
                 False);

               // Return to original record from while
               cdsQtyToStore.BookMark := BookMarkStr;
             end; // if
         end; // if
      end; // if
      Next;
    end; // while
  end; // with
end; // UpdateLinkJobs

// TD-25016 Decrease the qty or just store it for the cdsQtyToStore-record.
procedure TPersonalScreenDM.cdsQtyToStoreUpdate(AStartTime: TDateTime;
  APlantCode, AWorkspotCode,
  AJobCode: String; AQty: Double; ADecrease: Boolean; AInit: Boolean);
var
  NewQty: Double;
  Found: Boolean;
begin
  NewQty := AQty;
  if DateTimeFormatPatch then // TD-24736.60 Use this optional
    ORASystemDM.ADateFmt.SwitchDateTimeFmtORA;
  Found := cdsQtyToStore.
    Locate('STARTTIME;PLANT_CODE;WORKSPOT_CODE;JOB_CODE',
      VarArrayOf([AStartTime,
        APlantCode,
        AWorkspotCode,
        AJobCode]), []);
  if DateTimeFormatPatch then // TD-24736.60 Use this optional
    ORASystemDM.ADateFmt.SwitchDateTimeFmtWindows;
  if Found then
  begin
    // Decrease the qty
    if ADecrease then
    begin
      NewQty := cdsQtyToStore.FieldByName('QUANTITY').Value - AQty;
      if NewQty < 0 then
        NewQty := 0;
{$IFDEF DEBUG3}
WDebugLog('Decrease: ' +
  ' ST=' + DateTimeToStr(AStartTime) +
  ' P/W/J=' + APlantCode + ';' + AWorkspotCode + ';' + AJobCode + ':' +
  ' Qty=' + IntToStr(Round(cdsQtyToStore.FieldByName('QUANTITY').Value)) +
  ' Decr.Qty=' + IntToStr(Round(AQty)) +
  ' New Qty=' + IntToStr(Round(NewQty))
  );
{$ENDIF}
    end;
    cdsQtyToStore.Edit;
    if ADecrease or AInit then
      cdsQtyToStore.FieldByName('QUANTITY').Value :=
        NewQty
    else
      cdsQtyToStore.FieldByName('QUANTITY').Value :=
        cdsQtyToStore.FieldByName('QUANTITY').Value +
          NewQty;
    cdsQtyToStore.Post;
  end; // if
end; // cdsQtyToStoreUpdate

// TD-25016
procedure TPersonalScreenDM.cdsQtyToStoreUpdateLinkJob(APlantCode,
  AWorkspotCode, AJobCode: String; AQty: Double; ADecrease,
  AInit: Boolean);
begin
  with cdsQtyToStoreWork do
  begin
    Filtered := False;
    Filter :=
      'PLANT_CODE = ' + '''' + APlantCode + '''' +
      ' AND ' +
      'WORKSPOT_CODE = ' + '''' + AWorkspotCode + '''' +
      ' AND ' +
      'JOB_CODE = ' + '''' + AJobCode + '''';
    Filtered := True;
    while not Eof do
    begin
      cdsQtyToStoreUpdate(
        FieldByName('STARTTIME').AsDateTime,
        FieldByName('PLANT_CODE').AsString,
        FieldByName('WORKSPOT_CODE').AsString,
        FieldByName('JOB_CODE').AsString,
        AQty, ADecrease, AInit);
      Next;
    end; // while
    Filtered := False;
  end; // with
end; // cdsQtyToStoreUpdateLinkJob

// TD-25016
procedure TPersonalScreenDM.cdsQtyToStoreCopyToWork;
begin
  // Make a copy for later use
  cdsQtyToStoreWork.EmptyDataSet;
  cdsQtyToStore.First;
  while not cdsQtyToStore.Eof do
  begin
    cdsQtyToStoreWork.Insert;
    cdsQtyToStoreWork.FieldByName('STARTTIME').Value :=
      cdsQtyToStore.FieldByName('STARTTIME').Value;
    cdsQtyToStoreWork.FieldByName('PLANT_CODE').Value :=
      cdsQtyToStore.FieldByName('PLANT_CODE').Value;
    cdsQtyToStoreWork.FieldByName('WORKSPOT_CODE').Value :=
      cdsQtyToStore.FieldByName('WORKSPOT_CODE').Value;
    cdsQtyToStoreWork.FieldByName('JOB_CODE').Value :=
      cdsQtyToStore.FieldByName('JOB_CODE').Value;
    cdsQtyToStoreWork.FieldByName('SHIFT_NUMBER').Value :=
      cdsQtyToStore.FieldByName('SHIFT_NUMBER').Value;
    cdsQtyToStoreWork.FieldByName('QUANTITY').Value :=
      cdsQtyToStore.FieldByName('QUANTITY').Value;
    cdsQtyToStoreWork.FieldByName('COUNTER_VALUE').Value :=
      cdsQtyToStore.FieldByName('COUNTER_VALUE').Value;
    cdsQtyToStoreWork.FieldByName('ENDTIME').Value :=
      cdsQtyToStore.FieldByName('ENDTIME').Value;
    cdsQtyToStoreWork.FieldByName('SHIFT_DATE').Value :=
      cdsQtyToStore.FieldByName('SHIFT_DATE').Value;
    cdsQtyToStoreWork.FieldByName('UPDATE_YN').Value :=
      'N';
    cdsQtyToStoreWork.Post;
    cdsQtyToStore.Next;
  end;
end; // cdsQtyToStoreCopyToWork

// 20015178
function TPersonalScreenDM.FakeEmployeeNumber(APlantCode,
  AWorkspotCode: String): Integer;
begin
  try
    Result := StrToInt('999' + APlantCode + AWorkspotCode);
  except
    on E:Exception do
    begin
      Result := -1;
      WErrorLog('FakeEmployeeNumber: ' + E.Message);
    end;
  end;
end; // FakeEmployeeNumber

// 20015178
procedure TPersonalScreenDM.CheckCreateFakeEmployeeInDB(APlantCode,
  AWorkspotCode: String; AFakeEmployeeNumber: Integer);
var
  EmployeeNumber: Integer;
  Found: Boolean;
begin
  Found := False;
  EmployeeNumber := AFakeEmployeeNumber;
  if EmployeeNumber <> -1 then
  begin
    try
      with oqFakeEmp do
      begin
        ClearVariables;
        SetVariable('EMPLOYEE_NUMBER', EmployeeNumber);
        Execute;
        if not Eof then
          Found := True;
      end; // with
      if not Found then
      begin
        with oqInsertFakeEmp do
        begin
          ClearVariables;
          SetVariable('EMPLOYEE_NUMBER', EmployeeNumber);
          SetVariable('SHORT_NAME',      Copy(IntToStr(EmployeeNumber), 1, 6));
          SetVariable('DESCRIPTION',     IntToStr(EmployeeNumber));
          SetVariable('PLANT_CODE',      APlantCode);
          Execute;
        end; // with
      end; // if
    except
      on E:EOracleError do
      begin
        WErrorLog('CreateFakeEmployeeInDB: ' + E.Message);
      end;
      on E:Exception do
      begin
        WErrorLog('CreateFakeEmployeeInDB: ' + E.Message);
      end;
    end; // try
  end; // if
end; // CheckCreateFakeEmployeeInDB

// PIM-155
(*
// 20015178
// PIM-155 Return 'True' when 1 scan was added.
//         This only ADDS 1 scan when there was no old scan found, it does
//         NOT update an existing scan.
function TPersonalScreenDM.CheckAddScanFakeEmployeeInDB(APlantCode,
  AWorkspotCode, AJobCode: String; ADatetimeIn: TDateTime;
  AFakeEmployeeNumber: Integer): Boolean;
var
  EmployeeNumber: Integer;
  DateFromCurrent, DateToCurrent, HalfADayBefore: TDateTime;
  Found: Boolean;
begin
  Result := False;
  ADateTimeIn := RoundTimeConditional(ADateTimeIn, 1); // 20015346
  Found := False;
  EmployeeNumber := AFakeEmployeeNumber;
  if EmployeeNumber <> -1 then
  begin
    // 20015178.90 Round it to the upper minute by adding 30 seconds to the current time.
    // This prevents it will not find it because it was rounded to upper minute.
//    DateToCurrent := Now;
    DateToCurrent := RoundTime(IncSecond(Now, 30), 1);
    // Calculate a period of n-minutes before.
    DateFromCurrent := DateToCurrent -
      (PIMSDatacolTimeInterval * PIMSMultiplier / 60 / 24 / 60);
    HalfADayBefore := DateFromCurrent - 0.5;
    try
      // Only add a scan when there was no scan yet.
      // 20015178.70 Only look for OPEN scans!
      with oqScan do
      begin
        ClearVariables;
        SetVariable('DATEFROM',        DateFromCurrent);
        SetVariable('DATETO',          DateToCurrent);
        SetVariable('PLANT_CODE',      APlantCode);
        SetVariable('WORKSPOT_CODE',   AWorkspotCode);
        SetVariable('EMPLOYEE_NUMBER', EmployeeNumber);
        SetVariable('HALFADAY_BEFORE', HalfADayBefore);
        Execute;
        if not Eof then
          Found := True;
      end; // with
      if not Found then
      begin
        with oqInsertScan do
        begin
          ClearVariables;
          SetVariable('DATETIME_IN',     ADateTimeIn);
          SetVariable('EMPLOYEE_NUMBER', EmployeeNumber);
          SetVariable('PLANT_CODE',      APlantCode);
          SetVariable('WORKSPOT_CODE',   AWorkspotCode);
          SetVariable('JOB_CODE',        AJobCode);
          SetVariable('SHIFT_DATE',      Trunc(ADateTimeIn));
          Execute;
          Result := True; // 1 scan was added
        end; // with
      end; // if
    except
      on E:EOracleError do
      begin
        WErrorLog('AddScanFakeEmployeeInDB: ' + E.Message);
      end;
      on E:Exception do
      begin
        WErrorLog('AddScanFakeEmployeeInDB: ' + E.Message);
      end;
    end; // try
  end; // if
end; // AddScanFakeEmployeeInDB
*)

// 20014450
procedure TPersonalScreenDM.MachineHoursMerge(APlantCode,
  AMachineCode: String; AStartTimestamp: TDateTime;
  AShiftDate: TDateTime;
  AShiftNumber: Integer);
begin
  try
    with oqMergeMachHrs do
    begin
      ClearVariables;
      SetVariable('PLANT_CODE',      APlantCode);
      SetVariable('MACHINE_CODE',    AMachineCode);
      SetVariable('START_TIMESTAMP', AStartTimestamp); // RoundTime(AStartTimestamp,1));
      SetVariable('SHIFT_DATE',      AShiftDate);
      SetVariable('SHIFT_NUMBER',    AShiftNumber);
      SetVariable('END_TIMESTAMP',   Now); // RoundTime(Now,1));
      SetVariable('MUTATOR',         ORASystemDM.CurrentProgramUser);
      Execute;
    end;
  except
    on E:EOracleError do
    begin
      WErrorLog('MachineHoursMerge: ' + E.Message);
    end;
    on E:Exception do
    begin
      WErrorLog('MachineHoursMerge: ' + E.Message);
    end;
  end;
end; // MachineHoursMerge

// 20014450
procedure TPersonalScreenDM.UpdateMachineHours(APSList: TList);
var
  I: Integer;
  ADrawObject: PDrawObject;
  EmployeeData: TScannedIDCard;
  StartTime, EndTime: TDateTime;
begin
  if not ORASystemDM.RegisterMachineHours then
    Exit;

  if not cdsQtyToStore.IsEmpty then
  begin
    for I := 0 to APSList.Count - 1 do
    begin
      ADrawObject := APSList.Items[I];
      if Assigned(ADrawObject.AMachineTimeRec) then
      begin
        if ADrawObject.AMachineTimeRec.AStartTimeStamp = NullDate then
        begin
          ADrawObject.AMachineTimeRec.AStartTimeStamp := Now; // RoundTime(Now, 1);
          ADrawObject.AMachineTimeRec.AEndTimeStamp := Now;
        end;
        // Reset start date based on a setting that defined the max. period
        if DateUtils.MinutesBetween(Now,
          ADrawObject.AMachineTimeRec.AEndTimeStamp) > MachHrsMaxIdleTime then
          ADrawObject.AMachineTimeRec.AStartTimeStamp := Now;
        // Todo: Determine current shift here. When there is a change of shift
        //       then the end the machine hours for current shift and
        //       create a new one for new shift.
        EmployeeData.ShiftNumber := 1;
        EmployeeData.EmployeeCode := -1;
        EmployeeData.DepartmentCode := ADrawObject.AMachineTimeRec.ADepartmentCode;
        EmployeeData.PlantCode := ADrawObject.APlantCode;
        EmployeeData.DateIn := Now;
        EmployeeData.DateOut := EmployeeData.DateOut;
        AShiftDay.GetShiftDay(EmployeeData, StartTime, EndTime);
        ADrawObject.AMachineTimeRec.AShiftNumber := 1;
        ADrawObject.AMachineTimeRec.AShiftDate := Date;
        MachineHoursMerge(ADrawObject.APlantCode,
          ADrawObject.AMachineTimeRec.AMachineCode,
          ADrawObject.AMachineTimeRec.AStartTimeStamp,
          ADrawObject.AMachineTimeRec.AShiftDate,
          ADrawObject.AMachineTimeRec.AShiftNumber);
        ADrawObject.AMachineTimeRec.AEndTimeStamp := Now;
      end;
    end;
  end;
end; // UpdateMachineHours

// 20014450
function TPersonalScreenDM.MachineDepartment(APlantCode,
  AMachineCode: String): String;
begin
  Result := '';
  try
    with oqMachine do
    begin
      ClearVariables;
      SetVariable('PLANT_CODE',      APlantCode);
      SetVariable('MACHINE_CODE',    AMachineCode);
      Execute;
      if not Eof then
        Result := FieldAsString('DEPARTMENT_CODE');
    end;
  except
    on E:EOracleError do
    begin
      WErrorLog('MachineHoursMerge: ' + E.Message);
    end;
    on E:Exception do
    begin
      WErrorLog('MachineHoursMerge: ' + E.Message);
    end;
  end;
end; // MachineDepartment

// 20014450
procedure TPersonalScreenDM.MachineDownTimeMerge(APlantCode,
  AMachineCode: String; AStartTimestamp, AShiftDate: TDateTime;
  AShiftNumber: Integer; AReasonCode: String);
begin
  try
    with oqMergeMachDownTime do
    begin
      ClearVariables;
      SetVariable('PLANT_CODE',           APlantCode);
      SetVariable('MACHINE_CODE',         AMachineCode);
      SetVariable('START_TIMESTAMP',      AStartTimestamp); // RoundTime(AStartTimestamp,1));
      SetVariable('SHIFT_DATE',           AShiftDate);
      SetVariable('SHIFT_NUMBER',         AShiftNumber);
      SetVariable('END_TIMESTAMP',        Now); // RoundTime(Now,1));
      SetVariable('DOWNTIME_REASON_CODE', AReasonCode);
      SetVariable('MUTATOR',              ORASystemDM.CurrentProgramUser);
      Execute;
    end;
  except
    on E:EOracleError do
    begin
      WErrorLog('MachineDownTimeMerge: ' + E.Message);
    end;
    on E:Exception do
    begin
      WErrorLog('MachineDownTimeMerge: ' + E.Message);
    end;
  end;
end; // MachineDownTimeMerge

// 20014450
procedure TPersonalScreenDM.UpdateMachineDownTime(
  var ADrawObject: PDrawObject);
begin
  if not ORASystemDM.RegisterMachineHours then
    Exit;

  // Todo: Determine current shift here. When there is a change of shift
  //       then the end the machine hours for current shift and
  //       create a new one for new shift.
  if (ADrawObject.AMachineTimeRec.AReasonCode = MECHANICAL_DOWN_JOB) or
    (ADrawObject.AMachineTimeRec.AReasonCode = NO_MERCHANDIZE_JOB) then
  begin
    ADrawObject.AMachineTimeRec.AShiftDate := Date;
    ADrawObject.AMachineTimeRec.AShiftNumber := 1;
    MachineDownTimeMerge(ADrawObject.APlantCode,
      ADrawObject.AMachineTimeRec.AMachineCode,
      ADrawObject.AMachineTimeRec.AStartTimeStamp,
      ADrawObject.AMachineTimeRec.AShiftDate,
      ADrawObject.AMachineTimeRec.AShiftNumber,
      ADrawObject.AMachineTimeRec.AReasonCode);
  end;
end; // UpdateMachineDownTime

// 20014450
procedure TPersonalScreenDM.UpdateMachineDownTimeHours(var APSList: TList);
var
  I: Integer;
  ADrawObject: PDrawObject;
begin
  if not ORASystemDM.RegisterMachineHours then
    Exit;

  for I := 0 to APSList.Count - 1 do
  begin
    ADrawObject := APSList.Items[I];
    if Assigned(ADrawObject.AMachineTimeRec) then
      UpdateMachineDownTime(ADrawObject);
  end;
end;

end.
