(*
  MRA:29-NOV-2016 PIM-242
  - Check Shift during In/Out Scan
*)
unit DialogShiftCheckFRM;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseDialogFRM, dxCntner, Menus, StdActns, ActnList, ImgList,
  StdCtrls, Buttons, ComCtrls, ExtCtrls, DialogShiftCheckDMT, dxExEdtr,
  dxTL, dxDBCtrl, dxDBGrid, dxDBTLCl, dxGrClms, ORASystemDMT, UPimsConst,
  jpeg;

type
  TDialogShiftCheckForm = class(TBaseDialogForm)
    GroupBox1: TGroupBox;
    dxDBGrid1: TdxDBGrid;
    dxDBGrid1SHIFT_NUMBER: TdxDBGridMaskColumn;
    dxDBGrid1DESCRIPTION: TdxDBGridColumn;
    dxDBGrid1STARTDATE: TdxDBGridDateColumn;
    dxDBGrid1START: TdxDBGridTimeColumn;
    dxDBGrid1END: TdxDBGridTimeColumn;
    Panel1: TPanel;
    btnFirst: TBitBtn;
    btnUp: TBitBtn;
    btnLast: TBitBtn;
    btnLow: TBitBtn;
    procedure btnOKClick(Sender: TObject);
    procedure btnUpClick(Sender: TObject);
    procedure btnLowClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    FShiftNumber: Integer;
    FDateIn: TDateTime;
    procedure WMSysCommand(var Msg: TWMSysCommand); message WM_SYSCOMMAND;
  public
    { Public declarations }
    procedure SetFormPos(ATop, ALeft, AWidth, AHeight: Integer);
    property ShiftNumber: Integer read FShiftNumber write FShiftNumber;
    property DateIn: TDateTime read FDateIn write FDateIn;
  end;

var
  DialogShiftCheckForm: TDialogShiftCheckForm;

implementation

{$R *.dfm}

procedure TDialogShiftCheckForm.btnOKClick(Sender: TObject);
begin
  inherited;
  ShiftNumber :=
    DialogShiftCheckDM.cdsShift.FieldByName('SHIFT_NUMBER').AsInteger;
end;

procedure TDialogShiftCheckForm.btnUpClick(Sender: TObject);
begin
  inherited;
  DialogShiftCheckDM.cdsShift.Prior;
end;

procedure TDialogShiftCheckForm.btnLowClick(Sender: TObject);
begin
  inherited;
  DialogShiftCheckDM.cdsShift.Next;
end;

procedure TDialogShiftCheckForm.FormShow(Sender: TObject);
begin
  inherited;
  lblMessage.Caption := DateTimeToStr(DateIn);
  // Select first line in grid as default.
  if not DialogShiftCheckDM.cdsShift.Eof then
  begin
    // Point to shift of the employee here
    try
      if not DialogShiftCheckDM.cdsShift.Locate('SHIFT_NUMBER',
        DialogShiftCheckDM.DefaultShiftNumber, []) then
      DialogShiftCheckDM.cdsShift.First;
    except
      DialogShiftCheckDM.cdsShift.First;
    end;
  end;
end;

procedure TDialogShiftCheckForm.SetFormPos(ATop, ALeft, AWidth,
  AHeight: Integer);
begin
  Top := ATop;
  Left := ALeft;
  Width := AWidth;
  Height := AHeight;
end;

// Prevent it is moved
procedure TDialogShiftCheckForm.WMSysCommand(var Msg: TWMSysCommand);
begin
  if ((Msg.CmdType and $FFF0) = SC_MOVE) or
    ((Msg.CmdType and $FFF0) = SC_SIZE) then
  begin
    Msg.Result := 0;
    Exit;
  end;
end;

procedure TDialogShiftCheckForm.FormCreate(Sender: TObject);
begin
  inherited;
  dxDBGrid1.DataSource := DialogShiftCheckDM.dsShift;
end;

end.
