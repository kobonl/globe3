(*
  MRA:2-JUN-2015 20014450.50 Part 2
  - Real Time Efficiency
*)
unit UTranslateStringsRealTimeEff;

interface

resourceString

{$IFDEF PIMSDUTCH}
  STransMin = 'm';
{$ELSE}
  {$IFDEF PIMSFRENCH}
  STransMin = 'm';
{$ELSE}
  {$IFDEF PIMSGERMAN}
  STransMin = 'M';
{$ELSE}
  {$IFDEF PIMSDANISH}
  STransMin = 'm';
{$ELSE}
  {$IFDEF PIMSJAPANESE}
  STransMin = 'm';
{$ELSE}
  {$IFDEF PIMSCHINESE}
  STransMin = 'm';
{$ELSE}
  {$IFDEF PIMSNORWEGIAN}
  STransMin = 'm';
{$ELSE}
// ENGLISH (default language)
  STransMin = 'm';
            {$ENDIF}
          {$ENDIF}
        {$ENDIF}
      {$ENDIF}
    {$ENDIF}
  {$ENDIF}
{$ENDIF}

implementation

end.
