(*
  MRA:16-MAY-2014 TD-24046
  - Translate issues: Use a different method to translate.
    - Do NOT do this in separate files where the translation is done
      hard-coded!
    - New method:
      - Use 1 resource-strings-file that contains the translation-strings.
      - Use 1 translate-unit that assigns these strings to the properties
        in the forms.
      - In this way there is only 1 form that can be translated in multiple
        languages when the application is run.
      - The project-file defines the language using a condition.
  MRA:23-JUL-2015 PIM-50
  - Addition of strings for DialogEnterJobComment
*)
unit UTranslateStringsShared;

interface

resourceString

{$IFDEF PIMSDUTCH}
  // Change Job
  STransChangeJob = 'Wijzig job';
  STransChangeJob2 = 'WIJZIG JOB';
  STransDown = 'Trouble';
  STransContinue = 'DOORG.';
  STransOK = '&OK';
  STransCancel = '&ANNUL.';
  STransEndOfDay = '&EINDE';
  STransBreak = 'Pauze';
  STransLunch = 'Lunch';
  // DialogSelectDownType
  STransSelectDownType = 'Kies Type Storing';
  STransMechDown = 'Mechanische Storing';
  STransNoMerch = 'Geen Artikelen';
  // DialogEmpListView
  STransQuantity = 'Aantal';
  STransTime = 'Tijd';
  STransWorkspot = 'Werkplek';
  STransEmpListView = 'Werknemer Overzicht Productiviteit';
  STransJob = 'Job';
  STransActual = 'Huidig';
  STransNorm = 'Norm';
  STransDiff = 'Versch.';
  STransEfficiency = 'Eff.';
  STransTotal = 'Totaal';
  STransMin = 'm';
  STransLastUpdate = 'Laatste update:';
  // DialogEmpGraph
  STransSeries4Title = 'Tijd (min x 100)';
  STransSeries3Title = 'Norm Prod Level';
  STransSeries1Title = 'Uitvoer';
  // DialogEnterJobComment
  STransDialogEnterJobComment = 'Geef een melding voor de storing';
  STransComment = 'Melding';
  // DialogShiftCheck
  STransPleaseSelectShift = 'Kies a.u.b. de ploeg';
  STransShift = 'Ploeg';
  STransDescription = 'Omschrijving';
  STransDate = 'Datum';
  STransStart = 'Start';
  STransEnd = 'Einde';
{$ELSE}
{$IFDEF PIMSFRENCH}
  // Change Job
  STransChangeJob = 'Change travail';
  STransChangeJob2 = 'CHANGE TRAVAIL';
  STransDown = 'Trouble';
  STransContinue = 'CONT.';
  STransOK = '&OK';
  STransCancel = '&ANNUL.';
  STransEndOfDay = '&FIN';
  STransBreak = 'Pause';
  STransLunch = 'D閖euner';
  // DialogSelectDownType
  STransSelectDownType = 'Choisit type du trouble';
  STransMechDown = 'Trouble mecanique';
  STransNoMerch = 'Pas d''articles';
  // DialogEmpListView
  STransQuantity = 'Nombre';
  STransTime = 'Temps';
  STransWorkspot = 'Lieu de travail';
  STransEmpListView = 'Employee Productivity Overview';
  STransJob = 'Job';
  STransActual = 'Actual';
  STransNorm = 'Norm';
  STransDiff = 'Diff.';
  STransEfficiency = 'Efficiency';
  STransTotal = 'Total';
  STransMin = 'm';
  STransLastUpdate = 'Last updated:';
  // DialogEmpGraph
  STransSeries4Title = 'Temps (min x 100)';
  STransSeries3Title = 'Normes Prod Niveau';
  STransSeries1Title = 'Sortie';
  // DialogEnterJobComment
  STransDialogEnterJobComment = 'Enter comment for down job';
  STransComment = 'Comment';
  // DialogShiftCheck
  STransPleaseSelectShift = 'Please select a shift';
  STransShift = 'Shift';
  STransDescription = 'Description';
  STransDate = 'Date';
  STransStart = 'Start';
  STransEnd = 'End';
{$ELSE}
  {$IFDEF PIMSGERMAN}
  // Change Job
  STransChangeJob = '膎derung Job';
  STransChangeJob2 = '腘DERUNG JOB';
  STransDown = 'DOWN';
  STransContinue = 'FORTS.';
  STransOK = '&OK';
  STransCancel = '&ANNUL.';
  STransEndOfDay = '&ENDE';
  STransBreak = 'BREAK';
  STransLunch = 'LUNCH';
  // DialogSelectDownType
  STransSelectDownType = 'Wahl Steurungstype';
  STransMechDown = 'Mechan. Steurung';
  STransNoMerch = 'Keine Artikel';
  // DialogEmpListView
  STransQuantity = 'Anzahl';
  STransTime = 'Zeit';
  STransWorkspot = 'Arbeitsplatz';
  STransEmpListView = 'Employee Productivity Overview';
  STransJob = 'Job';
  STransActual = 'Actual';
  STransNorm = 'Norm';
  STransDiff = 'Diff.';
  STransEfficiency = 'Efficiency';
  STransTotal = 'Total';
  STransMin = 'M';
  STransLastUpdate = 'Letzte Update:';
  // DialogEmpGraph
  STransSeries4Title = 'Zeit (Min x 100)';
  STransSeries3Title = 'Norm Prod Level';
  STransSeries1Title = 'Ausfuhr';
  // DialogEnterJobComment
  STransDialogEnterJobComment = 'Eingabe Meldung fur Steurung';
  STransComment = 'Meldung';
  // DialogShiftCheck
  STransPleaseSelectShift = 'Bitte w鋒len Sie eine Schicht aus';
  STransShift = 'Schicht';
  STransDescription = 'Beschreibung';
  STransDate = 'Datum';
  STransStart = 'Anfang';
  STransEnd = 'Ende';
{$ELSE}
  {$IFDEF PIMSDANISH}
  // Change Job
  STransChangeJob = '苙dre Job';
  STransChangeJob2 = '芅DRE JOB';
  STransDown = 'NEDE';
  STransContinue = 'FORTS.';
  STransOK = '&OK';
  STransCancel = '&AFBRYD';
  STransEndOfDay = '&SLUT';
  STransBreak = 'PAUSE';
  STransLunch = 'LUNCH';
  // DialogSelectDownType
  STransSelectDownType = 'V鎙g lukning';
  STransMechDown = 'Mekanisk';
  STransNoMerch = 'Ingen varer';
  // DialogEmpListView
  STransQuantity = 'Antal';
  STransTime = 'Tide';
  STransWorkspot = 'Arb.sted';
  STransEmpListView = 'Employee Productivity Overview';
  STransJob = 'Job';
  STransActual = 'Aktuel';
  STransNorm = 'Norm';
  STransDiff = 'Forskel';
  STransEfficiency = 'Effektivitet';
  STransTotal = 'Total';
  STransMin = 'm';
  STransLastUpdate = 'Sidste 鎛dring:';
  // DialogEmpGraph
  STransSeries4Title = 'Tide (min x 100)';
  STransSeries3Title = 'Norm Prod Niveau';
  STransSeries1Title = 'Produktion';
  // DialogEnterJobComment
  STransDialogEnterJobComment = 'Enter comment for down job';
  STransComment = 'Comment';
  // DialogShiftCheck
  STransPleaseSelectShift = 'V鎙g venligst et skift';
  STransShift = 'Skift';
  STransDescription = 'Beskrivelse';
  STransDate = 'Dato';
  STransStart = 'Starte';
  STransEnd = 'Ende';
{$ELSE}
  {$IFDEF PIMSJAPANESE}
  // Change Job
  STransChangeJob = 'Change Job';
  STransChangeJob2 = 'CHANGE JOB';
  STransDown = 'DOWN';
  STransContinue = 'CONT.';
  STransOK = '&OK';
  STransCancel = '&CANCEL';
  STransEndOfDay = '&END';
  STransBreak = 'BREAK';
  STransLunch = 'LUNCH';
  // DialogSelectDownType
  STransSelectDownType = 'Select Down Type';
  STransMechDown = 'Mechanical Down';
  STransNoMerch = 'No Merchandize';
  // DialogEmpListView
  STransQuantity = 'Quantity';
  STransTime = 'Time';
  STransWorkspot = 'Workspot';
  STransEmpListView = 'Employee Productivity Overview';
  STransJob = 'Job';
  STransActual = 'Actual';
  STransNorm = 'Norm';
  STransDiff = 'Diff.';
  STransEfficiency = 'Efficiency';
  STransTotal = 'Total';
  STransMin = 'm';
  STransLastUpdate = 'Last updated:';
  // DialogEmpGraph
  STransSeries4Title = 'Time (min x 100)';
  STransSeries3Title = 'Norm Prod Level';
  STransSeries1Title = 'Output';
  // DialogEnterJobComment
  STransDialogEnterJobComment = 'Enter comment for down job';
  STransComment = 'Comment';
  // DialogShiftCheck
  STransPleaseSelectShift = 'Please select a shift';
  STransShift = 'Shift';
  STransDescription = 'Description';
  STransDate = 'Date';
  STransStart = 'Start';
  STransEnd = 'End';
{$ELSE}
  {$IFDEF PIMSCHINESE}
  // Change Job
  STransChangeJob = '更换工作';
  STransChangeJob2 = '更换工作';
  STransDown = '下';
  STransContinue = '继续.';
  STransOK = '&OK';
  STransCancel = '&取消';
  STransEndOfDay = '&结束';
  STransBreak = '停';
  STransLunch = 'LUNCH';
  // DialogSelectDownType
  STransSelectDownType = '选择向下模式';
  STransMechDown = '向下操作';
  STransNoMerch = '无货物';
  // DialogEmpListView
  STransQuantity = '数量';
  STransTime = '时间';
  STransWorkspot = '工作点';
  STransEmpListView = '员工生产力概览';
  STransJob = '工作';
  STransActual = '实际';
  STransNorm = '规范';
  STransDiff = '差速器.';
  STransEfficiency = '效率';
  STransTotal = '合计';
  STransMin = 'm';
  STransLastUpdate = '最新更新:';
  // DialogEmpGraph
  STransSeries4Title = '时间 (min x 100)';
  STransSeries3Title = '标准刺激水平';
  STransSeries1Title = '产量';
  // DialogEnterJobComment
  STransDialogEnterJobComment = '输入工作意见';
  STransComment = '品论';
  // DialogShiftCheck
  STransPleaseSelectShift = '请选择班次';
  STransShift = '转移';
  STransDescription = '描述';
  STransDate = '日期';
  STransStart = '开始';
  STransEnd = '结束';
{$ELSE}
  {$IFDEF PIMSNORWEGIAN}
  // Change Job
  STransChangeJob = 'Endre Jobb';
  STransChangeJob2 = 'ENDRE JOBB';
  STransDown = 'NEDE';
  STransContinue = 'ANTALL.';
  STransOK = '&OK';
  STransCancel = '&AVBRYT';
  STransEndOfDay = '&AVLSUTT';
  STransBreak = 'PAUSE';
  STransLunch = 'LUNSJ';
  // DialogSelectDownType
  STransSelectDownType = 'Velg Nedtid type';
  STransMechDown = 'Mekanisk brudd';
  STransNoMerch = 'Ingen produkter';
  // DialogEmpListView
  STransQuantity = 'Antall';
  STransTime = 'Tid';
  STransWorkspot = 'Arbeidsposisjon';
  STransEmpListView = 'Ansatts produktivitets oversikt';
  STransJob = 'Jobb';
  STransActual = 'Faktiske';
  STransNorm = 'Normal';
  STransDiff = 'Diff.';
  STransEfficiency = 'Effektivitet';
  STransTotal = 'Total';
  STransMin = 'm';
  STransLastUpdate = 'Sist oppdatert:';
  // DialogEmpGraph
  STransSeries4Title = 'Tid (min x 100)';
  STransSeries3Title = 'Norm Prod Niv?';
  STransSeries1Title = 'Resultat';
  // DialogEnterJobComment
  STransDialogEnterJobComment = 'Skriv inn kommentar for nedetid p?jobb';
  STransComment = 'Kommentar';
  // DialogShiftCheck
  STransPleaseSelectShift = 'Vennligst velg et skift';
  STransShift = 'Skift';
  STransDescription = 'Beskrivelse';
  STransDate = 'Dato';
  STransStart = 'Start';
  STransEnd = 'Avsluttet';
{$ELSE}
// ENGLISH (default language)
  // Change Job
  STransChangeJob = 'Change Job';
  STransChangeJob2 = 'CHANGE JOB';
  STransDown = 'DOWN';
  STransContinue = 'CONT.';
  STransOK = '&OK';
  STransCancel = '&CANCEL';
  STransEndOfDay = '&END';
  STransBreak = 'BREAK';
  STransLunch = 'LUNCH';
  // DialogSelectDownType
  STransSelectDownType = 'Select Down Type';
  STransMechDown = 'Mechanical Down';
  STransNoMerch = 'No Merchandize';
  // DialogEmpListView
  STransQuantity = 'Quantity';
  STransTime = 'Time';
  STransWorkspot = 'Workspot';
  STransEmpListView = 'Employee Productivity Overview';
  STransJob = 'Job';
  STransActual = 'Actual';
  STransNorm = 'Norm';
  STransDiff = 'Diff.';
  STransEfficiency = 'Efficiency';
  STransTotal = 'Total';
  STransMin = 'm';
  STransLastUpdate = 'Last updated:';
  // DialogEmpGraph
  STransSeries4Title = 'Time (min x 100)';
  STransSeries3Title = 'Norm Prod Level';
  STransSeries1Title = 'Output';
  // DialogEnterJobComment
  STransDialogEnterJobComment = 'Enter comment for down job';
  STransComment = 'Comment';
  // DialogShiftCheck
  STransPleaseSelectShift = 'Please select a shift';
  STransShift = 'Shift';
  STransDescription = 'Description';
  STransDate = 'Date';
  STransStart = 'Start';
  STransEnd = 'End';

            {$ENDIF}
          {$ENDIF}
        {$ENDIF}
      {$ENDIF}
    {$ENDIF}
  {$ENDIF}
{$ENDIF}

implementation

end.
