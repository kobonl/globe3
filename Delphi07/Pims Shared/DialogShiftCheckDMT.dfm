object DialogShiftCheckDM: TDialogShiftCheckDM
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Left = 439
  Top = 202
  Height = 362
  Width = 397
  object oqFirstScan: TOracleQuery
    SQL.Strings = (
      'SELECT * FROM'
      '('
      '  SELECT'
      '    TRS.IDCARD_IN,'
      '    TRS.DATETIME_IN,'
      '    TRS.DATETIME_OUT,'
      '    TRS.PLANT_CODE,'
      '    TRS.WORKSPOT_CODE,'
      '    TRS.JOB_CODE,'
      '    TRS.SHIFT_NUMBER,'
      '    TRS.PROCESSED_YN,'
      '    TRS.EMPLOYEE_NUMBER,'
      '    E.DESCRIPTION EDESCRIPTION,'
      '    P.INSCAN_MARGIN_EARLY,'
      '    P.INSCAN_MARGIN_LATE,'
      '    P.OUTSCAN_MARGIN_EARLY,'
      '    P.OUTSCAN_MARGIN_LATE,'
      '    P.DESCRIPTION,'
      '    W.MEASURE_PRODUCTIVITY_YN'
      '  FROM'
      '    TIMEREGSCANNING TRS INNER JOIN PLANT P ON'
      '      TRS.PLANT_CODE = P.PLANT_CODE'
      '    INNER JOIN EMPLOYEE E ON'
      '      TRS.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER'
      '    INNER JOIN WORKSPOT W ON'
      '      TRS.PLANT_CODE = W.PLANT_CODE AND'
      '      TRS.WORKSPOT_CODE = W.WORKSPOT_CODE'
      '  WHERE'
      '    ('
      '      ('
      '        (TRS.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER) AND'
      '        (:BYWORKSPOT = 0)'
      '      )'
      '      OR'
      '      ('
      '        (TRS.PLANT_CODE = :PLANT_CODE) AND'
      '        (TRS.WORKSPOT_CODE = :WORKSPOT_CODE) AND'
      '        (TRS.EMPLOYEE_NUMBER <> :EMPLOYEE_NUMBER) AND'
      '        (TRS.DATETIME_OUT IS NULL) AND'
      '        (:BYWORKSPOT = 1)'
      '      )'
      '    )'
      '    AND'
      '    ('
      '      (TRS.DATETIME_IN >= :TIMEIN) AND'
      '      (TRS.DATETIME_IN <= :TIMENOW) AND'
      '      (TRS.DATETIME_IN IS NOT NULL)'
      '    )'
      '    AND'
      '    ('
      '      (:ALLSCANS = 1)'
      '      OR'
      '      ((:ALLSCANS = 0)  AND (TRS.PROCESSED_YN = '#39'N'#39'))'
      '    )'
      '  ORDER BY'
      '    TRS.DATETIME_IN ASC'
      ')'
      'WHERE ROWNUM = 1')
    Session = ORASystemDM.OracleSession
    Variables.Data = {
      0300000007000000100000003A454D504C4F5945455F4E554D42455203000000
      00000000000000000B0000003A4259574F524B53504F54030000000000000000
      0000000B0000003A504C414E545F434F44450500000000000000000000000E00
      00003A574F524B53504F545F434F444505000000000000000000000007000000
      3A54494D45494E0C0000000000000000000000080000003A54494D454E4F570C
      0000000000000000000000090000003A414C4C5343414E530300000000000000
      00000000}
    Left = 56
    Top = 40
  end
  object oqShift: TOracleQuery
    SQL.Strings = (
      'SELECT S.SHIFT_NUMBER, S.DESCRIPTION,'
      '  NVL(P.INSCAN_MARGIN_EARLY, 0) INSCAN_MARGIN_EARLY,'
      '  NVL(P.INSCAN_MARGIN_LATE, 0) INSCAN_MARGIN_LATE'
      'FROM SHIFT S INNER JOIN PLANT P ON'
      '  S.PLANT_CODE = P.PLANT_CODE'
      'WHERE S.PLANT_CODE = :PLANT_CODE'
      'ORDER BY S.SHIFT_NUMBER')
    Session = ORASystemDM.OracleSession
    Variables.Data = {
      03000000010000000B0000003A504C414E545F434F4445050000000000000000
      000000}
    Left = 144
    Top = 40
  end
  object cdsShift: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 248
    Top = 40
    object cdsShiftPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Size = 6
    end
    object cdsShiftSHIFT_NUMBER: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'SHIFT_NUMBER'
    end
    object cdsShiftDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Size = 30
    end
    object cdsShiftSTARTDATE: TDateField
      FieldName = 'STARTDATE'
    end
    object cdsShiftSTART: TDateTimeField
      FieldName = 'START'
      DisplayFormat = 'hh:mm'
    end
    object cdsShiftEND: TDateTimeField
      FieldName = 'END'
      DisplayFormat = 'hh:mm'
    end
  end
  object dsShift: TDataSource
    DataSet = cdsShift
    Left = 320
    Top = 40
  end
end
