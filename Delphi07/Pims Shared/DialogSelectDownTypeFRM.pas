(*
  MRA:10-SEP-2012 TD-21191
  - Added timer for auto-close.
  MRA:10-OCT-2014 SO-20015569
  - Show extra buttons for Down/Break/Lunch
*)

unit DialogSelectDownTypeFRM;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseDialogFRM, dxCntner, Menus, StdActns, ActnList, ImgList,
  StdCtrls, Buttons, ComCtrls, ExtCtrls, jpeg;

type
  TDialogSelectDownTypeF = class(TBaseDialogForm)
    GroupBox1: TGroupBox;
    btnMechanicalDown: TButton;
    btnNoMerchandize: TButton;
    TimerAutoClose: TTimer;
    procedure btnMechanicalDownClick(Sender: TObject);
    procedure btnNoMerchandizeClick(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure TimerAutoCloseTimer(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    FDownType: Integer;
    procedure WMSysCommand(var Msg: TWMSysCommand); message WM_SYSCOMMAND;
  public
    { Public declarations }
    property DownType: Integer read FDownType write FDownType;
  end;

var
  DialogSelectDownTypeF: TDialogSelectDownTypeF;

implementation

uses ORASystemDMT;

{$R *.DFM}

(*
{$IFDEF PIMSDUTCH}
{$R *NLD.DFM}
{$ELSE}
  {$IFDEF PIMSGERMAN}
  {$R *DEU.DFM}
  {$ELSE}
    {$IFDEF PIMSFRENCH}
    {$R *FRA.DFM}
    {$ELSE}
    {$R *.DFM}
    {$ENDIF}
  {$ENDIF}
{$ENDIF}
*)

procedure TDialogSelectDownTypeF.btnMechanicalDownClick(Sender: TObject);
begin
  inherited;
  DownType := 1;
  ModalResult := mrOK;
end;

procedure TDialogSelectDownTypeF.btnNoMerchandizeClick(Sender: TObject);
begin
  inherited;
  DownType := 2;
  ModalResult := mrOK;
end;

procedure TDialogSelectDownTypeF.btnCancelClick(Sender: TObject);
begin
  inherited;
  DownType := 0;
  ModalResult := mrCancel;
end;

procedure TDialogSelectDownTypeF.FormShow(Sender: TObject);
begin
  inherited;
  btnOK.Visible := False;
  btnCancel.Left := Trunc(pnlBottom.Width / 2 - btnCancel.Width / 2); //  Trunc(pnlBottom.Width / 3);
  // TD-21191
  TimerAutoClose.Interval := ORASystemDM.AutoCloseInterval;
  if TimerAutoClose.Interval > 0 then
    TimerAutoClose.Enabled := True;
end;

procedure TDialogSelectDownTypeF.TimerAutoCloseTimer(Sender: TObject);
begin
  inherited;
  TimerAutoClose.Enabled := False;
  Close;
end;

procedure TDialogSelectDownTypeF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  TimerAutoClose.Enabled := False;
end;

// Prevent it is moved
procedure TDialogSelectDownTypeF.WMSysCommand(var Msg: TWMSysCommand);
begin
  if ((Msg.CmdType and $FFF0) = SC_MOVE) or
    ((Msg.CmdType and $FFF0) = SC_SIZE) then
  begin
    Msg.Result := 0;
    Exit;
  end;
  inherited;
end;

end.
