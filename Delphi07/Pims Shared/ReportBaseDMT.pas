unit ReportBaseDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DBTables, DBClient;

type

  TReportBaseDM = class(TDataModule)
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure ActiveTables(Table_Active: Boolean);
  end;

  TReportBaseClassDM = Class of TReportBaseDM;

var
  ReportBaseDM: TReportBaseDM;

implementation

{$R *.DFM}

procedure TReportBaseDM.ActiveTables(Table_Active: Boolean);
var
  DataSetCounter: Integer;
  Temp: TComponent;
begin
  if Self <> nil then
    for DataSetCounter := 0 to Self.ComponentCount - 1 do
    begin
      Temp := Self.Components[DataSetCounter];
      if (Temp is TTable) then
      begin
        if ((Temp as TTable).TableName <> '') then
          (Temp as TTable).Active := Table_Active;
      end;
//CAR 8-7-2003 -  ClentDataSet are assign to TQuery components
      if (Temp is TClientDataSet) then
      begin
        if ((Temp as TClientDataSet).ProviderName <> '') then
          (Temp as TClientDataSet).Active := Table_Active;
      end;

      if not Table_Active then
        if (Temp is TQuery) then
        begin
          (Temp as TQuery).Close;
          (Temp as TQuery).Unprepare;
        end;
    end;
end;

procedure TReportBaseDM.DataModuleCreate(Sender: TObject);
begin
   ActiveTables(True);
end;

procedure TReportBaseDM.DataModuleDestroy(Sender: TObject);
begin
   ActiveTables(False);
end;

end.
