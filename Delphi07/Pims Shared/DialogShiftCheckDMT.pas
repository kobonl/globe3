(*
  MRA:29-NOV-2016 PIM-242
  - Check Shift during In/Out Scan
  MRA:17-FEB-2017 PIM-242
  - Rework:
    - Use early/late margins from plant
    - Always include employee's shift
*)
unit DialogShiftCheckDMT;

interface

uses
  SysUtils, Classes, ORASystemDMT, CalculateTotalHoursDMT, Oracle, DB,
  OracleData, DBClient;

type
  TDialogShiftCheckDM = class(TDataModule)
    oqFirstScan: TOracleQuery;
    oqShift: TOracleQuery;
    cdsShift: TClientDataSet;
    cdsShiftPLANT_CODE: TStringField;
    cdsShiftSHIFT_NUMBER: TIntegerField;
    dsShift: TDataSource;
    cdsShiftDESCRIPTION: TStringField;
    cdsShiftSTARTDATE: TDateField;
    cdsShiftSTART: TDateTimeField;
    cdsShiftEND: TDateTimeField;
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
    FDefaultShiftNumber: Integer;
  public
    { Public declarations }
    function ShiftCheck(
      AEmployeeNumber, AShiftNumber: Integer;
      APlantCode, AWorkspotCode: String; AByWorkspot: Integer;
      AAllScans: Integer): Integer;
    property DefaultShiftNumber: Integer read FDefaultShiftNumber write FDefaultShiftNumber;
  end;

var
  DialogShiftCheckDM: TDialogShiftCheckDM;

implementation

uses
  UGlobalFunctions;

{$R *.dfm}

{ TDialogShiftCheckDM }

procedure TDialogShiftCheckDM.DataModuleCreate(Sender: TObject);
begin
  cdsShift.CreateDataSet;
  cdsShift.LogChanges := False;
end;

function TDialogShiftCheckDM.ShiftCheck(
  AEmployeeNumber, AShiftNumber: Integer;
  APlantCode, AWorkspotCode: String; AByWorkspot: Integer;
  AAllScans: Integer): Integer;
var
  CurrentNow: TDateTime;
  AShiftDayRecord: TShiftDayRecord;
  Margin: Integer;
  function DetermineFirstScan: Integer;
  begin
    Result := -1;
    with oqFirstScan do
    begin
      ClearVariables;
      SetVariable('EMPLOYEE_NUMBER', AEmployeeNumber);
      SetVariable('BYWORKSPOT', AByWorkspot);
      SetVariable('PLANT_CODE', APlantCode);
      SetVariable('WORKSPOT_CODE', AWorkspotCode);
      SetVariable('TIMEIN', CurrentNow - 0.5);
      SetVariable('TIMENOW', RoundTime(CurrentNow, 1));
      SetVariable('ALLSCANS', 1);
      Execute;
      // When found: Use the shift of the first found scan for this scan.
      if not Eof then
        Result := FieldAsInteger('SHIFT_NUMBER');
    end;
  end; // DetermineFirstScan
  function DetermineShift(AMargin: Integer): Integer;
  var
    SameShiftFound: Boolean;
    SameShiftValid: Boolean;
    InscanMarginEarly: Double;
    InscanMarginLate: Double;
    EarlyMargin: Double;
    LateMargin: Double;
    function ValidScan: Boolean;
    begin
      Result := (CurrentNow >= (AShiftDayRecord.AStart - EarlyMargin /24)) and
        (CurrentNow <= (AShiftDayRecord.AStart + LateMargin /24))
    end;
  begin
    SameShiftFound := False;
    SameShiftValid := False;
    DefaultShiftNumber := -1;
    // Determine the shift here for scan made at CurrentNow
    AShiftDayRecord.ADate := Trunc(CurrentNow);
    AShiftDayRecord.AStart := CurrentNow;
    AShiftDayRecord.AEnd := CurrentNow;
    AShiftDayRecord.AValid := False;
    AShiftDayRecord.APlantCode := APlantCode;
    with oqShift do
    begin
      ClearVariables;
      SetVariable('PLANT_CODE', APlantCode);
      Execute;
      while not Eof do
      begin
        // Store Plant-margin to margin here
        InscanMarginEarly := FieldAsInteger('INSCAN_MARGIN_EARLY') / 60;
        InscanMarginLate := FieldAsInteger('INSCAN_MARGIN_LATE') / 60;
        EarlyMargin := AMargin + InscanMarginEarly;
        LateMargin := AMargin + InscanMarginLate;
        AShiftDayRecord.AShiftNumber := FieldAsInteger('SHIFT_NUMBER');
        AShiftDay.DetermineShiftStartEnd(AShiftDayRecord);
        // Always include employee's shift
        if AShiftDayRecord.AValid or
          (AShiftNumber = AShiftDayRecord.AShiftNumber) then
        begin
          // Only compare with start of the shift.
          // Always include the employee's shift
          if
            (AShiftNumber = AShiftDayRecord.AShiftNumber) or ValidScan then
          begin
            // Is the default shift of the employee included here?
            if AShiftNumber = AShiftDayRecord.AShiftNumber then
            begin
              DefaultShiftNumber := AShiftNumber;
              SameShiftFound := True;
              if ValidScan then
                SameShiftValid := AShiftDayRecord.AValid;
            end;
{$IFDEF DEBUG1}
            WLog('ShiftCheck:' +
              ' Now=' + DateTimeToStr(CurrentNow) +
              ' Shift=' + IntToStr(AShiftDayRecord.AShiftNumber) +
              ' Start=' + DateTimeToStr(AShiftDayRecord.AStart) +
              ' End=' + DateTimeToStr(AShiftDayRecord.AEnd) +
              ' Valid=' + Bool2Str(AShiftDayRecord.AValid)
              );
{$ENDIF}
            cdsShift.Append;
            cdsShift.FieldByName('PLANT_CODE').AsString := APlantCode;
            cdsShift.FieldByName('SHIFT_NUMBER').AsInteger :=
              AShiftDayRecord.AShiftNumber;
            cdsShift.FieldByName('DESCRIPTION').AsString :=
              FieldAsString('DESCRIPTION');
            cdsShift.FieldByName('STARTDATE').AsDateTime :=
              Trunc(AShiftDayRecord.AStart);
            cdsShift.FieldByName('START').AsDateTime := AShiftDayRecord.AStart;
            cdsShift.FieldByName('END').AsDateTime := AShiftDayRecord.AEnd;
            cdsShift.Post;
{$IFDEF DEBUG1}
            WLog('-> Found');
{$ENDIF}
          end; // if
        end; // if
        Next;
      end; // while
    end; // with
    if cdsShift.RecordCount = 0 then
      Result := 0 // Nothing found
    else
      if cdsShift.RecordCount = 1 then
      begin
{$IFDEF DEBUG1}
        WLog('-> One shift found: ' +
          IntToStr(cdsShift.FieldByName('SHIFT_NUMBER').AsInteger));
{$ENDIF}
        Result := cdsShift.FieldByName('SHIFT_NUMBER').AsInteger;
      end
      else
      begin
        if SameShiftFound and SameShiftValid then
          Result := AShiftNumber
        else
          Result := -1; // Multiple shifts found
      end;
  end; // DetermineShift
begin
  CurrentNow := Now;

  cdsShift.EmptyDataSet;

  // First get the first scan
  Result := DetermineFirstScan;
  if Result = -1 then
  begin
    // Search till we find a shift (1 or more)
    Margin := 1;
    repeat
      Result := DetermineShift(Margin);
      inc(Margin);
    until (Margin = 23) or (Result <> 0);
    if Result = 0 then
      Result := -1;
  end;
end; // ShiftCheck

end.
