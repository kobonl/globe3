unit ULoginConst;

interface

const
// user rights consts
//Pims consts
  // register path - dialog login   - Pims
  APL_REGROOT_PATH_LOGIN_PIMS = '\Software\ABS\Pims\Login\';
  APL_MENU_LOGIN_PIMS = 120000;

//ProdScreen consts
  APL_REGROOT_PATH_LOGIN_PRODSCREEN = '\Software\ABS\PIMS\ProdScreen\Login';
  APL_MENU_LOGIN_PRODSCREEN = 130000;

// Administrator tool consts
  APL_REGROOT_PATH_LOGIN_ADM  = '\Software\ABS\Pims\ADMTOOL\';
  APL_MENU_LOGIN_ADM = 110000;

implementation

end.
