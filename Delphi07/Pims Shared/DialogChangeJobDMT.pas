(*
  MRA:26-APR-2013  TD-22462 Bugfix.
  - Machines: Changing job on machine-level does not work.
  MRA:30-MAY-2014 20015178
  - Measure Performance without employees
  MRA:17-JUN-2014 20015178.70
  - Always enable default-job, not only for this order.
*)
unit DialogChangeJobDMT;

interface

uses
  SysUtils, Classes, ORASystemDMT, PersonalScreenDMT, Oracle, DB,
  OracleData, Dialogs, {UProductionScreen} {UPersonalScreen}
  UProductionScreenDefs;

type
  TDialogChangeJobDM = class(TDataModule)
    dsJobs: TDataSource;
    odsJobs: TOracleDataSet;
    odsJobsWORKSPOT_CODE: TStringField;
    odsJobsJOB_CODE: TStringField;
    odsJobsDESCRIPTION: TStringField;
    procedure odsJobsFilterRecord(DataSet: TDataSet; var Accept: Boolean);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
    FProdScreenType: TProdScreenType;
    FPlantCode: String;
    FMachineCode: String;
    FWorkspotCode: String;
    FMachineDescription: String;
    FWorkspotDescription: String;
    FJobsFound: Boolean;
    FCurrentJobCode: String;
    FTimeRecByMachine: Boolean;
  public
    { Public declarations }
    procedure DetermineJobs;
    property ProdScreenType: TProdScreenType read FProdScreenType
      write FProdScreenType;
    property PlantCode: String read FPlantCode write FPlantCode;
    property MachineCode: String read FMachineCode write FMachineCode;
    property MachineDescription: String read FMachineDescription
      write FMachineDescription;
    property WorkspotCode: String read FWorkspotCode write FWorkspotCode;
    property WorkspotDescription: String read FWorkspotDescription
      write FWorkspotDescription;
    property JobsFound: Boolean read FJobsFound write FJobsFound;
    property CurrentJobCode: String read FCurrentJobCode write FCurrentJobCode;
    property TimeRecByMachine: Boolean read FTimeRecByMachine
      write FTimeRecByMachine; // 20015178
  end;

var
  DialogChangeJobDM: TDialogChangeJobDM;

implementation

{$R *.dfm}

uses
  UPimsConst;

{ TChangeJobDialogDM }

procedure TDialogChangeJobDM.DetermineJobs;
var
  SelectStr: String;
begin
  // TD-22462 For machines: To prevent it gives double value because
  //          job-descriptions are different for same job-codes, use
  //          a group by to get rid of them.
  if ProdScreenType = pstMachineTimeRec then // Machine level
  begin
{
      'SELECT DISTINCT J.JOB_CODE, J.DESCRIPTION, NULL AS WORKSPOT_CODE ' + NL +
      'FROM JOBCODE J INNER JOIN WORKSPOT W ON ' + NL +
      '  J.PLANT_CODE = W.PLANT_CODE AND ' + NL +
      '  J.WORKSPOT_CODE = W.WORKSPOT_CODE ' + NL +
      'WHERE J.SHOW_AT_PRODUCTIONSCREEN_YN = ''Y'' ' + NL +
      '  AND ((J.DATE_INACTIVE IS NULL) OR ' + NL +
      '  (J.DATE_INACTIVE IS NOT NULL AND J.DATE_INACTIVE > SYSDATE)) ' + NL +
      '  AND J.PLANT_CODE = :PLANT_CODE ' + NL +
      '  AND W.MACHINE_CODE = :MACHINE_CODE ' + NL +
      '  AND J.JOB_CODE NOT IN(:DOWNJOB1, :DOWNJOB2, :NOSCANJOB, :BREAKJOB, :NOJOB) ' + NL +
      'ORDER BY J.DESCRIPTION '
}
    // 20015178 Only trigger on EnableMachineTimeRec
    // 20015178.70 Always use default-job-functionality!
//    if ORASystemDM.EnableMachineTimeRec {and TimeRecByMachine } then
      SelectStr :=
        'SELECT J.JOB_CODE, MAX(J.DESCRIPTION) DESCRIPTION, MIN(J.DEFAULT_YN) AS DEFAULT_YN, ' +
        'NULL AS WORKSPOT_CODE ' + NL;
//    else
//      SelectStr :=
//        'SELECT J.JOB_CODE, MAX(J.DESCRIPTION) DESCRIPTION, NULL AS WORKSPOT_CODE ' + NL;
    SelectStr := SelectStr +
      'FROM JOBCODE J INNER JOIN WORKSPOT W ON ' + NL +
      '  J.PLANT_CODE = W.PLANT_CODE AND ' + NL +
      '  J.WORKSPOT_CODE = W.WORKSPOT_CODE ' + NL +
      'WHERE J.SHOW_AT_PRODUCTIONSCREEN_YN = ''Y'' ' + NL +
      '  AND ((J.DATE_INACTIVE IS NULL) OR ' + NL +
      '  (J.DATE_INACTIVE IS NOT NULL AND J.DATE_INACTIVE > SYSDATE)) ' + NL +
      '  AND J.PLANT_CODE = :PLANT_CODE ' + NL +
      '  AND W.MACHINE_CODE = :MACHINE_CODE ' + NL +
      '  AND J.JOB_CODE NOT IN(:DOWNJOB1, :DOWNJOB2, :NOSCANJOB, :BREAKJOB, :NOJOB) ' + NL +
      'GROUP BY J.JOB_CODE ' + NL;
    // 20015178 Only trigger on EnableMachineTimeRec
    // 20015178.70 Always use default-job-functionality!
//    if ORASystemDM.EnableMachineTimeRec {and TimeRecByMachine } then
      SelectStr := SelectStr +
        'ORDER BY 3 DESC, 2 ';
//    else
//      SelectStr := SelectStr +
//        'ORDER BY 2 ';
  end
  else
  begin
    // Workspot level
    SelectStr :=
      'SELECT J.WORKSPOT_CODE, J.JOB_CODE, J.DESCRIPTION ' + NL +
      'FROM JOBCODE J INNER JOIN WORKSPOT W ON ' + NL +
      '  J.PLANT_CODE = W.PLANT_CODE AND ' + NL +
      '  J.WORKSPOT_CODE = W.WORKSPOT_CODE ' + NL +
      'WHERE J.SHOW_AT_PRODUCTIONSCREEN_YN = ''Y'' ' + NL +
      '  AND ((J.DATE_INACTIVE IS NULL) OR ' + NL +
      '  (J.DATE_INACTIVE IS NOT NULL AND J.DATE_INACTIVE > SYSDATE)) ' + NL +
      '  AND J.PLANT_CODE = :PLANT_CODE ' + NL +
      '  AND W.WORKSPOT_CODE = :WORKSPOT_CODE ' + NL +
      '  AND J.JOB_CODE NOT IN(:DOWNJOB1, :DOWNJOB2, :NOSCANJOB, :BREAKJOB, :NOJOB) ' + NL;
    // 20015178 Only trigger on EnableMachineTimeRec
    // 20015178.70 Always use default-job-functionality!
//    if ORASystemDM.EnableMachineTimeRec {and TimeRecByMachine} then
      SelectStr := SelectStr +
        'ORDER BY J.DEFAULT_YN DESC, J.DESCRIPTION ';
//    else
//      SelectStr := SelectStr +
//        'ORDER BY J.DESCRIPTION ';
  end;
  odsJobs.Close;
  odsJobs.SQL.Clear;
  odsJobs.SQL.Add(SelectStr);
// odsJobs.SQL.SaveToFile('c:\temp\getjobs.sql');
  odsJobs.DeleteVariables;
  odsJobs.DeclareAndSet('PLANT_CODE', otString, PlantCode);
  if ProdScreenType = pstMachineTimeRec then // Machine level
    odsJobs.DeclareAndSet('MACHINE_CODE', otString, MachineCode)
  else
    odsJobs.DeclareAndSet('WORKSPOT_CODE', otString, WorkspotCode);
  odsJobs.DeclareAndSet('DOWNJOB1', otString, MECHANICAL_DOWN_JOB);
  odsJobs.DeclareAndSet('DOWNJOB2', otString, NO_MERCHANDIZE_JOB);
  odsJobs.DeclareAndSet('NOSCANJOB', otString, NO_SCAN);
  odsJobs.DeclareAndSet('BREAKJOB', otString, BREAKJOB);
  odsJobs.DeclareAndSet('NOJOB', otString, NOJOB);
  odsJobs.Open;
  JobsFound := not odsJobs.Eof;
end;

procedure TDialogChangeJobDM.odsJobsFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  Accept := True;
{
  // Get only workspot that are put on the Production-Screen.
  if ProdScreenType = pstMachineTimeRec then // Machine level
    Accept := PersonalScreenDM.MachineWorkspotExists(
      DataSet.FieldByName('PLANT_CODE').AsString,
      DataSet.FieldByName('MACHINE_CODE').AsString,
      DataSet.FieldByName('WORKSPOT_CODE').AsString);
}
end;

procedure TDialogChangeJobDM.DataModuleDestroy(Sender: TObject);
begin
  odsJobs.Close;
end;

end.
