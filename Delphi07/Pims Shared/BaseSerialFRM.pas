unit BaseSerialFRM;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BasePimsFRM, ImgList, dxExEdtr, dxEdLib, dxCntner, dxEditor,
  dxDBELib, ExtCtrls, ComDrvN,  StdCtrls, Buttons;

type
  TCodeReadEvent = procedure(const Code: String) of object;

type
  TBaseSerialForm = class(TBasePimsForm)
    Port1: TCommPortDriverN;
    StyleController: TdxEditStyleController;
    pnlBottom: TPanel;
    btnOk: TBitBtn;
    btnCancel: TBitBtn;
    StyleControllerRequired: TdxEditStyleController;
    procedure Port1ReceiveData(Sender: TObject; DataPtr: Pointer;
      DataSize: Integer);
    procedure pnlBottomResize(Sender: TObject);
  private
    { Private declarations }
    FLastChip       : String;
    FLastComReceive : String;
    FOnCodeRead: TCodeReadEvent;
    procedure DoCodeRead(const Code: String);
  public
    { Public declarations }
    property OnCodeRead: TCodeReadEvent read FOnCodeRead write FOnCodeRead;
    procedure FlushScanner;
    procedure OpenScanner;
    procedure CloseScanner;
  end;

var
  BaseSerialForm: TBaseSerialForm;

implementation

{$IFDEF PIMSDUTCH}
{$R *NLD.DFM}
{$ELSE}
  {$IFDEF PIMSGERMAN}
  {$R *DEU.DFM}
  {$ELSE}
    {$IFDEF PIMSFRENCH}
    {$R *FRA.DFM}
    {$ELSE}
    {$R *.DFM}
    {$ENDIF}
  {$ENDIF}
{$ENDIF}

procedure TBaseSerialForm.CloseScanner;
begin
  if Port1.Connected then
    Port1.Disconnect;
end;

procedure TBaseSerialForm.DoCodeRead(const Code: String);
begin
  CloseScanner;
  if Assigned(FOnCodeRead) then
    FOnCodeRead(Code);
  FlushScanner;
  OpenScanner;
end;

procedure TBaseSerialForm.FlushScanner;
begin
  FLastChip        := '';
  FLastComReceive  := '';
end;

procedure TBaseSerialForm.OpenScanner;
begin
  if not Port1.Connected then
    Port1.Connect;
end;

procedure TBaseSerialForm.Port1ReceiveData(Sender: TObject;
  DataPtr: Pointer; DataSize: Integer);
const
{ Serial Communication }
  LIF = #10; // LF line feed
  CRT = #13; // CR carriage return
var
  S,
  NewRead : String;
  CheckCR,
  CheckLF    : Integer;
begin
  inherited;
  S := String(PChar(DataPtr));
  SetLength(S, DataSize);
  S := FLastComReceive + S;
  FLastComReceive := S;

  CheckCR := Pos(CRT, S);
  CheckLF := Pos(LIF, S);

  if (CheckCr = CheckLF - 1) then
  begin
    NewRead := Copy(S, 1, (CheckLF - 2));

    if (NewRead <> FLastChip) then
    begin
      FLastChip := NewRead;
      DoCodeRead(NewRead);
    end;
    FLastComReceive := '';
    Delete(S, 1, CheckLF);
  end;
end;

procedure TBaseSerialForm.pnlBottomResize(Sender: TObject);
begin
  inherited;
  // centre buttons
  (Sender as TPanel).ParentBackground := False;
  (Sender as TPanel).ParentColor      := False;
  btnOk.Left     := (Sender as TPanel).Width div 2 - btnOk.Width - 4;
  btnCancel.Left := (Sender as TPanel).Width div 2 + 4;
end;

end.
