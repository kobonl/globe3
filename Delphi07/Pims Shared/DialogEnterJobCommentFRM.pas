(*
  PIM-50
  - Dialog for entering a job comment
*)
unit DialogEnterJobCommentFRM;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseDialogFRM, dxCntner, Menus, StdActns, ActnList, ImgList,
  StdCtrls, Buttons, ComCtrls, ExtCtrls, ORASystemDMT, UPimsConst, jpeg;

type
  TDialogEnterJobCommentF = class(TBaseDialogForm)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Edit1: TEdit;
    procedure btnOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    ThisTop, ThisLeft, ThisHeight, ThisWidth: Integer;
    FComment: String;
    procedure WMSysCommand(var Msg: TWMSysCommand); message WM_SYSCOMMAND;
  public
    { Public declarations }
    procedure AssignPosition(ParentForm: TForm);
    property Comment: String read FComment write FComment;
  end;

var
  DialogEnterJobCommentF: TDialogEnterJobCommentF;

implementation

{$R *.dfm}

uses
  UPimsMessageRes;

procedure TDialogEnterJobCommentF.FormCreate(Sender: TObject);
begin
  inherited;
  ThisTop := Top;
  ThisLeft := Left;
  ThisWidth := Width;
  ThisHeight := Height;
end; // FormCreate

procedure TDialogEnterJobCommentF.AssignPosition(ParentForm: TForm);
begin
  Top := ParentForm.Top + Round(ParentForm.Height / 4);
  Left := ParentForm.Left + Round(ParentForm.Width / 4);
  Height := ThisHeight;
  Width := ThisWidth;
end;

procedure TDialogEnterJobCommentF.btnOKClick(Sender: TObject);
begin
  inherited;
  if Edit1.Text = '' then
  begin
    ModalResult := mrNone;
    DisplayMessage(SPimsPleaseEnterComment, mtConfirmation, [mbOK]);
  end;
  Comment := Edit1.Text;
end;

procedure TDialogEnterJobCommentF.FormShow(Sender: TObject);
begin
  inherited;
  Edit1.Text := '';
  Edit1.SetFocus;
end;

// Prevent it is being moved
procedure TDialogEnterJobCommentF.WMSysCommand(var Msg: TWMSysCommand);
begin
  if ((Msg.CmdType and $FFF0) = SC_MOVE) or
    ((Msg.CmdType and $FFF0) = SC_SIZE) then
  begin
    Msg.Result := 0;
    Exit;
  end;
  inherited;
end;

end.
