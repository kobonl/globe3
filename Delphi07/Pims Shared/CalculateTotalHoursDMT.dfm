object CalculateTotalHoursDM: TCalculateTotalHoursDM
  OldCreateOrder = True
  OnCreate = DataModuleCreate
  Left = 296
  Top = 213
  Height = 531
  Width = 741
  object odsSelector: TOracleDataSet
    Session = ORASystemDM.OracleSession
    Left = 344
    Top = 128
  end
  object odsPrevScan1: TOracleDataSet
    SQL.Strings = (
      'SELECT'
      '  TRS.DATETIME_IN ,'
      '  TRS.DATETIME_OUT'
      'FROM'
      '  TIMEREGSCANNING TRS'
      'WHERE'
      '  TRS.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND'
      '  ROUND(TRS.DATETIME_OUT, '#39'MI'#39') = ROUND(:DATETIME_OUT, '#39'MI'#39') AND'
      '  ROUND(TRS.DATETIME_OUT, '#39'MI'#39') <> ROUND(:D_OUT, '#39'MI'#39')'
      ' '
      ' ')
    Variables.Data = {
      0300000003000000100000003A454D504C4F5945455F4E554D42455203000000
      00000000000000000D0000003A4441544554494D455F4F55540C000000000000
      0000000000060000003A445F4F55540C0000000000000000000000}
    Session = ORASystemDM.OracleSession
    Left = 464
    Top = 16
  end
  object ClientDataSetBShift: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'DataSetProviderBShift'
    Left = 56
    Top = 120
  end
  object ClientDataSetBDept: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'DataSetProviderBDept'
    Left = 56
    Top = 72
  end
  object ClientDataSetBEmpl: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'DataSetProviderBEmpl'
    Left = 56
    Top = 16
  end
  object DataSetProviderBEmpl: TDataSetProvider
    Left = 200
    Top = 16
  end
  object DataSetProviderBDept: TDataSetProvider
    Left = 200
    Top = 72
  end
  object DataSetProviderBShift: TDataSetProvider
    Left = 200
    Top = 128
  end
  object ClientDataSetShift: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'DataSetProviderShift'
    Left = 56
    Top = 344
  end
  object DataSetProviderShift: TDataSetProvider
    Left = 200
    Top = 344
  end
  object ClientDataSetTBShift: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'DataSetProviderTBShift'
    Left = 56
    Top = 288
  end
  object ClientDataSetTBDept: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'DataSetProviderTBDept'
    Left = 56
    Top = 232
  end
  object ClientDataSetTBEmpl: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'DataSetProviderTBEmpl'
    Left = 56
    Top = 168
  end
  object DataSetProviderTBEmpl: TDataSetProvider
    Left = 200
    Top = 176
  end
  object DataSetProviderTBDept: TDataSetProvider
    Left = 200
    Top = 232
  end
  object DataSetProviderTBShift: TDataSetProvider
    Left = 200
    Top = 288
  end
  object odsNextScan1: TOracleDataSet
    SQL.Strings = (
      'SELECT'
      '  TRS.DATETIME_IN,'
      '  TRS.DATETIME_OUT'
      'FROM '
      '  TIMEREGSCANNING TRS'
      'WHERE'
      '  TRS.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND'
      '  ROUND(TRS.DATETIME_OUT, '#39'MI'#39') = ROUND(:DATETIME_OUT, '#39'MI'#39') AND'
      '  ROUND(TRS.DATETIME_IN, '#39'MI'#39') <> ROUND(:D_IN, '#39'MI'#39')'
      ' ')
    Variables.Data = {
      0300000003000000100000003A454D504C4F5945455F4E554D42455203000000
      00000000000000000D0000003A4441544554494D455F4F55540C000000000000
      0000000000050000003A445F494E0C0000000000000000000000}
    Session = ORASystemDM.OracleSession
    Left = 464
    Top = 72
  end
  object oqShiftSchedule: TOracleQuery
    SQL.Strings = (
      'SELECT'
      '  S.PLANT_CODE,'
      '  S.SHIFT_NUMBER'
      'FROM'
      '  SHIFTSCHEDULE S'
      'WHERE'
      '  S.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND'
      '  S.SHIFT_SCHEDULE_DATE = :SHIFT_SCHEDULE_DATE AND'
      '  S.SHIFT_NUMBER <> -1'
      'ORDER BY'
      '  S.PLANT_CODE,'
      '  S.SHIFT_NUMBER')
    Session = ORASystemDM.OracleSession
    ReadBuffer = 1
    Variables.Data = {
      0300000002000000100000003A454D504C4F5945455F4E554D42455203000000
      0000000000000000140000003A53484946545F5343484544554C455F44415445
      0C0000000000000000000000}
    Left = 344
    Top = 72
  end
  object oqEmployeeShift: TOracleQuery
    SQL.Strings = (
      'SELECT'
      '  E.PLANT_CODE,'
      '  E.SHIFT_NUMBER'
      'FROM'
      '  EMPLOYEE E'
      'WHERE'
      '  E.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND'
      '  E.SHIFT_NUMBER IS NOT NULL'
      '')
    Session = ORASystemDM.OracleSession
    ReadBuffer = 1
    Variables.Data = {
      0300000001000000100000003A454D504C4F5945455F4E554D42455203000000
      0000000000000000}
    Left = 464
    Top = 128
  end
  object oqRequest: TOracleQuery
    SQL.Strings = (
      'SELECT'
      '  R.REQUESTED_EARLY_TIME,'
      '  R.REQUESTED_LATE_TIME'
      'FROM'
      '  REQUESTEARLYLATE R'
      'WHERE '
      '  ('
      '  R.REQUEST_DATE >= :DATEFROM AND'
      '  R.REQUEST_DATE <= :DATETO'
      '  )  AND'
      '  R.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER'
      'ORDER BY'
      '  R.REQUESTED_EARLY_TIME,'
      '  R.REQUESTED_LATE_TIME'
      ''
      '')
    Session = ORASystemDM.OracleSession
    ReadBuffer = 1
    Variables.Data = {
      0300000003000000100000003A454D504C4F5945455F4E554D42455203000000
      0000000000000000090000003A4441544546524F4D0C00000000000000000000
      00070000003A44415445544F0C0000000000000000000000}
    Left = 344
    Top = 16
  end
  object oqContractgroup: TOracleQuery
    SQL.Strings = (
      'SELECT'
      '  C.ROUND_TRUNC_SALARY_HOURS,'
      '  C.ROUND_MINUTE'
      'FROM'
      '  CONTRACTGROUP C,'
      '  EMPLOYEE E'
      'WHERE'
      '  C.CONTRACTGROUP_CODE = E.CONTRACTGROUP_CODE AND'
      '  E.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER'
      ''
      '')
    Session = ORASystemDM.OracleSession
    ReadBuffer = 1
    Optimize = False
    Variables.Data = {
      0300000001000000100000003A454D504C4F5945455F4E554D42455203000000
      0000000000000000}
    Scrollable = True
    Left = 344
    Top = 184
  end
  object odsSHE: TOracleDataSet
    SQL.Strings = (
      'SELECT'
      '  SHE.SALARY_DATE,'
      '  SHE.HOURTYPE_NUMBER,'
      '  SHE.SALARY_MINUTE'
      'FROM'
      '  SALARYHOURPEREMPLOYEE SHE'
      'WHERE'
      '  SHE.SALARY_DATE = :SALARY_DATE AND'
      '  SHE.MANUAL_YN = '#39'N'#39' AND'
      '  SHE.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND'
      
        '  ((:HOURTYPE_NUMBER = -1) OR (SHE.HOURTYPE_NUMBER = :HOURTYPE_N' +
        'UMBER))'
      'ORDER BY'
      '  SHE.HOURTYPE_NUMBER DESC  ')
    Variables.Data = {
      03000000030000000C0000003A53414C4152595F444154450C00000000000000
      00000000100000003A454D504C4F5945455F4E554D4245520300000000000000
      00000000100000003A484F5552545950455F4E554D4245520300000000000000
      00000000}
    Session = ORASystemDM.OracleSession
    Left = 344
    Top = 240
  end
  object oqFindPlanningBlocks: TOracleQuery
    SQL.Strings = (
      'SELECT'
      '  NVL(ET.SCHEDULED_TIMEBLOCK_1,'#39'-'#39') SCHEDULED_TIMEBLOCK_1,'
      '  NVL(ET.SCHEDULED_TIMEBLOCK_2,'#39'-'#39') SCHEDULED_TIMEBLOCK_2,'
      '  NVL(ET.SCHEDULED_TIMEBLOCK_3,'#39'-'#39') SCHEDULED_TIMEBLOCK_3,'
      '  NVL(ET.SCHEDULED_TIMEBLOCK_4,'#39'-'#39') SCHEDULED_TIMEBLOCK_4,'
      '  NVL(ET.SCHEDULED_TIMEBLOCK_5,'#39'-'#39') SCHEDULED_TIMEBLOCK_5,'
      '  NVL(ET.SCHEDULED_TIMEBLOCK_6,'#39'-'#39') SCHEDULED_TIMEBLOCK_6,'
      '  NVL(ET.SCHEDULED_TIMEBLOCK_7,'#39'-'#39') SCHEDULED_TIMEBLOCK_7,'
      '  NVL(ET.SCHEDULED_TIMEBLOCK_8,'#39'-'#39') SCHEDULED_TIMEBLOCK_8,'
      '  NVL(ET.SCHEDULED_TIMEBLOCK_9,'#39'-'#39') SCHEDULED_TIMEBLOCK_9,'
      '  NVL(ET.SCHEDULED_TIMEBLOCK_10,'#39'-'#39') SCHEDULED_TIMEBLOCK_10'
      'FROM'
      '  EMPLOYEEPLANNING ET'
      'WHERE'
      '  ET.EMPLOYEEPLANNING_DATE = :EMPLOYEEPLANNING_DATE AND'
      '  ET.PLANT_CODE = :PLANT_CODE AND'
      '  ET.WORKSPOT_CODE = :WORKSPOT_CODE AND'
      '  ET.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND'
      '  ET.SHIFT_NUMBER = :SHIFT_NUMBER AND'
      '  ('
      '    ET.SCHEDULED_TIMEBLOCK_1 IN ('#39'A'#39', '#39'B'#39', '#39'C'#39') OR'
      '    ET.SCHEDULED_TIMEBLOCK_2 IN ('#39'A'#39', '#39'B'#39', '#39'C'#39') OR'
      '    ET.SCHEDULED_TIMEBLOCK_3 IN ('#39'A'#39', '#39'B'#39', '#39'C'#39') OR'
      '    ET.SCHEDULED_TIMEBLOCK_4 IN ('#39'A'#39', '#39'B'#39', '#39'C'#39') OR'
      '    ET.SCHEDULED_TIMEBLOCK_5 IN ('#39'A'#39', '#39'B'#39', '#39'C'#39') OR'
      '    ET.SCHEDULED_TIMEBLOCK_6 IN ('#39'A'#39', '#39'B'#39', '#39'C'#39') OR'
      '    ET.SCHEDULED_TIMEBLOCK_7 IN ('#39'A'#39', '#39'B'#39', '#39'C'#39') OR'
      '    ET.SCHEDULED_TIMEBLOCK_8 IN ('#39'A'#39', '#39'B'#39', '#39'C'#39') OR'
      '    ET.SCHEDULED_TIMEBLOCK_9 IN ('#39'A'#39', '#39'B'#39', '#39'C'#39') OR'
      '    ET.SCHEDULED_TIMEBLOCK_10 IN ('#39'A'#39', '#39'B'#39', '#39'C'#39')'
      '  )'
      'UNION ALL'
      'SELECT'
      '  NVL(SP.SCHEDULED_TIMEBLOCK_1,'#39'-'#39'),'
      '  NVL(SP.SCHEDULED_TIMEBLOCK_2,'#39'-'#39'),'
      '  NVL(SP.SCHEDULED_TIMEBLOCK_3,'#39'-'#39'),'
      '  NVL(SP.SCHEDULED_TIMEBLOCK_4,'#39'-'#39'),'
      '  NVL(SP.SCHEDULED_TIMEBLOCK_5,'#39'-'#39'),'
      '  NVL(SP.SCHEDULED_TIMEBLOCK_6,'#39'-'#39'),'
      '  NVL(SP.SCHEDULED_TIMEBLOCK_7,'#39'-'#39'),'
      '  NVL(SP.SCHEDULED_TIMEBLOCK_8,'#39'-'#39'),'
      '  NVL(SP.SCHEDULED_TIMEBLOCK_9,'#39'-'#39'),'
      '  NVL(SP.SCHEDULED_TIMEBLOCK_10,'#39'-'#39')'
      'FROM'
      '  STANDARDEMPLOYEEPLANNING  SP'
      'WHERE'
      '  SP.DAY_OF_WEEK = :DAY_OF_WEEK AND'
      '  SP.PLANT_CODE = :PLANT_CODE AND'
      '  SP.WORKSPOT_CODE = :WORKSPOT_CODE AND'
      '  SP.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND'
      '  SP.SHIFT_NUMBER = :SHIFT_NUMBER AND'
      '  ('
      '    SP.SCHEDULED_TIMEBLOCK_1 IN ('#39'A'#39','#39'B'#39','#39'C'#39') OR'
      '    SP.SCHEDULED_TIMEBLOCK_2 IN ('#39'A'#39','#39'B'#39','#39'C'#39') OR'
      '    SP.SCHEDULED_TIMEBLOCK_3 IN ('#39'A'#39','#39'B'#39','#39'C'#39') OR'
      '    SP.SCHEDULED_TIMEBLOCK_4 IN ('#39'A'#39','#39'B'#39','#39'C'#39') OR'
      '    SP.SCHEDULED_TIMEBLOCK_5 IN ('#39'A'#39','#39'B'#39','#39'C'#39') OR'
      '    SP.SCHEDULED_TIMEBLOCK_6 IN ('#39'A'#39','#39'B'#39','#39'C'#39') OR'
      '    SP.SCHEDULED_TIMEBLOCK_7 IN ('#39'A'#39','#39'B'#39','#39'C'#39') OR'
      '    SP.SCHEDULED_TIMEBLOCK_8 IN ('#39'A'#39','#39'B'#39','#39'C'#39') OR'
      '    SP.SCHEDULED_TIMEBLOCK_9 IN ('#39'A'#39','#39'B'#39','#39'C'#39') OR'
      '    SP.SCHEDULED_TIMEBLOCK_10 IN ('#39'A'#39', '#39'B'#39', '#39'C'#39')'
      '  )'
      'UNION ALL'
      'SELECT'
      '  NVL(EA.AVAILABLE_TIMEBLOCK_1,'#39'-'#39'),'
      '  NVL(EA.AVAILABLE_TIMEBLOCK_2,'#39'-'#39'),'
      '  NVL(EA.AVAILABLE_TIMEBLOCK_3,'#39'-'#39'),'
      '  NVL(EA.AVAILABLE_TIMEBLOCK_4,'#39'-'#39'),'
      '  NVL(EA.AVAILABLE_TIMEBLOCK_5,'#39'-'#39'),'
      '  NVL(EA.AVAILABLE_TIMEBLOCK_6,'#39'-'#39'),'
      '  NVL(EA.AVAILABLE_TIMEBLOCK_7,'#39'-'#39'),'
      '  NVL(EA.AVAILABLE_TIMEBLOCK_8,'#39'-'#39'),'
      '  NVL(EA.AVAILABLE_TIMEBLOCK_9,'#39'-'#39'),'
      '  NVL(EA.AVAILABLE_TIMEBLOCK_10,'#39'-'#39')'
      'FROM'
      '  EMPLOYEEAVAILABILITY EA'
      'WHERE'
      '  EA.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND'
      '  EA.PLANT_CODE = :PLANT_CODE AND'
      '  EA.EMPLOYEEAVAILABILITY_DATE = :EMPLOYEEPLANNING_DATE AND'
      '  EA.SHIFT_NUMBER = :SHIFT_NUMBER AND'
      '  ('
      '    EA.AVAILABLE_TIMEBLOCK_1 = '#39'*'#39' OR'
      '    EA.AVAILABLE_TIMEBLOCK_2 = '#39'*'#39' OR'
      '    EA.AVAILABLE_TIMEBLOCK_3 = '#39'*'#39' OR'
      '    EA.AVAILABLE_TIMEBLOCK_4 = '#39'*'#39' OR'
      '    EA.AVAILABLE_TIMEBLOCK_5 = '#39'*'#39' OR'
      '    EA.AVAILABLE_TIMEBLOCK_6 = '#39'*'#39' OR'
      '    EA.AVAILABLE_TIMEBLOCK_7 = '#39'*'#39' OR'
      '    EA.AVAILABLE_TIMEBLOCK_8 = '#39'*'#39' OR'
      '    EA.AVAILABLE_TIMEBLOCK_9 = '#39'*'#39' OR'
      '    EA.AVAILABLE_TIMEBLOCK_10 = '#39'*'#39
      '  )'
      'UNION ALL'
      'SELECT'
      '  NVL(SA.AVAILABLE_TIMEBLOCK_1,'#39'-'#39'),'
      '  NVL(SA.AVAILABLE_TIMEBLOCK_2,'#39'-'#39'),'
      '  NVL(SA.AVAILABLE_TIMEBLOCK_3,'#39'-'#39'),'
      '  NVL(SA.AVAILABLE_TIMEBLOCK_4,'#39'-'#39'),'
      '  NVL(SA.AVAILABLE_TIMEBLOCK_5,'#39'-'#39'),'
      '  NVL(SA.AVAILABLE_TIMEBLOCK_6,'#39'-'#39'),'
      '  NVL(SA.AVAILABLE_TIMEBLOCK_7,'#39'-'#39'),'
      '  NVL(SA.AVAILABLE_TIMEBLOCK_8,'#39'-'#39'),'
      '  NVL(SA.AVAILABLE_TIMEBLOCK_9,'#39'-'#39'),'
      '  NVL(SA.AVAILABLE_TIMEBLOCK_10,'#39'-'#39')'
      'FROM'
      '  STANDARDAVAILABILITY SA'
      'WHERE'
      '  SA.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND'
      '  SA.PLANT_CODE = :PLANT_CODE AND'
      '  SA.DAY_OF_WEEK = :DAY_OF_WEEK AND'
      '  SA.SHIFT_NUMBER = :SHIFT_NUMBER AND'
      '  ('
      '    SA.AVAILABLE_TIMEBLOCK_1 = '#39'*'#39' OR'
      '    SA.AVAILABLE_TIMEBLOCK_2 = '#39'*'#39' OR'
      '    SA.AVAILABLE_TIMEBLOCK_3 = '#39'*'#39' OR'
      '    SA.AVAILABLE_TIMEBLOCK_4 = '#39'*'#39' OR'
      '    SA.AVAILABLE_TIMEBLOCK_5 = '#39'*'#39' OR'
      '    SA.AVAILABLE_TIMEBLOCK_6 = '#39'*'#39' OR'
      '    SA.AVAILABLE_TIMEBLOCK_7 = '#39'*'#39' OR'
      '    SA.AVAILABLE_TIMEBLOCK_8 = '#39'*'#39' OR'
      '    SA.AVAILABLE_TIMEBLOCK_9 = '#39'*'#39' OR'
      '    SA.AVAILABLE_TIMEBLOCK_10 = '#39'*'#39
      '  )'
      '')
    Session = ORASystemDM.OracleSession
    Variables.Data = {
      0300000006000000160000003A454D504C4F594545504C414E4E494E475F4441
      54450C00000000000000000000000B0000003A504C414E545F434F4445050000
      0000000000000000000E0000003A574F524B53504F545F434F44450500000000
      00000000000000100000003A454D504C4F5945455F4E554D4245520300000000
      000000000000000D0000003A53484946545F4E554D4245520300000000000000
      000000000C0000003A4441595F4F465F5745454B030000000000000000000000}
    Left = 464
    Top = 184
  end
  object oqWorkspotDept: TOracleQuery
    SQL.Strings = (
      'SELECT DEPARTMENT_CODE'
      'FROM WORKSPOT'
      'WHERE PLANT_CODE = :PLANT_CODE AND'
      'WORKSPOT_CODE = :WORKSPOT_CODE')
    Session = ORASystemDM.OracleSession
    Variables.Data = {
      03000000020000000B0000003A504C414E545F434F4445050000000000000000
      0000000E0000003A574F524B53504F545F434F44450500000000000000000000
      00}
    Left = 56
    Top = 400
  end
  object odsPrevScan2: TOracleDataSet
    SQL.Strings = (
      'SELECT'
      '  TRS.DATETIME_IN ,'
      '  TRS.DATETIME_OUT'
      'FROM'
      '  TIMEREGSCANNING TRS'
      'WHERE'
      '  TRS.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND'
      '  ROUND(TRS.DATETIME_OUT, '#39'MI'#39') = ROUND(:DATETIME_OUT, '#39'MI'#39')'
      ' '
      ' ')
    Variables.Data = {
      0300000002000000100000003A454D504C4F5945455F4E554D42455203000000
      00000000000000000D0000003A4441544554494D455F4F55540C000000000000
      0000000000}
    Session = ORASystemDM.OracleSession
    Left = 552
    Top = 16
  end
  object odsNextScan2: TOracleDataSet
    SQL.Strings = (
      'SELECT'
      '  TRS.DATETIME_IN ,'
      '  TRS.DATETIME_OUT'
      'FROM '
      '  TIMEREGSCANNING TRS'
      'WHERE'
      '  TRS.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND'
      '  ROUND(TRS.DATETIME_OUT, '#39'MI'#39') = ROUND(:DATETIME_OUT, '#39'MI'#39')'
      ' ')
    Variables.Data = {
      0300000002000000100000003A454D504C4F5945455F4E554D42455203000000
      00000000000000000D0000003A4441544554494D455F4F55540C000000000000
      0000000000}
    Session = ORASystemDM.OracleSession
    Left = 552
    Top = 72
  end
  object oqIsFirstScan: TOracleQuery
    SQL.Strings = (
      'SELECT'
      '  CASE :ADELETEACTION'
      '    WHEN 0 THEN IsFirstScan(EMPLOYEE_NUMBER, DATETIME_IN, NULL)'
      '    ELSE IsFirstScan(EMPLOYEE_NUMBER, DATETIME_IN, DATETIME_IN)'
      '  END FIRSTSCAN'
      'FROM'
      '  TIMEREGSCANNING'
      'WHERE'
      '  EMPLOYEE_NUMBER = :AEMPLOYEENUMBER AND'
      '  DATETIME_IN = :ADATETIMEIN')
    Session = ORASystemDM.OracleSession
    Variables.Data = {
      03000000030000000E0000003A4144454C455445414354494F4E030000000000
      000000000000100000003A41454D504C4F5945454E554D424552030000000000
      0000000000000C0000003A414441544554494D45494E0C000000000000000000
      0000}
    Left = 352
    Top = 296
  end
  object oqIsLastScan: TOracleQuery
    SQL.Strings = (
      'SELECT'
      '  CASE :ADELETEACTION'
      '  WHEN 0 THEN IsLastScan(EMPLOYEE_NUMBER, DATETIME_OUT, NULL)'
      '  ELSE IsLastScan(EMPLOYEE_NUMBER, DATETIME_OUT, DATETIME_IN)'
      '  END LASTSCAN'
      'FROM'
      '  TIMEREGSCANNING'
      'WHERE'
      '  EMPLOYEE_NUMBER = :AEMPLOYEENUMBER AND'
      '  DATETIME_OUT = :ADATETIMEOUT')
    Session = ORASystemDM.OracleSession
    Variables.Data = {
      03000000030000000E0000003A4144454C455445414354494F4E030000000000
      000000000000100000003A41454D504C4F5945454E554D424552030000000000
      0000000000000D0000003A414441544554494D454F55540C0000000000000000
      000000}
    Left = 464
    Top = 296
  end
  object oqFirstScan: TOracleQuery
    SQL.Strings = (
      'SELECT'
      '  MIN(DATETIME_IN) DATETIME_IN'
      'FROM'
      '  TIMEREGSCANNING'
      'WHERE'
      '  EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND'
      '  PROCESSED_YN = '#39'Y'#39' AND'
      '  DATETIME_IN BETWEEN :DATEFROM AND :DATETO'
      '  AND'
      '  ('
      
        '    (:CHECKDELETEDATE = 0) OR (:CHECKDELETEDATE = 1 AND DATETIME' +
        '_IN <> :DELETEDATE)'
      '  )')
    Session = ORASystemDM.OracleSession
    Variables.Data = {
      0300000005000000100000003A454D504C4F5945455F4E554D42455203000000
      0000000000000000090000003A4441544546524F4D0C00000000000000000000
      00070000003A44415445544F0C0000000000000000000000100000003A434845
      434B44454C455445444154450300000000000000000000000B0000003A44454C
      455445444154450C0000000000000000000000}
    Left = 584
    Top = 240
  end
  object oqLastScan: TOracleQuery
    SQL.Strings = (
      'SELECT'
      '  MAX(DATETIME_OUT) DATETIME_OUT'
      'FROM'
      '  TIMEREGSCANNING'
      'WHERE'
      '  EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND'
      '  PROCESSED_YN = '#39'Y'#39' AND'
      '  DATETIME_IN BETWEEN :DATEFROM and :DATETO'
      '  AND'
      '  ('
      
        '    (:CHECKDELETEDATE = 0) OR (:CHECKDELETEDATE = 1 AND DATETIME' +
        '_IN <> :DELETEDATE)'
      '  )'
      '')
    Session = ORASystemDM.OracleSession
    Variables.Data = {
      0300000005000000100000003A454D504C4F5945455F4E554D42455203000000
      0000000000000000090000003A4441544546524F4D0C00000000000000000000
      00070000003A44415445544F0C0000000000000000000000100000003A434845
      434B44454C455445444154450300000000000000000000000B0000003A44454C
      455445444154450C0000000000000000000000}
    Left = 584
    Top = 296
  end
end
