object PlanScanEmpDM: TPlanScanEmpDM
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Left = 262
  Top = 108
  Height = 715
  Width = 758
  object odsEmployeePlanning: TOracleDataSet
    SQL.Strings = (
      'SELECT'
      '  EP.PLANT_CODE,'
      '  EP.EMPLOYEEPLANNING_DATE,'
      '  EP.SHIFT_NUMBER,'
      '  EP.DEPARTMENT_CODE,'
      '  EP.WORKSPOT_CODE,'
      '  EP.EMPLOYEE_NUMBER,'
      '  E.SHORT_NAME,'
      '  E.DESCRIPTION,'
      '  E.TEAM_CODE,'
      '  E.FIRSTAID_YN,'
      '  E.FIRSTAID_EXP_DATE,'
      '  EP.SCHEDULED_TIMEBLOCK_1,'
      '  EP.SCHEDULED_TIMEBLOCK_2,'
      '  EP.SCHEDULED_TIMEBLOCK_3,'
      '  EP.SCHEDULED_TIMEBLOCK_4,'
      '  EP.SCHEDULED_TIMEBLOCK_5,'
      '  EP.SCHEDULED_TIMEBLOCK_6,'
      '  EP.SCHEDULED_TIMEBLOCK_7,'
      '  EP.SCHEDULED_TIMEBLOCK_8,'
      '  EP.SCHEDULED_TIMEBLOCK_9,'
      '  EP.SCHEDULED_TIMEBLOCK_10'
      'FROM '
      '  EMPLOYEEPLANNING EP,'
      '  EMPLOYEE E'
      'WHERE'
      '  ((E.ENDDATE IS NULL) OR (E.ENDDATE IS NOT NULL AND'
      '    E.ENDDATE >= :DATEFROM)) AND'
      '  EP.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER AND'
      '  (EP.EMPLOYEEPLANNING_DATE >= :DATEFROM AND'
      '   EP.EMPLOYEEPLANNING_DATE <= :DATETO) AND'
      '  ('
      '  EP.SCHEDULED_TIMEBLOCK_1 IN ('#39'A'#39', '#39'B'#39', '#39'C'#39') OR'
      '  EP.SCHEDULED_TIMEBLOCK_2 IN ('#39'A'#39', '#39'B'#39', '#39'C'#39') OR'
      '  EP.SCHEDULED_TIMEBLOCK_3 IN ('#39'A'#39', '#39'B'#39', '#39'C'#39') OR'
      '  EP.SCHEDULED_TIMEBLOCK_4 IN ('#39'A'#39', '#39'B'#39', '#39'C'#39') OR'
      '  EP.SCHEDULED_TIMEBLOCK_5 IN ('#39'A'#39', '#39'B'#39', '#39'C'#39') OR'
      '  EP.SCHEDULED_TIMEBLOCK_6 IN ('#39'A'#39', '#39'B'#39', '#39'C'#39') OR'
      '  EP.SCHEDULED_TIMEBLOCK_7 IN ('#39'A'#39', '#39'B'#39', '#39'C'#39') OR'
      '  EP.SCHEDULED_TIMEBLOCK_8 IN ('#39'A'#39', '#39'B'#39', '#39'C'#39') OR'
      '  EP.SCHEDULED_TIMEBLOCK_9 IN ('#39'A'#39', '#39'B'#39', '#39'C'#39') OR'
      '  EP.SCHEDULED_TIMEBLOCK_10 IN ('#39'A'#39', '#39'B'#39', '#39'C'#39')'
      '  )'
      '  AND'
      '  ('
      '    (:USER_NAME = '#39'*'#39') OR'
      '    (E.TEAM_CODE IN'
      
        '      (SELECT U.TEAM_CODE FROM TEAMPERUSER U WHERE U.USER_NAME =' +
        ' :USER_NAME))'
      ' )'
      '')
    Variables.Data = {
      0300000003000000090000003A4441544546524F4D0C00000000000000000000
      00070000003A44415445544F0C00000000000000000000000A0000003A555345
      525F4E414D45050000000000000000000000}
    QBEDefinition.QBEFieldDefs = {
      04000000150000000A000000504C414E545F434F444501000000000015000000
      454D504C4F594545504C414E4E494E475F444154450100000000000C00000053
      484946545F4E554D4245520100000000000F0000004445504152544D454E545F
      434F44450100000000000D000000574F524B53504F545F434F44450100000000
      000F000000454D504C4F5945455F4E554D4245520100000000000A0000005348
      4F52545F4E414D450100000000000B0000004445534352495054494F4E010000
      000000090000005445414D5F434F44450100000000000B000000464952535441
      49445F594E0100000000001100000046495253544149445F4558505F44415445
      010000000000150000005343484544554C45445F54494D45424C4F434B5F3101
      0000000000150000005343484544554C45445F54494D45424C4F434B5F320100
      00000000150000005343484544554C45445F54494D45424C4F434B5F33010000
      000000150000005343484544554C45445F54494D45424C4F434B5F3401000000
      0000150000005343484544554C45445F54494D45424C4F434B5F350100000000
      00150000005343484544554C45445F54494D45424C4F434B5F36010000000000
      150000005343484544554C45445F54494D45424C4F434B5F3701000000000015
      0000005343484544554C45445F54494D45424C4F434B5F380100000000001500
      00005343484544554C45445F54494D45424C4F434B5F39010000000000160000
      005343484544554C45445F54494D45424C4F434B5F3130010000000000}
    Session = ORASystemDM.OracleSession
    OnCalcFields = odsEmployeePlanningCalcFields
    Left = 72
    Top = 160
    object odsEmployeePlanningPLANLEVEL: TStringField
      FieldKind = fkCalculated
      FieldName = 'PLANLEVEL'
      Size = 1
      Calculated = True
    end
    object odsEmployeePlanningSTARTDATE: TDateTimeField
      FieldKind = fkCalculated
      FieldName = 'STARTDATE'
      Calculated = True
    end
    object odsEmployeePlanningENDDATE: TDateTimeField
      FieldKind = fkCalculated
      FieldName = 'ENDDATE'
      Calculated = True
    end
    object odsEmployeePlanningMARK: TBooleanField
      FieldKind = fkCalculated
      FieldName = 'MARK'
      Calculated = True
    end
    object odsEmployeePlanningPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Size = 6
    end
    object odsEmployeePlanningEMPLOYEEPLANNING_DATE: TDateTimeField
      FieldName = 'EMPLOYEEPLANNING_DATE'
    end
    object odsEmployeePlanningSHIFT_NUMBER: TIntegerField
      FieldName = 'SHIFT_NUMBER'
    end
    object odsEmployeePlanningDEPARTMENT_CODE: TStringField
      FieldName = 'DEPARTMENT_CODE'
      Size = 6
    end
    object odsEmployeePlanningWORKSPOT_CODE: TStringField
      FieldName = 'WORKSPOT_CODE'
      Size = 6
    end
    object odsEmployeePlanningEMPLOYEE_NUMBER: TIntegerField
      FieldName = 'EMPLOYEE_NUMBER'
    end
    object odsEmployeePlanningSHORT_NAME: TStringField
      FieldName = 'SHORT_NAME'
      Size = 6
    end
    object odsEmployeePlanningDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Size = 40
    end
    object odsEmployeePlanningTEAM_CODE: TStringField
      FieldName = 'TEAM_CODE'
      Size = 6
    end
    object odsEmployeePlanningFIRSTAID_YN: TStringField
      FieldName = 'FIRSTAID_YN'
      Size = 1
    end
    object odsEmployeePlanningFIRSTAID_EXP_DATE: TDateTimeField
      FieldName = 'FIRSTAID_EXP_DATE'
    end
    object odsEmployeePlanningSCHEDULED_TIMEBLOCK_1: TStringField
      FieldName = 'SCHEDULED_TIMEBLOCK_1'
      Size = 1
    end
    object odsEmployeePlanningSCHEDULED_TIMEBLOCK_2: TStringField
      FieldName = 'SCHEDULED_TIMEBLOCK_2'
      Size = 1
    end
    object odsEmployeePlanningSCHEDULED_TIMEBLOCK_3: TStringField
      FieldName = 'SCHEDULED_TIMEBLOCK_3'
      Size = 1
    end
    object odsEmployeePlanningSCHEDULED_TIMEBLOCK_4: TStringField
      FieldName = 'SCHEDULED_TIMEBLOCK_4'
      Size = 1
    end
    object odsEmployeePlanningFIRSTSTARTDATE: TDateTimeField
      FieldKind = fkCalculated
      FieldName = 'FIRSTSTARTDATE'
      Calculated = True
    end
    object odsEmployeePlanningSCHEDULED_TIMEBLOCK_5: TStringField
      FieldName = 'SCHEDULED_TIMEBLOCK_5'
      Size = 4
    end
    object odsEmployeePlanningSCHEDULED_TIMEBLOCK_6: TStringField
      FieldName = 'SCHEDULED_TIMEBLOCK_6'
      Size = 4
    end
    object odsEmployeePlanningSCHEDULED_TIMEBLOCK_7: TStringField
      FieldName = 'SCHEDULED_TIMEBLOCK_7'
      Size = 4
    end
    object odsEmployeePlanningSCHEDULED_TIMEBLOCK_8: TStringField
      FieldName = 'SCHEDULED_TIMEBLOCK_8'
      Size = 4
    end
    object odsEmployeePlanningSCHEDULED_TIMEBLOCK_9: TStringField
      FieldName = 'SCHEDULED_TIMEBLOCK_9'
      Size = 4
    end
    object odsEmployeePlanningSCHEDULED_TIMEBLOCK_10: TStringField
      FieldName = 'SCHEDULED_TIMEBLOCK_10'
      Size = 4
    end
  end
  object cdsEmployeePlanning: TClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <
      item
        Name = 'byPLNTWSSHFTEMP'
        Fields = 'PLANT_CODE;WORKSPOT_CODE;SHIFT_NUMBER;EMPLOYEE_NUMBER'
      end>
    IndexName = 'byPLNTWSSHFTEMP'
    Params = <
      item
        DataType = ftDateTime
        Name = 'EMPLOYEEPLANNING_DATE'
        ParamType = ptInput
      end>
    ProviderName = 'dspEmployeePlanning'
    StoreDefs = True
    Left = 368
    Top = 160
  end
  object dspEmployeePlanning: TDataSetProvider
    DataSet = odsEmployeePlanning
    Left = 224
    Top = 160
  end
  object odsTimeBlockPerEmployee: TOracleDataSet
    SQL.Strings = (
      'SELECT'
      '  TBE.EMPLOYEE_NUMBER,'
      '  TBE.PLANT_CODE,'
      '  TBE.SHIFT_NUMBER,'
      '  TBE.TIMEBLOCK_NUMBER,'
      '  TBE.DESCRIPTION,'
      '  TBE.STARTTIME1,'
      '  TBE.ENDTIME1,'
      '  TBE.STARTTIME2,'
      '  TBE.ENDTIME2,'
      '  TBE.STARTTIME3,'
      '  TBE.ENDTIME3,'
      '  TBE.STARTTIME4,'
      '  TBE.ENDTIME4,'
      '  TBE.STARTTIME5,'
      '  TBE.ENDTIME5,'
      '  TBE.STARTTIME6,'
      '  TBE.ENDTIME6,'
      '  TBE.STARTTIME7,'
      '  TBE.ENDTIME7'
      'FROM'
      '  TIMEBLOCKPEREMPLOYEE TBE INNER JOIN EMPLOYEE E ON'
      '    TBE.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER'
      'WHERE'
      ' ('
      '    (:USER_NAME = '#39'*'#39') OR'
      '    (E.TEAM_CODE IN'
      
        '      (SELECT U.TEAM_CODE FROM TEAMPERUSER U WHERE U.USER_NAME =' +
        ' :USER_NAME))'
      ' )'
      ''
      '  '
      ' ')
    Variables.Data = {
      03000000010000000A0000003A555345525F4E414D4505000000000000000000
      0000}
    Session = ORASystemDM.OracleSession
    Left = 72
    Top = 16
  end
  object odsTimeBlockPerDepartment: TOracleDataSet
    SQL.Strings = (
      'SELECT'
      '  TBD.PLANT_CODE,'
      '  TBD.DEPARTMENT_CODE,'
      '  TBD.SHIFT_NUMBER,'
      '  TBD.TIMEBLOCK_NUMBER,'
      '  TBD.DESCRIPTION,'
      '  TBD.STARTTIME1,'
      '  TBD.ENDTIME1,'
      '  TBD.STARTTIME2,'
      '  TBD.ENDTIME2,'
      '  TBD.STARTTIME3,'
      '  TBD.ENDTIME3,'
      '  TBD.STARTTIME4,'
      '  TBD.ENDTIME4,'
      '  TBD.STARTTIME5,'
      '  TBD.ENDTIME5,'
      '  TBD.STARTTIME6,'
      '  TBD.ENDTIME6,'
      '  TBD.STARTTIME7,'
      '  TBD.ENDTIME7'
      'FROM'
      '  TIMEBLOCKPERDEPARTMENT TBD'
      ''
      '  '
      ' ')
    Session = ORASystemDM.OracleSession
    Left = 72
    Top = 64
  end
  object odsTimeBlockPerShift: TOracleDataSet
    SQL.Strings = (
      'SELECT'
      '  TBS.PLANT_CODE,'
      '  TBS.SHIFT_NUMBER,'
      '  TBS.TIMEBLOCK_NUMBER,'
      '  TBS.DESCRIPTION,'
      '  TBS.STARTTIME1,'
      '  TBS.ENDTIME1,'
      '  TBS.STARTTIME2,'
      '  TBS.ENDTIME2,'
      '  TBS.STARTTIME3,'
      '  TBS.ENDTIME3,'
      '  TBS.STARTTIME4,'
      '  TBS.ENDTIME4,'
      '  TBS.STARTTIME5,'
      '  TBS.ENDTIME5,'
      '  TBS.STARTTIME6,'
      '  TBS.ENDTIME6,'
      '  TBS.STARTTIME7,'
      '  TBS.ENDTIME7'
      'FROM'
      '  TIMEBLOCKPERSHIFT TBS'
      ''
      '  '
      ' ')
    Session = ORASystemDM.OracleSession
    Left = 72
    Top = 112
  end
  object cdsTimeBlockPerEmployee: TClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <
      item
        Name = 'byEmployeePlantShiftTimeblock'
        Fields = 'PLANT_CODE;SHIFT_NUMBER;EMPLOYEE_NUMBER;TIMEBLOCK_NUMBER'
      end>
    IndexName = 'byEmployeePlantShiftTimeblock'
    Params = <>
    ProviderName = 'dspTimeBlockPerEmployee'
    StoreDefs = True
    Left = 368
    Top = 16
  end
  object cdsTimeBlockPerDepartment: TClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <
      item
        Name = 'byPlantDepartmentShiftTimeblock'
        Fields = 'PLANT_CODE;DEPARTMENT_CODE;SHIFT_NUMBER;TIMEBLOCK_NUMBER'
      end>
    IndexName = 'byPlantDepartmentShiftTimeblock'
    Params = <>
    ProviderName = 'dspTimeBlockPerDepartment'
    StoreDefs = True
    Left = 368
    Top = 64
  end
  object cdsTimeBlockPerShift: TClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <
      item
        Name = 'byPlantShiftTimeblock'
        Fields = 'PLANT_CODE;SHIFT_NUMBER;TIMEBLOCK_NUMBER'
      end>
    IndexName = 'byPlantShiftTimeblock'
    Params = <>
    ProviderName = 'dspTimeBlockPerShift'
    StoreDefs = True
    Left = 368
    Top = 112
  end
  object dspTimeBlockPerEmployee: TDataSetProvider
    DataSet = odsTimeBlockPerEmployee
    Left = 224
    Top = 16
  end
  object dspTimeBlockPerDepartment: TDataSetProvider
    DataSet = odsTimeBlockPerDepartment
    Left = 224
    Top = 64
  end
  object dspTimeBlockPerShift: TDataSetProvider
    DataSet = odsTimeBlockPerShift
    Left = 224
    Top = 112
  end
  object odsTimeRegScanning: TOracleDataSet
    SQL.Strings = (
      'SELECT'
      '  TRS.PLANT_CODE,'
      '  TRS.WORKSPOT_CODE, '
      '  TRS.JOB_CODE, '
      '  TRS.SHIFT_NUMBER, '
      '  TRS.EMPLOYEE_NUMBER, '
      '  E.SHORT_NAME,'
      '  E.DESCRIPTION,'
      '  E.TEAM_CODE,'
      '  E.FIRSTAID_YN,'
      '  E.FIRSTAID_EXP_DATE,'
      '  TRS.DATETIME_IN,'
      '  TRS.DATETIME_OUT,'
      '  (SELECT NVL(ROUND(SUM(T.EFF)),0)'
      '   FROM V_REALTIMECURREMPLOYEEEFF T'
      '   WHERE T.EMPLOYEE_NUMBER = TRS.EMPLOYEE_NUMBER) CURREFF,'
      '  (SELECT NVL(ROUND(SUM(T.EFF)),0)'
      '   FROM V_REALTIMESHIFTEMPLOYEEEFF T'
      '   WHERE T.EMPLOYEE_NUMBER = TRS.EMPLOYEE_NUMBER AND'
      
        '   T.PLANT_CODE = TRS.PLANT_CODE AND T.SHIFT_NUMBER = TRS.SHIFT_' +
        'NUMBER) SHIFTEFF,'
      '  (SELECT MIN(T2.DATETIME_IN) FROM TIMEREGSCANNING T2'
      '   WHERE T2.EMPLOYEE_NUMBER = TRS.EMPLOYEE_NUMBER AND'
      '   T2.SHIFT_DATE = TRS.SHIFT_DATE AND'
      '   T2.PLANT_CODE = TRS.PLANT_CODE AND'
      '   T2.SHIFT_NUMBER = TRS.SHIFT_NUMBER) FIRSTSCAN'
      'FROM'
      '  TIMEREGSCANNING TRS,'
      '  EMPLOYEE E'
      'WHERE'
      '  TRS.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER AND'
      '  TRS.DATETIME_IN >= :DATEFROM AND '
      '  ((TRS.DATETIME_IN <= :DATETO AND TRS.DATETIME_OUT IS NULL) OR '
      
        '  ((TRS.DATETIME_IN <= :DATETO AND TRS.DATETIME_OUT IS NOT NULL)' +
        ' AND'
      '  (TRS.DATETIME_OUT >= :DATETO)))'
      '  AND'
      '  ('
      '    (:USER_NAME = '#39'*'#39') OR'
      '    (E.TEAM_CODE IN'
      
        '      (SELECT U.TEAM_CODE FROM TEAMPERUSER U WHERE U.USER_NAME =' +
        ' :USER_NAME))'
      ' )'
      'ORDER BY'
      '  TRS.EMPLOYEE_NUMBER  ')
    Variables.Data = {
      0300000003000000090000003A4441544546524F4D0C00000000000000000000
      00070000003A44415445544F0C00000000000000000000000A0000003A555345
      525F4E414D45050000000000000000000000}
    QBEDefinition.QBEFieldDefs = {
      040000000F0000000A000000504C414E545F434F44450100000000000D000000
      574F524B53504F545F434F4445010000000000080000004A4F425F434F444501
      00000000000C00000053484946545F4E554D4245520100000000000F00000045
      4D504C4F5945455F4E554D4245520100000000000A00000053484F52545F4E41
      4D450100000000000B0000004445534352495054494F4E010000000000090000
      005445414D5F434F44450100000000000B00000046495253544149445F594E01
      00000000001100000046495253544149445F4558505F44415445010000000000
      0B0000004441544554494D455F494E0100000000000C0000004441544554494D
      455F4F5554010000000000070000004355525245464601000000000008000000
      53484946544546460100000000000900000046495253545343414E0100000000
      00}
    Session = ORASystemDM.OracleSession
    OnCalcFields = odsTimeRegScanningCalcFields
    Left = 72
    Top = 208
    object odsTimeRegScanningMARK: TBooleanField
      FieldKind = fkCalculated
      FieldName = 'MARK'
      Calculated = True
    end
    object odsTimeRegScanningPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Size = 6
    end
    object odsTimeRegScanningWORKSPOT_CODE: TStringField
      FieldName = 'WORKSPOT_CODE'
      Size = 6
    end
    object odsTimeRegScanningJOB_CODE: TStringField
      FieldName = 'JOB_CODE'
      Size = 6
    end
    object odsTimeRegScanningSHIFT_NUMBER: TIntegerField
      FieldName = 'SHIFT_NUMBER'
    end
    object odsTimeRegScanningEMPLOYEE_NUMBER: TIntegerField
      FieldName = 'EMPLOYEE_NUMBER'
    end
    object odsTimeRegScanningSHORT_NAME: TStringField
      FieldName = 'SHORT_NAME'
      Size = 6
    end
    object odsTimeRegScanningDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Size = 40
    end
    object odsTimeRegScanningTEAM_CODE: TStringField
      FieldName = 'TEAM_CODE'
      Size = 6
    end
    object odsTimeRegScanningFIRSTAID_YN: TStringField
      FieldName = 'FIRSTAID_YN'
      Size = 1
    end
    object odsTimeRegScanningFIRSTAID_EXP_DATE: TDateTimeField
      FieldName = 'FIRSTAID_EXP_DATE'
    end
    object odsTimeRegScanningDATETIME_IN: TDateTimeField
      FieldName = 'DATETIME_IN'
    end
    object odsTimeRegScanningDATETIME_OUT: TDateTimeField
      FieldName = 'DATETIME_OUT'
    end
    object odsTimeRegScanningCURREFF: TFloatField
      FieldName = 'CURREFF'
    end
    object odsTimeRegScanningSHIFTEFF: TFloatField
      FieldName = 'SHIFTEFF'
    end
    object odsTimeRegScanningFIRSTSCAN: TDateTimeField
      FieldName = 'FIRSTSCAN'
    end
  end
  object dspTimeRegScanning: TDataSetProvider
    DataSet = odsTimeRegScanning
    Left = 224
    Top = 208
  end
  object cdsTimeRegScanning: TClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <
      item
        Name = 'byPLNTWSEMPDIN'
        Fields = 'PLANT_CODE;WORKSPOT_CODE;EMPLOYEE_NUMBER;DATETIME_IN'
      end>
    IndexName = 'byPLNTWSEMPDIN'
    Params = <
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptInput
      end>
    ProviderName = 'dspTimeRegScanning'
    StoreDefs = True
    Left = 368
    Top = 208
  end
  object odsStandAvail: TOracleDataSet
    SQL.Strings = (
      'SELECT'
      '  SA.PLANT_CODE, '
      '  SA.DAY_OF_WEEK, '
      '  SA.SHIFT_NUMBER, '
      '  SA.EMPLOYEE_NUMBER, '
      '  E.SHORT_NAME,'
      '  E.DESCRIPTION,'
      '  E.DEPARTMENT_CODE,'
      '  E.TEAM_CODE,'
      '  E.FIRSTAID_YN,'
      '  E.FIRSTAID_EXP_DATE,'
      '  SA.AVAILABLE_TIMEBLOCK_1, '
      '  SA.AVAILABLE_TIMEBLOCK_2, '
      '  SA.AVAILABLE_TIMEBLOCK_3, '
      '  SA.AVAILABLE_TIMEBLOCK_4, '
      '  SA.AVAILABLE_TIMEBLOCK_5, '
      '  SA.AVAILABLE_TIMEBLOCK_6, '
      '  SA.AVAILABLE_TIMEBLOCK_7, '
      '  SA.AVAILABLE_TIMEBLOCK_8, '
      '  SA.AVAILABLE_TIMEBLOCK_9, '
      '  SA.AVAILABLE_TIMEBLOCK_10'
      'FROM'
      '  STANDARDAVAILABILITY SA,'
      '  EMPLOYEE E'
      'WHERE'
      '  ('
      '    (E.ENDDATE IS NULL) OR'
      '    (E.ENDDATE IS NOT NULL AND E.ENDDATE >= TRUNC(SYSDATE-1))'
      '  ) AND'
      '  E.IS_SCANNING_YN = '#39'Y'#39' AND'
      '  SA.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER AND'
      '  (SA.DAY_OF_WEEK = :DAY_OF_WEEK_FROM OR'
      '   SA.DAY_OF_WEEK = :DAY_OF_WEEK_TO) AND'
      '  ('
      '  SA.AVAILABLE_TIMEBLOCK_1 = '#39'*'#39' OR'
      '  SA.AVAILABLE_TIMEBLOCK_2 = '#39'*'#39' OR'
      '  SA.AVAILABLE_TIMEBLOCK_3 = '#39'*'#39' OR'
      '  SA.AVAILABLE_TIMEBLOCK_4 = '#39'*'#39' OR'
      '  SA.AVAILABLE_TIMEBLOCK_5 = '#39'*'#39' OR'
      '  SA.AVAILABLE_TIMEBLOCK_6 = '#39'*'#39' OR'
      '  SA.AVAILABLE_TIMEBLOCK_7 = '#39'*'#39' OR'
      '  SA.AVAILABLE_TIMEBLOCK_8 = '#39'*'#39' OR'
      '  SA.AVAILABLE_TIMEBLOCK_9 = '#39'*'#39' OR'
      '  SA.AVAILABLE_TIMEBLOCK_10 = '#39'*'#39
      '  )'
      '  AND'
      '  ('
      '    (:USER_NAME = '#39'*'#39') OR'
      '    (E.TEAM_CODE IN'
      
        '      (SELECT U.TEAM_CODE FROM TEAMPERUSER U WHERE U.USER_NAME =' +
        ' :USER_NAME))'
      ' )'
      'ORDER BY'
      '  SA.EMPLOYEE_NUMBER'
      '')
    Variables.Data = {
      0300000003000000110000003A4441595F4F465F5745454B5F46524F4D030000
      0000000000000000000F0000003A4441595F4F465F5745454B5F544F03000000
      00000000000000000A0000003A555345525F4E414D4505000000000000000000
      0000}
    QBEDefinition.QBEFieldDefs = {
      04000000140000000A000000504C414E545F434F44450100000000000B000000
      4441595F4F465F5745454B0100000000000C00000053484946545F4E554D4245
      520100000000000F000000454D504C4F5945455F4E554D424552010000000000
      0A00000053484F52545F4E414D450100000000000B0000004445534352495054
      494F4E0100000000000F0000004445504152544D454E545F434F444501000000
      0000090000005445414D5F434F44450100000000000B00000046495253544149
      445F594E0100000000001100000046495253544149445F4558505F4441544501
      000000000015000000415641494C41424C455F54494D45424C4F434B5F310100
      0000000015000000415641494C41424C455F54494D45424C4F434B5F32010000
      00000015000000415641494C41424C455F54494D45424C4F434B5F3301000000
      000015000000415641494C41424C455F54494D45424C4F434B5F340100000000
      0015000000415641494C41424C455F54494D45424C4F434B5F35010000000000
      15000000415641494C41424C455F54494D45424C4F434B5F3601000000000015
      000000415641494C41424C455F54494D45424C4F434B5F370100000000001500
      0000415641494C41424C455F54494D45424C4F434B5F38010000000000150000
      00415641494C41424C455F54494D45424C4F434B5F3901000000000016000000
      415641494C41424C455F54494D45424C4F434B5F3130010000000000}
    Session = ORASystemDM.OracleSession
    OnCalcFields = odsStandAvailCalcFields
    Left = 72
    Top = 256
    object odsStandAvailSTARTDATE: TDateTimeField
      FieldKind = fkCalculated
      FieldName = 'STARTDATE'
      Calculated = True
    end
    object odsStandAvailENDDATE: TDateTimeField
      FieldKind = fkCalculated
      FieldName = 'ENDDATE'
      Calculated = True
    end
    object odsStandAvailMARK: TBooleanField
      FieldKind = fkCalculated
      FieldName = 'MARK'
      Calculated = True
    end
    object odsStandAvailPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Size = 6
    end
    object odsStandAvailDAY_OF_WEEK: TIntegerField
      FieldName = 'DAY_OF_WEEK'
    end
    object odsStandAvailSHIFT_NUMBER: TIntegerField
      FieldName = 'SHIFT_NUMBER'
    end
    object odsStandAvailEMPLOYEE_NUMBER: TIntegerField
      FieldName = 'EMPLOYEE_NUMBER'
    end
    object odsStandAvailSHORT_NAME: TStringField
      FieldName = 'SHORT_NAME'
      Size = 6
    end
    object odsStandAvailDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Size = 40
    end
    object odsStandAvailDEPARTMENT_CODE: TStringField
      FieldName = 'DEPARTMENT_CODE'
      Size = 6
    end
    object odsStandAvailTEAM_CODE: TStringField
      FieldName = 'TEAM_CODE'
      Size = 6
    end
    object odsStandAvailFIRSTAID_YN: TStringField
      FieldName = 'FIRSTAID_YN'
      Size = 1
    end
    object odsStandAvailFIRSTAID_EXP_DATE: TDateTimeField
      FieldName = 'FIRSTAID_EXP_DATE'
    end
    object odsStandAvailAVAILABLE_TIMEBLOCK_1: TStringField
      FieldName = 'AVAILABLE_TIMEBLOCK_1'
      Size = 1
    end
    object odsStandAvailAVAILABLE_TIMEBLOCK_2: TStringField
      FieldName = 'AVAILABLE_TIMEBLOCK_2'
      Size = 1
    end
    object odsStandAvailAVAILABLE_TIMEBLOCK_3: TStringField
      FieldName = 'AVAILABLE_TIMEBLOCK_3'
      Size = 1
    end
    object odsStandAvailAVAILABLE_TIMEBLOCK_4: TStringField
      FieldName = 'AVAILABLE_TIMEBLOCK_4'
      Size = 1
    end
    object odsStandAvailAVAILABLE_TIMEBLOCK_5: TStringField
      FieldName = 'AVAILABLE_TIMEBLOCK_5'
      Size = 4
    end
    object odsStandAvailAVAILABLE_TIMEBLOCK_6: TStringField
      FieldName = 'AVAILABLE_TIMEBLOCK_6'
      Size = 4
    end
    object odsStandAvailAVAILABLE_TIMEBLOCK_7: TStringField
      FieldName = 'AVAILABLE_TIMEBLOCK_7'
      Size = 4
    end
    object odsStandAvailAVAILABLE_TIMEBLOCK_8: TStringField
      FieldName = 'AVAILABLE_TIMEBLOCK_8'
      Size = 4
    end
    object odsStandAvailAVAILABLE_TIMEBLOCK_9: TStringField
      FieldName = 'AVAILABLE_TIMEBLOCK_9'
      Size = 4
    end
    object odsStandAvailAVAILABLE_TIMEBLOCK_10: TStringField
      FieldName = 'AVAILABLE_TIMEBLOCK_10'
      Size = 4
    end
  end
  object dspStandAvail: TDataSetProvider
    DataSet = odsStandAvail
    Left = 224
    Top = 256
  end
  object cdsStandAvail: TClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <
      item
        Name = 'byPlantEmp'
        Fields = 'PLANT_CODE;EMPLOYEE_NUMBER'
      end>
    IndexName = 'byPlantEmp'
    Params = <
      item
        DataType = ftInteger
        Name = 'DAY_OF_WEEK'
        ParamType = ptInput
      end>
    ProviderName = 'dspStandAvail'
    StoreDefs = True
    Left = 368
    Top = 256
  end
  object odsEmployeeAvail: TOracleDataSet
    SQL.Strings = (
      'SELECT'
      '  EA.PLANT_CODE,'
      '  EA.EMPLOYEEAVAILABILITY_DATE,'
      '  EA.SHIFT_NUMBER,'
      '  EA.EMPLOYEE_NUMBER,'
      '  E.SHORT_NAME,'
      '  E.DESCRIPTION,'
      '  E.DEPARTMENT_CODE,'
      '  E.TEAM_CODE,'
      '  E.FIRSTAID_YN,'
      '  E.FIRSTAID_EXP_DATE,'
      '  EA.AVAILABLE_TIMEBLOCK_1,'
      '  EA.AVAILABLE_TIMEBLOCK_2,'
      '  EA.AVAILABLE_TIMEBLOCK_3,'
      '  EA.AVAILABLE_TIMEBLOCK_4,'
      '  EA.AVAILABLE_TIMEBLOCK_5,'
      '  EA.AVAILABLE_TIMEBLOCK_6,'
      '  EA.AVAILABLE_TIMEBLOCK_7,'
      '  EA.AVAILABLE_TIMEBLOCK_8,'
      '  EA.AVAILABLE_TIMEBLOCK_9,'
      '  EA.AVAILABLE_TIMEBLOCK_10'
      'FROM'
      '  EMPLOYEEAVAILABILITY EA,'
      '  EMPLOYEE E'
      'WHERE'
      '  ('
      '    (E.ENDDATE IS NULL) OR'
      '    (E.ENDDATE IS NOT NULL AND E.ENDDATE >= :DATEFROM)'
      '  ) AND'
      '  E.IS_SCANNING_YN = '#39'Y'#39' AND'
      '  EA.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER AND'
      '  (EA.EMPLOYEEAVAILABILITY_DATE >= :DATEFROM AND'
      '   EA.EMPLOYEEAVAILABILITY_DATE <= :DATETO) AND'
      '  ('
      '  (EA.AVAILABLE_TIMEBLOCK_1 NOT IN ('#39'*'#39', '#39'-'#39') AND'
      '     EA.AVAILABLE_TIMEBLOCK_1 IS NOT NULL) OR'
      '  (EA.AVAILABLE_TIMEBLOCK_2 NOT IN ('#39'*'#39', '#39'-'#39') AND'
      '    EA.AVAILABLE_TIMEBLOCK_2 IS NOT NULL) OR'
      '  (EA.AVAILABLE_TIMEBLOCK_3 NOT IN ('#39'*'#39', '#39'-'#39') AND'
      '    EA.AVAILABLE_TIMEBLOCK_3 IS NOT NULL) OR'
      '  (EA.AVAILABLE_TIMEBLOCK_4 NOT IN ('#39'*'#39', '#39'-'#39') AND'
      '     EA.AVAILABLE_TIMEBLOCK_4 IS NOT NULL) OR'
      '  (EA.AVAILABLE_TIMEBLOCK_5 NOT IN ('#39'*'#39', '#39'-'#39') AND'
      '    EA.AVAILABLE_TIMEBLOCK_5 IS NOT NULL) OR'
      '  (EA.AVAILABLE_TIMEBLOCK_6 NOT IN ('#39'*'#39', '#39'-'#39') AND'
      '    EA.AVAILABLE_TIMEBLOCK_6 IS NOT NULL) OR'
      '  (EA.AVAILABLE_TIMEBLOCK_7 NOT IN ('#39'*'#39', '#39'-'#39') AND'
      '     EA.AVAILABLE_TIMEBLOCK_7 IS NOT NULL) OR'
      '  (EA.AVAILABLE_TIMEBLOCK_8 NOT IN ('#39'*'#39', '#39'-'#39') AND'
      '    EA.AVAILABLE_TIMEBLOCK_8 IS NOT NULL) OR'
      '  (EA.AVAILABLE_TIMEBLOCK_9 NOT IN ('#39'*'#39', '#39'-'#39') AND'
      '    EA.AVAILABLE_TIMEBLOCK_9 IS NOT NULL) OR'
      '  (EA.AVAILABLE_TIMEBLOCK_10 NOT IN ('#39'*'#39', '#39'-'#39') AND'
      '    EA.AVAILABLE_TIMEBLOCK_10 IS NOT NULL)'
      '  )'
      '  AND'
      '  ('
      '    (:USER_NAME = '#39'*'#39') OR'
      '    (E.TEAM_CODE IN'
      
        '      (SELECT U.TEAM_CODE FROM TEAMPERUSER U WHERE U.USER_NAME =' +
        ' :USER_NAME))'
      ' )'
      'ORDER BY'
      '  EA.EMPLOYEE_NUMBER')
    Variables.Data = {
      0300000003000000090000003A4441544546524F4D0C00000000000000000000
      00070000003A44415445544F0C00000000000000000000000A0000003A555345
      525F4E414D45050000000000000000000000}
    QBEDefinition.QBEFieldDefs = {
      04000000140000000A000000504C414E545F434F444501000000000019000000
      454D504C4F594545415641494C4142494C4954595F444154450100000000000C
      00000053484946545F4E554D4245520100000000000F000000454D504C4F5945
      455F4E554D4245520100000000000A00000053484F52545F4E414D4501000000
      00000B0000004445534352495054494F4E0100000000000F0000004445504152
      544D454E545F434F4445010000000000090000005445414D5F434F4445010000
      0000000B00000046495253544149445F594E0100000000001100000046495253
      544149445F4558505F4441544501000000000015000000415641494C41424C45
      5F54494D45424C4F434B5F3101000000000015000000415641494C41424C455F
      54494D45424C4F434B5F3201000000000015000000415641494C41424C455F54
      494D45424C4F434B5F3301000000000015000000415641494C41424C455F5449
      4D45424C4F434B5F3401000000000015000000415641494C41424C455F54494D
      45424C4F434B5F3501000000000015000000415641494C41424C455F54494D45
      424C4F434B5F3601000000000015000000415641494C41424C455F54494D4542
      4C4F434B5F3701000000000015000000415641494C41424C455F54494D45424C
      4F434B5F3801000000000015000000415641494C41424C455F54494D45424C4F
      434B5F3901000000000016000000415641494C41424C455F54494D45424C4F43
      4B5F3130010000000000}
    Session = ORASystemDM.OracleSession
    OnCalcFields = odsEmployeeAvailCalcFields
    Left = 72
    Top = 304
    object odsEmployeeAvailSTARTDATE: TDateTimeField
      FieldKind = fkCalculated
      FieldName = 'STARTDATE'
      Calculated = True
    end
    object odsEmployeeAvailENDDATE: TDateTimeField
      FieldKind = fkCalculated
      FieldName = 'ENDDATE'
      Calculated = True
    end
    object odsEmployeeAvailABSENCEREASONCODE: TStringField
      FieldKind = fkCalculated
      FieldName = 'ABSENCEREASONCODE'
      Size = 1
      Calculated = True
    end
    object odsEmployeeAvailABSENCEREASONDESCRIPTION: TStringField
      FieldKind = fkCalculated
      FieldName = 'ABSENCEREASONDESCRIPTION'
      Size = 30
      Calculated = True
    end
    object odsEmployeeAvailMARK: TBooleanField
      FieldKind = fkCalculated
      FieldName = 'MARK'
      Calculated = True
    end
    object odsEmployeeAvailPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Size = 6
    end
    object odsEmployeeAvailEMPLOYEEAVAILABILITY_DATE: TDateTimeField
      FieldName = 'EMPLOYEEAVAILABILITY_DATE'
    end
    object odsEmployeeAvailSHIFT_NUMBER: TIntegerField
      FieldName = 'SHIFT_NUMBER'
    end
    object odsEmployeeAvailEMPLOYEE_NUMBER: TIntegerField
      FieldName = 'EMPLOYEE_NUMBER'
    end
    object odsEmployeeAvailSHORT_NAME: TStringField
      FieldName = 'SHORT_NAME'
      Size = 6
    end
    object odsEmployeeAvailDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Size = 40
    end
    object odsEmployeeAvailDEPARTMENT_CODE: TStringField
      FieldName = 'DEPARTMENT_CODE'
      Size = 6
    end
    object odsEmployeeAvailTEAM_CODE: TStringField
      FieldName = 'TEAM_CODE'
      Size = 6
    end
    object odsEmployeeAvailFIRSTAID_YN: TStringField
      FieldName = 'FIRSTAID_YN'
      Size = 1
    end
    object odsEmployeeAvailFIRSTAID_EXP_DATE: TDateTimeField
      FieldName = 'FIRSTAID_EXP_DATE'
    end
    object odsEmployeeAvailAVAILABLE_TIMEBLOCK_1: TStringField
      FieldName = 'AVAILABLE_TIMEBLOCK_1'
      Size = 1
    end
    object odsEmployeeAvailAVAILABLE_TIMEBLOCK_2: TStringField
      FieldName = 'AVAILABLE_TIMEBLOCK_2'
      Size = 1
    end
    object odsEmployeeAvailAVAILABLE_TIMEBLOCK_3: TStringField
      FieldName = 'AVAILABLE_TIMEBLOCK_3'
      Size = 1
    end
    object odsEmployeeAvailAVAILABLE_TIMEBLOCK_4: TStringField
      FieldName = 'AVAILABLE_TIMEBLOCK_4'
      Size = 1
    end
    object odsEmployeeAvailFIRSTSTARTDATE: TDateField
      FieldKind = fkCalculated
      FieldName = 'FIRSTSTARTDATE'
      Calculated = True
    end
    object odsEmployeeAvailAVAILABLE_TIMEBLOCK_5: TStringField
      FieldName = 'AVAILABLE_TIMEBLOCK_5'
      Size = 4
    end
    object odsEmployeeAvailAVAILABLE_TIMEBLOCK_6: TStringField
      FieldName = 'AVAILABLE_TIMEBLOCK_6'
      Size = 4
    end
    object odsEmployeeAvailAVAILABLE_TIMEBLOCK_7: TStringField
      FieldName = 'AVAILABLE_TIMEBLOCK_7'
      Size = 4
    end
    object odsEmployeeAvailAVAILABLE_TIMEBLOCK_8: TStringField
      FieldName = 'AVAILABLE_TIMEBLOCK_8'
      Size = 4
    end
    object odsEmployeeAvailAVAILABLE_TIMEBLOCK_9: TStringField
      FieldName = 'AVAILABLE_TIMEBLOCK_9'
      Size = 4
    end
    object odsEmployeeAvailAVAILABLE_TIMEBLOCK_10: TStringField
      FieldName = 'AVAILABLE_TIMEBLOCK_10'
      Size = 4
    end
  end
  object dspEmployeeAvail: TDataSetProvider
    DataSet = odsEmployeeAvail
    Left = 224
    Top = 304
  end
  object cdsEmployeeAvail: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftDateTime
        Name = 'EMPLOYEEAVAILABILITY_DATE'
        ParamType = ptInput
      end>
    ProviderName = 'dspEmployeeAvail'
    Left = 368
    Top = 304
  end
  object odsAbsenceReason: TOracleDataSet
    SQL.Strings = (
      'SELECT'
      '  AR.ABSENCEREASON_CODE,'
      '  AR.DESCRIPTION'
      'FROM'
      '  ABSENCEREASON AR '
      ' ')
    Session = ORASystemDM.OracleSession
    Left = 72
    Top = 352
    object odsAbsenceReasonABSENCEREASON_CODE: TStringField
      FieldName = 'ABSENCEREASON_CODE'
      Size = 1
    end
    object odsAbsenceReasonDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Size = 30
    end
  end
  object dspAbsenceReason: TDataSetProvider
    DataSet = odsAbsenceReason
    Left = 224
    Top = 352
  end
  object cdsAbsenceReason: TClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <
      item
        Name = 'byCode'
        Fields = 'ABSENCEREASON_CODE'
      end>
    IndexName = 'byCode'
    Params = <>
    ProviderName = 'dspAbsenceReason'
    StoreDefs = True
    Left = 368
    Top = 352
  end
  object odsEmployee: TOracleDataSet
    SQL.Strings = (
      'SELECT'
      '  E.EMPLOYEE_NUMBER,'
      '  E.PLANT_CODE,'
      '  E.SHORT_NAME,'
      '  E.DESCRIPTION,'
      '  E.TEAM_CODE '
      'FROM'
      '  EMPLOYEE E'
      'WHERE'
      '  ('
      '    (E.ENDDATE IS NULL) OR'
      '    (E.ENDDATE IS NOT NULL AND E.ENDDATE >= TRUNC(SYSDATE))'
      '  ) AND'
      '  E.IS_SCANNING_YN = '#39'Y'#39
      '  AND'
      '  ('
      '    (:USER_NAME = '#39'*'#39') OR'
      '    (E.TEAM_CODE IN'
      
        '      (SELECT U.TEAM_CODE FROM TEAMPERUSER U WHERE U.USER_NAME =' +
        ' :USER_NAME))'
      ' )'
      'ORDER BY'
      '  E.EMPLOYEE_NUMBER')
    Variables.Data = {
      03000000010000000A0000003A555345525F4E414D4505000000000000000000
      0000}
    Session = ORASystemDM.OracleSession
    Left = 72
    Top = 400
    object odsEmployeeEMPLOYEE_NUMBER: TIntegerField
      FieldName = 'EMPLOYEE_NUMBER'
    end
    object odsEmployeePLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Size = 6
    end
    object odsEmployeeSHORT_NAME: TStringField
      FieldName = 'SHORT_NAME'
      Size = 6
    end
    object odsEmployeeDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Size = 40
    end
    object odsEmployeeTEAM_CODE: TStringField
      FieldName = 'TEAM_CODE'
      Size = 6
    end
  end
  object dspEmployee: TDataSetProvider
    DataSet = odsEmployee
    Left = 224
    Top = 400
  end
  object cdsEmployee: TClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <
      item
        Name = 'byNumber'
        Fields = 'EMPLOYEE_NUMBER'
      end>
    IndexName = 'byNumber'
    Params = <>
    ProviderName = 'dspEmployee'
    StoreDefs = True
    Left = 368
    Top = 400
  end
  object cdsOnlyStandavail: TClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <
      item
        Name = 'byEmployeeNumber'
        Fields = 'EMPLOYEENUMBER'
      end>
    IndexName = 'byEmployeeNumber'
    Params = <>
    StoreDefs = True
    Left = 368
    Top = 456
    object cdsOnlyStandavailEMPLOYEENUMBER: TIntegerField
      FieldName = 'EMPLOYEENUMBER'
    end
    object cdsOnlyStandavailEMPLOYEESHORTNAME: TStringField
      FieldName = 'EMPLOYEESHORTNAME'
      Size = 6
    end
    object cdsOnlyStandavailEMPLOYEENAME: TStringField
      FieldName = 'EMPLOYEENAME'
      Size = 40
    end
    object cdsOnlyStandavailTEAMCODE: TStringField
      FieldName = 'TEAMCODE'
      Size = 6
    end
    object cdsOnlyStandavailPLANTCODE: TStringField
      FieldName = 'PLANTCODE'
      Size = 6
    end
    object cdsOnlyStandavailWORKSPOTCODE: TStringField
      FieldName = 'WORKSPOTCODE'
      Size = 6
    end
    object cdsOnlyStandavailDEPARTMENTCODE: TStringField
      FieldName = 'DEPARTMENTCODE'
      Size = 6
    end
    object cdsOnlyStandavailSHIFTNUMBER: TIntegerField
      FieldName = 'SHIFTNUMBER'
    end
    object cdsOnlyStandavailFIRSTAIDYN: TStringField
      FieldName = 'FIRSTAIDYN'
      Size = 1
    end
    object cdsOnlyStandavailFIRSTAIDEXPDATE: TDateTimeField
      FieldName = 'FIRSTAIDEXPDATE'
    end
    object cdsOnlyStandavailEMPLEVEL: TStringField
      FieldName = 'EMPLEVEL'
      Size = 1
    end
    object cdsOnlyStandavailSTARTDATE: TDateTimeField
      FieldName = 'STARTDATE'
    end
    object cdsOnlyStandavailENDDATE: TDateTimeField
      FieldName = 'ENDDATE'
    end
    object cdsOnlyStandavailMARK: TBooleanField
      FieldName = 'MARK'
    end
  end
  object cdsOnlyEmpAvail: TClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <
      item
        Name = 'byEmployeeNumber'
        Fields = 'EMPLOYEENUMBER'
      end>
    IndexName = 'byEmployeeNumber'
    Params = <>
    StoreDefs = True
    Left = 368
    Top = 504
    object cdsOnlyEmpAvailEMPLOYEENUMBER: TIntegerField
      FieldName = 'EMPLOYEENUMBER'
    end
    object cdsOnlyEmpAvailEMPLOYEESHORTNAME: TStringField
      FieldName = 'EMPLOYEESHORTNAME'
      Size = 6
    end
    object cdsOnlyEmpAvailEMPLOYEENAME: TStringField
      FieldName = 'EMPLOYEENAME'
      Size = 40
    end
    object cdsOnlyEmpAvailTEAMCODE: TStringField
      FieldName = 'TEAMCODE'
      Size = 6
    end
    object cdsOnlyEmpAvailPLANTCODE: TStringField
      FieldName = 'PLANTCODE'
      Size = 6
    end
    object cdsOnlyEmpAvailWORKSPOTCODE: TStringField
      FieldName = 'WORKSPOTCODE'
      Size = 6
    end
    object cdsOnlyEmpAvailDEPARTMENTCODE: TStringField
      FieldName = 'DEPARTMENTCODE'
      Size = 6
    end
    object cdsOnlyEmpAvailSHIFTNUMBER: TIntegerField
      FieldName = 'SHIFTNUMBER'
    end
    object cdsOnlyEmpAvailFIRSTAIDYN: TStringField
      FieldName = 'FIRSTAIDYN'
      Size = 1
    end
    object cdsOnlyEmpAvailFIRSTAIDEXPDATE: TDateTimeField
      FieldName = 'FIRSTAIDEXPDATE'
    end
    object cdsOnlyEmpAvailEMPLEVEL: TStringField
      FieldName = 'EMPLEVEL'
      Size = 1
    end
    object cdsOnlyEmpAvailABSENCEREASONCODE: TStringField
      FieldName = 'ABSENCEREASONCODE'
      Size = 1
    end
    object cdsOnlyEmpAvailABSENCEREASONDESCRIPTION: TStringField
      FieldName = 'ABSENCEREASONDESCRIPTION'
      Size = 30
    end
    object cdsOnlyEmpAvailPLANWORKSPOTCODE: TStringField
      FieldName = 'PLANWORKSPOTCODE'
      Size = 6
    end
    object cdsOnlyEmpAvailPLANSTARTDATE: TDateTimeField
      FieldName = 'PLANSTARTDATE'
    end
    object cdsOnlyEmpAvailPLANENDDATE: TDateTimeField
      FieldName = 'PLANENDDATE'
    end
    object cdsOnlyEmpAvailPLANLEVEL: TStringField
      FieldName = 'PLANLEVEL'
    end
    object cdsOnlyEmpAvailSTANDAVAILSTARTDATE: TDateTimeField
      FieldName = 'STANDAVAILSTARTDATE'
    end
    object cdsOnlyEmpAvailSTANDAVAILENDDATE: TDateTimeField
      FieldName = 'STANDAVAILENDDATE'
    end
    object cdsOnlyEmpAvailEMPAVAILSTARTDATE: TDateTimeField
      FieldName = 'EMPAVAILSTARTDATE'
    end
    object cdsOnlyEmpAvailEMPAVAILENDDATE: TDateTimeField
      FieldName = 'EMPAVAILENDDATE'
    end
    object cdsOnlyEmpAvailMARK: TBooleanField
      FieldName = 'MARK'
    end
    object cdsOnlyEmpAvailFIRSTSTARTDATE: TDateField
      FieldName = 'FIRSTSTARTDATE'
    end
  end
  object oqCreateWorkspotsPerEmployeeListXXX: TOracleQuery
    SQL.Strings = (
      'SELECT'
      '  WPE.PLANT_CODE,'
      '  WPE.DEPARTMENT_CODE,'
      '  WPE.WORKSPOT_CODE,     '
      '  WPE.EMPLOYEE_NUMBER,     '
      '  WPE.STARTDATE_LEVEL,     '
      '  WPE.EMPLOYEE_LEVEL     '
      'FROM     '
      '  WORKSPOTSPEREMPLOYEE WPE    '
      'WHERE     '
      '  WPE.STARTDATE_LEVEL <= :STARTDATE_LEVEL     '
      'ORDER BY     '
      '  WPE.PLANT_CODE,     '
      '  WPE.WORKSPOT_CODE,'
      '  WPE.EMPLOYEE_NUMBER,'
      '  WPE.STARTDATE_LEVEL DESC'
      '')
    Session = ORASystemDM.OracleSession
    Variables.Data = {
      0300000001000000100000003A5354415254444154455F4C4556454C0C000000
      0000000000000000}
    Left = 592
    Top = 16
  end
  object odsWorkspot: TOracleDataSet
    SQL.Strings = (
      'SELECT W.PLANT_CODE, W.WORKSPOT_CODE, W.DEPARTMENT_CODE'
      'FROM WORKSPOT W'
      'ORDER BY W.PLANT_CODE, W.WORKSPOT_CODE')
    QBEDefinition.QBEFieldDefs = {
      04000000030000000A000000504C414E545F434F44450100000000000D000000
      574F524B53504F545F434F44450100000000000F0000004445504152544D454E
      545F434F4445010000000000}
    Session = ORASystemDM.OracleSession
    Left = 72
    Top = 568
    object odsWorkspotPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Required = True
      Size = 6
    end
    object odsWorkspotWORKSPOT_CODE: TStringField
      FieldName = 'WORKSPOT_CODE'
      Required = True
      Size = 6
    end
    object odsWorkspotDEPARTMENT_CODE: TStringField
      FieldName = 'DEPARTMENT_CODE'
      Required = True
      Size = 6
    end
  end
  object dspWorkspot: TDataSetProvider
    DataSet = odsWorkspot
    Left = 224
    Top = 568
  end
  object cdsWorkspot: TClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <
      item
        Name = 'byPlantWorkspot'
        Fields = 'PLANT_CODE;WORKSPOT_CODE'
      end>
    IndexName = 'byPlantWorkspot'
    Params = <>
    ProviderName = 'dspWorkspot'
    StoreDefs = True
    Left = 368
    Top = 568
  end
  object odsWorkspotsPerEmployee: TOracleDataSet
    SQL.Strings = (
      'SELECT'
      '  WPE.PLANT_CODE,'
      '  WPE.DEPARTMENT_CODE,'
      '  WPE.WORKSPOT_CODE,     '
      '  WPE.EMPLOYEE_NUMBER,     '
      '  WPE.STARTDATE_LEVEL,     '
      '  WPE.EMPLOYEE_LEVEL     '
      'FROM     '
      '  WORKSPOTSPEREMPLOYEE WPE INNER JOIN EMPLOYEE E ON'
      '    WPE.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER    '
      'WHERE     '
      '  WPE.STARTDATE_LEVEL <= TRUNC(SYSDATE)     '
      '  AND'
      '  ('
      '    (:USER_NAME = '#39'*'#39') OR'
      '    (E.TEAM_CODE IN'
      
        '      (SELECT U.TEAM_CODE FROM TEAMPERUSER U WHERE U.USER_NAME =' +
        ' :USER_NAME))'
      ' )'
      'ORDER BY'
      '  WPE.PLANT_CODE,'
      '  WPE.DEPARTMENT_CODE,'
      '  WPE.WORKSPOT_CODE,'
      '  WPE.EMPLOYEE_NUMBER,'
      '  WPE.STARTDATE_LEVEL DESC'
      '')
    Variables.Data = {
      03000000010000000A0000003A555345525F4E414D4505000000000000000000
      0000}
    QBEDefinition.QBEFieldDefs = {
      04000000060000000A000000504C414E545F434F44450100000000000F000000
      4445504152544D454E545F434F44450100000000000D000000574F524B53504F
      545F434F44450100000000000F000000454D504C4F5945455F4E554D42455201
      00000000000F0000005354415254444154455F4C4556454C0100000000000E00
      0000454D504C4F5945455F4C4556454C010000000000}
    Session = ORASystemDM.OracleSession
    Left = 72
    Top = 616
    object odsWorkspotsPerEmployeePLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Required = True
      Size = 6
    end
    object odsWorkspotsPerEmployeeDEPARTMENT_CODE: TStringField
      FieldName = 'DEPARTMENT_CODE'
      Required = True
      Size = 6
    end
    object odsWorkspotsPerEmployeeWORKSPOT_CODE: TStringField
      FieldName = 'WORKSPOT_CODE'
      Required = True
      Size = 6
    end
    object odsWorkspotsPerEmployeeEMPLOYEE_NUMBER: TIntegerField
      FieldName = 'EMPLOYEE_NUMBER'
      Required = True
    end
    object odsWorkspotsPerEmployeeSTARTDATE_LEVEL: TDateTimeField
      FieldName = 'STARTDATE_LEVEL'
      Required = True
    end
    object odsWorkspotsPerEmployeeEMPLOYEE_LEVEL: TStringField
      FieldName = 'EMPLOYEE_LEVEL'
      Required = True
      Size = 1
    end
  end
  object dsWorkspotsPerEmployee: TDataSetProvider
    DataSet = odsWorkspotsPerEmployee
    Left = 224
    Top = 616
  end
  object cdsWorkspotsPerEmployee: TClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <
      item
        Name = 'byPlantWSEmp'
        Fields = 'PLANT_CODE;WORKSPOT_CODE;EMPLOYEE_NUMBER'
      end>
    IndexName = 'byPlantWSEmp'
    Params = <>
    ProviderName = 'dsWorkspotsPerEmployee'
    StoreDefs = True
    Left = 368
    Top = 616
  end
end
