unit GridBaseFRM;
(*******************************************************************************
Unit     : FTopMenuBase    TfrmTopMenuBase = (TfrmPims)
Function : Parent form. This form contains all top menu command items and
         : toolbar items for Pims.
Note     : -
Changes  : -
--------------------------------------------------------------------------------
Inheritance tree:
TForm
frmPims
  Changed design properties :
  + Font.Name := Tahoma (MicroSoft Outlook Style)
  + Position  := poScreenCenter
  + ShowHint  := True

  Added controls:
  -

  Events:
  + FormCreate: Update the progressbar on the splashscreen

  Methods:
  + procedure SetParent(AParent :TWinControl); override;
    : Allmost all Pims froms a placed on a panel in another form.
    : Only via this method it is posible to have multiple toolbars
    : and full control over a form within the other form.

frmTopMenuBase
  Changed design properties :
  -

  Added controls:
  + dxBarManBase: Menu and toolbar manager from Dev Express
  + the pims Menu structure (see below)
  + StandardMenuActionList

  Events:
  -

  Methods:
  -
*******************************************************************************)
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  dxBarDBNav, dxBar, ComDrvN, ExtCtrls, Db, dxCntner, dxTL, DbTables,
  dxDBGrid, ShellAPI, FileCtrl, DBCtrls, StdCtrls, ActnList, Dblup1a, Dblup1b,
  dxDBCtrl, ImgList, PimsFRM, GridBaseDMT, TopMenuBaseFRM ;


type

  TGridBaseF = class(TTopMenuBaseF)
    pnlMasterGrid: TPanel;
    pnlDetail: TPanel;
    pnlDetailGrid: TPanel;
    spltMasterGrid: TSplitter;
    spltDetail: TSplitter;
    dxMasterGrid: TdxDBGrid;
    dsrcActive: TDataSource;
    dxDetailGrid: TdxDBGrid;

    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormDeactivate(Sender: TObject);
    procedure FormHide(Sender: TObject);
    procedure FormShow(Sender: TObject);
    { Navigation buttons }
    procedure dxBarButtonNavigationClick(Sender: TObject);
    { DBNavigator }
    procedure dxBarBDBNavInsertClick(Sender: TObject);
    { Edit / Details }
    procedure dxBarButtonEditModeClick(Sender: TObject);
    procedure dxBarButtonRecordDetailsClick(Sender: TObject);
    { Grid customization buttons }
    procedure dxBarButtonShowGroupClick(Sender: TObject);
    procedure dxBarButtonCustColClick(Sender: TObject);
    procedure dxBarButtonExpandClick(Sender: TObject);
    procedure dxBarButtonCollapseClick(Sender: TObject);
    procedure dxBarButtonResetColumnsClick(Sender: TObject);
    { Grid export buttons }
    procedure dxBarButtonExportGridClick(Sender: TObject);
    { Grid design events }
    procedure dxGridCustomDrawCell(Sender: TObject;
      ACanvas: TCanvas; ARect: TRect; ANode: TdxTreeListNode;
      AColumn: TdxTreeListColumn; ASelected, AFocused, ANewItemRow: Boolean;
      var AText: String; var AColor: TColor; AFont: TFont;
      var AAlignment: TAlignment; var ADone: Boolean);
    procedure dxGridCustomDrawColumnHeader(Sender: TObject;
      AColumn: TdxTreeListColumn; ACanvas: TCanvas; ARect: TRect;
      var AText: String; var AColor: TColor; AFont: TFont;
      var AAlignment: TAlignment; var ASorted: TdxTreeListColumnSort;
      var ADone: Boolean);
    procedure dxGridEndColumnsCustomizing(Sender: TObject);
    procedure dxGridEnter(Sender: TObject);
    procedure dxDetailGridDblClick(Sender: TObject);
    procedure pnlDetailExit(Sender: TObject);
    procedure dxGridCustomDrawBand(Sender: TObject;
      ABand: TdxTreeListBand; ACanvas: TCanvas; ARect: TRect;
      var AText: String; var AColor: TColor; AFont: TFont;
      var AAlignment: TAlignment; var ADone: Boolean);
    procedure dxGridBackgroundDrawEvent(Sender: TObject;
      ACanvas: TCanvas; ARect: TRect);
    procedure dxBarButtonSortClick(Sender: TObject);
    procedure dxBarButtonSearchClick(Sender: TObject);
    procedure DefaultLookupComboBoxKeyUp(Sender: TObject;
			var Key: Word; Shift: TShiftState);

    procedure FormCloseAskForSave(Sender: TObject;
      var Action: TCloseAction);
  private
    { Private declarations }

    FGridFormDM: TGridBaseDM;
    FFloatEmpl: String;
    FExportGrid: TdxDBGrid;
    FActiveGrid: TCustomdxTreeListControl;
    FExportDir: String;
    //
    FForm_Edit_YN: String;
    FSaveDetailPanel_Height: Integer;
    sdlgExport: TSaveDialog;

    procedure AskForSaveChanges;
    procedure CustomizeColumns(Grid: TCustomdxTreeListControl; Down: Boolean);
    procedure SetActiveGrid(Value: TCustomdxTreeListControl);

    procedure SynchToolBar;
    procedure CreateSummaryGroup(dxDBGrid: TdxDBGrid);
    procedure CreateExportGrid;
  public
    { Public declarations }
    FLastScrollPos: Integer;
    FSort, FGiveError: Boolean;
    //CAR 24-10-2003 - Bug solved
    property SaveDetailPanel_Height: Integer
      read FSaveDetailPanel_Height write FSaveDetailPanel_Height;
    //CAR 8-5-2003
    property ExportGrid: TdxDBGrid read FExportGrid;

    property ActiveGrid: TCustomdxTreeListControl read FActiveGrid
      write SetActiveGrid;
    property GridFormDM : TGridBaseDM
      read FGridFormDM write FGridFormDM;
    property FloatEmpl: String
      read FFloatEmpl write FFloatEmpl;

   //car 16-9-2003 - rights user changes
    property Form_Edit_YN: String read FForm_Edit_YN write FForm_Edit_YN;
    procedure SetNotEditable;
    procedure SetEditable;
    procedure SetNotEnabled;
    //
    procedure CloseCustomizeColumns;
    procedure SetGridForm(Sender: TObject);
    function CreateFormDM(AGridFormDM: TGridBaseClassDM): pointer;
    	Virtual;
   
    //car 8-5-2003 for export of grid
    function CheckPKMasterTable(MasterFields: String): String;Virtual;
    procedure CreateExportColumns(GridTmp: TdxDBGrid; TableName,
      FieldName: String );
    procedure CreateExportColumnsDesc(CapitionColumn, FieldName: String);
    // end
     
  end;

var
  GridBaseF: TGridBaseF;

implementation

uses
  SystemDMT, UPimsConst, UPimsMessageRes;

{$IFDEF PIMSDUTCH}
{$R *NLD.DFM}
{$ELSE}
  {$IFDEF PIMSGERMAN}
  {$R *DEU.DFM}
  {$ELSE}
    {$IFDEF PIMSFRENCH}
    {$R *FRA.DFM}
    {$ELSE}
    {$R *.DFM}
    {$ENDIF}
  {$ENDIF}
{$ENDIF}

const
  aExt: array [0..1] of string = ('htm', 'xls');
  aFilter: array [0..1] of string = ('HTML File (*.htm; *.html)|*.htm',
    'Microsoft Excel 4.0 Worksheet (*.xls)|*.xls');
{ Scroll Grid by DevExpress }
{ Scroll Grid according visible grid order in stead of table order }

procedure GotoChildNode(Which: Integer; ANode: TdxTreeListNode);
begin
  if ANode = nil then Exit;

  if ANode.Count = 0 then
    ANode.Focused := True
  else
    if Which = NAV_FIRST then
      GotoChildNode(NAV_FIRST, ANode.Items[0])
    else
      GotoChildNode(NAV_LAST, ANode.Items[ANode.Count - 1]);
end;

procedure GoToNode(Which: Integer; Grid: TCustomdxTreeListControl);
var
  ANode: TdxTreeListNode;
begin
  if Grid.Count = 0 then Exit;
  with Grid do
    try
      BeginUpdate;
      if Which = NAV_FIRST then
      begin
        if FocusedNode.Parent <> nil then
          GotoChildNode(Which, Items[0])
        else
          Items[0].Focused := True;
      end;

      if Which = NAV_LAST then
      begin
        if FocusedNode.Parent <> nil then
          GotoChildNode(Which, Items[Count - 1])
        else
          Items[Count - 1].Focused := True;
      end;

      if Which = NAV_PRIOR then
      begin
        ANode := FocusedNode;
        if (ANode.GetPriorNode <> nil)
          and (ANode.Parent <> ANode.GetPriorNode) then
          ANode.GetPriorNode.Focused := True
        else
        begin
          while (ANode.GetPriorNode <> nil)
            and (ANode.Parent = ANode.GetPriorNode) do
            ANode := ANode.GetPriorNode;
          GotoChildNode(NAV_LAST, ANode.GetPriorNode);
        end;
      end;

      if Which = NAV_NEXT then
      begin
        ANode := FocusedNode;
        if (ANode.GetNextNode <> nil)
          and (ANode.Parent = ANode.GetNextNode.Parent) then
          ANode.GetNextNode.Focused := True
        else
        begin
          while (ANode.GetNextNode <> nil)
            and (ANode.Parent = ANode.GetNextNode) do
            ANode := ANode.GetNextNode;
          GotoChildNode(NAV_FIRST, ANode.GetNextNode);
        end;
      end;
    finally
      EndUpdate;
    end;
end;

procedure GoToRecord(Which: Integer; Grid: TdxDBGrid);
begin
  if not Grid.DataSource.DataSet.Active then
    exit;
  case Which of
    NAV_FIRST : Grid.DataSource.DataSet.First;
    NAV_PRIOR : Grid.DataSource.DataSet.Prior;
    NAV_NEXT  : Grid.DataSource.DataSet.Next;
    NAV_LAST  : Grid.DataSource.DataSet.Last;
  end; { case }
end;
{ End Scroll Grid DevExpress}


{ PRIVATE }
procedure TGridBaseF.AskForSaveChanges;
var
  QuestionResult: Integer;
begin
 //CAR 5-5-2003
  FGiveError := True;

  if (ActiveGrid is TdxDBGrid) then
    if (ActiveGrid as TdxDBGrid).DataSource <> nil then
      if (ActiveGrid as TdxDBGrid).DataSource.DataSet.State in
        [dsEdit, dsInsert] then
      begin
        //CAR 15-1-2004 BUG - if dataset is not only a TTable or a TQuery !
        // MR:18-10-2002 BEGIN
        // DataSet can also be Query!
        // Therefore both should be checked
        // It is either TTable OR TQuery
        if ((ActiveGrid as TdxDBGrid).DataSource.DataSet is TTable) then
          QuestionResult := DisplayMessage( 'Table ' +
          ((ActiveGrid as TdxDBGrid).DataSource.DataSet as TTable).TableName +
          #13 + SPimsSaveChanges, mtConfirmation, [mbYes, mbNo])
        else
          if ((ActiveGrid as TdxDBGrid).DataSource.DataSet is TQuery) then
            QuestionResult := DisplayMessage( 'Query ' +
            ((ActiveGrid as TdxDBGrid).DataSource.DataSet as TQuery).Name +
            #13 + SPimsSaveChanges, mtConfirmation, [mbYes, mbNo])
          else
            QuestionResult := DisplayMessage(SPimsSaveChanges,
              mtConfirmation, [mbYes, mbNo]);
       // MR:18-10-2002 END
        case QuestionResult of
          mrYes    :
          begin
           //CAR 5-5-2003 
            try
              (ActiveGrid as TdxDBGrid).DataSource.DataSet.Post;
             except
              FGiveError := False;
            end
          end;
          mrNo     : (ActiveGrid as TdxDBGrid).DataSource.DataSet.Cancel;
          {mrCancel : Abort;  } // to be studied
        end; { case }
      end;
end;

procedure TGridBaseF.CustomizeColumns(Grid: TCustomdxTreeListControl;
  Down: boolean);
begin
  if Grid <> nil then
  begin
    if Down then
    begin
      // Show Columns Customizing Dialog
      Grid.ColumnsCustomizing;
    end
    else
      // Hide Columns Customizing Dialog
      Grid.EndColumnsCustomizing;
  end;
end;

procedure TGridBaseF.SetActiveGrid(Value: TCustomdxTreeListControl);
begin
  if FActiveGrid <> Value then
  begin
    FActiveGrid := Value;
    // Due to designtime problems the grid can sometimes loose its connection
    // to its datasource. This check should be redundant, because there is
    // allways a datasource connected. However in some occasions the loadorder
    // of the forms at designtime conflicts, leaving the grid with no datasource
    if (ActiveGrid is TdxDBGrid) then
      if (ActiveGrid as TdxDBGrid).DataSource <> nil then
        dsrcActive.DataSet := (ActiveGrid as TdxDBGrid).DataSource.DataSet;

// MR START
// Disable Search and Sort buttons, these don't work
// in combination with a BTRIEVE database
  {  dxBarManBase.LockUpdate := True;
    dxBarButtonSort.Visible := ivNever; }
{    dxBarButtonSearch.Visible := ivNever;
    dxBarManBase.LockUpdate := False;}
// MR END

    SynchToolBar;
    // Only allow insert/delete for (detail) grids when tag set to > 0 in form
    if (Value.Tag > 0)  then
    begin
      //CAR 24-10-2003 Make visible only if form is editable
      if (Form_Edit_YN = ITEM_VISIBIL_EDIT) then
      begin
        dxBarManBase.LockUpdate := True;
        dxBarBDBNavInsert.Visible := ivAlways;
        dxBarBDBNavDelete.Visible := ivAlways;
        dxBarManBase.LockUpdate := False;
      end;
    end
    else
    begin
      dxBarManBase.LockUpdate := True;
      dxBarBDBNavInsert.Visible := ivNever;
      dxBarBDBNavDelete.Visible := ivNever;
      dxBarButtonEditMode.Visible := ivNever;
      dxBarManBase.LockUpdate := False;
    end;


    // show buttons according to grid / list
    if (ActiveGrid is TdxDBGrid) then
    begin
      dxBarManBase.LockUpdate := True;
      dxBarButtonShowGroup.Visible     := ivAlways;
      dxBarButtonResetColumns.Visible  := ivAlways;
      dxBarButtonEditMode.Visible      := ivAlways;
      dxBarButtonRecordDetails.Visible := ivAlways;
      dxBarManBase.CategoryItemsVisible[5] := ivAlways;
      dxBarManBase.CategoryItemsVisible[6] := ivAlways;
      dxBarManBase.LockUpdate := False;
    end
    else
    begin
      dxBarManBase.LockUpdate := True;
      dxBarButtonShowGroup.Visible     := ivNever;
      dxBarButtonResetColumns.Visible  := ivNever;
      dxBarButtonEditMode.Visible      := ivNever;
      dxBarButtonRecordDetails.Visible := ivNever;
      dxBarManBase.CategoryItemsVisible[5] := ivNever;
      dxBarManBase.CategoryItemsVisible[6] := ivNever;
      dxBarManBase.LockUpdate := False;
    end;

  end;
end;

procedure TGridBaseF.SynchToolBar;
begin
  // Synchronize buttons according to current active grid
  if (ActiveGrid is TdxDBGrid) then
  begin
    dxBarButtonEditMode.Down  :=
      (edgoEditing in (ActiveGrid as TdxDBGrid).OptionsBehavior);
    dxBarButtonSort.Down      :=
      (edgoLoadAllRecords in (ActiveGrid as TdxDBGrid).OptionsDB);
    dxBarButtonSearch.Down    :=
      (edgoAutoSearch in (ActiveGrid as TdxDBGrid).OptionsBehavior);

    dxBarButtonShowGroup.Down := (ActiveGrid as TdxDBGrid).ShowGroupPanel;
  end;
end;

{ TfrmGridBase }

procedure TGridBaseF.dxBarBDBNavInsertClick(Sender: TObject);
begin
  if (ActiveGrid.Tag > 0) then
  begin
    if not(dxBarButtonRecordDetails.Down) then
    begin
      dxBarButtonRecordDetails.Down := True;
      dxBarButtonRecordDetailsClick(dxBarButtonRecordDetails);
      pnlDetail.SetFocus;
    end;
  end
  else
  begin
    if not(dxBarButtonEditMode.Down) then
    begin
      dxBarButtonEditMode.Down := True;
      dxBarButtonEditModeClick(dxBarButtonEditMode);
    end;
  end;
  (ActiveGrid as TdxDBGrid).DataSource.DataSet.Append;
end;

procedure TGridBaseF.dxBarButtonCollapseClick(Sender: TObject);
begin
  if (ActiveGrid is TdxTreeList) then
  begin
    (ActiveGrid as TdxTreeList).ClearSelection;
  end;
  ActiveGrid.FullCollapse;
  if (ActiveGrid is TdxTreeList) then
  begin
//    (ActiveGrid as TdxTreeList).ClearSelection;
    if ((ActiveGrid as TdxTreeList).TopNode <> nil) then
    begin
      (ActiveGrid as TdxTreeList).TopNode.Selected := True;
      (ActiveGrid as TdxTreeList).TopNode.Focused  := True;
    end;
  end;
end;

procedure TGridBaseF.dxBarButtonCustColClick(Sender: TObject);
begin
  CustomizeColumns(ActiveGrid, (Sender as TdxBarButton).Down);
  if ActiveGrid.Visible then
    ActiveGrid.SetFocus;
end;

procedure TGridBaseF.dxBarButtonEditModeClick(Sender: TObject);
begin
  if (ActiveGrid is TdxDBGrid) then
    if ((Sender as TdxBarButton).Down) then
    begin
      with (ActiveGrid as TdxDBGrid) do
      begin
        HighLightTextColor := clBlack;
        OptionsBehavior :=
          OptionsBehavior + [edgoEditing, edgoTabs] - [edgoAutoSearch];
        OptionsDB       := OptionsDB  + [] - [edgoLoadAllRecords];
        OptionsView     := OptionsView  + [edgoInvertSelect] - [edgoRowSelect];
       
      end;
    end
    else
    begin
      if (ActiveGrid as TdxDBGrid).DataSource.State in [dsEdit, dsInsert] then
        if DisplayMessage(SPimsSaveChanges, mtConfirmation, [mbYes, mbNo])
          = mrYes then
          (ActiveGrid as TdxDBGrid).DataSource.DataSet.Post
        else
          (ActiveGrid as TdxDBGrid).DataSource.DataSet.Cancel;

      with (ActiveGrid as TdxDBGrid) do
      begin
        HighlightTextColor := clHighlightText;
        OptionsBehavior :=
          OptionsBehavior - [edgoEditing, edgoTabs] + [];
        OptionsDB       := OptionsDB  - [] + [];
        OptionsView     := OptionsView  - [edgoInvertSelect] + [edgoRowSelect];
      end;
    end;
  SynchToolbar;
end;

procedure TGridBaseF.dxBarButtonExpandClick(Sender: TObject);
begin
  ActiveGrid.FullExpand;
end;

//CAR 5-5-2003 - FUNCTIONS for export grid
procedure TGridBaseF.CreateExportColumnsDesc(CapitionColumn, FieldName: String);
var
  IndexColExport: Integer;
begin
  IndexColExport := FExportGrid.ColumnCount;
  ExportGrid.Columns[IndexColExport] := FExportGrid.CreateColumn(TdxDBGridColumn);
  ExportGrid.Columns[IndexColExport].ColIndex := IndexColExport;
  ExportGrid.Columns[IndexColExport].Caption   := CapitionColumn;
  ExportGrid.Columns[IndexColExport].FieldName := FieldName;
  ExportGrid.Columns[IndexColExport].Width := 120;
end;

procedure TGridBaseF.CreateExportColumns(GridTmp: TdxDBGrid; TableName,
  FieldName: String);
var
  IndexGridTmp, IndexColExport, BandIndex: Integer;
  function GetBandIndex(GridTmp: TdxDBGrid; CaptionBand: String): Integer;
  var
    i: Integer;
  begin
    Result := -1;
    if CaptionBand = '' then
      Exit;
    for i := 0 to GridTmp.Bands.Count - 1 do
    // get first band with the same caption or band not used - with caption empty
      if (GridTmp.Bands[i].Caption = CaptionBand) or
        (GridTmp.Bands[i].Caption = '') then
      begin
        Result := i;
        Exit;
      end;
  end;
  function GetColIndex(GridTmp: TdxDBGrid; FieldName: String): Integer;
  var
    i: Integer;
  begin
    Result := -1;
    for i := 0 to GridTmp.ColumnCount - 1 do
    begin
      if FieldName <> '' then
        if GridTmp.Columns[i].FieldName = FieldName then
        begin
          Result := i;
          Exit;
        end;
    end;
  end;
begin
  IndexGridTmp:= GetColIndex(ExportGrid, FieldName);
  // check if the column of Field Name is already added
  if IndexGridTmp >= 0 then
    exit;

  IndexGridTmp:= GetColIndex(GridTmp, FieldName);
  // check if field is in GridTmp  Detail or Master form grid
  if IndexGridTmp < 0 then
    exit;
  BandIndex := GetBandIndex(ExportGrid,
    GridTmp.Bands[GridTmp.Columns[IndexGridTmp].BandIndex].Caption);
// if it is not created then add to export grid
  if BandIndex < 0 then
  begin
    BandIndex := ExportGrid.Bands.Count;
    ExportGrid.Bands[BandIndex] := ExportGrid.Bands.Add;
  end;
  ExportGrid.Bands[BandIndex].Caption :=
    GridTmp.Bands[GridTmp.Columns[IndexGridTmp].BandIndex].Caption;
  ExportGrid.Bands[BandIndex].Alignment := taLeftJustify;

  IndexColExport := ExportGrid.ColumnCount;
  ExportGrid.Columns[IndexColExport] := ExportGrid.CreateColumn(TdxDBGridColumn);
  ExportGrid.Columns[IndexColExport].ColIndex := IndexColExport;
  ExportGrid.Columns[IndexColExport].Width :=
    GridTmp.Columns[IndexGridTmp].Width;
  ExportGrid.Columns[IndexColExport].BandIndex := BandIndex;

  if ('DESCRIPTION' =  FieldName) and (GridTmp.Name = 'dxMasterGrid') then
     ExportGrid.Columns[IndexColExport].FieldName :=  ExportDescriptonLU
  else
     ExportGrid.Columns[IndexColExport].FieldName := FieldName;
  ExportGrid.Columns[IndexColExport].Caption :=
    GridTmp.Columns[IndexGridTmp].Caption;

//add changes for big employee number
  if FieldName = FloatEmpl then
  begin
    ExportGrid.Columns[IndexColExport].FieldName := 'FLOAT_EMP';
    ExportGrid.Columns[IndexColExport].Width := 65;
  end;
end;
// get the fist master field and inserted in the export grid
function TGridBaseF.CheckPKMasterTable(MasterFields: String): String;
begin
  Result := MasterFields;
  if pos(MasterFields, ';') > 0 then
  begin
    Result := Copy(MasterFields, 0, Pos(';', MasterFields) - 1);
    // PK Field contains the table name
    if Pos(GridFormDM.TableMaster.TableName, Result) <= 0 then
      Result := '';
  end;
end;
//end car export grid

procedure TGridBaseF.dxBarButtonExportGridClick(Sender: TObject);
var
  ExportFormat: Integer;
  ExportAll: Integer;
  Index, IndexFloat : Integer;
  FieldName : String;
  function CheckIfIsMasterExport: Boolean;
  begin
    Result :=
      ((ActiveGrid.Name = 'dxMasterGrid') or (ActiveGrid.Name = 'dxExportGrid')) and
      (ExportGrid <> Nil) and (dxDetailGrid.Visible) and
      (dxMasterGrid.Visible) and (GridFormDM.TableDetail.MasterFields <> '') and
      (ExportGrid <> Nil);
  end;
begin
//car 27.02.2003
  if ((ActiveGrid as TdxDBGrid)= Nil) or
    ((ActiveGrid as TdxDBGrid).DataSource = Nil) or
    ( not (ActiveGrid as TdxDBGrid).DataSource.DataSet.Active) then
    exit;


  ActiveGrid.SetFocus;
// car 8-5-2003   : create the export grid at runtime: only if form is master/detail
  //only master grid it is different exported  into a master/detail form
  if CheckIfIsMasterExport then
  begin
    GridFormDM.TableExport.Refresh;
    //get PK Master table use for link - suppose that is only one field
    // this part can be corrected on each form if more fields whould be exported
    FieldName := CheckPKMasterTable(GridFormDM.TableDetail.MasterFields);
    if (FieldName <> '') then
    begin
      CreateExportColumns(dxMasterGrid, GridFormDM.TableMaster.TableName,
        FieldName);
      CreateExportColumns(dxMasterGrid, GridFormDM.TableMaster.TableName,
       'DESCRIPTION');
    end;//if field
    for Index := 0 to  dxDetailGrid.columncount - 1 do
      if dxDetailGrid.Columns[Index].Visible then
        CreateExportColumns(dxDetailGrid, '', dxDetailGrid.Columns[Index].FieldName);

   ExportGrid.Tag := ActiveGrid.Tag;
   ActiveGrid := ExportGrid;
  end;

 //if is not master/detail form or it is detail grid into a master/detail form
  IndexFloat := -1;
  if not CheckIfIsMasterExport then
  begin
    if FloatEmpl <> '' then
    begin
      for IndexFloat := 0 to ((ActiveGrid as TdxDBGrid).ColumnCount - 1) do
        if ((ActiveGrid as TdxDBGrid).Columns[IndexFloat].FieldName = FloatEmpl) and
           ((ActiveGrid as TdxDBGrid).Columns[IndexFloat].Visible ) then
        begin
          (ActiveGrid as TdxDBGrid).Columns[IndexFloat].FieldName := 'FLOAT_EMP';
          (ActiveGrid as TdxDBGrid).Columns[IndexFloat].Width := 65;
          break;
        end;
    end;
  end;
  inherited;

  ExportFormat := (Sender as TdxBarButton).Tag div 2;
  ExportAll    := (Sender as TdxBarButton).Tag mod 2;
  with sdlgExport do
  begin
    DefaultExt := aExt[ExportFormat];
    Filter := aFilter[ExportFormat];
    FileName := 'Export.' + DefaultExt;
    if Execute then
    begin
      FSort := True;
      if Boolean(ExportFormat) then
      begin
       if ((GridFormDM <> NIl) and CheckIfIsMasterExport) then
         ExportGrid.SaveToXLS(FileName, not(Boolean(ExportAll)))
       else
         (ActiveGrid as TdxDBGrid).SaveToXLS(FileName, not(Boolean(ExportAll)));
        if DisplayMessage(SPimsOpenXLS, mtConfirmation, [mbYes, mbNo])
        = mrYes then
          if ShellExecute(Handle, PChar('OPEN'),
            PChar(sdlgExport.FileName), nil, nil, SW_SHOWMAXIMIZED) <= 32 then
              DisplayMessage(SPimsCannotOpenXLS, mtInformation, [mbOk]);
      end
      else
      begin
        if ((GridFormDM <> NIl) and CheckIfIsMasterExport) then
         ExportGrid.SaveToHTML(FileName, not(Boolean(ExportAll)))
       else
         (ActiveGrid as TdxDBGrid).SaveToHTML(FileName, not(Boolean(ExportAll)));
        if DisplayMessage(SPimsOpenHTML, mtConfirmation, [mbYes, mbNo])
        = mrYes then
          if ShellExecute(Handle, PChar('OPEN'),
            PChar(sdlgExport.FileName), nil, nil, SW_SHOWMAXIMIZED) <= 32 then
              DisplayMessage(SPimsCannotOpenHTML, mtInformation, [mbOk]);
      end
    end;
  end;
  FSort := False;
  //if is not master/detail form or it is detail grid into a master/detail form
  //restore changes for big employee numbers
  if not CheckIfIsMasterExport then
    if FloatEmpl <> '' then
      if (IndexFloat <= (ActiveGrid as TdxDBGrid).ColumnCount - 1) and
        (IndexFloat >= 0) then
        (ActiveGrid as TdxDBGrid).Columns[IndexFloat].FieldName := FloatEmpl;
 //if it is master/detail form restore activegrid;
  if CheckIfIsMasterExport then
  begin
    ActiveGrid := dxMasterGrid;
//clear all columns of exportgrid;
    for Index := 0 to ExportGrid.columncount - 1 do
     ExportGrid.Columns[0].Destroy;
    for Index := 0 to ExportGrid.Bands.count - 1 do
      ExportGrid.Bands[0].Destroy;
  end;
end;

procedure TGridBaseF.dxBarButtonNavigationClick(Sender: TObject);
begin
  if (ActiveGrid is TdxDBGrid) then
  begin
    if (edgoLoadAllRecords in (ActiveGrid as TdxDBGrid).OptionsDB) then
      GoToNode((Sender as TdxBarButton).Tag, ActiveGrid)
    else
      GoToRecord((Sender as TdxBarButton).Tag, (ActiveGrid as TdxDBGrid));
  end
  else
    GoToNode((Sender as TdxBarButton).Tag, ActiveGrid);
end;

procedure TGridBaseF.dxBarButtonRecordDetailsClick(Sender: TObject);
begin
  if (ActiveGrid as TdxDBGrid).DataSource.DataSet.State in [dsEdit, dsInsert] then
    if DisplayMessage(SPimsSaveChanges, mtConfirmation, [mbYes, mbNo])
      = mrYes then
      (ActiveGrid as TdxDBGrid).DataSource.DataSet.Post
    else
      (ActiveGrid as TdxDBGrid).DataSource.DataSet.Cancel;
  // hide CustomizeColumns form
  CustomizeColumns(ActiveGrid, False);

//CAR 24-10-2003 Solve bug - because of the new aligh of panels
//  pnlDetail.Visible := (Sender as TdxBarButton).Down;
  if (Sender as TdxBarButton).Down then
    pnlDetail.Height := SaveDetailPanel_Height
  else
    pnlDetail.Height := 1;

  ActiveGrid := dxDetailGrid;
  if ActiveGrid.Count > 0 then
    if not(ActiveGrid.Items[0].Focused) then { Cursor is not on first record }
    begin
      { Let the grid scroll, so the detailpanel doesn't hide the grid }
      GoToNode(NAV_PRIOR, ActiveGrid);
      GoToNode(NAV_NEXT, ActiveGrid);
    end;
// user changes CAR 15-9-2003
  if pnlDetail.Visible and pnlDetail.Enabled then
    pnlDetail.SetFocus
  else
    ActiveGrid.SetFocus;
 
end;

procedure TGridBaseF.dxBarButtonResetColumnsClick(Sender: TObject);
begin
  (ActiveGrid as TdxDBGrid).RegistryPath := PIMS_REGROOT_PATH_GRID
    + 'Design\'
    + Name
    + '\' + (ActiveGrid as TdxDBGrid).Name;
  (ActiveGrid as TdxDBGrid).LoadFromRegistry(
    (ActiveGrid as TdxDBGrid).RegistryPath);

  SynchToolBar;
end;

procedure TGridBaseF.dxBarButtonSearchClick(Sender: TObject);
begin
  inherited;
  //car 27.02.2003
  if not (ActiveGrid as TdxDBGrid).DataSource.DataSet.Active then
    exit;
  if (ActiveGrid is TdxDBGrid) then
    if ((Sender as TdxBarButton).Down) then
    begin
      with (ActiveGrid as TdxDBGrid) do
      begin
        HighlightTextColor := clHighlightText;
        OptionsBehavior :=
          OptionsBehavior + [edgoAutoSearch, edgoTabs] - [edgoEditing];
        OptionsDB       := OptionsDB  + [] - [];
        OptionsView     := OptionsView  + [edgoInvertSelect] - [edgoRowSelect];
      end;
    end
    else
    begin
      with (ActiveGrid as TdxDBGrid) do
      begin
        OptionsBehavior := OptionsBehavior + [] - [edgoAutoSearch];
        OptionsDB       := OptionsDB  + [] - [];
        OptionsView     := OptionsView  + [edgoRowSelect] - [edgoInvertSelect];
      end;
    end;
  SynchToolbar;
end;

procedure TGridBaseF.dxBarButtonShowGroupClick(Sender: TObject);
begin
  (ActiveGrid as TdxDBGrid).ShowGroupPanel := (Sender as TdxBarButton).Down;
end;
 
procedure TGridBaseF.dxBarButtonSortClick(Sender: TObject);
begin
  inherited;
  if (ActiveGrid is TdxDBGrid) then
  begin
    if ((Sender as TdxBarButton).Down) then
    begin
      FSort := True;
      with (ActiveGrid as TdxDBGrid) do
      begin
        HighlightTextColor := clHighlightText;
        OptionsBehavior := OptionsBehavior + [] - [edgoEditing];
        OptionsDB       := OptionsDB  + [edgoLoadAllRecords] - [];
        if (dxBarButtonEditMode.Down) then
          OptionsView     := OptionsView  + [] - [edgoInvertSelect];
        OptionsView     := OptionsView  + [edgoRowSelect] - [];

      end;
    end
    else
    begin
      with (ActiveGrid as TdxDBGrid) do
      begin
        OptionsBehavior := OptionsBehavior + [] - [];
        OptionsDB       := OptionsDB  + [] - [edgoLoadAllRecords];
        OptionsView     := OptionsView  + [] - [];
 
      end;
    end;
  end;
  SynchToolbar;
  FSort := False;
end;

procedure TGridBaseF.dxDetailGridDblClick(Sender: TObject);
begin
  dxBarButtonRecordDetails.Down := not(dxBarButtonRecordDetails.Down);
  dxBarButtonRecordDetailsClick(dxBarButtonRecordDetails);
end;

procedure TGridBaseF.dxGridBackgroundDrawEvent(Sender: TObject;
  ACanvas: TCanvas; ARect: TRect);
begin
  // Fill Group Panels Background with Color
  ACanvas.Brush.Color := (Sender as TdxDBGrid).GroupPanelColor;
  ACanvas.FillRect(ARect);
  // Draw the grouping text
  // This event is isolated for translation purposes
  if (Sender as TdxDBGrid).GroupColumnCount = 0 then
  begin
    ACanvas.Font.Color := clWhite;
    ACanvas.Brush.Style := bsClear;
    ACanvas.TextOut(10,(ARect.Bottom - ARect.Top + ACanvas.Font.Height) div 2,
      SPimsDragColumnHeader);
  end;
end;

procedure TGridBaseF.dxGridCustomDrawBand(Sender: TObject;
  ABand: TdxTreeListBand; ACanvas: TCanvas; ARect: TRect;
  var AText: String; var AColor: TColor; AFont: TFont;
  var AAlignment: TAlignment; var ADone: Boolean);
var
  X, Y : Integer;
  IsClipRgnExists : Boolean;
  PrevClipRgn, Rgn : HRGN;
begin
  {save rgn}
  PrevClipRgn := CreateRectRgn(0, 0, 0, 0);
  IsClipRgnExists := GetClipRgn(ACanvas.Handle, PrevClipRgn) = 1;
  Rgn := CreateRectRgnIndirect(ARect);
  SelectClipRgn(ACanvas.Handle, Rgn);
  DeleteObject(Rgn);

  with Sender as TCustomdxTreeListControl do
  begin { Draw band texture }
    X := ARect.Left;
    while X < ARect.Right do
    begin
      Y := ARect.Top;
      while Y < ARect.Bottom do
      begin
        ACanvas.Draw(X, Y, imgBandTexture.Picture.Bitmap);
        inc(Y, imgBandTexture.Height);
      end;
      inc(X, imgBandTexture.Width);
    end;

    ACanvas.Font.Size := 9;
    X := ARect.Left + 4;
    Y := ARect.Top + (ARect.Bottom - ARect.Top - ACanvas.TextHeight(AText)) div 2;
    // Set background to neutral color
    if (DownBandIndex = ABand.VisibleIndex) and DownBandPushed then
      ACanvas.Brush.Color := clBtnFace;
    ACanvas.Brush.Style := bsClear;
    // Set font color to shadow effect or to normal color when pushed
    if (DownBandIndex = ABand.VisibleIndex) and DownBandPushed then
      ACanvas.Font.Color := clBtnText
    else
      ACanvas.Font.Color := clBtnShadow;
    // Draw shadow or caption when pushed
    ACanvas.TextRect(ARect, X + 1, Y + 1, AText );
    ACanvas.Font.Color := clBtnText;
    // Draw normal band caption
    if not((DownBandIndex = ABand.VisibleIndex) and DownBandPushed) then
      ACanvas.TextRect(ARect, X, Y, AText );
    // Draw buttonize3D effect band
    DrawEdge(ACanvas.Handle, ARect, BDR_RAISEDINNER, BF_TOPLEFT);
    DrawEdge(ACanvas.Handle, ARect, BDR_RAISEDOUTER, BF_BOTTOMRIGHT);
    ADone := True;
  end;

  {restore rgn}
  if IsClipRgnExists then
    SelectClipRgn(ACanvas.Handle, PrevClipRgn)
  else
    SelectClipRgn(ACanvas.Handle, 0);
  DeleteObject(PrevClipRgn);
end;

procedure TGridBaseF.dxGridCustomDrawCell(Sender: TObject;
  ACanvas: TCanvas; ARect: TRect; ANode: TdxTreeListNode;
  AColumn: TdxTreeListColumn; ASelected, AFocused, ANewItemRow: Boolean;
  var AText: String; var AColor: TColor; AFont: TFont;
  var AAlignment: TAlignment; var ADone: Boolean);
var
  G: TCustomdxTreeListControl;//TdxDBGrid;
begin
  // Greenbar function
  inherited;
  G := Sender as TCustomdxTreeListControl;//TdxDBGrid;

  // grouping is on so keep the grid group node normal color
  if (G is TdxDBGrid) and ANode.HasChildren then
    Exit;
 
  if ASelected then
  begin
  // Different rowcolor while editing
    if (G is TdxDBGrid) and (edgoEditing in (G as TdxDBGrid).OptionsBehavior) then
    begin
      if AFocused then
      begin
        AColor := clGridEdit;
        AFont.Color := clBlack;
      end
      else
      begin
        AColor := clGridNoFocus;
        AFont.Color := clBlack;
      end;
    end
    else
      if AFocused then
        AColor := clHighlight
      else
      begin
        AColor := clGridNoFocus;
        AFont.Color := clBlack;
      end;
  // Different rowcolor while editing
  end
  else { ASelected }
    if ANode.Parent = nil then // There is no groupping
      if (G is TdxDBGrid) then
      //CAR needed when sort is pressed and all records are already sorted loaded
        if ((edgoLoadAllRecords in (G as TdxDBGrid).OptionsDB) and (ANode.Index mod 2 <> 0))
          or (not (edgoLoadAllRecords in (G as TdxDBGrid).OptionsDB))
          and ((G as TdxDBGrid).DataSource.DataSet.RecNo mod 2 = 0) then
          AColor := clWindow
        else
          AColor := clSecondLine
      else { treelist }
        AColor := clTreeParent

    else // Tree Grouping
      if ANode.Index mod 2 <> 0 then
        AColor := clWindow
      else
        AColor := clSecondLine;
  // Greenbar function
end;

procedure TGridBaseF.dxGridCustomDrawColumnHeader(Sender: TObject;
  AColumn: TdxTreeListColumn; ACanvas: TCanvas; ARect: TRect;
  var AText: String; var AColor: TColor; AFont: TFont;
  var AAlignment: TAlignment; var ASorted: TdxTreeListColumnSort;
  var ADone: Boolean);
begin
 { AText    := LowerCase(AText);
  if AText <> '' then
    AText[1] := UpCase(AText[1]);}
end;

procedure TGridBaseF.dxGridEndColumnsCustomizing(Sender: TObject);
begin
  dxBarButtonCustCol.Down := False;
end;

procedure TGridBaseF.dxGridEnter(Sender: TObject);
begin
// car 16-9-2003 user rights changes
  if Form_Edit_YN = ITEM_VISIBIL then
    Exit;
  AskForSaveChanges;
  ActiveGrid := (Sender as TCustomdxTreeListControl);
  if ActiveGrid = dxMastergrid then
    dxBarButtonEditMode.Enabled := False
  else
    dxBarButtonEditMode.Enabled := True;
end;

// MR:10-12-2003 Alternative 'FormClose' that asks for save.
//               Must be used instead of the 'FormClose' without ask-for-save,
//               if a GridBase-Form is NOT called from the 'SideMenu'-form,
//               but using statements like:
//                 MyFormF := TMyFormF.Create(Application);
//                 MyFormF.OnClose := FormCloseAskForSave;
//                 MyFormF.ShowModal;
//                 MyFormF.Free;
//               Otherwise access-violations are raised.
procedure TGridBaseF.FormCloseAskForSave(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  CustomizeColumns(ActiveGrid, False);
//car 5-5-2003 Old bug- do not display save message twice
  AskForSaveChanges;
  Action := caFree;
end;

procedure TGridBaseF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  CustomizeColumns(ActiveGrid, False);
//car 5-5-2003 Old bug- do not display save message twice
//  AskForSaveChanges;
  Action := caFree;
end;

procedure TGridBaseF.FormCreate(Sender: TObject);
begin
  inherited;
  if csDesigning in ComponentState then
  begin
    dxMasterGrid.DataSource := GridFormDM.DataSourceMaster;
    dxDetailGrid.DataSource := GridFormDM.DataSourceDetail;
  end;
  { Get exportpath for dataexport to HTML or Excel }
  FExportDir := ExtractFilePath(Application.ExeName);
  // Strip Bin\
  FExportDir := Copy(FExportDir, 1, (Length(FExportDir) - 4)) + 'Export';
  if not (DirectoryExists(FExportDir)) then
    ForceDirectories(FExportDir);
  sdlgExport := TSaveDialog.Create(Application);
  sdlgExport.InitialDir := FExportDir;

  ActiveGrid := dxDetailGrid;
// CRAETE GROUP
  CreateSummaryGroup(dxMasterGrid);
  CreateSummaryGroup(dxDetailGrid);
// scroll changes
  GridBaseF := TGridBaseF(SENDER);
  FloatEmpl := '';
  FGiveError := True;
// car 16-9-2003 initialize property used for user rights
  Form_Edit_YN := ITEM_VISIBIL_EDIT;
//car 24-10-2003 - solve bugs because of the new align of panels
  SaveDetailPanel_Height := PnlDetail.Height;
end;

procedure TGridBaseF.CreateSummaryGroup(dxDBGrid: TdxDBGrid);
begin
  dxDBGrid.SummaryGroups.Add;
  dxDBGrid.SummaryGroups[0].SummaryItems.Add;
  dxDBGrid.SummaryGroups[0].DefaultGroup := True;
  dxDBGrid.SummaryGroups[0].SummaryItems[0].SummaryField := dxDBGrid.KeyField;
  dxDBGrid.SummaryGroups[0].SummaryItems[0].SummaryFormat := 'COUNT=0';
  dxDBGrid.SummaryGroups[0].SummaryItems[0].SummaryType := cstCount;
end;

procedure TGridBaseF.FormDeactivate(Sender: TObject);
begin
  inherited;
//  CustomizeColumns(ActiveGrid, False);
  AskForSaveChanges;
end;

procedure TGridBaseF.FormDestroy(Sender: TObject);
var
  Counter: Integer;
  TempComponent: TComponent;
begin
  inherited;
  //CAR 8-5-2003 -Destroy export grid
   if ExportGrid <> Nil then
    ExportGrid.Destroy;

  { Save/Restore grid settings      }
  for Counter := 0 to (ComponentCount - 1) do
  begin
    TempComponent := Components[Counter];
    if (TempComponent is TdxDBGrid) then
    begin
      with (TempComponent as TdxDBGrid) do
      begin
        DefaultRowHeight := 17;
        OptionsView := OptionsView - [edgoInvertSelect];
        RegistryPath := PIMS_REGROOT_PATH_GRID
          + 'User\'
          + (Sender as TForm).Name
          + '\' + (TempComponent as TdxDBGrid).Name;
        SaveToRegistry(RegistryPath);
      end;
    end;
  end;
  sdlgExport.Free;
end;

procedure TGridBaseF.FormHide(Sender: TObject);
begin
  inherited;
  CustomizeColumns(ActiveGrid, False);
  AskForSaveChanges;
//CAR 5-5-2003
{  if GridFormDM <> nil then
    ActiveTables(GridFormDM, False);}
end;

procedure TGridBaseF.SetGridForm(Sender: TObject);
begin
 GridBaseF := TGridBaseF(SENDER);
end;

//Car: create export grid
procedure TGridBaseF.CreateExportGrid;
begin
  FExportGrid := Nil;
  if (dxMasterGrid <> Nil) and (dxDetailGrid <> Nil) and
     (dxMasterGrid.Visible) and (dxDetailGrid.Visible) then
  begin
    FExportGrid :=  TdxDBGrid.Create(dxDetailGrid.Parent);
    FExportGrid.Parent := dxDetailGrid.Parent;
    FExportGrid.Name := 'dxExportGrid';
    FExportGrid.DataSource := GridFormDM.DataSourceExport;
    FExportGrid.KeyField := dxMasterGrid.KeyField;
    FExportGrid.Visible := False;
    FExportGrid.OptionsBehavior := dxMasterGrid.OptionsBehavior;
    FExportGrid.ShowBands := True;
  end;
end;

// car 16-9-2003 rights user changes
procedure TGridBaseF.SetNotEditable;
begin
  if Form_Edit_YN = ITEM_VISIBIL then
  begin
    dxBarBDBNavInsert.Visible := ivNever;
    dxBarBDBNavDelete.Visible := ivNever;
    dxBarBDBNavPost.Visible := ivNever;
    dxBarBDBNavCancel.Visible := ivNever;
    dxBarButtonEditMode.Visible := ivNever;
    pnlDetail.Enabled := False;
  end;
end;

procedure TGridBaseF.SetEditable;
begin
  if Form_Edit_YN = ITEM_VISIBIL_EDIT then
  begin
    dxBarBDBNavInsert.Visible := ivAlways;
    dxBarBDBNavDelete.Visible := ivAlways;
    dxBarBDBNavPost.Visible := ivAlways;
    dxBarBDBNavCancel.Visible := ivAlways;
    dxBarButtonEditMode.Visible := ivAlways;
    pnlDetail.Enabled := True;
  end;
end;

procedure TGridBaseF.SetNotEnabled;
begin
  if Form_Edit_YN = ITEM_VISIBIL then
  begin
    dxBarBDBNavInsert.Enabled := False;
    dxBarBDBNavDelete.Enabled := False;
    dxBarBDBNavPost.Enabled := False;
    dxBarBDBNavCancel.Enabled := False;
  end;
end;

procedure TGridBaseF.FormShow(Sender: TObject);
begin
  inherited;
  //export changes  CAR 8-5-2003  - create a grid for export only if form is master/detail
  CreateExportGrid;
 
  dxBarButtonRecordDetails.Down := pnlDetail.Visible;
  SynchToolbar;
  if GridFormDM <> nil then
  begin
    ActiveTables(GridFormDM, False);
    ActiveTables(GridFormDM, True);
  end;
// user rights changes
  SetNotEditable;
end;

procedure TGridBaseF.pnlDetailExit(Sender: TObject);
begin
//car 5-5-2003 Old bug- do not display save message twice
//  AskForSaveChanges;
end;

{ Public }
procedure TGridBaseF.CloseCustomizeColumns;
begin
  CustomizeColumns(ActiveGrid, False);
end;

function TGridBaseF.CreateFormDM(AGridFormDM: TGridBaseClassDM): pointer;
begin
  if Assigned(GridFormDM) then
  begin
    GridFormDM.free;
    GridFormDM := Nil;
  end;
  GridFormDM := AGridFormDM.Create(Self);
  Result := GridFormDM;
end;

procedure TGridBaseF.DefaultLookupComboBoxKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if (Sender is TDBLookupComboBox) then
  begin
    TDBLookupComboBox(Sender).DataSource.Edit;
    TDBLookupComboBox(Sender).Field.Clear;
  end;
end;


end.
