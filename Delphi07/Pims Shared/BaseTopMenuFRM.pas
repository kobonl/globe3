(*
  MRA:24-MAR-2014 TD-24046
  - Related to this order.
  - Changed ABS-home-page address to www.abslbs.com:
    - Do not open at all (for security reasons)
  - Changed some entries: Do not open current folder!
  - Disabled getting PDF-manual.
*)
unit BaseTopMenuFRM;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BasePimsFRM, ImgList, dxCntner, Menus, StdActns, ActnList, ShellAPI;

type
  TBaseTopMenuForm = class(TBasePimsForm)
    StandardMenuActionList: TActionList;
    HelpAboutABSAct: TAction;
    HelpABSHomePageAct: TAction;
    HelpOrderingInfoAct: TAction;
    HelpPimsHomePageAct: TAction;
    HelpContentsAct: TAction;
    HelpIndexAct: TAction;
    FileExit1: TFileExit;
    mmPims: TMainMenu;
    miFile1: TMenuItem;
    miExit1: TMenuItem;
    miHelp1: TMenuItem;
    Contents1: TMenuItem;
    Index1: TMenuItem;
    HorMenuLine1: TMenuItem;
    ABSGroupHomePage1: TMenuItem;
    HorMenuLine2: TMenuItem;
    AboutABSGroup1: TMenuItem;
    OrderingInformation1: TMenuItem;
    StyleController: TdxEditStyleController;
    StyleControllerRequired: TdxEditStyleController;
    FileSettingsAct: TAction;
    Settings1: TMenuItem;
    FilePrintAct: TAction;
    FileClose: TAction;
    procedure HelpAboutABSActExecute(Sender: TObject);
    procedure HelpABSHomePageActExecute(Sender: TObject);
    procedure HelpOrderingInfoActExecute(Sender: TObject);
    procedure HelpPimsHomePageActExecute(Sender: TObject);
    procedure HelpContentsActExecute(Sender: TObject);
    procedure HelpIndexActExecute(Sender: TObject);
    procedure FileCloseExecute(Sender: TObject);
  private
    { Private declarations }
//    function GetManualFileName: String;
  public
    { Public declarations }
  end;

var
  BaseTopMenuForm: TBaseTopMenuForm;

implementation

uses
  {ORASystemDMT, }AboutFRM, OrderInfoFRM, UPimsMessageRes, UPimsConst;

{$R *.DFM}

procedure TBaseTopMenuForm.HelpAboutABSActExecute(Sender: TObject);
begin
  inherited;
  with TAboutForm.Create(Application) do
  begin
    try
      ShowModal;
    finally
      Free;
    end;
  end; {with}
end;

procedure TBaseTopMenuForm.HelpABSHomePageActExecute(Sender: TObject);
begin
  inherited;
  MessageDlg('This option is disabled because of security reasons.',
    mtInformation, [mbOk], 0);
{
  if ShellExecute(Handle, PChar('OPEN'),
    PChar('http://www.abslbs.com'), nil, nil, SW_SHOWMAXIMIZED) <= 32 then
      DisplayMessage((SPimsCannotOpenHTML), mtInformation, [mbOk]);
}
end;

procedure TBaseTopMenuForm.HelpOrderingInfoActExecute(Sender: TObject);
begin
  inherited;
  with TOrderInfoForm.Create(Application) do
  begin
    try
      ShowModal;
    finally
      Free;
    end;
  end; {with}
end;

procedure TBaseTopMenuForm.HelpPimsHomePageActExecute(Sender: TObject);
begin
  inherited;
  HelpABSHomePageActExecute(Sender);
end;

procedure TBaseTopMenuForm.HelpContentsActExecute(Sender: TObject);
begin
  inherited;
  MessageDlg('Help Contents will be implemented soon!',  mtInformation, [mbOk], 0);
{
  if ShellExecute(Handle, PChar('OPEN'),
    PChar(GetManualFileName), nil, nil, SW_SHOWMAXIMIZED) <= 32 then
      MessageDlg('Help Contents will be implemented soon!',
        mtInformation, [mbOk], 0);
}
end;

procedure TBaseTopMenuForm.HelpIndexActExecute(Sender: TObject);
begin
  inherited;
  MessageDlg('Help Index will be implemented soon!',  mtInformation, [mbOk], 0);
{
  if ShellExecute(Handle, PChar('OPEN'),
    PChar(GetManualFileName), nil, nil, SW_SHOWMAXIMIZED) <= 32 then
      MessageDlg('Help Index will be implemented soon!',
        mtInformation, [mbOk], 0);
}        
end;

(*
function TBaseTopMenuForm.GetManualFileName: String;
var
  SearchRec : TSearchRec;
  ManualFileDir: String;
  ManualFileName: String;
begin
  // find the Manual file
  // Get path for Manual file
  ManualFileDir := ExtractFilePath(Application.ExeName);

  // Strip Bin\   add Manual\
  ManualFileDir :=
    Copy(ManualFileDir, 1, (Length(ManualFileDir) - 4)) + 'Manual\';
  if not (DirectoryExists(ManualFileDir)) then
    ForceDirectories(ManualFileDir);
  ManualFileName := ManualFileDir + '*.pdf';

  if (FindFirst(ManualFileName, faAnyFile, SearchRec) = 0) then
    Result := ManualFileDir + SearchRec.Name
  else
    Result := '';

  // free memory for searchrecs
  FindClose(SearchRec);
end;
*)

procedure TBaseTopMenuForm.FileCloseExecute(Sender: TObject);
begin
  inherited;
  Close;
end;

end.
