inherited DialogEmpListViewF: TDialogEmpListViewF
  Left = 436
  Top = 296
  VertScrollBar.Range = 0
  BorderStyle = bsDialog
  Caption = 'Employee Productivity Overview'
  ClientHeight = 401
  ClientWidth = 684
  Constraints.MinHeight = 460
  Constraints.MinWidth = 700
  Position = poMainFormCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlImageBase: TPanel
    Width = 684
    inherited imgBasePims: TImage
      Left = 567
    end
  end
  inherited stbarBase: TStatusBar
    Top = 382
    Width = 684
  end
  inherited pnlInsertBase: TPanel
    Width = 684
    Height = 282
    Font.Color = clBlack
    object pnlTop: TPanel
      Left = 0
      Top = 0
      Width = 684
      Height = 25
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object lblName: TLabel
        Left = 0
        Top = 0
        Width = 68
        Height = 25
        Align = alLeft
        Caption = 'lblName'
      end
      object lblDate: TLabel
        Left = 626
        Top = 0
        Width = 58
        Height = 25
        Align = alRight
        Alignment = taRightJustify
        Caption = 'lblDate'
      end
    end
    object pnlMain: TPanel
      Left = 0
      Top = 25
      Width = 684
      Height = 257
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object dxDBGridEmpEff: TdxDBGrid
        Left = 0
        Top = 0
        Width = 684
        Height = 257
        Bands = <
          item
            Width = 150
          end
          item
            Caption = 'Quantity'
            Width = 228
          end
          item
            Caption = 'Time'
            Width = 192
          end
          item
            Width = 80
          end>
        DefaultLayout = False
        HeaderPanelRowCount = 1
        KeyField = 'PRIMKEY'
        ShowSummaryFooter = True
        SummaryGroups = <>
        SummarySeparator = ', '
        Align = alClient
        Ctl3D = False
        Enabled = False
        ParentCtl3D = False
        TabOrder = 0
        DataSource = dsEmpEff
        DblClkExpanding = False
        Filter.Criteria = {00000000}
        HideFocusRect = True
        HideSelection = True
        OptionsBehavior = []
        OptionsCustomize = [edgoColumnSizing]
        OptionsDB = [edgoLoadAllRecords]
        OptionsView = [edgoUseBitmap]
        ShowBands = True
        OnCustomDrawCell = dxDBGridEmpEffCustomDrawCell
        object dxDBGridEmpEffColumnWORKSPOT: TdxDBGridColumn
          Caption = 'Workspot'
          Width = 158
          BandIndex = 0
          RowIndex = 0
          FieldName = 'WORKSPOT'
        end
        object dxDBGridEmpEffColumnJOB: TdxDBGridColumn
          Caption = 'Job'
          Width = 147
          BandIndex = 0
          RowIndex = 0
          FieldName = 'JOB'
        end
        object dxDBGridEmpEffColumnQACTUAL: TdxDBGridColumn
          Caption = 'Actual'
          HeaderAlignment = taRightJustify
          Width = 65
          BandIndex = 1
          RowIndex = 0
          FieldName = 'QACTUAL'
          SummaryFooterType = cstSum
          SummaryFooterField = 'QACTUAL'
          SummaryFooterFormat = '#####'
          SummaryType = cstSum
        end
        object dxDBGridEmpEffColumnQNORM: TdxDBGridColumn
          Caption = 'Norm'
          HeaderAlignment = taRightJustify
          Width = 81
          BandIndex = 1
          RowIndex = 0
          FieldName = 'QNORM'
          SummaryFooterType = cstSum
          SummaryFooterField = 'QNORM'
          SummaryFooterFormat = '#####'
        end
        object dxDBGridEmpEffColumnQDIFF: TdxDBGridColumn
          Caption = 'Diff.'
          HeaderAlignment = taRightJustify
          Width = 82
          BandIndex = 1
          RowIndex = 0
          FieldName = 'QDIFF'
          SummaryFooterType = cstSum
          SummaryFooterField = 'QDIFF'
          SummaryFooterFormat = '#####'
          OnDrawSummaryFooter = dxDBGridEmpEffColumnQDIFFDrawSummaryFooter
        end
        object dxDBGridEmpEffColumnTACTUAL: TdxDBGridColumn
          Caption = 'Actual'
          HeaderAlignment = taRightJustify
          Width = 60
          BandIndex = 2
          RowIndex = 0
          FieldName = 'TACTUAL'
          SummaryFooterType = cstSum
          SummaryFooterField = 'TACTUAL'
          SummaryFooterFormat = '#####'
          OnDrawSummaryFooter = dxDBGridEmpEffColumnTACTUALDrawSummaryFooter
        end
        object dxDBGridEmpEffColumnTNORM: TdxDBGridColumn
          Caption = 'Norm'
          HeaderAlignment = taRightJustify
          Width = 56
          BandIndex = 2
          RowIndex = 0
          FieldName = 'TNORM'
          SummaryFooterType = cstSum
          SummaryFooterField = 'TNORM'
          SummaryFooterFormat = '#####'
          OnDrawSummaryFooter = dxDBGridEmpEffColumnTNORMDrawSummaryFooter
        end
        object dxDBGridEmpEffColumnTDIFF: TdxDBGridColumn
          Caption = 'Diff.'
          HeaderAlignment = taRightJustify
          Width = 76
          BandIndex = 2
          RowIndex = 0
          FieldName = 'TDIFF'
          SummaryFooterType = cstSum
          SummaryFooterField = 'TDIFF'
          SummaryFooterFormat = '#####'
          OnDrawSummaryFooter = dxDBGridEmpEffColumnTDIFFDrawSummaryFooter
        end
        object dxDBGridEmpEffColumnEFF: TdxDBGridColumn
          Caption = 'Efficiency'
          HeaderAlignment = taRightJustify
          Width = 100
          BandIndex = 3
          RowIndex = 0
          FieldName = 'EFF'
          SummaryFooterType = cstSum
          SummaryFooterField = 'EFF'
          SummaryFooterFormat = '### %'
          OnDrawSummaryFooter = dxDBGridEmpEffColumnEFFDrawSummaryFooter
        end
      end
    end
  end
  inherited pnlBottom: TPanel
    Top = 319
    Width = 684
    Font.Height = -16
    inherited btnOK: TBitBtn
      Left = 360
    end
    inherited btnCancel: TBitBtn
      Left = 666
    end
  end
  inherited imgList16x16: TImageList
    Left = 416
    Top = 0
  end
  inherited imgNoYes: TImageList
    Top = 0
  end
  inherited StandardMenuActionList: TActionList
    Left = 512
    Top = 65528
  end
  inherited mmPims: TMainMenu
    Left = 520
    Top = 40
  end
  inherited StyleController: TdxEditStyleController
    Left = 568
    Top = 0
  end
  inherited StyleControllerRequired: TdxEditStyleController
    Left = 608
    Top = 0
  end
  object TimerAutoClose: TTimer
    Enabled = False
    Interval = 10000
    OnTimer = TimerAutoCloseTimer
    Left = 656
    Top = 65533
  end
  object dsEmpEff: TDataSource
    DataSet = RealTimeEffDM.odsRTEmpEff
    Left = 360
    Top = 6
  end
end
