(*
  MRA:16-MAY-2014 TD-24046
  - Translate issues: Use a different method to translate.
    - Do NOT do this in separate files where the translation is done
      hard-coded!
    - New method:
      - Use 1 resource-strings-file that contains the translation-strings.
      - Use 1 translate-unit that assigns these strings to the properties
        in the forms.
      - In this way there is only 1 form that can be translated in multiple
        languages when the application is run.
      - The project-file defines the language using a condition.
  MRA:23-JUL-2015 PIM-50
  - Addition of DialogEnterJobComment-translations
*)
unit UTranslateShared;

interface

type
  TTranslateHandlerShared = class
  private
  public
    constructor Create; overload;
    destructor Destroy; override;
    procedure TranslateAll;
    procedure TranslateDialogSelectDownType;
    procedure TranslateDialogEmpGraph;
    procedure TranslateDialogEmpListView;
    procedure TranslateDialogEnterJobComment;
    procedure TranslateDialogShiftCheck;
    procedure TranslateInit;
  end;

var
  ATranslateHandlerShared: TTranslateHandlerShared;

implementation

uses
  UTranslateStringsShared, DialogChangeJobFRM, DialogSelectDownTypeFRM,
  DialogEnterJobCommentFRM
{$IFNDEF TIMERECORDING}
  ,DialogEmpGraphFRM, DialogEmpListViewFRM
{$ENDIF}
  , DialogShiftCheckFRM
  ;

{ TTranslateHandlerShared }

constructor TTranslateHandlerShared.Create;
begin
//
end;

destructor TTranslateHandlerShared.Destroy;
begin
//
  inherited;
end;

procedure TTranslateHandlerShared.TranslateAll;
begin
  if Assigned(DialogChangeJobF) then
    with DialogChangeJobF do
    begin
      Caption := STransChangeJob;
      dxDBGrid1DESCRIPTION.Caption := STransChangeJob2;
      btnDown.Caption := STransDown;
      btnContinue.Caption := STransContinue;
      BitBtnOk.Caption := STransOK;
      BitBtnCancel.Caption := STransCancel;
      BitBtnEndOfDay.Caption := STransEndOfDay;
      BtnBreak.Caption := STransBreak;
      btnLunch.Caption := STransLunch;
    end;
  TranslateDialogEmpGraph;
  TranslateDialogEmpListView;
  TranslateDialogEnterJobComment;
  TranslateDialogShiftCheck;
end;

procedure TTranslateHandlerShared.TranslateDialogEmpGraph;
begin
{$IFNDEF TIMERECORDING}
  if Assigned(DialogEmpGraphF) then
    with DialogEmpGraphF do
    begin
      Series4.Title := STransSeries4Title;
      Series3.Title := STransSeries3Title;
      Series1.Title := STransSeries1Title;
    end;
{$ENDIF}
end;

procedure TTranslateHandlerShared.TranslateDialogEmpListView;
begin
{$IFNDEF TIMERECORDING}
  if Assigned(DialogEmpListViewF) then
    with DialogEmpListViewF do
    begin
      Caption := STransEmpListView;
      dxDBGridEmpEff.Bands[1].Caption := STransQuantity;
      dxDBGridEmpEff.Bands[2].Caption := STransTime;
      dxDBGridEmpEffColumnWORKSPOT.Caption := STransWorkspot;
      dxDBGridEmpEffColumnJOB.Caption := STransJob;
      dxDBGridEmpEffColumnQACTUAL.Caption := STransActual;
      dxDBGridEmpEffColumnQNORM.Caption := STransNorm;
      dxDBGridEmpEffColumnQDIFF.Caption := STransDiff;
      dxDBGridEmpEffColumnTACTUAL.Caption := STransActual;
      dxDBGridEmpEffColumnTNORM.Caption := STransNorm;
      dxDBGridEmpEffColumnTDIFF.Caption := STransDiff;
      dxDBGridEmpEffColumnEFF.Caption := STransEfficiency;
    end;
{$ENDIF}
end;

procedure TTranslateHandlerShared.TranslateDialogEnterJobComment;
begin
  if Assigned(DialogEnterJobCommentF) then
    with DialogEnterJobCommentF do
    begin
      Caption := STransDialogEnterJobComment;
      Label1.Caption := STransComment; 
    end;
end;

procedure TTranslateHandlerShared.TranslateDialogSelectDownType;
begin
  if Assigned(DialogSelectDownTypeF) then
    with DialogSelectDownTypeF do
    begin
      Caption := STransSelectDownType;
      GroupBox1.Caption := STransSelectDownType;
      btnMechanicalDown.Caption := STransMechDown;
      btnNoMerchandize.Caption := STransNoMerch;
      btnOk.Caption := STransOK;
      btnCancel.Caption := STransCancel;
    end;
end;

procedure TTranslateHandlerShared.TranslateDialogShiftCheck;
begin
  if Assigned(DialogShiftCheckForm) then
    with DialogShiftCheckForm do
    begin
      Caption := STransPleaseSelectShift;
      GroupBox1.Caption := STransPleaseSelectShift;
      dxDBGrid1SHIFT_NUMBER.Caption := STransShift;
      dxDBGrid1DESCRIPTION.Caption := STransDescription;
      dxDBGrid1STARTDATE.Caption := STransDate;
      dxDBGrid1START.Caption := STransStart;
      dxDBGrid1END.Caption := STransEnd;
    end;
end;

procedure TTranslateHandlerShared.TranslateInit;
begin
//
end;

initialization
  { Initialization section goes here }
  ATranslateHandlerShared := TTranslateHandlerShared.Create;

finalization
  { Finalization section goes here }
  ATranslateHandlerShared.Free;


end.
