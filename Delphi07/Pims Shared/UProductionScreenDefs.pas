(*
  MRA:9-SEP-2016 PIM-213
  - Addition of TProdScreenType: pstWSEmpEff
*)
unit UProductionScreenDefs;

interface

const
  NOJOB = 'NOJOB';
  BREAKJOB = 'BREAK';
  ModalBreak = 12;
  ModalLunch = 13;

type
  TProdScreenType = (pstNoTimeRec, pstMachineTimeRec, pstWorkspotTimeRec,
    pstMachine, pstWSEmpEff);
  TEffQuantityTime = (efQuantity, efTime);
  

implementation

end.
