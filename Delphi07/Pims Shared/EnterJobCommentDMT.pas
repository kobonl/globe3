(*
  23-JUL-2015 PIM-50
  - Enter Job Comment functionality
*)
unit EnterJobCommentDMT;

interface

uses
  SysUtils, Classes, Forms, Dialogs, Controls, ORASystemDMT, Oracle, UPimsConst;

type
  TEnterJobCommentDM = class(TDataModule)
    oqInsertJobComment: TOracleQuery;
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure StoreJobComment(APlantCode, AWorkspotCode, AJobCode,
      AComment: String; AEmployeeNumber: Integer);
    procedure EnterJobComment(APlantCode, AWorkspotCode, AJobCode: String;
      AEmployeeNumber: Integer; AParentForm: TForm);
  end;

var
  EnterJobCommentDM: TEnterJobCommentDM;

implementation

{$R *.dfm}

uses
  UGlobalFunctions, DialogEnterJobCommentFRM;

{ TEnterJobCommentDM }

procedure TEnterJobCommentDM.DataModuleCreate(Sender: TObject);
begin
  DialogEnterJobCommentF := TDialogEnterJobCommentF.Create(Application);
  DialogEnterJobCommentF.Visible := False;
end;

procedure TEnterJobCommentDM.StoreJobComment(APlantCode, AWorkspotCode,
  AJobCode, AComment: String; AEmployeeNumber: Integer);
begin
  if (AComment = '') then
    Exit;
  try
    with oqInsertJobComment do
    begin
      ClearVariables;
      SetVariable('PLANT_CODE',      APlantCode);
      SetVariable('WORKSPOT_CODE',   AWorkspotCode);
      SetVariable('JOB_CODE',        AJobCode);
      SetVariable('EMPLOYEE_NUMBER', AEmployeeNumber);
      SetVariable('MESSAGE',         Copy(AComment, 1, 80)); // Max 80 chars.
      SetVariable('MUTATOR',         ORASystemDM.CurrentProgramUser);
      Execute;
      if ORASystemDM.OracleSession.InTransaction then
        ORASystemDM.OracleSession.Commit;
    end;
  except
    on E:EOracleError do
    begin
      WErrorLog('StoreJobComment: ' + E.Message);
    end;
    on E:Exception do
    begin
      WErrorLog('StoreJobComment: ' + E.Message);
    end;
  end;
end; // StoreJobComment

procedure TEnterJobCommentDM.EnterJobComment(APlantCode, AWorkspotCode,
  AJobCode: String; AEmployeeNumber: Integer; AParentForm: TForm);
begin
  if not ORASystemDM.AddJobComment then
    Exit;
  if (AJobCode = MECHANICAL_DOWN_JOB) or (AJobCode = NO_MERCHANDIZE_JOB) then
  begin
    try
      try
        DialogEnterJobCommentF.AssignPosition(AParentForm);
        if DialogEnterJobCommentF.ShowModal = mrOK then
        begin
          StoreJobComment(APlantCode, AWorkspotCode,
            AJobCode, DialogEnterJobCommentF.Comment, AEmployeeNumber);
        end;
      except
      end;
    finally
    end;
  end;
end; // EnterJobComment

end.
