(*
  MRA:28-MAY-2013 20014289 New look Pims
  - Some pictures/colors are changed.
*)
unit BasePimsFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DBCtrls, dxCntner, dxTL, dxDBCtrl, dxDBGrid, StdCtrls,
  dxEditor, dxExEdtr, dxDBEdtr, dxDBELib, ImgList, ExtCtrls, dxEdLib;

type
  TBasePimsForm = class(TForm)
    imgList16x16: TImageList;
    imgNoYes: TImageList;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    procedure SetParent(AParent: TWinControl); override;
  public
    { Public declarations }
  end;

var
  BasePimsForm: TBasePimsForm;

{$IFDEF XPLOOK}
{$R WindowsXP.res}
{$ENDIF}

implementation

uses
  SplashFRM, UPimsConst;

{$R *.DFM}

procedure TBasePimsForm.FormCreate(Sender: TObject);
var
  Counter: Integer;
  TempComponent: TComponent;
begin
  // 20014289
//  Color := clPimsLBlue;
  if Starting then
     SplashForm.UpdateProgressBar(Self);
  { Set colours for edit components }
  { Save/Restore grid settings      }
  for Counter := 0 to (ComponentCount - 1) do
  begin
    TempComponent := Components[Counter];
    if (TempComponent is TEdit) then
    begin
      if (TempComponent as TEdit).Tag <> 0 then
        (TempComponent as TEdit).Color := clRequired;
      if not((TempComponent as TEdit).Enabled) then
        (TempComponent as TEdit).Color := clDisabled;
    end;

    if (TempComponent is TDBEdit) then
    begin
      if (TempComponent as TDBEdit).Tag <> 0 then
        (TempComponent as TDBEdit).Color := clRequired;
      if not((TempComponent as TDBEdit).Enabled) then
        (TempComponent as TDBEdit).Color := clDisabled;
    end;

    if (TempComponent is TdxEdit) then
    begin
      if (TempComponent as TdxEdit).Tag <> 0 then
        (TempComponent as TdxEdit).Color := clRequired;
      if not((TempComponent as TdxEdit).Enabled) then
        (TempComponent as TdxEdit).Color := clDisabled;
    end;

    if (TempComponent is TdxDBEdit) then
    begin
      if (TempComponent as TdxDBEdit).Tag <> 0 then
        (TempComponent as TdxDBEdit).Color := clRequired;
      if not((TempComponent as TdxDBEdit).Enabled) then
        (TempComponent as TdxDBEdit).Color := clDisabled;
    end;

    if (TempComponent is TdxButtonEdit) then
    begin
      if (TempComponent as TdxButtonEdit).Tag <> 0 then
        (TempComponent as TdxButtonEdit).Color := clRequired;
      if not((TempComponent as TdxButtonEdit).Enabled) then
        (TempComponent as TdxButtonEdit).Color := clDisabled;
    end;

    if (TempComponent is TdxDBButtonEdit) then
    begin
      if (TempComponent as TdxDBButtonEdit).Tag <> 0 then
        (TempComponent as TdxDBButtonEdit).Color := clRequired;
      if not((TempComponent as TdxDBButtonEdit).Enabled) then
        (TempComponent as TdxDBButtonEdit).Color := clDisabled;
    end;

    if (TempComponent is TdxLookupEdit) then
    begin
      if (TempComponent as TdxLookupEdit).Tag <> 0 then
        (TempComponent as TdxLookupEdit).Color := clRequired;
      if not ((TempComponent as TdxLookupEdit).Enabled) then
        (TempComponent as TdxLookupEdit).Color := clDisabled;
    end;

    if (TempComponent is TdxDBLookupEdit) then
    begin
      if (TempComponent as TdxDBLookupEdit).Tag <> 0 then
        (TempComponent as TdxDBLookupEdit).Color := clRequired;
      if not ((TempComponent as TdxDBLookupEdit).Enabled) then
        (TempComponent as TdxDBLookupEdit).Color := clDisabled;
    end;


    if (TempComponent is TdxPickEdit) then
    begin
      if (TempComponent as TdxPickEdit).Tag <> 0 then
        (TempComponent as TdxPickEdit).Color := clRequired;
      if not ((TempComponent as TdxPickEdit).Enabled) then
        (TempComponent as TdxPickEdit).Color := clDisabled;
    end;

    if (TempComponent is TdxDBPickEdit) then
    begin
      if (TempComponent as TdxDBPickEdit).Tag <> 0 then
        (TempComponent as TdxDBPickEdit).Color := clRequired;
      if not ((TempComponent as TdxDBPickEdit).Enabled) then
        (TempComponent as TdxDBPickEdit).Color := clDisabled;
    end;

    if (TempComponent is TdxImageEdit) then
    begin
      if (TempComponent as TdxImageEdit).Tag <> 0 then
        (TempComponent as TdxImageEdit).Color := clRequired;
      if not ((TempComponent as TdxImageEdit).Enabled) then
        (TempComponent as TdxImageEdit).Color := clDisabled;
    end;

    if (TempComponent is TdxDBImageEdit) then
    begin
      if (TempComponent as TdxDBImageEdit).Tag <> 0 then
        (TempComponent as TdxDBImageEdit).Color := clRequired;
      if not ((TempComponent as TdxDBImageEdit).Enabled) then
        (TempComponent as TdxDBImageEdit).Color := clDisabled;
    end;

    if (TempComponent is TdxCalcEdit) then
    begin
      if (TempComponent as TdxCalcEdit).Tag <> 0 then
        (TempComponent as TdxCalcEdit).Color := clRequired;
      if not ((TempComponent as TdxCalcEdit).Enabled) then
        (TempComponent as TdxCalcEdit).Color := clDisabled;
    end;

    if (TempComponent is TdxDBCalcEdit) then
    begin
      if (TempComponent as TdxDBCalcEdit).Tag <> 0 then
        (TempComponent as TdxDBCalcEdit).Color := clRequired;
      if not ((TempComponent as TdxDBCalcEdit).Enabled) then
        (TempComponent as TdxDBCalcEdit).Color := clDisabled;
    end;

    if (TempComponent is TdxDateEdit) then
    begin
      if (TempComponent as TdxDateEdit).Tag <> 0 then
        (TempComponent as TdxDateEdit).Color := clRequired;
      if not ((TempComponent as TdxDateEdit).Enabled) then
        (TempComponent as TdxDateEdit).Color := clDisabled;
    end;

    if (TempComponent is TdxDBDateEdit) then
    begin
      if (TempComponent as TdxDBDateEdit).Tag <> 0 then
        (TempComponent as TdxDBDateEdit).Color := clRequired;
      if not ((TempComponent as TdxDBDateEdit).Enabled) then
        (TempComponent as TdxDBDateEdit).Color := clDisabled;
    end;

    if (TempComponent is TdxDBGrid) then
    begin
    { Get registrypath for layoutsaving }
    { Save Design time settings for restore to factory }
      if (TempComponent as TdxDBGrid).Tag >= 0 then
      begin
        with (TempComponent as TdxDBGrid) do
        begin
          RegistryPath := PIMS_REGROOT_PATH_GRID
            + 'Design\'
            + (Sender as TForm).Name
            + '\' + (TempComponent as TdxDBGrid).Name;
          SaveToRegistry(RegistryPath);
        end;
       { Load User settings }
        with (TempComponent as TdxDBGrid) do
        begin
          try
            RegistryPath := PIMS_REGROOT_PATH_GRID
              + 'User\'
              + (Sender as TForm).Name
              + '\' + (TempComponent as TdxDBGrid).Name;
            LoadFromRegistry(RegistryPath);
          except
            // the last code was scrapped or invalid grouping
            RegistryPath := PIMS_REGROOT_PATH_GRID
              + 'Design\'
              + (Sender as TForm).Name
              + '\' + (TempComponent as TdxDBGrid).Name;
            LoadFromRegistry(RegistryPath);
          end;
       { Dynamically create a Group/Summary-field }
          SummaryGroups.Add;
          SummaryGroups[0].SummaryItems.Add;
          SummaryGroups[0].DefaultGroup := True;
          SummaryGroups[0].SummaryItems[0].SummaryField  := KeyField;
          SummaryGroups[0].SummaryItems[0].SummaryFormat := 'COUNT=0';
          SummaryGroups[0].SummaryItems[0].SummaryType   := cstCount;
        end; { with }
      end; {if}
    end;
  end;
end;

procedure TBasePimsForm.SetParent(AParent: TWinControl);
begin
  inherited;
  if Assigned(AParent) then
  begin
    Position := poDefault;
    BorderIcons := [];
    BorderStyle := bsNone;
    Align       := alClient;
//    SetBounds(0, 0, Parent.ClientWidth, Parent.ClientHeight);
  end;
end;

procedure TBasePimsForm.FormDestroy(Sender: TObject);
begin
//  be here
end;

procedure TBasePimsForm.FormShow(Sender: TObject);
begin
  // be here
end;

end.
