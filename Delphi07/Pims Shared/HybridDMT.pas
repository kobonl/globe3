(*
  MRA:19-MAY-2016 PIM-179
  - When using Hybrid-mode (Hybrid=1):
    - Show only job ATWORK as default,
      when workspot has setting 'Measure Productivity equals Yes'.
    - Also add job ATWORK and OCCUP when they do not exist yet for
      the selected workspot.
  MRA:20-MAY-2016 PIM-181
  - Only allow one employee with job ATWORK per workspot.
  - Other employee on same workspot are possible but get job OCCUP.
  MRA:20-JUN-2016 PIM-181
  - Only do the Hybrid=1 actions when workspot is defined as Measure
    Productivity.
  MRA:8-JUL-2019 GLOB3-330
  - Make functionality with 'occupied' optional
*)
unit HybridDMT;

interface

uses
  SysUtils, Classes, Db, Oracle, OracleData, ORASystemDMT, UScannedIDCard,
  DialogMessageFRM, Forms, Controls;

type
  THybridDM = class(TDataModule)
    oqCheckMachine: TOracleQuery;
  private
    { Private declarations }
    FCloseCurrentScan: Boolean;
    FIsOccupied: Boolean;
  public
    { Public declarations }
    LastIDCard: TScannedIDCard;
    NextIDCard: TScannedIDCard;
    function HybridDialogGoToSupervisorMessage(ALeft, ATop, AWidth, AHeight: Integer): TModalResult;
    function HybridDialogMessage(ALeft, ATop, AWidth, AHeight: Integer): TModalResult;
    function HybridDialogMessageOKCancel(ALeft, ATop, AWidth, AHeight: Integer): TModalResult;
    function HybridJobCheck(ANextIDCard: TScannedIDCard): Boolean;
    procedure HybridJobFilter(AOdsJobcode: TOracleDataSet; ANextIDCard: TScannedIDCard);
    function HybridMode: Integer;
    function HybridWorkspotCheck(ALastIDCard: TScannedIDCard;
      ANextIDCard: TScannedIDCard;
      ACurrentNow: TDateTime;
      APrevJobCode: String): Boolean;
    function NewHybridMode(ANextIDCard: TScannedIDCard): Integer;
    property CloseCurrentScan: Boolean read FCloseCurrentScan write FCloseCurrentScan;
    property IsOccupied: Boolean read FIsOccupied write FIsOccupied;
  end;

var
  HybridDM: THybridDM;

implementation

{$R *.dfm}

uses
  UPimsConst, UPimsMessageRes, TimeRecordingDMT, UProductionScreenDefs;

{ THybridDM }

function THybridDM.HybridDialogGoToSupervisorMessage(ALeft, ATop, AWidth,
  AHeight: Integer): TModalResult;
begin
  try
    DialogMessageF := TDialogMessageF.Create(Self);
    DialogMessageF.TimerCloseEnabled := False;
    DialogMessageF.Caption := SPimsWorkspotOccupied;
    DialogMessageF.InitPosition(ALeft, ATop, AWidth, AHeight);
    DialogMessageF.BitBtnYes.Caption := SPimsOK;
    DialogMessageF.BitBtnNo.Caption := '';
    DialogMessageF.BitBtnCancel.Caption := '';
    DialogMessageF.Label1.Caption := SPimsWorkspotOccupiedGoToSupervisor;
    Result := DialogMessageF.ShowModal;
  finally
    DialogMessageF.Free;
  end;
end;

function THybridDM.HybridDialogMessageOKCancel(ALeft, ATop, AWidth, AHeight: Integer): TModalResult;
begin
  try
    DialogMessageF := TDialogMessageF.Create(Self);
    DialogMessageF.TimerCloseEnabled := False;
    DialogMessageF.Caption := SPimsWorkspotOccupied;
    DialogMessageF.InitPosition(ALeft, ATop, AWidth, AHeight);
    DialogMessageF.BitBtnYes.Caption := SPimsOK;
    DialogMessageF.BitBtnNo.Caption := '';
    DialogMessageF.BitBtnCancel.Caption := SPimsCancel;
    DialogMessageF.Label1.Caption :=
      Format(SPimsWorkspotOccupiedQuestion2,
        [LastIDCard.EmplName, LastIDCard.EmplName]);
    Result := DialogMessageF.ShowModal;
  finally
    DialogMessageF.Free;
  end;
end;

function THybridDM.HybridDialogMessage(ALeft, ATop, AWidth, AHeight: Integer): TModalResult;
begin
  try
    DialogMessageF := TDialogMessageF.Create(Self);
    DialogMessageF.TimerCloseEnabled := False;
    DialogMessageF.Caption := SPimsWorkspotOccupied;
    DialogMessageF.InitPosition(ALeft, ATop, AWidth, AHeight);
    DialogMessageF.BitBtnYes.Caption := SPimsYes;
    DialogMessageF.BitBtnNo.Caption := SPimsNo;
    DialogMessageF.BitBtnCancel.Caption := SPimsCancel;
    DialogMessageF.Label1.Caption :=
      Format(SPimsWorkspotOccupiedQuestion,
        [LastIDCard.EmplName, LastIDCard.EmplName]);
    Result := DialogMessageF.ShowModal;
  finally
    DialogMessageF.Free;
  end;
end;

function THybridDM.HybridJobCheck(ANextIDCard: TScannedIDCard): Boolean;
var
  MachineCode: String;
begin
  Result := False;
  if ORASystemDM.HybridMode = hmHybrid then
  begin
    if ANextIDCard.MeasureProductivity then
    begin
      Result := True;
      // Check Machine
      with oqCheckMachine do
      begin
        ClearVariables;
        SetVariable('PLANT_CODE',    ANextIDCard.PlantCode);
        SetVariable('WORKSPOT_CODE', ANextIDCard.WorkspotCode);
        Execute;
        if not Eof then
        begin
          MachineCode := FieldAsString('MACHINE_CODE');
          // Add missing job for machine
          TimeRecordingDM.CheckAddJobForMachine(
            ANextIDCard.PlantCode, MachineCode,
            ATWORKJOB, SPimsAtWorkDescription);
        end;
      end;
      // Add missing jobs
      TimeRecordingDM.CheckAddJobForWorkspot(
        ANextIDCard.PlantCode, ANextIDCard.WorkSpotCode,
        ATWORKJOB, SPimsAtWorkDescription);
      TimeRecordingDM.CheckAddJobForWorkspot(
        ANextIDCard.PlantCode, ANextIDCard.WorkSpotCode,
        OCCUPJOB, SPimsOccupiedDescription);
      TimeRecordingDM.CheckAddJobForWorkspot(
        ANextIDCard.PlantCode, ANextIDCard.WorkSpotCode,
        BREAKJOB, SPimsBreakJobDescription);
      TimeRecordingDM.CheckAddJobForWorkspot(
        ANextIDCard.PlantCode, ANextIDCard.WorkSpotCode,
        NOJOB, SPimsNoJobDescription);
    end; // if
  end; // if
end; // HybridJobCheck

procedure THybridDM.HybridJobFilter(AOdsJobcode: TOracleDataSet;
  ANextIDCard: TScannedIDCard);
var
  JobCode: String;
begin
  AOdsJobCode.Filtered := False;
  AOdsJobCode.Filter := '';
  if ORASystemDM.HybridMode = hmHybrid then
  begin
    if ANextIDCard.MeasureProductivity then
    begin
      JobCode := ATWORKJOB;
      if IsOccupied { and (not CloseCurrentScan) } then
        JobCode := OCCUPJOB;
      AOdsJobCode.Filtered := False;
      AOdsJobCode.Filter := 'JOB_CODE = ' + '''' + JobCode + '''';
      AOdsJobCode.Filtered := True;
    end; // if
  end; // if
end; // HybridJobFilter

function THybridDM.HybridMode: Integer;
begin
  if ORASystemDM.HybridMode = hmHybrid then
    Result := 1
  else
    Result := 0;
end; // HybridMode

// Here we check if another employee works on the workspot we want to scan-in.
// Result will be true when:
// - We found an employee already working on the workspot we want to scan-in.
// Notes:
// - It is still allowed to change job when previous job was not OCCUPJOB.
function THybridDM.HybridWorkspotCheck(
  ALastIDCard: TScannedIDCard;
  ANextIDCard: TScannedIDCard;
  ACurrentNow: TDateTime;
  APrevJobCode: String): Boolean;
var
  EmployeeNumber: Integer;
  EmployeeName: String;
begin
  Result := False;
  if ORASystemDM.HybridMode = hmHybrid then
  begin
    if ANextIDCard.MeasureProductivity then
    begin
      CopyIDCard(LastIDCard, ANextIDCard);
      if TimeRecordingDM.DeterminePreviousWorkSpot(ACurrentNow, ANextIDCard,
        ACurrentNow, False, True, EmployeeNumber, EmployeeName) then
      begin
        ANextIDCard.EmployeeCode := EmployeeNumber;
        ANextIDCard.EmplName := EmployeeName;
        // Was there a last scan found for same plant+workspot but different emp?
        if (LastIDCard.PlantCode = ANextIDCard.PlantCode) and
          (LastIDCard.WorkSpotCode = ANextIDCard.WorkSpotCode) and
          (LastIDCard.EmployeeCode <> ANextIDCard.EmployeeCode) then
        begin
          // Change job within same workspot?
          if (ALastIDCard.WorkSpotCode = ANextIDCard.WorkSpotCode) and
             (APrevJobCode <> '') and
             (APrevJobCode <> OCCUPJOB) then
            Result := False
          else
          begin
            CopyIDCard(LastIDCard, ANextIDCard);
            Result := True;
          end; // if
        end; // if
      end; // if
    end; // if
  end; // if
end; // HybridWorkspotCheck

function THybridDM.NewHybridMode(ANextIDCard: TScannedIDCard): Integer;
begin
  Result := 0;
  if (HybridMode = 1) and ANextIDCard.MeasureProductivity then
    Result := 1;
end;

end.
