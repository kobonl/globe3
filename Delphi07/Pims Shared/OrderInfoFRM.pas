(*
  MRA:24-MAR-2014 TD-24046
  - Related to this order.
  - Center this based on calling form, not on screen.
*)
unit OrderInfoFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  AboutFRM, ExtCtrls, FileCtrl, StdCtrls, ImgList;

type
  TOrderInfoForm = class(TAboutForm)
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  OrderInfoForm: TOrderInfoForm;

implementation

{$R *.DFM}
(*
{$IFDEF PIMSDUTCH}
{$R *NLD.DFM}
{$ELSE}
  {$IFDEF PIMSGERMAN}
  {$R *DEU.DFM}
  {$ELSE}
    {$IFDEF PIMSFRENCH}
    {$R *FRA.DFM}
    {$ELSE}
    {$R *.DFM}
    {$ENDIF}
  {$ENDIF}
{$ENDIF}
*)
procedure TOrderInfoForm.FormCreate(Sender: TObject);
var
  SearchRec : TSearchRec;
  LogoFileName: String;
  LogoBMP: TBitMap;
begin
  inherited;
  // find the Distributors Logo file
  // Get datapath for Logo file
  LogoFileName := ExtractFilePath(Application.ExeName);
  // Strip Bin\   add Data\
  LogoFileName := Copy(LogoFileName, 1, (Length(LogoFileName) - 4)) + 'Data\';
  if not (DirectoryExists(LogoFileName)) then
    ForceDirectories(LogoFileName);
  LogoFileName := LogoFileName + 'logo.bmp';

  if (FindFirst(LogoFileName, faAnyFile, SearchRec) = 0) then
  begin
    // create bitmap
    LogoBMP := TBitMap.Create;
    try
      LogoBMP.LoadFromFile(LogoFileName);
      // check fileformat
      if (LogoBMP.Width = 360) and (LogoBMP.Height = 270) then
      begin
        imgAbs.Picture.Assign(LogoBMP);
      end;
    finally
      LogoBMP.Free;
    end;
  end;

  // free memory for searchrecs
  FindClose(SearchRec);
end;

end.
