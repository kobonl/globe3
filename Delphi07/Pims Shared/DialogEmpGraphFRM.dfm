inherited DialogEmpGraphF: TDialogEmpGraphF
  Left = 257
  Top = 126
  Caption = 'Graph'
  Constraints.MinHeight = 480
  Constraints.MinWidth = 640
  Position = poOwnerFormCenter
  OnClose = FormClose
  OnKeyDown = FormKeyDown
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  inherited stbarBase: TStatusBar
    Panels = <
      item
        Style = psOwnerDraw
        Text = ' Gotli Labs'
        Width = 120
      end
      item
        Style = psOwnerDraw
        Width = 100
      end
      item
        Style = psOwnerDraw
        Width = 150
      end
      item
        Style = psOwnerDraw
        Width = 110
      end
      item
        Style = psOwnerDraw
        Width = 110
      end
      item
        Style = psOwnerDraw
        Width = 120
      end>
    SimplePanel = False
    OnDrawPanel = stbarBaseDrawPanel
  end
  inherited pnlInsertBase: TPanel
    Font.Color = clBlack
    Font.Height = -12
    ParentColor = True
    object Splitter1: TSplitter
      Left = 0
      Top = 145
      Width = 624
      Height = 3
      Cursor = crVSplit
      Align = alTop
    end
    object pnlAllTopInfo: TPanel
      Left = 0
      Top = 0
      Width = 624
      Height = 145
      Align = alTop
      ParentColor = True
      TabOrder = 0
      object pnlTopInfo: TPanel
        Left = 1
        Top = 1
        Width = 622
        Height = 37
        Align = alTop
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object pnlDateInput: TPanel
          Left = 0
          Top = 0
          Width = 622
          Height = 35
          Align = alTop
          BevelOuter = bvNone
          ParentColor = True
          TabOrder = 0
          object Panel1: TPanel
            Left = 546
            Top = 0
            Width = 76
            Height = 35
            Align = alRight
            BevelOuter = bvNone
            ParentColor = True
            TabOrder = 0
            object GroupBox3: TGroupBox
              Left = 0
              Top = 0
              Width = 76
              Height = 35
              Align = alClient
              Caption = 'Graph'
              TabOrder = 0
              object cBoxScale: TCheckBox
                Left = 6
                Top = 15
                Width = 54
                Height = 17
                Caption = 'Scale'
                TabOrder = 0
                OnClick = cBoxScaleClick
              end
            end
          end
          object GroupBox2: TGroupBox
            Left = 0
            Top = 0
            Width = 546
            Height = 35
            Align = alClient
            Caption = 'Workspots'
            TabOrder = 1
            DesignSize = (
              546
              35)
            object cBoxShowEmpl: TCheckBox
              Left = 410
              Top = 13
              Width = 127
              Height = 17
              Anchors = [akRight, akBottom]
              Caption = 'Show Workspots'
              TabOrder = 0
              OnClick = cBoxShowEmplClick
            end
          end
        end
      end
      object pnlEmployees: TPanel
        Left = 1
        Top = 38
        Width = 622
        Height = 106
        Align = alClient
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object dxList: TdxTreeList
          Left = 0
          Top = 0
          Width = 622
          Height = 106
          Bands = <
            item
              Caption = 'Workspots'
            end>
          DefaultLayout = True
          HeaderPanelRowCount = 1
          Align = alClient
          TabOrder = 0
          Options = [aoColumnSizing, aoColumnMoving, aoEditing, aoTabThrough, aoRowSelect, aoAutoWidth, aoAutoSort]
          TreeLineColor = clGrayText
          ShowBands = True
          ShowGrid = True
          ShowRoot = False
          object dxListColumnMACHINE: TdxTreeListColumn
            Caption = 'MACHINE'
            BandIndex = 0
            RowIndex = 0
          end
          object dxListColumnWORKSPOT: TdxTreeListColumn
            Caption = 'WORKSPOT'
            BandIndex = 0
            RowIndex = 0
          end
          object dxListColumnJOB: TdxTreeListColumn
            Caption = 'JOB'
            BandIndex = 0
            RowIndex = 0
          end
        end
      end
    end
    object pnlChart: TPanel
      Left = 0
      Top = 148
      Width = 624
      Height = 212
      Align = alClient
      BevelOuter = bvNone
      ParentColor = True
      TabOrder = 1
      object Chart1: TChart
        Left = 0
        Top = 0
        Width = 624
        Height = 179
        AllowPanning = pmHorizontal
        AllowZoom = False
        BackWall.Brush.Color = clWhite
        BackWall.Brush.Style = bsClear
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clBlack
        Title.Font.Height = -12
        Title.Font.Name = 'Arial'
        Title.Font.Style = [fsBold]
        Title.Text.Strings = (
          'Production Output')
        OnClickSeries = Chart1ClickSeries
        Chart3DPercent = 10
        View3D = False
        View3DOptions.Elevation = 356
        OnAfterDraw = Chart1AfterDraw
        Align = alClient
        BevelWidth = 0
        ParentColor = True
        TabOrder = 0
        AutoSize = True
        object pnlInfo: TPanel
          Left = 476
          Top = 73
          Width = 148
          Height = 88
          BevelOuter = bvNone
          ParentColor = True
          TabOrder = 0
          object lblEfficiencyDescription: TLabel
            Left = 2
            Top = 4
            Width = 109
            Height = 13
            Caption = 'lblEfficiencyDescription'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object lblEfficiency: TLabel
            Left = 2
            Top = 20
            Width = 56
            Height = 13
            Caption = 'lblEfficiency'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object lblTotalProdDescription: TLabel
            Left = 2
            Top = 48
            Width = 78
            Height = 13
            Caption = 'Total Production'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object lblTotalProd: TLabel
            Left = 2
            Top = 64
            Width = 56
            Height = 13
            Caption = 'lblTotalProd'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
        end
        object Panel5: TPanel
          Left = 0
          Top = 0
          Width = 19
          Height = 179
          Align = alLeft
          BevelOuter = bvNone
          ParentColor = True
          TabOrder = 1
          object Panel6: TPanel
            Left = 0
            Top = 138
            Width = 19
            Height = 41
            Align = alBottom
            BevelOuter = bvNone
            ParentColor = True
            TabOrder = 0
            object lblEmpCnt: TLabel
              Left = 1
              Top = 10
              Width = 22
              Height = 23
              Caption = 'EC'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Height = -19
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
            end
          end
        end
        object Series2: TAreaSeries
          Marks.ArrowLength = 8
          Marks.Visible = False
          SeriesColor = clWhite
          ShowInLegend = False
          Title = 'Background'
          AreaBrush = bsClear
          AreaLinesPen.Color = clGray
          DrawArea = True
          LinePen.Color = clWhite
          LinePen.Visible = False
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          Pointer.Visible = False
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1.000000000000000000
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Y'
          YValues.Multiplier = 1.000000000000000000
          YValues.Order = loNone
        end
        object Series4: TAreaSeries
          Marks.ArrowLength = 8
          Marks.Visible = False
          SeriesColor = 9925120
          Title = 'Nr of Empl. (x100)'
          DrawArea = True
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          Pointer.Visible = False
          Stairs = True
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1.000000000000000000
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Y'
          YValues.Multiplier = 1.000000000000000000
          YValues.Order = loNone
        end
        object Series3: TLineSeries
          Marks.ArrowLength = 8
          Marks.Visible = False
          SeriesColor = 636154
          Title = 'Norm Prod. Level'
          LinePen.Width = 3
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          Pointer.Visible = False
          Stairs = True
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1.000000000000000000
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Y'
          YValues.Multiplier = 1.000000000000000000
          YValues.Order = loNone
        end
        object Series1: TLineSeries
          Marks.ArrowLength = 8
          Marks.Visible = False
          SeriesColor = 1649359
          Title = 'Output'
          Dark3D = False
          LinePen.Width = 3
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          Pointer.Visible = False
          Stairs = True
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1.000000000000000000
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Y'
          YValues.Multiplier = 1.000000000000000000
          YValues.Order = loNone
        end
      end
      object Panel2: TPanel
        Left = 0
        Top = 179
        Width = 624
        Height = 33
        Align = alBottom
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object Panel3: TPanel
          Left = 474
          Top = 0
          Width = 150
          Height = 33
          Align = alRight
          BevelOuter = bvNone
          ParentColor = True
          TabOrder = 0
          object btnCloseGraph: TButton
            Left = 32
            Top = 2
            Width = 114
            Height = 30
            Caption = 'Close Graph'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -17
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            OnClick = btnCloseGraphClick
          end
        end
        object Panel4: TPanel
          Left = 0
          Top = 0
          Width = 474
          Height = 33
          Align = alClient
          BevelOuter = bvNone
          ParentColor = True
          TabOrder = 1
          object lblHelp: TLabel
            Left = 64
            Top = 8
            Width = 35
            Height = 14
            Caption = 'lblHelp'
          end
          object sbtnHorScrollRight: TBitBtn
            Left = 34
            Top = 4
            Width = 25
            Height = 25
            TabOrder = 0
            OnClick = sbtnHorScrollRightClick
            Glyph.Data = {
              F6060000424DF606000000000000360000002800000018000000180000000100
              180000000000C0060000C40E0000C40E00000000000000000000FEFEFEFFFFFF
              FEFEFEFEFEFEFFFFFFFEFEFEFFFFFFFEFEFEFFFFFFFFFFFFFEFEFEFFFFFFFFFF
              FFFEFEFEFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFEFEFEFF
              FFFFFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFE
              FEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFE
              FEFEFEFEFEFEFEFEFEFEFFFFFFFFFFFFFCFCFCFFFFFFFFFFFFFFFFFFADADDFD3
              D3EEFFFFFFFFFFFFFFFFFFFCFCFCFFFFFFFFFFFFFCFCFCFFFFFFFCFCFCFFFFFF
              FFFFFFFCFCFCFFFFFFFCFCFCFFFFFFFFFFFFFEFEFEFFFFFFFEFEFEFEFEFEFFFF
              FFFEFEFE9999D72E2EABC6C6E8FFFFFFFEFEFEFFFFFFFFFFFFFEFEFEFFFFFFFF
              FFFFFEFEFEFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFEFEFEFFFFFFFEFEFEFEFEFE
              FCFCFCFCFCFCFEFEFEFEFEFE9999D62525A72B2BA9B6B6E2FCFCFCFCFCFCFCFC
              FCFEFEFEFCFCFCFEFEFEFCFCFCFCFCFCFEFEFEFCFCFCFEFEFEFCFCFCFEFEFEFE
              FEFEFFFFFFFFFFFFFCFCFCFFFFFFFFFFFFFFFFFF9999D62424A62424A62626A7
              A8A8DCFFFFFFFFFFFFFFFFFFFCFCFCFFFFFFFFFFFFFFFFFFFFFFFFFCFCFCFFFF
              FFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFCFCFCFCFCFCFCFCFCFCFCFC9999D625
              25A72424A72525A72525A79696D5FCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFC
              FCFCFCFCFCFCFCFCFCFCFCFCFEFEFEFFFFFFFEFEFEFEFEFEFCFCFCFEFEFEFEFE
              FEFEFEFE9999D62424A72525A62525A72525A62424A78282CDFBFBFDFCFCFCFE
              FEFEFCFCFCFEFEFEFEFEFEFCFCFCFEFEFEFCFCFCFEFEFEFEFEFEFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFF9999D72424A62525A62525A62424A62424A62424
              A67373C7F9F9FCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFEFEFEFFFFFFFCFCFCFCFCFCFFFFFFFEFEFE9999D72525A72525A72525A7
              2525A72525A72525A72525A76363C0F4F4FAFCFCFCFCFCFCFFFFFFFCFCFCFFFF
              FFFCFCFCFEFEFEFFFFFFFEFEFEFEFEFEFCFCFCFEFEFEFEFEFEFEFEFE9999D624
              24A62424A62424A62424A62424A62424A62525A62525A75454BAEDEDF7FEFEFE
              FEFEFEFCFCFCFEFEFEFEFEFEFEFEFEFEFEFEFFFFFFFFFFFFFCFCFCFCFCFCFCFC
              FCFCFCFC9999D62525A72424A72525A72525A72424A72525A72525A72525A725
              25A74949B6E7E7F5FCFCFCFCFCFCFCFCFCFCFCFCFFFFFFFFFFFFFEFEFEFFFFFF
              FCFCFCFEFEFEFFFFFFFEFEFE9999D72424A72525A62525A72525A62424A72424
              A72525A62525A72525A64949B6E7E7F5FFFFFFFCFCFCFFFFFFFCFCFCFEFEFEFF
              FFFFFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFE9999D62525A62525A62525A6
              2525A62424A62525A62525A62525A75656BBEEEEF8FEFEFEFEFEFEFEFEFEFEFE
              FEFEFEFEFEFEFEFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9999D724
              24A72525A62525A72525A72424A72424A72424A66464C1F6F6FBFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFCFCFCFCFCFCFCFC
              FCFCFCFC9999D62424A62525A62525A72424A62424A62424A67474C7FAFAFDFC
              FCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFEFEFEFFFFFFFEFEFEFEFEFE
              FCFCFCFCFCFCFCFCFCFCFCFC9999D62525A72424A72525A72525A72424A78686
              CEFCFCFDFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFEFEFEFE
              FEFEFFFFFFFFFFFFFCFCFCFFFFFFFFFFFFFFFFFF9999D72525A72525A62525A7
              2525A79797D6FCFCFCFFFFFFFCFCFCFFFFFFFFFFFFFCFCFCFFFFFFFCFCFCFFFF
              FFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFEFEFEFFFFFFFEFEFE9999D625
              25A62525A62626A7A9A9DCFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFEFEFEFFFFFF
              FFFFFFFEFEFEFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFF9999D72424A72B2BA9B9B9E3FEFEFEFFFFFFFFFFFFFEFEFEFFFFFFFF
              FFFFFEFEFEFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFEFEFEFFFFFFFEFEFEFEFEFE
              FCFCFCFCFCFCFCFCFCFCFCFC9999D63030ACC7C7E8FCFCFCFCFCFCFCFCFCFCFC
              FCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFEFEFEFE
              FEFEFEFEFEFFFFFFFCFCFCFCFCFCFCFCFCFCFCFCAEAEE0D3D3EDFCFCFCFCFCFC
              FCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFC
              FCFCFCFCFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE
              FEFEFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFEFEFEFFFFFF
              FFFFFFFEFEFEFFFFFFFFFFFFFEFEFEFFFFFFFEFEFEFEFEFEFEFEFEFEFEFEFFFF
              FFFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFE
              FEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFE}
          end
          object sbtnHorScrollLeft: TBitBtn
            Left = 3
            Top = 4
            Width = 25
            Height = 25
            TabOrder = 1
            OnClick = sbtnHorScrollLeftClick
            Glyph.Data = {
              F6060000424DF606000000000000360000002800000018000000180000000100
              180000000000C0060000C40E0000C40E00000000000000000000FEFEFEFFFFFF
              FEFEFEFEFEFEFFFFFFFEFEFEFFFFFFFEFEFEFFFFFFFFFFFFFEFEFEFFFFFFFFFF
              FFFEFEFEFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFEFEFEFF
              FFFFFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFE
              FEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFE
              FEFEFEFEFEFEFEFEFEFEFFFFFFFFFFFFFCFCFCFFFFFFFFFFFFFFFFFFFCFCFCFF
              FFFFFFFFFFFFFFFFFFFFFFFCFCFCFFFFFFFFFFFFFCFCFCFFFFFFD5D5EEACACDE
              FFFFFFFCFCFCFFFFFFFCFCFCFFFFFFFFFFFFFEFEFEFFFFFFFEFEFEFEFEFEFFFF
              FFFEFEFEFFFFFFFEFEFEFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFEFEFEFFFFFFC8
              C8E93030AC9797D5FFFFFFFEFEFEFFFFFFFFFFFFFEFEFEFFFFFFFEFEFEFEFEFE
              FCFCFCFCFCFCFEFEFEFEFEFEFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFC
              FCFEFEFEB8B8E22B2BA92525A79797D4FEFEFEFCFCFCFEFEFEFCFCFCFEFEFEFE
              FEFEFFFFFFFFFFFFFCFCFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFAAAADD2626A72424A62424A69898D4FFFFFFFCFCFCFFFF
              FFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFC
              FCFCFCFCFCFCFCFCFCFCFCFCFCFC9797D62525A72525A72525A72424A79898D4
              FCFCFCFCFCFCFCFCFCFCFCFCFEFEFEFFFFFFFEFEFEFEFEFEFCFCFCFEFEFEFEFE
              FEFEFEFEFCFCFCFEFEFEFEFEFEFEFEFEFCFCFD8585CE2424A72525A62525A725
              25A62424A79797D4FEFEFEFCFCFCFEFEFEFCFCFCFEFEFEFEFEFEFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFAFD7575C82424A62424
              A62525A62525A72525A62424A69797D5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFEFEFEFFFFFFFCFCFCFCFCFCFFFFFFFEFEFEFCFCFCFCFCFCF6F6FB6464C1
              2525A72525A72525A72525A72525A72525A72525A79898D5FFFFFFFCFCFCFFFF
              FFFCFCFCFEFEFEFFFFFFFEFEFEFEFEFEFCFCFCFEFEFEFEFEFEFEFEFEFEFEFEEE
              EEF85656BB2424A62424A62424A62424A62525A62525A72424A62424A69797D4
              FEFEFEFCFCFCFEFEFEFEFEFEFEFEFEFEFEFEFFFFFFFFFFFFFCFCFCFCFCFCFCFC
              FCFCFCFCE7E7F54949B62424A72525A72525A72424A72525A72525A72525A725
              25A72424A79898D4FCFCFCFCFCFCFCFCFCFCFCFCFFFFFFFFFFFFFEFEFEFFFFFF
              FCFCFCFEFEFEFFFFFFFEFEFEE9E9F64B4BB72525A62525A72525A62424A72424
              A72525A62525A72525A62424A79898D5FFFFFFFCFCFCFFFFFFFCFCFCFEFEFEFF
              FFFFFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEEEEEF85757BB2525A6
              2525A62424A62525A62525A62525A72525A62424A69797D4FEFEFEFEFEFEFEFE
              FEFEFEFEFEFEFEFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFF6F6FB6666C22525A72424A72424A72424A62525A72424A62424A79898D5
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFCFCFCFCFCFCFCFC
              FCFCFCFCFCFCFCFCFCFCFCFCFCFAFAFD7676C82424A62424A62525A62525A725
              25A62424A69898D4FCFCFCFCFCFCFCFCFCFCFCFCFEFEFEFFFFFFFEFEFEFEFEFE
              FCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFD8888CF2525
              A72525A72525A72525A72424A79797D4FCFCFCFCFCFCFCFCFCFCFCFCFEFEFEFE
              FEFEFFFFFFFFFFFFFCFCFCFFFFFFFFFFFFFFFFFFFCFCFCFCFCFCFFFFFFFFFFFF
              FFFFFFFFFFFF9999D62525A72525A72525A62525A79898D5FFFFFFFCFCFCFFFF
              FFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFEFEFEFFFFFFFEFEFEFFFFFFFE
              FEFEFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFABABDD2626A72525A62424A69898D5
              FFFFFFFEFEFEFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFEFEFEBABAE42B
              2BA92424A79898D5FFFFFFFEFEFEFFFFFFFFFFFFFEFEFEFFFFFFFEFEFEFEFEFE
              FCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFC
              FCFCFCFCFCFCFCC9C9E93131AC9797D4FCFCFCFCFCFCFCFCFCFCFCFCFEFEFEFE
              FEFEFEFEFEFFFFFFFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFC
              FCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCD4D4EDADADDEFCFCFCFCFCFCFCFC
              FCFCFCFCFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE
              FEFEFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFEFEFEFFFFFFFFFFFFFEFEFEFFFFFF
              FFFFFFFEFEFEFFFFFFFFFFFFFEFEFEFFFFFFFEFEFEFEFEFEFEFEFEFEFEFEFFFF
              FFFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFE
              FEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFE}
          end
        end
      end
    end
  end
  inherited imgList16x16: TImageList
    Left = 216
    Top = 8
  end
  inherited imgNoYes: TImageList
    Left = 248
    Top = 8
  end
  inherited StandardMenuActionList: TActionList
    Left = 400
    Top = 8
    inherited FileSettingsAct: TAction
      OnExecute = FileSettingsActExecute
    end
    inherited FilePrintAct: TAction
      OnExecute = FilePrintActExecute
    end
    object FileExportAct: TAction
      Category = 'File'
      Caption = 'Export Chart'
      OnExecute = FileExportActExecute
    end
  end
  inherited mmPims: TMainMenu
    Left = 288
    Top = 8
    inherited miFile1: TMenuItem
      object Print1: TMenuItem [0]
        Action = FilePrintAct
        Visible = False
      end
      object ExportChart1: TMenuItem [1]
        Action = FileExportAct
        Visible = False
      end
      inherited Settings1: TMenuItem
        Visible = False
      end
    end
  end
  inherited StyleController: TdxEditStyleController
    Left = 328
    Top = 8
  end
  inherited StyleControllerRequired: TdxEditStyleController
    Left = 368
    Top = 8
  end
  object TimerProductionChart: TTimer
    Enabled = False
    Interval = 60000
    OnTimer = TimerProductionChartTimer
    Left = 144
    Top = 8
  end
  object SaveDialog1: TSaveDialog
    DefaultExt = 'txt'
    Filter = 'Export-Textfile|*.txt'
    Options = [ofOverwritePrompt, ofHideReadOnly, ofEnableSizing]
    Left = 568
    Top = 312
  end
  object TimerStatus: TTimer
    Enabled = False
    OnTimer = TimerStatusTimer
    Left = 176
    Top = 7
  end
  object TimerAutoClose: TTimer
    Enabled = False
    Interval = 10000
    OnTimer = TimerAutoCloseTimer
    Left = 224
    Top = 45
  end
end
