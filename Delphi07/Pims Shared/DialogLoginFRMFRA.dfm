inherited DialogLoginF: TDialogLoginF
  VertScrollBar.Range = 0
  BorderStyle = bsDialog
  Caption = 'Database Login'
  ClientHeight = 185
  ClientWidth = 319
  OldCreateOrder = True
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlBottom: TPanel
    Top = 125
    Width = 319
    TabOrder = 3
    inherited btnOk: TBitBtn
      Left = 120
      Width = 89
      OnClick = btnOkOnClick
    end
    inherited btnCancel: TBitBtn
      Left = 226
      Width = 87
    end
  end
  inherited stbarBase: TStatusBar
    Top = 166
    Width = 319
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 32
    Width = 313
    Height = 89
    TabOrder = 1
    object Label2: TLabel
      Left = 24
      Top = 60
      Width = 46
      Height = 13
      Caption = '&Password'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label1: TLabel
      Left = 24
      Top = 24
      Width = 51
      Height = 13
      Caption = '&User name'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object EditPassword: TEdit
      Left = 112
      Top = 52
      Width = 177
      Height = 21
      MaxLength = 31
      PasswordChar = '*'
      TabOrder = 1
    end
    object EditUserName: TEdit
      Left = 112
      Top = 24
      Width = 177
      Height = 21
      CharCase = ecUpperCase
      MaxLength = 31
      TabOrder = 0
    end
  end
  object GroupBox2: TGroupBox
    Left = 0
    Top = 0
    Width = 313
    Height = 33
    TabOrder = 0
    object Label3: TLabel
      Left = 24
      Top = 12
      Width = 107
      Height = 13
      Caption = 'Database:            Pims'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
  end
end
