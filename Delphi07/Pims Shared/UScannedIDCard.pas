(*
  Changes:
    MRA:25-FEB-2010. RV055.1.
    - Added 4 fields to IDCard:
      - ContractgroupCode
      - ExceptionalBeforeOvertime
      - RoundTruncSalaryHours
      - RoundMinute
    MRA:24-NOV-2010. RV073.3. (D5:RV080.6.)
    - Added 1 field to IDCard:
      - WSHourtypeNumber
    MRA:1-OKT-2012 20013489 Shift-Track
    - Added 1 field to IDCard for Shift-Tracking:
      - ShiftDate
    - Addition of OvernightShift-field to IDCard, to know if it
      is about an overnight shift.
    MRA:1-NOV-2012 20013489
    - OvernightShift-boolean is not needed.
    MRA:21-MAY-2013 20014050 Automated Export Easy Labor
    - Addition of Processed for IDCard-record.
    MRA:13-NOV-2013 TD-23386
    - Hourtype on shift should not result in double hours.
    MRA:17-MAR-2015 20015346
    - Store scans in seconds:
      - Store original scans in 2 extra variable in TScannedIDCard
    MRA:3-JUL-2015 20014450.50 / PIM12
    - Real Time Efficiency
    - End-Of-Day-button blink at end-of-shift
    MRA:14-MAR-2016 PIM-12
    - Addition of NormProdLevel-field
    MRA:19-MAY-2016 PIM-179
    - For Hybrid-mode: Add MeasureProductivity-boolean to IDCard.
    MRA:23-JAN-2018 PIM-346
    - Add IDCardToStr-function to get the contents of ID-card back as string.
*)
unit UScannedIDCard;

interface

uses
  DB, Oracle, SysUtils;

const
  NullStr  = '';
  NullInt  = 0;
  NullDate = 0;

type
  TScannedIDCard = record
    EmployeeCode, ShiftNumber,
    InscanEarly, InscanLate,
    OutScanEarly, OutScanLate: Integer;
    EmplName, IDCard, WorkSpotCode, WorkSpot,
    JobCode, Job, PlantCode, Plant, DepartmentCode, CutOfTime: String;
    DateIn, DateOut, DateInCutOff, DateOutCutOff: TDateTime;
    FromTimeRecordingApp: Boolean; // MRA:07-MAR-2008 RV004.
    IgnoreForOvertimeYN: String; // MRA:17-SEP-2009 RV033. Bugfix 1.
    ContractgroupCode: String; // RV055.1.
    ExceptionalBeforeOvertime: String; // RV055.1.
    RoundTruncSalaryHours: Integer; // RV055.1.
    RoundMinute: Integer; // RV055.1.
    WSHourtypeNumber: Integer; // RV073.3./D5:RV080.6.
    ShiftHourtypeNumber: Integer; // TD-23386
    ShiftDate: TDateTime; // 20013489
    Processed: Boolean; // 20014050
    DateInOriginal, DateOutOriginal: TDateTime; // 20015346
    ShiftStartDateTime, ShiftEndDateTime: TDateTime; // 20014450.50
    NormProdLevel: Double; // PIM-12
    MeasureProductivity: Boolean; // PIM-179
  end;

procedure CopyIDCard(var ADestIDCard: TScannedIDCard;
  const ASourceIDCard: TScannedIDCard);
procedure EmptyIDCard(var AIDCard : TScannedIDCard);
procedure PopulateIDCard(var IDCard: TScannedIDCard; ADataSet: TDataSet);
function IDCardToStr(IDCard: TScannedIDCard): String;

implementation

uses GlobalDMT;

procedure CopyIDCard(var ADestIDCard: TScannedIDCard;
  const ASourceIDCard: TScannedIDCard);
begin
  ADestIDCard.EmployeeCode   := ASourceIDCard.EmployeeCode;
  ADestIDCard.IDCard         := ASourceIDCard.IDCard;
  ADestIDCard.ShiftNumber    := ASourceIDCard.ShiftNumber;
  ADestIDCard.InscanEarly    := ASourceIDCard.InscanEarly;
  ADestIDCard.InscanLate     := ASourceIDCard.InscanLate;
  ADestIDCard.OutscanEarly   := ASourceIDCard.OutscanEarly;
  ADestIDCard.OutScanLate    := ASourceIDCard.OutScanLate;
  ADestIDCard.WorkSpotCode   := ASourceIDCard.WorkSpotCode;
  ADestIDCard.WorkSpot       := ASourceIDCard.WorkSpot;
  ADestIDCard.JobCode        := ASourceIDCard.JobCode;
  ADestIDCard.Job            := ASourceIDCard.Job;
  ADestIDCard.DateIn         := ASourceIDCard.DateIn;
  ADestIDCard.DateOut        := ASourceIDCard.DateOut;
  ADestIDCard.PlantCode      := ASourceIDCard.PlantCode;
  ADestIDCard.Plant          := ASourceIDCard.Plant;
  ADestIDCard.DepartmentCode := ASourceIDCard.DepartmentCode;
  ADestIDCard.EmplName       := ASourceIDCard.EmplName;
  ADestIDCard.Plant          := ASourceIDCard.Plant;
  ADestIDCard.DateInCutOff   := ASourceIDCard.DateInCutOff;
  ADestIDCard.DateOutCutOff  := ASourceIDCard.DateOutCutOff;
  ADestIDCard.ContractgroupCode         := ASourceIDCard.ContractgroupCode; // RV055.1.
  ADestIDCard.ExceptionalBeforeOvertime := ASourceIDCard.ExceptionalBeforeOvertime; // RV055.1.
  ADestIDCard.RoundTruncSalaryHours     := ASourceIDCard.RoundTruncSalaryHours; // RV055.1.
  ADestIDCard.RoundMinute               := ASourceIDCard.RoundMinute; // RV055.1.
  // MRA:07-MAR-2008
  ADestIDCard.FromTimeRecordingApp := ASourceIDCard.FromTimeRecordingApp;
  // MRA:17-SEP-2009 RV033.1.
  ADestIDCard.IgnoreForOvertimeYN := ASourceIDCard.IgnoreForOvertimeYN;
  // RV073.3./D5:RV080.6.
  ADestIDCard.WSHourtypeNumber := ASourceIDCard.WSHourtypeNumber;
  // TD-23386
  ADestIDCard.ShiftHourtypeNumber := ASourceIDCard.ShiftHourtypeNumber;
  // 20013489
  ADestIDCard.ShiftDate := ASourceIDCard.ShiftDate;
//  ADestIDCard.OvernightShift := ASourceIDCard.OvernightShift; // 20013489
  ADestIDCard.Processed := ASourceIDCard.Processed; // 20014050
  ADestIDCard.DateInOriginal := ASourceIDCard.DateInOriginal; // 20015346
  ADestIDCard.DateOutOriginal := ASourceIDCard.DateOutOriginal; // 20015346
  ADestIDCard.ShiftStartDateTime := ASourceIDCard.ShiftStartDateTime; // 20014450.50
  ADestIDCard.ShiftEndDateTime := ASourceIDCard.ShiftEndDateTime; // 20014450.50
  ADestIDCard.NormProdLevel := ASourceIDCard.NormProdLevel; // PIM-12
  ADestIDCard.MeasureProductivity := ASourceIDCard.MeasureProductivity; // PIM-179
end;

procedure EmptyIDCard(var AIDCard : TScannedIDCard);
begin
  AIDCard.EmployeeCode   := NullInt;
  AIDCard.IDCard         := NullStr;
  AIDCard.ShiftNumber    := NullInt;
  AIDCard.InscanEarly    := NullInt;
  AIDCard.InscanLate     := NullInt;
  AIDCard.OutscanEarly   := NullInt;
  AIDCard.OutScanLate    := NullInt;
  AIDCard.WorkSpotCode   := NullStr;
  AIDCard.WorkSpot       := NullStr;
  AIDCard.JobCode        := NullStr;
  AIDCard.Job            := NullStr;
  AIDCard.DateIn         := NullDate;
  AIDCard.DateOut        := NullDate;
  AIDCard.PlantCode      := NullStr;
  AIDCard.Plant          := NullStr;
  AIDCard.DepartmentCode := NullStr;
  AIDCard.EmplName       := NullStr;
  AIDCard.Plant          := NullStr;
  AIDCard.DateInCutOff   := NullDate;
  AIDCard.DateOutCutOff  := NullDate;
  AIDCard.ContractgroupCode         := NullStr; // RV055.1.
  AIDCard.ExceptionalBeforeOvertime := NullStr; // RV055.1.
  AIDCard.RoundTruncSalaryHours     := NullInt; // RV055.1.
  AIDCard.RoundMinute               := NullInt; // RV055.1.
  // MR:07-MAR-2008 RV004.
  // 20013489 This must be TRUE when called from PersonalScreen/Timerecording!
  AIDCard.FromTimeRecordingApp := True;
  // MRA:17-SEP-2009 RV033.1.
  AIDCard.IgnoreForOvertimeYN := NullStr;
  // RV073.3./D5:RV080.6.
  AIDCard.WSHourtypeNumber := -1;
  // TD-23386
  AIDCard.ShiftHourtypeNumber := -1;
  // 20013489
  AIDCard.ShiftDate := NullDate;
//  AIDCard.OvernightShift := False; // 20013489
  AIDCard.Processed := False; // 20014050
  AIDCard.DateInOriginal := NullDate; // 20015346
  AIDCard.DateOutOriginal := NullDate; // 20015346
  AIDCard.ShiftStartDateTime := NullDate; // 20014450.50
  AIDCard.ShiftEndDateTime := NullDate; // 20014450.50
  AIDCard.NormProdLevel := 0; // PIM-12
  AIDCard.MeasureProductivity := False; // PIM-179
end;

procedure PopulateIDCard(var IDCard: TScannedIDCard;
  ADataSet: TDataSet);
begin
  GlobalDM.PopulateIDCard(IDCard, ADataSet);
end;

function IDCardToStr(IDCard: TScannedIDCard): String;
var
  DateIn: String;
  DateOut: String;
begin
  Result := '';
  try
    DateIn := DateTimeToStr(IDCard.DateIn);
  except
    DateIn := '';
  end;
  try
    if IDCard.DateOut = 0 then
      DateOut := ''
    else
      DateOut := DateTimeToStr(IDCard.DateOut);
  except
    DateOut := '';
  end;
  try
    Result := Format('E=%d P=%s W=%s J=%s In=%s Out=%s', [IDCard.EmployeeCode,
      IDCard.PlantCode, IDCard.WorkSpotCode, IDCard.JobCode, DateIn, DateOut]
      );
  except
    Result := '???';
  end;
end;

end.
