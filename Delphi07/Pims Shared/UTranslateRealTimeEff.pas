(*
  MRA:2-JUN-2015 20014450.50 Part 2
  - Real Time Efficiency
*)
unit UTranslateRealTimeEff;

interface

type
  TTranslateHandlerRealTimeEff = class
  private
  public
    constructor Create; overload;
    destructor Destroy; override;
    procedure TranslateAll;
    procedure TranslateInit;
  end;

var
  ATranslateHandlerRealTimeEff: TTranslateHandlerRealTimeEff;

implementation

uses
  UTranslateStringsRealTimeEff, DialogEmpListViewFRM, DialogEmpGraphFRM;

{ TTranslateHandlerRealTimeEff }

constructor TTranslateHandlerRealTimeEff.Create;
begin
//
end;

destructor TTranslateHandlerRealTimeEff.Destroy;
begin
//
  inherited;
end;

procedure TTranslateHandlerRealTimeEff.TranslateAll;
begin
  if Assigned(DialogEmpListViewF) then
    with DialogEmpListViewF do
    begin
      //
    end;
  if Assigned(DialogEmpGraphF) then
    with DialogEmpGraphF do
    begin
      //
    end;
end;

procedure TTranslateHandlerRealTimeEff.TranslateInit;
begin
//
end;

initialization
  { Initialization section goes here }
  ATranslateHandlerRealTimeEff := TTranslateHandlerRealTimeEff.Create;

finalization
  { Finalization section goes here }
  ATranslateHandlerRealTimeEff.Free;

end.
