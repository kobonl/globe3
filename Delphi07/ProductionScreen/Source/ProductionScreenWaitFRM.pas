(*
  MRA:28-MAY-2013 20014289 New look Pims
  - Some pictures/colors are changed.
*)
unit ProductionScreenWaitFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls;

type
  TProductionScreenWaitF = class(TForm)
    lblMessage: TLabel;
    lblInfo: TLabel;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ProductionScreenWaitF: TProductionScreenWaitF;

implementation

uses
  UPimsConst;

{$R *.DFM}

procedure TProductionScreenWaitF.FormCreate(Sender: TObject);
begin
//  Color := clPimsLBlue; // 20014289
  lblInfo.Caption := '';
end;

end.
