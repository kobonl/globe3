(*
  MRA:JUN-2010. RV063.4. Order 550478. Personal Screen.
  - Addition of Machine/Workspot + Time recording.
*)

unit PersonalScreenDMT;

interface

uses
  SysUtils, Classes, UProductionScreenDefs;

type
//  TProdScreenType = (pstNoTimeRec, pstMachineTimeRec, pstWorkspotTimeRec);
  TMachineType = (mtNone, mtWorkspot, mtMachine);

const
  MECHANICAL_DOWN_JOB='TDOWN';
  NO_MERCHANDIZE_JOB='PDOWN';

type
  PTMachineWorkspot = ^TMachineWorkspot;
  TMachineWorkspot = record
    APlantCode: String;
    AMachineCode: String;
    AWorkspotCode: String;
    ATag: Integer;
  end;

type
  TPersonalScreenDM = class(TDataModule)
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    MachineWorkspotList: TList;
    procedure ClearMachineWorkspotList;
    function MachineWorkspotExists(APlantCode, AMachineCode,
      AWorkspotCode: String): Boolean;
    procedure AddMachineWorkspotList(APlantCode, AMachineCode,
      AWorkspotCode: String; ATag: Integer);
  end;

var
  PersonalScreenDM: TPersonalScreenDM;

implementation

{$R *.dfm}

procedure TPersonalScreenDM.DataModuleCreate(Sender: TObject);
begin
  MachineWorkspotList := TList.Create;
end;

procedure TPersonalScreenDM.ClearMachineWorkspotList;
var
  I: Integer;
  AMachineWorkspot: PTMachineWorkspot;
begin
  for I := MachineWorkspotList.Count - 1 downto 0 do
  begin
    AMachineWorkspot := MachineWorkspotList.Items[I];
    Dispose(AMachineWorkspot);
    MachineWorkspotList.Remove(AMachineWorkspot);
  end;
end;

function TPersonalScreenDM.MachineWorkspotExists(APlantCode,
  AMachineCode, AWorkspotCode: String): Boolean;
var
  I: Integer;
  AMachineWorkspot: PTMachineWorkspot;
begin
  Result := False;
  for I := 0 to MachineWorkspotList.Count - 1 do
  begin
    AMachineWorkspot := MachineWorkspotList.Items[I];
    if (AMachineWorkspot.APlantCode = APlantCode) and
      (AMachineWorkspot.AMachineCode = AMachineCode) and
      (AMachineWorkspot.AWorkspotCode = AWorkspotCode) then
    begin
      Result := True;
      Break;
    end;
  end;

end;

procedure TPersonalScreenDM.AddMachineWorkspotList(APlantCode,
  AMachineCode, AWorkspotCode: String; ATag: Integer);
var
  AMachineWorkspot: PTMachineWorkspot;
begin
  new(AMachineWorkspot);
  AMachineWorkspot.APlantCode := APlantCode;
  AMachineWorkspot.AMachineCode := AMachineCode;
  AMachineWorkspot.AWorkspotCode := AWorkspotCode;
  AMachineWorkspot.ATag := ATag;
  MachineWorkspotList.Add(AMachineWorkspot);
end;

procedure TPersonalScreenDM.DataModuleDestroy(Sender: TObject);
begin
  ClearMachineWorkspotList;
  MachineWorkspotList.Free;
end;

end.
