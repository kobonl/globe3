(*
  Changes:
    RV043.1. Auto login option.
    - When argument is used like -u:pims then
      it should start with login-name e.g. 'pims', and get the password
      automatically from database.
    MRA:JUN-2010. RV063.4. Order 550478. Personal Screen.
    - Addition of Machine/Workspot + Time recording.
    MRA:21-JUN-2012 20013176
    - Timerecording & card reader:
      - This asks for an employee-number when the card-ID
        is not known yet.
      - The above must default NOT be done, but only with a parameter
        '-A' it must ask for the employee-number.
    MRA:11-MAR-2014 TD-24046
    - Translate Issues
    MRA:5-MAY-2014 TD-24852 Related to this todo
    - Removed part about Personal Screen
    MRA:13-APR-2015 20016449
    - Related to this order: Add ClientName parameter
*)

program ProductionScreenGR;

uses
  Forms,
  Windows,
  SysUtils,
  Dialogs,
  BasePimsFRM in '..\..\Pims Shared\BasePimsFRM.pas' {BasePimsForm},
  BaseTopMenuFRM in '..\..\Pims Shared\BaseTopMenuFRM.pas' {BaseTopMenuForm},
  BaseTopLogoFRM in '..\..\Pims Shared\BaseTopLogoFRM.pas' {BaseTopLogoForm},
  BaseDialogFRM in '..\..\Pims Shared\BaseDialogFRM.pas' {BaseDialogForm},
  AboutFRM in '..\..\Pims Shared\AboutFRM.pas' {AboutForm},
  OrderInfoFRM in '..\..\Pims Shared\OrderInfoFRM.pas' {OrderInfoForm},
  UPimsConst in '..\..\Pims Shared\UPimsConst.pas',
  UPimsMessageRes in '..\..\Pims Shared\UPimsMessageRes.pas',
  UPimsVersion in '..\..\Pims Shared\UPimsVersion.pas',
  UScannedIDCard in '..\..\Pims Shared\UScannedIDCard.pas',
  SplashFRM in '..\..\Pims Shared\SplashFRM.pas' {SplashForm},
  ORASystemDMT in '..\..\Pims Shared\ORASystemDMT.pas' {ORASystemDM: TDataModule},
  CalculateTotalHoursDMT in '..\..\Pims Shared\CalculateTotalHoursDMT.pas' {CalculateTotalHoursDM, TDataModule},
  UGlobalFunctions in '..\..\Pims Shared\UGlobalFunctions.pas',
  DialogSelectionFRM in '..\..\Pims Shared\DialogSelectionFRM.PAS' {DialogSelectionForm},
  DialogLoginDMT in '..\..\Pims Shared\DialogLoginDMT.pas' {DialogLoginDM: TDataModule},
  DialogLoginFRM in '..\..\Pims Shared\DialogLoginFRM.pas' {DialogLoginF},
  ULoginConst in '..\..\Pims Shared\ULoginConst.pas',
  EncryptIt in '..\..\Pims Shared\EncryptIt.pas',
  GlobalDMT in '..\..\Pims Shared\GlobalDMT.pas' {GlobalDM: TDataModule},
  HomeFRM in 'HomeFRM.pas' {HomeF},
  DialogImagePropertiesFRM in 'DialogImagePropertiesFRM.pas' {DialogImagePropertiesF},
  DialogWorkspotSelectFRM in 'DialogWorkspotSelectFRM.pas' {DialogWorkspotSelectF},
  DialogShowAllEmployeesFRM in '..\..\Pims Shared\DialogShowAllEmployeesFRM.pas' {DialogShowAllEmployeesF},
  ShowChartFRM in 'ShowChartFRM.pas' {ShowChartF},
  DialogPrintChartFRM in 'DialogPrintChartFRM.pas' {DialogPrintChartF},
  DialogSettingsFRM in 'DialogSettingsFRM.pas' {DialogSettingsF},
  DialogChartSettingsFRM in 'DialogChartSettingsFRM.pas' {DialogChartSettingsF},
  ProductionScreenDMT in 'ProductionScreenDMT.pas' {ProductionScreenDM: TDataModule},
  ProductionScreenWaitFRM in 'ProductionScreenWaitFRM.pas' {ProductionScreenWaitF},
  DialogDepartmentSelectFRM in 'DialogDepartmentSelectFRM.pas' {DialogDepartmentSelectF},
  PlanScanEmpDMT in '..\..\Pims Shared\PlanScanEmpDMT.pas' {PlanScanEmpDM: TDataModule},
  DialogSelectModeFRM in 'DialogSelectModeFRM.pas' {DialogSelectModeF},
  DialogLegendaFRM in 'DialogLegendaFRM.pas' {DialogLegendaF},
  BasePimsSerialFRM in '..\..\Pims Shared\BasePimsSerialFRM.pas' {BasePimsSerialForm},
  UProductionScreenDefs in '..\..\Pims Shared\UProductionScreenDefs.pas',
  UPaintPanel in 'UPaintPanel.pas',
  UTranslateStringsProdScreen in 'UTranslateStringsProdScreen.pas',
  UTranslateProdScreen in 'UTranslateProdScreen.pas',
  UTimeZone in '..\..\Pims Shared\UTimeZone.pas',
  RealTimeEffDMT in '..\..\Pims Shared\RealTimeEffDMT.pas' {RealTimeEffDM: TDataModule},
  DialogEmpListViewFRM in '..\..\Pims Shared\DialogEmpListViewFRM.pas' {DialogEmpListViewF},
  DialogEmpGraphFRM in '..\..\Pims Shared\DialogEmpGraphFRM.pas' {DialogEmpGraphF},
  UTranslateRealTimeEff in '..\..\Pims Shared\UTranslateRealTimeEff.pas',
  UTranslateStringsRealTimeEff in '..\..\Pims Shared\UTranslateStringsRealTimeEff.pas',
  ColorButton in '..\..\Pims Shared\ColorButton.pas',
  UTranslateStringsShared in '..\..\Pims Shared\UTranslateStringsShared.pas',
  UWorkspotEmpEff in '..\..\Pims Shared\UWorkspotEmpEff.pas',
  WorkspotEmpEffDMT in '..\..\Pims Shared\WorkspotEmpEffDMT.pas' {WorkspotEmpEffDM: TDataModule};

{$R *.RES}

// How to get an alternative CLIENTNAME based on param?
function NewClientName: String;
var
  I, IPos: Integer;
  Param1, PStr: String;
  function MySetEnvironmentVariable(AName, AValue: String): Boolean;
  begin
    Result := SetEnvironmentVariable(PChar(AName), PChar(AValue));
  end;
begin
  Result := '';
  Param1 := '-C:';
  for I := 1 to ParamCount do
  begin
    PStr := UpperCase(ParamStr(I));
    IPos := Pos(Param1, PStr);
    if IPos > 0 then
    begin
      Result := Copy(PStr, IPos + Length(Param1), Length(Pstr));
      Break;
    end;
  end;
  if Result <> '' then
    MySetEnvironmentVariable('CLIENTNAME', Result);
end;

// RV043.1. Auto login option. get the user from parameter, for example:
//          -u:pims
function AutoLoginUser: String;
var
  I, IPos: Integer;
  Param1, PStr: String;
begin
  Result := '';
  Param1 := '-U:';
  for I := 1 to ParamCount do
  begin
    PStr := UpperCase(ParamStr(I));
    IPos := Pos(Param1, PStr);
    if IPos > 0 then
    begin
      Result := Copy(PStr, IPos + Length(Param1), Length(Pstr));
      Break;
    end;
  end;
end;

begin
  Application.Initialize;
  // set property in order to not update database before login dialog
  // when SystemDM is created
  Application.Title := 'ORCL - PIMS - PRODUCTION SCREEN';
  NewClientName;
  Application.CreateForm(TORASystemDM, ORASystemDM);
  ORASystemDM.AppName := APPNAME_PRS;
  if not Application.Terminated then
  begin
    DialogLoginF := TDialogLoginF.Create(Application);
    ATranslateHandlerProdScreen.TranslateInit; // TD-24046
    // RV043.1. Auto Login.
    if (AutoLoginUser <> '') then
    begin
      DialogLoginF.AutoLogin := True;
      DialogLoginF.AutoLoginUser := AutoLoginUser;
    end
    else
    begin
      DialogLoginF.AutoLogin := False;
      DialogLoginF.AutoLoginUser := '';
    end;
    //verifyPassword and login as SYSDBA
    if DialogLoginF.VerifyPassword then
    begin
      //update database as PIMS user
      if ORASystemDM.UpdatePimsBase('Pims') then
      begin
        Application.CreateForm(TRealTimeEffDM, RealTimeEffDM);
        Application.CreateForm(TWorkspotEmpEffDM, WorkspotEmpEffDM);
        Application.CreateForm(TCalculateTotalHoursDM, CalculateTotalHoursDM);
        Application.CreateForm(TGlobalDM, GlobalDM);
        Application.CreateForm(TProductionScreenDM, ProductionScreenDM);
        Application.CreateForm(TPlanScanEmpDM, PlanScanEmpDM);
        Application.CreateForm(THomeF, HomeF);
        Application.CreateForm(TProductionScreenWaitF, ProductionScreenWaitF);
        // Start - Open all sub-dialogs here! TD-24046
        Application.CreateForm(TDialogSettingsF, DialogSettingsF);
        DialogSettingsF.Visible := False;
        Application.CreateForm(TShowChartF, ShowChartF);
        ShowChartF.Visible := False;
        Application.CreateForm(TDialogChartSettingsF, DialogChartSettingsF);
        DialogChartSettingsF.Visible := False;
        Application.CreateForm(TDialogLegendaF, DialogLegendaF);
        DialogLegendaF.Visible := False;
        Application.CreateForm(TDialogPrintChartF, DialogPrintChartF);
        DialogPrintChartF.Visible := False;
        Application.CreateForm(TDialogDepartmentSelectF, DialogDepartmentSelectF);
        DialogDepartmentSelectF.Visible := False;
        Application.CreateForm(TDialogWorkspotSelectF, DialogWorkspotSelectF);
        DialogWorkspotSelectF.Visible := False;
        Application.CreateForm(TDialogShowAllEmployeesF, DialogShowAllEmployeesF);
        DialogShowAllEmployeesF.Visible := False;
        Application.CreateForm(TDialogSelectModeF, DialogSelectModeF);
        DialogSelectModeF.Visible := False;
        Application.CreateForm(TDialogEmpListViewF, DialogEmpListViewF);
        Application.CreateForm(TDialogEmpGraphF, DialogEmpGraphF);
      end;
    end;
    ATranslateHandlerProdScreen.TranslateAll; // TD-24046
    ATranslateHandlerRealTimeEff.TranslateAll;
    Application.Run;
  end;
end.
