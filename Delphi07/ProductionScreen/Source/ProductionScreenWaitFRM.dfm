object ProductionScreenWaitF: TProductionScreenWaitF
  Left = 440
  Top = 301
  BorderIcons = []
  BorderStyle = bsSingle
  Caption = 'PIMS Production Screen'
  ClientHeight = 87
  ClientWidth = 563
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  Visible = True
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object lblMessage: TLabel
    Left = 8
    Top = 14
    Width = 96
    Height = 24
    Caption = 'lblMessage'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object lblInfo: TLabel
    Left = 8
    Top = 48
    Width = 28
    Height = 13
    Caption = 'lblInfo'
  end
end
