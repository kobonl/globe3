unit DialogDepartmentSelectFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BaseDialogFRM, ActnList, StdCtrls, Buttons, ComCtrls, Variants,
  ExtCtrls, dxEdLib, dxCntner, dxEditor, dxExEdtr, Menus, StdActns, ImgList,
  jpeg;

type
  TDialogDepartmentSelectF = class(TBaseDialogForm)
    Label3: TLabel;
    cmbPlusPlant: TdxPickEdit;
    Label1: TLabel;
    cmbPlusDepartment: TdxPickEdit;
    procedure FormCreate(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure cmbPlusPlantChange(Sender: TObject);
  private
    { Private declarations }
    FPlantCode: String;
    FDepartmentCode: String;
    FDepartmentDescription: String;
  public
    { Public declarations }
    property PlantCode: String read FPlantCode;
    property DepartmentCode: String read FDepartmentCode;
    property DepartmentDescription: String read FDepartmentDescription;
  end;

var
  DialogDepartmentSelectF: TDialogDepartmentSelectF;

implementation

uses
  ORASystemDMT,
  ProductionScreenDMT;

{$R *.DFM}

procedure TDialogDepartmentSelectF.FormCreate(Sender: TObject);
begin
  inherited;
  lblMessage.Caption := Caption;
//  cmbPlusPlant.ShowSpeedButton := False;
//  cmbPlusPlant.ShowSpeedButton := True;
//  cmbPlusDepartment.ShowSpeedButton := False;
//  cmbPlusDepartment.ShowSpeedButton := True;
  ProductionScreenDM.FillComboBox(
    ProductionScreenDM.odsPlant, cmbPlusPlant, '', True,
      '', 'PLANT_CODE', 'DESCRIPTION');
  ProductionScreenDM.FillComboBox(
    ProductionScreenDM.odsDepartment, cmbPlusDepartment,
      GetStrValue(cmbPlusPlant.Text), True,
      'PLANT_CODE', 'DEPARTMENT_CODE', 'DESCRIPTION');
end;

procedure TDialogDepartmentSelectF.btnOkClick(Sender: TObject);
begin
  inherited;
  FPlantCode := GetStrValue(cmbPlusPlant.Text);
  FDepartmentCode := GetStrValue(cmbPlusDepartment.Text);

  // Now get description of department
  ProductionScreenDM.odsDepartment.Active := True;
  if ProductionScreenDM.odsDepartment.Locate('PLANT_CODE;DEPARTMENT_CODE',
    VarArrayOf([FPlantCode, FDepartmentCode]), []) then
    FDepartmentDescription :=
      ProductionScreenDM.odsDepartment.FieldByName('DESCRIPTION').Value;
  ProductionScreenDM.odsDepartment.Active := False;
end;

procedure TDialogDepartmentSelectF.cmbPlusPlantChange(Sender: TObject);
begin
  inherited;
  ProductionScreenDM.FillComboBox(
    ProductionScreenDM.odsDepartment, cmbPlusDepartment,
      GetStrValue(cmbPlusPlant.Text), True,
      'PLANT_CODE', 'DEPARTMENT_CODE', 'DESCRIPTION');
end;

end.
