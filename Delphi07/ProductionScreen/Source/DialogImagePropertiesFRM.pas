unit DialogImagePropertiesFRM;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls,
  Buttons, ExtCtrls;

type
  TDialogImagePropertiesF = class(TForm)
    OKBtn: TButton;
    CancelBtn: TButton;
    Bevel1: TBevel;
    Label1: TLabel;
    Label2: TLabel;
    edtLeft: TEdit;
    edtTop: TEdit;
    Label3: TLabel;
    Label4: TLabel;
    edtWidth: TEdit;
    edtHeight: TEdit;
    lblPlantCode: TLabel;
    lblWorkspotCode: TLabel;
    lblWorkspotDescription: TLabel;
    edtPlantCode: TEdit;
    edtWorkspotCode: TEdit;
    edtWorkspotDescription: TEdit;
    procedure OKBtnClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    FImageTop: Integer;
    FImageLeft: Integer;
    FImageWidth: Integer;
    FImageHeight: Integer;
    FPlantCode: String;
    FWorkspotCode: String;
    FWorkspotDescription: String;
    procedure SetImageLeft(const Value: Integer);
    procedure SetImageTop(const Value: Integer);
    procedure SetImageHeight(const Value: Integer);
    procedure SetImageWidth(const Value: Integer);
    procedure SetPlantCode(const Value: String);
    procedure SetWorkspotCode(const Value: String);
    procedure SetWorkspotDescription(const Value: String);
    { Private declarations }
  public
    { Public declarations }
    property ImageTop: Integer read FImageTop write SetImageTop;
    property ImageLeft: Integer read FImageLeft write SetImageLeft;
    property ImageWidth: Integer read FImageWidth write SetImageWidth;
    property ImageHeight: Integer read FImageHeight write SetImageHeight;
    property PlantCode: String write SetPlantCode;
    property WorkspotCode: String write SetWorkspotCode;
    property WorkspotDescription: String write SetWorkspotDescription;
  end;

var
  DialogImagePropertiesF: TDialogImagePropertiesF;

implementation

{ TOKBottomDlg }

{$R *.DFM}

procedure TDialogImagePropertiesF.SetImageLeft(const Value: Integer);
begin
  FImageLeft := Value;
end;

procedure TDialogImagePropertiesF.SetImageTop(const Value: Integer);
begin
  FImageTop := Value;
end;

procedure TDialogImagePropertiesF.FormActivate(Sender: TObject);
begin
  edtTop.Text := IntToStr(FImageTop);
  edtLeft.Text := IntToStr(FImageLeft);
  edtWidth.Text := IntToStr(FImageWidth);
  edtHeight.Text := IntToStr(FImageHeight);
  edtPlantCode.Text := FPlantCode;
  edtWorkspotCode.Text := FWorkspotCode;
  edtWorkspotDescription.Text := FWorkspotDescription;
  if FPlantCode = '' then
  begin
    lblPlantCode.Enabled := False;
    lblWorkspotCode.Enabled := False;
    lblWorkspotDescription.Enabled := False;
  end
end;

procedure TDialogImagePropertiesF.SetImageHeight(const Value: Integer);
begin
  FImageHeight := Value;
end;

procedure TDialogImagePropertiesF.SetImageWidth(const Value: Integer);
begin
  FImageWidth := Value;
end;

procedure TDialogImagePropertiesF.OKBtnClick(Sender: TObject);
begin
  FImageTop := StrToInt(edtTop.Text);
  FImageLeft := StrToInt(edtLeft.Text);
  FImageWidth := StrToInt(edtWidth.Text);
  FImageHeight := StrToInt(edtHeight.Text);
end;

procedure TDialogImagePropertiesF.SetPlantCode(const Value: String);
begin
  FPlantCode := Value;
end;

procedure TDialogImagePropertiesF.SetWorkspotCode(const Value: String);
begin
  FWorkspotCode := Value;
end;

procedure TDialogImagePropertiesF.SetWorkspotDescription(
  const Value: String);
begin
  FWorkspotDescription := Value;
end;

end.
