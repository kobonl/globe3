inherited DialogSettingsF: TDialogSettingsF
  Left = 506
  Top = 228
  VertScrollBar.Range = 0
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Settings'
  ClientHeight = 388
  ClientWidth = 478
  Position = poOwnerFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlImageBase: TPanel
    Width = 478
    TabOrder = 1
    inherited imgBasePims: TImage
      Left = 361
    end
  end
  inherited stbarBase: TStatusBar
    Top = 306
    Width = 478
  end
  inherited pnlInsertBase: TPanel
    Width = 478
    Height = 269
    Font.Color = clBlack
    Font.Height = -11
    object pnlTop: TPanel
      Left = 0
      Top = 0
      Width = 478
      Height = 135
      Align = alTop
      Caption = 'pnlTop'
      ParentColor = True
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 1
        Top = 1
        Width = 476
        Height = 133
        Align = alClient
        Caption = 'Global Settings'
        TabOrder = 0
        object GroupBox3: TGroupBox
          Left = 2
          Top = 66
          Width = 472
          Height = 65
          Align = alClient
          Caption = 'Sound Alarm'
          TabOrder = 0
          object Label1: TLabel
            Left = 8
            Top = 41
            Width = 75
            Height = 13
            Caption = 'Sound Filename'
          end
          object Label2: TLabel
            Left = 8
            Top = 17
            Width = 81
            Height = 13
            Caption = 'Use Sound Alarm'
          end
          object cBoxUseSoundAlarm: TCheckBox
            Left = 104
            Top = 15
            Width = 33
            Height = 17
            TabOrder = 0
          end
          object edtSoundFilename: TEdit
            Left = 104
            Top = 37
            Width = 273
            Height = 21
            Enabled = False
            TabOrder = 1
            Text = 'edtSoundFilename'
          end
          object btnBrowse: TButton
            Left = 386
            Top = 36
            Width = 75
            Height = 25
            Caption = 'Browse'
            TabOrder = 2
            OnClick = btnBrowseClick
          end
        end
        object Panel1: TPanel
          Left = 2
          Top = 15
          Width = 472
          Height = 51
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 1
          object GroupBox6: TGroupBox
            Left = 0
            Top = 0
            Width = 241
            Height = 51
            Align = alLeft
            Caption = 'Refresh Time Interval'
            TabOrder = 0
            object Label6: TLabel
              Left = 8
              Top = 22
              Width = 38
              Height = 13
              Caption = 'Interval'
            end
            object Label7: TLabel
              Left = 185
              Top = 22
              Width = 39
              Height = 13
              Caption = 'seconds'
            end
            object dxSpinEditRefreshTimeInterval: TdxSpinEdit
              Left = 104
              Top = 20
              Width = 73
              TabOrder = 0
              MaxValue = 5000.000000000000000000
              MinValue = 1.000000000000000000
              Value = 1.000000000000000000
              StoredValues = 48
            end
          end
          object GroupBox8: TGroupBox
            Left = 241
            Top = 0
            Width = 231
            Height = 51
            Align = alClient
            Caption = 'Efficiency Color Boundaries'
            TabOrder = 1
            object lblRed: TLabel
              Left = 8
              Top = 24
              Width = 19
              Height = 13
              Caption = 'Red'
            end
            object lblOrange: TLabel
              Left = 112
              Top = 24
              Width = 36
              Height = 13
              Caption = 'Orange'
            end
            object dxSpinEditRed: TdxSpinEdit
              Left = 56
              Top = 20
              Width = 49
              TabOrder = 0
              MaxValue = 100.000000000000000000
              MinValue = 30.000000000000000000
              Value = 85.000000000000000000
              StoredValues = 48
            end
            object dxSpinEditOrange: TdxSpinEdit
              Left = 168
              Top = 20
              Width = 49
              TabOrder = 1
              MaxValue = 130.000000000000000000
              MinValue = 30.000000000000000000
              Value = 105.000000000000000000
              StoredValues = 48
            end
          end
        end
      end
    end
    object pnlClient: TPanel
      Left = 0
      Top = 135
      Width = 478
      Height = 134
      Align = alClient
      Caption = 'pnlClient'
      ParentColor = True
      TabOrder = 1
      object GroupBox2: TGroupBox
        Left = 1
        Top = 1
        Width = 476
        Height = 132
        Align = alClient
        Caption = 'Scheme Settings'
        TabOrder = 0
        object GroupBox4: TGroupBox
          Left = 2
          Top = 15
          Width = 223
          Height = 54
          Align = alLeft
          Caption = 'Workspot Scale'
          TabOrder = 0
          object Label3: TLabel
            Left = 8
            Top = 20
            Width = 55
            Height = 13
            Caption = 'Percentage'
          end
          object Label9: TLabel
            Left = 8
            Top = 49
            Width = 79
            Height = 13
            Caption = 'Gridline Distance'
          end
          object dxSpinEditWorkspotScalePercentage: TdxSpinEdit
            Left = 104
            Top = 17
            Width = 73
            TabOrder = 0
            MaxValue = 250.000000000000000000
            MinValue = 1.000000000000000000
            Value = 50.000000000000000000
            StoredValues = 48
          end
          object dxSpinEditGridLineDistance: TdxSpinEdit
            Left = 104
            Top = 46
            Width = 73
            TabOrder = 1
            MaxValue = 100.000000000000000000
            MinValue = 5.000000000000000000
            Value = 50.000000000000000000
            StoredValues = 48
          end
        end
        object GroupBox5: TGroupBox
          Left = 2
          Top = 69
          Width = 472
          Height = 61
          Align = alBottom
          Caption = 'Efficiency Meters'
          TabOrder = 1
          object Label4: TLabel
            Left = 8
            Top = 24
            Width = 51
            Height = 13
            Caption = 'Use Period'
          end
          object rGrpUsePeriod: TRadioGroup
            Left = 99
            Top = 6
            Width = 206
            Height = 49
            Columns = 3
            Items.Strings = (
              'Current'
              'Since'
              'Shift')
            TabOrder = 0
            OnClick = rGrpUsePeriodClick
          end
          object grpBxSinceTime: TGroupBox
            Left = 500
            Top = 11
            Width = 165
            Height = 37
            Caption = 'Since'
            TabOrder = 1
            Visible = False
            object Label5: TLabel
              Left = 8
              Top = 14
              Width = 22
              Height = 13
              Caption = 'Time'
            end
            object dxTimeEditSinceTime: TdxTimeEdit
              Left = 79
              Top = 9
              Width = 81
              TabOrder = 0
              TimeEditFormat = tfHourMin
              StoredValues = 4
            end
          end
          object gBoxShowEmpInfo: TGroupBox
            Left = 306
            Top = 6
            Width = 163
            Height = 49
            Caption = 'Show'
            TabOrder = 2
            object cBoxShowEmpInfo: TCheckBox
              Left = 8
              Top = 12
              Width = 150
              Height = 17
              Caption = 'Employee Info'
              TabOrder = 0
              OnClick = cBoxShowEmpInfoClick
            end
            object cBoxShowEmpEff: TCheckBox
              Left = 8
              Top = 29
              Width = 153
              Height = 17
              Caption = 'Employee Efficiency'
              TabOrder = 1
            end
          end
        end
        object GroupBox7: TGroupBox
          Left = 225
          Top = 15
          Width = 249
          Height = 54
          Align = alClient
          Caption = 'Font Scale'
          TabOrder = 2
          object Label8: TLabel
            Left = 8
            Top = 20
            Width = 55
            Height = 13
            Caption = 'Percentage'
          end
          object dxSpinEditFontScalePercentage: TdxSpinEdit
            Left = 104
            Top = 17
            Width = 73
            TabOrder = 0
            MaxValue = 250.000000000000000000
            MinValue = 1.000000000000000000
            Value = 50.000000000000000000
            StoredValues = 48
          end
        end
      end
    end
  end
  inherited pnlBottom: TPanel
    Top = 325
    Width = 478
    ParentColor = True
    inherited btnOK: TBitBtn
      Left = 128
      OnClick = btnOkClick
    end
    inherited btnCancel: TBitBtn
      Left = 242
    end
  end
  inherited imgList16x16: TImageList
    Left = 416
    Top = 0
  end
  inherited imgNoYes: TImageList
    Top = 0
  end
  inherited StandardMenuActionList: TActionList
    Left = 248
    Top = 0
  end
  inherited mmPims: TMainMenu
    Left = 320
    Top = 0
  end
  object OpenDialog1: TOpenDialog
    DefaultExt = 'wav'
    Filter = 'Wav-files|*.wav'
    Options = [ofHideReadOnly, ofNoChangeDir, ofEnableSizing]
    Left = 208
    Top = 4
  end
end
