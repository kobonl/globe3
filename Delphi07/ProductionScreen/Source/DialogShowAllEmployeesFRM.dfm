inherited DialogShowAllEmployeesF: TDialogShowAllEmployeesF
  Left = 236
  Top = 158
  Height = 515
  Caption = 'Employees on Workspot'
  Position = poOwnerFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlImageBase: TPanel
    ParentColor = True
    TabOrder = 1
    object lblMessage: TLabel
      Left = 6
      Top = 6
      Width = 91
      Height = 23
      Caption = 'lblMessage'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -19
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
  end
  inherited stbarBase: TStatusBar
    Top = 374
    Panels = <
      item
        Text = 'ABS-Group'
        Width = 100
      end
      item
        Width = 200
      end
      item
        Width = 50
      end>
    SimplePanel = False
  end
  inherited pnlInsertBase: TPanel
    Height = 337
    Font.Color = clBlack
    Font.Height = -11
    ParentColor = True
    object pnlWorkspot: TPanel
      Left = 0
      Top = 0
      Width = 624
      Height = 41
      Align = alTop
      BevelOuter = bvNone
      ParentColor = True
      TabOrder = 0
      object Label1: TLabel
        Left = 8
        Top = 12
        Width = 24
        Height = 13
        Caption = 'Plant'
      end
      object Label2: TLabel
        Left = 144
        Top = 13
        Width = 46
        Height = 13
        Caption = 'Workspot'
      end
      object edtPlantCode: TEdit
        Left = 64
        Top = 10
        Width = 65
        Height = 19
        Ctl3D = False
        Enabled = False
        ParentCtl3D = False
        TabOrder = 0
        Text = 'edtPlantCode'
      end
      object edtWorkspotCode: TEdit
        Left = 208
        Top = 11
        Width = 65
        Height = 19
        Ctl3D = False
        Enabled = False
        ParentCtl3D = False
        TabOrder = 1
        Text = 'edtWorkspotCode'
      end
      object edtWorkspotDescription: TEdit
        Left = 279
        Top = 11
        Width = 338
        Height = 19
        Ctl3D = False
        Enabled = False
        ParentCtl3D = False
        TabOrder = 2
        Text = 'edtWorkspotDescription'
      end
    end
    object pnlEmployees: TPanel
      Left = 0
      Top = 41
      Width = 624
      Height = 296
      Align = alClient
      BevelOuter = bvNone
      ParentColor = True
      TabOrder = 1
      object dxList: TdxTreeList
        Left = 0
        Top = 0
        Width = 624
        Height = 296
        Bands = <
          item
            Caption = 'Employee'
          end
          item
            Caption = 'Scanned'
          end
          item
            Caption = 'Planned'
          end
          item
            Caption = 'Stand. Available'
          end
          item
            Caption = 'Absent with reason'
          end
          item
            Caption = 'First Aid'
          end
          item
            Caption = 'Efficiency'
          end>
        DefaultLayout = False
        HeaderPanelRowCount = 1
        Align = alClient
        TabOrder = 0
        Options = [aoColumnSizing, aoColumnMoving, aoEditing, aoTabThrough, aoRowSelect, aoAutoWidth, aoAutoSort]
        TreeLineColor = clGrayText
        ShowBands = True
        ShowGrid = True
        ShowLines = False
        ShowRoot = False
        OnCustomDrawCell = dxListCustomDrawCell
        object dxListColumnCOLOR: TdxTreeListButtonColumn
          Width = 20
          BandIndex = 0
          RowIndex = 0
          Buttons = <
            item
              Default = True
            end>
        end
        object dxListColumnNUMBER: TdxTreeListColumn
          Caption = 'Number'
          BandIndex = 0
          RowIndex = 0
        end
        object dxListColumnNAME: TdxTreeListColumn
          Caption = 'Name'
          BandIndex = 0
          RowIndex = 0
        end
        object dxListColumnSHORTNAME: TdxTreeListColumn
          Caption = 'Short'
          BandIndex = 0
          RowIndex = 0
        end
        object dxListColumnTEAM: TdxTreeListColumn
          Caption = 'Team'
          BandIndex = 0
          RowIndex = 0
        end
        object dxListColumnSCANNEDWS: TdxTreeListColumn
          Caption = 'WS'
          BandIndex = 1
          RowIndex = 0
        end
        object dxListColumnPLANNEDWS: TdxTreeListColumn
          Caption = 'WS'
          BandIndex = 2
          RowIndex = 0
        end
        object dxListColumnSCANNEDDATEIN: TdxTreeListColumn
          Caption = 'Time-in'
          BandIndex = 1
          RowIndex = 0
        end
        object dxListColumnPLANNEDSTARTTIME: TdxTreeListColumn
          Caption = 'Start Time'
          BandIndex = 2
          RowIndex = 0
        end
        object dxListColumnPLANNEDENDTIME: TdxTreeListColumn
          Caption = 'End Time'
          BandIndex = 2
          RowIndex = 0
        end
        object dxListColumnELEVEL: TdxTreeListColumn
          Caption = 'Level'
          BandIndex = 0
          RowIndex = 0
        end
        object dxListColumnEPLANLEVEL: TdxTreeListColumn
          Caption = 'Level'
          BandIndex = 2
          RowIndex = 0
        end
        object dxListColumnSASTARTTIME: TdxTreeListColumn
          Caption = 'Start Time'
          BandIndex = 3
          RowIndex = 0
        end
        object dxListColumnEASTARTTIME: TdxTreeListColumn
          Caption = 'Start Time'
          BandIndex = 4
          RowIndex = 0
        end
        object dxListColumnSAENDTIME: TdxTreeListColumn
          Caption = 'End Time'
          BandIndex = 3
          RowIndex = 0
        end
        object dxListColumnEAENDTIME: TdxTreeListColumn
          Caption = 'End Time'
          BandIndex = 4
          RowIndex = 0
        end
        object dxListColumnABSENCEREASONCODE: TdxTreeListColumn
          Caption = 'Absence Reason Code'
          BandIndex = 4
          RowIndex = 0
        end
        object dxListColumnABSENCEREASONDESCR: TdxTreeListColumn
          Caption = 'Absence Reason Descr'
          BandIndex = 4
          RowIndex = 0
        end
        object dxListColumnFIRSTAIDEXPIRATIONDATE: TdxTreeListDateColumn
          Caption = 'Expiration date'
          BandIndex = 5
          RowIndex = 0
        end
        object dxListColumnCOLORVALUE: TdxTreeListColumn
          Visible = False
          BandIndex = 0
          RowIndex = 0
        end
        object dxListColumnCURREFF: TdxTreeListColumn
          Alignment = taRightJustify
          Caption = 'Current'
          BandIndex = 6
          RowIndex = 0
        end
        object dxListColumnSHIFTEFF: TdxTreeListColumn
          Alignment = taRightJustify
          Caption = 'Today'
          BandIndex = 6
          RowIndex = 0
        end
      end
    end
  end
  inherited pnlBottom: TPanel
    Top = 393
    ParentColor = True
  end
  inherited StandardMenuActionList: TActionList
    Left = 496
    Top = 152
  end
  inherited mmPims: TMainMenu
    Left = 496
    Top = 32
  end
  inherited StyleController: TdxEditStyleController
    Left = 536
    Top = 32
  end
  inherited StyleControllerRequired: TdxEditStyleController
    Left = 576
    Top = 32
  end
end
