(*
  MRA:28-NOV-2012 20013550
  - Show grid during edit-mode, for easier alignment of workspot.
  MRA:28-MAY-2013 20014289 New look Pims
  - Some pictures/colors are changed.
  MRA:20-JUN-2016 PIM-194
  - Use 3 colors for efficiency-bars.
  MRA:28-SEP-2016 PIM-223
  - Show/hide employee efficiency on scheme-level.
*)
unit DialogSettingsFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BaseDialogFRM, StdCtrls, ActnList, Buttons, ComCtrls,
  ExtCtrls, dxCntner, dxEditor, dxExEdtr, dxEdLib, Menus, StdActns, ImgList,
  ProductionScreenDMT, UPaintPanel, ORASystemDMT, UPimsConst, jpeg;

type
  TDialogSettingsF = class(TBaseDialogForm)
    OpenDialog1: TOpenDialog;
    pnlTop: TPanel;
    pnlClient: TPanel;
    GroupBox1: TGroupBox;
    GroupBox3: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    cBoxUseSoundAlarm: TCheckBox;
    edtSoundFilename: TEdit;
    btnBrowse: TButton;
    GroupBox2: TGroupBox;
    GroupBox4: TGroupBox;
    Label3: TLabel;
    dxSpinEditWorkspotScalePercentage: TdxSpinEdit;
    GroupBox5: TGroupBox;
    Label4: TLabel;
    rGrpUsePeriod: TRadioGroup;
    grpBxSinceTime: TGroupBox;
    dxTimeEditSinceTime: TdxTimeEdit;
    Label5: TLabel;
    GroupBox7: TGroupBox;
    Label8: TLabel;
    dxSpinEditFontScalePercentage: TdxSpinEdit;
    Label9: TLabel;
    dxSpinEditGridLineDistance: TdxSpinEdit;
    Panel1: TPanel;
    GroupBox6: TGroupBox;
    Label6: TLabel;
    Label7: TLabel;
    dxSpinEditRefreshTimeInterval: TdxSpinEdit;
    GroupBox8: TGroupBox;
    lblRed: TLabel;
    lblOrange: TLabel;
    dxSpinEditRed: TdxSpinEdit;
    dxSpinEditOrange: TdxSpinEdit;
    gBoxShowEmpInfo: TGroupBox;
    cBoxShowEmpInfo: TCheckBox;
    cBoxShowEmpEff: TCheckBox;
    procedure btnOkClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnBrowseClick(Sender: TObject);
    procedure rGrpUsePeriodClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cBoxShowEmpInfoClick(Sender: TObject);
  private
    { Private declarations }
    FUseSoundAlarm: Boolean;
    FSoundFilename: String;
    FSoundPath: String;
    FWorkspotScale: Integer;
    FEfficiencyPeriodSince: Boolean;
    FEfficiencyPeriodSinceTime: TDateTime;
    FRefreshTimeInterval: Integer;
    FFontScale: Integer;
    FEfficiencyPeriodMode: TEffPeriodMode;
    FGridLineDistance: Integer;
    procedure SetSoundAlarm(const Value: Boolean);
    procedure SetSoundFilename(const Value: String);
    procedure SetSoundPath(const Value: String);
    procedure SetWorkspotScale(const Value: Integer);
    procedure SetEfficiencyPeriodSince(const Value: Boolean);
    procedure SetEfficiencyPeriodSinceTime(const Value: TDateTime);
    procedure SetRefreshTimeInterval(const Value: Integer);
    procedure SetFontScale(const Value: Integer);
    procedure SetEfficiencyPeriodMode(const Value: TEffPeriodMode);
  public
    { Public declarations }
    property SettingUseSoundAlarm: Boolean read FUseSoundAlarm
      write SetSoundAlarm;
    property SettingSoundFilename: String read FSoundFilename
      write SetSoundFilename;
    property MySoundPath: String write SetSoundPath;
    property SettingWorkspotScale: Integer read FWorkspotScale
      write SetWorkspotScale;
    property SettingFontScale: Integer read FFontScale
      write SetFontScale;
    property SettingEfficiencyPeriodSince: Boolean read FEfficiencyPeriodSince
      write SetEfficiencyPeriodSince;
    property SettingEfficiencyPeriodSinceTime: TDateTime
      read FEfficiencyPeriodSinceTime write SetEfficiencyPeriodSinceTime;
    property SettingRefreshTimeInterval: Integer read FRefreshTimeInterval
      write SetRefreshTimeInterval;
    property EfficiencyPeriodMode: TEffPeriodMode read FEfficiencyPeriodMode
      write SetEfficiencyPeriodMode;
    property GridLineDistance: Integer read FGridLineDistance
      write FGridLineDistance;
  end;

var
  DialogSettingsF: TDialogSettingsF;

implementation

{$R *.DFM}

{ TDialogSettingsF }

procedure TDialogSettingsF.SetSoundAlarm(const Value: Boolean);
begin
  FUseSoundAlarm := Value;
end;

procedure TDialogSettingsF.SetSoundFilename(const Value: String);
begin
  FSoundFilename := Value;
end;

procedure TDialogSettingsF.btnOkClick(Sender: TObject);
begin
  inherited;
  SettingUseSoundAlarm := cBoxUseSoundAlarm.Checked;
  SettingSoundFilename := edtSoundFilename.Text;
  try
    SettingWorkspotScale := Trunc(dxSpinEditWorkspotScalePercentage.Value);
  except
    SettingWorkspotScale := 50;
  end;
  try
    SettingFontScale := Trunc(dxSpinEditFontScalePercentage.Value);
  except
    SettingFontScale := 50;
  end;
  EfficiencyPeriodMode := TEffPeriodMode(rGrpUsePeriod.ItemIndex);
  SettingEfficiencyPeriodSince := (rGrpUsePeriod.ItemIndex = 1);
  SettingEfficiencyPeriodSinceTime := dxTimeEditSinceTime.Time;
  try
    SettingRefreshTimeInterval := Trunc(dxSpinEditRefreshTimeInterval.Value);
  except
    SettingRefreshTimeInterval := 300;
  end;
  try
    GridLineDistance := Trunc(dxSpinEditGridLineDistance.Value);
  except
    GridLineDistance := 10;
  end;
  try
    // PIM-194
    ORASystemDM.RedBoundary := Trunc(dxSpinEditRed.Value);
    ORASystemDM.OrangeBoundary := Trunc(dxSpinEditOrange.Value);
  except
    ORASystemDM.RedBoundary := RED_BOUNDARY;
    ORASystemDM.OrangeBoundary := ORANGE_BOUNDARY;
  end;
  // PIM-223
  ORASystemDM.PSShowEmpInfo := cBoxShowEmpInfo.Checked;
  ORASystemDM.PSShowEmpEff := cBoxShowEmpEff.Checked;
  if not ORASystemDM.PSShowEmpInfo then
    ORASystemDM.PSShowEmpEff := False;
end;

procedure TDialogSettingsF.FormShow(Sender: TObject);
begin
  inherited;
  rGrpUsePeriodClick(Sender);
  cBoxUseSoundAlarm.Checked := SettingUseSoundAlarm;
  edtSoundFilename.Text := SettingSoundFilename;
  dxSpinEditWorkspotScalePercentage.Value := SettingWorkspotScale;
  dxSpinEditFontScalePercentage.Value := SettingFontScale;
{  if SettingEfficiencyPeriodSince then
    rGrpUsePeriod.ItemIndex := 1
  else
    rGrpUsePeriod.ItemIndex := 0; }
  rGrpUsePeriod.ItemIndex := Integer(FEfficiencyPeriodMode);
  dxTimeEditSinceTime.Time := SettingEfficiencyPeriodSinceTime;
  dxSpinEditRefreshTimeInterval.Value := SettingRefreshTimeInterval;
  dxSpinEditGridLineDistance.Value := GridLineDistance;
  // PIM-194
  dxSpinEditRed.MaxValue := MAX_COLOR_BOUNDARY;
  dxSpinEditRed.MinValue := MIN_COLOR_BOUNDARY;
  dxSpinEditOrange.MaxValue := MAX_COLOR_BOUNDARY;
  dxSpinEditOrange.MinValue := MIN_COLOR_BOUNDARY;
  dxSpinEditRed.Value := ORASystemDM.RedBoundary;
  dxSpinEditOrange.Value := ORASystemDM.OrangeBoundary;
  // PIM-223
  cBoxShowEmpInfo.Checked := ORASystemDM.PSShowEmpInfo;
  cBoxShowEmpEff.Checked := ORASystemDM.PSShowEmpEff;
  cBoxShowEmpInfoClick(Sender);
end;

procedure TDialogSettingsF.SetSoundPath(const Value: String);
begin
  FSoundPath := Value;
end;

procedure TDialogSettingsF.btnBrowseClick(Sender: TObject);
var
  Path: String;
  OtherPath: String;
begin
  inherited;
  Path := GetCurrentDir;
  try
    OpenDialog1.InitialDir := FSoundPath;
    OpenDialog1.Filename := SettingSoundFilename;
    if OpenDialog1.Execute then
    begin
      SettingSoundFilename := ExtractFileName(OpenDialog1.Filename);
      edtSoundFilename.Text := SettingSoundFilename;
      // If file came from another path, copy the file to
      // SoundPath
      OtherPath := ExtractFilePath(OpenDialog1.Filename);
      if OtherPath <> FSoundPath then
        CopyFile(PChar(OtherPath + SettingSoundFilename),
          PChar(FSoundPath + SettingSoundFilename), False);
    end;
  finally
    SetCurrentDir(Path);
  end;
end;

procedure TDialogSettingsF.SetWorkspotScale(const Value: Integer);
begin
  FWorkspotScale := Value;
end;

procedure TDialogSettingsF.SetEfficiencyPeriodSince(const Value: Boolean);
begin
  FEfficiencyPeriodSince := Value;
end;

procedure TDialogSettingsF.SetEfficiencyPeriodSinceTime(
  const Value: TDateTime);
begin
  FEfficiencyPeriodSinceTime := Value;
end;

procedure TDialogSettingsF.rGrpUsePeriodClick(Sender: TObject);
begin
  inherited;
  dxTimeEditSinceTime.Enabled := (rGrpUsePeriod.ItemIndex = 1);
end;

procedure TDialogSettingsF.SetRefreshTimeInterval(const Value: Integer);
begin
  FRefreshTimeInterval := Value;
end;

procedure TDialogSettingsF.SetFontScale(const Value: Integer);
begin
  FFontScale := Value;
end;

procedure TDialogSettingsF.SetEfficiencyPeriodMode(
  const Value: TEffPeriodMode);
begin
  rGrpUsePeriod.ItemIndex := Integer(Value);
  FEfficiencyPeriodMode := Value;
end;

procedure TDialogSettingsF.FormCreate(Sender: TObject);
begin
  inherited;
  lblMessage.Caption := ' ' + Caption;  // 20014289
end;

// PIM-223
procedure TDialogSettingsF.cBoxShowEmpInfoClick(Sender: TObject);
begin
  inherited;
  if cBoxShowEmpInfo.Checked then
  begin
    cBoxShowEmpEff.Enabled := True;
  end
  else
  begin
    cBoxShowEmpEff.Checked := False;
    cBoxShowEmpEff.Enabled := False;
  end;
end;

end.
