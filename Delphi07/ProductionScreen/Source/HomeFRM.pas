(*
  MRA: 8-JUN-2009. RV029.
    Use an ProductionScreen.INI-file to get the lastscheme-name from a
    section that is the computername, instead of getting this from registry.
    Note: Read/write all settings from/to this INI-file.
  MRA:JUN-2010. RV063.4. Order 550478. Personal Screen.
  - Addition of Machine/Workspot + Time recording.
  MRA:29-JUL-2010. RV063.5. Fix for current shown quantities: Mode=Current.
  - When employee just switched job, the shown quantities must still
    be based on previous job! Because the quantities are always from the
    previous period of the datacollection, because the actual period
    is not available yet.
    Rules: (when 'Current' is used).
    - When employee first scans in on a job:
      - There is no previous job, so it does not matter.
    - When employee switches job:
      - The previous period must be based on previous job, but
        the current shown job (in job-button) must show the new job.
        - The previous job should be determined by searching for the scan
          that is available in this previous period.
    - When employee finished his job (end-of-day):
      - The previous period must be based on job of last scan.
  MRA:28-SEP-2010. RV064.1. Bugfix.
  - Production Screen used as Personal Screen:
    - When used on 2 workstations it can happen a scan a new open
      scan is created, while there is already an open scan for
      the same employee.
    - Added a procedure that will check if there was interference.
      This is logged to a file.
    - Menu-option added named 'Show Status Info' that shows the
      info about the scan-interference.
  MRA:21-JAN-2011 RV075.1. Small change.
  - Production Screen 2.0.153.29
    - For Workspot/Timerecording-component:
      Lower part below job-button some pixels to make
      room for the select-rectangle.
    - JobButton is now placed in a different way.
  MRA:18-JUN-2012 20013311
  - Efficiency can be based on quantity or time.
  - The reading of Production Quantity-records has a delay.
    - Cause: It only looks at End-date that should fall between
      datefrom and dateto. So even when the record exists already
      it can still take about (max.) 5 minutes before it is read.
      - Example:
        - Current time is 14:21. PQ-record exists from 14:20-14:25.
          Only at 14:25 it will be read.
    - Note: Use IncMinute (DateUtils) to add 5 minutes.
  - Efficiency of current was wrong: It was based on PimsDatacolInterval,
    but this is in seconds, not in minutes, as was expected.
  MRA:29-JUN-2012 20013379
  - Compare Jobs handling.
  MRA:25-JUL-2012 Bugfix.
  - Changed MAXOBJECTS from 99 to 999.
  MRA:7-SEP-2012 20013516
  - When employee(s) scanned to down (mechanical down or
    no merchandize down),
    meaning jobcode equals TDOWN or PDOWN, then the workspot-icon
    must flash blue, similar as flashing during compare-jobs
    when there is a deviation.
  - Flash light-blue for TDOWN
  - Flash dark-blue for PDOWN
  - Additional: Only show selection-rectangle when editing.
  MRA:13-SEP-2012 SO-20013516 Related to this order
  - When using personal-screen-objects in this dialog there
    are 2 problems:
    - When there are 2 workspots and an employee scans in/out
      from one workspot to the other using personal-screen (or timerecording)
      then it still shows the old job for workspot 1.
    - NOJOB is not ignored, resulting in large quantities.
    - Adjust font for job-button based on name of job.
    - When no-one is scanned it at the moment, it still shows the efficiency.
    - For now:
      - Disable the 'personal screen' in this application, because
        now many things/bugs that are solved in personal-screen-app. are not
        solved here.
  MRA:17-SEP-2012 SO-20013563 Head count.
  - It should be possible to show a table with
    the number of currently scanned in employees
    per department.
  - Rework: Show ALL employees currently scanned in per
            department, this is NOT related to the workspots
            that are shown in the Production Screen.
  - Technical details:
    - Use ObjectType: otPuppetBox, this shows a rectangle with a title,
      but do not show puppets.
    - Use ShowMode = ShowHeadCount, this is used to know it is about a
      head count.
    - Use ACross (part of ADrawObject) to draw a cross in the rectangle,
      to form a table.
  MRA:25-SEP-2012 SO-20013563 Head count. Rework.
  - The department-descriptions can be longer when using another scale:
    - The width of the rectangle + cross must be enlarged.
  - The height (lines) is not always calculated correct.
  MRA:15-OCT-2012 20013516 Flash Blue - Rework
  - Make 'light blue' lighter, and 'dark blue' darker.
  MRA:19-OCT-2012 20013551 Production Screen info at tunnels
  - When a workspot is of type 'efficiency based on manhours',
    then:
    - Someone is scanned in on this workspot (default situation).
    - Use Job-field 'norm prod level minimum percentage' to
      determine if efficiency is <= this minimum, if so then
      show blinking red percentage above the workspot-icon.
      If not, then do not show any percentage above it.
  - When a workspot is of type 'efficiency based on running hours',
    then:
    - No-one is scanned in on this workspot.
    - Use workspot-shifts to determine the current shift
      for efficiency-calculation.
    - Use Job-field 'norm machine output minimum percentage' to
      determine if efficiency is <= this minimum, if so then
      show blinking red percentage above the workspot-icon.
      If not, then do not show any percentage above it.
  MRA:12-NOV-2012 20013549
  - Show comparison deviation as % at production screen.
  MRA:16-NOV-2012 TD-21162
  - Problem-solving for colors yellow and orange.
    - During 'ActionCreatePuppets' it filters out the grey-puppets
      (only planned) based on employees scanned in, but it compared the shift
      during this. This must be left out, or it will show too much employees.
  MRA:28-NOV-2012 20013550
  - Additions to make alignment of workspots in schemes easier:
    - Show a grid in edit-mode for alignment
    - Extra setting for grid line distance
    - During loading of scheme: Align the workspots
  MRA:3-DEC-2012 20013551 Production Screen Info at tunnels - Rework.
  - When using 'Since' it does not show correct efficiency.
  - Added it also for 'BigEffMeter', to make it easier to see what happens.
  - Special Note:
    - When using current, then the current job is based on last found PQ-record.
      But when there was no PQ-record for last 5 minutes, then it will show
      nothing. This is different from a workspot with employee, because then
      the employee who scanned in is used for determining last job.
  MRA:7-DEC-2012 20013549.10 Max Deviation label.
  - Resize with FontScale
  - Subtract label height to get the correct position after a FontScale-change.
  - Changed Top to smaller value.
  MRA:7-DEC-2012 20013551.10 Min Percentage label.
  - Resize with FontScale
  - Subtract label height to get the correct position after a FontScale-change.
  - Changed Top to smaller value.
  MRA:12-DEC-2012 20013548
  - Auto adjust resolution.
  ROP:08-JAN-2013 todo 21774 Lines not shown as designed
  - Loading lines from the ini-file did not load the width/height. Fixed.
  MRA:9-JAN-2013 20013550.10. Rework.
  - Alignment for vert/hor lines gives problems:
    They are shown on wrong positions because of this order, this makes it
    nearly impossible to put them on desired positions during edit-mode,
    because after a reload they are repositioned again.
    To solve this: Do not align vert/hor lines.
  MRA:9-JAN-2013 20013548.10. Rework.
  - Alignment for vert/hor lines gives problems:
    It appeared the rescale-procedure was always doing something, even
    when the screen-size was not changed.
    Cause: It compared with 1, but this is a double so can be around 1.
  MRA:8-FEB-2013 SO-20013910. Plan on departments issue
  - Employees who are planned on departments handling added:
    - Also look for employees planned on the department linked to the
      workspot.
  MRA:11-FEB-2013 TD-22082 Perfomance problems
  - Use a ClientDataSet instead of a TList for WorkspotsPerEmployee.
    - Reason: When this list is too big, it takes too much time to
              search something.
  - Added Init-property, to see if the lists are initialized or not.
  - Filter on teams-per-user to limit the number of employees read.
  - Use a ClientDataSet instead of a TList for WorkspotsPerEmployee.
    - Reason: When this list is too big, it takes too much time to
              search something.
  - Added Init-property, to see if the lists are initialized or not.
  - Filter on teams-per-user to limit the number of employees read.
  MRA:28-MAY-2013 20014289 New look Pims
  - Some pictures/colors are changed.
  MRA:21-NOV-2013 20013196
  - Multiple counters on one workspot
    - Detect if a link job is used for a workspot, if this is true, then:
      - Put a star after the text shown above the workspot-icon
      - Put text '(linked job)' after the hint.
  MRA:4-APR-2014 TD-24100 Percentage not shown at workspot
  - Sometimes it shows 0 for percentage in ShowChartFRM while it should be > 0.
  - Sometimes DrawObject.ACompareJob is True while it should be False!
  ROP:15-APR-2014 23466
  - Add multi-select and move
  MRA:5-MAY-2014 TD-24852
  - Production Screen does not show production quantity when using Shift-mode.
  MRA:30-SEP-2014 TD-25679
  - Change default value for Refresh Time Interval (from 180 to 30 seconds).
  MRA:03-OCT-2014 SO-20015402
  - Do a refresh:
    - upon start
    - after edit
    - after loading a scheme
    - after changing settings
    - show legenda
    - show info
  MRA:17-OCT-2014 TD-25881
  - Problems with show graph
  - When the ShowChartFRM is closed, it must uncheck the 'enter dates' checkbox,
    or it will not show a graph when you open this the graph-form again!
  - Because the efficiency was wrong when using 'enter dates' checkbox, it now
    does a call to ActionEfficiency, to ensure it is calculated in the same
    way as in the main-screen.
  MRA:17-NOV-2014 TD-26129
  - Production Screen
    - Inconsistency between Personal Screen and Production Screen
      when using 'current'.
    - When comparing efficiency shown by Personal Screen and
      Production Screen (when using current) it can show big
      differences.
  MRA:25-NOV-2014 TD-23466.150
  - changes for 'add multi select and move'.
  MRA:23-DEC-2014 SO-20016016
  - Make it possible on workspot-level to read data (refresh) from database
    instead of from blackboxes (read by Personal Screen,
    shown by both Personal Screen and Production Screen).
  MRA:13-MAR-2015 TD-23466.150 Rework
  - Moving of workspot and a line issue: The line moved too much compared
    with the workspot. An Align had to be added for the TShapeEx-objects.
  MRA:10-APR-2015 SO-20016449
  - Time Zone Implementation
  MRA:1-MAY-2015 20014450.50 Part 2
  - Real Time Efficiency
  MRA:14-MAR-2016 PIM-151
  - Show Ghost Counts
  MRA:20-JUN-2016 PIM-194
  - Use 3 colors for efficiency-bars.
  MRA:9-SEP-2016 PIM-213
  - New screens showing per workspot the efficiency and how the
    operator performs.
  MRA:28-SEP-2016 PIM-223
  - Show/hide employee efficiency on scheme-level.
  MRA:25-NOV-2016 PIM-237
  - ProductionScreen and Employees too late result can be wrong.
  - Reason: It compared with current planned time-block. It must compare
    with first planned time-block.
  MRA:28-NOV-2016 PIM-237
  - It compares with current open scan to determine too late! Instead of that
    it must compare with the first scan of the current shift-date + shift!
  MRA:8-MAR-2017 PIM-275
  - Planning at Department is shown wrong at Production Screen
  - When planning is done on workspots having 'plan on workspot = false' then
    it shows the same employees on workspots connected to this department. A
    check is needed to prevent this.
  - Example: Workspots 1001, 1002 and 1003 are of type 'plan on workspot=false'.
    When 3 employees are planned on the department connected to these workspots,
    it showed the same 3 employees for each workspot.
  MRA:30-JAN-2019 GLOB3-231
  - Productionscreen loading problem when start from client
  - Check path+schemename to be sure the file can be found before it is opened,
    and when not, give a message about it.
  - It uses SchemePath (path of the schemes) that is determined via the path
    where the program is started. Instead of that it should use a fixed path
    that is stored in ProductionScreen.INI-file.
  MRA:1-MAR-2019 PIM-237
  - ProductionScreen and Employees too late result can be wrong
  - It did not clear any puppets in a box (actionpuppetbox), which can happen
    for too late. This resulted in still showing puppets while they were not
    late anymore based on current timeblock.
  MRA:19-APR-2019 GLOB3-231
  - SchemePath: Read this first from 'PIMSORA'-section, if found, you that
    instead of other sections.
*)

unit HomeFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ORASystemDMT, ShapeEx, Buttons, ToolWin, ComCtrls, ExtCtrls, Variants,
  ImgList, Menus, IniFiles, dxTL, StdCtrls, ActnList,
  Db, Registry, FileCtrl, MPlayer, PlanScanEmpDMT,
  UScannedIDCard, BaseTopLogoFRM, StdActns, dxCntner, OracleData,
  ProductionScreenDMT, UProductionScreenDefs, DateUtils, UPaintPanel,
  ColorButton, dxfProgressBar, UWorkspotEmpEff, jpeg;

const
//  DEBUG=0;
  LOGFILENAME='PSLOG.TXT';
  DEFAULT_WORKFILENAME='Pims.ini';
  RECTANGLE=8;
  IMAGE_SIZE=64;
  LIGHT_SIZE=13;
  MAXOBJECTS=999;
  BIGEFFMETERS_SCALE_PERCENTAGE=350;
  DEFAULT_WORKSPOT_SCALE=100;
  IMAGEPATH_DEFAULT='c:\temp\pims\';
  SCHEMEPATH_DEFAULT='c:\temp\';
  SOUNDPATH_DEFAULT='c:\temp\';
  EXPORTPATH_DEFAULT='c:\temp\';
  DEFAULT_REFRESH_TIME_INTERVAL=30; // Time screen refreshes, in Seconds // TD-25679 180 -> 30
  DEFAULT_DATACOL_TIME_INTERVAL=300; // seconds
  DEFAULT_MULTIPLIER=2; // 20013379 Set this to 1 otherwise it takes 2 as long to update!
  MAX_PUPPET_PARTS=2; // was 5
  ProdScreenINIFilename='ProductionScreen.INI';
  CJ_CHANGE_JOB=0;
  CJ_END_OF_DAY=1;
  CJ_DOWN=2;
  CJ_CONTINUE=3;
  DefaultGridLineDistance=10; // 20013550
  PIMS_TIME_INTERVAL = 300; // Interval of 5 minutes for ProductionQuantity
  MaxWidth = 800; // TD-23620

// 20014550.50 Make OnClick-event publised +
//             add MyTag-property to keep track of EmployeeNumber
type
  TShape = class( ExtCtrls.TShape )
  published
    property OnClick;
  private
    FMyTag: Integer;
    FACurrEff: Integer;
    FAShiftEff: Integer;
  public
    property MyTag: Integer read FMyTag write FMyTag;
    property ACurrEff: Integer read FACurrEff write FACurrEff;
    property AShiftEff: Integer read FAShiftEff write FAShiftEff;
  end;

// 20014550.50 Addition of MyTag to keep track of EmployeeNumber
type
  TLabel = class( StdCtrls.TLabel )
  private
    FMyTag: Integer;
    FShiftNumber: Integer;
    FPlantCode: String;
  public
    property MyTag: Integer read FMyTag write FMyTag;
    property PlantCode: String read FPlantCode write FPlantCode;
    property ShiftNumber: Integer read FShiftNumber write FShiftNumber;
  end;

type
  PTPuppet = ^TPuppet;
  TPuppet = record
    Puppet: array[0..MAX_PUPPET_PARTS] of TShape;
    AColor: TColor;
    AEmployeeNumber: Integer;
    AEmployeeDescription: String;
    AEmployeeShortName: String;
    AEmployeeTeamCode: String;
    AEmpLevel: String;
    AScannedWS: String;
    AScannedDateTimeIn: TDateTime;
    APlannedDept: String; // PIM-275
    APlannedWS: String;
    APlannedStartTime: TDateTime;
    APlannedEndTime: TDateTime;
    AEPlanLevel: String;
    AScanned: Boolean; // has employee scanned in (TimeRegScanning)
    AJobCode: String; // if employee is scanned in, what's his jobcode?
    ACurrEff: Integer; // 20014550.50
    AShiftEff: Integer; // 20014550.50
    AFirstPlanStartDate: TDateTime; // PIM-237
    AFirstScan: TDateTime; // PIM-237
  end;

type
  PTPuppetShift = ^TPuppetShift;
  TPuppetShift = record
    APuppetList: TList;
    AShiftNumber: Integer;
  end;

// For use during temporary creation of employeelist to ensure
// unique employees are taken.
type
  PTTempEmployee = ^TTempEmployee;
  TTempEmployee = record
    AEmployeeNumber: Integer;
  end;

// For use in DrawObject: AEmployeeList
// Shows employees under BIG-EFF-METER
type
  PTAEmployee = ^TAEmployee;
  TAEmployee = record
    AEmployeeNumber: Integer;
    AEmployeeNameLabel: TLabel;
    ADateTimeOut: TDateTime;
  end;

type
  PTARectangle = ^TARectangle;
  TARectangle = record
    ATopLineHorz: TShapeEx;
    ARightLineVert: TShapeEx;
    ABottomLineHorz: TShapeEx;
    ALeftLineVert: TShapeEx;
  end;

// SO-20013563
type
  PTACross = ^TACross;
  TACross = record
    ALineHorz: TShapeEx;
    ALineVert: TShapeEx;
  end;

// SO-20013563
type
  PTADepartment = ^TADepartment;
  TADepartment = record
    ADepartmentDescription: String;
    ANr: Integer;
  end;

// SO-20013563
type
  PTAHeadCount = ^TAHeadCount;
  TAHeadCount = record
    ALabelDepartment: TLabel;
    ALabelNr: TLabel;
  end;

// 20013551
type
  PTAShift = ^TAShift;
  TAShift = record
    AShiftNumber: Integer;
    AShiftDescription: String;
    AStartTime: array[1..7] of TDateTime;
    AEndTime: array[1..7] of TDateTime;
  end;

type
  TObjectType = (otImage, otLineVert, otLineHorz, otRectangle, otPuppetBox);

type
  PTMachineTimeRec = ^TMachineTimeRec;
  TMachineTimeRec = record
    AMachineCode: String;
    AMachineDescription: String;
    AJobButton: TButton;
    ACurrentJobCode: String;
    ACurrentJobDescription: String;
  end;

type
  PTWorkspotTimerec = ^TWorkspotTimeRec;
  TWorkspotTimeRec = record
    AMachineCode: String;
    AMachineDescription: String;
    AJobButton: TButton;
    ACurrentJobCode: String;
    ACurrentJobDescription: String;
    APreviousJobCode: String;
    AInButton: TButton;
    AModeButton: TButton;
    APercentageLabel: TLabel;
    AActualNameLabel: TLabel;
    AActualValueLabel: TLabel;
    ATargetNameLabel: TLabel;
    ATargetValueLabel: TLabel;
    ADiffNameLabel: TLabel;
    ADiffValueLabel: TLabel;
    ADiffLineHorz: TShapeEx;
    ACurrentShiftNumber: Integer;
    ACurrentShiftStart: TDateTime;
    ACurrentShiftEnd: TDateTime;
  end;

type
  PDrawObject = ^DrawObject;
  DrawObject = record
    AObject: TObject;
{    APanel: TPanel; }
    AType: TObjectType;
    ADeptRect: PTARectangle;
    ALabel: TLabel;
    ALightRed: TShape;
    ALightYellow: TShape;
    ALightGreen: TShape;
    ABorder: TShape;
    AEffMeterGreen: TShape;
    AEffMeterRed: TShape;
    AEffPercentage: Integer;
    AImageName: String;
    APlantCode: String;
    AWorkspotCode: String;
    AWorkspotDescription: String;
    APuppetShiftList: TList;
    ABigEffMeters: Boolean;
    AEmployeeList: TList;
    AEfficiency: Double;
    ANormProdQuantity: Double;
    ANormProdLevel: Double;
    ATotalWorkedTime: Integer;
    ATotalProdQuantity: Double;
    ATotalProdQuantityCurrent: Double;
    ATotalNormQuantity: Double;
    ATotalPercentage: Integer;
    AEffTotalsLabel: TLabel;
    ABlinkPuppets: Boolean;
    ABigEffLabel: TLabel;
    ANoData: Boolean;
    AEmployeeCount: Integer;
    AShowMode: TShowMode;
    ABlinkCompareJobs: Boolean;
    AProdScreenType: TProdScreenType;
    APriority: Integer;
    ACompareJob: Boolean;
    ACompareJobCode: String;
    AEmpsScanIn: Boolean;
    ABlinkIsDown: Boolean;
    ADownJobCode: String;
    ACurrentNumberOfEmployees: Integer;
    ACross: PTACross; // // SO-20013563
    AHeadCountList: TList; // SO-20013563
    AEffRunningHrs: Boolean; // 20013551
    AShiftList: TList; // 20013551
    AMinPercLabel: TLabel; // 20013551
    AProdLevelMinPerc: Double; // 20013551
    AMachOutputMinPerc: Double; // 20013551
    AShiftNumber: Integer; // 20013551
    AShiftStartDateTime: TDateTime; // 20013551
    AJobCode: String; // 20013551
    AMinPercBlink: Boolean; // 20013551
    AMaxDeviationLabel: TLabel; // 20013549
    ADeviation: Integer; // 20013549
    AHasLinkJob: Boolean; // 20013196
    SelectionRectangle: TShapeEx; //23466
    Selected : boolean; //23466
    ACurrentShiftNumber: Integer; // 24852
    ACurrentShiftStart: TDateTime; // 24852
    ACurrentShiftEnd: TDateTime; // 24852
    ATimezonehrsdiff: Integer; // 20016449
    AListEmployeeNumber: Integer; // 2004550.50
    ATotalProdTime: Double; // 20014450.50
    ATotalNormTime: Double; // 20014450.50
    AGhostCountBlink: Boolean; // PIM-151
    ADefaultImage: TImage; // PIM-151
    AGhostImage: TImage; // PIM-151
    AProgressBar: TdxfProgressBar; // PIM-194
    ApnlWSEmpEff: TpnlWSEmpEff; // PIM-213
  end;

type
  THomeF = class(TBaseTopLogoForm)
    SaveDialog1: TSaveDialog;
    OpenDialog1: TOpenDialog;
    TimerMain: TTimer;
    PopupMainMenu: TPopupMenu;
    ShowAllEmployees1: TMenuItem;
    ShowEmployeesonwrongWokspot1: TMenuItem;
    ShowEmployeesnotscannedin1: TMenuItem;
    ShowEmployeesabsentwithreason1: TMenuItem;
    ShowEmployeesabsentwithoutreason1: TMenuItem;
    TimerBlink: TTimer;
    PopupWorkspotMenu: TPopupMenu;
    ShowChart: TMenuItem;
    ShowEmployees1: TMenuItem;
    TimerStatus: TTimer;
    ShowEmployeeswithFirstAid1: TMenuItem;
    ShowEmployeestoolate1: TMenuItem;
    ProductionScreenActionList: TActionList;
    actWorkspot: TAction;
    actEffiMeter: TAction;
    actLineHorz: TAction;
    actLineVert: TAction;
    actRectangle: TAction;
    actPuppetBox: TAction;
    actDelete: TAction;
    actSave: TAction;
    actEdit: TAction;
    tlbarGrid: TToolBar;
    tlbtnWorkSpot: TToolButton;
    tlbtnEffMeter: TToolButton;
    tlbtnLineHorz: TToolButton;
    tlbtnLineVert: TToolButton;
    tlbtnRectangle: TToolButton;
    tlbtnPuppetBox: TToolButton;
    tlbtnDelete: TToolButton;
    tlbtnSave: TToolButton;
    tlbtnEdit: TToolButton;
    actNew: TAction;
    actOpen: TAction;
    actSaveAs: TAction;
    New1: TMenuItem;
    Save1: TMenuItem;
    Open1: TMenuItem;
    SaveAs1: TMenuItem;
    N1: TMenuItem;
    HelpLegendaAct: TAction;
    Legenda1: TMenuItem;
    actShowStatusInfo: TAction;
    lblHr: TLabel;
    lblPcs: TLabel;
    lblNormPcs: TLabel;
    MediaPlayer1: TMediaPlayer;
    TimerMultiSelect: TTimer;
    PopupMenuRealTimeEff: TPopupMenu;
    MenuItemEmpGraph: TMenuItem;
    MenuItemEmpView: TMenuItem;
    MenuItemEmployeeName: TMenuItem;
    MenuItemCurrentEff: TMenuItem;
    MenuItemTodayEff: TMenuItem;
    ImageGhost: TImage;
    actWSEmpEff: TAction;
    tlbtnWSEmpEff: TToolButton; //23466
    procedure pnlDrawMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure ImageClick(Sender: TObject);
    procedure ImageMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure ImageMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure ImageMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure actOpenExecute(Sender: TObject);
    procedure actNewExecute(Sender: TObject);
    procedure actSaveAsExecute(Sender: TObject);
    procedure TimerMainTimer(Sender: TObject);
    procedure ActionShowChangeProperties(Sender: TObject);
    procedure ShowAllEmployees1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ShowEmployeesonwrongWokspot1Click(Sender: TObject);
    procedure ShowEmployeesnotscannedin1Click(Sender: TObject);
    procedure ShowEmployeesabsentwithreason1Click(Sender: TObject);
    procedure ShowEmployeesabsentwithoutreason1Click(Sender: TObject);
    procedure FileExitActExecute(Sender: TObject);
    procedure TimerBlinkTimer(Sender: TObject);
    procedure ShowChartClick(Sender: TObject);
    procedure ShowEmployees1Click(Sender: TObject);
    procedure actFileSettingsExecute(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure TimerStatusTimer(Sender: TObject);
    procedure ShowEmployeeswithFirstAid1Click(Sender: TObject);
    procedure ShowEmployeestoolate1Click(Sender: TObject);
    procedure HelpLegendaActExecute(Sender: TObject);
    procedure actPuppetBoxExecute(Sender: TObject);
    procedure actWorkspotExecute(Sender: TObject);
    procedure actEffiMeterExecute(Sender: TObject);
    procedure actLineHorzExecute(Sender: TObject);
    procedure actLineVertExecute(Sender: TObject);
    procedure actRectangleExecute(Sender: TObject);
    procedure actSaveExecute(Sender: TObject);
    procedure actDeleteExecute(Sender: TObject);
    procedure actEditExecute(Sender: TObject);
    procedure actShowStatusInfoExecute(Sender: TObject);
    procedure PanelPaint(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure stbarBaseDrawPanel(StatusBar: TStatusBar;
      Panel: TStatusPanel; const Rect: TRect);
// <23466>
    procedure ClearAllSelectionRectangles;
    function isAltDown : Boolean;
    function isCtrlDown : Boolean;
    function isShiftDown : Boolean;
    procedure TimerMultiSelectTimer(Sender: TObject);
// </23466>
    procedure HelpOrderingInfoActExecute(Sender: TObject);
    procedure MenuItemEmpGraphClick(Sender: TObject);
    procedure MenuItemEmpViewClick(Sender: TObject);
    procedure PuppetMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure EmployeeNameLabelMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure ListButtonClick(Sender: TObject);
    procedure GraphButtonClick(Sender: TObject);
    procedure actWSEmpEffExecute(Sender: TObject);
  protected
    procedure WMTimeChange(var Message: TMessage); message WM_TIMECHANGE;
  private
    { Private declarations }
    ThisCaption: String;
    RefreshTimeInterval: Integer; // Time screen refreshes
    LogPath: String;
    ImagePath: String;
    SchemePath: String;
    SoundPath: String;
    ExportPath: String;
    SoundFilename: String;
    UseSoundAlarm: Boolean;
    ExportSeparator: String;
    EfficiencyPeriodMode: TEffPeriodMode;
    EfficiencyPeriodSince: Boolean; // Use 'Current' or 'Since Time'.
    EfficiencyPeriodSinceTime: TDateTime; // H:M
    FDragOfX: Integer;
    FDragOfY: Integer;
    FDragging: Boolean;
    List: TList;
    WorkspotScale: Integer;
    FontScale: Integer; // for big-efficiency-meters
    WorkFilename: String;
    CurrentDrawObject: PDrawObject;
    SelectedDrawObject: PDrawObject; //23466
    SelectionRectangle: TShapeEx;
    Dirty: Boolean; // has scheme changed by user? Ask for save!
    TimerStartTime, TimerEndTime: TDateTime;
    TimerOnTime: TDateTime;
    InitForm: Boolean;
    Busy: Boolean;
    AppRootPath: String;
    FEffQuantityTime: TEffQuantityTime;
    GridLineDistance: Integer;
    pnlDraw: TPaintPanel;
    CurrentSchemeWidth, CurrentSchemeHeight: Integer;
    FScreenWidth: Integer;
    FScreenHeight: Integer;
    MultiSelectActive : boolean;
    FDateFromGraph: TDateTime;
    FDateToGraph: TDateTime;
    procedure SetCaption;
    function CreateImage(const AImageName: String;
      const ABigEffMeters: Boolean): TImage;
    function CreateGhostImage: TImage;      
    function CreateShapeEx(AShapeExType: TShapeExType): TShapeEx;
    function CreateRectangle(ADrawObject: PDrawObject): PTARectangle;
    procedure DeleteRectangle(ADrawObject: PDrawObject);
    procedure ShowRectangle(ADrawObject: PDrawObject);
    function NewDrawObject(AProdScreenType: TProdScreenType): PDrawObject;
    function NewItemIndex: Integer;
    procedure RenumberDrawObjects;
    procedure AddObjectToList(otType: TObjectType;
      AObject: TObject; APlantCode, AWorkspotCode, AWorkspotDescription,
      AImageName: String; ABigEffMeters: Boolean; AShowMode: Integer;
      AProdScreenType: TProdScreenType);
    procedure DeleteDrawObjectFromList(ATag: Integer);
    procedure ShowObjectNumber(Sender: TObject);
    procedure ShowObjectProperties(X: Integer; Y: Integer);
    procedure FreeList;
    procedure SaveList(Filename: String);
    procedure OpenList(Filename: String);
    function ThisDrawObject(ATag: Integer): PDrawObject;
    procedure ShowChangeProperties(Sender: TObject);
    procedure ShowSelectionRectangle(Sender: TObject; SelectRect : TShapeEx);
    procedure ShowDrawObject(ADrawObject: PDrawObject; Sender: TObject);
    function NewPuppet(ADrawObject: PDrawObject;
      AParent: TWinControl; APTEmployee: PTEmployee): PTPuppet;
    procedure DeletePuppet(var APuppet: PTPuppet);
    procedure DeletePuppetList(var APuppetList: TList);
    procedure DeletePuppetShiftList(var APuppetShiftList: TList);
    procedure ShowPuppet(APuppet: PTPuppet;
      ALeft, ATop: Integer; AColor: TColor);
    procedure ShowPuppets(ADrawObject: PDrawObject; Sender: TObject);
    function SearchCreateShift(var ADrawObject: PDrawObject;
      const AShiftNumber: Integer): Integer;
    function PrecalcPercentage(ADrawObject: PDrawObject;
      APercentage: Integer): Integer;
    procedure ShowEffMeter(ADrawObject: PDrawObject; APercentage: Integer;
      Sender: TObject);
    procedure ShowIndicators(ADrawObject: PDrawObject;
      ARed, AYellow, AGreen: TColor);
    function SearchLastProductionQuantityRecord(ADrawObject: PDrawObject;
      var Quantity: Double): Boolean;
    procedure ActionConnectionIndicators(ADrawObject: PDrawObject);
    procedure ActionPuppets(ADrawObject: PDrawObject);
    procedure MainActionForTimer;
    procedure ReadRegistry;
    procedure WriteRegistry;
    procedure ShowEmployees(const AShowMode: TShowMode);
    function AskForSave: Boolean;
    procedure PlaySound;
    procedure ShowFirstObject;
    procedure ClearEmployeeList(ADrawObject: PDrawObject);
    procedure ShowEmployeeList(ADrawObject: PDrawObject);
    procedure ShowActionTime;
    function DetermineWorkspotShortName(PlantCode,
      WorkspotCode: String; var ALinkJob: Boolean): String;
    function DetermineDepartmentDescription(PlantCode,
      DepartmentCode: String): String;
    procedure MainTimerSwitch(OnOff: Boolean);
    procedure ActionBlinkPuppets;
    procedure ActionPuppetBox(ADrawObject: PDrawObject);
    procedure ActionCreatePuppets(ADrawObject: PDrawObject;
      var AEmployeeList: TList);
    procedure ActionCompareJobsInit(MyNow: TDateTime);
    procedure ActionCompareJobs(ADrawObject: PDrawObject; MyNow: TDateTime);
    procedure ReadINIFile;
    procedure WriteINIFile;
    procedure ShowMsg(AMsg: String);
    procedure DeleteAnyHeadCountList;
    procedure DeleteHeadCountList(ADrawObject: PDrawObject);
    procedure ActionShowHeadCount(AMyDrawObject: PDrawObject);
    function NewFontSize: Integer;
    procedure DeleteShiftList(ADrawObject: PDrawObject);
    procedure DetermineEffRunningHrs(ADrawObject: PDrawObject);
    procedure DetermineShiftList(ADrawObject: PDrawObject);
    function AlignToGridLeft(ALeft: Integer): Integer;
    function AlignToGridTop(ATop: Integer): Integer;
    procedure RescaleAllObjects(ACurrentWidth, ACurrentHeight: Integer);
    procedure RepositionSubForm(ASubForm: TForm);
    procedure GhostImageDisable;
    function WSEmpEffCreate(ADrawObject: PDrawObject;
      AShortName: String): TpnlWSEmpEff;
  public
    { Public declarations }
    PIMSDatacolTimeInterval: Integer; // in seconds
    PIMSMultiplier: Integer; // Used to multiply with 'PIMSDatacolTimeInterval'
    APlanScanClass: TPlanScanClass;
    procedure WriteLog(Mess: String);
    function DetermineEmployeeList(
      ADrawObject: PDrawObject;
      const AShowMode: TShowMode;
      const APlantCode: String;
      const AWorkspotCode: String;
      var AEmployeeList: TList;
      var AEmployeeCount: Integer): Boolean;
    procedure DetermineSinceDates(MyNow: TDateTime;
      var DateFrom, DateTo: TDateTime);
(*    function ActionEfficiencyXXX(ADrawObject: PDrawObject): TEffPeriodMode; *)
    procedure ActionEfficiency(ADrawObject: PDrawObject);
    function ActionRealTimeEff(var ADrawObject: PDrawObject): Double;
    procedure ActionRealTimeEffMain;
    function EmployeeCreate(AEmployeeNumber: Integer;
      ADrawObject: PDrawObject): PTAEmployee;
    procedure WSEmpEffEditMode(AOn: Boolean);
    property EffQuantityTime: TEffQuantityTime read FEffQuantityTime
      write FEffQuantityTime;
    // 20013548.10.
    property ScreenWidth: Integer read FScreenWidth write FScreenWidth;
    property ScreenHeight: Integer read FScreenHeight write FScreenHeight;
    property DateFromGraph: TDateTime read FDateFromGraph write FDateFromGraph;
    property DateToGraph: TDateTime read FDateToGraph write FDateToGraph;
  end;

var
  HomeF: THomeF;

implementation

{$R *.DFM}

uses
  UPimsConst,
  CalculateTotalHoursDMT,
  DialogWorkspotSelectFRM,
  DialogImagePropertiesFRM,
  DialogShowAllEmployeesFRM,
  ShowChartFRM,
  DialogSettingsFRM,
  ProductionScreenWaitFRM,
  UGlobalFunctions,
  DialogDepartmentSelectFRM,
  UPimsMessageRes,
  DialogSelectModeFRM,
  //CAR user rights
  DialogLoginFRM,
  DialogLegendaFRM,
//  DialogChangeJobDMT,
//  DialogChangeJobFRM,
  GlobalDMT,
  UTranslateProdScreen,
  UTranslateStringsProdScreen,
  DialogEmpListViewFRM,
  DialogEmpGraphFRM, RealTimeEffDMT, WorkspotEmpEffDMT;

procedure THomeF.WMTimeChange(var Message: TMessage);
begin
  PimsTimeInitialised := False;
//  ShowMessage('WMTime changed');
//  TMessage(Message).Result := Integer(True);
end;

procedure THomeF.ShowActionTime;
begin
  TimerEndTime := SysUtils.Now;
  stBarBase.Panels[3].Text :=
    TimeToStr(TimerStartTime) + '-' + TimeToStr(TimerEndTime);
  Update;
end;

procedure THomeF.WriteLog(Mess: String);
var
  TFile: TextFile;
  Exists: Boolean;
begin
  Mess := DateTimeToStr(Now) + ' - ' + Mess;

  try
    Exists := FileExists(LogPath + LOGFILENAME);
    AssignFile(TFile, LogPath + LOGFILENAME);
    try
      if not Exists then
        Rewrite(TFile)
      else
        Append(TFile);
      WriteLn(TFile, Mess);
    except
//      MemoLog.Lines.Add('Error! Cannot find/access file: ' + LogPath);
//      Update;
    end;
  finally
    CloseFile(TFile);
  end;
end;

function PathCheck(Path: String): String;
begin
  if Path <> '' then
    if Path[length(Path)] <> '\' then
      Path := Path + '\';
  Result := Path;
end;

function THomeF.AskForSave: Boolean;
begin
  Result := False;
  if Dirty then
  begin
    Result := DisplayMessage(SPimsSaveChanges,
      mtConfirmation, [mbYes, mbNo]) = mrYes;
    if Result then
      SaveList(WorkFilename);
    Dirty := False;
  end;
end;

procedure THomeF.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if Busy then
  begin
    Action := caNone;
    Exit;
  end;

  TimerMultiSelect.Enabled := False; //23466
  TimerStatus.Enabled := False;
  TimerBlink.Enabled := False;
  TimerMain.Enabled := False;

  AskForSave;

  with ProductionScreenDM do
  begin
    cdsWorkspotShortName.Close;
    odsJobcode.Active := False;
  end;

  SelectionRectangle.Free;
//  DialogWorkspotSelectF.Close;
//  DialogWorkspotSelectF.Free;

//  DialogDepartmentSelectF.Close;
//  DialogDepartmentSelectF.Free;

  // MR:02-05-2003
//  DialogShowAllEmployeesF.Free;

  // Write last used workfilename to registry
  WriteRegistry;

  APlanScanClass.Free;
end;

procedure THomeF.ShowObjectNumber(Sender: TObject);
begin
  if CurrentDrawObject = nil then Exit;
  if Sender = nil then Exit;

  if (Sender is TImage) then
  begin
    case CurrentDrawObject.AProdScreenType of
    pstNoTimeRec, pstMachine, pstWSEmpEff: // No Time Rec: Workspot or Machine // 20014450.50 // PIM-213
      begin
        stBarBase.Panels[0].Text := 'Object=' +
          IntToStr((Sender as TImage).Tag) + ' (' +
          CurrentDrawObject.AWorkspotCode + Str_Sep +
          CurrentDrawObject.AWorkspotDescription + ')';
      end;
    end;
  end
  else
    if (Sender is TShapeEx) then
      stBarBase.Panels[0].Text := 'Object=' +
        IntToStr((Sender as TShapeEx).Tag) + ' (' +
        CurrentDrawObject.AWorkspotCode + Str_Sep +
        CurrentDrawObject.AWorkspotDescription + ')';
end;

procedure THomeF.ShowChangeProperties(Sender: TObject);
begin
  if Sender = nil then
    Exit;
  ShowObjectNumber(Sender);
  if (Sender is TImage) then
  begin
    CurrentDrawObject := ThisDrawObject((Sender as TImage).Tag);
    DialogImagePropertiesF := TDialogImagePropertiesF.Create(Application);
    DialogImagePropertiesF.ImageTop := (Sender as TImage).Top;
    DialogImagePropertiesF.ImageLeft := (Sender as TImage).Left;
    DialogImagePropertiesF.ImageWidth := (Sender as TImage).Width;
    DialogImagePropertiesF.ImageHeight := (Sender as TImage).Height;
    DialogImagePropertiesF.PlantCode := CurrentDrawObject.APlantCode;
    DialogImagePropertiesF.WorkspotCode := CurrentDrawObject.AWorkspotCode;
    DialogImagePropertiesF.WorkspotDescription :=
      CurrentDrawObject.AWorkspotDescription;
    ModalResult := DialogImagePropertiesF.ShowModal;
    if ModalResult = mrOK then
    begin
      (Sender as TImage).Top := DialogImagePropertiesF.ImageTop;
      (Sender as TImage).Left := DialogImagePropertiesF.ImageLeft;
      (Sender as TImage).Height := DialogImagePropertiesF.ImageHeight;
      (Sender as TImage).Width := DialogImagePropertiesF.ImageWidth;
    end;
    DialogImagePropertiesF.Free;
  end
  else
    if (Sender is TShapeEx) then
    begin
      CurrentDrawObject := ThisDrawObject((Sender as TShapeEx).Tag);
      DialogImagePropertiesF := TDialogImagePropertiesF.Create(Application);
      DialogImagePropertiesF.ImageTop := (Sender as TShapeEx).Top;
      DialogImagePropertiesF.ImageLeft := (Sender as TShapeEx).Left;
      DialogImagePropertiesF.ImageWidth := (Sender as TShapeEx).Width;
      DialogImagePropertiesF.ImageHeight := (Sender as TShapeEx).Height;
      DialogImagePropertiesF.PlantCode := '';
      DialogImagePropertiesF.WorkspotCode := '';
      DialogImagePropertiesF.WorkspotDescription := '';
      ModalResult := DialogImagePropertiesF.ShowModal;
      if ModalResult = mrOK then
      begin
        (Sender as TShapeEx).Top := DialogImagePropertiesF.ImageTop;
        (Sender as TShapeEx).Left := DialogImagePropertiesF.ImageLeft;
        (Sender as TShapeEx).Height := DialogImagePropertiesF.ImageHeight;
        (Sender as TShapeEx).Width := DialogImagePropertiesF.ImageWidth;
      end;
      DialogImagePropertiesF.Free;
    end;
end;

procedure THomeF.ImageMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if not Assigned(Sender) then
    Exit;

  if Button = mbLeft then
    if (Sender is TShape) or
      (Sender is TLabel) or
      (Sender is TPanel) or // PIM-213
      (Sender is TdxfProgressBar) then // PIM-213
      ImageClick(Sender);

  if tlbtnEdit.Down then
    if (Sender is TButton) then
      ImageClick(Sender);

  if Button = mbRight then ImageClick(Sender);

  if tlbtnEdit.Down then
    if Button = mbLeft then
      begin
        Dirty := True;
        FDragOfX := X;
        FDragOfY := Y;
        FDragging := True;
// <23466>
        // 23466.150 Do not only look for 'TImage' here.
        if ((Sender is TImage) or (Sender is TShape) or
            (Sender is TLabel) or (Sender is TButton) or
            (Sender is TShapeEx) or
            (Sender is TdxfProgressBar) or // PIM-213
            (Sender is TPanel)) and // PIM-213
           {(ssCtrl in Shift) and} // 23466.150 Only look for shift-key
           (ssShift in Shift) then
          begin
            if (Sender is TImage) then
              SelectedDrawObject := ThisDrawObject((Sender as TImage).Tag)
            else
              if (Sender is TPanel) then
                SelectedDrawObject := ThisDrawObject((Sender as TPanel).Tag) // PIM-213
            else
              if (Sender is TShape) then
                SelectedDrawObject := ThisDrawObject((Sender as TShape).Tag)
            else
              if (Sender is TLabel) then
                SelectedDrawObject := ThisDrawObject((Sender as TLabel).Tag)
            else
              if (Sender is TButton) then
                SelectedDrawObject := ThisDrawObject((Sender as TButton).Tag)
            else
              if (Sender is TShapeEx) then
                SelectedDrawObject := ThisDrawObject((Sender as TShapeEx).Tag)
            else
              if (Sender is TdxfProgressBar) then
                SelectedDrawObject := ThisDrawObject((Sender as TdxfProgressBar).Tag);
            if Assigned(SelectedDrawObject) then
              SelectedDrawObject.Selected := True;
          end
        else
          ClearAllSelectionRectangles;
// <23466>
      end;
end; // ImageMouseDown

// Recalculate percentage to a value:
// -1 to -100 for Red Meter
// 1 to 100 for Green Meter
function THomeF.PrecalcPercentage(
  ADrawObject: PDrawObject; APercentage: Integer): Integer;
const
  Low=60;
  High=140;
var
  LowRange, HighRange: Integer;
begin
  if APercentage <> 0 then
  begin
    LowRange := 100 - Low;
    HighRange := High - 100;
    // Pre-calculate percentage
    if APercentage <= Low then
      APercentage := Low;
    if APercentage >= High then
      APercentage := High;
    // 60 (Low) to 100 should be Red-indicator
    // 101 to 140 (High) should be Green-indicator
    if (APercentage >= Low) and (APercentage < 100) then
    begin
      // -1 to -39 (-1 to -LowRange)
      APercentage := APercentage - Low;
      APercentage := LowRange - APercentage;
      APercentage := APercentage * -1;
      // Now recalculate to a value from -1 to -100
      APercentage := Trunc(APercentage * 100 / LowRange);
    end
    else
    begin
      if (APercentage >= 100) and (APercentage <= High) then
      begin
        // 1 to 40 (1 to HighRange)
        APercentage := APercentage - 100;
        // Now recalculate to a value from -1 to -100
        APercentage := Trunc(APercentage * 100 / HighRange);
      end;
    end;
  end
  else
  begin
    if (not ADrawObject.ANoData) and
      (ADrawObject.AEmployeeCount > 0) then
      APercentage := -100;
  end;
  Result := APercentage;
end;

procedure THomeF.ShowEffMeter(ADrawObject: PDrawObject; APercentage: Integer;
  Sender: TObject);
const
  AWIDTH=42;
var
  APerc: Double;
  // Show efficiency meter that consists of a red and green rectangle.
  procedure ShowEfficiencyMeter;
  begin
    if APercentage >= 0 then // APercentage is 1 to 100
    begin
      APerc := AWIDTH / 100 * APercentage;
      ADrawObject.AEffPercentage := Round(APerc);

      // Green
      ADrawObject.AEffMeterRed.Visible := False;
      with ADrawObject.AEffMeterGreen do
      begin
        BringToFront;
        Visible := True;
        Left := (Sender as TImage).Left +
          (Sender as TImage).Width DIV 2;
        if ADrawObject.ABigEffMeters then
        begin
          Top := (Sender as TImage).Top + Trunc(-5 * WorkspotScale / 100);
          Height := Trunc(8 * BIGEFFMETERS_SCALE_PERCENTAGE / 100 *
            WorkspotScale / 100);
          Width := Trunc(ADrawObject.AEffPercentage *
            BIGEFFMETERS_SCALE_PERCENTAGE / 100 * WorkspotScale / 100);
        end
        else
        begin
          Top := (Sender as TImage).Top - Trunc(11 * WorkspotScale / 100);
          Width := Trunc(ADrawObject.AEffPercentage * WorkspotScale / 100);
        end;
      end;
    end
    else
    begin
      // Red
      if APercentage < 0 then // APercentage is -1 to -100
      begin
        // First make a positive number
        APercentage := APercentage * -1;

        APerc := AWIDTH / 100 * APercentage;
        ADrawObject.AEffPercentage := Round(APerc);

        ADrawObject.AEffMeterGreen.Visible := False;
        with ADrawObject.AEffMeterRed do
        begin
          BringToFront;
          Visible := True;
          if ADrawObject.ABigEffMeters then
          begin
            Top := (Sender as TImage).Top + Trunc(-5 * WorkspotScale / 100);
            Height := Trunc(8 * BIGEFFMETERS_SCALE_PERCENTAGE / 100 *
              WorkspotScale / 100);
            Left := (Sender as TImage).Left +
              (Sender as TImage).Width DIV 2 -
              Trunc(ADrawObject.AEffPercentage *
              BIGEFFMETERS_SCALE_PERCENTAGE / 100 * WorkspotScale / 100);
            Width := Trunc(ADrawObject.AEffPercentage *
              BIGEFFMETERS_SCALE_PERCENTAGE / 100 * WorkspotScale / 100);
          end
          else
          begin
            Top := (Sender as TImage).Top - Trunc(11 * WorkspotScale / 100);
            Left := (Sender as TImage).Left +
              (Sender as TImage).Width DIV 2 - Trunc(ADrawObject.AEffPercentage *
              WorkspotScale / 100);
            Width := Trunc(ADrawObject.AEffPercentage * WorkspotScale / 100);
          end;
        end;
        PlaySound;
      end;
    end;
  end; // ShowEfficiencyMeter
  // PIM-194
  procedure ShowProgressBar;
  begin
    with ADrawObject.AProgressBar do
    begin
      BringToFront;
      Max := ORASystemDM.OrangeBoundary;
      Visible := True;
      Position := Round(ADrawObject.ATotalPercentage);
      if (Position <= ORASystemDM.RedBoundary) then
      begin
        BeginColor := clMyRed;
        EndColor := clMyRed;
      end
      else
        if (Position <= ORASystemDM.OrangeBoundary) then
        begin
          BeginColor := clMyOrange;
          EndColor := clMyOrange
        end
        else
        begin
          BeginColor := clMyGreen;
          EndColor := clMyGreen;
        end;
      if ADrawObject.ABigEffMeters then
      begin
        ShowText := True;
        Font.Size := Trunc(pnlDraw.Font.Size * 1.5 * FontScale / 100);
        if (Position < 60) then
          Font.Color := clBlack
        else
          Font.Color := clWhite;
        Left := ADrawObject.ABorder.Left + 1;
        Top := ADrawObject.ABorder.Top + 1;
        Height := ADrawObject.ABorder.Height - 2;
        Width := ADrawobject.ABorder.Width - 2;
      end
      else
      begin
        ShowText := False;
        Top := (Sender as TImage).Top - Trunc(11 * WorkspotScale / 100);
        Left := (Sender as TImage).Left;
        Height := Round(((Sender as TImage).Top - ADrawObject.ABorder.Top) * 50 / 100);
        Width := (Sender as TImage).Width;
      end;
    end;
  end; // ShowProgressBar
begin
  if Sender = nil then
    Exit;

  ADrawObject.ATotalPercentage := APercentage;
  APercentage := PrecalcPercentage(ADrawObject, APercentage);

  if ADrawObject.ABigEffMeters then
  begin
    if ADrawObject.ANoData then
      ADrawObject.ABigEffLabel.Caption := SPimsNoData
    else
      ADrawObject.ABigEffLabel.Caption := '';
  end;

  // PIM-194
  ShowProgressBar;
  // ShowEfficiencyMeter;

end;

procedure THomeF.ShowPuppets(ADrawObject: PDrawObject; Sender: TObject);
var
  I, J, ALeft, ATop, AMaxWidth: Integer;
  APuppetShift: PTPuppetShift;
  APuppet: PTPuppet;
begin
  if ADrawObject = nil then
    Exit;
  if Sender = nil then
    Exit;
  if (ADrawObject.AProdScreenType = pstWSEmpEff) then // PIM-213
    Exit;

  ATop := 0;
  ALeft := 0;
  AMaxWidth := 0;
  // If Sender = TShapeEx then Sender is a 'PuppetBox'.
  // In that case, the shifts don't matter, all puppets must be shown
  // in space of the rectangle.
  if (Sender is TShapeEx) then
  begin
    AMaxWidth := (Sender as TShapeEx).Left +
      ADrawObject.ADeptRect.ATopLineHorz.Width;
  end;
  // Show all puppets of all shifts
  if ADrawObject.APuppetShiftList.Count > 0 then
  begin
    if (Sender is TImage) then
      ATop := (Sender as TImage).Top + (Sender as TImage).Height +
        Trunc(24 * WorkspotScale / 100)
    else
      if (Sender is TShapeEx) then // PuppetBox
        ATop := (Sender as TShapeEx).Top + Trunc(5 * WorkspotScale / 100);
    for I := 0 to ADrawObject.APuppetShiftList.Count - 1 do
    begin
      if (Sender is TImage) then
        ALeft := (Sender as TImage).Left -
          Trunc(15 * WorkspotScale / 100)
      else
        if (Sender is TShapeEx) then // PuppetBox
          if (I = 0) then // Do only first time!
            ALeft := (Sender as TShapeEx).Left +
              Trunc(5 * WorkspotScale / 100);
      APuppetShift := ADrawObject.APuppetShiftList.Items[I];
      // One row shows all puppets of one Shift!
      if APuppetShift.APuppetList.Count > 0 then
      begin
        for J := 0 to APuppetShift.APuppetList.Count - 1 do
        begin
          APuppet := APuppetShift.APuppetList.Items[J];
          ShowPuppet(APuppet, ALeft, ATop, APuppet.AColor);
          ALeft := ALeft +
            Trunc(15 * WorkspotScale / 100);
          if (Sender is TShapeEx) then // PuppetBox
          begin
            if ((ALeft + Trunc(15 * WorkspotScale / 100)) >= AMaxWidth) then
            begin
              // Go to next row on following line, at left position.
              ALeft := (Sender as TShapeEx).Left +
                Trunc(5 * WorkspotScale / 100);
              ATop := ATop + Trunc(32 * WorkspotScale / 100);
            end;
          end;
        end;
      end;
      // Next shift on next row!
      if (Sender is TImage) then
        ATop := ATop + Trunc(32 * WorkspotScale / 100);
    end;
  end;
end;

procedure THomeF.ShowEmployeeList(ADrawObject: PDrawObject);
var
  APTAEmployee: PTAEmployee;
  I, Top, Left: Integer;
  FontSize: Integer;
  TopAdd: Integer;
begin
  if ADrawObject = nil then
    Exit;
  if not ADrawObject.ABigEffMeters then
    Exit;

  case ADrawObject.AProdScreenType of
  pstWorkspotTimeRec: // Workspot + Time Rec
    begin
      FontSize := Round(12 * FontScale / 100);
      TopAdd := Round(FontSize * 1.5);
      Top := ADrawObject.ABorder.Top + ADrawObject.ABorder.Height;
    end;
  else // Default
    begin
      FontSize := 10; // No scaling for font
      TopAdd := Round(FontSize * 1.5);
      Top := ADrawObject.ABorder.Top + ADrawObject.ABorder.Height +
        ADrawObject.AEffTotalsLabel.Font.Size * 2;
    end;
  end;

  if Assigned(ADrawObject.AEmployeeList) then
    if ADrawObject.AEmployeeList.Count > 0 then
    begin
      Left := ADrawObject.ABorder.Left;
      for I := 0 to ADrawObject.AEmployeeList.Count - 1 do
      begin
        APTAEmployee := ADrawObject.AEmployeeList.Items[I];
        // Name
        APTAEmployee.AEmployeeNameLabel.Font.Size := FontSize;
        APTAEmployee.AEmployeeNameLabel.Top := Top;
        APTAEmployee.AEmployeeNameLabel.Left := Left  +
          Round(ADrawObject.ABorder.Width * 1/30);
        // Next employee on next row
        Top := Top + TopAdd;
      end;
    end;
end; // ShowEmployeeList

procedure THomeF.ShowDrawObject(ADrawObject: PDrawObject; Sender: TObject);

  procedure ShowBorder(AVisible: Boolean; ATop, ALeft, AWidth, AHeight: Integer);
  begin
    // Border
    ADrawObject.ABorder.Visible := AVisible;
    ADrawObject.ABorder.Top := (Sender as TImage).Top + Trunc(ATop * WorkspotScale / 100);
    ADrawObject.ABorder.Left := (Sender as TImage).Left + Trunc(ALeft * WorkspotScale / 100);
    ADrawObject.ABorder.Width := (Sender as TImage).Width + Trunc(AWidth * WorkspotScale / 100);
    ADrawObject.ABorder.Height := (Sender as TImage).Height + Trunc(AHeight * WorkspotScale / 100);
  end;
  procedure ShowLabel(AVisible: Boolean;
    ATop, ALeft, AWidth, AHeight: Integer);
  begin
    // Show Label
    ADrawObject.ALabel.Visible := AVisible;
    ADrawObject.ALabel.Font.Size :=
      Trunc(pnlDraw.Font.Size * 2.8 * WorkspotScale / 100);
    ADrawObject.ALabel.Top := (Sender as TImage).Top +
      Trunc(ATop * WorkspotScale / 100);
    ADrawObject.ALabel.Left := (Sender as TImage).Left +
      Trunc(ALeft * WorkspotScale / 100);
  end;
  procedure ShowEffTotalsLabel(AVisible: Boolean;
    ATop, ALeft, AWidth, AHeight: Integer);
  begin
    // Show Eff Totals Label
    // Totals for workspot
    // Show top-line in larger font
    ADrawObject.AEffTotalsLabel.Visible := AVisible;
    ADrawObject.AEffTotalsLabel.Font.Size :=
      Trunc(pnlDraw.Font.Size * 1.5 * FontScale / 100);
    ADrawObject.AEffTotalsLabel.Top :=
      ADrawObject.ABorder.Top + ADrawObject.ABorder.Height +
      Trunc(ATop * WorkspotScale / 100);
    ADrawObject.AEffTotalsLabel.Left :=
      ADrawObject.ABorder.Left;
  end;
  procedure ShowEffMeter(AVisible: Boolean;
    ATop, ALeft, AWidth, AHeight: Integer);
  begin
    ADrawObject.AEffMeterGreen.Visible := AVisible;
    ADrawObject.AEffMeterRed.Visible := AVisible;
    // Efficiency Meter
    // RV075.1 ATop: Add ATop, not subtract.
    ADrawObject.AEffMeterGreen.Top := (Sender as TImage).Top +
      Trunc(ATop * WorkspotScale / 100);
    ADrawObject.AEffMeterGreen.Left := (Sender as TImage).Left +
      (Sender as TImage).Width DIV 2;
    ADrawObject.AEffMeterGreen.Height :=
      Trunc(AHeight * BIGEFFMETERS_SCALE_PERCENTAGE / 100 * WorkspotScale / 100);
    ADrawObject.AEffMeterRed.Top := (Sender as TImage).Top +
      Trunc(ATop * WorkspotScale / 100);
    ADrawObject.AEffMeterRed.Left := (Sender as TImage).Left +
      (Sender as TImage).Width DIV 2 - Trunc(ADrawObject.AEffPercentage *
      BIGEFFMETERS_SCALE_PERCENTAGE / 100 * WorkspotScale / 100);
    ADrawObject.AEffMeterRed.Height :=
      Trunc(AHeight * BIGEFFMETERS_SCALE_PERCENTAGE / 100 * WorkspotScale / 100);
  end;
  // PIM-194
  procedure ShowProgressBar(AVisible: Boolean;
    ATop, ALeft, AWidth, AHeight: Integer);
  begin
    with ADrawObject.AProgressBar do
    begin
      Visible := AVisible;
      Top := ADrawObject.ABorder.Top + 1;
      Left := ADrawObject.ABorder.Left + 1;
      Height := ADrawObject.ABorder.Height - 2;
      Width := ADrawobject.ABorder.Width - 2;
    end;
  end; // ShowProgressBar
  procedure ShowBigEffLabel(AVisible: Boolean;
    ATop, ALeft, AWidth, AHeight: Integer);
  begin
    // Big Efficiency Meter Label
    ADrawObject.ABigEffLabel.Visible := AVisible;
    ADrawObject.ABigEffLabel.Font.Size :=
      Trunc(pnlDraw.Font.Size * 2.8 * WorkspotScale / 100);
    ADrawObject.ABigEffLabel.Left := ADrawObject.ABorder.Left +
      Trunc(ALeft * WorkspotScale / 100);
    ADrawObject.ABigEffLabel.Top := ADrawObject.ABorder.Top;
  end;
  procedure ShowMinPercLabel(ATop, ALeft: Integer);
  begin
    if Assigned(ADrawObject.AMinPercLabel) then
    begin
      // 20013551.10 Also subtract label.height
      ADrawObject.AMinPercLabel.Top := (ADrawObject.AObject as TImage).Top +
        Trunc(ATop * WorkspotScale / 100) - ADrawObject.AMinPercLabel.Height;
      ADrawObject.AMinPercLabel.Left := (ADrawObject.AObject as TImage).Left +
        Trunc(ALeft * WorkspotScale / 100);
    end;
  end; // ShowMinPercLabel
  procedure ShowMaxDeviationLabel(ATop, ALeft: Integer);
  begin
    if Assigned(ADrawObject.AMaxDeviationLabel) then
    begin
      // 20013549.10 Also subtract label.height
      ADrawObject.AMaxDeviationLabel.Top := (ADrawObject.AObject as TImage).Top +
        Trunc(ATop * WorkspotScale / 100) - ADrawObject.AMaxDeviationLabel.Height;
      ADrawObject.AMaxDeviationLabel.Left := (ADrawObject.AObject as TImage).Left +
        Trunc(ALeft * WorkspotScale / 100);
    end;
  end; // ShowMaxDeviationLabel
  // PIM-213
  procedure ShowWSEmpEffEmployees;
  var
    APTAEmployee: PTAEmployee;
    I: Integer;
    // PIM-213
    procedure ActionWSEmpEff;
    var
      APTAEmployee: PTAEmployee;
      A30MinEff, A15MinEff, A60MinEff, ATodayEff: Integer;
      Qty: Integer;
      I: Integer;
    begin
      if not (ADrawObject.AProdScreenType = pstWSEmpEff) then
        Exit;
      if not Assigned(ADrawObject.ApnlWSEmpEff) then
        Exit;

      if Assigned(ADrawObject.AEmployeeList) then
        if ADrawObject.AEmployeeList.Count > 0 then
        begin
          for I := 0 to ADrawObject.AEmployeeList.Count - 1 do
          begin
            APTAEmployee := ADrawObject.AEmployeeList.Items[I];
            // Is employee still scanned in?
            if WorkspotEmpEffDM.FindCurrentEmp(ADrawObject.APlantCode,
              ADrawObject.AWorkspotCode, APTAEmployee.AEmployeeNumber) then
            begin
              WorkspotEmpEffDM.WorkspotEmpEffGetValues(ADrawObject.APlantCode,
                ADrawObject.AWorkspotCode, APTAEmployee.AEmployeeNumber,
                A15MinEff, A30MinEff, A60MinEff, ATodayEff);
              ADrawObject.ApnlWSEmpEff.ShowEff(
                APTAEmployee.AEmployeeNumber, A15MinEff, A30MinEff, A60MinEff,
                  ATodayEff);
              // PIM-223
              if ORASystemDM.PSShowEmpInfo then
                ADrawObject.ApnlWSEmpEff.
                  ShowEmpName(APTAEmployee.AEmployeeNumber)
              else
                ADrawObject.ApnlWSEmpEff.
                  HideEmpName(APTAEmployee.AEmployeeNumber);
            end;
          end;
        end;
      WorkspotEmpEffDM.WorkspotQtyGetValues(ADrawObject.APlantCode,
        ADrawObject.AWorkspotCode, Qty);
      ADrawObject.ApnlWSEmpEff.ShowSummary(Qty);
    end; // ActionWSEmpEff
  begin
    if tlbtnEdit.Down then
      Exit;
    if Assigned(ADrawObject.AEmployeeList) then
      if ADrawObject.AEmployeeList.Count > 0 then
      begin
        for I := 0 to ADrawObject.AEmployeeList.Count - 1 do
        begin
          APTAEmployee := ADrawObject.AEmployeeList.Items[I];
          // Is employee still scanned in?
          if WorkspotEmpEffDM.FindCurrentEmp(ADrawObject.APlantCode,
            ADrawObject.AWorkspotCode, APTAEmployee.AEmployeeNumber) then
          begin
            APTAEmployee.AEmployeeNameLabel.Visible := False;
            if not Assigned(ADrawObject.ApnlWSEmpEff.FindEmployee(APTAEmployee.AEmployeeNumber)) then
              ADrawObject.ApnlWSEmpEff.AddEmployee(
                APTAEmployee.AEmployeeNumber,
                APTAEmployee.AEmployeeNameLabel.Caption,
                ImageClick, ImageMouseDown, ImageMouseMove, ImageMouseUp,
                (ADrawObject.AObject as TImage).Tag);
          end
          else
            ADrawObject.ApnlWSEmpEff.DelEmployee(APTAEmployee.AEmployeeNumber);
        end;
      end;
    ADrawObject.ApnlWSEmpEff.RefreshEmployees;
    ActionWSEmpEff;
  end; // ShowWSEmpEffEmployees
  // PIM-213
  procedure ShowWSEmpEff(AVisible: Boolean;
    ATop, ALeft, AWidth, AHeight: Integer);
  begin
    if tlbtnEdit.Down then
      Exit;
    // Defer updates
//    SendMessage(Handle, WM_SETREDRAW, WPARAM(False), 0);
    try
      ADrawObject.ALabel.Caption := '';
      ADrawObject.ALabel.Visible := False;
      (Sender as TImage).Visible := False;
      (ADrawObject.AObject as TImage).Visible := False;
      if Assigned(ADrawObject.ADefaultImage) then
        ADrawObject.ADefaultImage.Visible := False;
      ADrawObject.AGhostImage.Visible := False;
      with ADrawObject.ApnlWSEmpEff do
      begin
        Visible := AVisible;
      end;
      ShowWSEmpEffEmployees;
      ADrawObject.ApnlWSEmpEff.ShowTime;
    finally
      // Make sure updates are re-enabled
//      SendMessage(Handle, WM_SETREDRAW, WPARAM(True), 0);
      // Invalidate;  // Might be required to reflect the changes
    end;
  end; // ShowWSEmpEff
  procedure ShowWSTrafficLights;
  begin
    // Light Red
    ADrawObject.ALightRed.Top := (Sender as TImage).Top +
      (Sender as TImage).Height +
      Trunc(2 * WorkspotScale / 100);
    ADrawObject.ALightRed.Left := (Sender as TImage).Left;
    ADrawObject.ALightRed.Visible := True;

    // Light Yellow
    ADrawObject.ALightYellow.Top := (Sender as TImage).Top +
      (Sender as TImage).Height +
      Trunc(2 * WorkspotScale / 100);
    ADrawObject.ALightYellow.Left := (Sender as TImage).Left +
      Trunc((Sender as TImage).Width / 2 -  ADrawObject.ALightYellow.Width
      / 2);
    ADrawObject.ALightYellow.Visible := True;

    // Light Green
    ADrawObject.ALightGreen.Top := (Sender as TImage).Top +
      (Sender as TImage).Height +
      Trunc(2 * WorkspotScale / 100);
    ADrawObject.ALightGreen.Left := (Sender as TImage).Left +
      Trunc((Sender as TImage).Width - ADrawObject.ALightGreen.Width);
    ADrawObject.ALightGreen.Visible := True;

    // Efficiency Meter
    ADrawObject.AEffMeterGreen.Top := (Sender as TImage).Top -
      Trunc(11 * WorkspotScale / 100);
    ADrawObject.AEffMeterGreen.Left := (Sender as TImage).Left +
      (Sender as TImage).Width DIV 2;
    ADrawObject.AEffMeterRed.Top := (Sender as TImage).Top -
      Trunc(11 * WorkspotScale / 100);
    ADrawObject.AEffMeterRed.Left := (Sender as TImage).Left +
      (Sender as TImage).Width DIV 2 - Trunc(ADrawObject.AEffPercentage *
      WorkspotScale / 100);

    // Border
    ADrawObject.ABorder.Visible := True;
    ADrawObject.ABorder.Top := (Sender as TImage).Top -
      Trunc(16 * WorkspotScale / 100);
    ADrawObject.ABorder.Left := (Sender as TImage).Left -
      Trunc(16 * WorkspotScale / 100);
    ADrawObject.ABorder.Width := (Sender as TImage).Width +
      Trunc(32 * WorkspotScale / 100);
    ADrawObject.ABorder.Height := (Sender as TImage).Height +
      Trunc(38 * WorkspotScale / 100);

    // Label
    // Scale Font of Label, but it cannot get too small!
    ADrawObject.ALabel.Visible := True;
    ADrawObject.ALabel.Font.Size := NewFontSize;
    ADrawObject.ALabel.Left := ADrawObject.ABorder.Left;
    ADrawObject.ALabel.Top := ADrawObject.ABorder.Top -
      Trunc(ADrawObject.ALabel.Height +
        ADrawObject.ALabel.Height / 2 * WorkspotScale / 100);

    // 20013551
    if Assigned(ADrawObject.AMinPercLabel) then
    begin
      if ADrawObject.AMinPercLabel.Visible then
        ShowMinPercLabel(-40, -15); // -100, -15 // 20013551.10
    end;
    // 20013549
    if Assigned(ADrawObject.AMaxDeviationLabel) then
    begin
      if ADrawObject.AMaxDeviationLabel.Visible then
        ShowMaxDeviationLabel(-40, -15); // -100, -15 // 20013549.10
    end;
    ShowPuppets(ADrawObject, Sender);
  end; // ShowWSTrafficLights
begin
  if Assigned(ADrawObject) then
  begin
    if not Assigned(Sender) then
      Exit;
    ShowObjectNumber(Sender);
    if (Sender is TImage) then
    begin
      // BIG Efficiency Meters
      if ADrawObject.ABigEffMeters then
      begin
        // Don't show picture
        (Sender as TImage).Visible := False;

        // !!!TESTING!!!
//        (Sender as TImage).Visible := True;
        //
        // No Time Rec on workspot- or machine-level
        //
        case ADrawObject.AProdScreenType of
        pstNoTimeRec, pstMachine: // No Time Rec: Workspot or Machine // 20014450.50
          begin
            ShowBorder(True, -15, -143, 286, 15);
            ShowLabel(True, -15, -300, 0, 0);
            ShowEffTotalsLabel(True, 4, 0, 0, 0);
            ShowEffMeter(False, -5, 0, 0, 8); // RV075.1. 5 -> -5
            ShowProgressBar(True, -5, 0, 0, 8);
            ShowBigEffLabel(True, 0, 10, 0, 0);
          end;
        end; // ADrawObject.AProdScreenType

        ShowEmployeeList(ADrawObject);
      end
      else
      begin
        if (ADrawObject.AProdScreenType = pstWSEmpEff) then // PIM-213
        begin
          ShowBorder(False, 0, 0, 0, 0);
          ShowLabel(False, 0, 0, 0, 0);
          ShowEffTotalsLabel(False, 0, 0, 0, 0);
          ShowEffMeter(False, 0, 0, 0, 8);
          ShowProgressBar(False, 0, 0, 0, 8);
          ShowBigEffLabel(False, 0, 0, 0, 0);
          ShowWSEmpEff(True, 0, 0, 0, 0);
        end
        else
          ShowWSTrafficLights;
      end;
    end
    else
    begin
      if (Sender is TShapeEx) then
      begin
        ShowRectangle(ADrawObject);
        if ADrawObject.AType = otPuppetBox then
          if ADrawObject.AShowMode <> ShowHeadCount then
            ShowPuppets(ADrawObject, Sender);
      end;
    end;
  end;
end; // ShowDrawObject

procedure THomeF.ImageMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);

var i : integer;

  procedure DoMoveObject(SelDrawObj : PDrawObject);
  begin
    if not Assigned(SelDrawObj) then
      Exit;
    if (SelDrawObj.AObject is TImage) then
      begin
        // PIM-151
        if Assigned(SelDrawObj.ADefaultImage) then
          SelDrawObj.AObject := SelDrawObj.ADefaultImage;
{$IFDEF DEBUG}
WLog(
  'TImage.Start ' +
  ' Left=' + IntToStr((SelDrawObj.AObject as TImage).Left) + ' Top=' + IntToStr((SelDrawObj.AObject as TImage).Top) +
  ' X=' + IntToStr(X) + ' Y=' + IntToStr(Y) + ' DragOfX=' + IntToStr(FDragOfX) + ' DragOfY=' + IntToStr(FDragOfY)
  );
{$ENDIF}
        (SelDrawObj.AObject as TImage).Left := (SelDrawObj.AObject as TImage).Left + X - FDragOfX;
        (SelDrawObj.AObject as TImage).Top := (SelDrawObj.AObject as TImage).Top + Y - FDragOfY;
        // 20013550 Align to grid
        (SelDrawObj.AObject as TImage).Left := AlignToGridLeft((SelDrawObj.AObject as TImage).Left);
        (SelDrawObj.AObject as TImage).Top := AlignToGridTop((SelDrawObj.AObject as TImage).Top);
        // PIM-151
        SelDrawObj.AGhostImage.Left := (SelDrawObj.AObject as TImage).Left;
        SelDrawObj.AGhostImage.Top := (SelDrawObj.AObject as TImage).Top;
        // PIM-194
        if Assigned(SelDrawObj.AProgressBar) then
        begin
          SelDrawObj.AProgressBar.Left := (SelDrawObj.AObject as TImage).Left;
          SelDrawObj.AProgressBar.Top := (SelDrawObj.AObject as TImage).Top;
        end;
        // PIM-213
        if (SelDrawObj.AProdScreenType = pstWSEmpEff) then
          if Assigned(SelDrawObj.ApnlWSEmpEff) then
          begin
            SelDrawObj.ApnlWSEmpEff.Left := SelDrawObj.ApnlWSEmpEff.Left + X - FDragOfX;
            SelDrawObj.ApnlWSEmpEff.Top := SelDrawObj.ApnlWSEmpEff.Top + Y - FDragOfY;
            (SelDrawObj.AObject as TImage).Left := SelDrawObj.ApnlWSEmpEff.Left;
            (SelDrawObj.AObject as TImage).Top := SelDrawObj.ApnlWSEmpEff.Top;
            (SelDrawObj.AObject as TImage).Visible := False;
          end;
{$IFDEF DEBUG}
WLog(
  'TImage.End ' +
  ' Left=' + IntToStr((SelDrawObj.AObject as TImage).Left) + ' Top=' + IntToStr((SelDrawObj.AObject as TImage).Top) +
  ' X=' + IntToStr(X) + ' Y=' + IntToStr(Y) + ' DragOfX=' + IntToStr(FDragOfX) + ' DragOfY=' + IntToStr(FDragOfY)
  );
{$ENDIF}
      end
    else
      if (SelDrawObj.AObject is TShapeEx) then
        begin
{$IFDEF DEBUG}
WLog(
  'TShapeEx.Start ' +
  ' Left=' + IntToStr((SelDrawObj.AObject as TShapeEx).Left) + ' Top=' + IntToStr((SelDrawObj.AObject as TShapeEx).Top) +
  ' X=' + IntToStr(X) + ' Y=' + IntToStr(Y) + ' DragOfX=' + IntToStr(FDragOfX) + ' DragOfY=' + IntToStr(FDragOfY)
  );
{$ENDIF}
          (SelDrawObj.AObject as TShapeEx).Left := (SelDrawObj.AObject as TShapeEx).Left + X - FDragOfX;
          (SelDrawObj.AObject as TShapeEx).Top := (SelDrawObj.AObject as TShapeEx).Top + Y - FDragOfY;
        // 20013550 Align to grid
        // TD-23466.150 The Align must also be done for the TShapeEx! Or it will give move too much
        (SelDrawObj.AObject as TShapeEx).Left := AlignToGridLeft((SelDrawObj.AObject as TShapeEx).Left);
        (SelDrawObj.AObject as TShapeEx).Top := AlignToGridTop((SelDrawObj.AObject as TShapeEx).Top);
{$IFDEF DEBUG}
WLog(
  'TShapeEx.End ' +
  ' Left=' + IntToStr((SelDrawObj.AObject as TShapeEx).Left) + ' Top=' + IntToStr((SelDrawObj.AObject as TShapeEx).Top) +
  ' X=' + IntToStr(X) + ' Y=' + IntToStr(Y) + ' DragOfX=' + IntToStr(FDragOfX) + ' DragOfY=' + IntToStr(FDragOfY)
  );
{$ENDIF}
        end;
  end;

begin
  if not tlbtnEdit.Down then
    Exit;
  if not Assigned(Sender) then
    Exit;

  if FDragging then
    begin
      if (Sender is TShape) then
        CurrentDrawObject := ThisDrawObject((Sender as TShape).Tag)
      else
        if (Sender is TLabel) then
          CurrentDrawObject := ThisDrawObject((Sender as TLabel).Tag)
        else
          if (Sender is TImage) then
            CurrentDrawObject := ThisDrawObject((Sender as TImage).Tag)
          else
            if (Sender is TShapeEx) then
              CurrentDrawObject := ThisDrawObject((Sender as TShapeEx).Tag)
            else
              if (Sender is TButton) then
                CurrentDrawObject := ThisDrawObject((Sender as TButton).Tag)
              else
                if (Sender is TdxfProgressBar) then
                  CurrentDrawObject := ThisDrawObject((Sender as TdxfProgressBar).Tag)
                else
                  if (Sender is TPanel) then // PIM-213
                    CurrentDrawObject := ThisDrawObject((Sender as TPanel).Tag);

      // Resize Object (Resize Height for Rectangle)
      // CTRL + LeftMouseButton
      if (ssCtrl in Shift) and
         (ssLeft in Shift)
         and not (ssShift in Shift) // 23466
         then
        begin
          if Assigned(CurrentDrawObject) then
            begin
              if CurrentDrawObject.AObject is TShapeEx then
                begin
                  case CurrentDrawObject.AType of
                    otLineHorz: (CurrentDrawObject.AObject as TShapeEx).Width := X;
                    otRectangle, otPuppetBox:
                      begin
                        (CurrentDrawObject.AObject as TShapeEx).Height :=
                            (CurrentDrawObject.AObject as TShapeEx).Height + Y - FDragOfY;
                        // SO-20013563
                        if (CurrentDrawObject.AType = otPuppetBox) and
                           (CurrentDrawObject.AShowMode = ShowHeadCount) then
                          begin
                            // Adjust height of cross
                            if Assigned(CurrentDrawObject.ACross) then
                              CurrentDrawObject.ACross.ALineVert.Height :=
                                  CurrentDrawObject.ACross.ALineVert.Height + Y - FDragOfY;
                          end;
                      end;
                    otLineVert: (CurrentDrawObject.AObject as TShapeEx).Height := Y;
                  end; {case}
                end;
            end;
        end
      else
        begin
          // Resize Width (of Rectangle or LineHorz)
          // ALT + LeftMouseButton
          if (ssAlt in Shift) and (ssLeft in Shift) then
            begin
              if Assigned(CurrentDrawObject) then
                begin
                  if CurrentDrawObject.AObject is TShapeEx then
                    begin
                      case CurrentDrawObject.AType of
                        otLineHorz: (CurrentDrawObject.AObject as TShapeEx).Width := X;
                        otRectangle, otPuppetBox:
                          begin
                            (CurrentDrawObject.AObject as TShapeEx).Width :=
                                (CurrentDrawObject.AObject as TShapeEx).Width + X - FDragOfX;
                            // SO-20013563
                            if (CurrentDrawObject.AType = otPuppetBox) and
                               (CurrentDrawObject.AShowMode = ShowHeadCount) then
                              begin
                                if Assigned(CurrentDrawObject.ACross) then
                                  CurrentDrawObject.ACross.ALineHorz.Width :=
                                      CurrentDrawObject.ACross.ALineHorz.Width + X - FDragOfX;
                              end;
                          end;
                      end; {case}
                  end;
                end;
            end
          else
            // Move Object
            // LeftMouseButton
            if (ssLeft in Shift) then
              begin
// <23466>
                if {(ssCtrl in Shift) and } (ssShift in Shift) then // 23466.150 Only look for Shift
                  begin
                    for i := 0 to List.Count - 1 do
                      begin
                        SelectedDrawObject := List.Items[i];
                        if SelectedDrawObject.Selected then
                          begin
                            DoMoveObject(SelectedDrawObject);
                            ShowDrawObject(SelectedDrawObject, SelectedDrawObject.AObject);
                            ShowSelectionRectangle(SelectedDrawObject.AObject, SelectedDrawObject.SelectionRectangle);
                          end
                      end;
                  end
                else
                  begin
                    ClearAllSelectionRectangles;
// </23466>
                    DoMoveObject(CurrentDrawObject);
                  end;
              end;
        end;

// <23466>
      if not ({(ssCtrl in Shift) and }(ssShift in Shift)) then // 23466.150 Only look for Shift
// </23466>
      if Assigned(CurrentDrawObject) then
        begin
          ShowDrawObject(CurrentDrawObject, CurrentDrawObject.AObject);
          ShowSelectionRectangle(CurrentDrawObject.AObject, SelectionRectangle);
        end;
    end;
end; // ImageMouseMove

procedure THomeF.ImageMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  FDragging := False;
end;

function THomeF.ThisDrawObject(ATag: Integer): PDrawObject;
var
  I, Index: Integer;
  ADrawObject: PDrawObject;
begin
  Index := -1;
  for I := 0 to List.Count - 1 do
  begin
    ADrawObject := List.Items[I];
    if (ADrawObject.AObject is TImage) then
    begin
      if (ADrawObject.AObject as TImage).Tag = ATag then
      begin
        Index := I;
        Break;
      end;
    end
    else
      if (ADrawObject.AObject is TShapeEx) then
      begin
        if (ADrawObject.AObject as TShapeEx).Tag = ATag then
        begin
          Index := I;
          Break;
        end;
      end;
  end;
  if Index = -1 then
    Result := nil
  else
    Result := List.Items[Index];
end; // ImageMouseUp

// Set SelectionRectangle to Current Object-Position
procedure THomeF.ShowSelectionRectangle(Sender: TObject; SelectRect : TShapeEx);

var ATop, ALeft, AWidth, AHeight: Integer;

  procedure ShowSelectedObjSelRect(sDrawObject : PDrawObject; SelRect: TShapeEx);
  begin
    if not Assigned(sDrawObject) then
      Exit;
    if not tlbtnEdit.Down then
      Exit;

    ATop := 19; ALeft := 19; AWidth := 39; AHeight := 45;
    if sDrawObject.ABigEffMeters then
    begin
      case sDrawObject.AProdScreenType of
        pstNoTimeRec, pstMachine: begin ATop := 17; ALeft := 145; AWidth := 291; AHeight := 20; end; // No Time Rec: Workspot
        pstMachineTimeRec:  begin ATop := 65; ALeft := 270; AWidth := 546; AHeight := 25; end; // Machine + Time Rec
        pstWorkspotTimeRec: begin ATop := 65; ALeft := 270; AWidth := 546; AHeight := 25; end; // Workspot + Time Rec
      end; {case}
    end;

    // PIM-213
    if (sDrawObject.AProdScreenType = pstWSEmpEff) then
    begin
      if Assigned(sDrawObject.ApnlWSEmpEff) then
      begin
        SelRect.Top := sDrawObject.ApnlWSEmpEff.Top - 1;
        SelRect.Left := sDrawObject.ApnlWSEmpEff.Left - 1;
        SelRect.Width := sDrawObject.ApnlWSEmpEff.Width + 2;
        SelRect.Height := sDrawObject.ApnlWSEmpEff.Height + 2;
      end;
    end
    else
    begin
      SelRect.Top := (Sender as TImage).Top - Trunc(ATop * WorkspotScale / 100);
      SelRect.Left := (Sender as TImage).Left - Trunc(ALeft * WorkspotScale / 100);
      SelRect.Width := (Sender as TImage).Width + Trunc(AWidth * WorkspotScale / 100);
      SelRect.Height := (Sender as TImage).Height + Trunc(AHeight * WorkspotScale / 100);
    end;
    SelRect.Visible := True;
  end;

begin
  if not Assigned(Sender) then
    Exit;
  // 20013516 Only show when editing.
  try
    if (Sender is TImage) then
      begin
        CurrentDrawObject := ThisDrawObject((Sender as TImage).Tag);
        ShowSelectedObjSelRect(CurrentDrawObject, SelectRect);
      end
    else
      if (Sender is TShapeEx) then
        begin
          SelectionRectangle.Visible := True;
          // Note: Line-height
          SelectionRectangle.Top := (Sender as TShapeEx).Top - 3;
          SelectionRectangle.Left := (Sender as TShapeEx).Left - 3;
          SelectionRectangle.Width := (Sender as TShapeEx).Width + 6;
          SelectionRectangle.Height := (Sender as TShapeEx).Height + 6;
          CurrentDrawObject := ThisDrawObject((Sender as TShapeEx).Tag);
          if CurrentDrawObject <> nil then
            begin
              if not tlbtnEdit.Down then Exit;
              if CurrentDrawObject.AType = otLineHorz then
                begin
                  SelectionRectangle.Top := SelectionRectangle.Top + SelectionRectangle.Height DIV 2 - 4;
                  SelectionRectangle.Height := 9;
                end
              else
                if CurrentDrawObject.AType = otLineVert then
                  begin
                    SelectionRectangle.Left := SelectionRectangle.Left + SelectionRectangle.Width DIV 2 - 4;
                    SelectionRectangle.Width := 9;
                  end
                else
                  if (CurrentDrawObject.AType = otRectangle) or
                     (CurrentDrawObject.AType = otPuppetBox) then
                    begin
                      SelectionRectangle.Top := CurrentDrawObject.ADeptRect.ATopLineHorz.Top - 2;
                      SelectionRectangle.Left := CurrentDrawObject.ADeptRect.ATopLineHorz.Left - 2;
                      SelectionRectangle.Width := CurrentDrawObject.ADeptRect.ATopLineHorz.Width + 5;
                      SelectionRectangle.Height := CurrentDrawObject.ADeptRect.ARightLineVert.Height + 5;
                    end;
            end;
        end
      else
        begin
          SelectionRectangle.Visible := False;
          ClearAllSelectionRectangles; //23466
        end;
  finally
    if not tlbtnEdit.Down then //SelectionRectangle.Visible := False;
// <23466>
      begin
        SelectionRectangle.Visible := False;
        ClearAllSelectionRectangles;
      end;
// </23466>
    ShowObjectNumber(Sender);
  end;
end; // ShowSelectionRectangle

procedure THomeF.ShowFirstObject;
var
  ADrawObject: PDrawObject;
begin
  if List.Count > 0 then
    begin
      ADrawObject := List.Items[0];
      ShowSelectionRectangle(ADrawObject.AObject, SelectionRectangle);
    end;
end;

procedure THomeF.ImageClick(Sender: TObject);
var
  I: Integer;
begin
  if not Assigned(Sender) then
    Exit;

  if (Sender is TPanel) then
  begin
    stBarBase.Panels[0].Text := 'Object=' +
      IntToStr((Sender as TPanel).Tag);
    CurrentDrawObject := ThisDrawObject((Sender as TPanel).Tag);
  end
  else
  if (Sender is TShape) then
  begin
    stBarBase.Panels[0].Text := 'Object=' +
      IntToStr((Sender as TShape).Tag);
    CurrentDrawObject := ThisDrawObject((Sender as TShape).Tag);
  end
  else
  if (Sender is TButton) then
    CurrentDrawObject := ThisDrawObject((Sender as TButton).Tag)
  else
  if (Sender is TLabel) then
  begin
    stBarBase.Panels[0].Text := 'Object=' + IntToStr((Sender as TLabel).Tag);
    CurrentDrawObject := ThisDrawObject((Sender as TLabel).Tag);
    if not tlbtnEdit.Down then
    begin
      if CurrentDrawObject <> nil then
        if (CurrentDrawObject.AProdScreenType <> pstMachineTimeRec) then
          ShowChartClick(Sender);
    end;
  end
  else
  if (Sender is TImage) then
  begin
    stBarBase.Panels[0].Text := 'Object=' +
      IntToStr((Sender as TImage).Tag);
    CurrentDrawObject := ThisDrawObject((Sender as TImage).Tag);
  end
  else
  if (Sender is TShapeEx) then
  begin
    stBarBase.Panels[0].Text := 'Object=' +
      IntToStr((Sender as TShapeEx).Tag);
    CurrentDrawObject := ThisDrawObject((Sender as TShapeEx).Tag);
  end
  else
  if (Sender is TdxfProgressBar) then
  begin
    stBarBase.Panels[0].Text := 'Object=' +
      IntToStr((Sender as TdxfProgressBar).Tag);
    CurrentDrawObject := ThisDrawObject((Sender as TdxfProgressBar).Tag);
  end
  else
  begin
    // What is the sender?
    stBarBase.Panels[0].Text := 'Object=' + Sender.ClassName;
{    if Sender.ClassNameIs('TLabel') then
    begin
      stBarBase.Panels[0].Text := 'Object=' + IntToStr((Sender as TLabel).Tag);
      CurrentDrawObject := ThisDrawObject((Sender as TLabel).Tag);
    end; }
  end;

// <23466>
  if {isCtrlDown and } isShiftDown then // 23466.150 Only look for Shift
  begin
    for I := 0 to List.Count - 1 do
    begin
      SelectedDrawObject := List.Items[I];
      if SelectedDrawObject.Selected then
        ShowSelectionRectangle(SelectedDrawObject.AObject,
          SelectedDrawObject.SelectionRectangle);
    end;
  end
  else
// </23466>
    if Assigned(CurrentDrawObject) then
      ShowSelectionRectangle(CurrentDrawObject.AObject, SelectionRectangle);
end; // ImageClick

procedure THomeF.ShowObjectProperties(X: Integer; Y: Integer);
var
  I: Integer;
  ADrawObject: PDrawObject;
begin
  if (List = nil) then
    Exit;
  stBarBase.Panels[1].Text := 'Objects=' + IntToStr(List.Count);
  for I:=0 to List.Count-1 do
  begin
    ADrawObject := List.Items[I];
    if ADrawObject.AType = otImage then
    begin
      if ((ADrawObject.AObject as TImage).Top >= Y) and
         (((ADrawObject.AObject as TImage).Top <= Y +
          (ADrawObject.AObject as TImage).Height)) and
         ((ADrawObject.AObject as TImage).Left >= X) and
         (((ADrawObject.AObject as TImage).Left <= X +
          (ADrawObject.AObject as TImage).Width)) then
          stBarBase.Panels[0].Text := ADrawObject.AImageName;
    end;
  end;
end;

procedure THomeF.pnlDrawMouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
begin
  if Sender = nil then Exit;
  ShowObjectProperties(X, Y);
  stBarBase.Panels[2].Text := 'Y=' + IntToStr(Y) + ' X=' + IntToStr(X);
  // Show Properties Dialog
  if (ssRight in Shift) then ShowChangeProperties(Sender);
end;

// Create Workspot Image
function THomeF.CreateImage(const AImageName: String;
  const ABigEffMeters: Boolean): TImage;
var
  AnImage: TImage;
begin
  AnImage := TImage.Create(Application);
  AnImage.Parent := pnlDraw;
  if ABigEffMeters then
  begin
    AnImage.Left := 250;
    AnImage.Top := 50;
  end
  else
  begin
    AnImage.Left := 50;
    AnImage.Top := 50;
  end;
  AnImage.Canvas.Brush.Style := bsClear;
  AnImage.Picture.LoadFromFile(ImagePath + AImageName);
//  AnImage.Width := AnImage.Picture.Width;
//  AnImage.Height := AnImage.Picture.Height;
  if ABigEffMeters then
  begin
    AnImage.Width := Trunc(IMAGE_SIZE * WorkspotScale / 100 / 2);
    AnImage.Height := Trunc(IMAGE_SIZE * WorkspotScale / 100 / 2);
  end
  else
  begin
    AnImage.Width := Trunc(IMAGE_SIZE * WorkspotScale / 100);
    AnImage.Height := Trunc(IMAGE_SIZE * WorkspotScale / 100);
  end;
  AnImage.Stretch := True;
  AnImage.Tag := NewItemIndex;
  AnImage.OnClick := ImageClick;
  AnImage.OnDblClick := ActionShowChangeProperties;
//  AnImage.PopupMenu := PopupWorkspotMenu;
  AnImage.OnMouseDown := ImageMouseDown;
  AnImage.OnMouseMove := ImageMouseMove;
  AnImage.OnMouseUp := ImageMouseUp;
  AnImage.Visible := True;
  Result := AnImage;
end;

// PIM-151
function THomeF.CreateGhostImage: TImage;
var
  AnImage: TImage;
begin
  AnImage := TImage.Create(Application);
  AnImage.Parent := pnlDraw;
  AnImage.Left := 50;
  AnImage.Top := 50;
  AnImage.Canvas.Brush.Style := bsClear;
  AnImage.Picture.Assign(ImageGhost.Picture);
  AnImage.Width := Trunc(IMAGE_SIZE * WorkspotScale / 100);
  AnImage.Height := Trunc(IMAGE_SIZE * WorkspotScale / 100);
  AnImage.Stretch := True;
  AnImage.Tag := NewItemIndex;
  AnImage.OnClick := ImageClick;
  AnImage.OnDblClick := ActionShowChangeProperties;
  AnImage.OnMouseDown := ImageMouseDown;
  AnImage.OnMouseMove := ImageMouseMove;
  AnImage.OnMouseUp := ImageMouseUp;
  AnImage.Visible := False;
  Result := AnImage;
end; // CreateGhostImage

// Create Vertical or Horizontal Line
function THomeF.CreateShapeEx(AShapeExType: TShapeExType): TShapeEx;
var
  MyShapeEx: TShapeEx;
begin
  MyShapeEx := TShapeEx.Create(Application);
  MyShapeEx.Pen.Width := 2;
  MyShapeEx.Shape := AShapeExType;
  MyShapeEx.Brush.Style := bsClear;
  MyShapeEx.Top := 50;
  MyShapeEx.Left := 50;
  if AShapeExType = TShapeExType(RECTANGLE) then
  begin
    MyShapeEx.Height := Trunc(200 * WorkspotScale / 100);
    MyShapeEx.Width := Trunc(400 * WorkspotScale / 100);
  end
  else
  begin
    if AShapeExType = stLineVert then
    begin
      MyShapeEx.Height := Trunc(200 * WorkspotScale / 100);
      MyShapeEx.Width := 9;
    end
    else
      if AShapeExType = stLineHorz then
      begin
        MyShapeEx.Height := 9;
        MyShapeEx.Width := Trunc(400 * WorkspotScale / 100);
      end;
  end;
  MyShapeEx.Tag := NewItemIndex;
  MyShapeEx.Parent := pnlDraw;
  MyShapeEx.OnClick := ImageClick;
  MyShapeEx.OnDblClick := ActionShowChangeProperties;
  MyShapeEx.OnMouseDown := ImageMouseDown;
  MyShapeEx.OnMouseMove := ImageMouseMove;
  MyShapeEx.OnMouseUp := ImageMouseUp;
  if AShapeExType = TShapeExType(RECTANGLE) then
    MyShapeEx.Visible := False;
  Result := MyShapeEx;
end;

// Create Rectangle that consist of 4 lines in a record
function THomeF.CreateRectangle(ADrawObject: PDrawObject): PTARectangle;
var
  MyRect: PTARectangle;
begin
  MyRect := new(PTARectangle);
  MyRect.ATopLineHorz := CreateShapeEx(stLineHorz);
  MyRect.ARightLineVert := CreateShapeEx(stLineVert);
  MyRect.ABottomLineHorz := CreateShapeEx(stLineHorz);
  MyRect.ALeftLineVert := CreateShapeEx(stLineVert);
  ADrawObject.ABigEffLabel.Caption := '';
  if ADrawObject.AType = otPuppetBox then
    begin
      ADrawObject.ALabel.Caption := ADrawObject.AWorkspotCode;
      ADrawObject.ALabel.Font.Size := 8;
      // SO-20013563
      if ADrawObject.AShowMode = ShowHeadCount then
        begin
          ADrawObject.ACross := new(PTACross);
          ADrawObject.ACross.ALineHorz := CreateShapeEx(stLineHorz);
          ADrawObject.ACross.ALineVert := CreateShapeEx(stLineVert);
        end;
    end
  else
    begin
      // NOTE: For the Rectangle, the WorkspotCode stands for the
      // DepartmentCode!
      ADrawObject.ALabel.Caption :=
          DetermineDepartmentDescription(ADrawObject.APlantCode,
              ADrawObject.AWorkspotCode);
      ADrawObject.ALabel.Font.Size := 15;
    end;
  ADrawObject.ALabel.Visible := True;
  Result := MyRect;
end;

procedure THomeF.DeleteRectangle(ADrawObject: PDrawObject);
begin
  if (ADrawObject.AType = otRectangle) or
    (ADrawObject.AType = otPuppetBox) then
  begin
    if (ADrawObject.AType = otPuppetBox) and
      (ADrawObject.AShowMode = ShowHeadCount) then
    begin
      DeleteHeadCountList(ADrawObject);
      if Assigned(ADrawObject.ACross) then
      begin
        ADrawObject.ACross.ALineHorz.Free;
        ADrawObject.ACross.ALineVert.Free;
        Dispose(ADrawObject.ACross);
      end;
    end;
    with ADrawObject.ADeptRect.ATopLineHorz do
    begin
      OnClick := nil;
      OnDblClick := nil;
      OnMouseDown := nil;
      OnMouseMove := nil;
      OnMouseUp := nil;
      Free;
    end;
    with ADrawObject.ADeptRect.ARightLineVert do
    begin
      OnClick := nil;
      OnDblClick := nil;
      OnMouseDown := nil;
      OnMouseMove := nil;
      OnMouseUp := nil;
      Free;
    end;
    with ADrawObject.ADeptRect.ABottomLineHorz do
    begin
      OnClick := nil;
      OnDblClick := nil;
      OnMouseDown := nil;
      OnMouseMove := nil;
      OnMouseUp := nil;
      Free;
    end;
    with ADrawObject.ADeptRect.ALeftLineVert do
    begin
      OnClick := nil;
      OnDblClick := nil;
      OnMouseDown := nil;
      OnMouseMove := nil;
      OnMouseUp := nil;
      Free;
    end;
  end;
end;

procedure THomeF.ShowRectangle(ADrawObject: PDrawObject);
var
  ATop, ALeft, AWidth, AHeight: Integer;
  ALineWidth: Integer;
begin
  if ADrawObject = nil then
    Exit;
  if (ADrawObject.AObject is TShapeEx) then
  begin
    if (ADrawObject.AType = otRectangle) or
      (ADrawObject.AType = otPuppetBox) then
    begin
      ATop := (ADrawObject.AObject as TShapeEx).Top;
      ALeft := (ADrawObject.AObject as TShapeEx).Left;
      // SO-20013563
      if ADrawObject.AShowMode = ShowHeadCount then
      begin
        AHeight := Trunc((ADrawObject.AObject as TShapeEx).Height *
          WorkspotScale / 100); // * 1.2
        AWidth := Trunc((ADrawObject.AObject as TShapeEx).Width *
          WorkspotScale / 100); // * 1.2
      end
      else
      begin
        AHeight := Trunc((ADrawObject.AObject as TShapeEx).Height *
          WorkspotScale / 100);
        AWidth := Trunc((ADrawObject.AObject as TShapeEx).Width *
          WorkspotScale / 100);
      end;

      ALineWidth := ADrawObject.ADeptRect.ATopLineHorz.Pen.Width;

      with ADrawObject.ADeptRect.ATopLineHorz do
      begin
        Top := ATop;
        Left := ALeft;
        Height := ALineWidth;
        Width := AWidth;
      end;

      with ADrawObject.ADeptRect.ARightLineVert do
      begin
        Top := ATop;
        Left := ALeft + AWidth - ADrawObject.ADeptRect.ARightLineVert.Pen.Width;
        Height := AHeight;
        Width := ALineWidth;
      end;

      with ADrawObject.ADeptRect.ABottomLineHorz do
      begin
        Top := ATop + AHeight - ADrawObject.ADeptRect.ABottomLineHorz.Pen.Width;
        Left := ALeft;
        Height := ALineWidth;
        Width := AWidth;
      end;

      with ADrawObject.ADeptRect.ALeftLineVert do
      begin
        Top := ATop;
        Left := ALeft;
        Height := AHeight;
        Width := ALineWidth;
      end;

      with ADrawObject.ALabel do
      begin
        Left := ALeft;
        Top := ATop - Height - Trunc(Font.Size / 4 * WorkspotScale / 100);
//        ADrawObject.ALabel.Width := (Sender as TShapeEx).Width *
//        Trunc(WorkspotScale / 100);
        Visible := True;
      end;

      if ADrawObject.ABigEffMeters then
        with ADrawObject.ABigEffLabel do
        begin
          Left := ALeft;
          // MRA:19-SEP-2012 Use different top, and also set font.size
          // Top := ATop - Height - Trunc(Font.Size / 4 * WorkspotScale / 100);
          Top := ADrawObject.ABorder.Top;
          Font.Size := Trunc(pnlDraw.Font.Size * 2.8 * WorkspotScale / 100);
          Visible := True;
        end;

      // SO-20013563
      if ADrawObject.AShowMode = ShowHeadCount then
      begin
        if Assigned(ADrawObject.ACross) then
        begin
          // Horz. Line
          ADrawObject.ACross.ALineHorz.Left := ALeft;
          ADrawObject.ACross.ALineHorz.Top := ATop + Trunc(AHeight * 2/3);
          ADrawObject.ACross.ALineHorz.Width := AWidth;
          ADrawObject.ACross.ALineHorz.Height := ALineWidth;
          // Vert. Line
          ADrawObject.ACross.ALineVert.Left := ALeft + Trunc(AWidth * 5/6);
          ADrawObject.ACross.ALineVert.Top := ATop;
          ADrawObject.ACross.ALineVert.Width := ALineWidth;
          ADrawObject.ACross.ALineVert.Height := AHeight;
        end;
      end;
    end;
  end;
end;

procedure THomeF.ActionShowChangeProperties(Sender: TObject);
begin
  // Not Needed!
  Exit;
  if Sender = nil then
    Exit;
  try
    if not tlbtnEdit.Down then
      MainTimerSwitch(False);
    ShowChangeProperties(Sender);
    if CurrentDrawObject <> nil then
    begin
      ShowDrawObject(CurrentDrawObject, Sender);
      ShowSelectionRectangle(Sender, SelectionRectangle);
    end;
  finally
    if not tlbtnEdit.Down then
      MainTimerSwitch(True);
  end;
end;

function THomeF.NewDrawObject(AProdScreenType: TProdScreenType): PDrawObject;
var
  ADrawObject: PDrawObject;
begin
  new(ADrawObject);

//<23466>
  AdrawObject.SelectionRectangle := TShapeEx.Create(Application);
  with AdrawObject.SelectionRectangle do
    begin
      Visible := False;
      Pen.Width := 2;
      Parent := pnlDraw;
      Pen.Color := clMyRed;
      Shape := TShapeExType(RECTANGLE);
      Brush.Style := bsClear;
    end;
//</23466>

  ADrawObject.ALabel := TLabel.Create(Application);
  if AProdScreenType <> pstWSEmpEff then // PIM-213
    with ADrawObject.ALabel do
    begin
      Parent := pnlDraw;
      Tag := NewItemIndex;
      OnClick := ImageClick;
      OnMouseDown := ImageMouseDown;
      OnMouseMove := ImageMouseMove;
      OnMouseUp := ImageMouseUp;
      Alignment := taLeftJustify;
    end;

  ADrawObject.ABigEffLabel := TLabel.Create(Application);
  if AProdScreenType <> pstWSEmpEff then // PIM-213
    with ADrawObject.ABigEffLabel do
    begin
      Parent := pnlDraw;
      Tag := NewItemIndex;
      OnClick := ImageClick;
      OnMouseDown := ImageMouseDown;
      OnMouseMove := ImageMouseMove;
      OnMouseUp := ImageMouseUp;
      Alignment := taLeftJustify;
    end;

  ADrawObject.ALightRed := TShape.Create(Application);
  with ADrawObject.ALightRed do
  begin
    Shape := stCircle;
    Brush.Color := clMyRed;
    Height := Trunc(LIGHT_SIZE * WorkspotScale / 100);
    Width := Trunc(LIGHT_SIZE * WorkspotScale / 100);
    Visible := False;
    Parent := pnlDraw;
  end;

  ADrawObject.ALightYellow := TShape.Create(Application);
  with ADrawObject.ALightYellow do
  begin
    Shape := stCircle;
    Brush.Color := clMyYellow;
    Height := Trunc(LIGHT_SIZE * WorkspotScale / 100);
    Width := Trunc(LIGHT_SIZE * WorkspotScale / 100);
    Visible := False;
    Parent := pnlDraw;
  end;

  ADrawObject.ALightGreen := TShape.Create(Application);
  with ADrawObject.ALightGreen do
  begin
    Shape := stCircle;
    Brush.Color := clMyGreen;
    Height := Trunc(LIGHT_SIZE * WorkspotScale / 100);
    Width := Trunc(LIGHT_SIZE * WorkspotScale / 100);
    Visible := False;
    Parent := pnlDraw;
  end;

  ADrawObject.AEffMeterRed := TShape.Create(Application);
  with ADrawObject.AEffMeterRed do
  begin
    Tag := NewItemIndex;
    Visible := False;
    Height := Trunc(8 * WorkspotScale / 100);
    Brush.Color := clMyRed;
    Pen.Color := clMyRed;
    OnMouseDown := ImageMouseDown;
    OnMouseMove := ImageMouseMove;
    OnMouseUp := ImageMouseUp;
    Parent := pnlDraw;
  end;

  ADrawObject.AEffMeterGreen := TShape.Create(Application);
  with ADrawObject.AEffMeterGreen do
  begin
    Tag := NewItemIndex;
    Visible := False;
    Height := Trunc(8 * WorkspotScale / 100);
    Brush.Color := clMyGreen;
    Pen.Color := clMyGreen;
    OnMouseDown := ImageMouseDown;
    OnMouseMove := ImageMouseMove;
    OnMouseUp := ImageMouseUp;
    Parent := pnlDraw;
  end;

  ADrawObject.AProgressBar := TdxfProgressBar.Create(Application);
  with ADrawObject.AProgressBar do
  begin
    Tag := NewItemIndex;
    Visible := False;
    Height := Trunc(8 * WorkspotScale / 100);
    Max := 100;
    Min := 0;
    BeginColor := clMyRed;
    EndColor := clMyGreen;
    Font.Color := clBlack;
    Font.Style := [fsBold];
    ShowText := True;
    ShowTextStyle := stsPosition;
    Style := sExSolid;
    BevelOuter := bvNone;
    OnMouseDown := ImageMouseDown;
    OnMouseMove := ImageMouseMove;
    OnMouseUp := ImageMouseUp;
    Parent := pnlDraw;
  end;

  ADrawObject.AEffPercentage := 0;

  ADrawObject.ABorder := TShape.Create(Application);
  with ADrawObject.ABorder do
  begin
    Tag := NewItemIndex;
    Pen.Color := clGreen;
    Shape := stRectangle;
    Brush.Style := bsClear;
    OnMouseDown := ImageMouseDown;
    OnMouseMove := ImageMouseMove;
    OnMouseUp := ImageMouseUp;
    Parent := pnlDraw;
    Visible := False;
  end;

  ADrawObject.AEffTotalsLabel := TLabel.Create(Application);
  with ADrawObject.AEffTotalsLabel do
  begin
    Tag := NewItemIndex;
    OnMouseDown := ImageMouseDown;
    OnMouseMove := ImageMouseMove;
    OnMouseUp := ImageMouseUp;
    Parent := pnlDraw;
    Visible := False;
  end;

  ADrawObject.APuppetShiftList := TList.Create;

  ADrawObject.AEmployeeList := TList.Create;

  ADrawObject.ABigEffMeters := False;

  ADrawObject.ABlinkPuppets := False;

  ADrawObject.ABlinkCompareJobs := False;

  ADrawObject.ABlinkIsDown := False; // 20013516
  ADrawObject.ADownJobCode := ''; // 20013516

  ADrawObject.ACurrentNumberOfEmployees := 0; // SO-20013516

  ADrawObject.ANoData := False;

  ADrawObject.AProdScreenType := AProdScreenType;

  ADrawObject.ACross := nil; // SO-20013563
  ADrawObject.AHeadCountList := nil; // SO-20013563

  ADrawObject.AEffRunningHrs := False; // 20013551
  ADrawObject.AShiftList := TList.Create; // 20013551
  ADrawObject.AProdLevelMinPerc := 0; // 20013551
  ADrawObject.AMachOutputMinPerc := 0; // 20013551
  ADrawObject.AMinPercBlink := False; // 20013551
  ADrawObject.AMinPercLabel := nil; // 20013551
  ADrawObject.AMaxDeviationLabel := nil; // 20013549
  ADrawObject.AHasLinkJob := False; // 20013196

  // 20013196 Related to this order, turn 'Show Hint' on here!
  // Show Hint
  // Remark: It does not show a hint, why is not clear.
  ADrawObject.ALabel.ShowHint := True;
  ADrawObject.ALabel.ParentShowHint := False;
  // Set Workspotdescription as Hint to ABorder
  ADrawObject.ABorder.ShowHint := True;
  ADrawObject.ABorder.ParentShowHint := False;

  // TD-24100 Be sure these are initialised here.
  ADrawObject.ACompareJob := False;   // 20013379
  ADrawObject.ACompareJobCode := '';  // 20013379
  ADrawObject.AEmpsScanIn := False;   // 20013379

  ADrawObject.ATimezonehrsdiff := 0; // 20016449

  ADrawObject.AGhostCountBlink := False; // PIM-151
  ADrawObject.AGhostImage := CreateGhostImage; // PIM-151
  Application.ProcessMessages;

{  ADrawObject.APanel := nil; }

  case AProdScreenType of
  pstNoTimeRec, pstMachine:
    begin
//      ADrawObject.AMachineTimeRec := nil;
//      ADrawObject.AWorkspotTimeRec := nil;
      ADrawObject.APriority := 2;
      // 20013351 Create a label for 'minimum percentage'.
      ADrawObject.AMinPercLabel := TLabel.Create(Application);
      with ADrawObject.AMinPercLabel do
      begin
        Tag := NewItemIndex;
        Alignment := taRightJustify;
        OnMouseDown := ImageMouseDown;
        OnMouseMove := ImageMouseMove;
        OnMouseUp := ImageMouseUp;
        Parent := pnlDraw;
        Caption := '';
        Font.Color := clRed;
        Visible := False;
      end;
      // 20013549 Create a label for 'max. deviation'.
      ADrawObject.AMaxDeviationLabel := TLabel.Create(Application);
      with ADrawObject.AMaxDeviationLabel do
      begin
        Tag := NewItemIndex;
        Alignment := taRightJustify;
        OnMouseDown := ImageMouseDown;
        OnMouseMove := ImageMouseMove;
        OnMouseUp := ImageMouseUp;
        Parent := pnlDraw;
        Caption := '';
        Font.Color := clRed;
        Visible := False;
      end;
    end;
  end; // case

  Result := ADrawObject;
end; // NewDrawObject

procedure THomeF.AddObjectToList(otType: TObjectType;
  AObject: TObject; APlantCode, AWorkspotCode, AWorkspotDescription,
  AImageName: String; ABigEffMeters: Boolean; AShowMode: Integer;
  AProdScreenType: TProdScreenType);
var
  ADrawObject: PDrawObject;
  LinkedJobText: String; // 20013196
begin
  Dirty := True;

  ADrawObject := NewDrawObject(AProdScreenType);
  ADrawObject.AShowMode := TShowMode(AShowMode);
  ADrawObject.ACompareJob := False; // 20013379
  ADrawObject.ACompareJobCode := ''; // 20013379
  ADrawObject.AEmpsScanIn := False; // 20013379
  LinkedJobText := ''; // 20013196
  if otType = otImage then
  begin
    ADrawObject.AObject := CreateImage(AImageName, ABigEffMeters);
    ADrawObject.ADefaultImage := TImage(ADrawObject.AObject);
  end
  else
  begin
    ADrawObject.AObject := AObject;
    ADrawObject.ADefaultImage := nil;
  end;
  ADrawObject.AType := otType;

  // ProdScreenType: NoTimeRec, MachineTimeRec, WorkspotTimeRec, Machine // 20014450.50
  ADrawObject.AProdScreenType := AProdScreenType;

  // Plant
  ADrawObject.APlantCode := APlantCode;

  // 20016449
  ADrawObject.ATimezonehrsdiff :=
    ORASystemDM.PlantTimezonehrsdiff(ADrawObject.APlantCode);

  // Workspot
  ADrawObject.AWorkspotCode := AWorkspotCode;
  ADrawObject.AWorkspotDescription := AWorkspotDescription;

  case ADrawObject.AProdScreenType of
  pstNoTimeRec: // No Time Rec: Workspot
    begin
      ADrawObject.ALabel.Caption :=
        DetermineWorkspotShortName(ADrawObject.APlantCode,
          ADrawObject.AWorkspotCode, ADrawObject.AHasLinkJob);
      if ADrawObject.AHasLinkJob then
        LinkedJobText := ' ' + SPimsLinkedJob; // 20013196
      ADrawObject.ALabel.Hint := AWorkspotDescription + LinkedJobText; // 20013196
      ADrawObject.ABorder.Hint := AWorkspotDescription + LinkedJobText; // 20013196
      DetermineEffRunningHrs(ADrawObject); // 20013551
      DetermineShiftList(ADrawObject); // 20013551
    end;
  pstMachine: // 20014450.50
    begin
      ADrawObject.ALabel.Caption := AWorkspotDescription;
    end;
  pstWSEmpEff: // PIM-213
    begin
      ADrawObject.ApnlWSEmpEff :=
        WSEmpEffCreate(ADrawObject,
          DetermineWorkspotShortName(ADrawObject.APlantCode,
          ADrawObject.AWorkspotCode, ADrawObject.AHasLinkJob)
          );
    end;
  end;

  if (otType = otRectangle) or (otType = otPuppetBox) then
    ADrawObject.ADeptRect := CreateRectangle(ADrawObject);

  // Show Hint
  ADrawObject.ALabel.ShowHint := True;
  ADrawObject.ALabel.ParentShowHint := False;
  // Set Workspotdescription as Hint to ABorder
  ADrawObject.ABorder.ShowHint := True;
  ADrawObject.ABorder.ParentShowHint := False;

  ADrawObject.AImageName := AImageName;
  ADrawObject.ABigEffMeters := ABigEffMeters;

  ShowDrawObject(ADrawObject, ADrawObject.AObject);
  List.Add(ADrawObject);
  RenumberDrawObjects;
end; // AddObjectToList

procedure THomeF.DeleteDrawObjectFromList(ATag: Integer);
var
  I, ItemIndex: Integer;
  ADrawObject: PDrawObject;
  AnImage: TImage;
  AShapeEx: TShapeEx;
begin
  Dirty := True;

  ItemIndex := -1;
  ADrawObject := nil;
  for I:=0 to List.Count-1 do
  begin
    ADrawObject := List.Items[I];
    if ADrawObject.AObject is TImage then
    begin
      AnImage := (ADrawObject.AObject as TImage);
      if AnImage.Tag = ATag then
      begin
        AnImage.OnClick := nil;
        AnImage.OnDblClick := nil;
        AnImage.PopupMenu := nil;
        AnImage.OnMouseDown := nil;
        AnImage.OnMouseMove := nil;
        AnImage.OnMouseUp := nil;
        AnImage.Free;
        ItemIndex := I;
        break;
      end;
    end;
    if ADrawObject.AObject is TShapeEx then
    begin
      AShapeEx := (ADrawObject.AObject as TShapeEx);
      if AShapeEx.Tag = ATag then
      begin
        AShapeEx.OnClick := nil;
        AShapeEx.OnDblClick := nil;
        AShapeEx.OnMouseDown := nil;
        AShapeEx.OnMouseMove := nil;
        AShapeEx.OnMouseUp := nil;
        AShapeEx.Free;
        ItemIndex := I;
        break;
      end;
    end;
  end;
  if ItemIndex <> -1 then
  begin
    if ADrawObject <> nil then
    begin
      ADrawObject.SelectionRectangle.Free;
      ADrawObject.AObject := nil;
      ADrawObject.AGhostImage := nil; // PIM-151
      ADrawObject.ALabel.Free;
      ADrawObject.ABigEffLabel.Free;
      ADrawObject.ALightRed.Free;
      ADrawObject.ALightYellow.Free;
      ADrawObject.ALightGreen.Free;
      ADrawObject.ABorder.Free;
      ADrawObject.AEffMeterGreen.Free;
      ADrawObject.AEffMeterRed.Free;
      // PIM-194
      if Assigned(ADrawObject.AProgressBar) then
      begin
        ADrawObject.AProgressBar.Visible := False;
        ADrawObject.AProgressBar.Free;
      end;
      // PIM-213
      if (ADrawObject.AProdScreenType = pstWSEmpEff) then
        if Assigned(ADrawObject.ApnlWSEmpEff) then
        begin
          ADrawObject.ApnlWSEmpEff.Visible := False;
          ADrawObject.ApnlWSEmpEff.Free;
        end;
      DeletePuppetShiftList(ADrawObject.APuppetShiftList);
      ADrawObject.APuppetShiftList := nil;
      ClearEmployeeList(ADrawObject);
      ADrawObject.AEmployeeList := nil;
      ADrawObject.AEffTotalsLabel.Free;
      ADrawObject.AMinPercLabel.Free; // 20013551
      ADrawObject.AMaxDeviationLabel.Free; // 20013549
      DeleteRectangle(ADrawObject);
      DeleteShiftList(ADrawObject); // 20013551
    end;
    List.Delete(ItemIndex);
  end;
  RenumberDrawObjects;
end; // DeleteDrawObjectFromList

procedure THomeF.RenumberDrawObjects;
var
  I: Integer;
  ADrawObject: PDrawObject;

  function PriorityCompare(Item1, Item2: Pointer): Integer;
  var
    ADrawObject1, ADrawObject2: PDrawObject;
  begin
    ADrawObject1 := PDrawObject(Item1);
    ADrawObject2 := PDrawObject(Item2);
    Result := 0;
    if ADrawObject1.APriority > ADrawObject2.APriority then
      Result := 1
    else
      if ADrawObject1.APriority < ADrawObject2.APriority then
        Result := -1;
  end;

begin
  for I:=0 to List.Count-1 do
  begin
    ADrawObject := List.Items[I];
    ADrawObject.ALabel.Tag := I+1;
    ADrawObject.ABigEffLabel.Tag := I+1;
    ADrawObject.ABorder.Tag := I+1;
    ADrawObject.AEffMeterGreen.Tag := I+1;
    ADrawObject.AEffMeterRed.Tag := I+1;
    if Assigned(ADrawObject.AProgressBar) then // PIM-194
      ADrawObject.AProgressBar.Tag := I+1;
    if (ADrawObject.AObject is TImage) then
      (ADrawObject.AObject as TImage).Tag := I+1;
    if (ADrawObject.AObject is TShapeEx) then
      (ADrawObject.AObject as TShapeEx).Tag := I+1;
    ADrawObject.AEffTotalsLabel.Tag := I+1;
    if Assigned(ADrawObject.AMinPercLabel) then
      ADrawObject.AMinPercLabel.Tag := I+1; // 20013551
    if Assigned(ADrawObject.AMaxDeviationLabel) then
      ADrawObject.AMaxDeviationLabel.Tag := I+1; // 20013549
    if Assigned(ADrawObject.AGhostImage) then
      ADrawObject.AGhostImage.Tag := I+1; // PIM-151
    if ADrawObject.AProdScreenType = pstWSEmpEff then
      if Assigned(ADrawObject.ApnlWSEmpEff) then
        ADrawObject.ApnlWSEmpEff.Renumber(I+1);
  end;
  if List.Count > 1 then
    List.Sort(@PriorityCompare);
end;

function THomeF.NewItemIndex: Integer;
var
  I, Max: Integer;
  ADrawObject: PDrawObject;
  AnImage: TImage;
  AShapeEx: TShapeEx;
begin
  Max := 0;
  for I:=0 to List.Count-1 do
  begin
    ADrawObject := List.Items[I];
    if (ADrawObject.AObject is TImage) then
    begin
      AnImage := (ADrawObject.AObject as TImage);
      if AnImage.Tag > Max then
        Max := AnImage.Tag;
    end
    else
      if (ADrawObject.AObject is TShapeEx) then
      begin
        AShapeEx := (ADrawObject.AObject as TShapeEx);
        if AShapeEx.Tag > Max then
          Max := AShapeEx.Tag;
      end;
  end;
  Result := Max + 1;
end;

procedure THomeF.FreeList;
var
  I: Integer;
  ADrawObject: PDrawObject;
begin
  for I:=List.Count - 1  downto 0 do
  begin
    ADrawObject := List.Items[I];
    if (ADrawObject.AObject is TImage) then
      DeleteDrawObjectFromList((ADrawObject.AObject as TImage).Tag)
    else
      if (ADrawObject.AObject is TShapeEx) then
        DeleteDrawObjectFromList((ADrawObject.AObject as TShapeEx).Tag);
  end;
  List.Clear;
  List.Free;
end;

procedure THomeF.SaveList(Filename: String);
var
  I: Integer;
  ADrawObject: PDrawObject;
  IniFile: TIniFile;
  Obj: String;
  AnImage: TImage;
  AShapeEx: TShapeEx;
begin
  if not tlbtnEdit.Down then
    MainTimerSwitch(False);
  ProductionScreenWaitF.lblMessage.Caption := SPimsSavingScheme;
  ProductionScreenWaitF.lblInfo.Caption := '';
  ProductionScreenWaitF.Show;
  Update;

  if FileExists(ExtractFilePath(SchemePath) + ExtractFileName(Filename)) then
    DeleteFile(ExtractFilePath(SchemePath) + ExtractFileName(Filename));

  IniFile := TIniFile.Create(ExtractFilePath(SchemePath) +
    ExtractFileName(Filename));

  with IniFile do
  begin
    // Write Coordinates of Window for this Scheme
    WriteInteger('Main', 'Top', HomeF.Top);
    WriteInteger('Main', 'Left', HomeF.Left);
    WriteInteger('Main', 'Width', HomeF.Width);
    WriteInteger('Main', 'Height', HomeF.Height);

    // 20013548.10. Also write screen width/height
    WriteInteger('Main', 'ScreenWidth', Screen.Width);
    WriteInteger('Main', 'ScreenHeight', Screen.Height);

    // PIM-194
    WriteInteger('Main', 'RedBoundary', ORASystemDM.RedBoundary);
    WriteInteger('Main', 'OrangeBoundary', ORASystemDM.OrangeBoundary);

    // Write following for this Scheme
    WriteInteger('Main', 'WorkspotScale', WorkspotScale);
    WriteInteger('Main', 'FontScale', FontScale);
    // MRA:24-JUN-2010. For compatibility with older systems:
    if EfficiencyPeriodMode = epSince then
      WriteBool('Main', 'EfficiencyPeriodSince', True) //EfficiencyPeriodSince)
    else
      WriteBool('Main', 'EfficiencyPeriodSince', False); // EfficiencyPeriodSince)
    // MRA:24-JUN-2010. New setting:
    WriteInteger('Main', 'EfficiencyPeriodMode', Integer(EfficiencyPeriodMode));
    WriteDateTime('Main', 'EfficiencyPeriodSinceTime',
      EfficiencyPeriodSinceTime);
    // 20013550
    WriteInteger('Main', 'GridLineDistance', GridLineDistance);

    // PIM-223
    WriteBool('Main', 'ShowEmpInfo', ORASystemDM.PSShowEmpInfo);
    WriteBool('Main', 'ShowEmpEff', ORASystemDM.PSShowEmpEff);

    // Write Objects for this Scheme
    for I:=0 to List.Count-1 do
    begin
      Application.ProcessMessages;
      ADrawObject := List.Items[I];
      Obj := 'Object' + IntToStr(I);
      WriteInteger(Obj, 'AType', Integer(ADrawObject.AType));
      if ADrawObject.ABigEffMeters then
        WriteString(Obj, 'ABigEffMeters', 'True')
      else
        WriteString(Obj, 'ABigEffMeters', 'False');
      //
      // Machine/Workspot/TimeRec - Start
      //
      case ADrawObject.AProdScreenType of
      pstNoTimeRec: // No Time Rec: Workspot
        begin
          WriteString(Obj, 'AProdScreenType', 'NoTimeRec');
        end;
      pstMachine: // 20014450.50
        begin
          WriteString(Obj, 'AProdScreenType', 'Machine');
        end;
      pstWSEmpEff: // PIM-213
        begin
          WriteString(Obj, 'AProdScreenType', 'WSEmpEff');
        end;
      end;
      //
      // Machine/Workspot/TimeRec - End
      //
      WriteString(Obj, 'APlantCode', ADrawObject.APlantCode);
      WriteString(Obj, 'AWorkspotCode', ADrawObject.AWorkspotCode);
      WriteString(Obj, 'AWorkspotDescription',
        ADrawObject.AWorkspotDescription);
      WriteString(Obj, 'AImageName', ADrawObject.AImageName);
      if ADrawObject.AProdScreenType = pstWSEmpEff then // PIM-213
      begin
        if Assigned(ADrawObject.ApnlWSEmpEff) then
        begin
          WriteInteger(Obj, 'Top', ADrawObject.ApnlWSEmpEff.Top);
          WriteInteger(Obj, 'Left', ADrawObject.ApnlWSEmpEff.Left);
        end;
      end
      else
        if ADrawObject.AObject is TImage then
        begin
          AnImage := (ADrawObject.AObject as TImage);
          WriteInteger(Obj, 'Top', AnImage.Top);
          WriteInteger(Obj, 'Left', AnImage.Left);
        end
        else
          if ADrawObject.AObject is TShapeEx then
          begin
            AShapeEx := (ADrawObject.AObject as TShapeEx);
            WriteInteger(Obj, 'Top', AShapeEx.Top);
            WriteInteger(Obj, 'Left', AShapeEx.Left);
            WriteInteger(Obj, 'Width', AShapeEx.Width);
            WriteInteger(Obj, 'Height', AShapeEx.Height);
          end;
      if ADrawObject.AType = otPuppetBox then
        WriteInteger(Obj, 'ShowMode', Integer(ADrawObject.AShowMode));
    end;
  end;

  IniFile.Free;

  Dirty := False;

  ProductionScreenWaitF.Hide;
  if not tlbtnEdit.Down then
    MainTimerSwitch(True);
end;

procedure THomeF.OpenList(Filename: String);
var
  I: Integer;
  ADrawObject: PDrawObject;
  IniFile: TIniFile;
  Obj: String;
  ObjectName: String;
  BigEffMeters: Boolean;
  ProdScreenTypeDescription: String;
  ProdScreenType: TProdScreenType;
begin
  I := 0;
  // GLOB3-231
  // First check if file exists here
  try
    if not FileExists(ExtractFilePath(SchemePath) + ExtractFileName(Filename)) then
    begin
      DisplayMessage(SPimsSchemeNotFound + ' ' +
        ExtractFilePath(SchemePath) + ExtractFileName(Filename),
          mtWarning, [mbOk]);
      Exit;
    end;
  except
    on E:Exception do
    begin
      DisplayMessage(SPimsSchemeNotFound + ' ' +
        ExtractFilePath(SchemePath) + ExtractFileName(Filename),
          mtWarning, [mbOk]);
      WLog(SPimsSchemeNotFound + ' ' +
        ExtractFilePath(SchemePath) + ExtractFileName(Filename));
      Exit;
    end;
  end;

  if not tlbtnEdit.Down then
    MainTimerSwitch(False);
  Screen.Cursor := crHourGlass;

  // PIM-223
  ORASystemDM.PSShowEmpInfo := True;
  ORASystemDM.PSShowEmpEff := True;

  ProductionScreenWaitF.lblMessage.Caption := SPimsLoadingScheme;
  ProductionScreenWaitF.lblInfo.Caption :=
    ExtractFilePath(SchemePath) + ExtractFileName(Filename);
  ProductionScreenWaitF.Show;
  Update;

  try
    try
      FreeList;
      List := TList.Create;

      IniFile := TIniFile.Create(ExtractFilePath(SchemePath) +
        ExtractFileName(Filename));

      with IniFile do
      begin
        // Read Coordinates of Window for this Scheme
        HomeF.Top := ReadInteger('Main', 'Top', HomeF.Top);
        HomeF.Left := ReadInteger('Main', 'Left', HomeF.Left);
        HomeF.Width := ReadInteger('Main', 'Width', HomeF.Width);
        HomeF.Height := ReadInteger('Main', 'Height', HomeF.Height);

        // 20013548.10. Also read screen width/height
        if ValueExists('Main', 'ScreenWidth') then
          ScreenWidth := ReadInteger('Main', 'ScreenWidth', Screen.Width)
        else
          ScreenWidth := Screen.Width;
        if ValueExists('Main', 'ScreenHeight') then
          ScreenHeight := ReadInteger('Main', 'ScreenHeight', Screen.Height)
        else
          ScreenHeight := Screen.Height;

        // PIM-194
        if ValueExists('Main', 'RedBoundary') then
          ORASystemDM.RedBoundary :=
            ReadInteger('Main', 'RedBoundary', ORASystemDM.RedBoundary)
        else
          ORASystemDM.RedBoundary := RED_BOUNDARY;
        if ValueExists('Main', 'OrangeBoundary') then
          ORASystemDM.OrangeBoundary :=
            ReadInteger('Main', 'OrangeBoundary', ORASystemDM.OrangeBoundary)
        else
          ORASystemDM.OrangeBoundary := ORANGE_BOUNDARY;

        // 20013548
        CurrentSchemeWidth := HomeF.Width;
        CurrentSchemeHeight := HomeF.Height;
        // Read settings for this Scheme
        WorkspotScale := ReadInteger('Main', 'WorkspotScale', WorkspotScale);
        // 20013550
        GridLineDistance := ReadInteger('Main', 'GridLineDistance', GridLineDistance);
        FontScale := ReadInteger('Main', 'FontScale', FontScale);
        // MRA:24-JUN-2010. New setting for efficiency-period-mode:
        if ValueExists('Main', 'EfficiencyPeriodMode') then
        begin
          EfficiencyPeriodMode :=
            TEffPeriodMode(
              ReadInteger('Main', 'EfficiencyPeriodMode',
                Integer(EfficiencyPeriodMode)));
        end
        else
        begin
          // MRA:24-JUN-2010. For compatibility with older systems:
          EfficiencyPeriodSince := ReadBool('Main', 'EfficiencyPeriodSince',
            EfficiencyPeriodSince);
            if EfficiencyPeriodSince then
            EfficiencyPeriodMode := epSince
          else
            EfficiencyPeriodMode := epCurrent;
        end;
        try
          EfficiencyPeriodSinceTime := ReadDateTime('Main',
            'EfficiencyPeriodSinceTime', EfficiencyPeriodSinceTime);
        except
          EfficiencyPeriodSinceTime := Trunc(Now) + Frac(EncodeTime(7, 0, 0, 0));
        end;

        // PIM-223
        if ValueExists('Main', 'ShowEmpInfo') then
          ORASystemDM.PSShowEmpInfo :=
            ReadBool('Main', 'ShowEmpInfo', ORASystemDM.PSShowEmpInfo);
        if ValueExists('Main', 'ShowEmpEff') then
          ORASystemDM.PSShowEmpEff :=
            ReadBool('Main', 'ShowEmpEff', ORASystemDM.PSShowEmpEff);
        if not ORASystemDM.PSShowEmpInfo then
          ORASystemDM.PSShowEmpEff := False;

        I := 0;
        // Read objects for this Scheme
        while I < MAXOBJECTS do
        begin
          try
            Application.ProcessMessages;
            Obj := 'Object' + IntToStr(I);
            ObjectName := ReadString(Obj, 'AImageName', '');
            if ObjectName <> '' then
            begin
              ProdScreenType := pstNoTimeRec;
              if ValueExists(Obj, 'AProdScreenType') then
              begin
                ProdScreenTypeDescription :=
                  ReadString(Obj, 'AProdScreenType', 'NoTimeRec');
                if ProdScreenTypeDescription = 'Machine' then
                  ProdScreenType := pstMachine
                else // This is needed to prevent it reading a wrong scheme (from Personal Screen)
                  if ProdScreenTypeDescription = 'MachineTimeRec' then
                    ProdScreenType := pstMachineTimeRec
                  else
                    if ProdScreenTypeDescription = 'WorkspotTimeRec' then
                      ProdScreenType := pstWorkspotTimeRec
                    else
                      if ProdScreenTypeDescription = 'WSEmpEff' then
                        ProdScreenType := pstWSEmpEff;
              end;
              // SO-20013516 Disabled.
              if (ProdScreenType = pstMachineTimeRec) or
                (ProdScreenType = pstWorkspotTimeRec) then
              begin
                inc(I);
                Continue;
              end;
              ADrawObject := NewDrawObject(ProdScreenType);
              ADrawObject.AImageName := ObjectName;
              BigEffMeters := (ReadString(Obj, 'ABigEffMeters', 'False') = 'True');
              ADrawObject.ABigEffMeters := BigEffMeters;
              ADrawObject.APlantCode := ReadString(Obj, 'APlantCode', '');
              // 20016449
              ADrawObject.ATimezonehrsdiff :=
                ORASystemDM.PlantTimezonehrsdiff(ADrawObject.APlantCode);
              ADrawObject.AWorkspotCode := ReadString(Obj, 'AWorkspotCode', '');
              ADrawObject.AWorkspotDescription :=
                ReadString(Obj, 'AWorkspotDescription', '');
              ADrawObject.ALabel.Caption :=
                DetermineWorkspotShortName(ADrawObject.APlantCode,
                  ADrawObject.AWorkspotCode, ADrawObject.AHasLinkJob);
              //
              // Machine/Workspot/TimeRec - Start
              //
              ADrawObject.AProdScreenType := ProdScreenType;
              case ProdScreenType of
              pstNoTimeRec:
                begin
                  ADrawObject.ALabel.Caption :=
                    DetermineWorkspotShortName(ADrawObject.APlantCode,
                      ADrawObject.AWorkspotCode, ADrawObject.AHasLinkJob);
                  DetermineEffRunningHrs(ADrawObject); // 20013551
                  DetermineShiftList(ADrawObject); // 20013551
                end;
              pstMachine: // 20014450.50
                begin
                  ADrawObject.ALabel.Caption := ADrawObject.AWorkspotDescription;
                end;
              end;
              //
              // Machine/Workspot/TimeRec - End
              //
              ADrawObject.AType := TObjectType(ReadInteger(Obj, 'AType', 0));
              // SO-20013563 Read showmode earlier! Or Createrectangle goes wrong.
              // for 'head count'.
              if ADrawObject.AType = otPuppetBox then
                ADrawObject.AShowMode := TShowMode(ReadInteger(Obj, 'ShowMode',
                  Integer(ShowAllEmployees)));
              ADrawObject.ADefaultImage := nil; // PIM-151
              if ADrawObject.AType = otImage then
              begin
                ADrawObject.AObject := CreateImage(ObjectName, BigEffMeters);
                // PIM-213
                case ProdScreenType of
                pstWSEmpEff: // PIM-213
                  begin
                    ADrawObject.ApnlWSEmpEff :=
                      WSEmpEffCreate(ADrawObject,
                        DetermineWorkspotShortName(ADrawObject.APlantCode,
                        ADrawObject.AWorkspotCode, ADrawObject.AHasLinkJob)
                        );
                  end;
                end;
                ADrawObject.ADefaultImage := TImage(ADrawObject.AObject); // PIM-151
                with (ADrawObject.AObject as TImage) do
                begin
                  Left := ReadInteger(Obj, 'Left', 0);
                  Top := ReadInteger(Obj, 'Top', 0);
                  Left := AlignToGridLeft(Left);
                  Top := AlignToGridTop(Top);
                  if (ProdScreenType <> pstWSEmpEff) then // PIM-213
                    ADrawObject.ApnlWSEmpEff := nil;
                  if Assigned(ADrawObject.ApnlWSEmpEff) then // PIM-213
                  begin
                    ADrawObject.ApnlWSEmpEff.Left := Left;
                    ADrawObject.ApnlWSEmpEff.Top := Top;
                  end;
                end;
              end
              else
              begin
                if (ADrawObject.AType = otLineHorz) or
                   (ADrawObject.AType = otLineVert) or
                   (ADrawObject.AType = otRectangle) or
                  (ADrawObject.AType = otPuppetBox) then
                begin
                  if (ADrawObject.AType = otLineHorz) then
                    ADrawObject.AObject := CreateShapeEx(stLineHorz)
                  else
                    if (ADrawObject.AType = otLineVert) then
                      ADrawObject.AObject := CreateShapeEx(stLineVert)
                    else
                      if (ADrawObject.AType = otRectangle) or
                        (ADrawObject.AType = otPuppetBox) then
                      begin
                        ADrawObject.AObject :=
                          CreateShapeEx(TShapeExType(RECTANGLE));
                        ADrawObject.ADeptRect := CreateRectangle(ADrawObject);
                      end;
                  with (ADrawObject.AObject as TShapeEx) do
                  begin
                    Left := ReadInteger(Obj, 'Left', 0);
                    Top := ReadInteger(Obj, 'Top', 0);
                    // 20013550
                    // 20013550.10. Not for lines!
//                  Left := AlignToGridLeft(Left);
//                  Top := AlignToGridTop(Top);
// [ROP 08-01-2013 - TODO 21774 - also read width/height for lines ]
//                  if (ADrawObject.AType = otRectangle) or
//                    (ADrawObject.AType = otPuppetBox) then
//                  begin
                      Width := ReadInteger(Obj, 'Width', IMAGE_SIZE);
                      Height := ReadInteger(Obj, 'Height', IMAGE_SIZE);
//                  end;
// [END TODO 21774]
//                  Width := Trunc(Width * WorkspotScale / 100);
//                  Height := Trunc(Height * WorkspotScale / 100);
                  end;
                end;
              end;
              ShowDrawObject(ADrawObject, ADrawObject.AObject);
              Update;
              List.Add(ADrawObject);
            end;
            inc(I);
          except
            on E:Exception do
              WLog('Error in OpenList (Item=' + IntToStr(I) + '): ' + E.Message);
          end;
        end; // while
      end;
      IniFile.Free;
    except
      on E:Exception do
        WLog('Error in OpenList (Item=' + IntToStr(I) + '): ' + E.Message);
    end;

  finally
    Screen.Cursor := crDefault;
    // 20013548.10.
    RescaleAllObjects(ScreenWidth, ScreenHeight);
    RenumberDrawObjects;
    ShowFirstObject;
    ProductionScreenWaitF.Hide;
    if not tlbtnEdit.Down then
    begin
      MainActionForTimer; // 20015402 Refresh after open-file
    end;
  end;
end; // OpenList

procedure THomeF.SetCaption;
begin
  // MR:23-01-2008 RV002: Also show ComputerName.
  Caption := 'ORCL - ' + ORASystemDM.OracleSession.LogonDatabase + ' - ' +
    ThisCaption + ' - ' +
    ORASystemDM.CurrentComputerName + ' - ' +
    SchemePath + WorkFilename;
end;

function THomeF.DetermineWorkspotShortName(PlantCode,
  WorkspotCode: String; var ALinkJob: Boolean): String;
begin
  Result := WorkspotCode;
  with ProductionScreenDM do
  begin
    with cdsWorkspotShortName do
    begin
      if FindKey([PlantCode, WorkspotCode]) then
      begin
        Result := FieldByName('SHORT_NAME').Value;
        // 20013196 Also determine link-job here, if found, add a star to
        //          shortname.
        ALinkJob := FieldByName('LINK_JOB_CODE').AsString <> '';
        if ALinkJob then
          Result := Result + ' *';
      end;
    end;
  end;
end;

function THomeF.DetermineDepartmentDescription(PlantCode,
  DepartmentCode: String): String;
var
  WasActive: Boolean;
begin
  Result := DepartmentCode;
  with ProductionScreenDM do
  begin
    with odsDepartment do
    begin
      WasActive := Active;
      if not WasActive then
        Active := True;
      try
        if Locate('PLANT_CODE;DEPARTMENT_CODE',
          VarArrayOf([PlantCode, DepartmentCode]), []) then
          Result := FieldByName('DESCRIPTION').Value;
      except
        Result := DepartmentCode;
      end;
      if not WasActive then
        Active := False;
    end;
  end;
end;

procedure THomeF.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  ATag: Integer;
  OldTop, OldLeft,
  OldWidth, OldHeight: Integer;
begin
  if Key = VK_F5 then MainActionForTimer; { F5 - Refresh }
  if not tlbtnEdit.Down then Exit;

  OldTop := 0; OldHeight := 0;
  OldLeft := 0; OldWidth := 0;

  if CurrentDrawObject <> nil then
    begin
      Sender := CurrentDrawObject.AObject;
      if (Sender is TImage) then
        begin
          OldTop := (Sender as TImage).Top;
          OldLeft := (Sender as TImage).Left;
          OldHeight := (Sender as TImage).Height;
          OldWidth := (Sender as TImage).Width;
        end;
      if (sender is TShapeEx) then
        begin
          OldTop := (Sender as TShapeEx).Top;
          OldLeft := (Sender as TShapeEx).Left;
          OldHeight := (Sender as TShapeEx).Height;
          OldWidth := (Sender as TShapeEx).Width;
        end;
    end;

  // Goto next Object
  // Key = ENTER
  if (Key = 13) then // ENTER pressed
    begin
      if CurrentDrawObject <> nil then
        begin
          ATag := -1;
          if (CurrentDrawObject.AObject is TImage) then
            ATag := (CurrentDrawObject.AObject as TImage).Tag
          else
            if (CurrentDrawObject.AObject is TShapeEx) then
              ATag := (CurrentDrawObject.AObject as TShapeEx).Tag;
          if ATag <> -1 then
            begin
              inc(ATag);
              CurrentDrawObject := ThisDrawObject(ATag);
              if (CurrentDrawObject = nil) then
                begin
                  ATag := 1;
                  CurrentDrawObject := ThisDrawObject(ATag);
                end;
            end;
        end;
      if CurrentDrawObject <> nil then
        ShowSelectionRectangle(CurrentDrawObject.AObject, SelectionRectangle);
    end
  else
    begin
      // Move Object 1 pixel per step
      // CTRL + UP, DOWN, LEFT, RIGHT
      if (ssCtrl in Shift) then
        begin
          if CurrentDrawObject <> nil then
            begin
              Sender := CurrentDrawObject.AObject;
              case Key of
                VK_UP:    if (Sender is TImage) then
                            (Sender as TImage).Top := (Sender as TImage).Top - 1
                          else
                            if (Sender is TShapeEx) then
                              (Sender as TShapeEx).Top := (Sender as TShapeEx).Top - 1;
                VK_DOWN:  if (Sender is TImage) then
                            (Sender as TImage).Top := (Sender as TImage).Top + 1
                          else
                            if (Sender is TShapeEx) then
                              (Sender as TShapeEx).Top := (Sender as TShapeEx).Top + 1;
                VK_LEFT:  if (Sender is TImage) then
                            (Sender as TImage).Left := (Sender as TImage).Left - 1
                          else
                            if (Sender is TShapeEx) then
                              (Sender as TShapeEx).Left := (Sender as TShapeEx).Left - 1;
                VK_RIGHT: if (Sender is TImage) then
                            (Sender as TImage).Left := (Sender as TImage).Left + 1
                          else
                            if (Sender is TShapeEx) then
                              (Sender as TShapeEx).Left := (Sender as TShapeEx).Left + 1;
              end; {case}
            end;
          ShowDrawObject(CurrentDrawObject, Sender);
          ShowSelectionRectangle(Sender, SelectionRectangle);
        end;

      // Size Object 1 pixel per step
      // SHIFT + UP, DOWN, LEFT, RIGHT
      if (ssShift in Shift) then
        begin
          if CurrentDrawObject <> nil then
            begin
              Sender := CurrentDrawObject.AObject;
                case Key of
                  VK_UP:    if (Sender is TShapeEx) then
                              if ((Sender as TShapeEx).Shape = stLineVert) or
                                 ((Sender as TShapeEx).Shape = TShapeExType(RECTANGLE)) then
                                (Sender as TShapeEx).Height := (Sender as TShapeEx).Height - 1;
                  VK_DOWN:  if (Sender is TShapeEx) then
                              if ((Sender as TShapeEx).Shape = stLineVert) or
                                 ((Sender as TShapeEx).Shape = TShapeExType(RECTANGLE)) then
                                (Sender as TShapeEx).Height := (Sender as TShapeEx).Height + 1;
                  VK_LEFT:  if (Sender is TShapeEx) then
                              if ((Sender as TShapeEx).Shape = stLineHorz) or
                                 ((Sender as TShapeEx).Shape = TShapeExType(RECTANGLE)) then
                                (Sender as TShapeEx).Width := (Sender as TShapeEx).Width - 1;
                  VK_RIGHT: if (Sender is TShapeEx) then
                              if ((Sender as TShapeEx).Shape = stLineHorz) or
                                 ((Sender as TShapeEx).Shape = TShapeExType(RECTANGLE)) then
                                (Sender as TShapeEx).Width := (Sender as TShapeEx).Width + 1;
                end; {case}
            end;
          ShowDrawObject(CurrentDrawObject, Sender);
          ShowSelectionRectangle(Sender, SelectionRectangle);
        end;
    end;
	
  // Has something changed?
  if CurrentDrawObject <> nil then
    begin
      Sender := CurrentDrawObject.AObject;
      if not Dirty then
        begin
          if (Sender is TImage) then
            Dirty := (OldTop <> (Sender as TImage).Top) or
                     (OldLeft <> (Sender as TImage).Left) or
                     (OldHeight <> (Sender as TImage).Height) or
                     (OldWidth <> (Sender as TImage).Width);
          if (Sender is TShapeEx) then
            Dirty := (OldTop <> (Sender as TShapeEx).Top) or
                     (OldLeft <> (Sender as TShapeEx).Left) or
                     (OldHeight <> (Sender as TShapeEx).Height) or
                     (OldWidth <> (Sender as TShapeEx).Width);
        end;
    end;
end;

function THomeF.NewPuppet(ADrawObject: PDrawObject; AParent: TWinControl;
  APTEmployee: PTEmployee): PTPuppet;
var
  APuppet: PTPuppet;
begin
  APuppet := new(PTPuppet);
  // Head
  APuppet.Puppet[0] := TShape.Create(Application);
  with APuppet.Puppet[0] do
  begin
    Parent := AParent;
    Shape := stCircle;
    Height := Trunc(10 * WorkspotScale / 100);
    Width := Trunc(10 * WorkspotScale / 100);
    Left := Trunc(2 * WorkspotScale / 100);
    Top := Trunc(0  * WorkspotScale / 100);
    Visible := False;
    ShowHint := True;
    OnClick := ShowEmployees1Click;
    OnMouseDown := PuppetMouseDown;
    Tag := ADrawObject.ALabel.Tag;
    MyTag := APTEmployee.AEmployeeNumber; // 20014550.50
    Hint := //IntToStr(APTEmployee.AEmployeeNumber) + ' - ' +
      APTEmployee.AEmployeeName; // 20014550.50
    ACurrEff := APTEmployee.ACurrEff; // 20014550.50
    AShiftEff :=  APTEmployee.AShiftEff; // 20014550.50
  end;
  // Body
  APuppet.Puppet[1] := TShape.Create(Application);
  with APuppet.Puppet[1] do
  begin
    Parent := AParent;
    Shape := stRectangle;
    Height := Trunc(11 * WorkspotScale / 100);
    Width := Trunc(14 * WorkspotScale / 100);
    Left := Trunc(0 * WorkspotScale / 100);
    Top := Trunc(10 * WorkspotScale / 100);
    Visible := False;
    ShowHint := True;
    OnClick := ShowEmployees1Click;
    OnMouseDown := PuppetMouseDown;
    Tag := ADrawObject.ALabel.Tag;
    MyTag := APTEmployee.AEmployeeNumber; // 20014550.50
    Hint := //IntToStr(APTEmployee.AEmployeeNumber) + ' - ' +
      APTEmployee.AEmployeeName; // 20014550.50
    ACurrEff := APTEmployee.ACurrEff; // 20014550.50
    AShiftEff :=  APTEmployee.AShiftEff; // 20014550.50
  end;
  // Legs
  APuppet.Puppet[2] := TShape.Create(Application);
  with APuppet.Puppet[2] do
  begin
    Parent := AParent;
    Shape := stRectangle;
    Height := Trunc(10 * WorkspotScale / 100);
    Width := Trunc(8 * WorkspotScale / 100);
    Left := Trunc(3 * WorkspotScale / 100);
    Top := Trunc(20 * WorkspotScale / 100);
    Visible := False;
    ShowHint := True;
    OnMouseDown := PuppetMouseDown;
    OnClick := ShowEmployees1Click;
    Tag := ADrawObject.ALabel.Tag;
    MyTag := APTEmployee.AEmployeeNumber; // 20014550.50
    Hint := //IntToStr(APTEmployee.AEmployeeNumber) + ' - ' +
      APTEmployee.AEmployeeName; // 20014550.50
    ACurrEff := APTEmployee.ACurrEff; // 20014550.50
    AShiftEff :=  APTEmployee.AShiftEff; // 20014550.50
  end;
  APuppet.AColor := APTEmployee.AColor;
  APuppet.AEmployeeNumber := APTEmployee.AEmployeeNumber;
  APuppet.AEmployeeDescription := APTEmployee.AEmployeeName;
  APuppet.AEmployeeShortName := APTEmployee.AEmployeeShortName;
  APuppet.AEmployeeTeamCode := APTEmployee.ATeamCode;
  APuppet.AEmpLevel := APTEmployee.AEmpLevel;
  APuppet.AScannedWS := APTEmployee.AScanWorkspotCode;
  APuppet.AScannedDateTimeIn := APTEmployee.AScanDatetimeIn;
  APuppet.APlannedDept := APTEmployee.APlanDepartmentCode; // PIM-275
  APuppet.APlannedWS := APTEmployee.APlanWorkspotCode;
  APuppet.APlannedStartTime := APTEmployee.APlanStartDate;
  APuppet.APlannedEndTime := APTEmployee.APlanEndDate;
  APuppet.AFirstPlanStartDate := APTEmployee.AFirstPlanStartDate; // PIM-237
  APuppet.AFirstScan := APTEmployee.AFirstScan; // PIM-237
  APuppet.AEPlanLevel := APTEmployee.APlanLevel;
  APuppet.AScanned := APTEmployee.AScanned;
  APuppet.AJobCode := APTEmployee.AJobCode;
  APuppet.ACurrEff := APTEmployee.ACurrEff; // 20014550.50
  APuppet.AShiftEff := APTEmployee.AShiftEff; // 20014550.50
  Result := APuppet;
end;

procedure THomeF.DeletePuppet(var APuppet: PTPuppet);
var
  I: Integer;
begin
  for I:=MAX_PUPPET_PARTS downto 0 do
    APuppet.Puppet[I].Free;
end;

procedure THomeF.DeletePuppetList(var APuppetList: TList);
var
  I: Integer;
  APuppet: PTPuppet;
begin
  for I:=APuppetList.Count - 1  downto 0 do
  begin
    APuppet := APuppetList.Items[I];
    DeletePuppet(APuppet);
    APuppetList.Remove(APuppet);
  end;
  APuppetList.Free;
end;

procedure THomeF.DeletePuppetShiftList(var APuppetShiftList: TList);
var
  I: Integer;
  APuppetShift: PTPuppetShift;
begin
  if APuppetShiftList <> nil then
  begin
    for I:=APuppetShiftList.Count - 1 downto 0 do
    begin
      APuppetShift := APuppetShiftList.Items[I];
      DeletePuppetList(APuppetShift.APuppetList);
      APuppetShiftList.Remove(APuppetShift);
    end;
    APuppetShiftList.Free;
    APuppetShiftList := nil;
  end;
end;

procedure THomeF.ShowPuppet(APuppet: PTPuppet;
  ALeft, ATop: Integer; AColor: TColor);
begin
  // Head
  with APuppet.Puppet[0] do
  begin
    Brush.Color := AColor;
    Pen.Color := AColor;
    Left := ALeft + Trunc(2 * WorkspotScale / 100);
    Top := ATop;
    ShowHint := ORASystemDM.PSShowEmpInfo; // PIM-223
    Visible := True;
  end;
  // Body
  with APuppet.Puppet[1] do
  begin
    Brush.Color := AColor;
    Pen.Color := AColor;
    Left := ALeft + Trunc(0 * WorkspotScale / 100);
    Top := ATop + Trunc(10 * WorkspotScale / 100);
    ShowHint := ORASystemDM.PSShowEmpInfo; // PIM-223
    Visible := True;
  end;
  // Legs
  with APuppet.Puppet[2] do
  begin
    Brush.Color := AColor;
    Pen.Color := AColor;
    Left := ALeft + Trunc(3 * WorkspotScale / 100);
    Top := ATop + Trunc(20 * WorkspotScale / 100);
    ShowHint := ORASystemDM.PSShowEmpInfo; // PIM-223
    Visible := True;
  end;
end;

procedure THomeF.actOpenExecute(Sender: TObject);
var
  Path: String;
//  OtherPath: String;
begin
  inherited;
  AskForSave;
  Path := GetCurrentDir;
  try
    OpenDialog1.InitialDir := SchemePath;
    OpenDialog1.Filename := WorkFilename;
    if OpenDialog1.Execute then
    begin
      WorkFilename := ExtractFileName(OpenDialog1.Filename);
      // Do not copy the file!
      // GLOB3-231
{
      // If file came from another path, copy the file to
      // SchemePath
      OtherPath := ExtractFilePath(OpenDialog1.Filename);
      if OtherPath <> SchemePath then
        CopyFile(PChar(OtherPath + WorkFilename),
          PChar(SchemePath + WorkFilename), False);
}
      // Now read the file
      OpenList(WorkFilename);
    end;
    SetCaption;
  finally
    SetCurrentDir(Path);
  end;
  Dirty := False;
end;

procedure THomeF.actNewExecute(Sender: TObject);
begin
  inherited;
  AskForSave;
  FreeList;
  List := TList.Create;
  Dirty := False;
  Workfilename := DEFAULT_WORKFILENAME;
  SetCaption;
end;

procedure THomeF.actSaveAsExecute(Sender: TObject);
var
  Path: String;
begin
  inherited;
  Path := GetCurrentDir;
  try
    SaveDialog1.InitialDir := SchemePath;
    SaveDialog1.Filename := WorkFilename;
    if SaveDialog1.Execute then
    begin
      WorkFilename := ExtractFileName(SaveDialog1.Filename);
      SaveList(WorkFilename);
    end;
    SetCaption;
  finally
    SetCurrentDir(Path);
  end;
end;

// Search For Last (in DateTime) ProductionQuantity Record
function THomeF.SearchLastProductionQuantityRecord(
  ADrawObject: PDrawObject;
  var Quantity: Double): Boolean;
var
  ADate, DateFrom, DateTo: TDateTime;
begin
  Result := False;
  Quantity := 0;
  ADate := Now;

  with ProductionScreenDM do
  begin
    with odsProductionQuantity do
    begin
      DateFrom := ADate -
        (PIMSDatacolTimeInterval * PIMSMultiplier / 60 / 24 / 60);
      DateTo := ADate;
      // Now search for all records in given period
      ClearVariables;
      SetVariable('PLANT_CODE',    ADrawObject.APlantCode);
      SetVariable('WORKSPOT_CODE', ADrawObject.AWorkspotCode);
      SetVariable('JOB_CODE',      NOJOB); // SO-20013516
      SetVariable('DATEFROM',      DateFrom);
      SetVariable('DATETO',        MyDateTo(DateTo));
      Open;
      if RecordCount > 0 then
      begin
        Last;
        Result := True;
        Quantity := FieldByName('QUANTITYSUM').Value;
      end;
    end;
  end;
end;

// MR:10-03-2004
procedure THomeF.DetermineSinceDates(MyNow: TDateTime;
  var DateFrom, DateTo: TDateTime);
var
  Hour1, Min1, Sec1, MSec1: Word;
  Hour2, Min2, Sec2, MSec2: Word;
begin
  DateTo := MyNow;
  DecodeTime(EfficiencyPeriodSinceTime, Hour1, Min1, Sec1, MSec1);
  DecodeTime(DateTo, Hour2, Min2, Sec2, MSec2);
  // If Hour1 = 23 and Hour2 = 9 then we look at yesterday
  if Hour1 > Hour2 then
    DateFrom := Trunc(DateTo - 1) + Frac(EfficiencyPeriodSinceTime)
  else
    DateFrom := Trunc(DateTo) + Frac(EfficiencyPeriodSinceTime);
end;

procedure THomeF.ShowIndicators(ADrawObject: PDrawObject;
  ARed, AYellow, AGreen: TColor);
begin
  ADrawObject.ALightRed.Brush.Color := ARed;
  ADrawObject.ALightYellow.Brush.Color := AYellow;
  ADrawObject.ALightGreen.Brush.Color := AGreen;
end;

procedure THomeF.ActionConnectionIndicators(ADrawObject: PDrawObject);
var
  Quantity: Double;
begin
  // ConnectionIndicator
  if SearchLastProductionQuantityRecord(ADrawObject, Quantity) then
  begin
    if Quantity = 0 then
      // Yellow and Green light on
      ShowIndicators(ADrawObject, clWhite, clMyYellow, clMyGreen)
    else
      // Green light on
      ShowIndicators(ADrawObject, clWhite, clWhite, clMyGreen);
  end
  else
    // Red light on
    ShowIndicators(ADrawObject, clMyRed, clWhite, clWhite);
end;

// Search the shift in APupperShiftList by ShiftNumber
// If not found, create it.
// Result is Index of shift.
function THomeF.SearchCreateShift(var ADrawObject: PDrawObject;
  const AShiftNumber: Integer): Integer;
var
  ShiftIndex: Integer;
  CreateNew: Boolean;
  APuppetShift: PTPuppetShift;
begin
  CreateNew := True;
  Result := 0;
  for ShiftIndex := 0 to ADrawObject.APuppetShiftList.Count - 1 do
  begin
    APuppetShift := ADrawObject.APuppetShiftList.Items[ShiftIndex];
    if APuppetShift.AShiftNumber = AShiftNumber then
    begin
      Result := ShiftIndex;
      CreateNew := False;
      Break;
    end;
  end;
  if CreateNew then
  begin
    ADrawObject.APuppetShiftList.Add(New(PTPuppetShift));
    ShiftIndex := ADrawObject.APuppetShiftList.Count;
    APuppetShift := ADrawObject.APuppetShiftList.Items[ShiftIndex-1];
    APuppetShift.AShiftNumber := AShiftNumber;
    APuppetShift.APuppetList := TList.Create;
    Result := ShiftIndex - 1;
  end;
end;

procedure THomeF.ClearEmployeeList(ADrawObject: PDrawObject);
var
  APTAEmployee: PTAEmployee;
  I: Integer;
begin
  if Assigned(ADrawObject.AEmployeeList) then
  begin
    for I := ADrawObject.AEmployeeList.Count - 1  downto 0 do
    begin
      APTAEmployee := ADrawObject.AEmployeeList.Items[I];
      APTAEmployee.AEmployeeNameLabel.Visible := False; // PIM-213
      APTAEmployee.AEmployeeNameLabel.Free;
      Dispose(APTAEmployee);
      ADrawObject.AEmployeeList.Remove(APTAEmployee);
    end;
    ADrawObject.AEmployeeList.Clear;
  end;
end;

function EmployeeCompare(Item1, Item2: Pointer): Integer;
var
  Employee1, Employee2: PTEmployee;
begin
  Result := 0;
  Employee1 := Item1;
  Employee2 := Item2;
  if (Employee1.AEmployeeNumber < Employee2.AEmployeeNumber) then
    Result := -1
  else
    if (Employee1.AEmployeeNumber > Employee2.AEmployeeNumber) then
      Result := 1;
end;

// This creates puppets for ADrawObject based on an EmployeeList
// that is made via PlanScanClass-functions.
procedure THomeF.ActionCreatePuppets(ADrawObject: PDrawObject;
  var AEmployeeList: TList);
var
  I, J: Integer;
  APTEmployee, APTEmployee2: PTEmployee;
  APuppetShift: PTPuppetShift;
  ShiftIndex: Integer;
{$IFDEF DEBUG5}
  MyColor: String;
{$ENDIF}
  // PIM-275 Is the employee already added for another workspot?
  function FindPuppet(MyAPTEmployee: PTEmployee): Boolean;
  var
    I, PupI, PupJ: Integer;
    APuppetShift: PTPuppetShift;
    APuppet: PTPuppet;
    MyDrawObject: PDrawObject;
  begin
    Result := False;
    for I := 0 to List.Count - 1 do
    begin
      MyDrawObject := List.Items[I];
      if (ADrawObject.AObject is TImage) then
      begin
        if Assigned(MyDrawObject.APuppetShiftList) then
        begin
          if MyDrawObject.APuppetShiftList.Count > 0 then
          begin
            for PupI := 0 to MyDrawObject.APuppetShiftList.Count - 1 do
            begin
              APuppetShift := MyDrawObject.APuppetShiftList.Items[PupI];
              if APuppetShift.APuppetList.Count > 0 then
              begin
                for PupJ := 0 to APuppetShift.APuppetList.Count - 1 do
                begin
                  APuppet := APuppetShift.APuppetList.Items[PupJ];
                  if (APuppet.AEmployeeNumber = MyAPTEmployee.AEmployeeNumber)
                  then
                  begin
                    Result := True;
                    Break;
                  end;
                end; // for PupJ
              end; // if
            end; // for PupI
          end; // if
        end; // if
      end; // if
    end; // for I
  end; // FindPuppet
  procedure CreatePuppets;
  var
    I: Integer;
    OK: Boolean;
  begin
    // Now create the puppetlist
    for I := 0 to AEmployeeList.Count - 1 do
    begin
      APTEmployee := AEmployeeList.Items[I];
      OK := True;
      // PIM-275 When planning is on department the do not show a puppet
      //         that is scanned-in on a different workspot but belonging
      //         to the same department.
      if APTEmployee.APlanWorkspotCode = '0' then
        if APTEmployee.AScanWorkspotCode <> '' then
          if APTEmployee.APlantCode = ADrawObject.APlantCode then
            if APTEmployee.AScanWorkspotCode <> ADrawObject.AWorkspotCode then
              OK := False;
      if OK then
      begin
        // PIM-275 Plan on department: Skip employee when it was already added
        if (APTEmployee.APlanWorkspotCode = '0') then
          if (APTEmployee.AScanDatetimeIn = NullDate) then
            if FindPuppet(APTEmployee) then
              APTEmployee.AKeep := False;
        if APTEmployee.AKeep then
        begin
          ShiftIndex := SearchCreateShift(ADrawObject, APTEmployee.AShiftNumber);
          APuppetShift := ADrawObject.APuppetShiftList.Items[ShiftIndex];
         if APuppetShift <> nil then
             APuppetShift.APuppetList.Add(
              NewPuppet(ADrawObject, pnlDraw, APTEmployee));
        end;
      end; // if OK
    end; // for
  end; // CreatePuppets
begin
  // Now create the puppet-list
{$IFDEF DEBUG5}
  WLog('-> ActionCreatePuppets - START');
{$ENDIF}

  // First Delete the Puppets
  DeletePuppetShiftList(ADrawObject.APuppetShiftList);
  ADrawObject.APuppetShiftList := TList.Create;

  if not Assigned(AEmployeeList) then
    Exit;

  // Now decide which employees have to be kept or left out.
  // This depends on the color of an employee,
  // if color is Yellow or Orange, a planned employee
  // should be left out.
  if AEmployeeList.Count > 1 then
    AEmployeeList.Sort(EmployeeCompare);
  for I := 0 to AEmployeeList.Count - 1 do
  begin
    APTEmployee := AEmployeeList.Items[I];
{$IFDEF DEBUG5}
  MyColor := 'None';
  if APTEmployee.AColor = clGray then
    MyColor := 'Gray'
  else
    if APTEmployee.AColor = clRed then
      MyColor := 'Red'
    else
      if APTEmployee.AColor = clGreen then
        MyColor := 'Green';
  WLog(' WS=' + ADrawObject.AWorkspotCode +
    ' Emp=' + IntToStr(APTEmployee.AEmployeeNumber) +
    ' Dept=' + APTEmployee.APlanDepartmentCode +
    ' PlanWS=' + APTEmployee.APlanWorkspotCode +
    ' ScanWS=' + APTEmployee.AScanWorkspotCode +
    ' SDate=' + DateTimeToStr(APTEmployee.AScanDatetimeIn) +
    ' FirstPlan=' + DateTimeToStr(APTEmployee.AFirstPlanStartDate) +
    ' FirstScan=' + DateTimeToStr(APTEmployee.AFirstScan) +
    ' Color=' + MyColor +
    ' Keep=' + Bool2Str(APTEmployee.AKeep)
    );
{$ENDIF}
    if (APTEmployee.AColor = clMyYellow) or
      (APTEmployee.AColor = clMyOrange) then
    begin
      for J := 0 to AEmployeeList.Count - 1 do
      begin
        APTEmployee2 := AEmployeeList.Items[J];
        // TD-21162 Do not compare with shift!
        if (APTEmployee2.AKeep) {and
          (APTEmployee.AShiftNumber = APTEmployee2.AShiftNumber)} then
        begin
          if APTEmployee2.AColor = clGray then
          begin
            APTEmployee2.AKeep := False;
            Break;
          end;
        end;
      end;
    end;
  end; // for
  CreatePuppets;
{$IFDEF DEBUG5}
  WLog('-> ActionCreatePuppets - END');
{$ENDIF}
end; // ActionCreatePuppets

procedure THomeF.ActionPuppets(ADrawObject: PDrawObject);
begin
  // This gives as result a 'APlanScanClass.EmployeeWSList'
  APlanScanClass.DetermineEmployeeWSListPLAN_SCAN_EMP(ADrawObject.APlantCode,
    ADrawObject.AWorkspotCode);

  ActionCreatePuppets(ADrawObject, APlanScanClass.EmployeeList);
  ShowPuppets(ADrawObject, ADrawObject.AObject);
  ADrawObject.AEmployeeCount := APlanScanClass.EmployeeList.Count;
end; // ActionPuppets

procedure THomeF.MainActionForTimer;
var
  ADrawObject: PDrawObject;
  LastDrawObject: PDrawObject;
  I: Integer;
  MyNow: TDateTime;
  // 20013516 Is there a puppet with jobcode 'down'?
  procedure DetermineIsDown;
  var
    PupI, PupJ: Integer;
    APuppetShift: PTPuppetShift;
    APuppet: PTPuppet;
  begin
    ADrawObject.ABlinkIsDown := False;
    ADrawObject.ADownJobCode := '';
    if ADrawObject.APuppetShiftList.Count > 0 then
    begin
      for PupI := 0 to ADrawObject.APuppetShiftList.Count - 1 do
      begin
        APuppetShift := ADrawObject.APuppetShiftList.Items[PupI];
        if APuppetShift.APuppetList.Count > 0 then
        begin
          for PupJ := 0 to APuppetShift.APuppetList.Count - 1 do
          begin
            APuppet := APuppetShift.APuppetList.Items[PupJ];
            if (APuppet.AJobCode = MECHANICAL_DOWN_JOB) or
              (APuppet.AJobCode = NO_MERCHANDIZE_JOB) then
            begin
              ADrawObject.ABlinkIsDown := True;
              ADrawObject.ADownJobCode := APuppet.AJobCode;
              Break;
            end;
          end; // for PupJ
        end; // if
        if ADrawObject.ABlinkIsDown then
          Break;
      end; // for PupI
    end; // if
  end; // DetermineIsDown
  // First determine an employeesList for all workspots the plan/scan-info
  // Then compare this with the machine-employeeList.
  // Based on this, create the puppets.
  procedure ActionCreateMachinePuppets;
  var
    I: Integer;
    APTEmployee: PTEmployee;
    function FindMachineEmployee(AEmployeeNumber: Integer): Boolean;
    var
      J: Integer;
      APTAEmployee: PTAEmployee;
    begin
      Result := False;
      for J := 0 to ADrawObject.AEmployeeList.Count - 1 do
      begin
        APTAEmployee := ADrawObject.AEmployeeList.Items[J];
        if APTAEmployee.AEmployeeNumber = AEmployeeNumber then
        begin
          Result := True;
          Break;
        end;
      end;
    end; // FindMachineEmployee
  begin
    // Determine for ALL plants/workspots
    APlanScanClass.DetermineEmployeeWSListPLAN_SCAN_EMP('', '');
    // Compare with ADrawObject.EmployeeList and only keep the ones for
    // the machine
    for I := 0 to APlanScanClass.EmployeeList.Count - 1 do
    begin
      APTEmployee := APlanScanClass.EmployeeList.Items[I];
      APTEmployee.AKeep := FindMachineEmployee(APTEmployee.AEmployeeNumber);
    end;
    ActionCreatePuppets(ADrawObject, APlanScanClass.EmployeeList);
    ShowPuppets(ADrawObject, ADrawObject.AObject);
    ADrawObject.AEmployeeCount := APlanScanClass.EmployeeList.Count;
  end; // ActionCreateMachinePuppets
begin
  try
    // Check connection and reconnect if not connected.
    ORASystemDM.OracleSession.CheckConnection(True);
    if not ORASystemDM.OracleSession.Connected then
      Exit;
  except
    //
  end;

  MyNow := Now; // 20016449
  LastDrawObject := nil;
  ShowMsg('');

  try
    Busy := True;
    MainTimerSwitch(False);

    // Create lists for planned/scanned employees
    PlanScanEmpDM.CurrentNow := MyNow;
    APlanScanClass.OpenEmployee;
    APlanScanClass.OpenEmployeePlanning(MyNow);
    APlanScanClass.OpenTimeRegScanning(MyNow);
    APlanScanClass.OpenWorkspotsPerEmployee; // TD-22082
    ActionCompareJobsInit(MyNow);
    ActionRealTimeEffMain; // 20014450.50
    TimerStartTime := SysUtils.Now;
    LastDrawObject := CurrentDrawObject;
    ShowActionTime;
    for I := 0 to List.Count - 1 do
    begin
      Application.ProcessMessages;
      ADrawObject := List.Items[I];
      if (ADrawObject.AObject is TImage) then
      begin
        with ProductionScreenDM do
        begin
          ShowSelectionRectangle(ADrawObject.AObject, SelectionRectangle);
          Update;
          ShowActionTime;
          if not ADrawObject.ABigEffMeters then
          begin
            ShowActionTime;
            // Show Connection Indicators
            ActionConnectionIndicators(ADrawObject);
          end;
          // Calculate Efficiency
          try
            ActionEfficiency(ADrawObject);
          except
            on E:Exception do
              WErrorLog(E.Message);
          end;
          // Determine Employees/Puppets that are planned/scanned today
          if (not ADrawObject.ABigEffMeters) and
            (not (ADrawObject.AProdScreenType = pstWSEmpEff)) then // PIM-153
          begin
            // 20014450.50
            // We have to know the plan/scan-info for all employees
            // on all workspots here!
            if ADrawObject.AProdScreenType = pstMachine then
              ActionCreateMachinePuppets
            else
              ActionPuppets(ADrawObject);
            // 20013516
            DetermineIsDown;
          end;
          // Decide if puppets should blink:
          // There is production, but no employees scanned in for
          // the jobs.
          if not (ADrawObject.AProdScreenType = pstWSEmpEff) then // PIM-153
          begin
            ActionBlinkPuppets;

            // Decide if there are jobs to compare
            ActionCompareJobs(ADrawObject, MyNow);
          end;
        end;
      end
      else
      begin
        if ADrawObject.AType = otPuppetBox then
        begin
          if ADrawObject.AShowMode = ShowHeadCount then
            ActionShowHeadCount(ADrawObject)
          else
            ActionPuppetBox(ADrawObject);
        end;
      end;
    end;
  finally
    TimerEndTime := SysUtils.Now;
    if LastDrawObject <> nil then
      ShowSelectionRectangle(LastDrawObject.AObject, SelectionRectangle);
    ShowActionTime;
    if not tlbtnEdit.Down then
      MainTimerSwitch(True);
    Busy := False;
  end;
end;

procedure THomeF.TimerMainTimer(Sender: TObject);
begin
  inherited;
  if not tlbtnEdit.Down then
    MainActionForTimer;
end;

procedure THomeF.FormShow(Sender: TObject);
begin
  inherited;
  ShowMsg('');
  
  // Set Button Tool Bars to Edit/Non-Edit
  actEditExecute(Sender);

  UseSoundAlarm := False;
  SoundFilename := 'success.wav';
  ReadRegistry;
  // CAR User rights  10-10-2003
  if DialogLoginF.AplEditYN = UNCHECKEDVALUE then
  begin
    actEdit.Enabled := False;
    actNew.Enabled := False;
    actSave.Enabled := False;
    actSaveAs.Enabled := False;
    FileSettingsAct.Enabled := False;
  end;
end;

procedure THomeF.ReadINIFile;
var
  Ini: TIniFile;
  Section: String;
  LastScheme: String;
  NewDate: TDateTime;
begin
  if not FileExists(PathCheck(AppRootPath) + ProdScreenINIFilename) then
    WriteINIFile;
  Ini := TIniFile.Create(PathCheck(AppRootPath) + ProdScreenINIFilename);
  try
    // GLOB3-231 First check if there is a schemepath from PIMSORA-section
    //           If found, then use that, not the ones per computername.
    if Ini.ValueExists('PIMSORA', 'SchemePath') then
      SchemePath := Ini.ReadString('PIMSORA', 'SchemePath', '');
    Section :=  ORASystemDM.CurrentComputerName;
    LastScheme := WorkFileName;
    LastScheme := Ini.ReadString(Section, 'LastScheme', LastScheme);
    // GLOB3-231 Get fixed path for schemes here
    if SchemePath = '' then
      if Ini.ValueExists(Section, 'SchemePath') then
        SchemePath := Ini.ReadString(Section, 'SchemePath', SchemePath);
    UseSoundAlarm := Ini.ReadBool(Section, 'UseSoundAlarm', UseSoundAlarm);
    SoundFilename := Ini.ReadString(Section, 'SoundFilename', SoundFilename);
    ExportSeparator := Ini.ReadString(Section, 'ExportSeparator', ExportSeparator);
    // This is a global workspotscale, it will also
    // be saved for each file.
    WorkspotScale := Ini.ReadInteger(Section, 'WorkspotScale', WorkspotScale);
    FontScale := Ini.ReadInteger(Section, 'FontScale', FontScale);
    RefreshTimeInterval := Ini.ReadInteger(Section, 'RefreshTimeInterval', RefreshTimeInterval);
    // 20013550
    GridLineDistance :=
      Ini.ReadInteger(Section, 'GridLineDistance', GridLineDistance);
    if LastScheme <> '' then
      WorkFileName := ExtractFileName(LastScheme);
    try
      // Used only for testingpurposes
      if Ini.ValueExists(Section, 'NewDate') then
      begin
        NewDate := Ini.ReadDate(Section, 'NewDate', Date);
        ORASystemDM.Timewarp := Round(NewDate - Date);
      end;
    except
      ORASystemDM.Timewarp := 0;
    end;
  finally
    Ini.Free;
  end;
end;

procedure THomeF.WriteINIFile;
var
  Ini: TIniFile;
  Section: String;
begin
  Ini := TIniFile.Create(PathCheck(AppRootPath) + ProdScreenINIFilename);
  try
    Section :=  ORASystemDM.CurrentComputerName;
    Ini.WriteString(Section, 'LastScheme', ExtractFileName(WorkFilename));
    Ini.WriteString('PIMSORA', 'SchemePath', SchemePath); // GLOB3-231
    Ini.WriteString(Section, 'SchemePath', SchemePath); // GLOB3-231
    Ini.WriteBool(Section, 'UseSoundAlarm', UseSoundAlarm);
    Ini.WriteString(Section, 'SoundFilename', SoundFilename);
    Ini.WriteString(Section, 'ExportSeparator', ExportSeparator);
    // This is a global workspotscale, it will also
    // be saved for each file.
    Ini.WriteInteger(Section, 'WorkspotScale', WorkspotScale);
    Ini.WriteInteger(Section, 'FontScale', FontScale);
    Ini.WriteInteger(Section, 'RefreshTimeInterval', RefreshTimeInterval);
    // 20013550
    Ini.WriteInteger(Section, 'GridLineDistance', GridLineDistance);
  finally
    Ini.Free;
  end;
end;

procedure THomeF.ReadRegistry;
begin
  try
    ReadINIFile;
  except
    // Ignore error
  end;
end;

procedure THomeF.WriteRegistry;
begin
  try
    WriteINIFile;
  except
    // Ignore error
  end;
end;

procedure THomeF.FormCreate(Sender: TObject);
var
  Path: String;
  PimsRootPath: String;
begin
  inherited;
  MultiSelectActive := false;//23466

  // PIM-223
  ORASystemDM.PSShowEmpInfo := True;
  ORASystemDM.PSShowEmpEff := True;

  // 20014289
  stBarBase.Color := clDarkRed; // clPimsBlue; // PIM-250

  // 20013548
  CurrentSchemeWidth := 0;
  CurrentSchemeHeight := 0;

  // 20013548.10.
  ScreenWidth := Screen.Width;
  ScreenHeight := Screen.Height;

  APlanScanClass := TPlanScanClass.Create;
  AppRootPath := GetCurrentDir;
  // 20013550
  // pnlDraw is of a different type: TPaintPanel instead of TPanel
  pnlDraw := TPaintPanel.Create(nil);
  with pnlDraw do
  begin
    Parent := pnlInsertBase;
    Left := 0;
    Top := 0;
    Width := 683;
    Height := 391;
    Align := alClient;
    BevelOuter := TBevelCut(bvNone);
    Color := clActiveBorder;
    PopupMenu := PopupMainMenu;
    TabOrder := 0;
    OnMouseMove := pnlDrawMouseMove;
    OnPaint := PanelPaint;
    Color := clWhite; //clPimsLBlue; // 20014289
  end;
  // MRA: 8-JUN-2009 Not needed. RV029.
//  ORASystemDM.ProdScreen := True;
  Dirty := False;
  Busy := False;
  PIMSDatacolTimeInterval := DEFAULT_DATACOL_TIME_INTERVAL;
  PIMSMultiplier := DEFAULT_MULTIPLIER;
  RefreshTimeInterval := DEFAULT_REFRESH_TIME_INTERVAL;
  ThisCaption := Copy(Caption, 8, Length(Caption));
  FDragging := False;
  InitForm := True;
  WorkspotScale := DEFAULT_WORKSPOT_SCALE;
  FontScale := DEFAULT_WORKSPOT_SCALE;
  EfficiencyPeriodMode := epCurrent;
  EfficiencyPeriodSince := False;
  EfficiencyPeriodSinceTime := EncodeTime(7, 0, 0, 0);
  Path := GetCurrentDir;
  // Determine Path from where Pims has been started.
  PimsRootPath := PathCheck(Path);
  if Pos(UpperCase('\Bin'), UpperCase(Path)) > 0 then
    PimsRootPath := PathCheck(Copy(Path, 1,
      Pos(UpperCase('\Bin'), UpperCase(Path))));

  // Init variables
  LogPath := PimsRootPath + 'Bin\';
  ImagePath := PimsRootPath + 'Bitmaps\';
  SchemePath := PimsRootPath + 'Schemes\';
  SoundPath := PimsRootPath + 'Sounds\';
  ExportPath := PimsRootPath + 'Export\';
  if not DirectoryExists(ImagePath) then
    ForceDirectories(ImagePath);
  if not DirectoryExists(SchemePath) then
    ForceDirectories(SchemePath);
  if not DirectoryExists(SoundPath) then
    ForceDirectories(SoundPath);
  if not DirectoryExists(ExportPath) then
    ForceDirectories(ExportPath);
  ExportSeparator := ',';
  WorkFilename := DEFAULT_WORKFILENAME;
//  PIMSTimerInterval := TIMER_INTERVAL;

  List := TList.Create;
  CurrentDrawObject := nil;
  // Following is a rectangle that can show the Current Selected Object
  SelectionRectangle := TShapeEx.Create(Application);
  SelectionRectangle.Pen.Width := 2;
  SelectionRectangle.Parent := pnlDraw;
  SelectionRectangle.Pen.Color := clMyRed;
  SelectionRectangle.Shape := TShapeExType(RECTANGLE);
  SelectionRectangle.Brush.Style := bsClear;
  SelectionRectangle.Visible := False;

  // Get Pims-Settings
  with ORASystemDM.odsPimsSettings do
  begin
    if not Active then
      Active := True;
    try
      if FieldByName('DATACOL_TIME_INTERVAL').AsString <> '' then
        PIMSDatacolTimeInterval :=
          FieldByName('DATACOL_TIME_INTERVAL').Value;
    except
      PIMSDatacolTimeInterval := DEFAULT_DATACOL_TIME_INTERVAL;
    end;
  end;

  with ProductionScreenDM do
  begin
    cdsWorkspotShortName.Open;
    odsJobcode.Active := True;
  end;

  // Init a list for getting daynr!
  InitDayList;

  // Personal Screen -> This is a global setting (PIMSSETTING.EFF_BO_QUANT_YN)
  if ORASystemDM.EffBoQuantYN = 'Y' then
    EffQuantityTime := efQuantity
  else
    EffQuantityTime := efTime;

  // 20013550
  GridLineDistance := DefaultGridLineDistance;
end;

procedure THomeF.ShowEmployees(const AShowMode: TShowMode);
begin
  if AShowMode = ShowEmployeesOnWorkspot then // Workspot-specific
    if CurrentDrawObject = nil then
      Exit;

//  DialogShowAllEmployeesF := TDialogShowAllEmployeesF.Create(Application);
  if AShowMode = ShowEmployeesOnWorkspot then
  begin
    try
      DialogShowAllEmployeesF.MyTitle :=
        PopupWorkspotMenu.Items[1].Caption;
    except
      DialogShowAllEmployeesF.MyTitle := '';
    end;
    DialogShowAllEmployeesF.PlantCode := CurrentDrawObject.APlantCode;
    DialogShowAllEmployeesF.WorkspotCode := CurrentDrawObject.AWorkspotCode;
    DialogShowAllEmployeesF.WorkspotDescription :=
      CurrentDrawObject.AWorkspotDescription;
    DialogShowAllEmployeesF.MyDrawObject := CurrentDrawObject;
  end
  else
  begin
    try
      DialogShowAllEmployeesF.MyTitle :=
        PopupMainMenu.Items[Integer(AShowMode) - 1].Caption;
    except
      DialogShowAllEmployeesF.MyTitle := '';
    end;
  end;

  try
    if not tlbtnEdit.Down then
      MainTimerSwitch(False);
    DialogShowAllEmployeesF.ShowMode := AShowMode;
    RepositionSubForm(DialogShowAllEmployeesF);
    DialogShowAllEmployeesF.ShowModal;
  finally
//    DialogShowAllEmployeesF.Free;
    if not tlbtnEdit.Down then
      MainTimerSwitch(True);
  end;
end;

procedure THomeF.ShowAllEmployees1Click(Sender: TObject);
begin
  inherited;
  ShowEmployees(ShowAllEmployees);
end;

procedure THomeF.ShowEmployeesonwrongWokspot1Click(Sender: TObject);
begin
  inherited;
  ShowEmployees(ShowEmployeesOnWrongWorkspot);
end;

procedure THomeF.ShowEmployeesnotscannedin1Click(Sender: TObject);
begin
  inherited;
  ShowEmployees(ShowEmployeesNotScannedIn);
end;

procedure THomeF.ShowEmployeesabsentwithreason1Click(Sender: TObject);
begin
  inherited;
  ShowEmployees(ShowEmployeesAbsentWithReason);
end;

procedure THomeF.ShowEmployeesabsentwithoutreason1Click(Sender: TObject);
begin
  inherited;
  ShowEmployees(ShowEmployeesAbsentWithoutReason);
end;

procedure THomeF.ShowEmployeeswithFirstAid1Click(Sender: TObject);
begin
  inherited;
  ShowEmployees(ShowEmployeesWithFirstAid);
end;

procedure THomeF.FileExitActExecute(Sender: TObject);
begin
  inherited;
  AskForSave;
end;

// Blink the red Efficiency-meter
// Blink the Puppets if necessary
procedure THomeF.TimerBlinkTimer(Sender: TObject);
var
  MainI, PupI, PupJ, PupK: Integer;
  APuppetShift: PTPuppetShift;
  APuppet: PTPuppet;
  ADrawObject: PDrawObject;
begin
  inherited;
  if not tlbtnEdit.Down then
  begin
    for MainI := 0 to List.Count - 1 do
    begin
      ADrawObject := List.Items[MainI];
      // PIM-151
      // Show and blink Ghost if needed
      if (ADrawObject.AObject is TImage) then
      begin
        // Toggle between Ghost and default image
        if ADrawObject.AGhostCountBlink then
        begin
          // Skip when there are employee scanned in.
          if ADrawObject.ACurrentNumberOfEmployees > 0 then
            Continue;
          // For BigEffMeter: Do not assign it to AObject, only make it
          // visible or not.
          if ADrawObject.ABigEffMeters then
          begin
            if not ADrawObject.AGhostImage.Visible then
            begin
              // Show Ghost
              ADrawObject.AGhostImage.Top := ADrawObject.ABorder.Top;
              ADrawObject.AGhostImage.Left :=
                ADrawObject.ABorder.Left + Round(ADrawObject.ABorder.Width * 1.1);
              ADrawObject.AGhostImage.Width := ADrawObject.ABorder.Height;
              ADrawObject.AGhostImage.Height := ADrawObject.ABorder.Height;
              ADrawObject.AGhostImage.Visible := True;
            end
            else
            begin
              // Do not show ghost
              ADrawObject.AGhostImage.Visible := False;
            end;
          end
          else
          begin
            if ADrawObject.AObject = ADrawObject.ADefaultImage then
            begin
              // Show ghost
              ADrawObject.ADefaultImage.Visible := False;
              ADrawObject.AGhostImage.Top := TImage(ADrawObject.AObject).Top;
              ADrawObject.AGhostImage.Left := TImage(ADrawObject.AObject).Left;
              ADrawObject.AGhostImage.Width := TImage(ADrawObject.AObject).Width;
              ADrawObject.AGhostImage.Height := TImage(ADrawObject.AObject).Height;
              ADrawObject.AGhostImage.Tag := TImage(ADrawObject.AObject).Tag;
              ADrawObject.AObject := ADrawObject.AGhostImage;
              TImage(ADrawObject.AObject).Visible := True;
            end
            else
            begin
              // Do not show ghost
              if Assigned(ADrawObject.ADefaultImage) then
              begin
                ADrawObject.AGhostImage.Visible := False;
                ADrawObject.AObject := ADrawObject.ADefaultImage;
                TImage(ADrawObject.AObject).Visible := True;
              end;
            end;
          end;
        end // if ADrawObject.AGhostCountBlink
        else
        begin
          // Do not show ghost
          if ADrawObject.ABigEffMeters then
            ADrawObject.AGhostImage.Visible := False
          else
          begin
            if Assigned(ADrawObject.ADefaultImage) then
            begin
              ADrawObject.AObject := ADrawObject.ADefaultImage;
              TImage(ADrawObject.AObject).Visible := True;
            end;
          end;
        end;
        Application.ProcessMessages;
      end; // if (ADrawObject.AObject is TImage)
      // Blink EffMeter if needed
      if ADrawObject.AEffMeterRed.Visible then
      begin
        if ADrawObject.AEffMeterRed.Brush.Color = clMyRed then
          ADrawObject.AEffMeterRed.Brush.Color := clWhite
        else
          ADrawObject.AEffMeterRed.Brush.Color := clMyRed;
      end;
      // Blink Puppets if needed
      if ADrawObject.ABlinkPuppets then
      begin
        if ADrawObject.APuppetShiftList.Count > 0 then
        begin
          for PupI := 0 to ADrawObject.APuppetShiftList.Count - 1 do
          begin
            APuppetShift := ADrawObject.APuppetShiftList.Items[PupI];
            if APuppetShift.APuppetList.Count > 0 then
            begin
              for PupJ := 0 to APuppetShift.APuppetList.Count - 1 do
              begin
                APuppet := APuppetShift.APuppetList.Items[PupJ];
                for PupK := 0 to MAX_PUPPET_PARTS do
                begin
                  if APuppet.Puppet[PupK].Brush.Color = APuppet.AColor then
                  begin
                    APuppet.Puppet[PupK].Brush.Color := clWhite;
                    APuppet.Puppet[PupK].Pen.Color := clWhite;
                  end
                  else
                  begin
                    APuppet.Puppet[PupK].Brush.Color := APuppet.AColor;
                    APuppet.Puppet[PupK].Pen.Color := APuppet.AColor;
                  end;
                end;
              end;
            end;
          end;
        end;
      end;
      // Blink border if needed
      if ADrawObject.ABlinkCompareJobs then
      begin
        ADrawObject.ABorder.Pen.Width := 3; // Somewhat thicker
        if ADrawObject.ABorder.Pen.Color = clGreen then
          ADrawObject.ABorder.Pen.Color := clRed
        else
          ADrawObject.ABorder.Pen.Color := clGreen;
      end
      else // 20013516 Blink if a puppet has jobcode 'down'.
        if ADrawObject.ABlinkIsDown then
        begin
          // Show Light Blue
          if ADrawObject.ADownJobCode = MECHANICAL_DOWN_JOB then
          begin
            ADrawObject.ABorder.Pen.Width := 3; // Somewhat thicker
            if ADrawObject.ABorder.Pen.Color = clGreen then
              ADrawObject.ABorder.Pen.Color := clLightBlue // clBlue
            else
              ADrawObject.ABorder.Pen.Color := clGreen;
          end
          else
          begin
            // Show Dark Blue
            if ADrawObject.ADownJobCode = NO_MERCHANDIZE_JOB then
            begin
              ADrawObject.ABorder.Pen.Width := 3; // Somewhat thicker
              if ADrawObject.ABorder.Pen.Color = clGreen then
                ADrawObject.ABorder.Pen.Color := clNavy // clDarkBlue
              else
                ADrawObject.ABorder.Pen.Color := clGreen;
            end;
          end;
        end
        else
        begin
          ADrawObject.ABorder.Pen.Width := 1;
          ADrawObject.ABorder.Pen.Color := clGreen;
        end;
      // 20013551 Blink the 'min percentage label'. If it is visible then blink.
      if ADrawObject.AMinPercBlink then
      begin
        if Assigned(ADrawObject.AMinPercLabel) then
          if ADrawObject.AMinPercLabel.Visible then
          begin
            if ADrawObject.AMinPercLabel.Font.Color = clMyRed then
              ADrawObject.AMinPercLabel.Font.Color := clWhite
            else
              ADrawObject.AMinPercLabel.Font.Color := clMyRed;
          end;
      end;
    end; // for
  end; // if
end; // TimerBlinkTimer

procedure THomeF.ShowChartClick(Sender: TObject);
var
  MyGraphDrawObject: PDrawObject;
begin
  inherited;
Exit;

  if CurrentDrawObject = nil then
    Exit;
  MyGraphDrawObject := CurrentDrawObject;
  try
    if not tlbtnEdit.Down then
      MainTimerSwitch(False);

    ShowChartF.ADrawObject := MyGraphDrawObject; // TD-25881
    ShowChartF.Init := True; // TD-25881
    ShowChartF.ShowModal;
  finally
    if not tlbtnEdit.Down then
      MainTimerSwitch(True);
  end;
end;

procedure THomeF.ShowEmployees1Click(Sender: TObject);
begin
  inherited;
  if not tlbtnEdit.Down then
  begin
    if (Sender is TLabel) then
      CurrentDrawObject := ThisDrawObject((Sender as TLabel).Tag)
    else
      if (Sender is TShape) then
        CurrentDrawObject := ThisDrawObject((Sender as TShape).Tag);
    ShowEmployees(ShowEmployeesOnWorkspot);
  end;
end;

procedure THomeF.PlaySound;
begin
  if not UseSoundAlarm then
    Exit;
  if FileExists(SoundPath + SoundFilename) then
  begin
    with MediaPlayer1 do
    begin
      Filename := SoundPath + SoundFilename;
      DeviceType := dtWaveAudio;
      Open;
      Play;
    end;
  end;
end;

procedure THomeF.actFileSettingsExecute(Sender: TObject);
var
  OldWorkspotScale: Integer;
  OldFontScale: Integer;
  OldEfficiencyPeriodMode: TEffPeriodMode;
  OldEfficiencyPeriodSince: Boolean;
  OldEfficiencyPeriodSinceTime: TDateTime;
  OldGridLineDistance: Integer;
begin
  inherited;
  if not tlbtnEdit.Down then
    MainTimerSwitch(False);

//  DialogSettingsF := TDialogSettingsF.Create(Application);
  DialogSettingsF.SettingUseSoundAlarm := UseSoundAlarm;
  DialogSettingsF.SettingSoundFilename := SoundFilename;
  DialogSettingsF.MySoundPath := SoundPath;
  DialogSettingsF.SettingWorkspotScale := WorkspotScale;
  DialogSettingsF.SettingFontScale := FontScale;
  DialogSettingsF.SettingEfficiencyPeriodSince := EfficiencyPeriodSince;
  DialogSettingsF.SettingEfficiencyPeriodSinceTime := EfficiencyPeriodSinceTime;
  DialogSettingsF.SettingRefreshTimeInterval := RefreshTimeInterval;
  DialogSettingsF.EfficiencyPeriodMode := EfficiencyPeriodMode;
  DialogSettingsF.GridLineDistance := GridLineDistance; // 20013550
  OldWorkspotScale := WorkspotScale;
  OldFontScale := FontScale;
  OldEfficiencyPeriodMode := EfficiencyPeriodMode;
  OldEfficiencyPeriodSince :=  EfficiencyPeriodSince;
  OldEfficiencyPeriodSinceTime := EfficiencyPeriodSinceTime;
  OldGridLineDistance := GridLineDistance; // 20013550
  if DialogSettingsF.ShowModal = mrOK then
  begin
    UseSoundAlarm := DialogSettingsF.SettingUseSoundAlarm;
    SoundFilename := DialogSettingsF.SettingSoundFilename;
    WorkspotScale := DialogSettingsF.SettingWorkspotScale;
    FontScale := DialogSettingsF.SettingFontScale;
    EfficiencyPeriodMode := DialogSettingsF.EfficiencyPeriodMode;
    EfficiencyPeriodSince := DialogSettingsF.SettingEfficiencyPeriodSince;
    GridLineDistance := DialogSettingsF.GridLineDistance; // 20013550
    EfficiencyPeriodSinceTime :=
      DialogSettingsF.SettingEfficiencyPeriodSinceTime;
    if OldEfficiencyPeriodMode <> EfficiencyPeriodMode then
      Dirty := True;
    if OldEfficiencyPeriodSince <> EfficiencyPeriodSince then
      Dirty := True;
    if OldEfficiencyPeriodSinceTime <> EfficiencyPeriodSinceTime then
      Dirty := True;
    if OldGridLineDistance <> GridLineDistance then // 20013550
    begin
      Dirty := True;
      pnlDraw.Repaint;
    end;
    RefreshTimeInterval := DialogSettingsF.SettingRefreshTimeInterval;
    WriteRegistry;
    if WorkspotScale <> OldWorkspotScale then
    begin
      SaveList(WorkFilename);
      OpenList(WorkFilename);
    end;
    if FontScale <> OldFontScale then
    begin
      SaveList(WorkFilename);
      OpenList(WorkFilename);
    end;
  end;
//  DialogSettingsF.Free;

  if not tlbtnEdit.Down then
  begin
    MainActionForTimer; // 20015402 Refresh after file-settings
//    MainTimerSwitch(True); // 20015402 not needed here
  end;
end;

procedure THomeF.FormActivate(Sender: TObject);
begin
  inherited;
  if InitForm then
  begin
    TimerMain.Interval := RefreshTimeInterval * 1000;
    // GLOB3-231
    try
      try
        OpenList(WorkFilename);
      except
        on E:Exception do
          WLog(SPimsSchemeNotFound + ' ' +
            ExtractFilePath(SchemePath) + WorkFilename);
      end;
    finally
      // GLOB-231
      ProductionScreenWaitF.Hide;

      MainTimerSwitch(True);

      SetCaption;
      InitForm := False;

      HomeF.SetFocus;
    end;
  end;
end;

procedure THomeF.MainTimerSwitch(OnOff: Boolean);
begin
  TimerMain.Enabled := OnOff;
  if TimerMain.Enabled then
    TimerOnTime := SysUtils.Now;
end;

function ZeroFill(Str: String; Len: Integer): String;
begin
  while Length(Str) < Len do
    Str := '0' + Str;
  Result := Str;
end;

procedure THomeF.TimerStatusTimer(Sender: TObject);
const
  SecsADay = 86400;
var
  DurationInSec, (* Hour, *) Min, Sec: Cardinal;
  TimeStr: String;
begin
  inherited;
  if TimerMain.Enabled then
  begin
    DurationInSec := Trunc((SysUtils.Now - TimerOnTime) * SecsADay);
//    Hour := DurationInSec DIV 3600;
    Min := DurationInSec MOD 3600 DIV 60;
    Sec := DurationInSec MOD 3600 MOD 60 MOD 60;
    TimeStr :=
//      ZeroFill(IntToStr(Hour), 2) + ':' +
      ZeroFill(IntToStr(Min), 2) + ':' +
      ZeroFill(IntToStr(Sec), 2);
    stBarBase.Panels[4].Text := 'Timer=On' +
      ' (' + TimeStr + ')' +
      ' Interval=' + IntToStr(RefreshTimeInterval)
  end
  else
    stBarBase.Panels[4].Text := 'Timer=Off' +
      ' Interval=' + IntToStr(RefreshTimeInterval);
  stBarbase.Panels[5].Text := DateTimeToStr(Now);
  if DialogShowAllEmployeesF <> nil then
    DialogShowAllEmployeesF.stbarBase.Panels[2].Text := stBarbase.Panels[5].Text;
end;

procedure THomeF.ShowEmployeestoolate1Click(Sender: TObject);
begin
  inherited;
  ShowEmployees(ShowEmployeesTooLate);
end;

// Look if puppets (employees) of given workspot
// are scanned in and have the same jobcode as
// productionquantity-records.
procedure THomeF.ActionBlinkPuppets;
var
  PupI, PupJ: Integer;
  APuppetShift: PTPuppetShift;
  APuppet: PTPuppet;
  JOBFound: Boolean;
  PQFound: Boolean;
begin
  if CurrentDrawObject = nil then
    Exit;
  PQFound := False;
  JOBFound := False;
  with ProductionScreenDM.odsProductionQuantityBlinkPuppets do
  begin
//    Close;
    ClearVariables;
    SetVariable('PLANT_CODE',    CurrentDrawObject.APlantCode);
    SetVariable('WORKSPOT_CODE', CurrentDrawObject.AWorkspotCode);
    SetVariable('JOB_CODE',      NOJOB); // SO-20013516
    // Calculate (interval * 2) before. For example 10 minutes before 'now'.
    SetVariable('DATEFROM', Now -
      (PIMSDatacolTimeInterval * PIMSMultiplier / 60 / 24 / 60));
    SetVariable('DATETO',    MyDateTo(Now));
    Open;
//    Refresh; // 20016449 Why do a refresh?
    // Is there Production?
    if RecordCount > 0 then
    begin
      PQFound := True;
      First;
      while (not JOBFound) and (not Eof) do
      begin
        // Are there puppets scanned in on this Job?
        for PupI := 0 to CurrentDrawObject.APuppetShiftList.Count - 1 do
        begin
          APuppetShift := CurrentDrawObject.APuppetShiftList.Items[PupI];
          for PupJ := 0 to APuppetShift.APuppetList.Count - 1 do
          begin
            APuppet := APuppetShift.APuppetList.Items[PupJ];
            if APuppet.AScanned and (APuppet.AJobCode =
              FieldByName('JOB_CODE').AsString) then
              JOBFound := True;
          end;
        end;
        Next;
      end;
    end;
    Close;
  end;
  CurrentDrawObject.ABlinkPuppets := PQFound and (not JOBFound);
end;

function EmpCompare(Item1, Item2: Pointer): Integer;
var
  Employee1, Employee2: PTEmployee;
begin
  Result := 0;
  Employee1 := Item1;
  Employee2 := Item2;
  if (Employee1.AEmployeeNumber < Employee2.AEmployeeNumber) then
    Result := -1
  else
    if (Employee1.AEmployeeNumber > Employee2.AEmployeeNumber) then
      Result := 1;
end;

function THomeF.DetermineEmployeeList(
  ADrawObject: PDrawObject;
  const AShowMode: TShowMode;
  const APlantCode: String;
  const AWorkspotCode: String;
  var AEmployeeList: TList;
  var AEmployeeCount: Integer): Boolean;
var
  MyNow: TDateTime;
  I, J: Integer;
  APTEmployee: PTEmployee;
  AEmployee: TEmployee;
  APuppetShift: PTPuppetShift;
  APuppet: PTPuppet;
  procedure FreeEmpList;
  var
    I: Integer;
    APTEmployee: PTEmployee;
  begin
    for I := AEmployeeList.Count - 1  downto 0 do
    begin
      APTEmployee := AEmployeeList.Items[I];
      AEmployeeList.Remove(APTEmployee);
    end;
    AEmployeeList.Clear;
    AEmployeeList.Free;
  end;
  procedure AddEmp(var AEmployeeList: TList; APTEmployee: PTEmployee);
  var
    APTNewEmployee: PTEmployee;
  begin
    new(APTNewEmployee);
    APTNewEmployee.AEmployeeNumber := APTEmployee.AEmployeeNumber;
    APTNewEmployee.AEmployeeShortName := APTEmployee.AEmployeeShortName;
    APTNewEmployee.AEmployeeName := APTEmployee.AEmployeeName;
    APTNewEmployee.ATeamCode := APTEmployee.ATeamCode;
    APTNewEmployee.AShiftNumber := APTEmployee.AShiftNumber;
    APTNewEmployee.AEmpLevel := APTEmployee.AEmpLevel;
    APTNewEmployee.AStandAvailStartDate := 0;
    APTNewEmployee.AStandAvailEndDate := 0;
    APTNewEmployee.AEmpAvailStartDate := 0;
    APTNewEmployee.AEmpAvailEndDate := 0;
    APTNewEmployee.AAbsenceReasonCode := '';
    APTNewEmployee.AAbsenceReasonDescription := '';
    APTNewEmployee.AFirstAidYN := APTEmployee.AFirstAidYN;
    APTNewEmployee.AFirstAidExpDate := APTEmployee.AFirstAidExpDate;
    APTNewEmployee.APlanDepartmentCode := APTEmployee.APlanDepartmentCode; // PIM-275
    APTNewEmployee.APlanWorkspotCode := APTEmployee.APlanWorkspotCode;
    APTNewEmployee.AScanWorkspotCode := APTEmployee.AScanWorkspotCode;
    APTNewEmployee.AScanDateTimeIn := APTEmployee.AScanDatetimeIn;
    APTNewEmployee.APlanStartDate := APTEmployee.APlanStartDate;
    APTNewEmployee.APlanEndDate := APTEmployee.APlanEndDate;
    APTNewEmployee.AFirstPlanStartDate := APTEmployee.AFirstPlanStartDate; // PIM-237
    APTNewEmployee.AFirstScan := APTEmployee.AFirstScan; // PIM-237
    APTNewEmployee.APlanLevel := APTEmployee.APlanLevel;
    APTNewEmployee.AStandAvailStartDate := APTEmployee.AStandAvailStartDate;
    APTNewEmployee.AStandAvailEndDate := APTEmployee.AStandAvailEndDate;
    APTNewEmployee.AEmpAvailStartDate := APTEmployee.AEmpAvailStartDate;
    APTNewEmployee.AEmpAvailEndDate := APTEmployee.AEmpAvailEndDate;
    APTNewEmployee.AAbsenceReasonCode := APTEmployee.AAbsenceReasonCode;
    APTNewEmployee.AAbsenceReasonDescription :=
      APTEmployee.AAbsenceReasonDescription;
    APTNewEmployee.AColor := APTEmployee.AColor;
    APTNewEmployee.ACurrEff := APTEmployee.ACurrEff; // 20014550.50
    APTNewEmployee.AShiftEff := APTEmployee.AShiftEff; // 20014550.50
    APTNewEmployee.AKeep := True;
    AEmployeeList.Add(APTNewEmployee);
  end;
begin
  MyNow := Now;

  if AEmployeeList <> nil then
    FreeEmpList;
  AEmployeeList := TList.Create;
  AEmployeeCount := 0;

  // Employees Scanned and Planned on given Workspot
  // Take the list from the PUPPET-list if this is available.
  // Otherwise it will be determined again.
  if (AShowMode = ShowEmployeesOnWorkspot) then
  begin
    // Show all puppets of all shifts
    if ADrawObject.APuppetShiftList.Count > 0 then
    begin
      for I := 0 to ADrawObject.APuppetShiftList.Count - 1 do
      begin
        APuppetShift := ADrawObject.APuppetShiftList.Items[I];
        // One row shows all puppets of one Shift!
        if APuppetShift.APuppetList.Count > 0 then
        begin
          for J := 0 to APuppetShift.APuppetList.Count - 1 do
          begin
            APuppet := APuppetShift.APuppetList.Items[J];
            AEmployee.AEmployeeNumber := APuppet.AEmployeeNumber;
            AEmployee.AEmployeeName := APuppet.AEmployeeDescription;
            AEmployee.AEmployeeShortName := APuppet.AEmployeeShortName;
            AEmployee.ATeamCode := APuppet.AEmployeeTeamCode;
            AEmployee.AShiftNumber := 0;
            AEmployee.AEmpLevel := APuppet.AEmpLevel;
            AEmployee.AScanWorkspotCode := APuppet.AScannedWS;
            AEmployee.AScanDatetimeIn := APuppet.AScannedDateTimeIn;
            AEmployee.APlanWorkspotCode := APuppet.APlannedWS;
            AEmployee.APlanDepartmentCode := APuppet.APlannedDept; // PIM-275
            AEmployee.APlanStartDate := APuppet.APlannedStartTime;
            AEmployee.APlanEndDate := APuppet.APlannedEndTime;
            AEmployee.AFirstPlanStartDate := APuppet.AFirstPlanStartDate; // PIM-237
            AEmployee.AFirstScan := APuppet.AFirstScan; // PIM-237
            AEmployee.APlanLevel := APuppet.AEPlanLevel;
            AEmployee.AColor := APuppet.AColor;
            AEmployee.ACurrEff := APuppet.ACurrEff; // 20014550.50
            AEmployee.AShiftEff := APuppet.AShiftEff; // 20014550.50
            AddEmp(AEmployeeList, @AEmployee);
            inc(AEmployeeCount);
          end;
        end;
      end;
      Result := (AEmployeeCount > 0);
      Exit; // The puppetlist was available, leave now.
    end;
  end;

  // Create lists for planned/scanned employees
  PlanScanEmpDM.CurrentNow := MyNow;
  if not APlanScanClass.Init then // TD-22082
  begin
    APlanScanClass.OpenEmployee;
    APlanScanClass.OpenEmployeePlanning(MyNow);
    APlanScanClass.OpenTimeRegScanning(MyNow);
//  APlanScanClass.CreateWorkspotsPerEmployeeList(MyNow); // TD-22082
    APlanScanClass.OpenWorkspotsPerEmployee; // TD-22082
  end;

  if (AShowMode = ShowEmployeesNotScannedIn) or
    (AShowMode = ShowEmployeesAbsentWithReason) or
    (AShowMode = ShowEmployeesAbsentWithoutReason) then
  begin
    APlanScanClass.OpenStandAvail(MyNow);
  end;
  if (AShowMode = ShowEmployeesAbsentWithReason) or
    (AShowMode = ShowEmployeesAbsentWithoutReason) then
  begin
    APlanScanClass.OpenEmployeeAvail(MyNow);
  end;

  case AShowMode of
    ShowEmployeesOnWorkspot: // Employees scanned/planned on workspot
      begin
        if APlanScanClass.DetermineEmployeeWSListPLAN_SCAN_EMP(
          APlantCode, AWorkspotCode) then
        begin
          for I := 0 to APlanScanClass.EmployeeList.Count - 1 do
          begin
            APTEmployee := APlanScanClass.EmployeeList.Items[I];
            AddEmp(AEmployeeList, APTEmployee);
            inc(AEmployeeCount);
          end;
        end;
      end;
    ShowAllEmployees: // All: Scanned and Planned and Rest of employees
      begin
        if APlanScanClass.DetermineEmployeeWSListALL_EMP('', '') then
        begin
          for I := 0 to APlanScanClass.EmployeeList.Count - 1 do
          begin
            APTEmployee := APlanScanClass.EmployeeList.Items[I];
            AddEmp(AEmployeeList, APTEmployee);
            inc(AEmployeeCount);
          end;
        end;
      end;
    ShowEmployeesOnWrongWorkspot: // Scanned but on wrong Planned Workspot
      begin
        if APlanScanClass.DetermineEmployeeWSListPLAN_SCAN_EMP('', '') then
        begin
          for I := 0 to APlanScanClass.EmployeeList.Count - 1 do
          begin
            APTEmployee := APlanScanClass.EmployeeList.Items[I];
            if (APTEmployee.APlanWorkspotCode <> '') and
              (APTEmployee.AScanWorkspotCode <> '') then
              if
              (
                (  // planned on workspot
                  (APTEmployee.APlanWorkspotCode <> '0') and
                  (APTEmployee.APlanWorkspotCode <>
                    APTEmployee.AScanWorkspotCode)
                )
                or
                (  // planned on department
                  (APTEmployee.APlanWorkspotCode = '0') and
                  (APTEmployee.APlanDepartmentCode <>
                    APlanScanClass.DetermineWorkspotDepartment(
                      APTEmployee.APlantCode, APTEmployee.AScanWorkspotCode))
                )
              ) then
              begin
                AddEmp(AEmployeeList, APTEmployee);
                inc(AEmployeeCount);
              end;
          end;
        end;
      end;
    ShowEmployeesAbsentWithReason: // Absent with a reason
       // Employees not scanned, planned, not available,
       // but absent with reason
      begin
        if APlanScanClass.DetermineEmployeeWSListEMP_AVAIL_EMP_REASON(
          '', '') then
        begin
          for I := 0 to APlanScanClass.EmployeeList.Count - 1 do
          begin
            APTEmployee := APlanScanClass.EmployeeList.Items[I];
            AddEmp(AEmployeeList, APTEmployee);
            inc(AEmployeeCount);
          end;
        end;
      end;
    ShowEmployeesAbsentWithoutReason: // Absent without a reason
       // Employees not scanned, but planned
      begin
        if APlanScanClass.DetermineEmployeeWSListEMP_AVAIL_EMP_NO_REASON(
          '', '') then
        begin
          for I := 0 to APlanScanClass.EmployeeList.Count - 1 do
          begin
            APTEmployee := APlanScanClass.EmployeeList.Items[I];
            AddEmp(AEmployeeList, APTEmployee);
            inc(AEmployeeCount);
          end;
        end;
      end;
    ShowEmployeesNotScannedIn: // Not Scanned or Planned, but available
      begin
        if APlanScanClass.DetermineEmployeeWSListSTAND_AVAIL_EMP('', '') then
        begin
          for I := 0 to APlanScanClass.EmployeeList.Count - 1 do
          begin
            APTEmployee := APlanScanClass.EmployeeList.Items[I];
            AddEmp(AEmployeeList, APTEmployee);
            inc(AEmployeeCount);
          end;
        end;
      end;
    ShowEmployeesWithFirstAid: // Employee with first aid
      begin
        if APlanScanClass.DetermineEmployeeWSListALL_EMP('', '') then
        begin
          for I := 0 to APlanScanClass.EmployeeList.Count - 1 do
          begin
            APTEmployee := APlanScanClass.EmployeeList.Items[I];
            if APTEmployee.AFirstAidYN = 'Y' then
            begin
              AddEmp(AEmployeeList, APTEmployee);
              inc(AEmployeeCount);
            end;
          end;
        end;
      end;
    ShowEmployeesTooLate: // Employee too late
      // Show planned + scanned
      begin
        // Compare only FIRST Scan with FIRST Plan-timeblock
        if APlanScanClass.DetermineEmployeeWSListPLAN_SCAN_EMP('', '') then
        begin
          for I := 0 to APlanScanClass.EmployeeList.Count - 1 do
          begin
            APTEmployee := APlanScanClass.EmployeeList.Items[I];
{$IFDEF DEBUG3}
  WLog('TooLate: ' +
    'Emp=' + IntToStr(APTEmployee.AEmployeeNumber) +
    ' PlanWS=' + APTEmployee.APlanWorkspotCode +
    ' ScanWS=' + APTEmployee.AScanWorkspotCode +
    ' SDate=' + DateTimeToStr(APTEmployee.AScanDatetimeIn) +
    ' FirstPlan=' + DateTimeToStr(APTEmployee.AFirstPlanStartDate) +
    ' FirstScan=' + DateTimeToStr(APTEmployee.AFirstScan)
    );
{$ENDIF}
            if (APTEmployee.APlanWorkspotCode <> '') and
              (APTEmployee.AScanWorkspotCode <> '') then
              // PIM-237 Compare with FIRST SCAN!
              if (APTEmployee.AFirstScan >
                APTEmployee.AFirstPlanStartDate) then
              begin
                AddEmp(AEmployeeList, APTEmployee);
                inc(AEmployeeCount);
              end;
          end;
        end;
      end;
  end; // case
  if AEmployeeCount > 0 then
    AEmployeeList.Sort(EmpCompare);
  Result := (AEmployeeCount > 0);
end; // DetermineEmployeeList

procedure THomeF.ActionPuppetBox(ADrawObject: PDrawObject);
var
  AEmpList: TList;
  AEmpCount: Integer;
begin
  // PIM-237 Always re-create the puppets
  if ADrawObject.AType = otPuppetBox then
  begin
    AEmpList := nil;
    AEmpCount := 0;
    DetermineEmployeeList(ADrawObject, ADrawObject.AShowMode, '', '',
      AEmpList, AEmpCount);
    ActionCreatePuppets(ADrawObject, AEmpList);
    ShowPuppets(ADrawObject, ADrawObject.AObject);
    ADrawObject.AEmployeeCount := AEmpList.Count;
  end;
end;

// MR:01-03-2004
procedure THomeF.ActionCompareJobsInit(MyNow: TDateTime);
var
  DateFrom, DateTo: TDateTime;
begin
  // 20013379 ONLY LOOK FOR CURRENT!
  // MR:10-03-2004
{  DetermineSinceDates(MyNow, DateFrom, DateTo); }
  DateFrom := IncMinute(Now, -1 * Trunc(PIMSDatacolTimeInterval / 60));
  DateTo := Now;
  ProductionScreenDM.CompareJobInit(DateFrom, DateTo);
end;

// MR:01-03-2004
procedure THomeF.ActionCompareJobs(ADrawObject: PDrawObject; MyNow: TDateTime);
var
  DateFrom, DateTo: TDateTime;
  MaxDeviation: Double;
  ComparisonReject: Boolean;
  JobCode: String;
  JobPieces, SumJobPieces: Double;
  EmpsScanIn: Boolean;
  // 20013549
  procedure ShowMaxDeviationLabel(AVisible: Boolean;
    ATop, ALeft, AWidth, AHeight: Integer);
  begin
    if Assigned(ADrawObject.AMaxDeviationLabel) then
    begin
      ADrawObject.AMaxDeviationLabel.Visible := AVisible;
      // 20013549.10 Resize with FontScale
      ADrawObject.AMaxDeviationLabel.Font.Size :=
        Trunc(pnlDraw.Font.Size * 1.9 * FontScale / 100); // 1.5
//        Trunc(pnlDraw.Font.Size * 3.8 * WorkspotScale / 100); // 2.8
      // 20013549.10 Subtract label.height
      ADrawObject.AMaxDeviationLabel.Top := (ADrawObject.AObject as TImage).Top +
        Trunc(ATop * WorkspotScale / 100) - ADrawObject.AMaxDeviationLabel.Height;
      ADrawObject.AMaxDeviationLabel.Left := (ADrawObject.AObject as TImage).Left +
        Trunc(ALeft * WorkspotScale / 100);
    end;
  end; // ShowMinPercLabel
begin
  ADrawObject.ADeviation := 0; // 20013549
  ShowMaxDeviationLabel(False, -40, -15, 0, 0); // 20013549 // -100, -15
  // MR:10-03-2004
  // 20013379 ONLY LOOK FOR CURRENT! LAST 5 MINUTES!
{   DetermineSinceDates(MyNow, DateFrom, DateTo); }
  DateFrom := IncMinute(Now, -1 * Trunc(PIMSDatacolTimeInterval / 60));
{  DateFrom := MyNow -
    (PIMSDatacolTimeInterval * 1 / 60 / 24 / 60); }
  DateTo := Now;
//ShowMessage(DateTimeToStr(DateFrom) + ' - ' + DateTimeToStr(DateTo));
  ADrawObject.ABlinkCompareJobs := False;
  if ProductionScreenDM.CompareJob(ADrawObject.APlantCode,
    ADrawObject.AWorkspotCode, DateFrom, DateTo,
    MaxDeviation, ComparisonReject, JobCode, JobPieces, SumJobPieces,
    EmpsScanIn) then
  begin
    if ((SumJobPieces > 0) and (MaxDeviation > 0))
      and (JobCode <> '') then // TD-24100 Be sure JobCode is not empty
    begin
      ADrawObject.ACompareJob := True; // 20013379
      ADrawObject.ACompareJobCode := JobCode; // 20013379
      ADrawObject.AEmpsScanIn := EmpsScanIn; // 20013379
        // MR:21-04-2004 No use of percentage
//        if ((SumJobPieces - JobPieces) / SumJobPieces * 100 < MaxDeviation) then
        // MRA:26-JUN-2012 Calculate with percentages
//      if (Abs(SumJobPieces - JobPieces) > MaxDeviation) then
      ADrawObject.ADeviation :=
        Abs(Round((SumJobPieces - JobPieces) / SumJobPieces * 100));
{      if (Abs(Round(
             ((SumJobPieces - JobPieces) / SumJobPieces * 100))
             ) > MaxDeviation }
      if (ADrawObject.ADeviation > MaxDeviation) then
      begin
        // 20013549
        ADrawObject.AMaxDeviationLabel.Caption :=
          IntToStr(ADrawObject.ADeviation) + ' %';
        ShowMaxDeviationLabel(True, -40, -15, 0, 0); // -100, -15 // 20013549.10
        ADrawObject.ABlinkCompareJobs := True;
      end
      else // 20013549
        ShowMaxDeviationLabel(False, -40, -15, 0, 0); // -100, -15 // 20013549.10
    end;
  end;
end;

procedure THomeF.HelpLegendaActExecute(Sender: TObject);
begin
  inherited;
  try
    if not tlbtnEdit.Down then
      MainTimerSwitch(False);
//    DialogLegendaF := TDialogLegendaF.Create(nil);
    DialogLegendaF.ShowModal;
  finally
//    DialogLegendaF.Free;
    if not tlbtnEdit.Down then
    begin
      MainActionForTimer; // 20015402 Refresh after show legenda
      // MainTimerSwitch(True); // 20015402 not needed here
    end;
  end;
end;

// Action for Workspot-object
procedure THomeF.actWorkspotExecute(Sender: TObject);
begin
  inherited;
  if DialogWorkspotSelectF.RootPath = '' then
    DialogWorkspotSelectF.RootPath := ImagePath;
  DialogWorkspotSelectF.ActionWorkspot := False; // PIM-213
  DialogWorkspotSelectF.NoPicture := False; // PIM-213
  if DialogWorkspotSelectF.ShowModal = mrOK then
    AddObjectToList(otImage, nil,
      DialogWorkspotSelectF.PlantCode,
      DialogWorkspotSelectF.WorkspotCode,
      DialogWorkspotSelectF.WorkspotDescription,
      DialogWorkspotSelectF.ImageName,
      False, 0,
      DialogWorkspotSelectF.ProdScreenType);
end;

// Action for Efficiencymeter-object
procedure THomeF.actEffiMeterExecute(Sender: TObject);
begin
  inherited;
  if DialogWorkspotSelectF.RootPath = '' then
    DialogWorkspotSelectF.RootPath := ImagePath;
  DialogWorkspotSelectF.ActionWorkspot := False;
  DialogWorkspotSelectF.NoPicture := True; // PIM-213
  if DialogWorkspotSelectF.ShowModal = mrOK then
  begin
    // If ProdScreenType = pstMachineTimeRec then
    // add all workspots linked to the selected machine.
    // Add object for Workspot
    AddObjectToList(otImage, nil,
      DialogWorkspotSelectF.PlantCode,
      DialogWorkspotSelectF.WorkspotCode,
      DialogWorkspotSelectF.WorkspotDescription,
      DialogWorkspotSelectF.ImageName,
      True, 0,
      DialogWorkspotSelectF.ProdScreenType);
  end;
end;

// Action for Horizontalline-object
procedure THomeF.actLineHorzExecute(Sender: TObject);
begin
  inherited;
  AddObjectToList(otLineHorz, CreateShapeEx(stLineHorz),
    '', '', '', 'Line Horz', False, 0,
    pstNoTimeRec);
end;

// Action for Verticalline-object
procedure THomeF.actLineVertExecute(Sender: TObject);
begin
  inherited;
  AddObjectToList(otLineVert, CreateShapeEx(stLineVert),
    '', '', '', 'Line Vert', False, 0,
    pstNoTimeRec);
end;

// Action for Rectangle-object
procedure THomeF.actRectangleExecute(Sender: TObject);
var
  MyPlantCode: String;
  MyDepartmentCode: String;
  MyDepartmentDescription: String;
begin
  inherited;
  MyPlantCode := '';
  MyDepartmentCode := '';
  MyDepartmentDescription := 'Department';
  if DialogDepartmentSelectF.ShowModal = mrOK then
  begin
    MyPlantCode := DialogDepartmentSelectF.PlantCode;
    MyDepartmentCode := DialogDepartmentSelectF.DepartmentCode;
    MyDepartmentDescription := DialogDepartmentSelectF.DepartmentDescription;
    AddObjectToList(otRectangle, CreateShapeEx(TShapeExType(RECTANGLE)),
      MyPlantCode, MyDepartmentCode, MyDepartmentDescription, 'Rectangle',
      False, 0,
      pstNoTimeRec);
  end;
end;

// Action for Puppetbox-object
procedure THomeF.actPuppetBoxExecute(Sender: TObject);
begin
  inherited;
  // TD-24046 This is already created.
//  DialogSelectModeF := TDialogSelectModeF.Create(Application);
  if DialogSelectModeF.ShowModal = mrOK then
  begin
    AddObjectToList(otPuppetBox, CreateShapeEx(TShapeExType(RECTANGLE)),
      '', DialogSelectModeF.ModeString, '',
      'Puppet Box', False, DialogSelectModeF.SelectedMode,
      pstNoTimeRec);
  end;
  DialogSelectModeF.Hide; // TD-24046 Only hide it here, no free.
//  DialogSelectModeF.Free;
end;

procedure THomeF.actDeleteExecute(Sender: TObject);
begin
  inherited;
  if CurrentDrawObject <> nil then
  begin
    if DisplayMessage(tlbtnDelete.Hint + '?', mtConfirmation,
      [mbYes, mbNo]) = mrYes then
    begin
      if (CurrentDrawObject.AObject is TImage) then
      begin
        DeleteDrawObjectFromList((CurrentDrawObject.AObject as TImage).Tag);
        CurrentDrawObject := nil;
      end
      else
      begin
        if (CurrentDrawObject.AObject is TShapeEx) then
        begin
          DeleteDrawObjectFromList(
            (CurrentDrawObject.AObject as TShapeEx).Tag);
          CurrentDrawObject := nil;
        end;
      end;
    end; // if DisplayMessage
  end; // if CurrentDrawObject <> nil then
end; // actDeleteExecute

procedure THomeF.actSaveExecute(Sender: TObject);
begin
  inherited;
  SaveList(WorkFilename);
  SetCaption;
end;

procedure THomeF.actEditExecute(Sender: TObject);
begin
  inherited;
  if tlbtnEdit.Down then
  begin
    MainTimerSwitch(False);
    GhostImageDisable; // PIM-151
    // SO-20013563 Delete any head count list, to prevent
    // the contents is still shown during edit-mode.
    DeleteAnyHeadCountList;
    tlbtnWorkspot.Visible    := True;
    tlbtnEffMeter.Visible    := True;
    tlbtnWSEmpEff.Visible    := True;
    tlbtnLineHorz.Visible    := True;
    tlbtnLineVert.Visible    := True;
    tlbtnRectangle.Visible   := True;
    tlbtnPuppetBox.Visible   := True;
    tlbtnSave.Visible        := True;
    tlbtnDelete.Visible      := True;
    // Show some help!
    stBarBase.Panels[3].Text :=
      'Size: Ctrl / Alt + Left Mousebutton';
    WSEmpEffEditMode(True);
  end
  else
  begin
//    MainTimerSwitch(True); // 20015402 not needed here
    tlbtnWorkspot.Visible    := False;
    tlbtnEffMeter.Visible    := False;
    tlbtnWSEmpEff.Visible    := False;
    tlbtnLineHorz.Visible    := False;
    tlbtnLineVert.Visible    := False;
    tlbtnRectangle.Visible   := False;
    tlbtnPuppetBox.Visible   := False;
    tlbtnSave.Visible        := False;
    tlbtnDelete.Visible      := False;
    stBarBase.Panels[3].Text := '';
    WSEmpEffEditMode(False);
  end;
  pnlDraw.Repaint;
  Refresh;
  if not tlbtnEdit.Down then
  begin
    MainActionForTimer; // 20015402 Refresh after edit
  end;
end;

// Show a message at top-part of window.
procedure THomeF.ShowMsg(AMsg: String);
begin
  if AMsg = '' then
    AMsg := ' ' + ThisCaption;
  lblMessage.Caption := AMsg;
end;

// RV064.1.
procedure THomeF.actShowStatusInfoExecute(Sender: TObject);
begin
  inherited;
  try
    ProductionScreenDM.CheckInterference;
  except
  end;
end;

// SO-20013563
procedure THomeF.DeleteAnyHeadCountList;
var
  I: Integer;
  ADrawObject: PDrawObject;
begin
  if Assigned(List) then
    for I := 0 to List.Count - 1 do
    begin
      ADrawObject := List.Items[I];
      if Assigned(ADrawObject.AHeadCountList) then
        DeleteHeadCountList(ADrawObject);
    end;
end; // DeleteAnyHeadCountList

// SO-20013563
procedure THomeF.DeleteHeadCountList(ADrawObject: PDrawObject);
var
  AHeadCount: PTAHeadCount;
  I: Integer;
begin
  // Destroy an existing HeadCountList
  if Assigned(ADrawObject.AHeadCountList) then
  begin
    for I := ADrawObject.AHeadCountList.Count - 1 downto 0 do
    begin
      AHeadCount := ADrawObject.AHeadCountList.Items[I];
      AHeadCount.ALabelDepartment.Caption := '';
      AHeadCount.ALabelDepartment.Free;
      AHeadCount.ALabelNr.Caption := '';
      AHeadCount.ALabelNr.Free;
      Dispose(AHeadCount);
      ADrawObject.AHeadCountList.Remove(AHeadCount);
    end;
    ADrawObject.AHeadCountList.Free;
    ADrawObject.AHeadCountList := nil;
  end;
end; // DeleteHeadCountList

// SO-20013563
// Create a list of employees that are currently scanned in for
// the shown workspots in this scheme.
// Show this in a table with department + number-of-employees.
procedure THomeF.ActionShowHeadCount(AMyDrawObject: PDrawObject);
var
  AHeadCount: PTAHeadCount;
  ADeptList: TList;
  ADepartment: PTADepartment;
  I: Integer;
  TopAdd, Top, Left: Integer;
  FontSize: Integer;
  TotalNr: Integer;
  function IncludeDepartment(APlantCode: String; AWorkspotCode: String): Boolean;
  var
    I: Integer;
    ADrawObject: PDrawObject;
  begin
    // Result := False;

    // Rework: Do not filter on workspots: Include all workspots.
    Result := True;
    Exit; // Skip the rest

    for I := 0 to List.Count - 1 do
    begin
      ADrawObject := List.Items[I];
      if (ADrawObject.AObject is TImage) then
        if (ADrawObject.APlantCode = APlantCode) and
          (ADrawObject.AWorkspotCode = AWorkspotCode) then
        begin
          Result := True;
          Break;
        end;
    end;
  end; // IncludeDepartment
  procedure AddUpdateDepartment(ADepartmentDescription: String; ANr: Integer);
  var
    ANewDepartment: PTADepartment;
    ADepartment: PTADepartment;
    I: Integer;
    Found: Boolean;
  begin
    Found := False;
    ADepartment := nil;
    for I := 0 to ADeptList.Count - 1 do
    begin
      ADepartment := ADeptList.Items[I];
      if ADepartment.ADepartmentDescription = ADepartmentDescription then
      begin
        Found := True;
        Break;
      end;
    end;
    if not Found then // does not exist
    begin
      new(ANewDepartment);
      ANewDepartment.ADepartmentDescription := ADepartmentDescription;
      ANewDepartment.ANr := ANr;
      ADeptList.Add(ANewDepartment);
    end
    else
    begin
      if Assigned(ADepartment) then
        ADepartment.ANr := ADepartment.ANr + ANr;
    end;
  end; // AddUpdateDepartment
  procedure AddHeadCount(ADepartmentDescription: String; ANr: Integer);
  var
    NewHeight: Integer;
    NewWidth: Integer;
  begin
    new(AHeadCount);
    AHeadCount.ALabelDepartment := TLabel.Create(Application);
    AHeadCount.ALabelDepartment.Parent := pnlDraw;
    AHeadCount.ALabelDepartment.Caption := ADepartmentDescription;
    AHeadCount.ALabelDepartment.Font.Size := FontSize;
    AHeadCount.ALabelDepartment.Top := Top;

    // SO-20013563 - Rework
    // Set a new width based on font size
    NewWidth := AHeadCount.ALabelDepartment.
      Canvas.TextWidth('XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'); 
    AMyDrawObject.ADeptRect.ATopLineHorz.Width := NewWidth;
    AMyDrawObject.ADeptRect.ABottomLineHorz.Width := NewWidth;
    if Assigned(AMyDrawObject.ACross) then
    begin
      AMyDrawObject.ACross.ALineHorz.Width := NewWidth;
      AMyDrawObject.ACross.ALineVert.Left := Left + Trunc(NewWidth * 5/6) -
        AMyDrawObject.ADeptRect.ARightLineVert.Pen.Width;
    end;
    AMyDrawObject.ADeptRect.ARightLineVert.Left := Left + NewWidth -
      AMyDrawObject.ADeptRect.ARightLineVert.Pen.Width;

    AHeadCount.ALabelDepartment.Left := Left +
      Round(AMyDrawObject.ADeptRect.ATopLineHorz.Width * 1/100);
    AHeadCount.ALabelNr := TLabel.Create(Application);
    AHeadCount.ALabelNr.Parent := pnlDraw;
    AHeadCount.ALabelNr.Caption := IntToStr(ANr);
    AHeadCount.ALabelNr.Font.Size := FontSize;
    AHeadCount.ALabelNr.Top := Top;
    AHeadCount.ALabelNr.Left := Left  + 2 +
      Round(AMyDrawObject.ADeptRect.ATopLineHorz.Width * 5/6); // 2/3 to 5/6
    AMyDrawObject.AHeadCountList.Add(AHeadCount);
    if ADepartmentDescription = SPimsTotalHeadCount then
    begin
      // Adjust size of cross
      if Assigned(AMyDrawObject.ACross) then
      begin
        AMyDrawObject.ACross.ALineHorz.Top := Top -
          Round(AHeadCount.ALabelDepartment.Height / 2);
      end;
      // Adjust size of rectangle
      Top := Top + TopAdd; // next row (text line)
      AMyDrawObject.ADeptRect.ABottomLineHorz.Top := Top + 2;
      // Rework: Calculate new height based on difference between
      //         BottomLineHorz and TopLineHorz of DeptRec.
      NewHeight := AMyDrawObject.ADeptRect.ABottomLineHorz.Top -
        AMyDrawObject.ADeptRect.ATopLineHorz.Top +
        AMyDrawObject.ADeptRect.ARightLineVert.Pen.Width;
      AMyDrawObject.ADeptRect.ARightLineVert.Height := NewHeight;
      AMyDrawObject.ADeptRect.ALeftLineVert.Height := NewHeight;
      if Assigned(AMyDrawObject.ACross) then
        AMyDrawObject.ACross.ALineVert.Height := NewHeight;
    end;
  end; // AddHeadCount
begin
  // Destroy an existing HeadCountList
  DeleteHeadCountList(AMyDrawObject);
  AMyDrawObject.AHeadCountList := TList.Create;

  // Create a temporary department list.
  ADeptList := TList.Create;
  try
    with ProductionScreenDM do
    begin
      // Get all employees currently scanned in
      with oqHeadCount do
      begin
        // TD-22082 Filter on user to limit number of records.
        ClearVariables;
        SetVariable('USER_NAME', ORASystemDM.UserTeamLoginUser);
        SetVariable('DATETIME_IN', Now); // 20016449

        Execute;
        while not Eof do
        begin
          // Filter on workspots shown in production screen
          // Rework: Do not filter on workspots.
          if IncludeDepartment(FieldAsString('PLANT_CODE'),
            FieldAsString('WORKSPOT_CODE')) then
          begin
            AddUpdateDepartment(FieldAsString('DESCRIPTION'),
              FieldAsInteger('NR'));
          end;
          Next;
        end;
      end;
    end;
    // Build and show the HeadCountList.
    if ADeptList.Count > 0 then
    begin
//      FontSize := Round(12 * FontScale / 100 * (WorkspotScale / 100));
      FontSize := NewFontSize;
      TopAdd := Round(FontSize * 1.5); // Distance between text-lines
      Top := AMyDrawObject.ADeptRect.ATopLineHorz.Top + 2;
      Left := AMyDrawObject.ADeptRect.ATopLineHorz.Left + 2;
      TotalNr := 0;
      for I := 0 to ADeptList.Count - 1 do
      begin
        ADepartment := ADeptList.Items[I];
        AddHeadCount(ADepartment.ADepartmentDescription, ADepartment.ANr);
        TotalNr := TotalNr + ADepartment.ANr;
        Top := Top + TopAdd; // next row
      end;
      if ADeptList.Count > 0 then
      begin
        Top := Top + TopAdd; // add extra row before total
        AddHeadCount(SPimsTotalHeadCount, TotalNr);
      end;
    end;
  finally
    // Destroy temporary department list
    for I := ADeptList.Count - 1 downto 0 do
    begin
      ADepartment := ADeptList.Items[I];
      Dispose(ADepartment);
      ADeptList.Remove(ADepartment);
    end;
    ADeptList.Free;
  end;
end; // ActionShowHeadCount

// SO-20013563 - Rework
// Scale Font, but it cannot get too small!
// Calculate new FontSize based on WorkspotScale
function THomeF.NewFontSize: Integer;
begin
  case WorkspotScale of
  0..39:    Result := pnlDraw.Font.Size - 3;
  40..59:   Result := pnlDraw.Font.Size - 2;
  60..99:   Result := pnlDraw.Font.Size;
  100..199: Result := pnlDraw.Font.Size + 1;
  200..299: Result := pnlDraw.Font.Size + 2;
  else
    Result := pnlDraw.Font.Size + 3;
  end;
end;

// 20013551
procedure THomeF.DeleteShiftList(ADrawObject: PDrawObject);
var
  AShift: PTAShift;
  I: Integer;
begin
  // Destroy an existing HeadCountList
  if Assigned(ADrawObject.AShiftList) then
  begin
    for I := ADrawObject.AShiftList.Count - 1 downto 0 do
    begin
      AShift := ADrawObject.AShiftList.Items[I];
      Dispose(AShift);
      ADrawObject.AShiftList.Remove(AShift);
    end;
    ADrawObject.AShiftList.Free;
    ADrawObject.AShiftList := nil;
  end;
end; // DeleteShiftList

// 20013551
procedure THomeF.DetermineShiftList(ADrawObject: PDrawObject);
var
  AShift: PTAShift;
begin
  if not ADrawObject.AEffRunningHrs then
    Exit;

  with ProductionScreenDM do
  begin
    with oqWorkspotShift do
    begin
      if Assigned(ADrawObject.AShiftList) then
      begin
        DeleteShiftList(ADrawObject);
        ADrawObject.AShiftList := TList.Create;
      end;
      ClearVariables;
      SetVariable('PLANT_CODE',    ADrawObject.APlantCode);
      SetVariable('WORKSPOT_CODE', ADrawObject.AWorkspotCode);
      Execute;
      while not Eof do
      begin
        new(AShift);
        AShift.AShiftNumber := FieldAsInteger('SHIFT_NUMBER');
        AShift.AShiftDescription := FieldAsString('DESCRIPTION');
        AShift.AStartTime[1] := FieldAsDate('STARTTIME1');
        AShift.AEndTime[1]   := FieldAsDate('ENDTIME1');
        AShift.AStartTime[2] := FieldAsDate('STARTTIME2');
        AShift.AEndTime[2]   := FieldAsDate('ENDTIME2');
        AShift.AStartTime[3] := FieldAsDate('STARTTIME3');
        AShift.AEndTime[3]   := FieldAsDate('ENDTIME3');
        AShift.AStartTime[4] := FieldAsDate('STARTTIME4');
        AShift.AEndTime[4]   := FieldAsDate('ENDTIME4');
        AShift.AStartTime[5] := FieldAsDate('STARTTIME5');
        AShift.AEndTime[5]   := FieldAsDate('ENDTIME5');
        AShift.AStartTime[6] := FieldAsDate('STARTTIME6');
        AShift.AEndTime[6]   := FieldAsDate('ENDTIME6');
        AShift.AStartTime[7] := FieldAsDate('STARTTIME7');
        AShift.AEndTime[7]   := FieldAsDate('ENDTIME7');
        ADrawObject.AShiftList.Add(AShift);
        Next;
      end; // while
    end; // with
  end; // with
end; // DetermineShiftList

// 20013551
procedure THomeF.DetermineEffRunningHrs(ADrawObject: PDrawObject);
begin
  with ProductionScreenDM do
  begin
    ADrawObject.AEffRunningHrs := False;
    with oqWSEffRunHrs do
    begin
      ClearVariables;
      SetVariable('PLANT_CODE',    ADrawObject.APlantCode);
      SetVariable('WORKSPOT_CODE', ADrawObject.AWorkspotCode);
      Execute;
      if not Eof then
        ADrawObject.AEffRunningHrs :=
          FieldAsString('EFF_RUNNING_HRS_YN') = 'Y';
    end;
  end;
end; // DetermineEffRunningHrs

// 20013550
procedure THomeF.PanelPaint(Sender: TObject);
var
  x, y: Integer;
begin
  if not tlbtnEdit.Down then
    Exit;
  with pnlDraw do
    with Canvas do
    begin
      Pen.Color := clGray;
      y := GridLineDistance;
      while y < Height do
      begin
        MoveTo(0, y);
        LineTo(Width, y);
        y := y + GridLineDistance;
      end;
      x := GridLineDistance;
      while x < Width do
      begin
        MoveTo(x, 0);
        LineTo(x, Height);
        x := x + GridLineDistance;
      end;
    end;
end; // PanelPaint

procedure THomeF.FormDestroy(Sender: TObject);
begin
  inherited;
  // 20013550
  pnlDraw.Free;
end;

// 20013550
function THomeF.AlignToGridLeft(ALeft: Integer): Integer;
begin
  Result := (ALeft div GridLineDistance) * GridLineDistance;
end;

// 20013550
function THomeF.AlignToGridTop(ATop: Integer): Integer;
begin
  Result := (ATop div GridLineDistance) * GridLineDistance;
end;

// 20013548
procedure THomeF.RescaleAllObjects(ACurrentWidth, ACurrentHeight: Integer);
var
  FactorWidth, FactorHeight: Double;
  FactorScale: Double;
  NewWidth, NewHeight: Integer;
  I: Integer;
  ADrawObject: PDrawObject;
begin
  if (ACurrentWidth = 0) or (ACurrentHeight = 0) then
    Exit;
  NewWidth := Screen.Width;
  NewHeight := Screen.Height;
  if NewWidth < 100 then
    NewWidth := 100;
  if NewHeight < 100 then
    NewHeight := 100;
  FactorWidth := NewWidth / ACurrentWidth;
  FactorHeight := NewHeight / ACurrentHeight;
  FactorScale := (FactorHeight + FactorWidth) / 2;
  // 20013548.10. Do not compare with 1 because it is a double.
  if (FactorScale >= 0.9) and (FactorScale <= 1.1) then
    Exit;
  for I:=0 to List.Count-1 do
  begin
    ADrawObject := List.Items[I];
    if ADrawObject.AType = otImage then
    begin
      if (ADrawObject.AObject is TImage) then
      begin
        with (ADrawObject.AObject as TImage) do
        begin
          Top := Round(Top * FactorHeight);
          Left := Round(Left * FactorWidth);
          Height := Round(Height * FactorScale);
          Width := Round(Width * FactorScale);
        end;
      end;
    end
    else
    begin
      if (ADrawObject.AType = otLineHorz) or
        (ADrawObject.AType = otLineVert) or
        (ADrawObject.AType = otRectangle) or
        (ADrawObject.AType = otPuppetBox) then
      begin
        with (ADrawObject.AObject as TShapeEx) do
        begin
          Top := Round(Top * FactorHeight);
          Left := Round(Left * FactorWidth);
          Height := Round(Height * FactorScale);
          Width := Round(Width * FactorScale);
        end;
      end;
    end;
    ShowDrawObject(ADrawObject, ADrawObject.AObject);
  end; // for

  CurrentSchemeWidth := HomeF.Width;
  CurrentSchemeHeight := HomeF.Height;
end; // RescaleAllObjects

procedure THomeF.FormResize(Sender: TObject);
begin
  inherited;
//
end;

// 20014289
{
  To use this, you need a TStatusBar and at least one Panel.
  Then change the style of StatusBar1.Panels[0] to
  psOwnerDraw and add the code below to the OnDrawPanel handler.
}
procedure THomeF.stbarBaseDrawPanel(StatusBar: TStatusBar;
  Panel: TStatusPanel; const Rect: TRect);
var
  SomeText: String;
  I: Integer;
  MyLeft: Integer;
begin
  inherited;
  MyLeft := 0;
  for I := 0 to StatusBar.Panels.Count - 1 do
  begin
    if Panel = StatusBar.Panels[I] then
    begin
      SomeText := StatusBar.Panels[I].Text;
//      Panel.Width := Trunc(stbarBase.Canvas.TextWidth(SomeText) * 1.5);
      Panel.Width := StatusBar.Panels[I].Width;
      with StatusBar.Canvas do
      begin
        Brush.Color := clDarkRed; // clPimsBlue; // PIM-250
        FillRect(Rect);
         Font.Color := clWhite;
        TextRect(Rect, MyLeft + 5,
          {Rect.Left + 1, }Rect.Top, SomeText);
      end;
    end; // if
    MyLeft := MyLeft + StatusBar.Panels[I].Width; //Panel.Width;
  end; // for
end;

//23466
procedure THomeF.ClearAllSelectionRectangles;
var i : integer;
    ClearDrawObject: PDrawObject;
begin
  for i := 0 to List.Count - 1 do
    begin
      ClearDrawObject := List.Items[i];
      ClearDrawObject.Selected := false;
      ClearDrawObject.SelectionRectangle.Visible := false;
    end;
end;

function THomeF.isAltDown : Boolean;
var
  State: TKeyboardState;
begin { isAltDown }
  GetKeyboardState(State);
  Result := ((State[vk_Menu] and 128)<>0);
end; { isAltDown }

function THomeF.isCtrlDown : Boolean;
var
  State: TKeyboardState;
begin { isCtrlDown }
  GetKeyboardState(State);
  Result := ((State[vk_Control] and 128)<>0);
end; { isCtrlDown }

function THomeF.isShiftDown : Boolean;
var
  State: TKeyboardState;
begin { isShiftDown }
  GetKeyboardState(State);
  Result := ((State[vk_Shift] and 128)<>0);
end; { isShiftDown }

// 23466
procedure THomeF.TimerMultiSelectTimer(Sender: TObject);
begin
  inherited;
  If MultiSelectActive and not (isShiftDown {and isCtrlDown}) then // 23466.150 Only look for Shift
    ClearAllSelectionRectangles;
  MultiSelectActive := (isShiftDown {and isCtrlDown}); // 23466.150 Only look for Shift
end;

procedure THomeF.HelpOrderingInfoActExecute(Sender: TObject);
begin
  inherited;
  if not tlbtnEdit.Down then
    MainActionForTimer; // 20015402 Refresh after show info
end;

// 20014450.50
procedure THomeF.MenuItemEmpViewClick(Sender: TObject);
begin
  inherited;
  DialogEmpListViewF.ShowModal;
end;

// 20014450.50
procedure THomeF.MenuItemEmpGraphClick(Sender: TObject);
begin
  inherited;
  DialogEmpGraphF.ShowModal;
end;

// 20014450.50
procedure THomeF.PuppetMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
  point: TPoint;
begin
  if Sender = nil then Exit;

  if ORASystemDM.PSShowEmpInfo and ORASystemDM.PSShowEmpEff then
    if not tlbtnEdit.Down then
      if (Sender is TShape) then
        if (Button = mbRight) then
        begin
          getCursorPos(point);
          DialogEmpListViewF.EmployeeNumber := (Sender as TShape).MyTag;
          DialogEmpListViewF.EmployeeName := (Sender as TShape).Hint;
          DialogEmpGraphF.EmployeeNumber := (Sender as TShape).MyTag;
          DialogEmpGraphF.EmployeeName := (Sender as TShape).Hint;
          MenuItemEmployeeName.Caption := (Sender as TShape).Hint;
          MenuItemCurrentEff.Caption :=
            STransCurrEff + ' ' + IntToStr((Sender as TShape).ACurrEff) + ' %';
          MenuItemTodayEff.Caption :=
            STransTodaysEff + ' ' + IntToStr((Sender as TShape).AShiftEff) + ' %';
          PopupMenuRealTimeEff.Popup(point.X, point.Y);
        end;
end; // PuppetMouseDown

// 20014450.50
procedure THomeF.EmployeeNameLabelMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  point: TPoint;
  CurrEff, ShiftEff: Integer;
  PlantCode: String;
  ShiftNumber: Integer;
begin
  if Sender = nil then Exit;

  if ORASystemDM.PSShowEmpInfo and ORASystemDM.PSShowEmpEff then
    if not tlbtnEdit.Down then
      if (Sender is TLabel) then
        if (Button = mbRight) then
        begin
          getCursorPos(point);
          DialogEmpListViewF.EmployeeNumber := (Sender as TLabel).MyTag;
          DialogEmpListViewF.EmployeeName := (Sender as TLabel).Hint;
          DialogEmpGraphF.EmployeeNumber := (Sender as TLabel).MyTag;
          DialogEmpGraphF.EmployeeName := (Sender as TLabel).Hint;
          MenuItemEmployeeName.Caption := (Sender as TLabel).Hint;
          PlantCode := (Sender as TLabel).PlantCode;
          ShiftNumber := (Sender as TLabel).ShiftNumber;
          CurrEff := 0;
          ShiftEff := 0;
          with RealTimeEffDM.oqRTCurrEmpEff do
          begin
            ClearVariables;
            SetVariable('EMPLOYEE_NUMBER', DialogEmpListViewF.EmployeeNumber);
            Execute;
            if not Eof then
              CurrEff := Round(FieldAsFloat('EFF'));
          end;
          with RealTimeEffDM.oqRTShiftEmpEff do
          begin
            ClearVariables;
            SetVariable('EMPLOYEE_NUMBER', DialogEmpListViewF.EmployeeNumber);
            SetVariable('PLANT_CODE', PlantCode);
            SetVariable('SHIFT_NUMBER', ShiftNumber);
            Execute;
            if not Eof then
              ShiftEff := Round(FieldAsFloat('EFF'));
          end;
          MenuItemCurrentEff.Caption :=
            STransCurrEff + ' ' + IntToStr(CurrEff) + ' %';
          MenuItemTodayEff.Caption :=
            STransTodaysEff + ' ' + IntToStr(ShiftEff) + ' %';
          PopupMenuRealTimeEff.Popup(point.X, point.Y);
        end;
end; // EmployeeLabelMouseDown

// 20014450.50
function THomeF.EmployeeCreate(AEmployeeNumber: Integer;
  ADrawObject: PDrawObject): PTAEmployee;
var
  APTAEmployee: PTAEmployee;
begin
  new(APTAEmployee);
  APTAEmployee.AEmployeeNumber := AEmployeeNumber;
  APTAEmployee.AEmployeeNameLabel := TLabel.Create(Application);
  with APTAEmployee.AEmployeeNameLabel do
  begin
    Parent := pnlDraw;
    Tag := ADrawObject.ALabel.Tag;
    MyTag := AEmployeeNumber;
    PlantCode := ADrawObject.APlantCode;
    ShiftNumber := ADrawObject.AShiftNumber;
    Alignment := taLeftJustify;
    OnClick := ShowEmployees1Click;
    OnMouseDown := EmployeeNameLabelMouseDown;
    Visible := False; // PIM-213
  end;
  Result := APTAEmployee;
end; // EmployeeCreate

// 20014450.50
procedure THomeF.ListButtonClick(Sender: TObject);
begin
  if (Sender is TButton) then
  begin
    CurrentDrawObject := ThisDrawObject((Sender as TButton).Tag);
    CurrentDrawObject.AListEmployeeNumber := (Sender as TColorButton).MyTag;
    DialogEmpListViewF.ShowModal;
  end;
end;

// 20014450.50
procedure THomeF.GraphButtonClick(Sender: TObject);
begin
  if (Sender is TButton) then
  begin
    CurrentDrawObject := ThisDrawObject((Sender as TButton).Tag);
    CurrentDrawObject.AListEmployeeNumber := (Sender as TColorButton).MyTag;
    DialogEmpGraphF.ShowModal;
  end;
end;

// 20014450.50
function THomeF.ActionRealTimeEff(var ADrawObject: PDrawObject): Double;
var
  CurrScannedInFlag: String;
  ByWorkspot: Boolean;
  // Is employee currently scanned in?
  function FindCurrentEmployee(AEmployeeNumber: Integer): Boolean;
  begin
    Result := False;
    with RealTimeEffDM.odsRTCurrWSEffALL do
    begin
      Filtered := False;
      Filter := '(EMPLOYEE_NUMBER = ' + IntToStr(AEmployeeNumber) + ')';
      Filtered := True;
      if not Eof then
        Result := True;
    end;
  end; // FindCurrentEmployee
  procedure AddEmployee(AEmployeeNumber: Integer;
    ACurrScannedInFlag: String);
  var
    APTAEmployee: PTAEmployee;
  begin
    ADrawObject.ACurrentNumberOfEmployees :=
      ADrawObject.ACurrentNumberOfEmployees + 1;
    if ADrawObject.ABigEffMeters or
      (ADrawObject.AProdScreenType = pstMachine) or
      (ADrawObject.AProdScreenType = pstWSEmpeff) then // PIM-213
    begin
      // Add Employee to DrawObject for
      // BIG-EFF-METERS + WORKSPOT TIMEREC

      // 20014450.50
      APTAEmployee := EmployeeCreate(AEmployeeNumber, ADrawObject);
      // 20014450.50
      with APTAEmployee.AEmployeeNameLabel do
      begin
        if ADrawObject.ABigEffMeters or
          (ADrawObject.AProdScreenType = pstWSEmpeff) then // PIM-213
        begin
          // 20014450.50
          if PlanScanEmpDM.cdsEmployee.FindKey([AEmployeeNumber]) then
          begin
            Caption :=
              PlanScanEmpDM.cdsEmployee.
                FieldByName('DESCRIPTION').AsString;
                // + ' ' + ACurrScannedInFlag; // PIM-213
             Hint :=
              PlanScanEmpDM.cdsEmployee.
                FieldByName('DESCRIPTION').AsString;
             if ADrawObject.ABigEffMeters then // PIM-213
               Visible := ORASystemDM.PSShowEmpInfo // PIM-223
             else
               Visible := False;
          end
          else
            Caption := '???';
        end
        else
        begin
          Visible := False;
          Caption := '';
        end;
      end;
      ADrawObject.AEmployeeList.Add(APTAEmployee);
      ShowEmployeeList(ADrawObject);
    end; // if ADrawObject.ABigEffMeters then
  end; // AddEmployee
  procedure ActionAddEmployeesCurrent;
  begin
    with RealTimeEffDM.odsRTCurrWSEffALL do
    begin
      while not Eof do
      begin
        AddEmployee(FieldByName('EMPLOYEE_NUMBER').AsInteger, '*');
        ADrawObject.ATotalProdTime :=
          ADrawObject.ATotalProdTime + Round(FieldByName('SECONDSACTUAL').AsFloat);
        ADrawObject.ATotalNormTime :=
          ADrawObject.ATotalNormTime + Round(FieldByName('SECONDSNORM').AsFloat);
        ADrawObject.ATotalProdQuantity :=
          ADrawObject.ATotalProdQuantity + FieldByName('QTY').AsFloat;
        ADrawObject.ATotalNormQuantity :=
          ADrawObject.ATotalNormQuantity + FieldByName('NORMQTY').AsFloat;
        Next;
      end;
    end;
  end; // ActionAddEmployeesCurrent
  // PIM-151
  procedure ActionGhostCountCurrent;
  var
    LastTotalProdQuantity: Double;
  begin
    LastTotalProdQuantity := ADrawObject.ATotalProdQuantity;
    ADrawObject.AGhostCountBlink := False;
    with RealTimeEffDM.odsRTCurrGhostAll do
    begin
      Filtered := False;
      if ByWorkspot then
        Filter :=
          '(PLANT_CODE = ' + Quotes(ADrawObject.APlantCode) + ') AND ' +
          '(WORKSPOT_CODE = ' + Quotes(ADrawObject.AWorkspotCode) + ') AND ' +
          '((SHIFT_NUMBER = ' + IntToStr(ADrawObject.AShiftNumber) + ') OR ' +
          ' (SHIFT_NUMBER = 0))'
      else
        Filter :=
          '(PLANT_CODE = ' + Quotes(ADrawObject.APlantCode) + ') AND ' +
          '(MACHINE_CODE = ' + Quotes(ADrawObject.AWorkspotCode) + ') AND ' +
          '((SHIFT_NUMBER = ' + IntToStr(ADrawObject.AShiftNumber) + ') OR ' +
          ' (SHIFT_NUMBER = 0))';
      Filtered := True;
      while not Eof do
      begin
        if FieldByName('QTY').AsFloat > FieldByName('QTY2').AsFloat then
        begin
          ADrawObject.AGhostCountBlink := True;
          if LastTotalProdQuantity <= 0 then
          begin
            ADrawObject.ATotalProdTime :=
              ADrawObject.ATotalProdTime + Round(FieldByName('SECONDSACTUAL').AsFloat);
            ADrawObject.ATotalNormTime :=
              ADrawObject.ATotalNormTime + Round(FieldByName('SECONDSNORM').AsFloat);
            ADrawObject.ATotalProdQuantity :=
              ADrawObject.ATotalProdQuantity + FieldByName('QTY').AsFloat;
            ADrawObject.ATotalNormQuantity :=
              ADrawObject.ATotalNormQuantity + FieldByName('NORMQTY').AsFloat;
          end; // if
        end; // if
        Next;
      end; // while
    end; // with
  end; // ActionGhostCountCurrent
  procedure ActionAddEmployeesShift;
  begin
    with RealTimeEffDM.odsRTShiftWSEffALL do
    begin
      // Filter on what to find
      Filtered := False;
      if ByWorkspot then
        Filter :=
          '(PLANT_CODE = ' + Quotes(ADrawObject.APlantCode) + ') AND ' +
          '(WORKSPOT_CODE = ' + Quotes(ADrawObject.AWorkspotCode) + ') AND ' +
          '(SHIFT_NUMBER = ' + IntToStr(ADrawObject.AShiftNumber) + ')'
      else
        Filter :=
          '(PLANT_CODE = ' + Quotes(ADrawObject.APlantCode) + ') AND ' +
          '(MACHINE_CODE = ' + Quotes(ADrawObject.AWorkspotCode) + ') AND ' +
          '(SHIFT_NUMBER = ' + IntToStr(ADrawObject.AShiftNumber) + ')';
      Filtered := True;
      while not Eof do
      begin
        if FindCurrentEmployee(FieldByName('EMPLOYEE_NUMBER').AsInteger) then
          CurrScannedInFlag := '*'
        else
          CurrScannedInFlag := '';
        AddEmployee(FieldByName('EMPLOYEE_NUMBER').AsInteger, CurrScannedInFlag);
        ADrawObject.ATotalProdTime :=
          ADrawObject.ATotalProdTime + Round(FieldByName('SECONDSACTUAL').AsFloat);
        ADrawObject.ATotalNormTime :=
          ADrawObject.ATotalNormTime + Round(FieldByName('SECONDSNORM').AsFloat);
        ADrawObject.ATotalProdQuantity :=
          ADrawObject.ATotalProdQuantity + FieldByName('QTY').AsFloat;
        ADrawObject.ATotalNormQuantity :=
          ADrawObject.ATotalNormQuantity + FieldByName('NORMQTY').AsFloat;
        Next;
      end;
    end;
  end; // ActionAddEmployeesShift
  // PIM-151
  procedure ActionGhostCountShift;
  var
    LastTotalProdQuantity: Double;
  begin
    LastTotalProdQuantity := ADrawObject.ATotalProdQuantity;
    ADrawObject.AGhostCountBlink := False;
    with RealTimeEffDM.odsRTShiftGhostAll do
    begin
      Filtered := False;
      if ByWorkspot then
        Filter :=
          '(PLANT_CODE = ' + Quotes(ADrawObject.APlantCode) + ') AND ' +
          '(WORKSPOT_CODE = ' + Quotes(ADrawObject.AWorkspotCode) + ') AND ' +
          '((SHIFT_NUMBER = ' + IntToStr(ADrawObject.AShiftNumber) + ') OR ' +
          ' (SHIFT_NUMBER = 0))'
      else
        Filter :=
          '(PLANT_CODE = ' + Quotes(ADrawObject.APlantCode) + ') AND ' +
          '(MACHINE_CODE = ' + Quotes(ADrawObject.AWorkspotCode) + ') AND ' +
          '((SHIFT_NUMBER = ' + IntToStr(ADrawObject.AShiftNumber) + ') OR ' +
          ' (SHIFT_NUMBER = 0))';
      Filtered := True;
      while not Eof do
      begin
        if FieldByName('QTY').AsFloat > FieldByName('QTY2').AsFloat then
        begin
          ADrawObject.AGhostCountBlink := True;
          if LastTotalProdQuantity <= 0 then
          begin
            ADrawObject.ATotalProdTime :=
              ADrawObject.ATotalProdTime + Round(FieldByName('SECONDSACTUAL').AsFloat);
            ADrawObject.ATotalNormTime :=
              ADrawObject.ATotalNormTime + Round(FieldByName('SECONDSNORM').AsFloat);
            ADrawObject.ATotalProdQuantity :=
              ADrawObject.ATotalProdQuantity + FieldByName('QTY').AsFloat;
            ADrawObject.ATotalNormQuantity :=
              ADrawObject.ATotalNormQuantity + FieldByName('NORMQTY').AsFloat;
          end; // if
        end; // if
        Next;
      end; // while
    end; // with
  end; // ActionGhostCountShift
  // PIM-213
  function PrecheckWSEmpEffEmployees: Boolean;
  var
    I: Integer;
    APTAEmployee: PTAEmployee;
  begin
    Result := True;
    if Assigned(ADrawObject.AEmployeeList) then
      if ADrawObject.AEmployeeList.Count > 0 then
      begin
        if (ADrawObject.ApnlWSEmpEff.EmpList.Count = 0) or
          (ADrawObject.AEmployeeList.Count <>
            ADrawObject.ApnlWSEmpEff.EmpList.Count) then
          Result := False
        else
          for I := 0 to ADrawObject.AEmployeeList.Count - 1 do
          begin
            APTAEmployee := ADrawObject.AEmployeeList.Items[I];
            if not Assigned(ADrawObject.ApnlWSEmpEff.FindEmployee(APTAEmployee.AEmployeeNumber)) then
              Result := False;
          end;
      end
      else
        if ADrawObject.ApnlWSEmpEff.EmpList.Count > 0 then
          Result := False;
  end; // PrecheckWSEmpEffEmployees
begin
//  Result := 0;

  // PIM-213
  if (ADrawObject.AProdScreenType = pstWSEmpEff) then
    if Assigned(ADrawObject.ApnlWSEmpEff) then
      if not PrecheckWSEmpEffEmployees then
        ADrawObject.ApnlWSEmpEff.DelEmployees;

  if (ADrawObject.AProdScreenType = pstNoTimeRec) or
    (ADrawObject.AProdScreenType = pstWSEmpEff) then // PIM-213
    ByWorkspot := True
  else
    ByWorkspot := False; // By Machine

  // For BIG-EFF-METERS or by Machine // 20014450.50
  if ADrawObject.ABigEffMeters or
    (ADrawObject.AProdScreenType = pstWSEmpEff) or  // PIM-213
    (not ByWorkspot) then
    ClearEmployeeList(ADrawObject);

  ADrawObject.ATotalWorkedTime := 0;
  ADrawObject.ATotalProdQuantity := 0;
  ADrawObject.ATotalNormQuantity := 0;
  ADrawObject.ATotalProdTime := 0;
  ADrawObject.ATotalNormTime := 0;
  ADrawObject.ATotalPercentage := 0;
  ADrawObject.ACurrentNumberOfEmployees := 0;
  ADrawObject.AJobCode := '';
  ADrawObject.AProdLevelMinPerc := 0;
  ADrawObject.AMachOutputMinPerc := 0;
  ADrawObject.AShiftNumber := 1;
  ADrawObject.AEmployeeCount := 0;
  // Always look at current to know if someone is currently scanned in or not.
  with RealTimeEffDM.odsRTCurrWSEffALL do
  begin
    // Filter on what to find
    Filtered := False;
    if ByWorkspot then
      Filter :=
        '(PLANT_CODE = ' + Quotes(ADrawObject.APlantCode) + ') AND ' +
        '(WORKSPOT_CODE = ' + Quotes(ADrawObject.AWorkspotCode) + ')'
    else
      Filter :=
        '(PLANT_CODE = ' + Quotes(ADrawObject.APlantCode) + ') AND ' +
        '(MACHINE_CODE = ' + Quotes(ADrawObject.AWorkspotCode) + ')';
    Filtered := True;
    if not Eof then
    begin
      // Take some values from 1st record for current about the job and shift
      ADrawObject.AJobCode := FieldByName('JOB_CODE').AsString;
      ADrawObject.AProdLevelMinPerc := FieldByName('PRODLEVELMINPERC').AsInteger;
      ADrawObject.AMachOutputMinPerc := FieldByName('MACHOUTPUTMINPERC').AsInteger;
      ADrawObject.AShiftNumber := FieldByName('SHIFT_NUMBER').AsInteger;
    end;
  end;
  case EfficiencyPeriodMode of
  epCurrent: // Last 5-10 minutes
    begin
      ActionAddEmployeesCurrent;
      ActionGhostCountCurrent; // PIM-151
      // Do this to fill the list of employees for the Machine, needed to show
      // the puppets.
//      if ADrawObject.AProdScreenType = pstMachine then
//        ActionAddEmployeesShift;
    end;
  epSince, epShift: // Today, Shift
    begin
      ActionAddEmployeesShift;
      ActionGhostCountShift;
    end;
  end; // case
  ADrawObject.ATotalWorkedTime :=
    ADrawObject.ATotalWorkedTime + Round(ADrawObject.ATotalProdTime / 60);
  if ADrawObject.ATotalProdTime > 0 then
    ADrawObject.ATotalPercentage :=
      Round(ADrawObject.ATotalNormTime / ADrawObject.ATotalProdTime * 100);
  Result := ADrawObject.ATotalPercentage;
end; // ActionRealTimeEff

procedure THomeF.ActionEfficiency(ADrawObject: PDrawObject);
var
  Efficiency: Double;
  EmployeeCount: Integer;
  // 20013551
  procedure ShowMinPercLabel(AVisible: Boolean;
    ATop, ALeft, AWidth, AHeight: Integer);
  begin
    if Assigned(ADrawObject.AMinPercLabel) then
    begin
      ADrawObject.AMinPercLabel.Visible := AVisible;
      ADrawObject.AMinPercLabel.Font.Size :=
      // 20013551.10 Resize with FontScale
        Trunc(pnlDraw.Font.Size * 1.9 * FontScale / 100); // 1.5
      // 20013551.10 Subtract Label height
      ADrawObject.AMinPercLabel.Top := (ADrawObject.AObject as TImage).Top +
        Trunc(ATop * WorkspotScale / 100) - ADrawObject.AMinPercLabel.Height;
      ADrawObject.AMinPercLabel.Left := (ADrawObject.AObject as TImage).Left +
        Trunc(ALeft * WorkspotScale / 100);
    end;
  end; // ShowMinPercLabel
  // 20013551 Show Min. Efficiency Percentage
  procedure ShowMinPercPercentage;
  begin
    // When there is no data do not show it.
    if (EfficiencyPeriodMode = epCurrent) then
      if ADrawObject.ANoData then
      begin
        ShowMinPercLabel(False, -40, -15, 0, 0); // -100, -15
        Exit;
      end;

    if (not ADrawObject.ABigEffMeters) and
      (ADrawObject.AProdScreenType = pstNoTimeRec) then
    begin
      // For machine hours: No-one is scanned in!
      if ADrawObject.AEffRunningHrs then // Machine hour
      begin
        // When mode=shift but no shift-start was found then do not show it.
        if (EfficiencyPeriodMode = epShift) and
          (ADrawObject.AShiftStartDateTime = Nulldate) then
          ShowMinPercLabel(False, -40, -15, 0, 0) // -100, -15
        else
        begin
          if (ADrawObject.AMachOutputMinPerc > 0) and
            (Efficiency < ADrawObject.AMachOutputMinPerc) then
          begin
            ADrawObject.AMinPercLabel.Caption :=
              IntToStr(Trunc(Efficiency)) + ' %';
            ShowMinPercLabel(True, -40, -15, 0, 0); // -100, -15 // 20013551.10
            ADrawObject.AMinPercBlink := True;
          end
          else
            ShowMinPercLabel(False, -40, -15, 0, 0); // -100, -15
        end;
      end // if
      else // Man hours
      begin
        // For man-hours: Someone is scanned in.
        if (ADrawObject.AProdLevelMinPerc > 0) and
          (Efficiency < ADrawObject.AProdLevelMinPerc) then
        begin
          ADrawObject.AMinPercLabel.Caption :=
            IntToStr(Trunc(Efficiency)) + ' %';
          ShowMinPercLabel(True, -40, -15, 0, 0); // -100, -15 // 20013551.10
          ADrawObject.AMinPercBlink := True;
        end
        else
          ShowMinPercLabel(False, -40, -15, 0, 0); // -100, -15
      end;
    end; // if
  end; // ShowMinPercPercentage
  // 20013551
  procedure ActionShowMinPercInit;
  var
    I, Day: Integer;
    AShift: PTAShift;
    MyNow: TDateTime;
    StartDate, EndDate: TDateTime;
    StartDateTime, EndDateTime: TDateTime;
  begin
    DetermineEffRunningHrs(ADrawObject);
    ADrawObject.AMinPercBlink := False;
    // 20013551 Rework: Init vars here, before the if!
    ADrawObject.AShiftNumber := 0;
    ADrawObject.AShiftStartDateTime := Nulldate;
    ADrawObject.AJobCode := '';
    // 20013551 We have to calculate norm/production for this special situation:
    //          Machine hours!
    //          - No-one scans on here!
    //          - Look at the shift-times to determine the time.
    if {(not ADrawObject.ABigEffMeters) and }
      (ADrawObject.AProdScreenType = pstNoTimeRec) and
       ADrawObject.AEffRunningHrs and
      (EfficiencyPeriodMode = epShift) then // Machine hour
    begin
      DetermineShiftList(ADrawObject); // 20013551
      // Based on shift: Determine time for calculation of efficiency.
      // Get current day-number
      Day  := DayInWeek(ORASystemDM.WeekStartsOn, Date);
      MyNow := Now;
      for I := 0 to ADrawObject.AShiftList.Count - 1 do
      begin
        AShift := ADrawObject.AShiftList.Items[I];

        // Do the following to determine correct shift in case it was
        // an overnight shift!
        StartDate := Date + Frac(AShift.AStartTime[Day]);
        EndDate := Date + Frac(AShift.AEndTime[Day]);
        if StartDate > EndDate then
          EndDate := EndDate + 1;
        AShiftDay.Curr.AStart := StartDate;
        AShiftDay.Curr.AEnd := EndDate;
        AShiftDay.Curr.AValid := True;

        StartDate := Date + 1 + Frac(AShift.AStartTime[Day]);
        EndDate := Date + 1 + Frac(AShift.AEndTime[Day]);
        if StartDate > EndDate then
          EndDate := EndDate + 1;
        AShiftDay.Next.AStart := StartDate;
        AShiftDay.Next.AEnd := EndDate;
        AShiftDay.Next.AValid := True;

        StartDate := Date - 1 + Frac(AShift.AStartTime[Day]);
        EndDate := Date - 1 + Frac(AShift.AEndTime[Day]);
        if StartDate > EndDate then
          EndDate := EndDate + 1;
        AShiftDay.Prev.AStart := StartDate;
        AShiftDay.Prev.AEnd := EndDate;
        AShiftDay.Prev.AValid := True;

        AShiftDay.AScanStart := MyNow;
        AShiftDay.AScanEnd := MyNow;

        StartDateTime := MyNow;
        EndDateTime := MyNow;
        AShiftDay.DetermineNewStartEnd(StartDateTime, EndDateTime);
        StartDate := StartDateTime;
        EndDate := EndDateTime;

// TESTING!!!
{WriteLog('StartDate=' + DateTimeToStr(StartDate) + ' - ' +
  DateTimeToStr(EndDate)); }

        if (StartDate <= MyNow) and (EndDate >= MyNow) then
        begin
          ADrawObject.AShiftStartDateTime := StartDate;
//          DateFrom := ADrawObject.AShiftStartDateTime;
          ADrawObject.AShiftNumber := AShift.AShiftNumber;
          Break;
        end;
      end;
    end;
  end; // ActionMinPercInit
begin
  // Calculate Efficiency
  Efficiency := ActionRealTimeEff(ADrawObject);
  EmployeeCount := ADrawObject.ACurrentNumberOfEmployees;

  // 20013551
  ActionShowMinPercInit;

  // Show Efficiency
  if (ADrawObject.AObject is TImage) then
  begin
    ShowEffMeter(ADrawObject, Trunc(Efficiency), ADrawObject.AObject);
    // 20013551
    ShowMinPercPercentage;
  end;

  // Show Totals for BIG-EFF-METER
  if ADrawObject.ABigEffMeters then
  begin
    ADrawObject.AEffTotalsLabel.Visible := True;
    ADrawObject.AEffTotalsLabel.Caption := '';

    // 20013551 Rework
    if (EmployeeCount > 0) or
    (
      (ADrawObject.AProdScreenType = pstNoTimeRec) and
      ADrawObject.AEffRunningHrs // Machine hours
    ) then
    begin
      ADrawObject.AEffTotalsLabel.Caption :=
        ZeroFormat(IntToStr(ADrawObject.ATotalWorkedTime DIV 60), 2) +
        ':' +
        ZeroFormat(IntToStr(ADrawObject.ATotalWorkedTime MOD 60), 2) + ' ' +
        lblHr.Caption + ' ' +
        Format('%.0f', [ADrawObject.ATotalProdQuantity]) + ' ' +
        lblPcs.Caption + ' ' +
        Format('%.0f', [ADrawObject.ATotalNormQuantity]) + ' ' +
        lblNormPcs.Caption + ' ' +
        IntToStr(ADrawObject.ATotalPercentage) + ' %';
    end;
    ShowDrawObject(ADrawObject, ADrawObject.AObject);
  end
  else
  begin // PIM-213
    if (ADrawObject.AProdScreenType = pstWSEmpEff) then
      if Assigned(ADrawObject.ApnlWSEmpEff) then
        ShowDrawObject(ADrawObject, ADrawObject.AObject);
  end;
end; // ActionEfficiency

procedure THomeF.RepositionSubForm(ASubForm: TForm);
var
  NewTop, NewLeft: Integer;
begin
  // TD-21429 Centre sub-form relative to HomeF.
  if ASubForm.Name <> 'TimeRecordingF' then
  begin
    if HomeF.Width < MaxWidth then // TD-23620
    begin
      ASubForm.Height := HomeF.Height;
      ASubForm.Width := HomeF.Width;
    end
    else
    begin
      ASubForm.Height := (HomeF.Height - HomeF.Height div 10);
      ASubForm.Width := (HomeF.Width - HomeF.Width div 10);
      if ASubForm.Height > HomeF.Height then
        ASubForm.Height := HomeF.Height;
      if ASubForm.Width > HomeF.Width then
        ASubForm.Width := HomeF.Width;
    end;
  end
  else
  begin
    if HomeF.Width < MaxWidth then // TD-23620
    begin
      ASubForm.Height := HomeF.Height;
      ASubForm.Width := HomeF.Width;
    end
    else
    begin
      ASubForm.Height := (HomeF.Height - HomeF.Height div 10);
      ASubForm.Width := (HomeF.Width - HomeF.Width div 10);
      if ASubForm.Height > HomeF.Height then
        ASubForm.Height := HomeF.Height;
      if ASubForm.Width > HomeF.Width then
        ASubForm.Width := HomeF.Width;
    end;
  end;
  NewTop := HomeF.Top + (HomeF.Height - ASubForm.Height) div 2;
  NewLeft := HomeF.Left + (HomeF.Width - ASubForm.Width) div 2;
  if NewTop < 0 then
    NewTop := 0;
  ASubForm.Top := NewTop;
  if NewLeft < 0 then
    NewLeft := 0;
  ASubForm.Left := NewLeft;
end; // RepositionSubForm

// 20014450.50
// Find all real time efficiency
procedure THomeF.ActionRealTimeEffMain;
begin
  with RealTimeEffDM do
  begin
    with odsRTCurrWSEffALL do
    begin
      Close;
      Open;
    end;
    with odsRTShiftWSEffALL do
    begin
      Close;
      Open;
    end;
    // PIM-151
    with odsRTCurrGhostAll do
    begin
      Close;
      Open;
    end;
    with odsRTShiftGhostALL do
    begin
      Close;
      Open;
    end;
  end;
  // PIM-213
  WorkspotEmpEffDM.WorkspotEmpEffRefresh;
end; // ActionRealTimeEffMain

// PIM-151
procedure THomeF.GhostImageDisable;
var
  I: Integer;
  MyDrawObject: PDrawObject;
begin
  for I := 0 to List.Count - 1 do
  begin
    MyDrawObject := List.Items[i];
    if not MyDrawObject.ABigEffMeters then
      if (MyDrawObject.AObject is TImage) then
      begin
        if Assigned(MyDrawObject.ADefaultImage) then
        begin
          MyDrawObject.AObject := MyDrawObject.ADefaultImage;
         (MyDrawObject.AObject as TImage).Visible := True;
        end;
      end;
    if Assigned(MyDrawObject.AGhostImage) then
      MyDrawObject.AGhostImage.Visible := False;
  end;
  Application.ProcessMessages;
end;

// PIM-213
procedure THomeF.actWSEmpEffExecute(Sender: TObject);
begin
  inherited;
  if DialogWorkspotSelectF.RootPath = '' then
    DialogWorkspotSelectF.RootPath := ImagePath;
  DialogWorkspotSelectF.ActionWorkspot := True;
  DialogWorkspotSelectF.NoPicture := True;
  if DialogWorkspotSelectF.ShowModal = mrOK then
    AddObjectToList(otImage, nil,
      DialogWorkspotSelectF.PlantCode,
      DialogWorkspotSelectF.WorkspotCode,
      DialogWorkspotSelectF.WorkspotDescription,
      DialogWorkspotSelectF.ImageName,
      False, 0,
      pstWSEmpEff
      //DialogWorkspotSelectF.ProdScreenType
      );
end; // actWSEmpEffExecute

// PIM-213
function THomeF.WSEmpEffCreate(ADrawObject: PDrawObject;
  AShortName: String): TpnlWSEmpEff;
begin
  Result :=
    TpnlWSEmpEff.WSCreate(pnlDraw, ADrawObject.APlantCode,
      ADrawObject.AWorkspotCode, AShortName, WorkspotScale, FontScale,
      ImageClick, ImageMouseDown, ImageMouseMove, ImageMouseUp,
      (ADrawObject.AObject as TImage).Tag);
  Result.Tag := (ADrawObject.AObject as TImage).Tag;
  Result.OnClick := ImageClick;
  Result.OnMouseDown := ImageMouseDown;
  Result.OnMouseMove := ImageMouseMove;
  Result.OnMouseUp := ImageMouseUp;
end; // WSEmpEffCreate

procedure THomeF.WSEmpEffEditMode(AOn: Boolean);
var
  I: Integer;
  MyDrawObject: PDrawObject;
begin
  for I := 0 to List.Count - 1 do
  begin
    MyDrawObject := List.Items[i];
    if MyDrawObject.AProdScreenType = pstWSEmpEff then
    begin
      if Assigned(MyDrawObject.ApnlWSEmpEff) then
      begin
        MyDrawObject.ApnlWSEmpEff.EditMode(AOn);
        if AOn then
          MyDrawObject.ApnlWSEmpEff.pnlWSSummary.Caption :=
            MyDrawObject.ApnlWSEmpEff.lblWorkspot.Caption; 
      end;
    end;
  end;
end; // WSEmpEffEditMode

end.

