(*
  MRA:17-SEP-2012 SO-20013563 Head count.
  - Addition of option Head Count.
  MRA:3-JUN-2013 20014289 New look Pims
  - Some pictures/colors are changed.
*)
unit DialogSelectModeFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BaseDialogFRM, ActnList, StdCtrls, Buttons, ComCtrls,
  ExtCtrls, Menus, dxCntner, StdActns, ImgList, jpeg;

type
  TDialogSelectModeF = class(TBaseDialogForm)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    cmbBoxMode: TComboBox;
    procedure btnOkClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    FSelectedMode: Integer;
    FModeString: String;
    procedure SetSelectedMode(const Value: Integer);
    procedure SetModeString(const Value: String);
    procedure AddOptions;
    { Private declarations }
  public
    { Public declarations }
    property SelectedMode: Integer read FSelectedMode write SetSelectedMode;
    property ModeString: String read FModeString write SetModeString;
  end;

var
  DialogSelectModeF: TDialogSelectModeF;

implementation

{$R *.DFM}

uses
  UPimsMessageRes;

{ TDialogSelectModeF }

procedure TDialogSelectModeF.AddOptions;
begin
  cmbBoxMode.Items.Clear;
  // Must be in same sequence as 'TShowMode'-type!
  // but first item has been left out!
  // So, first item should start at 1 instead of 0. 
  cmbBoxMode.Items.Add(SPimsEmployeesAll);
  cmbBoxMode.Items.Add(SPimsEmployeesOnWrongWorkspot);
  cmbBoxMode.Items.Add(SPimsEmployeesNotScannedIn);
  cmbBoxMode.Items.Add(SPimsEmployeesAbsentWithReason);
  cmbBoxMode.Items.Add(SPimsEmployeesAbsentWithoutReason);
  cmbBoxMode.Items.Add(SPimsEmployeesWithFirstAid);
  cmbBoxMode.Items.Add(SPimsEmployeesTooLate);
  cmbBoxMode.Items.Add(SPimsEmployeesHeadCount); // SO-20013563 (option=8)
  cmbBoxMode.ItemIndex := 0;
end;

procedure TDialogSelectModeF.SetSelectedMode(const Value: Integer);
begin
  FSelectedMode := Value;
end;

procedure TDialogSelectModeF.btnOkClick(Sender: TObject);
begin
  inherited;
  if cmbBoxMode.ItemIndex <> -1 then
  begin
    SelectedMode := cmbBoxMode.ItemIndex + 1; // Mode should start with 1
    ModeString := cmbBoxMode.Items[cmbBoxMode.ItemIndex];
  end
  else
  begin
    SelectedMode := 0;
    ModeString := '';
  end;
end;

procedure TDialogSelectModeF.SetModeString(const Value: String);
begin
  FModeString := Value;
end;

procedure TDialogSelectModeF.FormCreate(Sender: TObject);
begin
  inherited;
  lblMessage.Caption := ' ' + Caption; // 20014289
  AddOptions;
end;

end.
