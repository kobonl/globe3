inherited DialogChartSettingsF: TDialogChartSettingsF
  VertScrollBar.Range = 0
  BorderStyle = bsDialog
  Caption = 'Settings'
  ClientHeight = 185
  ClientWidth = 491
  Position = poOwnerFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlImageBase: TPanel
    Width = 491
    TabOrder = 1
    inherited imgBasePims: TImage
      Left = 374
    end
  end
  inherited stbarBase: TStatusBar
    Top = 103
    Width = 491
  end
  inherited pnlInsertBase: TPanel
    Width = 491
    Height = 66
    Font.Color = clBlack
    Font.Height = -11
    object GroupBox1: TGroupBox
      Left = 0
      Top = 0
      Width = 491
      Height = 66
      Align = alClient
      Caption = 'Export Settings'
      TabOrder = 0
      object Label1: TLabel
        Left = 8
        Top = 24
        Width = 48
        Height = 13
        Caption = 'Separator'
      end
      object cBoxSeparator: TComboBox
        Left = 104
        Top = 22
        Width = 89
        Height = 21
        ItemHeight = 13
        TabOrder = 0
        Text = 'cBoxSeparator'
        Items.Strings = (
          ','
          ';')
      end
    end
  end
  inherited pnlBottom: TPanel
    Top = 122
    Width = 491
    inherited btnOK: TBitBtn
      Left = 128
      OnClick = btnOkClick
    end
    inherited btnCancel: TBitBtn
      Left = 242
    end
  end
end
