(*
  MRA:23-JAN-2014 TD-24046
  - Translate issues: Use a different method to translate.
    - Do NOT do this in separate files where the translation is done
      hard-coded!
    - New method:
      - Use 1 resource-strings-file that contains the translation-strings.
      - Use 1 translate-unit that assigns these strings to the properties
        in the forms.
      - In this way there is only 1 form that can be translated in multiple
        languages when the application is run.
      - The project-file defines the language using a condition.
  MRA:9-MAR-2017
  - Add Dept for translation.
*)
unit UTranslateProdScreen;

interface

type
  TTranslateHandlerProdScreen = class
  private
  public
    constructor Create; overload;
    destructor Destroy; override;
    procedure TranslateAll;
    procedure TranslateInit;
    procedure TranslateDialogChartSettings;
    procedure TranslateDialogLegenda;
    procedure TranslateDialogPrintChart;
    procedure TranslateDialogSelectMode;
    procedure TranslateDialogSettings;
    procedure TranslateProductionScreenWait;
    procedure TranslateShowChart;
  end;

var
  ATranslateHandlerProdScreen: TTranslateHandlerProdScreen;


implementation

uses
  UTranslateStringsProdScreen, HomeFRM, DialogChartSettingsFRM,
  DialogDepartmentSelectFRM, DialogImagePropertiesFRM, DialogLegendaFRM,
  DialogPrintChartFRM, DialogSelectModeFRM, DialogSettingsFRM,
  DialogShowAllEmployeesFRM, DialogLoginFRM, DialogWorkspotSelectFRM,
  ProductionScreenWaitFRM, ShowChartFRM, BaseTopMenuFRM;

{ TTranslateHandlerProdScreen }

constructor TTranslateHandlerProdScreen.Create;
begin
//
end;

destructor TTranslateHandlerProdScreen.Destroy;
begin
//
  inherited;
end;

procedure TTranslateHandlerProdScreen.TranslateAll;
begin
  if Assigned(BaseTopMenuForm) then
    with BaseTopMenuForm do
    begin
      HelpAboutABSAct.Caption := STransAboutABSGroup;
      HelpABSHomePageAct.Caption := STransABSGroupHomePage;
      HelpOrderingInfoAct.Caption := STransOrderInfo;
      HelpPimsHomePageAct.Caption := STransPimsHomePage;
      HelpContentsAct.Caption := STransContents;
      HelpIndexAct.Caption := STransIndex;
      FileExit1.Caption := STransExit1;
      FileSettingsAct.Caption := STransSettings;
      FilePrintAct.Caption := STransPrint;
      FileClose.Caption := STransExit2;
      miFile1.Caption := STransFile;
      miHelp1.Caption := STransHelp;
    end;
  if Assigned(HomeF) then
    with HomeF do
    begin
      Caption := STransHomeCaption;
      lblHr.Caption := STransHR;
      lblPcs.Caption := STransPCS;
      lblNormPcs.Caption := STransNormPCS;
      ShowAllEmployees1.Caption := STransShowAllEmps;
      ShowEmployeesonwrongWokspot1.Caption := STransShowEmpsOnWrongWorkspot;
      ShowEmployeesnotscannedin1.Caption := STransShowEmpsNotScannedIn;
      ShowEmployeesabsentwithreason1.Caption := STransShowEmpsAbsWithReason;
      ShowEmployeesabsentwithoutreason1.Caption := STransShowEmpsAbsWithoutReason;
      ShowEmployeeswithFirstAid1.Caption := STransShowEmpsWithFirstAid;
      ShowEmployeestoolate1.Caption := STransShowEmpsTooLate;
      ShowChart.Caption := STransShowChart;
      ShowEmployees1.Caption := STransShowEmps;
      actWorkspot.Caption := STransWorkspot;
      actWorkspot.Hint := STransWorkspot;
      actEffiMeter.Caption := STransEffMeter;
      actEffiMeter.Hint := STransWorkspotBigEffMeter;
      actLineHorz.Caption := STransLineHorz;
      actLineHorz.Hint := STransLineHorz;
      actLineVert.Caption := STransLineVert;
      actLineVert.Hint := STransLineVert;
      actRectangle.Caption := STransRect;
      actRectangle.Hint := STransRect;
      actPuppetBox.Caption := STransEmpOverview;
      actPuppetBox.Hint := STransEmpOverview;
      actDelete.Caption := STransDelete;
      actDelete.Hint := STransDelete;
      actSave.Caption := STransSave;
      actSave.Hint := STransSaveScheme;
      actEdit.Caption := STransEdit;
      actEdit.Hint := STransEdit;
      actNew.Caption := STransNew;
      actNew.Hint := STransNew;
      actOpen.Caption := STransOpen;
      actOpen.Hint := STransOpen;
      actSaveAs.Caption := STransSaveAs;
      actSaveAs.Hint := STransSaveAs;
      HelpLegendaAct.Caption := STransLegenda;
      actShowStatusInfo.Caption := STransShowStatusInfo;
      FileSettingsAct.Caption := STransSettings;
      // Menu-options
      HelpAboutABSAct.Caption := STransAboutABSGroup;
      HelpABSHomePageAct.Caption := STransABSGroupHomePage;
      HelpOrderingInfoAct.Caption := STransOrderInfo;
      HelpPimsHomePageAct.Caption := STransPimsHomePage;
      HelpContentsAct.Caption := STransContents;
      HelpIndexAct.Caption := STransIndex;
      FileExit1.Caption := STransExit1;
      FileSettingsAct.Caption := STransSettings;
      FilePrintAct.Caption := STransPrint;
      FileClose.Caption := STransExit2;
      miFile1.Caption := STransFile;
      miHelp1.Caption := STransHelp;
    end;
  if Assigned(DialogDepartmentSelectF) then
    with DialogDepartmentSelectF do
    begin
      Caption := STransSelectDept;
      Label3.Caption := STransPlant;
      Label1.Caption := STransDept;
      btnOK.Caption := STransOK;
      btnCancel.Caption := STransCancel;
    end;
  if Assigned(DialogImagePropertiesF) then
    with DialogImagePropertiesF do
    begin
      Caption := STransImageProperties;
      Label1.Caption := STransLeftPos;
      Label2.Caption := STransTopPos;
      Label3.Caption := STransWidth;
      Label4.Caption := STransHeight;
      lblPlantCode.Caption := STransPlantCode;
      lblWorkspotCode.Caption := STransWorkspotCode;
      lblWorkspotDescription.Caption := STransWorkspotDesc;
      OKBtn.Caption := STransOK;
      CancelBtn.Caption := STransCancel;
    end;
  if Assigned(DialogShowAllEmployeesF) then
    with DialogShowAllEmployeesF do
    begin
      Caption := STransEmpsOnWorkspot;
      dxList.Bands.Items[0].Caption := STransEmployee;
      dxList.Bands.Items[1].Caption := STransScanned;
      dxList.Bands.Items[2].Caption := STransPlanned;
      dxList.Bands.Items[3].Caption := STransStandAvail;
      dxList.Bands.Items[4].Caption := STransAbsWithReason;
      dxList.Bands.Items[5].Caption := STransFirstAid;
      dxList.Bands.Items[6].Caption := STransEfficiency;
      dxListColumnNUMBER.Caption := STransNumber;
      dxListColumnNAME.Caption := STransName;
      dxListColumnSHORTNAME.Caption := STransShort;
      dxListColumnTEAM.Caption := STransTeam;
      dxListColumnSCANNEDWS.Caption := STransWS;
      dxListColumnPLANNEDWS.Caption := STransWS;
      dxListColumnSCANNEDDATEIN.Caption := STransTimeIn;
      dxListColumnPLANNEDSTARTTIME.Caption := STransStartTime;
      dxListColumnPLANNEDENDTIME.Caption := STransEndTime;
      dxListColumnELEVEL.Caption := STransLevel;
      dxListColumnEPLANLEVEL.Caption := STransLevel;
      dxListColumnSASTARTTIME.Caption := STransStartTime;
      dxListColumnEASTARTTIME.Caption := STransStartTime;
      dxListColumnSAENDTIME.Caption := STransEndTime;
      dxListColumnEAENDTIME.Caption := STransEndTime;
      dxListColumnABSENCEREASONCODE.Caption := STransAbsReasonCode;
      dxListColumnABSENCEREASONDESCR.Caption := STransAbsReasonDescr;
      dxListColumnFIRSTAIDEXPIRATIONDATE.Caption := STransExpDate;
      dxListColumnCURREFF.Caption := STransCurrent;
      dxListColumnSHIFTEFF.Caption := STransToday;
      dxListColumnPLANNEDDEPT.Caption := STransDP; // PIM-275
      btnOK.Caption := STransOK;
      btnCancel.Caption := STransCancel;
      // Menu-options
      HelpAboutABSAct.Caption := STransAboutABSGroup;
      HelpABSHomePageAct.Caption := STransABSGroupHomePage;
      HelpOrderingInfoAct.Caption := STransOrderInfo;
      HelpPimsHomePageAct.Caption := STransPimsHomePage;
      HelpContentsAct.Caption := STransContents;
      HelpIndexAct.Caption := STransIndex;
      FileExit1.Caption := STransExit1;
      FileSettingsAct.Caption := STransSettings;
      FilePrintAct.Caption := STransPrint;
      FileClose.Caption := STransExit2;
      miFile1.Caption := STransFile;
      miHelp1.Caption := STransHelp;
    end;
  if Assigned(DialogWorkspotSelectF) then
    with DialogWorkspotSelectF do
    begin
      Caption := STransWorkspotSelect;
      lblMessage.Caption := STransWorkspotSelect;
      Label3.Caption := STransPlant;
      Label1.Caption := STransWorkspot;
      Label2.Caption := STransPicture;
      LabelMachine.Caption := STransMachine;
      dxTreeList1Column1.Caption := STransPicture;
      rgProdScreenType.Caption := STransTypeOfProdScreen;
      rgProdScreenType.Items.Strings[0] := STransProdBarsWithoutTR;
      rgProdScreenType.Items.Strings[1] := STransMachine;
      // Not used:
//      rgProdScreenType.Items.Strings[1] := STransProdBarsTRMachineLevel;
//      rgProdScreenType.Items.Strings[2] := STransProdBarsTRWorskpotLevel;
      btnOK.Caption := STransOK;
      btnCancel.Caption := STransCancel;
    end;
    TranslateDialogChartSettings;
    TranslateDialogLegenda;
    TranslateDialogPrintChart;
    TranslateDialogSelectMode;
    TranslateDialogSettings;
    TranslateProductionScreenWait;
    TranslateShowChart;
end; // TranslateAll

procedure TTranslateHandlerProdScreen.TranslateDialogChartSettings;
begin
  if Assigned(DialogChartSettingsF) then
    with DialogChartSettingsF do
    begin
      Caption := STransSettings;
      lblMessage.Caption := STransSettings;
      GroupBox1.Caption := STransExportSettings;
      Label1.Caption := STransSeparator;
      btnOK.Caption := STransOK;
      btnCancel.Caption := STransCancel;
    end;
end;

procedure TTranslateHandlerProdScreen.TranslateDialogLegenda;
begin
  if Assigned(DialogLegendaF) then
    with DialogLegendaF do
    begin
      Caption := STransLegenda;
      GroupBox1.Caption := STransPuppets;
      Label1.Caption := STransPlanned1;
      Label2.Caption := STransPlanned2;
      Label3.Caption := STransPlanned3;
      Label4.Caption := STransPlanned4;
      Label5.Caption := STransNotPlanned;
      Label6.Caption := STransBlinking;
      GroupBox2.Caption := STransWorkspot;
      Label7.Caption := STransWorkspotFrame;
      Label8.Caption := STransAndTheDev;
      GroupBox3.Caption := STransTrafficLight;
      Label9.Caption := STransTheLastTime1;
      Label10.Caption := STransTheLastTime2;
      Label11.Caption := STransTheLastTime3;
      btnOk.Caption := STransOK;
      btnCancel.Caption := STransCancel;
    end;
end;

procedure TTranslateHandlerProdScreen.TranslateDialogPrintChart;
begin
  if Assigned(DialogPrintChartF) then
    with DialogPrintChartF do
    begin
      Caption := STransPrintChart;
      rGrpOptions.Caption := STransPrintOptions;
      rGrpOptions.Items.Strings[0] := STransPortrait;
      rGrpOptions.Items.Strings[1] := STransLandscape;
      btnPrinterSetup.Caption := STransPrinterSetup;
      btnOk.Caption := STransOK;
      btnCancel.Caption := STransCancel;
      // Menu-options
      HelpAboutABSAct.Caption := STransAboutABSGroup;
      HelpABSHomePageAct.Caption := STransABSGroupHomePage;
      HelpOrderingInfoAct.Caption := STransOrderInfo;
      HelpPimsHomePageAct.Caption := STransPimsHomePage;
      HelpContentsAct.Caption := STransContents;
      HelpIndexAct.Caption := STransIndex;
      FileExit1.Caption := STransExit1;
      FileSettingsAct.Caption := STransSettings;
      FilePrintAct.Caption := STransPrint;
      FileClose.Caption := STransExit2;
      miFile1.Caption := STransFile;
      miHelp1.Caption := STransHelp;
    end;
end;

procedure TTranslateHandlerProdScreen.TranslateDialogSelectMode;
begin
  if Assigned(DialogSelectModeF) then
    with DialogSelectModeF do
    begin
      Caption := STransEmpOverview;
      lblMessage.Caption := STransEmpOverview;
      GroupBox1.Caption := STransSelectMode;
      Label1.Caption := STransMode;
      cmbBoxMode.Items.Strings[0] := STransShowAllEmployees;
      btnOk.Caption := STransOK;
      btnCancel.Caption := STransCancel;
    end;
end;

procedure TTranslateHandlerProdScreen.TranslateDialogSettings;
begin
  if Assigned(DialogSettingsF) then
    with DialogSettingsF do
    begin
      Caption := STransSettings;
      GroupBox1.Caption := STransGlobalSettings;
      GroupBox3.Caption := STransSoundAlarm;
      Label1.Caption := STransSoundFilename;
      Label2.Caption := STransUseSoundAlarm;
      btnBrowse.Caption := STransBrowse;
      GroupBox6.Caption := STransRefreshTimeInterval;
      Label6.Caption := STransInterval;
      Label7.Caption := STransSeconds;
      GroupBox2.Caption := STransSchemeSettings;
      GroupBox4.Caption :=  STransWorkspotScale;
      Label3.Caption := STransPercentage;
      Label9.Caption := STransGridlineDistance;
      GroupBox5.Caption := STransEfficiencyMeters;
      Label4.Caption := STransUsePeriod;
      rGrpUsePeriod.Items.Strings[0] := STransCurrent;
      rGrpUsePeriod.Items.Strings[1] := STransSince;
      rGrpUsePeriod.Items.Strings[2] := STransShift;
      grpBxSinceTime.Caption := STransSince;
      Label5.Caption := STransTime;
      GroupBox7.Caption := STransFontScale;
      btnOk.Caption := STransAccept;
      btnCancel.Caption := STransCancel;
      GroupBox8.Caption := STransEffColorBoundaries;
      lblRed.Caption := STransRed;
      lblOrange.Caption := STransOrange;
      gBoxShowEmpInfo.Caption := STransShow;
      cBoxShowEmpInfo.Caption := STransEmpInfo;
      cBoxShowEmpEff.Caption := STransEmpEff;
    end;
end; // TranslateDialogSettings

procedure TTranslateHandlerProdScreen.TranslateProductionScreenWait;
begin
  if Assigned(ProductionScreenWaitF) then
    with ProductionScreenWaitF do
    begin
      Caption := STransProdScreen;
    end;
end;

procedure TTranslateHandlerProdScreen.TranslateShowChart;
begin
  if Assigned(ShowChartF) then
    with ShowChartF do
    begin
      Caption := STransGraph;
      GroupBox1.Caption := STransStats;
      lblTimeIntTitle.Caption := STransTimeInt;
      lblTimeTitle.Caption := STransTime2;
      lblProdHrTitle.Caption := STransProdHr;
      lblProdNormTitle.Caption := STransProdNorm;
      lblNrOfEmplTitle.Caption := STransNrOfEmp;
      cBoxShowEmpl.Caption := STransShowEmp;
      GroupBox3.Caption := STransGraph;
      cBoxScale.Caption := STransScale;
      GroupBox2.Caption := STransDateInput;
      Label6.Caption := STransFromDate;
      Label7.Caption := STransTo;
      btnAccept.Caption := STransAccept;
      cBoxEnterDates.Caption := STransEnterDates;
      dxList.Bands.Items[0].Caption := STransEmployees;
      dxListColumnEMPNR.Caption := STransEmpNr;
      dxListColumnEMPNAME.Caption := STransEmpName;
      dxListColumnJOBCODE.Caption := STransJobCode;
      dxListColumnJOBNAME.Caption := STransJobName;
      dxListColumnSHIFTNR.Caption := STransShiftNr;
      dxListColumnDATETIME_IN.Caption := STransDateTimeIn;
      Chart1.Title.Text.Strings[0] := STransProdOutput;
      lblTotalProdDescription.Caption := STransTotalProd;
      lblEmpCnt.Caption := STransEC;
      Series2.Title := STransBackground;
      Series4.Title := STransNrOfEmp100;
      Series3.Title := STransNormProdLevel;
      Series1.Title := STransOutput;
      btnCloseGraph.Caption := STransCloseGraph;
      FileExportAct.Caption := STransExportChart;
      // Menu-options
      HelpAboutABSAct.Caption := STransAboutABSGroup;
      HelpABSHomePageAct.Caption := STransABSGroupHomePage;
      HelpOrderingInfoAct.Caption := STransOrderInfo;
      HelpPimsHomePageAct.Caption := STransPimsHomePage;
      HelpContentsAct.Caption := STransContents;
      HelpIndexAct.Caption := STransIndex;
      FileExit1.Caption := STransExit1;
      FileSettingsAct.Caption := STransSettings;
      FilePrintAct.Caption := STransPrint;
      FileClose.Caption := STransExit2;
      miFile1.Caption := STransFile;
      miHelp1.Caption := STransHelp;
    end;
end; // TranslateShowChart

procedure TTranslateHandlerProdScreen.TranslateInit;
begin
  if Assigned(DialogLoginF) then
    with DialogLoginF do
    begin
      Caption := STransDatabaseLogin;
      Label2.Caption := STransPassword;
      Label1.Caption := STransUsername;
      Label3.Caption := STransDatabasePims;
      btnOk.Caption := STransOK;
      btnCancel.Caption := STransCancel;
    end;
end; // TranslateInit

initialization
  { Initialization section goes here }
  ATranslateHandlerProdScreen := TTranslateHandlerProdScreen.Create;

finalization
  { Finalization section goes here }
  ATranslateHandlerProdScreen.Free;

end.

