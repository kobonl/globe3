unit DialogPrintChartFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BaseDialogFRM, ActnList,  StdCtrls, Buttons, ComCtrls,
  ExtCtrls, dxCntner, Menus, StdActns, ImgList;

type
  TDialogPrintChartF = class(TBaseDialogForm)
    rGrpOptions: TRadioGroup;
    btnPrinterSetup: TButton;
    PrinterSetupDialog1: TPrinterSetupDialog;
    procedure btnPrinterSetupClick(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure pnlBottomResize(Sender: TObject);
  private
    { Private declarations }
    FPortrait: Boolean;
  public
    { Public declarations }
    property Portrait: Boolean read FPortrait;
  end;

var
  DialogPrintChartF: TDialogPrintChartF;

implementation

{$R *.DFM}
(*
{$IFDEF PIMSDUTCH}
{$R *NLD.DFM}
{$ELSE}
  {$IFDEF PIMSGERMAN}
  {$R *DEU.DFM}
  {$ELSE}
    {$IFDEF PIMSFRENCH}
    {$R *FRA.DFM}
    {$ELSE}
    {$R *.DFM}
    {$ENDIF}
  {$ENDIF}
{$ENDIF}
*)
procedure TDialogPrintChartF.btnPrinterSetupClick(Sender: TObject);
begin
  inherited;
  PrinterSetupDialog1.Execute;
end;

procedure TDialogPrintChartF.btnOkClick(Sender: TObject);
begin
  inherited;
  FPortrait := (rGrpOptions.ItemIndex = 0);
end;

procedure TDialogPrintChartF.pnlBottomResize(Sender: TObject);
begin
// do not  inherited;
end;

end.
