(*
  Changes:
    MRA:26-08-2008: RV009 (Revision 009)
      - The function to get and show a chart was very slow. This is probably a
        Oracle 10-problem. To solve it, the query that gets the values
        is divided over 2 queries. One only gets the number of records,
        the other one gets the data. Also a hint is used.
        Changed: oqProductionQuantityChart
        Added: oqProductionQuantityChartCount
    MRA:22-SEP-2009 RV034.1.
      - Filter on workspot went wrong because no quotes were used.
        This gave an error with a plant_code like '05'.
    MRA:JUN-2010. RV063.4. Order 550478. Personal Screen.
    - Addition of Machine/Workspot + Time recording.
    MRA:29-JUL-2010. RV063.6.
    - Fix for Shift-mode. Also get dept. + empl. for Timeblocks-per-dept and
      Timeblocks-per-empl during detemination of the start/end-times.
    MRA:28-SEP-2010. RV064.1. Bugfix.
    - Production Screen used as Personal Screen:
      - When used on 2 workstations it can happen a scan a new open
        scan is created, while there is already an open scan for
        the same employee.
      - Added a procedure that will check if there was interference.
        This is logged to a file.
    MRA:21-NOV-2011. RV083.2. 20012357. Bugfix.
    - Query 'oqTimeRegScanningJob' and 'oqTimeRegScanning' did not look at
      the Plant, this could result in showing employees from a different
      plant in case workspotcode and jobcode are the same.
      Solved by adding PLANT_CODE to the query.
    MRA:18-JUN-2012 20013311
    - Addition of MyDateTo that can be used to add some minutes to the
      dateto for comparison.
    MRA:3-AUG-2012 20013478. Related to this order.
    - For this order a field was added to JOBCODE-table:
      Field: IGNOREQUANTITIESINREPORTS_YN
      This field must be added when jobs are autom. added like the BREAK-job.
    - Changes made to oqInsertJob.
    MRA:13-SEP-2012 SO-20013516 Related to this order
    - NOJOB is not ignored, resulting in large quantities.
    MRA:19-OCT-2012 20013551 Production Screen info at tunnels
    - Get extra fields PRODLEVELMINPERC and MACHOUTPUTMINPERC from job, from
      odsTimeRegScanningEFF.
    MRA:22-NOV-2012 200134549 Related to this order.
    - When an INSERT-statement is used, always use the structure:
      - INSERT xxx (field1, field2, ...) values (field1, field2, ...)
      - And be sure the fieldnames are all added, except for:
        - Fields that will be added later do not have to be included,
          when they are not-null and have a default value set on db-level.
    MRA:5-DEC-2012 20013551 Production Screen Info at tunnels - Rework.
    - For this order, production quantities are read from imports.
      - When quantities are determined from PQ-table, then it used
        END_DATE as compare-date. This gives however problems when
        a START_DATE starts longer than 5 minutes before END_DATE,
        this can also give data with a START_DATE from previous days!
        To solve this it must use START_DATE as comparison-date in
        every query where PRODUCTIONQUANTITY is used.
    MRA:11-FEB-2013 TD-22082 Perfomance problems
    - Added functionality related to:
      - Filter on teams-per-user to limit the number records read.
    MRA:5-MAY-2014 TD-24852
    - Production Screen does not show production quantity when using Shift-mode.
    MRA:2-MAR-2015 TD-25881.50 Rework
    - Problems with show graph
    - It found more than 1 record per period because of 'between', this has
      been changed to >= :fromdate and < :enddate. 
    MRA:10-APR-2015 SO-20016449
    - Time Zone Implementation
    - oqHeadCount: Do not use SYSDATE in query, but use an parameter and assign
                   with Now. Also, add a <= condition to prevent it looks in the
                   future.
*)

unit ProductionScreenDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ORASystemDMT, Db, dxEdLib, UPimsConst, {UProductionScreenConst, }
  DBClient, Provider, Variants, OracleData, Oracle, PersonalScreenDMT,
  DateUtils;

const
  MECHANICAL_DOWN_JOB='TDOWN';
  NO_MERCHANDIZE_JOB='PDOWN';
  
type
  TEffPeriodMode = (epCurrent, epSince, epShift, epGraph);

type
  TProductionScreenDM = class(TDataModule)
    odsProductionQuantity: TOracleDataSet;
    odsTimeRegScanningEFF: TOracleDataSet;
    odsProductionQuantityBlinkPuppets: TOracleDataSet;
    odsProductionQuantitySum: TOracleDataSet;
    odsProductionQuantitySumQUANTITYSUM: TFloatField;
    odsProductionQuantitySumPLANT_CODE: TStringField;
    odsProductionQuantitySumWORKSPOT_CODE: TStringField;
    odsProductionQuantitySumEND_DATE: TDateTimeField;
    odsProductionQuantityPLANT_CODE: TStringField;
    odsProductionQuantityWORKSPOT_CODE: TStringField;
    odsProductionQuantityEND_DATE: TDateTimeField;
    odsProductionQuantityQUANTITYSUM: TFloatField;
    dspWorkspotShortName: TDataSetProvider;
    cdsWorkspotShortName: TClientDataSet;
    odsCompProduction: TOracleDataSet;
    odsCompJob: TOracleDataSet;
    odsCompPQ: TOracleDataSet;
    odsPlant: TOracleDataSet;
    odsDepartment: TOracleDataSet;
    odsWorkspot: TOracleDataSet;
    odsJobcode: TOracleDataSet;
    odsWorkspotShortName: TOracleDataSet;
    oqProductionQuantityChartExport: TOracleQuery;
    oqTimeRegScanning: TOracleQuery;
    oqTimeRegScanningJob: TOracleQuery;
    oqProductionQuantityChart: TOracleQuery;
    oqProductionQuantityChartCount: TOracleQuery;
    odsCopyThis: TOracleDataSet;
    odsMachine: TOracleDataSet;
    oqWorkspotByMachine: TOracleQuery;
    oqPQTREmpPieces: TOracleQuery;
    oqCheckJob: TOracleQuery;
    oqInsertJob: TOracleQuery;
    oqGetPrevTRJob: TOracleQuery;
    odsGetTRShift: TOracleDataSet;
    oqGetTRJobPrevPQPeriod: TOracleQuery;
    odsGetTRShift2: TOracleDataSet;
    oqCheckInterference: TOracleQuery;
    oqHeadCount: TOracleQuery;
    oqWSEffRunHrs: TOracleQuery;
    oqWorkspotShift: TOracleQuery;
    odsProductionQuantitySumJOB_CODE: TStringField;
    odsProductionQuantitySumMACHOUTPUTMINPERC: TFloatField;
    odsProductionQuantitySumSTART_DATE: TDateTimeField;
    procedure DataModuleCreate(Sender: TObject);
    procedure odsPlantFilterRecord(DataSet: TDataSet; var Accept: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure FillComboBox(
      DataSet: TDataSet;
      var Combo: TdxPickEdit;
      MasterField: String;
      ShowFirst: Boolean;
      FieldMaster, Field1, Field2: String);
    procedure CompareJobInit(ADateFrom, ADateTo: TDateTime);
    function CompareJob(APlantCode, AWorkspotCode: String;
      ADateFrom, ADateTo: TDateTime;
      var AMaxDeviation: Double;
      var AComparisonReject: Boolean;
      var AJobCode: String; var AJobPieces: Double;
      var ASumJobPieces: Double;
      var AEmpsScanIn: Boolean): Boolean;
{    function EmployeePieces(APlantCode, AWorkspotCode: String;
      AEmployeeNumber, AShiftNumber: Integer; ADateFrom, ADateTo: TDateTime;
      AEfficiencyPeriodMode: TEffPeriodMode): Integer; }
    function DetermineActualShift(APlantCode, AWorkspotCode: String;
      ADateFrom, ADateTo: TDateTime;
      var AShiftStart, AShiftEnd: TDateTime): Integer;
    function DetermineTRJobPrevPQPeriod(APlantCode,
      AWorkspotCode, AJobCode: String; ADateFrom, ADateTo: TDateTime): String;
    procedure CheckInterference;
  end;

function MyDateTo(ADate: TDateTime): TDateTime;

var
  ProductionScreenDM: TProductionScreenDM;
var
  DayList: array[1..7] of Integer;

procedure InitDayList;
function GetDayListIndex(WeekDay: Integer): Integer;
function ZeroFormat(Value: String; Len: Integer): String;

implementation

{$R *.DFM}

uses
  CalculateTotalHoursDMT, UGlobalFunctions, DialogShowStatusInfoFRM,
  UProductionScreenDefs;

// This procedure must be called once!!!
procedure InitDayList;
var
  Index: Integer;
  DayNumber: Integer;
  MyWeekStartsOn: Integer;
begin
  if not ORASystemDM.odsPimsSettings.Active then
    ORASystemDM.odsPimsSettings.Active;
  ORASystemDM.odsPimsSettings.First;
  MyWeekStartsOn :=
    ORASystemDM.odsPimsSettings.FieldByName('WEEK_START_ON').AsInteger;
  DayNumber := MyWeekStartsOn;
  for Index := 1 to 7 do
  begin
    DayList[Index] := DayNumber;
    if DayNumber < 7 then
      inc(DayNumber)
    else
      DayNumber := 1;
  end;
end; // InitDayList

function GetDayListIndex(WeekDay: Integer): Integer;
var
  Index: Integer;
begin
  Result := 1;
  for Index := 1 to 7 do
    if DayList[Index] = WeekDay then
      Result := Index;
end;

function ZeroFormat(Value: String; Len: Integer): String;
begin
  while Length(Value) < Len do
    Value := '0' + Value;
  Result := Value;
end;

procedure TProductionScreenDM.FillComboBox(
  DataSet: TDataSet;
  var Combo: TdxPickEdit;
  MasterField: String;
  ShowFirst: Boolean;
  FieldMaster, Field1, Field2: String);
begin
  with DataSet do
  begin
    if (FieldMaster <> '') then
    begin
      DataSet.Filtered := True;
      // MRA:22-SEP-2009 RV034.1. Use quotes!
      DataSet.Filter := FieldMaster + '=' + '''' + MasterField + '''';
    end;
    Active := True;
    Combo.Items.Clear;
(*    Combo.ClearGridData;
    Combo.RowCount := RecordCount;
    Combo.ColCount := 3;
    Combo.ColWidths[0] := 0;
    Combo.ColWidths[1] := 220; *)
    First;
    while not EOF do
    begin
//      Combo.AddRow([FieldByName(Field1).AsString,
//                    FieldByName(Field1).AsString,
//                    FieldByName(Field1).AsString,
//                    FieldByName(Field2).AsString]);
{      Combo.AddRow([FieldByName(Field1).AsString + Str_Sep +
        FieldByName(Field2).AsString,
                    FieldByName(Field1).AsString + Str_Sep +
        FieldByName(Field2).AsString,
                    FieldByName(Field1).AsString + Str_Sep +
        FieldByName(Field2).AsString]); }

      if Combo <> nil then
      begin
        if ((Combo as TdxPickEdit).Items.IndexOf(FieldByName(Field1).AsString +
          Str_Sep + FieldByName(Field2).AsString) = - 1)
          and (Trim(FieldByName(Field1).AsString) <> '') then
          (Combo as TdxPickEdit).Items.Add(FieldByName(Field1).AsString +
            Str_Sep + FieldByName(Field2).AsString);
      end;


      Next;
    end;
//    Combo.SizeGridToData;
    if RecordCount > 0 then
    begin
      if ShowFirst then
        First
      else
        Last;
//      Combo.DisplayValue := FieldByName(Field1).AsString;
      Combo.Text := FieldByName(Field1).AsString + Str_Sep +
        FieldByName(Field2).AsString;
    end
    else
      Combo.Text := '';
  end;
end; // FillComboBox

// MR:01-03-2004
procedure TProductionScreenDM.CompareJobInit(ADateFrom, ADateTo: TDateTime);
begin
  // Open the query that looks for quantities in ProductionQuantity.
  odsCompPQ.Close;
  odsCompPQ.ClearVariables;
  odsCompPQ.SetVariable('FSTARTDATE', ADateFrom);
  odsCompPQ.SetVariable('FENDDATE',   MyDateTo(ADateTo));
  odsCompPQ.Open;
  odsCompPQ.Refresh;
end; // CompareJobInit

// MR:01-03-2004
// Check if there are jobs that must be compared with the production
// of other jobs.
// If a job has 'COMPARATION_JOB_YN' = 'Y', the table 'COMPAREJOB' should be
// checked.
function TProductionScreenDM.CompareJob(APlantCode, AWorkspotCode: String;
  ADateFrom, ADateTo: TDateTime;
  var AMaxDeviation: Double;
  var AComparisonReject: Boolean;
  var AJobCode: String; var AJobPieces: Double;
  var ASumJobPieces: Double;
  var AEmpsScanIn: Boolean): Boolean;
begin
  Result := False;

  AMaxDeviation := 0;
  AComparisonReject := False;
  AJobPieces := 0;
  ASumJobPieces := 0;
  AEmpsScanIn := False;

  if odsCompPQ.IsEmpty then
    Exit;

  // For one workspot, find the jobs that need comparing.
  odsCompProduction.Close;
  odsCompProduction.ClearVariables;
  odsCompProduction.SetVariable('PLANT_CODE',    APlantCode);
  odsCompProduction.SetVariable('WORKSPOT_CODE', AWorkspotCode);
  odsCompProduction.SetVariable('DATEFROM',      ADateFrom);
  odsCompProduction.SetVariable('DATETO',        MyDateTo(ADateTo));
  odsCompProduction.Open;
  odsCompProduction.Refresh;
  if not odsCompProduction.IsEmpty then
  begin
    odsCompProduction.First;
    while not odsCompProduction.Eof do
    begin
      AJobCode :=
        odsCompProduction.FieldByName('JOB_CODE').AsString;
      AJobPieces :=
        odsCompProduction.FieldByName('PIECES').AsFloat;
      // Now find the 'compare-workspots + jobs' that are defined for the
      // found workspot+job.
      odsCompJob.Close;
      odsCompJob.Filter :=
        '(PLANT_CODE=' +
        '''' + odsCompProduction.FieldByName('PLANT_CODE').AsString + '''' +
        ') AND (WORKSPOT_CODE=' +
        '''' + odsCompProduction.FieldByName('WORKSPOT_CODE').AsString + '''' +
        ') AND (JOB_CODE=' +
        '''' + odsCompProduction.FieldByName('JOB_CODE').AsString + '''' + ')';
      odsCompJob.Filter := UpperCase(odsCompJob.Filter);
      odsCompJob.Filtered := True;
      odsCompJob.Open;
      odsCompJob.Refresh;
      if not odsCompJob.IsEmpty then
      begin
        odsCompJob.First;
        try
          AMaxDeviation :=
            odsCompJob.FieldByName('MAXIMUM_DEVIATION').AsFloat;
          AComparisonReject :=
            (odsCompJob.FieldByName('COMPARISON_REJECT_YN').AsString = 'Y');
          AEmpsScanIn :=
            (odsCompJob.FieldByName('EMPS_SCAN_IN_YN').AsString = 'Y');
        except
          AMaxDeviation := 0;
          AComparisonReject := False;
          AEmpsScanIn := False;
        end;
        while not odsCompJob.Eof do
        begin
          // Now find the quantities for this compare-workspot + job
          // and summaries them.
          if odsCompPQ.Locate('PLANT_CODE;WORKSPOT_CODE;JOB_CODE',
            VarArrayOf([
              odsCompJob.FieldByName('PLANT_CODE').AsString,
              odsCompJob.FieldByName('COMPARE_WORKSPOT_CODE').AsString,
              odsCompJob.FieldByName('COMPARE_JOB_CODE').AsString]), []) then
          begin
            Result := True;
            ASumJobPieces := ASumJobPieces +
              odsCompPQ.FieldByName('QUANTITY').AsFloat;
          end;
          odsCompJob.Next;
        end;
      end;
      odsCompProduction.Next;
    end;
  end;
end; // CompareJob

// Machine/Workspot - Get employee pieces.
(*
function TProductionScreenDM.EmployeePieces(APlantCode,
  AWorkspotCode: String; AEmployeeNumber, AShiftNumber: Integer; ADateFrom,
  ADateTo: TDateTime;
  AEfficiencyPeriodMode: TEffPeriodMode): Integer;
var
  LastDate: TDateTime;
begin
  Result := 0;
  with oqPQTREmpPieces do
  begin
    ClearVariables;
    SetVariable('PLANT_CODE',      APlantCode);
    SetVariable('WORKSPOT_CODE',   AWorkspotCode);
    SetVariable('JOB_CODE',        NOJOB); // SO-20013516
    SetVariable('EMPLOYEE_NUMBER', AEmployeeNumber);
    if AEfficiencyPeriodMode = epShift then
      SetVariable('SHIFT_NUMBER',  AShiftNumber)
    else
      SetVariable('SHIFT_NUMBER',  -1);
    SetVariable('DATEFROM',        ADateFrom);
    SetVariable('DATETO',          ADateTo);
    Execute;
    if not Eof then
    begin
      case AEfficiencyPeriodMode of
      epSince, epShift:
        begin
          LastDate := 0;
          while not Eof do
          begin
            if LastDate <> FieldAsDate('END_DATE') then
              Result := Result +
                FieldAsInteger('PIECES');
            LastDate := FieldAsDate('END_DATE');
            Next;
          end;
        end;
      epCurrent:
        begin
          // Only take first value (latest in time).
          Result := FieldAsInteger('PIECES');
        end;
      end;
    end;
  end;
end; // EmployeePieces
*)
// Determine the actual Shift based on TR-records.
// If multiple shifts found, then return -1.
function TProductionScreenDM.DetermineActualShift(APlantCode,
  AWorkspotCode: String; ADateFrom, ADateTo: TDateTime;
  var AShiftStart, AShiftEnd: TDateTime): Integer;
var
  HalfADayBefore: TDateTime;
  ShiftDayRecord: TShiftDayRecord;
  DateIn: TDateTime;
  ThisShift, PrevShift: Integer;
  MultipleShifts: Boolean;
  OldShiftStart: TDateTime;
begin
// TD-24852 Added some debug-info
{$IFDEF DEBUG}
WLog('DetermineActualShift-START');
WLog('Plant=' + APlantCode + ' Workspot=' + AWorkspotCode);
{$ENDIF}
  Result := -1;
  AShiftStart := ADateFrom;
  AShiftEnd := ADateTo;
  MultipleShifts := False;

  // RV063.6. Also get dept. + empl.
  with odsGetTRShift do
  begin
    HalfADayBefore := ADateTo - 0.5;
{$IFDEF DEBUG}
WLog('HalfADayBefore=' + DateTimeToStr(HalfADayBefore) + ' DateFrom=' + DateTimeToStr(ADateFrom) + ' DateTo=' + DateTimeToStr(ADateTo));
{$ENDIF}
    Close;
    ClearVariables;
    SetVariable('PLANT_CODE',      APlantCode);
    SetVariable('WORKSPOT_CODE',   AWorkspotCode);
    SetVariable('DATEFROM',        ADateFrom);
    SetVariable('DATETO',          ADateTo);
    SetVariable('HALFADAY_BEFORE', HalfADayBefore);
    Open;
    Refresh;
    if not Eof then
    begin
      PrevShift := -1;
      while not Eof do
      begin
        ThisShift := FieldByName('SHIFT_NUMBER').AsInteger;
{$IFDEF DEBUG}
WLog('ThisShift=' + IntToStr(ThisShift));
{$ENDIF}
        if (PrevShift <> -1) then
          if PrevShift <> ThisShift then
          begin
            MultipleShifts := True;
            Break;
          end;
        PrevShift := ThisShift;
        Next;
      end;
      if not MultipleShifts then
      begin
        First;
        Result := FieldByName('SHIFT_NUMBER').AsInteger;
        ShiftDayRecord.ADate := Trunc(ADateTo);
        ShiftDayRecord.APlantCode := APlantCode;
        ShiftDayRecord.AShiftNumber := Result;
        // RV063.6. Also get dept + empl. for Timeblocks-per-dept and
        // Timeblocks-per-empl during determination of the start/end-times.
        AShiftDay.AEmployeeNumber := FieldByName('EMPLOYEE_NUMBER').AsInteger;
        AShiftDay.ADepartmentCode := FieldByName('DEPARTMENT_CODE').AsString;
        DateIn := FieldByName('DATETIME_IN').AsDateTime;
        // Note: The DateOut is not known yet!
        AShiftDay.DetermineShiftStartEnd(ShiftDayRecord);
        if ShiftDayRecord.AValid then
        begin
          AShiftStart := ShiftDayRecord.AStart;
          AShiftEnd := ShiftDayRecord.AEnd;
          // Correct the shiftstart-time based on scans for that shift.
          // Now look again for employees that were scanned in during that shift.
          // And use the lowest value found for the Shift-start.
          with odsGetTRShift2 do
          begin
            HalfADayBefore := ADateTo - 0.5;
            ADateFrom := AShiftStart - 1/24; // 1 hour earlier
            Close;
            ClearVariables;
            SetVariable('PLANT_CODE',      APlantCode);
            SetVariable('WORKSPOT_CODE',   AWorkspotCode);
            SetVariable('SHIFT_NUMBER',    ShiftDayRecord.AShiftNumber);
            SetVariable('DATEFROM',        ADateFrom);
            SetVariable('DATETO',          ADateTo);
            SetVariable('HALFADAY_BEFORE', HalfADayBefore);
            Open;
            Refresh;
            OldShiftStart := AShiftStart;
            try
              if not Eof then
                if FieldByName('DATETIME_IN').AsString <> '' then
                  AShiftStart := FieldByName('DATETIME_IN').AsDateTime;
            except
              AShiftStart := OldShiftStart;
            end;
          end;
          // RV063.6. Show shift, also if it is not started yet, the
          //          employee can be started earlier.
          //          When started later: the query only looks at the
          //          current moment! When using 'datein' you do not
          //          know the real shift-start anymore.
          // Is shift not started yet? (ADateTo is Current Time)
          if AShiftStart > ADateTo then
            AShiftStart := DateIn;
          // Is shift not ended yet?
          if AShiftEnd < ADateTo then
            AShiftEnd := ADateTo;
  {          Result := -1; }
        end
        else
          Result := -1;
      end; // if not MultipleShifts then
    end; // if not Eof then
  end; // with odsGetTRShift do
{$IFDEF DEBUG}
WLog('ShiftStart=' + DateTimeToStr(AShiftStart));
WLog('DetermineActualShift-END');
{$ENDIF}
end;

function TProductionScreenDM.DetermineTRJobPrevPQPeriod(APlantCode,
  AWorkspotCode, AJobCode: String; ADateFrom, ADateTo: TDateTime): String;
var
  HalfADayBefore: TDateTime;
begin
  Result := '';
  with oqGetTRJobPrevPQPeriod do
  begin
    HalfADayBefore := ADateTo - 0.5;
    ClearVariables;
    SetVariable('PLANT_CODE',      APlantCode);
    SetVariable('WORKSPOT_CODE',   AWorkspotCode);
    SetVariable('JOB_CODE',        AJobCode);
    SetVariable('DATEFROM',        ADateFrom);
    SetVariable('DATETO',          ADateTo);
    SetVariable('HALFADAY_BEFORE', HalfADayBefore);
    Execute;
    if not Eof then
    begin
      Result := FieldAsString('JOB_CODE');
    end;
  end;
end;

// RV064.1.
// Check for scans that interfere with each other.
// Log results to a file.
procedure TProductionScreenDM.CheckInterference;
var
  FoundCount: Integer;
  Emp1, Emp2: Integer;
  Plant: String;
  DateIn1, DateOut1: TDateTime;
  DateIn2: TDateTime;
  Days: Integer;
  Line: String;
begin
  FoundCount := 0;
  Days := 7;
  DialogShowStatusInfoF := TDialogShowStatusInfoF.Create(nil);
  Screen.Cursor := crHourGlass;
  try
    with oqCheckInterference do
    begin
      ClearVariables;
      SetVariable('DATEFROM', Trunc(Now-Days)); // Last n days
      Line := 'Searching scan-interference for last ' +
        IntToStr(Days) + ' days...';
      WLog(Line);
      DialogShowStatusInfoF.AddMemo(Line);
      Application.ProcessMessages;
      Execute;
      while not Eof do
      begin
        Emp1 := FieldAsInteger('EMPLOYEE_NUMBER');
        Plant := FieldAsString('PLANT_CODE');
        DateIn1 := FieldAsDate('DATETIME_IN');
        DateOut1 := FieldAsDate('DATETIME_OUT');
        Application.ProcessMessages;
        Next;
        if not Eof then
        begin
          Emp2 := FieldAsInteger('EMPLOYEE_NUMBER');
          DateIn2 := FieldAsDate('DATETIME_IN');
          if Emp2 = Emp1 then
          begin
            if DateOut1 > DateIn2 then
            begin
              inc(FoundCount);
              Line :=
                Plant + ';' +
                IntToStr(Emp1) + ';' +
                DateTimeToStr(DateIn1) + ';' +
                DateTimeToStr(DateOut1);
              WLog(Line);
              DialogShowStatusInfoF.AddMemo(Line);
            end;
          end;
        end;
      end;
    end;
    if FoundCount > 0 then
      Line := 'Found: ' + IntToStr(FoundCount)
    else
      Line := 'Nothing found.';
    Screen.Cursor := crDefault;
    WLog(Line);
    DialogShowStatusInfoF.AddMemo(Line);
    DialogShowStatusInfoF.ShowModal;
  finally
    Screen.Cursor := crDefault;
    DialogShowStatusInfoF.Free;
  end;
end;

//
function MyDateTo(ADate: TDateTime): TDateTime;
begin
  Result := IncMinute(ADate, 5);
end;

procedure TProductionScreenDM.DataModuleCreate(Sender: TObject);
begin
  // TD-22082
  ORASystemDM.PlantTeamFilterEnable(odsPlant);
end;

procedure TProductionScreenDM.odsPlantFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  // TD-22082
  Accept := ORASystemDM.PlantTeamFilter(DataSet);
end;

end.
