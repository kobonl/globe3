(*
  MRA:7-MAR-2014 TD-24046
  - Translate issues: Use a different method to translate.
    - Do NOT do this in separate files where the translation is done
      hard-coded!
    - New method:
      - Use 1 resource-strings-file that contains the translation-strings.
      - Use 1 translate-unit that assigns these strings to the properties
        in the forms.
      - In this way there is only 1 form that can be translated in multiple
        languages when the application is run.
      - The project-file defines the language using a condition.
  MRA:9-MAR-2017 PIM-275
  - Addition of dept for translation (STransDP).
*)
unit UTranslateStringsProdScreen;

interface

resourceString

{$IFDEF PIMSDUTCH}
  // HomeFRM
  STransHomeCaption = 'PIMS - Productiescherm';
  STransHR = 'hr';
  STransPCS = 'pcs.';
  STransNormPCS = 'norm pcs.';
  STransShowAllEmps = 'Toon alle werknemers';
  STransShowEmpsOnWrongWorkspot = 'Toon werknemers op verkeerde werkplek';
  STransShowEmpsNotScannedIn = 'Toon werknemers niet ingescand';
  STransShowEmpsAbsWithReason = 'Toon werknemers afwezig met reden';
  STransShowEmpsABsWithoutReason = 'Toon werknemers afwezig zonder reden';
  STransShowEmpsWithFirstAid = 'Toon werknemers met BHV';
  STransShowEmpsTooLate = 'Toon werknemers te laat';
  STransShowChart = 'Toon grafiek';
  STransShowEmps = 'Toon werknemers';
  STransWorkspot = 'Werkplek';
  STransEffMeter = 'Efficientie Meter';
  STransWorkspotBigEffMeter = 'Werkplek / Grote efficientie meter';
  STransLineHorz = 'Horizontale lijn';
  STransLineVert = 'Verticale lijn';
  STransRect = 'Rechthoek';
  STransEmpOverview = 'Werknemer Overzicht';
  STransDelete = 'Verwijder';
  STransDeleteCurrentObject = 'Verwijder huidig object';
  STransSave = 'Op&slaan';
  STransSaveScheme = 'Opslaan schema';
  STransEdit = 'Wijzig schema';
  STransNew = '&Nieuw';
  STransOpen = '&Open';
  STransSaveAs = 'Opslaan &Als';
  STransLegenda = '&Legenda';
  StransShowStatusInfo = 'Toon Status Info';
  STransCurrEff = 'Hudige Efficientie';
  STransTodaysEff = 'Efficientie van vandaag';
  // DialogChartSettings
  STransSettings = 'Instellingen';
  STransExportSettings = 'Export Instellingen';
  STransSeparator = 'Separator';
  // DialogDepartmentSelect
  STransSelectDept = 'Kies afdeling';
  STransPlant = 'Bedrijf';
  STransDept = 'Afdeling';
  // DialogImageProperties
  STransImageProperties =  'Image Properties';
  STransLeftPos = 'Left Position';
  STransTopPos = 'Top Position';
  STransWidth = 'Width';
  STransHeight = 'Height';
  STransPlantCode = 'Plant Code';
  STransWorkspotCode =  'Workspot Code';
  STransWorkspotDesc = 'Workspot Description';
  STransOK = 'OK';
  STransCancel = 'Annuleer';
  // DialogLegenda
  STransPuppets = 'Werknemers';
  STransPlanned1 = 'Gepland, maar niet ingescand (minder ingescand dan gepland)';
  STransPlanned2 = 'Gepland, ingescanned (correcte werknemer)';
  STransPlanned3 = 'Gepland, een andere werknemer met hetzelfde level ingescand';
  STransPlanned4 = 'Gepland, een andere werknemer met een ander level ingescand';
  STransNotPlanned = 'Niet gepland, ingescand (meer ingescand dan gepland)';
  STransBlinking = 'Knipperend, als er datacollectie is voor een job, maar geen werknemer hierop is ingescand';
  STransWorkspotFrame = 'Werkplek frame knippert als een van de jobs van de werkplek een vergelijkingsjob is';
  STransAndTheDev = 'en de afwijking meer is dan het vastgelegde maximum.';
  STransTrafficLight = 'Verkeerslicht';
  STransTheLastTime1 = 'De laatste keer dat er datacollectie was, was er geen data voor de werkplek';
  STransTheLastTime2 = 'De laatste keer dat er datacollectie was, was er data voor de werkplek, maar dit was 0';
  STransTheLastTime3 = 'De laatste keer dat er datacollectie was, was er data voor de werkplek (niet 0)';
  // DialogPrintChart
  STransPrintChart = 'Print Chart';
  STransPrintOptions = 'Print Opties';
  STransPrinterSetup = 'Printer Setup';
  STransPortrait = 'Portret';
  STransLandscape = 'Landschap';
  // DialogSelectMode
  STransSelectMode = 'Kies Mode';
  STransMode = 'Mode';
  STransShowAllEmployees = 'Toon alle werknemers';
  // DialogSettings
  STransGlobalSettings = 'Algemene instellingen';
  STransSoundAlarm = 'Geluidssignaal';
  STransSoundFilename = 'Bestandsnaam';
  STransUseSoundAlarm = 'Gebruik geluidsign';
  STransBrowse = 'Bladeren';
  STransRefreshTimeInterval = 'Ververs tijd interval';
  STransInterval = 'Interval';
  STransSeconds = 'secondes';
  STransSchemeSettings = 'Schema instellingen';
  STransWorkspotScale = 'Schaal werkplek';
  STransPercentage = 'Percentage';
  STransGridlineDistance = 'Gridregel Afstand';
  STransEfficiencyMeters = 'Effici雗tie meters';
  STransUsePeriod = 'Periode';
  STransCurrent = 'Huidig';
  STransSince = 'Sinds';
  STransShift = 'Ploeg';
  STransTime = 'Tijd';
  STransFontScale = 'Schaal Font';
  STransEffColorBoundaries = 'Effici雗tie Kleur Grenzen';
  STransRed = 'Rood';
  STransOrange = 'Oranje';
  // Login
  STransDatabaseLogin = 'Database Login';
  STransPassword = '&Wachtwoord';
  STransUsername = '&Gebruikersnaam';
  STransDatabasePims = 'Database:            Pims';
  // DialogShowAllEmployees
  STransEmpsOnWorkspot = 'Alle werknemers';
  STransEmployee = 'Werknemer';
  STransScanned = 'Gescand';
  STransPlanned = 'Gepland';
  STransStandAvail = 'Stand. beschikbaar';
  STransAbsWithReason = 'Afwezig met reden';
  STransFirstAid = 'E.H.B.O.';
  STransNumber = 'Nummer';
  STransName = 'Naam';
  STransShort = 'Verkort';
  STransTeam = 'Team';
  STransWS = 'WP';
  STransTimeIn = 'Tijd-in';
  STransStartTime = 'Start Tijd';
  STransEndTime = 'Eind Tijd';
  STransLevel = 'Nivo';
  STransAbsReasonCode = 'Afwezigheidscode';
  STransAbsReasonDescr = 'Afwezigheidsreden';
  STransExpDate = 'Vervaldatum';
  STransEfficiency = 'Effici雗tie';
  STransToday = 'Vandaag';
  // DialogWorkspotSelect
  STransWorkspotSelect = 'Werkplek Selectie';
  STransPicture = 'Afbeelding';
  STransMachine = 'Machine';
  STransTypeOfProdScreen = 'Type';
  STransProdBarsWithoutTR = 'Werkplek';
  STransProdBarsTRMachineLevel = 'Productiescherm / Tijdregistratie gecombineerd (Machine niveau)';
  STransProdBarsTRWorskpotLevel = 'Productiescherm / Tijdregistratie gecombineerd (Werkplek niveau)';
  // ProductionScreenWait
  STransProdScreen = 'PIMS Productiescherm';
  // ShowChart
  STransGraph = 'Grafiek';
  STransStats = 'Statistieken';
  STransTimeInt = 'Tijdsinterv:';
  STransTime2 = 'Tijd:';
  STransProdHr = 'Prod./Uur:';
  STransProdNorm = 'Prod. norm:';
  STransNrOfEmp =  'Werkn.:';
  STransShowEmp = 'Toon werkn.';
  STransScale = 'Schaal';
  STransDateInput = 'Datum sel.';
  STransFromDate = 'Van Datum:';
  STransTo = 'tot';
  STransAccept = 'Accepteer';
  STransEnterDates = 'Enter Dates';
  STransEmployees = 'Werknemers';
  STransEmpNr = 'Nummer';
  STransEmpName = 'Naam';
  STransJobCode = 'Job code';
  STransJobName = 'Job naam';
  STransShiftNr = 'Ploeg';
  STransDateTimeIn = 'Tijd In';
  STransProdOutput = 'Productie';
  STransTotalProd = 'Totaal Productie';
  STransEC = 'EC';
  STransBackground = 'Background';
  STransNrOfEmp100 = 'Aant. werkn. (x100)';
  STransNormProdLevel = 'Productienorm';
  STransOutput = 'Uitvoer';
  STransCloseGraph = 'Sluit Grafiek';
  STransExportChart = 'Export Grafiek';
  // BaseTopMenuFRM
  STransAboutABSGroup = 'Over Gotli Labs';
  STransABSGroupHomePage = 'Gotli Labs Home Pagina';
  STransOrderInfo = 'Bestel Informatie';
  STransPimsHomePage = 'Globe Home Pagina';
  STransContents = 'Inhoud';
  STransIndex = 'Index';
  STransExit1 = 'E&nde';
  STransPrint = 'Print';
  STransExit2 = 'Einde';
  STransFile = 'Bestand';
  STransHelp = 'Help';
  STransShow = 'Toon';
  STransEmpInfo = 'Werkn. Info';
  STransEmpEff = 'Werkn. Effici雗tie';
  STransDP = 'Afd.';
{$ELSE}
  {$IFDEF PIMSGERMAN}
  // HomeFRM
  STransHomeCaption = 'PIMS - Productionsschirm';
  STransHR = 'hr';
  STransPCS = 'pcs.';
  STransNormPCS = 'norm pcs.';
  STransShowAllEmps = 'Zeig alle Mitarbeiter';
  STransShowEmpsOnWrongWorkspot = 'Zeig Mitarbeiter am falschen Arbeitzplatz';
  STransShowEmpsNotScannedIn = 'Zeig nicht angemeldete Mitarbeiter';
  STransShowEmpsAbsWithReason = 'Zeig Mitarbeiter abwesend mit Grund';
  STransShowEmpsABsWithoutReason = 'Zeig Mitarbeiter abwesend ohne Grund';
  STransShowEmpsWithFirstAid = 'Zeig Mitarbeiter mit Erste Hilfe';
  STransShowEmpsTooLate = 'Zeig Mitabeiter zu Sp鋞';
  STransShowChart = 'Zeig Graph';
  STransShowEmps = 'Zeig Mitarbeiter';
  STransWorkspot = 'Arbeitsplatz';
  STransEffMeter = 'Effizienz Messer';
  STransWorkspotBigEffMeter = 'Arbeitsplatz / Effizienz Messer';
  STransLineHorz = 'Linie Horizontal';
  STransLineVert = 'Linie Vertikal';
  STransRect = 'Rechteck';
  STransEmpOverview = 'Mitarbeiter 躡ersicht';
  STransDelete = 'Entfern';
  STransDeleteCurrentObject = 'Entfern heutig Object';
  STransSave = 'Speichern';
  STransSaveScheme = 'Speichern Schema';
  STransEdit = '膎dern';
  STransNew = 'Neu';
  STransOpen = '&謋fnen';
  STransSaveAs = 'Speichern &Als';
  STransLegenda = 'Legenda';
  StransShowStatusInfo = 'Zeig Status Info';
  STransCurrEff = 'Aktuelle Effizienz';
  STransTodaysEff = 'Heute Effizienz';
  // DialogChartSettings
  STransSettings = 'Einstellungen';
  STransExportSettings = 'Export Einstellungen';
  STransSeparator = 'Separierer';
  // DialogDepartmentSelect
  STransSelectDept = 'Selektier Abteilung';
  STransPlant = 'Betrieb';
  STransDept = 'Abteilung';
  // DialogImageProperties
  STransImageProperties =  'Image Properties';
  STransLeftPos = 'Left Position';
  STransTopPos = 'Top Position';
  STransWidth = 'Width';
  STransHeight = 'Height';
  STransPlantCode = 'Plant Code';
  STransWorkspotCode =  'Workspot Code';
  STransWorkspotDesc = 'Workspot Description';
  STransOK = 'OK';
  STransCancel = 'Annulier';
  // DialogLegenda
  STransPuppets = 'Mitarbeiter';
  STransPlanned1 = 'Geplant, aber nicht eingescant (weniger eingescant als geplant)';
  STransPlanned2 = 'Geplant, eingescant (korrekte Mitarbeiter)';
  STransPlanned3 = 'Geplant, andere Mitarbeiter mit dasselbe Nivo eingescant';
  STransPlanned4 = 'Geplannt, andere Mitarbeiter mit anderes Nivo eingescant';
  STransNotPlanned = 'Nicht geplant, eingescant (mehr eingescant dann geplant)';
  STransBlinking = 'Blinkt, wenn es Datacollection gibt am Job aber es gibt kein eingescanter Mitarbeiter';
  STransWorkspotFrame = 'Arbeitsplatz Frame blinkt wenn einer von der Jobs auf der Arbeitsplatz ein Vergleichungsjob ist';
  STransAndTheDev = 'und der Deviation mehr ist als definiertes Maximum.';
  STransTrafficLight = 'Verkehrsampel';
  STransTheLastTime1 = 'Der letzte Zeit von Datacollection, es gab kein Data auf der Arbeitsplatz';
  STransTheLastTime2 = 'Der letzte Zeit von Datacollection, es gab Data f黵 der Arbeitsplatz aber es war 0';
  STransTheLastTime3 = 'Der letzte Zeit von Datacollection, es gab Data f黵 der Arbeitsplatz (nicht 0)';
  // DialogPrintChart
  STransPrintChart = 'Drucken Graph';
  STransPrintOptions = 'Drucker Optionen';
  STransPrinterSetup = 'Drucker Setup';
  STransPortrait = 'Portrait';
  STransLandscape = 'Landschaft';
  // DialogSelectMode
  STransSelectMode = 'Selektier Mode';
  STransMode = 'Mode';
  STransShowAllEmployees = 'Zeig alle Mitarbeiter';
  // DialogSettings
  STransGlobalSettings = 'Globale Einstellungen';
  STransSoundAlarm = 'Signal';
  STransSoundFilename = 'Signal Dateiname';
  STransUseSoundAlarm = 'Verwendung Signal';
  STransBrowse = 'Browser';
  STransRefreshTimeInterval = 'Zeit Interval Refresh';
  STransInterval = 'Interval';
  STransSeconds = 'Sekundes';
  STransSchemeSettings = 'Schema Einstellungen';
  STransWorkspotScale = 'Arbeitsplatz Skala';
  STransPercentage = 'Prozentual';
  STransGridlineDistance = 'Gridlinie Abstand';
  STransEfficiencyMeters = 'Effizienz Messer';
  STransUsePeriod = 'Periode';
  STransCurrent =  'Heute';
  STransSince = 'Seit';
  STransShift = 'Schicht';
  STransTime = 'Zeit';
  STransFontScale = 'Font Skala';
  STransEffColorBoundaries = 'Effizienz Farbgrenzen ';
  STransRed = 'Rot';
  STransOrange = 'Orange';
  // Login
  STransDatabaseLogin = 'Database Login';
  STransPassword = '&Kennwort';
  STransUsername = '&Benutzername';
  STransDatabasePims = 'Database:            Pims';
  // DialogShowAllEmployees
  STransEmpsOnWorkspot = 'Mitarbeiter auf Arbeitsplatz';
  STransEmployee = 'Mitarbeiter';
  STransScanned = 'Gescannt';
  STransPlanned = 'Geplannt';
  STransStandAvail = 'Stand. Verf黦bar';
  STransAbsWithReason = 'Abwesend mit Grund';
  STransFirstAid = 'Erste Hilfe';
  STransNumber = 'Nummer';
  STransName = 'Name';
  STransShort = 'Kurz';
  STransTeam = 'Team';
  STransWS = 'AP';
  STransTimeIn = 'Zeit-In';
  STransStartTime = 'Start Zeit';
  STransEndTime = 'Ende Zeit';
  STransLevel = 'Level';
  STransAbsReasonCode = 'Kode Abwesenheitsgrund';
  STransAbsReasonDescr = 'Abwesenheitsgrund Beschreibung';
  STransExpDate = 'Verfalldatum';
  STransEfficiency = 'Effizienz';
  STransToday = 'Heute';
  // DialogWorkspotSelect
  STransWorkspotSelect = 'Workspot Select';
  STransPicture = 'Picture';
  STransMachine = 'Machine';
  STransTypeOfProdScreen = 'Type';
  STransProdBarsWithoutTR = 'Arbeitsplatz';
  STransProdBarsTRMachineLevel = 'Production bars / Time recording combined (Machine level)';
  STransProdBarsTRWorskpotLevel = 'Production bars / Time recording combined (Workspot level)';
  // ProductionScreenWait
  STransProdScreen = 'PIMS Production Screen';
  // ShowChart
  STransGraph = 'Graph';
  STransStats = 'Statistiken';
  STransTimeInt = 'Zeit Int.:';
  STransTime2 = 'Zeit:';
  STransProdHr = 'Prod./St:';
  STransProdNorm = 'Prod. Norm:';
  STransNrOfEmp =  'Anz. Mitarb.:';
  STransShowEmp = 'Zeig Mitarb.';
  STransScale = 'Skale';
  STransDateInput = 'Datum';
  STransFromDate = 'von Datum:';
  STransTo = 'bis';
  STransAccept = 'Akzeptiere';
  STransEnterDates = 'Gib Datum';
  STransEmployees = 'Mitarbeiter';
  STransEmpNr = 'Nummer';
  STransEmpName = 'Name';
  STransJobCode = 'Jcbcode';
  STransJobName = 'Jobname';
  STransShiftNr = 'Schicht';
  STransDateTimeIn = 'Zeit In';
  STransProdOutput = 'Produktion Ergebnis';
  STransTotalProd = 'Total Produktion';
  STransEC = 'MA';
  STransBackground = 'Hintergrund';
  STransNrOfEmp100 = 'Anz. Mitarb. (x100)';
  STransNormProdLevel = 'Norm Prod. Nivo';
  STransOutput = 'Ergebnis';
  STransCloseGraph = 'Schlie?Graph';
  STransExportChart = 'Export Graphik';
  // BaseTopMenuFRM
  STransAboutABSGroup = '躡er Gotli Labs';
  STransABSGroupHomePage = 'Gotli Labs Home Page';
  STransOrderInfo = 'Bestell Information';
  STransPimsHomePage = 'Globe Home Page';
  STransContents = 'Inhalt';
  STransIndex = 'Index';
  STransExit1 = 'E&nde';
  STransPrint = 'Drucken';
  STransExit2 = 'Ende';
  STransFile = 'Datei';
  STransHelp = 'Hilfe';
  STransShow = 'Zeig';
  STransEmpInfo = 'Mitarb. Info';
  STransEmpEff = 'Mitarb. Eff.';
  STransDP = 'Abt.';
{$ELSE}
  {$IFDEF PIMSDANISH}
  // HomeFRM
  STransHomeCaption = 'PIMS - Production Screen';
  STransHR = 'hr';
  STransPCS = 'stk.';
  STransNormPCS = 'norm stk.';
  STransShowAllEmps = 'Vis alle medarb.';
  STransShowEmpsOnWrongWorkspot = 'Vis medarb. p?forkert arb.sted';
  STransShowEmpsNotScannedIn = 'Vis ikke ind-skannede medarb.';
  STransShowEmpsAbsWithReason = 'Vis medarb. frav鎟 med 錼sag';
  STransShowEmpsABsWithoutReason = 'Vis medarb. frav鎟 uden 錼sag';
  STransShowEmpsWithFirstAid = 'Vis medarb. med f鴕stehj鎙p';
  STransShowEmpsTooLate = 'Vis medarb. for sent';
  STransShowChart = 'Vis graf';
  STransShowEmps = 'Vis medarb.';
  STransWorkspot = 'Arb.sted';
  STransEffMeter = 'Effektm錶er';
  STransWorkspotBigEffMeter = 'Arb.sted / Stor effektm錶er';
  STransLineHorz = 'Vandret linie';
  STransLineVert = 'Lodret linie';
  STransRect = 'Rektangel';
  STransEmpOverview = 'Medarb. oversigt';
  STransDelete = 'Slet';
  STransDeleteCurrentObject = 'Slet dette objekt';
  STransSave = 'Gem';
  STransSaveScheme = 'Gem skema';
  STransEdit = 'Ret';
  STransNew = 'Ny';
  STransOpen = '舃n';
  STransSaveAs = 'Gem som';
  STransLegenda = 'Legenda';
  StransShowStatusInfo = 'Vis Status Info';
  STransCurrEff = 'Current Efficiency';
  STransTodaysEff = 'Today''s Efficiency';
  // DialogChartSettings
  STransSettings = 'Ops鎡n.';
  STransExportSettings = 'Eksport ops鎡ning';
  STransSeparator = 'Separator';
  // DialogDepartmentSelect
  STransSelectDept = 'V鎙g afdeling';
  STransPlant = 'Vask.';
  STransDept = 'Afdeling';
  // DialogImageProperties
  STransImageProperties =  'Billedegenskaber';
  STransLeftPos = 'Venstre pos.';
  STransTopPos = 'Top pos.';
  STransWidth = 'Widde';
  STransHeight = 'H鴍de';
  STransPlantCode = 'Vask. kode';
  STransWorkspotCode =  'Arb.sted kode';
  STransWorkspotDesc = 'Arb.sted beskr.';
  STransOK = 'OK';
  STransCancel = 'Annul.';
  // DialogLegenda
  STransPuppets = 'Puppets';
  STransPlanned1 = 'Planl. men ikke skannet ind (mindre indskannet end planl.)';
  STransPlanned2 = 'Planl. skannet ind (korrekt medarb.)';
  STransPlanned3 = 'Planl. anden medarb. med samme niveau skannet ind';
  STransPlanned4 = 'Planl. anden medarb. med andet niveay skannet ind';
  STransNotPlanned = 'Ikke planl. skannet ind(mere indskannet end planl.)';
  STransBlinking = 'Blinker n錼 der hentes data til et job, hvortil der ikke er skannet nogen medarb.';
  STransWorkspotFrame = 'Arb.sted ramme blinker, hvis et af jobbene er et sammenligningsjob';
  STransAndTheDev = 'og afvigelsen er mere end defineret maksimum.';
  STransTrafficLight = 'Trafik lys';
  STransTheLastTime1 = 'Sidst der blev hentet data, var der ingen data til dette arb.sted.';
  STransTheLastTime2 = 'Sidst der blev hentet data, var der der data til dette arb.sted, men det var 0.';
  STransTheLastTime3 = 'Sidst der blev hentet data, var der data til dette arb.sted (ikke 0)';
  // DialogPrintChart
  STransPrintChart = 'Udskriv graf';
  STransPrintOptions = 'Print Options';
  STransPrinterSetup = 'Printer Setup';
  STransPortrait = 'Portr鎡';
  STransLandscape = 'Landskab';
  // DialogSelectMode
  STransSelectMode = 'V鎙g Mode';
  STransMode = 'Mode';
  STransShowAllEmployees = 'Vis alle medarb.';
  // DialogSettings
  STransGlobalSettings = 'Global ops鎡n.';
  STransSoundAlarm = 'Alarmlyd';
  STransSoundFilename = 'Lyd filnavn';
  STransUseSoundAlarm = 'Brug alarlmlyd';
  STransBrowse = 'Browse';
  STransRefreshTimeInterval = 'Genopfrisk tidsinterv';
  STransInterval = 'Interval';
  STransSeconds = 'sek.';
  STransSchemeSettings = 'Skema ops鎡n.';
  STransWorkspotScale = 'Arb.sted skala';
  STransPercentage = 'Procent';
  STransGridlineDistance = 'Gridlinie afstand';
  STransEfficiencyMeters = 'Effektm錶ere';
  STransUsePeriod = 'Brug per.';
  STransCurrent =  'Nuv.';
  STransSince = 'Siden';
  STransShift = 'Skift';
  STransTime = 'Tid';
  STransFontScale = 'Font Skala';
  STransEffColorBoundaries = 'Effektivitet farvegr鎛ser';
  STransRed = 'R鴇';
  STransOrange = 'Orange';
  // Login
  STransDatabaseLogin = 'Database logind';
  STransPassword = '&Adgangskode';
  STransUsername = '&Brugernavn';
  STransDatabasePims = 'Database:            Pims';
  // DialogShowAllEmployees
  STransEmpsOnWorkspot = 'Medarb. p?arb.sted';
  STransEmployee = 'Medarb.';
  STransScanned = 'Skannet';
  STransPlanned = 'Planl.';
  STransStandAvail = 'Stand. ledig';
  STransAbsWithReason = 'Frav鎟 med 錼sag';
  STransFirstAid = 'F鴕stehj.';
  STransNumber = 'Nummer';
  STransName = 'Navn';
  STransShort = 'Kort';
  STransTeam = 'Team';
  STransWS = 'AS';
  STransTimeIn = 'Tid-ind';
  STransStartTime = 'Start Tid';
  STransEndTime = 'Slut tid';
  STransLevel = 'Niv.';
  STransAbsReasonCode = 'Frav鎟 錼sag kode';
  STransAbsReasonDescr = 'Frav鎟 錼sag beskr.';
  STransExpDate = 'Udl鴅sdato';
  STransEfficiency = 'Effektivitet';
  STransToday = 'I dag';
  // DialogWorkspotSelect
  STransWorkspotSelect = 'Arb.sted Valg';
  STransPicture = 'Billede';
  STransMachine = 'Maskine';
  STransTypeOfProdScreen = 'Type';
  STransProdBarsWithoutTR = 'Arb.sted';
  STransProdBarsTRMachineLevel = 'Produktion barer/komb. tidsregistrering (Maskine niv.)';
  STransProdBarsTRWorskpotLevel = 'Produktion barer/komb. tidsregistrering (Arb.sted niv.)';
  // ProductionScreenWait
  STransProdScreen = 'PIMS Produktion Sk鎟m';
  // ShowChart
  STransGraph = 'Graf';
  STransStats = 'Statistik';
  STransTimeInt = 'Tid Int.:';
  STransTime2 = 'Tid:';
  STransProdHr = 'Prod./Tim';
  STransProdNorm = 'Prod. Norm:';
  STransNrOfEmp =  'Ant. medarb:';
  STransShowEmp = 'Vis medarb';
  STransScale = 'Skala';
  STransDateInput = 'Dato Input';
  STransFromDate = 'Fra Dato:';
  STransTo = 'to';
  STransAccept = 'Accept';
  STransEnterDates = 'Indt. dato';
  STransEmployees = 'Medarb.';
  STransEmpNr = 'EMPL NR';
  STransEmpName = 'EMPL NAvn';
  STransJobCode = 'JOB KODE';
  STransJobName = 'JOB NAVN';
  STransShiftNr = 'SkIFT NR';
  STransDateTimeIn = 'DATOTID IND';
  STransProdOutput = 'Produktion Output';
  STransTotalProd = 'Total Produktion';
  STransEC = 'EC';
  STransBackground = 'Baggrund';
  STransNrOfEmp100 = 'Nr of Empl. (x100)';
  STransNormProdLevel = 'Norm Prod. Level';
  STransOutput = 'Output';
  STransCloseGraph = 'Close Graph';
  STransExportChart = 'Export Graf';
  // BaseTopMenuFRM
  STransAboutABSGroup = 'Om Gotli Labs';
  STransABSGroupHomePage = 'Gotli Labs Home Page';
  STransOrderInfo = 'Bestill. Information';
  STransPimsHomePage = 'Globe Home Page';
  STransContents = 'Indhold';
  STransIndex = 'Index';
  STransExit1 = 'E&xit';
  STransPrint = 'Print';
  STransExit2 = 'Exit';
  STransFile = 'Fil';
  STransHelp = 'Help';
  STransShow = 'Vis';
  STransEmpInfo = 'Medarb. Info';
  STransEmpEff = 'Medarb. Effektivitet';
  STransDP = 'Afd.';
{$ELSE}
  {$IFDEF PIMSJAPANESE}
  // HomeFRM
  STransHomeCaption = 'PIMS - Production Screen';
  STransHR = 'hr';
  STransPCS = 'pcs.';
  STransNormPCS = 'norm pcs.';
  STransShowAllEmps = 'Show All Employees';
  STransShowEmpsOnWrongWorkspot = 'Show Employees on wrong workspot';
  STransShowEmpsNotScannedIn = 'Show Employees not scanned in';
  STransShowEmpsAbsWithReason = 'Show Employees absent with reason';
  STransShowEmpsABsWithoutReason = 'Show Employees absent without reason';
  STransShowEmpsWithFirstAid = 'Show Employees with First Aid';
  STransShowEmpsTooLate = 'Show Employees too late';
  STransShowChart = 'Show Chart';
  STransShowEmps = 'Show Employees';
  STransWorkspot = 'Workspot';
  STransEffMeter = 'Efficiency Meter';
  STransWorkspotBigEffMeter = 'Workspot / Big Efficiency Meter';
  STransLineHorz = 'Line Horizontal';
  STransLineVert = 'Line Vertical';
  STransRect = 'Rectangle';
  STransEmpOverview = 'Employee Overview';
  STransDelete = 'Delete';
  STransDeleteCurrentObject = 'Delete current object';
  STransSave = 'Save';
  STransSaveScheme = 'Save Scheme';
  STransEdit = 'Edit';
  STransNew = 'New';
  STransOpen = 'Open';
  STransSaveAs = 'Save As';
  STransLegenda = 'Legenda';
  STransShowStatusInfo = 'Show Status Info';
  STransCurrEff = 'Current Efficiency';
  STransTodaysEff = 'Today''s Efficiency';
  // DialogChartSettings
  STransSettings = 'Settings';
  STransExportSettings = 'Export Settings';
  STransSeparator = 'Separator';
  // DialogDepartmentSelect
  STransSelectDept = 'Select Department';
  STransPlant = 'Plant';
  STransDept = 'Department';
  // DialogImageProperties
  STransImageProperties =  'Image Properties';
  STransLeftPos = 'Left Position';
  STransTopPos = 'Top Position';
  STransWidth = 'Width';
  STransHeight = 'Height';
  STransPlantCode = 'Plant Code';
  STransWorkspotCode =  'Workspot Code';
  STransWorkspotDesc = 'Workspot Description';
  STransOK = 'OK';
  STransCancel = 'Cancel';
  // DialogLegenda
  STransPuppets = 'Puppets';
  STransPlanned1 = 'Planned, but not scanned in (less scanned in than planned)';
  STransPlanned2 = 'Planned, scanned in (correct employee)';
  STransPlanned3 = 'Planned, other employee with same level scanned in';
  STransPlanned4 = 'Planned, other employee with other level scanned in';
  STransNotPlanned = 'Not planned, scanned in (more scanned in than planned)';
  STransBlinking = 'Blinking, when there is datacollection on a job without any employee scanned on that job';
  STransWorkspotFrame = 'Workspot frame blinks when one of the jobs of the workspot is a comparison job';
  STransAndTheDev = 'and the deviation is more than the defined maximum.';
  STransTrafficLight = 'Traffic light';
  STransTheLastTime1 = 'The last time there was datacollection, there was no data for the workspot';
  STransTheLastTime2 = 'The last time there was datacollection, there was data for the workspot but it was 0';
  STransTheLastTime3 = 'The last time there was datacollection, there was data for the workspot (not 0)';
  // DialogPrintChart
  STransPrintChart = 'Print Chart';
  STransPrintOptions = 'Print Options';
  STransPrinterSetup = 'Printer Setup';
  STransPortrait = 'Portrait';
  STransLandscape = 'Landscape';
  // DialogSelectMode
  STransSelectMode = 'Select Mode';
  STransMode = 'Mode';
  STransShowAllEmployees = 'Show All Employees';
  // DialogSettings
  STransGlobalSettings = 'Global Settings';
  STransSoundAlarm = 'Sound Alarm';
  STransSoundFilename = 'Sound Filename';
  STransUseSoundAlarm = 'Use Sound Alarm';
  STransBrowse = 'Browse';
  STransRefreshTimeInterval = 'Refresh Time Interval';
  STransInterval = 'Interval';
  STransSeconds = 'seconds';
  STransSchemeSettings = 'Scheme Settings';
  STransWorkspotScale = 'Workspot Scale';
  STransPercentage = 'Percentage';
  STransGridlineDistance = 'Gridline Distance';
  STransEfficiencyMeters = 'Efficiency Meters';
  STransUsePeriod = 'Use Period';
  STransCurrent =  'Current';
  STransSince = 'Since';
  STransShift = 'Shift';
  STransTime = 'Time';
  STransFontScale = 'Font Scale';
  STransEffColorBoundaries = 'Efficiency Color Boundaries';
  STransRed = 'Red';
  STransOrange = 'Orange';
  // Login
  STransDatabaseLogin = 'Database Login';
  STransPassword = '&Password';
  STransUsername = '&User name';
  STransDatabasePims = 'Database:            Pims';
  // DialogShowAllEmployees
  STransEmpsOnWorkspot = 'Employees on Workspot';
  STransEmployee = 'Employee';
  STransScanned = 'Scanned';
  STransPlanned = 'Planned';
  STransStandAvail = 'Stand. Available';
  STransAbsWithReason = 'Absent with reason';
  STransFirstAid = 'First Aid';
  STransNumber = 'Number';
  STransName = 'Name';
  STransShort = 'Short';
  STransTeam = 'Team';
  STransWS = 'WS';
  STransTimeIn = 'Time-in';
  STransStartTime = 'Start Time';
  STransEndTime = 'End Time';
  STransLevel = 'Level';
  STransAbsReasonCode = 'Absence Reason Code';
  STransAbsReasonDescr = 'Absence Reason Descr';
  STransExpDate = 'Expiration date';
  STransEfficiency = 'Efficiency';
  STransToday = 'Today';
  // DialogWorkspotSelect
  STransWorkspotSelect = 'Workspot Select';
  STransPicture = 'Picture';
  STransMachine = 'Machine';
  STransTypeOfProdScreen = 'Type';
  STransProdBarsWithoutTR = 'Workspot';
  STransProdBarsTRMachineLevel = 'Production bars / Time recording combined (Machine level)';
  STransProdBarsTRWorskpotLevel = 'Production bars / Time recording combined (Workspot level)';
  // ProductionScreenWait
  STransProdScreen = 'PIMS Production Screen';
  // ShowChart
  STransGraph = 'Graph';
  STransStats = 'Statistics';
  STransTimeInt = 'Time Int.:';
  STransTime2 = 'Time:';
  STransProdHr = 'Prod./Hr:';
  STransProdNorm = 'Prod. Norm:';
  STransNrOfEmp =  'Nr of Empl.:';
  STransShowEmp = 'Show Empl.';
  STransScale = 'Scale';
  STransDateInput = 'Date Input';
  STransFromDate = 'From Date:';
  STransTo = 'to';
  STransAccept = 'Accept';
  STransEnterDates = 'Enter Dates';
  STransEmployees = 'Employees';
  STransEmpNr = 'EMPL NR';
  STransEmpName = 'EMPL NAME';
  STransJobCode = 'JOB CODE';
  STransJobName = 'JOB NAME';
  STransShiftNr = 'SHIFT NR';
  STransDateTimeIn = 'DATETIME IN';
  STransProdOutput = 'Production Output';
  STransTotalProd = 'Total Production';
  STransEC = 'EC';
  STransBackground = 'Background';
  STransNrOfEmp100 = 'Nr of Empl. (x100)';
  STransNormProdLevel = 'Norm Prod. Level';
  STransOutput = 'Output';
  STransCloseGraph = 'Close Graph';
  STransExportChart = 'Export Chart';
  // BaseTopMenuFRM
  STransAboutABSGroup = 'About Gotli Labs-Group';
  STransABSGroupHomePage = 'Gotli Labs Home Page';
  STransOrderInfo = 'Ordering Information';
  STransPimsHomePage = 'Globe Home Page';
  STransContents = 'Contents';
  STransIndex = 'Index';
  STransExit1 = 'E&xit';
  STransPrint = 'Print';
  STransExit2 = 'Exit';
  STransFile = 'File';
  STransHelp = 'Help';
  STransShow = 'Show';
  STransEmpInfo = 'Employee Info';
  STransEmpEff = 'Employee Efficiency';
  STransDP = 'Afd.';
{$ELSE}
  {$IFDEF PIMSCHINESE}
  // HomeFRM
  STransHomeCaption = 'PIMS - 生产界面';
  STransHR = 'hr';
  STransPCS = 'pcs.';
  STransNormPCS = '标准 pcs.';
  STransShowAllEmps = '显示所有员工';
  STransShowEmpsOnWrongWorkspot = '显示错误工作点员工';
  STransShowEmpsNotScannedIn = '显示未扫描到的员工';
  STransShowEmpsAbsWithReason = '显示因故缺席员工';
  STransShowEmpsABsWithoutReason = '线束无故缺席员工';
  STransShowEmpsWithFirstAid = '显示紧急状态员工';
  STransShowEmpsTooLate = '显示太晚员工';
  STransShowChart = '显示图表';
  STransShowEmps = '显示员工';
  STransWorkspot = '工作点';
  STransEffMeter = '效率测量';
  STransWorkspotBigEffMeter = '工作点 / 大效率测量';
  STransLineHorz = '水平线';
  STransLineVert = '垂直线';
  STransRect = '矩形';
  STransEmpOverview = '员工概览';
  STransDelete = '删除';
  STransDeleteCurrentObject = '删除当前对象 ';
  STransSave = '保存';
  STransSaveScheme = '保存计划';
  STransEdit = '编辑';
  STransNew = '新建';
  STransOpen = '打开';
  STransSaveAs = '以 保存';
  STransLegenda = '图例';
  STransShowStatusInfo = '显示状态信息';
  STransCurrEff = '目前效率';
  STransTodaysEff = '今天的效率';
  // DialogChartSettings
  STransSettings = '设置';
  STransExportSettings = '导出设置';
  STransSeparator = '分离器';
  // DialogDepartmentSelect
  STransSelectDept = '选择部门';
  STransPlant = '工厂';
  STransDept = '部门';
  // DialogImageProperties
  STransImageProperties =  '图像属性';
  STransLeftPos = '左侧位置';
  STransTopPos = '顶部位置';
  STransWidth = '宽度';
  STransHeight = '高度';
  STransPlantCode = '工厂码';
  STransWorkspotCode =  '工作点码';
  STransWorkspotDesc = '工作点描述';
  STransOK = 'OK';
  STransCancel = '取消';
  // DialogLegenda
  STransPuppets = '傀儡';
  STransPlanned1 = '已计划, 但并未扫描 (比计划扫描的少)';
  STransPlanned2 = '已计划, 已扫描 (正确的员工)';
  STransPlanned3 = '已计划, 与其他有相同等级的员工一同扫描';
  STransPlanned4 = '已计划, 与其他有不同等级的员工一同扫描';
  STransNotPlanned = '未计划, 已扫描 (比预计扫描的多)';
  STransBlinking = '闪烁, 当数据收集到未对进行此工作的员工进行扫描 ';
  STransWorkspotFrame = '当工作点进行比较工作时工作点框架闪烁';
  STransAndTheDev = '以及偏差值大于预设最大值.';
  STransTrafficLight = '信号灯';
  STransTheLastTime1 = '最后一次数据收集时, 没有关于工作点的数据';
  STransTheLastTime2 = '最后一次数据收集时, 有关于工作点的数据但是值是 0';
  STransTheLastTime3 = '最后一次数据收集时, 有关于工作点的数据 (不是0)';
  // DialogPrintChart
  STransPrintChart = '打印图表';
  STransPrintOptions = '打印选项';
  STransPrinterSetup = '打印机设置';
  STransPortrait = '人像';
  STransLandscape = '景观';
  // DialogSelectMode
  STransSelectMode = '选择模式';
  STransMode = '模式';
  STransShowAllEmployees = '显示所有员工';
  // DialogSettings
  STransGlobalSettings = '整体设置';
  STransSoundAlarm = '警报音';
  STransSoundFilename = '声音文件名';
  STransUseSoundAlarm = '使用警报音';
  STransBrowse = '浏览';
  STransRefreshTimeInterval = '刷新时间间隔';
  STransInterval = '间隔';
  STransSeconds = '秒';
  STransSchemeSettings = '计划设置';
  STransWorkspotScale = '工作点规模';
  STransPercentage = '百分比';
  STransGridlineDistance = '栅格线距离';
  STransEfficiencyMeters = '效率米';
  STransUsePeriod = '使用周期';
  STransCurrent =  '当前的';
  STransSince = '从';
  STransShift = '移动';
  STransTime = '时间';
  STransFontScale = '字体规模';
  STransEffColorBoundaries = '效率颜色边界';
  STransRed = '红';
  STransOrange = '橙';
  // Login
  STransDatabaseLogin = '数据库登录';
  STransPassword = '&密码';
  STransUsername = '&用户名';
  STransDatabasePims = '数据库:            Pims';
  // DialogShowAllEmployees
  STransEmpsOnWorkspot = '工作点的员工';
  STransEmployee = '员工';
  STransScanned = '已扫描';
  STransPlanned = '已计划';
  STransStandAvail = '站立. 可用';
  STransAbsWithReason = '因故缺席';
  STransFirstAid = '紧急';
  STransNumber = '号码';
  STransName = '姓名';
  STransShort = '缺少';
  STransTeam = '队伍';
  STransWS = 'WS';
  STransTimeIn = '重新计时';
  STransStartTime = '开始时间';
  STransEndTime = '结束时间';
  STransLevel = '等级';
  STransAbsReasonCode = '缺席原因码';
  STransAbsReasonDescr = '缺席原因 Descr';
  STransExpDate = '截止日期';
  STransEfficiency = '效率';
  STransToday = '今天';
  // DialogWorkspotSelect
  STransWorkspotSelect = '工作点选择';
  STransPicture = '图像';
  STransMachine = '机器';
  STransTypeOfProdScreen = '类型';
  STransProdBarsWithoutTR = '工作点';
  STransProdBarsTRMachineLevel = '产出 / 有时间记录 (机器等级)';
  STransProdBarsTRWorskpotLevel = '产出 / 有时间记录 (工作点等级)';
  // ProductionScreenWait
  STransProdScreen = 'PIMS 产品界面';
  // ShowChart
  STransGraph = '图表';
  STransStats = '统计';
  STransTimeInt = '时间间隔:';
  STransTime2 = '时间:';
  STransProdHr = '例./小时:';
  STransProdNorm = '例. 标准:';
  STransNrOfEmp =  'Nr of Empl.:';
  STransShowEmp = '展示 Empl.';
  STransScale = '规模';
  STransDateInput = '投入时间';
  STransFromDate = '开始日期:';
  STransTo = '到';
  STransAccept = '接受';
  STransEnterDates = '输入日期';
  STransEmployees = '员工';
  STransEmpNr = '员工 NR';
  STransEmpName = '员工姓名';
  STransJobCode = '工号';
  STransJobName = '职业';
  STransShiftNr = '移动 NR';
  STransDateTimeIn = '输入日期时间';
  STransProdOutput = '产量';
  STransTotalProd = '总产量';
  STransEC = 'EC';
  STransBackground = '后台';
  STransNrOfEmp100 = 'Nr of Empl. (x100)';
  STransNormProdLevel = 'Norm Prod. Level';
  STransOutput = '产出';
  STransCloseGraph = '关闭图表';
  STransExportChart = '输出图表';
  // BaseTopMenuFRM
  STransAboutABSGroup = '关于 ABS-Group';
  STransABSGroupHomePage = 'ABS-Group 主页';
  STransOrderInfo = '订购须知';
  STransPimsHomePage = 'Pims 主页';
  STransContents = '目录';
  STransIndex = '索引';
  STransExit1 = '退出';
  STransPrint = '打印';
  STransExit2 = '退出';
  STransFile = '文件';
  STransHelp = '帮助';
  STransShow = '展示';
  STransEmpInfo = '员工信息';
  STransEmpEff = '员工效率';
  STransDP = '部';
{$ELSE}
  {$IFDEF PIMSNORWEGIAN}
  // HomeFRM
  STransHomeCaption = 'PIMS - Produksjons skjerm';
  STransHR = 'time';
  STransPCS = 'stk.';
  STransNormPCS = 'nnorm ant.';
  STransShowAllEmps = 'Vis alle ansatte';
  STransShowEmpsOnWrongWorkspot = 'Vis ansatte p?feil arbeidsposisjon';
  STransShowEmpsNotScannedIn = 'Vis ansatte ikke skannet inn';
  STransShowEmpsAbsWithReason = 'Vis ansatte frav鎟ende med grunn';
  STransShowEmpsABsWithoutReason = 'Vis ansatte frav鎟ende uten grunn';
  STransShowEmpsWithFirstAid = 'Vis ansatte med Industrivern sertifikat';
  STransShowEmpsTooLate = 'Vis ansatte, sent oppm鴗e';
  STransShowChart = 'Vis diagram';
  STransShowEmps = 'Vis ansatte';
  STransWorkspot = 'Arbeidsposisjon';
  STransEffMeter = 'Effektivitetsm錶er';
  STransWorkspotBigEffMeter = 'Arbeidsposisjon / Stor Effektivitetsm錶er';
  STransLineHorz = 'Linje horisontal';
  STransLineVert = 'Linje vertikalt';
  STransRect = 'Rektangel';
  STransEmpOverview = 'Ansatte oversikt';
  STransDelete = 'Slett';
  STransDeleteCurrentObject = 'Slett aktuelt objekt';
  STransSave = 'Lagre';
  STransSaveScheme = 'Lagre skjema';
  STransEdit = 'Redigere';
  STransNew = 'Ny';
  STransOpen = '舙ne';
  STransSaveAs = 'Lagre som';
  STransLegenda = 'Legenda';
  STransShowStatusInfo = 'Vis status informasjon';
  STransCurrEff = 'Aktuell effektivitet';
  STransTodaysEff = 'Dagen effektivitet';
  // DialogChartSettings
  STransSettings = 'Innstillinger';
  STransExportSettings = 'Ekspoter innstillinger';
  STransSeparator = 'Separator';
  // DialogDepartmentSelect
  STransSelectDept = 'Velg avdeling';
  STransPlant = 'Anlegg';
  STransDept = 'Avdeling';
  // DialogImageProperties
  STransImageProperties =  'Bilde egenskaper';
  STransLeftPos = 'Venstre posisjon';
  STransTopPos = 'Topp posisjon';
  STransWidth = 'Bredde';
  STransHeight = 'H鴜de';
  STransPlantCode = 'Anleggs kode';
  STransWorkspotCode =  'Abeidsposisjons kode';
  STransWorkspotDesc = 'Abeidsposisjons beskrivelse';
  STransOK = 'OK';
  STransCancel = 'Avbryt';
  // DialogLegenda
  STransPuppets = 'dukker';
  STransPlanned1 = 'Planlagt, men ikke skannet inn (mindre skannet inn enn planlagt)';
  STransPlanned2 = 'Planlagt, skannet inn (korrekt ansatt)';
  STransPlanned3 = 'Planlagt, annen ansatt med samme niv?er skannet inn ';
  STransPlanned4 = 'Planlagt, annen ansatt med annet niv?skannet inn';
  STransNotPlanned = 'Ikke planlagt, skannet inn (mer skannet inn enn planlagt)';
  STransBlinking = 'Blinker, n錼 det er datainnsammling p?en jobb uten noen ansatt skannet p?den jobben';
  STransWorkspotFrame = 'Arbeidsposisjon-rammen blinker n錼 en av jobbene i arbeidsposisjon er en sammenligbar jobb';
  STransAndTheDev = 'og avviket er mer enn det definerte maksimum tillatt.';
  STransTrafficLight = 'Trafikklys';
  STransTheLastTime1 = 'Siste gang det var data innsammling, var det ingen data for arbeidsposisjon';
  STransTheLastTime2 = 'Siste gang det var data innsammling, var det data for arbeidsposisjon, men det var 0';
  STransTheLastTime3 = 'Siste gang det var data innsammling, var det data for arbeidsposisjon (ikke 0)';
  // DialogPrintChart
  STransPrintChart = 'Skriv ut diagramet';
  STransPrintOptions = 'Skriv ut valgene';
  STransPrinterSetup = 'Skriver ut oppsettet';
  STransPortrait = 'Portrett';
  STransLandscape = 'Landskap';
  // DialogSelectMode
  STransSelectMode = 'Velg modus';
  STransMode = 'Modus';
  STransShowAllEmployees = 'Vis alle ansatte';
  // DialogSettings
  STransGlobalSettings = 'Globale instillinger';
  STransSoundAlarm = 'Lyd Alarm';
  STransSoundFilename = 'Lyd Filenavn';
  STransUseSoundAlarm = 'Bruk lyd Alarm';
  STransBrowse = 'Utforsk';
  STransRefreshTimeInterval = 'Oppdater tidsintervall';
  STransInterval = 'Intervall';
  STransSeconds = 'Sekunder';
  STransSchemeSettings = 'Skjema innstillinger';
  STransWorkspotScale = 'Arbeidsposisjon m錶estokk';
  STransPercentage = 'Prosenten';
  STransGridlineDistance = 'Linjenett avstand';
  STransEfficiencyMeters = 'Effektivitetsm錶ere';
  STransUsePeriod = 'Brukt periode';
  STransCurrent =  'Aktuell';
  STransSince = 'Siden';
  STransShift = 'Skift';
  STransTime = 'Tid';
  STransFontScale = 'Font st鴕relse';
  STransEffColorBoundaries = 'Effektivitetsfarge grenser';
  STransRed = 'R鴇';
  STransOrange = 'Oransje';
  // Login
  STransDatabaseLogin = 'Database innlogging';
  STransPassword = '&Passord';
  STransUsername = '&Brukernavn';
  STransDatabasePims = 'Database:            Pims';
  // DialogShowAllEmployees
  STransEmpsOnWorkspot = 'Ansatt p?Arbeidsposisjon';
  STransEmployee = 'Ansatt';
  STransScanned = 'Skannet';
  STransPlanned = 'Planlagt';
  STransStandAvail = 'Standard tilgjenglighet';
  STransAbsWithReason = 'Frav鎟 med grunn';
  STransFirstAid = 'Industrivern';
  STransNumber = 'Nummer';
  STransName = 'Navn';
  STransShort = 'Kort';
  STransTeam = 'Team';
  STransWS = 'WS';
  STransTimeIn = 'Tid-inn';
  STransStartTime = 'Start Tid';
  STransEndTime = 'Slutt Tid';
  STransLevel = 'Niv?';
  STransAbsReasonCode = 'Frav鎟s grunn kode';
  STransAbsReasonDescr = 'Frav鎟s grunn beskrivelse';
  STransExpDate = 'Utl鴓sdato';
  STransEfficiency = 'Effektivitet';
  STransToday = 'I Dag';
  // DialogWorkspotSelect
  STransWorkspotSelect = 'Valgt Arbeidsposisjon';
  STransPicture = 'Bilde';
  STransMachine = 'Maskin';
  STransTypeOfProdScreen = 'Type';
  STransProdBarsWithoutTR = 'Arbeidsposisjon';
  STransProdBarsTRMachineLevel = 'Produksjons s鴜ler / Tidsregistrering kombinert (Maskin niv?';
  STransProdBarsTRWorskpotLevel = 'Produksjons s鴜ler / Tidsregistrering kombinert (Arbeidsposisjon niv?';
  // ProductionScreenWait
  STransProdScreen = 'PIMS Produksjons skjerm';
  // ShowChart
  STransGraph = 'Graf';
  STransStats = 'Statistikk';
  STransTimeInt = 'Tid Int.:';
  STransTime2 = 'Tid:';
  STransProdHr = 'Prod./time:';
  STransProdNorm = 'Prod. Norm:';
  STransNrOfEmp =  'ant. Empl.:';
  STransShowEmp = 'Vis Empl.';
  STransScale = 'M錶estokk';
  STransDateInput = 'Dato';
  STransFromDate = 'Fra Date:';
  STransTo = 'til';
  STransAccept = 'Akseptert';
  STransEnterDates = 'Skriv inn datoer';
  STransEmployees = 'Ansatte';
  STransEmpNr = 'Ansatt NR';
  STransEmpName = 'Ansatt NAME';
  STransJobCode = 'JOB KODE';
  STransJobName = 'JOB NAVN';
  STransShiftNr = 'SKIFT NR';
  STransDateTimeIn = 'DATOTID INN';
  STransProdOutput = 'Produsksjons Ut resultat';
  STransTotalProd = 'Totalt Produsert';
  STransEC = 'EC';
  STransBackground = 'Bakgrunn';
  STransNrOfEmp100 = 'Antall ansatt. (x100)';
  STransNormProdLevel = 'Norm Prod. Niv?';
  STransOutput = 'Output/resultat';
  STransCloseGraph = 'Steng Graf';
  STransExportChart = 'Ekspoter diagram';
  // BaseTopMenuFRM
  STransAboutABSGroup = 'Om Gotli Labs';
  STransABSGroupHomePage = 'Gotli Labs Hjemmeside';
  STransOrderInfo = 'Bestillings informasjon';
  STransPimsHomePage = 'Globe Hjemmeside';
  STransContents = 'Innhold';
  STransIndex = 'Hovedsiden';
  STransExit1 = 'Avslutt';
  STransPrint = 'Skriv ut';
  STransExit2 = 'Avslutt';
  STransFile = 'Fil';
  STransHelp = 'Hjelp';
  STransShow = 'Vis';
  STransEmpInfo = 'Ansatt info';
  STransEmpEff = 'Ansattes effektivitet';
  STransDP = 'Avd.';
{$ELSE}
// ENGLISH (default language)
  // HomeFRM
  STransHomeCaption = 'PIMS - Production Screen';
  STransHR = 'hr';
  STransPCS = 'pcs.';
  STransNormPCS = 'norm pcs.';
  STransShowAllEmps = 'Show All Employees';
  STransShowEmpsOnWrongWorkspot = 'Show Employees on wrong workspot';
  STransShowEmpsNotScannedIn = 'Show Employees not scanned in';
  STransShowEmpsAbsWithReason = 'Show Employees absent with reason';
  STransShowEmpsABsWithoutReason = 'Show Employees absent without reason';
  STransShowEmpsWithFirstAid = 'Show Employees with First Aid';
  STransShowEmpsTooLate = 'Show Employees too late';
  STransShowChart = 'Show Chart';
  STransShowEmps = 'Show Employees';
  STransWorkspot = 'Workspot';
  STransEffMeter = 'Efficiency Meter';
  STransWorkspotBigEffMeter = 'Workspot / Big Efficiency Meter';
  STransLineHorz = 'Line Horizontal';
  STransLineVert = 'Line Vertical';
  STransRect = 'Rectangle';
  STransEmpOverview = 'Employee Overview';
  STransDelete = 'Delete';
  STransDeleteCurrentObject = 'Delete current object';
  STransSave = 'Save';
  STransSaveScheme = 'Save Scheme';
  STransEdit = 'Edit';
  STransNew = 'New';
  STransOpen = 'Open';
  STransSaveAs = 'Save As';
  STransLegenda = 'Legenda';
  STransShowStatusInfo = 'Show Status Info';
  STransCurrEff = 'Current Efficiency';
  STransTodaysEff = 'Today''s Efficiency';
  // DialogChartSettings
  STransSettings = 'Settings';
  STransExportSettings = 'Export Settings';
  STransSeparator = 'Separator';
  // DialogDepartmentSelect
  STransSelectDept = 'Select Department';
  STransPlant = 'Plant';
  STransDept = 'Department';
  // DialogImageProperties
  STransImageProperties =  'Image Properties';
  STransLeftPos = 'Left Position';
  STransTopPos = 'Top Position';
  STransWidth = 'Width';
  STransHeight = 'Height';
  STransPlantCode = 'Plant Code';
  STransWorkspotCode =  'Workspot Code';
  STransWorkspotDesc = 'Workspot Description';
  STransOK = 'OK';
  STransCancel = 'Cancel';
  // DialogLegenda
  STransPuppets = 'Puppets';
  STransPlanned1 = 'Planned, but not scanned in (less scanned in than planned)';
  STransPlanned2 = 'Planned, scanned in (correct employee)';
  STransPlanned3 = 'Planned, other employee with same level scanned in';
  STransPlanned4 = 'Planned, other employee with other level scanned in';
  STransNotPlanned = 'Not planned, scanned in (more scanned in than planned)';
  STransBlinking = 'Blinking, when there is datacollection on a job without any employee scanned on that job';
  STransWorkspotFrame = 'Workspot frame blinks when one of the jobs of the workspot is a comparison job';
  STransAndTheDev = 'and the deviation is more than the defined maximum.';
  STransTrafficLight = 'Traffic light';
  STransTheLastTime1 = 'The last time there was datacollection, there was no data for the workspot';
  STransTheLastTime2 = 'The last time there was datacollection, there was data for the workspot but it was 0';
  STransTheLastTime3 = 'The last time there was datacollection, there was data for the workspot (not 0)';
  // DialogPrintChart
  STransPrintChart = 'Print Chart';
  STransPrintOptions = 'Print Options';
  STransPrinterSetup = 'Printer Setup';
  STransPortrait = 'Portrait';
  STransLandscape = 'Landscape';
  // DialogSelectMode
  STransSelectMode = 'Select Mode';
  STransMode = 'Mode';
  STransShowAllEmployees = 'Show All Employees';
  // DialogSettings
  STransGlobalSettings = 'Global Settings';
  STransSoundAlarm = 'Sound Alarm';
  STransSoundFilename = 'Sound Filename';
  STransUseSoundAlarm = 'Use Sound Alarm';
  STransBrowse = 'Browse';
  STransRefreshTimeInterval = 'Refresh Time Interval';
  STransInterval = 'Interval';
  STransSeconds = 'seconds';
  STransSchemeSettings = 'Scheme Settings';
  STransWorkspotScale = 'Workspot Scale';
  STransPercentage = 'Percentage';
  STransGridlineDistance = 'Gridline Distance';
  STransEfficiencyMeters = 'Efficiency Meters';
  STransUsePeriod = 'Use Period';
  STransCurrent =  'Current';
  STransSince = 'Since';
  STransShift = 'Shift';
  STransTime = 'Time';
  STransFontScale = 'Font Scale';
  STransEffColorBoundaries = 'Efficiency Color Boundaries';
  STransRed = 'Red';
  STransOrange = 'Orange';
  // Login
  STransDatabaseLogin = 'Database Login';
  STransPassword = '&Password';
  STransUsername = '&User name';
  STransDatabasePims = 'Database:            Pims';
  // DialogShowAllEmployees
  STransEmpsOnWorkspot = 'Employees on Workspot';
  STransEmployee = 'Employee';
  STransScanned = 'Scanned';
  STransPlanned = 'Planned';
  STransStandAvail = 'Stand. Available';
  STransAbsWithReason = 'Absent with reason';
  STransFirstAid = 'First Aid';
  STransNumber = 'Number';
  STransName = 'Name';
  STransShort = 'Short';
  STransTeam = 'Team';
  STransWS = 'WS';
  STransTimeIn = 'Time-in';
  STransStartTime = 'Start Time';
  STransEndTime = 'End Time';
  STransLevel = 'Level';
  STransAbsReasonCode = 'Absence Reason Code';
  STransAbsReasonDescr = 'Absence Reason Descr';
  STransExpDate = 'Expiration date';
  STransEfficiency = 'Efficiency';
  STransToday = 'Today';
  // DialogWorkspotSelect
  STransWorkspotSelect = 'Workspot Select';
  STransPicture = 'Picture';
  STransMachine = 'Machine';
  STransTypeOfProdScreen = 'Type';
  STransProdBarsWithoutTR = 'Workspot';
  STransProdBarsTRMachineLevel = 'Production bars / Time recording combined (Machine level)';
  STransProdBarsTRWorskpotLevel = 'Production bars / Time recording combined (Workspot level)';
  // ProductionScreenWait
  STransProdScreen = 'PIMS Production Screen';
  // ShowChart
  STransGraph = 'Graph';
  STransStats = 'Statistics';
  STransTimeInt = 'Time Int.:';
  STransTime2 = 'Time:';
  STransProdHr = 'Prod./Hr:';
  STransProdNorm = 'Prod. Norm:';
  STransNrOfEmp =  'Nr of Empl.:';
  STransShowEmp = 'Show Empl.';
  STransScale = 'Scale';
  STransDateInput = 'Date Input';
  STransFromDate = 'From Date:';
  STransTo = 'to';
  STransAccept = 'Accept';
  STransEnterDates = 'Enter Dates';
  STransEmployees = 'Employees';
  STransEmpNr = 'EMPL NR';
  STransEmpName = 'EMPL NAME';
  STransJobCode = 'JOB CODE';
  STransJobName = 'JOB NAME';
  STransShiftNr = 'SHIFT NR';
  STransDateTimeIn = 'DATETIME IN';
  STransProdOutput = 'Production Output';
  STransTotalProd = 'Total Production';
  STransEC = 'EC';
  STransBackground = 'Background';
  STransNrOfEmp100 = 'Nr of Empl. (x100)';
  STransNormProdLevel = 'Norm Prod. Level';
  STransOutput = 'Output';
  STransCloseGraph = 'Close Graph';
  STransExportChart = 'Export Chart';
  // BaseTopMenuFRM
  STransAboutABSGroup = 'About Gotli Labs';
  STransABSGroupHomePage = 'Gotli Labs Home Page';
  STransOrderInfo = 'Ordering Information';
  STransPimsHomePage = 'Globe Home Page';
  STransContents = 'Contents';
  STransIndex = 'Index';
  STransExit1 = 'E&xit';
  STransPrint = 'Print';
  STransExit2 = 'Exit';
  STransFile = 'File';
  STransHelp = 'Help';
  STransShow = 'Show';
  STransEmpInfo = 'Employee Info';
  STransEmpEff = 'Employee Efficiency';
  STransDP = 'Dept.';

          {$ENDIF}
        {$ENDIF}
      {$ENDIF}
    {$ENDIF}
  {$ENDIF}
{$ENDIF}

implementation

end.
