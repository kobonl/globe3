(*
  Changes:
    RV043.1. Auto login option.
    - When argument is used like -u:pims then
      it should start with login-name e.g. 'pims', and get the password
      automatically from database.
    MRA:JUN-2010. RV063.4. Order 550478. Personal Screen.
    - Addition of Machine/Workspot + Time recording.
*)

program ProductionScreen;

uses
  Forms,
  SysUtils,
  Dialogs,
  BasePimsFRM in '..\..\Pims Shared\BasePimsFRM.pas' {BasePimsForm},
  BaseTopMenuFRM in '..\..\Pims Shared\BaseTopMenuFRM.pas' {BaseTopMenuForm},
  BaseTopLogoFRM in '..\..\Pims Shared\BaseTopLogoFRM.pas' {BaseTopLogoForm},
  BaseDialogFRM in '..\..\Pims Shared\BaseDialogFRM.pas' {BaseDialogForm},
  AboutFRM in '..\..\Pims Shared\AboutFRM.pas' {AboutForm},
  OrderInfoFRM in '..\..\Pims Shared\OrderInfoFRM.pas' {OrderInfoForm},
  UPimsConst in '..\..\Pims Shared\UPimsConst.pas',
  UPimsMessageRes in '..\..\Pims Shared\UPimsMessageRes.pas',
  UPimsVersion in '..\..\Pims Shared\UPimsVersion.pas',
  UScannedIDCard in '..\..\Pims Shared\UScannedIDCard.pas',
  SplashFRM in '..\..\Pims Shared\SplashFRM.pas' {SplashForm},
  ORASystemDMT in '..\..\Pims Shared\ORASystemDMT.pas' {ORASystemDM: TDataModule},
  CalculateTotalHoursDMT in '..\..\Pims Shared\CalculateTotalHoursDMT.pas' {CalculateTotalHoursDM, TDataModule},
  UGlobalFunctions in '..\..\Pims Shared\UGlobalFunctions.pas',
  DialogSelectionFRM in '..\..\Pims Shared\DialogSelectionFRM.PAS' {DialogSelectionForm},
  DialogLoginDMT in '..\..\Pims Shared\DialogLoginDMT.pas' {DialogLoginDM: TDataModule},
  DialogLoginFRM in '..\..\Pims Shared\DialogLoginFRM.pas' {DialogLoginF},
  ULoginConst in '..\..\Pims Shared\ULoginConst.pas',
  EncryptIt in '..\..\Pims Shared\EncryptIt.pas',
  GlobalDMT in '..\..\Pims Shared\GlobalDMT.pas' {GlobalDM: TDataModule},
  HomeFRM in 'HomeFRM.pas' {HomeF},
  DialogImagePropertiesFRM in 'DialogImagePropertiesFRM.pas' {DialogImagePropertiesF},
  DialogWorkspotSelectFRM in 'DialogWorkspotSelectFRM.pas' {DialogWorkspotSelectF},
  DialogShowAllEmployeesFRM in 'DialogShowAllEmployeesFRM.pas' {DialogShowAllEmployeesF},
  ShowChartFRM in 'ShowChartFRM.pas' {ShowChartF},
  DialogPrintChartFRM in 'DialogPrintChartFRM.pas' {DialogPrintChartF},
  DialogSettingsFRM in 'DialogSettingsFRM.pas' {DialogSettingsF},
  DialogChartSettingsFRM in 'DialogChartSettingsFRM.pas' {DialogChartSettingsF},
  ProductionScreenDMT in 'ProductionScreenDMT.pas' {ProductionScreenDM: TDataModule},
  ProductionScreenWaitFRM in 'ProductionScreenWaitFRM.pas' {ProductionScreenWaitF},
  DialogDepartmentSelectFRM in 'DialogDepartmentSelectFRM.pas' {DialogDepartmentSelectF},
  PlanScanEmpDMT in 'PlanScanEmpDMT.pas' {PlanScanEmpDM: TDataModule},
  DialogSelectModeFRM in 'DialogSelectModeFRM.pas' {DialogSelectModeF},
  DialogLegendaFRM in 'DialogLegendaFRM.pas' {DialogLegendaF},
  BasePimsSerialFRM in '..\..\Pims Shared\BasePimsSerialFRM.pas' {BasePimsSerialForm},
  TimeRecordingDMT in '..\..\TimeRecording\Source\TimeRecordingDMT.pas' {TimeRecordingDM: TDataModule},
  TimeRecordingFRM in '..\..\TimeRecording\Source\TimeRecordingFRM.pas' {TimeRecordingF},
  SelectorFRM in '..\..\TimeRecording\Source\SelectorFRM.pas' {SelectorF},
  PersonalScreenDMT in '..\..\Pims Shared\PersonalScreenDMT.pas' {PersonalScreenDM: TDataModule},
  DialogChangeJobDMT in '..\..\Pims Shared\DialogChangeJobDMT.pas' {DialogChangeJobDM: TDataModule},
  DialogChangeJobFRM in '..\..\Pims Shared\DialogChangeJobFRM.pas' {DialogChangeJobF},
  DialogSelectDownTypeFRM in '..\..\Pims Shared\DialogSelectDownTypeFRM.pas' {DialogSelectDownTypeF};

{$R *.RES}

// RV043.1. Auto login option. get the user from parameter, for example:
//          -u:pims
function AutoLoginUser: String;
var
  I, IPos: Integer;
  Param1, PStr: String;
begin
  Result := '';
  Param1 := '-U:';
  for I := 1 to ParamCount do
  begin
    PStr := UpperCase(ParamStr(I));
    IPos := Pos(Param1, PStr);
    if IPos > 0 then
    begin
      Result := Copy(PStr, IPos + Length(Param1), Length(Pstr));
      Break;
    end;
  end;
end;

begin
  Application.Initialize;
  // set property in order to not update database before login dialog
  // when SystemDM is created
  Application.Title := 'ORCL - PIMS - PRODUCTION SCREEN';
  Application.CreateForm(TORASystemDM, ORASystemDM);
  if not Application.Terminated then
  begin
    DialogLoginF := TDialogLoginF.Create(Application);
    // RV043.1. Auto Login.
    if (AutoLoginUser <> '') then
    begin
      DialogLoginF.AutoLogin := True;
      DialogLoginF.AutoLoginUser := AutoLoginUser;
    end
    else
    begin
      DialogLoginF.AutoLogin := False;
      DialogLoginF.AutoLoginUser := '';
    end;
    //verifyPassword and login as SYSDBA
    if DialogLoginF.VerifyPassword then
    begin
      //update database as PIMS user
      if ORASystemDM.UpdatePimsBase('Pims') then
      begin
        Application.CreateForm(TCalculateTotalHoursDM, CalculateTotalHoursDM);
        Application.CreateForm(TGlobalDM, GlobalDM);
        Application.CreateForm(TPersonalScreenDM, PersonalScreenDM);
        Application.CreateForm(TDialogChangeJobDM, DialogChangeJobDM);
        Application.CreateForm(TProductionScreenDM, ProductionScreenDM);
        Application.CreateForm(TPlanScanEmpDM, PlanScanEmpDM);
        Application.CreateForm(TTimeRecordingDM, TimeRecordingDM);
        TimeRecordingDM.PersonalScreen := True;
        Application.CreateForm(THomeF, HomeF);
        Application.CreateForm(TProductionScreenWaitF, ProductionScreenWaitF);
        Application.CreateForm(TDialogChangeJobF, DialogChangeJobF);
        DialogChangeJobF.Visible := False;
      end;
    end;
    Application.Run;
  end;
end.
