unit DialogShowStatusInfoFRM;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseDialogFRM, dxCntner, Menus, StdActns, ActnList, ImgList,
  StdCtrls, Buttons, ComCtrls, ExtCtrls;

type
  TDialogShowStatusInfoF = class(TBaseDialogForm)
    Memo1: TMemo;
  private
    { Private declarations }
  public
    { Public declarations }
    procedure AddMemo(AMsg: String);
  end;

var
  DialogShowStatusInfoF: TDialogShowStatusInfoF;

implementation

{$R *.dfm}

{ TDialogShowStatusInfoF }

procedure TDialogShowStatusInfoF.AddMemo(AMsg: String);
begin
  Memo1.Lines.Add(AMsg);
end;

end.
