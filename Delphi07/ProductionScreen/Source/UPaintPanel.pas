(*
  MRA:28-NOV-2012 20013550
  - Addition of PaintPanel, which makes it possible to draw on it.
*)
unit UPaintPanel;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls;

type
  TPaintPanel = class(TPanel)
  private
    FOnPaint: TNotifyEvent;
  protected
    procedure Paint; override;
  public
    property Canvas;
  published
    property OnPaint: TNotifyEvent read FOnPaint write FOnPaint;
end;

procedure Register;

implementation

procedure TPaintPanel.Paint;
begin
  inherited;
  if Assigned(fOnPaint) then fOnPaint(Self);
end;

procedure Register;
begin
  RegisterComponents('Samples', [TPaintPanel]);
end;

end.
