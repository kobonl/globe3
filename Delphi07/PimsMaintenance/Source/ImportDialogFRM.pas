unit ImportDialogFRM;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls;

type
  TImportDialogF = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    btnOK: TButton;
    btnCancel: TButton;
    Label1: TLabel;
    lblDirectory: TLabel;
    lblFilename: TLabel;
    lblFromUser: TLabel;
    edtAbsDumpDir: TEdit;
    edtFilename: TEdit;
    edtFromUser: TEdit;
    btnBrowse: TButton;
    procedure btnOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnBrowseClick(Sender: TObject);
  private
    FFromUser: String;
    FDumpDirectory: String;
    FFilename: String;
    procedure SetDumpDirectory(const Value: String);
    procedure SetFilename(const Value: String);
    procedure SetFromUser(const Value: String);
    { Private declarations }
  public
    { Public declarations }
    property DumpDirectory: String read FDumpDirectory write SetDumpDirectory;
    property Filename: String read FFilename write SetFilename;
    property FromUser: String read FFromUser write SetFromUser;
  end;

var
  ImportDialogF: TImportDialogF;

implementation

{$R *.dfm}

uses
  HomeDMT;

{ TImportDialogF }

procedure TImportDialogF.SetDumpDirectory(const Value: String);
begin
  FDumpDirectory := Value;
  edtABSDumpDir.Text := Value;
end;

procedure TImportDialogF.SetFilename(const Value: String);
begin
  FFilename := Value;
  edtFilename.Text := Value;
end;

procedure TImportDialogF.SetFromUser(const Value: String);
begin
  FFromUser := Value;
  edtFromUser.Text := Value;
end;

procedure TImportDialogF.btnOKClick(Sender: TObject);
begin
  Filename := edtFilename.Text;
  FromUser := edtFromUser.Text;
end;

procedure TImportDialogF.FormCreate(Sender: TObject);
begin
  lblFromUser.Visible := True;
  edtFromUser.Visible := True;
(*
{$IFDEF ABSMODE}
  lblFromUser.Visible := True;
  edtFromUser.Visible := True;
{$ELSE}
  lblFromUser.Visible := False;
  edtFromUser.Visible := False;
{$ENDIF}
*)
end;

procedure TImportDialogF.btnBrowseClick(Sender: TObject);
var
  NewFileName, NewDirName: String;
begin
  NewFileName := FileName;
  if HomeDM.AskFileName(NewFileName, 'Enter filename for import-file',
    False, DumpDirectory, 'dmp') then
  begin
    FileName := ExtractFileName(NewFileName);
    NewDirName := ExtractFileDir(NewFileName);
    if LowerCase(NewDirName) <> LowerCase(DumpDirectory) then
      ShowMessage('ERROR: Wrong Dump Directory! (' + NewDirName + ')' + #13 +
        'It can only be: '  + DumpDirectory);
  end;
end;

end.
