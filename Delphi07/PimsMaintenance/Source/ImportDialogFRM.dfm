object ImportDialogF: TImportDialogF
  Left = 361
  Top = 299
  BorderStyle = bsDialog
  Caption = 'Import Settings'
  ClientHeight = 204
  ClientWidth = 596
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 596
    Height = 41
    Align = alTop
    TabOrder = 0
    object Label1: TLabel
      Left = 8
      Top = 5
      Width = 177
      Height = 29
      Caption = 'Import Settings'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -24
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 158
    Width = 596
    Height = 46
    Align = alBottom
    TabOrder = 1
    object btnOK: TButton
      Left = 232
      Top = 8
      Width = 75
      Height = 25
      Caption = 'OK'
      Default = True
      ModalResult = 1
      TabOrder = 0
      OnClick = btnOKClick
    end
    object btnCancel: TButton
      Left = 320
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Cancel'
      ModalResult = 2
      TabOrder = 1
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 41
    Width = 596
    Height = 117
    Align = alClient
    TabOrder = 2
    object lblDirectory: TLabel
      Left = 8
      Top = 18
      Width = 42
      Height = 13
      Caption = 'Directory'
    end
    object lblFilename: TLabel
      Left = 8
      Top = 50
      Width = 42
      Height = 13
      Caption = 'Filename'
    end
    object lblFromUser: TLabel
      Left = 8
      Top = 83
      Width = 48
      Height = 13
      Caption = 'From User'
    end
    object edtAbsDumpDir: TEdit
      Left = 96
      Top = 14
      Width = 409
      Height = 21
      Color = clBtnFace
      Enabled = False
      ReadOnly = True
      TabOrder = 0
    end
    object edtFilename: TEdit
      Left = 96
      Top = 48
      Width = 409
      Height = 21
      TabOrder = 1
    end
    object edtFromUser: TEdit
      Left = 96
      Top = 80
      Width = 409
      Height = 21
      TabOrder = 2
    end
    object btnBrowse: TButton
      Left = 512
      Top = 48
      Width = 75
      Height = 25
      Caption = 'Browse'
      TabOrder = 3
      OnClick = btnBrowseClick
    end
  end
end
