(*
  MRA:23-FEB-2010
  - Changes for Oracle 11. This is case-sensitive!
  MRA:8-FEB-2011
  - Addition of Convert To UTF8-option.
  MRA:8-FEB-2011
  - Addition of option to use this tool for Solar.
  - Addition of option to use this tool for Admin where
    multiple database-users can be entered and used in PIMSMAINT.INI-file,
    which can be selected from a combobox.
  MRA:1-FEB-2012 2.0.0.11.
  - It gave problems when it could not create a folder that was
    defined in PIMSMAINT.INI: It skipped a part of the code resulting
    in missing info. This gave problems with drop/create user.
  MRA:1-FEB-2012 2.0.0.11.
  - When using ImpDP and ExpDP it fails.
    Reason: This can only be done by Oracle-user 'sys' and
    with 'as sysdba'-addition (?)
  MRA:2-FEB-2012 2.0.0.11.
  - If logging in as 'sys', then 'OracleLogon' must be changed, to
    enable 'sysdba'. But this gives problems when sql-statements are
    run, even if the session was changed to 'pims'.
  - For now: Only 'DBVersion' and 'Script View' are changed, so it
    can show info when logged in as 'sys'.
  - Also: imp/exp/impdp/expdp are prepared for 'sysdba'-use.
  - But because other things go wrong the 'sysdba' has been disabled.
  - Changes made for 'impdp' after testing on ORA11-server.
  MRA:13-MAR-2.0.0.12.
  - Add option to export-dp (datapump) to 10g-format. This makes it possible
    to export a database in 11g-environment to 10g.
    - Option VERSION=10.2 must be added to exportdp-line.
  - Add option to export-dp using a CMD-box. This gives information about what
    it is doing.
  MRA:3-FEB-2.0.0.14.
  - When using non-admin-mode then Drop/Create user is 1 button. But it can give
    problems when it drops+creates the user in 1 step. To solve this, use
    separate buttons/actions for this, just as when admin-mode is used.
  MRA:20-JUL-2015-2.0.0.15.
  - Addition of option to log in as sysdba.
  - Addition of 2 extra 'job'-options during CreateUser (can only be added
    by sys-user as sysdba).
  - Addition of button to grant extra job-rights for user.
  - REMARK:
    - For 'as sysdba' use other syntax for imp/exp/impdp/expdp then for sqlplus.
    - Examples:
      - imp "'sys/ABSSYS@ABS1 as sysdba'" ...
      - sqlplus "sys/ABSSYS@ABS1 as sysdba" ...
  MRA:26-JAN-2016 2.0.0.16. PIM-142
  - Addition of checks for the real-time-efficiency-system
  MRA:1-APR-2016 2.0.0.17 PIM-158
  - Addition of plant-selection to have jobs for multiple plants.
  MRA:22-JUL-2016 2.0.0.19 PIM-200
  - Add extra rights to prevent problem during import datapump
  MRA:19-OCT-2016 PIM-91.2
  - Addition:
    - Create files needed for import-from-files-procedure
  MRA:6-DEC-2016 VEI001-7
  - Addition of options for Hybrid-mode.
  MRA:25-JAN-2017 VEI001-7
  - Add option to show remote operators, to check if scans_2_cockpit
    is working.
  MRA:3-FEB-2017 PIM-271
  - Import-from-files: Changed view job-info, also show last-run-date and
    next-run-date.
  PIM-333:14-DEC-2017 PIM-333
  - Add option to view sessions-info + blocked sessions.
  - Add option to kill blocked sessions.
  PIM-333:21-DEC-2017 PIM-333
  - Add option to show locked tables
  MRA:9-MAR-2018 GLOG3-93
  - Show pims-scans from last 12 hours and also show workspot.description
    and employee.description.
  - Show name of employee for operators + workspot description.
  MRA:12-NOV-2018 PIM-403 (2.0.0.26)
  - Create user need more rights
  MRA:27-SEP-2019 GLOB3-338 (2.0.0.27)
  - Add option in Pims-main-program to run the load-all script for Hybrid-mode
*)
unit HomeFRM;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Menus, ComCtrls, IniFiles, Oracle;

const
  PIMS_INI_FILENAME='PIMSMAINT.INI';
  PIMS_LOGFILE='PimsMaintLog';
  PIMS_SQL_DIR='SqlScripts';
  PIMS_SQL_LOG_DIR='Log';
  PIMS_SQL_COMPLETED_DIR='Completed';
  PIMS_DUMP_DIR='Dumps';
  PIMS_DUMP_FILENAME='pimsdump.dmp';
  PIMS_ABSDUMPDIR='ABSDUMPDIR';
  PIMS_ABSDUMPDIR_DIRECTORYNAME='c:\oracle\absdumps';
  PIMS_IMPORTDIR='PIMSIMP_DIR';
  PIMS_IMPORTDIR_DIRECTORYNAME='c:\oracle\absimport';
  PIMS_IMPORTBACKUPDIR='PIMSIMPBK_DIR';
  PIMS_IMPORTBINDIR='PIMSIMPBIN_DIR';
  PIMS_IMPORTJOB = 'PIMS_IMPORT_JOB';
  PIMS_IMPORTPACKAGE = 'PACKAGE_DATACOLLECTION';
  PIMS_PACKAGE_COCKPIT = 'package_cockpit';

type
  TMode=(tmDropUser, tmCreateUser, tmImport, tmExport, tmRunScripts,
         tmGrantJob, tmResetJobQueue, tmTurnOffJob, tmTurnOnJob, tmDropJob,
         tmCreateJob, tmCreateProgram, tmDropProgram, tmCreateImportTable,
         tmCreateImportJob, tmDropImportJob);

type
  THomeF = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    Memo1: TMemo;
    Label1: TLabel;
    MainMenu1: TMainMenu;
    File1: TMenuItem;
    Exit1: TMenuItem;
    tmrBusy: TTimer;
    StatusBar1: TStatusBar;
    Help1: TMenuItem;
    About1: TMenuItem;
    OpenDialog1: TOpenDialog;
    Panel4: TPanel;
    Panel5: TPanel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    btnRunScript: TButton;
    TabSheet2: TTabSheet;
    btnImport: TButton;
    btnExport: TButton;
    TabSheet3: TTabSheet;
    btnImportDP: TButton;
    btnExportDP: TButton;
    btnSetBackupLocation: TButton;
    btnSetBackupPermission: TButton;
    btnCheckPermission: TButton;
    TabSheet4: TTabSheet;
    btnDropCreateUser: TButton;
    btnDropUser: TButton;
    btnCreateUser: TButton;
    TabSheet5: TTabSheet;
    btnReindex: TButton;
    btnScriptView: TButton;
    btnConvertUTF8: TButton;
    cmbDatabaseUsers: TComboBox;
    lblDBUser: TLabel;
    CheckBoxExportDPTo10g: TCheckBox;
    CheckBoxImportExportDPWithCMD: TCheckBox;
    CheckBoxImportExportWithCMD: TCheckBox;
    btnGrantJob: TButton;
    TabSheet6: TTabSheet;
    btnCheckJob: TButton;
    btnCheckLogCalcEff: TButton;
    btnCheckEffCalcPeriod: TButton;
    TabSheet7: TTabSheet;
    btnTurnOffJob: TButton;
    btnTurnOnJob: TButton;
    btnResetJobQueue: TButton;
    btnDropJob: TButton;
    btnCreateJob: TButton;
    cmbPlants: TComboBox;
    Label2: TLabel;
    btnDropProgram: TButton;
    btnCreateProgram: TButton;
    btnViewJobs: TButton;
    TabSheet8: TTabSheet;
    btnSetImportLocation: TButton;
    btnSetImportPermission: TButton;
    btnCreateImportTable: TButton;
    btnCreateImportJob: TButton;
    btnDropImportJob: TButton;
    btnViewImportJob: TButton;
    btnCheckImportPermission: TButton;
    btnViewImportLog: TButton;
    TabSheet9: TTabSheet;
    btnViewLogHybrid: TButton;
    btnViewJobsHybrid: TButton;
    btnViewRemoteOperators: TButton;
    btnViewEmployeeScans: TButton;
    btnViewSessions: TButton;
    btnEndBlockedSessions: TButton;
    btnLoadAllData: TButton;
    procedure Exit1Click(Sender: TObject);
    procedure btnExportClick(Sender: TObject);
    procedure tmrBusyTimer(Sender: TObject);
    procedure btnImportClick(Sender: TObject);
    procedure btnDropCreateUserClick(Sender: TObject);
    procedure About1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnRunScriptClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnReindexClick(Sender: TObject);
    procedure btnDropUserClick(Sender: TObject);
    procedure btnCreateUserClick(Sender: TObject);
    procedure btnImportDPClick(Sender: TObject);
    procedure btnExportDPClick(Sender: TObject);
    procedure btnSetBackupLocationClick(Sender: TObject);
    procedure btnSetBackupPermissionClick(Sender: TObject);
    procedure btnCheckPermissionClick(Sender: TObject);
    procedure btnScriptViewClick(Sender: TObject);
    procedure btnConvertUTF8Click(Sender: TObject);
    procedure cmbDatabaseUsersChange(Sender: TObject);
    procedure btnGrantJobClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnCheckJobClick(Sender: TObject);
    procedure btnCheckLogCalcEffClick(Sender: TObject);
    procedure btnCheckEffCalcPeriodClick(Sender: TObject);
    procedure btnResetJobQueueClick(Sender: TObject);
    procedure btnTurnOffJobClick(Sender: TObject);
    procedure btnTurnOnJobClick(Sender: TObject);
    procedure btnDropJobClick(Sender: TObject);
    procedure btnCreateJobClick(Sender: TObject);
    procedure btnCreateProgramClick(Sender: TObject);
    procedure btnDropProgramClick(Sender: TObject);
    procedure btnViewJobsClick(Sender: TObject);
    procedure btnSetImportLocationClick(Sender: TObject);
    procedure btnSetImportPermissionClick(Sender: TObject);
    procedure btnCreateImportTableClick(Sender: TObject);
    procedure btnCreateImportJobClick(Sender: TObject);
    procedure btnDropImportJobClick(Sender: TObject);
    procedure btnViewImportJobClick(Sender: TObject);
    procedure btnCheckImportPermissionClick(Sender: TObject);
    procedure btnViewImportLogClick(Sender: TObject);
    procedure btnViewLogHybridClick(Sender: TObject);
    procedure btnViewJobsHybridClick(Sender: TObject);
    procedure btnViewRemoteOperatorsClick(Sender: TObject);
    procedure btnViewEmployeeScansClick(Sender: TObject);
    procedure btnViewSessionsClick(Sender: TObject);
    procedure btnEndBlockedSessionsClick(Sender: TObject);
    procedure btnLoadAllDataClick(Sender: TObject);
//    procedure btnRunScriptClick(Sender: TObject);
  private
    { Private declarations }
    MyDatabaseUsername: String;
    MyDatabasePassword: String;
    PimsStartPath: String; // Path where application was started
    PimsRootPath: String; // Path without 'Bin'-part
    PimsDumpPath: String;
    PimsDumpFileName: String;
    PimsBusy: Boolean;
    LogFile: String;
    BatchFile: String;
    SQLFile1, SQLFile2: String;
    LogRootPath: String;
    SQLRootPath: String;
    Mode: TMode;
    ScriptNames: TStringList;
    FIsPims: Boolean;
    FIsAdmin: Boolean;
    procedure ReadIniFileMaintenance;
    procedure WriteIniFileMaintenance;
    function AskScriptNames: Boolean;
    procedure CreateStartBatchFile(ALine1: String;
      AUseCommandPrompt: Boolean=False);
    procedure ButtonsEnable(AEnable: Boolean);
    procedure AddLogFile(AMsg: String; ATimeStamp: Boolean);
    procedure AddMemoToLogFile;
    function MyUserAndPassword: String;
    function MyDatabaseUserAndPassword: String;
    function MyLogonDatabase: String;
    procedure DropUser;
    procedure CreateUser;
    procedure RunScriptLine(ATitle, AScriptLine: String; ASwitchUser: Boolean);
    procedure DetermineDBVersion;
    procedure ChangeDatabaseUser;
    function IsSysDBA(ACurrentUser: String): Boolean;
    function IsSysOPER(ACurrentUser: String): Boolean;
    function AsSysdba(ACurrentUser: String): String;
    function AsSysdbaQuoteStart(ACurrentUser: String): String;
    function AsSysdbaQuoteEnd(ACurrentUser: String): String;
    function AsSysdbaSingleQuoteStart(ACurrentUser: String): String;
    function AsSysdbaSingleQuoteEnd(ACurrentUser: String): String;
    procedure TurnOnJob;
    procedure TurnOffJob;
    procedure CreateJob;
    procedure DropJob;
    procedure CreateProgram;
    procedure DropProgram;
    procedure ReadPlants;
    function PlantArg: String;
    function JobPlantArg: String;
    procedure CreateImportTable;
    procedure CreateImportJob;
    procedure DropImportJob;
  public
    { Public declarations }
    procedure AddLog(AMsg: String);
    property IsPims: Boolean read FIsPims write FIsPims;
    property IsAdmin: Boolean read FIsAdmin write FIsAdmin;
  end;

var
  HomeF: THomeF;

implementation

{$R *.dfm}

uses
  HomeDMT, AboutFRM, WaitFRM, ImportDialogFRM;

// 2.0.0.12
function ExecAndWait(const ACommand: String): Boolean;
var
  ProcInfo: TProcessInformation;
  StartInfo: TStartupInfo;
  MyExitCode: longword;
begin
  FillChar(ProcInfo, SizeOf(TProcessInformation), 0);
  FillChar(StartInfo, SizeOf(TStartupInfo), 0);
  StartInfo.cb := SizeOf(TStartupInfo);
  if CreateProcess(nil, PChar(ACommand), nil,
      nil, false, CREATE_DEFAULT_ERROR_MODE
      + NORMAL_PRIORITY_CLASS, nil, nil, StartInfo,
      ProcInfo) <> False then
  begin
    WaitForSingleObject(ProcInfo.hProcess, INFINITE);
    GetExitCodeProcess(ProcInfo.hProcess, MyExitCode);  // Optional
    CloseHandle(ProcInfo.hProcess);
    Result := True;
  end
  else
  begin
    Result := False;
  end;
end; // ExecAndWait

// Get the Short Path Name.
function ShortPathName(sLongName: String): String;
var
  sShortName:    string;
  nShortNameLen: Integer;
begin
  SetLength(sShortName, MAX_PATH);
  nShortNameLen :=
    GetShortPathName(PChar(sLongName), PChar(sShortName), MAX_PATH - 1);
  if (0 = nShortNameLen) then
  begin
    // Error?
    Result := '';
  end
  else
  begin
    SetLength(sShortName, nShortNameLen);
    Result := sShortName;
  end;
end;

function THomeF.MyUserAndPassword: String;
begin
  // Since Oracle 11 it is case-sensitive!!!
  Result := HomeDM.MyLogonUsername + '/' +
    HomeDM.MyLogonPassword;
end;

function THomeF.MyLogonDatabase: String;
begin
  Result := HomeDM.OracleSession.LogonDatabase;
end;

procedure THomeF.ReadIniFileMaintenance;
var
  PimsIniFile: TIniFile;
  item: Integer;
  procedure ReadExtraSettings(ASection: String);
  var
    i: Integer;
    ThisSection, DBUser, DBPassword: String;
  begin
    cmbDatabaseUsers.Items.Clear;
    for i := 1 to 99 do
    begin
      ThisSection := ASection + IntToStr(i);
      with PimsIniFile do
      begin
        if SectionExists(ThisSection) then
          if ValueExists(ThisSection, 'DatabaseUsername') then
          begin
            DBUser := ReadString(ThisSection, 'DatabaseUsername', '');
            if DBUser <> '' then
            begin
              DBPassword := ReadString(ThisSection, 'DatabasePassword', '');
              if DBPassword <> '' then
                cmbDatabaseUsers.Items.Add(DBUser + ';' + DBPassword);
            end;
          end;
      end;
    end;
    if cmbDatabaseUsers.Items.Count > 0 then
    begin
      cmbDatabaseUsers.ItemIndex := 0;
      if not IsPims then
        ChangeDatabaseUser;
    end;
  end;
begin
  if FileExists(PathCheck(PimsStartPath) + PIMS_INI_FILENAME) then
  begin
    PimsIniFile := nil;
    try
      PimsIniFile := TIniFile.Create(
        PathCheck(PimsStartPath) + PIMS_INI_FILENAME);
      with PimsIniFile do
      begin
        PimsDumpPath := ReadString('General', 'PimsDumpPath', PimsDumpPath);
        PimsDumpFileName := ReadString('General', 'PimsDumpFileName',
          PimsDumpFileName);
        if not IsPims then
          ReadExtraSettings('Solar')
        else
        begin
          // Only do this for Pims+Admin-combination.
          if IsAdmin then
          begin
            // Pims: For Admin+Pims store the entry that
            //       was read from PIMSORA.INI in this combobox.
            ReadExtraSettings('Pims');
            if cmbDatabaseUsers.Items.Count = 0 then
            begin
              cmbDatabaseUsers.Items.Clear;
              cmbDatabaseUsers.Items.Add(HomeDM.MyDatabaseUserName + ';' +
                HomeDM.MyDatabasePassword);
              cmbDatabaseUsers.ItemIndex := 0;
            end
            else
            begin
              // Set item-index to current database-user, based on PIMSORA.INI
              for item := 0 to cmbDatabaseUsers.Items.Count - 1 do
              begin
                if HomeDM.MyDatabaseUserName + ';' + HomeDM.MyDatabasePassword =
                  cmbDatabaseUsers.Items.Strings[item] then
                  cmbDatabaseUsers.ItemIndex := item;
              end;
            end;
          end;
        end;
      end;
    finally
      if PimsIniFile <> nil then
        PimsIniFile.Free;
    end;
  end;
end;

procedure THomeF.ReadPlants;
begin
  try
    cmbPlants.Items.Clear;
    with HomeDM do
    begin
      try
        with oqPlant do
        begin
          Execute;
          while not Eof do
          begin
            cmbPlants.Items.Add(FieldAsString('PLANT_CODE'));
            Next;
          end;
        end;
      except
        on E:EOracleError do
          Addlog(E.Message);
        on E:Exception do
        begin
          Addlog(E.Message);
          Addlog('Warning: No plants found...');
        end;
      end;
    end;
  finally
    if cmbPlants.Items.Count = 0 then
      AddLog('No plants found.')
    else
      AddLog(IntTostr(cmbPlants.Items.Count) + ' plants found.');
    if cmbPlants.Items.Count > 0 then
      cmbPlants.ItemIndex := 0;
  end;
end; // ReadPlants

procedure THomeF.WriteIniFileMaintenance;
var
  PimsIniFile: TIniFile;
begin
  PimsIniFile := nil;
  try
    PimsIniFile := TIniFile.Create(
      PathCheck(PimsStartPath) + PIMS_INI_FILENAME);
    with PimsIniFile do
    begin
      WriteString('General', 'PimsDumpPath', PimsDumpPath);
      WriteString('General', 'PimsDumpFileName', PimsDumpFileName);
    end;
  finally
    if PimsIniFile <> nil then
      PimsIniFile.Free;
  end;
end;

procedure THomeF.FormCreate(Sender: TObject);
var
  AppRootPath: String;
  PimsDumpPathRetry: String;
(*
  procedure WriteINIFile;
  var
    Ini: TIniFile;
  begin
    Ini := TIniFile.Create(PathCheck(AppRootPath) + PimINIFilename);
    try
      Ini.WriteString('PIMSORA', 'Database', MyDatabase);
      Ini.WriteString('PIMSORA', 'DatabaseUsername', MyDatabaseUsername);
      Ini.WriteString('PIMSORA', 'DatabasePassword', MyDatabasePassword);
    finally
      Ini.Free;
    end;
  end;
*)
  procedure HandleMode;
  begin
    if IsAdmin then
    begin
      btnDropCreateUser.Visible := False;
      btnDropUser.Visible := True;
      btnCreateUser.Visible := True;
      btnGrantJob.Visible := True;
      btnConvertUTF8.Visible := True;
      cmbDatabaseUsers.Visible := True;
      cmbDatabaseUsers.Enabled := True;
      lblDBUser.Caption := 'Database User:';
    end
    else
    begin
      btnDropCreateUser.Visible := False; // 2.0.0.14
      btnDropUser.Visible := True;  // 2.0.0.14
      btnCreateUser.Visible := True;  // 2.0.0.14
      btnGrantJob.Visible := True;
      btnConvertUTF8.Visible := False;
      cmbDatabaseUsers.Visible := False;
      cmbDatabaseUsers.Enabled := False;
      lblDBUser.Caption := '';
    end;
    if IsPims then // Pims
    begin
      Caption := 'Pims Maintenance';
      Label1.Caption := Caption;
    end
    else
    begin // Solar
      Caption := 'Solar Maintenance';
      Label1.Caption := Caption;
      btnGrantJob.Visible := False;
      // Do not show last 3 pages
      PageControl1.Pages[PageControl1.PageCount-1].TabVisible := False;
      PageControl1.Pages[PageControl1.PageCount-2].TabVisible := False;
      PageControl1.Pages[PageControl1.PageCount-3].TabVisible := False;
      btnCheckJob.Visible := False;
      btnCheckLogCalcEff.Visible := False;
      btnCheckEffCalcPeriod.Visible := False;
      btnResetJobQueue.Visible := False;
      btnTurnOffJob.Visible := False;
      btnTurnOnJob.Visible := False;
      btnDropJob.Visible := False;
      btnCreateJob.Visible := False;
    end;
  end;
  // Determine root-path without 'Bin'-part.
  // Needed?
{
  function DetermineRootPath(const ARootPath: String): String;
  begin
    try
      try
        // go to one directory before the current one
        ChDir(ARootpath);
        ChDir('..\');
        Result := GetCurrentDir;
        Result := PathCheck(Result);
      except
        Result := ARootPath;
      end;
    finally
      // always go back to original path
      ChDir(ARootPath);
    end;
  end;
}
begin
  PimsStartPath := GetCurrentDir;
  AppRootPath := PimsStartPath;

  PageControl1.ActivePageIndex := 0;

  IsPims := True;
  IsAdmin := False;
{$IFDEF ABSMODE}
  IsPims := True;
  IsAdmin := True;
{$ELSE}
  {$IFDEF SOLARMODE}
    IsPims := False;
    IsAdmin := True;
  {$ELSE}
    IsPims := True;
    IsAdmin := False;
  {$ENDIF}
{$ENDIF}
  HomeDM.IsPims := IsPims;
  HandleMode;

  // 2.0.0.11. This value was empty!
  // Determine Path from where Pims has been started.
  PimsRootPath := PathCheck(PimsStartPath);
  if Pos(UpperCase('\Bin'), UpperCase(PimsStartPath)) > 0 then
    PimsRootPath := PathCheck(Copy(PimsStartPath, 1,
      Pos(UpperCase('\Bin'), UpperCase(PimsStartPath))));

  // Ini-File-specific variables.
  PimsDumpPath := PathCheck(PimsRootPath) + PIMS_DUMP_DIR;
  PimsDumpPathRetry := PimsDumpPath;
  PimsDumpFileName := PIMS_DUMP_FILENAME;
  ReadIniFileMaintenance;
  // 2.0.0.11. Added try-except for each part that creates folders.
  try
    // first try and do not show error when it fails.
    if not DirectoryExists(PimsDumpPath) then
      ForceDirectories(PimsDumpPath);
  except
    try
      // Now try again and show error when it still fails.
      PimsDumpPath := PimsDumpPathRetry;
      if not DirectoryExists(PimsDumpPath) then
        ForceDirectories(PimsDumpPath);
    except
      on E:Exception do
        AddLog(E.Message);
    end;
  end;

  // Make database-settings based on PIMSORA.INI-file.
  // The settings are read by HomeDMT
//  MyLogonDatabase := HomeDM.MyLogonDatabase;
  MyDatabaseUsername := HomeDM.MyDatabaseUserName;
  MyDatabasePassword := HomeDM.MyDatabasePassword;

  // Init variables
  LogRootPath := PathCheck(PimsRootPath) + 'Log\';
  try
    if not DirectoryExists(LogRootPath) then
      ForceDirectories(LogRootPath);
  except
    on E:Exception do
      AddLog(E.Message);
  end;

  // Define SQL Script paths
  SQLRootPath := PathCheck(PimsRootPath) + PIMS_SQL_DIR;
  try
    if not DirectoryExists(SQLRootPath) then
      ForceDirectories(SQLRootPath);
  except
    on E:Exception do
      AddLog(E.Message);
  end;
  try
    if not DirectoryExists(PathCheck(SQLRootPath) + PIMS_SQL_LOG_DIR) then
      ForceDirectories(PathCheck(SQLRootPath) + PIMS_SQL_LOG_DIR);
  except
    on E:Exception do
      AddLog(E.Message);
  end;
  try
    if not DirectoryExists(PathCheck(SQLRootPath) + PIMS_SQL_COMPLETED_DIR) then
      ForceDirectories(PathCheck(SQLRootPath) + PIMS_SQL_COMPLETED_DIR);
  except
    on E:Exception do
      AddLog(E.Message);
  end;

  HomeDM.OracleSession.Connected := False;
(*
  AddLog('Database: ' + MyLogonDatabase);
  Caption := Label1.Caption + '. Database: ' + MyLogonDatabase;
*)
(*
{$IFDEF ABSMODE}
//  AddLog('Login User/Password: ' + MyUserAndPassword);
  AddLog('DatabaseUsername: ' + MyDatabaseUsername);
  AddLog('DatabasePassword: ' + MyDatabasePassword);
{$ELSE}
  {$IFDEF SOLARMODE}
    AddLog('DatabaseUsername: ' + MyDatabaseUsername);
    AddLog('DatabasePassword: ' + MyDatabasePassword);
  {$ENDIF}
{$ENDIF}
*)
  PimsBusy := False;

  try
    if IsPims then
      DetermineDBVersion; // This is done in a different place for Solar.
  except
    on E:EOracleError do
      Addlog(E.Message);
    on E:Exception do
      AddLog(E.Message);
  end;
  try
    StatusBar1.Panels[2].Text := DisplayVersionInfo;
  except
  end;
end;

procedure THomeF.AddLogFile(AMsg: String; ATimeStamp: Boolean);
var
  TFile: TextFile;
  Exists: Boolean;
  LogPath: String;
  function MyZeroFormat(Value: String; Len: Integer): String;
  begin
    while Length(Value) < Len do
      Value := '0' + Value;
    Result := Value;
  end;
  function NewLogPath: String;
  var
    Year, Month, Day: Word;
    DateString: String;
  begin
    try
      DecodeDate(Date, Year, Month, Day);
      DateString :=
        MyZeroFormat(IntToStr(Year), 4) +
        MyZeroFormat(IntToStr(Month), 2) +
        MyZeroFormat(IntToStr(Day), 2);
    except
      DateString := '20020601';
    end;
    Result := PathCheck(LogRootPath) + PIMS_LOGFILE + DateString + '.TXT';
  end;
begin
  if ATimeStamp then
    AMsg := DateTimeToStr(Now) + ' - ' + AMsg;

  LogPath := NewLogPath;
  try
    Exists := FileExists(LogPath);
    AssignFile(TFile, LogPath);
    try
      {$I-}
      if not Exists then
        Rewrite(TFile)
      else
        Append(TFile);
      WriteLn(TFile, AMsg);
      {$I+}
      if IOResult <> 0 then
        AddLog('File access error');
    except
      AddLog('Error! Cannot find/access file: ' + LogPath);
    end;
  finally
    CloseFile(TFile);
  end;
end;

procedure THomeF.AddMemoToLogFile;
var
  I: Integer;
begin
  for I := 0 to Memo1.Lines.Count - 1 do
    AddLogFile(Memo1.Lines[I], False);
end;

procedure THomeF.AddLog(AMsg: String);
begin
  AddLogFile(AMsg, True);
  Memo1.Lines.Add(AMsg);
end;

procedure THomeF.Exit1Click(Sender: TObject);
begin
  Close;
end;

procedure THomeF.About1Click(Sender: TObject);
begin
  with TAboutForm.Create(Application) do
  begin
    try
      ShowModal;
    finally
      Free;
    end;
  end; {with}
end;

// Ask for one or more script-names
function THomeF.AskScriptNames: Boolean;
begin
  Result := False;
  OpenDialog1.InitialDir := SQLRootPath;
  OpenDialog1.Options := [ofAllowMultiSelect, ofFileMustExist];
  OpenDialog1.Filter := 'Script files (*.sql)|*.sql';
  OpenDialog1.FilterIndex := 1; // use first filter
  if OpenDialog1.Execute then
  begin
    Result := (OpenDialog1.Files.Count > 0);
    if Result then
    begin
      ScriptNames.AddStrings(OpenDialog1.Files);
      ScriptNames.Sort;
    end;
  end;
end;

function GetTmpDir: String;
var
  pc: PChar;
begin
  pc := StrAlloc(MAX_PATH + 1);
  GetTempPath(MAX_PATH, pc);
  Result := string(pc);
  StrDispose(pc);
end;

procedure THomeF.CreateStartBatchFile(ALine1: String;
  AUseCommandPrompt: Boolean=False);
var
  BatchfileList: TStringList;
  Batchname: String;
  TmpDir: String;
  function GetTmpFileName(Ext: String): String;
  var
    pc: PChar;
  begin
    pc := StrAlloc(MAX_PATH + 1);
    GetTempFileName(PChar(GetTmpDir), 'uis', 0, pc);
    Result := string(pc);
    Result := ChangeFileExt(Result, Ext);
    StrDispose(pc);
  end;
begin
  Batchname := GetTmpFileName('.bat');
  BatchFile := Batchname;
  BatchfileList := TStringList.Create;
  with BatchfileList do
  begin
    try
      Add(ALine1);
      if AUseCommandPrompt then
        Add('pause');
      SaveToFile(batchname);
      TmpDir := GetTmpDir;
      ChDir(TmpDir);
      AddLog('Busy... Please wait...');
      StatusBar1.Panels[1].Text := 'Busy... Please wait...';
      Update;
      if not AUseCommandPrompt then
        WinExec(PChar(Batchname), SW_HIDE)
      else
        ExecAndWait(BatchName);
//      WaitF := TWaitF.Create(Application);
//      WaitF.Show;
    finally
      BatchfileList.Free;
    end;
  end;
end;

procedure THomeF.ButtonsEnable(AEnable: Boolean);
begin
  // 2.0.0.14 Do not use DropCreate-button anymore
  btnDropUser.Enabled := AEnable;
  btnCreateUser.Enabled := AEnable;
  btnGrantJob.Enabled := AEnable;
  btnImport.Enabled := AEnable;
  btnExport.Enabled := AEnable;
  btnRunScript.Enabled := AEnable;
  btnReindex.Enabled := AEnable;
end;

procedure THomeF.tmrBusyTimer(Sender: TObject);
var
  FileHandle: Integer;
begin
  FileHandle := FileOpen(LogFile, fmOpenRead);
  if FileHandle >= 0 then
  begin
    tmrBusy.Enabled := False;
    StatusBar1.Panels[1].Text := '';
    FileClose(FileHandle);
    ButtonsEnable(True);
    Memo1.Lines.LoadFromFile(LogFile);
    AddMemoToLogFile;
    DeleteFile(BatchFile);
    DeleteFile(ChangeFileExt(BatchFile, '.tmp'));
    AddLog('Ready');
//    WaitF.Close;
//    WaitF.Free;
    if (Mode in [tmCreateUser, tmDropUser, tmRunScripts, tmGrantJob,
      tmResetJobQueue, tmTurnOffJob, tmTurnOnJob, tmDropJob, tmCreateJob,
      tmCreateProgram, tmDropProgram, tmCreateImportTable,
      tmCreateImportJob, tmDropImportJob]) then
    begin
      DeleteFile(SQLFile1);
      DeleteFile(SQLFile2);
    end;
    Update;
    PimsBusy := False;
  end;
end;

procedure THomeF.btnExportClick(Sender: TObject);
var
  FileName: String;
  DumpFile, ShortPimsDumpPath: String;
  CommandLine: String;
begin
  Mode := tmExport;
  PimsDumpFileName := MyDatabaseUsername + '_' +
    HomeDM.PimsDBVersion + '_' +
    MyLogonDatabase + '_' +
    FormatDateTime('yyyymmdd_hhnn', Now) + '.dmp';
  FileName := PimsDumpFileName;
  if HomeDM.AskFileName(FileName, 'Enter filename for export-file',
    True, PimsDumpPath, 'dmp') then
  begin
    PimsDumpPath := ExtractFileDir(FileName);
    ShortPimsDumpPath := ShortPathName(PimsDumpPath);
    if ShortPimsDumpPath = '' then
      AddLog('Error! Dump-location [' + PimsDumpPath + '] not found.')
    else
    begin
      PimsDumpFileName := ExtractFileName(FileName);
      DumpFile := PathCheck(ShortPimsDumpPath) + PimsDumpFileName;
      LogFile := PathCheck(ShortPimsDumpPath) + 'exportpims.log';
      DeleteFile(LogFile);
      tmrBusy.Enabled := True;
      PimsBusy := True;
      AddLog('Export');
      ButtonsEnable(False);
      CommandLine :=
        'exp ' +
        AsSysdbaQuoteStart(HomeDM.MyLogonUserName) +
        MyUserAndPassword + '@' + MyLogonDatabase +
        AsSysdba(HomeDM.MyLogonUserName) +
        AsSysdbaQuoteEnd(HomeDM.MyLogonUserName) +
        ' file=' + '"' + DumpFile + '"' +
        ' owner=' + MyDatabaseUsername +
        ' log=' + '"' + LogFile + '"';
{$IFDEF ABSMODE}
  AddLog(CommandLine);
{$ENDIF}
      try
        HomeDM.SetORA64Env;
        if CheckBoxImportExportWithCMD.Checked then
          CreateStartBatchFile(CommandLine, True)
        else
          CreateStartBatchFile(CommandLine);
      finally
        HomeDM.SetORA32Env;
      end;
      Update;
    end;
  end;
end; // btnExportClick(

procedure THomeF.btnImportClick(Sender: TObject);
var
  FileName: String;
  DumpFile, ShortPimsDumpPath: String;
  FromDatabaseUsername, NewDatabaseUsername: String;
  CommandLine: String;
  procedure AskFromDatabaseUser;
  begin
    // Ask first the From Database User.
    if (InputQuery('Import'  , 'Enter From Database User', NewDatabaseUsername)) then
    begin
      // NewString has been changed by the user, who clicked ok
      FromDatabaseUsername := NewDatabaseUsername;
    end
    else
      Exit; // User clicked Cancel
    AddLog('From Database User: ' + FromDatabaseUsername);
  end;
begin
  Mode := tmImport;
  FileName := PimsDumpFileName;

  FromDatabaseUsername := MyDatabaseUsername;
  NewDatabaseUsername := MyDatabaseUsername;

{$IFDEF ABSMODE}
  AskFromDatabaseUser;
{$ENDIF}
{$IFDEF SOLARMODE}
  AskFromDatabaseUser;
{$ENDIF}

  if HomeDM.AskFileName(FileName, 'Enter filename for import-file',
    False, PimsDumpPath, 'dmp') then
  begin
    PimsDumpPath := ExtractFileDir(FileName);
    ShortPimsDumpPath := ShortPathName(PimsDumpPath);
    if ShortPimsDumpPath = '' then
      AddLog('Error! Dump-location [' + PimsDumpPath + '] not found.')
    else
    begin
      PimsDumpFileName := ExtractFileName(FileName);
      DumpFile := PathCheck(ShortPimsDumpPath) + PimsDumpFileName;
      LogFile := PathCheck(ShortPimsDumpPath) + 'importpims.log';
      DeleteFile(LogFile);
      tmrBusy.Enabled := True;
      PimsBusy := True;
      AddLog('Import');
      ButtonsEnable(False);
      CommandLine :=
        'imp ' +
        AsSysdbaQuoteStart(HomeDM.MyLogonUserName) +
        MyUserAndPassword + '@' + MyLogonDatabase +
        AsSysdba(HomeDM.MyLogonUserName) +
        AsSysdbaQuoteEnd(HomeDM.MyLogonUserName) +
        ' file=' + '"' + DumpFile + '"' +
        ' fromuser=' + FromDatabaseUsername +
        ' touser=' + MyDatabaseUsername +
        ' log=' + '"' + LogFile + '"' +
        ' commit=yes';
{$IFDEF ABSMODE}
  AddLog(CommandLine);
{$ENDIF}
      try
        HomeDM.SetORA64Env;
        if CheckBoxImportExportWithCMD.Checked then
          CreateStartBatchFile(CommandLine, True)
        else
          CreateStartBatchFile(CommandLine);
      finally
        HomeDM.SetORA32Env;
      end;
      Update;
    end;
  end;
end; // btnImportClick

procedure THomeF.btnRunScriptClick(Sender: TObject);
var
  FileName: String;
  ScriptFileName: String;
  I: Integer;
  procedure AddSQLScriptTable;
  begin
    with HomeDM do
    begin
      try
        oqSQLScriptSearch.SetVariable('SCRIPTFILENAME', ScriptFileName);
        oqSQLScriptSearch.Execute;
        if oqSQLScriptSearch.Eof then
        begin
          oqSQLScriptInsert.SetVariable('SCRIPTFILENAME', ScriptFileName);
          oqSQLScriptInsert.Execute;
        end
        else
        begin
          oqSQLScriptUpdate.SetVariable('SCRIPTFILENAME', ScriptFileName);
          oqSQLScriptUpdate.Execute;
        end;
      except
        AddLog('Cannot add to SQLSCRIPT-Table!');
      end;
    end;
  end;
  procedure UpdateSQLScriptTable;
  var
    IsSuccessFul: String;
  begin
    with HomeDM do
    begin
      if ScriptError then
        IsSuccessFul := 'N'
       else
        IsSuccessFul := 'Y';
      try
        oqSQLScriptUpdateState.SetVariable('SCRIPTFILENAME', ScriptFileName);
        oqSQLScriptUpdateState.SetVariable('ISSUCCESSFUL', IsSuccessFul);
        oqSQLScriptUpdateState.Execute;
      except
        AddLog('Cannot update SCRLSCRIPT-Table!');
      end;
    end;
  end;
  procedure AddMemoToScriptLogFile;
  begin
    Memo1.Lines.SaveToFile(
      PathCheck(PathCheck(SQLRootPath) + PIMS_SQL_LOG_DIR) +
        ChangeFileExt(ScriptFileName, '.log'));
  end;
begin
  FileName := '';
  ScriptNames := TStringList.Create;
  try
    if AskScriptNames then
    begin
      PimsBusy := True;
      Memo1.Lines.Clear;
      AddLog('Starting script(s)');
      Update;
      Application.ProcessMessages;
      Update;
      with HomeDM do
      begin
        ScriptError := False;
        // MR:11-11-2005
        // Temporary switch to user PIMS
        // Otherwise the scripts are not run correctly!
        OracleSession.Connected := False;
        OracleSession.LogonUsername := HomeDM.MyDatabaseUsername;
        OracleSession.LogonPassword := HomeDM.MyDatabasePassword;
        OracleSession.Connected := True;
        try
          // First check if table SQLSCRIPT exists, if not create it.
          try
            osCreateSQLScriptTable.Execute;
          except
            AddLog('Unknown error!');
          end;
          // Run one or more scripts
          for I := 0 to ScriptNames.Count - 1 do
          begin
            Memo1.Lines.Clear;
            FileName := ScriptNames[I];
            ScriptFileName := ExtractFileName(ScriptNames[I]);
            // Run the script.
            try
              AddSQLScriptTable;
              // This will result in a true or false for 'ScriptError'
              // using the Event 'OnError'.
              osSQLScript.Lines.LoadFromFile(FileName);
              osSQLScript.Execute;
            except
              ScriptError := True;
              AddLog('Unknown error!');
            end;
            AddMemoToLogFile;
            AddMemoToScriptLogFile;
            UpdateSQLScriptTable;
            if not ScriptError then
            begin
              AddLog('Ready');
              // Script was completed, save it to 'completed'-folder
              osSQLScript.Lines.SaveToFile(
                PathCheck(PathCheck(SQLRootPath) + PIMS_SQL_COMPLETED_DIR) +
                  ScriptFileName);
              // Delete script from 'sqlscripts'-folder
              DeleteFile(FileName);
            end
            else
            begin
              AddLog('Error during running the script');
              Break;
            end;
          end;
        finally
          OracleSession.Connected := False;
          // MR:11-11-2005
          // Switch back to user SYSTEM.
          OracleSession.LogonUsername := HomeDM.MyLogonUsername;
          OracleSession.LogonPassword := HomeDM.MyLogonPassword;
          PimsBusy := False;
        end;
      end;
    end;
  finally
    ScriptNames.Free;
  end;
end; // btnRunScriptClick

procedure THomeF.btnReindexClick(Sender: TObject);
var
  Line: String;
  Status: Integer;
begin
  if MessageDlg('Start Re-index?', mtConfirmation,
    [mbYes, mbNo], 0) = mrYes then
  begin
    try
      PimsBusy := True;
      ButtonsEnable(False);
      Memo1.Lines.Clear;
      AddLog('Starting re-index');
      Update;
//      WaitF := TWaitF.Create(Application);
//      WaitF.Show;
      Application.ProcessMessages;
      Update;
      with HomeDM do
      begin
        OracleSession.Connected := False;
        OracleSession.LogonUsername := HomeDM.MyDatabaseUsername;
        OracleSession.LogonPassword := HomeDM.MyDatabasePassword;
        OracleSession.Connected := True;
        if OracleSession.Connected then
        begin
          AddLog('Busy... Please wait...');
          StatusBar1.Panels[1].Text := 'Busy... Please wait...';
          Update;
          try
            OracleSession.DBMS_Output.Enable(100000);
            oqReindex.Execute;
            repeat
              OracleSession.DBMS_Output.Get_Line(Line, Status);
              if Status <> glSuccess then
                break;
              AddLog(Line);
            until False;
            OracleSession.DBMS_Output.Disable;
            AddLog('Ready');
          except
            AddLog('Error during re-index');
          end;
        end;
      end;
    finally
//      WaitF.Close;
//      WaitF.Free;
      with HomeDM do
      begin
        OracleSession.Connected := False;
        OracleSession.LogonUsername := HomeDM.MyLogonUsername;
        OracleSession.LogonPassword := HomeDM.MyLogonPassword;
      end;
      ButtonsEnable(True);
      StatusBar1.Panels[1].Text := '';
      Update;
      PimsBusy := False;
    end;
  end;
end; // btnReindexClick

procedure THomeF.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if PimsBusy then
  begin
    ShowMessage('System is busy... please wait.');
    Action := caNone;
    Exit;
  end;
  WriteIniFileMaintenance;
end;

procedure THomeF.DropUser;
var
  TmpDir: String;
  MyList: TStringList;
  CommandLine: String;
begin
  Mode := tmDropUser;
  try
    // SQLPLUS SYSTEM/ABSSYSTEM@ABS1 @DROPUSER.SQL
    TmpDir := GetTmpDir;
    SQLFile1 := PathCheck(TmpDir) + 'DROPUSER.SQL';
    LogFile := PathCheck(TmpDir) + 'DROPUSER.LOG';
    DeleteFile(LogFile);
    tmrBusy.Enabled := True;
    PimsBusy := True;
    AddLog('Drop User');
    ButtonsEnable(False);
    MyList := TStringList.Create;
    MyList.Add('SPOOL DROPUSER.LOG');
    MyList.Add('DROP USER ' + MyDatabaseUsername + ' CASCADE;');
    MyList.Add('SPOOL OFF');
    MyList.Add('EXIT');
    MyList.SaveToFile(SQLFile1);
    MyList.Free;
    CommandLine :=
      'SQLPLUS ' +
      AsSysdbaSingleQuoteStart(HomeDM.MyLogonUserName) +
      MyUserAndPassword +
      '@' +
      MyLogonDatabase +
      AsSysdba(HomeDM.MyLogonUserName) +
      AsSysdbaSingleQuoteEnd(HomeDM.MyLogonUserName) +
      ' @DROPUSER.SQL';
    CreateStartBatchFile(CommandLine);
    Update;
  except
    AddLog('Error during drop user');
  end;
end; // DropUser

procedure THomeF.CreateUser;
var
  TmpDir: String;
  MyList: TStringList;
  CommandLine: String;
begin
  Mode := tmCreateUser;
  try
    // SQLPLUS SYSTEM/ABSSYSTEM@ABS1 @CREATEUSER.SQL
    TmpDir := GetTmpDir;
    SQLFile1 := PathCheck(TmpDir) + 'CREATEUSER.SQL';
    SQLFile2 := PathCheck(TmpDir) + 'CREAPIMS.SQL';
    LogFile := PathCheck(TmpDir) + 'CREATEUSER.LOG';
    DeleteFile(LogFile);
    tmrBusy.Enabled := True;
    PimsBusy := True;
    AddLog('Create User');
    ButtonsEnable(False);
    MyList := TStringList.Create;
    MyList.Add('SPOOL CREATEUSER.LOG');
    MyList.Add('@CREAPIMS.SQL');
    MyList.Add('SPOOL OFF');
    MyList.Add('EXIT');
    MyList.SaveToFile(SQLFile1);
    MyList.Clear;
    MyList.Add('CREATE USER ' + MyDatabaseUsername +
      ' identified by ' + MyDatabasePassword);
    // PIM-403
    MyList.Add('default tablespace ABS_DATA temporary tablespace TEMP ');
    MyList.Add('profile DEFAULT; ');
    MyList.Add('GRANT CONNECT, RESOURCE TO ' + MyDatabaseUsername + ';');
    MyList.Add('GRANT EXP_FULL_DATABASE TO ' + MyDatabaseUsername + ';');
    MyList.Add('GRANT IMP_FULL_DATABASE TO ' + MyDatabaseUsername + ';');
    MyList.Add('grant create any job to ' + MyDatabaseUsername + ';');
    MyList.Add('grant debug connect session to ' + MyDatabaseUsername + ';');
    MyList.Add('grant execute any program to ' + MyDatabaseUsername + ';');
    MyList.Add('grant unlimited tablespace to ' + MyDatabaseUsername + ';');
    MyList.Add('ALTER USER ' + MyDatabaseUsername + ' DEFAULT ROLE ALL;');
    MyList.Add('GRANT SELECT ANY TABLE TO ' + MyDatabaseUsername + ';');
    MyList.Add('GRANT SELECT ANY DICTIONARY TO ' + MyDatabaseUsername + ';');
    // PIM-200:
    MyList.Add('ALTER USER ' + MyDatabaseUsername + ' QUOTA UNLIMITED ON ABS_DATA;');
    // 2.0.0.15 This can only be done by sys/abssys as sysdba:
    if IsSysDBA(HomeDM.MyLogonUserName) then
    begin
      MyList.Add('grant execute on dbms_scheduler to ' + MyDatabaseUsername + ';');
      MyList.Add('grant create any job to ' + MyDatabaseUsername + ';');
    end;
    MyList.SaveToFile(SQLFile2);
    MyList.Free;
    CommandLine :=
      'SQLPLUS ' +
      AsSysdbaSingleQuoteStart(HomeDM.MyLogonUserName) +
      MyUserAndPassword +
      '@' +
      MyLogonDatabase +
      AsSysdba(HomeDM.MyLogonUserName) +
      AsSysdbaSingleQuoteEnd(HomeDM.MyLogonUserName) +
      ' @CREATEUSER.SQL';
    CreateStartBatchFile(CommandLine);
    Update;
  except
    AddLog('Error during create user');
  end;
end; // CreateUser

procedure THomeF.btnDropCreateUserClick(Sender: TObject);
begin
  if MessageDlg('WARNING: This will delete your database!' + #13 + #13 +
    'Drop/Create User can take some time. Are you sure?',
    mtConfirmation, [mbYes, mbNo], 0) = mrYes then
  begin
    try
      DropUser;
      CreateUser;
    except
      AddLog('Error during Drop/Create user');
    end;
  end;
end;

procedure THomeF.btnDropUserClick(Sender: TObject);
begin
  if MessageDlg('DROP USER: ' + MyDatabaseUsername + #13 + #13 +
    'WARNING: This will DELETE your DATABASE!' + #13 + #13 +
    'Drop User ' + MyDatabaseUsername +
    ' can take some time. Are you sure?',
    mtConfirmation, [mbYes, mbNo], 0) = mrYes then
  begin
    try
      DropUser;
    except
      AddLog('Error during drop user');
    end;
  end;
end;

procedure THomeF.btnCreateUserClick(Sender: TObject);
begin
  if MessageDlg('CREATE USER: ' + MyDatabaseUsername + #13 + #13 +
    'Create User ' + MyDatabaseUsername +
    ' can take some time. Are you sure?',
    mtConfirmation, [mbYes, mbNo], 0) = mrYes then
  begin
    try
      CreateUser;
    except
      AddLog('Error during create user');
    end;
  end;
end;

{
impdp \'$1/$2@$3 as sysdba\' DUMPFILE=$4 LOGFILE=$5
  SCHEMAS=$1 REMAP_SCHEMA=$6:$1 TRANSFORM=oid:n  $7
Where:
$1   = USERSCHEMA to be imported (in context or DBA dialog)
$2  = PASSWORD (gathered from popup menu)
$3  = V_TNSNAMES_ENTRY (connection)
$4  = V_DUMPFILE  (name of dumpfile with ORACLE_DIRECTORY reference)
Example:  ABSDUMPDIR: ABSSOLUTE_AMM_abs2_v08.02.00_11.1_20090929_18h57_expdp.dmp
$5  = V_LOGFILE should be the same as $6 with _impdp.log extension
Example:  ABSDUMPDIR: ABSSOLUTE_AMM_abs2_v08.02.00_11.1_20090929_18h57_impdp.log
$6 = "From user" from dialog
When checkbox "Use parameter file" is set:
$7  = "PARFILE=value"(value is absolute path and par file from lookup)
When checkbox "Use parameter file" is not set: $7="" (empty)
}
procedure THomeF.btnImportDPClick(Sender: TObject);
var
  FileName, FromUser, DumpDir: String;
  CommandLine: String;
begin
  // 2.0.0.11.
{
  if not IsSys(HomeDM.MyLogonUserName) then
  begin
    AddLog('This can only be done by a user named sys with sysdba-rights.');
    Exit;
  end;
}  
  Mode := tmImport;
  // DumpDir: Folder where dumps are stored and defined in Oracle.
  DumpDir := PIMS_ABSDUMPDIR_DIRECTORYNAME;
  HomeDM.ABSDumpDir := PIMS_ABSDUMPDIR;
  HomeDM.Grantee := MyDatabaseUsername;
  if HomeDM.DirectoryGrantee(False) then
  begin
    DumpDir := HomeDM.DirectoryName;
  end;
{  else
  begin
    ShowMessage('Error: ' + #13 +
      'There is no backup location and permission set-up' + #13 +
      'for database user: ' + MyDatabaseUsername
      );
    Exit;
  end; }
{$IFDEF ABSMODE}
  FileName := 'Pims.dmp';
  FromUser := 'PIMS';
{$ELSE}
  FileName := 'Solar5.dmp';
  FromUser := 'SOLAR5';
{$ENDIF}
  try
    ImportDialogF := TImportDialogF.Create(Self);
    ImportDialogF.DumpDirectory := DumpDir;
    ImportDialogF.Filename := FileName;
    ImportDialogF.FromUser := FromUser;
    if ImportDialogF.ShowModal = mrOK then
    begin
      FileName := ImportDialogF.Filename;
      FromUser := ImportDialogF.FromUser;
      LogFile := 'importpims.log';
      DeleteFile(LogFile);
      tmrBusy.Enabled := True;
//      PimsBusy := True;
      AddLog('Import');
      ButtonsEnable(False);
      // 2.0.0.11. User/Password must be sys/abssys, not pims/pims
      //           Also add 'as sysbda'.
      // 2.0.0.11. - User/Password must be same as scheme to import!
      //             Instead of system/abssystem use: pims/pims
      //           - Schemas: Do not use this! This is already indicated
      //             by User/Password e.g. 'pims/pims'.
      CommandLine :=
        'IMPDP ' +
        AsSysdbaQuoteStart(HomeDM.MyLogonUserName) +
//        HomeDM.MyLogonUserName + '/' +
//        HomeDM.MyLogonPassword +
        HomeDM.MyDatabaseUserName + '/' +
        HomeDM.MyDatabasePassword +
        { MyUserAndPassword + }
        '@' + MyLogonDatabase +
        AsSysdba(HomeDM.MyLogonUserName) +
        AsSysdbaQuoteEnd(HomeDM.MyLogonUserName) +
        ' DIRECTORY=' + PIMS_ABSDUMPDIR +
        ' DUMPFILE=' + '"' + FileName + '"' +
        ' LOGFILE=' + '"' + LogFile + '"' +
//        ' SCHEMAS=' + MyDatabaseUsername +
        ' REMAP_SCHEMA=' + FromUser + ':'  + MyDatabaseUsername;
{$IFDEF ABSMODE}
  AddLog(CommandLine);
{$ENDIF}
{$IFDEF SOLARMODE}
  AddLog(CommandLine);
{$ENDIF}
      try
        HomeDM.SetORA64Env;
        if CheckBoxImportExportDPWithCMD.Checked then
          CreateStartBatchFile(CommandLine, True)
        else
          CreateStartBatchFile(CommandLine);
      finally
        HomeDM.SetORA32Env;
      end;
      Update;
    end;
  finally
    ImportDialogF.Free;
  end;
end; // btnImportDPClick

// EXPDP PIMS/PIMS@ABS1 SCHEMAS=PIMS
//   DIRECTORY=ABSDUMPDIR DUMPFILE=PIMS_CLF_2010_02_23.dmp
//   LOGFILE=PIMS_CLF_EXPORT_LOG.txt
procedure THomeF.btnExportDPClick(Sender: TObject);
var
  FileName, DumpFile: String;
  CommandLine: String;
  OptionExportTo10g: String;
  Ver10g: String;
begin
  // 2.0.0.11.
{
  if not IsSys(HomeDM.MyLogonUserName) then
  begin
    AddLog('This can only be done by a user named sys with sysdba-rights.');
    Exit;
  end;
}
  Mode := tmExport;
  if CheckBoxExportDPTo10g.Checked then
  begin
    OptionExportTo10g := ' VERSION=10.2';
    Ver10g := 'R10_';
  end
  else
  begin
    OptionExportTo10g := '';
    Ver10g := '';
  end;
  FileName := MyDatabaseUsername + '_' +
    Ver10G +
    HomeDM.PimsDBVersion + '_' +
    MyLogonDatabase + '_' +
    FormatDateTime('yyyymmdd_hhnn', Now) + '_DP.dmp';
  // Ask first export-filename
  if (InputQuery('Export'  , 'Enter filename for export-file', FileName)) then
  begin
    DumpFile := FileName;
    LogFile := 'exportpims.log';
    DeleteFile(LogFile);
    tmrBusy.Enabled := True;
//    PimsBusy := True;
    AddLog('Export');
    ButtonsEnable(False);
    // 2.0.0.11. User/Password must be sys/abssys, not pims/pims
    //           Also add 'as sysbda'.
    CommandLine :=
      'EXPDP ' +
      AsSysdbaQuoteStart(HomeDM.MyLogonUserName) +
      HomeDM.MyLogonUserName + '/' +
      HomeDM.MyLogonPassword +
      {MyUserAndPassword + }
      '@' + MyLogonDatabase +
      AsSysdba(HomeDM.MyLogonUserName) +
      AsSysdbaQuoteEnd(HomeDM.MyLogonUserName) +
      ' SCHEMAS=' + MyDatabaseUsername +
      OptionExportTo10g +
      ' DIRECTORY=' + PIMS_ABSDUMPDIR +
      ' DUMPFILE=' + '"' + DumpFile + '"' +
      ' LOGFILE=' + '"' + LogFile + '"';
{$IFDEF ABSMODE}
  AddLog(CommandLine);
{$ENDIF}
{$IFDEF SOLARMODE}
  AddLog(CommandLine);
{$ENDIF}
    try
      HomeDM.SetORA64Env;
      if CheckBoxImportExportDPWithCMD.Checked then
        CreateStartBatchFile(CommandLine, True)
      else
        CreateStartBatchFile(CommandLine);
    finally
      HomeDM.SetORA32Env;
    end;
    Update;
  end;
end; // btnExportDPClick

procedure THomeF.RunScriptLine(ATitle, AScriptLine: String;
  ASwitchUser: Boolean);
begin
  PimsBusy := True;
  AddLog(ATitle);
  Update;
  Application.ProcessMessages;
  Update;
  with HomeDM do
  begin
    try
      OracleSession.Connected := False;
      if ASwitchUser then
      begin
        // Switch to PIMSxxx user
        OracleSession.LogonUsername := HomeDM.MyDatabaseUsername;
        OracleSession.LogonPassword := HomeDM.MyDatabasePassword;
      end
      else
      begin
        // Switch to SYSTEM user
        OracleSession.LogonUsername := HomeDM.MyLogonUserName;
        OracleSession.LogonPassword := HomeDM.MyLogonPassword;
      end;
      OracleSession.Connected := True;
      try
        // This will result in a true or false for 'ScriptError'
        // using the Event 'OnError'.
        osSQLScript.Lines.Clear;
        osSQLScript.Lines.Add(AScriptLine);
        osSQLScript.Execute;
        AddLog('Ready.');
      except
        ScriptError := True;
        AddLog('Unknown error!');
      end; // try...except
    finally
      OracleSession.Connected := False;
      // Switch back to user SYSTEM.
      OracleSession.LogonUsername := HomeDM.MyLogonUserName;
      OracleSession.LogonPassword := HomeDM.MyLogonPassword;
      PimsBusy := False;
    end; // try...finally
  end; // with HomeDM do
end; // RunScriptLine

{
  CREATE OR REPLACE DIRECTORY $1 AS '$2';

	Where $1 = ORACLE_DIRECTORY name: ABSDUMPDIR
	Where $2 = directory location from dialog
}
procedure THomeF.btnSetBackupLocationClick(Sender: TObject);
var
  AbsDumpDir: String;
  ScriptLine: String;
begin
  AbsDumpDir := PIMS_ABSDUMPDIR_DIRECTORYNAME;
  if (InputQuery('Set Backup Location'  ,
    'Please define existing directory at database server', AbsDumpDir)) then
  begin
    ScriptLine :=
      'CREATE OR REPLACE DIRECTORY ' +
      PIMS_ABSDUMPDIR +
      ' AS ' +
      '''' + AbsDumpDir + '''' +
      ';';
    RunScriptLine('Set Backup Location', ScriptLine, True);
  end; // if (InputQuery...
end;

// GRANT READ, WRITE ON DIRECTORY ABSDUMPDIR TO PIMS;
procedure THomeF.btnSetBackupPermissionClick(Sender: TObject);
var
  ScriptLine: String;
begin
  ScriptLine := 'GRANT READ, WRITE ON DIRECTORY ' + PIMS_ABSDUMPDIR + ' TO ' +
    MyDatabaseUsername + ';';
  RunScriptLine('Set Backup Permission to ' + MyDatabaseUsername,
    ScriptLine, False);
  ScriptLine := 'GRANT READ, WRITE ON DIRECTORY ' + PIMS_ABSDUMPDIR + ' TO ' +
    HomeDM.MyLogonUsername + ';';
  RunScriptLine('Set Backup Permission to ' + HomeDM.MyLogonUsername,
    ScriptLine, True);
  // PIM-200
  ScriptLine := 'ALTER USER ' + MyDatabaseUsername + ' QUOTA UNLIMITED ON ABS_DATA;';
  RunScriptLine('Alter user permission for ' + MyDatabaseUsername,
    ScriptLine, True);
end;

procedure THomeF.btnCheckPermissionClick(Sender: TObject);
begin
  HomeDM.ABSDumpDir := PIMS_ABSDUMPDIR;
  HomeDM.Grantee := MyDatabaseUsername;
  HomeDM.DirectoryGrantee(True);
end;

procedure THomeF.btnScriptViewClick(Sender: TObject);
begin
  try
    try
      HomeDM.OracleSession.Connected := False;
      HomeDM.OracleSession.LogonUsername := HomeDM.MyDatabaseUsername;
      HomeDM.OracleSession.LogonPassword := HomeDM.MyDatabasePassword;
      HomeDM.OracleSession.Connected := True;
      if HomeDM.OracleSession.Connected then
      begin
        AddLog('<Script View Start>');
        with HomeDM.odsScriptView do
        begin
          Close;
          SQL.Clear;
          SQL.Add(
            'SELECT UPPER(S.SCRIPTNAME) SNAME, S.SCRIPTNAME, S.ISSUCCESSFUL, ' +
            'S.CREATIONDATE, S.MUTATIONDATE, S.MUTATOR ' +
            'FROM ' + HomeDM.SchemeName + 'SQLSCRIPT S ' +
            'ORDER BY 1'
             );
          Open;
          while not Eof do
          begin
            AddLog(FieldByName('SCRIPTNAME').AsString + ' | ' +
               FieldByName('ISSUCCESSFUL').AsString + ' | ' +
               DateTimeToStr(FieldByName('CREATIONDATE').AsDateTime) + ' | ' +
               DateTimeToStr(FieldByName('MUTATIONDATE').AsDateTime) + ' | ' +
               FieldByName('MUTATOR').AsString
               );
            Next;
          end;
        end;
        AddLog('<Script View End>');
      end;
    except
      Addlog('Warning: User/Database not found...');
    end;
  finally
    // Set back to system-user
    HomeDM.OracleSession.Connected := False;
    HomeDM.OracleSession.LogonUsername := HomeDM.MyLogonUsername;
    HomeDM.OracleSession.LogonPassword := HomeDM.MyLogonPassword;
  end;
end; // btnScriptViewClick

procedure THomeF.btnConvertUTF8Click(Sender: TObject);
var
  Counter: Integer;
begin
  if MessageDlg('Convert to UTF8 ?',
    mtConfirmation, [mbYes, mbNo], 0) = mrYes then
  begin
    AddLog('Converting to UTF8... Please wait...');
    with HomeDM do
    begin
      try
        // Switch to PIMSxxx user
        HomeDM.OracleSession.Connected := False;
        HomeDM.OracleSession.LogonUsername := HomeDM.MyDatabaseUsername;
        HomeDM.OracleSession.LogonPassword := HomeDM.MyDatabasePassword;
        HomeDM.OracleSession.Connected := True;
        if HomeDM.OracleSession.Connected then
        begin
          try
            osSQLScript.Lines.Clear;
            oqConvertUTF8.Execute;
            Counter := 0;
            while not oqConvertUTF8.Eof do
            begin
              osSQLScript.Lines.Add(oqConvertUTF8.FieldAsString('LINE1'));
              inc(Counter);
              oqConvertUTF8.Next;
            end;
            if Counter > 0 then
            begin
              osSQLScript.Execute;
              AddLog('Ready...');
            end
            else
              AddLog('Database was already converted...');
          except
            AddLog('Error during convert!');
          end;
        end;
      finally
        // Set back to system-user
        HomeDM.OracleSession.Connected := False;
        HomeDM.OracleSession.LogonUsername := HomeDM.MyLogonUsername;
        HomeDM.OracleSession.LogonPassword := HomeDM.MyLogonPassword;
      end;
    end;
  end;
end; // btnConvertUTF8Click

procedure THomeF.cmbDatabaseUsersChange(Sender: TObject);
begin
  ChangeDatabaseUser;
end;

procedure THomeF.DetermineDBVersion;
begin
  AddLog('Database: ' + MyLogonDatabase);
  Caption := Label1.Caption + '. Database: ' + MyLogonDatabase;
  // 2.0.0.16 Always show this
//  if IsAdmin then
  begin
    AddLog('DatabaseUsername: ' + MyDatabaseUsername);
    AddLog('DatabasePassword: ' + MyDatabasePassword);
  end;
  // Switch temporary from system-user to pims-user
  try
    try
      HomeDM.OracleSession.Connected := False;
      HomeDM.OracleSession.LogonUsername := HomeDM.MyDatabaseUsername;
      HomeDM.OracleSession.LogonPassword := HomeDM.MyDatabasePassword;
      HomeDM.OracleSession.Connected := True;
      if HomeDM.OracleSession.Connected then
      begin
        AddLog('Database version: ' + HomeDM.DBVersion);
        ReadPlants;
      end;
    except
      on E:EOracleError do
        Addlog(E.Message);
      on E:Exception do
      begin
        Addlog(E.Message);
        Addlog('Warning: User/Database not found...');
      end;
    end;
  finally
    // Set back to system-user
    HomeDM.OracleSession.Connected := False;
    HomeDM.OracleSession.LogonUsername := HomeDM.MyLogonUsername;
    HomeDM.OracleSession.LogonPassword := HomeDM.MyLogonPassword;
  end;
end; // DetermineDBVersion

procedure THomeF.ChangeDatabaseUser;
var
  DBUser, DBPassword: String;
  function DetermineUserPassword: Boolean;
  var
    Line: String;
    IPos: Integer;
  begin
    Result := False;
    if cmbDatabaseUsers.Items.Count > 0 then
      if cmbDatabaseUsers.ItemIndex <> -1 then
      begin
        Line := cmbDatabaseUsers.Items[cmbDatabaseUsers.ItemIndex];
        IPos := Pos(';', Line);
        if (IPos > 0) then
        begin
          DBUser := Copy(Line, 1, IPos - 1);
          DBPassword := Copy(Line, IPos + 1, Length(Line));
          Result := True;
        end;
      end;
  end;
begin
  if DetermineUserPassword then
  begin
    HomeDM.MyDatabaseUserName := DBUser;
    HomeDM.MyDatabasePassword := DBPassword;
    MyDatabaseUsername := HomeDM.MyDatabaseUserName;
    MyDatabasePassword := HomeDM.MyDatabasePassword;
    DetermineDBVersion;
  end;
end; // ChangeDatabaseUser

function THomeF.IsSysDBA(ACurrentUser: String): Boolean;
begin
  Result := HomeDM.IsSysDBA(ACurrentUser);
end;

function THomeF.IsSysOPER(ACurrentUser: String): Boolean;
begin
  Result := HomeDM.IsSysOPER(ACurrentUser);
end;

// 2.0.0.11.
function THomeF.AsSysdba(ACurrentUser: String): String;
begin
  if IsSysDBA(ACurrentUser) then
    Result := ' as sysdba'
  else
    if IsSysOPER(ACurrentUser) then
      Result := ' as sysoper'
    else
      Result := '';
end;

// 2.0.0.11.
function THomeF.AsSysdbaQuoteStart(ACurrentUser: String): String;
begin
  if IsSysDBA(ACurrentUser) or IsSysOPER(ACurrentUser) then
    Result := '"'''
  else
    Result := '';
end;

// 2.0.0.15.
function THomeF.AsSysdbaQuoteEnd(ACurrentUser: String): String;
begin
  if IsSysDBA(ACurrentUser) or IsSysOPER(ACurrentUser) then
    Result := '''"'
  else
    Result := '';
end;

// 2.0.0.15.
function THomeF.AsSysdbaSingleQuoteStart(ACurrentUser: String): String;
begin
  if IsSysDBA(ACurrentUser) or IsSysOPER(ACurrentUser) then
    Result := '"'
  else
    Result := '';
end;

// 2.0.0.15.
function THomeF.AsSysdbaSingleQuoteEnd(ACurrentUser: String): String;
begin
  if IsSysDBA(ACurrentUser) or IsSysOPER(ACurrentUser) then
    Result := '"'
  else
    Result := '';
end;

// 2.0.0.15 This can only be done by sys/abssys as sysdba:
procedure THomeF.btnGrantJobClick(Sender: TObject);
var
  TmpDir: String;
  MyList: TStringList;
  CommandLine: String;
begin
  Mode := tmGrantJob;
  try
    // SQLPLUS SYSTEM/ABSSYSTEM@ABS1 @GRANTJOB.SQL
    TmpDir := GetTmpDir;
    SQLFile1 := PathCheck(TmpDir) + 'GRANTJOB.SQL';
    SQLFile2 := PathCheck(TmpDir) + 'GRANTJOBPIMS.SQL';
    LogFile := PathCheck(TmpDir) + 'GRANTJOB.LOG';
    DeleteFile(LogFile);
    tmrBusy.Enabled := True;
    PimsBusy := True;
    AddLog('Grant Job');
    ButtonsEnable(False);
    MyList := TStringList.Create;
    MyList.Add('SPOOL GRANTJOB.LOG');
    MyList.Add('@GRANTJOBPIMS.SQL');
    MyList.Add('SPOOL OFF');
    MyList.Add('EXIT');
    MyList.SaveToFile(SQLFile1);
    MyList.Clear;
    MyList.Add('grant execute on dbms_scheduler to ' + MyDatabaseUsername + ';');
    MyList.Add('grant create any job to ' + MyDatabaseUsername + ';');
    MyList.SaveToFile(SQLFile2);
    MyList.Free;
    CommandLine :=
      'SQLPLUS ' +
      AsSysdbaSingleQuoteStart(HomeDM.MyLogonUserName) +
      MyUserAndPassword +
      '@' +
      MyLogonDatabase +
      AsSysdba(HomeDM.MyLogonUserName) +
      AsSysdbaSingleQuoteEnd(HomeDM.MyLogonUserName) +
      ' @GRANTJOB.SQL';
    CreateStartBatchFile(CommandLine);
    Update;
  except
    on E:EOracleError do
      Addlog(E.Message);
    on E:Exception do
      Addlog(E.Message);
  end;
end; // btnGrantJobClick

// 2.0.0.15
// 2.0.0.16 - PIM-142
procedure THomeF.FormShow(Sender: TObject);
begin
  if not IsSysDBA(HomeDM.MyLogonUserName) then
  begin
    btnGrantJob.Visible := False;
    btnResetJobQueue.Visible := False;
  end;
end;

// 2.0.0.16 - PIM-142
procedure THomeF.btnCheckJobClick(Sender: TObject);
begin
  try
    try
      HomeDM.OracleSession.Connected := False;
      HomeDM.OracleSession.LogonUsername := HomeDM.MyDatabaseUsername;
      HomeDM.OracleSession.LogonPassword := HomeDM.MyDatabasePassword;
      HomeDM.OracleSession.Connected := True;
      if HomeDM.OracleSession.Connected then
      begin
        AddLog('<Check Job Start>');
        with HomeDM.odsScriptView do
        begin
          Close;
          SQL.Clear;
{
select s.program_action, s.schedule_expr, s.start_date,
  s.last_enabled_time, s.job_status, s.next_run_date,
  s.last_start_date, s.last_end_date
from sys.scheduler$_job s
where s.program_action='PIMSCLO"CALC_EFFICIENCY_PROG'
}
          SQL.Add(
            'select s.program_action, s.schedule_expr, s.start_date, ' +
            '  s.last_enabled_time, s.job_status, s.next_run_date, ' +
            '  s.last_start_date, s.last_end_date ' +
            'from sys.scheduler$_job s ' +
            'where s.program_action=''' +
            HomeDM.MyDatabaseUsername + '"CALC_EFFICIENCY_PROG'' '
            );
          Open;
          if not Eof then
          begin
            AddLog('PROGRAM_ACTION | SCHEDULE_EXPR | START_DATE | LAST_ENABLED_TIME');
            AddLog('JOB_STATUS | NEXT_RUN_DATE | LAST_START_DATE | LAST_END_DATE');
            while not Eof do
            begin
              AddLog(
                FieldByName('PROGRAM_ACTION').AsString + ' | ' +
                FieldByName('SCHEDULE_EXPR').AsString + ' | ' +
                DateTimeToStr(FieldByName('START_DATE').AsDateTime) + ' | ' +
                DateTimeToStr(FieldByName('LAST_ENABLED_TIME').AsDateTime));
              AddLog(
                FieldByName('JOB_STATUS').AsString + ' | ' +
                DateTimeToStr(FieldByName('NEXT_RUN_DATE').AsDateTime) + ' | ' +
                DateTimeToStr(FieldByName('LAST_START_DATE').AsDateTime) + ' | ' +
                DateTimeToStr(FieldByName('LAST_END_DATE').AsDateTime)
                );
              Next;
            end;
          end
          else
            AddLog('Nothing found.');
        end;
        AddLog('<Check Job End>');
      end;
    except
      on E:EOracleError do
        Addlog(E.Message);
      on E:Exception do
        Addlog(E.Message);
    end;
  finally
    // Set back to system-user
    HomeDM.OracleSession.Connected := False;
    HomeDM.OracleSession.LogonUsername := HomeDM.MyLogonUsername;
    HomeDM.OracleSession.LogonPassword := HomeDM.MyLogonPassword;
  end;
end; // btnCheckJobClick

// 2.0.0.16 - PIM-142
procedure THomeF.btnCheckLogCalcEffClick(Sender: TObject);
begin
  try
    try
      HomeDM.OracleSession.Connected := False;
      HomeDM.OracleSession.LogonUsername := HomeDM.MyDatabaseUsername;
      HomeDM.OracleSession.LogonPassword := HomeDM.MyDatabasePassword;
      HomeDM.OracleSession.Connected := True;
      if HomeDM.OracleSession.Connected then
      begin
        AddLog('<Check Log Start>');
        with HomeDM.odsScriptView do
        begin
          Close;
          SQL.Clear;
{
select t.abslog_timestamp, t.logmessage
from abslog t
where t.abslog_timestamp >= trunc(sysdate) and t.logsource = 'CALC_EFFICIENCY' and rownum <= 10
order by 1 desc
}
          SQL.Add(
            'select t.abslog_timestamp, t.logmessage ' +
            'from ' + HomeDM.SchemeName + 'abslog t ' +
            'where t.abslog_timestamp >= trunc(sysdate) and ' +
            '  t.logsource = ''CALC_EFFICIENCY'' and rownum <= 10 ' +
            'order by 1 desc'
            );
          Open;
          if not Eof then
          begin
            AddLog('ABSLOG_TIMESTAMP | LOGMESSAGE');
            while not Eof do
            begin
              AddLog(
                DateTimeToStr(FieldByName('ABSLOG_TIMESTAMP').AsDateTime) + ' | ' +
                FieldByName('LOGMESSAGE').AsString
                );
              Next;
            end;
          end
          else
            AddLog('Nothing found.');
        end;
        AddLog('<Check Log End>');
      end;
    except
      on E:EOracleError do
        Addlog(E.Message);
      on E:Exception do
        Addlog(E.Message);
    end;
  finally
    // Set back to system-user
    HomeDM.OracleSession.Connected := False;
    HomeDM.OracleSession.LogonUsername := HomeDM.MyLogonUsername;
    HomeDM.OracleSession.LogonPassword := HomeDM.MyLogonPassword;
  end;
end; // btnCheckLogCalcEffClick

// 2.0.0.16 - PIM-142
procedure THomeF.btnCheckEffCalcPeriodClick(Sender: TObject);
begin
  try
    try
      HomeDM.OracleSession.Connected := False;
      HomeDM.OracleSession.LogonUsername := HomeDM.MyDatabaseUsername;
      HomeDM.OracleSession.LogonPassword := HomeDM.MyDatabasePassword;
      HomeDM.OracleSession.Connected := True;
      if HomeDM.OracleSession.Connected then
      begin
        AddLog('<Check Eff. calc period Start>');
        with HomeDM.odsScriptView do
        begin
          Close;
          SQL.Clear;
{
select t.plant_code, t.currintervalstarttime,
  t.currintervalendtime, t.lastintervalendtime
from efficiencycalcperiod t
}
          SQL.Add(
            'select t.plant_code, t.currintervalstarttime, ' +
            '  t.currintervalendtime, t.lastintervalendtime ' +
            'from ' + HomeDM.SchemeName + 'efficiencycalcperiod t'
            );
          Open;
          if not Eof then
          begin
            AddLog('PLANT_CODE | CURRINTERVALSTARTTIME | CURRINTERVALENDTIME | LASTINTERVALENDTIME');
            while not Eof do
            begin
              AddLog(
                FieldByName('PLANT_CODE').AsString + ' | ' +
                DateTimeToStr(FieldByName('CURRINTERVALSTARTTIME').AsDateTime) + ' | ' +
                DateTimeToStr(FieldByName('CURRINTERVALENDTIME').AsDateTime) + ' | ' +
                DateTimeToStr(FieldByName('LASTINTERVALENDTIME').AsDateTime)
                );
              Next;
            end;
          end
          else
            AddLog('Nothing found.');
        end;
        AddLog('<Check Eff. calc period End>');
      end;
    except
      on E:EOracleError do
        Addlog(E.Message);
      on E:Exception do
        Addlog(E.Message);
    end;
  finally
    // Set back to system-user
    HomeDM.OracleSession.Connected := False;
    HomeDM.OracleSession.LogonUsername := HomeDM.MyLogonUsername;
    HomeDM.OracleSession.LogonPassword := HomeDM.MyLogonPassword;
  end;
end; // btnCheckEffCalcPeriodClick

// 2.0.0.16 - PIM-142
procedure THomeF.btnResetJobQueueClick(Sender: TObject);
var
  TmpDir: String;
  MyList: TStringList;
  CommandLine: String;
begin
  if MessageDlg('Reset Job Queue?', mtConfirmation,
    [mbYes, mbNo], 0) = mrYes then
  begin
    Mode := tmResetJobQueue;
    try
      // SQLPLUS "SYS/ABSSYS@ABS1 AS SYSDBA" @RESETJOBQ.SQL
      TmpDir := GetTmpDir;
      SQLFile1 := PathCheck(TmpDir) + 'RESETJOBQ.SQL';
      SQLFile2 := PathCheck(TmpDir) + 'RESETJOBQPIMS.SQL';
      LogFile := PathCheck(TmpDir) + 'RESETJOBQ.LOG';
      DeleteFile(LogFile);
      tmrBusy.Enabled := True;
      PimsBusy := True;
      AddLog('Reset Job Queue');
      ButtonsEnable(False);
      MyList := TStringList.Create;
      MyList.Add('SPOOL RESETJOBQ.LOG');
      MyList.Add('@RESETJOBQPIMS.SQL');
      MyList.Add('SPOOL OFF');
      MyList.Add('EXIT');
      MyList.SaveToFile(SQLFile1);
      MyList.Clear;
{
exec dbms_scheduler.set_scheduler_attribute('SCHEDULER_DISABLED', 'TRUE');
alter system set job_queue_processes=0;
exec dbms_ijob.set_enabled(FALSE);
alter system flush shared_pool;
exec dbms_ijob.set_enabled(TRUE);
alter system set job_queue_processes=99;
exec dbms_scheduler.set_scheduler_attribute('SCHEDULER_DISABLED', 'FALSE');
}
      MyList.Add('exec dbms_scheduler.set_scheduler_attribute(''SCHEDULER_DISABLED'', ''TRUE'');');
      MyList.Add('alter system set job_queue_processes=0;');
      MyList.Add('exec dbms_ijob.set_enabled(FALSE);');
      MyList.Add('alter system flush shared_pool;');
      MyList.Add('exec dbms_ijob.set_enabled(TRUE);');
      MyList.Add('alter system set job_queue_processes=99;');
      MyList.Add('exec dbms_scheduler.set_scheduler_attribute(''SCHEDULER_DISABLED'', ''FALSE'');');
      MyList.SaveToFile(SQLFile2);
      MyList.Free;
      CommandLine :=
        'SQLPLUS ' +
        AsSysdbaSingleQuoteStart(HomeDM.MyLogonUserName) +
        MyUserAndPassword +
        '@' +
        MyLogonDatabase +
        AsSysdba(HomeDM.MyLogonUserName) +
        AsSysdbaSingleQuoteEnd(HomeDM.MyLogonUserName) +
        ' @RESETJOBQ.SQL';
      CreateStartBatchFile(CommandLine);
      Update;
    except
      on E:EOracleError do
        Addlog(E.Message);
      on E:Exception do
        AddLog(E.Message);
    end;
  end; // if MessageDlg
end; // btnResetJobQueueClick

// 2.0.0.16 - PIM-142
function THomeF.MyDatabaseUserAndPassword: String;
begin
  Result := HomeDM.MyDatabaseUsername + '/' +
    HomeDM.MyDatabasePassword;
end;

procedure THomeF.TurnOffJob;
var
  TmpDir: String;
  MyList: TStringList;
  CommandLine: String;
begin
  try
    try
      // SQLPLUS SYSTEM/ABSSYSTEM@ABS1 @DISABLEJOB.SQL
      TmpDir := GetTmpDir;
      SQLFile1 := PathCheck(TmpDir) + 'DISABLEJOB.SQL';
      SQLFile2 := PathCheck(TmpDir) + 'DISABLEJOBPIMS.SQL';
      LogFile := PathCheck(TmpDir) + 'DISABLEJOB.LOG';
      DeleteFile(LogFile);
      tmrBusy.Enabled := True;
      PimsBusy := True;
      AddLog('Turn Off Job');
      ButtonsEnable(False);
      MyList := TStringList.Create;
      MyList.Add('SPOOL DISABLEJOB.LOG');
      MyList.Add('@DISABLEJOBPIMS.SQL');
      MyList.Add('SPOOL OFF');
      MyList.Add('EXIT');
      MyList.SaveToFile(SQLFile1);
      MyList.Clear;
{
begin
dbms_scheduler.disable('CALC_EFFICIENCY_JOB');
end;
/
}
      MyList.Add('begin');
      MyList.Add('  dbms_scheduler.disable(' + JobPlantArg + ');');
      MyList.Add('end;');
      MyList.Add('/');
      MyList.SaveToFile(SQLFile2);
      MyList.Free;
      CommandLine :=
        'SQLPLUS ' +
        MyDatabaseUserAndPassword +
        '@' +
        MyLogonDatabase +
        ' @DISABLEJOB.SQL';
      CreateStartBatchFile(CommandLine);
      Update;
    except
      on E:EOracleError do
        Addlog(E.Message);
      on E:Exception do
        AddLog(E.Message);
    end;
  finally
  end;
end; // TurnOffJob

// 2.0.0.16 - PIM-142
procedure THomeF.btnTurnOffJobClick(Sender: TObject);
begin
  if MessageDlg('Turn Off Job?', mtConfirmation,
    [mbYes, mbNo], 0) = mrYes then
  begin
    Mode := tmTurnOffJob;
    TurnOffJob;
  end; // if MessageDlg
end; // btnTurnOffJobClick

procedure THomeF.TurnOnJob;
var
  TmpDir: String;
  MyList: TStringList;
  CommandLine: String;
begin
  try
    try
      // SQLPLUS SYSTEM/ABSSYSTEM@ABS1 @ENABLEJOB.SQL
      TmpDir := GetTmpDir;
      SQLFile1 := PathCheck(TmpDir) + 'ENABLEJOB.SQL';
      SQLFile2 := PathCheck(TmpDir) + 'ENABLEJOBPIMS.SQL';
      LogFile := PathCheck(TmpDir) + 'ENABLEJOB.LOG';
      DeleteFile(LogFile);
      tmrBusy.Enabled := True;
      PimsBusy := True;
      AddLog('Turn On Job');
      ButtonsEnable(False);
      MyList := TStringList.Create;
      MyList.Add('SPOOL ENABLEJOB.LOG');
      MyList.Add('@ENABLEJOBPIMS.SQL');
      MyList.Add('SPOOL OFF');
      MyList.Add('EXIT');
      MyList.SaveToFile(SQLFile1);
      MyList.Clear;
{
begin
dbms_scheduler.enable('CALC_EFFICIENCY_JOB');
end;
/
}
      MyList.Add('begin');
      MyList.Add('  dbms_scheduler.enable(' + JobPlantArg + ');');
      MyList.Add('end;');
      MyList.Add('/');
      MyList.SaveToFile(SQLFile2);
      MyList.Free;
      CommandLine :=
        'SQLPLUS ' +
        MyDatabaseUserAndPassword +
        '@' +
        MyLogonDatabase +
        ' @ENABLEJOB.SQL';
      CreateStartBatchFile(CommandLine);
      Update;
    except
      on E:EOracleError do
        Addlog(E.Message);
      on E:Exception do
        AddLog(E.Message);
    end;
  finally
  end;
end; // TurnOnJob

// 2.0.0.16 - PIM-142
procedure THomeF.btnTurnOnJobClick(Sender: TObject);
begin
  if MessageDlg('Turn On Job?', mtConfirmation,
    [mbYes, mbNo], 0) = mrYes then
  begin
    Mode := tmTurnOnJob;
    TurnOnJob;
  end; // if MessageDlg
end; // btnTurnOnJobClick

procedure THomeF.DropJob;
var
  TmpDir: String;
  MyList: TStringList;
  CommandLine: String;
begin
  try
    try
      // SQLPLUS SYSTEM/ABSSYSTEM@ABS1 @DROPJOB.SQL
      TmpDir := GetTmpDir;
      SQLFile1 := PathCheck(TmpDir) + 'DROPJOB.SQL';
      SQLFile2 := PathCheck(TmpDir) + 'DROPJOBPIMS.SQL';
      LogFile := PathCheck(TmpDir) + 'DROPJOB.LOG';
      DeleteFile(LogFile);
      tmrBusy.Enabled := True;
      PimsBusy := True;
      AddLog('Drop Job');
      ButtonsEnable(False);
      MyList := TStringList.Create;
      MyList.Add('SPOOL DROPJOB.LOG');
      MyList.Add('@DROPJOBPIMS.SQL');
      MyList.Add('SPOOL OFF');
      MyList.Add('EXIT');
      MyList.SaveToFile(SQLFile1);
      MyList.Clear;
{
begin
dbms_scheduler.disable('CALC_EFFICIENCY_JOB');
end;
/
begin
  dbms_scheduler.drop_job('CALC_EFFICIENCY_JOB');
end;
/
begin
dbms_scheduler.drop_program('CALC_EFFICIENCY_PROG');
end;
/
}
      MyList.Add('begin');
      MyList.Add('  dbms_scheduler.disable(' + JobPlantArg + ');');
      MyList.Add('end;');
      MyList.Add('/');
      MyList.Add('begin');
      MyList.Add('  dbms_scheduler.drop_job(' + JobPlantArg + ');');
      MyList.Add('end;');
      MyList.Add('/');
{
      MyList.Add('begin');
      MyList.Add('  dbms_scheduler.drop_program(''CALC_EFFICIENCY_PROG'');');
      MyList.Add('end;');
      MyList.Add('/');
}
      MyList.SaveToFile(SQLFile2);
      MyList.Free;
      CommandLine :=
        'SQLPLUS ' +
        MyDatabaseUserAndPassword +
        '@' +
        MyLogonDatabase +
        ' @DROPJOB.SQL';
      CreateStartBatchFile(CommandLine);
      Update;
    except
      on E:EOracleError do
        Addlog(E.Message);
      on E:Exception do
        AddLog(E.Message);
    end;
  finally
  end;
end; // DropJob

// 2.0.0.16 - PIM-142
procedure THomeF.btnDropJobClick(Sender: TObject);
begin
  if MessageDlg('Drop Job?', mtConfirmation,
    [mbYes, mbNo], 0) = mrYes then
  begin
    Mode := tmDropJob;
    DropJob;
  end; // if MessageDlg
end; // DropJobClick

procedure THomeF.CreateJob;
var
  TmpDir: String;
  MyList: TStringList;
  CommandLine: String;
begin
  try
    try
      // SQLPLUS SYSTEM/ABSSYSTEM@ABS1 @CREATEJOB.SQL
      TmpDir := GetTmpDir;
      SQLFile1 := PathCheck(TmpDir) + 'CREATEJOB.SQL';
      SQLFile2 := PathCheck(TmpDir) + 'CREATEJOBPIMS.SQL';
      LogFile := PathCheck(TmpDir) + 'CREATEJOB.LOG';
      DeleteFile(LogFile);
      tmrBusy.Enabled := True;
      PimsBusy := True;
      AddLog('Create Job');
      ButtonsEnable(False);
      MyList := TStringList.Create;
      MyList.Add('SPOOL CREATEJOB.LOG');
      MyList.Add('@CREATEJOBPIMS.SQL');
      MyList.Add('SPOOL OFF');
      MyList.Add('EXIT');
      MyList.SaveToFile(SQLFile1);
      MyList.Clear;
{
-- create a program with two arguments and define both
begin
dbms_scheduler.create_program
(
program_name=>'CALC_EFFICIENCY_PROG',
program_action=>'pims.CALC_EFFICIENCY',
program_type=>'STORED_PROCEDURE',
number_of_arguments=>1, enabled=>FALSE
);

dbms_scheduler.DEFINE_PROGRAM_ARGUMENT(
program_name=>'CALC_EFFICIENCY_PROG',
argument_position=>1,
argument_type=>'VARCHAR2', DEFAULT_VALUE=>'1');

-- In case it does not work:
-- should be?  ->   DEFAULT_VALUE=>1); --- without quotes?

dbms_scheduler.enable('CALC_EFFICIENCY_PROG');
end;
/

-- create a job pointing to a program and set both argument values
begin
dbms_scheduler.create_job('CALC_EFFICIENCY_JOB',program_name=>'CALC_EFFICIENCY_PROG',start_date =>sysdate,repeat_interval=>'FREQ=MINUTELY; BYSECOND=10',enabled=>TRUE);
dbms_scheduler.set_job_argument_value('CALC_EFFICIENCY_JOB',1,'1');
dbms_scheduler.enable('CALC_EFFICIENCY_JOB');
end;
/
}

{
      MyList.Add('-- create a program with two arguments and define both');
      MyList.Add('begin');
      MyList.Add('dbms_scheduler.create_program');
      MyList.Add('(');
      MyList.Add('program_name=>''CALC_EFFICIENCY_PROG'',');
      MyList.Add('program_action=>''' + HomeDM.MyDatabaseUserName + '.CALC_EFFICIENCY'',');
      MyList.Add('program_type=>''STORED_PROCEDURE'',');
      MyList.Add('number_of_arguments=>1, enabled=>FALSE');
      MyList.Add(');');

      MyList.Add('dbms_scheduler.DEFINE_PROGRAM_ARGUMENT(');
      MyList.Add('program_name=>''CALC_EFFICIENCY_PROG'',');
      MyList.Add('argument_position=>1,');
      MyList.Add('argument_type=>''VARCHAR2'', DEFAULT_VALUE=>''1'');');

      MyList.Add('-- In case it does not work:');
      MyList.Add('-- should be?  ->   DEFAULT_VALUE=>1); --- without quotes?');

      MyList.Add('dbms_scheduler.enable(''CALC_EFFICIENCY_PROG'');');
      MyList.Add('end;');
      MyList.Add('/');
}
      MyList.Add('-- create a job pointing to a program and set both argument values');
      MyList.Add('begin');
      MyList.Add('dbms_scheduler.create_job(' + JobPlantArg + ',program_name=>''CALC_EFFICIENCY_PROG'',start_date =>sysdate,repeat_interval=>''FREQ=MINUTELY; BYSECOND=10'',enabled=>TRUE);');
      MyList.Add('dbms_scheduler.set_job_argument_value(' + JobPlantArg + ',1,' + PlantArg + ');');
      MyList.Add('dbms_scheduler.enable(' + JobPlantArg + ');');
      MyList.Add('end;');
      MyList.Add('/');
      MyList.SaveToFile(SQLFile2);
      MyList.Free;
      CommandLine :=
        'SQLPLUS ' +
        MyDatabaseUserAndPassword +
        '@' +
        MyLogonDatabase +
        ' @CREATEJOB.SQL';
      CreateStartBatchFile(CommandLine);
      Update;
    except
      on E:EOracleError do
        Addlog(E.Message);
      on E:Exception do
        AddLog(E.Message);
    end;
  finally
  end;
end; // CreateJob

// 2.0.0.16 - PIM-142
procedure THomeF.btnCreateJobClick(Sender: TObject);
begin
  if MessageDlg('Create Job?', mtConfirmation,
    [mbYes, mbNo], 0) = mrYes then
  begin
    Mode := tmCreateJob;
    CreateJob;
  end;
end; // CreateJobClick

// PIM-158
function THomeF.PlantArg: String;
var
  PlantCodeArg: String;
begin
  PlantCodeArg := '1';
  if cmbPlants.Items.Count > 0 then
    PlantCodeArg := cmbPlants.Items.Strings[cmbPlants.ItemIndex];
  Result := '''' + PlantCodeArg + '''';
end;

// PIM-158
function THomeF.JobPlantArg: String;
var
  PlantCodeArg: String;
begin
  PlantCodeArg := '';
  if cmbPlants.Items.Count > 0 then
  begin
    // Skip the first one (that is at ItemIndex 0)
    if cmbPlants.ItemIndex > 0 then
      PlantCodeArg := cmbPlants.Items.Strings[cmbPlants.ItemIndex];
  end;
  if PlantCodeArg <> '' then
    Result := '''' + 'CALC_EFFICIENCY_JOB_' + PlantCodeArg + ''''
  else
    Result := '''' + 'CALC_EFFICIENCY_JOB' + '''';
end;

procedure THomeF.CreateProgram;
var
  TmpDir: String;
  MyList: TStringList;
  CommandLine: String;
begin
  try
    try
      // SQLPLUS SYSTEM/ABSSYSTEM@ABS1 @CREATEPROGRAM.SQL
      TmpDir := GetTmpDir;
      SQLFile1 := PathCheck(TmpDir) + 'CREATEPROGRAM.SQL';
      SQLFile2 := PathCheck(TmpDir) + 'CREATEPROGRAMPIMS.SQL';
      LogFile := PathCheck(TmpDir) + 'CREATEPROGRAM.LOG';
      DeleteFile(LogFile);
      tmrBusy.Enabled := True;
      PimsBusy := True;
      AddLog('Create Program');
      ButtonsEnable(False);
      MyList := TStringList.Create;
      MyList.Add('SPOOL CREATEPROGRAM.LOG');
      MyList.Add('@CREATEPROGRAMPIMS.SQL');
      MyList.Add('SPOOL OFF');
      MyList.Add('EXIT');
      MyList.SaveToFile(SQLFile1);
      MyList.Clear;

      MyList.Add('-- create a program with two arguments and define both');
      MyList.Add('begin');
      MyList.Add('dbms_scheduler.create_program');
      MyList.Add('(');
      MyList.Add('program_name=>''CALC_EFFICIENCY_PROG'',');
      MyList.Add('program_action=>''' + HomeDM.MyDatabaseUserName + '.CALC_EFFICIENCY'',');
      MyList.Add('program_type=>''STORED_PROCEDURE'',');
      MyList.Add('number_of_arguments=>1, enabled=>FALSE');
      MyList.Add(');');

      MyList.Add('dbms_scheduler.DEFINE_PROGRAM_ARGUMENT(');
      MyList.Add('program_name=>''CALC_EFFICIENCY_PROG'',');
      MyList.Add('argument_position=>1,');
      MyList.Add('argument_type=>''VARCHAR2'', DEFAULT_VALUE=>''1'');');

      MyList.Add('-- In case it does not work:');
      MyList.Add('-- should be?  ->   DEFAULT_VALUE=>1); --- without quotes?');

      MyList.Add('dbms_scheduler.enable(''CALC_EFFICIENCY_PROG'');');
      MyList.Add('end;');
      MyList.Add('/');
      MyList.SaveToFile(SQLFile2);
      MyList.Free;
      CommandLine :=
        'SQLPLUS ' +
        MyDatabaseUserAndPassword +
        '@' +
        MyLogonDatabase +
        ' @CREATEPROGRAM.SQL';
      CreateStartBatchFile(CommandLine);
      Update;
    except
      on E:EOracleError do
        Addlog(E.Message);
      on E:Exception do
        AddLog(E.Message);
    end;
  finally
  end;
end; // CreateProgram

procedure THomeF.btnCreateProgramClick(Sender: TObject);
begin
  if MessageDlg('Create Program?', mtConfirmation,
    [mbYes, mbNo], 0) = mrYes then
  begin
    Mode := tmCreateProgram;
    CreateProgram;
  end;
end; // btnCreateProgramClick

procedure THomeF.DropProgram;
var
  TmpDir: String;
  MyList: TStringList;
  CommandLine: String;
begin
  try
    try
      // SQLPLUS SYSTEM/ABSSYSTEM@ABS1 @DROPPROGRAM.SQL
      TmpDir := GetTmpDir;
      SQLFile1 := PathCheck(TmpDir) + 'DROPPROGRAM.SQL';
      SQLFile2 := PathCheck(TmpDir) + 'DROPPROGRAMPIMS.SQL';
      LogFile := PathCheck(TmpDir) + 'DROPPROGRAM.LOG';
      DeleteFile(LogFile);
      tmrBusy.Enabled := True;
      PimsBusy := True;
      AddLog('Drop Program');
      ButtonsEnable(False);
      MyList := TStringList.Create;
      MyList.Add('SPOOL DROPPROGRAM.LOG');
      MyList.Add('@DROPPROGRAMPIMS.SQL');
      MyList.Add('SPOOL OFF');
      MyList.Add('EXIT');
      MyList.SaveToFile(SQLFile1);
      MyList.Clear;

      MyList.Add('begin');
      MyList.Add('  dbms_scheduler.drop_program(''CALC_EFFICIENCY_PROG'');');
      MyList.Add('end;');
      MyList.Add('/');
      MyList.SaveToFile(SQLFile2);
      MyList.Free;
      CommandLine :=
        'SQLPLUS ' +
        MyDatabaseUserAndPassword +
        '@' +
        MyLogonDatabase +
        ' @DROPPROGRAM.SQL';
      CreateStartBatchFile(CommandLine);
      Update;
    except
      on E:EOracleError do
        Addlog(E.Message);
      on E:Exception do
        AddLog(E.Message);
    end;
  finally
  end;
end; // DropProgram

procedure THomeF.btnDropProgramClick(Sender: TObject);
begin
  if MessageDlg('Drop Program?', mtConfirmation,
    [mbYes, mbNo], 0) = mrYes then
  begin
    Mode := tmDropProgram;
    DropProgram;
  end; // if MessageDlg
end; // btnDropProgramClick

procedure THomeF.btnViewJobsClick(Sender: TObject);
begin
  try
    try
      HomeDM.OracleSession.Connected := False;
      HomeDM.OracleSession.LogonUsername := HomeDM.MyDatabaseUsername;
      HomeDM.OracleSession.LogonPassword := HomeDM.MyDatabasePassword;
      HomeDM.OracleSession.Connected := True;
      if HomeDM.OracleSession.Connected then
      begin
        AddLog('<View Jobs Start>');
        with HomeDM.odsScriptView do
        begin
          Close;
          SQL.Clear;
{
SELECT T.JOB_NAME, T.STATE, T.OWNER, T.program_name FROM DBA_SCHEDULER_JOBS T
WHERE T.OWNER = 'PIMSRSS';
}
          SQL.Add(
            '  SELECT T.JOB_NAME, T.STATE, T.OWNER, T.program_name ' +
            '  FROM DBA_SCHEDULER_JOBS T ' +
            '  WHERE T.OWNER =''' +  HomeDM.MyDatabaseUsername + ''''
            );
          Open;
          if not Eof then
          begin
            AddLog('JOB_NAME | STATE | OWNER | PROGRAM_NAME');
            AddLog('---------------------------------------');
            while not Eof do
            begin
              AddLog(
                FieldByName('JOB_NAME').AsString + ' | ' +
                FieldByName('STATE').AsString + ' | ' +
                FieldByName('OWNER').AsString + ' | ' +
                FieldByName('PROGRAM_NAME').AsString + ' | '
                );
              Next;
            end;
          end
          else
            AddLog('Nothing found.');
        end;
        AddLog('<View Jobs End>');
      end;
    except
      on E:EOracleError do
        Addlog(E.Message);
      on E:Exception do
        Addlog(E.Message);
    end;
  finally
    // Set back to system-user
    HomeDM.OracleSession.Connected := False;
    HomeDM.OracleSession.LogonUsername := HomeDM.MyLogonUsername;
    HomeDM.OracleSession.LogonPassword := HomeDM.MyLogonPassword;
  end;
end; // btnViewJobsClick

{ Datacol Import Files }

procedure THomeF.btnSetImportLocationClick(Sender: TObject);
var
  AbsImportDir: String;
  ScriptLine: String;
  function AddDir(ASourceDir, ADir: String): String;
  begin
    Result := ASourceDir;
    if ASourceDir <> '' then
    begin
      if ASourceDir[Length(ASourceDir)] <> '\' then
        Result := ASourceDir + '\' + ADir
      else
        Result := ASourceDir + ADir;
    end;
  end;
  // PIM-91.2 Create some files that are needed for the import-procedure
  procedure CreateFoldersAndFiles;
  var
    MyList: TStringList;
    MyPath: String;
  begin
    MyList := TStringList.Create;
    try
      // First create all needed folders
      try
        MyPath := AbsImportDir;
        if not DirectoryExists(MyPath) then
          ForceDirectories(MyPath);
        MyPath := AddDir(AbsImportDir, 'backup');
        if not DirectoryExists(MyPath) then
          ForceDirectories(MyPath);
        MyPath := AddDir(AbsImportDir, 'bin');
        if not DirectoryExists(MyPath) then
          ForceDirectories(MyPath);
      except
        on E:Exception do
          Addlog(E.Message);
      end;
      // dynamic_list_files.txt
      try
        MyList.Add('@echo off');
        MyList.Add('for /F %%X in (%1) do dir %%X');
        MyPath := AddDir(AbsImportDir, 'bin');
        if not DirectoryExists(MyPath) then
          ForceDirectories(MyPath);
        if DirectoryExists(MyPath) then
          MyList.SaveToFile(PathCheck(MyPath) + 'dynamic_list_files.bat')
        else
          AddLog('Datacol Import files: Cannot store file: dynamic_list_files.bat');
      except
        on E:Exception do
          Addlog(E.Message);
      end;
      // directories.txt
      try
        MyList.Clear;
        MyList.Add(AbsImportDir);
        MyPath := AbsImportDir;
        if not DirectoryExists(MyPath) then
          ForceDirectories(MyPath);
        if DirectoryExists(MyPath) then
          MyList.SaveToFile(PathCheck(MyPath) + 'directories.txt')
        else
          AddLog('Datacol Import files: Cannot store file: directories.txt');
      except
        on E:Exception do
          Addlog(E.Message);
      end;
    finally
      MyList.Free;
    end;
  end; // CreateFiles
begin
  AbsImportDir := PIMS_IMPORTDIR_DIRECTORYNAME;
  if (InputQuery('Set Import Location'  ,
    'Please define existing directory at database server', AbsImportDir)) then
  begin
    // Import Dir
    ScriptLine :=
      'CREATE OR REPLACE DIRECTORY ' +
      PIMS_IMPORTDIR +
      ' AS ' +
      '''' + AbsImportDir + '''' +
      ';';
    RunScriptLine('Set Import Location', ScriptLine, True);
    // Import Backup Dir
    ScriptLine :=
      'CREATE OR REPLACE DIRECTORY ' +
      PIMS_IMPORTBACKUPDIR +
      ' AS ' +
      '''' + AddDir(AbsImportDir, 'backup') + '''' +
      ';';
    RunScriptLine('Set Import Location', ScriptLine, True);
    // Import Bin Dir
    ScriptLine :=
      'CREATE OR REPLACE DIRECTORY ' +
      PIMS_IMPORTBINDIR +
      ' AS ' +
      '''' + AddDir(AbsImportDir, 'bin') + '''' +
      ';';
    RunScriptLine('Set Import Location', ScriptLine, True);
    // PIM-91.2
    CreateFoldersAndFiles;
  end; // if (InputQuery...
end; // btnSetImportLocationClick

procedure THomeF.btnSetImportPermissionClick(Sender: TObject);
var
  ScriptLine: String;
begin
  ScriptLine := 'GRANT READ, WRITE ON DIRECTORY ' + PIMS_IMPORTDIR + ' TO ' +
    MyDatabaseUsername + ';';
  RunScriptLine('Set Backup Permission to ' + MyDatabaseUsername,
    ScriptLine, False);
  ScriptLine := 'GRANT READ, WRITE ON DIRECTORY ' + PIMS_IMPORTBACKUPDIR + ' TO ' +
    HomeDM.MyLogonUsername + ';';
  RunScriptLine('Set Backup Permission to ' + HomeDM.MyLogonUsername,
    ScriptLine, True);
  ScriptLine := 'GRANT READ, WRITE ON DIRECTORY ' + PIMS_IMPORTBINDIR + ' TO ' +
    HomeDM.MyLogonUsername + ';';
  RunScriptLine('Set Backup Permission to ' + HomeDM.MyLogonUsername,
    ScriptLine, True);
  ScriptLine := 'GRANT EXECUTE ON DBMS_LOCK TO ' + HomeDM.MyLogonUsername + ';';
  RunScriptLine('Set Backup Permission to ' + HomeDM.MyLogonUsername,
    ScriptLine, True);
  // Also for public
  ScriptLine := 'GRANT READ, WRITE ON DIRECTORY ' + PIMS_IMPORTDIR + ' TO PUBLIC;';
  RunScriptLine('Set Backup Permission to PUBLIC', ScriptLine, False);
  ScriptLine := 'GRANT READ, WRITE ON DIRECTORY ' + PIMS_IMPORTBACKUPDIR + ' TO PUBLIC;';
  RunScriptLine('Set Backup Permission to PUBLIC', ScriptLine, True);
  ScriptLine := 'GRANT READ, WRITE ON DIRECTORY ' + PIMS_IMPORTBINDIR + ' TO PUBLIC;';
  RunScriptLine('Set Backup Permission to PUBLIC', ScriptLine, True);
  ScriptLine := 'GRANT EXECUTE ON DBMS_LOCK TO ' + HomeDM.MyLogonUsername + ';';
  RunScriptLine('Set Backup Permission to PUBLIC', ScriptLine, True);
end; // btnSetImportPermissionClick

procedure THomeF.CreateImportTable;
var
  TmpDir: String;
  MyList: TStringList;
  CommandLine: String;
begin
  try
    try
      // SQLPLUS SYSTEM/ABSSYSTEM@ABS1 @CREATEIMPORT.SQL
      TmpDir := GetTmpDir;
      SQLFile1 := PathCheck(TmpDir) + 'CREATEIMPORT.SQL';
      SQLFile2 := PathCheck(TmpDir) + 'CREATEIMPORTPIMS.SQL';
      LogFile := PathCheck(TmpDir) + 'CREATEIMPORT.LOG';
      DeleteFile(LogFile);
      tmrBusy.Enabled := True;
      PimsBusy := True;
      AddLog('Create Import Table');
      ButtonsEnable(False);
      MyList := TStringList.Create;
      MyList.Add('SPOOL CREATEIMPORT.LOG');
      MyList.Add('@CREATEIMPORTPIMS.SQL');
      MyList.Add('SPOOL OFF');
      MyList.Add('EXIT');
      MyList.SaveToFile(SQLFile1);
      MyList.Clear;

      MyList.Add('-- Create external table');
      MyList.Add('DROP TABLE pimsimpfiles_xt;');
      MyList.Add('CREATE TABLE pimsimpfiles_xt');
      MyList.Add('( file_date VARCHAR2(50)');
      MyList.Add(', file_time VARCHAR2(50)');
      MyList.Add(', file_size VARCHAR2(50)');
      MyList.Add(', file_name VARCHAR2(255)');
      MyList.Add(')');
      MyList.Add('ORGANIZATION EXTERNAL');
      MyList.Add('(');
      MyList.Add('  TYPE ORACLE_LOADER');
      MyList.Add('  DEFAULT DIRECTORY pimsimp_dir');
      MyList.Add('  ACCESS PARAMETERS');
      MyList.Add('  (');
      MyList.Add('     RECORDS DELIMITED by NEWLINE');
      MyList.Add('     LOAD WHEN file_size != ''<DIR>'' ');
      MyList.Add('     PREPROCESSOR pimsimpbin_dir: ''dynamic_list_files.bat''');
      MyList.Add('     FIELDS TERMINATED BY WHITESPACE');
      MyList.Add('  )');
      MyList.Add('  LOCATION (''directories.txt'')');
      MyList.Add(')');
      MyList.Add('REJECT LIMIT UNLIMITED;');

      MyList.Add('-- Create/Change the view if needed');
      MyList.Add('-- Here we only need files with extension .ASC');
      MyList.Add('CREATE OR REPLACE VIEW pimsimpfiles_vxt');
      MyList.Add('AS');
      MyList.Add('   SELECT file_name');
      MyList.Add('   ,      TO_DATE(');
      MyList.Add('             file_date||'',''||file_time,');
      MyList.Add('             ''DD-MM-YYYY HH24:MI'') AS file_time');
      MyList.Add('   ,      TO_NUMBER(');
      MyList.Add('             file_size,');
      MyList.Add('             ''fm999,999,999,999'') AS file_size');
      MyList.Add('   FROM   pimsimpfiles_xt');
      MyList.Add('   WHERE  file_name like ''%.ASC'';');

      MyList.Add('/');
      
      MyList.SaveToFile(SQLFile2);
      MyList.Free;
      CommandLine :=
        'SQLPLUS ' +
        MyDatabaseUserAndPassword +
        '@' +
        MyLogonDatabase +
        ' @CREATEIMPORT.SQL';
      CreateStartBatchFile(CommandLine);
      Update;
    except
      on E:EOracleError do
        Addlog(E.Message);
      on E:Exception do
        AddLog(E.Message);
    end;
  finally
  end;
end; // CreateImportTable

procedure THomeF.btnCreateImportTableClick(Sender: TObject);
begin
  if MessageDlg('Create Import Table?', mtConfirmation,
    [mbYes, mbNo], 0) = mrYes then
  begin
    Mode := tmCreateImportTable;
    CreateImportTable;
  end;
end; // btnCreateImportTableClick

procedure THomeF.CreateImportJob;
var
  TmpDir: String;
  MyList: TStringList;
  CommandLine: String;
begin
  try
    try
      // SQLPLUS SYSTEM/ABSSYSTEM@ABS1 @CREATEIMPORTJOB.SQL
      TmpDir := GetTmpDir;
      SQLFile1 := PathCheck(TmpDir) + 'CREATEIMPORTJOB.SQL';
      SQLFile2 := PathCheck(TmpDir) + 'CREATEIMPORTJOBPIMS.SQL';
      LogFile := PathCheck(TmpDir) + 'CREATEIMPORTJOB.LOG';
      DeleteFile(LogFile);
      tmrBusy.Enabled := True;
      PimsBusy := True;
      AddLog('Create Job');
      ButtonsEnable(False);
      MyList := TStringList.Create;
      MyList.Add('SPOOL CREATEIMPORTJOB.LOG');
      MyList.Add('@CREATEIMPORTJOBPIMS.SQL');
      MyList.Add('SPOOL OFF');
      MyList.Add('EXIT');
      MyList.SaveToFile(SQLFile1);
      MyList.Clear;

      MyList.Add('');

      MyList.Add('-- Alternative way to create a job: This worked!');
      MyList.Add('BEGIN');
      MyList.Add('DBMS_SCHEDULER.CREATE_JOB (');
      MyList.Add('   job_name             => ''' + PIMS_IMPORTJOB + ''',');
      MyList.Add('   job_type             => ''PLSQL_BLOCK'',');
      MyList.Add('   job_action           => ''BEGIN ' + MyDatabaseUsername + '.PACKAGE_DATACOLLECTION.READPROCESSFILES; END;'',');
      MyList.Add('   start_date           => sysdate,');
      MyList.Add('   repeat_interval      => ''FREQ=MINUTELY; BYSECOND=0'',');
      MyList.Add('   enabled              =>  TRUE,');
      MyList.Add('   comments             => ''Pims Import from files'');');
      MyList.Add('END;');
      MyList.Add('/');

      MyList.SaveToFile(SQLFile2);
      MyList.Free;
      CommandLine :=
        'SQLPLUS ' +
        MyDatabaseUserAndPassword +
        '@' +
        MyLogonDatabase +
        ' @CREATEIMPORTJOB.SQL';
      CreateStartBatchFile(CommandLine);
      Update;
    except
      on E:EOracleError do
        Addlog(E.Message);
      on E:Exception do
        AddLog(E.Message);
    end;
  finally
  end;
end; // CreateImportJob

procedure THomeF.btnCreateImportJobClick(Sender: TObject);
begin
  if MessageDlg('Create Import Job?', mtConfirmation,
    [mbYes, mbNo], 0) = mrYes then
  begin
    Mode := tmCreateImportJob;
    CreateImportJob;
  end;
end; // btnCreateImportJobClick

procedure THomeF.DropImportJob;
var
  TmpDir: String;
  MyList: TStringList;
  CommandLine: String;
  JobArg: String;
begin
  JobArg := '''' + PIMS_IMPORTJOB + '''';
  try
    try
      // SQLPLUS SYSTEM/ABSSYSTEM@ABS1 @DROPIMPORTJOB.SQL
      TmpDir := GetTmpDir;
      SQLFile1 := PathCheck(TmpDir) + 'DROPIMPORTJOB.SQL';
      SQLFile2 := PathCheck(TmpDir) + 'DROPIMPORTJOBPIMS.SQL';
      LogFile := PathCheck(TmpDir) + 'DROPIMPORTJOB.LOG';
      DeleteFile(LogFile);
      tmrBusy.Enabled := True;
      PimsBusy := True;
      AddLog('Drop Job');
      ButtonsEnable(False);
      MyList := TStringList.Create;
      MyList.Add('SPOOL DROPIMPORTJOB.LOG');
      MyList.Add('@DROPIMPORTJOBPIMS.SQL');
      MyList.Add('SPOOL OFF');
      MyList.Add('EXIT');
      MyList.SaveToFile(SQLFile1);
      MyList.Clear;
{
begin
dbms_scheduler.disable('PIMS_IMPORT_JOB');
end;
/
begin
  dbms_scheduler.drop_job('PIMS_IMPORT_JOB');
end;
/
}
      MyList.Add('begin');
      MyList.Add('  dbms_scheduler.disable(' + JobArg + ');');
      MyList.Add('end;');
      MyList.Add('/');
      MyList.Add('begin');
      MyList.Add('  dbms_scheduler.drop_job(' + JobArg + ');');
      MyList.Add('end;');
      MyList.Add('/');

      MyList.SaveToFile(SQLFile2);
      MyList.Free;
      CommandLine :=
        'SQLPLUS ' +
        MyDatabaseUserAndPassword +
        '@' +
        MyLogonDatabase +
        ' @DROPIMPORTJOB.SQL';
      CreateStartBatchFile(CommandLine);
      Update;
    except
      on E:EOracleError do
        Addlog(E.Message);
      on E:Exception do
        AddLog(E.Message);
    end;
  finally
  end;
end; // DropImportJob

procedure THomeF.btnDropImportJobClick(Sender: TObject);
begin
  if MessageDlg('Drop Import Job?', mtConfirmation,
    [mbYes, mbNo], 0) = mrYes then
  begin
    Mode := tmDropImportJob;
    DropImportJob;
  end; // if MessageDlg
end; // btnDropImportJobClick

procedure THomeF.btnViewImportJobClick(Sender: TObject);
begin
  try
    try
      HomeDM.OracleSession.Connected := False;
      HomeDM.OracleSession.LogonUsername := HomeDM.MyDatabaseUsername;
      HomeDM.OracleSession.LogonPassword := HomeDM.MyDatabasePassword;
      HomeDM.OracleSession.Connected := True;
      if HomeDM.OracleSession.Connected then
      begin
        AddLog('<View Import Job Start>');
        with HomeDM.odsScriptView do
        begin
          Close;
          SQL.Clear;
{
SELECT T.JOB_NAME, T.STATE, T.OWNER, T.program_name FROM DBA_SCHEDULER_JOBS T
WHERE T.OWNER = 'PIMSRSS';
}

          SQL.Add(
            '  SELECT T.JOB_NAME, T.STATE, T.OWNER, T.LAST_START_DATE, T.NEXT_RUN_DATE ' +
            '  FROM DBA_SCHEDULER_JOBS T ' +
            '  WHERE T.JOB_NAME = ''' + PIMS_IMPORTJOB + '''' +
            '  AND T.OWNER = ''' + HomeDM.MyDatabaseUsername + ''''
            );
          Open;
          if not Eof then
          begin
            AddLog('JOB_NAME | STATE | OWNER | LAST_START_DATE | NEXT_RUN_DATE');
            AddLog('----------------------------------------------------------');
            while not Eof do
            begin
              AddLog(
                FieldByName('JOB_NAME').AsString + ' | ' +
                FieldByName('STATE').AsString + ' | ' +
                FieldByName('OWNER').AsString + ' | ' +
                FieldByName('LAST_START_DATE').AsString + ' | ' +
                FieldByName('NEXT_RUN_DATE').AsString
                );
              Next;
            end;
          end
          else
            AddLog('Nothing found.');
        end;
        AddLog('<View Import Job End>');
      end;
    except
      on E:EOracleError do
        Addlog(E.Message);
      on E:Exception do
        Addlog(E.Message);
    end;
  finally
    // Set back to system-user
    HomeDM.OracleSession.Connected := False;
    HomeDM.OracleSession.LogonUsername := HomeDM.MyLogonUsername;
    HomeDM.OracleSession.LogonPassword := HomeDM.MyLogonPassword;
  end;
end; // btnViewImportJobClick

procedure THomeF.btnCheckImportPermissionClick(Sender: TObject);
begin
  HomeDM.ABSDumpDir := PIMS_IMPORTDIR;
  HomeDM.Grantee := MyDatabaseUsername;
  HomeDM.DirectoryGrantee(True);
  HomeDM.ABSDumpDir := PIMS_IMPORTBACKUPDIR;
  HomeDM.Grantee := MyDatabaseUsername;
  HomeDM.DirectoryGrantee(True);
  HomeDM.ABSDumpDir := PIMS_IMPORTBINDIR;
  HomeDM.Grantee := MyDatabaseUsername;
  HomeDM.DirectoryGrantee(True);
end; // btnCheckImportPermissionClick

procedure THomeF.btnViewImportLogClick(Sender: TObject);
begin
  try
    try
      HomeDM.OracleSession.Connected := False;
      HomeDM.OracleSession.LogonUsername := HomeDM.MyDatabaseUsername;
      HomeDM.OracleSession.LogonPassword := HomeDM.MyDatabasePassword;
      HomeDM.OracleSession.Connected := True;
      if HomeDM.OracleSession.Connected then
      begin
        AddLog('<View Import Log Start>');
        with HomeDM.odsScriptView do
        begin
          Close;
          SQL.Clear;
{
select t.abslog_timestamp, t.logmessage
from abslog t
where t.abslog_timestamp >= trunc(sysdate) and t.logsource = 'CALC_EFFICIENCY' and rownum <= 10
order by 1 desc
}
          SQL.Add(
            'select t.abslog_timestamp, t.logmessage ' +
            'from ' + HomeDM.SchemeName + 'abslog t ' +
            'where t.abslog_timestamp >= trunc(sysdate) and ' +
            '  t.logsource = ''' + PIMS_IMPORTPACKAGE + ''' and rownum <= 10 ' +
            'order by 1 desc'
            );
          Open;
          if not Eof then
          begin
            AddLog('ABSLOG_TIMESTAMP | LOGMESSAGE');
            AddLog('-----------------------------');
            while not Eof do
            begin
              AddLog(
                DateTimeToStr(FieldByName('ABSLOG_TIMESTAMP').AsDateTime) + ' | ' +
                FieldByName('LOGMESSAGE').AsString
                );
              Next;
            end;
          end
          else
            AddLog('Nothing found.');
        end;
        AddLog('<View Import Log End>');
      end;
    except
      on E:EOracleError do
        Addlog(E.Message);
      on E:Exception do
        Addlog(E.Message);
    end;
  finally
    // Set back to system-user
    HomeDM.OracleSession.Connected := False;
    HomeDM.OracleSession.LogonUsername := HomeDM.MyLogonUsername;
    HomeDM.OracleSession.LogonPassword := HomeDM.MyLogonPassword;
  end;
end; // btnViewImportLogClick

procedure THomeF.btnViewLogHybridClick(Sender: TObject);
begin
  try
    try
      HomeDM.OracleSession.Connected := False;
      HomeDM.OracleSession.LogonUsername := HomeDM.MyDatabaseUsername;
      HomeDM.OracleSession.LogonPassword := HomeDM.MyDatabasePassword;
      HomeDM.OracleSession.Connected := True;
      if HomeDM.OracleSession.Connected then
      begin
        AddLog('<View Log Start>');
        with HomeDM.odsScriptView do
        begin
          Close;
          SQL.Clear;
{
select t.abslog_timestamp, t.computer_name, t.logmessage
from abslog t
where t.abslog_timestamp >= trunc(sysdate) and t.logsource = 'package_cockpit' and rownum <= 10
order by 1 desc
}
          SQL.Add(
            'select t.abslog_timestamp, t.computer_name, t.logmessage ' +
            'from (select * from ' + HomeDM.SchemeName + 'abslog ' +
            'where abslog_timestamp >= trunc(sysdate) and ' +
            '  logsource = ''' + PIMS_PACKAGE_COCKPIT + ''' order by 1 desc) t ' +
            'where rownum <= 10 '
            );
          Open;
          if not Eof then
          begin
            AddLog('ABSLOG_TIMESTAMP | COMPUTER_NAME | LOGMESSAGE');
            AddLog('---------------------------------------------');
            while not Eof do
            begin
              AddLog(
                DateTimeToStr(FieldByName('ABSLOG_TIMESTAMP').AsDateTime) + ' | ' +
                FieldByName('COMPUTER_NAME').AsString + ' | ' +
                FieldByName('LOGMESSAGE').AsString
                );
              Next;
            end;
          end
          else
            AddLog('Nothing found.');
        end;
        AddLog('<View Log End>');
      end;
    except
      on E:EOracleError do
        Addlog(E.Message);
      on E:Exception do
        Addlog(E.Message);
    end;
  finally
    // Set back to system-user
    HomeDM.OracleSession.Connected := False;
    HomeDM.OracleSession.LogonUsername := HomeDM.MyLogonUsername;
    HomeDM.OracleSession.LogonPassword := HomeDM.MyLogonPassword;
  end;
end; // btnViewLogHybridClick

procedure THomeF.btnViewJobsHybridClick(Sender: TObject);
begin
  try
    try
      HomeDM.OracleSession.Connected := False;
      HomeDM.OracleSession.LogonUsername := HomeDM.MyDatabaseUsername;
      HomeDM.OracleSession.LogonPassword := HomeDM.MyDatabasePassword;
      HomeDM.OracleSession.Connected := True;
      if HomeDM.OracleSession.Connected then
      begin
        AddLog('<View Job Start>');
        with HomeDM.odsScriptView do
        begin
          Close;
          SQL.Clear;
{
SELECT T.JOB_NAME, T.STATE, T.OWNER, T.program_name FROM DBA_SCHEDULER_JOBS T
WHERE T.OWNER = 'PIMSRSS';
}
          SQL.Add(
            '  SELECT T.JOB_NAME, T.STATE, T.OWNER, T.LAST_START_DATE, T.NEXT_RUN_DATE ' +
            '  FROM DBA_SCHEDULER_JOBS T ' +
            '  WHERE ' +
            '    (' +
            '       (T.JOB_NAME LIKE ' + '''' + '%COCKPIT_JOB' + '''' + ') ' +
            '       OR ' +
            '       (T.JOB_NAME LIKE ' + '''' + '%PIMS_JOB' + '''' + ') ' +
            '    ) ' +
            '    AND ' +
            '    T.OWNER = ''' +  HomeDM.MyDatabaseUsername + ''''
            );
          Open;
          if not Eof then
          begin
            AddLog('JOB_NAME | STATE | OWNER | LAST_START_DATE | NEXT_RUN_DATE');
            AddLog('----------------------------------------------------------');
            while not Eof do
            begin
              AddLog(
                FieldByName('JOB_NAME').AsString + ' | ' +
                FieldByName('STATE').AsString + ' | ' +
                FieldByName('OWNER').AsString + ' | ' +
                FieldByName('LAST_START_DATE').AsString + ' | ' +
                FieldByName('NEXT_RUN_DATE').AsString
                );
              Next;
            end;
          end
          else
            AddLog('Nothing found.');
        end;
        AddLog('<View Job End>');
      end;
    except
      on E:EOracleError do
        Addlog(E.Message);
      on E:Exception do
        Addlog(E.Message);
    end;
  finally
    // Set back to system-user
    HomeDM.OracleSession.Connected := False;
    HomeDM.OracleSession.LogonUsername := HomeDM.MyLogonUsername;
    HomeDM.OracleSession.LogonPassword := HomeDM.MyLogonPassword;
  end;
end; // btnViewJobsHybridClick

procedure THomeF.btnViewRemoteOperatorsClick(Sender: TObject);
begin
  try
    try
      HomeDM.OracleSession.Connected := False;
      HomeDM.OracleSession.LogonUsername := HomeDM.MyDatabaseUsername;
      HomeDM.OracleSession.LogonPassword := HomeDM.MyDatabasePassword;
      HomeDM.OracleSession.Connected := True;
      if HomeDM.OracleSession.Connected then
      begin
        AddLog('<View Remote Operators Start>');
        with HomeDM.odsScriptView do
        begin
          Close;
          SQL.Clear;
{
SELECT T.JOB_NAME, T.STATE, T.OWNER, T.program_name FROM DBA_SCHEDULER_JOBS T
WHERE T.OWNER = 'PIMSRSS';
}
// TO_CHAR(T."LOGTIME",'DD-MM-YYY RemoteOperatorLogID LogTime       MachineID  OperatorID       SubID       State jgLogDataRemoteID UpdateTime   SubregType SubregTypeID
          SQL.Add(
            ' select to_char(t."LogTime", ''dd-mm-yyyy hh24:mi:ss'') LogTime, ' +
            ' t."MachineID", t."SubID", t."ShortDescription", t."OperatorID", t.Name, t."State", ' +
            ' case t."State" ' +
            '   when 1 then ''IN'' else ''OUT'' ' +
            ' end State2, ' +
            ' t."jgLogDataRemoteID", t."UpdateTime", t."SubregType", ' +
            ' t."SubregTypeID" ' +
            ' from (select a.*, b."ShortDescription" as Name, c."ShortDescription" from tblRemoteOperatorLog@gtlab a inner join tblOperators@gtlab b on a."OperatorID" = b."idJensen" inner join tblMachines@gtlab c on a."MachineID" = c."idJensen" order by 1 desc) t ' +
            ' where rownum <= 10 '
            );
// SQL.SaveToFile('c:\temp\ViewRemoteOperators.sql');
          Open;
          if not Eof then
          begin
            AddLog('LogTime       MachineID  SubID OperatorID              State jgLogDataRemoteID UpdateTime   SubregType SubregTypeID');
            AddLog('------------------------------------------------------------------------------------------------------------------');
            while not Eof do
            begin
              AddLog(
                FieldByName('LogTime').AsString + ' | ' +
                FieldByName('MachineID').AsString + ' | ' +
                FieldByName('SubID').AsString + ' | ' +
                FieldByName('ShortDescription').AsString + ' | ' +
                FieldByName('OperatorID').AsString + ' | ' +
                FieldByName('Name').AsString + ' | ' +
                FieldByName('State').AsString + ' | ' +
                FieldByName('State2').AsString + ' | ' +
                FieldByName('jgLogDataRemoteID').AsString + ' | ' +
                FieldByName('UpdateTime').AsString + ' | ' +
                FieldByName('SubregType').AsString + ' | ' +
                FieldByName('SubregTypeID').AsString
                );
              Next;
            end;
          end
          else
            AddLog('Nothing found.');
        end;
        AddLog('<View Remote Operators End>');
      end;
    except
      on E:EOracleError do
        Addlog(E.Message);
      on E:Exception do
        Addlog(E.Message);
    end;
  finally
    // Set back to system-user
    HomeDM.OracleSession.Connected := False;
    HomeDM.OracleSession.LogonUsername := HomeDM.MyLogonUsername;
    HomeDM.OracleSession.LogonPassword := HomeDM.MyLogonPassword;
  end;
end; // btnViewRemoteOperatorsClick

procedure THomeF.btnViewEmployeeScansClick(Sender: TObject);
begin
  try
    try
      HomeDM.OracleSession.Connected := False;
      HomeDM.OracleSession.LogonUsername := HomeDM.MyDatabaseUsername;
      HomeDM.OracleSession.LogonPassword := HomeDM.MyDatabasePassword;
      HomeDM.OracleSession.Connected := True;
      if HomeDM.OracleSession.Connected then
      begin
        AddLog('<View Employee Scans Start>');
        with HomeDM.odsScriptView do
        begin
          Close;
          SQL.Clear;
{
SELECT T.JOB_NAME, T.STATE, T.OWNER, T.program_name FROM DBA_SCHEDULER_JOBS T
WHERE T.OWNER = 'PIMSRSS';
}
          SQL.Add(
            'select ' +
            ' to_char(t.datetime_in, ''dd-mm-yyyy hh24:mi:ss'') datetime_in, ' +
            ' t.workspot_code, ' +
            ' t.wdescription, ' +
            ' t.job_code, ' +
            ' t.employee_number, ' +
            ' t.name, ' +
            ' t.employee_id, ' +
            ' to_char(t.datetime_out, ''dd-mm-yyyy hh24:mi:ss'') datetime_out ' +
            'from (select t2.*, e.employee_id, e.description name, w.description wdescription ' +
            '      from timeregscanning t2 ' +
            '        inner join workspot w on ' +
            '          t2.plant_code = w.plant_code and t2.workspot_code = w.workspot_code ' +
            '        inner join employee e on ' +
            '          t2.employee_number = e.employee_number ' +
            '      where (t2.datetime_in >= sysdate-0.5) or ' +
            '        ((t2.datetime_out is not null) and (t2.datetime_out >= sysdate-0.5)) ' +
            '      order by t2.datetime_in desc) t ' +
            'where rownum <= 10'
            );
// SQL.SaveToFile('c:\temp\EmployeeScans.sql');
          Open;
          if not Eof then
          begin
            AddLog('Date_in       Workspot  Name           Job   EmpNr  Name EmpID   Date_out');
            AddLog('-------------------------------------------------------------------------');
            while not Eof do
            begin
              AddLog(
                FieldByName('Datetime_in').AsString + ' | ' +
                FieldByName('Workspot_code').AsString + ' | ' +
                FieldByName('WDescription').AsString + ' | ' +
                FieldByName('Job_code').AsString + ' | ' +
                FieldByName('Employee_number').AsString + ' | ' +
                FieldByName('Name').AsString + ' | ' +
                FieldByName('Employee_id').AsString + ' | ' +
                FieldByName('Datetime_out').AsString
                );
              Next;
            end;
          end
          else
            AddLog('Nothing found.');
        end;
        AddLog('<View Employee Scans End>');
      end;
    except
      on E:EOracleError do
        Addlog(E.Message);
      on E:Exception do
        Addlog(E.Message);
    end;
  finally
    // Set back to system-user
    HomeDM.OracleSession.Connected := False;
    HomeDM.OracleSession.LogonUsername := HomeDM.MyLogonUsername;
    HomeDM.OracleSession.LogonPassword := HomeDM.MyLogonPassword;
  end;
end; //btnViewEmployeeScansClick

procedure THomeF.btnViewSessionsClick(Sender: TObject);
begin
  try
    try
      // Do this as login-user
      HomeDM.OracleSession.Connected := False;
      HomeDM.OracleSession.LogonUsername := HomeDM.MyLogonUsername;
      HomeDM.OracleSession.LogonPassword := HomeDM.MyLogonPassword;
      HomeDM.OracleSession.Connected := True;
      if HomeDM.OracleSession.Connected then
      begin
        AddLog('<View Sessions>');
        with HomeDM.odsViewSessions do
        begin
          Close;
          SQL.Clear;
          SQL.Add(
            'select ' +
            '  substr(a.spid,1,9) pid, ' +
            '  substr(b.sid,1,5) sid, ' +
            '  substr(b.serial#,1,5) ser#, ' +
            '  substr(b.machine,1,32) box, ' +
            '  substr(b.username,1,10) username, ' +
            '  substr(b.osuser,1,32) os_user, ' +
            '  substr(b.program,1,32) program ' +
            'from v$session b, v$process a ' +
            'where ' +
            '  b.paddr = a.addr ' +
            '  and type=''USER'' ' +
            'order by spid '
            );
// SQL.SaveToFile('c:\temp\ViewSessions.sql');
          Open;
          if not Eof then
          begin
            AddLog('Pid      Sid    Ser#     Box     Username    OS_User    Program');
            AddLog('---------------------------------------------------------------');
            while not Eof do
            begin
              AddLog(
                FieldByName('pid').AsString + ' | ' +
                FieldByName('sid').AsString + ' | ' +
                FieldByName('ser#').AsString + ' | ' +
                FieldByName('box').AsString + ' | ' +
                FieldByName('username').AsString + ' | ' +
                FieldByName('os_user').AsString + ' | ' +
                FieldByName('program').AsString
                );
              Next;
            end;
          end
          else
            AddLog('Nothing found.');
        end;
        AddLog('<View Sessions End>');

        AddLog('<View Blocked Sessions>');
        with HomeDM.odsViewBlockedSessions do
        begin
          Close;
          SQL.Clear;
          SQL.Add(
            'select ' +
            '  sid, ' +
            '  serial# ' +
            'from ' +
            '  v$session ' +
            'where ' +
            '  blocking_session is not NULL ' +
            'order by ' +
            '  blocking_session'
            );
// SQL.SaveToFile('c:\temp\ViewBlockedSessions.sql');
          Open;
          if not Eof then
          begin
            AddLog('Sid    Serial#');
            AddLog('--------------');
            while not Eof do
            begin
              AddLog(
                FieldByName('sid').AsString + ' | ' +
                FieldByName('serial#').AsString
                );
              Next;
            end;
          end
          else
            AddLog('Nothing found.');
        end;
        AddLog('<View Blocked Sessions End>');

{
-- View locked tables
select
   c.owner,
   c.object_name,
   c.object_type,
   b.sid,
   b.serial#,
   b.status,
   b.osuser,
   b.machine
from
   v$locked_object a ,
   v$session b,
   dba_objects c
where
   b.sid = a.session_id
and
   a.object_id = c.object_id;
}
        AddLog('<View locked tables>');
        with HomeDM.odsViewLockedTables do
        begin
          Close;
// SQL.SaveToFile('c:\temp\ViewLockedTables.sql');
          Open;
          if not Eof then
          begin
            AddLog('Owner  Object-name  Object-type Sid  Serial#  Status  OsUser  Machine');
            AddLog('---------------------------------------------------------------------');
            while not Eof do
            begin
              AddLog(
                FieldByName('owner').AsString + ' | ' +
                FieldByName('object_name').AsString + ' | ' +
                FieldByName('object_type').AsString + ' | ' +
                FieldByName('sid').AsString + ' | ' +
                FieldByName('serial#').AsString + ' | ' +
                FieldByName('status').AsString + ' | ' +
                FieldByName('osuser').AsString + ' | ' +
                FieldByName('machine').AsString
                );
              Next;
            end;
          end
          else
            AddLog('Nothing found.');
        end;
        AddLog('<View locked tables End>');
      end;
    except
      on E:EOracleError do
        Addlog(E.Message);
      on E:Exception do
        Addlog(E.Message);
    end;
  finally
    // Set back to system-user
    HomeDM.OracleSession.Connected := False;
    HomeDM.OracleSession.LogonUsername := HomeDM.MyLogonUsername;
    HomeDM.OracleSession.LogonPassword := HomeDM.MyLogonPassword;
  end;
end; // btnViewSessionsClick

procedure THomeF.btnEndBlockedSessionsClick(Sender: TObject);
begin
  try
    try
      // Connect as sys-user!
      HomeDM.OracleSession.Connected := False;
      HomeDM.OracleSession.LogonUsername := HomeDM.MyLogonUsername;
      HomeDM.OracleSession.LogonPassword := HomeDM.MyLogonPassword;
      HomeDM.OracleSession.Connected := True;
      if HomeDM.OracleSession.Connected then
      begin
        with HomeDM do
        begin
          osSQLScript.Lines.Clear;
          osSQLScript.Lines.Add(
            'declare ');
          osSQLScript.Lines.Add(
            '  -- Local variables here ');
          osSQLScript.Lines.Add(
            '  l_sid varchar2(30); ');
          osSQLScript.Lines.Add(
            '  l_serial varchar2(30); ');
          osSQLScript.Lines.Add(
            '  l_arg varchar2(30); ');
          osSQLScript.Lines.Add(
            '  l_command varchar2(300); ');
          osSQLScript.Lines.Add(
            '  CURSOR C_SHE IS ');
          osSQLScript.Lines.Add(
            '    select ');
          osSQLScript.Lines.Add(
            '      sid, ');
          osSQLScript.Lines.Add(
            '      serial# ');
          osSQLScript.Lines.Add(
            '    from ');
          osSQLScript.Lines.Add(
            '      v$session ');
          osSQLScript.Lines.Add(
            '    where ');
          osSQLScript.Lines.Add(
            '      blocking_session is not NULL ');
          osSQLScript.Lines.Add(
            '    order by ');
          osSQLScript.Lines.Add(
            '      blocking_session; ');
          osSQLScript.Lines.Add(
            'begin ');
          osSQLScript.Lines.Add(
            '  for R_SHE IN C_SHE loop ');
          osSQLScript.Lines.Add(
            '    l_arg := R_SHE.SID || '','' || R_SHE.SERIAL#; ');
          // EXECUTE IMMEDIATE 'alter system kill session ''' || r_she.sid || ',' || r_she.serial# || '''';
          osSQLScript.Lines.Add(
            '    l_command := ''alter system kill session '' || '''' || l_arg || '''';');
          // '    EXECUTE IMMEDIATE alter system kill session || '''' || R_SHE.sid || ',' || R_SHE.serial# || '''';');
//            '    EXECUTE IMMEDIATE '''alter system kill session '''' || R_SHE.sid || '','' || R_SHE.serial# || '''' || '';'' '
          osSQLScript.Lines.Add(
            ' EXECUTE IMMEDIATE ' +
            '''alter system kill session''''''' +
            ' || R_SHE.sid || ' +
            ''',''' +
            ' || R_SHE.serial# || ' +
            '''''''''' +
            ';'
            );
          osSQLScript.Lines.Add(
            '  end loop; ');
          osSQLScript.Lines.Add(
            'end; ');
          osSQLScript.Execute;
        end;
      end;
    except
      on E:EOracleError do
        Addlog(E.Message);
      on E:Exception do
        Addlog(E.Message);
    end;
  finally
    // Set back to system-user
    HomeDM.OracleSession.Connected := False;
    HomeDM.OracleSession.LogonUsername := HomeDM.MyLogonUsername;
    HomeDM.OracleSession.LogonPassword := HomeDM.MyLogonPassword;
  end;
end; // btnEndBlockedSessionsClick

procedure THomeF.btnLoadAllDataClick(Sender: TObject);
begin
  if MessageDlg('Load All DATA Now? ?',
    mtConfirmation, [mbYes, mbNo], 0) = mrYes then
  begin
    AddLog('Loading All DATA.. Please wait...');
    try
      try
        // Switch to PIMSxxx user
        HomeDM.OracleSession.Connected := False;
        HomeDM.OracleSession.LogonUsername := HomeDM.MyDatabaseUsername;
        HomeDM.OracleSession.LogonPassword := HomeDM.MyDatabasePassword;
        HomeDM.OracleSession.Connected := True;
        if HomeDM.OracleSession.Connected then
        begin
          with HomeDM do
          begin
            osSQLScript.Lines.Clear;
            osSQLScript.Lines.Add(
              'BEGIN ');
            osSQLScript.Lines.Add(
              '  PACKAGE_COCKPIT.operators_2_cockpit;');
            osSQLScript.Lines.Add(
              '  PACKAGE_COCKPIT.department_2_pims;');
            osSQLScript.Lines.Add(
              '  PACKAGE_COCKPIT.machine_2_pims;');
            osSQLScript.Lines.Add(
              '  PACKAGE_COCKPIT.workspot_2_pims;');
            osSQLScript.Lines.Add(
              'END;');
            osSQLScript.Execute;
          end;
        end;
      except
        on E:EOracleError do
          Addlog(E.Message);
        on E:Exception do
          Addlog(E.Message);
      end;
    finally
      // Set back to system-user
      HomeDM.OracleSession.Connected := False;
      HomeDM.OracleSession.LogonUsername := HomeDM.MyLogonUsername;
      HomeDM.OracleSession.LogonPassword := HomeDM.MyLogonPassword;
    end;
  end;
end; // btnLoadAllDataClick

end.
