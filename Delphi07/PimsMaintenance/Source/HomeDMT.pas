(*
  MRA:9-SEP-2010 2.0.0.8.
  - Do not check on a user named 'system' during login.
  MRA:4-MAR-2013 2.0.0.12.
  - When getting dbversion:
    - Get max of DBVERSION, in case there are multiple records.
  MRA:20-JUL-2015-2.0.0.15.
  - Addition of option to log in as sysdba.
  - Addition of 2 extra 'job'-options during CreateUser (can only be added
    by sys-user as sysdba).
  - Addition of button to grant extra job-rights for user.
  - REMARK:
    - For 'as sysdba' use other syntax for imp/exp/impdp/expdp then for sqlplus.
    - Examples:
      - imp "'sys/ABSSYS@ABS1 as sysdba'" ...
      - sqlplus "sys/ABSSYS@ABS1 as sysdba" ...
  MRA:13-SEP-2019 GLOB3-337
  - All programs should be startable via exe instead of batch, just like TR
*)
unit HomeDMT;

interface

uses
  SysUtils, Classes, Graphics, Oracle, Dialogs, DB, OracleData, IniFiles,
  Windows, Messages, Controls, Forms, Registry, DateUtils;

const
  PIMS_LOGON_SYSTEM_USER = 'SYSTEM';

type
  THomeDM = class(TDataModule)
    OracleSession: TOracleSession;
    OracleLogon: TOracleLogon;
    osSQLScript: TOracleScript;
    oqSQLScriptInsert: TOracleQuery;
    oqSQLScriptUpdateState: TOracleQuery;
    oqSQLScriptSearch: TOracleQuery;
    oqSQLScriptUpdate: TOracleQuery;
    osCreateSQLScriptTABLE: TOracleScript;
    oqPimsDBVersion: TOracleQuery;
    oqReindex: TOracleQuery;
    oqDirectory: TOracleQuery;
    odsScriptView: TOracleDataSet;
    oqConvertUTF8: TOracleQuery;
    oqPlant: TOracleQuery;
    odsViewSessions: TOracleDataSet;
    odsViewBlockedSessions: TOracleDataSet;
    odsViewLockedTables: TOracleDataSet;
    procedure DataModuleCreate(Sender: TObject);
    procedure osSQLScriptError(Sender: TOracleScript);
    procedure osSQLScriptOutput(Sender: TOracleScript; const Msg: String);
    procedure osCreateSQLScriptTABLEOutput(Sender: TOracleScript;
      const Msg: String);
    procedure osCreateSQLScriptTABLEError(Sender: TOracleScript);
    procedure OracleSessionBeforeLogOn(Sender: TOracleSession);
  private
    { Private declarations }
    FScriptError: Boolean;
    FPimsDBVersion: String;
    FGrantee: String;
    FDirectoryName: String;
    FABSDumpDir: String;
    FMyLogonUserName: String;
    FMyLogonPassword: String;
    FMyDatabaseUserName: String;
    FMyDatabasePassword: String;
    FMyLogonDatabase: String;
    FMyHalt: Boolean;
    FIsPims: Boolean;
    FORA64PathEnv: String;
    FORA32OracleHomeEnv: String;
    FORA32TNSAdminEnv: String;
    FORA32PathEnv: String;
    FORA64OracleHomeEnv: String;
    FORA64TNSAdminEnv: String;
    FORANLSLangEnv: String;
    procedure CheckLogon;
  public
    { Public declarations }
    function AskFilename(var AFileName: String;
      const ATitle: String; const ASaveDialog: Boolean;
      const AInitialDir: String; const AExtension: String): Boolean;
    function DBVersion: String;
    function DirectoryGrantee(const ACheckPermission: Boolean): Boolean;
    function IsSys(ACurrentUser: String): Boolean;
    function IsSysDBA(ACurrentUser: String): Boolean;
    function IsSysOPER(ACurrentUser: String): Boolean;
    function SchemeName: String;
    procedure SetORA32Env;
    procedure SetORA64Env;
    property MyLogonUserName: String read FMyLogonUserName write FMyLogonUserName;
    property MyLogonPassword: String read FMyLogonPassword write FMyLogonPassword;
    property MyLogonDatabase: String read FMyLogonDatabase write FMyLogonDatabase;
    property MyDatabaseUserName: String read FMyDatabaseUserName write FMyDatabaseUserName;
    property MyDatabasePassword: String read FMyDatabasePassword write FMyDatabasePassword;
    property ScriptError: Boolean read FScriptError write FScriptError;
    property PimsDBVersion: String read FPimsDBVersion write FPimsDBVersion;
    property ABSDumpDir: String read FABSDumpDir write FABSDumpDir;
    property DirectoryName: String read FDirectoryName write FDirectoryName;
    property Grantee: String read FGrantee write FGrantee;
    property MyHalt: Boolean read FMyHalt write FMyHalt;
    property IsPims: Boolean read FIsPims write FIsPims;
    property ORA32OracleHomeEnv: String read FORA32OracleHomeEnv write FORA32OracleHomeEnv;
    property ORA32PathEnv: String read FORA32PathEnv write FORA32PathEnv;
    property ORA32TNSAdminEnv: String read FORA32TNSAdminEnv write FORA32TNSAdminEnv;
    property ORANLSLangEnv: String read FORANLSLangEnv write FORANLSLangEnv;
    property ORA64OracleHomeEnv: String read FORA64OracleHomeEnv write FORA64OracleHomeEnv;
    property ORA64PathEnv: String read FORA64PathEnv write FORA64PathEnv;
    property ORA64TNSAdminEnv: String read FORA64TNSAdminEnv write FORA64TNSAdminEnv;
  end;

function PathCheck(Path: String): String;
procedure GetBuildInfo(var V1, V2, V3, V4: Word);
function DisplayVersionInfo: String;

var
  HomeDM: THomeDM;

implementation

{$R *.dfm}

uses
  HomeFRM;

// Check the directory-path:
// Add a backslash to the end, if this could not be found
function PathCheck(Path: String): String;
begin
  if Path <> '' then
    if Path[length(Path)] <> '\' then
      Path := Path + '\';
  Result := Path;
end;

function SetEnvVarValue(const VarName,
  VarValue: String): Integer;
begin
  // Simply call API function
  if Windows.SetEnvironmentVariable(PChar(VarName),
    PChar(VarValue)) then
    Result := 0
  else
    Result := GetLastError;
end;

procedure THomeDM.SetORA32Env;
begin
  if ORA32OracleHomeEnv <> '' then
  begin
    SetEnvVarValue('ORACLE_HOME', ORA32OracleHomeEnv);
  end;
  if ORA32PathEnv <> '' then
  begin
    SetEnvVarValue('PATH', ORA32PathEnv + ';' + GetEnvironmentVariable('PATH'));
  end;
  if ORA32TNSAdminEnv <> '' then
  begin
    SetEnvVarValue('TNS_ADMIN', ORA32TNSAdminEnv);
  end;
  if ORANLSLangEnv <> '' then
  begin
    SetEnvVarValue('NLS_LANG', ORANLSLangEnv);
  end;
end; // SetORA32Env

procedure THomeDM.SetORA64Env;
begin
  if ORA64OracleHomeEnv <> '' then
  begin
    SetEnvVarValue('ORACLE_HOME', ORA64OracleHomeEnv);
  end;
  if ORA64PathEnv <> '' then
  begin
    SetEnvVarValue('PATH', ORA64PathEnv + ';' + GetEnvironmentVariable('PATH'));
  end;
  if ORA64TNSAdminEnv <> '' then
  begin
    SetEnvVarValue('TNS_ADMIN', ORA64TNSAdminEnv);
  end;
  if ORANLSLangEnv <> '' then
  begin
    SetEnvVarValue('NLS_LANG', ORANLSLangEnv);
  end;
end; // SetORA64Env

procedure THomeDM.DataModuleCreate(Sender: TObject);
var
  AppRootPath: String;
const
  PimINIFilename='PIMSORA.INI';
  procedure ReadINIFile;
  var
    Ini: TIniFile;
  begin
    if not FileExists(PathCheck(AppRootPath) + PimINIFilename) then
      Exit;
    Ini := TIniFile.Create(PathCheck(AppRootPath) + PimINIFilename);
    try
      MyLogonDatabase :=
        UpperCase(Ini.ReadString('PIMSORA', 'Database', MyLogonDatabase));
      MyDatabaseUsername :=
        UpperCase(Ini.ReadString('PIMSORA', 'DatabaseUsername',
          MyDatabaseUsername));
      HomeDM.MyDatabaseUserName := MyDatabaseUserName;
      MyDatabasePassword :=
        UpperCase(Ini.ReadString('PIMSORA', 'DatabasePassword',
          MyDatabasePassword));
      HomeDM.MyDatabasePassword := MyDatabasePassword;
      // Also read environment variables here for ORACLE 32bit and 64bit:
      {
        Set ORACLE_HOME=C:\PIMSROOT\OraClient32b
        Set PATH=C:\PIMSROOT\OraClient32b
        Set TNS_ADMIN=C:\PIMSROOT\OraClient32b\network\admin
      }
      // ORA 32
      ORA32OracleHomeEnv := '';
      ORA32PathEnv := '';
      ORA32TNSAdminEnv := '';
      ORANLSLangEnv := '';
      // GLOB3-337 First look for section ORA, if not exists then ORA32.
      if Ini.ValueExists('ORA', 'ORACLE_HOME') then
        ORA32OracleHomeEnv := Ini.ReadString('ORA', 'ORACLE_HOME', ORA32OracleHomeEnv)
      else
        if Ini.ValueExists('ORA32', 'ORACLE_HOME') then
          ORA32OracleHomeEnv := Ini.ReadString('ORA32', 'ORACLE_HOME', ORA32OracleHomeEnv);
      if Ini.ValueExists('ORA', 'PATH') then
        ORA32PathEnv := Ini.ReadString('ORA', 'PATH', ORA32PathEnv)
      else
        if Ini.ValueExists('ORA32', 'PATH') then
          ORA32PathEnv := Ini.ReadString('ORA32', 'PATH', ORA32PathEnv);
      if Ini.ValueExists('ORA', 'TNS_ADMIN') then
        ORA32TNSAdminenv := Ini.ReadString('ORA', 'TNS_ADMIN', ORA32TNSAdminEnv)
      else
        if Ini.ValueExists('ORA32', 'TNS_ADMIN') then
          ORA32TNSAdminenv := Ini.ReadString('ORA32', 'TNS_ADMIN', ORA32TNSAdminEnv);
      if Ini.ValueExists('ORA', 'NLS_LANG') then
        ORANLSLangEnv := Ini.ReadString('ORA', 'NLS_LANG', ORANLSLangEnv)
      else
        if Ini.ValueExists('ORA32', 'NLS_LANG') then
          ORANLSLangEnv := Ini.ReadString('ORA32', 'NLS_LANG', ORANLSLangEnv);
      SetORA32Env; // GLOB3-337
      // ORA 64
      ORA64OracleHomeEnv := '';
      ORA64PathEnv := '';
      ORA64TNSAdminEnv := '';
      if Ini.ValueExists('ORA64', 'ORACLE_HOME') then
        ORA64OracleHomeEnv := Ini.ReadString('ORA64', 'ORACLE_HOME', ORA64OracleHomeEnv);
      if Ini.ValueExists('ORA64', 'PATH') then
        ORA64PathEnv := Ini.ReadString('ORA64', 'PATH', ORA64PathEnv);
      if Ini.ValueExists('ORA64', 'TNS_ADMIN') then
        ORA64TNSAdminenv := Ini.ReadString('ORA64', 'TNS_ADMIN', ORA64TNSAdminEnv);
      if Ini.ValueExists('ORA64', 'NLS_LANG') then
        ORANLSLangEnv := Ini.ReadString('ORA64', 'NLS_LANG', ORANLSLangEnv);
    finally
      Ini.Free;
    end;
  end;
begin
  AppRootPath := GetCurrentDir;
  MyHalt := False;

  IsPims := True;

  MyLogonUserName := '';
  MyLogonPassword := '';
  MyLogonDatabase := 'ABS1';
  MyDatabaseUserName := 'PIMS';
  MyDatabasePassword := 'PIMS';
  ReadINIFile;
  OracleSession.Connected := False;
  OracleSession.LogonDatabase := MyLogonDatabase;
  OracleSession.LogonUsername := '';
  OracleSession.LogonPassword := '';
  if OracleLogon.Execute then
  begin
    // System-user is logging in to database!
    MyLogonDatabase := OracleSession.LogonDatabase;
    MyLogonUserName := OracleSession.LogonUsername;
    MyLogonPassword := OracleSession.LogonPassword;
    OracleSession.Connected := True;
    if not OracleSession.Connected then
    begin
      MyHalt := True;
      Halt;
    end;
  end
  else
  begin
    MyHalt := True;
    Halt;
  end;
end;

procedure THomeDM.osSQLScriptError(Sender: TOracleScript);
begin
  ScriptError := True;
  HomeF.AddLog('An error occured.');
end;

procedure THomeDM.osSQLScriptOutput(Sender: TOracleScript;
  const Msg: String);
begin
  HomeF.AddLog(Msg);
end;

procedure THomeDM.osCreateSQLScriptTABLEOutput(Sender: TOracleScript;
  const Msg: String);
begin
//  HomeF.AddLog(Msg);
end;

procedure THomeDM.osCreateSQLScriptTABLEError(Sender: TOracleScript);
begin
  HomeF.AddLog('Error during creation of SQLSCRIPT-Table!');
end;

procedure THomeDM.CheckLogon;
begin
// 2.0.0.8.
{
  if not (UpperCase(OracleSession.LogonUsername) = PIMS_LOGON_SYSTEM_USER) then
  begin
    ShowMessage('Incorrect username and/or password or not enough rights!');
    Halt;
  end
  else
}
  begin
    MyLogonUserName := OracleSession.LogonUsername;
    MyLogonPassword := OracleSession.LogonPassword;
    // Remove the event
    OracleSession.BeforeLogOn := nil;
  end;
end;

procedure THomeDM.OracleSessionBeforeLogOn(Sender: TOracleSession);
begin
  CheckLogon;
end;

function THomeDM.DBVersion: String;
begin
  Result := 'nnn';
  try
    oqPimsDBVersion.SQL.Clear;
    if IsPims then
    begin
      // 2.0.0.12.
      oqPimsDBVersion.SQL.Add(
        'SELECT MAX(DBVERSION) DBVERSION ' + #13#10 +
        'FROM ' + SchemeName + 'PIMSDBVERSION'
        );
    end
    else
    begin
      // There is already a SOLARDBVERSION-procedure, so
      // use a different name.
      // 2.0.0.12.
      oqPimsDBVersion.SQL.Add(
        'SELECT MAX(DBVERSION) DBVERSION ' + #13#10 +
        'FROM ' + SchemeName + 'ABSSOLARDBVERSION'
        );
    end;
    oqPimsDBVersion.Execute;
    if not oqPimsDBVersion.Eof then
      Result := oqPimsDBVersion.FieldAsString('DBVERSION');
  except
    Result := 'nnn';
  end;
  PimsDBVersion := Result;
end;

function THomeDM.DirectoryGrantee(const ACheckPermission: Boolean): Boolean;
var
  RecCount: Integer;
begin
//  Result := False;
  try
    OracleSession.Connected := False;
    OracleSession.LogonUsername := MyDatabaseUserName; // MyLogonUserName;
    OracleSession.LogonPassword := MyDatabasePassword; // MyLogonPassword;
    OracleSession.Connected := True;
    if not OracleSession.Connected then
    begin
      Result := False;
      Exit;
    end;
    with oqDirectory do
    begin
      ClearVariables;
      SetVariable('DIRECTORY_NAME', ABSDumpDir);
      SetVariable('GRANTEE',        Grantee);
      Execute;
      RecCount := 0;
      // This must at least give 2 records: 1 for 'Read'-permission and
      // 1 for 'Write'-permission
      if ACheckPermission then
        HomeF.AddLog('GRANTEE - PRIVILEGE - DIRECTORY - DIRECTORY_NAME');
      while not Eof do
      begin
        inc(RecCount);
        if ACheckPermission then
          HomeF.AddLog(FieldAsString('GRANTEE') + ' - ' +
            FieldAsString('PRIVILEGE') + ' - ' + FieldAsString('DIRECTORY') +
            ' - ' + FieldAsString('DIRECTORY_NAME'));
        DirectoryName := FieldAsString('DIRECTORY');
        Next;
      end;
      if ACheckPermission then
        if RecCount = 0 then
          HomeF.AddLog('Error: No permissions found!');
      Result := (RecCount >= 2);
    end;
    OracleSession.Connected := False;
    OracleSession.LogonUsername := MyLogonUserName;
    OracleSession.LogonPassword := MyLogonPassword;
  except
    Result := False;
  end;
end;

function THomeDM.AskFilename(var AFileName: String;
  const ATitle: String; const ASaveDialog: Boolean;
  const AInitialDir: String; const AExtension: String): Boolean;
var
  Filter, DefaultExt: String;
begin
  Result := False;
  // Ask for filename and directory
  Filter := '*.' + AExtension;
  DefaultExt := AExtension;
  if PromptForFileName(AFileName, Filter, DefaultExt,
    ATitle, AInitialDir, ASaveDialog) then
  begin
    if AFileName <> '' then
      Result := True;
  end;
end;

function THomeDM.IsSys(ACurrentUser: String): Boolean;
begin
  Result := (LowerCase(ACurrentUser) = 'sys');
end;

function THomeDM.IsSysDBA(ACurrentUser: String): Boolean;
begin
// 2.0.0.15 Only look for 'ConnectAs'.
//  Result := (LowerCase(ACurrentUser) = 'sys') and
//    (OracleSession.ConnectAs = caSYSDBA);
  Result := (OracleSession.ConnectAs = caSYSDBA);
end;

function THomeDM.IsSysOPER(ACurrentUser: String): Boolean;
begin
  Result := (OracleSession.ConnectAs = caSYSOPER);
end;

function THomeDM.SchemeName: String;
begin
  if IsSys(MyLogonUserName) then
    Result := MyDatabaseUserName + '.'
  else
    Result := '';
end;

procedure GetBuildInfo(var V1, V2, V3, V4: Word);
var
  VerInfoSize, VerValueSize, Dummy : DWORD;
  VerInfo : Pointer;
  VerValue : PVSFixedFileInfo;
begin
  VerInfoSize := GetFileVersionInfoSize(PChar(ParamStr(0)), Dummy);
  GetMem(VerInfo, VerInfoSize);
  GetFileVersionInfo(PChar(ParamStr(0)), 0, VerInfoSize, VerInfo);
  VerQueryValue(VerInfo, '\', Pointer(VerValue), VerValueSize);
  with VerValue^ do
  begin
    V1 := dwFileVersionMS shr 16;
    V2 := dwFileVersionMS and $FFFF;
    V3 := dwFileVersionLS shr 16;
    V4 := dwFileVersionLS and $FFFF;
  end;
  FreeMem(VerInfo, VerInfoSize);
end;

function DisplayVersionInfo: String;
var
  V1,       // Major Version
  V2,       // Minor Version
  V3,       // Release
  V4: Word; // Build Number
begin
  GetBuildInfo(V1, V2, V3, V4);
  Result := IntToStr(V1) + '.'  + IntToStr(V2) + '.'  + IntToStr(V3) + '.'
    + IntToStr(V4);
end;

end.
