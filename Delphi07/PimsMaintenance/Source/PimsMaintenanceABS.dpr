program PimsMaintenanceABS;

uses
  Forms,
  HomeFRM in 'HomeFRM.pas' {HomeF},
  HomeDMT in 'HomeDMT.pas' {HomeDM: TDataModule},
  BasePimsFRM in '..\..\Pims Shared\BasePimsFRM.pas' {BasePimsForm},
  OrderInfoFRM in '..\..\Pims Shared\OrderInfoFRM.pas' {OrderInfoForm},
  SplashFRM in '..\..\Pims Shared\SplashFRM.pas' {SplashForm},
  UPimsConst in '..\..\Pims Shared\UPimsConst.pas',
  ColorButton in '..\..\Pims Shared\ColorButton.pas',
  AboutFRM in '..\..\Pims Shared\AboutFRM.pas' {AboutForm},
  WaitFRM in 'WaitFRM.pas' {WaitF},
  ImportDialogFRM in 'ImportDialogFRM.pas' {ImportDialogF};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(THomeDM, HomeDM);
  Application.CreateForm(THomeF, HomeF);
  Application.Run;
end.
