object HomeF: THomeF
  Left = 431
  Top = 202
  Width = 740
  Height = 531
  BorderIcons = [biSystemMenu, biMinimize]
  Caption = 'Pims Maintenance'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 724
    Height = 41
    Align = alTop
    BevelOuter = bvNone
    Color = clBtnShadow
    TabOrder = 0
    object Label1: TLabel
      Left = 8
      Top = 5
      Width = 197
      Height = 29
      Caption = 'Pims Maintenance'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -24
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 41
    Width = 724
    Height = 247
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Memo1: TMemo
      Left = 0
      Top = 0
      Width = 724
      Height = 247
      Align = alClient
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Courier New'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      ScrollBars = ssBoth
      TabOrder = 0
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 288
    Width = 724
    Height = 165
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    object Panel4: TPanel
      Left = 0
      Top = 0
      Width = 724
      Height = 28
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object lblDBUser: TLabel
        Left = 8
        Top = 8
        Width = 71
        Height = 13
        Caption = 'Database User'
      end
      object Label2: TLabel
        Left = 288
        Top = 8
        Width = 24
        Height = 13
        Caption = 'Plant'
      end
      object cmbDatabaseUsers: TComboBox
        Left = 96
        Top = 3
        Width = 184
        Height = 21
        ItemHeight = 13
        TabOrder = 0
        Text = 'cmbDatabaseUsers'
        OnChange = cmbDatabaseUsersChange
      end
      object cmbPlants: TComboBox
        Left = 328
        Top = 3
        Width = 184
        Height = 21
        ItemHeight = 13
        TabOrder = 1
        Text = 'cmbPlants'
      end
    end
    object Panel5: TPanel
      Left = 0
      Top = 28
      Width = 724
      Height = 137
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object PageControl1: TPageControl
        Left = 0
        Top = 0
        Width = 724
        Height = 137
        ActivePage = TabSheet9
        Align = alClient
        TabOrder = 0
        object TabSheet1: TTabSheet
          Caption = 'Scripts'
          object btnRunScript: TButton
            Left = 4
            Top = 4
            Width = 217
            Height = 49
            Caption = 'Run Scripts'
            Default = True
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -24
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            OnClick = btnRunScriptClick
          end
        end
        object TabSheet2: TTabSheet
          Caption = 'Dumpfile Legacy'
          ImageIndex = 1
          object btnImport: TButton
            Left = 4
            Top = 4
            Width = 225
            Height = 49
            Caption = 'Import'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -24
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            OnClick = btnImportClick
          end
          object btnExport: TButton
            Left = 4
            Top = 57
            Width = 225
            Height = 49
            Caption = 'Export'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -24
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
            OnClick = btnExportClick
          end
          object CheckBoxImportExportWithCMD: TCheckBox
            Left = 240
            Top = 8
            Width = 249
            Height = 17
            Caption = 'Import/Export with command box'
            Checked = True
            State = cbChecked
            TabOrder = 2
          end
        end
        object TabSheet3: TTabSheet
          Caption = 'Dumpfile Datapump'
          ImageIndex = 2
          object btnImportDP: TButton
            Left = 4
            Top = 4
            Width = 225
            Height = 49
            Caption = 'Import'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -24
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            OnClick = btnImportDPClick
          end
          object btnExportDP: TButton
            Left = 4
            Top = 57
            Width = 225
            Height = 49
            Caption = 'Export'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -24
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
            OnClick = btnExportDPClick
          end
          object btnSetBackupLocation: TButton
            Left = 236
            Top = 4
            Width = 225
            Height = 49
            Caption = 'Set Backup Location'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -19
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 2
            OnClick = btnSetBackupLocationClick
          end
          object btnSetBackupPermission: TButton
            Left = 236
            Top = 57
            Width = 225
            Height = 49
            Caption = 'Set Backup Permission'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -19
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 3
            OnClick = btnSetBackupPermissionClick
          end
          object btnCheckPermission: TButton
            Left = 468
            Top = 57
            Width = 225
            Height = 49
            Caption = 'Check Permission'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -19
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 4
            OnClick = btnCheckPermissionClick
          end
          object CheckBoxExportDPTo10g: TCheckBox
            Left = 472
            Top = 4
            Width = 233
            Height = 17
            Caption = 'Export compatible with 10g'
            TabOrder = 5
          end
          object CheckBoxImportExportDPWithCMD: TCheckBox
            Left = 472
            Top = 24
            Width = 217
            Height = 17
            Caption = 'Import/Export with command-box'
            Checked = True
            State = cbChecked
            TabOrder = 6
          end
        end
        object TabSheet4: TTabSheet
          Caption = 'User'
          ImageIndex = 3
          object btnDropCreateUser: TButton
            Left = 4
            Top = 4
            Width = 209
            Height = 49
            Caption = 'Drop/Create User'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -24
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            OnClick = btnDropCreateUserClick
          end
          object btnDropUser: TButton
            Left = 4
            Top = 4
            Width = 209
            Height = 49
            Caption = 'Drop User'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -24
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
            OnClick = btnDropUserClick
          end
          object btnCreateUser: TButton
            Left = 4
            Top = 57
            Width = 209
            Height = 49
            Caption = 'Create User'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -24
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 2
            OnClick = btnCreateUserClick
          end
          object btnGrantJob: TButton
            Left = 500
            Top = 4
            Width = 209
            Height = 49
            Caption = 'Add job-rights to user'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -19
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 3
            OnClick = btnGrantJobClick
          end
        end
        object TabSheet5: TTabSheet
          Caption = 'Extra'
          ImageIndex = 4
          object btnReindex: TButton
            Left = 4
            Top = 4
            Width = 217
            Height = 49
            Caption = 'Re-index'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -24
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            OnClick = btnReindexClick
          end
          object btnScriptView: TButton
            Left = 4
            Top = 58
            Width = 217
            Height = 49
            Caption = 'Script View'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -24
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
            OnClick = btnScriptViewClick
          end
          object btnConvertUTF8: TButton
            Left = 228
            Top = 4
            Width = 217
            Height = 49
            Caption = 'Convert UTF8'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -24
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 2
            OnClick = btnConvertUTF8Click
          end
          object btnViewSessions: TButton
            Left = 452
            Top = 4
            Width = 217
            Height = 49
            Caption = 'View Sessions'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -24
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 3
            OnClick = btnViewSessionsClick
          end
          object btnEndBlockedSessions: TButton
            Left = 452
            Top = 58
            Width = 217
            Height = 49
            Caption = 'End blocked sessions'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -20
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 4
            OnClick = btnEndBlockedSessionsClick
          end
        end
        object TabSheet6: TTabSheet
          Caption = 'Real Time Eff. Check'
          ImageIndex = 5
          object btnCheckJob: TButton
            Left = 4
            Top = 4
            Width = 165
            Height = 49
            Caption = 'Check job calc-eff.'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -19
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            OnClick = btnCheckJobClick
          end
          object btnCheckLogCalcEff: TButton
            Left = 4
            Top = 60
            Width = 165
            Height = 49
            Caption = 'Check log calc-eff.'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -19
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
            OnClick = btnCheckLogCalcEffClick
          end
          object btnCheckEffCalcPeriod: TButton
            Left = 172
            Top = 4
            Width = 197
            Height = 49
            Caption = 'Check eff. calc period'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -19
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 2
            OnClick = btnCheckEffCalcPeriodClick
          end
          object btnViewJobs: TButton
            Left = 172
            Top = 60
            Width = 197
            Height = 49
            Caption = 'View jobs'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -19
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 3
            OnClick = btnViewJobsClick
          end
        end
        object TabSheet7: TTabSheet
          Caption = 'Real Time Eff.Tools'
          ImageIndex = 6
          object btnTurnOffJob: TButton
            Left = 4
            Top = 4
            Width = 161
            Height = 49
            Caption = 'Turn off job'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -19
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            OnClick = btnTurnOffJobClick
          end
          object btnTurnOnJob: TButton
            Left = 4
            Top = 60
            Width = 161
            Height = 49
            Caption = 'Turn on job'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -19
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
            OnClick = btnTurnOnJobClick
          end
          object btnResetJobQueue: TButton
            Left = 512
            Top = 4
            Width = 161
            Height = 49
            Caption = 'Reset job queue'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -19
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 2
            OnClick = btnResetJobQueueClick
          end
          object btnDropJob: TButton
            Left = 173
            Top = 4
            Width = 161
            Height = 49
            Caption = 'Drop job'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -19
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 3
            OnClick = btnDropJobClick
          end
          object btnCreateJob: TButton
            Left = 175
            Top = 60
            Width = 161
            Height = 49
            Caption = 'Create job'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -19
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 4
            OnClick = btnCreateJobClick
          end
          object btnDropProgram: TButton
            Left = 341
            Top = 4
            Width = 161
            Height = 49
            Caption = 'Drop program'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -19
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 5
            OnClick = btnDropProgramClick
          end
          object btnCreateProgram: TButton
            Left = 341
            Top = 60
            Width = 161
            Height = 49
            Caption = 'Create program'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -19
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 6
            OnClick = btnCreateProgramClick
          end
        end
        object TabSheet8: TTabSheet
          Caption = 'Datacol Import Files'
          ImageIndex = 7
          object btnSetImportLocation: TButton
            Left = 4
            Top = 4
            Width = 197
            Height = 49
            Caption = 'Set Import Location'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -19
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            OnClick = btnSetImportLocationClick
          end
          object btnSetImportPermission: TButton
            Left = 4
            Top = 57
            Width = 197
            Height = 49
            Caption = 'Set Import Permission'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -19
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
            OnClick = btnSetImportPermissionClick
          end
          object btnCreateImportTable: TButton
            Left = 212
            Top = 4
            Width = 199
            Height = 49
            Caption = 'Create import table'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -19
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 2
            OnClick = btnCreateImportTableClick
          end
          object btnCreateImportJob: TButton
            Left = 429
            Top = 57
            Width = 141
            Height = 49
            Caption = 'Create job'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -19
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 3
            OnClick = btnCreateImportJobClick
          end
          object btnDropImportJob: TButton
            Left = 429
            Top = 4
            Width = 140
            Height = 49
            Caption = 'Drop job'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -19
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 4
            OnClick = btnDropImportJobClick
          end
          object btnViewImportJob: TButton
            Left = 584
            Top = 57
            Width = 125
            Height = 49
            Caption = 'View job'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -19
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 5
            OnClick = btnViewImportJobClick
          end
          object btnCheckImportPermission: TButton
            Left = 212
            Top = 57
            Width = 199
            Height = 49
            Caption = 'Check permission'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -19
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 6
            OnClick = btnCheckImportPermissionClick
          end
          object btnViewImportLog: TButton
            Left = 583
            Top = 4
            Width = 125
            Height = 49
            Caption = 'View log'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -19
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 7
            OnClick = btnViewImportLogClick
          end
        end
        object TabSheet9: TTabSheet
          Caption = 'Hybrid Mode'
          ImageIndex = 8
          object btnViewLogHybrid: TButton
            Left = 583
            Top = 4
            Width = 125
            Height = 49
            Caption = 'View log'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -19
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            OnClick = btnViewLogHybridClick
          end
          object btnViewJobsHybrid: TButton
            Left = 584
            Top = 57
            Width = 125
            Height = 49
            Caption = 'View job'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -19
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
            OnClick = btnViewJobsHybridClick
          end
          object btnViewRemoteOperators: TButton
            Left = 5
            Top = 57
            Width = 218
            Height = 49
            Caption = 'View remote operators'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -19
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 2
            OnClick = btnViewRemoteOperatorsClick
          end
          object btnViewEmployeeScans: TButton
            Left = 5
            Top = 4
            Width = 218
            Height = 49
            Caption = 'View employee scans'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -19
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 3
            OnClick = btnViewEmployeeScansClick
          end
          object btnLoadAllData: TButton
            Left = 303
            Top = 4
            Width = 194
            Height = 49
            Caption = 'Load All Data Now'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -19
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 4
            OnClick = btnLoadAllDataClick
          end
        end
      end
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 453
    Width = 724
    Height = 19
    Panels = <
      item
        Text = 'ABS Laundry Business Solutions'
        Width = 200
      end
      item
        Width = 400
      end
      item
        Width = 50
      end>
  end
  object MainMenu1: TMainMenu
    Left = 136
    Top = 97
    object File1: TMenuItem
      Caption = 'File'
      object Exit1: TMenuItem
        Caption = 'Exit'
        OnClick = Exit1Click
      end
    end
    object Help1: TMenuItem
      Caption = 'Help'
      object About1: TMenuItem
        Caption = 'About'
        OnClick = About1Click
      end
    end
  end
  object tmrBusy: TTimer
    Enabled = False
    Interval = 5000
    OnTimer = tmrBusyTimer
    Left = 344
    Top = 97
  end
  object OpenDialog1: TOpenDialog
    Left = 448
    Top = 201
  end
end
