(*
  Changes:
  MR:23-1-2004 For Employees from type 'Is_Scanning_yn' = N no
               salary should be calculated.
  MR:09-09-2004 Cleaning up of source and Optimizing.
  MR:01-03-2005 Order 550387

  MRA:07-MAR-2008 RV004. Overtime-fix.
  RV040. 29-OCT-2009.
  - Synchronize time between 3:05 and 3:10 AM.
  MRA:07-DEC-2009 RV047.1. 889939.
  - When employees scans in too early, it does not show the
    planned workspot. This must be changed, so it looks for a
    worspot in a range of 5 hours.
  MRA:26-FEB-2010. RV048.1.
  - Do not use any logging.
  - Try to reconnect, when connection was lost.
  MRA:15-JUN-2010. RV063.4. 550478. Personal Screen.
  - Addition of Machine/Workspot + Time recording.
  - Make it possible to call 'TimeRecording' from Production Screen:
    - Use a boolean to decide if TimeRecording is called from itself or
      from Production Screen.
    If TimeRecording is called from Production Screen:
    - Show it in a smaller dialog?
    - Add Cancel-button.
    - Show a different set of jobs, based on a certain Machine / Workspot.
    - The MachineCode, PlantCode,  WorkspotCode depend on the call from
      ProductionScreen.
    If TimeRecording is called from itself:
    - Use standard behaviour.
  MRA:27-SEP-2010. RV064.1. Bugfix.
  - It is possible there are scans that interfere.
  - Add more try-except-statements, with logging.
  MRA:28-SEP-2010. RV064.2.
  - Log-routines are moved to UGlobalFunctions.
  MRA:9-NOV-2010 RV072.4.
  - When an employee of type 'non-scanner' and
    'book prod. hours based on standard planning' is scanning,
    then give a message and block this action.
  MRA:13-MAY-2011 RV076.3. SR-890031. Bugfix.
  - Inactive day
    - When for an employee the end-date is set, then
      this enddate is the last day it is possible to
      make a scanning. But Timerecording gives message
      about 'employee is not active' on that enddate.
  MRA:2-SEP-2011 RV079.1. Update.
  - Make it possible to maximize the dialog.
  - Make the buttons bigger (higher),
    but only when maximized, or by using
    a setting?
  MRA:12-APR-2012 20012858
  - Additions for New Personal Screen:
    - Break-button
    - Lunch-button
  MRA:14-MAY-2012 20012858
  - When dialog is re-opened, some fields are still filled:
    - Clear the fields during FormShow.
  - When dialog is (re-)opened, refresh shown time.
  - When dialog is openend, close it automatically after 10 seconds.
  MRA:23-MAY-2012 20013176
  - Check for any Smart Card Commands, if found
    then it should be possible to read an ID from a smart card
    using a smart card reader.
  MRA:8-JUN-2012 20013315
  - Make option in Timerecording to enter emp.nr. belonging to ID.
  MRA:13-JUN-2012 20013176.2 Bugfix.
  - When using under Windows 7 it gives errors!
  - NOTE: Plug and Play for smart cards must be disabled!
  - CAUSE: The card must be opened Shared instead of Exclusive!
  MRA:21-JUN-2012 20013176.3 Bugfix.
  - Default, do not ask for an employeenumber, only when app. called with '-A'.
  MRA:22-JUN-2012 20013176.4 Bugfix.
  - When an ID-card was not found and ask-for-emp-nr was turned off, then
    it was not possible to read another card or enter an ID by keyboard.
    CAUSE: The ScanStatus was not reset.
  MRA:16-NOV-2012 TD-21429 Related to this order.
  - Set property Position for this form to 'poDefault', because it is
    repositioned later (when called from Personal Screen).
  MRA:22-NOV-2012 TD-21642 Interfering scans problem
  - Timerecording:
    - Sometimes it creates interfering scans.
      - Reason: Timerecording normally checks if there
        is an old scan within 12 hours. But when using
        F10 without starting a new scan, it starts
        looking for an old scan within 24 hours.
        This can lead to interfering scans because it
        is also dividing the 24-hour-scan it finds into
        2 parts.
      - Solution:
        - Do not look for old scans within 24 hours,
          only for 12 hours.
  MRA:3-JAN-2012 20013489.10. Overnight-Shift-System.
  - When called from Timerecording or Personal Screen, only add the
    production hours, do no recalculate them!
  MRA:5-FEB-2013 TD-22069 (Solution not clear yet). !!!CANCELLED!!!
  - Staff hours vs shift date Issue.
  Example:
  When making a scan for Sunday:
  In Shift Schedule it finds shifts 2, 21, 2 for Saturday, Sunday and Monday.
  In Standard Planning it searches on these shifts to see if there is standard
  planning.
  But in standard planning there are shifts 2, 13, 2 for Saturday, Sunday and
  Monday.
  This results in nothing found, because these shifts are not there.
  It uses 21 as shift (for Sunday in Shift Schedule) to find the shift-day.
  With this it finds Saturday as start of the shift.
  Because for Saturday the shift is 2, it also uses 2 to assign to the shift
  of the scan.
  MRA:11-MAR-2013 20014035
  - Hide ID optionally in Timerecording/PersonalScreen
    - When HIDE_ID_YN in WORKSTATION-table for current workstation
      is set to 'Y' then hide the (Card-) ID, show stars instead of the ID.
      Otherwise show it in the normal way.
  MRA:27-SEP-2013 Card Reader Troubleshoot. CANCELLED
  - Added INI-file-setting to make it possible to ConnectShared or
    ConnectExclusive.
  MRA:2-OCT-2013 TD-23247
  - Addition of new WORKSTATION-field CARDREADER_DELAY that can be
    used to slow done the reading of cards via contactless card
    readers in case the reading is not reliable. This value is in
    milliseconds.
  MRA:19-NOV-2013 TD-23620
  - Adjustments needed for screen size. When screen is too small, buttons
    are not all shown. A mimimum size should be 640 x 480.
  MRA:14-JAN-2014 20014327 Related to this order.
  - It did not get the department using DetermineShift when someone is
    planned on a shift. To solve it, there is made a separate procedure
    that always get the department (from workspot).
  MRA:25-MAR-2014 TD-24520
  - Timerecording -A: Also accept keyboard-entry.
  - This can be needed when using a card-reader that works via the
    keyboard-entry.
  - Here 2 fields (SERIALDEVICE-record) are used to make it possible to
    truncate the ID:
    - VERIFYPOSITION - Start position from where to truncate
    - VALUEPOSITION - Length that must be kept after truncation.
    - Example, when VERIFYPOSITION=8 and VALUEPOSITION=6 then it will copy
      the part starting from position=8 and over a length of 6 characters.
      - When ID is: 9200000001247000
      - Then result after truncation will be: 001247
  MRA:16-MAY-2014 SO-20015371
  - Embed customized function to convert id-card-number
    - When SERIALDEVICE.INITSTRING is filled, then used that a the name
      of the (Oracle)-function to convert the ID.
  - SPECIAL NOTES:
    - It is only used for keyboard/serial readers, not for smart card
      readers, because the latter uses RAW-ID's.
    - It works only for IDs that are 10 or longer! Otherwise it is not possible
      to enter a number manually (by keyboard).
  MRA:20-MAY-2014 TD-24520.60 Rework
  - The check on length when using VERIFYPOSITION and VALUEPOSITION is wrong. It
    should add 1 for a correct comparison.
  - Example:
  - When ID is 123456789 then to get the last 6 positions we use
    VERIFYPOSTION=4 and VALUEPOSITION=6. Here 4 + 6 is 10. But the length is 9,
    so we must add 1 to get the length on check right.
  - Also: When VERIFYSTRING contains NOTRIM then do not trim the string!
  MRA:10-NOV-2014 20014826
  - Log error messages optionally to database.
  MRA:16-MAR-2015 20015346
    - Store scans in seconds
    - Cut off time and breaks handling
    - Creation-date and Mutation-date handling for scans
    - Addition of 2 Pims-Settings:
      - TimerecordingInSecs
      - IgnoreBreaks
  MRA:24-MAR-2015 20015346
  - It must assign a DateIn or DateOut at the moment it is stored in DB.
  - Show the time always and with an interval of 1 second instead of 5.
    - A new timer (Timer1Sec) has been added that only shows the time each second.
  MRA:6-MAY-2015 20014450.50
  - Real Time Efficiency
  - Allow adding scans within 1 minute, because seconds-part is also stored.
  - End-Of-Day-button blink at end of shift.
  MRA:22-JUL-2015 PIM-50
  - Add Job Comment
  MRA:21-SEP-2015 PIM-12 Bugfix
  - Real time efficiency, store scans in seconds.
  - Via ChangeJob + Personal Screen -> Sometimes the next scan has an older
    timestamp than the previous scan.
  - Also: Be sure the creationdate/mutationdate get the same timestamp as
          the DateIn or DateOut.
  MRA:28-SEP-2015 PIM-12
  - Show optionally Break- Lunch-Button (Pers. Screen + TimeRecording),
    End-Of-Day-Button (TimeRecording), based on system settings.
  MRA:30-DEC-2015 PIM-112
  - Addition of break/lunch-buttons for Timerecording-application.
  MRA:13-JAN-2016 ABS-18715
  - No Employee
  - There was a rounding-problem with scans resulting in no-employee in report
    production details.
  MRA:27-JAN-2016 PIM-139
  - Lunch button optionally scan out.
  MRA:11-MAR-2016 PIM-150
  - Add option to show only the default job (only for timerecording-app).
  - Based on workstation-setting SHOW_ONLY_DEFJOB_YN.
  - Note: Be sure there is a default job defined!
  MRA:31-MAR-2016 PIM-161
  - Look for scans older within 24 hours, when it was older than 12 hours
    give a message about: Last scan is not closed. End of Day? Yes or No.
  - We must look for the last scan open or not!
  MRA:19-MAY-2016 PIM-179
  - When using Hybrid-mode (Hybrid=1):
    - Show only job ATWORK as default,
      when workspot has setting 'Measure Productivity equals Yes'.
    - Also add job ATWORK and OCCUP when they do not exist yet for
      the selected workspot.
  MRA:19-MAY-2016 PIM-181
  - When using Hybrid-mode (Hybrid=1):
    - Allow only 1 employee per workspot
  MRA:7-JUN-2016 PIM-150 Rework
  - When using SHOW_ONLY_DEFJOB_YN = 'Y' then be sure there
    is a job found. If not, then show an error-message and do not continue!
    This prevents it will store an empty job!
  MRA:20-JUN-2016 PIM-181
  - Only do the Hybrid=1 actions when workspot is defined as Measure
    Productivity.
  MRA:12-JUL-2016 PIM-150.1. Bugfix.
  - Do not check on no-default-job found!
  - REASON: It is possible there are not jobs defined, but
  -        'show businessunits' is used for the workspot!
  MRA:12-JUL-2016 PIM-203
  - TimerecordingCR
    - New application
    - Read ID's without visual information
    - Related to this order: Some parts of this source had to be reorganized.
  MRA:16-SEP-2016 PIM-230
  - Wrong workspot-sequence during selection of workspots.
  - It should take the sequence as defined via workspots-per-workstations,
    but it does not do this anymore.
  MRA:3-OCT-2016 PIM-228
  - Move break (+lunch) button to first screen.
  MRA:29-NOV-2016 PIM-242
  - Check Shift during In/Out Scan
  MRA:4-JAN-2017 PIM-254
  - Accept 8 positions for Rosslare-cards.
  MRA:6-JAN-2017 PIM-256
  - Read IDs from FOBs via pcProcs-Plus-Reader with USB-connection
  MRA:16-JAN-2017 PIM-256
  - Problem-solving for worked hours that are shown when this is not defined.
  MRA:18-DEC-2017 PIM-330
  - Problem with job-selection solved: When a break-job was defined but not as
    Measure Productivity it did not take that job as default, but 0 as dummy job.
    It also could never find a job in that case, because via HybridDM it
    put a filter on it (to filter only on ATWORK). This filter was however
    not removed, so after that it did never founnd a job (for a workspot with
    Measure Productivity set to false).
  MRA:27-DEC-2017 PIM-337
  - Maximize form optional via argument -M
  - Do not close (or minimize) form optional via argument -N
  - It should log when there was a no-Oracle-connection-error.
  MRA:10-JAN-2018 PIM-345
  - TimeRecording
  - PersonalScreen
    - Access violation in Timerecording-program + PersonalScreen
    - When using a Mifare-card-reader it gives an access violation
      when using Timerecording-program and Timerecording-part in PersonalScreen.
    - Solution: During OpenScanner and CloseScanner it referred to a base-form that did
      not exist/was not created.
    - Solved by changing OpenScanner/CloseScanner-procedures in TimeRecordingFRM to
      MyOpenScanner/MyCloseScanner and within these procedures call OpenScanner/CloseScanner,
      instead of BaseSerial.OpenScanner/BaseSerial.CloseScanner!
  MRA:23-JAN-2018 PIM-346
  - Related to this issue.
  - Be sure it does a Commit or Rollback after a database-insert/update/delete.
  MRA:23-JAN-2018 PIM-346
  - Turn off extra logging (UseLogging = False)
  MRA:26-FEB-2018 GLOB3-85
  - TimeRecording
  - Show non-occupied workspots
  MRA:06-APR-2018 GLOB3-112
  - Related to this order:
    - It should be possible to read via RAW-ID via Device=RAW, so the ID
      that is read is first checked via RAW-field in IDCard. When found,
      it reads the IDCARD-ID itself and uses that for the rest.
    - This is triggered via a serialdevice where SERIALDEVICE_CODE = 'RAW'.
      This sets a RAW-property to True.
      Via this we know we first have to check the RAW-field and when found
      read the IDCARD-id.
  MRA:09-APR-2018 PIM-368
  - Timerecording and Shift Selection - rework
  - Only give the shift-selection if there was shift-schedule found, if there
    is no shift-schedule defined then do not give the shift-selection and take
    the shift on employee-level as default.
  MRA:30-APR-2018 PIM-337.2 Rework
  - When -M is used then the dialog is not maximized so it fills the
    whole screen. This should be checked/fixed. It should start at 0,0
    (left corner) till the taskbar (taskbar should still be visible).
  MRA:2-JUL-2018 GLOB3-60
  - Extension 4 to 10 blocks for planning
  MRA:23-JUL-2018 PIM-383
  - Timerecording and ask for emp-nr gives problem with raw-option
  MRA:23-JUL-2018 GLOB3-139
  - Add function that changes ID via bit-changes
  - It reads an ID in bits with length 16, convert this to decimal value.
  - Triggered via serial device named H5427.
  MRA:5-SEP-2018 PIM-397
  - Timerecording automatic close when computer is shutdown
  MRA:4-JAN-2019 GLOB3-202
  - Scan via machine-card readers and do not scan out
  - Also added the handling for single employee (one employee allowed on one
    workspot) and roaming workspot (related to PIM-181).
  MRA:20-FEB-2019 GLOB3-262
  - Timerecording and ATWORK-job handling
  - Do not ask for a job when only ATWORK/OCCUPIED can be selected, in that
    case take this job as default job.
  MRA:8-MAR-2019 GLOB3-272
  - Timerecording and change needed for 'last scan is not closed'-dialog
  MRA:17-APR-2019 GLOB3-272 - Rework
  - Timerecording and change needed for 'last scan is not closed'-dialog
  MRA:21-JUN-2019     GLOB3-324
  - Add device named pcProx for Timerecording-app.
  - This is used for pcProx Plus readers with cards that should be read
    without changing it. Now it uses Rosslare that triggers the fact it
    is a pcProx Plus-reader, but also converts the ID.
  MRA:8-JUL-2019 GLOB3-330
  - Make functionality with 'occupied' optional
  MRA:22-JUL-2019 GLOB3-324
  - Related to this issue: Show device as extra info in caption.
*)

unit TimeRecordingFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BasePimsSerialFRM, StdCtrls, ComDrvN, Buttons, ExtCtrls, TimeRecordingDMT,
  UGlobalFunctions, DB, Registry, FileCtrl, UScannedIDCard, ImgList,
  Variants, Oracle, DateUtils, {PersonalScreenDMT, } {UProductionScreen}
  {UPersonalScreen, } UProductionScreenDefs,
  PCSCRaw, PCSCDef, Reader, UPCSCCardReader, ComCtrls, // 20013176
  UPimsConst, PCPROXAPI, UPCProcsPlusReader, jpeg;

const
  TimeNull = '00:00' ;
  LOG_FILENAME = 'TREC_';
  Height_BoxMessage = 50;
  MaxWidth = 800; // TD-23620
  MaxHeight = 600; // TD-23620
  NOTRIM_COMMAND = 'NOTRIM'; // TD-24520.60

type
  TState = (SEnterID, SScanning, SScaned, SWaiting, SError);
//  TErrorCode = (ecOK, ecIDCardExpired, ecIDCardNotFound, ecEmployeeExpired,
//    ecEmployeeNotFound, ecScanExists, ecNonScanEmployee, ecUnKnown);

//  TScanError = procedure(const Msg : string) of object;
  TScanAbort = procedure of object;

  TTimeRecordingF = class(TBasePimsSerialForm)
    GroupBoxEmployee: TGroupBox;
    GroupBoxDateTime: TGroupBox;
    LabelCurentDate: TLabel;
    Timer5Sec: TTimer;
    GroupBoxError: TGroupBox;
    GroupBoxWorkspot: TGroupBox;
    GroupBoxPrevious: TGroupBox;
    GroupBoxWorked: TGroupBox;
    LabelLastWeek: TLabel;
    LabelThisWeek: TLabel;
    GroupBoxTime: TGroupBox;
    LabelLastWeekVal: TLabel;
    LabelThisWeekVal: TLabel;
    LabelTimeForTime: TLabel;
    LabelHoliday: TLabel;
    GroupBoxMessages: TGroupBox;
    LabelTimeForTimeVal: TLabel;
    LabelTimeReductionVal: TLabel;
    LabelErrorMsg: TLabel;
    LabelTimeReduction: TLabel;
    LabelHolidayVal: TLabel;
    lblError: TLabel;
    TimerAutoClose: TTimer;
    CommandComboBox: TComboBox;
    GroupBoxSmartCard: TGroupBox;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    RichEdit: TRichEdit;
    ReaderListBox: TListBox;
    pnlEmployeeTop: TPanel;
    pnlEmployeeLeft: TPanel;
    LabelIDCard: TLabel;
    Panel1: TPanel;
    BitBtnAccept: TBitBtn;
    BitBtnCancel: TBitBtn;
    pnlEmployeeClient: TPanel;
    EditIDCard: TEdit;
    pnlEmployeeClient2: TPanel;
    pnlEmployeeLeft2: TPanel;
    LabelEmployee: TLabel;
    EditEmployeeCode: TEdit;
    pnlEmployeeClient3: TPanel;
    EditEmployeeDescription: TEdit;
    pnlPreviousLeft1: TPanel;
    pnlPreviousClient1: TPanel;
    pnlPreviousLeft2: TPanel;
    LabelPlant: TLabel;
    LabelFrom: TLabel;
    pnlPreviousClient2: TPanel;
    EditPreDateFrom: TEdit;
    EditPrePlant: TEdit;
    pnlPreviousLeft3: TPanel;
    LabelJob: TLabel;
    LabelWorkspot: TLabel;
    pnlPreviousClient3: TPanel;
    EditPreWorkspot: TEdit;
    EditPreJob: TEdit;
    Timer1Sec: TTimer;
    TimerPCProxPlus: TTimer;
    Image1: TImage;
    procedure Timer5SecTimer(Sender: TObject); //every 5 sec exec procedure
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure BitBtnAcceptClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EditIDCardKeyPress(Sender: TObject; var Key: Char);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure EditPreDateFromKeyPress(Sender: TObject; var Key: Char);
    procedure EditPreDateFromClick(Sender: TObject);
    procedure BitBtnCancelClick(Sender: TObject);
    procedure TimerAutoCloseTimer(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure Timer1SecTimer(Sender: TObject);
    procedure TimerPCProxPlusTimer(Sender: TObject);
  protected
    procedure WMTimeChange(var Message: TMessage); message WM_TIMECHANGE;
    procedure WMSysCommand(var Msg: TWMSysCommand); message WM_SYSCOMMAND;
    procedure WMQueryEndSession(var Message: TWMQueryEndSession); message WM_QUERYENDSESSION; // PIM-397
  private
    { Private declarations }
    ThisCaption: String;
    FStatus : TState;
    FOnScanError : TScanError;
    FOnScanAbort : TScanAbort;
    FVerifyPosition: Integer;
    FVerifyString: String;
    FValuePosition: Integer;
    FDeviceSettingsFound: Boolean;
    DatesStart, DatesEnd: array of TDateTime;
    TimeRecording_YN, Show_Hour_YN, Hide_ID_YN: String; // 20014035
    IsScanWorkstation: Boolean;
    FCurrentNow: TDateTime;
    // Logging
    UseLogging: Boolean;
//    LogPath: String;
    LogRootPath: String;
    FIsScanning: Boolean;
    FIsError: Boolean;
    IsMaximized: Boolean;
    BtnAcceptHeight: Integer;
    BtnAcceptTop: Integer;
    BtnCancelHeight: Integer;
    BtnCancelTop: Integer;
    FStoreRawID: Boolean;
    FIsSmartCard: Boolean;
    FInitString: String;
    FNoTrim: Boolean;
    FLastNow: TDateTime;
    FMyEndOfDay: Boolean;
    FAutoClose: Boolean;
    FDoNotScanOut: Boolean;
    procedure DetermineSerialDeviceSettings;
    procedure ProcessCodeRead(const ACodeRead: String);
    procedure SetVerifyPosition(const Value: Integer);
    procedure SetVerifyString(const Value: String);
    procedure SetValuePosition(const Value: Integer);
    procedure SetDeviceSettingsFound(const Value: Boolean);
    procedure SetIsScanning(const Value: Boolean);
    function DetermineTimeBlocks(const APlantCode, ADepartmentCode: String;
      const AEmployeeNumber, AShiftNumber: Integer; const Where: String;
      var ADataSet: TDataSet; const ADayStr: String;
      var AShiftStartTime: TDateTime): Boolean;
    function DetermineTBTimes(const APlantCode, ADepartmentCode: String;
      const AEmployeeNumber, AShiftNumber: Integer;
      const AEmployeePlanningDate: TDateTime; Where: String): Boolean;
    procedure DetermineScheduledTimeblocks(AQuery: TOracleQuery;
      const AShiftNumber: Integer;
      var APlannedWorkspot, APlannedPlant, APlannedDepartment: String;
      var APlannedShiftNumber: Integer; var ANextIDCard: TScannedIDCard;
      const AEmployeePlanningDate: TDateTime;
      var TBEnd: TDateTime);
    procedure DetermineWorkstation;
    procedure DeterminePlannedWorkspot(var ANextIDCard: TScannedIDCard;
      var APlannedWorkspot, APlannedPlant, APlannedDepartment: String;
      var APlannedShiftNumber: Integer);
    procedure DetermineStandardPlanningWorkspot(var ANextIDCard: TScannedIDCard;
      var APlannedWorkspot, APlannedPlant, APlannedDepartment: String;
      var APlannedShiftNumber: Integer);
    procedure DetermineWorkspotByDepartment(const APlannedPlant,
      APlannedDepartment: String; var APlannedWorkspot: String);
    function DetermineEmployeeSelectsNextWorkspotAndJob(
      ALastIDCard: TScannedIDCard; // PIM-180
      var ANextIDCard: TScannedIDCard;
      const APlannedWorkspot, APlannedPlant: String;
      const APlannedShiftNumber: Integer;
      const AIsFromPlanning: Boolean): Boolean; // PIM-112
    function DeterminePersonalScreenJob(
      ALastIDCard: TScannedIDCard;
      var ANextIDCard: TScannedIDCard;
      const APlannedWorkspot, APlannedPlant: String;
      const APlannedShiftNumber: Integer;
      const AIsFromPlanning: Boolean): Boolean;
    procedure DetermineShift(var ANextIDCard: TScannedIDCard;
      const APlannedShiftNumber: Integer);
    procedure DetermineDepartmentByWorkspot(var ANextIDCard: TScannedIDCard);
    procedure DetermineStatistics(ANextIDCard, ALastIDCard: TScannedIDCard);
(*    procedure Determine24HourScan(var AEndOfDayError: Boolean;
      var ASavePrevPlant, ASavePrevWK, ASavePrevJob, ASavePrevDate: String;
      var ALastIDCard, ANextIDCard: TScannedIDCard); *)
    function ValueIDCardKeyboard(var ACode : String): TErrorCode;
    function ValueIDCardSmartCard(var ACode : String): TErrorCode;
    function ValueIDCard(ACode : String): TErrorCode;
// they are all private  public
    { Public declarations }
    property DeviceSettingsFound: Boolean read FDeviceSettingsFound
      write SetDeviceSettingsFound;
    property ScanStatus: TState read FStatus write FStatus;
    property OnScanError: TScanError read FOnScanError write FOnScanError;
    property OnScanAbort: TScanAbort read FOnScanAbort write FOnScanAbort;
    property VerifyString: String read FVerifyString write SetVerifyString;
    property VerifyPosition: Integer read FVerifyPosition write SetVerifyPosition;
    property ValuePosition: Integer read FValuePosition write SetValuePosition;
    procedure StartScanningPost(const ACode: String);
    procedure StartScanning(ACode: String);
    procedure ErrorScanning(const AMsg: String);
    procedure ScanAbort;
    procedure ClearFields;
    //CAR 27-03-2003
    procedure EnabledWk_JobField(const AEnabledFields: Boolean);
    procedure SetEndOfDay(var AIDCard: TScannedIDCard);
    procedure ResetEndOfDay;
(*    procedure UpdateTRSCompleteScan(var ANextIDCard, ALastIDCard:
      TScannedIDCard; const Is24HrScan: Boolean); *)
(*    procedure UpdateTRSMultipleScan(AIDCard: TScannedIDCard); *)
    // Logging
    procedure AddLog(AMsg: String);
//    function NewLogPath: String;
    procedure ReadRegistry;
    procedure WriteRegistry;
    procedure LabelErrorMsgDone;
    procedure MyOpenScanner;
    procedure MyCloseScanner;
    function HandleScanAction(ALastIDCard,
      ANextIDCard: TScannedIDCard;
      AEditPrePlantText: String;
      AIsScanning: Boolean;
      ASavePrevPlant, ASavePrevWK, ASavePrevJob,
      ASavePrevDate: String;
      ADoNotScanOut: Boolean=False): Boolean;
    property IsScanning: Boolean read FIsScanning write SetIsScanning;
    property IsError: Boolean read FIsError write FIsError;
  public
    // 20013176
    MyBuffer: String;
    MyHexBuffer: String;
    CardReader: TPCSCCardReader;
    PCProcsPlusReader: TPCProcsPlusReader;
    procedure CardReaderProcessScan;
    procedure ReadCardID_3a;
    procedure ReadCardID_3b;
    procedure AddErrorLog(AMsg: String);
    procedure HandleButtonSize;
    function IDConvert(AIDCode: String): String;
    function EndOfShiftTest(ANextIDCard: TScannedIDCard): Boolean;
    procedure InsertTRSNewScan(var AIDCard: TScannedIDCard;
      const AOpen: Boolean; const AIDCardOut: String);
    property StoreRawID: Boolean read FStoreRawID write FStoreRawID;
    property IsSmartCard: Boolean read FIsSmartCard write FIsSmartCard;
    property InitString: String read FInitString write FInitString; // 20015371
    property NoTrim: Boolean read FNoTrim write FNoTrim; // TD-24520.60
    property LastNow: TDateTime read FLastNow write FLastNow; // 20015346
    property CurrentNow: TDateTime read FCurrentNow write FCurrentNow; // ABS-18715
    property MyEndOfDay: Boolean read FMyEndOfDay write FMyEndOfDay; // PIM-161
    property AutoClose: Boolean read FAutoClose write FAutoClose; // PIM-397
    property DoNotScanOut: Boolean read FDoNotScanOut write FDoNotScanOut; // GLOB3-202
  end;

var
  TimeRecordingF: TTimeRecordingF;

implementation

uses
  SelectorFRM, UPimsMessageRes, ORASystemDMT, {GridBaseDMT, }
  CalculateTotalHoursDMT, GlobalDMT, OracleData, DialogChangeJobDMT,
  DialogChangeJobFRM, DialogAskEmployeeNumberFRM,
  EnterJobCommentDMT, DialogMessageFRM, HybridDMT, DialogShiftCheckDMT,
  DialogShiftCheckFRM;

{$R *.DFM}

function PathCheck(Path: String): String;
begin
  if Path <> '' then
    if Path[length(Path)] <> '\' then
      Path := Path + '\';
  Result := Path;
end;

function MyZeroFormat(Value: String; Len: Integer): String;
begin
  while Length(Value) < Len do
    Value := '0' + Value;
  Result := Value;
end;

// RV064.2.
procedure TTimeRecordingF.AddLog(AMsg: String);
begin
  // Use log-procedure from UGlobalFunctions.
  if UseLogging then
    WDebugLog(AMsg);
end;

procedure TTimeRecordingF.ClearFields;
var
  Cmps : integer;
begin
  For Cmps := 0 to ComponentCount-1 do
  begin
    if (Components[cmps] is TEdit) then
      TEdit(Components[Cmps]).text := NullStr;
  end;
  LabelErrorMsg.Caption         := '';
  LabelErrorMsg.Visible         := False;
  LabelLastWeekVal.Caption      := TimeNull;
  LabelThisWeekVal.Caption      := TimeNull;
  LabelTimeForTimeVal.Caption   := TimeNull;
  LabelTimeReductionVal.Caption := TimeNull;
  LabelHolidayVal.Caption       := TimeNull;
//car 27-03-2003
  EnabledWk_JobField(False);

  if Self.Active then
    EditIDCard.SetFocus;
  lblError.Caption := '';
end; // ClearFields

// 550478. Personal Screen.
// Personal Screen Job
// 20012858 - Addition of Break/Continue buttons
function TTimeRecordingF.DeterminePersonalScreenJob(
  ALastIDCard: TScannedIDCard;
  var ANextIDCard: TScannedIDCard;
  const APlannedWorkspot, APlannedPlant: String;
  const APlannedShiftNumber: Integer;
  const AIsFromPlanning: Boolean): Boolean;
var
  SelectRes: Integer;
  // Not needed.
{  NewPlantCode, NewPlantDescription: String;
  NewWorkspotCode, NewWorkspotDescription: String;
  NewJobCode, NewJobDescription: String; }
  procedure SetSelectorPos;
  begin
    // TD-23620
    // When smaller than MaxWidth:
    // - Instead of relative to GroupBoxError, make it relative to
    //   GroupBoxWorkspot
    if Width < MaxWidth then
      DialogChangeJobF.Top := ClientOrigin.Y + GroupBoxWorkspot.Top
    else
      DialogChangeJobF.Top := ClientOrigin.Y + GroupBoxError.Top;
    DialogChangeJobF.Left   := ClientOrigin.X + 2;
    if Width < MaxWidth then
      DialogChangeJobF.Height := GroupBoxWorkspot.Height + GroupboxError.Height
    else
      DialogChangeJobF.Height := GroupboxError.Height;
    DialogChangeJobF.Width  := GroupboxError.Width;
  end;
begin
  Result := False;
  // Show Dialog Change Job.
  SetSelectorPos;
  DialogChangeJobDM.ProdScreenType := TimeRecordingDM.ProdScreenType;
  DialogChangeJobF.ProdScreenType := TimeRecordingDM.ProdScreenType;
  // Either 'Down/Continue' can be used OR
  // 'Break/Continue'. Not both.
  DialogChangeJobF.UseDownButton(False);
  // Use Break/Continue buttons.
  DialogChangeJobF.UseBreakButton(ORASystemDM.BreakBtnVisible); // PIM-12
  // Do NOT use Continue button.
  DialogChangeJobF.UseContinueButton(False);
  // Use Lunch button.
  DialogChangeJobF.UseLunchButton(ORASystemDM.LunchBtnVisible); // PIM-12
  // Use End-Of-Day-button.
  DialogChangeJobF.UseEndOfDayButton(ORASystemDM.PSEndOfDayBtnVisible); // PIM-12
  // Is last job a break job? -> Not needed.
//  NewJobCode := '';
  if ALastIDCard.JobCode = BREAKJOB then
  begin
    // This is not needed, because continue-button is not used.
    // Find a previous job
{
    TimeRecordingDM.DeterminePrevJob(TimeRecordingDM.PlantCode,
      TimeRecordingDM.WorkspotCode,
      BREAKJOB, BREAKJOB,
      ANextIDCard.EmployeeCode,
      NewPlantCode, NewWorkspotCode, NewJobCode,
      NewPlantDescription, NewWorkspotDescription,
      NewJobDescription);
}
    // Enable/disable of break-button is done in the dialog itself based
    // on this boolean.
    DialogChangeJobF.IsBreak := True;
//    DialogChangeJobF.BreakButtonEnable(False);
    // Do not use continue button for break.
{
    if NewJobCode <> '' then
      DialogChangeJobF.ContinueButtonEnable(True)
    else
      DialogChangeJobF.ContinueButtonEnable(False);
}
  end
  else
  begin
    // Enable/disable of break-button is done in the dialog itself based
    // on this boolean.
    DialogChangeJobF.IsBreak := False;
//    DialogChangeJobF.BreakButtonEnable(True);
    // Do not use continue button for break.
{    DialogChangeJobF.ContinueButtonEnable(False); }
  end;
  DialogChangeJobDM.CurrentJobCode := TimeRecordingDM.CurrentJobCode;
  DialogChangeJobDM.PlantCode := TimeRecordingDM.PlantCode;
  case TimeRecordingDM.ProdScreenType of
  pstMachineTimeRec: // Machine + Time Rec
    begin
      DialogChangeJobDM.MachineCode :=
        TimeRecordingDM.MachineCode;
      DialogChangeJobDM.MachineDescription :=
        TimeRecordingDM.MachineDescription;
      DialogChangeJobDM.WorkspotCode := '';
      DialogChangeJobF.EndOfDayBlink := EndOfShiftTest(ANextIDCard);
      SelectRes := DialogChangeJobF.ShowModal;
      case SelectRes of
      ModalOK:
        begin
          Result := True;
          // Set Plant + Workspot
          ANextIDCard.WorkSpotCode := TimeRecordingDM.WorkspotCode;
          ANextIDCard.WorkSpot     := TimeRecordingDM.WorkspotDescription;
          ANextIDCard.PlantCode    := TimeRecordingDM.PlantCode;
          ANextIDCard.Plant        := '';
          // Switch job for machine.
//          NewWorkspotCode := DialogChangeJobF.WorkspotCode;
          ANextIDCard.JobCode := DialogChangeJobF.JobCode;
          ANextIDCard.Job     := DialogChangeJobF.JobDescription;
        end;
      ModalEndOfDay:
        begin
          Result := True;
          SetEndOfDay(ANextIDCard);
        end;
      ModalCancel:
        begin
          Result := False;
          OnScanAbort;
          Exit;
        end;
      end; // case
    end; // machine
  pstWorkspotTimeRec, pstWSEmpEff: // Workspot + Time Rec
    begin
      DialogChangeJobDM.MachineCode := '';
      DialogChangeJobDM.WorkspotCode := TimeRecordingDM.WorkspotCode;
      DialogChangeJobDM.WorkspotDescription :=
        TimeRecordingDM.WorkspotDescription;
      DialogChangeJobF.EndOfDayBlink := EndOfShiftTest(ANextIDCard);
      SelectRes := DialogChangeJobF.ShowModal;
      case SelectRes of
      ModalOK:
        begin
          Result := True;
          // Set Plant + Workspot
          ANextIDCard.WorkSpotCode := TimeRecordingDM.WorkspotCode;
          ANextIDCard.WorkSpot     := TimeRecordingDM.WorkspotDescription;
          ANextIDCard.PlantCode    := TimeRecordingDM.PlantCode;
          ANextIDCard.Plant        := '';
          // Switch job for workspot.
          // When there were no jobs to choose from, it will be an empty job.
          if DialogChangeJobF.JobCode <> '' then
          begin
            ANextIDCard.JobCode := DialogChangeJobF.JobCode;
            ANextIDCard.Job     := DialogChangeJobF.JobDescription;
          end
          else
          begin
            ANextIDCard.JobCode := DUMMYSTR;
            ANextIDCard.Job     := DUMMYSTR
          end;
        end;
      ModalLunch, // PIM-139
      ModalEndOfDay:
        begin
          Result := True;
          SetEndOfDay(ANextIDCard);
        end;
      ModalCancel:
        begin
          Result := False;
          OnScanAbort;
          Exit;
        end;
      ModalBreak: // Switch to Break Job. Must autom. be created!
        begin
          Result := True;
          // Create break job, if it did not exist yet.
          TimeRecordingDM.CheckAddJobForWorkspot(TimeRecordingDM.PlantCode,
            TimeRecordingDM.WorkspotCode, BREAKJOB, SPimsBreakJobDescription);
          // Set Plant + Workspot
          ANextIDCard.WorkSpotCode := TimeRecordingDM.WorkspotCode;
          ANextIDCard.WorkSpot     := TimeRecordingDM.WorkspotDescription;
          ANextIDCard.PlantCode    := TimeRecordingDM.PlantCode;
          ANextIDCard.Plant        := '';
          ANextIDCard.JobCode      := BREAKJOB;
          ANextIDCard.Job          := SPimsBreakJobDescription;
        end;
      // Do not use continue for break.
{
      ModalContinue: // Return from Break Job
        begin
          Result := True;
          // Was there a previous job?
          if NewJobCode <> '' then
          begin
            ANextIDCard.WorkSpotCode := NewWorkspotCode;
            ANextIDCard.WorkSpot     := NewWorkspotDescription;
            ANextIDCard.PlantCode    := NewPlantCode;
            ANextIDCard.Plant        := NewPlantDescription;
            ANextIDCard.JobCode      := NewJobCode;
            ANextIDCard.Job          := NewJobDescription;
          end
          else
          begin
            Result := False;
            OnScanAbort;
            Exit;
          end;
        end;
}
      end; // case
    end; // workspot
  end;
  if Result then
  begin
    // show description
    //CAR 24-03-2003
    EditPreJob.Text      := ANextIDCard.Job;
    EditPreDateFrom.Text := FormatDateTime(LONGDATE, ANextIDCard.DateIn);
    EnabledWk_JobField(True);
  end;
end; // DeterminePersonalScreenJob

function TTimeRecordingF.DetermineEmployeeSelectsNextWorkspotAndJob(
  ALastIDCard: TScannedIDCard;
  var ANextIDCard: TScannedIDCard;
  const APlannedWorkspot, APlannedPlant: String;
  const APlannedShiftNumber: Integer;
  const AIsFromPlanning: Boolean): Boolean;
var
  SelectorPos: TFormPos;
  SelectRes: Integer;
  SelectorColumns: array of String;
  JobRecordCount: Integer;
  procedure SetSelectorPos(var AFormPos : TFormPos);
  begin
    // TD-23620
    // When smaller than MaxWidth:
    // - Instead of relative to GroupBoxError, make it relative to
    //   GroupBoxWorkspot
    if Width < MaxWidth then
      AFormPos.Top := ClientOrigin.Y + GroupBoxWorkspot.Top
    else
      AFormPos.Top := ClientOrigin.Y + GroupBoxError.Top;
    AFormPos.Left   := ClientOrigin.X + 2;
    if Width < MaxWidth then
      AFormPos.Height := GroupBoxWorkspot.Height + GroupboxError.Height
    else
      AFormPos.Height := GroupboxError.Height;
    AFormPos.Width  := GroupboxError.Width;
  end;
  // PIM-181
  // After an employee has selected a workspot but not yet the job:
  // Check if there is already an employee scanned in on the same workspot,
  // if so, then ask if that scan must be closed or not.
  // When yes: Close the scan for other employee and after that let
  //           employee choose the job, this can only be the ATWORK-job.
  // When no: Leave the other scan open and let the employee choose the job,
  //          this can only be the OCCUPIED-job.
  function HybridModeAction: Boolean;
  var
    DialogMessageResult: TModalResult;
    SavePrevPlant, SavePrevWK, SavePrevJob, SavePrevDate: String;
  begin
    Result := True;
    // PIM-181
    if HybridDM.HybridMode = 1 then
    begin
      // PIM-179 Only do this for workspot with Measure Productivity = Yes
      if (ANextIDCard.Plant = SpimsEndDay) then
      begin
        if not ALastIDCard.MeasureProductivity then
          Exit;
      end
      else
        if not ANextIDCard.MeasureProductivity then
          Exit;

      try
        HybridDM.IsOccupied := False;
        HybridDM.CloseCurrentScan := False;
        if (ANextIDCard.Plant = SpimsEndDay) then
          HybridDM.IsOccupied := False
        else
        begin
          // A scan for the same plant+workspot found?
          HybridDM.IsOccupied :=
            HybridDM.HybridWorkspotCheck(ALastIDCard,
              ANextIDcard, CurrentNow, ALastIDCard.JobCode);
        end;
        if HybridDM.IsOccupied then
        begin
          case ORASystemDM.Occup_Handling of
          0: // Show message 'workspot is occupied, go to supervisor'. No further action.
            begin
              DialogMessageResult := HybridDM.HybridDialogGoToSupervisorMessage(
                TimeRecordingF.Left,
                TimeRecordingF.Top + GroupBoxEmployee.Height + GroupBoxDateTime.Height,
                TimeRecordingF.Width,
                TimeRecordingF.Height);
              Result := False;
            end;
          1: // Show message 'workspot is occupied', and give option to change
             //  places or not.
            begin
              DialogMessageResult := HybridDM.HybridDialogMessageOKCancel(
                TimeRecordingF.Left,
                TimeRecordingF.Top + GroupBoxEmployee.Height + GroupBoxDateTime.Height,
                TimeRecordingF.Width,
                TimeRecordingF.Height);
              if DialogMessageResult = mrYes then
              begin
                // GLOB3-202 Get roaming workspot based on plant of other employee
                if DoNotScanOut then
                  TimeRecordingDM.
                    DetermineRoamingWorkspot(HybridDM.LastIDCard.PlantCode);

                // Close current scan from other employee
                HybridDM.NextIDCard.DateIn := RoundTimeConditional(CurrentNow, 1);
                SetEndOfDay(HybridDM.NextIDCard);
                HandleScanAction(HybridDM.LastIDCard, HybridDM.NextIDCard,
                  SpimsEndDay, True, SavePrevPlant, SavePrevWK,
                  SavePrevJob, SavePrevDate,
                  DoNotScanOut);
                if ORASystemDM.OracleSession.InTransaction then
                  ORASystemDM.OracleSession.Commit;
                AddLog('HandleScanAction-Commit');
                ResetEndOfDay;
                HybridDM.CloseCurrentScan := True;
              end
              else
                Result := False;
            end;
          2: // Show message 'workspot is occupied.' Give option to change
             // places or not, with option to scan-in on occupied-job.
            begin
              DialogMessageResult := HybridDM.HybridDialogMessage(
                TimeRecordingF.Left,
                TimeRecordingF.Top + GroupBoxEmployee.Height + GroupBoxDateTime.Height,
                TimeRecordingF.Width,
                TimeRecordingF.Height);
              if DialogMessageResult = mrYes then
              begin
                // GLOB3-202 Get roaming workspot based on plant of other employee
                if DoNotScanOut then
                  TimeRecordingDM.
                    DetermineRoamingWorkspot(HybridDM.LastIDCard.PlantCode);

                // Close current scan from other employee
                HybridDM.NextIDCard.DateIn := RoundTimeConditional(CurrentNow, 1);
                SetEndOfDay(HybridDM.NextIDCard);
                HandleScanAction(HybridDM.LastIDCard, HybridDM.NextIDCard,
                  SpimsEndDay, True, SavePrevPlant, SavePrevWK,
                  SavePrevJob, SavePrevDate,
                  DoNotScanOut);
                if ORASystemDM.OracleSession.InTransaction then
                  ORASystemDM.OracleSession.Commit;
                AddLog('HandleScanAction-Commit');
                ResetEndOfDay;
                HybridDM.CloseCurrentScan := True;
              end // if
              else
                if DialogMessageResult = mrCancel then
                  Result := False;
            end; // case 2
          end; // end case
        end; // if
      except
        on E:EOracleError do
        begin
          if ORASystemDM.OracleSession.InTransaction then // PIM-346
            ORASystemDM.OracleSession.Rollback;
          FOnScanError(E.Message);
        end;
        on E:Exception do
        begin
          if ORASystemDM.OracleSession.InTransaction then // PIM-346
            ORASystemDM.OracleSession.Rollback;
          FOnScanError(E.Message);
        end;
      end;
    end; // if
  end; // HybridModeAction
  // GLOB3-85 Via IsNTS we can show only non-occupied workspots. If nothing
  //          was found, call it again with AGo=False.
  procedure DetermineWorkspots(AGo: Boolean);
  begin
    with TimeRecordingDM.odsWork do
    begin
      Active := False;
      DeleteVariables;
      SQL.Clear;
      SQL.Add(
        'SELECT DISTINCT' + NL +
        '  ''  '' || P.DESCRIPTION PLANTNAME,' + NL +
        '  ''  '' || W.DESCRIPTION WORKSPOTNAME,' + NL +
        '  WPS.PLANT_CODE,' + NL +
        '  WPS.WORKSPOT_CODE,' + NL +
        '  W.MEASURE_PRODUCTIVITY_YN,' + NL + // PIM-179
        '  WPS.SEQUENCE_NUMBER' + NL +
        'FROM' + NL +
        '  WORKSPOTPERWORKSTATION WPS,' + NL +
        '  WORKSPOT W,' + NL +
        '  PLANT P' + NL +
        'WHERE' + NL +
        '  WPS.COMPUTER_NAME = :COMPUTER_NAME' + NL + // & -> : (GLOB3-85)
        '  AND WPS.PLANT_CODE = W.PLANT_CODE' + NL +
        '  AND WPS.WORKSPOT_CODE = W.WORKSPOT_CODE' + NL +
        '  AND WPS.PLANT_CODE = P.PLANT_CODE' + NL +
        '  AND' + NL +
        '  (' + NL +
        '    (W.DATE_INACTIVE IS NULL) OR' + NL +
        '    (W.DATE_INACTIVE > :DATE_INACTIVE)' + NL + // & -> : (GLOB3-85)
        '  )'
        );
      // GLOB3-85
      if AGo and (HybridDM.HybridMode = 1) and ORASystemDM.IsNTS then
        SQL.Add(
        '  AND W.WORKSPOT_CODE NOT IN ( ' + NL +
        '  SELECT ' + NL +
        '    TRS.WORKSPOT_CODE ' + NL +
        '  FROM ' + NL +
        '    TIMEREGSCANNING TRS INNER JOIN WORKSPOT W ON ' + NL +
        '      TRS.PLANT_CODE = W.PLANT_CODE AND ' + NL +
        '      TRS.WORKSPOT_CODE = W.WORKSPOT_CODE ' + NL +
        '  WHERE ' + NL +
        '    W.MEASURE_PRODUCTIVITY_YN = ''Y'' AND ' + NL +
        '    (TRS.PLANT_CODE = P.PLANT_CODE) AND ' + NL +
        '    ( ' + NL +
        '      (TRS.DATETIME_IN >= :TIMEIN) AND ' + NL +
        '      (TRS.DATETIME_IN <= :TIMENOW) AND ' + NL +
        '      (TRS.DATETIME_IN IS NOT NULL) AND ' + NL +
        '      (TRS.PROCESSED_YN = ''N'') ' + NL +
        '    ) ' + NL +
        '  ) ' + NL
        );
      // Add to main select the planned Workspots
      if APlannedWorkspot <> NullStr then
      begin
        SQL.Add(
          '  AND ((W.WORKSPOT_CODE <> :WORKSPOT_CODE)' + NL +
          '  OR (W.PLANT_CODE <> :PLANT_CODE))' + NL +
          'UNION' + NL +
          'SELECT DISTINCT'
          );
        if AIsFromPlanning then
        begin
          // if workspot is planeed on
          SQL.Add('''* '' || P.DESCRIPTION PLANTNAME,' + NL +
                  '''* '' || W.DESCRIPTION WORKSPOTNAME,' + NL
            )
        end
        else
        begin
          // if workspot is standard planeed on
          SQL.Add('''  '' || P.DESCRIPTION PLANTNAME,' + NL +
                  '''  '' || W.DESCRIPTION WORKSPOTNAME,' + NL
            );
        end;
        SQL.Add(
          '  P.PLANT_CODE,' + NL +
          '  W.WORKSPOT_CODE,' + NL +
          '  W.MEASURE_PRODUCTIVITY_YN,' + NL + // PIM-179
          '  -1' + NL +
          'FROM' + NL +
          '  WORKSPOT W,' + NL +
          '  PLANT P' + NL +
          'WHERE' + NL +
          '  P.PLANT_CODE = W.PLANT_CODE AND' + NL +
          '  W.WORKSPOT_CODE = :WORKSPOT_CODE AND' + NL +
          '  P.PLANT_CODE = :PLANT_CODE' + NL
          );
      end; {if APlannedWorkspot <> NullStr}
      SQL.Add('ORDER BY 6'); // PIM-230 IMPORTANT: This must refer to the SEQUENCE_NUMBER !!! (changed from 5 to 6).
// SQL.SaveToFile('c:\temp\TimeRecording-SelectWorkspots.sql');
      DeclareAndSet('COMPUTER_NAME', otString, ORASystemDM.CurrentComputerName);
      DeclareAndSet('DATE_INACTIVE', otDate,   ANextIDCard.DateIn);
      if APlannedWorkspot <> NullStr then
      begin
        DeclareAndSet('WORKSPOT_CODE', otString, APlannedWorkspot);
        DeclareAndSet('PLANT_CODE',    otString, APlannedPlant);
      end;
      // GLOB3-85
      if AGo and (HybridDM.HybridMode = 1) and ORASystemDM.IsNTS then
      begin
        DeclareAndSet('TIMEIN', otDate, Now - 0.5);
        DeclareAndSet('TIMENOW', otDate, RoundTimeConditional(Now, 1));
      end;
      Active := True;
    end; // with
  end;  // DetermineWorkspots;
  function SelectNextWorkspot: Boolean;
  begin
    Result := True;
    with TimeRecordingDM.odsWork do
    begin
      // GLOB3-85
      DetermineWorkspots(True);
      if Eof then
        DetermineWorkspots(False);

      SetLength(SelectorColumns, 3);
      SelectorColumns[0] := SPimsWorkspot;
      SelectorColumns[1] := SPimsColumnPlant;
      SelectorColumns[2] := SPimsColumnWorkspot;

      if not Eof then
      begin
        //Select the planned workspot or the first department-workspot
        if APlannedWorkspot <> NullStr then
          Locate('WORKSPOT_CODE;PLANT_CODE',
          VarArrayOf([APlannedWorkspot, APlannedPlant]),[]);
        SetSelectorPos(SelectorPos);
        SelectorF.JobSelection := False;
        try
          // PIM-228 There was a previous scan?
          if ALastIDCard.Plant <> SPimsStartDay then
          begin
            SelectorF.OldJobCode := ALastIDCard.JobCode;
            SelectorF.btnBreak.Visible := ORASystemDM.BreakBtnVisible;
            SelectorF.btnLunch.Visible := ORASystemDM.LunchBtnVisible;
            // PIM-181 Do no allow to choose for break/lunch when current job is
            //         OCCUPIED.
            if (HybridDM.HybridMode = 1) and (ALastIDCard.MeasureProductivity) then
            begin
              if (ALastIDCard.JobCode = '') or (ALastIDCard.JobCode = OCCUPJOB) then
              begin
                if ORASystemDM.BreakBtnVisible then
                  SelectorF.btnBreak.Visible := False;
                if ORASystemDM.LunchBtnVisible then
                  SelectorF.btnLunch.Visible := False;
              end;
            end;
          end
          else
          begin
            SelectorF.OldJobCode := '';
            SelectorF.btnBreak.Visible := False;
            SelectorF.btnLunch.Visible := False;
          end;
          SelectorF.EndOfDayBtnVisible := ORASystemDM.TREndOfDayBtnVisible; // PIM-12
          if SelectorF.EndOfDayBtnVisible then // PIM-12
            SelectorF.EndOfDayBlink := EndOfShiftTest(ANextIDCard); // 20014450.50
          SelectRes := SelectorF.EnableSelection(SelectorPos, SelectorColumns,
            TimeRecordingDM.dsrcWork);
        except
          on E:EOracleError do
          begin
            FOnScanError(E.Message);
            Result := False;
            Exit;
          end;
          on E: Exception do
          begin
            FOnScanError(E.Message);
            Result := False;
            Exit;
          end;
        end;
        case SelectRes of
          ModalOk:
            begin
              ANextIDCard.WorkSpotCode := FieldByName('WORKSPOT_CODE').AsString;
              ANextIDCard.WorkSpot     := FieldByName('WORKSPOTNAME').AsString;
              ANextIDCard.PlantCode    := FieldByName('PLANT_CODE').AsString;
              ANextIDCard.Plant        := FieldByName('PLANTNAME').AsString;
              ANextIDCard.MeasureProductivity :=
                FieldByName('MEASURE_PRODUCTIVITY_YN').AsString = 'Y'; // PIM-179
            end;
          ModalLunch,
          ModalEndOfDay:
            begin
              SetEndOfDay(ANextIDCard);
            end;
          ModalBreak: // PIM-112 Switch to Break Job. Must autom. be created! // PIM-228
            begin
              Result := True;
              ANextIDCard.WorkSpotCode := FieldByName('WORKSPOT_CODE').AsString;
              ANextIDCard.WorkSpot     := FieldByName('WORKSPOTNAME').AsString;
              ANextIDCard.PlantCode    := FieldByName('PLANT_CODE').AsString;
              ANextIDCard.Plant        := FieldByName('PLANTNAME').AsString;
              // Create break job, if it did not exist yet.
              TimeRecordingDM.CheckAddJobForWorkspot(ANextIDCard.PlantCode,
                ANextIDCard.WorkSpotCode, BREAKJOB, SPimsBreakJobDescription);
              // Set Job
              ANextIDCard.JobCode := BREAKJOB;
              ANextIDCard.Job     := SPimsBreakJobDescription;
              ANextIDCard.MeasureProductivity :=
                FieldByName('MEASURE_PRODUCTIVITY_YN').AsString = 'Y'; // PIM-179
            end;
          ModalCancel:
            begin
              OnScanAbort;
              Result := False;
              Exit;
            end;
        end; {case}
        // show description
        if ANextIDCard.WorkSpot = SpimsEndDay then
        begin
          EditPreDateFrom.Text := FormatDateTime(LONGDATE, ANextIDCard.DateIn);
        end
        else
        begin
          EditPrePlant.Text    := Copy(ANextIDCard.Plant, 3,
            Length(ANextIDCard.Plant) - 2);
          EditPreWorkspot.Text := Copy(ANextIDCard.WorkSpot, 3,
            Length(ANextIDCard.WorkSpot) - 2);
          // PIM-228
          if ANextIDCard.JobCode = BREAKJOB then
          begin
            EditPreJob.Text      := ANextIDCard.Job;
            EditPreDateFrom.Text := FormatDateTime(LONGDATE, ANextIDCard.DateIn);
          end
          else
          begin
            EditPreJob.Text      := '';
            EditPreDateFrom.Text := '';
          end;
        end;
       EnabledWk_JobField(True);
      end   {not Eof}
      else
      begin {else not Eof}
        FOnScanError(SPimsNoWorkspot);
        Result := False;
        Exit;
      end;  {else not Eof}
      Active := False;
    end;{with TimeRecordingDM.odsWork}
  end; // SelectNextWorkspot
begin
  repeat
    Result := SelectNextWorkspot;
    if not Result then
      Exit;
  until HybridModeAction; // PIM-181 // GLOB3-330

  SelectRes := -1;
  // Determine JobCode
  with TimeRecordingDM.odsJobcode do
  begin
    // PIM-330 HybridJobFilter will put a filter on odsJobCode!
    // Be sure it is not filtered to start with!
    Filtered := False;
    Filter := '';
    // Select Next Job
    if (ANextIDCard.WorkSpotCode <> NullStr) and
     (ANextIDCard.Plant <> SpimsEndDay) and
     (ANextIDCard.JobCode <> BREAKJOB) then
    begin
      if HybridDM.HybridJobCheck(ANextIDCard) then // PIM-179
      begin
        HybridDM.IsOccupied :=
          HybridDM.HybridWorkspotCheck(ALastIDCard, ANextIDcard, CurrentNow,
            ALastIDCard.JobCode);
        HybridDM.HybridJobFilter(TimeRecordingDM.odsJobCode, ANextIDCard); // PIM-179
      end;
      Active := False;
      ClearVariables;
      SetVariable('PLANT_CODE', ANextIDCard.PlantCode);
      SetVariable('WORKSPOT_CODE', ANextIDCard.WorkSpotCode);
      SetVariable('SHOW_AT_SCANNING_YN', CHECKEDVALUE);
      SetVariable('DATE_INACTIVE', Date); // PIM-330
      SetVariable('COMPUTER_NAME', ORASystemDM.CurrentComputerName); // PIM-150
      SetVariable('HYBRID', HybridDM.NewHybridMode(ANextIDCard)); // PIM-179
      Active := True;
      JobRecordCount := RecordCount;

      // PIM-150 Be sure there is a job!!!
      // PIM-150.1 Bugfix: This must NOT be done!
      // REASON: It is possible there are not jobs defined, but
      //         'show businessunits' is used for the workspot!
{      if IsEmpty then
      begin
        FOnScanError(SPimsNoDefaultJobFound);
        Result := False;
        Exit;
      end; }
      SelectorColumns[0] := SPimsJob;
      SelectorColumns[1] := SPimsColumnCode;
      SelectorColumns[2] := SPimsColumnDescription;
      // GLOB3-262 Do not ask for job when it is ATWORK
      if (JobRecordCount > 1) { or
        ((HybridDM.HybridMode = 1) and
         (ANextIDCard.MeasureProductivity)) } then // PIM-179
      begin
        SetSelectorPos(SelectorPos);
        try
          SelectorF.JobSelection := True;
          SelectorF.btnBreak.Visible := ORASystemDM.BreakBtnVisible;
          SelectorF.btnLunch.Visible := ORASystemDM.LunchBtnVisible;
          // PIM-181 Do no allow to choose for break/lunch when current job is
          //         OCCUPIED.
          if (HybridDM.HybridMode = 1) and (ANextIDCard.MeasureProductivity) then
          begin
            if (ALastIDCard.JobCode = '') or (ALastIDCard.JobCode = OCCUPJOB) then
            begin
              if ORASystemDM.BreakBtnVisible then
                SelectorF.btnBreak.Visible := False;
              if ORASystemDM.LunchBtnVisible then
                SelectorF.btnLunch.Visible := False;
            end;
          end;
          SelectorF.OldJobCode := ALastIDCard.JobCode;
          SelectRes := SelectorF.EnableSelection(SelectorPos,SelectorColumns,
            TimeRecordingDM.dsrcJobcode);
        except
          on E:EOracleError do
          begin
            FOnScanError(E.Message);
            Result := False;
            Exit;
          end;
          on E: Exception do
          begin
            FOnScanError(E.Message);
            Result := False;
            Exit;
          end;
        end;
        case SelectRes of
          ModalOk:
            begin
              ANextIDCard.JobCode := FieldByName('JOB_CODE').AsString;
              ANextIDCard.Job     := FieldByName('DESCRIPTION').AsString;
            end;
          ModalLunch,
          ModalEndOfDay: SetEndOfDay(ANextIDCard);
          ModalBreak: // PIM-112 Switch to Break Job. Must autom. be created!
            begin
              Result := True;
              // Create break job, if it did not exist yet.
              TimeRecordingDM.CheckAddJobForWorkspot(ANextIDCard.PlantCode,
                ANextIDCard.WorkSpotCode, BREAKJOB, SPimsBreakJobDescription);
              // Set Job
              ANextIDCard.JobCode := BREAKJOB;
              ANextIDCard.Job     := SPimsBreakJobDescription;
            end;
          ModalCancel:
            begin
              OnScanAbort;
              Result := False;
              Exit;
            end;
        end; {case}
      end;
      // PIM-181 Prevent it goes to the else, that can overwrite the break-job
      // GLOB3-262
{      if ((HybridDM.HybridMode = 1) and ANextIDcard.MeasureProductivity) then
      begin
        ;
      end
      else }
      begin
        // PIM-181 Bugfix: Also look if OK-button was clicked! Or it goes
        //         wrong when Break-button was clicked.
        if (JobRecordCount = 1) and (SelectRes = ModalOK) then
        begin
          ANextIDCard.JobCode := FieldByName('JOB_CODE').AsString;
          ANextIDCard.Job     := FieldByName('DESCRIPTION').AsString;
        end
        else // PIM-330 Bugfix - Job is stored when nothing was selected but there was a job defined.
        begin
          if (JobRecordCount = 1) and (SelectRes <> ModalBreak) then
          begin
            ANextIDCard.JobCode := FieldByName('JOB_CODE').AsString;
            ANextIDCard.Job     := FieldByName('DESCRIPTION').AsString;
          end;
        end;
      end;
      // PIM-330 Be sure there is always something filled for JobCode!
      if (JobRecordCount <= 0) or (ANextIDCard.JobCode = '') then
      begin
        ANextIDCard.JobCode := DUMMYSTR;
        ANextIDCard.Job     := DUMMYSTR
      end;
      // show description
      //CAR 24-03-2003
      EditPreJob.Text      := ANextIDCard.Job;
      EditPreDateFrom.Text := FormatDateTime(LONGDATE, ANextIDCard.DateIn);
      EnabledWk_JobField(True);
      Active := False;
    end; {(ANextIDCard.WorkSpotCode <> NullStr)}
  end;{with TimeRecordingDM.odsJobcode}
end; // DetermineEmployeeSelectsNextWorkspotAndJob

procedure TTimeRecordingF.DeterminePlannedWorkspot(
  var ANextIDCard: TScannedIDCard;
  var APlannedWorkspot, APlannedPlant, APlannedDepartment: String;
  var APlannedShiftNumber: Integer);
var
  TBEnd, StartDate, EndDate: TDateTime;
begin
  StartDate := ANextIDCard.DateIn;
  GetStartEndDay(StartDate, EndDate); // startdate := trunc(startdate)
                                      // enddate := startdate = 1(day) - 1 msec
  // employeplanning_date is date only, so startdate is Ok
  // 550387
  StartDate := StartDate - 1;
  EndDate   := EndDate + 1;
  with TimeRecordingDM.oqDeterminePlannedWorkspot do
  begin
    ClearVariables;
    SetVariable('STARTDATE',       StartDate);
    SetVariable('ENDDATE',         EndDate);
    SetVariable('EMPLOYEE_NUMBER', ANextIDCard.EmployeeCode);
    Execute;
    APlannedWorkSpot   := NullStr;
    APlannedPlant      := NullStr;
    APlannedDepartment := NullStr;
    // MR:05-12-2002
    APlannedShiftNumber := -1;
    TBEnd := 0;
    if not Eof then
    begin
      while not Eof do
      begin
        DetermineScheduledTimeblocks(TimeRecordingDM.oqDeterminePlannedWorkspot,
          FieldAsInteger('SHIFT_NUMBER'),
          APlannedWorkspot, APlannedPlant, APlannedDepartment,
          APlannedShiftNumber, ANextIDCard,
          FieldAsDate('EMPLOYEEPLANNING_DATE'),
          TBEnd);
        Next;
      end;
    end;
  end; {with TimeRecordingDM.oqDeterminePlannedWorkspot}
end; // DeterminePlannedWorkspot

procedure TTimeRecordingF.DetermineScheduledTimeblocks(
  AQuery: TOracleQuery;
  const AShiftNumber: Integer;
  var APlannedWorkspot, APlannedPlant, APlannedDepartment: String;
  var APlannedShiftNumber: Integer;
  var ANextIDCard: TScannedIDCard;
  const AEmployeePlanningDate: TDateTime;
  var TBEnd: TDateTime);
var
  Where: String;
  Index: Integer;
begin
  Where := '';
  for Index := 1 to ORASystemDM.MaxTimeblocks do
  begin
    if AQuery.FieldAsString('SCHEDULED_TIMEBLOCK_' + IntToStr(Index))[1]
      in ['A','B','C'] then
    begin
      if Where = '' then
        Where := ' AND (TIMEBLOCK_NUMBER = ' + IntToStr(Index)
      else
        Where := Where + ' OR TIMEBLOCK_NUMBER = ' + IntToStr(Index);
    end;
  end;
  if Where <> '' then
    Where := Where + ')';
  // MR:09-09-2004 Optimising
  DetermineTBTimes(
    AQuery.FieldAsString('PLANT_CODE'),
    AQuery.FieldAsString('DEPARTMENT_CODE'),
    ANextIDCard.EmployeeCode,
    AShiftNumber,
    AEmployeePlanningDate,
    Where);
  // Test for correct planning
  for Index:=0 to Length(DatesEnd)-1 do   // find min date
  begin
{$IFDEF DEBUG1}
  WDebugLog('- DatesStart[' + IntToStr(Index) + ']=' + DateTimeToStr(DatesStart[Index]) +
    ' DatesEnd[' + IntToStr(Index) + ']=' + DateTimeToStr(DatesEnd[Index]) +
    ' DateIn=' + DateTimeToStr(ANextIDCard.DateIn) +
    ' TBEnd=' + DateTimeToStr(TBEnd)
    );
{$ENDIF}
    if (DatesEnd[Index] > ANextIDCard.DateIn) and
      ((DatesEnd[Index] <= TBEnd) or (TBEnd = 0)) then
    begin
{$IFDEF DEBUG1}
  WDebugLog('- DatesStart[' + IntToStr(Index) + ']=' + DateTimeToStr(DatesStart[Index]) +
    ' DateIn=' + DateTimeToStr(ANextIDCard.DateIn) +
    ' DateIn + 1/24=' + DateTimeToStr(ANextIDCard.DateIn + (5 / 24))
    );
{$ENDIF}
      // RV047.1. From 1/24 to 5/24. Look 5 hours ahead for planning.
      if DatesStart[Index] <= (ANextIDCard.DateIn + (5 / 24)) then
      begin
{$IFDEF DEBUG1}
  WDebugLog('- Found. PlannedWorkspot=' + AQuery.FieldAsString('WORKSPOT_CODE')
    );
{$ENDIF}
        APlannedWorkspot    := AQuery.FieldAsString('WORKSPOT_CODE');
        APlannedPlant       := AQuery.FieldAsString('PLANT_CODE');
        APlannedDepartment  := AQuery.FieldAsString('DEPARTMENT_CODE');
        // MR:05-12-2002
        APlannedShiftNumber := AQuery.FieldAsInteger('SHIFT_NUMBER');
      end;
      TBEnd := DatesEnd[Index];
    end;
  end;
end; // DetermineScheduledTimeblocks

procedure TTimeRecordingF.DetermineSerialDeviceSettings;
begin
  DeviceSettingsFound := False;
  with TimeRecordingDM do
  begin
    // Set scanner by database-settings
    oqComportConnection.ClearVariables;
    oqComportConnection.SetVariable('COMPUTER_NAME',
      ORASystemDM.CurrentComputerName);
    oqComportConnection.Execute;
    // Only One Serial-device setting is allowed!
    if not oqComportConnection.Eof then
    begin
//      oqComportConnection.First;
      // GLOB3-324 Store this in property for later use
      TimeRecordingDM.SerialDeviceCode :=
        oqComportConnection.FieldAsString('SERIALDEVICE_CODE');
      TimeRecordingDM.ReaderRosslare := False;
      TimerecordingDM.pcProxPlus := False;
      TimeRecordingDM.Raw := False;
      TimerecordingDM.H5427 := False;
      if oqComportConnection.FieldAsString('SERIALDEVICE_CODE') = 'ROSSLA' then
      begin
        TimeRecordingDM.ReaderRosslare := True;
        TimerecordingDM.pcProxPlus := True;
      end;
      // GLOB3-112
      if oqComportConnection.FieldAsString('SERIALDEVICE_CODE') = 'RAW' then
        TimeRecordingDM.Raw := True;
      // GLOB3-139
      if oqComportConnection.FieldAsString('SERIALDEVICE_CODE') = 'H5427' then
        TimerecordingDM.H5427 := True;
      // GLOB3-324
      if oqComportConnection.FieldAsString('SERIALDEVICE_CODE') = 'PCPROX' then
        TimerecordingDM.pcProxPlus := True;
      try
        ScannerSettings(
          oqComportConnection.FieldAsInteger('COMPORT'),
          oqComportConnection.FieldAsInteger('DATABITS'),
          oqComportConnection.FieldAsString('PARITY')[1],
          oqComportConnection.FieldAsInteger('BAUDRATE'),
          oqComportConnection.FieldAsInteger('STOPBITS')
          );
        DeviceSettingsFound := True;
        try
          if not oqComportConnection.FieldIsNull('VERIFYSTRING') then
            VerifyString :=
              oqComportConnection.FieldAsString('VERIFYSTRING');
          if VerifyString = NOTRIM_COMMAND then // TD-24520.60
          begin
            NoTrim := True;
            VerifyString := '';
          end;
          if not oqComportConnection.FieldIsNull('VERIFYPOSITION') then
            VerifyPosition :=
              oqComportConnection.FieldAsInteger('VERIFYPOSITION');
        except
          VerifyString   := '';
          VerifyPosition := 0;
        end;
        try
          if not oqComportConnection.FieldIsNull('VALUEPOSITION') then
            ValuePosition :=
              oqComportConnection.FieldAsInteger('VALUEPOSITION');
        except
          ValuePosition := 0;
        end;
        // 20015371
        try
          if not oqComportConnection.FieldIsNull('INITSTRING') then
            InitString :=
              oqComportConnection.FieldAsString('INITSTRING');
          if InitString <> '' then
            IDConvertInit(InitString);
        except
          InitString := '';
        end;
      except
        ScannerSettings(1, 8, 'N', 9600, 1);
        VerifyString   := '';
        VerifyPosition := 0;
        ValuePosition  := 0;
        InitString := '';
      end;
    end
    else
    begin
      ScannerSettings(1, 8, 'N', 9600, 1);
      VerifyString   := '';
      VerifyPosition := 0;
      ValuePosition  := 0;
      InitString := '';
    end;
// not for oq's    oqComportConnection.Close;
  end;
end; // DetermineSerialDeviceSettings

procedure TTimeRecordingF.DetermineShift(
  var ANextIDCard: TScannedIDCard;
  const APlannedShiftNumber: Integer);
var
  ShiftFound: Boolean;
  ShiftScheduleFound: Boolean; // PIM-368
  StartDateTime, EndDateTime: TDateTime;
  procedure DialogShiftCheckFormPosition;
  var
    ATop, ALeft, AWidth, AHeight: Integer;
  begin
    if Width <= MaxWidth then
      ATop := ClientOrigin.Y + GroupBoxWorkspot.Top
    else
      ATop := ClientOrigin.Y + GroupBoxError.Top;
    ALeft   := ClientOrigin.X + 2;
    if Width <= MaxWidth then
      AHeight := GroupBoxWorkspot.Height + GroupboxError.Height
    else
      AHeight := GroupboxError.Height;
    AWidth  := GroupboxError.Width;
    DialogShiftCheckForm.SetFormPos(ATop, ALeft, AWidth, AHeight);
  end; // DialogShiftCheckFormPosition
begin
  // MR:05-12-2002 Set Planned shiftnumber
  if APlannedShiftNumber <> -1 then
    ANextIDCard.ShiftNumber := APlannedShiftNumber
  else
  begin
    with TimeRecordingDM do
    begin
      ShiftFound := False;
      // MR:03-01-2003 There is no planned-shiftnumber,
      // so get it from table SHIFTSCHEDULE
      // MR:08-03-2005 Order 550387
      // Determine Department and also Shift with 'GetShiftDay'.
      // 20014327 Do this using a seperate procedure, because it is skipped
      //          here when there is a plant-shift found!
{      with oqWorkspot do
      begin
        ClearVariables;
        SetVariable('PLANT_CODE',    ANextIDCard.PlantCode);
        SetVariable('WORKSPOT_CODE', ANextIDCard.WorkspotCode);
        Execute;
        if not Eof then
          ANextIDCard.DepartmentCode := FieldAsString('DEPARTMENT_CODE')
      end;
}
      AShiftDay.GetShiftDay(ANextIDCard, StartDateTime, EndDateTime);
      // TD-22069 !!!CANCELLED!!!
      //          Always assign the shift based on date of DateIn of scan.
      //          Not on 'startdatetime', or it can result in a shift that
      //          is based on the day before which can be a 'day-shift'
      //          instead of a shift that begins the day-before.
      ShiftScheduleFound := False; // PIM-368
      with oqDetermineShiftFromShiftSchedule do
      begin
        ClearVariables;
        SetVariable('EMPLOYEE_NUMBER',          ANextIDCard.EmployeeCode);
        SetVariable('SHIFT_SCHEDULE_DATE_FROM', Trunc(StartDateTime));
        SetVariable('SHIFT_SCHEDULE_DATE_TO',   Trunc(StartDateTime));
        // TD-22069 Always base this on start-date of scan? CANCELLED
//        SetVariable('SHIFT_SCHEDULE_DATE_FROM', Trunc(ANextIDCard.DateIn));
//        SetVariable('SHIFT_SCHEDULE_DATE_TO',   Trunc(ANextIDCard.DateIn));
        Execute;
        if not Eof then
        begin
          if not FieldIsNull('SHIFT_NUMBER') and
            (FieldAsInteger('SHIFT_NUMBER') <> -1) then
          begin
            ANextIDCard.ShiftNumber := FieldAsInteger('SHIFT_NUMBER');
            ShiftScheduleFound := True; // PIM-368
            ShiftFound := True;
          end;
        end;
      end; {with oqDetermineShiftFromShiftSchedule}
      // MR:24-02-2005 Order 550386
      // Search default shift from the employee
      if not ShiftFound then
      begin
        with oqEmployeeShift do
        begin
          ClearVariables;
          SetVariable('EMPLOYEE_NUMBER', ANextIDCard.EmployeeCode);
          Execute;
          if not Eof then
          begin
            if not FieldIsNull('SHIFT_NUMBER') then
            begin
              ANextIDCard.ShiftNumber := FieldAsInteger('SHIFT_NUMBER');
              ShiftFound := True;
            end;
          end;
        end; {with oqEmployeeShift}
      end; {not ShiftFound}

      if ShiftScheduleFound then // PIM-368
      begin
        // PIM-242
        ANextIDCard.ShiftNumber :=
          DialogShiftCheckDM.ShiftCheck(
            ANextIDCard.EmployeeCode,
            ANextIDCard.ShiftNumber, ANextIDCard.PlantCode,
            ANextIDCard.WorkSpotCode, 0, 1);
        // Incorrect shift? Ask for correct shift here.
        if ANextIDCard.ShiftNumber = -1 then
        begin
          // Prevent it shows this dialog without any shifts found
          if not DialogShiftCheckDM.cdsShift.Eof then
          begin
            DialogShiftCheckForm.DateIn := ANextIDCard.DateIn;
            DialogShiftCheckForm.ShiftNumber := ANextIDCard.ShiftNumber;
            DialogShiftCheckFormPosition;
            if DialogShiftCheckForm.ShowModal = mrOK then
            begin
              ANextIDCard.ShiftNumber := DialogShiftCheckForm.ShiftNumber;
              ShiftFound := True;
            end
            else
              ShiftFound := False;
          end;
        end;
      end; // if // PIM-368

      if not ShiftFound then
      begin
        // Select SHIFT
        with oqDetermineShiftByPlant do
        begin
          ClearVariables;
          SetVariable('PLANT_CODE', ANextIDCard.PlantCode);
          Execute;
          if not Eof then
            ANextIDCard.ShiftNumber := FieldAsInteger('SHIFT_NUMBER')
          else
            ANextIDCard.ShiftNumber := 1;
        end; {with oqDetermineShiftByPlant}
      end; {not ShiftFound}
    end; {with TimeRecordingDM}
  end; {else APlannedShiftNumber <> -1 }
end; // DetermineShift

procedure TTimeRecordingF.DetermineStandardPlanningWorkspot(
  var ANextIDCard: TScannedIDCard;
  var APlannedWorkspot, APlannedPlant, APlannedDepartment: String;
  var APlannedShiftNumber: Integer);
var
  TBEnd: TDateTime;
  ShiftNumber: array[1..3] of Integer;
  I, DayDifference: Integer;
  EmployeeShiftNumber, ShiftShiftNumber: Integer;
begin
  // CAR 550289 - take the workspot fom standard planning...
  // if the employee is not planned then ...
  if (APlannedPlant = NullStr) then
  begin
    for I := 1 to 3 do
      ShiftNumber[I] := -1;
    TBEnd := 0;
    APlannedShiftNumber := -1;
    // first look for shiftnumber
    with TimeRecordingDM do
    begin
//      AddLog('  DeterminePlanningWorkspot');
      // get shift for yesterday, today and tomorrow
      with oqDetermineShiftFromShiftSchedule do
      begin
        ClearVariables;
        SetVariable('EMPLOYEE_NUMBER',          ANextIDCard.EmployeeCode);
        SetVariable('SHIFT_SCHEDULE_DATE_FROM', Trunc(ANextIDCard.DateIn - 1));
        SetVariable('SHIFT_SCHEDULE_DATE_TO',   Trunc(ANextIDCard.DateIn + 1));
        Execute;
        if not Eof then
        begin
          while not Eof do
          begin
            I := (Trunc(FieldAsDate('SHIFT_SCHEDULE_DATE')) -
              Trunc(ANextIDCard.DateIn)) + 2;
            ShiftNumber[I] := FieldAsInteger('SHIFT_NUMBER');
            Next;
          end; {while not Eof}
        end; {if Eof}
      end; {with oqDetermineShiftFromShiftSchedule}
      // if any sgift = -1 then get employee.shift_numer and shift.shift_number
      if (ShiftNumber[1] = -1) or (ShiftNumber[2] = -1) or (ShiftNumber[3] = -1) then
      begin
        EmployeeShiftNumber := -1;
        ShiftShiftNumber    := -1;
        with oqEmployeeShift do
        begin
          ClearVariables;
          SetVariable('EMPLOYEE_NUMBER', ANextIDCard.EmployeeCode);
          Execute;
          if not Eof then
          begin
            EmployeeShiftNumber := FieldAsInteger('SHIFT_NUMBER');
          end;
        end; {with oqEmployeeShift}
        with oqDetermineShiftByEmployee do
        begin
          ClearVariables;
          SetVariable('EMPLOYEE_NUMBER', ANextIDCard.EmployeeCode);
          Execute;
          if not Eof then
            ShiftShiftNumber := FieldAsInteger('SHIFT_NUMBER');
        end; {with oqDetermineShiftByEmployee}

        for I := 1 to 3 do
        begin
          if ShiftNumber[I] = -1 then
            ShiftNumber[I] := EmployeeShiftNumber;
          if ShiftNumber[I] = -1 then
            ShiftNumber[I] := ShiftShiftNumber;
        end; {for}
      end; {(ShiftNumber[1] = -1) or (ShiftNumber[2] = -1) or (ShiftNumber[3] = -1)}

      // now get workspot etc. from standardplanning
      with oqDetermineStandardPlanningWorkspot do
      begin
        ClearVariables;
        SetVariable('EMPLOYEE_NUMBER', ANextIDCard.EmployeeCode);
        SetVariable('SHIFT_NUMBER_1',  ShiftNumber[1]);
        SetVariable('DAY_1',           PimsDayOfWeek(ANextIDCard.DateIn - 1));
        SetVariable('SHIFT_NUMBER_2',  ShiftNumber[2]);
        SetVariable('DAY_2',           PimsDayOfWeek(ANextIDCard.DateIn));
        SetVariable('SHIFT_NUMBER_3',  ShiftNumber[3]);
        SetVariable('DAY_3',           PimsDayOfWeek(ANextIDCard.DateIn + 1));
        Execute;
        while not Eof do
        begin
          DayDifference := FieldAsInteger('DAY_OF_WEEK') -
            PimsDayOfWeek(ANextIDCard.DateIn);
          if DayDifference = 6 then
            DayDifference := -1
          else
            if DayDifference = -6 then
              DayDifference := 1;
          DetermineScheduledTimeblocks(oqDetermineStandardPlanningWorkspot,
            ShiftNumber[DayDifference + 2],
            APlannedWorkspot, APlannedPlant, APlannedDepartment,
            APlannedShiftNumber, ANextIDCard,
            ANextIDCard.DateIn + DayDifference,
            TBEnd);
          Next;
        end;
      end; {with oqDetermineStandardPlanningWorkspot}
      // END -550289
    end; {with TimeRecordingDM}
  end; {(APlannedPlant = NullStr)}
end; // DetermineStandardPlanningWorkspot

procedure TTimeRecordingF.DetermineStatistics(
  ANextIDCard, ALastIDCard: TScannedIDCard);
var
  Planned: Integer;
begin
//  AddLog('  DetermineStatistics');
  //    Holiday
  with TimeRecordingDM.oqAbsenceStatistics do
  begin
    ClearVariables;
    SetVariable('EMPLOYEE_NUMBER', ALastIDCard.EmployeeCode);
    SetVariable('YEAR',            YearOf(ANextIDCard.DateIn));
    Execute;
    if not Eof then
    begin
    Planned := 0;
    LabelHolidayVal.Caption := IntMin2StringTime(
      Trunc(FieldAsFloat('LAST_YEAR_HOLIDAY_MINUTE')) +
      Trunc(FieldAsFloat('NORM_HOLIDAY_MINUTE')) -
      Trunc(FieldAsFloat('USED_HOLIDAY_MINUTE')) - Planned, True);
    LabelTimeForTimeVal.Caption := IntMin2StringTime(
      Trunc(FieldAsFloat('LAST_YEAR_TFT_MINUTE')) +
      Trunc(FieldAsFloat('EARNED_TFT_MINUTE')) -
      Trunc(FieldAsFloat('USED_TFT_MINUTE')) - Planned, True);
    LabelTimeReductionVal.Caption := IntMin2StringTime(
      Trunc(FieldAsFloat('LAST_YEAR_WTR_MINUTE')) +
      Trunc(FieldAsFloat('EARNED_WTR_MINUTE')) -
      Trunc(FieldAsFloat('USED_WTR_MINUTE')) - Planned, True);
    end; {Eof}
  end; {with}
  // no longer execute 3 queries but get all field
end; // DetermineStatistics

// MR:09-09-2004 Optimise
function TTimeRecordingF.DetermineTBTimes(
  const APlantCode, ADepartmentCode: String;
  const AEmployeeNumber, AShiftNumber: Integer;
  const AEmployeePlanningDate: TDateTime; Where: String): Boolean;
var
  ADataSet: TDataSet;
  Normal: Integer;
  DayStr: String;
  ShiftStartTime: TDateTime;
begin
  SetLength(DatesStart, 0);
  SetLength(DatesEnd, 0);
  DayStr := IntToStr(PimsDayOfWeek(AEmployeePlanningDate));
  if DetermineTimeBlocks(
    APlantCode, ADepartmentCode, AEmployeeNumber, AShiftNumber,
    Where, ADataSet, DayStr, ShiftStartTime) then
  begin
    Result := True;
    ADataSet.First;
    while not ADataSet.Eof do
    begin
      SetLength(DatesStart, Length(DatesStart) + 1);
      SetLength(DatesEnd, Length(DatesEnd) + 1);
      Normal := Length(DatesStart) - 1;
      DatesStart[Normal]:=
        ADataSet.FieldByName('STARTTIME' + DayStr).AsDateTime;
      DatesEnd[Normal]:=
        ADataSet.FieldByName('ENDTIME' + DayStr).AsDateTime;
      ReplaceDate(DatesStart[Normal], AEmployeePlanningDate);
      ReplaceDate(DatesEnd[Normal], AEmployeePlanningDate);
      ReplaceDate(ShiftStartTime, AEmployeePlanningDate);
      if DatesStart[Normal] < ShiftStartTime then
        DatesStart[Normal] := DatesStart[Normal] + 1; // next day
      if DatesEnd[Normal] < ShiftStartTime then
        DatesEnd[Normal] := DatesEnd[Normal] + 1; // next day
      ADataSet.Next;
    end;
  end
  else
  begin
    // No TB-Times could be found.
    Result := False;
  end;
end; // DetermineTBTimes

// MR:09-09-2004 Optimise
// Get TimeBlock-data from clientdatasets that are available
// in CalculateTotalHoursDMT, instead of retrieving the data
// here again.
// MR:10-03-2005 Order 550387
// Function changed, so it returns starttime of first valid timeblock.
function TTimeRecordingF.DetermineTimeBlocks(
  const APlantCode, ADepartmentCode: String;
  const AEmployeeNumber, AShiftNumber: Integer;
  const Where: String; var ADataSet: TDataSet; const ADayStr: String;
  var AShiftStartTime: TDateTime): Boolean;
begin
  Result := False;
  AShiftStartTime := 0;

  // Timeblocks Per Employee
  ADataSet := CalculateTotalHoursDM.ClientDataSetTBEmpl;
  ADataSet.Filtered := False;
  ADataSet.Filter :=
    'PLANT_CODE = ' + '''' + DoubleQuote(APlantCode) + '''' +
    ' AND ' +
    'EMPLOYEE_NUMBER = ' + IntToStr(AEmployeeNumber) + ' AND ' +
    'SHIFT_NUMBER = ' + IntToStr(AShiftNumber);
  ADataSet.Filtered := True;
  if not ADataSet.IsEmpty then
  begin
    ADataSet.First;
    while (not ADataSet.Eof) and
      (ADataSet.FieldByName('STARTTIME' + ADayStr).AsDateTime =
        ADataSet.FieldByName('ENDTIME' + ADayStr).AsDateTime) do
      ADataSet.Next;
    if (not ADataSet.Eof) then
    begin
      Result := True;
      AShiftStartTime :=
        ADataSet.FieldByName('STARTTIME' + ADayStr).AsDateTime;
      ADataSet.Filtered := False;
      ADataSet.Filter :=
        'PLANT_CODE = ' + '''' + DoubleQuote(APlantCode) + '''' +
        ' AND ' +
        'EMPLOYEE_NUMBER = ' + IntToStr(AEmployeeNumber) + ' AND ' +
        'SHIFT_NUMBER = ' + IntToStr(AShiftNumber) +
        Where;
      ADataSet.Filtered := True;
    end;
  end;

  // Timeblocks Per Department
  if not Result then
  begin
    // Timeblocks per Department
    ADataSet := CalculateTotalHoursDM.ClientDataSetTBDept;
    ADataSet.Filtered := False;
    ADataSet.Filter :=
      'PLANT_CODE = ' + '''' + DoubleQuote(APlantCode) +
      '''' + ' AND ' +
      'DEPARTMENT_CODE = ''' + DoubleQuote(ADepartmentCode) +
      '''' + ' AND ' +
      'SHIFT_NUMBER = ' + IntToStr(AShiftNumber);
    ADataSet.Filtered := True;
    if not ADataSet.IsEmpty then
    begin
      ADataSet.First;
      while (not ADataSet.Eof) and
        (ADataSet.FieldByName('STARTTIME' + ADayStr).AsDateTime =
          ADataSet.FieldByName('ENDTIME' + ADayStr).AsDateTime) do
        ADataSet.Next;
      if (not ADataSet.Eof) then
      begin
        Result := True;
        AShiftStartTime :=
          ADataSet.FieldByName('STARTTIME' + ADayStr).AsDateTime;
        ADataSet.Filtered := False;
        ADataSet.Filter :=
          'PLANT_CODE = ' + '''' + DoubleQuote(APlantCode) +
          '''' + ' AND ' +
          'DEPARTMENT_CODE = ''' + DoubleQuote(ADepartmentCode) +
          '''' + ' AND ' +
          'SHIFT_NUMBER = ' + IntToStr(AShiftNumber) +
          Where;
        ADataSet.Filtered := True;
      end;
    end;

    // Timeblocks Per Shift
    if not Result then
    begin
      // Timeblocks Per Shift
      ADataSet := CalculateTotalHoursDM.ClientDataSetTBShift;
      ADataSet.Filtered := False;
      ADataSet.Filter :=
        'PLANT_CODE = ' + '''' + DoubleQuote(APlantCode) +
        '''' + ' AND ' +
        'SHIFT_NUMBER = ' + IntToStr(AShiftNumber);
      ADataSet.Filtered := True;
      if not ADataSet.IsEmpty then
      begin
        ADataSet.First;
        while (not ADataSet.Eof) and
          (ADataSet.FieldByName('STARTTIME' + ADayStr).AsDateTime =
            ADataSet.FieldByName('ENDTIME' + ADayStr).AsDateTime) do
          ADataSet.Next;
        if (not ADataSet.Eof) then
        begin
          Result := True;
          AShiftStartTime :=
            ADataSet.FieldByName('STARTTIME' + ADayStr).AsDateTime;
          ADataSet.Filtered := False;
          ADataSet.Filter :=
            'PLANT_CODE = ' + '''' + DoubleQuote(APlantCode) +
            '''' + ' AND ' +
            'SHIFT_NUMBER = ' + IntToStr(AShiftNumber) +
            Where;
          ADataSet.Filtered := True;
        end;
      end;
    end;
  end;
end; // DetermineTimeBlocks

procedure TTimeRecordingF.DetermineWorkspotByDepartment(
  const APlannedPlant, APlannedDepartment: String;
  var APlannedWorkspot: String);
begin
  // 16-1-2004 Car:- bug replace NullStr with DummyStr  - for checks on workspot
  if (APlannedWorkspot = DUMMYSTR) and
   (APlannedPlant <> NullStr) and
   (APlannedDepartment <> NullStr) then //Not Planned on WorkSpot but Department
  begin
//    AddLog('  DetermineWorkspotByDepartment');
    with TimeRecordingDM.oqDetermineWorkspotByDepartment do
    begin
      ClearVariables;
      SetVariable('PLANT_CODE',      APlannedPlant);
      SetVariable('DEPARTMENT_CODE', APlannedDepartment);
      Execute;
      if not Eof then
        APlannedWorkspot := FieldAsString('WORKSPOT_CODE');
    end; {with oqDetermineWorkspotByDepartment}
    // takes the first workspot per department
  end;
end; // DetermineWorkspotByDepartment

// MR:15-11-2002
// MR:09-09-2004
// Determine some settings for this workstation
procedure TTimeRecordingF.DetermineWorkstation;
begin
  IsScanWorkstation := False;
  TimeRecording_YN  := 'N';
  Show_Hour_YN      := 'N';
  Hide_ID_YN        := 'N'; // 20014035
  with TimeRecordingDM do
  begin
// not for oq's    Close;
    oqWorkstation.ClearVariables;
    oqWorkstation.SetVariable('COMPUTER_NAME', ORASystemDM.CurrentComputerName);
    oqWorkstation.Execute;
    if not oqWorkstation.Eof then
    begin
      IsScanWorkstation := True;
      TimeRecording_YN  := oqWorkstation.FieldAsString('TIMERECORDING_YN');
      Show_Hour_YN      := oqWorkstation.FieldAsString('SHOW_HOUR_YN');
      Hide_ID_YN        := oqWorkstation.FieldAsString('HIDE_ID_YN');
    end;
//not for oq's    Close;
  end; {with TimeRecordingDM}
  // PIM-256
  if Show_Hour_YN = 'N' then
  begin
    GroupBoxWorked.Visible := False;
    GroupBoxTime.Visible := False;
  end;
end; // DetermineWorkstation

procedure TTimeRecordingF.EnabledWk_JobField(const AEnabledFields: Boolean);
begin
  if AEnabledFields then
  begin
    EditPreDateFrom.Brush.Color := clWhite;
    EditPrePlant.Brush.Color    := clWhite;
    EditPreJob.Brush.Color      := clWhite;
    EditPreWorkspot.Brush.Color := clWhite;
  end
  else
  begin
    EditPreDateFrom.Brush.Color := clDisabled;
    EditPrePlant.Brush.Color    := clDisabled;
    EditPreJob.Brush.Color      := clDisabled;
    EditPreWorkspot.Brush.Color := clDisabled;
  end;
  EditPreDateFrom.Enabled := AEnabledFields;
  EditPreJob.Enabled      := AEnabledFields;
  EditPrePlant.Enabled    := AEnabledFields;
  EditPreWorkspot.Enabled := AEnabledFields;
end; // EnabledWk_JobField

procedure TTimeRecordingF.ErrorScanning(const AMsg: String);
begin
  {Error Messages}
  LabelErrorMsg.Font.Color := clRed;
  LabelErrorMsg.Caption    := AMsg;
  LabelErrorMsg.Visible    := True;
  ScanStatus               := SError;
  EditIDCard.SetFocus;
  BitBtnAccept.Enabled     := True;
  if BitBtnCancel.Visible then
    BitBtnCancel.Enabled := True;
  AddErrorLog(AMsg);
end; // ErrorScanning

// Insert a new Timeregscan-record, to create a new 'open' scan.
procedure TTimeRecordingF.InsertTRSNewScan(var AIDCard: TScannedIDCard;
  const AOpen: Boolean; const AIDCardOut: String);
var
  StartDate, EndDate: TDateTime;
begin
  // RV064.1.
  try
    // 20013489
    // 20015346
{
When inserting a new (open) scan-record:
- DateIn := SYSDATE
- CreationDate := SYSDATE
- MutationDate := SYSDATE
}
    // 20015346
    // PIM-12 Bugfix Do not assign the timestamp here! It should be done
    //        already in a earlier stadium.
{
    if ORASystemDM.TimerecordingInSecs then
    begin
      AIDcard.DateIn := Now;
      LastNow := AIDCard.DateIn;
      if not AOpen then
      begin
        AIDCard.DateOut := LastNow;
        LastNow := AIDCard.DateOut;
      end;
    end;
}
    AProdMinClass.GetShiftDay(AIDCard, StartDate, EndDate);
    AIDCard.ShiftDate := Trunc(StartDate);
    with TimeRecordingDM.oqInsertTRSNewScan do
    begin
      ClearVariables;
      SetVariable('DATETIME_IN',     AIDCard.DateIn);
      SetVariable('IDCARD_IN',       AIDCard.IDCard);
      if AOpen then
      begin
        SetVariable('DATETIME_OUT',  Null);
        SetVariable('PROCESSED_YN',  UNCHECKEDVALUE);
        SetVariable('IDCARD_OUT',    '');
      end
      else
      begin
        SetVariable('DATETIME_OUT',  AIDCard.DateOut);
        SetVariable('PROCESSED_YN',  CHECKEDVALUE);
        SetVariable('IDCARD_OUT',    AIDCardOut);
      end;
      SetVariable('EMPLOYEE_NUMBER', AIDCard.EmployeeCode);
      SetVariable('PLANT_CODE',      AIDCard.PlantCode);
      SetVariable('WORKSPOT_CODE',   AIDCard.WorkSpotCode);
      SetVariable('JOB_CODE',        AIDCard.JobCode);
      SetVariable('SHIFT_NUMBER',    AIDCard.ShiftNumber);
      SetVariable('MUTATOR',         ORASystemDM.CurrentComputerName);
      // 20013489
      SetVariable('SHIFT_DATE',      AIDCard.ShiftDate);
      SetVariable('CREATIONDATE',    CurrentNow);
      SetVariable('MUTATIONDATE',    CurrentNow);
      Execute;
      if ORASystemDM.OracleSession.InTransaction then // PIM-346
        ORASystemDM.OracleSession.Commit;
      AddLog('InsertNewScan-Commit');
    end; {with}
  except
    on E:EOracleError do
    begin
      if ORASystemDM.OracleSession.InTransaction then
      begin
        AddLog('Rollback');
        ORASystemDM.OracleSession.Rollback;
      end;
      FOnScanError(E.Message);
    end;
    on E: Exception do
    begin
      if ORASystemDM.OracleSession.InTransaction then
      begin
        AddLog('Rollback');
        ORASystemDM.OracleSession.Rollback;
      end;
      FOnScanError(E.Message);
    end;
  end;
end; // InsertTRSNewScan
(*
function TTimeRecordingF.NewLogPath: String;
var
  Year, Month, Day: Word;
  DateString: String;
begin
  try
    DecodeDate(Date, Year, Month, Day);
    DateString :=
      MyZeroFormat(IntToStr(Year), 4) +
      MyZeroFormat(IntToStr(Month), 2) +
      MyZeroFormat(IntToStr(Day), 2);
  except
    DateString := '20020601';
  end;
  Result := PathCheck(LogRootPath) +
    ORASystemDM.CurrentComputerName + LOG_FILENAME + DateString + '.TXT';
end;
*)

// MR:15-11-2002
// Code has been read by Serial-port
procedure TTimeRecordingF.ProcessCodeRead(const ACodeRead: String);
var
  BracketPosition: Integer;
begin
  FlushScanner;
  EditIDCard.Text := ACodeRead;
  AddLog('Start ProcessCodeRead');
  AddLog('CodeRead=[' + ACodeRead + ']');
  // Filter on VerifyString
  if (VerifyString <> '') and (ACodeRead <> '') then
  begin
    try
      if (Length(ACodeRead) > VerifyPosition) then
      begin
        if (Copy(ACodeRead, VerifyPosition, Length(VerifyString)) =
          VerifyString) then
        begin
          // Now get ID-card-code
          BracketPosition := Pos(')', ACodeRead);
          EditIDCard.Text := Copy(ACodeRead, ValuePosition,
            BracketPosition - ValuePosition);
          AddLog('Match');
        end
        else
        begin
          EditIDCard.Text := '';
          if (EditIDCard.Text = '') then
          begin
            FOnScanError(SPimsNoMatch);
            AddLog('No Match');
          end;
        end;
      end
      else
        EditIDCard.Text := '';
    except
        EditIDCard.Text := '';
    end;
  end;
  // 20015371 Convert ID Card
  EditIDCard.Text := IDConvert(EditIDCard.Text);
  if EditIDCard.Text <> '' then
    StartScanningPost(EditIDCard.Text);
end; // ProcessCodeRead

procedure TTimeRecordingF.ReadRegistry;
var
  Reg: TRegistry;
  InitThis: Boolean;
begin
  InitThis := False;
  Reg := TRegistry.Create;
  try
    try
      Reg.RootKey := HKEY_LOCAL_MACHINE;
      if Reg.OpenKey('\Software\ABS\Pims', True) then
      begin
        // RV048.1.
//        UseLogging := Reg.ReadBool('TRECUseLogging');
        UseLogging := False; // PIM-346
      end;
    except
      InitThis := True;
      UseLogging := False;
    end;
  finally
    Reg.CloseKey;
    Reg.Free;
  end;
  if InitThis then
    WriteRegistry;
end; // ReadRegistry

procedure TTimeRecordingF.ScanAbort;
begin
  ClearFields;
  ScanStatus := SWaiting;
  BitBtnAccept.Enabled := True;
  if BitBtnCancel.Visible then
    BitBtnCancel.Enabled := True;
end; // ScanAbort

procedure TTimeRecordingF.SetDeviceSettingsFound(const Value: Boolean);
begin
  FDeviceSettingsFound := Value;
end; // SetDeviceSettingsFound

procedure TTimeRecordingF.SetEndOfDay(var AIDCard: TScannedIDCard);
var
  EndDate: TDateTime;
begin
  EndDate := AIDCard.DateIn;
  EmptyIDCard(AIDCard);
  AIDCard.DateIn       := EndDate;
  AIDCard.Plant        := SpimsEndDay;
  AIDcard.WorkSpot     := SpimsEndDay;
  AIDCard.Job          := SpimsEndDay;
  //CAR 24-03-2003 WAS NEXT
  EditPreJob.Text      := SpimsEndDay;
  EditPrePlant.Text    := SpimsEndDay;
  EditPreWorkspot.Text := SpimsEndDay;
end; // SetEndOfDay

// PIM-181
procedure TTimeRecordingF.ResetEndOfDay;
begin
  EditPreJob.Text      := '';
  EditPrePlant.Text    := '';
  EditPreWorkspot.Text := '';
end; // ResetEndOfDay

procedure TTimeRecordingF.SetIsScanning(const Value: Boolean);
begin
  FIsScanning := Value;
end; // SetIsScanning

procedure TTimeRecordingF.SetValuePosition(const Value: Integer);
begin
  FValuePosition := Value;
end; // SetValuePosition

procedure TTimeRecordingF.SetVerifyPosition(const Value: Integer);
begin
  FVerifyPosition := Value;
end; // SetVerifyPosition

procedure TTimeRecordingF.SetVerifyString(const Value: String);
begin
  FVerifyString := Value;
end; // SetVerifyString

// Handle the Scan Action:
// - Create or Update a Timerecording-record.
function TTimeRecordingF.HandleScanAction(ALastIDCard,
  ANextIDCard: TScannedIDCard;
  AEditPrePlantText: String;
  AIsScanning: Boolean;
  ASavePrevPlant, ASavePrevWK, ASavePrevJob, ASavePrevDate: String;
  ADoNotScanOut: Boolean=False): Boolean;
var
  AEditPrePlantText2, AEditPreDateFromText: String;
  AEditPreWorkspotText, AEditPreJobText: String;
  ALastNow: TDateTime;
begin
  ALastNow := LastNow;
  AEditPrePlantText2 := EditPrePlant.Text;
  AEditPreDateFromText := EditPreDateFrom.Text;
  AEditPreWorkspotText := EditPreWorkspot.Text;
  AEditPreJobText := EditPreJob.Text;
  Result := TimeRecordingDM.HandleScanAction(
    ALastIDCard,  ANextIDCard, AEditPrePlantText,
    AIsScanning,
    ASavePrevPlant, ASavePrevWK, ASavePrevJob, ASavePrevDate,
    CurrentNow,
    ALastNow,
    AEditPrePlantText2, AEditPreDateFromText, AEditPreWorkspotText,
    AEditPreJobText,
    ADoNotScanOut);
  Update;
  LastNow := ALastNow;
  EditPrePlant.Text := AEditPrePlantText2;
  EditPreDateFrom.Text := AEditPreDateFromText;
  EditPreWorkspot.Text := AEditPreWorkspotText;
  EditPreJob.Text := AEditPreJobText;
end; // HandleScanAction

procedure TTimeRecordingF.StartScanning(ACode: String);
var
  LastIDCard, NextIDCard: TScannedIDCard;
  PlannedWorkSpot, PlannedPlant, PlannedDepartment,
  SavePrevPlant, SavePrevWK , SavePrevJob,  SavePrevDate: String;
  StartDate, EndDate{, ProdDate2}: TDateTime;
  WorkedMin, PlannedShiftNumber: Integer;
  {Done, EndOfDayError,} IsFromPlanning: Boolean;
  MyErrorCode: TErrorCode;
  DialogMessageResult: TModalResult; // PIM-161
  EmployeeNumberDummy: Integer;
  EmployeeNameDummy: String;
  ErrorString: String;
  // PIM-181
  // When an employee scans out but there is still another employee
  // with OCCUPIED-job on same workspot, then change the job for this
  // other employee to ATWORK-job.
  // When an employee only changes job for same workspot
  // then no action must be taken!
  // Database actions:
  // - Close current scan for job OCCUPIED for other employee
  // - Open new scan for job ATWORK for other employee
  procedure HybridModeAction2;
  begin
    if HybridDM.HybridMode = 1 then
    begin
      if (LastIDCard.MeasureProductivity or NextIDCard.MeasureProductivity) then
      begin
        try
          // If:
          // - End of day
          // - Not start-of-day (first scan)
          // - Change job but on different workspot
          if (EditPrePlant.Text = SpimsEndDay) or
            ((SavePrevPlant <> SPimsStartDay) and
             (LastIDCard.WorkSpotCode <> NextIDCard.WorkSpotCode)) then
          begin
            // A scan for the same plant+workspot found?
            HybridDM.IsOccupied := HybridDM.HybridWorkspotCheck(
              LastIDCard, LastIDcard,
              CurrentNow, '');
            if HybridDM.IsOccupied then
            begin
              if HybridDM.LastIDCard.JobCode <> ATWORKJOB then
              begin
                // GLOB3-202 Get roaming workspot based on plant of other employee
//                if DoNotScanOut then
//                  TimeRecordingDM.
//                    DetermineRoamingWorkspot(HybridDM.LastIDCard.PlantCode);

                CopyIDCard(HybridDM.NextIDCard, HybridDM.LastIDCard);
                HybridDM.NextIDCard.JobCode := ATWORKJOB;
                HybridDM.NextIDCard.Job := SPimsAtWorkDescription;
                HybridDM.NextIDCard.DateIn := CurrentNow;
                ResetEndOfDay;
                SavePrevPlant := HybridDM.LastIDCard.PlantCode;
                SavePrevWK := HybridDM.LastIDCard.WorkSpotCode;
                SavePrevJob := OCCUPJOB;
                HandleScanAction(HybridDM.LastIDCard, HybridDM.NextIDCard,
                  EditPrePlant.Text, True,
                  SavePrevPlant, SavePrevWK, SavePrevJob, SavePrevDate);
//                  DoNotScanOut);
                if ORASystemDM.OracleSession.InTransaction then
                  ORASystemDM.OracleSession.Commit;
                AddLog('HandleScanAction-Commit');
              end;
            end;
          end;
        except
          on E:EOracleError do
          begin
            if ORASystemDM.OracleSession.InTransaction then // PIM-346
              ORASystemDM.OracleSession.Rollback;
            FOnScanError(E.Message);
          end;
          on E:Exception do
          begin
            if ORASystemDM.OracleSession.InTransaction then // PIM-346
              ORASystemDM.OracleSession.Rollback;
            FOnScanError(E.Message);
          end;
        end;
      end; // if
    end; // if
  end; // HybridModeAction2
begin
  {Code Scan}
  CurrentNow := Now;
  LastNow := 0;

  // GLOB3-112 Get ID via RAW-ID
  if TimeRecordingDM.Raw and (not Assigned(DialogAskEmployeeNumberF)) then
    ACode := TimeRecordingDM.DetermineIDCardRaw(ACode);

  // GLOB3-139
  if TimeRecordingDM.H5427 then
  begin
    ACode := TimeRecordingDM.H5427Convert(ACode);
    EditIDCard.Text := ACode;
  end;

  AddLog('Start of StartScanning');
  AddLog('  Code=[' + ACode + ']');
  if ScanStatus = SScanning then
    Exit;
  if IsSmartCard then
    MyErrorCode := ValueIDCardSmartCard(ACode)
  else
    MyErrorCode := ValueIDCardKeyboard(ACode);
  ErrorString := TimeRecordingDM.ErrorToString(MyErrorCode);
  if ErrorString <> '' then
    FOnScanError(ErrorString);
  if ScanStatus = SError then
    Exit;
  ClearFields;
  EditIDCard.Text       := ACode;
  ScanStatus            := SScanning;
  LabelErrorMsg.Visible := False;
  EmptyIDCard(NextIDCard);
  NextIDCard.IDCard     := ACode;
  NextIDCard.DateIn     := RoundTimeConditional(CurrentNow, 1); //round min // 20015346

  IsScanning := True; // PIM-203 Set always to true!
  TimeRecordingDM.PopulateIDCard(NextIDCard); //combine with ValueIDCard // PIM-203
  EmptyIDCard(LastIDCard);

  LastIDCard.EmployeeCode := NextIDCard.EmployeeCode;
  LastIDCard.EmplName     := NextIDCard.EmplName;
  LastIDCard.CutOfTime    := NextIDCard.CutOfTime; //copy here to skip below

  TimeRecordingDM.DeterminePreviousWorkSpot(NextIDCard.DateIn, LastIDCard,
    CurrentNow, False, False, EmployeeNumberDummy, EmployeeNameDummy);
  // only fills cutoftime for same employee, so copy above
  //  PopulateIDCard(LastIDCard);
  // PIM-161
  if not LastIDCard.Processed then
    LastIDCard.DateOut   := NextIDCard.DateIn;

  if NextIDCard.EmployeeCode = NullInt then
  begin
    FOnScanError(SPimsEmployeeNotFound);
    Exit;
  end;

  // 550478. Personal Screen.
  // When called from 'personal screen', no check needed
  //  for 'IsScanWorkstation'.
  if not TimeRecordingDM.PersonalScreen then
  begin
    // MR:09-09-2004 variables used instead of query
    if IsScanWorkstation then
    begin
      SelectorF.BitBtnEndOfDay.Enabled := (TimeRecording_YN = CHECKEDVALUE);
      if (TimeRecording_YN = UNCHECKEDVALUE) and
        ((LastIDCard.WorkSpotCode = SPimsStartDay) or
         (LastIDCard.WorkSpotCode = NullStr)) then
      begin
        FOnScanError(SPimsNonScanningStation);
        Exit;
      end;
    end
    else
    begin
      FOnScanError(SPimsNonScanningStation);
      Exit;
    end;
  end;

  try
    BitBtnAccept.Enabled         := False;
    if BitBtnCancel.Visible then
      BitBtnCancel.Enabled := False;
    EditEmployeeCode.Text        := IntToStr(NextIDCard.EmployeeCode);
    EditEmployeeDescription.text := NextIDCard.EmplName;
    if (LastIDCard.DateIn > NullDate) and (not LastIDCard.Processed) then // PIM-161
      EditPreDateFrom.Text := FormatDateTime(LONGDATE, LastIDCard.DateIn)
    else
      EditPreDateFrom.Text := FormatDateTime(LONGDATE, //FormatDateTime(Copy(LONGDATE, 1, 6), // PIM-161 This showed only date (not time) ???
        RoundTimeConditional(CurrentNow, 1)); // 20015346

    EditPrePlant.Text    := LastIDCard.Plant;
    EditPreWorkspot.Text := LastIDCard.WorkSpot;
    EditPreJob.Text      := LastIDCard.Job;
    SavePrevPlant        := EditPrePlant.Text;
    SavePrevWK           := EditPreWorkspot.Text;
    SavePrevJob          := EditPreJob.Text;
    SavePrevDate         := EditPreDateFrom.Text;
//
    // do we have to all these procedures??
    DeterminePlannedWorkspot(NextIDCard,
      PlannedWorkspot, PlannedPlant, PlannedDepartment, PlannedShiftNumber);

    IsFromPlanning := (PlannedPlant <> NullStr);

    DetermineStandardPlanningWorkspot(NextIDCard,
      PlannedWorkspot, PlannedPlant, PlannedDepartment, PlannedShiftNumber);

    DetermineWorkspotByDepartment(PlannedPlant,
      PlannedDepartment, PlannedWorkspot);

    // 20014450.50 End-Of-Day-Blink What is end of shift?
    if (LastIDCard.DateIn > NullDate) and (not LastIDCard.Processed) then // PIM-161
    begin
      AProdMinClass.GetShiftDay(LastIDCard, StartDate, EndDate);
      NextIDCard.ShiftStartDateTime := StartDate;
      NextIDCard.ShiftEndDateTime := EndDate;
      // PIM-242 For Blink-at-end-of-shift, assign the shift here based
      // on previous scan! This prevents it looks at the wrong shift.
      NextIDCard.ShiftNumber := LastIDCard.ShiftNumber;
    end
    else
    begin
      NextIDCard.ShiftStartDateTime := NullDate;
      NextIDCard.ShiftEndDateTime := NullDate;
    end;

    // PIM-161
    MyEndOfDay := False;
    if not TimeRecordingDM.PersonalScreen then
    begin
      if (LastIDCard.DateIn > NullDate) and (not LastIDCard.Processed) then
      begin
        // Is previous scan older than 12 hours?
        if (NextIDCard.DateIn - LastIDCard.DateIn) > 0.5 then
        begin
          try
            DialogMessageF := TDialogMessageF.Create(Self);
            DialogMessageF.Caption := GroupBoxMessages.Caption;
            DialogMessageF.InitPosition(
              TimeRecordingF.Left,
              TimeRecordingF.Top + GroupBoxEmployee.Height +
              GroupBoxDateTime.Height,
              TimeRecordingF.Width, TimeRecordingF.Height);
            DialogMessageF.BitBtnYes.Caption := SPimsEndingWork; // GLOB3-272
            DialogMessageF.BitBtnNo.Caption := SPimsStartToWork; // GLOB3-272
            DialogMessageF.BitBtnCancel.Caption := SPimsCancel; // GLOB3-272
            DialogMessageF.Label1.Caption := SPimsLastScanNotClosed;
            DialogMessageF.Label2.Caption := SPimsSeeSR; // GLOB3-272
            DialogMessageResult := DialogMessageF.ShowModal;
            if DialogMessageResult = mrCancel then
              Exit;
            MyEndOfDay := (DialogMessageResult = mrYes);
            if MyEndOfDay then
              EditPrePlant.Text := SpimsEndDay
            else
              SavePrevPlant := SPimsStartDay;
          finally
            DialogMessageF.Free;
          end;
        end;
      end;
    end;

    // 550478. Personal Screen.
    if TimeRecordingDM.PersonalScreen then
    begin
//      AddLog('  DeterminePersonalScreenJob');
      if not DeterminePersonalScreenJob(LastIDCard, NextIDCard,
        PlannedWorkspot, PlannedPlant, PlannedShiftNumber, IsFromPlanning) then
        Exit;
    end
    else
    begin
//      AddLog('  DetermineEmployeeSelectsNextWorkspotAndJob');
      try
        if not MyEndOfDay then // PIM-161
          if not DetermineEmployeeSelectsNextWorkspotAndJob(LastIDCard, NextIDCard,
            PlannedWorkspot, PlannedPlant, PlannedShiftNumber, IsFromPlanning) then
            Exit;
      except
        on E:EOracleError do
        begin
          FOnScanError(E.Message);
          Exit;
        end;
        on E: Exception do
        begin
          FOnScanError(E.Message);
          Exit;
        end;
      end;
    end;

    try // PIM-346
      // 20014327
      DetermineDepartmentByWorkspot(NextIDCard);
//      AddLog('  DetermineShift');
      // PIM-242
      if NextIDCard.Plant <> SPimsEndDay then
        DetermineShift(NextIDCard, PlannedShiftNumber);

      // All data ready. Start write in Data_Base
//      AddLog('  ProcessIllnessMessage');
      TimeRecordingDM.ProcessIllnessMessage(LastIDCard.EmployeeCode);

      HandleScanAction(LastIDCard, NextIDCard, EditPrePlant.Text,
        IsScanning, SavePrevPlant, SavePrevWK, SavePrevJob, SavePrevDate,
        False);

      // PIM-181
      HybridModeAction2;

      if ORASystemDM.OracleSession.InTransaction then // PIM-346
        ORASystemDM.OracleSession.Commit;

      AddLog('HandleScanAction-Commit');
    except // PIM-346
      on E:EOracleError do
      begin
        if ORASystemDM.OracleSession.InTransaction then
        begin
          AddLog('Rollback');
          ORASystemDM.OracleSession.Rollback;
        end;
        FOnScanError(E.Message);
      end;
      on E:Exception do
      begin
        // RV048.1.
        if ORASystemDM.OracleSession.InTransaction then
        begin
          AddLog('Rollback');
          ORASystemDM.OracleSession.Rollback;
        end;
        FOnScanError(E.Message);
      end;
    end;

    // 20015346 Show the correct time with seconds here that was assigned the
    //          last time it was stored to DB.
    if ORASystemDM.TimerecordingInSecs then
    begin
      if LastNow <> 0 then
        EditPreDateFrom.Text := FormatDateTime(LONGDATE, LastNow);
      Update;
    end;

    // 	Worked time
    //  	Current Week
    if Show_Hour_YN = CHECKEDVALUE then  {checked}
    begin
      StartDate := Trunc(NextIDCard.DateIn - DayInWeek(ORASystemDM.WeekStartsOn,
        NextIDCard.DateIn) + 1);
      // trunc above  ReplaceTime(StartDate, EncodeTime(0,0,0,0));
      EndDate   := StartDate + 6;
      WorkedMin :=
        TimeRecordingDM.GetWorkedMin(LastIDCard.EmployeeCode, StartDate, EndDate);
      LabelThisWeekVal.Caption :=
        Format('%.2d:%.2d',[(WorkedMin div 60), (WorkedMin mod 60)]);
      // Last Week
      StartDate := StartDate - 7;
      EndDate   := StartDate + 6;
      WorkedMin :=
        TimeRecordingDM.GetWorkedMin(LastIDCard.EmployeeCode, StartDate, EndDate);
      LabelLastWeekVal.Caption :=
        Format('%.2d:%.2d',[(WorkedMin div 60), WorkedMin mod 60]);

      DetermineStatistics(NextIDCard, LastIDCard);
    end; { if Show_Hour_YN = UNCHECKEDVALUE }
  finally
    // Ready
    ScanStatus := SScaned;
    BitBtnAccept.Enabled := True;
    if BitBtnCancel.Visible then
      BitBtnCancel.Enabled := True;
    EditIDCard.SetFocus;
    AddLog('End of StartScanning');
  end;
end; // StartScanning

procedure TTimeRecordingF.StartScanningPost(const ACode: String);
begin
  try
    // 20013176
    // Reset the TimerAutoClose to give some more time to choose
    // for jobs ?
    if TimeRecordingDM.PersonalScreen then
    begin
//      TimerAutoClose.Enabled := False;
//      TimerAutoClose.Enabled := True;
    end;
    Timer5Sec.Enabled := False;
    // RV048.1.
    // Check connection and reconnect if not connected.
    ORASystemDM.OracleSession.CheckConnection(True);
    if not ORASystemDM.OracleSession.Connected then
      FOnScanError(SPimsConnectionLost);
    AddLog('Start - StartScanningPost');
    try
      if DeviceSettingsFound then
        MyCloseScanner;
      StartScanning(ACode);
      if DeviceSettingsFound then
        MyOpenScanner;
      if ORASystemDM.OracleSession.InTransaction then
      begin
        AddLog('Commit');
        ORASystemDM.OracleSession.Commit;
      end;
    except
      on E:EOracleError do
      begin
        if ORASystemDM.OracleSession.InTransaction then
        begin
          AddLog('Rollback');
          ORASystemDM.OracleSession.Rollback;
        end;
        FOnScanError(E.Message);
      end;
      on E:Exception do
      begin
        // RV048.1.
        if ORASystemDM.OracleSession.InTransaction then
        begin
          AddLog('Rollback');
          ORASystemDM.OracleSession.Rollback;
        end;
        FOnScanError(E.Message);
      end;
    end;
    AddLog('End - StartScanningPost');
  finally
    // 550478. Personal Screen.
    if TimeRecordingDM.PersonalScreen then
    begin
      if ScanStatus <> SError then
        Close;
    end
    else
      if not IsError then
        Timer5Sec.Enabled := True;
  end;
end; // StartScanningPost

// TD-24520 This only checks the IDCard.
function TTimeRecordingF.ValueIDCard(ACode : String): TErrorCode;
begin
  Result := TimeRecordingDM.ValueIDCard(ACode, CurrentNow);
end; // ValueIDCard

// TD-24520 Function has been added, so it can handle 'Ask for Emp.Nr.'
function TTimeRecordingF.ValueIDCardKeyboard(var ACode: String): TErrorCode;
var
  EmployeeNumber: Integer;
begin
  // GLOB3-112
  if TimeRecordingDM.Raw and Assigned(DialogAskEmployeeNumberF) then
  begin
    Result := ValueIDCardSmartCard(ACode);
    Exit;
  end;
  
  Result := ecUnknown;  // Unknown error
  with TimeRecordingDM do
  begin
    oqIDCard.ClearVariables;
    oqIDCard.SetVariable('IDCARD_NUMBER', ACode);
    oqIDCard.Execute;
    if oqIDCard.Eof then // Not found?
    begin
      // TD-24520 Ask for Employeenumber or not, based on startup-argument -A.
      if not Assigned(DialogAskEmployeeNumberF) then
      begin
        Result := ecIDCardNotFound;  // ID card not found
        Exit;
      end;
      // Ask employee number here
      if DialogAskEmployeeNumberF.ShowModal = mrOK then
      begin
        try
          if DialogAskEmployeeNumberF.EmployeeNumber = '' then
            Exit;
          EmployeeNumber := StrToInt(DialogAskEmployeeNumberF.EmployeeNumber);
        except
          EmployeeNumber := -1;
        end;
        if EmployeeNumber <> -1 then
        begin
          // First check if employee exists.
          oqEmployee.ClearVariables;
          oqEmployee.SetVariable('EMPLOYEE_NUMBER', EmployeeNumber);
          oqEmployee.Execute;
          if not oqEmployee.Eof then
          begin
            // Check if there is already an IDCARD-record with same emp. nr.
            oqIDCardSelect.ClearVariables;
            oqIDCardSelect.SetVariable('EMPLOYEE_NUMBER', EmployeeNumber);
            oqIDCardSelect.Execute;
            if not oqIDCardSelect.Eof then
            begin
              // Update existing record with IDCard-field.
              oqIDCardUpdate.ClearVariables;
              oqIDCardUpdate.SetVariable('IDCARD_NUMBER', ACode);
              oqIDCardUpdate.SetVariable('EMPLOYEE_NUMBER', EmployeeNumber);
              oqIDCardUpdate.Execute;
              if ORASystemDM.OracleSession.InTransaction then // PIM-346
                ORASystemDM.OracleSession.Commit;
            end
            else
            begin
              // Insert new IDCard-record
              oqIDCardInsert.ClearVariables;
              oqIDCardInsert.SetVariable('EMPLOYEE_NUMBER', EmployeeNumber);
              oqIDCardInsert.SetVariable('IDCARD_NUMBER', ACode);
              oqIDCardInsert.SetVariable('STARTDATE', Date);
              oqIDCardInsert.SetVariable('MUTATOR',
                ORASystemDM.CurrentProgramUser);
              oqIDCardInsert.Execute;
              if ORASystemDM.OracleSession.InTransaction then // PIM-346
                ORASystemDM.OracleSession.Commit;
            end;
          end
          else
            Exit;
        end;
      end
      else
        Exit;
    end;
  end; {with TimeRecordingDM}
  Result := ValueIDCard(ACode);
end; // ValueIDCardKeyboard

procedure TTimeRecordingF.WriteRegistry;
var
  Reg: TRegistry;
begin
  Reg := TRegistry.Create;
  try
    Reg.RootKey := HKEY_LOCAL_MACHINE;
    if Reg.OpenKey('\Software\ABS\Pims', True) then
    begin
      Reg.WriteBool('TRECUseLogging', UseLogging);
    end;
  finally
    Reg.CloseKey;
    Reg.Free;
  end;
end; // WriteRegistry
{ end Private}

{ Published }
procedure TTimeRecordingF.Timer5SecTimer(Sender: TObject);
begin
  inherited;
  if Timer5Sec.Enabled then
    case ScanStatus of
      SScanning, SEnterID: Exit;
      SScaned, SError:     ScanStatus := SWaiting;
      SWaiting:            ClearFields;
    end;
end; // Timer5SecTimer

procedure TTimeRecordingF.FormCreate(Sender: TObject);
var
  Path, PimsRootPath: String;
begin
  inherited;
  // TESTING
  // GLOB3-202 For this part, this must be disabled.
  // GLOB3-202 We use it also here, but only for Hybrid+Single Employee
  //           handling, via an extra boolean.
  DoNotScanOut := ORASystemDM.TimeRecCRDoNotScanOut;
  ORASystemDM.TimeRecCRDoNotScanOut := False;

  AutoClose := False;
  // 20014450.50 Change colors?
{
  GroupBoxEmployee.Color := clPimsBlue;
  pnlEmployeeClient2.Color := clPimsBlue;
  pnlEmployeeTop.Color := clPimsBlue;
  Panel1.Color := clPimsBlue;
  pnlEmployeeClient.Color := clPimsBlue;
  pnlEmployeeLeft.Color := clPimsBlue;
  pnlEmployeeLeft2.Color := clPimsBlue;
  pnlEmployeeClient3.Color := clPimsBlue;
  pnlEmployeeTop.Color := clPimsBlue;
  Panel1.Color := clPimsBlue;
}
//  Position := poScreenCenter;
  // 20013315
  IsSmartCard := False;
  StoreRawID := False;
  // 20013176
  MyBuffer := '';
  MyHexBuffer := '';
  CardReader := nil;
  // PIM-256
  PCProcsPlusReader := nil;
  // 20015371
  InitString := '';
  // TD-24520.60
  NoTrim := False;
  if TimeRecordingDM.SmartCardCommandsFound then
  begin
    // Initialise needed for smart card reading.
    CardReader := TPCSCCardReader.Create;
    CardReader.MyBuffer := MyBuffer;
    CardReader.MyHexBuffer := MyHexBuffer;
    CardReader.ReaderListBox := ReaderListBox;
    CardReader.CommandComboBox := CommandComboBox;
    CardReader.RichEdit := RichEdit;
    if UpperCase(TimeRecordingDM.SerialDeviceCode) <> 'ICLASS' then
      CardReader.MyReadCardID := ReadCardID_3a
    else
      CardReader.MyReadCardID := ReadCardID_3b;
    CardReader.MyAddErrorLog := AddErrorLog;
    CardReader.Init;
  end;
  // RV079.1.
  IsMaximized := False;
  BtnAcceptHeight := BitBtnAccept.Height;
  BtnAcceptTop := BitBtnAccept.Top;
  BtnCancelHeight := BitBtnCancel.Height;
  BtnCancelTop := BitBtnCancel.Top;

  ThisCaption := Caption;
  // MR:23-01-2008 RV002: Also show ComputerName.
  // GLOB3-324 Do this later.
{  Caption := 'ORCL - ' +
    ORASystemDM.OracleSession.LogonDatabase + ' - ' + Caption + ' - ' +
    DisplayVersionInfo + ' - ' + ORASystemDM.CurrentComputerName
}

  // RV064.2.
  UGLogFilenameSet(LOG_FILENAME);

  LabelCurentDate.Caption := FormatDateTime(LONGDATE, Now);
  lblError.Caption := '';
  IsError := False;
  // Logging
  UseLogging := False; // MRA:07-MAR-2008 Default must be False! // PIM-346
  TimeRecordingDM.UseLogging := UseLogging;
  Path := GetCurrentDir;
  // Determine Path from where Pims has been started.
  PimsRootPath := PathCheck(Path);
  if Pos(UpperCase('\Bin'), UpperCase(Path)) > 0 then
    PimsRootPath := PathCheck(Copy(Path, 1,
      Pos(UpperCase('\Bin'), UpperCase(Path))));
  // Init variables
  LogRootPath := PathCheck(PimsRootPath) + 'Log\';
  if not DirectoryExists(LogRootPath) then
    ForceDirectories(LogRootPath);
  ReadRegistry;
  AddLog('***** START OF TIMERECORDING ***** (' + DisplayVersionInfo + ')');

  SelectorF := TSelectorF.create(self);
  SelectorF.Hide;
  OnScanError := ErrorScanning;
  TimeRecordingDM.OnScanError := ErrorScanning; // PIM-203
  TimeRecordingDM.EnabledWk_JobField := EnabledWk_JobField; // PIM-203
  TimeRecordingDM.LabelErrorMsgDone := LabelErrorMsgDone; // PIM-203
  OnScanAbort := ScanAbort;
  ScanStatus  := SWaiting;
  LabelErrorMsg.Caption := '';

  if TimeRecordingDM.PersonalScreen then
  begin
    // Change Job Dialog, create needed files here.
    // MRA: They are already created in ProductionScreen!
    //      Do not create them again!
//    DialogChangeJobDM := TDialogChangeJobDM.Create(Application);
//    DialogChangeJobF := TDialogChangeJobF.Create(Application);
//    DialogChangeJobF.Hide;
    BitBtnCancel.Visible := True;
  end
  else
  begin
    // TD-23620
    if Screen.Width >= MaxWidth then
      Width := MaxWidth;
    if Screen.Height >= MaxHeight then
      Height := MaxHeight;
  end;

  try
    DetermineSerialDeviceSettings;

    // PIM-256 // GLOB3-324
    if TimeRecordingDM.pcProxPlus then
    begin
      PCPROXAPI.CheckLoadPCProxAPI_DLL();
      PCProcsPlusReader := TPCProcsPlusReader.Create;
      PCPRocsPlusReader.Rosslare := TimeRecordingDM.ReaderRosslare; // GLOB3-324
      if PCPROXAPI.PCProxAPI_DLLLoaded then
        MyCloseScanner;
    end;

    if DeviceSettingsFound then
    begin
      MyOpenScanner;
      OnCodeRead := ProcessCodeRead;
    end;
  except
    on E:Exception do
      FOnScanError(E.Message);
  end;
  // MR:23-1-2004
  IsScanning := True;
  // MR:09-09-2004
  DetermineWorkstation;
  // 20014035 Hide ID or not
  if Hide_ID_YN = CHECKEDVALUE then
    EditIDCard.PasswordChar := '*'
  else
    EditIDCard.PasswordChar := #0;
  // GLOB3-324 Show also SerialDeviceCode
  Caption := 'ORCL - ' +
    ORASystemDM.OracleSession.LogonDatabase + ' - ' + Caption + ' - ' +
    DisplayVersionInfo + ' - ' + ORASystemDM.CurrentComputerName + ' - ' +
    '[' + TimeRecordingDM.SerialDeviceCode + ']';
end; // FormCreate

procedure TTimeRecordingF.FormDestroy(Sender: TObject);
begin
  inherited;
  Timer5Sec.Enabled := False;
  if DeviceSettingsFound then
  begin
    MyCloseScanner;
    OnCodeRead := nil;
  end;
  SelectorF.Release;
  // 20013176
  if Assigned(CardReader) then
    CardReader.Destroy;
end; // FormDestroy

// Code has been read by Keyboard
procedure TTimeRecordingF.BitBtnAcceptClick(Sender: TObject);
  // TD-24520 Trim any leading zeroes
  function TrimLeadingZeros(const S: String): String;
  var
    I, L: Integer;
  begin
    L := Length(S);
    I := 1;
    while (I < L) and (S[I] = '0') do Inc(I);
    Result:= Copy(S, I, Length(S));
  end;
begin
  inherited;
  IsSmartCard := False; // 20013176.4 When Accept-button is clicked, then it is no smart card.
  ScanStatus := SWaiting; // 20013176.4 Reset this!
  // TD-24520 Truncate the ID here.
  // The ID can be from Keyboard or from Magnetic-Stripe-Reader!
  try
    // IMPORTANT: Do not truncate when length of entry is smaller than it
    // is calculated here, to prevent you cannot enter the ID by keyboard.
    if (VerifyPosition <> 0) and (ValuePosition <> 0) then
      if Length(EditIDCard.Text) + 1 >=
        (VerifyPosition + ValuePosition) then // TD-24520.60 Add 1 here and use >= instead of >!
      begin
        EditIDCard.Text := Copy(EditIDCard.Text, VerifyPosition, ValuePosition);
        if not NoTrim then // TD-24520.60
          EditIDCard.Text := TrimLeadingZeros(EditIDCard.Text);
      end;
  except
    //
  end;
  // 20015371 Convert ID Card
  EditIDCard.Text := IDConvert(EditIDCard.Text);
  StartScanningPost(EditIDCard.Text);
end; // BitBtnAcceptClick

procedure TTimeRecordingF.FormShow(Sender: TObject);
begin
  inherited;
  if TimeRecordingDM.NoClose then // PIM-337
  begin
    try
      EnableMenuItem(GetSystemMenu( handle, False ),SC_CLOSE, MF_BYCOMMAND or MF_GRAYED);
//      BorderIcons := BorderIcons - [biMinimize];
    except
    end;
//    BorderIcons := BorderIcons - [biMaximize, biMinimize];
  end;
  if TimeRecordingDM.StartMaximized then // PIM-337
  begin
    Self.WindowState := wsMaximized;
    // PIM-337.2
{    Self.SetBounds(
      Screen.WorkAreaRect.Left, Screen.WorkAreaRect.Top,
      Screen.WorkAreaRect.Right - Screen.WorkAreaRect.Left,
      Screen.WorkAreaRect.Bottom - Screen.WorkAreaRect.Top); }
    Self.Update;
  end;
  // TD-21429
  // Do this in project! Or it gives an error during start!
//  if not TimeRecordingDM.PersonalScreen then
//    Position := poScreenCenter;

  // PersonalScreen - Clear values first!
  EmptyIDCard(TimeRecordingDM.LastIDCard);
  EmptyIDCard(TimeRecordingDM.NextIDCard);
  // RV079.1. Handling when form is maximized during start.
  case WindowState of
  wsNormal: ;
  wsMinimized: ;
  wsMaximized:
    begin
      IsMaximized := True;
      HandleButtonSize;
    end;
  end;

  GroupBoxSmartCard.Visible := False;

  IsScanning := True;
  Timer5Sec.Enabled := True;
  // RV079.1. Do not do this! (maximize is possible)
//  TimeRecordingF.Height := 600;
//  TimeRecordingF.Width  := 800;
  if Show_Hour_YN = UNCHECKEDVALUE then
  begin
// 20013176
{$IFDEF DEBUG}
    if TimeRecordingDM.SmartCardCommandsFound then
    begin
      GroupBoxWorked.Visible := False;
      GroupBoxTime.Visible := False;
      GroupBoxSmartCard.Visible := True;
    end
    else
      GroupBoxMessages.Height := GroupBoxError.Height - GroupBoxMessages.Top;
{$ELSE}
    GroupBoxMessages.Height := GroupBoxError.Height - GroupBoxMessages.Top;
{$ENDIF}
  end
  else
  begin
    GroupBoxSmartCard.Visible := False;
    GroupBoxMessages.Height := Height_BoxMessage;
    GroupBoxWorked.Width := GroupBoxMessages.Width div 2;
  end;
  // 20012858
  ClearFields;
  Timer5SecTimer(Sender); // refreshes time shown on screen.

  // 20013176
  // Close automatic after x seconds. Only do this for Personal Screen!
  if TimeRecordingDM.PersonalScreen then
  begin
    TimerAutoClose.Interval := ORASystemDM.AutoCloseInterval;
    if TimerAutoClose.Interval > 0 then
      TimerAutoClose.Enabled := True;
  end;

  // PIM-256
  if TimeRecordingDM.pcProxPlus then
  begin
    if not PCPROXAPI.PCProxAPI_DLLLoaded then
    begin
      ScanStatus := SError;
      OnScanError('ERROR: Cannot find or load pcProxAPI.dll!');
    end;
  end;

  EditIDCard.SetFocus;
end; // FormShow

procedure TTimeRecordingF.FormKeyPress(Sender: TObject; var Key: Char);
begin
  case Key of
  #13: if ScanStatus <> SScanning then
           StartScanningPost(EditIDCard.Text);
  else
    begin
      inherited;
      Exit;
    end;
  end;
  Key := #0;
end; // FormKeyPress

procedure TTimeRecordingF.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  case key of
  vk_escape: OnScanAbort;
  end;
end; // FormKeyDown

procedure TTimeRecordingF.EditIDCardKeyPress(Sender: TObject;
  var Key: Char);
//var
//	ID: string;
begin
  if ScanStatus in [SScaned, SWaiting, SError] then
  begin
    OnScanAbort;
    if Key <> #27{Escape} then
    begin
      EditIdCard.Text:= Key;
      Key := #0;
    end;
    EditIdCard.SelStart := Length(EditIdCard.text);
    ScanStatus := SEnterID;
  end;
  inherited;
end; // EditIDCardKeyPress

procedure TTimeRecordingF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  if (not AutoClose) and TimeRecordingDM.NoClose then
    Action := caNone
  else
  begin
    Timer5Sec.Enabled := False;
    TimerAutoClose.Enabled := False;
    IsScanning := False;
    AddLog('***** END OF TIMERECORDING *****');
  end;
end; // FormClose

procedure TTimeRecordingF.EditPreDateFromKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;
  Key := #0;
  EditIDCard.SetFocus;
end; // EditPreDateFromKeyPress

procedure TTimeRecordingF.EditPreDateFromClick(Sender: TObject);
begin
  inherited;
  EditIDCard.SetFocus;
end; // EditPreDateFromClick

procedure TTimeRecordingF.WMTimeChange(var Message: TMessage);
begin
  PimsTimeInitialised := False;
//  ShowMessage('WMTime changed');
//  TMessage(Message).Result := Integer(True);
end; // WMTimeChange

procedure TTimeRecordingF.BitBtnCancelClick(Sender: TObject);
begin
  inherited;
  OnScanAbort;
  ModalResult := mrCancel;
end; // BitBtnCancelClick

// RV063.4. 550478.
// When an error occured it must always be logged.
procedure TTimeRecordingF.AddErrorLog(AMsg: String);
begin
  WErrorLog(AMsg); // 20014826
end;

// RV079.1.
procedure TTimeRecordingF.HandleButtonSize;
begin
// 20014289 Keep buttons in a fixed size.
{
  SelectorF.IsMaximized := IsMaximized;
  if IsMaximized then
  begin
    BitBtnAccept.Height := Round(BtnAcceptHeight * 1.7);
    BitBtnAccept.Top := BitBtnAccept.Top - Round(BtnAcceptTop / 2);
    BitBtnCancel.Height := Round(BtnCancelHeight * 1.7);
    BitBtnCancel.Top := BitBtnCancel.Top - Round(BtnCancelTop / 2);
  end
  else
  begin
    BitBtnAccept.Height := BtnAcceptHeight;
    BitBtnAccept.Top := BtnAcceptTop;
    BitBtnCancel.Height := BtnCancelHeight;
    BitBtnCancel.Top := BtnCancelTop;
  end;
}
end; // HandleButtonSize

// RV079.1.
procedure TTimeRecordingF.WMSysCommand(var Msg: TWMSysCommand);
begin
  if TimeRecordingDM.NoClose then // PIM-337
  begin
    if Msg.CmdType <> SC_MINIMIZE then
      inherited;
  end
  else
  begin
    if (Msg.CmdType = SC_RESTORE) then
    begin
      IsMaximized := False;
      HandleButtonSize;
    end
    else
      if (Msg.CmdType = SC_MINIMIZE) then
      begin
        IsMaximized := False;
        HandleButtonSize;
      end
      else
        if (Msg.CmdType = SC_MAXIMIZE) then
        begin
          IsMaximized := True;
          HandleButtonSize;
        end;
    DefaultHandler(Msg);
  end;
end; // WMSysCommand

procedure TTimeRecordingF.TimerAutoCloseTimer(Sender: TObject);
begin
  inherited;
  TimerAutoClose.Enabled := False;
  Close;
end;

// 20013176
// CardID has been read from smart card reader.
// Process Scan here.
procedure TTimeRecordingF.CardReaderProcessScan;
begin
  // CardID should be in MyBuffer.
  // Now process the scan.
  if CardReader.MyHexBuffer <> '' then
  begin
    EditIDCard.Text := CardReader.MyHexBuffer;
    StartScanningPost(EditIDCard.Text);
//  BitBtnAcceptClick(nil);
  end;
end; // ReadCardProcessScan

// 20013176
// HID OMNIKEY 5321 Card Reader - MIFARE Classic 1K Card
// TD-23247 Added some delays (Wait) between cardreader-commands.
procedure TTimeRecordingF.ReadCardID_3a;
var
  I: Integer;
  CardReaderIsConnected, TransmitOK: Boolean;
begin
  if not Visible then
    Exit;
  IsSmartCard := True;
  StoreRawID := True;
  CardReader.MyBuffer := '';
  CardReader.MyHexBuffer := '';
  // 20013176.2 Connect Shared instead of Exclusive!
  // If needed, we can switch to 'shared' or to 'exclusive'.
  ORASystemDM.Wait(ORASystemDM.CardReaderDelay);
  if ORASystemDM.CardReaderConnectExclusive then
  begin
    CardReader.AddLog('CardReader.ConnectExclusive:');
    CardReaderIsConnected := CardReader.ConnectExclusive;
  end
  else
  begin
    CardReader.AddLog('CardReader.ConnectShared:');
    CardReaderIsConnected := CardReader.ConnectShared;
  end;
  if CardReaderIsConnected then
  begin
    ORASystemDM.Wait(ORASystemDM.CardReaderDelay);
    // Use commands that were read from database.
    for I := 0 to TimeRecordingDM.SmartCardCommandList.Count - 1 do
    begin
      CommandComboBox.Text := TimeRecordingDM.SmartCardCommandList[I];
      TransmitOK := CardReader.Transmit;
      ORASystemDM.Wait(ORASystemDM.CardReaderDelay);
      if not TransmitOK then
        Break;
    end;
    CardReader.Disconnect;
    ORASystemDM.Wait(ORASystemDM.CardReaderDelay);
    // MRA:26-JUN-2012 Truncate the read key if needed.
    if VerifyPosition <> 0 then
      CardReader.MyHexBuffer := Copy(CardReader.MyHexBuffer, 1, VerifyPosition);
    CardReader.AddLog('MyHexBuffer=' + CardReader.MyHexBuffer);
    CardReaderProcessScan;
  end;
{
  if CardReader.ConnectExclusive then
  begin
    // Load (Mifare Default) key in reader (key location 0)
    CommandComboBox.Text := 'FF 82 20 00 06 FF FF FF FF FF FF';
    if CardReader.Transmit then
    begin
      // Authenticate sector 0, Block 1 with key at location 0
      CommandComboBox.Text := 'FF 88 00 01 60 00';
      if CardReader.Transmit then                     //       bl
      begin                                // bl=block
        // Read the full 16 bytes from Sector 0, Block 1
        CommandComboBox.Text := 'FF B0 00 01 10';
        CardReader.Transmit;                      // bl by
      end;                  // bl=block by=bytes to read
    end;
    CardReader.Disconnect;
  end;
  CardReader.AddLog('MyBuffer=' + CardReader.MyBuffer);
}
end; // ReadCardID_3a

// 20013176
// HID OMNIKEY 5321 Card Reader
// For use with ICard.
procedure TTimeRecordingF.ReadCardID_3b;
var
  I: Integer;
  CardReaderIsConnected, TransmitOK: Boolean;
begin
  if not Visible then
    Exit;
  IsSmartCard := True;
  StoreRawID := True;
  CardReader.MyBuffer := '';
  CardReader.MyHexBuffer := '';
  // 20013176.2 Connect Shared instead of Exclusive!
  // If needed, we can switch to 'shared' or to 'exclusive'.
  ORASystemDM.Wait(ORASystemDM.CardReaderDelay);
  if ORASystemDM.CardReaderConnectExclusive then
  begin
    CardReaderIsConnected := CardReader.ConnectExclusive;
  end
  else
  begin
    CardReaderIsConnected := CardReader.ConnectShared;
  end;
  if CardReaderIsConnected then
  begin
    ORASystemDM.Wait(ORASystemDM.CardReaderDelay);
    // Use commands that were read from database.
    for I := 0 to TimeRecordingDM.SmartCardCommandList.Count - 1 do
    begin
      CommandComboBox.Text := TimeRecordingDM.SmartCardCommandList[I];
      TransmitOK := CardReader.TransmitCLICC;
      ORASystemDM.Wait(ORASystemDM.CardReaderDelay);
      if not TransmitOK then
        Break;
    end;
    CardReader.Disconnect;
    ORASystemDM.Wait(ORASystemDM.CardReaderDelay);
    CardReader.AddLog('MyHexBuffer=' + CardReader.MyHexBuffer);
    CardReaderProcessScan;
  end;
{
  if CardReader.ConnectExclusive then
  begin
    // Load (Mifare Default) key in reader (key location 0)
    CommandComboBox.Text := 'FF 82 20 00 06 FF FF FF FF FF FF';
    if CardReader.TransmitCLICC then
    begin
      // Authenticate sector 0, Block 1 with key at location 0
      CommandComboBox.Text := 'FF 88 00 01 60 00';
      if CardReader.TransmitCLICC then                     //       bl
      begin                                // bl=block
        // Read the full 16 bytes from Sector 0, Block 1
        CommandComboBox.Text := 'FF B0 00 01 10';
        CardReader.TransmitCLICC;                      // bl by
      end;                  // bl=block by=bytes to read
    end;
    CardReader.Disconnect;
  end;
  CardReader.AddLog('MyBuffer=' + CardReader.MyBuffer);
}
end; // ReadCardID_3b

// 20013315 Contactless Card Reader
// This routine checks the ID from the Card stored in an extra field.
// When there is no match, then optionally (based on parameter at startup)
// it asks for the employee number and creates/updates the IDCARD-record.
function TTimeRecordingF.ValueIDCardSmartCard(
  var ACode: String): TErrorCode;
var
  EmployeeNumber: Integer;
  MyOK: Boolean;
  ARawCode: String;
  TryI: Integer;
  function FillZero(const Value: String; const Len: Integer): String;
  begin
    Result := Value;
    while Length(Result) < Len do
      Result := '0' + Result;
  end;
  function MyRandomNumber: String;
  var
    Nr: Integer;
  begin
    // int := 1 + Random(100);    // The 100 value gives a range 0..99
    Randomize;
    Nr := Random(10000000);
    ACode := FillZero(IntToStr(Nr), 8);
    Result := ACode;
  end;
  function InsertIDCardRawRecord: Boolean;
  begin
    Result := False;
    try
      // Insert new record with RAW-field.
      with TimeRecordingDM do
      begin
        oqIDCardRawInsert.ClearVariables;
        oqIDCardRawInsert.SetVariable('EMPLOYEE_NUMBER', EmployeeNumber);
        oqIDCardRawInsert.SetVariable('IDCARD_NUMBER', MyRandomNumber);
        oqIDCardRawInsert.SetVariable('IDCARD_RAW', ARawCode);
        oqIDCardRawInsert.SetVariable('STARTDATE', Date);
        oqIDCardRawInsert.SetVariable('MUTATOR', ORASystemDM.CurrentProgramUser);
        oqIDCardRawInsert.Execute;
        Result := True;
      end;
    except
      // Trap duplicate value error here.
      on E:EOracleError do
      begin
        if E.ErrorCode = 00001 then // duplicate value detected
        begin
          Result := False;
        end;
      end;
      on E:Exception do
      begin
        Result := True;
      end;
    end;
  end; // InsertIDCardRawRecord
begin
  Result := ecUnknown;  // Unknown error
  EmployeeNumber := -1;
  ARawCode := ACode;
  ScanStatus := SWaiting; // 20013176.4 Reset this!
  with TimeRecordingDM do
  begin
    oqIDCardRaw.ClearVariables;
    oqIDCardRaw.SetVariable('IDCARD_RAW', ARawCode);
    oqIDCardRaw.Execute;
    if not oqIDCardRaw.Eof then
      ACode := oqIDCardRaw.FieldAsString('IDCARD_NUMBER')
    else
    begin
      // PIM-383 It's possible the ID is the 'normal' ID.
      oqIDCard.ClearVariables;
      oqIDCard.SetVariable('IDCARD_NUMBER', ARawCode);
      oqIDCard.Execute;
      if not oqIDCard.Eof then
        ACode := oqIDCard.FieldAsString('IDCARD_NUMBER')
      else
      begin
        // 20013176.3 Based on parameter at startup ask for
        // employeenumber or not.
        if not Assigned(DialogAskEmployeeNumberF) then
        begin
          Result := ecIDCardNotFound;
          Exit;
        end;
        // Ask employee number here
        if DialogAskEmployeeNumberF.ShowModal = mrOK then
        begin
          try
            if DialogAskEmployeeNumberF.EmployeeNumber = '' then
              Exit;
            EmployeeNumber := StrToInt(DialogAskEmployeeNumberF.EmployeeNumber);
          except
            EmployeeNumber := -1;
          end;
          if EmployeeNumber <> -1 then
          begin
            // First check if employee exists.
            oqEmployee.ClearVariables;
            oqEmployee.SetVariable('EMPLOYEE_NUMBER', EmployeeNumber);
            oqEmployee.Execute;
            if not oqEmployee.Eof then
            begin
              // Check if there is already an IDCARD-record with same emp. nr.
              oqIDCardRawSelect.ClearVariables;
              oqIDCardRawSelect.SetVariable('EMPLOYEE_NUMBER', EmployeeNumber);
              oqIDCardRawSelect.Execute;
              if not oqIDCardRawSelect.Eof then
              begin
                ACode := oqIDCardRawSelect.FieldAsString('IDCARD_NUMBER');
                // Update existing record with RAW-field.
                oqIDCardRawUpdate.ClearVariables;
                oqIDCardRawUpdate.SetVariable('IDCARD_RAW', ARawCode);
                oqIDCardRawUpdate.SetVariable('EMPLOYEE_NUMBER', EmployeeNumber);
                oqIDCardRawUpdate.Execute;
              end
              else
              begin
                TryI := 0;
                repeat
                  MyOK := InsertIDCardRawRecord;
                  inc(TryI);
                until MyOK or (TryI >= 10);
              end;
            end
            else
              Exit;
          end;
        end
        else
          Exit;
      end;
    end;
  end;
  Result := ValueIDCard(ACode);
end; // ValueIDCardSmartCard

procedure TTimeRecordingF.FormResize(Sender: TObject);
begin
  inherited;
  // TD-23620
  EditIDCard.Width := pnlEmployeeClient.Width;
  EditEmployeeDescription.Width := pnlEmployeeClient3.Width;
  pnlPreviousLeft1.Width := GroupBoxPrevious.Width div 2;
  EditPreDateFrom.Width := pnlPreviousClient2.Width;
  EditPrePlant.Width := pnlPreviousClient2.Width;
  EditPreWorkspot.Width := pnlPreviousClient3.Width;
  EditPreJob.Width := pnlPreviousClient3.Width;
end;

// 20014327
procedure TTimeRecordingF.DetermineDepartmentByWorkspot(
  var ANextIDCard: TScannedIDCard);
begin
  with TimeRecordingDM.oqWorkspot do
  begin
    ClearVariables;
    SetVariable('PLANT_CODE',    ANextIDCard.PlantCode);
    SetVariable('WORKSPOT_CODE', ANextIDCard.WorkspotCode);
    Execute;
    if not Eof then
      ANextIDCard.DepartmentCode := FieldAsString('DEPARTMENT_CODE')
  end; {with oqWorkspot}
end; // DetermineDepartmentByWorkspot

// 20015371 Convert ID Card
function TTimeRecordingF.IDConvert(AIDCode: String): String;
begin
  Result := AIDCode;
  try
    if Length(AIDCode) >= 10 then
      if InitString <> '' then
        Result := TimeRecordingDM.IDConvert(AIDCode);
  except
    Result := AIDCode;
  end;
end;

// 20015346 Only show the time.
procedure TTimeRecordingF.Timer1SecTimer(Sender: TObject);
  // RV040. Synchronize time each 24 hours, at about 03:05.
  procedure SynchronizeTime;
  var
    Hour, Min, Sec, MSec: word;
  begin
    DecodeTime(Now, Hour, Min, Sec, MSec);
    if (Hour = 3) then
      if ((Min >= 5) and (Min <= 10)) then
        PimsTimeInitialised := False;
  end;
begin
  inherited;
  try
    LabelCurentDate.Caption := FormatDateTime(LONGDATE, Now);
    SynchronizeTime;
  except
    //
  end;
end; // Timer1SecTimer

// 20014450.50 Check if DateIn is at end of shift
function TTimeRecordingF.EndOfShiftTest(
  ANextIDCard: TScannedIDCard): Boolean;
begin
  Result := False;
  if (ANextIDCard.ShiftStartDateTime > NullDate) then
    if (ANextIDCard.DateIn >= ANextIDCard.ShiftEndDateTime) and
      (ANextIDcard.DateIn <= (ANextIDCard.ShiftEndDateTime + 1/24)) then // Add 1 hour to end
        Result := True;
end;

// PIM-203
procedure TTimeRecordingF.LabelErrorMsgDone;
begin
  LabelErrorMsg.Font.Color := clWindowText;
  LabelErrorMsg.Caption    := SPimsScanDone;
end; // LabelErrorMsgDone

// PIM-256
procedure TTimeRecordingF.MyOpenScanner;
begin
  if Assigned(PCProcsPlusReader) then
  begin
    PCProcsPlusReader.Connect;
    if PCPROXAPI.PCProxAPI_DLLLoaded then
      TimerPCProxPlus.Enabled := True;
  end
  else
    OpenScanner;
end;

// PIM-256
procedure TTimeRecordingF.MyCloseScanner;
begin
  if Assigned(PCProcsPlusReader) then
  begin
    PCProcsPlusReader.DisConnect;
    if PCPROXAPI.PCProxAPI_DLLLoaded then
      TimerPCProxPlus.Enabled := False;
  end
  else
    CloseScanner;
end;

procedure TTimeRecordingF.TimerPCProxPlusTimer(Sender: TObject);
begin
  inherited;
  if Assigned(PCProcsPlusReader) then
  begin
    PCProcsPlusReader.ErrorMsg := '';
    PCProcsPlusReader.GetActiveID;
    if PCProcsPlusReader.ID <> '' then
    begin
      // Conversion is done during read, do not do it here!
      EditIDCard.Text := PCProcsPlusReader.ID;
      if EditIDCard.Text <> '' then
        StartScanningPost(EditIDCard.Text);
    end;
    if PCProcsPlusReader.ErrorMsg <> '' then
      OnScanError(PCProcsPlusReader.ErrorMsg);
    Sleep(250);
  end;
end;

// PIM-397
procedure TTimeRecordingF.WMQueryEndSession(
  var Message: TWMQueryEndSession);
begin
  // respond to shutdown message form the system
  Message.Result := Integer(True);
  AutoClose := True;
  Close;
end;

end.

