(*
  MRA:8-MAR-2019 GLOB3-272
  - Timerecording and change needed for 'last scan is not closed'-dialog
  - Resize buttons based on length of text used for caption
  MRA:17-APR-2019 GLOB3-272 - Rework
  - Timerecording and change needed for 'last scan is not closed'-dialog
  MRA:8-JUL-2019 GLOB3-330
  - Make functionality with 'occupied' optional
*)
unit DialogMessageFRM;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, UPimsConst, ORASystemDMT;

const
  TimeOutSecs   = 60;

type
  TDialogMessageF = class(TForm)
    Panel2: TPanel;
    Panel3: TPanel;
    Label1: TLabel;
    tmrCancel: TTimer;
    Panel1: TPanel;
    BitBtnCancel: TBitBtn;
    BitBtnYes: TBitBtn;
    Label2: TLabel;
    BitBtnNo: TBitBtn;
    procedure BitBtnYesClick(Sender: TObject);
    procedure BitBtnCancelClick(Sender: TObject);
    procedure tmrCancelTimer(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BitBtnNoClick(Sender: TObject);
  protected
    procedure CreateParams(var params: TCreateParams); override;
  private
    { Private declarations }
    FTimerCloseEnabled: Boolean;
    procedure WMSysCommand(var Msg: TWMSysCommand); message WM_SYSCOMMAND;
  public
    { Public declarations }
    procedure InitPosition(ALeft, ATop, AWidth, AHeight: Integer);
    property TimerCloseEnabled: Boolean read FTimerCloseEnabled write FTimerCloseEnabled;
  end;

var
  DialogMessageF: TDialogMessageF;

implementation

{$R *.dfm}

procedure TDialogMessageF.BitBtnYesClick(Sender: TObject);
begin
  ModalResult := mrYes;
end;

procedure TDialogMessageF.BitBtnCancelClick(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

// Prevent it is moved
procedure TDialogMessageF.WMSysCommand(var Msg: TWMSysCommand);
begin
  if ((Msg.CmdType and $FFF0) = SC_MOVE) or
    ((Msg.CmdType and $FFF0) = SC_SIZE) then
  begin
    Msg.Result := 0;
    Exit;
  end;
end;

procedure TDialogMessageF.InitPosition(ALeft, ATop, AWidth, AHeight: Integer);
begin
  if Width > AWidth then
    Width := AWidth;
  if Height > AHeight then
    Height := AHeight;
  Left := ALeft + Round(AWidth / 2 - Width / 2);
  if Left < 0 then
    Left := ALeft;
  Top := ATop + Round(AHeight / 2 - Height / 2);
  if Top < 0 then
    Top := ATop;
end;

// Ensure the form stays on top of the calling form.
procedure TDialogMessageF.CreateParams(var params: TCreateParams);
begin
  inherited;
  try
    params.WndParent := Screen.ActiveForm.Handle;
    if (params.WndParent <> 0) and (IsIconic(params.WndParent)
      or not IsWindowVisible(params.WndParent)
      or not IsWindowEnabled(params.WndParent)) then
      params.WndParent := 0;

    if params.WndParent = 0 then
      params.WndParent := Application.Handle;
  except
  end;
end;

procedure TDialogMessageF.tmrCancelTimer(Sender: TObject);
begin
  ModalResult := mrCancel;
  Close;
end;

procedure TDialogMessageF.FormCreate(Sender: TObject);
begin
  tmrCancel.Interval := TimeOutSecs * 1000;
  TimerCloseEnabled := True;
  Label1.Caption := '';
  Label2.Caption := '';
  Self.BitBtnCancel.Caption := '';
  Self.BitBtnNo.Caption := '';
end;

procedure TDialogMessageF.FormShow(Sender: TObject);
begin
  tmrCancel.Enabled := TimerCloseEnabled;

  Self.BitBtnCancel.Visible := Self.BitBtnCancel.Caption <> '';
  Self.BitBtnNo.Visible := Self.BitBtnNo.Caption <> '';

  Self.BitBtnCancel.Width := Round(Length(Self.BitBtnCancel.Caption) * Self.BitBtnCancel.Font.Size + 50);
  Self.BitBtnNo.Width := Round(Length(Self.BitBtnNo.Caption) * Self.BitBtnNo.Font.Size + 50);
  Self.BitBtnYes.Width := Round(Length(Self.BitBtnYes.Caption) * Self.BitBtnYes.Font.Size + 50);
  Self.BitBtnCancel.Left := Panel1.Width - Self.BitBtnCancel.Width - 5;
  Self.BitBtnNo.Left := Panel1.Width - Self.BitBtnYes.Width - 5 - Self.BitBtnCancel.Width - 5;
  Self.BitBtnYes.Left := Panel1.Width - Self.BitBtnYes.Width - 5 - Self.BitBtnNo.Width - 5 - Self.BitBtnCancel.Width - 5;

  if (not Self.BitBtnNo.Visible) and (not Self.BitBtnCancel.Visible) then
  begin
    Self.BitBtnYes.Left := Self.BitBtnCancel.Left;
  end
  else
    if (not Self.BitBtnCancel.Visible) then
    begin
      Self.BitBtnNo.Left := Self.BitBtnCancel.Left;
      Self.BitBtnYes.Left := Self.BitBtnNo.Left;
    end
    else
      if (not Self.BitBtnNo.Visible) then
      begin
        Self.BitBtnYes.Left := Self.BitBtnNo.Left;
      end;
end; // FormShow

procedure TDialogMessageF.BitBtnNoClick(Sender: TObject);
begin
  ModalResult := mrNo;
end;

end.
