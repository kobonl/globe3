unit CopyGlobalDMT;

interface

uses
  SysUtils, Classes, DB, DBTables, UScannedIDCard;

type
  TTimeBlock = array [1..4] of Boolean;

type
  TGlobalDM = class(TDataModule)
    Query1: TQuery;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  GlobalDM: TGlobalDM;

// procedures - functions moved from UGlobalFunctions
(*  Books... these functions are not public and not used
function BooksExceptionalTime(AQuery: TQuery; BookingDay: TDateTime;
  ExceptRegularMin: Integer;
  EmployeeData: TScannedIDcard; Manual: string): Integer;
// This function save records in SalaryHoursPerEmployee with
// ExceptRegularMin contain the sum of exceptional minute and regular minute
// 1. Exceptional hour (min)
// 2. Regular hours (min)
// result = regular minutes
procedure BooksHoursForShift(AQuery: TQuery; BookingDay: TDateTime;
  EmployeeIDCard: TScannedIDCard; ProdMinPlusPayedBreaks: Integer;
  Manual: String);
// Book hours if there is a hourtype set for Shift where Employee worked.

function BooksOverTime(AQuery: TQuery; BookingDay: TDateTime;
  WorkedMin: Integer; EmployeeData: TScannedIDcard;
  Manual: string;
  BookExceptionalBeforeOvertime, RoundMinutes: Boolean): Integer;
// This function save records in SalaryHoursPerEmployee
// WorkedMin= OvertimeMin + ExceptionalMin + RegularMin.
// insert all overtime records (minutes and hourtype).
// result = the remaining minutes = ExceptionalMin + RegularMin.

procedure BooksRegularSalaryHours(AQuery: TQuery; BookingDay: TDateTime;
  EmployeeData: TScannedIDcard; BookMinutes: Integer; Manual: String);

procedure ProcessBookings(AQuery: TQuery; EmployeeIDCard: TScannedIDCard;
  BookingDay: TDateTime; UpdateProdMin: Boolean;
  ProdMin, PayedBreaks: Integer;
  Manual: String; RoundMinutes: Boolean);
// Combine some booking-procedures to ONE procedure.

procedure ProcessBookingsForContractRoundMinutes(AQuery: TQuery;
  EmployeeIDCard: TScannedIDCard;
  BookingDay: TDateTime; UpdateProdMin: Boolean;
  ProdMin, PayedBreaks: Integer;
  Manual: String; RoundMinutes: Boolean);

function DetermineExceptionalBeforeOvertime(AQuery: TQuery;
  EmployeeData: TScannedIDCard): Boolean;
*)
procedure ProcessTimeRecording(AQuery: TQuery; EmployeeIDCard: TScannedIDCard;
  FirstScan, LastScan, UpdateSalMin, UpdateProdMinOut, UpdateProdMinIn: Boolean;
  DeleteAction: Boolean; DeleteEmployeeIDCard: TScannedIDCard;
  LastScanRecordForDay: Boolean);
// move to timerecording
// This procedure process time recording for a particular scan = EmployeeIDCard.
// can be used only for manual records !!
(*
function UpdateAbsenceTotal(AQuery: TQuery;BookingDay: TDateTime;
  EmployeeData: TScannedIDCard; Minutes: Double): Integer;
// This function find record in ABSENCETOTAL
// if record found then add Minutes to EARNED_TFT_MINUTE
// if not insert a new record with EARNED_TFT_MINUTE = Minutes
// Returns
// -1 if any error
//  0 if update record
//  1 if insert new record

*)

procedure UnprocessProcessScans(AQuery: TQuery; FromDate: TDateTime;
  EmployeeIDCard: TScannedIDCard; ProcessSalary: Boolean; DeleteAction: Boolean;
  ProdDate1, ProdDate2, ProdDate3, ProdDate4: TDateTime);
// This procedure subtract Production minute for employee - scan EmployeeIDCard
// Delete all salary hours with booking date in overtimeperiod of EmployeeIDCard
// Recalculate the salary hours for scans resulting booking date in overtime
// period.
// If ProcessScan=true then the curent scan EMployeeIDCard is processed also in
// order with the others scan by datetime_in.

(*
function UpdateProductionHour(AQuery: Tquery; ProductionDate: TDateTime;
  EmployeeData: TScannedIDCard; Minutes, PayedBreaks: Integer;
  Manual: string):Integer;
// This function find record in PRODHOURPEREMPLOYEE
// if record found then add Minutes to PRODUCTION_MINUTE
// if not, insert a new record with PRODUCTION_MINUTE = Minutes
// Returns
// -1 if any error
//  0 if update record
//  1 if insert new record
// !!! If updated ProdMin <=0 then the record for update will be deleted !!!
// move cth
*)
(*
procedure FillProdHourPerHourType(AQuery: TQuery; EmployeeData: TScannedIDCard;
   ProductionDate: TDateTime; HourType, Minutes: Integer; Manual: String;
   InsertProdHrs: Boolean);
// This function fill a new table PRODHOURPEREMPLPERTYPE which keep the relation
// between production minutes and salary minutes per hourtype is call only if
// column  FILLPRODUCTIONHOURS_YN  = 'Y' in pimssetting
// move cth
//not public
*)
(*
procedure DeleteProdHourForAllTypes(AQuery:TQuery; EmployeeData: TScannedIDCard;
  FromDate, ToDate: TDateTime;  Manual: String);
// move cth
// not public
*)

procedure ComputeOvertimePeriod(var StartDate, EndDate: TDateTime;
						AQuery: TQuery; Employeedata: TScannedIDCard);
// Overtime could be defined on
// 1. day
// 2. week
// 3. period in weeks.
// The StartDate/EndDate are necessary to add all employee worked minute from
// 1. SalaryHoursPerEmployee
// 2. AbsenceHoursPerEmployee
// StartDate & EndDate are included in interval !!
// move to calctotal

function UpdateSalaryHour(AQuery: TQuery; SalaryDate: TDateTime;
  EmployeeData: TScannedIDCard;
{	Employee_Number, }HourTypeNumber, Minutes: Integer; Manual: string;
  	 Replace: Boolean): Integer;
// This function find record in SALARYHOURSPEREMPLOYEE
// if record found then add Minutes to SALARY_MINUTE
// if not, insert a new record with SALARY_MINUTE = Minutes
// this function return:
// the number of minute added to existing record
// result = Minutes - Existing Minutes in database !!!
// used public by HoursPerEmployee
procedure UpdateAbsenceTotalMode(AQuery: TQuery; EmployeeNumber,
  AbsenceYear: Integer; Minutes: Double;	AbsenceTypeCode: Char);
// This procedure looking for record in table ABSENCETOTAL
// if found then update minutes (add or subtract)
// if not, add a record with Minutes.
// depending on WhatToUpdate the procedure update minute for
// "I"	ILLNESS_TOTAL_MINUTES
// "W"  USED_WTR_MINUTES
// "T"	USED_TFT_MINUTES
// "H"  USED_HOLIDAY_MINUTES
// used in HoursPerEmployeeDMT, DialogProcessAbsenceHours
// move to cth

(*
function WhatBreaks(AQuery: TQuery; Employee_number,Shift_Number: Integer;
  Plant_Code,Department_Code: string; Payed: Boolean): string;
// This function determine if Breaks are definde per Employee, Department or
// Shift
// so the function will return the table name of breaks.
// ex: BREAKPEREMPLOYEE
// move cth
*)
(*
function WhatTimeBlock(AQuery: TQuery; Employee_number,Shift_Number: Integer;
  Plant_Code,Department_Code: string;var TB: TTimeBlock): string;
// This function determine if the TimeBlocks are defined per Employee,Department
// or Shift and return the table name ex: TIMEBLOCKPEREMPLOYEE
// return valid Time blocks in TB structure
// used in Pims.exe only
//CAR - CHANGE EMPLOYEE_NUMBER into EmployeeData//
*)
(*
procedure QueryCopy(var QuerySource, QueryDestination: TQuery);
// This procedure copys the property from Source to destination
// Both queries must be created.
//move cth
// not public
*)

implementation

uses
  ORASystemDMT, UPimsConst, CalculateTotalHoursDMT,
  UGlobalFunctions;

{$R *.dfm}

// procedures - functions moved from UGlobal
procedure QueryCopy(var QuerySource, QueryDestination: TQuery);
begin
  QueryDestination.SessionName  := QuerySource.SessionName;
  QueryDestination.DataBaseName := QuerySource.DatabaseName;
end;
(* used in Pims.exe only
function WhatTimeBlock(AQuery: TQuery; Employee_number,Shift_Number: Integer;
  Plant_Code,Department_Code: string;var TB: TTimeBlock): string;
var
  QueryWork: TQuery;
  Index: Integer;
begin
  Result := NullStr;
  QueryWork := TQuery.Create(Nil);
  QueryCopy(AQuery,QueryWork);
  for Index := 1 to length(TB) do
    TB[Index] := False;
  // TIMEBLOCKPEREMPLOYEE
  QueryWork.Active := False;
  QueryWork.SQL.Clear;
  QueryWork.SQL.Add('SELECT TIMEBLOCK_NUMBER');
  QueryWork.SQL.Add('FROM TIMEBLOCKPEREMPLOYEE');
  QueryWork.SQL.Add('WHERE EMPLOYEE_NUMBER=:EMPNO AND PLANT_CODE=:PCODE');
  QueryWork.SQL.Add('AND SHIFT_NUMBER=:SHNO');
  QueryWork.Prepare;
  QueryWork.ParamByName('EMPNO').AsInteger := Employee_Number;
  QueryWork.ParamByName('PCODE').AsString := Plant_Code;
  QueryWork.ParamByName('SHNO').AsInteger := Shift_Number;
  QueryWork.Active := True;
  while not QueryWork.Eof do
  begin
    Result := 'TIMEBLOCKPEREMPLOYEE';
    Index := QueryWork.FieldByName('TIMEBLOCK_NUMBER').AsInteger;
    if Index <= length(TB) then
      TB[Index] := True;
    QueryWork.Next;
  end;
  if Result = Nullstr then
  begin
    // TIMEBLOCKPERDEPARTMENT
    QueryWork.Active := False;
    QueryWork.SQL.Clear;
    QueryWork.SQL.Add('SELECT TIMEBLOCK_NUMBER');
    QueryWork.SQL.Add('FROM TIMEBLOCKPERDEPARTMENT');
    QueryWork.SQL.Add('WHERE PLANT_CODE=:PCODE AND DEPARTMENT_CODE=:DCODE AND ');
    QueryWork.SQL.Add('SHIFT_NUMBER=:SHNO');
    QueryWork.Prepare;
    QueryWork.ParamByName('PCODE').AsString := Plant_Code;
    QueryWork.ParamByName('DCODE').AsString := Department_Code;
    QueryWork.ParamByName('SHNO').AsInteger := Shift_Number;
    QueryWork.Active := True;
    while not QueryWork.Eof do
    begin
      Result := 'TIMEBLOCKPERDEPARTMENT';
      Index := QueryWork.FieldByName('TIMEBLOCK_NUMBER').AsInteger;
      if Index <= length(TB) then
        TB[Index] := True;
      QueryWork.Next;
    end;
    if Result = Nullstr then
    begin
      // TIMEBLOCKPERSHIFT
      QueryWork.Active := False;
      QueryWork.SQL.Clear;
      QueryWork.SQL.Add('SELECT TIMEBLOCK_NUMBER');
      QueryWork.SQL.Add('FROM TIMEBLOCKPERSHIFT');
      QueryWork.SQL.Add('WHERE PLANT_CODE=:PCODE AND SHIFT_NUMBER=:SHNO');
      QueryWork.Prepare;
      QueryWork.ParamByName('PCODE').AsString := Plant_Code;
      QueryWork.ParamByName('SHNO').AsInteger := Shift_Number;
      QueryWork.Active := True;
      while not QueryWork.Eof do
      begin
        Result := 'TIMEBLOCKPERSHIFT';
        Index := QueryWork.FieldByName('TIMEBLOCK_NUMBER').AsInteger;
        if Index <= length(TB) then
          TB[Index] := True;
        QueryWork.Next;
      end;
    end;
  end;
  QueryWork.Free;
end;
function WhatBreaks(AQuery: TQuery; Employee_number,Shift_Number: Integer;
  Plant_Code,Department_Code: string; Payed: Boolean): string;
var
  QueryWork: TQuery;
begin
  Result := NullStr;
  QueryWork := TQuery.Create(Nil);
  QueryCopy(AQuery,QueryWork);
  // BREAKPEREMPLOYEE
  QueryWork.Active := False;
  QueryWork.SQL.Clear;
  QueryWork.SQL.Add('SELECT PLANT_CODE');
  QueryWork.SQL.Add('FROM BREAKPEREMPLOYEE');
  QueryWork.SQL.Add('WHERE EMPLOYEE_NUMBER=:EMPNO AND PLANT_CODE=:PCODE');
  QueryWork.SQL.Add('AND SHIFT_NUMBER=:SHNO AND PAYED_YN=:PAYED_YN');
  QueryWork.Prepare;
  QueryWork.ParamByName('EMPNO').AsInteger := Employee_Number;
  QueryWork.ParamByName('PCODE').AsString := Plant_Code;
  QueryWork.ParamByName('SHNO').AsInteger := Shift_Number;
  if Payed then
    QueryWork.ParamByName('PAYED_YN').AsString := CHECKEDVALUE
  else
    QueryWork.ParamByName('PAYED_YN').AsString := UNCHECKEDVALUE;
  QueryWork.Active := True;
  if not QueryWork.IsEmpty then
    Result := 'BREAKPEREMPLOYEE'
  else
  begin
    // BREAKPERDEPARTMENT
    QueryWork.Active := False;
    QueryWork.SQL.Clear;
    QueryWork.SQL.Add('SELECT PLANT_CODE');
    QueryWork.SQL.Add('FROM BREAKPERDEPARTMENT');
    QueryWork.SQL.Add('WHERE DEPARTMENT_CODE=:DCODE AND PLANT_CODE=:PCODE');
    QueryWork.SQL.Add('AND SHIFT_NUMBER=:SHNO AND PAYED_YN=:PAYED_YN');
    QueryWork.Prepare;
    QueryWork.ParamByName('DCODE').AsString := Department_Code;
    QueryWork.ParamByName('PCODE').AsString := Plant_Code;
    QueryWork.ParamByName('SHNO').AsInteger := Shift_Number;
    if Payed then
      QueryWork.ParamByName('PAYED_YN').AsString := CHECKEDVALUE
    else
      QueryWork.ParamByName('PAYED_YN').AsString := UNCHECKEDVALUE;
    QueryWork.Active := True;
    if not QueryWork.IsEmpty then
      Result := 'BREAKPERDEPARTMENT'
    else
    begin
      // BREAKPERSHIFT
      QueryWork.Active := False;
      QueryWork.SQL.Clear;
      QueryWork.SQL.Add('SELECT PLANT_CODE');
      QueryWork.SQL.Add('FROM BREAKPERSHIFT');
      QueryWork.SQL.Add('WHERE PLANT_CODE=:PCODE');
      QueryWork.SQL.Add('AND SHIFT_NUMBER=:SHNO AND PAYED_YN=:PAYED_YN');
      QueryWork.Prepare;
      QueryWork.ParamByName('PCODE').AsString := Plant_Code;
      QueryWork.ParamByName('SHNO').AsInteger := Shift_Number;
      if Payed then
      	QueryWork.ParamByName('PAYED_YN').AsString := CHECKEDVALUE
      else
        QueryWork.ParamByName('PAYED_YN').AsString := UNCHECKEDVALUE;
      QueryWork.Active := True;
      if not QueryWork.IsEmpty then
        Result := 'BREAKPERSHIFT';
    end;
  end;
  QueryWork.Free;
end;
*)
(* only used in Pims.exe
procedure UpdateAbsenceTotalMode(AQuery: TQuery; EmployeeNumber,
  AbsenceYear: Integer;	Minutes: double; AbsenceTypeCode: Char);
var
  WorkQuery: TQuery;
  UpdateField: String;
  OldMinutes: Double;
begin
  case AbsenceTypeCode of
    'H':  UpdateField := 'USED_HOLIDAY_MINUTE';
    'I':  UpdateField := 'ILLNESS_MINUTE';
    'T':  UpdateField := 'USED_TFT_MINUTE';
    'W':  UpdateField := 'USED_WTR_MINUTE';
    else
      exit;
  end;
  WorkQuery := Tquery.Create(Nil);
  QueryCopy(AQuery,WorkQuery);
  WorkQuery.Active := False;
  WorkQuery.SQL.Clear;
  WorkQuery.SQL.Add(Format('SELECT %s',[UpdateField]));
  WorkQuery.SQL.Add('FROM ABSENCETOTAL');
  WorkQuery.SQL.Add('WHERE EMPLOYEE_NUMBER=:EMPNO AND');
  WorkQuery.SQL.Add('ABSENCE_YEAR=:AYEAR');
  WorkQuery.Prepare;
  WorkQuery.ParamByName('EMPNO').AsInteger := EmployeeNumber;
  WorkQuery.ParamByName('AYEAR').AsInteger := AbsenceYear;
  WorkQuery.Active := True;
  if not WorkQuery.IsEmpty then
  begin
    OldMinutes := WorkQuery.Fields[0].AsFloat;
    WorkQuery.Active := False;
    WorkQuery.SQL.Clear;
    WorkQuery.SQL.Add('UPDATE ABSENCETOTAL');
    WorkQuery.SQL.Add(Format('SET %s=:NEWMIN',[UpdateField]));
    WorkQuery.SQL.Add(', MUTATOR = :MUTATOR, MUTATIONDATE = :MUTATIONDATE');
    WorkQuery.SQL.Add('WHERE EMPLOYEE_NUMBER=:EMPNO AND');
    WorkQuery.SQL.Add('ABSENCE_YEAR=:AYEAR');
    WorkQuery.Prepare;
    WorkQuery.ParamByName('EMPNO').AsInteger := EmployeeNumber;
    WorkQuery.ParamByName('AYEAR').AsInteger := AbsenceYear;
    WorkQuery.ParamByName('NEWMIN').AsFloat := OldMinutes + Minutes;
    WorkQuery.ParamByName('MUTATOR').AsString :=  ORASystemDM.CurrentProgramUser;
    WorkQuery.ParamByName('MUTATIONDATE').AsDateTime := Now;
    WorkQuery.ExecSQL;
  end
  else
  begin
    WorkQuery.Active := False;
    WorkQuery.SQL.Clear;
    WorkQuery.SQL.Add('INSERT INTO ABSENCETOTAL(');
    WorkQuery.SQL.Add('EMPLOYEE_NUMBER,ABSENCE_YEAR,CREATIONDATE,MUTATIONDATE,');
    WorkQuery.SQL.Add(Format('MUTATOR,%s)',[UpdateField]));
    WorkQuery.SQL.Add('VALUES(:EMPNO,:AYEAR,:ADATE,:ADATE,:MUT,:NEWMIN)');
    WorkQuery.Prepare;
    WorkQuery.ParamByName('EMPNO').AsInteger := EmployeeNumber;
    WorkQuery.ParamByName('AYEAR').AsInteger := AbsenceYear;
    WorkQuery.ParamByName('ADATE').AsDateTime := Now;
    WorkQuery.ParamByName('MUT').AsString := ORASystemDM.CurrentProgramUser;
    WorkQuery.ParamByName('NEWMIN').AsFloat := Minutes;
    WorkQuery.ExecSQL;
  end;
  WorkQuery.Active := False;
  WorkQuery.free;
end;
*)
// MR:02-01-2003 Changed, also update record! Not only delete+insert new record.
procedure FillProdHourPerHourType(AQuery:Tquery; EmployeeData: TScannedIDCard;
  ProductionDate: TDateTime; HourType, Minutes: Integer; Manual: String;
  InsertProdHrs: Boolean);
var
  WorkQuery: TQuery;
  TotalMinutes: Integer;
  MyNow: TDateTime;
begin
  if (ORASystemDM.GetFillProdHourPerHourTypeYN = UNCHECKEDVALUE) then
    Exit;
  MyNow := Now;
  WorkQuery := TQuery.Create(Nil);
  QueryCopy(AQuery,WorkQuery);
  if InsertProdHrs then
  begin
    // Exists record?
    WorkQuery.Active := False;
    WorkQuery.SQL.Clear;
    WorkQuery.SQL.Add('SELECT PRODUCTION_MINUTE');
    WorkQuery.SQL.Add('FROM PRODHOURPEREMPLPERTYPE');
    WorkQuery.SQL.Add('WHERE PRODHOUREMPLOYEE_DATE=:PDATE AND');
    WorkQuery.SQL.Add('PLANT_CODE=:PCODE AND ');
    WorkQuery.SQL.Add('SHIFT_NUMBER=:SHNO AND ');
    WorkQuery.SQL.Add('EMPLOYEE_NUMBER=:EMPNO AND');
    WorkQuery.SQL.Add('WORKSPOT_CODE=:WCODE AND');
    WorkQuery.SQL.Add('JOB_CODE=:JCODE AND');
    WorkQuery.SQL.Add('MANUAL_YN=:MAN AND');
    WorkQuery.SQL.Add('HOURTYPE_NUMBER =:HourType');
    WorkQuery.Prepare;
    WorkQuery.ParamByName('PDATE').AsDateTime := ProductionDate;
    WorkQuery.ParamByName('PCODE').AsString := EmployeeData.PlantCode;
    WorkQuery.ParamByName('SHNO').AsInteger := EmployeeData.ShiftNumber;
    WorkQuery.ParamByName('EMPNO').AsInteger := EmployeeData.EmployeeCode;
    WorkQuery.ParamByName('WCODE').AsString := EmployeeData.WorkSpotCode;
    WorkQuery.ParamByName('JCODE').AsString := EmployeeData.JobCode;
    WorkQuery.ParamByName('MAN').AsString := Manual;
    WorkQuery.ParamByName('HOURTYPE').AsInteger := HourType;
    WorkQuery.Open;
    if WorkQuery.RecordCount > 0 then
    begin
      // Update record
      WorkQuery.First;
      TotalMinutes := WorkQuery.FieldByName('PRODUCTION_MINUTE').AsInteger +
        Minutes;
      WorkQuery.Active := False;
      WorkQuery.SQL.Clear;
      WorkQuery.SQL.Add('UPDATE PRODHOURPEREMPLPERTYPE');
      WorkQuery.SQL.Add('SET PRODUCTION_MINUTE = :PMIN,');
      WorkQuery.SQL.Add('MUTATIONDATE = :MDATE,');
      WorkQuery.SQL.Add('MUTATOR = :MUTATOR');
      WorkQuery.SQL.Add('WHERE PRODHOUREMPLOYEE_DATE=:PDATE AND');
      WorkQuery.SQL.Add('PLANT_CODE=:PCODE AND ');
      WorkQuery.SQL.Add('SHIFT_NUMBER=:SHNO AND ');
      WorkQuery.SQL.Add('EMPLOYEE_NUMBER=:EMPNO AND');
      WorkQuery.SQL.Add('WORKSPOT_CODE=:WCODE AND');
      WorkQuery.SQL.Add('JOB_CODE=:JCODE AND');
      WorkQuery.SQL.Add('MANUAL_YN=:MAN AND');
      WorkQuery.SQL.Add('HOURTYPE_NUMBER =:HourType');
      WorkQuery.Prepare;
      WorkQuery.ParamByName('PMIN').AsInteger := TotalMinutes;
      WorkQuery.ParamByName('MDATE').AsDateTime := MyNow;
      WorkQuery.ParamByName('MUTATOR').AsString := ORASystemDM.CurrentProgramUser;
      WorkQuery.ParamByName('PDATE').AsDateTime := ProductionDate;
      WorkQuery.ParamByName('PCODE').AsString := EmployeeData.PlantCode;
      WorkQuery.ParamByName('SHNO').AsInteger := EmployeeData.ShiftNumber;
      WorkQuery.ParamByName('EMPNO').AsInteger := EmployeeData.EmployeeCode;
      WorkQuery.ParamByName('WCODE').AsString := EmployeeData.WorkSpotCode;
      WorkQuery.ParamByName('JCODE').AsString := EmployeeData.JobCode;
      WorkQuery.ParamByName('MAN').AsString := Manual;
      WorkQuery.ParamByName('HOURTYPE').AsInteger := HourType;
      WorkQuery.ExecSQL;
    end
    else
    begin
      WorkQuery.Active := False;
      WorkQuery.SQL.Clear;
      WorkQuery.SQL.Add('INSERT INTO PRODHOURPEREMPLPERTYPE(');
      WorkQuery.SQL.Add('PRODHOUREMPLOYEE_DATE,PLANT_CODE,');
      WorkQuery.SQL.Add('SHIFT_NUMBER,EMPLOYEE_NUMBER,');
      WorkQuery.SQL.Add('WORKSPOT_CODE,JOB_CODE,');
      WorkQuery.SQL.Add('PRODUCTION_MINUTE, HOURTYPE_NUMBER, MANUAL_YN,');
      WorkQuery.SQL.Add('CREATIONDATE,MUTATIONDATE,MUTATOR)');
      WorkQuery.SQL.Add('VALUES(:PDATE,:PCODE,:SHNO,:EMPNO,');
      WorkQuery.SQL.Add(':WCODE,:JCODE,:PMIN,:HOURTYPE, :MAN,');
      WorkQuery.SQL.Add(':CDATE,:MDATE,:MUTATOR)');
      WorkQuery.Prepare;
      WorkQuery.ParamByName('PDATE').AsDateTime := ProductionDate;
      WorkQuery.ParamByName('PCODE').AsString := EmployeeData.PlantCode;
      WorkQuery.ParamByName('SHNO').AsInteger := EmployeeData.ShiftNumber;
      WorkQuery.ParamByName('EMPNO').AsInteger := EmployeeData.EmployeeCode;
      WorkQuery.ParamByName('WCODE').AsString := EmployeeData.WorkSpotCode;
      WorkQuery.ParamByName('JCODE').AsString := EmployeeData.JobCode;
      WorkQuery.ParamByName('PMIN').AsInteger := Minutes;
      WorkQuery.ParamByName('MAN').AsString := Manual;
      WorkQuery.ParamByName('HOURTYPE').AsInteger := HourType;
      WorkQuery.ParamByName('CDATE').AsDateTime := MyNow;
      WorkQuery.ParamByName('MDATE').AsDateTime := MyNow;
      WorkQuery.ParamByName('MUTATOR').AsString := ORASystemDM.CurrentProgramUser;
      WorkQuery.ExecSQL;
    end;
  end;
  // MR:13-10-2003 Free the WorkQuery
  WorkQuery.Active := False;
  WorkQuery.Free;
end;

function UpdateSalaryHour(AQuery: TQuery; SalaryDate: TDateTime;
  EmployeeData: TScannedIDCard;
  HourTypeNumber, Minutes: Integer; Manual: string;
  Replace: Boolean): Integer;
var
  WorkQuery: TQuery;
  TotMin: Integer;
  Employee_Number: Integer;
  InsertNewProdHrs: Boolean;
  MinuteNewProdHrs: Integer;
begin
  result := Minutes;
  Employee_Number := EmployeeData.EmployeeCode;
  if Replace and (Minutes = 0) then exit;
  InsertNewProdHrs := False;
  MinuteNewProdHrs := 0;
  WorkQuery := TQuery.Create(Nil);
  QueryCopy(AQuery,WorkQuery);
  WorkQuery.Active := False;
  WorkQuery.SQL.Clear;
  WorkQuery.SQL.Add(' SELECT * FROM SALARYHOURPEREMPLOYEE ');
  WorkQuery.SQL.Add(' WHERE SALARY_DATE=:SALARY_DATE AND ');
  WorkQuery.SQL.Add(' EMPLOYEE_NUMBER=:EMPLOYEE_NUMBER AND ');
  WorkQuery.SQL.Add(' HOURTYPE_NUMBER=:HOURTYPE_NUMBER AND');
  WorkQuery.SQL.Add(' MANUAL_YN=:MANUAL_YN ');
  WorkQuery.Prepare;
  WorkQuery.ParamByName('SALARY_DATE').AsDateTime := SalaryDate;
  WorkQuery.ParamByName('EMPLOYEE_NUMBER').AsInteger := Employee_Number;
  WorkQuery.ParamByName('HOURTYPE_NUMBER').AsInteger := HourTypenumber;
  WorkQuery.ParamByName('MANUAL_YN').AsString := Manual;
  WorkQuery.Active := True;
  if not WorkQuery.IsEmpty then // Update existing record
  begin
    TotMin := WorkQuery.FieldByName('SALARY_MINUTE').AsInteger;
    if (TotMin + Minutes) <> 0 then
    begin
      WorkQuery.Active := False;
      WorkQuery.SQL.Clear;
      WorkQuery.SQL.Add(' UPDATE SALARYHOURPEREMPLOYEE ');
      WorkQuery.SQL.Add(' SET SALARY_MINUTE=:SALARY_MINUTE, ');
//CAR 4-4-2003 Fill Mutator & MutationDate
      WorkQuery.SQL.Add(' MUTATIONDATE=:MDATE, ');
      WorkQuery.SQL.Add(' MUTATOR=:MUTATOR ');
//
      WorkQuery.SQL.Add(' WHERE SALARY_DATE=:SALARY_DATE AND ');
      WorkQuery.SQL.Add(' EMPLOYEE_NUMBER=:EMPLOYEE_NUMBER AND ');
      WorkQuery.SQL.Add(' HOURTYPE_NUMBER=:HOURTYPE_NUMBER AND ');
      WorkQuery.SQL.Add(' MANUAL_YN=:MANUAL_YN ');
      WorkQuery.Prepare;
      if Replace then
      begin
        WorkQuery.ParamByName('SALARY_MINUTE').AsInteger := Minutes;
        Result := Minutes - TotMin;
      end
      else
        WorkQuery.ParamByName('SALARY_MINUTE').AsInteger := TotMin + Minutes;
      WorkQuery.ParamByName('MDATE').AsDateTime := now;
      WorkQuery.ParamByName('MUTATOR').AsString := ORASystemDM.CurrentProgramUser;
      WorkQuery.ParamByName('SALARY_DATE').AsDateTime := SalaryDate;
      WorkQuery.ParamByName('EMPLOYEE_NUMBER').AsInteger := Employee_Number;
      WorkQuery.ParamByName('HOURTYPE_NUMBER').AsInteger := HourTypeNumber;
      WorkQuery.ParamByName('MANUAL_YN').AsString := Manual;
      WorkQuery.ExecSQL;
//
      InsertNewProdHrs := True;

      // MR:02-01-2003 Use 'minutes' instead of 'WorkQuery...'
      // This is also to prevent that a sum of minutes is used.
//      MinuteNewProdHrs := WorkQuery.ParamByName('SALARY_MINUTE').AsInteger;
      MinuteNewProdHrs := Minutes;

//
    end
    else
    begin
      WorkQuery.SQL.Clear;
      WorkQuery.SQL.Add(' DELETE FROM SALARYHOURPEREMPLOYEE ');
      WorkQuery.SQL.Add(' WHERE SALARY_DATE=:SALARY_DATE AND ');
      WorkQuery.SQL.Add(' EMPLOYEE_NUMBER=:EMPLOYEE_NUMBER AND ');
      WorkQuery.SQL.Add(' HOURTYPE_NUMBER=:HOURTYPE_NUMBER AND ');
      WorkQuery.SQL.Add(' MANUAL_YN=:MANUAL_YN ');
      WorkQuery.Prepare;
      WorkQuery.ParamByName('SALARY_DATE').AsDateTime := SalaryDate;
      WorkQuery.ParamByName('EMPLOYEE_NUMBER').AsInteger := Employee_Number;
      WorkQuery.ParamByName('HOURTYPE_NUMBER').AsInteger := HourTypeNumber;
      WorkQuery.ParamByName('MANUAL_YN').AsString := Manual;
      WorkQuery.ExecSQL;
      InsertNewProdHrs := False;
      MinuteNewProdHrs := 0;
    end;
  end
  else // Insert new record in SalaryHourerEmployee
  begin
    if Minutes <> 0 then
    begin
      WorkQuery.Active := False;
      WorkQuery.SQL.Clear;
      WorkQuery.SQL.Add('INSERT INTO SALARYHOURPEREMPLOYEE ');
      WorkQuery.SQL.Add('(SALARY_DATE,EMPLOYEE_NUMBER,HOURTYPE_NUMBER,');
      WorkQuery.SQL.Add('SALARY_MINUTE,MANUAL_YN,CREATIONDATE,MUTATIONDATE, ');
      WorkQuery.SQL.Add('EXPORTED_YN,MUTATOR) ');
      WorkQuery.SQL.Add('VALUES(:SALARY_DATE,:EMPLOYEE_NUMBER,:HOURTYPE_NUMBER,');
      WorkQuery.SQL.Add(':SALARY_MINUTE,:MANUAL_YN,:CREATIONDATE,:MUTATIONDATE,');
      WorkQuery.SQL.Add(':EXPORTED_YN,:MUTATOR)');
      WorkQuery.Prepare;
      WorkQuery.ParamByName('SALARY_DATE').AsDateTime := SalaryDate;
      WorkQuery.ParamByName('EMPLOYEE_NUMBER').AsInteger := Employee_Number;
      WorkQuery.ParamByName('HOURTYPE_NUMBER').AsInteger := HourTypeNumber;
      WorkQuery.ParamByName('SALARY_MINUTE').AsInteger := Minutes;
      WorkQuery.ParamByName('MANUAL_YN').AsString := Manual;
      WorkQuery.ParamByName('CREATIONDATE').AsDateTime := now;
      WorkQuery.ParamByName('MUTATIONDATE').AsDateTime := now;
      WorkQuery.ParamByName('EXPORTED_YN').AsString := UNCHECKEDVALUE;
      WorkQuery.ParamByName('MUTATOR').AsString := ORASystemDM.CurrentProgramUser;
      WorkQuery.ExecSql;
      result := Minutes;
// fill the extra table prodhours per hourtype
      InsertNewProdHrs := True;

      // MR:02-01-2003
      // Use Minutes instead of 'WorkQuery...'
//      MinuteNewProdHrs := WorkQuery.ParamByName('SALARY_MINUTE').AsInteger;
      MinuteNewProdHrs := Minutes;

//
    end;
  end;
  WorkQuery.Free;
  FillProdHourPerHourType(AQuery, EmployeeData, SalaryDate,
    HourTypeNumber, MinuteNewProdHrs, Manual, InsertNewProdHrs);
end;

procedure ContractRoundMinutes(AQuery: TQuery; AEmployeeIDCard: TScannedIDCard;
  BookingDay: TDateTime; var AProdMin: Integer);
var
  WorkQuery: TQuery;
  NewTotalMinutes, TotalMinutes: Integer;
  RoundTruncSalaryHours, RoundMinute: Integer;
  function RoundMinutes(RoundTruncSalaryHours, RoundMinute: Integer;
    Minutes: Integer): Integer;
  var
    Hours, Mins: Integer;
  begin
    Hours := Minutes DIV 60;
    Mins := Minutes MOD 60;
    if RoundTruncSalaryHours = 1 then // Round
      Mins := Round(Mins / RoundMinute) * RoundMinute
    else
      Mins := Trunc(Mins / RoundMinute) * RoundMinute;
    Result := Hours * 60 + Mins;
  end;
begin
  AProdMin := 0;
  WorkQuery := TQuery.Create(nil);
  QueryCopy(AQuery, WorkQuery);
  WorkQuery.Close;
  WorkQuery.SQL.Clear;
  WorkQuery.SQL.Add('SELECT SUM(SALARY_MINUTE) AS TOTALMINUTE');
  WorkQuery.SQL.Add('FROM SALARYHOURPEREMPLOYEE');
  WorkQuery.SQL.Add('WHERE SALARY_DATE = :SALARY_DATE AND');
  WorkQuery.SQL.Add('EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER');
  WorkQuery.ParamByName('SALARY_DATE').AsDateTime := BookingDay;
  WorkQuery.ParamByName('EMPLOYEE_NUMBER').AsInteger :=
    AEmployeeIDCard.EmployeeCode;
  WorkQuery.Open;
  if not WorkQuery.IsEmpty then
  begin
    WorkQuery.First;
    TotalMinutes := WorkQuery.FieldByName('TOTALMINUTE').AsInteger;
    AProdMinClass.ReadContractRoundMinutesParams(AEmployeeIDCard,
      RoundTruncSalaryHours, RoundMinute);
    NewTotalMinutes := RoundMinutes(RoundTruncSalaryHours, RoundMinute,
      TotalMinutes);
    AProdMin := NewTotalMinutes - TotalMinutes;
  end;
  WorkQuery.Active := False;
  WorkQuery.Free;
end;

procedure DeleteProdHourForAllTypes(AQuery:TQuery; EmployeeData: TScannedIDCard;
  FromDate, ToDate: TDateTime;  Manual: String);
var
  WorkQuery: TQuery;
begin
  if (ORASystemDM.GetFillProdHourPerHourTypeYN = UNCHECKEDVALUE) then
    Exit;
  WorkQuery := TQuery.Create(Nil);
  QueryCopy(AQuery,WorkQuery);
  WorkQuery.Active := False;
  WorkQuery.SQL.Clear;
  // MR:11-10-2003 Only select on 'Date' and 'Employee'!
  WorkQuery.SQL.Add('DELETE FROM PRODHOURPEREMPLPERTYPE ');
  WorkQuery.SQL.Add('WHERE PRODHOUREMPLOYEE_DATE>=:FROMDATE AND');
  WorkQuery.SQL.Add('PRODHOUREMPLOYEE_DATE<=:TODATE AND');
  WorkQuery.SQL.Add('EMPLOYEE_NUMBER=:EMPNO AND');
  WorkQuery.SQL.Add('MANUAL_YN=:MAN ');
  WorkQuery.Prepare;
  WorkQuery.ParamByName('FROMDATE').AsDateTime := Trunc(FromDate);
  WorkQuery.ParamByName('TODATE').AsDateTime := Trunc(ToDate);
  WorkQuery.ParamByName('EMPNO').AsInteger := EmployeeData.EmployeeCode;
  WorkQuery.ParamByName('MAN').AsString := Manual;
  WorkQuery.ExecSql;
  // MR:13-10-2003 Free the WorkQuery
  WorkQuery.Active := False;
  WorkQuery.Free;
end;

function UpdateProductionHour(AQuery: Tquery; ProductionDate: TDateTime;
  EmployeeData: TScannedIDCard; Minutes, PayedBreaks: Integer;
  Manual: string):Integer;
var
  WorkQuery: TQuery;
  OldProdMin, OldPayedBreaks: Integer;
  MyNow: TDateTime;
begin
//  Result := -1;
  MyNow := Now;
  WorkQuery := TQuery.Create(Nil);
  QueryCopy(AQuery,WorkQuery);
  WorkQuery.Active := False;
  WorkQuery.SQL.Clear;
  WorkQuery.SQL.Add('SELECT P.PRODUCTION_MINUTE,');
  WorkQuery.SQL.Add('P.PAYED_BREAK_MINUTE, S.HOURTYPE_NUMBER ');
  WorkQuery.SQL.Add('FROM PRODHOURPEREMPLOYEE P, SHIFT S WHERE ');
  WorkQuery.SQL.Add('P.PLANT_CODE=S.PLANT_CODE AND');
  WorkQuery.SQL.Add('P.SHIFT_NUMBER=S.SHIFT_NUMBER AND');
  WorkQuery.SQL.Add('P.PRODHOUREMPLOYEE_DATE=:PDATE AND');
  WorkQuery.SQL.Add('P.PLANT_CODE=:PCODE AND ');
  WorkQuery.SQL.Add('P.SHIFT_NUMBER=:SHNO AND ');
  WorkQuery.SQL.Add('P.EMPLOYEE_NUMBER=:EMPNO AND');
  WorkQuery.SQL.Add('P.WORKSPOT_CODE=:WCODE AND');
  WorkQuery.SQL.Add('P.JOB_CODE=:JCODE AND');
  WorkQuery.SQL.Add('P.MANUAL_YN=:MAN');
  WorkQuery.ParamByName('PDATE').AsDateTime := ProductionDate;
  WorkQuery.ParamByName('PCODE').AsString := EmployeeData.PlantCode;
  WorkQuery.ParamByName('SHNO').AsInteger := EmployeeData.ShiftNumber;
  WorkQuery.ParamByName('EMPNO').AsInteger := EmployeeData.EmployeeCode;
  WorkQuery.ParamByName('WCODE').AsString := EmployeeData.WorkSpotCode;
  WorkQuery.ParamByName('JCODE').AsString := EmployeeData.JobCode;
  WorkQuery.ParamByName('MAN').AsString := Manual;
  WorkQuery.Active := True;
  if not WorkQuery.IsEmpty then
  begin
    OldProdMin :=
      WorkQuery.FieldByName('PRODUCTION_MINUTE').AsInteger;
    OldPayedBreaks := WorkQuery.FieldByName('PAYED_BREAK_MINUTE').AsInteger;
    if (OldProdMin + Minutes) <= 0 then
    begin
      WorkQuery.Active := False;
      WorkQuery.SQL.Clear;
      WorkQuery.SQL.Add('DELETE FROM PRODHOURPEREMPLOYEE ');
      WorkQuery.SQL.Add('WHERE PRODHOUREMPLOYEE_DATE=:PDATE AND');
      WorkQuery.SQL.Add('PLANT_CODE=:PCODE AND ');
      WorkQuery.SQL.Add('SHIFT_NUMBER=:SHNO AND ');
      WorkQuery.SQL.Add('EMPLOYEE_NUMBER=:EMPNO AND');
      WorkQuery.SQL.Add('WORKSPOT_CODE=:WCODE AND');
      WorkQuery.SQL.Add('JOB_CODE=:JCODE AND');
      WorkQuery.SQL.Add('MANUAL_YN=:MAN');
      WorkQuery.ParamByName('PDATE').AsDateTime := ProductionDate;
      WorkQuery.ParamByName('PCODE').AsString := EmployeeData.PlantCode;
      WorkQuery.ParamByName('SHNO').AsInteger := EmployeeData.ShiftNumber;
      WorkQuery.ParamByName('EMPNO').AsInteger := EmployeeData.EmployeeCode;
      WorkQuery.ParamByName('WCODE').AsString := EmployeeData.WorkSpotCode;
      WorkQuery.ParamByName('JCODE').AsString := EmployeeData.JobCode;
      WorkQuery.ParamByName('MAN').AsString := Manual;
      WorkQuery.ExecSql;
// delete all prodhourperemplperalltypes is were inserted before in extra table
      DeleteProdHourForAllTypes(AQuery, EmployeeData, ProductionDate,
        ProductionDate,  Manual);
    end
    else
    begin
      WorkQuery.Active := False;
      WorkQuery.SQL.Clear;
      WorkQuery.SQL.Add('UPDATE PRODHOURPEREMPLOYEE ');
      WorkQuery.SQL.Add('SET PRODUCTION_MINUTE=:PMIN,');
      WorkQuery.SQL.Add('PAYED_BREAK_MINUTE=:PAIDBREAK,');
      WorkQuery.SQL.Add('MUTATOR=:MUTATOR, MUTATIONDATE =:MUTATIONDATE');
      WorkQuery.SQL.Add('WHERE PRODHOUREMPLOYEE_DATE=:PDATE AND');
      WorkQuery.SQL.Add('PLANT_CODE=:PCODE AND ');
      WorkQuery.SQL.Add('SHIFT_NUMBER=:SHNO AND ');
      WorkQuery.SQL.Add('EMPLOYEE_NUMBER=:EMPNO AND');
      WorkQuery.SQL.Add('WORKSPOT_CODE=:WCODE AND');
      WorkQuery.SQL.Add('JOB_CODE=:JCODE AND');
      WorkQuery.SQL.Add('MANUAL_YN=:MAN ');
      WorkQuery.ParamByName('PMIN').AsInteger := OldProdMin + Minutes;
      WorkQuery.ParamByName('PAIDBREAK').AsInteger := PayedBreaks + OldPayedBreaks;
      WorkQuery.ParamByName('PDATE').AsDateTime := ProductionDate;
      WorkQuery.ParamByName('PCODE').AsString := EmployeeData.PlantCode;
      WorkQuery.ParamByName('SHNO').AsInteger := EmployeeData.ShiftNumber;
      WorkQuery.ParamByName('EMPNO').AsInteger := EmployeeData.EmployeeCode;
      WorkQuery.ParamByName('WCODE').AsString := EmployeeData.WorkSpotCode;
      WorkQuery.ParamByName('JCODE').AsString := EmployeeData.JobCode;
      WorkQuery.ParamByName('MAN').AsString := Manual;
      WorkQuery.ParamByName('MUTATOR').AsString := ORASystemDM.CurrentProgramUser;
      WorkQuery.ParamByName('MUTATIONDATE').AsDateTime := MyNow;
      WorkQuery.ExecSql;
    end;
    Result := 0;
  end
  else
  begin
    WorkQuery.Active := False;
    WorkQuery.SQL.Clear;
    WorkQuery.SQL.Add('INSERT INTO PRODHOURPEREMPLOYEE(');
    WorkQuery.SQL.Add('PRODHOUREMPLOYEE_DATE,PLANT_CODE,');
    WorkQuery.SQL.Add('SHIFT_NUMBER,EMPLOYEE_NUMBER,');
    WorkQuery.SQL.Add('WORKSPOT_CODE,JOB_CODE,');
    WorkQuery.SQL.Add('PRODUCTION_MINUTE,');
    WorkQuery.SQL.Add('MANUAL_YN,PAYED_BREAK_MINUTE,');
    WorkQuery.SQL.Add('CREATIONDATE,MUTATIONDATE,MUTATOR)');
    WorkQuery.SQL.Add('VALUES(:PDATE,:PCODE,:SHNO,:EMPNO,');
    WorkQuery.SQL.Add(':WCODE,:JCODE,:PMIN,:MAN,:PAYEDBREAK,');
    WorkQuery.SQL.Add(':CDATE,:MDATE,:MUTATOR)');
    WorkQuery.ParamByName('PDATE').AsDateTime := ProductionDate;
    WorkQuery.ParamByName('PCODE').AsString := EmployeeData.PlantCode;
    WorkQuery.ParamByName('SHNO').AsInteger := EmployeeData.ShiftNumber;
    WorkQuery.ParamByName('EMPNO').AsInteger := EmployeeData.EmployeeCode;
    WorkQuery.ParamByName('WCODE').AsString := EmployeeData.WorkSpotCode;
    WorkQuery.ParamByName('JCODE').AsString := EmployeeData.JobCode;
    WorkQuery.ParamByName('PMIN').AsInteger := Minutes;
    WorkQuery.ParamByName('MAN').AsString := Manual;
    WorkQuery.ParamByName('PAYEDBREAK').AsInteger := PayedBreaks;
    WorkQuery.ParamByName('CDATE').AsDateTime := MyNow;
    WorkQuery.ParamByName('MDATE').AsDateTime := MyNow;
    WorkQuery.ParamByName('MUTATOR').AsString := ORASystemDM.CurrentProgramUser;
    WorkQuery.ExecSQL;
    Result := 1;
  end;
  if Minutes >= 60 then
  begin
  // Changing the Employee level to C
    WorkQuery.Close;
    WorkQuery.SQL.Clear;
    WorkQuery.SQL.Add('SELECT EMPLOYEE_LEVEL FROM WORKSPOTSPEREMPLOYEE');
    WorkQuery.SQL.Add('WHERE EMPLOYEE_NUMBER=:EMPNO AND PLANT_CODE=:PCODE AND');
    WorkQuery.SQL.Add('DEPARTMENT_CODE=:DCODE AND WORKSPOT_CODE=:WCODE');
    WorkQuery.ParamByName('EMPNO').AsInteger := EmployeeData.EmployeeCode;
    WorkQuery.ParamByName('PCODE').AsString := EmployeeData.PlantCode;
    WorkQuery.ParamByName('DCODE').AsString := EmployeeData.DepartmentCode;
    WorkQuery.ParamByName('WCODE').AsString := EmployeeData.WorkSpotCode;
    WorkQuery.Active := True;
    if (not WorkQuery.IsEmpty) then
    begin
      if WorkQuery.FieldByName('EMPLOYEE_LEVEL').IsNull then
      begin
        WorkQuery.Close;
        WorkQuery.SQL.Clear;
        WorkQuery.SQL.Add('UPDATE WORKSPOTSPEREMPLOYEE');
        WorkQuery.SQL.Add('SET EMPLOYEE_LEVEL=:EMPLEVEL,');
        WorkQuery.SQL.Add('STARTDATE_LEVEL=:STLDATE, MUTATOR=:MUT,');
        WorkQuery.SQL.Add('MUTATIONDATE=:MUTDATE');
        WorkQuery.SQL.Add('WHERE EMPLOYEE_NUMBER=:EMPNO AND ');
        WorkQuery.SQL.Add('PLANT_CODE=:PCODE AND');
        WorkQuery.SQL.Add('DEPARTMENT_CODE=:DCODE AND WORKSPOT_CODE=:WCODE');
        WorkQuery.ParamByName('EMPLEVEL').AsString := LevelC;
        WorkQuery.ParamByName('STLDATE').AsDateTime := MyNow;
        WorkQuery.ParamByName('MUT').AsString := ORASystemDM.CurrentProgramUser;
        WorkQuery.ParamByName('MUTDATE').AsDateTime := MyNow;
        WorkQuery.ParamByName('EMPNO').AsInteger := EmployeeData.EmployeeCode;
        WorkQuery.ParamByName('PCODE').AsString := EmployeeData.PlantCode;
        WorkQuery.ParamByName('DCODE').AsString := EmployeeData.DepartmentCode;
        WorkQuery.ParamByName('WCODE').AsString := EmployeeData.WorkSpotCode;
        WorkQuery.ExecSQL;
      end;
    end
    else
    begin
      WorkQuery.Close;
      WorkQuery.SQL.Clear;
      WorkQuery.SQL.Add('INSERT INTO WORKSPOTSPEREMPLOYEE(EMPLOYEE_NUMBER,');
      WorkQuery.SQL.Add('PLANT_CODE, EMPLOYEE_LEVEL, DEPARTMENT_CODE,');
      WorkQuery.SQL.Add('CREATIONDATE, WORKSPOT_CODE, MUTATIONDATE, MUTATOR,');
      WorkQuery.SQL.Add('STARTDATE_LEVEL) VALUES(');
      WorkQuery.SQL.Add(':EMPLNO, :PCODE, :EMPLEVEL, :DCODE, :CDATE, :WCODE,');
      WorkQuery.SQL.Add(':MDATE, :MUT, :STLEVDATE)');
      WorkQuery.ParamByName('EMPLNO').AsInteger := EmployeeData.EmployeeCode;
      WorkQuery.ParamByName('PCODE').AsString := EmployeeData.PlantCode;
      WorkQuery.ParamByName('EMPLEVEL').AsString := LevelC;
      WorkQuery.ParamByName('DCODE').AsString := EmployeeData.DepartmentCode;
      WorkQuery.ParamByName('CDATE').AsDateTime := MyNow;
      WorkQuery.ParamByName('WCODE').AsString := EmployeeData.WorkSpotCode;
      WorkQuery.ParamByName('MDATE').AsDateTime := MyNow;
      WorkQuery.ParamByName('MUT').AsString := ORASystemDM.CurrentProgramUser;
      WorkQuery.ParamByName('STLEVDATE').AsDateTime := MyNow;
      WorkQuery.ExecSQL;
    end;
  end;
  WorkQuery.Active := False;
  WorkQuery.Free;
end;

function UpdateAbsenceTotal(AQuery: TQuery;BookingDay: TDateTime;
  EmployeeData: TScannedIDCard; Minutes: double): Integer;
var
  TotOverMinQuery: TQuery;
  EarnedTime: double;
  year, month, day: word;
begin
//  result := -1;
  EarnedTime := Minutes;
  TotOverMinQuery := TQuery.Create(Nil);
  QueryCopy(AQuery,TotOverMinQuery);
  DecodeDate(BookingDay,year,month,day);
  TotOverMinQuery.Active := False;
  TotOverMinQuery.SQL.Clear;
  TotOverMinQuery.SQL.Add('SELECT EARNED_TFT_MINUTE FROM ABSENCETOTAL');
  TotOverMinQuery.SQL.Add('WHERE EMPLOYEE_NUMBER=:EMPNO AND ');
  TotOverMinQuery.SQL.Add('ABSENCE_YEAR=:AYEAR');
  TotOverMinQuery.Prepare;
  TotOverMinQuery.ParamByName('EMPNO').AsInteger :=
    EmployeeData.EmployeeCode;
  TotOverMinQuery.ParamByName('AYEAR').AsInteger := year;
  TotOverMinQuery.Active := True;
  if not TotOverMinQuery.IsEmpty then
  begin
    EarnedTime := EarnedTime +
      TotOverMinQuery.FieldByName('EARNED_TFT_MINUTE').AsFloat;
    TotOverMinQuery.Active := False;
    TotOverMinQuery.SQL.Clear;
    TotOverMinQuery.SQL.Add('UPDATE ABSENCETOTAL');
    TotOverMinQuery.SQL.Add('SET EARNED_TFT_MINUTE=:OTIME,');
    TotOverMinQuery.SQL.Add('MUTATIONDATE=:MDATE, MUTATOR=:MUTATOR');
    TotOverMinQuery.SQL.Add('WHERE EMPLOYEE_NUMBER=:EMPNO AND ');
    TotOverMinQuery.SQL.Add('ABSENCE_YEAR=:AYEAR');
    TotOverMinQuery.Prepare;
    TotOverMinQuery.ParamByName('OTIME').AsFloat := EarnedTime;
    TotOverMinQuery.ParamByName('MDATE').AsDateTime := Now;
    TotOverMinQuery.ParamByName('MUTATOR').AsString :=
      ORASystemDM.CurrentProgramUser;
    TotOverMinQuery.ParamByName('EMPNO').AsInteger :=
      EmployeeData.EmployeeCode;
    TotOverMinQuery.ParamByName('AYEAR').AsInteger := year;
    TotOverMinQuery.ExecSQL;
    result := 0;
  end
  else
  begin
    TotOverMinQuery.Active := False;
    TotOverMinQuery.SQL.Clear;
    TotOverMinQuery.SQL.Add('INSERT INTO ABSENCETOTAL(');
    TotOverMinQuery.SQL.Add('EMPLOYEE_NUMBER,ABSENCE_YEAR,');
    TotOverMinQuery.SQL.Add('EARNED_TFT_MINUTE,CREATIONDATE,');
    TotOverMinQuery.SQL.Add('MUTATIONDATE,MUTATOR) VALUES(');
    TotOverMinQuery.SQL.Add(':EMPNO,:AYEAR,:OTIME,:CDATE,:MDATE,');
    TotOverMinQuery.SQL.Add(':MUTATOR)');
    TotOverMinQuery.Prepare;
    TotOverMinQuery.ParamByName('EMPNO').AsInteger := EmployeeData.EmployeeCode;
    TotOverMinQuery.ParamByName('AYEAR').AsInteger := year;
    TotOverMinQuery.ParamByName('OTIME').AsFloat    := EarnedTime;
    TotOverMinQuery.ParamByName('CDATE').AsDateTime := Now;
    TotOverMinQuery.ParamByName('MDATE').AsDateTime := Now;
    TotOverMinQuery.ParamByName('MUTATOR').AsString :=
      ORASystemDM.CurrentProgramUser;
    TotOverMinQuery.ExecSQL;
    result := 1;
  end;
  TotOverMinQuery.Active := False;
  TotOverMinQuery.Free;
end;

function DetermineExceptionalBeforeOvertime(AQuery: TQuery;
  EmployeeData: TScannedIDCard): Boolean;
var
  WorkQuery: TQuery;
begin
  Result := False;
  WorkQuery := TQuery.Create(Nil);
  QueryCopy(AQuery,WorkQuery);
  try
    WorkQuery.Active := False;
    WorkQuery.SQL.Clear;
    WorkQuery.SQL.Add('SELECT EXCEPTIONAL_BEFORE_OVERTIME');
    WorkQuery.SQL.Add('FROM EMPLOYEE E, CONTRACTGROUP C');
    WorkQuery.SQL.Add('WHERE E.CONTRACTGROUP_CODE = C.CONTRACTGROUP_CODE AND');
    WorkQuery.SQL.Add('E.EMPLOYEE_NUMBER=:EMPNO');
    WorkQuery.ParamByName('EMPNO').AsInteger := EmployeeData.EmployeeCode;
    WorkQuery.Prepare;
    WorkQuery.Active := True;
    if WorkQuery.RecordCount > 0 then
    begin
      WorkQuery.First;
      try
        if WorkQuery.FieldByName('EXCEPTIONAL_BEFORE_OVERTIME').AsString <> '' then
          if WorkQuery.FieldByName('EXCEPTIONAL_BEFORE_OVERTIME').AsString = 'Y' then
            Result := True;
      except
        // not found?
      end;
    end;
  finally
    WorkQuery.Free;
  end;
end;

function BooksExceptionalTime(AQuery: TQuery; BookingDay: TDateTime;
  ExceptRegularMin: Integer; EmployeeData: TScannedIDcard;
  Manual: string): Integer;
var
  WorkQuery, InsertQuery: TQuery;
  StartTime, EndTime, STime, ETime: TDateTime;
  HourType, ProdMin, BreaksMin, PayedBreaks, IntersectionMin: Integer;
  TempEndTime: TDateTime;
  PreviousBreaksTotal, BreaksTotal: Integer;
  SelectStr: String;
  DayOfWeek: Integer;
begin
  // MR:03-12-2003 Use 'cutoff'-datein-time
  StartTime := EmployeeData.DateInCutOff;
  EndTime := StartTime + ExceptRegularMin/DayToMin;
  // MR:16-06-2003 Start
  // Calculate the complete not-payed-break in case the
  // break is partly within the StartTime and EndTime.
  TempEndTime := EndTime;
  PreviousBreaksTotal := 0;
  while (True) do
  begin
    // MR: 25-09-2003 - See: CalculateTotalHoursDMT
    AProdMinClass.ComputeBreaks(EmployeeData, EmployeeData.DateIn, TempEndTime,
      1, ProdMin, BreaksMin, PayedBreaks);
    BreaksTotal := (Breaksmin - PayedBreaks);
    if (BreaksTotal = PreviousBreaksTotal) then
      Break;
    TempEndTime := TempEndTime + (1 / 60 / 24); // add 1 minute
    PreviousBreaksTotal := BreaksTotal;
  end;
  EndTime := EndTime + (Breaksmin - PayedBreaks)/DayToMin; //Unpaid Breaks.
  // MR:15-09-2004 RoundTime is a function, so 'endtime' will never
  // be rounded + also second argument should be 1 to round to minutes.
  Result := ExceptRegularMin;
  WorkQuery := TQuery.Create(Nil);
  QueryCopy(AQuery,WorkQuery);
  InsertQuery := TQuery.Create(Nil);
  QueryCopy(AQuery,InsertQuery);
  WorkQuery.Active := False;
  WorkQuery.SQL.Clear;
  SelectStr :=
    'SELECT DISTINCT C.DAY_OF_WEEK, C.STARTTIME, C.ENDTIME, ' +
    'C.HOURTYPE_NUMBER, C.CONTRACTGROUP_CODE ' +
     'FROM EMPLOYEE A, CONTRACTGROUP B, EXCEPTIONALHOURDEF C ' +
     'WHERE (A.CONTRACTGROUP_CODE = B.CONTRACTGROUP_CODE) AND ' +
     '(B.CONTRACTGROUP_CODE = C.CONTRACTGROUP_CODE) AND ';
  // MR:05-11-2003 Is Start- and End-time on same day?
  if Trunc(StartTime) = Trunc(EndTime) then
    SelectStr := SelectStr + '(C.DAY_OF_WEEK = :WEEKDAY) AND '
  else
    SelectStr := SelectStr + '((C.DAY_OF_WEEK = :WEEKDAYSTART) ' +
      'OR (C.DAY_OF_WEEK = :WEEKDAYEND)) AND ';
  SelectStr := SelectStr + '(A.EMPLOYEE_NUMBER = :EMPNO) ';
  WorkQuery.SQL.Add(SelectStr);
  WorkQuery.ParamByName('EMPNO').AsInteger := EmployeeData.EmployeeCode;
  if Trunc(StartTime) = Trunc(EndTime) then
    WorkQuery.ParamByName('WEEKDAY').AsInteger :=
      DayInWeek(ORASystemDM.WeekStartsOn, StartTime)
  else
  begin
    WorkQuery.ParamByName('WEEKDAYSTART').AsInteger :=
      DayInWeek(ORASystemDM.WeekStartsOn, StartTime);
    WorkQuery.ParamByName('WEEKDAYEND').AsInteger :=
      DayInWeek(ORASystemDM.WeekStartsOn, EndTime);
  end;
  WorkQuery.Prepare;
  WorkQuery.Active := True;
  WorkQuery.First;
  while not WorkQuery.Eof do
  begin
    DayOfWeek := WorkQuery.FieldByName('DAY_OF_WEEK').AsInteger;
    HourType := WorkQuery.FieldByName('HOURTYPE_NUMBER').AsInteger;
    STime := WorkQuery.FieldByName('STARTTIME').AsDateTime;
    ETime := WorkQuery.FieldByName('ENDTIME').AsDateTime;
    // MR:05-11-2003
    // Because a scan can go from one day to another, here
    // we check which day we must use for comparing.
    if DayOfWeek = DayInWeek(ORASystemDM.WeekStartsOn, StartTime) then
    begin
      ReplaceDate(STime, StartTime);
      ReplaceDate(ETime, StartTime);
    end
    else
    begin
      ReplaceDate(STime, EndTime);
      ReplaceDate(ETime, EndTime);
    end;
    // MR:07-11-2003 Correct midnight-enddate
    ETime := MidnightCorrection(STime, ETime);
    IntersectionMin := ComputeMinutesOfIntersection(StartTime, EndTime,
      STime, ETime);
    if IntersectionMin > 0 then
    begin
      // MR:13-06-2003 Start
      if STime < StartTime then
        STime := StartTime;
      if ETime > EndTime then
        ETime := EndTime;
      if STime > ETime then
        STime := ETime;
      // MR:13-06-2003 End
      // Compute the UnPayed breaks min in interval
      // MR: 25-09-2003
      AProdMinClass.ComputeBreaks(Employeedata, STime, ETime, 0, ProdMin,
        BreaksMin, PayedBreaks);
      IntersectionMin := IntersectionMin - BreaksMin + PayedBreaks;
      if IntersectionMin > 0 then
      begin
        UpdateSalaryHour(InsertQuery,BookingDay,EmployeeData,
          HourType,IntersectionMin,Manual,False);
        // MR:12-06-2003 Included in the 'if'.
        //               To prevent miscalculation if 'IntersectionMin' is
        //               negative.
        Result := Result - IntersectionMin;
      end;
    end;
    WorkQuery.Next;
  end;
  WorkQuery.Active := False;
  WorkQuery.Free;
  InsertQuery.Active := False;
  InsertQuery.Free;
end;

// MR:19-11-2002
// Book hours for shift, if shift has a hourtype
procedure BooksHoursForShift(AQuery: TQuery; BookingDay: TDateTime;
  EmployeeIDCard: TScannedIDCard; ProdMinPlusPayedBreaks: Integer;
  Manual: String);
var
  QueryWork: TQuery;
  HourtypeNumber: Integer;
begin
  if ProdMinPlusPayedBreaks > 0 then
  begin
    QueryWork := TQuery.Create(Nil);
    QueryCopy(AQuery, QueryWork);
    QueryWork.SQL.Clear;
    QueryWork.SQL.Add('SELECT HOURTYPE_NUMBER ');
    QueryWork.SQL.Add('FROM SHIFT ');
    QueryWork.SQL.Add('WHERE PLANT_CODE = :PLANT_CODE AND ');
    QueryWork.SQL.Add('SHIFT_NUMBER = :SHIFT_NUMBER');
    QueryWork.ParamByName('PLANT_CODE').AsString :=
      EmployeeIDCard.PlantCode;
    QueryWork.ParamByName('SHIFT_NUMBER').AsInteger :=
      EmployeeIDCard.ShiftNumber;
    QueryWork.Prepare;
    QueryWork.Open;
    if QueryWork.RecordCount > 0 then
    begin
      QueryWork.First;
      if QueryWork.FieldByName('HOURTYPE_NUMBER').AsString <> '' then
      begin
        HourtypeNumber := QueryWork.FieldByName('HOURTYPE_NUMBER').Value;
        UpdateSalaryHour(AQuery, BookingDay,
          EmployeeIDCard{.EmployeeCode}, HourtypeNumber,
          ProdMinPlusPayedBreaks, Manual, False { Don't replace! } );
      end;
    end;
    QueryWork.Free;
  end;
end;

function BooksOverTime(AQuery: TQuery; BookingDay: TdateTime;
  WorkedMin: Integer; EmployeeData: TScannedIDcard;
  Manual: String;
  BookExceptionalBeforeOvertime, RoundMinutes: Boolean): Integer;
var
  WorkQuery, TotOverMinQuery: TQuery;
  StartMin,EndMin,HourType: Integer;
  EarnedMin, RegisteredMin, OverTimeMin, {OverMin,} TotOverTime: Integer;
  StartDate, EndDate: TDateTime;
  EarnedTime, Percentage: Double;
  TotalWorkedMin: Integer;
begin
  WorkQuery := TQuery.Create(Nil);
  QueryCopy(AQuery,WorkQuery);
  TotOverMinQuery := TQuery.Create(Nil);
  QueryCopy(AQuery,TotOverMinQuery);
  RegisteredMin := WorkedMin;
  TotalWorkedMin := WorkedMin;
  TotOverTime := 0;
  EarnedTime := 0;
  //EarnedMin:=0;
  // MR:30-09-2003
  StartDate := BookingDay;
  EndDate := BookingDay;

  ComputeOvertimePeriod(StartDate, EndDate, AQuery, EmployeeData);
  // SALARYHOURPEREMPLOYEE
  // MR:20-11-2002 Also select on IGNORE_FOR_OVERTIME_YN for 'Hourtype on Shift'
  TotOverMinQuery.Active := False;
  TotOverMinQuery.SQL.Clear;
  TotOverMinQuery.SQL.Add('SELECT SUM(SALARY_MINUTE) ');
  TotOverMinQuery.SQL.Add('FROM SALARYHOURPEREMPLOYEE S, HOURTYPE H ');
  TotOverMinQuery.SQL.Add('WHERE ');
  TotOverMinQuery.SQL.Add('S.HOURTYPE_NUMBER=H.HOURTYPE_NUMBER AND ');
  TotOverMinQuery.SQL.Add('SALARY_DATE>=:STARTDATE AND SALARY_DATE<=:ENDDATE');
  TotOverMinQuery.SQL.Add('AND EMPLOYEE_NUMBER=:EMPNO AND');
  TotOverMinQuery.SQL.Add('H.IGNORE_FOR_OVERTIME_YN=:IGNORE_FOR_OVERTIME_YN ');
  TotOverMinQuery.Prepare;
  TotOverMinQuery.ParamByName('STARTDATE').AsDateTime := StartDate;
  TotOverMinQuery.ParamByName('ENDDATE').AsDateTime := EndDate;
  TotOverMinQuery.ParamByName('EMPNO').AsInteger :=
    EmployeeData.EmployeeCode;
  TotOverMinQuery.ParamByName('IGNORE_FOR_OVERTIME_YN').AsString :=
    UNCHECKEDVALUE;
  TotOverMinQuery.Active := True;
  RegisteredMin := RegisteredMin + TotOverMinQuery.Fields[0].AsInteger;
  // MR:20-11-2002 Also select on IGNORE_FOR_OVERTIME_YN for 'Hourtype on Shift'
  TotOverMinQuery.Active := False;
  TotOverMinQuery.SQL.Clear;
  TotOverMinQuery.SQL.Add('SELECT SUM(ABSENCE_MINUTE) ');
  TotOverMinQuery.SQL.Add('FROM ABSENCEHOURPEREMPLOYEE AE, ');
  TotOverMinQuery.SQL.Add('ABSENCEREASON AR, HOURTYPE H ');
  TotOverMinQuery.SQL.Add('WHERE ');
  TotOverMinQuery.SQL.Add('AE.ABSENCEREASON_ID=AR.ABSENCEREASON_ID AND ');
  TotOverMinQuery.SQL.Add('AR.HOURTYPE_NUMBER=H.HOURTYPE_NUMBER AND ');
  TotOverMinQuery.SQL.Add('ABSENCEHOUR_DATE>=:STARTDATE AND ');
  TotOverMinQuery.SQL.Add('ABSENCEHOUR_DATE<=:ENDDATE AND ');
  TotOverMinQuery.SQL.Add('H.IGNORE_FOR_OVERTIME_YN=:IGNORE_FOR_OVERTIME_YN AND ');
  TotOverMinQuery.SQL.Add('EMPLOYEE_NUMBER=:EMPNO');
  TotOverMinQuery.Prepare;
  TotOverMinQuery.ParamByName('STARTDATE').AsDateTime := StartDate;
  TotOverMinQuery.ParamByName('ENDDATE').AsDateTime := EndDate;
  TotOverMinQuery.ParamByName('EMPNO').AsInteger :=
    EmployeeData.EmployeeCode;
  TotOverMinQuery.ParamByName('IGNORE_FOR_OVERTIME_YN').AsString :=
    UNCHECKEDVALUE;
  TotOverMinQuery.Active := True;
  RegisteredMin := RegisteredMin + TotOverMinQuery.Fields[0].AsInteger;

  WorkQuery.Active := False;
  WorkQuery.SQL.Clear;
  WorkQuery.SQL.Add(' SELECT B.TIME_FOR_TIME_YN,B.BONUS_IN_MONEY_YN, ');
  WorkQuery.SQL.Add(' C.HOURTYPE_NUMBER,C.STARTTIME,C.ENDTIME, ');
  WorkQuery.SQL.Add(' D.BONUS_PERCENTAGE');
  WorkQuery.SQL.Add(' FROM EMPLOYEE A, CONTRACTGROUP B, ');
  WorkQuery.SQL.Add(' OVERTIMEDEFINITION C, HOURTYPE D ');
  WorkQuery.SQL.Add(' WHERE A.EMPLOYEE_NUMBER=:EMPNO AND ');
  WorkQuery.SQL.Add(' A.CONTRACTGROUP_CODE=B.CONTRACTGROUP_CODE AND ');
  WorkQuery.SQL.Add(' B.CONTRACTGROUP_CODE=C.CONTRACTGROUP_CODE AND');
  WorkQuery.SQL.Add(' C.HOURTYPE_NUMBER=D.HOURTYPE_NUMBER ');
  WorkQuery.SQL.Add(' ORDER BY LINE_NUMBER ');
  WorkQuery.ParamByName('EMPNO').AsInteger := EmployeeData.EmployeeCode;
  WorkQuery.Prepare;
  WorkQuery.Active := True;
  While not WorkQuery.Eof do
  begin
    StartMin := WorkQuery.FieldByName('STARTTIME').AsInteger;
    EndMin := WorkQuery.FieldByName('ENDTIME').AsInteger;
    HourType := WorkQuery.FieldByName('HOURTYPE_NUMBER').AsInteger;
    OverTimeMin := Min(EndMin, RegisteredMin) -
      Max(StartMin, (RegisteredMin - WorkedMin));
    if (OverTimeMin < 0) then
      OverTimeMin := 0;
    if StartMin >= RegisteredMin then
    begin
      WorkQuery.Last;
    end
// MR:13-06-2003 End
    else
    begin
      TotOverTime := TotOverTime + OverTimeMin;
      // Decrease to left minutes
      if BookExceptionalBeforeOvertime then
      begin
        if OverTimeMin > TotalWorkedMin then
          OverTimeMin := TotalWorkedMin;
        TotalWorkedMin := TotalWorkedMin - OverTimeMin;
        if TotalWorkedMin < 0 then
          TotalWorkedMin := 0;
      end;

      if OverTimeMin > 0 then
      begin
        EarnedMin := UpdateSalaryHour(TotOverMinQuery, BookingDay,
      	  EmployeeData{.EmployeeCode},
          HourType, OverTimeMin, Manual, False { MR:25-11-2002 Was True: Set to False }
          );
      end
      else
        EarnedMin := 0;
      // Time For Time;
      if WorkQuery.FieldByName('TIME_FOR_TIME_YN').AsString=CHECKEDVALUE then
      begin
        Percentage := WorkQuery.FieldByName('BONUS_PERCENTAGE').AsFloat;
        if WorkQuery.FieldByName('BONUS_IN_MONEY_YN').AsString=
        	UNCHECKEDVALUE then
          	EarnedTime := EarnedTime +
            	Round(EarnedMin + EarnedMin * Percentage / 100)
        else
          EarnedTime := EarnedTime + EarnedMin;
      end;
    end;
    WorkQuery.Next;
  end;
  // booking Time for Time
  if EarnedTime > 0 then
    UpdateAbsenceTotal(WorkQuery,BookingDay,EmployeeData,EarnedTime);
  Result := WorkedMin - TotOverTime;
  if not RoundMinutes then
    if Result < 0 then
      Result := 0;
  WorkQuery.Free;
  TotOverMinQuery.Free;
end;

procedure BooksRegularSalaryHours(AQuery: TQuery; BookingDay: TDateTime;
  EmployeeData: TScannedIDcard; BookMinutes: Integer; Manual: String);
var
  HourType: Integer;
  WorkQuery, InsertQuery: TQuery;
begin
  // regular salary hours
  // Hour Type = ? for regular hours.
  WorkQuery := TQuery.Create(Nil);
  QueryCopy(AQuery, WorkQuery);
  InsertQuery := TQuery.Create(Nil);
  QueryCopy(AQuery, InsertQuery);
  HourType := 1;
  WorkQuery.Active := False;
  WorkQuery.SQL.Clear;
  WorkQuery.SQL.Add('SELECT HOURTYPE_NUMBER FROM WORKSPOT');
  WorkQuery.SQL.Add('WHERE PLANT_CODE=:PCODE AND ');
  WorkQuery.SQL.Add('WORKSPOT_CODE=:WCODE');
  WorkQuery.Prepare;
  WorkQuery.ParamByName('PCODE').AsString :=
    Employeedata.PlantCode;
  WorkQuery.ParamByName('WCODE').AsString :=
    Employeedata.WorkSpotCode;
  WorkQuery.Active := True;
  if (not WorkQuery.IsEmpty)and
    (not WorkQuery.FieldByName('HOURTYPE_NUMBER').IsNull) then
    HourType := WorkQuery.FieldByName('HOURTYPE_NUMBER').AsInteger;
//  if BookMinutes > 0 then
  if BookMinutes <> 0 then
  begin
    UpdateSalaryHour(InsertQuery, BookingDay, EmployeeData,
      HourType, BookMinutes, Manual, False);
  end;
  WorkQuery.Active := False;
  WorkQuery.Free;
  InsertQuery.Active := False;
  InsertQuery.Free;
end;

procedure ProcessBookings(AQuery: TQuery; EmployeeIDCard: TScannedIDCard;
  BookingDay: TDateTime; UpdateProdMin: Boolean;
  ProdMin, PayedBreaks: Integer;
  Manual: String; RoundMinutes: Boolean);
var
  NonOverTime: Integer;
  BookExceptionalBeforeOvertime: Boolean;
begin
  BookExceptionalBeforeOvertime := DetermineExceptionalBeforeOvertime(
    AQuery, EmployeeIDCard);
  // Production Hours
  if (ProdMin > 0) or RoundMinutes then
  begin
    // MR:18-11-2002
    // Process salary hours per Employee
    if not BookExceptionalBeforeOvertime then
    begin
      // Default: First book overtime, then exceptional
      NonOverTime := BooksOverTime(AQuery, BookingDay, ProdMin+PayedBreaks,
        EmployeeIDCard, Manual, BookExceptionalBeforeOvertime, RoundMinutes);
      NonOverTime := BooksExceptionalTime(AQuery, BookingDay, NonOverTime,
        EmployeeIDCard, Manual);
      BooksRegularSalaryHours(AQuery, BookingDay, EmployeeIDCard, NonOverTime,
        Manual);
    end
    else
    begin
      // First book exceptional then overtime
      NonOverTime := BooksExceptionalTime(AQuery, BookingDay, ProdMin+PayedBreaks,
        EmployeeIDCard, Manual);
      NonOverTime := BooksOverTime(AQuery, BookingDay, NonOverTime,
        EmployeeIDCard, Manual, BookExceptionalBeforeOvertime, RoundMinutes);
      BooksRegularSalaryHours(AQuery, BookingDay, EmployeeIDCard, NonOverTime,
        Manual);
    end;
    // MR:19-11-2002
    // Book hours for hourtype of Shift
    BooksHoursForShift(AQuery, BookingDay, EmployeeIDCard, ProdMin+PayedBreaks,
      Manual);
  end;
end;

// MR:30-1-2004
procedure ProcessBookingsForContractRoundMinutes(AQuery: TQuery;
  EmployeeIDCard: TScannedIDCard;
  BookingDay: TDateTime; UpdateProdMin: Boolean;
  ProdMin, PayedBreaks: Integer;
  Manual: String; RoundMinutes: Boolean);
var
  WorkQuery: TQuery;
  NonOverTime, SalaryMinutes: Integer;
begin
  WorkQuery := TQuery.Create(nil);
  QueryCopy(AQuery, WorkQuery);
  // Look for overtimedefinitions for the employee's contractgroup
  // and look if there are salary-minutes for these.
  WorkQuery.Close;
  WorkQuery.SQL.Clear;
  WorkQuery.SQL.Add(' SELECT B.TIME_FOR_TIME_YN,B.BONUS_IN_MONEY_YN,');
  WorkQuery.SQL.Add(' C.HOURTYPE_NUMBER,C.STARTTIME,C.ENDTIME,');
  WorkQuery.SQL.Add(' D.BONUS_PERCENTAGE, E.SALARY_MINUTE');
  WorkQuery.SQL.Add(' FROM EMPLOYEE A, CONTRACTGROUP B,');
  WorkQuery.SQL.Add(' OVERTIMEDEFINITION C, HOURTYPE D,');
  WorkQuery.SQL.Add(' SALARYHOURPEREMPLOYEE E');
  WorkQuery.SQL.Add(' WHERE A.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND');
  WorkQuery.SQL.Add(' E.EMPLOYEE_NUMBER = A.EMPLOYEE_NUMBER AND');
  WorkQuery.SQL.Add(' E.SALARY_DATE = :SALARY_DATE AND');
  WorkQuery.SQL.Add(' A.CONTRACTGROUP_CODE=B.CONTRACTGROUP_CODE AND');
  WorkQuery.SQL.Add(' B.CONTRACTGROUP_CODE=C.CONTRACTGROUP_CODE AND');
  WorkQuery.SQL.Add(' C.HOURTYPE_NUMBER=D.HOURTYPE_NUMBER AND');
  WorkQuery.SQL.Add(' D.HOURTYPE_NUMBER = E.HOURTYPE_NUMBER');
  WorkQuery.SQL.Add(' ORDER BY LINE_NUMBER DESC'); // In Descending order!
  WorkQuery.ParamByName('EMPLOYEE_NUMBER').AsInteger :=
    EmployeeIDCard.EmployeeCode;
  WorkQuery.ParamByName('SALARY_DATE').AsDateTime :=
    BookingDay;
  WorkQuery.Open;
  if not WorkQuery.IsEmpty then
  begin
    WorkQuery.First;
    while not WorkQuery.Eof and (ProdMin < 0) do
    begin
      // Correct salary with 'prodmin'.
      UpdateSalaryHour(AQuery, BookingDay, EmployeeIDCard,
        WorkQuery.FieldByName('HOURTYPE_NUMBER').AsInteger,
        ProdMin, Manual, False);
      // Calculate how many minutes are left
      SalaryMinutes := WorkQuery.FieldByName('SALARY_MINUTE').AsInteger +
        ProdMin;
      if SalaryMinutes < 0 then
        ProdMin := SalaryMinutes
      else
        ProdMin := 0;
      WorkQuery.Next;
    end;
  end;
  // If there are still negative minutes left, then book these on regular hours.
  if ProdMin < 0 then
  begin
    NonOverTime := BooksExceptionalTime(AQuery, BookingDay, ProdMin,
      EmployeeIDCard, Manual);
    BooksRegularSalaryHours(AQuery, BookingDay, EmployeeIDCard, NonOverTime,
      Manual);
  end;
  WorkQuery.Active := False;
  WorkQuery.Free;
end;

// MR:30-10-2003 'UpdateProdMin' has been divided in 'UpdateProdMinIn' and
// 'UpdateProdMinOut', because of overnight scans, to decide if only the
// day before midnight should be processed, or the day after, or both.
procedure ProcessTimeRecording(AQuery: TQuery; EmployeeIDCard: TScannedIDCard;
  FirstScan, LastScan, UpdateSalMin, UpdateProdMinOut, UpdateProdMinIn: Boolean;
  DeleteAction: Boolean; DeleteEmployeeIDCard: TScannedIDCard;
  LastScanRecordForDay: Boolean);
var
  WorkspotQuery: TQuery;
  BookingDay, SaveDate_Out, SaveDate_In, SaveDate_In_CutOff: TDateTime;
  ProdMin,BreaksMin, PayedBreaks,PayedBreaksTot, ProdMinTot: Integer;
  IsOvernight: Boolean;
  MyProdMin: Integer;
begin
  // MR:13-06-2003 Start
  // Get Department of Workspot for EmployeeIDCard
  WorkspotQuery := TQuery.Create(nil);
  QueryCopy(AQuery, WorkspotQuery);
  WorkspotQuery.Close;
  WorkspotQuery.SQL.Clear;
  WorkspotQuery.SQL.Add('SELECT DEPARTMENT_CODE');
  WorkspotQuery.SQL.Add('FROM WORKSPOT');
  WorkspotQuery.SQL.Add('WHERE PLANT_CODE = :PLANT_CODE AND');
  WorkspotQuery.SQL.Add('WORKSPOT_CODE = :WORKSPOT_CODE');
  WorkspotQuery.ParamByName('PLANT_CODE').AsString :=
    EmployeeIDCard.PlantCode;
  WorkspotQuery.ParamByName('WORKSPOT_CODE').AsString :=
    EmployeeIDCard.WorkspotCode;
  WorkspotQuery.Open;
  if not WorkspotQuery.IsEmpty then
    EmployeeIDCard.DepartmentCode :=
      WorkspotQuery.FieldByName('DEPARTMENT_CODE').AsString;
  WorkspotQuery.Close;
  WorkspotQuery.Free;
  ProdMinTot := 0;
  PayedBreaksTot := 0;
  SaveDate_In_CutOff := 0; // MR:06-01-2004
  SaveDate_Out := EmployeeIDCard.DateOut;
  SaveDate_In := EmployeeIDCard.DateIn;
  // MR:08-09-2003
  // Overnight scan?
  IsOverNight := Trunc(EmployeeIDCard.DateOut) > Trunc(EmployeeIDCard.DateIn);
  if IsOverNight then
  begin
    EmployeeIDCard.DateOut := Trunc(EmployeeIDCard.DateOut);
    // MR:10-10-2003 Added 'DeleteAction' and 'DeleteEmployeeIDCard
    AProdMinClass.GetProdMin(
      EmployeeIDCard, FirstScan, False,
      DeleteAction, DeleteEmployeeIDCard,
      BookingDay, ProdMin, BreaksMin, PayedBreaks);
    // MR:03-12-2003 If overnight, the first value must be saved here.
    SaveDate_In_CutOff := EmployeeIDCard.DateInCutOff;
    ProdMinTot := ProdMinTot + ProdMin;
    PayedBreaksTot := PayedBreaksTot + PayedBreaks;
    if (ProdMin > 0) and UpdateProdMinIn then
      UpdateProductionHour(AQuery, Trunc(EmployeeIDCard.DateIn), EmployeeIDCard,
      ProdMin, PayedBreaks, UNCHECKEDVALUE);
    EmployeeIDCard.DateIn := EmployeeIDCard.DateOut;
    EmployeeIDCard.DateOut := SaveDate_Out;
  end;
  { FirstScan (if scan is overnight: false) }
  if IsOverNight then
    FirstScan := False;
  // MR:10-10-2003 Added 'DeleteAction' and 'DeleteEmployeeIDCard
  AProdMinClass.GetProdMin(
    EmployeeIDCard, FirstScan, LastScan,
    DeleteAction, DeleteEmployeeIDCard,
    BookingDay, ProdMin, BreaksMin, PayedBreaks);
  // MR:03-12-2003 If overnight, the first value must be restored here.
  if IsOverNight then
    EmployeeIDCard.DateInCutOff := SaveDate_In_CutOff;
  ProdMinTot := ProdMinTot + ProdMin;
  PayedBreaksTot := PayedBreaksTot + PayedBreaks;
  if (ProdMin > 0) and UpdateProdMinOut then
    UpdateProductionHour(AQuery, Trunc(EmployeeIDCard.DateOut),
    EmployeeIDCard, ProdMin, PayedBreaks, UNCHECKEDVALUE);
  EmployeeIDCard.DateIn := SaveDate_In;
  EmployeeIDCard.DateOut := SaveDate_Out;
  if IsOverNight then
  begin
    AProdMinClass.GetShiftDay(EmployeeIdCard, SaveDate_In, SaveDate_Out);
    BookingDay := SaveDate_In;
    ReplaceTime(BookingDay,EncodeTime(0,0,0,0));
  end;
  // The total amount of minute for write in
  // PRODHOURPEREMPLOYEE is: ProdMin + PayedBreaks
  // MR:09-09-2003
  if UpdateSalMin then
  begin
    ProcessBookings(AQuery, EmployeeIDCard,
      BookingDay, UpdateProdMinOut or UpdateProdMinIn,
      ProdMinTot , PayedBreaksTot,
      UNCHECKEDVALUE, False);
    if AProdMinClass.AContractRoundMinutes and LastScanRecordForDay then
    begin
      // Round minutes... for total of salary hours for this scan-day.
      ContractRoundMinutes(AQuery, EmployeeIDCard, BookingDay, MyProdMin);
      if MyProdMin > 0 then
        ProcessBookings(AQuery, EmployeeIDCard,
          BookingDay, UpdateProdMinOut or UpdateProdMinIn,
          MyProdMin, 0, {PayedBreaksTot}
          UNCHECKEDVALUE, True)
      else
        if MyProdMin < 0 then
          ProcessBookingsForContractRoundMinutes(AQuery, EmployeeIDCard,
          BookingDay, UpdateProdMinOut or UpdateProdMinIn,
          MyProdMin, 0, {PayedBreaksTot}
          UNCHECKEDVALUE, True);
    end;
  end;
end;

procedure UnprocessProcessScans(AQuery: TQuery; FromDate: TDateTime;
  EmployeeIDCard: TScannedIDCard; ProcessSalary: Boolean; DeleteAction: Boolean;
  ProdDate1, ProdDate2, ProdDate3, ProdDate4: TDateTime);
var
  WorkQuery, DeleteQuery, HourTypeQuery: TQuery;
  BookingDay,StartDate,EndDate, CurrentBookingDay,
  CurrentStart, CurrentEnd { SaveDate_Out,  SaveDate_In}: TDateTime;
  Index {ProdMin, BreaksMin, PayedBreaks ,HourTypeNumber}: Integer;
  TotEarnedMin: Double;
  CurrentIDCard: TScannedIDCard;
  CurrentTFT, CurrentBonusInMoney {,Processed, DeletedOld}: Boolean;
  RecordsForUpdate: TList;
  WhereSelect: String;
  AddToProdListOut, AddToProdListIn, AddToSalList: Boolean;
  ABookmarkRecord: PTBookmarkRecord;
  ProdDate: array[1..4] of TDateTime;
//  DateDeleted: array[1..4] of Boolean;
  I: Integer;
  LastScanRecordForDay: Boolean;
  BookingDayCurrent, BookingDayNext: TDateTime;
  procedure GetTFT_Bonus( DeleteQuery:TQuery; Employee: Integer;
    var CurrentTFT, CurrentBonusInMoney: Boolean);
  begin
    DeleteQuery.Active := False;
    DeleteQuery.SQL.Clear;
    DeleteQuery.SQL.Add(' SELECT A.TIME_FOR_TIME_YN, BONUS_IN_MONEY_YN');
    DeleteQuery.SQL.Add(' FROM CONTRACTGROUP A, EMPLOYEE B');
    DeleteQuery.SQL.Add(' WHERE B.EMPLOYEE_NUMBER=:EMPNO AND ');
    DeleteQuery.SQL.Add(' A.CONTRACTGROUP_CODE=B.CONTRACTGROUP_CODE');
    DeleteQuery.Prepare;
    DeleteQuery.ParamByName('EMPNO').AsInteger := Employee;
    DeleteQuery.Active := True;
    CurrentTFT := False;
    CurrentBonusInMoney := False;
    if not DeleteQuery.IsEmpty then
    begin
      CurrentTFT := (DeleteQuery.FieldByName('TIME_FOR_TIME_YN').AsString=
       	CHECKEDVALUE);
      CurrentBonusInMoney :=
      	(DeleteQuery.FieldByName('BONUS_IN_MONEY_YN').AsString=CHECKEDVALUE);
    end;
  end;
  procedure DeleteAbsenceHours(DeleteQuery:TQuery; FromDate, ToDate: TDateTime;
    CurrentTFT, CurrentBonusInMoney: Boolean; Employee: Integer);
  var
    HourTypeNumber: Integer;
  begin
    DeleteQuery.Active := False;
    DeleteQuery.SQL.Clear;
    DeleteQuery.SQL.Add(' SELECT HOURTYPE_NUMBER, SALARY_MINUTE, SALARY_DATE ');
    DeleteQuery.SQL.Add(' FROM SALARYHOURPEREMPLOYEE');
    DeleteQuery.SQL.Add(' WHERE SALARY_DATE>=:FROMDATE AND');
    DeleteQuery.SQL.Add(' SALARY_DATE<=:TODATE AND');
    DeleteQuery.SQL.Add(' EMPLOYEE_NUMBER=:EMPNO AND MANUAL_YN=:MANUAL');
    DeleteQuery.Prepare;
    DeleteQuery.ParamByName('FROMDATE').AsDateTime := Trunc(FromDate);
    DeleteQuery.ParamByName('TODATE').AsDateTime := Trunc(ToDate);
    DeleteQuery.ParamByName('EMPNO').AsInteger := Employee;
    DeleteQuery.ParamByName('MANUAL').AsString := UNCHECKEDVALUE;
    DeleteQuery.Active := True;
    while not DeleteQuery.Eof do
    begin
      HourTypeNumber := DeleteQuery.FieldByName('HOURTYPE_NUMBER').AsInteger;
      if (HourTypeNumber <> 1) and CurrentTFT  then
      // Overtime must be substarcted from earned time for time
      begin
        TotEarnedMin := DeleteQuery.FieldByName('SALARY_MINUTE').AsInteger;
        HourTypeQuery.Active := False;
        HourTypeQuery.SQL.Clear;
        HourTypeQuery.SQL.Add('SELECT BONUS_PERCENTAGE FROM HOURTYPE');
        HourTypeQuery.SQL.Add('WHERE HOURTYPE_NUMBER=:HOURTYPE_NUMBER');
        HourTypeQuery.Prepare;
        HourTypeQuery.ParamByName('HOURTYPE_NUMBER').AsInteger :=
          HourTypeNumber;
        HourTypeQuery.Active := True;
        if (not HourTypeQuery.IsEmpty) and
          (not HourTypeQuery.Fields[0].IsNull) and
          (not CurrentBonusInMoney) then
          TotEarnedMin := Round(TotEarnedMin +
            TotEarnedMin * HourTypeQuery.Fields[0].AsFloat / 100);
        if TotEarnedMin > 0 then
          UpdateAbsenceTotal(AQuery,
            DeleteQuery.FieldByName('SALARY_DATE').AsDateTime,
            CurrentIDCard, (-1) * TotEarnedMin);
        HourTypeQuery.Active := False;
      end;
      DeleteQuery.Next;
    end;
  end;
  procedure DeleteSalary(DeleteQuery: TQuery;
    FromDate, ToDate: TDateTime; Employee: Integer;
    EmployeeIDCard: TScannedIDCard);
  begin
    DeleteQuery.Active := False;
    DeleteQuery.SQL.Clear;
    DeleteQuery.SQL.Add(' DELETE FROM SALARYHOURPEREMPLOYEE');
    DeleteQuery.SQL.Add(' WHERE SALARY_DATE >=:FROMDATE AND ');
    DeleteQuery.SQL.Add(' SALARY_DATE <=:TODATE AND ');
    DeleteQuery.SQL.Add(' EMPLOYEE_NUMBER=:EMPNO AND ');
    DeleteQuery.SQL.Add(' MANUAL_YN=:MANUAL ');
    DeleteQuery.Prepare;
    DeleteQuery.ParamByName('FROMDATE').AsDateTime := Trunc(FromDate);
    DeleteQuery.ParamByName('TODATE').AsDateTime := Trunc(ToDate);
    DeleteQuery.ParamByName('EMPNO').AsInteger := Employee;
    DeleteQuery.ParamByName('MANUAL').AsString := UNCHECKEDVALUE;
    DeleteQuery.ExecSql;
    // MR:07-11-2003 Changes because 'CurrentIDCard' is probably not filled.
    // Now 'EmployeeIDCard' is used and also a parameter.
    DeleteProdHourForAllTypes(AQuery, EmployeeIDCard, FromDate, ToDate,
      UNCHECKEDVALUE);
  end;
  procedure DeleteProduction(DeleteQuery: TQuery;
    CurrentBookingDay: TDateTime; Employee: Integer);
  begin
    DeleteQuery.Active := False;
    DeleteQuery.SQL.Clear;
    DeleteQuery.SQL.Add(' DELETE FROM PRODHOURPEREMPLOYEE');
    DeleteQuery.SQL.Add(' WHERE PRODHOUREMPLOYEE_DATE =:PRODDATE AND ');
    DeleteQuery.SQL.Add(' EMPLOYEE_NUMBER=:EMPNO AND ');
    DeleteQuery.SQL.Add(' MANUAL_YN=:MANUAL ');
    DeleteQuery.Prepare;
    DeleteQuery.ParamByName('PRODDATE').AsDateTime := Trunc(CurrentBookingDay);
    DeleteQuery.ParamByName('EMPNO').AsInteger := Employee;
    DeleteQuery.ParamByName('MANUAL').AsString := UNCHECKEDVALUE;
    DeleteQuery.ExecSql;
  end;
begin
  WorkQuery := TQuery.Create(nil);
  DeleteQuery := TQuery.Create(nil);
  HourTypeQuery := TQuery.Create(nil);
  QueryCopy(AQuery,WorkQuery);
  QueryCopy(AQuery,DeleteQuery);
  QueryCopy(AQuery,HourTypeQuery);

  ProdDate[1] := ProdDate1;
  ProdDate[2] := ProdDate2;
  ProdDate[3] := ProdDate3;
  ProdDate[4] := ProdDate4;
  // booking date
  AProdMinClass.GetShiftDay(EmployeeIDCard, StartDate, EndDate);
  // Calculate the employe booking day
  BookingDay := StartDate;
  ReplaceTime(BookingDay,EncodeTime(0,0,0,0));
  // Over Time Period
  ComputeOvertimePeriod(StartDate, EndDate, AQuery, EmployeeIDCard);

  if ProcessSalary then
  begin
    GetTFT_Bonus(DeleteQuery, EmployeeIDCard.EmployeeCode,
      CurrentTFT, CurrentBonusInMoney );
    if (FromDate > StartDate) and (FromDate < EndDate) then
    begin
      DeleteAbsenceHours(DeleteQuery, FromDate, EndDate,
        CurrentTFT, CurrentBonusInMoney, EmployeeIDCard.EmployeeCode);
      DeleteSalary(DeleteQuery, FromDate, EndDate,
        EmployeeIDCard.EmployeeCode, EmployeeIDCard);
    end
    else
    begin
      DeleteAbsenceHours(DeleteQuery, StartDate, EndDate,
        CurrentTFT, CurrentBonusInMoney, EmployeeIDCard.EmployeeCode);
      DeleteSalary(DeleteQuery, StartDate, EndDate,
        EmployeeIDCard.EmployeeCode, EmployeeIDCard);
    end;
  end;

  for I := 1 to 4 do
    if ProdDate[I] <> NullDate then
    begin
      DeleteProduction(DeleteQuery, ProdDate[I],
        EmployeeIDCard.EmployeeCode);
    end;

  // Al data ready. Start write in Data_Base
  RecordsForUpdate := Tlist.Create;
  // Select all records from TIMEREGSCANNING that result booking date for
  // Salary hours in OverTime period:
  // StartDateTime - 1day and EndDateTime + 1day

  // MR:05-09-2003
  if ProcessSalary then
  begin
    WhereSelect :=
      ' WHERE DATETIME_IN >= :DSTART AND ' +
      ' DATETIME_IN <= :DEND AND ';
  end
  else
  begin
    WhereSelect :=
      ' WHERE ' +
      '(((DATETIME_IN >= :FROMDATE1) AND  (DATETIME_IN < :TODATE1)) OR ' +
      ' ((DATETIME_IN >= :FROMDATE2) AND  (DATETIME_IN < :TODATE2)) OR ' +
      ' ((DATETIME_IN >= :FROMDATE3) AND  (DATETIME_IN < :TODATE3)) OR ' +
      ' ((DATETIME_IN >= :FROMDATE4) AND  (DATETIME_IN < :TODATE4)) OR ' +
      ' ((DATETIME_OUT >= :FROMDATE1) AND (DATETIME_OUT < :TODATE1)) OR ' +
      ' ((DATETIME_OUT >= :FROMDATE2) AND (DATETIME_OUT < :TODATE2)) OR ' +
      ' ((DATETIME_OUT >= :FROMDATE3) AND (DATETIME_OUT < :TODATE3)) OR ' +
      ' ((DATETIME_OUT >= :FROMDATE4) AND (DATETIME_OUT < :TODATE4))) AND ';
  end;

  // MR:03-02-2005 Also get department from workspot!
  WorkQuery.Active := False;
  WorkQuery.SQL.Clear;
  WorkQuery.SQL.Add(
    'SELECT ' +
    '  TRS.DATETIME_IN, TRS.EMPLOYEE_NUMBER, TRS.PLANT_CODE, ' +
    '  TRS.WORKSPOT_CODE, TRS.JOB_CODE, TRS.SHIFT_NUMBER, ' +
    '  TRS.DATETIME_OUT, TRS.PROCESSED_YN, TRS.IDCARD_IN, ' +
    '  TRS.IDCARD_OUT, WS.DEPARTMENT_CODE ' +
    'FROM ' +
    '  TIMEREGSCANNING TRS, WORKSPOT WS' +
    WhereSelect +
    '  TRS.PLANT_CODE = WS.PLANT_CODE AND ' +
    '  TRS.WORKSPOT_CODE = WS.WORKSPOT_CODE AND ' +
    '  TRS.EMPLOYEE_NUMBER = :EMPNO AND ' +
    '  TRS.PROCESSED_YN = :PROCESSED ' +
    '  ORDER BY DATETIME_IN '
    );
  WorkQuery.Prepare;
  if ProcessSalary then
  begin
    WorkQuery.ParamByName('DSTART').AsDateTime := StartDate - 1;
    WorkQuery.ParamByName('DEND').AsDateTime := EndDate + 1;
  end
  else
  begin
    WorkQuery.ParamByName('FROMDATE1').AsDate := Trunc(ProdDate1);
    WorkQuery.ParamByName('FROMDATE2').AsDate := Trunc(ProdDate2);
    WorkQuery.ParamByName('FROMDATE3').AsDate := Trunc(ProdDate3);
    WorkQuery.ParamByName('FROMDATE4').AsDate := Trunc(ProdDate4);
    WorkQuery.ParamByName('TODATE1').AsDate := Trunc(ProdDate1) + 1;
    WorkQuery.ParamByName('TODATE2').AsDate := Trunc(ProdDate2) + 1;
    WorkQuery.ParamByName('TODATE3').AsDate := Trunc(ProdDate3) + 1;
    WorkQuery.ParamByName('TODATE4').AsDate := Trunc(ProdDate4) + 1;
  end;
  WorkQuery.ParamByName('EMPNO').AsInteger := EmployeeIDCard.EmployeeCode;
  WorkQuery.ParamByName('PROCESSED').AsString := CHECKEDVALUE;
  WorkQuery.Active := True;
  WorkQuery.First;
// MR:05-09-2003 NEW while...
  while not WorkQuery.Eof do
  begin
    AddToProdListOut := False;
    AddToProdListIn := False;
    AddToSalList := False;
    EmptyIDCard(CurrentIDCard);
    PopulateIDCard(AQuery, CurrentIDCard, WorkQuery);
    // MR:03-02-2005 Get department from workspot!
    CurrentIDCard.DepartmentCode :=
      WorkQuery.FieldByName('DEPARTMENT_CODE').AsString;
    if ProcessSalary then
    begin
      AProdMinClass.GetShiftDay(CurrentIDCard, CurrentStart, CurrentEnd);
      // Calculate the employe booking day
      CurrentBookingDay := CurrentStart;
      ReplaceTime(CurrentBookingDay, EncodeTime(0, 0, 0, 0));
      ComputeOvertimePeriod(CurrentStart, CurrentEnd, AQuery, CurrentIDCard);
      // if same overtimeperiod and ...
      if (Trunc(FromDate) >= Trunc(CurrentStart)) and
        (Trunc(FromDate) <= Trunc(CurrentEnd)) and
        (Trunc(CurrentBookingDay) >= Trunc(FromDate)) then
      begin
        if not (DeleteAction and
          (CurrentIDCard.DateIn = EmployeeIDCard.DateIn) and
          (CurrentIDCard.EmployeeCode = EmployeeIDCard.EmployeeCode)) then
          AddToSalList := True;
      end;
    end; // if ProcessSalary then
    // Determine if add to Production
    for I := 1 to 4 do
      if (Trunc(CurrentIDCard.DateOut) = Trunc(ProdDate[I])) then
      begin
        if not (DeleteAction and
          (CurrentIDCard.DateIn = EmployeeIDCard.DateIn) and
          (CurrentIDCard.EmployeeCode = EmployeeIDCard.EmployeeCode)) then
         AddToProdListOut := True;
      end;
    // If Overnight then determine if add to Production
    if (Trunc(CurrentIDCard.DateIn) <> Trunc(CurrentIDCard.DateOut)) then
    begin
      for I := 1 to 4 do
        if (Trunc(CurrentIDCard.DateIn) = Trunc(ProdDate[I])) then
        begin
          if not (DeleteAction and
            (CurrentIDCard.DateIn = EmployeeIDCard.DateIn) and
            (CurrentIDCard.EmployeeCode = EmployeeIDCard.EmployeeCode)) then
            AddToProdListIn := True;
        end;
    end;
    // Add Production and/or Salary
    if AddToProdListOut or AddToProdListIn or AddToSalList then
    begin
      new(ABookmarkRecord);
      ABookmarkRecord.Bookmark := WorkQuery.GetBookmark;
      ABookmarkRecord.AddToSalList := AddToSalList;
      ABookmarkRecord.AddToProdListOut := AddToProdListOut;
      ABookmarkRecord.AddToProdListIn := AddToProdListIn;
      RecordsForUpdate.Add(ABookmarkRecord);
    end;
    WorkQuery.Next;
  end; {while}

// MR:05-09-2003 NEW
  // Recalculate all deleted records from Salary and Production Table
  // Compute Salary hours for deleted Records
  { Processed := False; }
  WorkQuery.First;
  for Index := 0 to RecordsForUpdate.Count - 1 do
  begin
    LastScanRecordForDay := False;
    BookingDayNext := 0;
    // MR:29-07-2004
    if Index = RecordsForUpdate.Count - 1 then
      LastScanRecordForDay := True
    else
    begin
      ABookmarkRecord := RecordsForUpdate.Items[Index + 1];
      WorkQuery.GotoBookmark(ABookmarkRecord.Bookmark);
      // Don't free the bookmark here.
      EmptyIDCard(CurrentIDCard);
      PopulateIDCard(AQuery, CurrentIDCard, WorkQuery);
      // MR:03-02-2005 Get department from workspot!
      CurrentIDCard.DepartmentCode :=
        WorkQuery.FieldByName('DEPARTMENT_CODE').AsString;
      // Get Bookingday of Next Scan-record
      AProdMinClass.GetShiftDay(CurrentIDCard, StartDate, EndDate);
      BookingDayNext := StartDate;
    end;
    ABookmarkRecord := RecordsForUpdate.Items[Index];
    WorkQuery.GotoBookmark(ABookmarkRecord.Bookmark);
    WorkQuery.FreeBookmark(ABookmarkRecord.Bookmark);
    EmptyIDCard(CurrentIDCard);
    PopulateIDCard(AQuery, CurrentIDCard, WorkQuery);
    // MR:03-02-2005 Get department from workspot!
    CurrentIDCard.DepartmentCode :=
      WorkQuery.FieldByName('DEPARTMENT_CODE').AsString;
    if not LastScanRecordForDay then
    begin
      // Get Bookingday of Current Scan-record
      AProdMinClass.GetShiftDay(CurrentIDCard, StartDate, EndDate);
      BookingDayCurrent := StartDate;
      // Is Bookingday of Current different from Next?
      if BookingDayCurrent <> BookingDayNext then
        LastScanRecordForDay := True;
    end;
    // MR:10-10-2003 Added 'DeleteAction' and 'EmployeeIDCard'
    // In case of a DeleteAction, the data about this scan-to-delete
    // must be known.
    ProcessTimeRecording(DeleteQuery, CurrentIDCard, True, True,
      ABookmarkRecord.AddToSalList, ABookmarkrecord.AddToProdListOut,
      ABookmarkrecord.AddToProdListIn, DeleteAction, EmployeeIDCard,
      LastScanRecordForDay);
  end;

  // MR:08-09-2003
  // Clear list
  for Index := RecordsForUpdate.Count - 1 downto 0 do
  begin
    ABookmarkRecord := RecordsForUpdate.Items[Index];
    // MR:10-10-2003 'dispose' record
    Dispose(ABookMarkRecord);
    RecordsForUpdate.Remove(ABookmarkRecord);
  end;
  RecordsForUpdate.Free;
  WorkQuery.Free;
  DeleteQuery.Free;
  HourTypeQuery.Free;
end;

procedure ComputeOvertimePeriod(var StartDate, EndDate: TDateTime;
  AQuery: TQuery; EmployeeData: TScannedIDCard);
var
  WorkQuery: TQuery;
  OverTimeType, MyDayInWeek: Integer;
  PeriodStart, PeriodLength, StartWeek, PeriodNumber: Integer;
  WeekNumber, Year{, Month, Day}: Word;
  BookingDate: TDateTime;
begin
  WorkQuery := TQuery.Create(Nil);
  QueryCopy(AQuery, WorkQuery);
  BookingDate := StartDate;
//  OverTimeType := 0;
  WorkQuery.Active := False;
  WorkQuery.SQL.Clear;
  WorkQuery.SQL.Add(' SELECT A.PERIOD_STARTS_IN_WEEK,');
  WorkQuery.SQL.Add(' A.OVERTIME_PER_DAY_WEEK_PERIOD, ');
  WorkQuery.SQL.Add(' A.WEEKS_IN_PERIOD ');
  WorkQuery.SQL.Add(' FROM CONTRACTGROUP A, EMPLOYEE B');
  WorkQuery.SQL.Add(' WHERE (A.CONTRACTGROUP_CODE=B.CONTRACTGROUP_CODE) AND ');
  WorkQuery.SQL.Add(' (B.EMPLOYEE_NUMBER=:EMPNO) ');
  WorkQuery.Prepare;
  WorkQuery.ParamByName('EMPNO').AsInteger := EmployeeData.EmployeeCode;
  WorkQuery.Active := True;
  WorkQuery.First;
  if not WorkQuery.IsEmpty then
  begin
    OverTimeType :=
      WorkQuery.FieldByName('OVERTIME_PER_DAY_WEEK_PERIOD').AsInteger;
    case OverTimeType of
      1 : begin  // period = day
            StartDate := BookingDate;
            EndDate := BookingDate;
          end;
      2 : begin  // period = week
            MyDayInWeek := DayInWeek(ORASystemDM.WeekStartsOn, BookingDate);
            StartDate := BookingDate - MyDayInWeek + 1;
            EndDate := BookingDate + 7 - MyDayInWeek;
      	  end;
      3 : begin  // period = weeks
            PeriodStart :=
              WorkQuery.FieldByName('PERIOD_STARTS_IN_WEEK').AsInteger;
            PeriodLength := WorkQuery.FieldByName('WEEKS_IN_PERIOD').AsInteger;
            if (PeriodStart <= 0) or (PeriodLength <= 0) then
            // incorrect values
            begin
              StartDate := BookingDate;
              Enddate := BookingDate;
            end
            else
            begin
              // START MR:06-01-2004 Rewritten, to prevent errors with
              //                     wrong year...
              WeekUitDat(BookingDate, Year, WeekNumber);
              PeriodNumber :=
                Trunc((WeekNumber - PeriodStart) / PeriodLength + 1);
              StartWeek := (PeriodNumber - 1) * PeriodLength + PeriodStart;
              StartDate := StartOfYear(Year);
              StartDate := StartDate + (StartWeek - 1) * 7;
              EndDate := StartDate + (PeriodLength * 7) - 1;
            end;
          end;
    end; // case
    // MR:17-09-2003 Use '0,0,0,0' istead of '0,0,0,1' for 'EncodeTime'
    // Otherwise comparisons can go wrong.
    ReplaceTime(StartDate, EncodeTime(0, 0, 0, 0));
    ReplaceTime(EndDate, EncodeTime(23, 59, 59, 99));
  end; // if
  WorkQuery.Free;
end;

// end move UGlobals
end.
