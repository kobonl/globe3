(*
  MRA:23-JAN-2014 TD-24046
  - Translate issues: Use a different method to translate.
    - Do NOT do this in separate files where the translation is done
      hard-coded!
    - New method:
      - Use 1 resource-strings-file that contains the translation-strings.
      - Use 1 translate-unit that assigns these strings to the properties
        in the forms.
      - In this way there is only 1 form that can be translated in multiple
        languages when the application is run.
      - The project-file defines the language using a condition.
*)
unit UTranslateTimeRec;

interface

type
  TTranslateHandlerTimeRec = class
  private
  public
    constructor Create; overload;
    destructor Destroy; override;
    procedure TranslateAll;
    procedure TranslateInit;
  end;

var
  ATranslateHandlerTimeRec: TTranslateHandlerTimeRec;

implementation

uses
  SelectorFRM, TimeRecordingFRM, {TimeRecordingSelectorFRM,}
  AboutFRM, BaseDialogFRM, {BaseSerialFRM, } BaseTopLogoFRM,
  BaseTopMenuFRM, {DialogBaseFRM,} DialogChangeJobFRM, {DialogLoginFRM,}
  DialogSelectDownTypeFRM, {DialogSelectionFRM,} {GridBaseFRM,}
  OrderInfoFRM, SplashFRM{, WaitFRM}, DialogAskEmployeeNumberFRM,
  UTranslateStringsTimeRec;

{ TTranslateHandlerTimeRec }

constructor TTranslateHandlerTimeRec.Create;
begin
  inherited;
//
end;

destructor TTranslateHandlerTimeRec.Destroy;
begin
//
  inherited;
end;

procedure TTranslateHandlerTimeRec.TranslateAll;
begin
  if Assigned(SelectorF) then
    with SelectorF do
    begin
      Caption := STransSelectWorkspot;
      BitBtnOk.Caption := STransBtnOK;
      BitBtnCancel.Caption := STransBtnCancel;
      BitBtnEndOfDay.Caption := STransBtnEndOfDay;
    end;
  if Assigned(TimeRecordingF) then
    with TimeRecordingF do
    begin
//      Caption := STransTimeRecording;
      GroupBoxEmployee.Caption := STransEmployee;
      LabelIDCard.Caption := STransIDCard;
      BitBtnAccept.Caption := STransBtnAccept;
      BitBtnCancel.Caption := STransBtnCancel;
      LabelEmployee.Caption := STransEmp;
      GroupBoxDateTime.Caption := STransDateAndTime;
      LabelCurentDate.Caption := STransCurrentDate;
      GroupBoxWorked.Caption := STransWorkedTimeHM;
      LabelLastWeek.Caption := STransLastWeek;
      LabelThisWeek.Caption := STransThisWeek;
      GroupBoxTime.Caption := STransTimeHM;
      LabelTimeForTime.Caption := STransTimeForTime;
      LabelHoliday.Caption := STransHoliday;
      LabelTimeReduction.Caption := STransWorkTimeReduction;
      GroupBoxMessages.Caption := STransMessages;
      LabelErrorMsg.Caption := STransScanned;
      GroupBoxSmartCard.Caption := STransSmartCardInfo;
      GroupBox1.Caption := STransCardReaders;
      GroupBox2.Caption := STransLogMessage;
      GroupBoxWorkspot.Caption := STransWorkspotJob;
      LabelPlant.Caption := STransPlant;
      LabelFrom.Caption := STransFrom;
      LabelJob.Caption := STransJob;
      LabelWorkspot.Caption := STransWorkspot;
    end;
  if Assigned(DialogAskEmployeeNumberF) then
    with DialogAskEmployeeNumberF do
    begin
      Caption := STransEnterEmplNr;
      GroupBox1.Caption := STransEnterEmplNr;
      Label1.Caption := STransEmpNr;
    end;
end;

procedure TTranslateHandlerTimeRec.TranslateInit;
begin
//
end;

initialization
  { Initialization section goes here }
  ATranslateHandlerTimeRec := TTranslateHandlerTimeRec.Create;

finalization
  { Finalization section goes here }
  ATranslateHandlerTimeRec.Free;

end.
