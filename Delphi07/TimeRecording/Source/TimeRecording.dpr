(*
  MRA:21-JUN-2012 20013176
  - Timerecording & card reader:
    - This asks for an employee-number when the card-ID
      is not known yet.
    - The above must default NOT be done, but only with a parameter
      '-A' it must ask for the employee-number.
  MRA:30-APR-2018 PIM-337.2 Rework
  - When -M is used then the dialog is not maximized so it fills the
    whole screen. This should be checked/fixed. It should start at 0,0
    (left corner) till the taskbar (taskbar should still be visible).
*)
program TimeRecording;

uses
  Forms,
  SysUtils,
  BasePimsFRM in '..\..\Pims Shared\BasePimsFRM.pas' {BasePimsForm},
  BasePimsSerialFRM in '..\..\Pims Shared\BasePimsSerialFRM.pas' {BasePimsSerialForm},
  BaseTopMenuFRM in '..\..\Pims Shared\BaseTopMenuFRM.pas' {BaseTopMenuForm},
  BaseTopLogoFRM in '..\..\Pims Shared\BaseTopLogoFRM.pas' {BaseTopLogoForm},
  BaseDialogFRM in '..\..\Pims Shared\BaseDialogFRM.pas' {BaseDialogForm},
  AboutFRM in '..\..\Pims Shared\AboutFRM.pas' {AboutForm},
  OrderInfoFRM in '..\..\Pims Shared\OrderInfoFRM.pas' {OrderInfoForm},
  UPimsConst in '..\..\Pims Shared\UPimsConst.pas',
  UPimsMessageRes in '..\..\Pims Shared\UPimsMessageRes.pas',
  UPimsVersion in '..\..\Pims Shared\UPimsVersion.pas',
  SplashFRM in '..\..\Pims Shared\SplashFRM.pas' {SplashForm},
  ORASystemDMT in '..\..\Pims Shared\ORASystemDMT.pas' {ORASystemDM: TDataModule},
  TimeRecordingFRM in 'TimeRecordingFRM.pas' {TimeRecordingF},
  TimeRecordingDMT in 'TimeRecordingDMT.pas' {TimeRecordingDM: TDataModule},
  SelectorFRM in 'SelectorFRM.pas' {SelectorF},
  UGlobalFunctions in '..\..\Pims Shared\UGlobalFunctions.pas',
  UScannedIDCard in '..\..\Pims Shared\UScannedIDCard.pas',
  CalculateTotalHoursDMT in '..\..\Pims Shared\CalculateTotalHoursDMT.pas' {CalculateTotalHoursDM: TDataModule},
  GlobalDMT in '..\..\Pims Shared\GlobalDMT.pas' {GlobalDM: TDataModule},
  DialogChangeJobDMT in '..\..\Pims Shared\DialogChangeJobDMT.pas' {DialogChangeJobDM: TDataModule},
  DialogChangeJobFRM in '..\..\Pims Shared\DialogChangeJobFRM.pas' {DialogChangeJobF},
  PersonalScreenDMT in '..\..\Pims Shared\PersonalScreenDMT.pas' {PersonalScreenDM: TDataModule},
  DialogSelectDownTypeFRM in '..\..\Pims Shared\DialogSelectDownTypeFRM.pas' {DialogSelectDownTypeF},
  UPersonalScreen in '..\..\PersonalScreen\Source\UPersonalScreen.pas',
  UPCSCCardReader in 'UPCSCCardReader.pas',
  DialogAskEmployeeNumberFRM in 'DialogAskEmployeeNumberFRM.pas' {DialogAskEmployeeNumberF},
  UProductionScreenDefs in '..\..\Pims Shared\UProductionScreenDefs.pas',
  UTranslateTimeRec in 'UTranslateTimeRec.pas',
  UTranslateStringsTimeRec in 'UTranslateStringsTimeRec.pas',
  UTranslateShared in '..\..\Pims Shared\UTranslateShared.pas',
  UTranslateStringsShared in '..\..\Pims Shared\UTranslateStringsShared.pas',
  UTimeZone in '..\..\Pims Shared\UTimeZone.pas',
  ColorButton in '..\..\Pims Shared\ColorButton.pas',
  RealTimeEffDMT in '..\..\Pims Shared\RealTimeEffDMT.pas' {RealTimeEffDM: TDataModule},
  EnterJobCommentDMT in '..\..\Pims Shared\EnterJobCommentDMT.pas' {EnterJobCommentDM: TDataModule},
  DialogEnterJobCommentFRM in '..\..\Pims Shared\DialogEnterJobCommentFRM.pas' {DialogEnterJobCommentF},
  DialogMessageFRM in 'DialogMessageFRM.pas' {DialogMessageF},
  HybridDMT in '..\..\Pims Shared\HybridDMT.pas' {HybridDM: TDataModule},
  DialogShiftCheckFRM in '..\..\Pims Shared\DialogShiftCheckFRM.pas' {DialogShiftCheckForm},
  UWorkspotEmpEff in '..\..\Pims Shared\UWorkspotEmpEff.pas',
  DialogShiftCheckDMT in '..\..\Pims Shared\DialogShiftCheckDMT.pas' {DialogShiftCheckDM: TDataModule},
  PCPROXAPI in '..\..\Pims Shared\PCPROXAPI.PAS',
  UPCProcsPlusReader in '..\..\Pims Shared\UPCProcsPlusReader.pas';

{$R *.RES}

function IsParam(AParam: String): Boolean;
var
  I, IPos: Integer;
  PStr: String;
begin
  Result := False;
  for I := 1 to ParamCount do
  begin
    PStr := UpperCase(ParamStr(I));
    IPos := Pos(AParam, PStr);
    if IPos > 0 then
    begin
      Result := True;
      Break;
    end;
  end;
end;

// 20013176
function AutoAskForEmployeeNumber: Boolean;
begin
  Result := IsParam('-A');
end; // AutoAskForEmployeeNumber

begin
  Application.Initialize;
  Application.Title := 'ORCL - PIMS - Time Recording';
  Application.CreateForm(TORASystemDM, ORASystemDM);
  Application.CreateForm(TRealTimeEffDM, RealTimeEffDM);
  Application.CreateForm(TEnterJobCommentDM, EnterJobCommentDM);
  Application.CreateForm(THybridDM, HybridDM);
  Application.CreateForm(TDialogShiftCheckDM, DialogShiftCheckDM);
  ORASystemDM.AppName := APPNAME_TRS;
  Application.CreateForm(TCalculateTotalHoursDM, CalculateTotalHoursDM);
  Application.CreateForm(TGlobalDM, GlobalDM);
  Application.CreateForm(TPersonalScreenDM, PersonalScreenDM);
  Application.CreateForm(TTimeRecordingDM, TimeRecordingDM);
  if IsParam('-M') then
    TimeRecordingDM.StartMaximized := True;
  if IsParam('-N') then
    TimeRecordingDM.NoClose := True;
  Application.CreateForm(TTimeRecordingF, TimeRecordingF);
  // TD-21429
  // PIM-337.2
  if not TimeRecordingDM.StartMaximized then
    TimeRecordingF.Position := poScreenCenter;
  Application.CreateForm(TDialogChangeJobF, DialogChangeJobF);
  DialogAskEmployeeNumberF := nil;
  if AutoAskForEmployeeNumber then
  begin
    Application.CreateForm(TDialogAskEmployeeNumberF, DialogAskEmployeeNumberF);
    DialogAskEmployeeNumberF.Visible := False;
  end;
  DialogChangeJobF.Visible := False;
  Application.CreateForm(TDialogShiftCheckForm, DialogShiftCheckForm);
  DialogShiftCheckForm.Visible := False;
  ATranslateHandlerTimeRec.TranslateAll;
  ATranslateHandlerShared.TranslateAll;
  Application.Run;
end.
