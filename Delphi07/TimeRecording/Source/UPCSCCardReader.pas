(*
  MRA:8-JUN-2012
  - Also return buffer in hex-format (MyHexBuffer).
*)
unit UPCSCCardReader;

interface

uses
  Windows, SysUtils, Messages, Classes, Forms, Controls, ComCtrls, StdCtrls,
  ExtCtrls, Graphics, PCSCRaw, PCSCDef, Reader;

type
  TMyReadCardID = procedure of object;
  TMyAddErrorLog = procedure(AMsg: String) of object;

type
  TPCSCCardReader = class
  private
    FPCSCRaw:TPCSCRaw;
    FPCSCDeviceContext:DWORD;
    FReaderListThread:TReaderListThread;
    FMyBuffer: String;
    FReaderListBox: TListBox;
    FCommandComboBox: TComboBox;
    FRichEdit: TRichEdit;
    FMyReadCardID: TMyReadCardID;
    FMyAddErrorLog: TMyAddErrorLog;
    FMyHexBuffer: String;
    procedure InitPCSC;
    procedure UpdatePCSCReaderList;
    procedure GetPCSCReaderList(ReaderList:TStringList);
    procedure CardStateChanged(Sender:TObject);
    procedure ReaderListChanged;
    procedure UpdateButtons;
    procedure LogInBuffer(Buffer:PByteArray;BufferSize:DWORD);
    procedure LogOutBuffer(Buffer:PByteArray;BufferSize:DWORD);
    function StringToBuffer(Command:string;Buffer:PByteArray):DWORD;
  public
    constructor Create;
    destructor Destroy; override;
    procedure Init;
    procedure AddLog(Msg:string;Color:TColor=clBlack;LineBreak:boolean=true;Bold:boolean=false);
    procedure AddErrorLog(Msg:string;Color:TColor=clBlack;LineBreak:boolean=true;Bold:boolean=false);
    function ConnectShared: Boolean;
    function ConnectExclusive: Boolean;
    function Disconnect: Boolean;
    function Transmit: Boolean;
    function TransmitCLICC: Boolean;
    property MyBuffer: String read FMyBuffer write FMyBuffer;
    property ReaderListBox: TListBox read FReaderListBox write FReaderListBox;
    property CommandComboBox: TComboBox read FCommandComboBox write FCommandComboBox;
    property RichEdit: TRichEdit read FRichEdit write FRichEdit;
    property MyReadCardID: TMyReadCardID read FMyReadCardID write FMyReadCardID;
    property MyAddErrorLog: TMyAddErrorLog read FMyAddErrorLog write FMyAddErrorLog;
    property MyHexBuffer: String read FMyHexBuffer write FMyHexBuffer;
  end;

function MyScardCLICCTransmit(
  AScardHandle: DWord; // Cardinal;
  ApucSendData: Pointer;
  AulSendDataBufLen: DWord;
  ApucReceivedData: Pointer;
  var ApulReceivedDataBufLen: DWord): DWord;

implementation

{ TPCSCCardReader }

constructor TPCSCCardReader.Create;
begin
  inherited;
  FPCSCRaw := TPCSCRaw.Create;
end;

procedure TPCSCCardReader.Init;
begin
  InitPCSC;
  ReaderListChanged;
  FReaderListThread := TReaderListThread.Create(FPCSCRaw);
  FReaderListThread.OnReaderListChanged := ReaderListChanged;
  FReaderListThread.Resume;
end;

destructor TPCSCCardReader.Destroy;
var
  i:integer;
begin
  FReaderListThread.Terminate;
  for i := 0 to ReaderListBox.Count-1 do
    TReaderObject(ReaderListBox.Items.Objects[i]).Free;

  if FPCSCDeviceContext <> 0 then
  begin
    FPCSCRaw.SCardCancel(FPCSCDeviceContext);
    FPCSCRaw.SCardReleaseContext(FPCSCDeviceContext);
  end;
  FReaderListThread.Free;
  FPCSCRaw.Shutdown;
  FPCSCRaw.Free;
  inherited;
end;

procedure TPCSCCardReader.InitPCSC;
var
  PCSCResult:DWORD;
begin
  if FPCSCRaw.Initialize <> TPCSC_INIT_OK then
    AddErrorLog('Cannot access Winscard.dll.',clRed,true,true)
  else
    AddLog('Winscard.dll successfully loaded.',clGreen,true,true);

  // establishing PC/SC context
  PCSCResult := FPCSCRaw.SCardEstablishContext(SCARD_SCOPE_SYSTEM,nil,nil,FPCSCDeviceContext);
  if PCSCResult = SCARD_S_SUCCESS then
    AddLog('SCardEstablishContext succeeded.',clGreen,true,true)
  else
  begin
    AddErrorLog(Format('SCardEstablishContext failed with error code %s ',[IntToHex(PCSCResult,8)]),clRed,false,true);
    AddErrorLog('('+FPCSCRaw.ScErrToSymbol(PCSCResult)+').',clBlack,true,false);
  end;
end;

type
  string2=string[2];

function HexStr2ToByte(HexStr2:string2):byte;
begin
  if HexStr2[1] in ['0'..'9'] then
    result := (byte(HexStr2[1])-byte('0'))*16
  else
    result := (byte(HexStr2[1])-byte('A')+10)*16;
  if HexStr2[2] in ['0'..'9'] then
    result := result+(byte(HexStr2[2])-byte('0'))
  else
    result := result+(byte(HexStr2[2])-byte('A')+10);
end;

procedure TPCSCCardReader.AddLog(Msg: string; Color: TColor; LineBreak,
  Bold: boolean);
begin
  RichEdit.SelAttributes.Color := Color;
  if Bold then
    RichEdit.SelAttributes.Style := [fsBold]
  else
    RichEdit.SelAttributes.Style := [];
  RichEdit.SelText := Msg;
  if LineBreak then
    RichEdit.SelText := #13#10;
  RichEdit.Perform(EM_SCROLLCARET, 0, 0);
end;

procedure TPCSCCardReader.CardStateChanged(Sender: TObject);
var
  CardState:TCardState;
  ReaderName:string;
  sState:string;
begin
  CardState := TReaderObject(Sender).CardState;
  ReaderName := TReaderObject(Sender).ReaderName;
  case CardState of
    csExclusive : sState := 'exclusive';
    csShared    : sState := 'shared';
    csAvailable : sState := 'available';
    csBadCard   : sState := 'bad card';
    csNoCard    : sState := 'no card';
    else          sState := 'unknown';
  end;
  AddLog('Card State changed in '+ReaderName+' to ',clBlack,false,false);
  AddLog(sState,clBlue,true,true);
  ReaderListBox.Repaint;
  UpdateButtons;
  // MRA
//  if cBoxReadIDCard.Checked then
    if CardState = csAvailable then
      if Assigned(MyReadCardID) then
        MyReadCardID;
end;

function TPCSCCardReader.ConnectExclusive: Boolean;
var
  PCSCResult:DWORD;
  ReaderObject:TReaderObject;
begin
  Result := False;
  try
    if ReaderListBox.ItemIndex<0 then
      exit;
    ReaderObject := TReaderObject(ReaderListBox.Items.Objects[ReaderListBox.ItemIndex]);
    PCSCResult := ReaderObject.SCConnect(FPCSCRaw,FPCSCDeviceContext,SCARD_SHARE_EXCLUSIVE);
    if PCSCResult=SCARD_S_SUCCESS then
    begin
      Result := True;
      AddLog('SCardConnect succeeded.',clGreen,true,true);
    end
    else
    begin
      AddErrorLog(Format('SCardConnect failed with error code %s ',[IntToHex(PCSCResult,8)]),clRed,false,true);
      AddErrorLog('('+FPCSCRaw.ScErrToSymbol(PCSCResult)+').',clBlack,true,false);
    end;
  finally
    UpdateButtons;
  end;
end;

function TPCSCCardReader.Disconnect: Boolean;
var
  PCSCResult:DWORD;
  ReaderObject:TReaderObject;
begin
  Result := False;
  try
    if ReaderListBox.ItemIndex<0 then
      exit;
    ReaderObject := TReaderObject(ReaderListBox.Items.Objects[ReaderListBox.ItemIndex]);
    PCSCResult := ReaderObject.SCDisconnect(FPCSCRaw,FPCSCDeviceContext);
    if PCSCResult=SCARD_S_SUCCESS then
    begin
      Result := True;
      AddLog('SCardDisconnect succeeded.',clGreen,true,true);
    end
    else
    begin
      AddErrorLog(Format('SCardDisconnect failed with error code %s ',[IntToHex(PCSCResult,8)]),clRed,false,true);
      AddErrorLog('('+FPCSCRaw.ScErrToSymbol(PCSCResult)+').',clBlack,true,false);
    end;
  finally
    UpdateButtons;
  end;
end;

procedure TPCSCCardReader.GetPCSCReaderList(ReaderList: TStringList);
var
  pReaders:PChar;
  PCSCResult:DWORD;
  SizeReaders:DWORD;
begin
  ReaderList.Clear;

  PCSCResult := FPCSCRaw.SCardListReaders(FPCSCDeviceContext, nil, nil, SizeReaders);
  if PCSCResult = SCARD_S_SUCCESS then
  begin
    GetMem(pReaders,SizeReaders);
    try
      PCSCResult := FPCSCRaw.SCardListReaders(FPCSCDeviceContext, nil, pReaders, SizeReaders);
      if PCSCResult = SCARD_S_SUCCESS then
      begin
        MultiStrToStringList(pReaders,SizeReaders,ReaderList);
        AddLog('SCardListReaders succeeded.',clGreen,true,true);
      end;
    finally
      if pReaders<>nil then
        FreeMem(pReaders);
    end;
  end
  else
  begin
    AddErrorLog(Format('SCardListReaders failed with error code %s ',[IntToHex(PCSCResult,8)]),clRed,false,true);
    AddErrorLog('('+FPCSCRaw.ScErrToSymbol(PCSCResult)+').',clBlack,true,false);
  end;
end;

procedure TPCSCCardReader.LogInBuffer(Buffer: PByteArray;
  BufferSize: DWORD);
var
  s:string;
  i:integer;
begin
  AddLog('Sending APDU to card: ',clBlack,false);
  s := '';
  for i := 0 to BufferSize-1 do
    s := s+IntToHex(Buffer^[i],2)+' ';
  AddLog(s,clPurple, true, true);
end;

procedure TPCSCCardReader.LogOutBuffer(Buffer: PByteArray;
  BufferSize: DWORD);
var
  s:string;
  i:integer;
  SW12:Word;
begin
  if BufferSize<2 then
    exit;
  SW12 := (Buffer[BufferSize-2]shl 8) or Buffer[BufferSize-1];
  AddLog('Card response status word: ',clBlack,false);
  AddLog(IntToHex(SW12,4),clPurple,true,true);

  BufferSize := BufferSize-2;
  if Buffersize>0 then
  begin
    AddLog('Card response data: ',clBlack,false);
    s := '';
    for i := 0 to BufferSize-1 do
      s := s+IntToHex(Buffer^[i],2)+' ';
    MyHexBuffer := s;
    AddLog(s,clPurple, true, true);
  end;
end;

procedure TPCSCCardReader.ReaderListChanged;
begin
  AddLog('Reader list changed',clMaroon);
  UpdatePCSCReaderList;
  if (ReaderListBox.ItemIndex<1) and (ReaderListBox.Items.Count>0) then
    ReaderListBox.ItemIndex := 0;
  UpdateButtons;
end;

function TPCSCCardReader.StringToBuffer(Command: string;
  Buffer: PByteArray): DWORD;
const
  HexChars=['A'..'F','0'..'9'];
var
  s:string;
  i:integer;
begin
  result := 0;
  if Buffer=nil then
    exit;
  Command := UpperCase(Command);
  s := '';
  for i := 1 to length(Command) do
    if Command[i] in HexChars then
      s := s+Command[i];
  if length(s) mod 2<>0 then
    s := s+'0';
  result := length(s) div 2;
  for i := 0 to result-1 do
  begin
   Buffer[i] := HexStr2ToByte(copy(s,i*2+1,2));
  end;
end;

function TPCSCCardReader.Transmit: Boolean;
var
  InSize:DWORD;
  OutSize:DWORD;
  PCSCResult:DWORD;
  ReaderObject:TReaderObject;
  InBuffer:array[0..260] of byte;
  OutBuffer:array[0..260] of byte;
  ThisBuffer: String;
begin
  Result := False;
  ThisBuffer := '';
  MyBuffer := '';
  try
    if ReaderListBox.ItemIndex<0 then
      exit;
    ReaderObject := TReaderObject(ReaderListBox.Items.Objects[ReaderListBox.ItemIndex]);
    if CommandComboBox.Items.IndexOf(lowercase(trim(CommandComboBox.Text)))<0 then
      CommandComboBox.Items.Insert(0,CommandComboBox.Text);
    InSize := StringToBuffer(CommandComboBox.Text,@InBuffer[0]);
    CommandComboBox.Text := '';
    LogInBuffer(@InBuffer[0],InSize);
    OutSize := sizeof(OutBuffer);
    PCSCResult := ReaderObject.SCTransmit(FPCSCRaw,
      FPCSCDeviceContext,@InBuffer[0],@OutBuffer[0],InSize,OutSize);
    if PCSCResult=SCARD_S_SUCCESS then
    begin
      Result := True;
      SetString(ThisBuffer, PAnsiChar(@OutBuffer[0]), OutSize);
      MyBuffer := ThisBuffer;
      AddLog('SCardTransmit succeeded.',clGreen,true,true);
      LogOutBuffer(@OutBuffer[0],OutSize);
    end
    else
    begin
      AddErrorLog(Format('SCardTransmit failed with error code %s ',[IntToHex(PCSCResult,8)]),clRed,false,true);
      AddErrorLog('('+FPCSCRaw.ScErrToSymbol(PCSCResult)+').',clBlack,true,false);
    end;
  finally
    UpdateButtons;
  end;
end;

function TPCSCCardReader.TransmitCLICC: Boolean;
var
  InSize:DWORD;
  OutSize:DWORD;
  PCSCResult:DWORD;
  ReaderObject:TReaderObject;
  InBuffer:array[0..260] of byte;
  OutBuffer:array[0..260] of byte;
  ThisBuffer: String;
begin
  Result := False;
  ThisBuffer := '';
  MyBuffer := '';
  try
    if ReaderListBox.ItemIndex<0 then
      exit;
    ReaderObject := TReaderObject(ReaderListBox.Items.Objects[ReaderListBox.ItemIndex]);
    if CommandComboBox.Items.IndexOf(lowercase(trim(CommandComboBox.Text)))<0 then
      CommandComboBox.Items.Insert(0,CommandComboBox.Text);
    InSize := StringToBuffer(CommandComboBox.Text,@InBuffer[0]);
    CommandComboBox.Text := '';
    LogInBuffer(@InBuffer[0],InSize);
    OutSize := sizeof(OutBuffer);
//    PCSCResult := ReaderObject.SCTransmit(FPCSCRaw,
//      FPCSCDeviceContext,@InBuffer[0],@OutBuffer[0],InSize,OutSize);
{
function MySCardCLICCTransmit(
  ASCardHandle: DWord; // Cardinal;
  ApucSendData: Pointer;
  AulSendDataBufLen: DWord;
  ApucReceivedData: Pointer;
  var ApulReceivedDataBufLen: DWord): DWord;
}
    PCSCResult := MySCardCLICCTransmit(
      ReaderObject.CardHandle,
      @InBuffer[0],
      InSize,
      @OutBuffer[0],
      OutSize);
    if PCSCResult=SCARD_S_SUCCESS then
    begin
      Result := True;
      SetString(ThisBuffer, PAnsiChar(@OutBuffer[0]), OutSize);
      MyBuffer := ThisBuffer;
      AddLog('SCardTransmit succeeded.',clGreen,true,true);
      LogOutBuffer(@OutBuffer[0],OutSize);
    end
    else
    begin
      AddErrorLog(Format('SCardTransmit failed with error code %s ',[IntToHex(PCSCResult,8)]),clRed,false,true);
      AddErrorLog('('+FPCSCRaw.ScErrToSymbol(PCSCResult)+').',clBlack,true,false);
    end;
  finally
    UpdateButtons;
  end;
end;

procedure TPCSCCardReader.UpdateButtons;
begin
//
end;

procedure TPCSCCardReader.UpdatePCSCReaderList;
var
  i,j:integer;
  Found:boolean;
  ReaderName:string;
  ReaderList:TStringList;
  ReaderObject:TReaderObject;
begin
  ReaderList := TStringList.Create;
  try
    GetPCSCReaderList(ReaderList);
    for i := ReaderListBox.Items.Count-1 downto 0 do
    begin
      ReaderName := ReaderListBox.Items[i];
      Found := false;
      for j := 0 to ReaderList.Count-1 do
      begin
        if ReaderName=ReaderList[j] then
        begin
          Found := true;
          break;
        end;
      end;
      if not Found then
      begin
        AddErrorLog('Reader removed: '+ReaderName,clMaroon);
        TReaderObject(ReaderListBox.Items.Objects[i]).Free;
        ReaderListBox.Items.Delete(i);
      end;
    end;

    for i := 0 to ReaderList.Count-1 do
    begin
      Found := false;
      for j := 0 to ReaderListBox.Items.Count-1 do
      begin
        ReaderName := ReaderListBox.Items[j];
        if ReaderName=ReaderList[i] then
        begin
          Found := true;
          break;
        end;
      end;
      if not Found then
      begin
        ReaderName := ReaderList[i];
        ReaderObject := TReaderObject.Create(ReaderName,FPCSCRaw);
        ReaderObject.OnCardStateChanged := CardStateChanged;
        ReaderListBox.Items.AddObject(ReaderName,ReaderObject);
        AddLog('New reader found: '+ReaderName,clMaroon);
      end;
    end;
  finally
    ReaderList.Free;
  end;
end;

function TPCSCCardReader.ConnectShared: Boolean;
var
  PCSCResult:DWORD;
  ReaderObject:TReaderObject;
begin
  Result := False;
  try
    if ReaderListBox.ItemIndex<0 then
      exit;
    ReaderObject := TReaderObject(ReaderListBox.Items.Objects[ReaderListBox.ItemIndex]);
    PCSCResult := ReaderObject.SCConnect(FPCSCRaw,FPCSCDeviceContext,SCARD_SHARE_SHARED);
    if PCSCResult=SCARD_S_SUCCESS then
    begin
      Result := True;
      AddLog('SCardConnect succeeded.',clGreen,true,true);
    end
    else
    begin
      AddErrorLog(Format('SCardConnect failed with error code %s ',[IntToHex(PCSCResult,8)]),clRed,false,true);
      AddErrorLog('('+FPCSCRaw.ScErrToSymbol(PCSCResult)+').',clBlack,true,false);
    end;
  finally
    UpdateButtons;
  end;
end;

// Wrapper function to call a dll-function from 'scardsyn.dll'.
// For use with HID OMNIKEY 5321 and ICLASS card.
function MySCardCLICCTransmit(
  ASCardHandle: DWord; // Cardinal;
  ApucSendData: Pointer;
  AulSendDataBufLen: DWord;
  ApucReceivedData: Pointer;
  var ApulReceivedDataBufLen: DWord): DWord;
type
{
  TSCardCLICCTransmit = function(SCardhandle: DWORD; // Cardinal;
    pucSendData: Pointer;
    ulSendDataBufLen: DWORD;
    pucReceivedData: Pointer;
    var pulReceivedDataBufLen: DWORD): Longint;
}
  TSCardCLICCTransmit = function(SCardhandle: LONGWORD;
    pucSendData: Pointer;
    ulSendDataBufLen: LONGWORD;
    pucReceivedData: Pointer;
    var pulReceivedDataBufLen: LONGWORD): LONGWORD; stdcall;
var
  SCardCLICCTransmit: TSCardCLICCTransmit;
  hDLL: Integer;
begin
  Result := 1;
  hDLL := LoadLibrary('scardsyn.dll');
  if hDLL <> 0 then
  begin
    @SCardCLICCTransmit := GetProcAddress(hDLL, 'SCardCLICCTransmit');
    if @SCardCLICCTransmit <> nil then
    begin
      Result := SCardCLICCTransmit(ASCardHandle, ApucSendData,
        AulSendDataBufLen, ApucReceivedData, ApulReceivedDataBufLen);
    end;
    FreeLibrary(hDLL);
  end;
end; // MyScardCLICCTransmit

procedure TPCSCCardReader.AddErrorLog(Msg:string;Color:TColor=clBlack;
  LineBreak:boolean=true;Bold:boolean=false);
begin
  AddLog(Msg, Color, LineBreak, Bold);
  if Assigned(MyAddErrorLog) then
    MyAddErrorLog(Msg);
end;

end.
