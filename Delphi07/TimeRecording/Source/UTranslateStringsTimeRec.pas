(*
  MRA:23-JAN-2014 TD-24046
  - Translate issues: Use a different method to translate.
    - Do NOT do this in separate files where the translation is done
      hard-coded!
    - New method:
      - Use 1 resource-strings-file that contains the translation-strings.
      - Use 1 translate-unit that assigns these strings to the properties
        in the forms.
      - In this way there is only 1 form that can be translated in multiple
        languages when the application is run.
      - The project-file defines the language using a condition.
*)
unit UTranslateStringsTimeRec;

interface

resourceString

{$IFDEF PIMSDUTCH}
  STransSelectWorkspot = 'Selecteer Werkplek';
  STransBtnOK = '&OK';
  STransBtnCancel = '&ANNUL.';
  STransBtnEndOfDay = '&EINDE';
  STransTimeRecording = 'Tijdregistratie';
  STransEmployee = 'Werknemer';
  STransEmp = 'Werkn.';
  STransIDCard = 'Pas';
  STransBtnAccept = 'Accoord';
  STransDateAndTime = 'Datum en tijd';
  STransCurrentDate = 'Datum';
  STransWorkedTimeHM = 'Gewerkte tijd (uren:minuten)';
  STransLastWeek = 'Vorige week';
  STransThisWeek = 'Deze week';
  STransTimeHM = 'Tijd (uren:minuten)';
  STransTimeForTime = 'Tijd voor Tijd';
  STransHoliday = 'Vakantie';
  STransWorkTimeReduction = 'Variabele werktijd';
  STransMessages = 'Melding';
  STransScanned = 'Gescand';
  STransSmartCardInfo = 'Smart Card Info';
  STransCardReaders = 'Card Readers';
  STransLogMessage = 'Log Messages';
  STransWorkspotJob = 'Werkplek/ Job';
  STransPlant = 'Bedrijf';
  STransFrom = 'Van';
  STransJob = 'Job';
  STransWorkspot = 'Werkplek';
  // DialogAskEmployeeNumber
  STransEnterEmplNr = 'Invoer Werknemernummer';
  STransEmpNr = 'Werknemernummer';

{$ELSE}
  {$IFDEF PIMSFRENCH}
  STransSelectWorkspot = 'S�lecte lieu de travail';
  STransBtnOK = '&OK';
  STransBtnCancel = '&ANNULE';
  STransBtnEndOfDay = '&FIN';
  STransTimeRecording = 'Enregistrement du temps';
  STransEmployee = 'Employ�';
  STransEmp = 'Emp.';
  STransIDCard = 'Carte d''identit�';
  STransBtnAccept = 'D''accord';
  STransDateAndTime = 'Date et temps';
  STransCurrentDate = 'Date';
  STransWorkedTimeHM = 'Le temps de travail (heure:minutes)';
  STransLastWeek = 'La semaine pass�e';
  STransThisWeek = 'Cette semaine';
  STransTimeHM = 'Temps (heure:minutes)';
  STransTimeForTime = 'Temps pour Temps';
  STransHoliday = 'Vacances';
  STransWorkTimeReduction = 'Temps de travail variable';
  STransMessages = 'Message';
  STransScanned = 'Scann�';
  STransSmartCardInfo = 'Smart Card Info';
  STransCardReaders = 'Card Readers';
  STransLogMessage = 'Log Messages';
  STransWorkspotJob = 'Lieu de travail/ Travail';
  STransPlant = 'Entreprise';
  STransFrom = 'De';
  STransJob = 'Travail';
  STransWorkspot = 'Lieu de travail';
  // DialogAskEmployeeNumber
  STransEnterEmplNr = 'Entre le num�ro d''employ�';
  STransEmpNr = 'Num�ro d''employ�';

{$ELSE}
  {$IFDEF PIMSGERMAN}
  STransSelectWorkspot = 'Selektiere Arbeitsplatz';
  STransBtnOK = '&OK';
  STransBtnCancel = '&ANNUL.';
  STransBtnEndOfDay = '&ENDE';
  STransTimeRecording = 'Zeiterfassung';
  STransEmployee = 'Mitarbeiter';
  STransEmp = 'Mitarb.';
  STransIDCard = 'ID-Karte';
  STransBtnAccept = 'Akzeptier';
  STransDateAndTime = 'Datum und Zeit';
  STransCurrentDate = 'Jetzige Datum';
  STransWorkedTimeHM = 'Arbeitszeit (Stunden:Minuten)';
  STransLastWeek = 'Letzte Woche';
  STransThisWeek = 'Diese Woche';
  STransTimeHM = 'Zeit (Stunden:Minuten)';
  STransTimeForTime = 'Zeit f�r Zeit';
  STransHoliday = 'Urlaub';
  STransWorkTimeReduction = 'Gleitzeit';
  STransMessages = 'Meldung';
  STransScanned = 'Gescant';
  STransSmartCardInfo = 'Smart Card Info';
  STransCardReaders = 'Card Readers';
  STransLogMessage = 'Log Messages';
  STransWorkspotJob = 'Arbeitsplatz/Job';
  STransPlant = 'Betrieb';
  STransFrom = 'Von';
  STransJob = 'Job';
  STransWorkspot = 'A-Platz';
  // DialogAskEmployeeNumber
  STransEnterEmplNr = 'Eingabe Mitarbeiternummer';
  STransEmpNr = 'Mitarbeiternummer';

{$ELSE}
  {$IFDEF PIMSDANISH}
  STransSelectWorkspot = 'V�lg arb.sted';
  STransBtnOK = '&OK';
  STransBtnCancel = '&ANNUL.';
  STransBtnEndOfDay = '&SLUT';
  STransTimeRecording = 'Tidsregistr.';
  STransEmployee = 'Medarbejder';
  STransEmp = 'Medarb.';
  STransIDCard = 'ID Kort';
  STransBtnAccept = '&Accept';
  STransDateAndTime = 'Dato og tid';
  STransCurrentDate = 'Denne dato';
  STransWorkedTimeHM = 'Arb. tid (timer:min.)';
  STransLastWeek = 'Forr. uge';
  STransThisWeek = 'Denne uge';
  STransTimeHM = 'Tid (timer:min.)';
  STransTimeForTime = 'Time for Time';
  STransHoliday = 'Ferie';
  STransWorkTimeReduction = 'Arb. tid Reduktion';
  STransMessages = 'Beskeder';
  STransScanned = 'Skannet';
  STransSmartCardInfo = 'Smart Card Info';
  STransCardReaders = 'Kort l�sere';
  STransLogMessage = 'Log beskeder';
  STransWorkspotJob = 'Arbejdsplads / Job';
  STransPlant = 'Vaskeri';
  STransFrom = 'Fra';
  STransJob = 'Job';
  STransWorkspot = 'Arb. plads';
  // DialogAskEmployeeNumber
  STransEnterEmplNr = 'Indtaste medarbejdernummer';
  STransEmpNr = 'Medarbejdernummer';

{$ELSE}
  {$IFDEF PIMSJAPANESE}
  STransSelectWorkspot = 'Select Workspot';
  STransBtnOK = '&OK';
  STransBtnCancel = '&CANCEL';
  STransBtnEndOfDay = '&END';
  STransTimeRecording = 'Time Recording';
  STransEmployee = 'Employee';
  STransEmp = 'Employee';
  STransIDCard = 'ID Card';
  STransBtnAccept = '&Accept';
  STransDateAndTime = 'Date and Time';
  STransCurrentDate = 'Current Date';
  STransWorkedTimeHM = 'Worked Time (hours:minutes)';
  STransLastWeek = 'Last Week';
  STransThisWeek = 'This Week';
  STransTimeHM = 'Time (hours:minutes)';
  STransTimeForTime = 'Time for Time';
  STransHoliday = 'Holiday';
  STransWorkTimeReduction = 'Work Time Reduction';
  STransMessages = 'Messages';
  STransScanned = 'Scanned';
  STransSmartCardInfo = 'Smart Card Info';
  STransCardReaders = 'Card Readers';
  STransLogMessage = 'Log Messages';
  STransWorkspotJob = 'Workspot / Job';
  STransPlant = 'Plant';
  STransFrom = 'From';
  STransJob = 'Job';
  STransWorkspot = 'Workspot';
  // DialogAskEmployeeNumber
  STransEnterEmplNr = 'Enter Employee Number';
  STransEmpNr = 'Employee Number';

{$ELSE}
  {$IFDEF PIMSCHINESE}
  STransSelectWorkspot = 'ѡ������';
  STransBtnOK = '&OK';
  STransBtnCancel = '&ȡ��L';
  STransBtnEndOfDay = '&����';
  STransTimeRecording = 'ʱ���¼';
  STransEmployee = 'Ա��';
  STransEmp = 'Ա��';
  STransIDCard = 'ID ��';
  STransBtnAccept = '&����';
  STransDateAndTime = '������ʱ��';
  STransCurrentDate = '��ǰ����';
  STransWorkedTimeHM = '����ʱ�� (Сʱ:����)';
  STransLastWeek = '����';
  STransThisWeek = '����';
  STransTimeHM = 'ʱ�� (Сʱ:����)';
  STransTimeForTime = 'ʱ��';
  STransHoliday = '�ڼ���';
  STransWorkTimeReduction = '����ʱ�����';
  STransMessages = '��Ϣ';
  STransScanned = '��ɨ��';
  STransSmartCardInfo = 'Smart ����Ϣ';
  STransCardReaders = '������';
  STransLogMessage = '��¼��Ϣ';
  STransWorkspotJob = '������ / ����';
  STransPlant = '����';
  STransFrom = '����';
  STransJob = '����';
  STransWorkspot = '������';
  // DialogAskEmployeeNumber
  STransEnterEmplNr = '����Ա����';
  STransEmpNr = 'Ա����';
{$ELSE}
  {$IFDEF PIMSNORWEGIAN}
  STransSelectWorkspot = 'Velg arbeidsposisjon';
  STransBtnOK = '&OK';
  STransBtnCancel = '&AVBRYT';
  STransBtnEndOfDay = '&AVSLUTT';
  STransTimeRecording = 'Tids registrering';
  STransEmployee = 'Ansatt';
  STransEmp = 'Ansatt';
  STransIDCard = 'ID Kort';
  STransBtnAccept = '&Akseptert';
  STransDateAndTime = 'Dato og Tid';
  STransCurrentDate = 'Aktuell Dato';
  STransWorkedTimeHM = 'Jobbet tid (Time:Minutter)';
  STransLastWeek = 'Forrige uke';
  STransThisWeek = 'Denne uken';
  STransTimeHM = 'Tid (Time:minutter)';
  STransTimeForTime = 'Tid for Tid';
  STransHoliday = 'Ferie';
  STransWorkTimeReduction = 'Arbeisdstid reduksjon';
  STransMessages = 'Beskjed';
  STransScanned = 'Skannet';
  STransSmartCardInfo = 'Smart Card Info';
  STransCardReaders = 'Kort lesere';
  STransLogMessage = 'Logg Beskjeder';
  STransWorkspotJob = 'Arbeidsposisjon / Jobb';
  STransPlant = 'Anlegg';
  STransFrom = 'Fra';
  STransJob = 'Jobb';
  STransWorkspot = 'Arbeidsposisjon';
  // DialogAskEmployeeNumber
  STransEnterEmplNr = 'Skriv inn Ansatt Nr';
  STransEmpNr = 'Ansatt Nr';
{$ELSE}
// ENGLISH (default language)
  STransSelectWorkspot = 'Select Workspot';
  STransBtnOK = '&OK';
  STransBtnCancel = '&CANCEL';
  STransBtnEndOfDay = '&END';
  STransTimeRecording = 'Time Recording';
  STransEmployee = 'Employee';
  STransEmp = 'Employee';
  STransIDCard = 'ID Card';
  STransBtnAccept = '&Accept';
  STransDateAndTime = 'Date and Time';
  STransCurrentDate = 'Current Date';
  STransWorkedTimeHM = 'Worked Time (hours:minutes)';
  STransLastWeek = 'Last Week';
  STransThisWeek = 'This Week';
  STransTimeHM = 'Time (hours:minutes)';
  STransTimeForTime = 'Time for Time';
  STransHoliday = 'Holiday';
  STransWorkTimeReduction = 'Work Time Reduction';
  STransMessages = 'Messages';
  STransScanned = 'Scanned';
  STransSmartCardInfo = 'Smart Card Info';
  STransCardReaders = 'Card Readers';
  STransLogMessage = 'Log Messages';
  STransWorkspotJob = 'Workspot / Job';
  STransPlant = 'Plant';
  STransFrom = 'From';
  STransJob = 'Job';
  STransWorkspot = 'Workspot';
  // DialogAskEmployeeNumber
  STransEnterEmplNr = 'Enter Employee Number';
  STransEmpNr = 'Employee Number';

            {$ENDIF}
          {$ENDIF}
        {$ENDIF}
      {$ENDIF}
    {$ENDIF}
  {$ENDIF}
{$ENDIF}

implementation

end.
