inherited DialogAskEmployeeNumberF: TDialogAskEmployeeNumberF
  Left = 429
  Top = 306
  Height = 229
  Caption = 'Enter Employee Number'
  Position = poOwnerFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  inherited stbarBase: TStatusBar
    Top = 88
  end
  inherited pnlInsertBase: TPanel
    Height = 51
    Font.Color = clBlack
    object GroupBox1: TGroupBox
      Left = 0
      Top = 0
      Width = 624
      Height = 51
      Align = alClient
      Caption = 'Enter Employee Number'
      TabOrder = 0
      object Label1: TLabel
        Left = 8
        Top = 32
        Width = 155
        Height = 23
        Caption = 'Employee Number'
      end
      object EditEmpNr: TEdit
        Left = 192
        Top = 29
        Width = 417
        Height = 31
        TabOrder = 0
      end
    end
  end
  inherited pnlBottom: TPanel
    Top = 107
    inherited btnOK: TBitBtn
      OnClick = btnOkClick
    end
  end
end
