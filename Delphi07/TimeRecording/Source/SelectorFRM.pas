(*
  MRA:2-SEP-2011 RV079.1. Update. 20012013.
  - Make it possible to maximize the dialog.
  - Make the buttons bigger (higher),
    but only when maximized, or by using
    a setting?
  MRA:19-SEP-2011 RV080.1. Update. 20012013.
  - Make font of grid larger when maximized.
  MRA:3-JUL-2015 20014450.50 / PIM12
  - Real Time Efficiency
  - End-Of-Day-button blink at end-of-shift
  MRA:28-SEP-2015 PIM-12
  - Optionally show End-Of-Day-button
  - Also: Set End-Of-Day-button to original color in case it was blinking
    previously.
  MRA:30-DEC-2015 PIM-112
  - Addition of break/lunch-buttons for Timerecording-application.
  - These must be shown optionally based on system-settings.
  - Also changed: 'Options -> dgAlwaysShowSelection' for TDBGrid set to True,
    because it did not show the selected line in color after up/down.
  - Also changed: At the right part up/down buttons are now added. 
  MRA:27-JAN-2016 PIM-139
  - Lunch button optionally scan out.
  MRA:3-OCT-2016 PIM-228
  - Move break (+lunch) button to first screen.
*)
unit SelectorFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BasePimsFRM, StdCtrls, Buttons, Grids, DBGrids, DB, ExtCtrls, ImgList,
  UPimsConst, ORASystemDMT;

const
	ModalOK       = 1;
  ModalCancel   = 2;
  ModalEndofDay = 3;
  TimeOutSecs   = 60;

type

  TFormPos = record
    Top, Left, Height, Width: Integer;
  end;

  TSelectorF = class(TBasePimsForm)
    PanelButtons: TPanel;
    PanelGrid: TPanel;
    DBGridSelector: TDBGrid;
    PanelControls: TPanel;
    BitBtnOk: TBitBtn;
    BitBtnCancel: TBitBtn;
    BitBtnEndOfDay: TBitBtn;
    PanelBFirst: TPanel;
    SpeedButtonFirst: TBitBtn;
    PanelBLast: TPanel;
    PanelBPrior: TPanel;
    SpeedButtonPrior: TBitBtn;
    PanelNext: TPanel;
    SpeedButtonNext: TBitBtn;
    SpeedButtonLast: TBitBtn;
    tmrCancel: TTimer;
    tmrEndOfDayBlink: TTimer;
    btnBreak: TBitBtn;
    btnLunch: TBitBtn;
    Panel1: TPanel;
    btnFirst: TBitBtn;
    btnUp: TBitBtn;
    btnLast: TBitBtn;
    btnLow: TBitBtn;
    procedure BitBtnEndOfDayClick(Sender: TObject);
    procedure SpeedButtonFirstClick(Sender: TObject);
    procedure SpeedButtonPriorClick(Sender: TObject);
    procedure SpeedButtonNextClick(Sender: TObject);
    procedure SpeedButtonLastClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure tmrCancelTimer(Sender: TObject);
    procedure tmrEndOfDayBlinkTimer(Sender: TObject);
    procedure btnBreakClick(Sender: TObject);
    procedure btnLunchClick(Sender: TObject);
    procedure btnUpClick(Sender: TObject);
    procedure btnLowClick(Sender: TObject);
  private
    { Private declarations }
    SpeedButtonFirstHeight, SpeedButtonPriorHeight, SpeedButtonNextHeight,
    SpeedButtonLastHeight, BitBtnOkHeight, BitBtnCancelHeight,
    BitBtnEndOfDayHeight, PanelButtonsHeight: Integer;
    SpeedButtonFirstWidth, SpeedButtonPriorWidth, SpeedButtonNextWidth,
    SpeedButtonLastWidth: Integer;
    PanelBFirstWidth, PanelBPriorWidth, PanelNextWidth,
    PanelBLastWidth, DBGridSelectorFontSize: Integer;
    FIsMaximized: Boolean;
    FOldJobCode: String;
    FDisableCancelTimer: Boolean;
    FEndOfDayBlink: Boolean;
    EndOfDayColor: TColor;
    ButtonOkLeft: Integer;
    ButtonCancelLeft: Integer;
    ButtonEndOfDayLeft: Integer;
    FEndOfDayBtnVisible: Boolean;
    PanelControlsLeft: Integer;
    PanelControlsWidth: Integer;
    PanelControlsNewLeft: Integer;
    PanelControlsNewWidth: Integer;
    FJobSelection: Boolean;
    procedure WMSysCommand(var Msg: TWMSysCommand); message WM_SYSCOMMAND;
  public
    { Public declarations }
    function EnableSelection(var AFormPos: TFormPos;
       const AColumnNames: array of String; ADataSource: TDataSource): Integer;
    property IsMaximized: Boolean read FIsMaximized write FIsMaximized;
    property OldJobCode: String read FOldJobCode write FOldJobCode;
    property DisableCancelTimer: Boolean
      read FDisableCancelTimer write FDisableCancelTimer; // 20015302
    property EndOfDayBlink: Boolean read FEndOfDayBlink write FEndOfDayBlink; // 20014450.50
    property EndOfDayBtnVisible: Boolean read FEndOfDayBtnVisible write FEndOfDayBtnVisible; // PIM-12
    property JobSelection: Boolean read FJobSelection write FJobSelection; // PIM-228 
  end;

var
  SelectorF: TSelectorF;

implementation

uses
  UPimsMessageRes, UProductionScreenDefs;

{$R *.DFM}

{ TSelectorF }

procedure TSelectorF.BitBtnEndOfDayClick(Sender: TObject);
begin
  inherited;
  ModalResult := ModalEndOfDay;
end;

function TSelectorF.EnableSelection(var AFormPos: TFormPos;
  const AColumnNames: array of String;
  ADataSource: TDataSource): Integer;
var
	ColIndex : Integer;
begin
  BitBtnEndOfDay.Visible := EndOfDayBtnVisible; // PIM-12
  // Structure of ColumnNames
  // 0. Selector form Caption
  // 1 - x grid column text
  // only 2 columns - set width based on this
  ModalResult := ModalCancel;
  // RV080.1. Do this here!
  Top    := AFormPos.Top;
  Left   := AFormPos.Left;
  Height := AFormPos.Height;
  Width  := AFormPos.Width;
  DBGridSelector.Columns.Clear;
  if High(AColumnNames) > 0 then
    Caption := AColumnNames[0];
  for ColIndex := 1 to (High(AColumnNames)) do
  begin
    DBGridSelector.Columns.Add;
    DBGridSelector.Columns[ColIndex - 1].Field :=
    	ADataSource.DataSet.Fields[ColIndex - 1];
    DBGridSelector.Columns[ColIndex - 1].Title.Caption :=
      AColumnNames[ColIndex];
    if (AColumnNames[ColIndex] = SPimsColumnCode) or
       (AColumnNames[ColIndex] = SPimsColumnPlant) then
      DBGridSelector.Columns[ColIndex - 1].Width :=
        Round(DBGridSelector.Width / (High(AColumnNames) + 2))
    else
      DBGridSelector.Columns[ColIndex - 1].Width :=
     	  DBGridSelector.Width - DBGridSelector.Columns[ColIndex - 2].Width;
  end;
  DBGridSelector.DataSource := ADataSource;
  if JobSelection then // PIM-228
    if OldJobCode <> '' then
      DBGridSelector.DataSource.DataSet.Locate('JOB_CODE', OldJobCode, []);
  // RV080.1. Moved to higher pos.
{  Top    := AFormPos.Top;
  Left   := AFormPos.Left;
  Height := AFormPos.Height;
  Width  := AFormPos.Width; }
  // MR: 01-10-2007 F10-button was disabled? Why was not clear, so
  // we set all to enabled to be sure.
  BitBtnOK.Enabled := True;
  BitBtnCancel.Enabled := True;
  if BitBtnEndOfDay.Visible then // PIM-12
    BitBtnEndOfDay.Enabled := True;
  if btnBreak.Visible then // PIM-112
    btnBreak.Enabled := OldJobCode <> BREAKJOB;
  if btnLunch.Visible then // PIM-139
  begin
    if not ORASystemDM.LunchBtnScanOut then
      btnLunch.Enabled := OldJobCode <> BREAKJOB;
  end;
  Result := ShowModal;
end;

procedure TSelectorF.SpeedButtonFirstClick(Sender: TObject);
begin
  inherited;
  DBGridSelector.DataSource.DataSet.First;
end;

procedure TSelectorF.SpeedButtonPriorClick(Sender: TObject);
begin
  inherited;
  DBGridSelector.DataSource.DataSet.Prior;
end;

procedure TSelectorF.SpeedButtonNextClick(Sender: TObject);
begin
  inherited;
  DBGridSelector.DataSource.DataSet.Next;
end;

procedure TSelectorF.SpeedButtonLastClick(Sender: TObject);
begin
  inherited;
	DBGridSelector.DataSource.DataSet.Last;
end;

procedure TSelectorF.FormShow(Sender: TObject);
begin
  inherited;
  BitBtnEndOfDay.Visible := EndOfDayBtnVisible; // PIM-12
  // PIM-12 Reposition buttons in case End-Of-Day-button is not shown.
  if BitBtnEndOfDay.Visible then
  begin
    BitBtnOk.Left := ButtonOkLeft;
    BitBtnCancel.Left := ButtonCancelLeft;
    BitBtnEndOfDay.Left := ButtonEndOfDayleft;
    PanelControls.Width := PanelControlsWidth;
    PanelControls.Left := PanelControlsLeft;
  end
  else
  begin
    PanelControls.Width := PanelControlsNewWidth;
    PanelControls.Left := PanelControlsNewLeft;
  end;
  DBGridSelector.SetFocus;
  if not DisableCancelTimer then // 20015302
    tmrCancel.Enabled := True;
  if BitBtnEndOfDay.Visible then // PIM-12
  begin
    BitBtnEndOfDay.Color := EndOfDayColor; // PIM-12
    tmrEndOfDayBlink.Enabled := EndOfDayBlink; // PIM-12
  end;
end;

procedure TSelectorF.FormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  case Key of
    VK_F10		: begin
                  if BitBtnEndOfDay.Visible then
                    BitBtnEndOfDayClick(self);
                  Key := 0;
                end;
    VK_ESCAPE : begin
                  ModalResult := ModalCancel;
                  Key := 0;
                end;
    {VK_UP			: begin
                  SpeedButtonPriorClick(self);
                  Key := 0;
                end;
    VK_DOWN		: begin
                  SpeedButtonNextClick(self);
                  Key := 0;
                end;}
    VK_LEFT		: begin
                  SpeedButtonFirstClick(self);
                  Key := 0;
                end;
    VK_RIGHT	: begin
                  SpeedButtonLastClick(self);
                  Key := 0;
                end;
  end;
end;

procedure TSelectorF.FormCreate(Sender: TObject);
begin
  inherited;
  EndOfDayBtnVisible := True; // PIM-12
  EndOfDayColor := BitBtnEndOfDay.Color;
  OldJobCode := ''; // 20015302
  DisableCancelTimer := False; // 20015302
  tmrCancel.Interval := TimeOutSecs * 1000;
  // RV079.1.
  SpeedButtonFirstHeight := SpeedButtonFirst.Height;
  SpeedButtonPriorHeight := SpeedButtonPrior.Height;
  SpeedButtonNextHeight := SpeedButtonNext.Height;
  SpeedButtonLastHeight := SpeedButtonLast.Height;
  SpeedButtonFirstWidth := SpeedButtonFirst.Width;
  SpeedButtonPriorWidth := SpeedButtonPrior.Width;
  SpeedButtonNextWidth := SpeedButtonNext.Width;
  SpeedButtonLastWidth := SpeedButtonLast.Width;
  PanelBFirstWidth := PanelBFirst.Width;
  PanelBPriorWidth := PanelBPrior.Width;
  PanelNextWidth := PanelNext.Width;
  PanelBLastWidth := PanelBLast.Width;
  BitBtnOkHeight := BitBtnOk.Height;
  BitBtnCancelHeight := BitBtnCancel.Height;
  BitBtnEndOfDayHeight := BitBtnEndOfDay.Height;
  PanelButtonsHeight := PanelButtons.Height;
  // RV080.1.
  DBGridSelectorFontSize := DBGridSelector.Font.Size;
  // PIM-12
  ButtonOkLeft := BitBtnOk.Left;
  ButtonCancelLeft := BitBtnCancel.Left;
  ButtonEndOfDayLeft := BitBtnEndOfDay.Left;
  PanelControlsLeft := PanelControls.Left;
  PanelControlsWidth := PanelControls.Width;
  PanelControlsNewLeft := PanelControls.Left + BitBtnEndOfDay.Width;
  PanelControlsNewWidth := PanelControls.Width - BitBtnEndOfDay.Width;
end;

procedure TSelectorF.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  tmrCancel.Enabled := False;
  tmrEndOfDayBlink.Enabled := False;
end;

// MR:01-NOV-2007 Version 2.0.139.14
// Build in a timer for a time-out of 60 seconds. This prevents the creation
// of a record if a user forgot to confirm his/her action. This can happen
// when using more than 1 PC for Timerecording and can lead to wrong
// timerecording-records with wrong hours (too many).
procedure TSelectorF.tmrCancelTimer(Sender: TObject);
begin
  inherited;
  ModalResult := ModalCancel;
  Close;
end;

// 20015302 Prevent it is moved
procedure TSelectorF.WMSysCommand(var Msg: TWMSysCommand);
begin
  if ((Msg.CmdType and $FFF0) = SC_MOVE) or
    ((Msg.CmdType and $FFF0) = SC_SIZE) then
  begin
    Msg.Result := 0;
    Exit;
  end;
  inherited;
end;

// 20014450.50
procedure TSelectorF.tmrEndOfDayBlinkTimer(Sender: TObject);
begin
  inherited;
  if BitBtnEndOfDay.Visible then // PIM-12
  begin
    if BitBtnEndOfDay.Color = EndOfDayColor then
      BitBtnEndOfDay.Color := clMyRed
    else
      BitBtnEndOfDay.Color := EndOfDayColor;
  end;
end;

// PIM-112
procedure TSelectorF.btnBreakClick(Sender: TObject);
begin
  inherited;
  ModalResult := ModalBreak;
end;

// PIM-112
procedure TSelectorF.btnLunchClick(Sender: TObject);
begin
  inherited;
  if ORASystemDM.LunchBtnScanOut then // PIM-139
    ModalResult := ModalLunch
  else
    ModalResult := ModalBreak;
end;

// PIM-112
procedure TSelectorF.btnUpClick(Sender: TObject);
begin
  inherited;
  SpeedButtonPriorClick(Sender);
end;

// PIM-112
procedure TSelectorF.btnLowClick(Sender: TObject);
begin
  inherited;
  SpeedButtonNextClick(Sender);
end;

end.

