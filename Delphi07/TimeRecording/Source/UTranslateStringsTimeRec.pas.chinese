(*
  MRA:23-JAN-2014 TD-24046
  - Translate issues: Use a different method to translate.
    - Do NOT do this in separate files where the translation is done
      hard-coded!
    - New method:
      - Use 1 resource-strings-file that contains the translation-strings.
      - Use 1 translate-unit that assigns these strings to the properties
        in the forms.
      - In this way there is only 1 form that can be translated in multiple
        languages when the application is run.
      - The project-file defines the language using a condition.
*)
unit UTranslateStringsTimeRec;

interface

resourceString

{$IFDEF PIMSDUTCH}
  STransSelectWorkspot = 'Selecteer Werkplek';
  STransBtnOK = '&OK';
  STransBtnCancel = '&ANNUL.';
  STransBtnEndOfDay = '&EINDE';
  STransTimeRecording = 'Tijdregistratie';
  STransEmployee = 'Werknemer';
  STransEmp = 'Werkn.';
  STransIDCard = 'Pas';
  STransBtnAccept = 'Accoord';
  STransDateAndTime = 'Datum en tijd';
  STransCurrentDate = 'Datum';
  STransWorkedTimeHM = 'Gewerkte tijd (uren:minuten)';
  STransLastWeek = 'Vorige week';
  STransThisWeek = 'Deze week';
  STransTimeHM = 'Tijd (uren:minuten)';
  STransTimeForTime = 'Tijd voor Tijd';
  STransHoliday = 'Vakantie';
  STransWorkTimeReduction = 'Variabele werktijd';
  STransMessages = 'Melding';
  STransScanned = 'Gescand';
  STransSmartCardInfo = 'Smart Card Info';
  STransCardReaders = 'Card Readers';
  STransLogMessage = 'Log Messages';
  STransWorkspotJob = 'Werkplek/ Job';
  STransPlant = 'Bedrijf';
  STransFrom = 'Van';
  STransJob = 'Job';
  STransWorkspot = 'Werkplek';
  // DialogAskEmployeeNumber
  STransEnterEmplNr = 'Invoer Werknemernummer';
  STransEmpNr = 'Werknemernummer';

{$ELSE}
  {$IFDEF PIMSFRENCH}
  STransSelectWorkspot = 'Sélecte lieu de travail';
  STransBtnOK = '&OK';
  STransBtnCancel = '&ANNULE';
  STransBtnEndOfDay = '&FIN';
  STransTimeRecording = 'Enregistrement du temps';
  STransEmployee = 'Employé';
  STransEmp = 'Emp.';
  STransIDCard = 'Carte d''identité';
  STransBtnAccept = 'D''accord';
  STransDateAndTime = 'Date et temps';
  STransCurrentDate = 'Date';
  STransWorkedTimeHM = 'Le temps de travail (heure:minutes)';
  STransLastWeek = 'La semaine passée';
  STransThisWeek = 'Cette semaine';
  STransTimeHM = 'Temps (heure:minutes)';
  STransTimeForTime = 'Temps pour Temps';
  STransHoliday = 'Vacances';
  STransWorkTimeReduction = 'Temps de travail variable';
  STransMessages = 'Message';
  STransScanned = 'Scanné';
  STransSmartCardInfo = 'Smart Card Info';
  STransCardReaders = 'Card Readers';
  STransLogMessage = 'Log Messages';
  STransWorkspotJob = 'Lieu de travail/ Travail';
  STransPlant = 'Entreprise';
  STransFrom = 'De';
  STransJob = 'Travail';
  STransWorkspot = 'Lieu de travail';
  // DialogAskEmployeeNumber
  STransEnterEmplNr = 'Entre le numéro d''employé';
  STransEmpNr = 'Numéro d''employé';

{$ELSE}
  {$IFDEF PIMSGERMAN}
  STransSelectWorkspot = 'Selektiere Arbeitsplatz';
  STransBtnOK = '&OK';
  STransBtnCancel = '&ANNUL.';
  STransBtnEndOfDay = '&ENDE';
  STransTimeRecording = 'Zeiterfassung';
  STransEmployee = 'Mitarbeiter';
  STransEmp = 'Mitarb.';
  STransIDCard = 'ID-Karte';
  STransBtnAccept = 'Akzeptier';
  STransDateAndTime = 'Datum und Zeit';
  STransCurrentDate = 'Jetzige Datum';
  STransWorkedTimeHM = 'Arbeitszeit (Stunden:Minuten)';
  STransLastWeek = 'Letzte Woche';
  STransThisWeek = 'Diese Woche';
  STransTimeHM = 'Zeit (Stunden:Minuten)';
  STransTimeForTime = 'Zeit für Zeit';
  STransHoliday = 'Urlaub';
  STransWorkTimeReduction = 'Gleitzeit';
  STransMessages = 'Meldung';
  STransScanned = 'Gescant';
  STransSmartCardInfo = 'Smart Card Info';
  STransCardReaders = 'Card Readers';
  STransLogMessage = 'Log Messages';
  STransWorkspotJob = 'Arbeitsplatz/Job';
  STransPlant = 'Betrieb';
  STransFrom = 'Von';
  STransJob = 'Job';
  STransWorkspot = 'A-Platz';
  // DialogAskEmployeeNumber
  STransEnterEmplNr = 'Eingabe Mitarbeiternummer';
  STransEmpNr = 'Mitarbeiternummer';

{$ELSE}
  {$IFDEF PIMSDANISH}
  STransSelectWorkspot = 'Vælg arb.sted';
  STransBtnOK = '&OK';
  STransBtnCancel = '&ANNUL.';
  STransBtnEndOfDay = '&SLUT';
  STransTimeRecording = 'Tidsregistr.';
  STransEmployee = 'Medarbejder';
  STransEmp = 'Medarb.';
  STransIDCard = 'ID Kort';
  STransBtnAccept = '&Accept';
  STransDateAndTime = 'Dato og tid';
  STransCurrentDate = 'Denne dato';
  STransWorkedTimeHM = 'Arb. tid (timer:min.)';
  STransLastWeek = 'Forr. uge';
  STransThisWeek = 'Denne uge';
  STransTimeHM = 'Tid (timer:min.)';
  STransTimeForTime = 'Time for Time';
  STransHoliday = 'Ferie';
  STransWorkTimeReduction = 'Arb. tid Reduktion';
  STransMessages = 'Beskeder';
  STransScanned = 'Skannet';
  STransSmartCardInfo = 'Smart Card Info';
  STransCardReaders = 'Kort læsere';
  STransLogMessage = 'Log beskeder';
  STransWorkspotJob = 'Arbejdsplads / Job';
  STransPlant = 'Vaskeri';
  STransFrom = 'Fra';
  STransJob = 'Job';
  STransWorkspot = 'Arb. plads';
  // DialogAskEmployeeNumber
  STransEnterEmplNr = 'Indtaste medarbejdernummer';
  STransEmpNr = 'Medarbejdernummer';

{$ELSE}
  {$IFDEF PIMSJAPANESE}
  STransSelectWorkspot = 'Select Workspot';
  STransBtnOK = '&OK';
  STransBtnCancel = '&CANCEL';
  STransBtnEndOfDay = '&END';
  STransTimeRecording = 'Time Recording';
  STransEmployee = 'Employee';
  STransEmp = 'Employee';
  STransIDCard = 'ID Card';
  STransBtnAccept = '&Accept';
  STransDateAndTime = 'Date and Time';
  STransCurrentDate = 'Current Date';
  STransWorkedTimeHM = 'Worked Time (hours:minutes)';
  STransLastWeek = 'Last Week';
  STransThisWeek = 'This Week';
  STransTimeHM = 'Time (hours:minutes)';
  STransTimeForTime = 'Time for Time';
  STransHoliday = 'Holiday';
  STransWorkTimeReduction = 'Work Time Reduction';
  STransMessages = 'Messages';
  STransScanned = 'Scanned';
  STransSmartCardInfo = 'Smart Card Info';
  STransCardReaders = 'Card Readers';
  STransLogMessage = 'Log Messages';
  STransWorkspotJob = 'Workspot / Job';
  STransPlant = 'Plant';
  STransFrom = 'From';
  STransJob = 'Job';
  STransWorkspot = 'Workspot';
  // DialogAskEmployeeNumber
  STransEnterEmplNr = 'Enter Employee Number';
  STransEmpNr = 'Employee Number';

{$ELSE}
  {$IFDEF PIMSCHINESE}
  STransSelectWorkspot = '选择工作点';
  STransBtnOK = '&好';
  STransBtnCancel = '&取消';
  STransBtnEndOfDay = '&结束';
  STransTimeRecording = '时间记录';
  STransEmployee = '雇员';
  STransEmp = '雇员';
  STransIDCard = '身份证';
  STransBtnAccept = '&接受';
  STransDateAndTime = 'Date and Time';
  STransCurrentDate = 'Current Date';
  STransWorkedTimeHM = 'Worked Time (hours:minutes)';
  STransLastWeek = 'Last Week';
  STransThisWeek = 'This Week';
  STransTimeHM = 'Time (hours:minutes)';
  STransTimeForTime = 'Time for Time';
  STransHoliday = 'Holiday';
  STransWorkTimeReduction = 'Work Time Reduction';
  STransMessages = 'Messages';
  STransScanned = 'Scanned';
  STransSmartCardInfo = 'Smart Card Info';
  STransCardReaders = 'Card Readers';
  STransLogMessage = 'Log Messages';
  STransWorkspotJob = 'Workspot / Job';
  STransPlant = 'Plant';
  STransFrom = 'From';
  STransJob = 'Job';
  STransWorkspot = 'Workspot';
  // DialogAskEmployeeNumber
  STransEnterEmplNr = 'Enter Employee Number';
  STransEmpNr = 'Employee Number';

{$ELSE}
// ENGLISH (default language)
  STransSelectWorkspot = 'Select Workspot';
  STransBtnOK = '&OK';
  STransBtnCancel = '&CANCEL';
  STransBtnEndOfDay = '&END';
  STransTimeRecording = 'Time Recording';
  STransEmployee = 'Employee';
  STransEmp = 'Employee';
  STransIDCard = 'ID Card';
  STransBtnAccept = '&Accept';
  STransDateAndTime = 'Date and Time';
  STransCurrentDate = 'Current Date';
  STransWorkedTimeHM = 'Worked Time (hours:minutes)';
  STransLastWeek = 'Last Week';
  STransThisWeek = 'This Week';
  STransTimeHM = 'Time (hours:minutes)';
  STransTimeForTime = 'Time for Time';
  STransHoliday = 'Holiday';
  STransWorkTimeReduction = 'Work Time Reduction';
  STransMessages = 'Messages';
  STransScanned = 'Scanned';
  STransSmartCardInfo = 'Smart Card Info';
  STransCardReaders = 'Card Readers';
  STransLogMessage = 'Log Messages';
  STransWorkspotJob = 'Workspot / Job';
  STransPlant = 'Plant';
  STransFrom = 'From';
  STransJob = 'Job';
  STransWorkspot = 'Workspot';
  // DialogAskEmployeeNumber
  STransEnterEmplNr = 'Enter Employee Number';
  STransEmpNr = 'Employee Number';

          {$ENDIF}
        {$ENDIF}
      {$ENDIF}
    {$ENDIF}
  {$ENDIF}
{$ENDIF}

implementation

end.
