unit TimeRecordingSelectorFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BasePimsFRM, StdCtrls, Buttons, Grids, DBGrids, TimeRecordingFRM, DB,
  ExtCtrls;

const
	ModalOK = 1;
  ModalCancel = 2;
  ModalEndofDay = 3;

type

  TFormPos = record
    Top, Left, Height, Width : integer;
  end;

  TTimeRecordingSelectorF = class(TBasePimsForm)
    PanelButtons: TPanel;
    PanelGrid: TPanel;
    DBGridSelector: TDBGrid;
    PanelControls: TPanel;
    BitBtnOk: TBitBtn;
    BitBtnCancel: TBitBtn;
    BitBtnEndOfDay: TBitBtn;
    PanelBFirst: TPanel;
    SpeedButtonFirst: TSpeedButton;
    PanelBLast: TPanel;
    PanelBPrior: TPanel;
    SpeedButtonPrior: TSpeedButton;
    PanelNext: TPanel;
    SpeedButtonNext: TSpeedButton;
    SpeedButtonLast: TSpeedButton;
    procedure BitBtnEndOfDayClick(Sender: TObject);
    procedure SpeedButtonFirstClick(Sender: TObject);
    procedure SpeedButtonPriorClick(Sender: TObject);
    procedure SpeedButtonNextClick(Sender: TObject);
    procedure SpeedButtonLastClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
    function EnableSelection(	var FormPos : TFormPos;
       const ColumnNames : array of string;
       ActiveDataSet : TDataSource) : integer;
  end;

var
  TimeRecordingSelectorF: TTimeRecordingSelectorF;

implementation
  
uses 
  UPimsMessageRes;

{$IFDEF PIMSDUTCH}
{$R *NLD.DFM}
{$ELSE}
  {$IFDEF PIMSGERMAN}
  {$R *DEU.DFM}
  {$ELSE}
    {$IFDEF PIMSFRENCH}
    {$R *FRA.DFM}
    {$ELSE}
    {$R *.DFM}
    {$ENDIF}
  {$ENDIF}
{$ENDIF}

{ TSelectorF }

procedure TTimeRecordingSelectorF.BitBtnEndOfDayClick(Sender: TObject);
begin
  inherited;
  ModalResult := ModalEndOfDay;
end;

function TTimeRecordingSelectorF.EnableSelection(	var FormPos : TFormPos;
  const ColumnNames : array of string;
  ActiveDataSet : TDataSource	) : integer;
var
	colindex : integer;
begin
  // Structure of ColumnNames
  // 0. Selector form Caption
  // 1 - x grid column text
  // only 2 columns - set width based on this
  ModalResult := ModalCancel;
  DBGridSelector.Columns.Clear;
  if High(ColumnNames) > 0 then	Caption := ColumnNames[0];
  for colindex := 1 to (High(ColumnNames)) do
    begin
      DBGridSelector.Columns.Add;
      DBGridSelector.Columns[colindex-1].Field :=
      	ActiveDataSet.DataSet.Fields[colindex-1];
      DBGridSelector.Columns[colindex-1].Title.Caption := ColumnNames[colindex];
      if  (ColumnNames[colindex] = SPimsColumnCode) or
          (ColumnNames[colindex] = SPimsColumnPlant) then
        DBGridSelector.Columns[colindex-1].Width :=
          Round(DBGridSelector.Width / (High(ColumnNames) + 2))
      else
       DBGridSelector.Columns[colindex-1].Width :=
       	Round(DBGridSelector.Width - DBGridSelector.Columns[colindex-2].Width);
    end;
  DBGridSelector.DataSource  := ActiveDataSet;
  Top := FormPos.Top;
  Left := FormPos.Left;
  Height := FormPos.Height;
  Width := FormPos.Width;
  result := ShowModal;
end;

procedure TTimeRecordingSelectorF.SpeedButtonFirstClick(Sender: TObject);
begin
  inherited;
  DBGridSelector.DataSource.DataSet.First;
end;

procedure TTimeRecordingSelectorF.SpeedButtonPriorClick(Sender: TObject);
begin
  inherited;
  DBGridSelector.DataSource.DataSet.Prior;
end;

procedure TTimeRecordingSelectorF.SpeedButtonNextClick(Sender: TObject);
begin
  inherited;
  DBGridSelector.DataSource.DataSet.Next;
end;

procedure TTimeRecordingSelectorF.SpeedButtonLastClick(Sender: TObject);
begin
  inherited;
	DBGridSelector.DataSource.DataSet.Last;
end;

procedure TTimeRecordingSelectorF.FormShow(Sender: TObject);
begin
  inherited;
  DBGridSelector.SetFocus;
end;


procedure TTimeRecordingSelectorF.FormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  case key of
    vk_F10		: begin
                  BitBtnEndOfDayClick(self);
                  key := 0;
                end;
    vk_escape : begin
                  ModalResult := ModalCancel;
                  key := 0;
                end;
    {vk_up			: begin
                  SpeedButtonPriorClick(self);
                  key := 0;
                end;
    vk_down		: begin
                  SpeedButtonNextClick(self);
                  key := 0;
                end;}
    vk_left		: begin
                  SpeedButtonFirstClick(self);
                  key := 0;
                end;
    vk_right	: begin
                  SpeedButtonLastClick(self);
                  key := 0;
                end;
  end;
end;

end.

