(*
  Changes:
    RV005: (Revision 005). Oracle-10-fix.
    2.0.139.18 - MRA:13-MAR-2008. Oracle 10 test.
      - During testing for Oracle 10g 10.2.0.1 and error occured:
        ORA-24909 Call in progress. Current operation cancelled.
        This is a problem with the special Oracle DOA-components.
        (Direct Oracle Access).
        This can be solved as follows:
        - Set 'Optimize' to 'False' for TOracleQuery and TOracleDataSet-
          components. In most cases this can happen with
          Insert/Update/Delete-statements, but also with Select-statements.
       Note: In Pims-applications made in Delphi5 did error did not happen,
             but there the DOA-components are not used.
    MRA:15-JUN-2010. RV063.4. 550478. Personal Screen.
    - Addition of Machine/Workspot + Time recording.
    - Make it possible to call 'TimeRecording' from Production Screen:
      - Use a boolean to decide if TimeRecording is called from itself or
        from Production Screen.
      If TimeRecording is called from Production Screen:
      - Show it in a smaller dialog?
      - Add Cancel-button.
      - Show a different set of jobs, based on a certain Machine / Workspot.
      - The MachineCode, PlantCode,  WorkspotCode depend on the call from
        ProductionScreen.
      If TimeRecording is called from itself:
      - Use standard behaviour.
    MRA:9-NOV-2010 RV072.4.
    - When an employee of type 'non-scanner' and
      'book prod. hours based on standard planning' is scanning,
      then give a message and block this action.
    MRA:12-APR-2012 20012858
    - Additions for New Personal Screen:
      - Break-button
      - Lunch-button
    MRA:23-MAY-2012 20013176
    - Check for any Smart Card Commands, if found
      then it should be possible to read an ID from a smart card
      using a smart card reader.
    MRA:5-JUL-2012 20012858.90
    - Because 1 field was added, oqInsertJob had to be changed!
      - Extra field: EMPS_SCAN_IN_YN  (default: 'N').
    MRA:3-AUG-2012 20013478. Related to this order.
    - For this order a field was added to JOBCODE-table:
      Field: IGNOREQUANTITIESINREPORTS_YN
      This field must be added when jobs are autom. added like the BREAK-job.
    - Changes made to oqInsertJob.
    MRA:22-NOV-2012 200134549 Related to this order.
    - When an INSERT-statement is used, always use the structure:
      - INSERT xxx (field1, field2, ...) values (field1, field2, ...)
      - And be sure the fieldnames are all added, except for:
        - Fields that will be added later do not have to be included,
          when they are not-null and have a default value set on db-level.
    MRA:11-MAR-2013 20014035
    - Hide ID optionally in Timerecording/PersonalScreen
      - Addition of field HIDE_ID_YN in WORKSTATION-table.
    MRA:16-MAY-2014 SO-20015371
    - Embed customized function to convert id-card-number
      - When SERIALDEVICE.INITSTRING is filled, then used that a the name
        of the (Oracle)-function to convert the ID.
    MRA:1-APR-2015 SO-20016449
    - Time zone implementation
    MRA:13-JAN-2016 ABS-18715
    - No Employee
    - There was a rounding-problem with scans resulting in no-employee in report
      production details.
    MRA:11-MAR-2016 PIM-149
    - Show job defined as default job as first job. Also sort on description
      instead of on code because that is also done in Personal Screen.
    MRA:11-MAR-2016 PIM-150
    - Add option to show only the default job (only for timerecording-app).
    - Based on workstation-setting SHOW_ONLY_DEFJOB_YN.
    - Note: Be sure there is a default job defined!
    MRA:31-MAR-2016 PIM-161
    - Look for scans older within 24 hours, when it was older than 12 hours
      give a message about: Last scan is not closed. End of Day? Yes or No.
    - We must look for the last scan open or not!
    MRA:19-MAY-2016 PIM-179
    - When using Hybrid-mode (Hybrid=1):
      - Show only job ATWORK as default,
        when workspot has setting 'Measure Productivity equals Yes'.
      - Also add job ATWORK and OCCUP when they do not exist yet for
        the selected workspot.
    MRA:20-MAY-2016 PIM-181
    - Only allow one employee with job ATWORK per workspot.
    MRA:23-JUN-2016 PIM-161.2
    - Timerecording and scans older than 12 hours
    - Only do this for Timerecording-program and not the Personal Screen.
    MRA:13-JUL-2016 PIM-203
    - Related to this order:
      - Some scan-handling functions are moved to this file so it can be used
        in other parts by only including this file in the project.
    MRA:4-JAN-2017 PIM-254
    - Accept 8 positions for Rosslare-cards.
    MRA:6-JAN-2017 PIM-256
    - Read IDs from FOBs via pcProcs-Plus-Reader with USB-connection
    MRA:20-DEC-2017 PIM-330
    - To prevent a blocked session, be sure there is either a commit
      or rollback done.
    MRA:12-JAN-2018 PIM-346
    - Blocked Sessions bugfix
    - Determine previous workspot:
      - Get workspot.description and jobcode.description from 1 query, instead
        of 3 to improve performance.
    MRA:22-JAN-2018 PIM-346
    - Add logging
    - Add Commit/Rollback when needed.
    MRA:22-JAN-2018 PIM-351
    - Timerecording and error about IllnessMessagesDateBI_BU
    - Use try-except to catch an error with Illness-messages.
    MRA:28-MAR-2018 GLOB3-112
    - TimeRecordingCR Add option to read ID via RAW-field of IDCard-table
    MRA:2-JUL-2018 GLOB3-60
    - Extension 4 to 10 blocks for planning
    MRA:23-JUL-2018 PIM-383
    - Timerecording and ask for emp-nr gives problem with raw-option
    - oqIDCard changed, also return IDCard_Number.
    MRA:23-JUL-2018 GLOB3-60
    - Extension 4 to 10 blocks for planning
    - Changed queries: oqDeterminePlannedWorkspot and
      oqDetermineStandardPlannedWorkspot, because they gave an error when
      timeblocks above 4 were empty.
    MRA:23-JUL-2018 GLOB3-139
    - Add function that changes ID via bit-changes
    MRA:4-JAN-2019 GLOB3-202
    - Scan via machine-card readers and do not scan out
    - Also added the handling for single employee (one employee allowed on one
      workspot) and roaming workspot (related to PIM-181).
    MRA:21-JUN-2019     GLOB3-324
    - Add device named pcProx for Timerecording-app.
    - This is used for pcProx Plus readers with cards that should be read
      without changing it. Now it uses Rosslare that triggers the fact it
      is a pcProx Plus-reader, but also converts the ID. 
*)

unit TimeRecordingDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, Oracle, OracleData, {PersonalScreenDMT, } {UProductionScreen}
  {UPersonalScreen,} UProductionScreenDefs, UScannedIDCard, Variants;


type
  TErrorCode = (ecOK, ecIDCardExpired, ecIDCardNotFound, ecEmployeeExpired,
    ecEmployeeNotFound, ecScanExists, ecNonScanEmployee, ecUnKnown);
  TScanError = procedure(const Msg : string) of object;
  TEnabledWk_JobField = procedure(const AEnabledFields: Boolean) of object;
  TLabelErrorMsgDone = procedure of object;

type
  TTimeRecordingDM = class(TDataModule)
    dsrcWork: TDataSource;
    oqComportConnection: TOracleQuery;
    oqWorkstation: TOracleQuery;
    oqIDCard: TOracleQuery;
    oqEmployee: TOracleQuery;
    oqJob: TOracleQuery;
    oqWorkspot: TOracleQuery;
    oqGetWorkedMin: TOracleQuery;
    oqProcessIllnessMessage: TOracleQuery;
    oqUpdateIllnessMessage: TOracleQuery;
    oqUpdateEMA_1: TOracleQuery;
    oqUpdateEMA_2: TOracleQuery;
    oqUpdateEMA_3: TOracleQuery;
    oqUpdateEMA_4: TOracleQuery;
    oqAbsenceStatistics: TOracleQuery;
    oqUpdateTRSMultipleScan: TOracleQuery;
    oqWork: TOracleQuery;
    oqInsertTRSNewScan: TOracleQuery;
    oqUpdateTRSCompleteScan: TOracleQuery;
    oqDeterminePreviousWorkSpot: TOracleQuery;
    oqDeterminePlannedWorkspot: TOracleQuery;
    oqDetermineShiftFromShiftSchedule: TOracleQuery;
    oqDetermineShiftByPlant: TOracleQuery;
    oqDetermineShiftByEmployee: TOracleQuery;
    oqDetermineStandardPlanningWorkspot: TOracleQuery;
    oqDetermineWorkspotByDepartment: TOracleQuery;
    odsWork: TOracleDataSet;
    odsJobcode: TOracleDataSet;
    dsrcJobcode: TDataSource;
    oqScanExists: TOracleQuery;
    oqEmployeeShift: TOracleQuery;
    oqCheckJob: TOracleQuery;
    oqInsertJob: TOracleQuery;
    oqGetPrevTRJob: TOracleQuery;
    oqTRSCommands: TOracleQuery;
    oqIDCardRaw: TOracleQuery;
    oqIDCardRawSelect: TOracleQuery;
    oqIDCardRawUpdate: TOracleQuery;
    oqIDCardRawInsert: TOracleQuery;
    oqIDCardSelect: TOracleQuery;
    oqIDCardUpdate: TOracleQuery;
    oqIDCardInsert: TOracleQuery;
    oqIDConvert: TOracleQuery;
    oqCheckMachJob: TOracleQuery;
    oqInsertMachJob: TOracleQuery;
    oqUpdateEMA_5: TOracleQuery;
    oqUpdateEMA_6: TOracleQuery;
    oqUpdateEMA_7: TOracleQuery;
    oqUpdateEMA_8: TOracleQuery;
    oqUpdateEMA_9: TOracleQuery;
    oqUpdateEMA_10: TOracleQuery;
    oqRoamingWorkspot: TOracleQuery;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
    FPersonalScreen: Boolean;
    FProdScreenType: TProdScreenType;
    FPlantCode: String;
    FMachineCode: String;
    FMachineDescription: String;
    FWorkspotCode: String;
    FWorkspotDescription: String;
    FCurrentJobCode: String;
    FSmartCardCommandsFound: Boolean;
    FSerialDeviceCode: String;
    FOnScanError: TScanError;
    FEnabledWk_JobField: TEnabledWk_JobField;
    FLabelErrorMsgDone: TLabelErrorMsgDone;
    FReaderRosslare: Boolean;
    FStartMaximized: Boolean;
    FNoClose: Boolean;
    FUseLogging: Boolean;
    FRaw: Boolean;
    FH5427: Boolean;
    FRoamingWorkspotCode: String;
    FRoamingJobCode: String;
    FpcProxPlus: Boolean;
    procedure UpdateTRSMultipleScan(AIDCard: TScannedIDCard;
      ACurrentNow: TDateTime);
    procedure InsertTRSNewScan(var AIDCard: TScannedIDCard;
      const AOpen: Boolean; const AIDCardOut: String; ACurrentNow: TDateTime);
    procedure UpdateTRSCompleteScan(var ANextIDCard, ALastIDCard:
      TScannedIDCard; const Is24HrScan: Boolean; ACurrentNow: TDateTime);
    procedure Determine24HourScan(var AEndOfDayError: Boolean;
      var ASavePrevPlant, ASavePrevWK, ASavePrevJob, ASavePrevDate: String;
      var ALastIDCard, ANextIDCard: TScannedIDCard; ACurrentNow: TDateTime;
      var AEditPrePlantText, AEditPreDateFromText, AEditPreWorkspotText,
      AEditPreJobText: String);
  public
    { Public declarations }
    LastIDCard, NextIDCard: TScannedIDCard;
    SmartCardCommandList: TStringList;
    function GetWorkedMin(const AEmployeeNumber: Integer;
     const AStartDate, AEndDate: TDateTime): Integer;
    procedure ProcessIllnessMessage(const AEmployeeNumber: Integer);
    function CheckAddJobForWorkspot(APlantCode, AWorkspotCode,
      AJobCode, AJobDescription: String): Boolean;
    function CheckAddJobForMachine(APlantCode, AMachineCode,
      AJobCode, AJobDescription: String): Boolean;
    function DeterminePrevJob(APlantCode,
      AWorkspotCode, AJobCode1, AJobCode2: String;
      AEmployeeNumber: Integer;
      var ANewPlantCode, ANewWorkspotCode, ANewJobCode: String;
      var ANewPlantDescription, ANewWorkspotDescription,
      ANewJobDescription: String): Boolean;
    function CheckSmartCardCommands: Boolean;
    procedure IDConvertInit(AFunction: String);
    function IDConvert(AIDCode: String): String;
    function DeterminePreviousWorkSpot(AScanInDate: TDateTime;
      var AIDCard: TScannedIDCard; ACurrentNow: TDateTime;
      AEndOfDay: Boolean; AByWorkspot: Boolean;
      var AEmployeeNumber: Integer;
      var AEmployeeName: String;
      AHoursAgo: Double = 0): Boolean;
    procedure AddLog(AMsg: String);
    function HandleScanAction(ALastIDCard,
      ANextIDCard: TScannedIDCard;
      APrePlantText: String;
      AIsScanning: Boolean;
      ASavePrevPlant, ASavePrevWK, ASavePrevJob,
      ASavePrevDate: String;
      ACurrentNow: TDateTime;
      var ALastNow: TDateTime;
      var AEditPrePlantText, AEditPreDateFromText, AEditPreWorkspotText,
      AEditPreJobText: String;
      ADoNotScanOut: Boolean=False): Boolean;
    function ValueIDCard(ACode : String; ACurrentNow: TDateTime): TErrorCode;
    function ErrorToString(AErrorCode: TErrorCode): String;
    procedure PopulateIDCard(var AIDCard : TScannedIDCard);
    function IDConvertRosslare(AID: String): String;
    function DetermineIDCardRaw(AID: String): String;
    function H5427Convert(AID: String): String;
    procedure DetermineRoamingWorkspot(APlantCode: String);
    // 550478.
    property PersonalScreen: Boolean read FPersonalScreen write FPersonalScreen;
    property ProdScreenType: TProdScreenType read FProdScreenType
      write FProdScreenType;
    property PlantCode: String read FPlantCode write FPlantCode;
    property MachineCode: String read FMachineCode write FMachineCode;
    property MachineDescription: String read FMachineDescription
      write FMachineDescription;
    property WorkspotCode: String read FWorkspotCode write FWorkspotCode;
    property WorkspotDescription: String read FWorkspotDescription
      write FWorkspotDescription;
    property CurrentJobCode: String read FCurrentJobCode write FCurrentJobCode;
    property SmartCardCommandsFound: Boolean read FSmartCardCommandsFound
      write FSmartCardCommandsFound;
    property SerialDeviceCode: String read FSerialDeviceCode
      write FSerialDeviceCode;
    property OnScanError: TScanError read FOnScanError write FOnScanError;
    property EnabledWk_JobField: TEnabledWk_JobField read FEnabledWk_JobField write FEnabledWk_JobField;
    property LabelErrorMsgDone: TLabelErrorMsgDone read FLabelErrorMsgDone write FLabelErrorMsgDone;
    property ReaderRosslare: Boolean read FReaderRosslare write FReaderRosslare;
    property StartMaximized: Boolean read FStartMaximized write FStartMaximized;
    property NoClose: Boolean read FNoClose write FNoClose;
    property UseLogging: Boolean read FUseLogging write FUseLogging;
    property Raw: Boolean read FRaw write FRaw;
    property H5427: Boolean read FH5427 write FH5427; // GLOB3-139
    property RoamingWorkspotCode: String read FRoamingWorkspotCode write FRoamingWorkspotCode; // GLOB3-202
    property RoamingJobCode: String read FRoamingJobCode write FRoamingJobCode; // GLOB3-202
    property pcProxPlus: Boolean read FpcProxPlus write FpcProxPlus; // GLOB3-324
  end;

var
  TimeRecordingDM: TTimeRecordingDM;

implementation

{$R *.DFM}
uses
  ORASystemDMT, UGlobalFunctions, UPimsMessageRes, UPimsConst,
  CalculateTotalHoursDMT, GlobalDMT;

procedure TTimeRecordingDM.DataModuleCreate(Sender: TObject);
begin
  inherited;
  UseLogging := False;
  StartMaximized := False;
  NoClose := False;
  Raw := False;
  H5427 := False; // GLOB3-139
  
  PersonalScreen := False;
  ActiveTables(Self, True);
  ActiveTables(ORASystemDM, True);
  // 20016449 Related to this order.
  // This is already done in ORASystemDM itself
//  ORASystemDM.SynchronizeWorkStation;
//  ORASystemDM.GetSettings;
  // 20013176
  SmartCardCommandList := TStringList.Create;
  SmartCardCommandsFound := CheckSmartCardCommands;

  // GLOB3-202 It should also use the roaming workspot here.
  //           For what plant???
//  if ORASystemDM.TimeRecCRDoNotScanOut then
//  Self.DetermineRoamingWorkspot('1');
end;

function TTimeRecordingDM.GetWorkedMin(const AEmployeeNumber: Integer;
  const AStartDate, AEndDate: TDateTime): Integer;
begin
  Result := 0;
  with oqGetWorkedMin do
  begin
    ClearVariables;
    SetVariable('STARTDATE',       AStartDate);
    SetVariable('ENDDATE',         AEndDate);
    SetVariable('EMPLOYEE_NUMBER', AEmployeeNumber);
    Execute;
    // query asks for SUM so is never empty
    if not FieldIsNull('WORKED_MIN') then
      Result := FieldAsInteger('WORKED_MIN')
  end; {with oqGetWorkedMin}
end;

procedure TTimeRecordingDM.ProcessIllnessMessage(
  const AEmployeeNumber: Integer);
var
  MyResult: Boolean;
  MyAbsenceReasonCode: String;
begin
  try // PIM-346
    // Are there any IllnessMessages for this employee
    with oqProcessIllnessMessage do
    begin
      ClearVariables;
      SetVariable('EMPLOYEE_NUMBER', AEmployeeNumber);
      Execute;
      MyResult := not Eof;
      if not MyResult then
        Exit
      else
      begin
        // get absence reasons to update
        while not Eof do
        begin
          MyAbsenceReasonCode := FieldAsString('ABSENCEREASON_CODE');
          // update EmployeeAvailability set timeblocks to '*' where timerbocks are filled
          with oqUpdateEMA_1 do
          begin
            ClearVariables;
            SetVariable('EMPLOYEE_NUMBER',     AEmployeeNumber);
            SetVariable('MUTATOR',             ORASystemDM.CurrentProgramUser);
            SetVariable('STARTDATE',           Trunc(Now));
            SetVariable('ABSENCEREASON_CODES', MyAbsenceReasonCode);
            Execute;
          end; {with oqUpdateEMA_1}
          with oqUpdateEMA_2 do
          begin
            ClearVariables;
            SetVariable('EMPLOYEE_NUMBER',     AEmployeeNumber);
            SetVariable('MUTATOR',             ORASystemDM.CurrentProgramUser);
            SetVariable('STARTDATE',           Trunc(Now));
            SetVariable('ABSENCEREASON_CODES', MyAbsenceReasonCode);
            Execute;
          end; {with oqUpdateEMA_2}
          with oqUpdateEMA_3 do
          begin
            ClearVariables;
            SetVariable('EMPLOYEE_NUMBER',     AEmployeeNumber);
            SetVariable('MUTATOR',             ORASystemDM.CurrentProgramUser);
            SetVariable('STARTDATE',           Trunc(Now));
            SetVariable('ABSENCEREASON_CODES', MyAbsenceReasonCode);
            Execute;
          end; {with oqUpdateEMA_3}
          with oqUpdateEMA_4 do
          begin
            ClearVariables;
            SetVariable('EMPLOYEE_NUMBER',     AEmployeeNumber);
            SetVariable('MUTATOR',             ORASystemDM.CurrentProgramUser);
            SetVariable('STARTDATE',           Trunc(Now));
            SetVariable('ABSENCEREASON_CODES', MyAbsenceReasonCode);
            Execute;
          end; {with oqUpdateEMA_4}
          if ORASystemDM.MaxTimeblocks > MAX_TBS then
          begin
            with oqUpdateEMA_5 do
            begin
              ClearVariables;
              SetVariable('EMPLOYEE_NUMBER',     AEmployeeNumber);
              SetVariable('MUTATOR',             ORASystemDM.CurrentProgramUser);
              SetVariable('STARTDATE',           Trunc(Now));
              SetVariable('ABSENCEREASON_CODES', MyAbsenceReasonCode);
              Execute;
            end; {with oqUpdateEMA_5}
            with oqUpdateEMA_6 do
            begin
              ClearVariables;
              SetVariable('EMPLOYEE_NUMBER',     AEmployeeNumber);
              SetVariable('MUTATOR',             ORASystemDM.CurrentProgramUser);
              SetVariable('STARTDATE',           Trunc(Now));
              SetVariable('ABSENCEREASON_CODES', MyAbsenceReasonCode);
              Execute;
            end; {with oqUpdateEMA_6}
            with oqUpdateEMA_7 do
            begin
              ClearVariables;
              SetVariable('EMPLOYEE_NUMBER',     AEmployeeNumber);
              SetVariable('MUTATOR',             ORASystemDM.CurrentProgramUser);
              SetVariable('STARTDATE',           Trunc(Now));
              SetVariable('ABSENCEREASON_CODES', MyAbsenceReasonCode);
              Execute;
            end; {with oqUpdateEMA_7}
            with oqUpdateEMA_8 do
            begin
              ClearVariables;
              SetVariable('EMPLOYEE_NUMBER',     AEmployeeNumber);
              SetVariable('MUTATOR',             ORASystemDM.CurrentProgramUser);
              SetVariable('STARTDATE',           Trunc(Now));
              SetVariable('ABSENCEREASON_CODES', MyAbsenceReasonCode);
              Execute;
            end; {with oqUpdateEMA_8}
            with oqUpdateEMA_9 do
            begin
              ClearVariables;
              SetVariable('EMPLOYEE_NUMBER',     AEmployeeNumber);
              SetVariable('MUTATOR',             ORASystemDM.CurrentProgramUser);
              SetVariable('STARTDATE',           Trunc(Now));
              SetVariable('ABSENCEREASON_CODES', MyAbsenceReasonCode);
              Execute;
            end; {with oqUpdateEMA_9}
            with oqUpdateEMA_10 do
            begin
              ClearVariables;
              SetVariable('EMPLOYEE_NUMBER',     AEmployeeNumber);
              SetVariable('MUTATOR',             ORASystemDM.CurrentProgramUser);
              SetVariable('STARTDATE',           Trunc(Now));
              SetVariable('ABSENCEREASON_CODES', MyAbsenceReasonCode);
              Execute;
            end; {with oqUpdateEMA_10}
          end; // if
          Next;
        end; {while not Eof}
      end; {if MyResult}
    end; {with oqProcessIllnessMessage}
    with oqUpdateIllnessMessage do
    begin
      ClearVariables;
      SetVariable('EMPLOYEE_NUMBER', AEmployeeNumber);
      SetVariable('ILLNESSMESSAGE_ENDDATE', Trunc(Now)); // 20016449
      SetVariable('MUTATOR',         ORASystemDM.CurrentProgramUser);
      Execute;
    end; {with oqUpdateIllnessMessage}
    if ORASystemDM.OracleSession.InTransaction then // PIM-346
      ORASystemDM.OracleSession.Commit;
  except
    on E: EOracleError do
    begin
      if ORASystemDM.OracleSession.InTransaction then
        ORASystemDM.OracleSession.Rollback;
      WErrorLog(E.Message);
    end;
    on E: Exception do
    begin
      if ORASystemDM.OracleSession.InTransaction then
        ORASystemDM.OracleSession.Rollback;
      WErrorLog(E.Message);
    end;
  end;
end;

// PersonalScreen
// Check add a job for the workspot, if it did not exist yet.
// Can be used for 'break'-job
function TTimeRecordingDM.CheckAddJobForWorkspot(APlantCode, AWorkspotCode,
  AJobCode, AJobDescription: String): Boolean;
begin
  Result := True;
  try
    with TimeRecordingDM do
    begin
      oqCheckJob.ClearVariables;
      oqCheckJob.SetVariable('PLANT_CODE',    APlantCode);
      oqCheckJob.SetVariable('WORKSPOT_CODE', AWorkspotCode);
      oqCheckJob.SetVariable('JOB_CODE',      AJobCode);
      oqCheckJob.Execute;
      if oqCheckJob.Eof then
      begin
        // Add the job for the workspot.
        oqInsertJob.ClearVariables;
        oqInsertJob.SetVariable('PLANT_CODE',    APlantCode);
        oqInsertJob.SetVariable('WORKSPOT_CODE', AWorkspotCode);
        oqInsertJob.SetVariable('JOB_CODE',      AJobCode);
        oqInsertJob.SetVariable('DESCRIPTION',   AJobDescription);
        oqInsertJob.SetVariable('MUTATOR',
          ORASystemDM.CurrentProgramUser);
        oqInsertJob.Execute;
        if ORASystemDM.OracleSession.InTransaction then
          ORASystemDM.OracleSession.Commit;
      end;
    end;
  except
    on E: EOracleError do
    begin
      if ORASystemDM.OracleSession.InTransaction then
        ORASystemDM.OracleSession.Rollback;
      WErrorLog(E.Message);
      Result := False;
    end;
    on E: Exception do
    begin
      if ORASystemDM.OracleSession.InTransaction then
        ORASystemDM.OracleSession.Rollback;
      WErrorLog(E.Message);
      Result := False;
    end;
  end;
end; // CheckAddJobForWorkspot

// PersonalScreen
// Determine previous job based on scans.
function TTimeRecordingDM.DeterminePrevJob(APlantCode,
  AWorkspotCode, AJobCode1, AJobCode2: String;
  AEmployeeNumber: Integer;
  var ANewPlantCode, ANewWorkspotCode, ANewJobCode: String;
  var ANewPlantDescription, ANewWorkspotDescription,
    ANewJobDescription: String): Boolean;
begin
  Result := False;
  try
    ANewJobCode := '';
    oqGetPrevTRJob.ClearVariables;
    oqGetPrevTRJob.SetVariable('PLANT_CODE',      APlantCode);
    oqGetPrevTRJob.SetVariable('WORKSPOT_CODE',   AWorkspotCode);
    oqGetPrevTRJob.SetVariable('DOWNJOB1',        AJobCode1); // MECHANICAL_DOWN_JOB
    oqGetPrevTRJob.SetVariable('DOWNJOB2',        AJobCode2); // NO_MERCHANDIZE_JOB
    oqGetPrevTRJob.SetVariable('EMPLOYEE_NUMBER', AEmployeeNumber);
    oqGetPrevTRJob.SetVariable('DATEFROM',        Now);
    oqGetPrevTRJob.SetVariable('DATETO',          Now);
    oqGetPrevTRJob.Execute;
    if not oqGetPrevTRJob.Eof then
    begin
      Result := True;
      ANewPlantCode := oqGetPrevTRJob.FieldAsString('PLANT_CODE');
      ANewWorkspotCode := oqGetPrevTRJob.FieldAsString('WORKSPOT_CODE');
      ANewJobCode := oqGetPrevTRJob.FieldAsString('JOB_CODE');
      ANewPlantDescription := oqGetPrevTRJob.FieldAsString('PDESCRIPTION');
      ANewWorkspotDescription := oqGetPrevTRJob.FieldAsString('WDESCRIPTION');
      ANewJobDescription := oqGetPrevTRJob.FieldAsString('JDESCRIPTION');
     end;
  except
    on E: EOracleError do
    begin
      if ORASystemDM.OracleSession.InTransaction then
        ORASystemDM.OracleSession.Rollback;
      WErrorLog(E.Message);
      Result := False;
    end;
    on E: Exception do
    begin
      if ORASystemDM.OracleSession.InTransaction then
        ORASystemDM.OracleSession.Rollback;
      WErrorLog(E.Message);
      Result := False;
    end;
  end;
end; // DeterminePrevJob

// 20013176 Check for any Smart Card Commands
// Also get SerialDeviceCode to determine if there is something special to do.
function TTimeRecordingDM.CheckSmartCardCommands: Boolean;
begin
  Result := False;
  SerialDeviceCode := '';
  with oqTRSCommands do
  begin
    ClearVariables;
    SetVariable('COMPUTER_NAME', ORASystemDM.CurrentComputerName);
    Execute;
    if not Eof then
    begin
      SerialDeviceCode := FieldAsString('SERIALDEVICE_CODE');
      Result := True;
      // Store the commands in a stringlist for later use.
      while not Eof do
      begin
        SmartCardCommandList.Add(FieldAsString('COMMAND'));
        Next;
      end;
    end;
  end;
end; // CheckSmartCardCommands

procedure TTimeRecordingDM.DataModuleDestroy(Sender: TObject);
begin
  // 20013176
  SmartCardCommandList.Free;
end;

// 20015371
procedure TTimeRecordingDM.IDConvertInit(AFunction: String);
var
  SelectStr: String;
begin
  SelectStr := 'SELECT ' + AFunction + '(:IDCODE) MYRESULT' + ' FROM DUAL';
  oqIDConvert.SQL.Clear;
  oqIDConvert.SQL.Add(SelectStr);
end;  // IDConvertInit

// 20015371
function TTimeRecordingDM.IDConvert(AIDCode: String): String;
begin
  Result := AIDCode;
  try
    with oqIDConvert do
    begin
      ClearVariables;
      SetVariable('IDCODE', AIDCode);
      Execute;
      if not Eof then
        Result := FieldAsString('MYRESULT');
    end;
  except
    on E: EOracleError do
    begin
      if ORASystemDM.OracleSession.InTransaction then
        ORASystemDM.OracleSession.Rollback;
      WErrorLog('IDConvert: ' + E.Message);
    end;
    on E: Exception do
    begin
      if ORASystemDM.OracleSession.InTransaction then
        ORASystemDM.OracleSession.Rollback;
      WErrorLog('IDConvert: ' + E.Message);
    end;
  end;
end; // IDConvertInit

// PIM-161 We must look for the last scan that can be open or not, instead of
//         only looking for the last open scan to prevent it finds an open scan
//         while there are closed scans made later, which can happen when
//         end-of-day is used a lot.
// PIM-181 With parameter AByWorkspot we can decide to look for last scan
//         for same employee or for last scan for same plant+workspot.
//         When AByWorkspot is true we assign AEmployeeNumber and
//         AEmployeeName with found values that can be a different employee
//         then the AIDCard indicates.
function TTimeRecordingDM.DeterminePreviousWorkSpot(
  AScanInDate: TDateTime; var AIDCard: TScannedIDCard;
  ACurrentNow: TDateTime;
  AEndOfDay, AByWorkspot: Boolean;
  var AEmployeeNumber: Integer;
  var AEmployeeName: String;
  AHoursAgo: Double = 0): Boolean;
var
  MyTestDate : TDateTime;
begin
  Result := False;
  if AIDCard.EmployeeCode = NullInt then
    Exit;
  try
    with oqDeterminePreviousWorkSpot do
    begin
      ClearVariables;
      SetVariable('EMPLOYEE_NUMBER', AIDcard.EmployeeCode);
      // PIM-161
      if AEndOfDay then
        SetVariable('ALLSCANS', 0)
      else
        SetVariable('ALLSCANS', 1);

      // PIM-181
      if not AByWorkspot then
        SetVariable('BYWORKSPOT', 0) // Look for employee
      else
        SetVariable('BYWORKSPOT', 1); // Look for plant+workspot
      // PIM-181 Fill this when looking for plant+workspot
      SetVariable('PLANT_CODE', AIDCard.PlantCode);
      SetVariable('WORKSPOT_CODE', AIDCard.WorkSpotCode);

      // PIM-161 Test on max. 24 hours ago
      // PIM-161.2 Bugfix Only do this for Timerecording application!
      if PersonalScreen then
        MyTestDate := AScanInDate - 0.5 // 12 hours ago ...
      else
      begin
        if AHoursAgo = 0 then
          MyTestDate := AScanInDate - 1 // 24 hours ago ...
        else
          MyTestDate := AScanInDate - AHoursAgo; // n hours ago
      end;
      SetVariable('TIMEIN',  MyTestDate);
      SetVariable('TIMENOW', RoundTimeConditional(ACurrentNow, 1)); // 20015346
      Execute;
      if not Eof then
      begin
        Result := True;
        // PIM-181
        if not AByWorkspot then
        begin
          AIDCard.EmployeeCode := FieldAsInteger('EMPLOYEE_NUMBER');
          AIDCard.EmplName := FieldAsString('EDESCRIPTION');
        end
        else
        begin
          // PIM-181
          // In case we found the same plant+workspot but a different employee,
          // we assign it to different variables.
          AEmployeeNumber := FieldAsInteger('EMPLOYEE_NUMBER');
          AEmployeeName := FieldAsString('EDESCRIPTION');
        end;
        // Timerecording-specific information
        AIDCard.PlantCode    := FieldAsString('PLANT_CODE');
        AIDCard.WorkSpotCode := FieldAsString('WORKSPOT_CODE');
        AIDCard.JobCode      := FieldAsString('JOB_CODE');
        AIDCard.ShiftNumber  := FieldAsInteger('SHIFT_NUMBER');
        AIDCard.DateIn       := FieldAsDate('DATETIME_IN');
        if AEndOfDay then
          AIDCard.DateOut    := FieldAsDate('DATETIME_OUT');
        AIDCard.IDCard       := FieldAsString('IDCARD_IN');
        // Plant-specific information
        AIDCard.Plant        := FieldAsString('DESCRIPTION');
        AIDCard.InscanEarly  := FieldAsInteger('INSCAN_MARGIN_EARLY');
        AIDCard.InscanLate   := FieldAsInteger('INSCAN_MARGIN_LATE');
        AIDCard.OutscanEarly := FieldAsInteger('OUTSCAN_MARGIN_EARLY');
        AIDCard.OutscanLate  := FieldAsInteger('OUTSCAN_MARGIN_LATE');
        AIDCard.Processed    := FieldAsString('PROCESSED_YN') = 'Y'; // PIM-161
        AIDCard.MeasureProductivity := FieldAsString('MEASURE_PRODUCTIVITY_YN') = 'Y'; // PIM-181
        // PIM-161 We found a closed scan
        if AIDCard.Processed then
        begin
          AIDCard.WorkSpot := SPimsStartDay;
          AIDCard.Plant    := SPimsStartDay;
          AIDCard.Job      := SPimsStartDay;
          AIDCard.JobCode  := ''; // PIM-181
          Exit;
        end;
      end
      else
      begin
        AIDCard.WorkSpot := SPimsStartDay;
        AIDCard.Plant    := SPimsStartDay;
        AIDCard.Job      := SPimsStartDay;
        AIDCard.JobCode  := ''; // PIM-181
        Exit;
      end;
    end; {oqDeterminePreviousWorkSpot}
    if AIDCard.WorkSpotCode <> NullStr then
    begin
      // PIM-346
{
      oqWorkspot.ClearVariables;
      oqWorkspot.SetVariable('PLANT_CODE',    AIDCard.PlantCode);
      oqWorkspot.SetVariable('WORKSPOT_CODE', AIDCard.WorkspotCode);
      oqWorkspot.Execute;
      if not oqWorkspot.Eof then
      begin
        AIDCard.WorkSpot       := oqWorkspot.FieldAsString('DESCRIPTION');
        AIDCard.DepartmentCode := oqWorkspot.FieldAsString('DEPARTMENT_CODE')
      end
}
      if oqDeterminePreviousWorkSpot.FieldAsString('WDESCRIPTION') <> '' then
      begin
        AIDCard.WorkSpot       :=
          oqDeterminePreviousWorkSpot.FieldAsString('WDESCRIPTION');
        AIDCard.DepartmentCode :=
          oqDeterminePreviousWorkSpot.FieldAsString('WDEPARTMENT_CODE');
      end
      else
      begin
        AIDCard.WorkSpot := SPimsStartDay;
        AIDCard.Job      := SPimsStartDay;
        AIDCard.JobCode  := ''; // PIM-181
        Exit;
      end;
    end;
    // Get Job-description
    if AIDCard.JobCode <> DUMMYSTR then
    begin
      // PIM-346
{
      oqJob.ClearVariables;
      oqJob.SetVariable('PLANT_CODE',    AIDCard.PlantCode);
      oqJob.SetVariable('WORKSPOT_CODE', AIDCard.WorkspotCode);
      oqJob.SetVariable('JOB_CODE',      AIDCard.JobCode);
      oqJob.Execute;
      if not oqJob.Eof then
        AIDCard.Job := oqJob.FieldAsString('DESCRIPTION')
}
      if oqDeterminePreviousWorkSpot.FieldAsString('JDESCRIPTION') <> '' then
        AIDCard.Job := oqDeterminePreviousWorkSpot.FieldAsString('JDESCRIPTION')
      else
      begin
        AIDCard.Job := SPimsStartDay;
        Exit;
      end;
    end;
  finally
  end;
end; // DeterminePreviousWorkSpot

// PIM-179
function TTimeRecordingDM.CheckAddJobForMachine(APlantCode, AMachineCode,
  AJobCode, AJobDescription: String): Boolean;
begin
  Result := True;
  try
    with TimeRecordingDM do
    begin
      oqCheckMachJob.ClearVariables;
      oqCheckMachJob.SetVariable('PLANT_CODE',    APlantCode);
      oqCheckMachJob.SetVariable('MACHINE_CODE',  AMachineCode);
      oqCheckMachJob.SetVariable('JOB_CODE',      AJobCode);
      oqCheckMachJob.Execute;
      if oqCheckMachJob.Eof then
      begin
        // Add the job for the workspot.
        oqInsertMachJob.ClearVariables;
        oqInsertMachJob.SetVariable('PLANT_CODE',    APlantCode);
        oqInsertMachJob.SetVariable('MACHINE_CODE',  AMachineCode);
        oqInsertMachJob.SetVariable('JOB_CODE',      AJobCode);
        oqInsertMachJob.SetVariable('DESCRIPTION',   AJobDescription);
        oqInsertMachJob.SetVariable('MUTATOR',
          ORASystemDM.CurrentProgramUser);
        oqInsertMachJob.Execute;
        if ORASystemDM.OracleSession.InTransaction then
          ORASystemDM.OracleSession.Commit;
      end;
    end;
  except
    on E: EOracleError do
    begin
      if ORASystemDM.OracleSession.InTransaction then
        ORASystemDM.OracleSession.Rollback;
      WErrorLog(E.Message);
      Result := False;
    end;
    on E: Exception do
    begin
      if ORASystemDM.OracleSession.InTransaction then
        ORASystemDM.OracleSession.Rollback;
      WErrorLog(E.Message);
      Result := False;
    end;
  end;
end; // CheckAddJobForMachine

// PIMS-203
procedure TTimeRecordingDM.AddLog(AMsg: String);
begin
  if UseLogging then // PIM-346
    WDebugLog(AMsg);
end; // AddLog

// PIMS-203 Moved to TimeRecordingDMT:
procedure TTimeRecordingDM.UpdateTRSMultipleScan(AIDCard: TScannedIDCard;
  ACurrentNow: TDateTime);
var
  StartDate, EndDate: TDateTime;
begin
  // RV064.1.
  try
// 20015346
{
When changing an existing scan-record (because of a job-change within 1 minute):
- MutationDate := SYSDATE
}
    // 20013489
    AProdMinClass.GetShiftDay(AIDCard, StartDate, EndDate);
    AIDCard.ShiftDate := Trunc(StartDate);
    with TimeRecordingDM.oqUpdateTRSMultipleScan do
    begin
      ClearVariables;
      SetVariable('WORKSPOT_CODE',   AIDCard.WorkSpotCode);
      SetVariable('JOB_CODE',        AIDCard.JobCode);
      SetVariable('SHIFT_NUMBER',    AIDCard.ShiftNumber);
      SetVariable('IDCARD_OUT',      AIDCard.IDCard);
      SetVariable('MUTATOR',         ORASystemDM.CurrentComputerName);
      SetVariable('DATETIME_IN',     AIDCard.DateIn);
      SetVariable('EMPLOYEE_NUMBER', AIDCard.EmployeeCode);
      // 20013489
      SetVariable('SHIFT_DATE',      AIDCard.ShiftDate);
      // PIM-12
      SetVariable('MUTATIONDATE',    ACurrentNow);
      Execute;
    end; {with}
  except
    on E: EOracleError do
    begin
      if ORASystemDM.OracleSession.InTransaction then
        ORASystemDM.OracleSession.Rollback;
      WErrorLog(E.Message);
      FOnScanError(E.Message);
    end;
    on E: Exception do
    begin
      if ORASystemDM.OracleSession.InTransaction then
        ORASystemDM.OracleSession.Rollback;
      WErrorLog(E.Message);
      FOnScanError(E.Message);
    end;
  end;
end; // UpdateTRSMultipleScan

// PIMS-203
procedure TTimeRecordingDM.InsertTRSNewScan(var AIDCard: TScannedIDCard;
  const AOpen: Boolean; const AIDCardOut: String; ACurrentNow: TDateTime);
var
  StartDate, EndDate: TDateTime;
begin
  // RV064.1.
  try
    // 20013489
    // 20015346
{
When inserting a new (open) scan-record:
- DateIn := SYSDATE
- CreationDate := SYSDATE
- MutationDate := SYSDATE
}
    // 20015346
    // PIM-12 Bugfix Do not assign the timestamp here! It should be done
    //        already in a earlier stadium.
{
    if ORASystemDM.TimerecordingInSecs then
    begin
      AIDcard.DateIn := Now;
      LastNow := AIDCard.DateIn;
      if not AOpen then
      begin
        AIDCard.DateOut := LastNow;
        LastNow := AIDCard.DateOut;
      end;
    end;
}
    AProdMinClass.GetShiftDay(AIDCard, StartDate, EndDate);
    AIDCard.ShiftDate := Trunc(StartDate);
    with TimeRecordingDM.oqInsertTRSNewScan do
    begin
      ClearVariables;
      SetVariable('DATETIME_IN',     AIDCard.DateIn);
      SetVariable('IDCARD_IN',       AIDCard.IDCard);
      if AOpen then
      begin
        SetVariable('DATETIME_OUT',  Null);
        SetVariable('PROCESSED_YN',  UNCHECKEDVALUE);
        SetVariable('IDCARD_OUT',    '');
      end
      else
      begin
        SetVariable('DATETIME_OUT',  AIDCard.DateOut);
        SetVariable('PROCESSED_YN',  CHECKEDVALUE);
        SetVariable('IDCARD_OUT',    AIDCardOut);
      end;
      SetVariable('EMPLOYEE_NUMBER', AIDCard.EmployeeCode);
      SetVariable('PLANT_CODE',      AIDCard.PlantCode);
      SetVariable('WORKSPOT_CODE',   AIDCard.WorkSpotCode);
      SetVariable('JOB_CODE',        AIDCard.JobCode);
      SetVariable('SHIFT_NUMBER',    AIDCard.ShiftNumber);
      SetVariable('MUTATOR',         ORASystemDM.CurrentComputerName);
      // 20013489
      SetVariable('SHIFT_DATE',      AIDCard.ShiftDate);
      SetVariable('CREATIONDATE',    ACurrentNow);
      SetVariable('MUTATIONDATE',    ACurrentNow);
      Execute;
    end; {with}
  except
    on E: EOracleError do
    begin
      // GLOB3-202
      if E.ErrorCode <> PIMS_EX_UNIQUEFIELD then
      begin
        if ORASystemDM.OracleSession.InTransaction then
          ORASystemDM.OracleSession.Rollback;
        WErrorLog(E.Message);
        FOnScanError(E.Message);
      end;
    end;
    on E: Exception do
    begin
      if ORASystemDM.OracleSession.InTransaction then
        ORASystemDM.OracleSession.Rollback;
      WErrorLog(E.Message);
      FOnScanError(E.Message);
    end;
  end;
end; // InsertTRSNewScan

// PIM-203
procedure TTimeRecordingDM.UpdateTRSCompleteScan(var ANextIDCard,
  ALastIDCard: TScannedIDCard; const Is24HrScan: Boolean;
  ACurrentNow: TDateTime);
var
  StartDate, EndDate: TDateTime;
begin
{$IFDEF DEBUG}
  WDebugLog('- UpdateTRSCompleteScan() ' +
    ' ALastIDCard.EmployeeCode=' + IntToStr(ALastIDCard.EmployeeCode) +
    ' ALastIDCard.DateIn=' + DateTimeToStr(ALastIDCard.DateIn)
    );
{$ENDIF}
// 20015346
{
When closing an existing scan-record:
- DateOut := SYSDATE
- MutationDate := SYSDATE
}
  // RV064.1.
  try
    // 20015346
    // PIM-12 Bugfix Do not assign the timestamp here! It should be done
    //        already in a earlier stadium.
{
    if ORASystemDM.TimerecordingInSecs then
    begin
      if Is24HrScan then
      begin
        ALastIDCard.DateOut := Now;
        LastNow := ALastIDCard.DateOut;
      end
      else
      begin
        ANextIDCard.DateIn := Now;
        LastNow := ANextIDCard.DateIn;
      end;
    end;
}
    // 20013489
    AProdMinClass.GetShiftDay(ALastIDCard, StartDate, EndDate);
    ALastIDCard.ShiftDate := Trunc(StartDate);
    with TimeRecordingDM.oqUpdateTRSCompleteScan do
    begin
      ClearVariables;
      SetVariable('PROCESSED_YN',    CHECKEDVALUE);
      SetVariable('EMPLOYEE_NUMBER', ALastIDCard.EmployeeCode);
      SetVariable('IDCARD_OUT',      ALastIDCard.IDCard);
      if Is24HrScan then
        SetVariable('DATETIME_OUT',  ALastIDCard.DateOut)
      else
      begin
        SetVariable('DATETIME_OUT',  ANextIDcard.DateIn);
        ALastIDCard.DateOut := ANextIDCard.DateIn; // GLOB3-202
      end;
      SetVariable('DATETIME_IN',     ALastIDCard.DateIn);
      SetVariable('MUTATOR',         ORASystemDM.CurrentComputerName);
      // 20013489
      SetVariable('SHIFT_DATE',      ALastIDCard.ShiftDate);
      // PIM-12
      if ORASystemDM.TimerecordingInSecs then
      begin
        if Is24HrScan then
          SetVariable('MUTATIONDATE',    ALastIDCard.DateOut)
        else
          SetVariable('MUTATIONDATE',    ANextIDCard.DateIn);
      end
      else
      begin
        SetVariable('MUTATIONDATE',      ACurrentNow);
      end;
      Execute;
    end; {with}
  except
    on E: EOracleError do
    begin
      if ORASystemDM.OracleSession.InTransaction then
        ORASystemDM.OracleSession.Rollback;
      WErrorLog(E.Message);
      FOnScanError(E.Message);
    end;
    on E: Exception do
    begin
      if ORASystemDM.OracleSession.InTransaction then
        ORASystemDM.OracleSession.Rollback;
      WErrorLog(E.Message);
      FOnScanError(E.Message);
    end;
  end;
end; // UpdateTRSCompleteScan

// PIM-203
procedure TTimeRecordingDM.Determine24HourScan(var AEndOfDayError: Boolean;
  var ASavePrevPlant, ASavePrevWK, ASavePrevJob, ASavePrevDate: String;
  var ALastIDCard, ANextIDCard: TScannedIDCard; ACurrentNow: TDateTime;
  var AEditPrePlantText, AEditPreDateFromText, AEditPreWorkspotText,
  AEditPreJobText: String);
var
  IDCardOut: String;
  UpdateProdMinOut, UpdateProdMinIn: Boolean;
  ProdDate2: TDateTime;
  StartDate, EndDate: TDateTime;
  EmployeeNumberDummy: Integer;
  EmployeeNameDummy: String;
begin
  // MR:24-01-2003 - 24-hour scans
  // Exception: If employee presses F10 (End-Of-Day) and there was not a
  // previous-workspot (12-hour-scan), check if there's a 24-hour-scan.
  // (max. 24 hours before).
  // If this is the case create TRS-records till now, where each record
  // has a maximum of 12 hours.
  if (ALastIDCard.DateIn = NullDate) and (ALastIDCard.WorkSpot = SPimsStartDay) then
  begin
    AddLog('  Determine24HourScan');
    AddLog('    DeterminePreviousWorkspot');
    TimeRecordingDM.DeterminePreviousWorkSpot(ANextIDCard.DateIn,
      ALastIDCard, ACurrentNow, True, False,
      EmployeeNumberDummy, EmployeeNameDummy);
    // We found a previous workspot?
    if ALastIDCard.WorkSpot <> SPimsStartDay then
    begin
      // First update last TRS-record.
      // Is it in reach of 12 hours?
      if ALastIDCard.DateIn + 0.5 < ANextIDCard.DateIn then
        ALastIDCard.DateOut := ALastIDCard.DateIn + 0.5
      else
        ALastIDCard.DateOut := ANextIDCard.DateIn; // end-datetime
      IDCardOut := ALastIDCard.IDCard;
      if ALastIDCard.DateOut < ANextIDCard.DateIn then
        IDCardOut := '';
      //Completing time recording scanning oqUpdateTRSCompleteScan
      AddLog('    Update TRS');
      UpdateTRSCompleteScan(ANextIDCard, ALastIDCard, True{Is24HrScan}, ACurrentNow);

      ASavePrevPlant := ALastIDCard.Plant;
      ASavePrevWK    := ALastIDCard.WorkSpot;
      ASavePrevJob   := ALastIDCard.Job;
      ASavePrevDate  := FormatDateTime(LONGDATE, ALastIDCard.DateIn);
//      Update;

      // MR:30-10-2003 'UpdateProdMin' divided over 2 booleans.
      UpdateProdMinOut := True;
      UpdateProdMinIn  := False;
      if Trunc(ALastIDCard.DateIn) <> Trunc(ALastIDCard.DateOut) then
        UpdateProdMinIn := True;
      AddLog('    ProcessTimeRecording');
      GlobalDM.ProcessTimeRecording(ALastIDCard, ALastIDCard,
        True, (AEditPrePlantText = SpimsEndDay),
        True, //IsScanning, // MR:23-1-2004
        UpdateProdMinOut, UpdateProdMinIn, False,
        (AEditPrePlantText = SpimsEndDay) {LastScanRecordForDay} );
      // MR:01-10-2003 END
      ALastIDCard.DateIn := ALastIDCard.DateOut;
      while ALastIDCard.DateIn < ANextIDCard.DateIn do
      begin
        if ALastIDCard.DateIn + 0.5 < ANextIDCard.DateIn then
          ALastIDCard.DateOut := ALastIDCard.DateIn + 0.5
        else
          ALastIDCard.DateOut := ANextIDCard.DateIn; // end-datetime
        IDCardOut := ALastIDCard.IDCard;
        if ALastIDCard.DateOut < ANextIDCard.DateIn then
          IDCardOut := '';
        // Insert new TRS-record
        // Register time recording scanning
        AddLog('    Insert TRS');
        InsertTRSNewScan(ALastIDCard, False{Open=False}, IDCardOut, ACurrentNow);

        // Show some information
        //CAR 24-03-2003 - WAS NEXT
        // MR:02-01-2007 Order 550433
        // Instead of start-time (DateIn), the end-time (DateOut) must be shown,
        // because this is an additional scan for dividing a 24 hour-scan.
        AEditPreDateFromText := FormatDateTime(LONGDATE, ALastIDCard.DateOut
          {ALastIDCard.DateIn});
        AEditPrePlantText    := ALastIDCard.Plant;
        AEditPreWorkspotText := ALastIDCard.WorkSpot;
        AEditPreJobText      := ALastIDCard.Job;

        if Assigned(EnabledWk_JobField) then
          EnabledWk_JobField(True);
        //
//        Update;
        // MR:30-10-2003 'UpdateProdMin' divided over 2 booleans.
        UpdateProdMinOut := True;
        UpdateProdMinIn  := False;
        if Trunc(ALastIDCard.DateIn) <> Trunc(ALastIDCard.DateOut) then
          UpdateProdMinIn := True;
        AddLog('    ProcessTimeRecording');
        GlobalDM.ProcessTimeRecording(ALastIDCard, ALastIDCard,
          True, (AEditPrePlantText = SpimsEndDay),
          True, //IsScanning, // MR:23-1-2004
          UpdateProdMinOut, UpdateProdMinIn, False,
          (AEditPrePlantText = SpimsEndDay) {LastScanRecordForDay} );
        // MR:01-10-2003 END
        ALastIDCard.DateIn := ALastIDCard.DateOut;
        // MR:03-01-2005 To prevent errors with rounding we do this.
        if Trunc(ALastIDCard.DateOut) <> Trunc(ALastIDCard.DateIn) then
          ProdDate2 := Trunc(ALastIDCard.DateIn)
        else
          ProdDate2 := NullDate;
        AddLog('    UnprocessProcessScans');
        // MR:08-03-2005 Order 550387
        // Use 'GetShiftDay' to get the correct 'startdate'.
        AProdMinClass.GetShiftDay(ALastIDCard, StartDate, EndDate);
        // 20013489
        ALastIDCard.ShiftDate := Trunc(StartDate);
        GlobalDM.UnprocessProcessScans(ALastIDCard,
          Trunc(StartDate), //Trunc(ALastIDCard.DateIn),
          Trunc(ALastIDCard.DateOut), ProdDate2, NullDate, NullDate,
          True, // 20013489 Always do this, not only for 'IsScanning'.
          // IsScanning, {AProcessSalary}// MR:23-1-2004
          False, {ADeleteAction}
          False {ARecalcProdHours} ); // RV056.1.
      end; // while ALastIDCard.DateIn < NextIDCard.DateIn do
    end // if ALastIDCard.WorkSpot <> SPimsStartDay then
    else
      AEndOfDayError := True;
  end; {(ALastIDCard.DateIn = NullDate) and (ALastIDCard.WorkSpot = SPimsStartDay)}
end; // Determine24HourScan

// PIM-203
function TTimeRecordingDM.HandleScanAction(ALastIDCard,
  ANextIDCard: TScannedIDCard; APrePlantText: String;
  AIsScanning: Boolean; ASavePrevPlant, ASavePrevWK, ASavePrevJob,
  ASavePrevDate: String;
  ACurrentNow: TDateTime;
  var ALastNow: TDateTime;
  var AEditPrePlantText, AEditPreDateFromText, AEditPreWorkspotText,
  AEditPreJobText: String;
  ADoNotScanOut: Boolean=False): Boolean;
var
  StartDate, EndDate, ProdDate2: TDateTime;
  EndOfDayError, Done: Boolean;
  // 20014450.50
  function MultipleScan: Boolean;
  begin
    if ORASystemDM.TimerecordingInSecs then
      Result := (ALastIDCard.DateIn = ANextIDCard.DateIn)
    else
      Result :=
        (RoundTime(ALastIDCard.DateIn, 1) = RoundTime(ANextIDCard.DateIn, 1));
  end;
begin
  Done := False;
  EndOfDayError := False;
  // PersonalScreen - Clear values first!
  EmptyIDCard(TimeRecordingDM.LastIDCard);
  EmptyIDCard(TimeRecordingDM.NextIDCard);
  try
    // RV064.1.
    try
      // Multiple scan within 1 minute,
      // overwrite the last scan.
      // 20015346 When scans are stored with seconds, then we will not find the
      // scan for the last minute, so we must use the RoundMin here during compare.
      // 20015346 Also check if end-of-day was choosen.
      if (APrePlantText <> SpimsEndDay) and
        (ALastIDCard.DateIn > NullDate) and
        MultipleScan then // 20015346 // 20014450.50
      begin
        AddLog('  UpdateTRSMultipleScan');
        // 20015346 When scans are stored in seconds, search for LastIDCard,
        // not for NextIDCard, or we will not find the last scan
        // within the last minute.
        // 20015346 Assign the DateIn of LastIDCard to NextIDCard, or we will
        //          not find the previous scan, but use NextIDCard because
        //          other fields can be changed (like job-change).
        if ORASystemDM.TimerecordingInSecs then // 20015346
        begin
          ANextIDCard.DateIn := ALastIDCard.DateIn;
          ALastNow := ALastIDCard.DateIn;
        end;
        UpdateTRSMultipleScan(ANextIDCard, ACurrentNow);
        Done := True;
      end
      else
      begin {else (ALastIDCard.DateIn > NullDate) and (ALastIDCard.DateIn = ANextIDCard.DateIn)}
        // start of day + regular scan
        // Insert a new (open) timercording-scan-record.
        if APrePlantText <> SpimsEndDay then
        begin
          AddLog('  InsertTRSNewOpenScan:' + IDCardToStr(ANextIDCard));
          InsertTRSNewScan(ANextIDCard, True{Open}, NullStr, ACurrentNow);
          Done := True;
        end;
        // regular scan + end of day
        // Update and complete an existing timerecording-scan-record.
        if ASavePrevPlant <> SPimsStartDay then
        begin
          AddLog('  UpdateTRSCompleteScan:' + IDCardToStr(ALastIDCard));
          UpdateTRSCompleteScan(ANextIDCard, ALastIDCard, False{Is24HrScan},
            ACurrentNow);
          // MR:14-10-2003
          if Trunc(ALastIDCard.DateOut) <> Trunc(ALastIDCard.DateIn) then
            ProdDate2 := Trunc(ALastIDCard.DateIn)
          else
            ProdDate2 := NullDate;

          // 20013489.10
          // This will only book the production hours for current scan!
          GlobalDM.ProcessTimeRecording(ALastIDCard, ALastIDCard,
            True, True,
            True,
            True, True,
            False,
            (APrePlantText = SpimsEndDay) {LastScanRecordForDay} );

//          AddLog('  UnprocessProcessScans');
          // MR:08-03-2005 Order 550387
          // Use 'GetShiftDay' to get the correct 'startdate'.
          AProdMinClass.GetShiftDay(ALastIDCard, StartDate, EndDate);
          // 20013489
          // 20013489.10 Be sure we do NOT process the production hours again!
          ALastIDCard.ShiftDate := Trunc(StartDate);
          GlobalDM.UnprocessProcessScans(ALastIDCard,
              Trunc(StartDate), //Trunc(ALastIDCard.DateIn),
              Trunc(ALastIDCard.DateOut), ProdDate2, NullDate, NullDate,
              True, // 20013489 Always do this, not only for 'IsScanning'.
              // AIsScanning, {AProcessSalary}// MR:23-1-2004
              False, {ADeleteAction}
              False {ARecalcProdHours} ); // RV056.1.

          // GLOB3-202
          if ORASystemDM.TimeRecCRDoNotScanOut or ADoNotScanOut then
          begin
            // End of Day:
            // Create a new open scan for roaming workspot,
            // but only when last open scan was NOT the Roaming workspot
            if RoamingWorkspotCode <> '' then
              if ALastIDCard.WorkSpotCode <> RoamingWorkspotCode then
              begin
                ALastIDCard.WorkSpotCode := RoamingWorkspotCode;
                ALastIDCard.JobCode := RoamingJobCode;
                ALastIDCard.DateIn := ALastIDCard.DateOut;
                AddLog('  InsertTRSNewOpenScan:' + IDCardToStr(ALastIDCard));
                InsertTRSNewScan(ALastIDCard, True{Open}, NullStr, ACurrentNow);
              end;
          end;

          Done := True;
        end;
      end;  {else (ALastIDCard.DateIn > NullDate) and (ALastIDCard.DateIn = ANextIDCard.DateIn)}

      if not Done then
        Determine24HourScan(EndOfDayError, ASavePrevPlant, ASavePrevWK,
          ASavePrevJob,  ASavePrevDate, ALastIDCard, ANextIDCard,
          ACurrentNow, AEditPrePlantText, AEditPreDateFromText,
          AEditPreWorkspotText, AEditPreJobText);

      // This is used by TimeRecording itself.
      if Assigned(LabelErrorMsgDone) then
        LabelErrorMsgDone;
//      LabelErrorMsg.Font.Color := clWindowText;
//      LabelErrorMsg.Caption    := SPimsScanDone;

      // MR:28-01-2003
      if EndOfDayError then
        FOnScanError(SPimsNoPrevScanningFound);
      // PIM-203
{      else
      begin
        // PIM-50
        if ORASystemDM.AddJobComment then
          EnterJobCommentDM.EnterJobComment(
            ALastIDCard.PlantCode,
            ALastIDCard.WorkSpotCode,
            ALastIDCard.JobCode,
            AlastIDCard.EmployeeCode,
            TimeRecordingF);
      end; }
    except
      on E:EOracleError do
      begin
        if ORASystemDM.OracleSession.InTransaction then
          ORASystemDM.OracleSession.Rollback;
        WErrorLog(E.Message);
        FOnScanError(E.Message);
      end;
      on E: Exception do
      begin
        if ORASystemDM.OracleSession.InTransaction then
          ORASystemDM.OracleSession.Rollback;
        WErrorLog(E.Message);
        FOnScanError(E.Message);
      end;
    end;
  finally
    // PersonalScreen - Store values for later use
    CopyIDCard(TimeRecordingDM.LastIDCard, ALastIDCard);
    CopyIDCard(TimeRecordingDM.NextIDCard, ANextIDCard);
    Result := Done;
  end;
end; // HandleScanAction

// PIM-203
function TTimeRecordingDM.ValueIDCard(ACode: String; ACurrentNow: TDateTime): TErrorCode;
var
  EmployeeNumber: Integer;
begin
  Result := ecUnknown;  // Unknown error
  EmployeeNumber := -1;
  with TimeRecordingDM do
  begin
    oqIDCard.ClearVariables;
    oqIDCard.SetVariable('IDCARD_NUMBER', ACode);
    oqIDCard.Execute;
    if not oqIDCard.Eof then
    begin
      EmployeeNumber := oqIDCard.FieldAsInteger('EMPLOYEE_NUMBER'); // TD-24520
      // RV076.3. This failed when 'enddate' was same as today: Use 'trunc'!
      if (not oqIDCard.FieldIsNull('ENDDATE')) and
        (oqIDCard.FieldAsDate('ENDDATE') < Trunc(ACurrentNow)) then
          Result := ecIDCardExpired;  // ID-card expired
      oqEmployee.ClearVariables;;
      oqEmployee.SetVariable('EMPLOYEE_NUMBER',
        oqIDCard.FieldAsInteger('EMPLOYEE_NUMBER'));
      oqEmployee.Execute;
      if not oqEmployee.Eof then
      begin
        // RV076.3. This failed when 'enddate' was same as today: Use 'trunc'!
        if (not oqEmployee.FieldIsNull('ENDDATE')) and
          (oqEmployee.FieldAsDate('ENDDATE') < Trunc(ACurrentNow)) then
          Result := ecEmployeeExpired  // Employee has expired
        else
          // RV072.4.
          if (oqEmployee.FieldAsString('IS_SCANNING_YN') = UNCHECKEDVALUE) and
            (oqEmployee.FieldAsString('BOOK_PROD_HRS_YN') = CHECKEDVALUE) then
            Result := ecNonScanEmployee;
      end
      else
        Result := ecEmployeeNotFound;  // Employee not found
    end
    else
      Result := ecIDCardNotFound; // PIM-203 // This was missing!
    if Result <> ecUnknown then
      Exit;
    // Value existing completed scans that include current date/time
    if Result = ecUnknown then
    begin
      with oqScanExists do
      begin
        ClearVariables;
        SetVariable('EMPLOYEE_NUMBER', EmployeeNumber); // TD-24520
        SetVariable('STARTDATE',       RoundTimeConditional(ACurrentNow, 1)); // 20015346
        Execute;
        if not Eof then
        begin
          Result := ecScanExists;  // Scan exists already
          Exit;
        end;
      end; {with oqScanExists}
    end; {if Result = ecUnknown};
  end; {with TimeRecordingDM}
  Result := ecOK; // OK
end; // ValueIDCard

// PIM-203
function TTimeRecordingDM.ErrorToString(AErrorCode: TErrorCode): String;
begin
  Result := '';
  case AErrorCode of // add other deafult values
    ecIDCardNotFound   : // ID Card not found
      Result := SPimsIDCardNotFound;
    ecIDCardExpired    : // ID Card expired
      Result := SPimsIDCardExpired;
    ecEmployeeExpired  : // Employee has expired
      Result := SPimsInactiveEmployee;
    ecEmployeeNotFound : // Employee not found
      Result := SPimsEmployeeNotFound;
    ecScanExists       : // There already is a scan that includes this time
      Result := SPimsDateinPreviousPeriod;
    ecNonScanEmployee  : // Employee is non-scanner+book-prod-hrs // RV072.4.
      Result :=  SPimsNonScanEmployee;
  end;
end; // ErrorToString

// PIM-203
procedure TTimeRecordingDM.PopulateIDCard(var AIDCard: TScannedIDCard);
begin
  with TimeRecordingDM do
  begin
    if (AIDCard.IDCard <> NullStr) then
    begin
      oqIDCard.ClearVariables;
      oqIDCard.SetVariable('IDCARD_NUMBER', AIDCard.IDCard);
      oqIDCard.Execute;
      if not oqIDCard.Eof then
      begin
        AIDCard.EmployeeCode :=
          oqIDCard.FieldAsInteger('EMPLOYEE_NUMBER');
      end;
    end;
    oqEmployee.ClearVariables;
    oqEmployee.SetVariable('EMPLOYEE_NUMBER', AIDCard.EmployeeCode);
    oqEmployee.Execute;
    if not oqEmployee.Eof then
    begin
      AIDCard.EmplName  := oqEmployee.FieldAsString('DESCRIPTION');
      AIDCard.CutOfTime := oqEmployee.FieldAsString('CUT_OF_TIME_YN');
//      IsScanning := (oqEmployee.FieldAsString('IS_SCANNING_YN') = 'Y'); // PIM-203 Not needed!
      AIDCard.ShiftNumber :=
        oqEmployee.FieldAsInteger('SHIFT_NUMBER'); // PIM-203 Also fill this!
    end;
  end; // with
  AIDCard.FromTimeRecordingApp := True;
end; // PopulateIDCard

// PIM-254
function TTimeRecordingDM.IDConvertRosslare(AID: String): String;
var
  Part1, Part2: String;
begin
  Result := AID;
  if ReaderRosslare then
  begin
    // Split first in 2 parts
    try
      Part1 := Copy(AID, 1, 4);
      Part2 := Copy(AID, 5, 4);
      Result := IntToStr(StrToInt('$' + Part1)) +
        IntToStr(StrToInt('$' + Part2));
    except
      on E: Exception do
        WErrorLog(E.Message);
    end;
  end;
end; // IDConvertRosslare

// GLOB3-112
function TTimeRecordingDM.DetermineIDCardRaw(AID: String): String;
begin
  Result := AID;
  with oqIDCardRaw do
  begin
    ClearVariables;
    SetVariable('IDCARD_RAW', AID);
    Execute;
    if not Eof then
      Result := FieldAsString('IDCARD_NUMBER');
  end;
end; // DetermineIDCardRaw

// GLOB3-139
function TTimeRecordingDM.H5427Convert(AID: String): String;
  function BinToInt(Value: string): Integer;
  var
    i, iValueSize: Integer;
  begin
    Result := 0;
    iValueSize := Length(Value);
    for i := iValueSize downto 1 do
      if Value[i] = '1' then Result := Result + (1 shl (iValueSize - i));
  end; // BinToInt
  function IntToBin(d: Longint): String;
  var
    x: Integer;
    bin: string;
  begin
    bin := '';
    for x := 1 to 8 * SizeOf(d) do
    begin
      if Odd(d) then bin := '1' + bin
      else
        bin := '0' + bin;
      d := d shr 1;
    end;
    Delete(bin, 1, 8 * ((Pos('1', bin) - 1) div 8));
    Result := bin;
  end; // IntToBin
begin
{
When reading ID 27110, it will return:
0110100111100110
This must be converted to decimal 27110.
}
{
It is also possible to configure the reader it returns decimal value:
3986380 (hex: 003CD3CC)
with ID=27110 (printed on card)
This is hex:
3CD3CC
This in bin:
001111001101001111001100
This must be converted to:
0110100111100110
(remove 7 bits from start and remove 1 bit from end)
Convert to dec: 27110

}
  Result := AID;
  try
    if Length(AID) = 16 then // 16 bits received?
      Result := IntToStr(BinToInt(AID))
    else
    begin
      Result := IntToBin(StrToInt(AID));
      if Length(Result) = 24 then
      begin
        Result := Copy(Result, 8, 16);
        Result := IntToStr(BinToInt(Result));
      end
      else
        Result := AID;
    end;
  except
    on E: Exception do
    begin
      Result := AID;
      WErrorLog(E.Message);
    end;
  end;
end; // H5427Convert

// GLOB3-202
procedure TTimeRecordingDM.DetermineRoamingWorkspot(APlantCode: String);
begin
  RoamingWorkspotCode := '';
  RoamingJobCode := '';
  with oqRoamingWorkspot do
  begin
    ClearVariables;
    SetVariable('PLANT_CODE', APlantCode);
    Execute;
    if not Eof then
    begin
      RoamingWorkspotCode := FieldAsString('WORKSPOT_CODE');
      RoamingJobCode := FieldAsString('JOB_CODE');
    end;
  end;
end; // DetermineRoamingWorkspot

end.
