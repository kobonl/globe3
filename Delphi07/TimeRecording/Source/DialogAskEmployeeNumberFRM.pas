(*
  MRA:8-JUN-2012 20013315
  - Make option in Timerecording to enter emp.nr. belonging to ID.
*)
unit DialogAskEmployeeNumberFRM;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseDialogFRM, dxCntner, Menus, StdActns, ActnList, ImgList,
  StdCtrls, Buttons, ComCtrls, ExtCtrls, jpeg;

type
  TDialogAskEmployeeNumberF = class(TBaseDialogForm)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    EditEmpNr: TEdit;
    procedure btnOkClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    FEmployeeNumber: String;
  public
    { Public declarations }
    property EmployeeNumber: String read FEmployeeNumber write FEmployeeNumber;
  end;

var
  DialogAskEmployeeNumberF: TDialogAskEmployeeNumberF;

implementation

{$R *.dfm}

procedure TDialogAskEmployeeNumberF.btnOkClick(Sender: TObject);
begin
  inherited;
  EmployeeNumber := EditEmpNr.Text;
end;

procedure TDialogAskEmployeeNumberF.FormShow(Sender: TObject);
begin
  inherited;
  EditEmpNr.Text := '';
  EditEmpNr.SetFocus;
end;

end.
