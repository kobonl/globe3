unit HomeFRM;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, URFIDeasSocketPort, IdException, IniFiles;

const
  DefaultConnectTimeOut=10000;
  DefaultINIFilename='CardReaderTest.INI';

type
  TForm1 = class(TForm)
    Memo1: TMemo;
    edtHost: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    edtPort: TEdit;
    Timer1: TTimer;
    cboxTest: TCheckBox;
    btnLEDRed: TButton;
    btnLEDGreen: TButton;
    btnSave: TButton;
    CheckBoxRosslare: TCheckBox;
    procedure cboxTestClick(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnLEDRedClick(Sender: TObject);
    procedure btnLEDGreenClick(Sender: TObject);
    procedure btnSaveClick(Sender: TObject);
    procedure CheckBoxRosslareClick(Sender: TObject);
  private
    { Private declarations }
    AppRootPath: String;
    FARFSocketPort: TRFSocketPort;
    FHost: String;
    FPort: String;
    FINIFilename: String;
    procedure AssignSocketPort;
    function RFSocketPortHandler: Boolean;
    procedure ReadINIFile;
    procedure WriteINIFile;
  public
    { Public declarations }
    property ARFSocketPort: TRFSocketPort read FARFSocketPort write FARFSocketPort;
    property Host: String read FHost write FHost;
    property Port: String read FPort write FPort;
    property INIFilename: String read FINIFilename write FINIFilename;
  end;

procedure WErrorLog(AMsg: String);
procedure WLog(AMsg: String);

var
  Form1: TForm1;

implementation

{$R *.dfm}

function PathCheck(Path: String): String;
begin
  if Path <> '' then
    if Path[length(Path)] <> '\' then
      Path := Path + '\';
  Result := Path;
end;

procedure WErrorLog(AMsg: String);
begin
  Form1.Memo1.Lines.Add(AMsg);
end;

procedure WLog(AMsg: String);
begin
  Form1.Memo1.Lines.Add(AMsg);
end;

procedure TForm1.AssignSocketPort;
begin
  try
WErrorLog('-> AssignSocketPort');
    URFIDeasSocketPort.AssignSocketPortList(
      edtHost.Text, StrToInt(edtPort.Text), 8,
      '1', '21331');
    ARFSocketPort := RFSocketPortList[0];
    ARFSocketPort.Host := edtHost.Text;
    ARFSocketPort.Port := StrToInT(edtPort.Text);
    ARFSocketPort.IDLength := 8;
    ARFSocketPort.MyErrorLog := WErrorLog;
    ARFSocketPort.MyLog := WLog;
    ARFSocketPort.OnProcessScan := nil; //ProcessOneScan;
    ARFSocketPort.OnHandleLEDS := nil; //HandleLEDS;
    ARFSocketPort.ConnectTimeOut := DefaultConnectTimeOut;
  except
    on E:EAccessViolation do
    begin
      WErrorLog(E.Message);
    end;
    on E:EIdClosedSocket do
    begin
      WErrorLog(E.Message);
    end;
    on E:Exception do
    begin
      WErrorLog(E.Message);
    end;
  end;
end;

procedure TForm1.cboxTestClick(Sender: TObject);
begin
  if cBoxTest.Checked then
  begin
    if not Assigned(Self.ARFSocketPort) then
      AssignSocketPort;
    Timer1.Enabled := True;
    btnLEDRed.Enabled := True;
    btnLEDGreen.Enabled := True;
  end
  else
  begin
    btnLEDRed.Enabled := False;
    btnLEDGreen.Enabled := False;
    Timer1.Enabled := False;
  end;
end;

function TForm1.RFSocketPortHandler: Boolean;
begin
  Result := False;
  try
    // PortConnect must ALWAYS be done, to assign the correct host + port
{
    if not ARFSocketPort.Open then
    begin
      AddLog('Host;Port:' + ARFSocketPort.ToString + '. Opening port...');
      ARFSocketPort.PortConnect(ARFSocketPort.Host, ARFSocketPort.Port);
      AddLog('Host;Port:' + ARFSocketPort.ToString + '. Port opened');
    end;
    if ARFSocketPort.Open then
}
    // Troubleshooting: (11-JAN-2017)
    // - When a reader is not available anymore then PortConnect still gives
    //   True. To solve this, try to send a command to the reader every minute
    //   or every 5 minutes, to know if it is still available.
WErrorLog('->PortConnect');
    if ARFSocketPort.PortConnect(ARFSocketPort.Host, ARFSocketPort.Port) then
    begin
      try
WErrorLog('->Connected');
        if (ARFSocketPort.ReadBuffer <> '') and
          ARFSocketPort.IDRead or ARFSocketPort.ColorRead then
        begin
          if ARFSocketPort.IDRead then
          begin
            Result := True;
            WErrorLog('Buffer=' + ARFSocketPort.ReadBuffer);
            // Buffer has already changed! Use ID
//            AssignWSIDList(ARFSocketPort.ReadBuffer,
//              ARFSocketPort.PlantCode, ARFSocketPort.WorkspotCode);
            ARFSocketPort.IDRead := False;
          end
          else
          begin
            if ARFSocketPort.ColorRead then
            begin
              WErrorLog('Color=' + ARFSocketPort.ReadBuffer);
              ARFSocketPort.ColorRead := False;
            end;
          end;
          ARFSocketPort.ReadBuffer := '';
        end;
      except
        on E:EAccessViolation do
        begin
          WErrorLog(E.Message);
        end;
        on E:EIdClosedSocket do
        begin
          WErrorLog(E.Message);
        end;
        on E:Exception do
        begin
          WErrorLog(E.Message);
        end;
      end;
    end
    else
    begin
      WErrorLog('Error: ' + ARFSocketPort.ToString +
        '. Could not open port!');
    end;
  except
    on E:EAccessViolation do
    begin
      WErrorLog(E.Message);
    end;
    on E:EIdClosedSocket do
    begin
      WErrorLog(E.Message);
    end;
    on E:Exception do
    begin
      WErrorLog(E.Message);
    end;
  end;
end;

procedure TForm1.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled := False;
  try
WErrorLog('RFSocketPortHandler');
    RFSocketPortHandler;
  finally
    Timer1.Enabled := True;
  end;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  AppRootPath := GetCurrentDir;
  INIFilename := PathCheck(AppRootPath) + DefaultINIFilename;
  ARFSocketPort := nil;
  btnLEDGreen.Enabled := False;
  btnLEDRed.Enabled := False;
  ReadINIFile;
  edtHost.Text := Host;
  edtPort.Text := Port;
  ReaderRosslare := CheckBoxRosslare.Checked;
end;

procedure TForm1.btnLEDRedClick(Sender: TObject);
begin
  WErrorLog('Red');
  ARFSocketPort.LEDRed;
end;

procedure TForm1.btnLEDGreenClick(Sender: TObject);
begin
  WErrorLog('Green');
  ARFSocketPort.LEDGreen;
end;

procedure TForm1.ReadINIFile;
var
  Ini: TIniFile;
begin
  if not FileExists(INIFilename) then
    WriteINIFile;
  Ini := TIniFile.Create(INIFilename);
  try
    Host := Ini.ReadString('GLOBAL', 'Host', Host);
    Port := Ini.ReadString('GLOBAL', 'Port', Port);
  finally
    Ini.Free;
  end;
end;

procedure TForm1.WriteINIFile;
var
  Ini: TIniFile;
begin
  Ini := TIniFile.Create(INIFilename);
  try
    Ini.WriteString('GLOBAL', 'Host', Host);
    Ini.WriteString('GLOBAL', 'Port', Port);
  finally
    Ini.Free;
  end;
end;

procedure TForm1.btnSaveClick(Sender: TObject);
begin
  Host := edtHost.Text;
  Port := edtPort.Text;
  WriteINIFile;
end;

procedure TForm1.CheckBoxRosslareClick(Sender: TObject);
begin
  ReaderRosslare := CheckBoxRosslare.Checked;
end;

end.
