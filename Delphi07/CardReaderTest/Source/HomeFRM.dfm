object Form1: TForm1
  Left = 180
  Top = 125
  Width = 410
  Height = 656
  Caption = 'Form1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 48
    Top = 12
    Width = 22
    Height = 13
    Caption = 'Host'
  end
  object Label2: TLabel
    Left = 48
    Top = 40
    Width = 19
    Height = 13
    Caption = 'Port'
  end
  object Memo1: TMemo
    Left = 48
    Top = 120
    Width = 305
    Height = 465
    ScrollBars = ssBoth
    TabOrder = 0
  end
  object edtHost: TEdit
    Left = 96
    Top = 8
    Width = 177
    Height = 21
    TabOrder = 1
    Text = '172.16.201.20'
  end
  object edtPort: TEdit
    Left = 96
    Top = 40
    Width = 121
    Height = 21
    TabOrder = 2
    Text = '10001'
  end
  object cboxTest: TCheckBox
    Left = 48
    Top = 88
    Width = 97
    Height = 17
    Caption = 'Test'
    TabOrder = 3
    OnClick = cboxTestClick
  end
  object btnLEDRed: TButton
    Left = 192
    Top = 80
    Width = 75
    Height = 25
    Caption = 'Red'
    TabOrder = 4
    OnClick = btnLEDRedClick
  end
  object btnLEDGreen: TButton
    Left = 280
    Top = 80
    Width = 75
    Height = 25
    Caption = 'Green'
    TabOrder = 5
    OnClick = btnLEDGreenClick
  end
  object btnSave: TButton
    Left = 288
    Top = 8
    Width = 75
    Height = 25
    Caption = 'Save'
    TabOrder = 6
    OnClick = btnSaveClick
  end
  object CheckBoxRosslare: TCheckBox
    Left = 104
    Top = 88
    Width = 65
    Height = 17
    Caption = 'Rosslare'
    TabOrder = 7
    OnClick = CheckBoxRosslareClick
  end
  object Timer1: TTimer
    Enabled = False
    OnTimer = Timer1Timer
    Left = 344
    Top = 8
  end
end
