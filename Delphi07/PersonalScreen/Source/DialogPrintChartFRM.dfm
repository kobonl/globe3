inherited DialogPrintChartF: TDialogPrintChartF
  Width = 517
  Height = 199
  Caption = 'Print Chart'
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlImageBase: TPanel
    Width = 501
    TabOrder = 1
    inherited imgBasePims: TImage
      Left = 384
    end
  end
  inherited stbarBase: TStatusBar
    Top = 58
    Width = 501
  end
  inherited pnlInsertBase: TPanel
    Width = 501
    Height = 21
    Font.Color = clBlack
    Font.Height = -11
    object rGrpOptions: TRadioGroup
      Left = 0
      Top = 0
      Width = 501
      Height = 21
      Align = alClient
      Caption = 'Print Options'
      Columns = 2
      ItemIndex = 0
      Items.Strings = (
        'Portrait'
        'Landscape')
      TabOrder = 0
    end
  end
  inherited pnlBottom: TPanel
    Top = 77
    Width = 501
    inherited btnOKSmall: TBitBtn
      TabOrder = 4
    end
    inherited btnCancelSmall: TBitBtn
      TabOrder = 0
    end
    inherited btnOK: TBitBtn
      Left = 202
      TabOrder = 3
      OnClick = btnOkClick
    end
    inherited btnCancel: TBitBtn
      Left = 316
      TabOrder = 2
    end
    object btnPrinterSetup: TButton
      Left = 88
      Top = 8
      Width = 105
      Height = 25
      Caption = 'Printer Setup'
      TabOrder = 1
      OnClick = btnPrinterSetupClick
    end
  end
  object PrinterSetupDialog1: TPrinterSetupDialog
    Left = 80
    Top = 120
  end
end
