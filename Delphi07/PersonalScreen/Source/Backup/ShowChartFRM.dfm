inherited ShowChartF: TShowChartF
  Left = 256
  Top = 125
  Width = 686
  Height = 487
  Caption = 'Chart'
  OnClose = FormClose
  OnKeyDown = FormKeyDown
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlImageBase: TPanel
    Width = 678
    inherited imgBasePims: TImage
      Left = 380
    end
  end
  inherited stbarBase: TStatusBar
    Top = 414
    Width = 678
    Panels = <
      item
        Text = 'ABS-Group'
        Width = 120
      end
      item
        Width = 100
      end
      item
        Width = 150
      end
      item
        Width = 110
      end
      item
        Width = 110
      end
      item
        Width = 120
      end>
    SimplePanel = False
  end
  inherited pnlInsertBase: TPanel
    Width = 678
    Height = 377
    Font.Color = clBlack
    Font.Height = -12
    object Splitter1: TSplitter
      Left = 0
      Top = 145
      Width = 678
      Height = 3
      Cursor = crVSplit
      Align = alTop
    end
    object pnlAllTopInfo: TPanel
      Left = 0
      Top = 0
      Width = 678
      Height = 145
      Align = alTop
      TabOrder = 0
      object pnlTopInfo: TPanel
        Left = 1
        Top = 1
        Width = 676
        Height = 70
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object pnlStatistics: TPanel
          Left = 0
          Top = 35
          Width = 676
          Height = 35
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object GroupBox1: TGroupBox
            Left = 0
            Top = 0
            Width = 676
            Height = 35
            Align = alClient
            Caption = 'Statistics'
            TabOrder = 0
            object lblTimeInterval: TLabel
              Left = 67
              Top = 15
              Width = 79
              Height = 14
              Caption = 'lblTimeInterval'
            end
            object lblTimeIntTitle: TLabel
              Left = 7
              Top = 15
              Width = 55
              Height = 14
              Caption = 'Time Int.:'
            end
            object lblTimeTitle: TLabel
              Left = 81
              Top = 15
              Width = 31
              Height = 14
              Caption = 'Time:'
            end
            object lblTime: TLabel
              Left = 116
              Top = 15
              Width = 38
              Height = 14
              Caption = 'lblTime'
            end
            object lblProdHrTitle: TLabel
              Left = 249
              Top = 15
              Width = 50
              Height = 14
              Caption = 'Prod./Hr:'
            end
            object lblProdPerHr: TLabel
              Left = 305
              Top = 15
              Width = 66
              Height = 14
              Caption = 'lblProdPerHr'
            end
            object lblProdNormTitle: TLabel
              Left = 353
              Top = 15
              Width = 66
              Height = 14
              Caption = 'Prod. Norm:'
            end
            object lblProdNorm: TLabel
              Left = 425
              Top = 15
              Width = 65
              Height = 14
              Caption = 'lblProdNorm'
            end
            object lblNrOfEmplTitle: TLabel
              Left = 473
              Top = 15
              Width = 65
              Height = 14
              Caption = 'Nr of Empl.:'
            end
            object lblNrOfEmp: TLabel
              Left = 545
              Top = 15
              Width = 60
              Height = 14
              Caption = 'lblNrOfEmp'
            end
            object cBoxShowEmpl: TCheckBox
              Left = 578
              Top = 13
              Width = 89
              Height = 17
              Caption = 'Show Empl.'
              TabOrder = 0
              OnClick = cBoxShowEmplClick
            end
          end
        end
        object pnlDateInput: TPanel
          Left = 0
          Top = 0
          Width = 676
          Height = 35
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 1
          object Panel1: TPanel
            Left = 600
            Top = 0
            Width = 76
            Height = 35
            Align = alRight
            BevelOuter = bvNone
            TabOrder = 0
            object GroupBox3: TGroupBox
              Left = 0
              Top = 0
              Width = 76
              Height = 35
              Align = alClient
              Caption = 'Graph'
              TabOrder = 0
              object cBoxScale: TCheckBox
                Left = 7
                Top = 15
                Width = 54
                Height = 17
                Caption = 'Scale'
                TabOrder = 0
                OnClick = cBoxScaleClick
              end
            end
          end
          object GroupBox2: TGroupBox
            Left = 0
            Top = 0
            Width = 600
            Height = 35
            Align = alClient
            Caption = 'Date Input'
            TabOrder = 1
            object Label6: TLabel
              Left = 9
              Top = 14
              Width = 61
              Height = 14
              Caption = 'From Date:'
            end
            object Label7: TLabel
              Left = 249
              Top = 14
              Width = 12
              Height = 14
              Caption = 'to'
            end
            object dxDateFrom: TdxDateEdit
              Left = 77
              Top = 10
              Width = 164
              TabOrder = 0
              Date = -700000.000000000000000000
            end
            object dxDateTo: TdxDateEdit
              Left = 270
              Top = 10
              Width = 164
              TabOrder = 1
              Date = -700000.000000000000000000
            end
            object btnAccept: TButton
              Left = 439
              Top = 10
              Width = 62
              Height = 21
              Caption = 'Accept'
              TabOrder = 2
              OnClick = btnAcceptClick
            end
            object cBoxEnterDates: TCheckBox
              Left = 506
              Top = 14
              Width = 88
              Height = 17
              Caption = 'Enter Dates'
              TabOrder = 3
              OnClick = cBoxEnterDatesClick
            end
          end
        end
      end
      object pnlEmployees: TPanel
        Left = 1
        Top = 71
        Width = 676
        Height = 73
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        object dxList: TdxTreeList
          Left = 0
          Top = 0
          Width = 676
          Height = 73
          Bands = <
            item
              Caption = 'Employees'
            end>
          DefaultLayout = True
          HeaderPanelRowCount = 1
          Align = alClient
          TabOrder = 0
          Options = [aoColumnSizing, aoColumnMoving, aoEditing, aoTabThrough, aoRowSelect, aoAutoWidth, aoAutoSort]
          TreeLineColor = clGrayText
          ShowBands = True
          ShowGrid = True
          ShowRoot = False
          object dxListColumnEMPNR: TdxTreeListColumn
            Caption = 'EMPL NR'
            BandIndex = 0
            RowIndex = 0
          end
          object dxListColumnEMPNAME: TdxTreeListColumn
            Caption = 'EMPL NAME'
            BandIndex = 0
            RowIndex = 0
          end
          object dxListColumnJOBCODE: TdxTreeListColumn
            Caption = 'JOB CODE'
            BandIndex = 0
            RowIndex = 0
          end
          object dxListColumnJOBNAME: TdxTreeListColumn
            Caption = 'JOB NAME'
            BandIndex = 0
            RowIndex = 0
          end
          object dxListColumnSHIFTNR: TdxTreeListColumn
            Caption = 'SHIFT NR'
            BandIndex = 0
            RowIndex = 0
          end
          object dxListColumnDATETIME_IN: TdxTreeListColumn
            Caption = 'DATETIME IN'
            BandIndex = 0
            RowIndex = 0
          end
        end
      end
    end
    object pnlChart: TPanel
      Left = 0
      Top = 148
      Width = 678
      Height = 229
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object Chart1: TChart
        Left = 0
        Top = 0
        Width = 678
        Height = 196
        AllowPanning = pmHorizontal
        AllowZoom = False
        BackWall.Brush.Color = clWhite
        BackWall.Brush.Style = bsClear
        Title.Text.Strings = (
          'Production Output')
        OnClickSeries = Chart1ClickSeries
        Chart3DPercent = 10
        View3DOptions.Elevation = 356
        Align = alClient
        BevelWidth = 0
        TabOrder = 0
        object pnlInfo: TPanel
          Left = 536
          Top = 73
          Width = 144
          Height = 88
          BevelOuter = bvNone
          TabOrder = 0
          object lblEfficiencyDescription: TLabel
            Left = 10
            Top = 4
            Width = 109
            Height = 13
            Caption = 'lblEfficiencyDescription'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object lblEfficiency: TLabel
            Left = 10
            Top = 20
            Width = 56
            Height = 13
            Caption = 'lblEfficiency'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object lblTotalProdDescription: TLabel
            Left = 10
            Top = 48
            Width = 78
            Height = 13
            Caption = 'Total Production'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object lblTotalProd: TLabel
            Left = 10
            Top = 64
            Width = 56
            Height = 13
            Caption = 'lblTotalProd'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
        end
        object Panel5: TPanel
          Left = 0
          Top = 0
          Width = 19
          Height = 196
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 1
          object Panel6: TPanel
            Left = 0
            Top = 155
            Width = 19
            Height = 41
            Align = alBottom
            BevelOuter = bvNone
            TabOrder = 0
            object lblEmpCnt: TLabel
              Left = 1
              Top = 10
              Width = 22
              Height = 23
              Caption = 'EC'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Height = -19
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
            end
          end
        end
        object Series2: TAreaSeries
          Marks.ArrowLength = 8
          Marks.Visible = False
          SeriesColor = clWhite
          ShowInLegend = False
          Title = 'Background'
          AreaBrush = bsClear
          AreaLinesPen.Color = clGray
          DrawArea = True
          LinePen.Color = clWhite
          LinePen.Visible = False
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          Pointer.Visible = False
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1.000000000000000000
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Y'
          YValues.Multiplier = 1.000000000000000000
          YValues.Order = loNone
        end
        object Series4: TAreaSeries
          Marks.ArrowLength = 8
          Marks.Visible = False
          SeriesColor = 16777088
          Title = 'Nr of Empl. (x100)'
          DrawArea = True
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          Pointer.Visible = False
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1.000000000000000000
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Y'
          YValues.Multiplier = 1.000000000000000000
          YValues.Order = loNone
        end
        object Series3: TLineSeries
          Marks.ArrowLength = 8
          Marks.Visible = False
          SeriesColor = 65408
          Title = 'Norm Prod. Level'
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          Pointer.Visible = False
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1.000000000000000000
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Y'
          YValues.Multiplier = 1.000000000000000000
          YValues.Order = loNone
        end
        object Series1: TLineSeries
          Marks.ArrowLength = 8
          Marks.Visible = False
          SeriesColor = clRed
          Title = 'Output'
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          Pointer.Visible = False
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1.000000000000000000
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Y'
          YValues.Multiplier = 1.000000000000000000
          YValues.Order = loNone
        end
      end
      object Panel2: TPanel
        Left = 0
        Top = 196
        Width = 678
        Height = 33
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 1
        object Panel3: TPanel
          Left = 528
          Top = 0
          Width = 150
          Height = 33
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 0
          object btnCloseGraph: TButton
            Left = 32
            Top = 2
            Width = 114
            Height = 30
            Caption = 'Close Graph'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -17
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            OnClick = btnCloseGraphClick
          end
        end
        object Panel4: TPanel
          Left = 0
          Top = 0
          Width = 528
          Height = 33
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 1
          object sbtnHorScrollRight: TSpeedButton
            Left = 34
            Top = 4
            Width = 25
            Height = 25
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              0400000000000001000000000000000000001000000010000000000000000000
              800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
              3333333333333333333333333333333333333333333333333333333333333333
              3333333333333333333333333333333333333333333FF3333333333333003333
              3333333333773FF3333333333309003333333333337F773FF333333333099900
              33333FFFFF7F33773FF30000000999990033777777733333773F099999999999
              99007FFFFFFF33333F7700000009999900337777777F333F7733333333099900
              33333333337F3F77333333333309003333333333337F77333333333333003333
              3333333333773333333333333333333333333333333333333333333333333333
              3333333333333333333333333333333333333333333333333333}
            NumGlyphs = 2
            OnClick = sbtnHorScrollRightClick
          end
          object sbtnHorScrollLeft: TSpeedButton
            Left = 3
            Top = 4
            Width = 25
            Height = 25
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              0400000000000001000000000000000000001000000010000000000000000000
              800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
              3333333333333333333333333333333333333333333333333333333333333333
              3333333333333FF3333333333333003333333333333F77F33333333333009033
              333333333F7737F333333333009990333333333F773337FFFFFF330099999000
              00003F773333377777770099999999999990773FF33333FFFFF7330099999000
              000033773FF33777777733330099903333333333773FF7F33333333333009033
              33333333337737F3333333333333003333333333333377333333333333333333
              3333333333333333333333333333333333333333333333333333333333333333
              3333333333333333333333333333333333333333333333333333}
            NumGlyphs = 2
            OnClick = sbtnHorScrollLeftClick
          end
          object lblHelp: TLabel
            Left = 64
            Top = 8
            Width = 35
            Height = 14
            Caption = 'lblHelp'
          end
        end
      end
    end
  end
  inherited StandardMenuActionList: TActionList
    Left = 544
    Top = 128
    inherited FileSettingsAct: TAction
      OnExecute = FileSettingsActExecute
    end
    inherited FilePrintAct: TAction
      OnExecute = FilePrintActExecute
    end
    object FileExportAct: TAction
      Category = 'File'
      Caption = 'Export Chart'
      OnExecute = FileExportActExecute
    end
  end
  inherited mmPims: TMainMenu
    inherited miFile1: TMenuItem
      object Print1: TMenuItem [0]
        Action = FilePrintAct
      end
      object ExportChart1: TMenuItem [1]
        Action = FileExportAct
      end
    end
  end
  object TimerProductionChart: TTimer
    Enabled = False
    Interval = 60000
    OnTimer = TimerProductionChartTimer
    Left = 216
    Top = 8
  end
  object SaveDialog1: TSaveDialog
    DefaultExt = 'txt'
    Filter = 'Export-Textfile|*.txt'
    Options = [ofOverwritePrompt, ofHideReadOnly, ofEnableSizing]
    Left = 568
    Top = 312
  end
  object TimerStatus: TTimer
    Enabled = False
    OnTimer = TimerStatusTimer
    Left = 256
    Top = 7
  end
end
