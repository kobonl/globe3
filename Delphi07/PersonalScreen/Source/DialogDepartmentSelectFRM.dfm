inherited DialogDepartmentSelectF: TDialogDepartmentSelectF
  Width = 391
  Height = 254
  Caption = 'Select Department'
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlImageBase: TPanel
    Width = 375
    TabOrder = 1
    inherited imgBasePims: TImage
      Left = 79
    end
  end
  inherited stbarBase: TStatusBar
    Top = 113
    Width = 375
  end
  inherited pnlInsertBase: TPanel
    Width = 375
    Height = 76
    Font.Color = clBlack
    Font.Height = -11
    object Label3: TLabel
      Left = 16
      Top = 15
      Width = 24
      Height = 13
      Caption = 'Plant'
    end
    object Label1: TLabel
      Left = 16
      Top = 41
      Width = 57
      Height = 13
      Caption = 'Department'
    end
    object cmbPlusPlant: TdxPickEdit
      Left = 120
      Top = 12
      Width = 241
      TabOrder = 0
      StyleController = StyleController
      OnChange = cmbPlusPlantChange
    end
    object cmbPlusDepartment: TdxPickEdit
      Left = 120
      Top = 38
      Width = 241
      TabOrder = 1
      StyleController = StyleController
    end
  end
  inherited pnlBottom: TPanel
    Top = 132
    Width = 375
    inherited btnOk: TBitBtn
      Left = 88
      OnClick = btnOkClick
    end
    inherited btnCancel: TBitBtn
      Left = 202
    end
  end
end
