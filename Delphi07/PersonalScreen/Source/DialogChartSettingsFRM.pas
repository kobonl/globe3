unit DialogChartSettingsFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BaseDialogFRM, ActnList, StdCtrls, Buttons, ComCtrls,
  ExtCtrls, dxCntner, Menus, StdActns, ImgList, jpeg;

type
  TDialogChartSettingsF = class(TBaseDialogForm)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    cBoxSeparator: TComboBox;
    procedure FormShow(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
  private
    { Private declarations }
    FExportSeparator: String;
    procedure SetExportSeparator(const Value: String);
  public
    { Public declarations }
    property MyExportSeparator: String read FExportSeparator
      write SetExportSeparator;
  end;

var
  DialogChartSettingsF: TDialogChartSettingsF;

implementation

{ TDialogChartSettingsF }

{$R *.DFM}

procedure TDialogChartSettingsF.SetExportSeparator(const Value: String);
begin
  FExportSeparator := Value;
end;

procedure TDialogChartSettingsF.FormShow(Sender: TObject);
var
  I: Integer;
begin
  inherited;
  cBoxSeparator.ItemIndex := 0;
  for I:=0 to cBoxSeparator.Items.Count-1 do
    if cBoxSeparator.Items[I] = MyExportSeparator then
      cBoxSeparator.ItemIndex := I;
end;

procedure TDialogChartSettingsF.btnOkClick(Sender: TObject);
begin
  inherited;
  if cBoxSeparator.ItemIndex <> -1 then
    MyExportSeparator := cBoxSeparator.Items[cBoxSeparator.ItemIndex];
end;

end.
