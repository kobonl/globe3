inherited DialogSettingsF: TDialogSettingsF
  Left = 385
  Top = 155
  VertScrollBar.Range = 0
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Settings'
  ClientHeight = 493
  ClientWidth = 478
  Position = poMainFormCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlImageBase: TPanel
    Width = 478
    TabOrder = 1
    inherited imgBasePims: TImage
      Left = 361
    end
  end
  inherited stbarBase: TStatusBar
    Top = 411
    Width = 478
  end
  inherited pnlInsertBase: TPanel
    Width = 478
    Height = 374
    Font.Color = clBlack
    Font.Height = -11
    object pnlTop: TPanel
      Left = 0
      Top = 0
      Width = 478
      Height = 72
      Align = alTop
      Caption = 'pnlTop'
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 1
        Top = 1
        Width = 476
        Height = 72
        Align = alTop
        Caption = 'Global Settings'
        TabOrder = 0
        object GroupBox6: TGroupBox
          Left = 2
          Top = 15
          Width = 247
          Height = 55
          Align = alLeft
          Caption = 'Refresh Time Interval'
          TabOrder = 0
          object Label6: TLabel
            Left = 8
            Top = 22
            Width = 38
            Height = 13
            Caption = 'Interval'
          end
          object Label7: TLabel
            Left = 185
            Top = 22
            Width = 39
            Height = 13
            Caption = 'seconds'
          end
          object dxSpinEditRefreshTimeInterval: TdxSpinEdit
            Left = 104
            Top = 20
            Width = 73
            TabOrder = 0
            MaxValue = 500000.000000000000000000
            MinValue = 1.000000000000000000
            Value = 1.000000000000000000
            StoredValues = 48
          end
        end
        object GroupBox3: TGroupBox
          Left = 249
          Top = 15
          Width = 225
          Height = 55
          Align = alClient
          Caption = 'Efficiency Color Boundaries'
          TabOrder = 1
          object lblRed: TLabel
            Left = 8
            Top = 24
            Width = 19
            Height = 13
            Caption = 'Red'
          end
          object lblOrange: TLabel
            Left = 112
            Top = 24
            Width = 36
            Height = 13
            Caption = 'Orange'
          end
          object dxSpinEditRed: TdxSpinEdit
            Left = 56
            Top = 20
            Width = 49
            TabOrder = 0
            MaxValue = 100.000000000000000000
            MinValue = 30.000000000000000000
            Value = 85.000000000000000000
            StoredValues = 48
          end
          object dxSpinEditOrange: TdxSpinEdit
            Left = 168
            Top = 20
            Width = 49
            TabOrder = 1
            MaxValue = 130.000000000000000000
            MinValue = 30.000000000000000000
            Value = 105.000000000000000000
            StoredValues = 48
          end
        end
      end
    end
    object pnlClient: TPanel
      Left = 0
      Top = 72
      Width = 478
      Height = 302
      Align = alClient
      TabOrder = 1
      object GroupBox2: TGroupBox
        Left = 1
        Top = 1
        Width = 476
        Height = 127
        Align = alTop
        Caption = 'Scheme Settings'
        TabOrder = 0
        object GroupBox4: TGroupBox
          Left = 2
          Top = 15
          Width = 223
          Height = 51
          Align = alLeft
          Caption = 'Workspot Scale'
          TabOrder = 0
          object Label3: TLabel
            Left = 8
            Top = 20
            Width = 55
            Height = 13
            Caption = 'Percentage'
          end
          object dxSpinEditWorkspotScalePercentage: TdxSpinEdit
            Left = 104
            Top = 17
            Width = 73
            TabOrder = 0
            MaxValue = 250.000000000000000000
            MinValue = 1.000000000000000000
            Value = 50.000000000000000000
            StoredValues = 48
          end
        end
        object GroupBox5: TGroupBox
          Left = 2
          Top = 66
          Width = 472
          Height = 59
          Align = alBottom
          Caption = 'Efficiency Meters'
          TabOrder = 1
          object Label4: TLabel
            Left = 8
            Top = 24
            Width = 51
            Height = 13
            Caption = 'Use Period'
          end
          object rGrpUsePeriod: TRadioGroup
            Left = 99
            Top = 6
            Width = 206
            Height = 49
            Columns = 3
            Items.Strings = (
              'Current'
              'Since'
              'Shift')
            TabOrder = 0
            OnClick = rGrpUsePeriodClick
          end
          object grpBxSinceTime: TGroupBox
            Left = 600
            Top = 11
            Width = 165
            Height = 37
            Caption = 'Since'
            TabOrder = 1
            Visible = False
            object Label5: TLabel
              Left = 8
              Top = 14
              Width = 22
              Height = 13
              Caption = 'Time'
            end
            object dxTimeEditSinceTime: TdxTimeEdit
              Left = 79
              Top = 9
              Width = 81
              TabOrder = 0
              TimeEditFormat = tfHourMin
              StoredValues = 4
            end
          end
          object gBoxShowEmpInfo: TGroupBox
            Left = 306
            Top = 6
            Width = 163
            Height = 49
            Caption = 'Show'
            TabOrder = 2
            object cBoxShowEmpInfo: TCheckBox
              Left = 8
              Top = 12
              Width = 150
              Height = 17
              Caption = 'Employee Info'
              TabOrder = 0
              OnClick = cBoxShowEmpInfoClick
            end
            object cBoxShowEmpEff: TCheckBox
              Left = 8
              Top = 29
              Width = 153
              Height = 17
              Caption = 'Employee Efficiency'
              TabOrder = 1
            end
          end
        end
        object GroupBox7: TGroupBox
          Left = 225
          Top = 15
          Width = 249
          Height = 51
          Align = alClient
          Caption = 'Font Scale'
          TabOrder = 2
          object Label8: TLabel
            Left = 8
            Top = 20
            Width = 55
            Height = 13
            Caption = 'Percentage'
          end
          object dxSpinEditFontScalePercentage: TdxSpinEdit
            Left = 104
            Top = 17
            Width = 73
            TabOrder = 0
            MaxValue = 250.000000000000000000
            MinValue = 1.000000000000000000
            Value = 50.000000000000000000
            StoredValues = 48
          end
        end
      end
      object GroupBox8: TGroupBox
        Left = 1
        Top = 128
        Width = 476
        Height = 75
        Align = alClient
        Caption = 'Personal Screen Settings'
        TabOrder = 1
        object Label9: TLabel
          Left = 8
          Top = 21
          Width = 81
          Height = 13
          Caption = 'Datacol. Interval'
        end
        object Label10: TLabel
          Left = 184
          Top = 21
          Width = 29
          Height = 13
          Caption = 'msecs'
        end
        object Label11: TLabel
          Left = 224
          Top = 200
          Width = 65
          Height = 13
          Caption = 'Eff. based on'
          Visible = False
        end
        object Label12: TLabel
          Left = 8
          Top = 48
          Width = 92
          Height = 13
          Caption = 'Update DB Interval'
        end
        object Label13: TLabel
          Left = 184
          Top = 48
          Width = 37
          Height = 13
          Caption = 'minutes'
        end
        object Label14: TLabel
          Left = 240
          Top = 21
          Width = 55
          Height = 13
          Caption = 'Read Delay'
        end
        object Label15: TLabel
          Left = 415
          Top = 21
          Width = 29
          Height = 13
          Caption = 'msecs'
        end
        object Label16: TLabel
          Left = 240
          Top = 72
          Width = 92
          Height = 13
          Caption = 'Refresh Employees'
        end
        object Label17: TLabel
          Left = 240
          Top = 48
          Width = 66
          Height = 13
          Caption = 'Read Timeout'
        end
        object Label18: TLabel
          Left = 415
          Top = 48
          Width = 29
          Height = 13
          Caption = 'msecs'
        end
        object Label19: TLabel
          Left = 8
          Top = 72
          Width = 70
          Height = 13
          Caption = 'Current period'
        end
        object Label20: TLabel
          Left = 184
          Top = 72
          Width = 37
          Height = 13
          Caption = 'minutes'
        end
        object dxSpinEditDatacolInterval: TdxSpinEdit
          Left = 104
          Top = 18
          Width = 73
          TabOrder = 0
          MaxValue = 5000.000000000000000000
          MinValue = 1.000000000000000000
          Value = 1.000000000000000000
          StoredValues = 48
        end
        object rgrpEffQuantityTime: TRadioGroup
          Left = 296
          Top = 192
          Width = 201
          Height = 33
          Columns = 2
          ItemIndex = 0
          Items.Strings = (
            'Quantity'
            'Time')
          TabOrder = 1
          Visible = False
        end
        object dxSpinEditUpdateDBInterval: TdxSpinEdit
          Left = 104
          Top = 43
          Width = 73
          TabOrder = 2
          MaxValue = 5000.000000000000000000
          MinValue = 1.000000000000000000
          Value = 1.000000000000000000
          StoredValues = 48
        end
        object dxSpinEditReadDelay: TdxSpinEdit
          Left = 336
          Top = 18
          Width = 73
          TabOrder = 3
          MaxValue = 5000.000000000000000000
          MinValue = 1.000000000000000000
          Value = 1.000000000000000000
          StoredValues = 48
        end
        object cBoxRefreshEmployees: TCheckBox
          Left = 336
          Top = 72
          Width = 17
          Height = 17
          Caption = 'Refresh Employees'
          TabOrder = 4
        end
        object dxSpinEditReadTimeout: TdxSpinEdit
          Left = 336
          Top = 44
          Width = 73
          TabOrder = 5
          MaxValue = 50000.000000000000000000
          MinValue = 1000.000000000000000000
          Value = 1000.000000000000000000
          StoredValues = 48
        end
        object dxSpinEditCurrentPeriod: TdxSpinEdit
          Left = 104
          Top = 68
          Width = 73
          TabOrder = 6
          MaxValue = 60.000000000000000000
          MinValue = 5.000000000000000000
          Value = 5.000000000000000000
          StoredValues = 48
        end
      end
      object GroupBox9: TGroupBox
        Left = 1
        Top = 251
        Width = 476
        Height = 50
        Align = alBottom
        Caption = 'Machine Hours Settings'
        TabOrder = 2
        object Label21: TLabel
          Left = 8
          Top = 23
          Width = 66
          Height = 13
          Caption = 'Max. idle time'
        end
        object Label22: TLabel
          Left = 184
          Top = 24
          Width = 37
          Height = 13
          Caption = 'minutes'
        end
        object dxSpinEditMachHrsMaxIdleTime: TdxSpinEdit
          Left = 104
          Top = 20
          Width = 73
          TabOrder = 0
          MaxValue = 59.000000000000000000
          Value = 5.000000000000000000
          StoredValues = 48
        end
      end
      object GroupBox10: TGroupBox
        Left = 1
        Top = 203
        Width = 476
        Height = 48
        Align = alBottom
        Caption = 'Read data from external application'
        TabOrder = 3
        object Label23: TLabel
          Left = 8
          Top = 21
          Width = 107
          Height = 13
          Caption = 'Read every minute at '
        end
        object Label24: TLabel
          Left = 216
          Top = 24
          Width = 39
          Height = 13
          Caption = 'seconds'
        end
        object dxSpinEditReadExtDataSeconds: TdxSpinEdit
          Left = 136
          Top = 19
          Width = 73
          TabOrder = 0
          MaxValue = 59.000000000000000000
          Value = 5.000000000000000000
          StoredValues = 48
        end
      end
    end
  end
  inherited pnlBottom: TPanel
    Top = 430
    Width = 478
    Font.Height = -13
    inherited btnOK: TBitBtn
      Left = 136
      OnClick = btnOkClick
    end
    inherited btnCancel: TBitBtn
      Left = 256
    end
  end
  inherited imgList16x16: TImageList
    Left = 416
    Top = 8
  end
  inherited imgNoYes: TImageList
    Left = 448
    Top = 8
  end
  inherited StandardMenuActionList: TActionList
    Left = 240
    Top = 8
  end
  inherited mmPims: TMainMenu
    Left = 272
    Top = 8
  end
  object OpenDialog1: TOpenDialog
    DefaultExt = 'wav'
    Filter = 'Wav-files|*.wav'
    Options = [ofHideReadOnly, ofNoChangeDir, ofEnableSizing]
    Left = 200
    Top = 12
  end
  object TimerAutoClose: TTimer
    Enabled = False
    Interval = 10000
    OnTimer = TimerAutoCloseTimer
    Left = 139
    Top = 5
  end
end
