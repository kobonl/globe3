(*
  MRA:23-JAN-2014 TD-24046
  - Translate issues: Use a different method to translate.
    - Do NOT do this in separate files where the translation is done
      hard-coded!
    - New method:
      - Use 1 resource-strings-file that contains the translation-strings.
      - Use 1 translate-unit that assigns these strings to the properties
        in the forms.
      - In this way there is only 1 form that can be translated in multiple
        languages when the application is run.
      - The project-file defines the language using a condition.
*)
unit UTranslateStringsPS;

interface

resourceString

{$IFDEF PIMSDUTCH}
  // HomeFRM
  STransPimsPersonalScreen = 'PIMS - Personal Screen';
  STransHR = 'Uur';
  STransPCS = 'St.';
  STransNormPCS = 'norm st.';
  STransSaveExit = 'Opslaan+Einde';
  STransRefresh = 'Verversen';
  STransShowAllEmp = 'Toon alle werknemers';
  STransShowEmpWrongWorkspot = 'Toon werknemers op verkeerde werkplek';
  STransShowEmpNotScannedIn = 'Toon werknemers niet ingescand';
  STransShowEmpAbsentWithReason = 'Toon werknemers afwezig met reden';
  STransShowEmpAbsentWithoutReason = 'Toon werknemers afwezig zonder reden';
  STransShowEmpWithFirstAid = 'Toon werknemers met EHBO';
  STransShowEmpTooLate = 'Toon werknemers te laat';
  STransShowChart = 'Toon grafiek';
  STransShowEmployees = 'Toon werknemers';
  STransEfficiencyMeter = 'Effici�ntie Meter';
  STransLineHorz = 'Horizontale lijn';
  STransLineVert = 'Verticale lijn';
  STransRectangle = 'Rechthoek';
  STransEmployeeOverview = 'Werknemer Overzicht';
  STransDelete = 'Verwijder';
  STransSave = 'Opslaan';
  STransEdit = 'Wijzig schema';
  STransNew = 'Nieuw';
  STransOpen = 'Open';
  STransSaveAs = 'Opslaan Als';
  STransLegenda = 'Legenda';
  STransShowStatusInfo = 'Toon Status Info';
  STransSaveLog = 'Opslaan Log';
  // Settings
  STransSettings = 'Instellingen';
  STransGlobalSettings = 'Algemene instellingen';
  STransSoundAlarm =  'Geluidssignaal';
  STransSoundFilename = 'Bestandsnaam';
  STransUseSoundAlarm = 'Gebruik geluidssign.';
  STransBrowse = 'Bladeren';
  STransRefrehTimeInterval = 'Ververs tijd interval';
  STransInterval = 'Interval';
  STransSeconds = 'secondes';
  STransSchemeSettings = 'Scherm instellingen';
  STransWorkspotScale = 'Schaal werkplek';
  STransPercentage = 'Percentage';
  STransEffMeters = 'Effici�ntie meters';
  STransUsePeriod = 'Periode';
  STransCurrent = 'Huidig';
  STransSince = 'Vandaag';
  STransShift = 'Ploeg';
  STransFontScale = 'Schaal Font';
  STransPersonalScreenSettings = 'Personal Screen Instellingen';
  STransDatacolInterval = 'Datacol. Interval';
  STransMSecs = 'msecs';
  STransEffBasedOn = 'Eff. gebaseerd op';
  STransUpdateDBInterval = 'Update DB Interval';
  STransMinutes = 'minuten';
  STransReadDelay = 'Lees Delay';
  STransRefreshEmployees = 'Ververs werkn.';
  STransReadTimeout = 'Lees Timeout';
  STransCurrentPeriod = 'Huidige periode';
  STransScale = 'Schaal';
  STransEffColorBoundaries = 'Effici�ntie Kleur Grenzen';
  STransRed = 'Rood';
  STransOrange = 'Oranje';
  // Change Job
  STransChangeJob = 'Wijzig Job';
  STransChangeJob2 = 'WIJZIG JOB';
  STransDown = 'STORING';
  STransContinue = 'DOORGAAN';
  STransOK = '&OK';
  STransCancel = '&ANNUL.';
  STransEndOfDay = '&EINDE';
  STransBreak = 'PAUZE';
  STransLunch = 'LUNCH';
  // Login
  STransDatabaseLogin = 'Database Login';
  STransPassword = '&Wachtwoord';
  STransUsername = '&Gebruikersnaam';
  STransDatabasePims = 'Database:            Pims';
  // Dialog Selection
  STransDialogSelection = 'Dialog Selection';
  STransABSLBS = '   Gotli Labs';
  // DialogShowAllEmployees
  STransEmpsOnWorkspot = 'Alle werknemers';
  STransEmployee = 'Werknemer';
  STransScanned = 'Gescand';
  STransPlanned = 'Gepland';
  STransStandAvail = 'Stand. beschikbaar';
  STransAbsWithReason = 'Afwezig met reden';
  STransFirstAid = 'E.H.B.O.';
  STransNumber = 'Nummer';
  STransName = 'Naam';
  STransShort = 'Verkort';
  STransTeam = 'Team';
  STransWS = 'WP';
  STransTimeIn = 'Tijd-in';
  STransStartTime = 'Start Tijd';
  STransEndTime = 'Eind Tijd';
  STransLevel = 'Nivo';
  STransAbsReasonCode = 'Afwezigheidscode';
  STransAbsReasonDescr = 'Afwezigheidsreden';
  STransExpDate = 'Vervaldatum';
  STransEfficiency = 'Effici�ntie';
  STransToday = 'Vandaag';
  // BaseTopMenuFRM
  STransAboutABSGroup = 'Over Gotli Labs';
  STransABSGroupHomePage = 'Gotli Labs Home Pagina';
  STransOrderInfo = 'Bestel Informatie';
  STransPimsHomePage = 'Globe Home Pagina';
  STransContents = 'Inhoud';
  STransIndex = 'Index';
  STransExit1 = 'E&nde';
  STransPrint = 'Print';
  STransExit2 = 'Einde';
  STransFile = 'Bestand';
  STransHelp = 'Help';
  // DialogWorkspotSelect
  STransWorkspotSelect = 'Werkplek Selectie';
  STransPlant = 'Bedrijf';
  STransPicture = 'Afbeelding';
  STransMachine = 'Machine';
  STransTypeOfProdScreen = 'Type Person. Scherm';
  STransProdBarsWithoutTR = 'Productie zonder Tijdregistratie';
  STransProdBarsTRMachineLevel = 'Productie / Tijdregistratie gecombineerd (Machine niveau)';
  STransProdBarsTRWorskpotLevel = 'Productie / Tijdregistratie gecombineerd (Werkplek niveau)';
  // DialogSelectDownType
  STransSelectDownType = 'Kies Type Storing';
  STransMechDown = 'Mechanische Storing';
  STransNoMerch = 'Geen Artikelen';
  STransShow = 'Toon';
  STransEmpInfo = 'Werkn. Info';
  STransEmpEff = 'Werkn. Effici�ntie';

{$ELSE}
{$IFDEF PIMSFRENCH}
  // HomeFRM
  STransPimsPersonalScreen = 'PIMS - �cran personnel';
  STransHR = 'Heure';
  STransPCS = 'Pcs.';
  STransNormPCS = 'pcs. norme';
  STransSaveExit = 'Enregistrer+Fin';
  STransRefresh = 'Rafra��hir';
  STransShowAllEmp = 'Montre tous les employ�s';
  STransShowEmpWrongWorkspot = 'Montre les employ�s au lieu de travail incorrect';
  STransShowEmpNotScannedIn = 'Montre les employ�s pas scann�s';
  STransShowEmpAbsentWithReason = 'Montre les employ�s absents pour raison';
  STransShowEmpAbsentWithoutReason = 'Montre les employ�s absents sans raison';
  STransShowEmpWithFirstAid = 'Montre les employ�s avec BEPS';
  STransShowEmpTooLate = 'Montre les employ�s trop tard';
  STransShowChart = 'Montre graphique';
  STransShowEmployees = 'Montre les employ�s';
  STransEfficiencyMeter = 'M�tre de eff.';
  STransLineHorz = 'Ligne horizontale';
  STransLineVert = 'Ligne verticale';
  STransRectangle = 'Rectangle';
  STransEmployeeOverview = 'Aper�u des employ�s';
  STransDelete = 'Supprimer';
  STransSave = 'Enregistrer';
  STransEdit = 'Modifier le sch�me';
  STransNew = 'Nouveau';
  STransOpen = 'Ouvrir';
  STransSaveAs = 'Enregistrer sous';
  STransLegenda = 'L�gende';
  STransShowStatusInfo = 'Montre Status Info';
  STransSaveLog = 'Enregistrer Log';
  // Settings
  STransSettings = 'Param�tres';
  STransGlobalSettings = 'Param�tres g�n�raux';
  STransSoundAlarm =  'Signal sonore';
  STransSoundFilename = 'Nom de fichier';
  STransUseSoundAlarm = 'Mettre le signal sonore';
  STransBrowse = 'Feuilleter';
  STransRefrehTimeInterval = 'Rafra��hir intervalle de temps';
  STransInterval = 'Intervalle';
  STransSeconds = 'Secondes';
  STransSchemeSettings = '�cran des param�tres';
  STransWorkspotScale = 'Lieu de travail';
  STransPercentage = 'Percentage';
  STransEffMeters = 'M�tres de eff.';
  STransUsePeriod = 'P�riode';
  STransCurrent = 'Actuel';
  STransSince = 'Jour';
  STransShift = '�quipe';
  STransFontScale = 'La taille des caract�res de la police';
  STransPersonalScreenSettings = '�cran personnel param�tres';
  STransDatacolInterval = 'Datacoll. Intervalle';
  STransMSecs = 'msecs';
  STransEffBasedOn = 'Eff. fond�Esur';
  STransUpdateDBInterval = 'Update DB Interval';
  STransMinutes = 'minutes';
  STransReadDelay = 'Lis D�lai';
  STransRefreshEmployees = 'Rafra��hir employ�s';
  STransReadTimeout = 'Lire Timeout';
  STransCurrentPeriod = 'P�riode actuelle';
  STransScale = '�chelle';
  STransEffColorBoundaries = 'Des limites de couleur d''eff.';
  STransRed = 'Red';
  STransOrange = 'Orange';
  // Change Job
  STransChangeJob = 'Modifier Travail';
  STransChangeJob2 = 'MODIFIER TRAVAIL';
  STransDown = 'TROUBLE';
  STransContinue = 'CONTINUER';
  STransOK = '&OK';
  STransCancel = '&ANNUL.';
  STransEndOfDay = '&FIN';
  STransBreak = 'PAUSE';
  STransLunch = 'D�JEUNER';
  // Login
  STransDatabaseLogin = 'Database Login';
  STransPassword = '&Mot de passe';
  STransUsername = '&Nom d''utilisateur';
  STransDatabasePims = 'Database:            Pims';
  // Dialog Selection
  STransDialogSelection = 'S�lection Dialogue';
  STransABSLBS = '   Gotli Labs';
  // DialogShowAllEmployees
  STransEmpsOnWorkspot = 'Tous les employ�s sur lieu de travail';
  STransEmployee = 'Employ�';
  STransScanned = 'Scann�';
  STransPlanned = 'Projet�';
  STransStandAvail = 'Stand. disponible';
  STransAbsWithReason = 'Absent pour raison';
  STransFirstAid = 'B.E.P.S.';
  STransNumber = 'Num�ro';
  STransName = 'Nom';
  STransShort = 'Bref';
  STransTeam = 'Team';
  STransWS = 'LDT';
  STransTimeIn = 'Heure de d�but';
  STransStartTime = 'Heure de d�but';
  STransEndTime = 'Heure de fin';
  STransLevel = 'Niveau';
  STransAbsReasonCode = 'Code d''absence';
  STransAbsReasonDescr = 'Raison d''absence';
  STransExpDate = 'Date d''expiration';
  STransEfficiency = 'Efficacit�';
  STransToday = 'Aujourd''hui';
  // BaseTopMenuFRM
  STransAboutABSGroup = 'Sur Gotli Labs';
  STransABSGroupHomePage = 'Page d''accueil Gotli Labs';
  STransOrderInfo = 'Information sur l''ordre';
  STransPimsHomePage = 'Page d''accueil Globe';
  STransContents = 'Contenus';
  STransIndex = 'Index';
  STransExit1 = 'F&in';
  STransPrint = 'Imprime';
  STransExit2 = 'Fin';
  STransFile = 'Fiche';
  STransHelp = 'Aide';
  // DialogWorkspotSelect
  STransWorkspotSelect = 'S�lecte lieu de travail';
  STransPlant = 'Entreprise';
  STransPicture = 'Image';
  STransMachine = 'Machine';
  STransTypeOfProdScreen = 'Type �cran des produits';
  STransProdBarsWithoutTR = 'Production sans enregistrement de temps';
  STransProdBarsTRMachineLevel = 'Combination Production / enregistrement de temps (niveau de machine)';
  STransProdBarsTRWorskpotLevel = 'Combination Production / enregistrement de temps (niveau de lieu de travail)';
  // DialogSelectDownType
  STransSelectDownType = 'Choisis le temps';
  STransMechDown = 'Trouble m�canique';
  STransNoMerch = 'Pas d''articles';
  STransShow = 'Montrer';
  STransEmpInfo = 'Employ�Info';
  STransEmpEff = 'Employ�Efficacit�';

{$ELSE}
  {$IFDEF PIMSGERMAN}
  // HomeFRM
  STransPimsPersonalScreen = 'PIMS - Personal Schirm';
  STransHR = 'Std';
  STransPCS = 'Anz.';
  STransNormPCS = 'Norm Anz.';
  STransSaveExit = 'Speicher+Ende';
  STransRefresh = 'Refresh';
  STransShowAllEmp = 'Zeig alle Mitarbeiter';
  STransShowEmpWrongWorkspot = 'Zeig Mitarbeiter am falschen Arbeitzplatz';
  STransShowEmpNotScannedIn = 'Zeig nicht angemeldete Mitarbeite';
  STransShowEmpAbsentWithReason = 'Zeig Mitarbeiter abwesend mit Grund';
  STransShowEmpAbsentWithoutReason = 'Zeig Mitarbeiter abwesend ohne Grund';
  STransShowEmpWithFirstAid = 'Zeig Mitarbeiter mit Erste Hilfe';
  STransShowEmpTooLate = 'Zeig Mitabeiter zu Sp�t';
  STransShowChart = 'Zeig Graph';
  STransShowEmployees = 'Zeig Mitarbeiter';
  STransEfficiencyMeter = 'Effizienz Messer';
  STransLineHorz = 'Linie Horizontal';
  STransLineVert = 'Linie Vertikal';
  STransRectangle = 'Rechteck';
  STransEmployeeOverview = 'Mitarbeiter �bersicht';
  STransDelete = 'Entfern';
  STransSave = 'Speichern';
  STransEdit = '�ndern';
  STransNew = 'Neu';
  STransOpen = '�ffnen';
  STransSaveAs = 'Speichern Als';
  STransLegenda = 'Legenda';
  STransShowStatusInfo = 'Zeig Status Info';
  STransSaveLog = 'Speichern Log';
  // Settings
  STransSettings = 'Einstellungen';
  STransGlobalSettings = 'Globale Einstellungen';
  STransSoundAlarm =  'Signal';
  STransSoundFilename = 'Signal Dateiname';
  STransUseSoundAlarm = 'Verwenden Signal';
  STransBrowse = 'Browser';
  STransRefrehTimeInterval = 'Zeit Interval Refresh';
  STransInterval = 'Interval';
  STransSeconds = 'Sekundes';
  STransSchemeSettings = 'Schema Einstellungen';
  STransWorkspotScale = 'Arbeitsplatz Skala';
  STransPercentage = 'Prozentual';
  STransEffMeters = 'Effizienz Messer';
  STransUsePeriod = 'Periode';
  STransCurrent = 'Aktuell';
  STransSince = 'Heute';
  STransShift = 'Schicht';
  STransFontScale = 'Font Skala';
  STransPersonalScreenSettings = 'Personal Screen Settings';
  STransDatacolInterval = 'Datacol. Interval';
  STransMSecs = 'MSeks';
  STransEffBasedOn = 'Eff. basiert auf';
  STransUpdateDBInterval = 'Update DB Interval';
  STransMinutes = 'Minuten';
  STransReadDelay = 'Lese Delay';
  STransRefreshEmployees = 'Refresh Mitarb.';
  STransReadTimeout = 'Lese Timeout';
  STransCurrentPeriod = 'Heutige Periode';
  STransScale = 'Skala';
  STransEffColorBoundaries = 'Effizienz Farbgrenzen ';
  STransRed = 'Rot';
  STransOrange = 'Orange';
  // Change Job
  STransChangeJob = '�nderung Job';
  STransChangeJob2 = '�NDERUNG JOB';
  STransDown = 'DOWN';
  STransContinue = 'FORTSETZ';
  STransOK = '&OK';
  STransCancel = '&ANNUL.';
  STransEndOfDay = '&ENDE';
  STransBreak = 'BREAK';
  STransLunch = 'LUNCH';
  // Login
  STransDatabaseLogin = 'Database Login';
  STransPassword = '&Kennwort';
  STransUsername = '&Benutzername';
  STransDatabasePims = 'Database:            Pims';
  // Dialog Selection
  STransDialogSelection = 'Dialog Selection';
  STransABSLBS = '   Gotli Labs';
  // DialogShowAllEmployees
  STransEmpsOnWorkspot = 'Mitarbeiter auf Arbeitsplatz';
  STransEmployee = 'Mitarbeiter';
  STransScanned = 'Gescannt';
  STransPlanned = 'Geplannt';
  STransStandAvail = 'Stand. Verf�Ebar';
  STransAbsWithReason = 'Abwesend mit Grund';
  STransFirstAid = 'Erste Hilfe';
  STransNumber = 'Nummer';
  STransName = 'Name';
  STransShort = 'Kurz';
  STransTeam = 'Team';
  STransWS = 'AP';
  STransTimeIn = 'Zeit-In';
  STransStartTime = 'Start Zeit';
  STransEndTime = 'Ende Zeit';
  STransLevel = 'Level';
  STransAbsReasonCode = 'Kode Abwesenheitsgrund';
  STransAbsReasonDescr = 'Abwesenheitsgrund Beschreibung';
  STransExpDate = 'Verfalldatum';
  STransEfficiency = 'Effizienz';
  STransToday = 'Heute';
  // BaseTopMenuFRM
  STransAboutABSGroup = '�ber Gotli Labs';
  STransABSGroupHomePage = 'Gotli Labs Home Page';
  STransOrderInfo = 'Bestell Information';
  STransPimsHomePage = 'Globe Home Page';
  STransContents = 'Inhalt';
  STransIndex = 'Index';
  STransExit1 = 'E&nde';
  STransPrint = 'Drucken';
  STransExit2 = 'Ende';
  STransFile = 'Datei';
  STransHelp = 'Hilfe';
  // DialogWorkspotSelect
  STransWorkspotSelect = 'Arbeitsplatz Selektion';
  STransPlant = 'Betrieb';
  STransPicture = 'Picture';
  STransMachine = 'Machine';
  STransTypeOfProdScreen = 'Personal Schirm Type';
  STransProdBarsWithoutTR = 'Production bars without time recording';
  STransProdBarsTRMachineLevel = 'Production bars / Time recording combined (Machine level)';
  STransProdBarsTRWorskpotLevel = 'Production bars / Time recording combined (Workspot level)';
  // DialogSelectDownType
  STransSelectDownType = 'Wahl Steurungstype';
  STransMechDown = 'Mechan. Steurung';
  STransNoMerch = 'Keine Artikel';
  STransShow = 'Zeig';
  STransEmpInfo = 'Mitarb. Info';
  STransEmpEff = 'Mitarb. Eff.';

{$ELSE}
  {$IFDEF PIMSDANISH}
  // HomeFRM
  STransPimsPersonalScreen = 'PIMS - Personal Screen';
  STransHR = 'ti';
  STransPCS = 'stk.';
  STransNormPCS = 'norm stk.';
  STransSaveExit = 'Gem+forl.';
  STransRefresh = 'Refresh';
  STransShowAllEmp = 'Vis alle medarb.';
  STransShowEmpWrongWorkspot = 'Vis medarb. p�Eforkert arb.sted';
  STransShowEmpNotScannedIn = 'Vis ikke indskannede medarb.';
  STransShowEmpAbsentWithReason = 'Vis medarb. frav�r med �rsag';
  STransShowEmpAbsentWithoutReason = 'Vis medarb. frav�r uden �rsag';
  STransShowEmpWithFirstAid = 'Vis medarb. med f�rstehj�lp';
  STransShowEmpTooLate = 'Show Employees too late';
  STransShowChart = 'Vis graf';
  STransShowEmployees = 'Vis medarb.';
  STransEfficiencyMeter = 'Effektm�ler';
  STransLineHorz = 'Vandret linie';
  STransLineVert = 'Lodret linie';
  STransRectangle = 'Rektangel';
  STransEmployeeOverview = 'Medarb. oversigt';
  STransDelete = 'Slet';
  STransSave = 'Gem';
  STransEdit = '�ndr';
  STransNew = 'Ny';
  STransOpen = '�ben';
  STransSaveAs = 'Gem som';
  STransLegenda = 'Legenda';
  STransShowStatusInfo = 'Vis Status Info';
  STransSaveLog = 'Gem Log';
  // Settings
  STransSettings = 'Ops�tn.';
  STransGlobalSettings = 'Global ops�tn.';
  STransSoundAlarm =  'Alarmlyd';
  STransSoundFilename = 'Lyd filnavn';
  STransUseSoundAlarm = 'Brug alarmlyd';
  STransBrowse = 'Browse';
  STransRefrehTimeInterval = 'genopfrisk tidsinterv.';
  STransInterval = 'Interval';
  STransSeconds = 'sek.';
  STransSchemeSettings = 'Skema ops�tn.';
  STransWorkspotScale = 'Arbejd.sted skala';
  STransPercentage = 'Procent';
  STransEffMeters = 'Effektm�lere';
  STransUsePeriod = 'Brug per.';
  STransCurrent = 'Nuv.';
  STransSince = 'I dag';
  STransShift = 'Skift';
  STransFontScale = 'Font Skala';
  STransPersonalScreenSettings = 'Personal Sk�rm ops�tn.';
  STransDatacolInterval = 'Datahentn. interv.';
  STransMSecs = 'msek';
  STransEffBasedOn = 'Eff. bas. p.';
  STransUpdateDBInterval = 'Opdater DB interv.';
  STransMinutes = 'min.';
  STransReadDelay = 'L�s fors.';
  STransRefreshEmployees = 'Genopfr. medarb.';
  STransReadTimeout = 'L�sn. Timeout';
  STransCurrentPeriod = 'Nuv. periode';
  STransScale = 'Scale';
  STransEffColorBoundaries = 'Effektivitet farvegr�nser';
  STransRed = 'R�d';
  STransOrange = 'Orange';
  // Change Job
  STransChangeJob = '�ndre Job';
  STransChangeJob2 = '�NDRE JOB';
  STransDown = 'NEDE';
  STransContinue = 'FORTS.';
  STransOK = '&OK';
  STransCancel = '&AFBRYD';
  STransEndOfDay = '&SLUT';
  STransBreak = 'PAUSE';
  STransLunch = 'LUNCH';
  // Login
  STransDatabaseLogin = 'Database logind';
  STransPassword = '&Adgangskode';
  STransUsername = '&Brugernavn';
  STransDatabasePims = 'Database:            Pims';
  // Dialog Selection
  STransDialogSelection = 'Dialog Selection';
  STransABSLBS = '   Gotli Labs';
  // DialogShowAllEmployees
  STransEmpsOnWorkspot = 'Medarb. p�Earb.sted';
  STransEmployee = 'Medarb.';
  STransScanned = 'Skannet';
  STransPlanned = 'Planl.';
  STransStandAvail = 'Stand. ledig';
  STransAbsWithReason = 'Frav�r med �rsag';
  STransFirstAid = 'F�rstehj.';
  STransNumber = 'Nummer';
  STransName = 'Navn';
  STransShort = 'Kort';
  STransTeam = 'Team';
  STransWS = 'AS';
  STransTimeIn = 'Tid-ind';
  STransStartTime = 'Start Tid';
  STransEndTime = 'Slut tid';
  STransLevel = 'Niv.';
  STransAbsReasonCode = 'Frav�r �rsag kode';
  STransAbsReasonDescr = 'Frav�r �rsag beskr.';
  STransExpDate = 'Udl�bsdato';
  STransEfficiency = 'Effektivitet';
  STransToday = 'I dag';
  // BaseTopMenuFRM
  STransAboutABSGroup = 'Om Gotli Labs';
  STransABSGroupHomePage = 'Gotli Labs Home Page';
  STransOrderInfo = 'Bestill. Information';
  STransPimsHomePage = 'Globe Home Page';
  STransContents = 'Indhold';
  STransIndex = 'Index';
  STransExit1 = 'E&xit';
  STransPrint = 'Print';
  STransExit2 = 'Exit';
  STransFile = 'Fil';
  STransHelp = 'Help';
  // DialogWorkspotSelect
  STransWorkspotSelect = 'Arb.sted valg';
  STransPlant = 'Vask.';
  STransPicture = 'Billede';
  STransMachine = 'Maskine';
  STransTypeOfProdScreen = 'Type personlig sk�rm';
  STransProdBarsWithoutTR = 'Produktion barer uden tidsregistrering';
  STransProdBarsTRMachineLevel = 'Produktion barer/komb. tidsregistering (Maskine niv.)';
  STransProdBarsTRWorskpotLevel = 'Produktion barer/komb. tidsregistrering (Arb.sted niv.)';
  // DialogSelectDownType
  STransSelectDownType = 'V�lg lukning';
  STransMechDown = 'Mekanisk';
  STransNoMerch = 'Ingen varer';
  STransShow = 'Vis';
  STransEmpInfo = 'Medarb. Info';
  STransEmpEff = 'Medarb. Effektivitet';

{$ELSE}
  {$IFDEF PIMSJAPANESE}

  // HomeFRM
  STransPimsPersonalScreen = 'PIMS - �p�[�\�i���X�N���[��';
  STransHR = '����';
  STransPCS = '�_��';
  STransNormPCS = '�m���}�_��';
  STransSaveExit = '�ۑ�+����';
  STransRefresh = '�X�V';
  STransShowAllEmp = '�S�]�ƈ��\��';
  STransShowEmpWrongWorkspot = '��������[�N�X�|�b�g�̏]�ƈ��\��';
  STransShowEmpNotScannedIn = '�X�L�����C�����Ă��Ȃ��]�ƈ��\��';
  STransShowEmpAbsentWithReason = 'Show Employees absent with reason';
  STransShowEmpAbsentWithoutReason = 'Show Employees absent without reason';
  STransShowEmpWithFirstAid = 'Show Employees with First Aid';
  STransShowEmpTooLate = 'Show Employees too late';
  STransShowChart = 'Show Chart';
  STransShowEmployees = 'Show Employees';
  STransEfficiencyMeter = 'Efficiency Meter';
  STransLineHorz = 'Line Horizontal';
  STransLineVert = 'Line Vertical';
  STransRectangle = 'Rectangle';
  STransEmployeeOverview = 'Employee Overview';
  STransDelete = 'Delete';
  STransSave = 'Save';
  STransEdit = 'Edit';
  STransNew = 'New';
  STransOpen = 'Open';
  STransSaveAs = 'Save As';
  STransLegenda = 'Legenda';
  STransShowStatusInfo = 'Show Status Info';
  STransSaveLog = 'Save Log';
  // Settings
  STransSettings = 'Settings';
  STransGlobalSettings = 'Global Settings';
  STransSoundAlarm =  'Sound Alarm';
  STransSoundFilename = 'Sound Filename';
  STransUseSoundAlarm = 'Use Sound Alarm';
  STransBrowse = 'Browse';
  STransRefrehTimeInterval = 'Refresh Time Interval';
  STransInterval = 'Interval';
  STransSeconds = 'seconds';
  STransSchemeSettings = 'Scheme Settings';
  STransWorkspotScale = 'Workspot Scale';
  STransPercentage = 'Percentage';
  STransEffMeters = 'Efficiency Meters';
  STransUsePeriod = 'Use Period';
  STransCurrent = 'Current';
  STransSince = 'Today';
  STransShift = 'Shift';
  STransFontScale = 'Font Scale';
  STransPersonalScreenSettings = 'Personal Screen Settings';
  STransDatacolInterval = 'Datacol. Interval';
  STransMSecs = 'msecs';
  STransEffBasedOn = 'Eff. based on';
  STransUpdateDBInterval = 'Update DB Interval';
  STransMinutes = 'minutes';
  STransReadDelay = 'Read Delay';
  STransRefreshEmployees = 'Refresh Employees';
  STransReadTimeout = 'Read Timeout';
  STransCurrentPeriod = 'Current period';
  STransScale = 'Scale';
  STransEffColorBoundaries = 'Efficiency Color Boundaries';
  STransRed = 'Red';
  STransOrange = 'Orange';
  // Change Job
  STransChangeJob = 'Change Job';
  STransChangeJob2 = 'CHANGE JOB';
  STransDown = 'DOWN';
  STransContinue = 'CONTINUE';
  STransOK = '&OK';
  STransCancel = '&CANCEL';
  STransEndOfDay = '&END';
  STransBreak = 'BREAK';
  STransLunch = 'LUNCH';
  // Login
  STransDatabaseLogin = 'Database Login';
  STransPassword = '&Password';
  STransUsername = '&User name';
  STransDatabasePims = 'Database:            Pims';
  // Dialog Selection
  STransDialogSelection = 'Dialog Selection';
  STransABSLBS = '   Gotli Labs';
  // DialogShowAllEmployees
  STransEmpsOnWorkspot = 'Employees on Workspot';
  STransEmployee = 'Employee';
  STransScanned = 'Scanned';
  STransPlanned = 'Planned';
  STransStandAvail = 'Stand. Available';
  STransAbsWithReason = 'Absent with reason';
  STransFirstAid = 'First Aid';
  STransNumber = 'Number';
  STransName = 'Name';
  STransShort = 'Short';
  STransTeam = 'Team';
  STransWS = 'WS';
  STransTimeIn = 'Time-in';
  STransStartTime = 'Start Time';
  STransEndTime = 'End Time';
  STransLevel = 'Level';
  STransAbsReasonCode = 'Absence Reason Code';
  STransAbsReasonDescr = 'Absence Reason Descr';
  STransExpDate = 'Expiration date';
  STransEfficiency = 'Efficiency';
  STransToday = 'Today';
  // BaseTopMenuFRM
  STransAboutABSGroup = 'About Gotli Labs';
  STransABSGroupHomePage = 'Gotli Labs Home Page';
  STransOrderInfo = 'Ordering Information';
  STransPimsHomePage = 'Globe Home Page';
  STransContents = 'Contents';
  STransIndex = 'Index';
  STransExit1 = 'E&xit';
  STransPrint = 'Print';
  STransExit2 = 'Exit';
  STransFile = 'File';
  STransHelp = 'Help';
  // DialogWorkspotSelect
  STransWorkspotSelect = 'Workspot Select';
  STransPlant = 'Plant';
  STransPicture = 'Picture';
  STransMachine = 'Machine';
  STransTypeOfProdScreen = 'Type of Personal Screen';
  STransProdBarsWithoutTR = 'Production bars without time recording';
  STransProdBarsTRMachineLevel = 'Production bars / Time recording combined (Machine level)';
  STransProdBarsTRWorskpotLevel = 'Production bars / Time recording combined (Workspot level)';
  // DialogSelectDownType
  STransSelectDownType = 'Select Down Type';
  STransMechDown = 'Mechanical Down';
  STransNoMerch = 'No Merchandize';
  STransShow = 'Show';
  STransEmpInfo = 'Employee Info';
  STransEmpEff = 'Employee Efficiency';

{$ELSE}
  {$IFDEF PIMSCHINESE}
  
  // HomeFRM
  STransPimsPersonalScreen = 'PIMS - ������Ļ';
  STransHR = 'Сʱ';
  STransPCS = 'pcs.';
  STransNormPCS = '��׼ pcs.';
  STransSaveExit = '����+�˳�';
  STransRefresh = 'ˢ��';
  STransShowAllEmp = 'չʾ����Ա��';
  STransShowEmpWrongWorkspot = '���������Ա��';
  STransShowEmpNotScannedIn = 'Ϊɨ�赽��Ա��';
  STransShowEmpAbsentWithReason = '���ȱϯ��Ա��';
  STransShowEmpAbsentWithoutReason = '�޹�ȱϯ��Ա��;
  STransShowEmpWithFirstAid = '����Ա��';
  STransShowEmpTooLate = '̫��Ա��';
  STransShowChart = '��ʾͼ��';
  STransShowEmployees = '��ʾԱ��';
  STransEfficiencyMeter = 'Ч�ʱ�';
  STransLineHorz = 'ˮƽ��';
  STransLineVert = '��ֱ��';
  STransRectangle = '����';
  STransEmployeeOverview = 'Ա������';
  STransDelete = 'ɾ��';
  STransSave = '����';
  STransEdit = '�༭';
  STransNew = '�½�';
  STransOpen = '��';
  STransSaveAs = '�� ����';
  STransLegenda = 'ͼ��';
  STransShowStatusInfo = '��ʾ״̬��Ϣ';
  STransSaveLog = '�����¼';
  // Settings
  STransSettings = '����';
  STransGlobalSettings = '��������';
  STransSoundAlarm =  '��������';
  STransSoundFilename = '�����ļ���';
  STransUseSoundAlarm = 'ʹ����������';
  STransBrowse = '���';
  STransRefrehTimeInterval = 'ˢ��ʱ������';
  STransInterval = '���';
  STransSeconds = '����';
  STransSchemeSettings = '�ƻ�����';
  STransWorkspotScale = '�������ģ';
  STransPercentage = '�ְٱ�';
  STransEffMeters = 'Ч����';
  STransUsePeriod = 'ʹ������';
  STransCurrent = '��ǰ';
  STransSince = '����';
  STransShift = '�仯';
  STransFontScale = '�����ģ';
  STransPersonalScreenSettings = '������Ļ�趨';
  STransDatacolInterval = '������. ���';
  STransMSecs = 'msecs';
  STransEffBasedOn = 'Eff. ����';
  STransUpdateDBInterval = '���� DB ���';
  STransMinutes = '����';
  STransReadDelay = '�����Ķ�y';
  STransRefreshEmployees = 'ˢ��Ա��';
  STransReadTimeout = '��ȡ��ʱ';
  STransCurrentPeriod = '��ǰ�ڼ�';
  STransScale = '��ģ';
  STransEffColorBoundaries = 'Ч��ɫ�ʱ߽�';
  STransRed = '��';
  STransOrange = '��';
  // Change Job
  STransChangeJob = '��������';
  STransChangeJob2 = '��������';
  STransDown = '����';
  STransContinue = '����';
  STransOK = '&OK';
  STransCancel = '&ȡ��';
  STransEndOfDay = '&����';
  STransBreak = '��Ϣ';
  STransLunch = '���';
  // Login
  STransDatabaseLogin = '���ݿ��¼';
  STransPassword = '&����';
  STransUsername = '&�û���';
  STransDatabasePims = '���ݿ�:            Pims';
  // Dialog Selection
  STransDialogSelection = '�Ի�ѡ��';
  STransABSLBS = '   ABS ϴ�ӽ������';
  // DialogShowAllEmployees
  STransEmpsOnWorkspot = '������Ա��';
  STransEmployee = 'Ա��';
  STransScanned = '��ɨ��';
  STransPlanned = '���ƻ�';
  STransStandAvail = 'ֹͣ. ����';
  STransAbsWithReason = '���ȱϯ';
  STransFirstAid = '����';
  STransNumber = 'Ա����';
  STransName = '����';
  STransShort = 'ȱ��';
  STransTeam = '��';
  STransWS = 'WS';
  STransTimeIn = '���¼�ʱ';
  STransStartTime = '��ʱ��ʼ';
  STransEndTime = '��ʱ����';
  STransLevel = '�ȼ�';
  STransAbsReasonCode = 'ȱϯԭ����';
  STransAbsReasonDescr = 'ȱϯԭ��ע˵��';
  STransExpDate = '��ֹ��';
  STransEfficiency = 'Ч��';
  STransToday = '����';
  // BaseTopMenuFRM
  STransAboutABSGroup = '���� ABS-Group';
  STransABSGroupHomePage = 'ABS-Group ��ҳ';
  STransOrderInfo = '������Ϣ ';
  STransPimsHomePage = 'Pims ��ҳ';
  STransContents = 'Ŀ¼';
  STransIndex = '����';
  STransExit1 = '�˳�';
  STransPrint = '��ӡ';
  STransExit2 = '�˳�';
  STransFile = '�ļ�';
  STransHelp = '����';
  // DialogWorkspotSelect
  STransWorkspotSelect = '������ѡ��';
  STransPlant = '����';
  STransPicture = 'ͼƬ';
  STransMachine = '����';
  STransTypeOfProdScreen = '������Ļ����';
  STransProdBarsWithoutTR = '�޼�ʱ��Ʒ';
  STransProdBarsTRMachineLevel = '��Ʒ / ��ʱ (�豸�ȼ�)';
  STransProdBarsTRWorskpotLevel = '��Ʒ / ��ʱ (������ȼ�)';
  // DialogSelectDownType
  STransSelectDownType = 'ѡ���½�����';
  STransMechDown = '�½�����';
  STransNoMerch = '����Ʒ';
  STransShow = '��ʾ';
  STransEmpInfo = 'Ա����Ϣ';
  STransEmpEff = 'Ա��Ч��';

{$ELSE}
  {$IFDEF PIMSNORWEGIAN}
  
  // HomeFRM
  STransPimsPersonalScreen = 'PIMS - Personlig skjerm';
  STransHR = 'time';
  STransPCS = 'ant.';
  STransNormPCS = 'norm ant.';
  STransSaveExit = 'Lagre+Avslutt';
  STransRefresh = 'Oppdater';
  STransShowAllEmp = 'Vis alle Ansatte';
  STransShowEmpWrongWorkspot = 'Vis ansatte p� feil arbeidsposisjon';
  STransShowEmpNotScannedIn = 'Vis ansatte som ikke er innskannet';
  STransShowEmpAbsentWithReason = 'Vis ansatte med Frav�r, med grunn';
  STransShowEmpAbsentWithoutReason = 'Vis ansatte med frav�r, uten grunn';
  STransShowEmpWithFirstAid = 'Vis ansatte med Industrivern sertifikat';
  STransShowEmpTooLate = 'Vis ansatte med sent oppm�te';
  STransShowChart = 'Vis diagram';
  STransShowEmployees = 'Vis ansatte';
  STransEfficiencyMeter = 'Effektivitetsm�l';
  STransLineHorz = 'Linje Horisontalt';
  STransLineVert = 'Linje Vertikalt';
  STransRectangle = 'Rektangel';
  STransEmployeeOverview = 'Ansatte oversikt';
  STransDelete = 'Slett';
  STransSave = 'Lagre';
  STransEdit = 'Rediger';
  STransNew = 'Ny';
  STransOpen = '�pne';
  STransSaveAs = 'Lagre som';
  STransLegenda = 'Legenda';
  STransShowStatusInfo = 'Vis status info';
  STransSaveLog = 'Lagre Logg';
  // Settings
  STransSettings = 'Innstillinger';
  STransGlobalSettings = 'Globale Instillinger';
  STransSoundAlarm =  'Lyd Alarm';
  STransSoundFilename = 'Lyd Filenavn';
  STransUseSoundAlarm = 'Bruk Lyd Alarm';
  STransBrowse = 'Utforsk';
  STransRefrehTimeInterval = 'Oppdater tidsintervall';
  STransInterval = 'Intervall';
  STransSeconds = 'Sekunder';
  STransSchemeSettings = 'Skjema Innstillinger';
  STransWorkspotScale = 'Arbeids posisjon m�lestokk';
  STransPercentage = 'Prosent';
  STransEffMeters = 'Effektivitets m�l';
  STransUsePeriod = 'Bruk Periode';
  STransCurrent = 'Aktuell';
  STransSince = 'I Dag';
  STransShift = 'Skift';
  STransFontScale = 'Font St�rrelse';
  STransPersonalScreenSettings = 'Personlige Skjerm innstillinger';
  STransDatacolInterval = 'Datainnsammling. Intervall';
  STransMSecs = 'msek';
  STransEffBasedOn = 'Eff. basert p�';
  STransUpdateDBInterval = 'Oppdater DB Intervall';
  STransMinutes = 'minutter';
  STransReadDelay = 'Lese forsinkelse';
  STransRefreshEmployees = 'Oppdater Ansatt';
  STransReadTimeout = 'Tid for lesing overskredet';
  STransCurrentPeriod = 'Aktuell periode';
  STransScale = 'M�lestokk';
  STransEffColorBoundaries = 'Effektivitets farger, avgrensing';
  STransRed = 'R�d';
  STransOrange = 'Oransje';
  // Change Job
  STransChangeJob = 'Endre Jobb';
  STransChangeJob2 = 'ENDRE JOBB';
  STransDown = 'NEDE';
  STransContinue = 'FORTSETT';
  STransOK = '&OK';
  STransCancel = '&AVBRYT';
  STransEndOfDay = '&AVSLUTT';
  STransBreak = 'PAUSE';
  STransLunch = 'LUNSJ';
  // Login
  STransDatabaseLogin = 'Database Innlogging';
  STransPassword = '&Passord';
  STransUsername = '&Brukernavn';
  STransDatabasePims = 'Database:            Pims';
  // Dialog Selection
  STransDialogSelection = 'Dialog Valg';
  STransABSLBS = '   Gotli Labs';
  // DialogShowAllEmployees
  STransEmpsOnWorkspot = 'Ansatt p� Arbeidsposisjon';
  STransEmployee = 'Ansatt';
  STransScanned = 'Skannet';
  STransPlanned = 'Planlagt';
  STransStandAvail = 'Stand. Tilgjenglighet';
  STransAbsWithReason = 'Frav�r med grunn';
  STransFirstAid = 'Industrivern';
  STransNumber = 'Nummer';
  STransName = 'Navn';
  STransShort = 'Kort';
  STransTeam = 'Team';
  STransWS = 'WS';
  STransTimeIn = 'Tid-in';
  STransStartTime = 'Start Tid';
  STransEndTime = 'Slutt Tid';
  STransLevel = 'Niv�';
  STransAbsReasonCode = 'Frav�rs grunn kode';
  STransAbsReasonDescr = 'Frav�rs grunn beskrivelse';
  STransExpDate = 'Utl�psdato';
  STransEfficiency = 'Effektivitet';
  STransToday = 'I Dag';
  // BaseTopMenuFRM
  STransAboutABSGroup = 'OM Gotli Labs';
  STransABSGroupHomePage = 'Gotli Labs Hjemmeside';
  STransOrderInfo = 'Bestillilngs informasjon';
  STransPimsHomePage = 'Globe Hjemmeside';
  STransContents = 'Innhold';
  STransIndex = 'Hovedside';
  STransExit1 = 'Avslutt';
  STransPrint = 'Skriv ut';
  STransExit2 = 'Avslutt';
  STransFile = 'Fil';
  STransHelp = 'Hjelp';
  // DialogWorkspotSelect
  STransWorkspotSelect = 'Velg Arbeidsposisjon';
  STransPlant = 'Anlegg';
  STransPicture = 'Bilde';
  STransMachine = 'Maskin';
  STransTypeOfProdScreen = 'Type, Personlig Skjerm';
  STransProdBarsWithoutTR = 'Produksjons s�yler uten tidsregistrering';
  STransProdBarsTRMachineLevel = 'Produksjons s�yler / Tidsregistrering kombinert (Maskin niv�)';
  STransProdBarsTRWorskpotLevel = 'Produksjons s�yler / Tidsregistrering kombinert (Arbeidsposisjon niv�)';
  // DialogSelectDownType
  STransSelectDownType = 'Velg Nede �rsak';
  STransMechDown = 'Mekanisk brudd';
  STransNoMerch = 'Ingen produkter';
  STransShow = 'Vis';
  STransEmpInfo = 'Ansatt info';
  STransEmpEff = 'Ansatt effektivitet';

{$ELSE}

// ENGLISH (default language)
  // HomeFRM
  STransPimsPersonalScreen = 'PIMS - Personal Screen';
  STransHR = 'hr';
  STransPCS = 'pcs.';
  STransNormPCS = 'norm pcs.';
  STransSaveExit = 'Save+Exit';
  STransRefresh = 'Refresh';
  STransShowAllEmp = 'Show All Employees';
  STransShowEmpWrongWorkspot = 'Show Employees on wrong workspot';
  STransShowEmpNotScannedIn = 'Show Employees not scanned in';
  STransShowEmpAbsentWithReason = 'Show Employees absent with reason';
  STransShowEmpAbsentWithoutReason = 'Show Employees absent without reason';
  STransShowEmpWithFirstAid = 'Show Employees with First Aid';
  STransShowEmpTooLate = 'Show Employees too late';
  STransShowChart = 'Show Chart';
  STransShowEmployees = 'Show Employees';
  STransEfficiencyMeter = 'Efficiency Meter';
  STransLineHorz = 'Line Horizontal';
  STransLineVert = 'Line Vertical';
  STransRectangle = 'Rectangle';
  STransEmployeeOverview = 'Employee Overview';
  STransDelete = 'Delete';
  STransSave = 'Save';
  STransEdit = 'Edit';
  STransNew = 'New';
  STransOpen = 'Open';
  STransSaveAs = 'Save As';
  STransLegenda = 'Legenda';
  STransShowStatusInfo = 'Show Status Info';
  STransSaveLog = 'Save Log';
  // Settings
  STransSettings = 'Settings';
  STransGlobalSettings = 'Global Settings';
  STransSoundAlarm =  'Sound Alarm';
  STransSoundFilename = 'Sound Filename';
  STransUseSoundAlarm = 'Use Sound Alarm';
  STransBrowse = 'Browse';
  STransRefrehTimeInterval = 'Refresh Time Interval';
  STransInterval = 'Interval';
  STransSeconds = 'seconds';
  STransSchemeSettings = 'Scheme Settings';
  STransWorkspotScale = 'Workspot Scale';
  STransPercentage = 'Percentage';
  STransEffMeters = 'Efficiency Meters';
  STransUsePeriod = 'Use Period';
  STransCurrent = 'Current';
  STransSince = 'Today';
  STransShift = 'Shift';
  STransFontScale = 'Font Scale';
  STransPersonalScreenSettings = 'Personal Screen Settings';
  STransDatacolInterval = 'Datacol. Interval';
  STransMSecs = 'msecs';
  STransEffBasedOn = 'Eff. based on';
  STransUpdateDBInterval = 'Update DB Interval';
  STransMinutes = 'minutes';
  STransReadDelay = 'Read Delay';
  STransRefreshEmployees = 'Refresh Employees';
  STransReadTimeout = 'Read Timeout';
  STransCurrentPeriod = 'Current period';
  STransScale = 'Scale';
  STransEffColorBoundaries = 'Efficiency Color Boundaries';
  STransRed = 'Red';
  STransOrange = 'Orange';
  // Change Job
  STransChangeJob = 'Change Job';
  STransChangeJob2 = 'CHANGE JOB';
  STransDown = 'DOWN';
  STransContinue = 'CONTINUE';
  STransOK = '&OK';
  STransCancel = '&CANCEL';
  STransEndOfDay = '&END';
  STransBreak = 'BREAK';
  STransLunch = 'LUNCH';
  // Login
  STransDatabaseLogin = 'Database Login';
  STransPassword = '&Password';
  STransUsername = '&User name';
  STransDatabasePims = 'Database:            Pims';
  // Dialog Selection
  STransDialogSelection = 'Dialog Selection';
  STransABSLBS = '   Gotli Labs';
  // DialogShowAllEmployees
  STransEmpsOnWorkspot = 'Employees on Workspot';
  STransEmployee = 'Employee';
  STransScanned = 'Scanned';
  STransPlanned = 'Planned';
  STransStandAvail = 'Stand. Available';
  STransAbsWithReason = 'Absent with reason';
  STransFirstAid = 'First Aid';
  STransNumber = 'Number';
  STransName = 'Name';
  STransShort = 'Short';
  STransTeam = 'Team';
  STransWS = 'WS';
  STransTimeIn = 'Time-in';
  STransStartTime = 'Start Time';
  STransEndTime = 'End Time';
  STransLevel = 'Level';
  STransAbsReasonCode = 'Absence Reason Code';
  STransAbsReasonDescr = 'Absence Reason Descr';
  STransExpDate = 'Expiration date';
  STransEfficiency = 'Efficiency';
  STransToday = 'Today';
  // BaseTopMenuFRM
  STransAboutABSGroup = 'About Gotli Labs';
  STransABSGroupHomePage = 'Gotli Labs Home Page';
  STransOrderInfo = 'Ordering Information';
  STransPimsHomePage = 'Globe Home Page';
  STransContents = 'Contents';
  STransIndex = 'Index';
  STransExit1 = 'E&xit';
  STransPrint = 'Print';
  STransExit2 = 'Exit';
  STransFile = 'File';
  STransHelp = 'Help';
  // DialogWorkspotSelect
  STransWorkspotSelect = 'Workspot Select';
  STransPlant = 'Plant';
  STransPicture = 'Picture';
  STransMachine = 'Machine';
  STransTypeOfProdScreen = 'Type of Personal Screen';
  STransProdBarsWithoutTR = 'Production bars without time recording';
  STransProdBarsTRMachineLevel = 'Production bars / Time recording combined (Machine level)';
  STransProdBarsTRWorskpotLevel = 'Production bars / Time recording combined (Workspot level)';
  // DialogSelectDownType
  STransSelectDownType = 'Select Down Type';
  STransMechDown = 'Mechanical Down';
  STransNoMerch = 'No Merchandize';
  STransShow = 'Show';
  STransEmpInfo = 'Employee Info';
  STransEmpEff = 'Employee Efficiency';

            {$ENDIF}
          {$ENDIF}
        {$ENDIF}
      {$ENDIF}
    {$ENDIF}
  {$ENDIF}
{$ENDIF}


implementation

end.
