inherited DialogWorkspotSelectF: TDialogWorkspotSelectF
  Left = 466
  Top = 280
  VertScrollBar.Range = 0
  BorderStyle = bsDialog
  Caption = 'Workspot Select'
  ClientHeight = 289
  ClientWidth = 496
  Position = poOwnerFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlImageBase: TPanel
    Width = 496
    TabOrder = 1
    inherited imgBasePims: TImage
      Left = 379
    end
  end
  inherited stbarBase: TStatusBar
    Top = 207
    Width = 496
  end
  inherited pnlInsertBase: TPanel
    Top = 142
    Width = 496
    Height = 65
    Font.Color = clBlack
    Font.Height = -13
    object Label3: TLabel
      Left = 16
      Top = 11
      Width = 28
      Height = 16
      Caption = 'Plant'
    end
    object Label1: TLabel
      Left = 16
      Top = 37
      Width = 54
      Height = 16
      Caption = 'Workspot'
    end
    object Label2: TLabel
      Left = 16
      Top = 67
      Width = 39
      Height = 16
      Caption = 'Picture'
      Visible = False
    end
    object LabelMachine: TLabel
      Left = 16
      Top = 37
      Width = 47
      Height = 16
      Caption = 'Machine'
    end
    object cmbPlusPlant: TdxPickEdit
      Left = 120
      Top = 8
      Width = 241
      TabOrder = 0
      StyleController = StyleController
      OnChange = cmbPlusPlantChange
    end
    object cmbPlusWorkspot: TdxPickEdit
      Left = 120
      Top = 34
      Width = 241
      TabOrder = 1
      StyleController = StyleController
    end
    object dxPicList: TdxTreeList
      Left = 120
      Top = 67
      Width = 241
      Height = 179
      Bands = <
        item
        end>
      DefaultLayout = True
      HeaderPanelRowCount = 1
      TabOrder = 2
      Visible = False
      Images = imgList
      Options = [aoColumnSizing, aoColumnMoving, aoEditing, aoTabThrough, aoRowSelect, aoAutoWidth]
      TreeLineColor = clGrayText
      ShowLines = False
      object dxTreeList1Column1: TdxTreeListColumn
        Caption = 'Picture'
        MinWidth = 32
        BandIndex = 0
        RowIndex = 0
      end
    end
    object ListBox1: TListBox
      Left = 368
      Top = 116
      Width = 121
      Height = 97
      ItemHeight = 16
      Sorted = True
      TabOrder = 3
      Visible = False
    end
    object cmbPlusMachine: TdxPickEdit
      Left = 120
      Top = 34
      Width = 241
      TabOrder = 4
      StyleController = StyleController
    end
  end
  inherited pnlBottom: TPanel
    Top = 226
    Width = 496
    inherited btnOK: TBitBtn
      Left = 136
      OnClick = btnOkClick
    end
    inherited btnCancel: TBitBtn
      Left = 250
    end
  end
  object rgProdScreenType: TRadioGroup [4]
    Left = 0
    Top = 37
    Width = 496
    Height = 105
    Align = alTop
    Caption = 'Type of Production Screen'
    ItemIndex = 1
    Items.Strings = (
      'Production bars / Time recording combined (Machine level)'
      'Production bars / Time recording combined (Workspot level)')
    TabOrder = 4
    OnClick = rgProdScreenTypeClick
  end
  object imgList: TImageList
    Height = 32
    Masked = False
    Width = 32
    Left = 408
    Top = 132
  end
end
