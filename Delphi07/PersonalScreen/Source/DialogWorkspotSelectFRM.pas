(*
  MRA:19-MAY-2010. RV063.4. Order 550478.
  - Addition of Machines.
  - Addition of Time Recording.
  MRA:16-MAR-2012 20012858. New ProductionScreen combined with datacol.
  - Based on ProductionScreen a new project is made named: PersonalScreen.
  - All that is not needed is left out here or disabled.
  MRA:28-MAY-2013 20014289 New look Pims
  - Some pictures/colors are changed.
  MRA:15-NOV-2016 PIM-213
  - Related to this order.
  - Make picture-selection optional.
*)
unit DialogWorkspotSelectFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BaseDialogFRM, ActnList, StdCtrls, Buttons, ComCtrls, Variants,
  ExtCtrls, ImgList, dxTL, dxCntner, dxEdLib, dxExEdtr, dxEditor, Menus,
  StdActns, ORASystemDMT, ProductionScreenDMT, PersonalScreenDMT,
  {UProductionScreen} UPersonalScreen, UProductionScreenDefs, jpeg;

type
  TDialogWorkspotSelectF = class(TBaseDialogForm)
    Label3: TLabel;
    cmbPlusPlant: TdxPickEdit;
    Label1: TLabel;
    cmbPlusWorkspot: TdxPickEdit;
    Label2: TLabel;
    dxPicList: TdxTreeList;
    dxTreeList1Column1: TdxTreeListColumn;
    imgList: TImageList;
    ListBox1: TListBox;
    LabelMachine: TLabel;
    cmbPlusMachine: TdxPickEdit;
    rgProdScreenType: TRadioGroup;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure cmbPlusPlantChange(Sender: TObject);
    procedure rgProdScreenTypeClick(Sender: TObject);
  private
    { Private declarations }
    FWorkspotCode: String;
    FImageName: String;
    FRootPath: String;
    FPlantCode: String;
    FWorkspotDescription: String;
    InitForm: Boolean;
    FProdScreenType: TProdScreenType;
    FMachineDescription: String;
    FMachineCode: String;
    FActionWorkspot: Boolean;
    PanelInsertBaseHeight: Integer;
    FNoPicture: Boolean;
    procedure AddFileNames;
    procedure AddImagesToImageList;
    procedure AddImages;
    procedure SetRootPath(const Value: String);
  public
    { Public declarations }
    property RootPath: String read FRootPath write SetRootPath;
    property PlantCode: String read FPlantCode;
    property WorkspotCode: String read FWorkspotCode;
    property WorkspotDescription: String read FWorkspotDescription;
    property ImageName: String read FImageName;
    property ProdScreenType: TProdScreenType read FProdScreenType
      write FProdScreenType;
    property MachineCode: String read FMachineCode;
    property MachineDescription: String read FMachineDescription;
    property ActionWorkspot: Boolean read FActionWorkspot write FActionWorkspot;
    property NoPicture: Boolean read FNoPicture write FNoPicture;
  end;

var
  DialogWorkspotSelectF: TDialogWorkspotSelectF;

implementation

{$R *.DFM}

//uses
//  ORASystemDMT,
//  ProductionScreenDMT;

procedure TDialogWorkspotSelectF.FormCreate(Sender: TObject);
begin
  inherited;
  lblMessage.Caption := ' ' + Caption; // 20014289
//  cmbPlusPlant.ShowSpeedButton := False;
//  cmbPlusPlant.ShowSpeedButton := True;
//  cmbPlusWorkspot.ShowSpeedButton := False;
//  cmbPlusWorkspot.ShowSpeedButton := True;
  ProductionScreenDM.FillComboBox(
    ProductionScreenDM.odsPlant, cmbPlusPlant, '', True,
      '', 'PLANT_CODE', 'DESCRIPTION');
  ProductionScreenDM.FillComboBox(
    ProductionScreenDM.odsMachine, cmbPlusMachine,
      GetStrValue(cmbPlusPlant.Text), True,
      'PLANT_CODE', 'MACHINE_CODE', 'DESCRIPTION');
  ProductionScreenDM.FillComboBox(
    ProductionScreenDM.odsWorkspot, cmbPlusWorkspot,
      GetStrValue(cmbPlusPlant.Text), True,
      'PLANT_CODE', 'WORKSPOT_CODE', 'DESCRIPTION');
  InitForm := False;
  PanelInsertBaseHeight := pnlInsertBase.Height + pnlImageBase.Height + 50;
  ActionWorkspot := True;
end;

procedure TDialogWorkspotSelectF.AddFileNames;
var
  SearchRec: TSearchRec;
  SearchPath: String;
  Done: Integer;
begin
  ListBox1.Items.Clear;
  SearchPath := RootPath + '*.bmp';
  Done := FindFirst(SearchPath, faAnyFile, SearchRec);
  while Done = 0 do
  begin
    try
      ListBox1.Items.Add(SearchRec.Name);
    finally
      Done := FindNext(SearchRec);
    end;
  end;
  SysUtils.FindClose(SearchRec);
end;

procedure TDialogWorkspotSelectF.AddImagesToImageList;
var
  AImage: TImage;
  Index: Integer;
begin
  imgList.Clear;
  for Index := 0 to ListBox1.Items.Count - 1 do
  begin
    AImage := TImage.Create(Application);
    AImage.Picture.LoadFromFile(RootPath + ListBox1.Items[Index]);
    AImage.Stretch := True;
//      AImage.Width := imgList.Width;
//      AImage.Height := imgList.Height;
//      if AImage.Picture.Bitmap.Width < imgList.Width then
//        AImage.Picture.Bitmap.Width := imgList.Width;
//      if AImage.Picture.Bitmap.Height < imgList.Width then
//        AImage.Picture.Bitmap.Height := imgList.Width;
    AImage.Picture.Bitmap.Height := imgList.Height;
    AImage.Picture.Bitmap.Width := imgList.Width;
    imgList.Add(AImage.Picture.Bitmap, nil);
  end;
end;

// Add items to a TdxTreeList-component.
// Because the 'image-list doesn't contain the names of the
// bitmaps, we have to get them again.
procedure TDialogWorkspotSelectF.AddImages;
var
  Item: TdxTreeListNode;
  Index: Integer;
begin
  dxPicList.ClearNodes;
  for Index := 0 to ListBox1.Items.Count - 1 do
  begin
    Item := dxPicList.Add;
    Item.Values[0] := ListBox1.Items[Index];
    Item.ImageIndex := Index;
    Item.SelectedIndex := Index;
  end;
end;

procedure TDialogWorkspotSelectF.FormShow(Sender: TObject);
begin
  inherited;
  // MR:20-04-2004 This should only be done once!
  // This to prevent a memory-leak during
  // creation of images (TImage).
  if not InitForm then
  begin
    AddFileNames;
    AddImagesToImageList;
    AddImages;
    InitForm := True;
  end;
  rgProdScreenType.ItemIndex := 1; // PIM-213
  if ActionWorkspot then
  begin
    rgProdScreenType.Visible := False;
    LabelMachine.Visible := False;
    cmbPlusMachine.Visible := False;
  end
  else
  begin
    rgProdScreenType.Visible := True;
    rgProdScreenTypeClick(Sender);
    Height :=  pnlBottom.Height + PanelInsertBaseHeight +
      rgProdScreenType.Height;
  end;
  // PIM-213
  if NoPicture then
  begin
    Label2.Enabled := False;
    dxPicList.Visible := False;
  end
  else
  begin
    Label2.Enabled := True;
    dxPicList.Visible := True;
  end;
end;

procedure TDialogWorkspotSelectF.btnOkClick(Sender: TObject);
var
  Node: TdxTreeListNode;
begin
  inherited;
  FPlantCode := GetStrValue(cmbPlusPlant.Text);
  
  case ProdScreenType of
  pstNoTimeRec, pstWorkspotTimeRec:
    begin
      FWorkspotCode := GetStrValue(cmbPlusWorkspot.Text);
      // Now get description of workspot
      ProductionScreenDM.odsWorkspot.Active := True;
      if ProductionScreenDM.odsWorkspot.Locate('PLANT_CODE;WORKSPOT_CODE',
        VarArrayOf([FPlantCode, FWorkspotCode]), []) then
        FWorkspotDescription :=
          ProductionScreenDM.odsWorkspot.FieldByName('DESCRIPTION').Value;
      ProductionScreenDM.odsWorkspot.Active := False;
      FMachineCode := '';
      FMachineDescription := '';
    end;
  pstMachineTimeRec:
    begin
      FMachineCode := GetStrValue(cmbPlusMachine.Text);
      // Now get description of machine
      ProductionScreenDM.odsMachine.Active := True;
      if ProductionScreenDM.odsMachine.Locate('PLANT_CODE;MACHINE_CODE',
        VarArrayOf([FPlantCode, FMachineCode]), []) then
          FMachineDescription :=
            ProductionScreenDM.odsMachine.FieldByName('SHORT_NAME').Value;
      ProductionScreenDM.odsMachine.Active := False;
    end;
  end;

  Node := dxPicList.FocusedNode;
  FImageName := Node.Values[0];
end;

procedure TDialogWorkspotSelectF.SetRootPath(const Value: String);
begin
  FRootPath := Value;
end;

procedure TDialogWorkspotSelectF.cmbPlusPlantChange(Sender: TObject);
begin
  inherited;
  ProductionScreenDM.FillComboBox(
    ProductionScreenDM.odsMachine, cmbPlusMachine,
      GetStrValue(cmbPlusPlant.Text), True,
      'PLANT_CODE', 'MACHINE_CODE', 'DESCRIPTION');
  ProductionScreenDM.FillComboBox(
    ProductionScreenDM.odsWorkspot, cmbPlusWorkspot,
      GetStrValue(cmbPlusPlant.Text), True,
      'PLANT_CODE', 'WORKSPOT_CODE', 'DESCRIPTION');
end;

// RV063.4.
procedure TDialogWorkspotSelectF.rgProdScreenTypeClick(Sender: TObject);
var
  Choice: TProdScreenType;
begin
  inherited;
  Choice := TProdScreenType(rgProdScreenType.ItemIndex);
{$IFDEF PERSONALSCREEN}
  case rgProdScreenType.ItemIndex of
  0: Choice := pstMachineTimeRec;
  1: Choice := pstWorkspotTimeRec;
  end;
{$ENDIF}
  case Choice of
  pstNoTimeRec: // Production bars without time recording
    begin
      Label1.Visible := True;
      cmbPlusWorkspot.Visible := True;
      LabelMachine.Visible := False;
      cmbPlusMachine.Visible := False;
      ProdScreenType := pstNoTimeRec;
    end;
  pstMachineTimeRec: // Production bars / Time recording combined (Machine level)
    begin
      Label1.Visible := False;
      cmbPlusWorkspot.Visible := False;
      LabelMachine.Visible := True;
      cmbPlusMachine.Visible := True;
      ProdScreenType := pstMachineTimeRec;
    end;
  pstWorkspotTimeRec: // Production bars / Time recording combined (Workspot level)
    begin
      Label1.Visible := True;
      cmbPlusWorkspot.Visible := True;
      LabelMachine.Visible := False;
      cmbPlusMachine.Visible := False;
      ProdScreenType := pstWorkspotTimeRec;
    end;
  end;
end;

end.
