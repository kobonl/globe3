(*
  MRA:19-JUN-2012 Bugfix.
  - The shown time for employee is not accurate anymore after some time.
    Cause: Probably because the 'elapsed time' is rounded to a value.
    Instead of using integers for the stored time, use doubles.
  MRA:22-JUN-2012 20013379
  - Compare Jobs Handling.
  MRA:27-JUN-2012
  - Job Button Caption handling: When text is too long, make font smaller!
  MRA:28-JUN-2012 20013379
  - Compare Jobs Handling: Do not create employee/scan for this!
  MRA:3-JUL-2012 20012858.80
  - Addition of routines to test blackboxes.
  MRA:7-JUL-2012 20012858.90
  - It added the current qty per workspot for NOJOB, which must not be done.
  MRA:18-JUL-2012 20012858.160.
  - Added changes for 'Refresh Employees'.
  MRA:28-AUG-2012 TODO 21191
  - IPExists does not work without admin-rights. It is disabled here.
  MRA:10-SEP-2012 TODO-21191
  - Use check-digits for reading blackboxes.
  - An extra field 'Checkdigits' must be added for this purpose. It can
    be left empty. It must be in the range from '1001' till
    '4200' (and always 4 positions).
  MRA:8-OCT-2012 20013489 Overnight-shift-system.
  - Determine ShiftDate and store in QtyToStoreList for later use.
  - Also store LastShiftDate in DrawObject to also know the ShiftDate when
    employees are scanned out, but quantities are not stored yet to database.
  MRA:16-NOV-2012 TD-21162
  - Changing job does not update target value in a correct way (for Current).
  MRA:4-JAN-2013 TD-21738
  - Refresh emps gives wrong target for today.
  MRA:26-APR-2013  TD-22462 Bugfix.
  - Machines: Changing job on machine-level does not work.
  MRA:21-NOV-2013 20013196
  - Multiple counters on one workspot
  MRA:6-MAY-2014 TD-24819
  - Personal Screen and Comparison Jobs must use NOJOB for initial value
  MRA:12-MAY-2014 SO-20015330
  - Automatic reset of counters.
  MRA:12-MAY-2014 SO-20015331
  - Ignore Interface Codes handling
  MRA:30-MAY-2014 20015178
  - Measure Performance without employees
  MRA:12-JUN-2014 20014450
  - Machine hours registration
  MRA:17-JUN-2014 20015178.70 Rework
  - Related to this order:
  - When using 'Refresh employees'-option (settings) then it resets the
    efficiency-bar.
  MRA:8-SEP-2014 20013476
  - Make current variable, based on job-field SAMPLE_TIME_MINS.
  MRA:6-OCT-2014 20015376
  - Link Job Code Workspot - Show actual quantities
  - Related to this order: Assign time for the compare workspots based on
    workspot that is connected to it to ensure a corect value for the link-job.
  MRA:5-DEC-2014 SO-20015406
  - Add option to set workspot-scale and font-scale per workspot and
    efficiency-period-mode.
  MRA:23-DEC-2014 SO-20016016
  - Make it possible on workspot-level to read data (refresh) from database
    instead of from blackboxes.
  MRA:10-MAR-2015 SO-20015376.60
  - Link Job Code Work spot - Show actual quantities
  - It did not refresh the employee-info for a workspot that functions as a
    link-job.
  MRA:16-MAR-2015 20015346
  - Store scans in seconds
  - Cut off time and breaks handling
  - Creation-date and Mutation-date handling for scans
  - Addition of 2 Pims-Settings:
    - TimerecordingInSecs
    - IgnoreBreaks
  MRA:24-APR-2015 20016447
  - Improvement Machine-workspots functionality
  MRA:1-MAY-2015 20014450.50 Part 2
  - Real time efficiency
  MRA:1-JUN-2015 PIM-42
  - Trap errors when writing to a file.
  MRA:8-JUN-2015 ABS-8116
  - Do not show/log error when workspot is defined as 'receive qty from
    external app'.
  MRA:13-JUL-2015 PIM-12
  - Use a global setting for EfficiencyPeriodSinceTime that is assigned
    during reading of all data and based on plant-level of the first
    DrawObject that is read.
  MRA:6-OCT-2015 PIM-12 Bugfix
  - Use CurrentTimeWindow instead of (ARealTimeIntervalMinutes * 60)
  - Corrections for SampleTimeSeconds
  MRA:7-OCT-2015 PIM-12
  - Problem when showing current + efficiency based on time:
    - Theo time current must be determined in a different way!
  MRA:23-OCT-2015 PIM-12.2
  - Link job handling had to be changed because of changes for PIM-12.
  MRA:23-DEC-2015 PIM-12.3
  - Bugfixing for access violations:
    - When it recreates the employee-display-list be sure it does not
      conflict with clicking by the user on the AListButton and
      AGraphButton or it results in an access violation!
  MRA:30-DEC-2015 PIM-114
  - Determine shift-date before PQ is stored.
  - Addition + assignment of ADepartmentCode for TWorkspotTimeRec
  MRA:30-DEC-2015 PIM-116
  - During edit-mode it gives an access-violation when deleting a workspot.
  - IMPORTANT: Before 'free' of a TButton, be sure it is non-visible!
  -            Something to do with TColorButton?
  - To be sure: Before any component is freed, it is first made invisible.
  MRA:24-FEB-2016 PIM-12
  - Bugfix: The real-time-eff-table (workspoteffperminute) was not filled
    correct! It resulted in too high qty-values!
    To solve it, it is now done in a similar way as WriteToDB, by using the
    qty-to-store-list to store the quantities.
  MRA:3-MAR-2016 PIM-147
  - When multiple employees are currently working on the same workspot:
    - When an employee is currently scanned in on a different job:
      - Show alternatively name/job in an interval of about 2 seconds
  - When multiple employees work on 1 workspot then determine
    the current job based on the most used.
  MRA:4-MAR-2016 PIM-125
  - When no one is scanned in anymore, do not log errors.
  MRA:9-MAR-2016 PIM-111
  - For machine-workspot functionality:
    - It gave a wrong result when 1 workspot had already a different job and
      the second worked became different because of employees changing jobs
      via In/Out-button, then the colors were wrong, although it showed all
      jobs equal.
  MRA:17-MAR-2016 PIM-12.3
  - Bugfix for linked-jobs.
  - The eff. based on time was calculated wrong (too low).
    It should be based on the norm of the compare-workspots x
    number of compare-workspots. Example: When norm is 300 then it must be
    900 when there are 3 compare-workspots involved. Reason: Otherwise the
    reports will show wrong values!
    - This must NOT be done here, but by changing the norm of the workspot
      that is used as linked job. For example this must be set to 900, when
      3 compare-workspots are linked with each a norm of 300.
  MRA:18-MAR-2016 PIM-147
  - Show employee/job functionality.
  - Also show this when no counters are defined.
  - Show job in red.
  MRA:21-MAR-2016 PIM-155
  - Changed for Fake-Employee handling.
  MRA:29-MAR-2016 PIM-111
  - Machine Workspot handling:
    - It did not show the correct colors after clicking Refresh-button.
  MRA:30-MAR-2016 PIM-159
  - When using a workspot for comparison it shows a double value when
    employee scans out from the workspot that links to this comparison workspot.
  - Cause: It read this extra quantity on NOJOB; solved in MemorizeCounterValue.
  MRA:3-MAY-2016 PIM-151
  - Accept 'NOJOB' during 'AddQtyCurrent' for the job.
  MRA:20-JUN-2016 PIM-194
  - Use 3 colors for efficiency-bars.
  MRA:22-JUN-2016 PIM-195
  - Problem with reset of efficiency for Shift and Today.
  - When Refresh Employee-option is used it resets the efficiency to 0 when
    using Shift/Today-mode.
*)
unit UPersonalScreen;

interface

uses
  ShapeEx, StdCtrls, ExtCtrls, Classes, Forms, Controls, SysUtils, DateUtils,
  UPimsMessageRes, ORASystemDMT, UGlobalFunctions, UScannedIDCard,
  CalculateTotalHoursDMT, UProductionScreenDefs, IdIcmpClient,
  Graphics, ColorButton, Dialogs, UPimsConst, dxfProgressBar, UWorkspotEmpEff;

const
  CURRENT_TIME_WINDOW = 300; // Seconds. Example: 600 = 10 minutes
  MIN_TIME_WINDOW_MINUTE = 1; // Minutes. 20013476
  MAX_TIME_WINDOW_MINUTE = 60; // Minutes. 20013476
  PIMS_TIME_INTERVAL = 300; // Interval of 5 minutes for ProductionQuantity
  DAY_IN_SECS = 86400; // Seconds in 1 day
  QTYTOSTOREFILENAME = 'QtyToStoreList.txt';
  FAKE_EMPLOYEE_START_NUMBER: Integer = 999999;
  DEFAULT_WORKSPOT_SCALE = 100; // 20015406
//  NOJOB = 'NOJOB';
//  BREAKJOB = 'BREAK';
//  ModalBreak = 12;
//  ModalLunch = 13;

// 20014450.50: added: otPicture (for showing picture(s) in scheme)  
type
  TObjectType = (otImage, otLineVert, otLineHorz, otRectangle, otPuppetBox, otPicture);

type
  TShowMode = (
    ShowEmployeesOnWorkspot,
    ShowAllEmployees,
    ShowEmployeesOnWrongWorkspot,
    ShowEmployeesNotScannedIn,
    ShowEmployeesAbsentWithReason,
    ShowEmployeesAbsentWithoutReason,
    ShowEmployeesWithFirstAid,
    ShowEmployeesTooLate
  );

type
//  TProdScreenType = (pstNoTimeRec, pstMachineTimeRec, pstWorkspotTimeRec);
  TMachineType = (mtNone, mtWorkspot, mtMachine);
  TEffPeriodMode = (epCurrent, epSince, epShift);
//  TEffQuantityTime = (efQuantity, efTime);

type
  TCreateShapeEx = function(Value: TShapeExType): TShapeEx of object;
  TNewItemIndex = function: Integer of object;

// Machine
type
  PTMachineTimeRec = ^TMachineTimeRec;
  TMachineTimeRec = record
    AMachineCode: String;
    AMachineDescription: String;
    ACurrentJobCode: String;
    ACurrentJobDescription: String;
    AJobButton: TButton; // 20016447
    ADepartmentCode: String; // 20014450
    AStartTimeStamp: TDateTime; // 20014450
    AEndTimeStamp: TDateTime; // 20014450
    AShiftNumber: Integer; // 20014450
    AShiftDate: TDateTime; // 20014450
    AReasonCode: String; // 20014450
    AJobCount: Integer; // 20014450
    ADown: Boolean; // 20014550
  end; // Machine

// Workspot
type
  PTWorkspotTimeRec = ^TWorkspotTimeRec;
  TWorkspotTimeRec = record
    AMachineCode: String;
    AMachineDescription: String;
    AJobButton: TButton; // 20016447
    ACurrentJobCode: String;
    ACurrentJobDescription: String;
    ACompareJobCode: String;
    ACompareJobDescription: String;
    AEmpsScanIn: Boolean;
    ACurrentShiftNumber: Integer;
    ACurrentShiftStart: TDateTime;
    ACurrentShiftEnd: TDateTime;
    AMultipleShifts: Boolean;
    APreviousJobCode: String;
    AInButton: TButton;
    AModeButton: TButton;
    APercentageLabel: TLabel;
    AActualNameLabel: TLabel;
    AActualValueLabel: TLabel;
    ATargetNameLabel: TLabel;
    ATargetValueLabel: TLabel;
    ADiffNameLabel: TLabel;
    ADiffValueLabel: TLabel;
    ADiffLineHorz: TShapeEx;
    AIsCompareWorkspot: Boolean; // TD-24819
    ATimeRecByMachine: Boolean; // 20015178
    AFakeEmployeeNumber: Integer; // 20015178
    AFakeEmployeeNumberInit: Integer; // 20015178
    AReceiveQtyFromExternalApp: Boolean; // 20016016
    AIsLinkJobWorkspot: Boolean; // 20015376.60
    ABackColor, AForeColor, AHoverColor: TColor; // 20016447
    ADepartmentCode: String; // PIM-114
    ALastJobCode: String; // PIM-159
  end; // Workspot

// Fifo - Keeps a list of qty that were read over a period of max. 10 minutes.
//        When 10 minutes is reached, it must delete the first (oldest entry,
//        before it adds the new one.
type
  PTFifoRec = ^TFifoRec;
  TFifoRec = record
    TimeStamp: TDateTime;
    Qty: Integer;
    TimeElapsed: Double; // seconds
  end; // FifoRec

// Jobcode
type
  PJobCodeRec = class(TObject)
    JobCode: String;
    JobCodeDescription: String;
    InterfaceCode: String;
    Norm_prod_level: Integer;
    Last_Counter_Value: Integer;
    Act_prodqty_day: Integer;
    Act_prodqty_shift: Integer;
    Act_prodqty_current: Integer;
    Theo_prodqty_day: Integer;
    Theo_prodqty_shift: Integer;
    Theo_prodqty_current: Integer;
    Act_time_day: Double; // secs
    Act_time_shift: Double; // secs
    Act_time_current: Double; // secs
    Theo_time_day: Double; // secs
    Theo_time_shift: Double; // secs
    Theo_time_current: Double; // secs
    Sample_time_mins: Integer; // 20013476
    Fifo_list: TList;
    Done: Boolean;
    Link_WorkspotCode: String; // 20013196
    Link_JobCode: String;
    ARealTimeIntervalMinutes: Double; // 20014450.50
    Count: Integer; // PIM-147
  public
    constructor Create;
    destructor Destroy; override;
    procedure InitFifoList(AFree: Boolean);
    procedure AddQtyDay(AQty: Integer);
    procedure AddQtyShift(AQty: Integer);
    procedure AddQtyCurrent; overload;
    procedure AddQtyCurrent(AQty: Integer); overload;
    procedure AddTimeDay(ATimeElapsed: Double);
    procedure AddTimeShift(ATimeElapsed: Double);
    procedure AddTimeCurrent(ACurrentNumberOfEmployees: Integer);
    function Copy(AJobCodeRec: PJobCodeRec): PJobCodeRec;
    function SumOfFifoListCurrentQty: Integer;
    function SumOfFifoListCurrentTimeElapsed: Double;
    function SumOfFifoListQty(AStartTime, AEndTime: TDateTime): Double;
    procedure RemovePartFromFifoList(ACurrentTime: TDateTime);
//    function SampleTimeSeconds: Integer; // 20013476 // PIM-12
  end; // Jobcode

// BlackBox
type
  PBlackBoxRec = ^TBlackBoxRec;
  TBlackBoxRec = record
    InterfaceCode: String;
    IPAddress: String;
    Port: String;
    ModuleAddress: String;
    CounterNumber: Integer; // can be 1 or 2
    ActualCounterValue: Integer;
    ActualCounterDateTime: TDateTime;
    Disabled: Boolean; // Set this to 'true' to disable the counter.
    CheckDigits: String; // TODO-21191
    TestCounter: Integer; // For TEST-version.
    Reset: Boolean; // SO-20015330
    AddFakeCounterNow: Boolean; // For TEST-version;
  end; // BlackBox

// Employee: All employees worked/are working today at this workspot
type
//  PTEmployeeRec = ^TEmployeeRec;
  PEmployeeRec = class(TObject)
    EmployeeNumber: Integer;
    EmployeeName: String;
    ShiftNumber: Integer;
    Act_Time_Day: Double; // secs
    Act_Time_Shift: Double; // secs
    //	NOT  ---------  Act_Time_Current
	  CurrentlyScannedIn: Boolean;
    TeamCode: String;
    Level: String;
    DateTimeIn: TDateTime;
    DateTimeOut: TDateTime;
    PlannedWorkspot: String;
    PlannedStartTime: TDateTime;
    PlannedEndTime: TDateTime;
    PlannedLevel: String;
    Visible: Boolean; // 20013379
    JobCode: String; // PIM-147
    JobDescription: String; // PIM-147
  public
    constructor Create;
    destructor Destroy; override;
    procedure AddTimeDay(ATimeElapsed: Double);
    procedure AddTimeShift(ATimeElapsed: Double);
  end; // EmployeeRec

// EmployeeDisplay: Employees to display in scheme
type
  PTEmployeeDisplayRec = ^TEmployeeDisplayRec;
  TEmployeeDisplayRec = record
    EmployeeNumber: Integer;
    EmployeeName: String;
    AEmployeeNameLabel: TLabel;
    AWorkedMinutesLabel: TLabel; // 20014450.50 Optionally used
    APercentage: Integer; // 20014450.50
    APercentagePanel: TPanel; // 20014450.50
    AListButton: TBitBtn; // 20014450.50
    AGraphButton: TBitBtn; // 20014450.50
    JobCode: String; // PIM-147
    JobDescription: String; // PIM-147
    DefaultFontColor: TColor; // PIM-147
    ToggleFontColor: TColor; // PIM-147
  end; // EmployeeDisplayRec

// QtyToStore: What must be stored per job?
type
  PQtyToStoreRec = ^TQtyToStoreRec;
  TQtyToStoreRec = record
    Timestamp: TDateTime;
    JobCode: String;
    ShiftNumber: Integer;
    Quantity: Integer;
    Countervalue: Integer;
    ShiftDate: TDateTime;
  end; // QtyToStoreRec

// 20013196
// To make a list of link-jobs.
type
  PLinkJobRec = ^TLinkJobRec;
  TLinkJobRec = record
    PlantCode: String;
    WorkspotCode: String;
    JobCode: String;
    JobList: TList; // 20015376 Store the jobs that are linked to this linkjob
  end; // TLinkJobRec

// TD-24819
type
  PCompareJobRec = ^TCompareJobRec;
  TCompareJobRec = record
    WorkspotCode: String;
    JobCode: String;
  end; // TCompareJobRec

// PIM-213
type
  PTAEmployee = ^TAEmployee;
  TAEmployee = record
    AEmployeeNumber: Integer;
    AEmployeeNameLabel: TLabel;
    ADateTimeOut: TDateTime;
  end;

// 20015331
type
  PIgnoreInterfaceCodeRec = ^TIgnoreInterfaceCodeRec;
  TIgnoreInterfaceCodeRec = record
    PlantCode: String;
    WorkspotCode: String;
    JobCode: String;
    InterfaceCode: String;
  end; // TIgnoreInterfaceCodeRec

type
  PDrawObject = class(TObject)
    Application: TApplication;
    // Workspot
    APlantCode: String;
    AWorkspotCode: String;
    AWorkspotDescription: String;
    AMachineTimeRec: PTMachineTimeRec;
    AWorkspotTimeRec: PTWorkspotTimerec;
    ACurrentNumberOfEmployees: Integer;
    ALastCounterReadTime: TDateTime;
    AJobCodeList: TList; // All jobcodes defined for this workspot
    ABlackBoxList: TList; // All blackbox-info defined for this workspot
    AEmployeeList: TList; // All employees worked/are working today at this workspot
    AEmployeeDisplayList: TList; // Employees to display in scheme
    AInit: Boolean;
    AJobChanged: Boolean; // job has been changed.
    ARefreshTimeStamp: TDateTime;
    LastShiftDate: TDateTime; // 20013489
    ACompareJobList: TList; // TD-24819
    // Counters
    Last_Counter_Value: Integer;
    Act_prodqty_day: Integer;
    Act_prodqty_shift: Integer;
    Act_prodqty_current: Integer;
    Theo_prodqty_day: Integer;
    Theo_prodqty_shift: Integer;
    Theo_prodqty_current: Integer;
    Act_time_day: Double; // secs
    Act_time_shift: Double; // secs
    Act_time_current: Double; // secs
    Theo_time_day: Double; // secs
    Theo_time_shift: Double; // secs
    Theo_time_current: Double; // secs
    QtyToStoreList: TList; // Qty to Store List
    APrevJobCode: String; // 20013476
    APrevJobSampleTimeMins: Integer; // 20013476
    ACurJobSampleTimeMins: Integer; // 20013476
    ACurrentSampleTimeMins: Integer; // 20013476
    AUseSampleTime: Boolean; // 20013476
    // Object var.
    AVisible: Boolean;
    AObject: TObject;
    AType: TObjectType;
    ALabel: TLabel;
    ABorder: TShape;
    AEffMeterGreen: TShape;
    AEffMeterRed: TShape;
    AEffPercentage: Integer;
    AImageName: String;
//    APuppetShiftList: TList;
    ABigEffMeters: Boolean;
    AEfficiency: Double;
    ANormProdQuantity: Double;
    ANormProdLevel: Double;
    ATotalWorkedTime: Integer;
    ATotalProdQuantity: Double;
    ATotalProdQuantityCurrent: Double;
    ATotalNormQuantity: Double;
    ATotalPercentage: Double; // Integer->Double // 20014550.50
    AEffTotalsLabel: TLabel;
    ABlinkPuppets: Boolean;
    ABigEffLabel: TLabel;
    ANoData: Boolean;
    AEmployeeCount: Integer;
    AShowMode: TShowMode;
    ABlinkCompareJobs: Boolean;
    AProdScreenType: TProdScreenType;
    APriority: Integer;
    AWorkspotScale: Integer; // 20015406
    AFontScale: Integer; // 20015406
    AEffPeriodMode: TEffPeriodMode; // 20015406 0=Current 1=Since 2=Shift
    AMachineWorkspotJobDifferent: Boolean; // 20016447
    AMachineDrawObject: PDrawObject; // 20016447
    ACutOffTimeShiftDate: TDateTime; // 20014450.50
    ARealTimeIntervalMinutes: Double; // 20014450.50
    AListEmployeeNumber: Integer; // 20014450.50
    ARecreateEmployeeDisplayList: Boolean; // 20014450.50
    AHeight: Integer; // 20014450.50
    AWidth: Integer; // 20014450.50
    AMachineJobChanged: Boolean; // PIM-111 
    AGhostCountBlink: Boolean; // PIM-151
    ADefaultImage: TImage; // PIM-151
    AGhostImage: TImage; // PIM-151
    AProgressBar: TdxfProgressBar; // PIM-194
    ApnlWSEmpEff: TpnlWSEmpEff; // PIM-213
    AEmployeeDisplayListHasChanged: Boolean; // PIM-213
  private
    procedure InitLists(AFree: Boolean; AAll: Boolean=True);
  public
    constructor Create;
    procedure InitEmployeeList(AFree: Boolean);
    procedure CreateMachine(AApplication: TApplication;
      ANewItemIndex: TNewItemIndex; AButtonChangeJobClick: TNotifyEvent;
      AImageMouseDown: TMouseEvent; AImageMousemove: TMouseMoveEvent;
      AImageMouseUp: TMouseEvent; APnlDraw: TPanel);
    procedure CreateWorkspot(AApplication: TApplication;
      ANewItemIndex: TNewItemIndex; AButtonChangeJobClick: TNotifyEvent;
      AButtonInClick: TNotifyEvent; AButtonChangeModeClick: TNotifyEvent;
      AImageMouseDown: TMouseEvent; AImageMousemove: TMouseMoveEvent;
      AImageMouseUp: TMouseEvent; APnlDraw: TPanel;
      ACreateShapeEx: TCreateShapeEx; AEfficiencyPeriodMode: TEffPeriodMode);
    procedure CreateEmployee(AApplication: TApplication;
      AEmployeeRec: PEmployeeRec;
      AListButtonClick: TNotifyEvent; AGraphButtonClick: TNotifyEvent;
      AShowEmployeeListClick: TNotifyEvent;
      AImageMouseDown: TMouseEvent; AImageMousemove: TMouseMoveEvent;
      AImageMouseUp: TMouseEvent; APnlDraw: TPanel;
      AEfficiencyPeriodMode: TEffPeriodMode);
    function FindEmployeeListIndex(AEmployeeNumber: Integer): Integer;
    function FindEmployeeDisplayListIndex(AEmployeeNumber: Integer): Integer;
    function FindJobCodeListIndex(AJobCode: String): Integer;
    function FindJobCodeListRec(AJobCode: String): PJobCodeRec;
    function EmployeeListChangedTest: Boolean;
    procedure InitEmployeeDisplayList(AFree: Boolean);
    procedure MemorizeCounterValue(ADrawObject: PDrawObject;
      ABlackBoxRec: PBlackBoxRec);
    procedure AddQtyDay(AQty: Integer);
    procedure AddQtyShift(AQty: Integer);
    procedure AddQtyCurrent; overload;
    procedure AddQtyCurrent(AQty: Integer); overload;
    procedure AddTimeDay(ATimeElapsed: Double);
    procedure AddTimeShift(ATimeElapsed: Double);
    procedure AddTimeDayEmployeeList(ATimeElapsed: Double);
    procedure AddTimeShiftEmployeeList(ATimeElapsed: Double);
    procedure AddTimeCurrent;
    procedure AddTheoTimeDay(var AJobCodeRec: PJobCodeRec);
    procedure AddTheoTimeShift(var AJobCodeRec: PJobCodeRec);
    procedure AddTheoTimeCurrent(var AJobCodeRec: PJobCodeRec);
    procedure AddTheoQtyDay(var AJobCodeRec: PJobCodeRec);
    procedure AddTheoQtyShift(var AJobCodeRec: PJobCodeRec);
    procedure AddTheoQtyCurrent(var AJobCodeRec: PJobCodeRec);
    procedure AddElementToQtyToStoreList(AQty, ACounterValue: Integer);
    procedure InitQtyToStoreList(AFree: Boolean);
    procedure WriteQtyToStoreListToFile(ALogPath: String);
    procedure AddUpdateEmployeeList(AEmployeeNumber: Integer;
      AEmployeeName: String; AShiftNumber: Integer; ATeamCode: String;
      ALevel: String; ADateTimeIn, ADateTimeOut: TDateTime;
      AProcessedYN: String; AJobCode, AJobCodeDescription: String;
      AReadAll: Boolean;
      AVisible: Boolean=True
      {ADeleteWhenProcessed: Boolean=False}
      );
    procedure DefineMultipleShiftsYN;
    procedure ResetShiftCounters;
    procedure ResetJobsTheoTime;
    destructor Destroy; override;
    procedure FreeLists(AAll: Boolean=True);
    procedure Init(AAll: Boolean=True);
    procedure InitTime;
    procedure InitQty;
    procedure InitCompareJobList(AFree: Boolean);
    procedure InitFifoList(AFree: Boolean);
    procedure CompareJobInitFifoList(APSList: TList);
    procedure RemovePartFromFifoList;
    procedure DetermineTheoProdQtyCurrent;
    procedure DetermineTheoTimeCurrent;
//    function NewSampleTimeSeconds: Double; // 20013476 // PIM-12
{    procedure InsertWorkspotEfficiencyPerMinute(
      APSList: TList;
      AIntervalStartTime, AIntervalEndTime: TDateTime; ATillNow: Boolean); } // 20014450.50 // PIM-12
    function DetermineEmployeeName(AEmployeeNumber: Integer): String;
    function FindEmployeeRec(AEmployeeNumber: Integer): PEmployeeRec; // PIM-147
    function DetermineCurrentJobByEmployeeList: PJobCodeRec;
  end; // PDrawObject

// ReadCounter
type
  PReadCounterRec = ^TReadCounterRec;
  TReadCounterRec = record
    DrawObject: PDrawObject; // Reference to DrawObject
    BlackBoxRec: PBlackBoxRec; // Reference to BlackBoxRec
  end; // ReadCounter

function FindDrawObject(APSList: TList;
  APlantCode, AWorkspotCode: String): PDrawObject;
function FindDrawObjectMachine(APSList: TList;
  APlantCode, AMachineCode: String): PDrawObject; // 20016447
procedure DeleteReadCounterRecForDrawObject(var AReadCounterList: TList;
  ADrawObject: PDrawObject);
procedure ClearReadCounterList(var AReadCounterList: TList);
function IPExists(IPAddr: String): Boolean;
function BlackBoxFind(AReadCounterList: TList; AHost: String): PBlackBoxRec;
procedure RecreateReadCounterList(APSList: TList; var AReadCounterList: TList);
procedure SetCurrentPeriod(ACurrentPeriod: Integer);
procedure AddLinkJobList(APlantCode, AWorkspotCode, AJobCode: String;
  ALinkPlantCode, ALinkWorkspotCode, ALinkJobCode: String); // 20013196
function FindLinkJobList(APlantCode, AWorkspotCode, AJobCode: String): PLinkJobRec; // 20013196 // 20015376
procedure InitIgnoreInterfaceCodeList(AFree: Boolean); // 20015331
procedure ClearLinkJobList; // 20015376
procedure HandleLinkJobsActualValues(APSList: TList); // 20015376
procedure HandleCompareJobsTime(APSList: TList); // 20015376
procedure MachineWorkspotsInit(APSList: TList); // 20016447
(* procedure MachineWorkspotsDetermineMachineJob(APSList: TList); // 20016447 *)
function MachineWorkspotsCheckJobs(APSList: TList): Boolean; // 20016447
{procedure ActionRealTimeEfficiencyMain(APSList: TList; ANow: TDateTime;
  ATillNow: Boolean); } // 20014450.50 // PIM-12
{procedure ActionRealTimeEfficiency(APSList: TList; ANow: TDateTime); } // 20014450.50
{procedure ActionRealTimeEfficiencyTillNow(APSList: TList; ANow: TDateTime); } // 20014450.50
procedure MarkAllRecreateEmployeeDisplayList(APSList: TList);
function FindJobCodeRec(APSList: TList; APlantCode, AWorkspotCode, AJobCode: String): PJobCodeRec;
function EmployeesCurrentlyScannedInCheck(APSList: TList): Boolean;
function DetermineEfficiencyPeriodMode(ADrawObject: PDrawObject): TEffPeriodMode;

var
  CurrentTimeWindow: Integer=CURRENT_TIME_WINDOW;
  LinkJobList: TList; // 20013196
  IgnoreInterfaceCodeList: TList; // 20015331
  WorkspotScale: Integer; // 20015406
  FontScale: Integer; // 20015406
  EffPeriodMode: TEffPeriodMode; // 20015406 0=Current 1=Since 2=Shift
  LastTimeInsertWorkspotPerMinute: TDateTime = 0; // 20014450.50
  BBCount: Integer; // ABS-8116
  GlobalEfficiencyPeriodSinceTime: TDateTime; // 20014450.50
  LastTimeReadExtData: TDateTime = 0; // PIM-90
  ReadExternalData: Boolean = False; // PIM-90

implementation

uses PersonalScreenDMT, RealTimeEffDMT;

procedure SetCurrentPeriod(ACurrentPeriod: Integer);
begin
  if (ACurrentPeriod >= MIN_TIME_WINDOW_MINUTE) and
    (ACurrentPeriod <= MAX_TIME_WINDOW_MINUTE) then // 20013476
    CurrentTimeWindow := ACurrentPeriod * 60
  else
    CurrentTimeWindow := CURRENT_TIME_WINDOW;
end;

// 20012858.80
function BlackBoxFind(AReadCounterList: TList;
  AHost: String): PBlackBoxRec;
var
  I: Integer;
  ABlackBoxRec: PBlackBoxRec;
  AReadCounterRec: PReadCounterRec;
begin
  Result := nil;
  try
    if Assigned(AReadCounterList) then
      for I := 0 to AReadCounterList.Count - 1 do
      begin
        AReadCounterRec := AReadCounterList.Items[I];
        ABlackBoxRec := AReadCounterRec.BlackBoxRec;
        if ABlackBoxRec.IPAddress = AHost then
          Result := ABlackBoxRec;
      end;
  except
    //
  end;
end; // BlackBoxFind

// 20012858.70
// This tests the IPaddress (ping).
// TODO 21191 Disable this function (does not work without admin-rights).
function IPExists(IPAddr: String): Boolean;
{
var
  AIdIcmpClient: TIdIcmpClient;
  Rec: Integer;
}
begin
  Result := True;
{
  Result:= False;
  AIdIcmpClient := TIdIcmpClient.Create(nil);
  try
    AIdIcmpClient.Host:= IPAddr;
    AIdIcmpClient.Ping();
    Sleep(2000);
    Rec := AIdIcmpClient.ReplyStatus.BytesReceived;
    if Rec <> 0 then
      Result:= True
    else
      Result:= False;
  finally
    AIdIcmpClient.Free;
  end;
}
end; // IPExists

(*
procedure MyWriteLog(AMsg: String);
const
  MYLOGFILENAME='PSLOGTEST.txt';
var
  TFile: TextFile;
  Exists: Boolean;
  LogName: String;
begin
  AMsg := DateTimeToStr(Now) + ' - ' + AMsg;
  LogName := '..\Log\' + ORASystemDM.CurrentComputerName + '_' + MYLOGFILENAME;

  try
    Exists := FileExists(LogName);
    AssignFile(TFile, LogName);
    try
      if not Exists then
        Rewrite(TFile)
      else
        Append(TFile);
      WriteLn(TFile, AMsg);
    except
//      MemoLog.Lines.Add('Error! Cannot find/access file: ' + LogPath);
//      Update;
    end;
  finally
    CloseFile(TFile);
  end;
end; // WriteLog
*)

function FindDrawObject(APSList: TList; APlantCode,
  AWorkspotCode: String): PDrawObject;
var
  I: Integer;
  ADrawObject: PDrawObject;
begin
  Result := nil;
  for I := 0 to APSList.Count - 1 do
  begin
    ADrawObject := APSList.Items[I];
    if Assigned(ADrawObject.AWorkspotTimeRec) then
    begin
      if (ADrawObject.APlantCode = APlantCode) and
        (ADrawObject.AWorkspotCode = AWorkSpotCode) then
      begin
        Result := ADrawObject;
        Break;
      end;
    end;
  end;
end; // FindDrawObject

// 20016447
function FindDrawObjectMachine(APSList: TList;
  APlantCode, AMachineCode: String): PDrawObject;
var
  I: Integer;
  ADrawObject: PDrawObject;
begin
  Result := nil;
  for I := 0 to APSList.Count - 1 do
  begin
    ADrawObject := APSList.Items[I];
    if Assigned(ADrawObject.AMachineTimeRec) then
    begin
      if (ADrawObject.APlantCode = APlantCode) and
        (ADrawObject.AMachineTimeRec.AMachineCode = AMachineCode) then
      begin
        Result := ADrawObject;
        Break;
      end;
    end;
  end;
end; // FindDrawObjectMachine

// 20016447 Assign DrawObject of Machine to Workspots that are linked to it.
procedure MachineWorkspotsInit(APSList: TList);
var
  I: Integer;
  ADrawObject: PDrawObject;
begin
  for I := 0 to APSList.Count - 1 do
  begin
    ADrawObject := APSList.Items[I];
    if Assigned(ADrawObject.AWorkspotTimeRec) then
      ADrawObject.AMachineDrawObject :=
        FindDrawObjectMachine(APSlist, ADrawObject.APlantCode,
          ADrawObject.AWorkspotTimeRec.AMachineCode);
  end;
//  MachineWorkspotsDetermineMachineJob(APSList);
end; // MachineWorkspotsInit

(*
// 20016447 Determine the current job of the machine based on the current job
//          of the workspots that are linked to it.
procedure MachineWorkspotsDetermineMachineJob(APSList: TList);
var
  I: Integer;
  ADrawObject: PDrawObject;
begin
  for I := 0 to APSList.Count - 1 do
  begin
    ADrawObject := APSList.Items[I];
    if Assigned(ADrawObject.AWorkspotTimeRec) then
    begin
      if Assigned(ADrawObject.AMachineDrawObject) then
      begin
        if ADrawObject.ACurrentNumberOfEmployees > 0 then
        begin
          ADrawObject.AMachineDrawObject.AMachineTimeRec.ACurrentJobCode :=
            ADrawObject.AWorkspotTimerec.ACurrentJobCode;
          ADrawObject.AMachineDrawObject.AMachineTimeRec.ACurrentJobDescription :=
            ADrawObject.AWorkspotTimerec.ACurrentJobDescription;
        end;
      end;
    end;
  end;
end; // MachineWorkspotsDetermineMachineJob
*)
// 20016447 Machine Workspots Check Jobs
// PIM-111 Return the result
function MachineWorkspotsCheckJobs(APSList: TList): Boolean;
var
  I: Integer;
  ADrawObject: PDrawObject;
begin
  Result := False;
  for I := 0 to APSList.Count - 1 do
  begin
    ADrawObject := APSList.Items[I];
    if Assigned(ADrawObject.AWorkspotTimeRec) then
    begin
      if Assigned(ADrawObject.AMachineDrawObject) then
      begin
        ADrawObject.AMachineWorkspotJobDifferent := False;
        if (ADrawObject.AMachineDrawObject.AMachineTimeRec.ACurrentJobCode <> '') and
          (ADrawObject.AWorkspotTimeRec.ACurrentJobCode <> '') then
          if ADrawObject.AWorkspotTimeRec.ACurrentJobCode <>
            ADrawObject.AMachineDrawObject.AMachineTimeRec.ACurrentJobCode then
          begin
            ADrawObject.AMachineWorkspotJobDifferent := True;
            Result := True;
          end;
      end;
    end;
  end;
end; // MachineWorkspotCheckJobs

procedure DeleteReadCounterRecForDrawObject(var AReadCounterList: TList;
  ADrawObject: PDrawObject);
var
  I: Integer;
  AReadCounterRec: PReadCounterRec;
begin
  for I := AReadCounterList.Count - 1 downto 0 do
  begin
    AReadCounterRec := AReadCounterList.Items[I];
    if AReadCounterRec.DrawObject = ADrawObject then
    begin
      AReadCounterList.Remove(AReadCounterRec);
      Dispose(AReadCounterRec);
      Break;
    end;
  end;
end; // DeleteReadCounterRecForDrawObject

procedure ClearReadCounterList(var AReadCounterList: TList);
var
  I: Integer;
  AReadCounterRec: PReadCounterRec;
begin
  for I := AReadCounterList.Count - 1 downto 0 do
  begin
    AReadCounterRec := AReadCounterList.Items[I];
    AReadCounterList.Remove(AReadCounterRec);
    Dispose(AReadCounterRec);
  end;
  AReadCounterList.Clear;
end; // ClearReadCounterList

function DetermineEfficiencyPeriodMode(
  ADrawObject: PDrawObject): TEffPeriodMode;
begin
  Result := epCurrent;
  if Assigned(ADrawObject) then
    if Assigned(ADrawObject.AWorkspotTimeRec) then
    begin
      if ADrawObject.AWorkspotTimeRec.AModeButton.Caption =
        SPimsModeButtonCurrent then
        Result := epCurrent
      else
        if ADrawObject.AWorkspotTimeRec.AModeButton.Caption =
          SPimsModeButtonSince then
            Result := epSince
       else
          if ADrawObject.AWorkspotTimeRec.AModeButton.Caption =
            SPimsModeButtonShift then
              Result := epShift;
    end;
end; // DetermineEfficiencyPeriodMode

{ DrawObject }

constructor PDrawObject.Create;
begin
  inherited Create;
//
end;

destructor PDrawObject.Destroy;
begin
//
  inherited Destroy;
end;

{ PJobCodeRec }

procedure PJobCodeRec.RemovePartFromFifoList(ACurrentTime: TDateTime);
var
  AFifoRec: PTFifoRec;
  I, Diff: Integer;
begin
  // Remove items older than x minutes.
  if Assigned(Fifo_list) then
  begin
    for I := Fifo_list.Count - 1 downto 0 do
    begin
      AFifoRec := Fifo_list.Items[I];
      Diff := Round((ACurrentTime - AFifoRec.TimeStamp) * DAY_IN_SECS);
      if Diff > CurrentTimeWindow then // 20013476 // 20014450.50
      begin
        Fifo_list.Remove(AFifoRec);
        Dispose(AFifoRec);
      end; // if
    end; // for
  end; // if
end; // RemovePartFromFifoList

function PJobCodeRec.SumOfFifoListCurrentQty: Integer;
var
  AFifoRec: PTFifoRec;
  I, Diff: Integer;
begin
  Result := 0;
  if Assigned(Fifo_list) then
  begin
    for I := 0 to Fifo_list.Count - 1 do
    begin
      AFifoRec := Fifo_list.Items[I];
      // For CURRENT: Only add qty for last CurrentTimeWindow
      Diff := Round((Now - AFifoRec.TimeStamp) * DAY_IN_SECS);
      if Diff <= CurrentTimeWindow then // 20013476 // 20014450.50
      begin
        Result := Result +
          AFifoRec.Qty;
      end;
    end; // for
  end; // if
end; // SumOfFifoListCurrentQty

// 20014450.50
function PJobCodeRec.SumOfFifoListQty(AStartTime,
  AEndTime: TDateTime): Double;
var
  AFifoRec: PTFifoRec;
  I: Integer;
begin
  Result := 0;
  if Assigned(Fifo_list) then
  begin
    for I := Fifo_list.Count - 1 downto 0 do
    begin
      AFifoRec := Fifo_list.Items[I];
      if AFifoRec.Qty <> 0 then
        if (AFifoRec.TimeStamp >= AStartTime) and
          (AFifoRec.TimeStamp < AEndTime) then
            Result := Result + AFifoRec.Qty;
//      if (AFifoRec.TimeStamp < AStartTime) then
//        Break;
    end; // for
  end; // if
end; // SumOfFifoListQty

function PJobCodeRec.SumOfFifoListCurrentTimeElapsed: Double;
var
  AFifoRec: PTFifoRec;
  I, Diff: Integer;
begin
  Result := 0;
  if Assigned(Fifo_list) then
  begin
    for I := 0 to Fifo_list.Count - 1 do
    begin
      AFifoRec := Fifo_list.Items[I];
      // For CURRENT: Only add qty for last CurrentTimeWindow
      Diff := Round((Now - AFifoRec.TimeStamp) * DAY_IN_SECS);
      if Diff <= CurrentTimeWindow then // 20013476 // 20014450.50
      begin
        Result := Result +
          AFifoRec.TimeElapsed;
      end;
    end;
  end;
end; // SumOfFifoListCurrentTimeElapsed

procedure PJobCodeRec.AddQtyDay(AQty: Integer);
begin
  Act_prodqty_day := Act_prodqty_day + AQty;
end;

procedure PJobCodeRec.AddQtyShift(AQty: Integer);
begin
  Act_prodqty_shift := Act_prodqty_shift + AQty;
end;

// Sum of qty in fifo_list is Qty-Current
procedure PJobCodeRec.AddQtyCurrent;
begin
  // ATTENTION: Do not add the qty for NOJOB, to prevent
  // it shows a negative value for CURRENT.
//  if JobCode <> NOJOB then // PIM-151
    Act_prodqty_current := SumOfFifoListCurrentQty;
end; // AddQtyCurrent

procedure PJobCodeRec.AddQtyCurrent(AQty: Integer);
begin
  inherited;
//  if JobCode <> NOJOB then // PIM-151
    Act_prodqty_current := Act_prodqty_current + AQty;
end; // AddQtyCurrent

procedure PJobCodeRec.AddTimeDay(ATimeElapsed: Double);
begin
  // TimeElapsed is in secs
  Act_time_day := Act_time_day + ATimeElapsed;
end; // AddTimeDay

procedure PJobCodeRec.AddTimeShift(ATimeElapsed: Double);
begin
  // TimeElapsed is in secs
  Act_time_shift := Act_time_shift + ATimeElapsed
end; // AddTimeShift

procedure PJobCodeRec.AddTimeCurrent(ACurrentNumberOfEmployees: Integer);
begin
  // For current: This must be fixed, because for current
  // always look at (the last) 5 minutes.
  // If the fifo-list is used, then the first 5 minutes it
  // will give a wrong value! (too low).
  // TD-21162
//  Act_time_current := CurrentTimeWindow * ACurrentNumberOfEmployees;

  // TD-21162
  Act_time_current := SumOfFifoListCurrentTimeElapsed;

  // TD-21162
  if Act_time_current > CurrentTimeWindow then // 20014450.50
    Act_time_current := CurrentTimeWindow; // 20013476 // 20014450.50
end; // AddTimeCurrent

constructor PJobCodeRec.Create;
begin
  inherited Create;
  JobCode := '';
  JobCodeDescription := '';
  InterfaceCode := '';
  Norm_prod_level := 0;
  Last_Counter_Value := 0;
  Act_prodqty_day := 0;
  Act_prodqty_shift := 0;
  Act_prodqty_current := 0;
  Act_time_day := 0;
  Act_time_shift := 0;
  Act_time_current := 0;
  Theo_prodqty_day := 0;
  Theo_prodqty_shift := 0;
  Theo_prodqty_current := 0;
  Theo_time_day := 0;
  Theo_time_shift := 0;
  Theo_time_current := 0;
  Sample_time_mins := 0; // 20013476
  Link_WorkspotCode := ''; // 20013196
  Link_JobCode := ''; // 20013196
  ARealTimeIntervalMinutes := -1; // 20014450.50
  Fifo_list := TList.Create;
end; // Create

destructor PJobCodeRec.Destroy;
begin
//
  inherited Destroy;
end; // Destroy

procedure PJobCodeRec.InitFifoList(AFree: Boolean);
var
  AJobCodeRec: PJobCodeRec;
  AFifoRec: PTFifoRec;
  I: Integer;
begin
  AJobCodeRec := Self;
  if Assigned(AJobCodeRec.Fifo_list) then
  begin
    for I := AJobCodeRec.Fifo_list.Count - 1 downto 0 do
    begin
      AFifoRec := AJobCodeRec.Fifo_list.Items[I];
      Dispose(AFifoRec);
    end;
    AJobCodeRec.Fifo_list.Clear;
  end;
  if AFree then
    AJobCodeRec.Fifo_list.Free;
end; // InitFifoList

procedure PDrawObject.InitEmployeeList(AFree: Boolean);
var
  I: Integer;
  ADrawObject: PDrawObject;
  AEmployeeRec: PEmployeeRec;
begin
  ADrawObject := Self;
  if Assigned(ADrawObject.AEmployeeList) then
  begin
    for I := ADrawObject.AEmployeeList.Count - 1 downto 0 do
    begin
      AEmployeeRec := ADrawObject.AEmployeeList.Items[I];
      AEmployeeRec.Free;
    end;
    ADrawObject.AEmployeeList.Clear;
    if AFree then
      ADrawObject.AEmployeeList.Free;
  end;
end; // InitEmployeeList

procedure PDrawObject.InitLists(AFree: Boolean; AAll: Boolean=True);
var
  ADrawObject: PDrawObject;
  AJobCodeRec: PJobCodeRec;
  ABlackBoxRec: PBlackBoxRec;
  I: Integer;
begin
  ADrawObject := Self;
  if Assigned(ADrawObject.AJobCodeList) then
  begin
    for I := ADrawObject.AJobCodeList.Count - 1 downto 0 do
    begin
      AJobCodeRec := ADrawObject.AJobCodeList.Items[I];
      AJobCodeRec.InitFifoList(AFree);
      AJobCodeRec.Free;
    end;
    ADrawObject.AJobCodeList.Clear;
    if AFree then
      ADrawObject.AJobCodeList.Free;
  end;
  if AAll then
  begin
    if Assigned(ADrawObject.ABlackBoxList) then
    begin
      for I := ADrawObject.ABlackBoxList.Count - 1 downto 0 do
      begin
        ABlackBoxRec := ADrawObject.ABlackBoxList.Items[I];
        Dispose(ABlackBoxRec);
      end;
      ADrawObject.ABlackBoxList.Clear;
      if AFree then
        ADrawObject.ABlackBoxList.Free;
    end;
  end;
  ADrawObject.InitEmployeeList(AFree);
  ADrawObject.InitEmployeeDisplayList(AFree);
  ADrawObject.InitQtyToStoreList(AFree);
  ADrawObject.InitCompareJobList(AFree); // TD-24819
end; // InitLists

// Reset all shift counter before the shift is changing.
procedure PDrawObject.ResetShiftCounters;
var
  I: Integer;
  ADrawObject: PDrawObject;
  AJobCodeRec: PJobCodeRec;
  AEmployeeRec: PEmployeeRec;
begin
  ADrawObject := Self;
  ADrawObject.Act_prodqty_shift := 0;
  ADrawObject.Act_time_shift := 0;
  ADrawObject.Theo_prodqty_shift := 0;
  ADrawObject.Theo_time_shift := 0;
  if Assigned(ADrawObject.AJobCodeList) then
  begin
    for I := 0 to ADrawObject.AJobCodeList.Count - 1 do
    begin
      AJobCodeRec := ADrawObject.AJobCodeList[I];
      AJobCodeRec.Act_prodqty_shift := 0;
      AJobCodeRec.Act_time_shift := 0;
      AJobCodeRec.Theo_prodqty_shift := 0;
      AJobCodeRec.Theo_time_shift := 0;
    end;
  end;
  if Assigned(ADrawObject.AEmployeeList) then
  begin
    for I := 0 to ADrawObject.AEmployeeList.Count - 1 do
    begin
      AEmployeeRec := ADrawObject.AEmployeeList[I];
      if AEmployeeRec.CurrentlyScannedIn then
        AEmployeeRec.Act_Time_Shift := 0;
    end;
  end;
end; // ResetShiftCounters

procedure PDrawObject.FreeLists(AAll: Boolean=True);
begin
  InitLists(True, AAll);
end; // FreeLists

// Only init time-variables
procedure PDrawObject.InitTime;
var
  ADrawObject: PDrawObject;
begin
  ADrawObject := Self;

  ADrawObject.Act_time_day := 0;
  ADrawObject.Act_time_shift := 0;
  ADrawObject.Act_time_current := 0;
  ADrawObject.Theo_time_day := 0;
  ADrawObject.Theo_time_shift := 0;
  ADrawObject.Theo_time_current := 0;
  //
end; // InitTime

// TD-21738
// Only init qty-varables
procedure PDrawObject.InitQty;
var
  ADrawObject: PDrawObject;
begin
  ADrawObject := Self;

  ADrawObject.Theo_prodqty_day := 0;
  ADrawObject.Theo_prodqty_shift := 0;
  ADrawObject.Theo_prodqty_current := 0;
end; // InitQty;

procedure PDrawObject.Init(AAll: Boolean=True);
var
  ADrawObject: PDrawObject;
begin
  ADrawObject := Self;

  InitLists(False, AAll);

  ADrawObject.Act_prodqty_day := 0;
  ADrawObject.Act_prodqty_shift := 0;
  ADrawObject.Act_prodqty_current := 0;
  ADrawObject.Theo_prodqty_day := 0;
  ADrawObject.Theo_prodqty_shift := 0;
  ADrawObject.Theo_prodqty_current := 0;
  ADrawObject.APrevJobCode := ''; // 20013476
  ADrawObject.APrevJobSampleTimeMins := 0; // 20013476
  ADrawObject.ACurJobSampleTimeMins := 0;  // 20013476
  ADrawObject.ACurrentSampleTimeMins := 0; // 20013476
  ADrawObject.AUseSampleTime := False; // 20013476
  InitTime;
  if Assigned(ADrawObject.AMachineTimeRec) then
  begin
    ADrawObject.AMachineTimeRec.ACurrentJobCode := '';
    ADrawObject.AMachineTimeRec.ACurrentJobDescription := '';
  end;
  if Assigned(ADrawObject.AWorkspotTimeRec) then
  begin
    ADrawObject.AWorkspotTimeRec.ACurrentJobCode := '';
    ADrawObject.AWorkspotTimeRec.ACurrentJobDescription := '';
    ADrawObject.AWorkspotTimeRec.ACurrentShiftNumber := 0;
    ADrawObject.AWorkspotTimeRec.ACurrentShiftStart := 0;
    ADrawObject.AWorkspotTimeRec.ACurrentShiftEnd := 0;
    ADrawObject.AWorkspotTimeRec.AMultipleShifts := False;
    ADrawObject.AWorkspotTimeRec.APreviousJobCode := '';
  end;
  ADrawObject.ACurrentNumberOfEmployees := 0;
  ADrawObject.ALastCounterReadTime := 0;
end; // Init

procedure PDrawObject.CreateMachine(AApplication: TApplication;
  ANewItemIndex: TNewItemIndex; AButtonChangeJobClick: TNotifyEvent;
  AImageMouseDown: TMouseEvent; AImageMousemove: TMouseMoveEvent;
  AImageMouseUp: TMouseEvent; APnlDraw: TPanel);
var
  ADrawObject: PDrawObject;
begin
  ADrawObject := Self;
// PIM-116 During Create, do not use AApplication by ApnlDraw
  ADrawObject.AMachineTimeRec := new(PTMachineTimeRec);
  ADrawObject.AWorkspotTimeRec := nil;
  ADrawObject.APriority := 3;
  ADrawObject.APrevJobCode := ''; // 20013476
  ADrawObject.APrevJobSampleTimeMins := 0; // 20013476
  ADrawObject.ACurJobSampleTimeMins := 0;  // 20013476
  ADrawObject.ACurrentSampleTimeMins := 0; // 20013476
  ADrawObject.AUseSampleTime := False; // 20013476
  ADrawObject.AWorkspotScale := WorkspotScale; // 20015406
  ADrawObject.AFontScale := FontScale; // 20015406
  ADrawObject.AEffPeriodMode := EffPeriodMode; // 20015406
  ADrawObject.AMachineTimeRec.AJobButton := TButton.Create(APnlDraw); // 20016447
  ADrawObject.ACutOffTimeShiftDate := NullDate; // 20014450.50
  ADrawObject.ARealTimeIntervalMinutes := 5; // 20014450.50 // PIM-12 0 -> 5
  ADrawObject.ARecreateEmployeeDisplayList := False; // 20014450.50
  with ADrawObject.AMachineTimeRec.AJobButton do
  begin
    Tag := ANewItemIndex;
    // Temporary disabled -> Should this also work for a machine?
    // TD-22462 Enabled this button.
    OnClick := AButtonChangeJobClick;
    OnMouseDown := AImageMouseDown;
    OnMouseMove := AImageMouseMove;
    OnMouseUp := AImageMouseUp;
    Parent := APnlDraw; // ADrawObject.APanel;
    Visible := False;
  end;
end; // CreateMachine

procedure PDrawObject.CreateWorkspot(AApplication: TApplication;
  ANewItemIndex: TNewItemIndex; AButtonChangeJobClick: TNotifyEvent;
  AButtonInClick: TNotifyEvent; AButtonChangeModeClick: TNotifyEvent;
  AImageMouseDown: TMouseEvent; AImageMousemove: TMouseMoveEvent;
  AImageMouseUp: TMouseEvent; APnlDraw: TPanel;
  ACreateShapeEx: TCreateShapeEx; AEfficiencyPeriodMode: TEffPeriodMode
  );
var
  ADrawObject: PDrawObject;
begin
  ADrawObject := Self;
  // PIM-116 During Create, do not use AApplication by ApnlDraw
  ADrawObject.AMachineTimeRec := nil;
  ADrawObject.AWorkspotTimeRec := new(PTWorkspotTimeRec);
  ADrawObject.AWorkspotTimeRec.AJobButton := TButton.Create(ApnlDraw); // 20016447
  ADrawObject.AWorkspotTimeRec.ABackColor :=
    ADrawObject.AWorkspotTimeRec.AJobButton.BackColor; // 20016447
  ADrawObject.AWorkspotTimeRec.AForeColor :=
    ADrawObject.AWorkspotTimeRec.AJobButton.ForeColor; // 20016447
  ADrawObject.AWorkspotTimeRec.AHoverColor :=
    ADrawObject.AWorkspotTimeRec.AJobButton.HoverColor; // 20016447
  ADrawObject.AWorkspotTimeRec.APreviousJobCode := '';
  // 20013379 Compare Job Handling
  ADrawObject.AWorkspotTimeRec.ACompareJobCode := '';
  ADrawObject.AWorkspotTimeRec.ACompareJobDescription := '';
  ADrawObject.AWorkspotTimeRec.AIsCompareWorkspot := False; // TD-24819
  ADrawObject.AWorkspotTimeRec.AIsLinkJobWorkspot := False; // 20015376.60
  ADrawObject.APriority := 1;
  ADrawObject.AInit := True;
  ADrawObject.AJobChanged := False;
  ADrawObject.Last_Counter_Value := 0;
  ADrawObject.LastShiftDate := NullDate; // 20013489
  ADrawObject.AWorkspotTimeRec.ATimeRecByMachine := False; // 20015178
  ADrawObject.AWorkspotTimeRec.AFakeEmployeeNumber := -1; // 20015178
  ADrawObject.AWorkspotTimeRec.AFakeEmployeeNumberInit := -1; // 20015178
  ADrawObject.AWorkspotTimeRec.AReceiveQtyFromExternalApp := False; // 20016016
  ADrawObject.APrevJobCode := ''; // 20013476
  ADrawObject.APrevJobSampleTimeMins := 0; // 20013476
  ADrawObject.ACurJobSampleTimeMins := 0; // 20013476
  ADrawObject.ACurrentSampleTimeMins := 0; // 20013476
  ADrawObject.AUseSampleTime := False; // 20013476
  ADrawObject.AWorkspotScale := WorkspotScale; // 20015406
  ADrawObject.AFontScale := FontScale; // 20015406
  ADrawObject.AEffPeriodMode := EffPeriodMode; // 20015406
  ADrawObject.AMachineWorkspotJobDifferent := False; // 20016447
  ADrawObject.AMachineDrawObject := nil; // 20016447
  ADrawObject.ACutOffTimeShiftDate := NullDate; // 20014450.50
  ADrawObject.ARealTimeIntervalMinutes := 5; // 20014450.50 // PIM-12 0 -> 5
  ADrawObject.ARecreateEmployeeDisplayList := True; // 20014450.50
  ADrawObject.AWorkspotTimeRec.ADepartmentCode :=
    CalculateTotalHoursDM.DetermineWorkspotDept(
      ADrawObject.APlantCode, ADrawObject.AWorkspotCode); // PIM-114
  with ADrawObject.AWorkspotTimeRec.AJobButton do
  begin
    Tag := ANewItemIndex;
    OnClick := AButtonChangeJobClick;
    OnMouseDown := AImageMouseDown;
    OnMouseMove := AImageMouseMove;
    OnMouseUp := AImageMouseUp;
    Parent := APnlDraw;
    Visible := False;
  end;
  ADrawObject.AWorkspotTimeRec.AInButton := TButton.Create(APnlDraw);
  with ADrawObject.AWorkspotTimeRec.AInButton do
  begin
    Tag := ANewItemIndex;
    OnClick := AButtonInClick;
    OnMouseDown := AImageMouseDown;
    OnMouseMove := AImageMouseMove;
    OnMouseUp := AImageMouseUp;
    Parent := APnlDraw;
    Caption := SPimsInButton;
    Visible := False;
  end;
  ADrawObject.AWorkspotTimeRec.AModeButton := TButton.Create(APnlDraw);
  with ADrawObject.AWorkspotTimeRec.AModeButton do
  begin
    Tag := ANewItemIndex;
    OnClick := AButtonChangeModeClick;
    OnMouseDown := AImageMouseDown;
    OnMouseMove := AImageMouseMove;
    OnMouseUp := AImageMouseUp;
    Parent := APnlDraw;
    case AEfficiencyPeriodMode of
    epCurrent: Caption := SPimsModeButtonCurrent;
    epSince:   Caption := SPimsModeButtonSince;
    epShift:   Caption := SPimsModeButtonShift;
    end;
    ShowHint := True;
    Visible := False;
  end;
  ADrawObject.AWorkspotTimeRec.APercentageLabel := TLabel.Create(APnlDraw);
  with ADrawObject.AWorkspotTimeRec.APercentageLabel do
  begin
    Tag := ANewItemIndex;
    OnMouseDown := AImageMouseDown;
    OnMouseMove := AImageMouseMove;
    OnMouseUp := AImageMouseUp;
    Parent := APnlDraw;
    Caption := '';
    Visible := False;
  end;
  ADrawObject.AWorkspotTimeRec.AActualNameLabel := TLabel.Create(APnlDraw);
  with ADrawObject.AWorkspotTimeRec.AActualNameLabel do
  begin
    Tag := ANewItemIndex;
    OnMouseDown := AImageMouseDown;
    OnMouseMove := AImageMouseMove;
    OnMouseUp := AImageMouseUp;
    Parent := APnlDraw;
    Caption := SPimsActual;
    Visible := False;
  end;
  ADrawObject.AWorkspotTimeRec.AActualValueLabel := TLabel.Create(APnlDraw);
  with ADrawObject.AWorkspotTimeRec.AActualValueLabel do
  begin
    Tag := ANewItemIndex;
    Alignment := taRightJustify;
    OnMouseDown := AImageMouseDown;
    OnMouseMove := AImageMouseMove;
    OnMouseUp := AImageMouseUp;
    Parent := APnlDraw;
    Caption := '';
    Visible := False;
  end;
  ADrawObject.AWorkspotTimeRec.ATargetNameLabel := TLabel.Create(APnlDraw);
  with ADrawObject.AWorkspotTimeRec.ATargetNameLabel do
  begin
    Tag := ANewItemIndex;
    OnMouseDown := AImageMouseDown;
    OnMouseMove := AImageMouseMove;
    OnMouseUp := AImageMouseUp;
    Parent := APnlDraw;
    Caption := SPimsTarget;
    Visible := False;
  end;
  ADrawObject.AWorkspotTimeRec.ATargetValueLabel := TLabel.Create(APnlDraw);
  with ADrawObject.AWorkspotTimeRec.ATargetValueLabel do
  begin
    Tag := ANewItemIndex;
    Alignment := taRightJustify;
    OnMouseDown := AImageMouseDown;
    OnMouseMove := AImageMouseMove;
    OnMouseUp := AImageMouseUp;
    Parent := APnlDraw;
    Caption := '';
    Visible := False;
  end;
  ADrawObject.AWorkspotTimeRec.ADiffNameLabel := TLabel.Create(APnlDraw);
  with ADrawObject.AWorkspotTimeRec.ADiffNameLabel do
  begin
    Tag := ANewItemIndex;
    OnMouseDown := AImageMouseDown;
    OnMouseMove := AImageMouseMove;
    OnMouseUp := AImageMouseUp;
    Parent := APnlDraw;
    Caption := SPimsExtra;
    Visible := False;
  end;
  ADrawObject.AWorkspotTimeRec.ADiffValueLabel := TLabel.Create(APnlDraw);
  with ADrawObject.AWorkspotTimeRec.ADiffValueLabel do
  begin
    Tag := ANewItemIndex;
    Alignment := taRightJustify;
    OnMouseDown := AImageMouseDown;
    OnMouseMove := AImageMouseMove;
    OnMouseUp := AImageMouseUp;
    Parent := APnlDraw;
    Caption := '';
    Visible := False;
  end;
  ADrawObject.AWorkspotTimeRec.ADiffLineHorz := ACreateShapeEx(stLineHorz);
  with ADrawObject.AWorkspotTimeRec.ADiffLineHorz do
  begin
    Visible := False;
  end;
end; // CreateWorkspot

// 20014450.50
procedure PDrawObject.CreateEmployee(AApplication: TApplication;
  AEmployeeRec: PEmployeeRec;
  AListButtonClick,  AGraphButtonClick: TNotifyEvent;
  AShowEmployeeListClick: TNotifyEvent;
  AImageMouseDown: TMouseEvent;
  AImageMousemove: TMouseMoveEvent; AImageMouseUp: TMouseEvent;
  APnlDraw: TPanel; AEfficiencyPeriodMode: TEffPeriodMode);
var
  ADrawObject: PDrawObject;
  AEmployeeDisplayRec: PTEmployeeDisplayRec;
  ThisTag: Integer;
begin
  ADrawObject := Self;

  if not Assigned(ADrawObject.AWorkspotTimeRec) then
    Exit;

  // PIM-12.3 Be sure this is all done before doing anything else in-between
  try
    //
  finally
    ThisTag := ADrawObject.AWorkspotTimeRec.AJobButton.Tag;

    new(AEmployeeDisplayRec);
    AEmployeeDisplayRec.EmployeeNumber := AEmployeeRec.EmployeeNumber;
    AEmployeeDisplayRec.EmployeeName := AEmployeeRec.EmployeeName;
    AEmployeeDisplayRec.AEmployeeNameLabel := TLabel.Create(APnlDraw);
    AEmployeeDisplayRec.JobCode := AEmployeeRec.JobCode;
    AEmployeeDisplayRec.JobDescription := AEmployeeRec.JobDescription;
    AEmployeeDisplayRec.DefaultFontColor := AEmployeeDisplayRec.AEmployeeNameLabel.Font.Color; // PIM-147
    AEmployeeDisplayRec.ToggleFontColor := clMyRed; // PIM-147
    // Name of the employee
    with AEmployeeDisplayRec.AEmployeeNameLabel do
    begin
      Parent := APnlDraw;
      OnClick := AShowEmployeeListClick;
      OnMouseDown := AImageMouseDown;
      OnMouseMove := AImageMouseMove;
      OnMouseUp := AImageMouseUp;
      Tag := ThisTag;
      Alignment := taLeftJustify;
      Caption := AEmployeeRec.EmployeeName;
    // 20014450.50 Do not show star
//    if AEmployeeRec.CurrentlyScannedIn then
//      Caption := Caption + ' *';
      Visible := False;
    end;
  // 20014450.50 Not used anymore

    // Worked Minutes (Worked Time) of employee
    AEmployeeDisplayRec.AWorkedMinutesLabel := TLabel.Create(Application);
    with AEmployeeDisplayRec.AWorkedMinutesLabel do
    begin
      Parent := APnlDraw;
      Tag := ThisTag;
      OnMouseDown := AImageMouseDown;
      OnMouseMove := AImageMouseMove;
      OnMouseUp := AImageMouseUp;
      Alignment := taLeftJustify;
      Visible := False;
(*
    case AEfficiencyPeriodMode of
    epCurrent: // Last 5-10 minutes
      // 20013476 What is shown here depends on sample_time_mins of job
      //          (previous and current).
      // 20014450.50
{      if ADrawObject.AUseSampleTime then
       begin
         // Only show it for the first employee
         if ADrawObject.AEmployeeDisplayList.Count = 0 then
           Caption :=
             Seconds2StringMinSec(Round(ADrawObject.NewSampleTimeSeconds));
       end
       else } // 20014450.50
      Caption :=
        IntMin2StringTime(Round(
          ADrawObject.ARealTimeIntervalMinutes * 60) {CurrentTimeWindow} div 60, True);
    epSince: // Whole day (show all)
      Caption :=
        IntMin2StringTime(Round(AEmployeeRec.Act_Time_Day) div 60, True);
    epShift: // Only current shift
      Caption :=
        IntMin2StringTime(Round(AEmployeeRec.Act_Time_Shift) div 60, True);
    end; // case
*)
    end; // with

  // 20014450.50
    AEmployeeDisplayRec.APercentage := 0;
    AEmployeeDisplayRec.APercentagePanel := TPanel.Create(APnlDraw);
    with AEmployeeDisplayRec.APercentagePanel do
    begin
      Parent := APnlDraw;
      OnClick := AShowEmployeeListClick;
      OnMouseDown := AImageMouseDown;
      OnMouseMove := AImageMouseMove;
      OnMouseUp := AImageMouseUp;
      Tag := ThisTag;
      Alignment := taRightJustify;
      Font.Color := clWhite;
      Caption := '150 %';
      Visible := False;
    end;
    AEmployeeDisplayRec.AListButton := TBitBtn.Create(APnlDraw);
    with AEmployeeDisplayRec.AListButton do
    begin
      Tag := ThisTag;
      MyTag := AEmployeeRec.EmployeeNumber;
      OnClick := AListButtonClick;
      OnMouseDown := AImageMouseDown;
      OnMouseMove := AImageMouseMove;
      OnMouseUp := AImageMouseUp;
      Parent := APnlDraw;
      Caption := ''; // L
//      Color := clPimsBlue; // PIM-250
      Glyph := nil;
      Init := False;
      Visible := False;
    end;
    AEmployeeDisplayRec.AGraphButton := TBitBtn.Create(APnlDraw);
    with AEmployeeDisplayRec.AGraphButton do
    begin
      Tag := ThisTag;
      MyTag := AEmployeeRec.EmployeeNumber;
      OnClick := AGraphButtonClick;
      OnMouseDown := AImageMouseDown;
      OnMouseMove := AImageMouseMove;
      OnMouseUp := AImageMouseUp;
      Parent := APnlDraw;
      Caption := ''; // G
//      Color := clPimsBlue; // PIM-250
      Glyph := nil;
      Init := False;
      Visible := False;
    end;
    ADrawObject.AEmployeeDisplayList.Add(AEmployeeDisplayRec);
  end; // finally
end; // CreateEmployee

function PDrawObject.FindEmployeeListIndex(
  AEmployeeNumber: Integer): Integer;
var
  ADrawObject: PDrawObject;
  I2: Integer;
  AEmployeeRec2: PEmployeeRec;
begin
  ADrawObject := Self;
  Result := -1; // Not found
  for I2 := 0 to ADrawObject.AEmployeeList.Count - 1 do
  begin
    AEmployeeRec2 := ADrawObject.AEmployeeList.Items[I2];
    if AEmployeeRec2.EmployeeNumber = AEmployeeNumber then
    begin
      Result := I2;
      Break;
    end;
  end;
end; // FindEmployeeListIndex

function PDrawObject.FindEmployeeDisplayListIndex(
  AEmployeeNumber: Integer): Integer;
var
  ADrawObject: PDrawObject;
  I2: Integer;
  AEmployeeDisplayRec2: PTEmployeeDisplayRec;
begin
  ADrawObject := Self;
  Result := -1; // Not found
  // PIM-12.3
  if not Assigned(ADrawObject.AEmployeeDisplayList) then
    Exit;
  // PIM-12.3 try-except added
  try
    for I2 := 0 to ADrawObject.AEmployeeDisplayList.Count - 1 do
    begin
      AEmployeeDisplayRec2 := ADrawObject.AEmployeeDisplayList.Items[I2];
      if AEmployeeDisplayRec2.EmployeeNumber = AEmployeeNumber then
      begin
        Result := I2;
        Break;
      end;
    end;
  except
    // Ignore error
  end;
end; // FindEmployeeDisplayListIndex

function PDrawObject.FindJobCodeListIndex(AJobCode: String): Integer;
var
  ADrawObject: PDrawObject;
  I2: Integer;
  AJobCodeRec2: PJobCodeRec;
begin
  ADrawObject := Self;
  Result := -1; // Not found
  for I2 := 0 to ADrawObject.AJobCodeList.Count - 1 do
  begin
    AJobCodeRec2 := ADrawObject.AJobCodeList.Items[I2];
    if AJobCodeRec2.JobCode = AJobCode then
    begin
      Result := I2;
      Break;
    end;
  end;
end; // FindJobCodeListIndex

// PIM-159
function PDrawObject.FindJobCodeListRec(AJobCode: String): PJobCodeRec;
var
  JobCodeIndex: Integer;
begin
  Result := nil;
  if Assigned(Self.AJobCodeList) then
  begin
    JobCodeIndex :=
      Self.FindJobCodeListIndex(AJobCode);
    if JobCodeIndex <> -1 then
      Result := Self.AJobCodeList.Items[JobCodeIndex];
  end;
end; // FindJobCodeListRec

procedure PDrawObject.InitEmployeeDisplayList(AFree: Boolean);
var
  I2: Integer;
  ADrawObject: PDrawObject;
  AEmployeeDisplayRec2: PTEmployeeDisplayRec;
begin
  ADrawObject := Self;
  // PIM-12.3
  try
    // First delete any old employee-labels
    if Assigned(ADrawObject.AEmployeeDisplayList) then
    begin
      if ADrawObject.AEmployeeDisplayList.Count > 0 then
      begin
        for I2 := ADrawObject.AEmployeeDisplayList.Count - 1 downto 0 do
        begin
          AEmployeeDisplayRec2 := ADrawObject.AEmployeeDisplayList.Items[I2];
          if Assigned(AEmployeeDisplayRec2.AEmployeeNameLabel) then
          begin
            AEmployeeDisplayRec2.AEmployeeNameLabel.Visible := False; // PIM-116
            AEmployeeDisplayRec2.AEmployeeNameLabel.Free;
          end;
          // 20014550.50
         if Assigned(AEmployeeDisplayRec2.AWorkedMinutesLabel) then
          begin
             AEmployeeDisplayRec2.AWorkedMinutesLabel.Visible := False; // PIM-116
           AEmployeeDisplayRec2.AWorkedMinutesLabel.Free;
           end;
          if Assigned(AEmployeeDisplayRec2.APercentagePanel) then
          begin
            AEmployeeDisplayRec2.APercentagePanel.Visible := False; // PIM-116
            AEmployeeDisplayRec2.APercentagePanel.Free;
          end;
          if Assigned(AEmployeeDisplayRec2.AListButton) then
          begin
            AEmployeeDisplayRec2.AListButton.OnClick := nil;
            AEmployeeDisplayRec2.AListButton.OnMouseDown := nil;
            AEmployeeDisplayRec2.AListButton.OnMouseMove := nil;
            AEmployeeDisplayRec2.AListButton.OnMouseUp := nil;
            AEmployeeDisplayRec2.AListButton.Visible := False; // PIM-116
            AEmployeeDisplayRec2.AListButton.Free;
          end;
          if Assigned(AEmployeeDisplayRec2.AGraphButton) then
          begin
            AEmployeeDisplayRec2.AGraphButton.OnClick := nil;
            AEmployeeDisplayRec2.AGraphButton.OnMouseDown := nil;
            AEmployeeDisplayRec2.AGraphButton.OnMouseMove := nil;
            AEmployeeDisplayRec2.AGraphButton.OnMouseUp := nil;
            AEmployeeDisplayRec2.AGraphButton.Visible := False; // PIM-116
            AEmployeeDisplayRec2.AGraphButton.Free;
          end;
          Dispose(AEmployeeDisplayRec2);
        end; // for
        ADrawObject.AEmployeeDisplayList.Clear;
      end; // if
      if AFree then
        ADrawObject.AEmployeeDisplayList.Free;
    end
    else
    begin
      ADrawObject.AEmployeeDisplayList := TList.Create;
    end;
    ADrawObject.ARecreateEmployeeDisplayList := False;
  except
    //
  end; 
end; // InitEmployeeDisplayList

// Memorize the countervalue that was read from black box:
// - Store it in memory for the current job.
procedure PDrawObject.MemorizeCounterValue(ADrawObject: PDrawObject;
  ABlackBoxRec: PBlackBoxRec);
var
  AJobCodeRec: PJobCodeRec;
  Qty: Integer;
  CurrentTime: TDateTime;
  TimeElapsed: Double;
//  Shift: Integer;
  function DetermineCurrentJob: PJobCodeRec;
  var
    I: Integer;
    AJobCodeRec2: PJobCodeRec;
    // PIM-159 Assign lastcountervalue of current job to nojob, to prevent
    //         it finds a quantity for nojob when no-one is scanned in anymore,
    //         based on the old value for lastcountervalue.
    procedure NoJobHandling;
    var
      NoJobJobCodeRec: PJobCodeRec;
    begin
      if Assigned(AJobCodeRec2) then
      begin
        NoJobJobCodeRec := ADrawObject.FindJobCodeListRec(NOJOB);
        if Assigned(NoJobJobCodeRec) then
          NoJobJobCodeRec.Last_Counter_Value := AJobCodeRec2.Last_Counter_Value;
      end;
    end; // NoJobHandling
  begin
    Result := nil;
    if Assigned(ADrawObject.AWorkspotTimeRec) then
    begin
      // Take current job
      for I := 0 to ADrawObject.AJobCodeList.Count - 1 do
      begin
        AJobCodeRec2 := ADrawObject.AJobCodeList[I];
        // Exception: When job is 'BREAKJOB' then still add the quantities.
        if AJobCodeRec2.JobCode = ADrawObject.AWorkspotTimeRec.ACurrentJobCode then
          if (AJobCodeRec2.InterfaceCode = ABlackBoxRec.InterfaceCode) or
            (AJobCodeRec2.JobCode = BREAKJOB) then
          begin
            Result := AJobCodeRec2;
            Break;
          end; // if
      end; // for
      if Assigned(Result) then // PIM-159
        NoJobHandling;
      // If there was no current job, then take NOJOB.
      // In case there are still quantities coming in, then they
      // are assigned to NOJOB.
      if not Assigned(Result) then
        Result := ADrawObject.FindJobCodeListRec(NOJOB);
      // IMPORTANT: When there is NO current job, do not look further,
      //            or it will add the qty to a job were no countings
      //            are done for.
{
      if Result = nil then
      begin
        // Take first job with same interface code
        for I := 0 to ADrawObject.AJobCodeList.Count - 1 do
        begin
          AJobCodeRec := ADrawObject.AJobCodeList[I];
          if AJobCodeRec.InterfaceCode = ABlackBoxRec.InterfaceCode then
          begin
            Result := AJobCodeRec;
          end; // if
        end; // for
      end; // if
      if Result = nil then
      begin
        // Take first job
        if ADrawObject.AJobCodeList.Count > 0 then
        begin
          AJobCodeRec := ADrawObject.AJobCodeList[0];
          Result := AJobCodeRec;
        end; // if
      end; // if
}
    end; // if
  end; // DetermineCurrentJob
  procedure HandleFifoList(AJobCodeRec: PJobCodeRec);
  var
    AFifoRec: PTFifoRec;
    procedure AddToFifoList;
    begin
      new(AFifoRec);
      AFifoRec.TimeStamp := CurrentTime;
      AFifoRec.Qty := Qty;
      AFifoRec.TimeElapsed := TimeElapsed;
(*
{$IFDEF DEBUG}
WLog('FifoList: Add:' + DateTimeToStr(AFifoRec.TimeStamp) +
';' + IntToStr(AFifoRec.Qty) + ';' + FloatToStr(AFifoRec.TimeElapsed));
{$ENDIF}
*)
      AJobCodeRec.Fifo_list.Add(AFifoRec);
    end; // AddToFifoList
  begin
    // ATTENTION: When NOJOB then do not add any qty!
    // Overruled: The NOJOB-qty MUST be added, so it will be stored
    // later in database.
//    if AJobCodeRec.JobCode = NOJOB then
//      Qty := 0;

    if not Assigned(AJobCodeRec.Fifo_list) then
      AJobCodeRec.Fifo_list := TList.Create;
    // Always add to Fifolist!
    AddToFifoList;
    // Only add to QtyToStoreList when Qty <> 0
    if Qty <> 0 then
      ADrawObject.AddElementToQtyToStoreList(Qty,
        AJobCodeRec.Last_Counter_Value);
    AJobCodeRec.RemovePartFromFifoList(CurrentTime);
  end; // HandleFifoList
  // Init Action: For the workspot: Do this only when the machine-counter
  //              is read for the first time.
  //              Look if the ActualCounterValue is the same as the
  //              LastCounterValue. If not, then assign the difference to
  //              job 'nojob' and set the 'lastcountervalue' to
  //              'actualcountervalue'.
  // Note: The NOJOB-job must already exist! Add them when data is read
  //       from database.
  function InitAction: Boolean;
    function AssignToNoJob: Boolean;
    var
      AJobCodeRec2: PJobCodeRec;
      NewQty: Integer;
    begin
      Result := False;
      // Look if there is already a current job, which can happen when
      // an employee was already scanned in, before the Personal Screen
      // was started. If so, then assign the current counter value to
      // this current job.
      AJobCodeRec2 := nil;
      if Assigned(ADrawObject.AWorkspotTimeRec) then
      begin
        if (ADrawObject.AWorkspotTimeRec.ACurrentJobCode <> '') and
          (ADrawObject.ACurrentNumberOfEmployees > 0) then
        begin
          AJobCodeRec2 := ADrawObject.
            FindJobCodeListRec(ADrawObject.AWorkspotTimeRec.ACurrentJobCode);
          AJobCodeRec2.Last_Counter_Value := ADrawObject.Last_Counter_Value;
          ADrawObject.ALastCounterReadTime := Now;
        end;
      end;
      // If there was no current job then use NOJOB.
      if not Assigned(AJobCodeRec2) then
      begin
        AJobCodeRec2 := ADrawObject.FindJobCodeListRec(NOJOB);
        if Assigned(AJobCodeRec2) then
        begin
          NewQty := ABlackBoxRec.ActualCounterValue -
            ADrawObject.Last_Counter_Value;
{
          // If there was a current job, then if NewQty is negative,
          // then assign use the actual counter value for NewQty.
          if AJobCodeRec2.JobCode <> NOJOB then
            if NewQty < 0 then
              NewQty := ABlackBoxRec.ActualCounterValue;
}
          AJobCodeRec2.Last_Counter_Value := ABlackBoxRec.ActualCounterValue;
          AJobCodeRec2.AddQtyDay(NewQty);
          Qty := NewQty; // Needed for HandleFifoList
          // Add to fifo list, so it wil be stored later.
          HandleFifoList(AJobCodeRec2);
          ADrawObject.ALastCounterReadTime := 0;
          Result := True;
(*
{$IFDEF DEBUG}
WLog('P=' + ADrawObject.APlantCode +
  ' W=' + ADrawObject.AWorkspotCode +
  ' J=' + AJobCodeRec2.JobCode +
  ' LastCounterValue=' + IntToStr(AJobCodeRec2.Last_Counter_Value) +
  ' Qty=' + IntToStr(NewQty)
  );
{$ENDIF}
*)
        end; // if
      end; // if
    end; // AssignToNoJob
  begin
(*
{$IFDEF DEBUG}
WLog('InitAction. ' +
  ' LastCounterValue=' + IntToStr(ADrawObject.Last_Counter_Value) +
  ' ActualCounterValue=' + IntToStr(ABlackBoxRec.ActualCounterValue)
  );
{$ENDIF}
*)
    Result := False;
    try
      if Assigned(ADrawObject.AWorkspotTimeRec) then
      begin
        if ADrawObject.Last_Counter_Value <>
          ABlackBoxRec.ActualCounterValue then
        begin
          // Assign difference to job 'NOJOB'
          CurrentTime := ABlackBoxRec.ActualCounterDateTime; // Needed for HandleFifoList
          Result := AssignToNoJob;
        end;
      end;
    finally
      ADrawObject.AInit := False;
    end;
  end; // InitAction
begin
  // Init -> Do this only once per workspot
  if ADrawObject.AInit then
    if InitAction then
      Exit;

  // Search for job(s) with same interface code.
  // Only do this for CURRENT JOB
  AJobCodeRec := DetermineCurrentJob;
  if Assigned(AJobCodeRec) then
  begin
//
// TODO: When there is no CV-record, it still uses the machine-counter as Qty
//       after an employee scans in! This is wrong!
//
    // If this was 0 then there was no last-counter-value in CV-record found
    // in which case we set the LastCounterValue for AJobCodeRec to
    // blackbox-value, otherwise it is counting the first machine-counter!
    if (ADrawObject.ALastCounterReadTime = 0) or ADrawObject.AJobChanged then
    begin
      AJobCodeRec.Last_Counter_Value := ABlackBoxRec.ActualCounterValue;
      ADrawObject.AJobChanged := False;
    end;

// TEST
{
WLog('P=' + ADrawObject.APlantCode +
  ' W=' + ADrawObject.AWorkspotCode +
  ' J=' + AJobCodeRec.JobCode + '(' + AJobCodeRec.JobCodeDescription + ')' +
  ' ActualCounterValue=' + IntToStr(ABlackBoxRec.ActualCounterValue) +
  ' LastCounterValue=' + IntToStr(AJobCodeRec.Last_Counter_Value));
}
    // Qty:
    // What has been counted?
    Qty := ABlackBoxRec.ActualCounterValue - AJobCodeRec.Last_Counter_Value;
    // If Qty is negative, it can mean there was no previous countervalue found,
    // in which case we set it to 0.
    if (ADrawObject.ALastCounterReadTime = 0) or (Qty < 0) then
      Qty := 0;

    //
    // TODO 21191
    // Filter out a value that is too high here!
    //
    if AJobCodeRec.JobCode <> NOJOB then
    begin
      if Qty > 10000 then
      begin
        try
          WLog('Skip ' +
            ' P=' + ADrawObject.APlantCode +
            ' W=' + ADrawObject.AWorkspotCode +
            ' J=' + AJobCodeRec.JobCode +
            ' Q=' + IntToStr(Qty) +
            ' ACV=' + IntToStr(ABlackBoxRec.ActualCounterValue) +
            ' LCV=' + IntToStr(AJobCodeRec.Last_Counter_Value)
            );
        except
        end;
        Qty := 0;
      end;
    end;

// TEST
{
WLog('Qty=' + IntToStr(Qty));
}
//      Qty := ABlackBoxRec.ActualCounterValue;
    AJobCodeRec.Last_Counter_Value := ABlackBoxRec.ActualCounterValue;
    // Time:
    CurrentTime := ABlackBoxRec.ActualCounterDateTime;
    // Time elapsed in seconds -> (DateTime2 - DateTime1) * DAY_IN_SECS
    // SPECIAL NOTE:
    // Example: When the CV is from an hour ago and the PS is restarted, it will
    // add an hour to the act_time_day, but there is already time added
    // based on scans. This can result in too many hours.
    if (ADrawObject.ALastCounterReadTime = 0) then
      TimeElapsed := 0
    else
      // Use Double for TimeElapsed, do not round anymore!
{      TimeElapsed := Round(
        ((CurrentTime - ADrawObject.ALastCounterReadTime) * DAY_IN_SECS) *
          ADrawObject.ACurrentNumberOfEmployees); }
      TimeElapsed :=
        ((CurrentTime - ADrawObject.ALastCounterReadTime) * DAY_IN_SECS) *
          ADrawObject.ACurrentNumberOfEmployees;
(*
{$IFDEF DEBUG}
WLog('ADrawObject.AWorkspotCode=' + ADrawObject.AWorkspotCode +
  ' ABlackBoxRec.InterfaceCode=' + ABlackBoxRec.InterfaceCode +
  ' ABlackBoxRec.ActualCounterDateTime=' + DateTimeToStr(ABlackBoxRec.ActualCounterDateTime) +
  ' CurrentTime=' + DateTimeToStr(CurrentTime) +
  ' ADrawObject.ALastCounterReadTime=' +
  DateTimeToStr(ADrawObject.ALastCounterReadTime) +
  ' TimeElapsed=' + FloatToStr(TimeElapsed) +
  ' ADrawObject.ACurrentNumberOfEmployees=' +
  IntToStr(ADrawObject.ACurrentNumberOfEmployees));
{$ENDIF}
*)
    ADrawObject.ALastCounterReadTime := CurrentTime;
    // Add Qty and Time
    // Job
    AJobCodeRec.AddQtyDay(Qty);
    AJobCodeRec.AddQtyShift(Qty);
    AJobCodeRec.AddQtyCurrent;
    AJobCodeRec.AddTimeDay(TimeElapsed);
    AJobCodeRec.AddTimeShift(TimeElapsed);
    AJobCodeRec.AddTimeCurrent(ADrawObject.ACurrentNumberOfEmployees);
    // Workspot
    ADrawObject.AddQtyDay(Qty);
    ADrawObject.AddQtyShift(Qty);
    ADrawObject.AddQtyCurrent;
    ADrawObject.AddTimeDay(TimeElapsed);
    ADrawObject.AddTimeDayEmployeeList(TimeElapsed);
    ADrawObject.AddTimeShift(TimeElapsed);
    ADrawObject.AddTimeShiftEmployeeList(TimeElapsed);
    ADrawObject.AddTimeCurrent;
    ADrawObject.AddTheoTimeDay(AJobCodeRec);
    ADrawObject.AddTheoTimeShift(AJobCodeRec);
//    ADrawObject.AddTheoTimeCurrent(AJobCodeRec); // PIM-12 this is not used anymore!
    ADrawObject.AddTheoQtyDay(AJobCodeRec);
    ADrawObject.AddTheoQtyShift(AJobCodeRec);
//    ADrawObject.AddTheoQtyCurrent(AJobCodeRec); // This is not used anymore!
    // PIM-12 Do this before showing efficiency, not here.
//    ADrawObject.DetermineTheoProdQtyCurrent;
//    ADrawObject.DetermineTheoTimeCurrent;
    // Handle Job FifoList
    HandleFifoList(AJobCodeRec);
    // Handle all jobs for fifolist for this drawobject
    ADrawObject.RemovePartFromFifoList;
  end // if
  else
  begin
    // When there were countings for jobs, then what to store?

    // Do this, to prevent it uses an old timestamp when an employee
    // later on scans-in to this workspot, resulting in a wrong
    // elapsed time for the employee.
    ADrawObject.ALastCounterReadTime := 0;
  end;
end; // MemorizeCounterValue

procedure PDrawObject.AddQtyDay(AQty: Integer);
begin
  Act_prodqty_day := Act_prodqty_day + AQty;
end; // AddQtyDay

procedure PDrawObject.AddQtyShift(AQty: Integer);
begin
  Act_prodqty_shift := Act_prodqty_shift + AQty;
end; // AddQtyShift

procedure PDrawObject.AddTimeDay(ATimeElapsed: Double);
begin
  Act_time_day := Act_time_day + ATimeElapsed;
end; // AddTimeDay

procedure PDrawObject.AddTimeShift(ATimeElapsed: Double);
begin
  Act_time_shift := Act_time_shift + ATimeElapsed;
end; // AddTimeShift

procedure PDrawObject.AddTimeDayEmployeeList(ATimeElapsed: Double);
var
  ADrawObject: PDrawObject;
  AEmployeeRec: PEmployeeRec;
  I: Integer;
begin
  ADrawObject := Self;
  for I := 0 to ADrawObject.AEmployeeList.Count - 1 do
  begin
    AEmployeeRec := ADrawObject.AEmployeeList.Items[I];
    if AEmployeeRec.CurrentlyScannedIn then
      if ADrawObject.ACurrentNumberOfEmployees > 0 then
        AEmployeeRec.AddTimeDay(
          ATimeElapsed / ADrawObject.ACurrentNumberOfEmployees);
  end; // for
end; // AddTimeDayEmployeeList

procedure PDrawObject.AddTimeShiftEmployeeList(ATimeElapsed: Double);
var
  ADrawObject: PDrawObject;
  AEmployeeRec: PEmployeeRec;
  I: Integer;
begin
  ADrawObject := Self;
  for I := 0 to ADrawObject.AEmployeeList.Count - 1 do
  begin
    AEmployeeRec := ADrawObject.AEmployeeList.Items[I];
    if AEmployeeRec.CurrentlyScannedIn then
      if ADrawObject.ACurrentNumberOfEmployees > 0 then
        AEmployeeRec.AddTimeShift(
          ATimeElapsed / ADrawObject.ACurrentNumberOfEmployees);
  end; // for
end; // AddTimeShiftEmployeeList

procedure PDrawObject.AddTheoTimeDay(var AJobCodeRec: PJobCodeRec);
var
  Old_JobCode_theo_time_day: Double;
begin
  // Today - Time - 'theo_time_day' is in seconds!
  if AJobCodeRec.Norm_prod_level <> 0 then
  begin
    Old_JobCode_theo_time_day := AJobCodeRec.Theo_time_day;
    if AJobCodeRec.Norm_prod_level <> 0 then
      AJobCodeRec.Theo_time_day :=
        AJobCodeRec.Act_prodqty_day /
          AJobCodeRec.Norm_prod_level * 60 * 60;
    Theo_time_day := Theo_time_day +
      (AJobCodeRec.Theo_time_day - Old_JobCode_theo_time_day);
  end
  else
    Theo_time_day := 0;
(*
WLog('Job=' + AJobCodeRec.JobCode +
  ' Old_JobCode_theo_time_day= ' + IntToStr(Old_JobCode_theo_time_day) +
  ' Act_time_day=' + IntToStr(AJobCodeRec.Act_time_day) +
  ' Theo_time_day=' + IntToStr(AJobCodeRec.Theo_time_day) +
  ' Norm_prod_level=' + IntToStr(AJobCodeRec.Norm_prod_level) +
  ' Job.Act_prodqty_day=' + IntToStr(AJobCodeRec.Act_prodqty_day) +
  ' Theo_time_day=' + IntToStr(Theo_time_day)
  );
*)
end; // AddTheoDay

procedure PDrawObject.AddTheoTimeShift(var AJobCodeRec: PJobCodeRec);
var
  Old_JobCode_theo_time_shift: Double;
begin
  // Shift - Time - 'theo_time_shift' is in seconds!
  if AJobCodeRec.Norm_prod_level <> 0 then
  begin
    Old_JobCode_theo_time_shift := AJobCodeRec.Theo_time_shift;
    if AJobCodeRec.Norm_prod_level <> 0 then
      AJobCodeRec.Theo_time_shift :=
        AJobCodeRec.Act_prodqty_shift /
          AJobCodeRec.Norm_prod_level * 60 * 60;
    Theo_time_shift := Theo_time_shift +
      (AJobCodeRec.Theo_time_shift - Old_JobCode_theo_time_shift);
  end
  else
    Theo_time_shift := 0;
end; // AddTheoShift

// PIM-12 Theo Time must be calculated here!
// PIM-12 This gives a wrong result!
procedure PDrawObject.DetermineTheoTimeCurrent;
var
  ADrawObject: PDrawObject;
  AJobCodeRec: PJobCodeRec;
  I: Integer;
  CurrentQty: Double;
  CurrentNrOfEmployees: Integer;
//  TimeElapsed: Double;
  // 20016016
  function ReceiveFromExternalApp: Boolean;
  begin
    Result := False;
    if Assigned(ADrawObject.AWorkspotTimeRec) then
      if ADrawObject.AWorkspotTimeRec.AReceiveQtyFromExternalApp then
        Result := True;
  end; // ReceiveFromExternalApp
  // PIM-12.2
  function LinkJobWorkspot: Boolean;
  begin
    Result := False;
    if Assigned(ADrawObject.AWorkspotTimeRec) then
      if ADrawObject.AWorkspotTimeRec.AIsLinkJobWorkspot then
        Result := True;
  end;
begin
  ADrawObject := Self;
  if LinkJobWorkspot then // PIM-12.2
    Exit;
  CurrentNrOfEmployees := ACurrentNumberOfEmployees;
  if CurrentNrOfEmployees <= 0 then
    CurrentNrOfEmployees := 1;
  ADrawObject.Theo_time_current := 0;
  if Assigned(ADrawObject.AJobCodeList) then
  begin
    for I := 0 to ADrawObject.AJobCodeList.Count - 1 do
    begin
      AJobCodeRec := ADrawObject.AJobCodeList.Items[I];
      AJobCodeRec.Theo_time_current := 0;
      if AJobCodeRec.Norm_prod_level <> 0 then
      begin
        CurrentQty := 0;
//        TimeElapsed := 0;
        // 20016016
        if ReceiveFromExternalApp then
        begin
          if (ADrawObject.AWorkspotTimeRec.ACurrentJobCode = AJobCodeRec.JobCode)
            {and (AJobCodeRec.JobCode <> NOJOB)} then // PIM-151
          begin
            CurrentQty := AJobCodeRec.Act_prodqty_current;
//            TimeElapsed := CurrentTimeWindow; // CURRENT_TIME_WINDOW; // 20014450.50
          end;
        end
        else
        begin
          CurrentQty := AJobCodeRec.SumOfFifoListCurrentQty;
//          TimeElapsed := AJobCodeRec.SumOfFifoListCurrentTimeElapsed;
        end;
(*
        // PIM-12
        // Theo-time-current will be 0 when there is no actual-qty yet, and
        // this gives a wrong percentaqe based on time!
        // ???
        AJobCodeRec.Theo_time_current := 0;
        if TimeElapsed > 0 then
        begin
          if CurrentQty > 0 then
            AJobCodeRec.Theo_time_current :=
              CurrentQty /
                AJobCodeRec.Norm_prod_level * 60 * 60 / CurrentNrOfEmployees
          else
            AJobCodeRec.Theo_time_current :=
              TimeElapsed;
//                AJobCodeRec.Norm_prod_level * 60 * 60 / CurrentNrOfEmployees;
        end;
*)
        if CurrentQty > 0 then
          AJobCodeRec.Theo_time_current :=
            CurrentQty /
              AJobCodeRec.Norm_prod_level * 60 * 60 / CurrentNrOfEmployees;
        Theo_time_current := Theo_time_current +
          AJobCodeRec.Theo_time_current;
        if Theo_time_current < 0 then
          Theo_time_current := Theo_time_current * -1;
{$IFDEF DEBUG}
  WLog('Job=' + AJobCodeRec.JobCode + '-' + AJobCodeRec.JobCodeDescription +
      ' NrOfEmp=' + IntToStr(CurrentNrOfEmployees) +
      ' Job.TheoQtyCur=' + IntToStr(Round(AJobCodeRec.Theo_prodqty_current)) +
      ' Job.TheoTimeCur=' + IntToStr(Round(AJobCodeRec.Theo_time_current)) +
      ' CurQty=' + IntToStr(Round(CurrentQty)) +
      ' Norm=' + IntToStr(Round(AJobCodeRec.Norm_prod_level)) +
      ' DrawObject.TheoTimeCur=' + IntToStr(Round(Theo_time_current))
      );
{$ENDIF}
      end;
    end; // for
  end; // if Assigned
end; // DetermineTheoTimeCurrent

procedure PDrawObject.AddTheoTimeCurrent(var AJobCodeRec: PJobCodeRec);
var
  Old_JobCode_theo_time_current: Double;
  CurrentNrOfEmployees: Integer;
begin
  Exit; // PIM-12 See DetermineTheoTimeCurrent!

  // Time is in seconds!
  // NOTE: When there is no norm for the job, then just set time to 0.
  // 20014450.50 For theo-time: Also use current-nr-of-employees in calculation.
  if AJobCodeRec.Norm_prod_level <> 0 then
  begin
    CurrentNrOfEmployees := Self.ACurrentNumberOfEmployees;
    if CurrentNrOfEmployees <= 0 then
      CurrentNrOfEmployees := 1;

    Old_JobCode_theo_time_current := AJobCodeRec.Theo_time_current;
    if AJobCodeRec.Norm_prod_level <> 0 then
      AJobCodeRec.Theo_time_current :=
        AJobCodeRec.Act_prodqty_current /
          AJobCodeRec.Norm_prod_level * 60 * 60 / CurrentNrOfEmployees;
    Theo_time_current := Theo_time_current +
      (AJobCodeRec.Theo_time_current - Old_JobCode_theo_time_current);
    if Theo_time_current < 0 then
      Theo_time_current := Theo_time_current * -1;
  end
  else
    Theo_time_current := 0;
end; // AddTheoTimeCurrent

procedure PDrawObject.AddTheoQtyDay(var AJobCodeRec: PJobCodeRec);
var
  Old_JobCode_theo_prodqty_day: Integer;
begin
  // Today - Qty - Note: Act_time_day is in secs!
  Old_JobCode_theo_prodqty_day := AJobCodeRec.Theo_prodqty_day;
  AJobCodeRec.Theo_prodqty_day :=
    Round((AJobCodeRec.Act_time_day / 60) * AJobCodeRec.Norm_prod_level / 60);
  Theo_prodqty_day := Theo_prodqty_day +
    (AJobCodeRec.Theo_prodqty_day - Old_JobCode_theo_prodqty_day);
{
WLog('Job=' + AJobCodeRec.JobCode +
  ' Old_Job_theo_prodqty_day=' + IntToStr(Old_JobCode_theo_prodqty_day) +
  ' Job.Act_time_day=' + FloatToStr(AJobCodeRec.Act_time_day) +
  ' Job.Norm_prod_level=' + IntToStr(AJobCodeRec.Norm_prod_level) +
  ' Job.Theo_prodqty_day=' + IntToStr(AJobCodeRec.Theo_prodqty_day) +
  ' Theo_prodqty_day=' + IntToStr(Theo_prodqty_day)
  );
}
end; // AddTheoQtyDay

procedure PDrawObject.AddTheoQtyShift(var AJobCodeRec: PJobCodeRec);
var
  Old_JobCode_theo_prodqty_shift: Integer;
begin
  // Shift - Qty - Note: Act_time_shift is in secs!
  Old_JobCode_theo_prodqty_shift := AJobCodeRec.Theo_prodqty_shift;
  AJobCodeRec.Theo_prodqty_shift :=
    Round((AJobCodeRec.Act_time_shift / 60) * AJobCodeRec.Norm_prod_level / 60);
  Theo_prodqty_shift := Theo_prodqty_shift +
    (AJobCodeRec.Theo_prodqty_shift - Old_JobCode_theo_prodqty_shift);
end; // AddTheoQtyShift

// TD-21162
// This is done calculated during: DetermineTheoProdQtyCurrent
procedure PDrawObject.AddTheoQtyCurrent(var AJobCodeRec: PJobCodeRec);
var
  Old_JobCode_theo_prodqty_current: Integer;
begin
  Exit; // See DetermineTheoProdQtyCurrent

  // Do nothing with NOJOB, because this can result in negative values.
  if (AJobCodeRec.JobCode = NOJOB) then
    Exit;
  // Time is in secs!
  // NOTE: When there is no norm, set it to 0.
  //
  // REMARK: This is now adding values based on jobs that were done during the
  // day. For TODAY/SHIFT this seems to be OK. But for CURRENT this is never
  // reset, so will be too high when there were multiple jobs.
  // -> It should only show the TARGET for jobs for the LAST 5 minutes.
  // Example for CURRENT:
  // 12:00: Job1 starts with norm 300. Target: 300/60 * 5 = 25
  // 12:02: Job2 starts with norm 300. Target: 300/60 * 5 = 25 + 25 = 50
  // 12:03: Job3 starts with norm 600. Target: 600/60 * 5 = 50 + 25 + 25 = 100
  // 12:05: Job3 ends. Target: 100 - 50 = 50 (when 5 minutes have past!).
  // 12:06: Job2 ends. Target: 50 - 25 = 25 (when 5 mnutes have past!).
  // 12:07: Target: 25 (for job1) (when 5 minutes have past!).
  // 12:09: Job1 ends. Target: 25 (for last 5 minutes)
  // 12:16: Target: 0 (last 5 minutes there were no jobs).
  //
  if AJobCodeRec.Norm_prod_level <> 0 then
  begin
    // Reset this value when current-has-passed?
{
    AJobCodeRec.SumOfFifoListCurrent(CurrentHasPassed);
    if CurrentHasPassed then
      Theo_prodqty_current := 0;
}

    // TD-21162 This gives a wrong Target!

    // TD-21162-Rework
    // Determine actual-time-current based on fifo-list ?
    // This is already done during Add_Time_current for job.
//    AJobCodeRec.Act_time_current := AJobCodeRec.SumOfFifoListCurrentTimeElapsed;

    Old_JobCode_theo_prodqty_current := AJobCodeRec.Theo_prodqty_current;
    AJobCodeRec.Theo_prodqty_current :=
      Round((AJobCodeRec.Act_time_current / 60) * AJobCodeRec.Norm_prod_level / 60);
//  For now, only show target for current job ? See 'remark' above.

    Theo_prodqty_current := Theo_prodqty_current +
      (AJobCodeRec.Theo_prodqty_current - Old_JobCode_theo_prodqty_current);

    // This only sets it to current job.
    // TD-21162 Do this:
{    AJobCodeRec.Theo_prodqty_current :=
      Round((AJobCodeRec.Act_time_current / 60) * AJobCodeRec.Norm_prod_level / 60);
    Theo_prodqty_current := AJobCodeRec.Theo_prodqty_current; }
{
WLog('Job=' + AJobCodeRec.JobCode +
  ' - ' + AJobCodeRec.JobCodeDescription +
  ' Old_Job_theo_prodqty_cur=' + IntToStr(Old_JobCode_theo_prodqty_current) +
  ' Job.Act_time_cur=' + FloatToStr(AJobCodeRec.Act_time_current) +
  ' Job.Norm_prod_level=' + IntToStr(AJobCodeRec.Norm_prod_level) +
  ' Job.Theo_prodqty_cur=' + IntToStr(AJobCodeRec.Theo_prodqty_current) +
  ' Theo_prodqty_cur=' + IntToStr(Theo_prodqty_current)
  );
}
  end
  else
    Theo_prodqty_current := 0;
end; // AddTheoQtyCurrent

// TD-21162
// Determine Theo_ProdQty_Current (Target) based on what is left
// in fifo-lists. This ensures the Target is calculated based on
// jobs that were done during the last x minutes (time_window)
// and also when it is (partly) past these x minutes.
// PIM-12.2 Skip the link-job-workspot, because that is handled in a
// different way.
procedure PDrawObject.DetermineTheoProdQtyCurrent;
var
  ADrawObject: PDrawObject;
  AJobCodeRec: PJobCodeRec;
  I: Integer;
  TimeElapsed: Double;
  // 20016016
  function ReceiveFromExternalApp: Boolean;
  begin
    Result := False;
    if Assigned(ADrawObject.AWorkspotTimeRec) then
      if ADrawObject.AWorkspotTimeRec.AReceiveQtyFromExternalApp then
        Result := True;
  end; // ReceiveFromExternalApp
  // PIM-12.2
  function LinkJobWorkspot: Boolean;
  begin
    Result := False;
    if Assigned(ADrawObject.AWorkspotTimeRec) then
      if ADrawObject.AWorkspotTimeRec.AIsLinkJobWorkspot then
        Result := True;
  end;
begin
  ADrawObject := Self;
  if LinkJobWorkspot then // PIM-12.2
    Exit;
  ADrawObject.Theo_prodqty_current := 0;
  if Assigned(ADrawObject.AJobCodeList) then
  begin
    for I := 0 to ADrawObject.AJobCodeList.Count - 1 do
    begin
      AJobCodeRec := ADrawObject.AJobCodeList.Items[I];
      if AJobCodeRec.Norm_prod_level <> 0 then
      begin
        TimeElapsed := 0;
        // 20013476
        // 20014450.50
{        if ADrawObject.AUseSampleTime then
        begin
          // 20016016
          if ReceiveFromExternalApp then
          begin
            if (ADrawObject.AWorkspotTimeRec.ACurrentJobCode = AJobCodeRec.JobCode) and
              (AJobCodeRec.JobCode <> NOJOB) then
              TimeElapsed := ADrawObject.NewSampleTimeSeconds;
          end
          else
            if AJobCodeRec.SumOfFifoListCurrentTimeElapsed > 0 then
              TimeElapsed := ADrawObject.NewSampleTimeSeconds;
        end
        else }
        begin
          // 20016016
          if ReceiveFromExternalApp then
          begin
            if (ADrawObject.AWorkspotTimeRec.ACurrentJobCode = AJobCodeRec.JobCode) and
              (AJobCodeRec.JobCode <> NOJOB) then
              TimeElapsed := CurrentTimeWindow; // CURRENT_TIME_WINDOW; // 20014450.50
          end
          else
            TimeElapsed := AJobCodeRec.SumOfFifoListCurrentTimeElapsed;
        end;
        AJobCodeRec.Theo_prodqty_current :=
          Round((TimeElapsed / 60) * AJobCodeRec.Norm_prod_level / 60);
        ADrawObject.Theo_prodqty_current :=
          ADrawObject.Theo_prodqty_current +
            AJobCodeRec.Theo_prodqty_current;
        // 20013476
        // 20014450.50
{        if ADrawObject.AUseSampleTime then
          if TimeElapsed <> 0 then
            Break; }
      end
      else
        AJobCodeRec.Theo_prodqty_current := 0;
    end;
  end;
end; // DetermineTheoProdQtyCurrent

procedure PDrawObject.AddQtyCurrent;
var
  AJobCodeRec: PJobCodeRec;
  I: Integer;
begin
  // When application has been restarted/refreshed, the qty-variable
  // contains the last quatity for last 5 minutes (CurrentTimeWindow).
  // After that time, it must not be used/shown in application, and
  // must only be based on 'sum-of-fifo-list'.
//  if IncSecond(ARefreshTimeStamp, 5) > Now then
  Act_prodqty_current := 0;
  if Assigned(AJobCodeList) then
    for I := 0 to AJobCodeList.Count - 1 do
    begin
      AJobCodeRec := AJobCodeList[I];
      // 20012858.90.
      // ATTENTION: Do not add the qty for NOJOB, to prevent
      // it shows a negative value for CURRENT.
      if AJobCodeRec.JobCode <> NOJOB then
        Act_prodqty_current := Act_prodqty_current +
          AJobCodeRec.SumOfFifoListCurrentQty;
    end; // for
end; // AddQtyCurrent

procedure PDrawObject.AddQtyCurrent(AQty: Integer);
begin
  Act_prodqty_current := Act_prodqty_current + AQty;
end; // AddQtyCurrent

procedure PDrawObject.AddTimeCurrent;
var
  AJobCodeRec: PJobCodeRec;
  I: Integer;
{  function MyNewSampleTimeSeconds: Integer;
  begin
    if ACurrentSampleTimeMins <> 0 then
      Result := ACurrentSampleTimeMins * 60
    else
      Result := CurrentTimeWindow;
  end; }
begin
  // This must be set to a fixed value!
  // TD-21162
//  Act_time_current := CurrentTimeWindow * ACurrentNumberOfEmployees;
  // Do not calculate!

  // TD-21162
  Act_time_current := 0;
  if Assigned(AJobCodeList) then
    for I := 0 to AJobCodeList.Count - 1 do
    begin
      AJobCodeRec := AJobCodeList[I];
      Act_time_current := Act_time_current +
        AJobCodeRec.Act_time_current;
    end;
  // TD-21162
  if Act_time_current > CurrentTimeWindow then // 20013476
    Act_time_current := CurrentTimeWindow;
end; // AddTimeCurrent

// Add an element to the QtyToStoreList
procedure PDrawObject.AddElementToQtyToStoreList(AQty,
  ACounterValue: Integer);
var
  ADrawObject: PDrawObject;
  AQtyToStoreRec: PQtyToStoreRec;
  NewJobCode: String;
  // Prevent it is stored again.
  function FindQtyToStoreRec: Boolean;
  var
    I: Integer;
  begin
    Result := False;
    for I := 0 to ADrawObject.QtyToStoreList.Count - 1 do
    begin
      AQtyToStoreRec := ADrawObject.QtyToStoreList[I];
      if (AQtyToStoreRec.JobCode = NewJobCode) and
        (AQtyToStoreRec.ShiftNumber =
         ADrawObject.AWorkspotTimeRec.ACurrentShiftNumber) and
        (AQtyToStoreRec.Quantity = AQty) and
        (AQtyToStoreRec.Countervalue = ACounterValue) then
      begin
        Result := True;
        Break;
      end; // if
    end; // for
  end; // FindQtyToStoreRec
begin
  ADrawObject := Self;
  if not Assigned(ADrawObject.QtyToStoreList) then
    ADrawObject.QtyToStoreList := TList.Create;
  if AQty <> 0 then
  begin
    if Assigned(ADrawObject.AWorkspotTimeRec) then
    begin
      if (ADrawObject.AWorkspotTimeRec.ACurrentJobCode <> '') and
         (ADrawObject.AWorkspotTimeRec.ACurrentJobCode <> NOJOB) then
        NewJobCode := ADrawObject.AWorkspotTimeRec.ACurrentJobCode
      else
        NewJobCode := NOJOB;
      if (not FindQtyToStoreRec) then
      begin
        new(AQtyToStoreRec);
        AQtyToStoreRec.Timestamp := Now;
        AQtyToStoreRec.JobCode := NewJobCode;
        AQtyToStoreRec.ShiftNumber :=
          ADrawObject.AWorkspotTimeRec.ACurrentShiftNumber;
        AQtyToStoreRec.Quantity := AQty;
        AQtyToStoreRec.Countervalue := ACounterValue;
        ADrawObject.QtyToStoreList.Add(AQtyToStoreRec);
      end; // if
    end; // if
  end; // if
end; // AddElementToQtyToStoreList

procedure PDrawObject.WriteQtyToStoreListToFile(ALogPath: String);
var
  TempList: TStringList;
  I: Integer;
  ADrawObject: PDrawObject;
  AQtyToStoreRec: PQtyToStoreRec;
  Line: String;
  MyFileName: String;
  // 20013489
  // Use GetProdDay to get ShiftDate, for this, info is needed about:
  // - AEmployeeNumber (from EmployeeData.EmployeeCode)
  // - AScanStart (from EmployeeData.DateIn)
  // - AScanEnd (from EmployeeData.DateOut)
  // - APlantCode (from EmployeeData.PlantCode)
  // - AShiftNumber (from EmployeeData.ShiftNumber)
  // - ADepartmentCode (from EmployeeData.DepartmentCode)
  function DetermineShiftDate(ADrawObject: PDrawObject): TDateTime;
  var
    AEmployeeRec: PEmployeeRec;
    AIDCard: TScannedIDCard;
    J: Integer;
    StartDate, EndDate: TDateTime;
    function DetermineWorkspotDept(ADrawObject: PDrawObject): String;
    begin
      Result := CalculateTotalHoursDM.DetermineWorkspotDept(
        ADrawObject.APlantCode, ADrawObject.AWorkspotCode);
    end; // DetermineWorkspotDept
  begin
    Result := Nulldate;
    try
      if Assigned(ADrawObject.AEmployeeList) then
      begin
        // Take shiftdate only from first found employee.
        for J := 0 to ADrawObject.AEmployeeList.Count - 1 do
        begin
          AEmployeeRec := ADrawObject.AEmployeeList[J];
          if AEmployeeRec.CurrentlyScannedIn then
          begin
            AIDCard.EmployeeCode := AEmployeeRec.EmployeeNumber;
            AIDCard.DateIn := AEmployeeRec.DateTimeIn;
            AIDCard.DateOut := AEmployeeRec.DateTimeOut;
            AIDCard.PlantCode := ADrawObject.APlantCode;
            AIDCard.ShiftNumber := AEmployeeRec.ShiftNumber;
            AIDCard.DepartmentCode := DetermineWorkspotDept(ADrawObject);
            AProdMinClass.GetShiftDay(AIDCard, StartDate, EndDate);
            Result := Trunc(StartDate);
            // 20013489 Keep ShiftDate in ADrawObject, to know the ShiftDate
            //          when no-one is scanned anymore, but when quantities must
            //          still be stored in database.
            ADrawObject.LastShiftDate := Result;
{$IFDEF DEBUG2}
if Result <> NullDate then
  WLog('DetermineShiftDate: ' + DateToStr(ADrawObject.LastShiftDate))
else
  WLog('DetermineShiftDate: NULL');
{$ENDIF}
            Break;
          end;
        end;
      end;
    except
      Result := NullDate;
    end;
  end; // DetermineShiftDate
begin
  ADrawObject := Self;
  if not Assigned(ADrawObject.AWorkspotTimeRec) then
    Exit;
  if not Assigned(ADrawObject.QtyToStoreList) then
    Exit;
  MyFileName := ALogPath +
    ORASystemDM.CurrentComputerName + '_' + QTYTOSTOREFILENAME;
  TempList := TStringList.Create;
  // Load any existing file
  if FileExists(MyFileName) then
    try // PIM-42 Trap error
      TempList.LoadFromFile(MyFileName);
    except
      on E: EInOutError do
        WErrorLog(E.Message + ' (Error during WriteQtyToStoreList)');
      on E:Exception do
        WErrorLog(E.Message + ' (Error during WriteQtyToStoreList)');
    end;
  // Append
  try
    for I := 0 to ADrawObject.QtyToStoreList.Count - 1 do
    begin
      AQtyToStoreRec := ADrawObject.QtyToStoreList.Items[I];
      if ORASystemDM.UseShiftDateSystem then
      begin
        // 20013489.
        // Assign ShiftDate here to record.
        try
          AQtyToStoreRec.ShiftDate := DetermineShiftDate(ADrawObject);
        except
          AQtyToStoreRec.ShiftDate := Trunc(AQtyToStoreRec.TimeStamp);
        end;
      end
      else
        AQtyToStoreRec.ShiftDate := Trunc(AQtyToStoreRec.TimeStamp);
      Line :=
        FormatDateTime('dd-mm-yyyy hh:nn:ss', AQtyToStoreRec.TimeStamp) + ';' +
        ADrawObject.APlantCode + ';' +
        ADrawObject.AWorkspotCode + ';' +
        AQtyToStoreRec.JobCode + ';' +
        IntToStr(AQtyToStoreRec.ShiftNumber) + ';' +
        IntToStr(AQtyToStoreRec.Quantity) + ';' +
        IntToStr(AQtyToStoreRec.Countervalue) + ';' +
        FormatDateTime('dd-mm-yyyy', AQtyToStoreRec.ShiftDate);

{$IFDEF DEBUG2}
  WLog('WriteToFile: ' + Line);
{$ENDIF}

      TempList.Add(Line);
    end; // for
    try  // PIM-42 Trap error
      TempList.SaveToFile(MyFileName);
    except
      on E: EInOutError do
        WErrorLog(E.Message + ' (Error during WriteQtyToStoreList)');
      on E:Exception do
        WErrorLog(E.Message + ' (Error during WriteQtyToStoreList)');
    end;
  finally
    TempList.Free;
  end;
end; // WriteQtyToStoreListToFile

procedure PDrawObject.InitQtyToStoreList(AFree: Boolean);
var
  ADrawObject: PDrawObject;
  AQtyToStoreRec: PQtyToStoreRec;
  I: Integer;
begin
  ADrawObject := Self;
  if Assigned(ADrawObject.QtyToStoreList) then
  begin
    for I := ADrawObject.QtyToStoreList.Count - 1 downto 0 do
    begin
      AQtyToStoreRec := ADrawObject.QtyToStoreList.Items[I];
      Dispose(AQtyToStoreRec);
    end;
    ADrawObject.QtyToStoreList.Clear;
    if AFree then
      ADrawObject.QtyToStoreList.Free;
  end; // if
end; // InitQtyToStoreList

// Add/Update Employee List, based on scans for current ADrawObject
// - Note: The employee should probably never be deleted from this list,
//         because it should be visible for 'today'.
procedure PDrawObject.AddUpdateEmployeeList(AEmployeeNumber: Integer;
  AEmployeeName: String; AShiftNumber: Integer; ATeamCode, ALevel: String;
  ADateTimeIn, ADateTimeOut: TDateTime; AProcessedYN: String;
  AJobCode, AJobCodeDescription: String;
  AReadAll: Boolean;
  AVisible: Boolean=True
{  ADeleteWhenProcessed: Boolean=False} // Is this needed?
  );
var
  ADrawObject: PDrawObject;
  AEmployeeRec: PEmployeeRec;
  AJobCodeRec: PJobCodeRec;
  EmployeeIndex: Integer;
  NewDateTimeOut: TDateTime;
  ProdMinutes: Integer;
  function ComputeTime(APlantCode: String;
    AEmployeeNumber: Integer; AWorkspotCode: String;
    AShiftNumber: Integer; ADateTimeIn, ADateTimeOut: TDateTime): Integer;
  var
    EmployeeData: TScannedIDCard;
    STime, ETime: TDateTime;
    ProdMin, BreaksMin, PayedBreaks: Integer;
  begin
    // Compute breaks
    EmployeeData.PlantCode := APlantCode;
    EmployeeData.EmployeeCode := AEmployeeNumber;
    EmployeeData.WorkSpotCode := AWorkspotCode;
    EmployeeData.ShiftNumber := AShiftNumber;
    STime := ADateTimeIn;
    ETime := ADateTimeOut;
    // Value is NULL
    // Or in FUTURE? MR:02-06-2003
    if (ETime = 0) or (ETime > Now) then
      ETime := Now;
    ProdMin := 0;
    AProdMinClass.ComputeBreaks(
      EmployeeData,
      STime,
      ETime,
      1,
      (* var *) ProdMin, BreaksMin, PayedBreaks);
    if ORASystemDM.IgnoreBreaks then // 20015346
      Result := ProdMin + BreaksMin // Time with breaks
    else
      // Time without breaks!
      Result := ProdMin;
  end; // ComputeTime
  // Check if there are still employees scanned in, if not
  // then there is no current job anymore.
  procedure CheckCurrentJobNotScannedIn;
  var
    I: Integer;
    AEmployeeRec: PEmployeeRec;
    ScannedInCount: Integer;
  begin
    ScannedInCount := 0;
    for I := 0 to ADrawObject.AEmployeeList.Count - 1 do
    begin
      AEmployeeRec := ADrawObject.AEmployeeList[I];
      if AEmployeeRec.CurrentlyScannedIn then
      begin
        inc(ScannedInCount);
        Break;
      end; // if
    end; // for
    if ScannedInCount = 0 then
      if Assigned(ADrawObject.AWorkspotTimeRec) then
      begin
        ADrawObject.AWorkspotTimeRec.ACurrentJobCode := '';
        ADrawObject.AWorkspotTimeRec.ACurrentJobDescription := '';
        // Do not reset the shift.
//        ADrawObject.AWorkspotTimeRec.ACurrentShiftNumber := 0;
      end; // if
  end; // CheckCurrentJobNotScannedIn
  // Re(count) the current number of employees:
  // - These are the employee who are scanned in at the moment
  //   at the workspot.
  procedure DefineCurrentNumberOfEmployees;
  var
    I: Integer;
    AEmployeeRec: PEmployeeRec;
  begin
    ADrawObject.ACurrentNumberOfEmployees := 0;
    for I := 0 to ADrawObject.AEmployeeList.Count - 1 do
    begin
      AEmployeeRec := ADrawObject.AEmployeeList[I];
      if AEmployeeRec.CurrentlyScannedIn then
      begin
        ADrawObject.ACurrentNumberOfEmployees :=
          ADrawObject.ACurrentNumberOfEmployees + 1;
      end; // if
    end; // for
  end; // DefineCurrentNumberOfEmployees.
begin
  ADrawObject := Self;
  if not Assigned(ADrawObject.AEmployeeList) then
    ADrawObject.AEmployeeList := TList.Create;
  EmployeeIndex :=
    ADrawObject.FindEmployeeListIndex(AEmployeeNumber);
  if EmployeeIndex = -1 then // Not found
  begin
    // New record
    AEmployeeRec := PEmployeeRec.Create;
    AEmployeeRec.EmployeeNumber := AEmployeeNumber;
    AEmployeeRec.EmployeeName := AEmployeeName;
    AEmployeeRec.ShiftNumber := AShiftNumber;
    AEmployeeRec.TeamCode := ATeamCode;
    AEmployeeRec.Level := ALevel;
    AEmployeeRec.Visible := AVisible; // 20013379
    AEmployeeRec.DateTimeIn := ADateTimeIn;
    AEmployeeRec.JobCode := AJobCode;
    AEmployeeRec.JobDescription := AJobCodeDescription;
    if (AProcessedYN = 'Y') then // 20014450.50
      AEmployeeRec.DateTimeOut := ADateTimeOut
    else
      AEmployeeRec.DateTimeOut := 0;
    NewDateTimeOut := AEmployeeRec.DateTimeOut;
    AEmployeeRec.CurrentlyScannedIn := (AProcessedYN = 'N');
    if AEmployeeRec.CurrentlyScannedIn then
    begin
      // Calculated values for new employee: 1 employee is added.
      ADrawObject.ACurrentNumberOfEmployees :=
        ADrawObject.ACurrentNumberOfEmployees + 1;
    end;
    ADrawObject.AEmployeeList.Add(AEmployeeRec);
  end
  else
  begin
    // Existing record
    // -> job can change!
    // -> shift can change!
    // Add time to 'act_time_day' and 'act_time_shift',
    // based on scan that was found.
    AEmployeeRec := ADrawObject.AEmployeeList[EmployeeIndex];
    if (AProcessedYN = 'Y') then
      AEmployeeRec.DateTimeOut := ADateTimeOut
    else
    begin
      AEmployeeRec.DateTimeOut := 0;
      AEmployeeRec.JobCode := AJobCode; // PIM-147
      AEmployeeRec.JobDescription := AJobCodeDescription; // PIM-147
    end;
    NewDateTimeOut := AEmployeeRec.DateTimeOut;
    // During readall, this must not be done, or it will change this
    // when reading a closed (older) scan for the same employee.
    if not AReadAll then
      AEmployeeRec.CurrentlyScannedIn := (AProcessedYN = 'N');
    AEmployeeRec.ShiftNumber := AShiftNumber;
  end;
  // Calculated values
  // Workspot-level
  if Assigned(ADrawObject.AWorkspotTimeRec) then
  begin
    // Initialising of current shift:
    // When 'currentshiftnumber' is 0 then assign it, even if the
    // employee is not scanned in. If this is not done, then
    // calculation of shift time will go wrong.
    //
    // NOTE: Current shift must be based on OPEN scan during scanning.
    //       During ReadAll always take the first found shift (from latest scan)
    //       if it was open or not, as current shift. This ensures time/qty
    //       are calculated during ReadAll.
    //
    if AReadAll then
      if ADrawObject.AWorkspotTimeRec.ACurrentShiftNumber = 0 then
        ADrawObject.AWorkspotTimeRec.ACurrentShiftNumber := AShiftNumber;
    // Current job/shift is based on open scan
    ADrawObject.AUseSampleTime := False; // 20013476
    if (AProcessedYN = 'N') then
    begin
      // PIM-147 When multiple employees are scanned in on same workspot:
      //         Do not change the workspot-job when 1 employee changes job!
      //         We must determine this in a different way, via the employee list
      //         and by looking for the most used job for that workspot.
      //         Exception: When there is only 1 employee, then just take
      //         that job as workspot-job.
      DefineCurrentNumberOfEmployees;
      if ADrawObject.ACurrentNumberOfEmployees = 1 then
      begin
        ADrawObject.AWorkspotTimeRec.ACurrentJobCode := AJobCode;
        ADrawObject.AWorkspotTimeRec.ACurrentJobDescription := AJobCodeDescription;
      end
      else
        ADrawObject.DetermineCurrentJobByEmployeeList;

      // 20013476
//      AJobCodeRec := ADrawObject.FindJobCodeListRec(AJobCode);
//      ADrawObject.ACurJobSampleTimeMins := AJobCodeRec.Sample_time_mins; // 20014450.50
//      ADrawObject.AUseSampleTime := (AJobCodeRec.Sample_time_mins <> 0); // 20014450.50
      if AReadAll then
      begin
        if ADrawObject.AWorkspotTimeRec.ACurrentShiftNumber <> AShiftNumber then
        begin
          ADrawObject.ResetShiftCounters;
          ADrawObject.AWorkspotTimeRec.ACurrentShiftNumber := AShiftNumber;
        end;
      end
      else
        ADrawObject.AWorkspotTimeRec.ACurrentShiftNumber := AShiftNumber;
    end; // if open scan
    // Handling of datetime-in (start of current shift).
    // The shift-datetimeIn should be based on start-time of the current shift.
    // During readall this can be assigned.
    // Note: This value is not used yet.
    if AReadAll then
    begin
      if ADrawObject.AWorkspotTimeRec.ACurrentShiftNumber = AShiftNumber then
      begin
        ADrawObject.AWorkspotTimeRec.ACurrentShiftStart := ADateTimeIn;
      end;
    end
    else
    begin
      // Upon shift change, keep track of start-date-of-shift.
      if ADrawObject.AWorkspotTimeRec.ACurrentShiftNumber <> AShiftNumber then
      begin
        ADrawObject.AWorkspotTimeRec.ACurrentShiftStart := ADateTimeIn;
      end;
    end; // if readall
  end; // if Assigned
  // Do this only during 'readall', not when an employee is currently
  // scanning in/out or it will add again time for the previous scan!
  if AReadAll then
  begin
    // Compute Actual time
    ProdMinutes := ComputeTime(ADrawObject.APlantCode,
      AEmployeeNumber,
      ADrawObject.AWorkspotCode,
      AShiftNumber,
      ADateTimeIn,
      NewDateTimeOut);
{$IFDEF DEBUG2}
WLog(
  'WS=' + ADrawObject.AWorkspotCode +
  ' Emp=' + IntToStr(AEmployeeNumber) +
  ' ProdMinutes=' + IntToStr(ProdMinutes)
  );
{$ENDIF}
    // Act_Time_xxx is in secs!
    AEmployeeRec.AddTimeDay(ProdMinutes * 60);
    if Assigned(ADrawObject.AWorkspotTimeRec) then
      if ADrawObject.AWorkspotTimeRec.ACurrentShiftNumber = AShiftNumber then
        AEmployeeRec.AddTimeShift(ProdMinutes * 60);
    // Fill values on jobcode-level.
    AJobCodeRec :=
      ADrawObject.FindJobCodeListRec(AJobCode);
    if Assigned(AJobCodeRec) then
    begin
      // Act_Time_xxx is in secs!
      AJobCodeRec.AddTimeDay(ProdMinutes * 60);
      ADrawObject.AddTimeDay(ProdMinutes * 60);
      // Also calculate theo qty/time day!
      ADrawObject.AddTheoQtyDay(AJobCoderec);
      ADrawObject.AddTheoTimeDay(AJobCodeRec);
      // Add only for current shift
      if ADrawObject.AWorkspotTimeRec.ACurrentShiftNumber = AShiftNumber then
      begin
        AJobCodeRec.AddTimeShift(ProdMinutes * 60);
        ADrawObject.AddTimeShift(ProdMinutes * 60);
        // Also calculate theo qty/time shift!
        ADrawObject.AddTheoQtyShift(AJobCodeRec);
        ADrawObject.AddTheoTimeShift(AJobCodeRec);
      end;
      // 20015178.70
      // Also add qty/time for current here
      // 20016016
      if not ADrawObject.AWorkspotTimeRec.AReceiveQtyFromExternalApp then
        ADrawObject.AddQtyCurrent;
      ADrawObject.AddTimeCurrent;
//      ADrawObject.AddTheoQtyCurrent(AJobCodeRec); // This is not used anymore!
//      ADrawObject.AddTheoTimeCurrent(AJobCodeRec); // PIM-12 This is not used anymore!
      DefineCurrentNumberOfEmployees; // PIM-12 Bugfix: Be sure the nr is correct.
      // PIM-12 Do this before showing efficiency, not here.
//      ADrawObject.DetermineTheoProdQtyCurrent;
//      ADrawObject.DetermineTheoTimeCurrent; // PIM-12
    end; // if
  end // if AReadAll
  else
  begin
    CheckCurrentJobNotScannedIn;
  end;
  DefineMultipleShiftsYN;
  DefineCurrentNumberOfEmployees;
  // Employee has scanned out (end-of-day) -> Delete from list
  // after hours are added to the last job of employee.
  // NOTE: This is probably never needed, because employees should always be
  // visible for 'today'.
{
  if ADeleteWhenProcessed and (AProcessedYN = 'Y') then
  begin
    if Assigned(AEmployeeRec) then
    begin
      ADrawObject.AEmployeeList.Remove(AEmployeeRec);
      Dispose(AEmployeeRec);
    end;
  end;
}
end; // AddUpdateEmployeeList

procedure PDrawObject.DefineMultipleShiftsYN;
var
  ADrawObject: PDrawObject;
  AEmployeeRec: PEmployeeRec;
  I: Integer;
begin
  ADrawObject := Self;
  if Assigned(ADrawObject.AEmployeeList) then
  begin
    ADrawObject.AWorkspotTimeRec.AMultipleShifts := False;
    for I := 0 to ADrawObject.AEmployeeList.Count - 1 do
    begin
      AEmployeeRec := ADrawObject.AEmployeeList[I];
      if AEmployeeRec.CurrentlyScannedIn then
        if ADrawObject.AWorkspotTimeRec.ACurrentShiftNumber <>
          AEmployeeRec.ShiftNumber then
        begin
          ADrawObject.AWorkspotTimeRec.AMultipleShifts := True;
          Break;
        end; // if
    end; // for
  end; // if
end; // DefineMultipleShiftsYN

// Remove part from fifo list for all jobs belonging to drawobject.
procedure PDrawObject.RemovePartFromFifoList;
var
  ADrawObject: PDrawObject;
  AJobCodeRec: PJobCodeRec;
  I: Integer;
  CurrentTime: TDateTime;
begin
  ADrawObject := Self;
  CurrentTime := Now;
  if Assigned(ADrawObject.AJobCodeList) then
  begin
    for I := 0 to ADrawObject.AJobCodeList.Count - 1 do
    begin
      AJobCodeRec := ADrawObject.AJobCodeList[I];
      if AJobCOdeRec.Norm_prod_level <> 0 then
        AJobCodeRec.RemovePartFromFifoList(CurrentTime);
    end;
  end;
end; // RemovePartFromFifoList

// Create a copy of this object.
// For a 'deep copy', the target must first be created!
function PJobCodeRec.Copy(AJobCodeRec: PJobCodeRec): PJobCodeRec;
var
  JobCodeRec: PJobCodeRec;
begin
  JobCodeRec := Self;
  if Assigned(AJobCodeRec) then
  begin
    JobCodeRec.JobCode := AJobCodeRec.JobCode;
    JobCodeRec.JobCodeDescription := AJobCodeRec.JobCodeDescription;
    JobCodeRec.InterfaceCode := AJobCodeRec.InterfaceCode;
    JobCodeRec.Norm_prod_level := AJobCodeRec.Norm_prod_level;
    JobCodeRec.Last_Counter_Value := AJobCodeRec.Last_Counter_Value;
    JobCodeRec.Act_prodqty_day := AJobCodeRec.Act_prodqty_day;
    JobCodeRec.Act_prodqty_shift := AJobCodeRec.Act_prodqty_shift;
    JobCodeRec.Act_prodqty_current := AJobCodeRec.Act_prodqty_current;
    JobCodeRec.Act_time_day := AJobCodeRec.Act_time_day;
    JobCodeRec.Act_time_shift := AJobCodeRec.Act_time_shift;
    JobCodeRec.Act_time_current := AJobCodeRec.Act_time_current;
    JobCodeRec.Sample_time_mins := AJobCodeRec.Sample_time_mins;
    JobCodeRec.ARealTimeIntervalMinutes := AJobCodeRec.ARealTimeIntervalMinutes; // 20014450.50
  end;
  Result := JobCodeRec;
end;

// 20013476 // PIM-12
(*
function PJobCodeRec.SampleTimeSeconds: Integer;
begin
{  if Sample_time_mins <> 0 then
    Result := Sample_time_mins * 60
  else
    Result := CurrentTimeWindow; } // 20014450.50
  Result := CurrentTimeWindow; // 20014450.50
end;
*)
{ PEmployeeRec }

procedure PEmployeeRec.AddTimeDay(ATimeElapsed: Double);
begin
  Act_time_day := Act_time_day + ATimeElapsed;
end; // AddTimeDay

procedure PEmployeeRec.AddTimeShift(ATimeElapsed: Double);
begin
  Act_time_shift := Act_time_shift + ATimeElapsed;
end; // AddTimeShift

constructor PEmployeeRec.Create;
begin
  inherited Create;
  EmployeeNumber := 0;
  EmployeeName := '';
  ShiftNumber := 0;
  TeamCode := '';
  Level := '';
  DateTimeIn := 0;
  DateTimeOut := 0;
  CurrentlyScannedIn := False;
  PlannedWorkspot := '';
  PlannedStartTime := 0;
  PlannedEndTime := 0;
  PlannedLevel := '';
  Act_Time_Day := 0;
  Act_Time_Shift := 0;
  Visible := True; // 20013379
  JobCode := ''; // PIM-147
  JobDescription := ''; // PIM-147
end; // Create

destructor PEmployeeRec.Destroy;
begin
  inherited;
end; // Destroy

procedure RecreateReadCounterList(APSList: TList; var AReadCounterList: TList);
var
  I, J: Integer;
  ADrawObject: PDrawObject;
  ABlackBoxRec: PBlackBoxRec;
  AReadCounterRec: PReadCounterRec;
begin
  try
    ClearReadCounterList(AReadCounterList);
    for I := 0 to APSList.Count - 1 do
    begin
      ADrawObject := APSList.Items[I];
      if Assigned(ADrawObject.ABlackBoxList) then
      begin
        for J := 0 to ADrawObject.ABlackBoxList.Count - 1 do
        begin
          ABlackBoxRec := ADrawObject.ABlackBoxList.Items[J];
          // Add this to ReadCounterList
          new(AReadCounterRec);
          AReadCounterRec.DrawObject := ADrawObject;
          AReadCounterRec.BlackBoxRec := ABlackBoxRec;
          AReadCounterList.Add(AReadCounterRec);
        end;
      end;
    end;
  finally
  end;
end; // RecreateReadCounterList

// 20013196
procedure ClearLinkJobList;
var
  I: Integer;
  ALinkJobRec: PLinkJobRec;
  procedure ClearJobList; // 20015376
  var
    J: Integer;
    AJobRec: PLinkJobRec;
  begin
    if Assigned(ALinkJobRec.JobList) then
    begin
      for J := ALinkJobRec.JobList.Count - 1 downto 0 do
      begin
        AJobRec := ALinkJobRec.JobList.Items[J];
        ALinkJobRec.JobList.Remove(AJobRec);
        Dispose(AJobRec);
      end;
      ALinkJobRec.JobList.Clear;
      ALinkJobRec.JobList.Free;
    end;
  end;
begin
  for I := LinkJobList.Count - 1 downto 0 do
  begin
    ALinkJobRec := LinkJobList.Items[I];
    ClearJobList; // 20015376
    ALinkJobRec.JobList := nil;
    LinkJobList.Remove(ALinkJobRec);
    Dispose(ALinkJobRec);
  end;
  LinkJobList.Clear;
end; // ClearLinkJobList

// 20013196
function FindLinkJobList(APlantCode, AWorkspotCode, AJobCode: String): PLinkJobRec; // 20015376
var
  I: Integer;
  ALinkJobRec: PLinkJobRec;
begin
  Result := nil;
  for I := 0 to LinkJobList.Count - 1 do
  begin
    ALinkJobRec := LinkJobList[I];
    if (APlantCode = ALinkJobRec.PlantCode) and
      (AWorkspotCode = ALinkJobRec.WorkspotCode) and
      (AJobCode = ALinkJobRec.JobCode) then
      begin
        Result := ALinkJobRec;
        Break;
      end;
  end;
end; // FindLinkJobList

// 20013196
procedure AddLinkJobList(APlantCode, AWorkspotCode, AJobCode: String;
  ALinkPlantCode, ALinkWorkspotCode, ALinkJobCode: String);  // 20015376
var
  ALinkJobRec, AJobRec: PLinkJobRec;
begin
  // 20015376
  ALinkJobRec := FindLinkJobList(APlantCode, AWorkspotCode, AJobCode);
  if not Assigned(ALinkJobRec) then
  begin
    new(ALinkJobRec);
    ALinkJobRec.PlantCode := APlantCode;
    ALinkJobRec.WorkspotCode := AWorkspotCode;
    ALinkJobRec.JobCode := AJobCode;
    ALinkJobRec.JobList := TList.Create;
    LinkJobList.Add(ALinkJobRec);
  end;
  // 20015376
  new(AJobRec);
  AJobRec.PlantCode := ALinkPlantCode;
  AJobRec.WorkspotCode := ALinkWorkspotCode;
  AJobRec.JobCode := ALinkJobCode;
  ALinkJobRec.JobList.Add(AJobRec);
end; // AddLinkJobList

// TD-24819
procedure PDrawObject.InitCompareJobList(AFree: Boolean);
var
  ADrawObject: PDrawObject;
  ACompareJobRec: PCompareJobRec;
  I: Integer;
begin
  ADrawObject := Self;
  if Assigned(ADrawObject.ACompareJobList) then
  begin
    for I := ADrawObject.ACompareJobList.Count - 1 downto 0 do
    begin
      ACompareJobRec := ADrawObject.ACompareJobList.Items[I];
      Dispose(ACompareJobRec);
    end;
    ADrawObject.ACompareJobList.Clear;
    if AFree then
      ADrawObject.ACompareJobList.Free;
  end;
end; // InitCompareJobList

// 20015331
procedure InitIgnoreInterfaceCodeList(AFree: Boolean);
var
  AIgnoreInterfaceCodeRec: PIgnoreInterfaceCodeRec;
  I: Integer;
begin
  if Assigned(IgnoreInterfaceCodeList) then
  begin
    for I := IgnoreInterfaceCodeList.Count - 1 downto 0 do
    begin
      AIgnoreInterfaceCodeRec := IgnoreInterfaceCodeList[I];
      Dispose(AIgnoreInterfaceCodeRec);
    end;
    IgnoreInterfaceCodeList.Clear;
  end;
  if AFree then
    IgnoreInterfaceCodeList.Free;
end; // InitIgnoreInterfaceCodeList

// 20013476 // PIM-12
(*
function PDrawObject.NewSampleTimeSeconds: Double;
var
  ADrawObject: PDrawObject;
  ACurJobCodeRec: PJobCodeRec;
  CurrentJobCode: String;
  FromMinutes, ToMinutes: Double;
  R, S1, S2, Step, X: Double;
  CurrentElapsedTimeSeconds: Double;
  I, Min, Max: Double;
  function MyTime(AValue: Double): String;
  begin
    Result := Format('%.2d:%.2d', [Round(AValue) div 60, Round(AValue) mod 60])
  end;
begin
  ADrawObject := Self;

  CurrentJobCode := '';
  ACurJobCodeRec := nil;
//  APrevJobCodeRec := nil;
  S1 := 0;
  S2 := 0;
//  Step := 0;
  Result := CurrentTimeWindow;
  try
    CurrentElapsedTimeSeconds := 0;
    if Assigned(ADrawObject.AMachineTimeRec) then
      CurrentJobCode := ADrawObject.AMachineTimeRec.ACurrentJobCode
    else
      if Assigned(ADrawObject.AWorkspotTimeRec) then
        CurrentJobCode := ADrawObject.AWorkspotTimeRec.ACurrentJobCode;
    if CurrentJobCode <> '' then
    begin
      ACurJobCodeRec := ADrawObject.FindJobCodeListRec(CurrentJobCode);
      if Assigned(ACurJobCodeRec) then
        CurrentElapsedTimeSeconds := ACurJobCodeRec.SumOfFifoListCurrentTimeElapsed;
//      if ADrawObject.APrevJobCode <> '' then
//        APrevJobCodeRec := ADrawObject.FindJobCodeListRec(ADrawObject.APrevJobCode);
    end;
    if (ADrawObject.APrevJobSampleTimeMins <> 0) and
      (ADrawObject.ACurJobSampleTimeMins <> 0)
      then
    begin
      FromMinutes := ADrawObject.APrevJobSampleTimeMins;
      ToMinutes := ADrawObject.ACurJobSampleTimeMins;
      // NOTE: If there was a previous job then take the left time of that instead
      //       of the fixed Sample time to prevent a wrong calculation.
{
      if Assigned(APrevJobCodeRec) then
      begin
        FromMinutes := Round(APrevJobCodeRec.SumOfFifoListCurrentTimeElapsed / 60);
        if FromMinutes > ADrawObject.APrevJobSampleTimeMins then
          FromMinutes := ADrawObject.APrevJobSampleTimeMins;
      end;
}
//    Formula := 0;
      R := 0;
      X := CurrentElapsedTimeSeconds;
      if FromMinutes = ToMinutes then
      begin
        Result := X;
        Exit;
      end;
      // Be sure X is not already higher then the sample minutes!
      if Round(X / 60) > ToMinutes then
        X := ToMinutes * 60;
      Min := 0;
      Max := 0;
      try
        S1 := FromMinutes * 60;
        S2 := ToMinutes * 60;
        if S1 < S2 then
          Min := S1
        else
          Min := S2;
        if S1 > S2 then
          Max := S1
        else
          Max := S2;
        Step := 1 - S1/S2;
        if (S1 < S2) then
          Step := Step + 0.1;
//    WLog('Step=' + Format('%f', [Step]));
//      Formula := S1 + (1 - S1/S2) * X;
//    WLog(Format('Formula=%f X=%d (%s)', [Formula, Round(X), MyTime(Formula)]));
        R := S1;
        I := 1;
        while (I <= X) do
        begin
          if (S1 >= S2) then
            R := Trunc(R + Step)
          else
          begin
            if (R = Round(R + Step)) then
              Step := Step + 0.1;
            R := Round(R + Step);
          end;
          I := I + 1;
        end;
      finally
        // Be sure R is between Min and Max here!
        if (S1 > S2) then // From High to Low -> Check the Minimum
          if (R < Min) then
            R := Min;
        if (S1 < S2) then // From Low to High -> Check the Maximum
          if (R > Max) then
            R := Max;
// WLog(Format('S1=%f S2=%f Step=%f X=%f (%s) R=%f (%s) Min=%f Max=%f', [S1, S2, Step, X, MyTime(X), R, MyTime(R), Min, Max]));
        Result := R;
      end;
    end
    else
    begin
      if CurrentJobCode <> '' then
      begin
        Result := Round(CurrentElapsedTimeSeconds);
        if Assigned(ACurJobCodeRec) then
          if ACurJobCodeRec.Sample_time_mins <> 0 then
            if Result > ACurJobCodeRec.Sample_time_mins * 60 then
              Result := ACurJobCodeRec.Sample_time_mins * 60;
      end
      else
        if ADrawObject.ACurJobSampleTimeMins <> 0 then
          Result := ADrawObject.ACurJobSampleTimeMins * 60
        else
          Result := CurrentTimeWindow;
    end;
  finally
    // This is used during 'ADrawObject.AddTimeCurrent'
    ADrawObject.ACurrentSampleTimeMins := Round(Result);
  end;
end; // NewSampleTimeSeconds
*)

function FindJobCodeRec(APSList: TList; APlantCode, AWorkspotCode, AJobCode: String): PJobCodeRec;
var
  ADrawObject: PDrawObject;
begin
  Result := nil;
  ADrawObject := FindDrawObject(APSList, APlantCode, AWorkspotCode);
  if Assigned(ADrawObject) then
    if (ADrawObject.APlantCode = APlantCode) and
      (ADrawObject.AWorkspotCode = AWorkspotCode) then
      Result := ADrawObject.FindJobCodeListRec(AJobCode);
end; // FindJobCodeRec

// 20014450.50
// PIM-12 This is not used anymore, because it gave wrong (too high) qty-values!
{
procedure PDrawObject.InsertWorkspotEfficiencyPerMinute(
  APSList: TList;
  AIntervalStartTime, AIntervalEndTime: TDateTime;
  ATillNow: Boolean);
var
  ADrawObject: PDrawObject;
  AJobCodeRec: PJobCodeRec;
  NormLevel, Qty, SecondsNorm: Double;
  I: Integer;
  ShiftDate: TDateTime;
  procedure InsertQty;
  begin
    if Qty <> 0 then
    begin
      NormLevel := AJobCodeRec.Norm_prod_level;
      SecondsNorm := 0;
      if NormLevel <> 0 then
        SecondsNorm := Qty * 3600 / NormLevel;
      if Frac(AIntervalStartTime) < Frac(ADrawObject.ACutOffTimeShiftDate) then
        ShiftDate := Trunc(AIntervalStartTime - 1)
      else
        ShiftDate := Trunc(AIntervalStartTime);
      RealTimeEffDM.MergeWorkspotEffPerMinute(
        ShiftDate,
        ADrawObject.AWorkspotTimeRec.ACurrentShiftNumber,
        ADrawObject.APlantCode,
        ADrawObject.AWorkspotCode,
        AJobCodeRec.JobCode,
        AIntervalStartTime, AIntervalEndTime,
        0,            // AWorkspotspotSecondsActual (Always 0)
        SecondsNorm,  // AWorkspotSecondsNorm
        Qty,          // AWorkspotQuantity
        NormLevel,
        AIntervalStartTime, AIntervalEndTime);
    end; // if
  end; // InsertQty
  // PIM-12.2
  procedure LinkJobHandling;
  var
    J, K: Integer;
    ALinkJobRec, AJobRec: PLinkJobRec;
    AJobCodeRecLink: PJobCodeRec;
  begin
    // Note: There can be more than 1 compare-workspot, so first add all the
    //       quantities before inserting them.
    Qty := 0;
    for J := 0 to LinkJobList.Count - 1 do
    begin
      ALinkJobRec := LinkJobList.Items[J];
      if Assigned(ALinkJobRec.JobList) then
        for K := 0 to ALinkJobRec.JobList.Count - 1 do
        begin
          AJobRec := ALinkJobRec.JobList.Items[K];
          AJobCodeRecLink := FindJobCodeRec(APSList, AJobRec.PlantCode,
            AJobRec.WorkspotCode, AJobRec.JobCode);
          if Assigned(AJobCodeRecLink) then
          begin
            // Get sum of qty of fifo list for last minute
            // OR till Now when closing app to prevent a qty is missing.
            if ATillNow then
              Qty := Qty + AJobCodeRecLink.SumOfFifoListQty(AIntervalStartTime, Now)
            else
              Qty := Qty + AJobCodeRecLink.SumOfFifoListQty(AIntervalStartTime, AIntervalEndTime);
          end;
        end; // for
    end; // for
    InsertQty;
  end; // LinkJobHandling
begin
  ADrawObject := Self;
  with PersonalScreenDM do
  begin
    if Assigned(ADrawObject.AWorkspotTimeRec) then
    begin
      // Do this for EACH workspot-job with qty <> 0
      for I := 0 to  ADrawObject.AJobCodeList.Count - 1 do
      begin
        ADrawObject.AJobCodeList.Items[I];
        AJobCodeRec := ADrawObject.AJobCodeList.Items[I];
        // PIM-12.2 Link-job handling
        //          If it is a link-job-workspot then take the qty from the
        //          linked job.
        if (ADrawObject.AWorkspotTimeRec.AIsLinkJobWorkspot) then
        begin
          if (AJobCodeRec.Norm_prod_level <> 0) then
            LinkJobHandling;
        end
        else
        begin
          // Get sum of qty of fifo list for last minute
          // OR till Now when closing app to prevent a qty is missing.
          if ATillNow then
            Qty := AJobCodeRec.SumOfFifoListQty(AIntervalStartTime, Now)
          else
            Qty := AJobCodeRec.SumOfFifoListQty(AIntervalStartTime, AIntervalEndTime);
          InsertQty;
        end;
      end; // for
    end; // if
  end; // with
end; // InsertWorkspotEffPerMinute
}
function PDrawObject.DetermineEmployeeName(
  AEmployeeNumber: Integer): String;
var
  I: Integer;
  AEmployeeDisplayRec: PTEmployeeDisplayRec;
begin
  Result := '';
  // PIM-12.3 try-except added
  try
    if Assigned(Self.AEmployeeDisplayList) then
      for I := 0 to Self.AEmployeeDisplayList.Count - 1 do
      begin
        AEmployeeDisplayRec := Self.AEmployeeDisplayList.Items[I];
        if AEmployeeDisplayRec.EmployeeNumber = AEmployeeNumber then
        begin
          Result := AEmployeeDisplayRec.EmployeeName;
          Break;
        end;
      end;
  except
    // Ignore error
  end;
end; // DetermineEmployeeName

// 20015376
// PIM-12.2 For calculate eff-by-time: Also add time!
procedure HandleLinkJobsActualValues(APSList: TList);
var
  ALinkDrawObject, ADrawObject: PDrawObject;
  ALinkJobRec, AJobRec: PLinkJobRec;
  J, K: Integer;
  AJobCodeRec: PJobCodeRec;
  CurrentNrOfEmployees: Integer;
  function FindJobCodeRec: PJobCodeRec;
  var
    I: Integer;
    AJobCodeRec2: PJobCodeRec;
  begin
    Result := nil;
    for I := 0 to ALinkDrawObject.AJobCodeList.Count - 1 do
    begin
      AJobCodeRec2 := ALinkDrawObject.AJobCodeList.Items[I];
      if AJobCodeRec2.Norm_prod_level <> 0 then
      begin
        Result := AJobCodeRec2;
        Break;
      end;
    end;
  end;
begin
  for J := 0 to LinkJobList.Count - 1 do
  begin
    ALinkJobRec := LinkJobList.Items[J];
    if Assigned(ALinkJobRec.JobList) then
    begin
      ALinkDrawObject :=
        FindDrawObject(APSList, ALinkJobRec.PlantCode, ALinkJobRec.WorkspotCode);
      if Assigned(ALinkDrawObject) then
      begin
        ALinkDrawObject.Act_prodqty_day := 0;
        ALinkDrawObject.Act_prodqty_shift := 0;
        ALinkDrawObject.Act_prodqty_current := 0;
        ALinkDrawObject.Theo_prodqty_day := 0;
        ALinkDrawObject.Theo_prodqty_shift := 0;
        ALinkDrawObject.Theo_prodqty_current := 0;
        // PIM-12.2 Times:
        ALinkDrawObject.Act_time_day := 0;
        ALinkDrawObject.Act_time_shift := 0;
        ALinkDrawObject.Act_time_current := 0;
        ALinkDrawObject.Theo_time_day := 0;
        ALinkDrawObject.Theo_time_shift := 0;
        ALinkDrawObject.Theo_time_current := 0;
        for K := 0 to ALinkJobRec.JobList.Count - 1 do
        begin
          AJobRec := ALinkJobRec.JobList.Items[K];
          ADrawObject :=
            FindDrawObject(APSList, AJobRec.PlantCode, AJobRec.WorkspotCode);
          if Assigned(ADrawObject) then
          begin
            ALinkDrawObject.Act_prodqty_day :=
              ALinkDrawObject.Act_prodqty_day + ADrawObject.Act_prodqty_day;
            ALinkDrawObject.Act_prodqty_shift :=
              ALinkDrawObject.Act_prodqty_shift + ADrawObject.Act_prodqty_shift;
            ALinkDrawObject.Act_prodqty_current :=
              ALinkDrawObject.Act_prodqty_current + ADrawObject.Act_prodqty_current;
            ALinkDrawObject.Theo_prodqty_day :=
              ALinkDrawObject.Theo_prodqty_day + ADrawObject.Theo_prodqty_day;
            ALinkDrawObject.Theo_prodqty_shift :=
              ALinkDrawObject.Theo_prodqty_shift + ADrawObject.Theo_prodqty_shift;
            ALinkDrawObject.Theo_prodqty_current :=
              ALinkDrawObject.Theo_prodqty_current + ADrawObject.Theo_prodqty_current;
            // PIM-12.2 Add time only once (because there can be multiple compare-workspot connected to it).
            // Do this at last record found! To be sure the Act_Prodqty_current is filled!
            if K = ALinkJobRec.JobList.Count - 1 then
            begin
              ALinkDrawObject.Act_time_day := ADrawObject.Act_time_day;
              ALinkDrawObject.Act_time_shift := ADrawObject.Act_time_shift;
              ALinkDrawObject.Act_time_current := ADrawObject.Act_time_current;
              AJobCodeRec := FindJobCodeRec;
              if Assigned(AJobCodeRec) then
              begin
                ALinkDrawObject.AddTheoTimeDay(AJobCodeRec);
                ALinkDrawObject.AddTheoTimeShift(AJobCodeRec);
                CurrentNrOfEmployees :=
                  ALinkDrawObject.ACurrentNumberOfEmployees;
                if CurrentNrOfEmployees = 0 then
                  CurrentNrOfEmployees := 1;
                // PIM-12.3 Rework: Multiple norm by number of linked-jobs!
                //          This prevents the eff. based on time is too low?
                //          NOTE: Do not do this!
                //          This must be solved by changing the norm of the
                //          linked-job!
                if AJobCodeRec.Norm_prod_level <> 0 then
                  ALinkDrawObject.Theo_time_current :=
                    ALinkDrawObject.Act_prodqty_current /
                      AJobCodeRec.Norm_prod_level * 60 * 60 /
                        CurrentNrOfEmployees;
              end; // if
            end; // if
          end; // if
        end; // for
      end; // if
    end; // if
  end; // for
end; // HandleLinkJobsActualValues

// 20015376 Related to this order
procedure HandleCompareJobsTime(APSList: TList);
var
  ACompDrawObject, ADrawObject: PDrawObject;
  ACompareJobRec: PCompareJobRec;
  AJobCodeRec: PJobCodeRec;
  I, J, K: Integer;
begin
  for I := 0 to APSList.Count - 1 do
  begin
    ACompDrawObject := APSList.Items[I];
    if Assigned(ACompDrawObject.ACompareJobList) then
    begin
      for J := 0 to ACompDrawObject.ACompareJobList.Count - 1 do
      begin
        ACompareJobRec := ACompDrawObject.ACompareJobList.Items[J];
        ADrawObject :=
          FindDrawObject(APSList, ACompDrawObject.APlantCode,
            ACompareJobRec.WorkspotCode);
        if Assigned(ADrawObject) then
        begin
          ACompDrawObject.Act_time_day := ADrawObject.Act_time_day;
          ACompDrawObject.Act_time_shift := ADrawObject.Act_time_shift;
          ACompDrawObject.Act_time_current := ADrawObject.Act_time_current;
          if Assigned(ACompDrawObject.AJobCodeList) then
            for K := 0 to ACompDrawObject.AJobCodeList.Count - 1 do
            begin
              AJobCodeRec := ACompDrawObject.AJobCodeList.Items[K];
              if Assigned(AJobCodeRec) then
              begin
                if AJobCodeRec.InterfaceCode <> '' then
                begin
                  AJobCodeRec.Act_time_day := ADrawObject.Act_time_day;
                  AJobCodeRec.Act_time_shift := ADrawObject.Act_time_shift;
                  AJobCodeRec.Act_time_current := ADrawObject.Act_time_current;
                  Break;
                end;
              end;
            end;
          Break;
        end;
      end;
    end;
  end;
end; // HandleCompareJobsTime

// 20014450.50
// PIM-12 Not used anymore!
{
procedure ActionRealTimeEfficiencyMain(APSList: TList; ANow: TDateTime;
  ATillNow: Boolean);
var
  I: Integer;
  ADrawObject: PDrawObject;
  IntervalStartTime, IntervalEndTime: TDateTime;
  Hrs, Mins, Secs, MSecs: Word;
  function Bool2Str(AValue: Boolean): String;
  begin
    if AValue then
      Result := 'True'
    else
      Result := 'False';
  end;
begin
  DecodeTime(ANow, Hrs, Mins, Secs, MSecs);
  // Look from the last whole minute till now.
  IntervalEndTime := Trunc(ANow) + Frac(EncodeTime(Hrs, Mins, 0, 0));
  IntervalStartTime := IntervalEndTime - 1/24/60; // 1 minute earlier
  for I := 0 to APSList.Count - 1 do
  begin
    ADrawObject := APSList.Items[I];
    if Assigned(ADrawObject.AWorkspotTimeRec) then
      ADrawObject.InsertWorkspotEfficiencyPerMinute(APSList,
        IntervalStartTime, IntervalEndTime, ATillNow);
  end;
  LastTimeInsertWorkspotPerMinute := IntervalEndTime;
end; // ActionRealTimeEfficiency
}
// 20014450.50
{
procedure ActionRealTimeEfficiency(APSList: TList; ANow: TDateTime);
begin
  ActionRealTimeEfficiencyMain(APSList, ANow, False);
end;
}
// 20014450.50
{
procedure ActionRealTimeEfficiencyTillNow(APSList: TList; ANow: TDateTime);
begin
  ActionRealTimeEfficiencyMain(APSList, ANow, True);
end;
}
// 20014450.50
procedure MarkAllRecreateEmployeeDisplayList(APSList: TList);
var
  I: Integer;
  ADrawObject: PDrawObject;
begin
  for I := 0 to APSList.Count - 1 do
  begin
    ADrawObject := APSList.Items[I];
    if Assigned(ADrawObject.AWorkspotTimeRec) then
      ADrawObject.ARecreateEmployeeDisplayList := True;
  end;
end;

// 20014450.50
function PDrawObject.EmployeeListChangedTest: Boolean;
var
  I, J, Count: Integer;
  AEmployeeRec: PEmployeeRec;
  AEmployeeDisplayRec: PTEmployeeDisplayRec;
  ADrawObject: PDrawObject;
begin
  ADrawObject := Self;
  Result := False;
  // PIM12.3 try-except added
  try
    Count := 0;
    if Assigned(ADrawObject.AEmployeeList) then
    begin
      if Assigned(ADrawObject.AEmployeeDisplayList) then
      begin
        for I := 0 to ADrawObject.AEmployeeList.Count - 1 do
        begin
          AEmployeeRec := ADrawObject.AEmployeeList.Items[I];
          if AEmployeeRec.CurrentlyScannedIn then
            for J := 0 to ADrawObject.AEmployeeDisplayList.Count - 1 do
            begin
              AEmployeeDisplayRec := ADrawObject.AEmployeeDisplayList.Items[J];
              if AEmployeeRec.EmployeeNumber = AEmployeeDisplayRec.EmployeeNumber then
                inc(Count);
            end;
        end;
        Result := (ADrawObject.AEmployeeList.Count <> Count);
      end;
    end;
  except
    // Ignore error
  end;
end; // EmployeeListChangedTest

// PIM-12
procedure PDrawObject.InitFifoList(AFree: Boolean);
var
  ADrawObject: PDrawObject;
  AJobCodeRec: PJobCodeRec;
  I: Integer;
begin
  ADrawObject := Self;
  if Assigned(ADrawObject.AJobCodeList) then
  begin
    for I := ADrawObject.AJobCodeList.Count - 1 downto 0 do
    begin
      AJobCodeRec := ADrawObject.AJobCodeList.Items[I];
      AJobCodeRec.Act_prodqty_current := 0;
      AJobCodeRec.Act_time_current := 0;
      AJobCodeRec.Theo_prodqty_current :=0;
      AJobCodeRec.Theo_time_current := 0;
      AJobCodeRec.InitFifoList(AFree);
    end;
    ADrawObject.Act_prodqty_current := 0;
    ADrawObject.Act_time_current := 0;
    ADrawObject.Theo_prodqty_current := 0;
    ADrawObject.Theo_time_current := 0;
  end;
end; // InitFifoList

// PIM-147
function PDrawObject.FindEmployeeRec(
  AEmployeeNumber: Integer): PEmployeeRec;
var
  I: Integer;
begin
  Result := nil;
  I := Self.FindEmployeeListIndex(AEmployeeNumber);
  if I <> -1 then
    Result := Self.AEmployeeList.Items[I];
end; // FindEmployeeRec

// PIM-147 When multiple employees work on 1 workspot then determine
//         the current job based on the most used.
function PDrawObject.DetermineCurrentJobByEmployeeList: PJobCodeRec;
var
  I, Max: Integer;
  AJobCodeRec: PJobCodeRec;
  AEmployeeRec: PEmployeeRec;
  function IsValidJob: Boolean;
  begin
//    Result := True;
    Result := AJobCodeRec.Norm_prod_level <> 0;
//  if (AJobCodeRec.InterfaceCode <> '') or (AJobCodeRec.JobCode = BREAKJOB) then
  end;
  function EmployeeCurrentlyScannedInCount: Integer;
  var
    I: Integer;
  begin
    Result := 0;
    for I := 0 to Self.AEmployeeList.Count - 1 do
    begin
      AEmployeeRec := Self.AEmployeeList.Items[I];
      if AEmployeeRec.CurrentlyScannedIn then
        Result := Result + 1;
    end;
  end;
begin
  Result := nil;
  if Assigned(Self.AWorkspotTimeRec) then
  begin
    if Self.AWorkspotTimeRec.ACurrentJobCode <> '' then
      Result := Self.FindJobCodeListRec(Self.AWorkspotTimeRec.ACurrentJobCode);
    // Any employee?
    if EmployeeCurrentlyScannedInCount > 0 then
    begin
      // Only 1 employee?
      if EmployeeCurrentlyScannedInCount = 1 then
      begin
        // Just take the current job of the workspot (when that was assigned)?
        AEmployeeRec := Self.AEmployeeList.Items[0];
        AJobCodeRec := Self.FindJobCodeListRec(AEmployeeRec.JobCode);
        Result := AJobCodeRec;
      end // 1 employee
      else
      begin
        // Exception:
        // First check if one of the employees jobs is the same as the current
        // workspot-job. If that is the case, then just take that job.
        Result := nil;
{        for I := 0 to Self.AEmployeeList.Count - 1 do
        begin
          AEmployeeRec := Self.AEmployeeList.Items[I];
          if AEmployeeRec.CurrentlyScannedIn then
            if (AEmployeeRec.JobCode = Self.AWorkspotTimeRec.ACurrentJobCode) then
            begin
              Result := FindJobCodeListRec(AEmployeeRec.JobCode);
              Break;
            end;
        end; }
        if not Assigned(Result) then
        begin
          // Find the most used job
          // Multiple employees?
          // Init the counts for all jobs
          for I := 0 to Self.AJobCodeList.Count - 1 do
          begin
            AJobCodeRec := Self.AJobCodeList.Items[I];
            AJobCodeRec.Count := 0;
          end;
          // Count the jobs
          for I := 0 to Self.AEmployeeList.Count - 1 do
          begin
            AEmployeeRec := Self.AEmployeeList.Items[I];
            if AEmployeeRec.CurrentlyScannedIn then
            begin
              AJobCodeRec := Self.FindJobCodeListRec(AEmployeeRec.JobCode);
              if IsValidJob then
                inc(AJobCodeRec.Count);
            end;
          end;
          // Which job has maximum count?
          Max := 0;
          for I := 0 to Self.AJobCodeList.Count - 1 do
          begin
            AJobCodeRec := Self.AJobCodeList.Items[I];
            if AJobCodeRec.Count > Max then
              Max := AJobCodeRec.Count;
          end;
          if Max > 0 then
          begin
            for I := 0 to Self.AJobCodeList.Count - 1 do
            begin
              AJobCodeRec := Self.AJobCodeList.Items[I];
              if AJobCodeRec.Count = Max then
              begin
                Result := AJobCodeRec;
                Break;
              end
            end;
          end; // max > 0
        end; // if not Assigned(Result)
      end; // multiple employees
    end; // if any employee
    if Assigned(Result) then
    begin
      // Set flag to indicate if current workspot-job has changed, this is
      // used later to determine if the current qty must be reset.
      Self.AJobChanged :=
        Self.AWorkspotTimeRec.ACurrentJobCode <> Result.JobCode;
      Self.AWorkspotTimeRec.ACurrentJobCode := Result.JobCode;
      Self.AWorkspotTimeRec.ACurrentJobDescription := Result.JobCodeDescription;
    end
    else
    begin
      // When nothing found: Leave it like it was (old values will be used).
    //  Self.AWorkspotTimeRec.ACurrentJobCode := '';
    //  Self.AWorkspotTimeRec.ACurrentJobDescription := '';
    end;
  end; // if
end; // DetermineCurrentJobByEmployeeList

// PIM-125
function EmployeesCurrentlyScannedInCheck(APSList: TList): Boolean;
var
  I, J: Integer;
  ADrawObject: PDrawObject;
  AEmployeeRec: PEmployeeRec;
begin
  Result := False;
  if Assigned(APSList) then
  begin
    I := 0;
    while (I < APSList.Count) and (not Result) do
    begin
      ADrawObject := APSList.Items[I];
      if Assigned(ADrawObject.AEmployeeList) then
      begin
        J := 0;
        while (J < ADrawObject.AEmployeeList.Count) and (not Result) do
        begin
          AEmployeeRec := ADrawObject.AEmployeeList.Items[J];
          Result := AEmployeeRec.CurrentlyScannedIn;
          inc(J);
        end; // while
      end; // if
      inc(I);
    end; // while
  end; // if
end; // EmployeesCurrentlyScannedInCheck

// PIM-155
// Init the Fifo List for compare jobs that are connected to it.
procedure PDrawObject.CompareJobInitFifoList(APSList: TList);
var
  I, J: Integer;
  ADrawObject: PDrawObject;
  ACompareJobRec: PCompareJobRec;
begin
  if not Assigned(Self.AWorkspotTimeRec) then
    Exit;
  for I := 0 to APSList.Count - 1 do
  begin
    ADrawObject := APSList.Items[I];
    if Assigned(ADrawObject.ACompareJobList) then
    begin
      for J := 0 to ADrawObject.ACompareJobList.Count - 1 do
      begin
        ACompareJobRec := ADrawObject.ACompareJobList.Items[J];
        if ADrawObject.APlantCode = Self.APlantCode then
          if ACompareJobRec.WorkspotCode = Self.AWorkspotCode then
          begin
            // PIM-159
            if Self.AWorkspotTimeRec.ACurrentJobCode <> '' then
            begin
              if ACompareJobRec.JobCode = Self.AWorkspotTimeRec.ACurrentJobCode then
                ADrawObject.InitFifoList(False);
            end
            else
              if ACompareJobRec.JobCode = Self.AWorkspotTimeRec.ALastJobCode then
                ADrawObject.InitFifoList(False);
          end;
      end;
    end;
  end;
end; // CompareJobInitFifoList

// PIM-195 Reset the Theo-time for all jobs for Shift and Day.
procedure PDrawObject.ResetJobsTheoTime;
var
  I: Integer;
  ADrawObject: PDrawObject;
  AJobCodeRec: PJobCodeRec;
begin
  ADrawObject := Self;
  ADrawObject.Theo_time_shift := 0;
  ADrawobject.Theo_time_day := 0;
  if Assigned(ADrawObject.AJobCodeList) then
  begin
    for I := 0 to ADrawObject.AJobCodeList.Count - 1 do
    begin
      AJobCodeRec := ADrawObject.AJobCodeList[I];
      AJobCodeRec.Theo_time_shift := 0;
      AJobCodeRec.Theo_time_day := 0;
    end;
  end;
end; // ResetJobsTheoTime

initialization
  LinkJobList := TList.Create; // 20013196
  IgnoreInterfaceCodeList := TList.Create; // 20015331
  WorkspotScale := DEFAULT_WORKSPOT_SCALE; // 20015406
  FontScale := DEFAULT_WORKSPOT_SCALE; // 20015406
  EffPeriodMode := epCurrent; // 20015406
  BBCount := 0; // ABS-8116
  GlobalEfficiencyPeriodSinceTime := NullDate; // 20014450.50

finalization
  ClearLinkJobList;
  LinkJobList.Free; // 20013196
  InitIgnoreInterfaceCodeList(True); // 20015331

end.

