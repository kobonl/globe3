inherited DialogObjectSettingsF: TDialogObjectSettingsF
  Left = 546
  Top = 242
  VertScrollBar.Range = 0
  BorderStyle = bsDialog
  Caption = 'Settings'
  ClientHeight = 255
  ClientWidth = 508
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlImageBase: TPanel
    Width = 508
    inherited imgBasePims: TImage
      Left = 212
    end
  end
  inherited stbarBase: TStatusBar
    Top = 236
    Width = 508
  end
  inherited pnlInsertBase: TPanel
    Width = 508
    Height = 136
    Font.Color = clBlack
    Font.Height = -13
    object GroupBox1: TGroupBox
      Left = 0
      Top = 0
      Width = 508
      Height = 136
      Align = alClient
      Caption = 'Settings'
      TabOrder = 0
      object GroupBox4: TGroupBox
        Left = 2
        Top = 18
        Width = 255
        Height = 54
        Align = alClient
        Caption = 'Workspot Scale'
        TabOrder = 0
        object Label3: TLabel
          Left = 8
          Top = 20
          Width = 64
          Height = 16
          Caption = 'Percentage'
        end
        object dxSpinEditWorkspotScalePercentage: TdxSpinEdit
          Left = 104
          Top = 17
          Width = 73
          TabOrder = 0
          MaxValue = 250.000000000000000000
          MinValue = 1.000000000000000000
          Value = 50.000000000000000000
          StoredValues = 48
        end
      end
      object GroupBox7: TGroupBox
        Left = 257
        Top = 18
        Width = 249
        Height = 54
        Align = alRight
        Caption = 'Font Scale'
        TabOrder = 1
        object Label8: TLabel
          Left = 8
          Top = 20
          Width = 64
          Height = 16
          Caption = 'Percentage'
        end
        object dxSpinEditFontScalePercentage: TdxSpinEdit
          Left = 104
          Top = 17
          Width = 73
          TabOrder = 0
          MaxValue = 250.000000000000000000
          Value = 50.000000000000000000
          StoredValues = 48
        end
      end
      object GroupBox5: TGroupBox
        Left = 2
        Top = 72
        Width = 504
        Height = 62
        Align = alBottom
        Caption = 'Efficiency Meters'
        TabOrder = 2
        object Label4: TLabel
          Left = 8
          Top = 28
          Width = 61
          Height = 16
          Caption = 'Use Period'
        end
        object rGrpMode: TRadioGroup
          Left = 99
          Top = 15
          Width = 390
          Height = 37
          Columns = 3
          Items.Strings = (
            'Current'
            'Since'
            'Shift')
          TabOrder = 0
        end
      end
    end
  end
  inherited pnlBottom: TPanel
    Top = 173
    Width = 508
    inherited btnOK: TBitBtn
      Left = 147
      OnClick = btnOkClick
    end
    inherited btnCancel: TBitBtn
      Left = 261
    end
  end
  inherited imgList16x16: TImageList
    Left = 496
    Top = 8
  end
  inherited imgNoYes: TImageList
    Left = 544
    Top = 8
  end
  inherited StandardMenuActionList: TActionList
    Left = 432
    Top = 32
  end
  inherited mmPims: TMainMenu
    Left = 472
    Top = 32
  end
  inherited StyleController: TdxEditStyleController
    Left = 528
    Top = 40
  end
  inherited StyleControllerRequired: TdxEditStyleController
    Left = 568
    Top = 40
  end
end
