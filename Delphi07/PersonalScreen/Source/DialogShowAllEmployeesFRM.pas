(*
  MRA:16-NOV-2012 TD-21429 Related to this order.
  - Set property Position for this form to 'poDefault', because it is
    repositioned later.
  MRA:28-MAY-2013 20014289 New look Pims
  - Some pictures/colors are changed.
  MRA:4-JUN-2015 20014450.50 Part 2
  - Real Time Efficiency
  - Show Curr + Shift Efficiency in employee-overview
*)
unit DialogShowAllEmployeesFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ORASystemDMT, BaseDialogFRM, ActnList, StdCtrls, Buttons,
  ComCtrls, ExtCtrls, dxCntner, dxTL,
  dxGrClms, dxTLClms, HomeFRM,
  PlanScanEmpDMT, dxExEdtr, Menus, StdActns, ImgList, {UProductionScreen}
  UPersonalScreen;

type
  TBandType = (bEmployee, bScanned, bPlanned, bAvailable, bAbsent, bFirstAid);

type
  TDialogShowAllEmployeesF = class(TBaseDialogForm)
    pnlWorkspot: TPanel;
    pnlEmployees: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    edtPlantCode: TEdit;
    edtWorkspotCode: TEdit;
    edtWorkspotDescription: TEdit;
    dxList: TdxTreeList;
    dxListColumnCOLOR: TdxTreeListButtonColumn;
    dxListColumnNUMBER: TdxTreeListColumn;
    dxListColumnNAME: TdxTreeListColumn;
    dxListColumnSHORTNAME: TdxTreeListColumn;
    dxListColumnTEAM: TdxTreeListColumn;
    dxListColumnSCANNEDWS: TdxTreeListColumn;
    dxListColumnPLANNEDWS: TdxTreeListColumn;
    dxListColumnELEVEL: TdxTreeListColumn;
    dxListColumnEPLANLEVEL: TdxTreeListColumn;
    dxListColumnABSENCEREASONCODE: TdxTreeListColumn;
    dxListColumnABSENCEREASONDESCR: TdxTreeListColumn;
    dxListColumnFIRSTAIDEXPIRATIONDATE: TdxTreeListDateColumn;
    dxListColumnCOLORVALUE: TdxTreeListColumn;
    dxListColumnPLANNEDSTARTTIME: TdxTreeListColumn;
    dxListColumnPLANNEDENDTIME: TdxTreeListColumn;
    dxListColumnSASTARTTIME: TdxTreeListColumn;
    dxListColumnSAENDTIME: TdxTreeListColumn;
    dxListColumnEASTARTTIME: TdxTreeListColumn;
    dxListColumnEAENDTIME: TdxTreeListColumn;
    dxListColumnSCANNEDDATEIN: TdxTreeListColumn;
    TimerAutoClose: TTimer;
    lblMessage: TLabel;
    dxListColumnCURREFF: TdxTreeListColumn;
    dxListColumnSHIFTEFF: TdxTreeListColumn;
    procedure FormShow(Sender: TObject);
    procedure dxListCustomDrawCell(Sender: TObject; ACanvas: TCanvas;
      ARect: TRect; ANode: TdxTreeListNode; AColumn: TdxTreeListColumn;
      ASelected, AFocused, ANewItemRow: Boolean; var AText: String;
      var AColor: TColor; AFont: TFont; var AAlignment: TAlignment;
      var ADone: Boolean);
    procedure TimerAutoCloseTimer(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
  private
    FMyTitle: String;
    FPlantCode: String;
    FWorkspotCode: String;
    FWorkspotDescription: String;
    FShowMode: TShowMode;
    FDrawObject: PDrawObject;
    procedure EmpListAdd(APTEmployee: PTEmployee);
    procedure ActionShowEmployees(ADrawObject: PDrawObject);
    procedure SetPlantCode(const Value: String);
    procedure SetWorkspotCode(const Value: String);
    procedure SetWorkspotDescription(const Value: String);
    procedure SetShowMode(const Value: TShowMode);
    procedure SetMyTitle(const Value: String);
    procedure SetDrawObject(const Value: PDrawObject);
    { Private declarations }
  public
    { Public declarations }
    property MyTitle: String write SetMyTitle;
    property PlantCode: String write SetPlantCode;
    property WorkspotCode: String write SetWorkspotCode;
    property WorkspotDescription: String write SetWorkspotDescription;
    property ShowMode: TShowMode write SetShowMode;
    property MyDrawObject: PDrawObject read FDrawObject write SetDrawObject;
  end;

var
  DialogShowAllEmployeesF: TDialogShowAllEmployeesF;

implementation

{$R *.DFM}

procedure TDialogShowAllEmployeesF.EmpListAdd(APTEmployee: PTEmployee);
var
  Item: TdxTreeListNode;
//  DateTemp: TDateTime;
  function MyDateFormat(AValue: TDateTime): String;
  begin
    Result := FormatDateTime('hh:nn (dd-mm)', AValue);
  end;
begin
  Item := dxList.Add;
  Item.Values[dxListColumnCOLORVALUE.Index] := APTEmployee.AColor;
  Item.Values[dxListColumnNUMBER.Index] := APTEmployee.AEmployeeNumber;
  Item.Values[dxListColumnNAME.Index] := APTEmployee.AEmployeeName;
  Item.Values[dxListColumnSHORTNAME.Index] := APTEmployee.AEmployeeShortName;
  Item.Values[dxListColumnTEAM.Index] := APTEmployee.ATeamCode;
  Item.Values[dxListColumnSCANNEDWS.Index] := APTEmployee.AScanWorkspotCode;
  if APTEmployee.AScanDateTimeIn <> 0 then
  begin
//    DateTemp := Frac(APTEmployee.AScanDateTimeIn);
    Item.Values[dxListColumnSCANNEDDATEIN.Index] :=
      MyDateFormat(APTEmployee.AScanDateTimeIn)
  end
  else
    Item.Values[dxListColumnSCANNEDDATEIN.Index] := '';
  Item.Values[dxListColumnPLANNEDWS.Index] := APTEmployee.APlanWorkspotCode;
  if APTEmployee.APlanStartDate <> 0 then
    Item.Values[dxListColumnPLANNEDSTARTTIME.Index] :=
      MyDateFormat(APTEmployee.APlanStartDate)
  else
    Item.Values[dxListColumnPLANNEDSTARTTIME.Index] := '';
  if APTEmployee.APlanEndDate <> 0 then
    Item.Values[dxListColumnPLANNEDENDTIME.Index] :=
      MyDateFormat(APTEmployee.APlanEndDate)
  else
    Item.Values[dxListColumnPLANNEDENDTIME.Index] := '';
  Item.Values[dxListColumnELEVEL.Index] := APTEmployee.AEmpLevel;
  Item.Values[dxListColumnEPLANLEVEL.Index] := APTEmployee.APlanLevel;
  if APTEmployee.AStandAvailStartDate <> 0 then
    Item.Values[dxListColumnSASTARTTIME.Index] :=
      MyDateFormat(APTEmployee.AStandAvailStartDate)
  else
    Item.Values[dxListColumnSASTARTTIME.Index] := '';
  if APTEmployee.AStandAvailEndDate <> 0 then
    Item.Values[dxListColumnSAENDTIME.Index] :=
      MyDateFormat(APTEmployee.AStandAvailEndDate)
  else
    Item.Values[dxListColumnSAENDTIME.Index] := '';
  if APTEmployee.AEmpAvailStartDate <> 0 then
    Item.Values[dxListColumnEASTARTTIME.Index] :=
      MyDateFormat(APTEmployee.AEmpAvailStartDate)
  else
    Item.Values[dxListColumnEASTARTTIME.Index] := '';
  if APTEmployee.AEmpAvailEndDate <> 0 then
    Item.Values[dxListColumnEAENDTIME.Index] :=
      MyDateFormat(APTEmployee.AEmpAvailEndDate)
  else
    Item.Values[dxListColumnEAENDTIME.Index] := '';
  Item.Values[dxListColumnABSENCEREASONCODE.Index] :=
    APTEmployee.AAbsenceReasonCode;
  Item.Values[dxListColumnABSENCEREASONDESCR.Index] :=
    APTEmployee.AAbsenceReasonDescription;
  if APTEmployee.AFirstAidExpDate <> 0 then
    Item.Values[dxListColumnFIRSTAIDEXPIRATIONDATE.Index] :=
      APTEmployee.AFirstAidExpDate
  else
    Item.Values[dxListColumnFIRSTAIDEXPIRATIONDATE.Index] := '';
  if ORASystemDM.PSShowEmpInfo and ORASystemDM.PSShowEmpEff then
  begin
    Item.Values[dxListColumnCURREFF.Index] :=
      IntToStr(APTEmployee.ACurrEff); // 20014450.50
    Item.Values[dxListColumnSHIFTEFF.Index] :=
      IntToStr(APTEmployee.AShiftEff); // 20014450.50
  end;
end;

procedure TDialogShowAllEmployeesF.ActionShowEmployees(
  ADrawObject: PDrawObject);
var
  AEmployeeList: TList;
  APTEmployee: PTEmployee;
  AEmployeeCount: Integer;
  I: Integer;
  Band: TBandType;
begin
  dxList.ClearNodes;
  AEmployeeList := nil;
  for Band := bEmployee to bFirstAid do
    dxList.Bands[Integer(Band)].Visible := True;
  // Set Bands of Grid
  case FShowMode of
  ShowEmployeesOnWorkspot: // Employees scanned/planned on workspot
    begin
      dxList.Bands[Integer(bAvailable)].Visible := False;
      dxList.Bands[Integer(bAbsent)].Visible := False;
      dxList.Bands[Integer(bFirstAid)].Visible := False;
    end;
  ShowAllEmployees: // All: Scanned and Planned
    begin
      dxList.Bands[Integer(bAvailable)].Visible := False;
      dxList.Bands[Integer(bAbsent)].Visible := False;
      dxList.Bands[Integer(bFirstAid)].Visible := False;
    end;
  ShowEmployeesOnWrongWorkspot: // Scanned but on wrong Planned Workspot
    begin
      dxList.Bands[Integer(bAvailable)].Visible := False;
      dxList.Bands[Integer(bAbsent)].Visible := False;
      dxList.Bands[Integer(bFirstAid)].Visible := False;
    end;
  ShowEmployeesNotScannedIn: // Not Scanned or Planned, but available
    begin
      dxList.Bands[Integer(bAbsent)].Visible := False;
      dxList.Bands[Integer(bFirstAid)].Visible := False;
    end;
  ShowEmployeesAbsentWithReason: // Absent with a reason
    // Employees not scanned, planned, not available,
    // but absent with reason
    begin
      dxList.Bands[Integer(bScanned)].Visible := False;
      dxList.Bands[Integer(bPlanned)].Visible := False;
      dxList.Bands[Integer(bFirstAid)].Visible := False;
    end;
  ShowEmployeesAbsentWithoutReason: // Absent without a reason
    // Employees not scanned, but planned
    begin
      dxList.Bands[Integer(bScanned)].Visible := False;
      dxList.Bands[Integer(bFirstAid)].Visible := False;
    end;
  ShowEmployeesWithFirstAid: // Employee with first aid
    // Show scanned + planned
    begin
      dxList.Bands[Integer(bAvailable)].Visible := False;
      dxList.Bands[Integer(bAbsent)].Visible := False;
    end;
  ShowEmployeesTooLate: // Employee too late
    // Show planned + scanned
    begin
      dxList.Bands[Integer(bAvailable)].Visible := False;
      dxList.Bands[Integer(bAbsent)].Visible := False;
      dxList.Bands[Integer(bFirstAid)].Visible := False;
    end;
  end;
  if HomeF.DetermineEmployeeList(
    ADrawObject,
    FShowMode, FPlantCode, FWorkspotCode,
    AEmployeeList, AEmployeeCount) then
  begin
    for I := 0  to AEmployeeCount - 1 do
    begin
      APTEmployee := AEmployeeList.Items[I];
      EmpListAdd(APTEmployee);
    end;
  end;
  stBarBase.Panels[1].Text := IntToStr(AEmployeeCount);
end;

procedure TDialogShowAllEmployeesF.SetPlantCode(const Value: String);
begin
  FPlantCode := Value;
end;

procedure TDialogShowAllEmployeesF.SetWorkspotCode(const Value: String);
begin
  FWorkspotCode := Value;
end;

procedure TDialogShowAllEmployeesF.SetWorkspotDescription(
  const Value: String);
begin
  FWorkspotDescription := Value;
end;

procedure TDialogShowAllEmployeesF.FormShow(Sender: TObject);
var
  AmpersandPos: Integer;
begin
  inherited;
  edtPlantCode.Text := FPlantCode;
  edtWorkspotCode.Text := FWorkspotCode;
  edtWorkspotDescription.Text := FWorkspotDescription;
  // Filter ampersand out of string
  AmpersandPos := Pos('&', FMyTitle);
  if (AmperSandPos > 0) then
    FMyTitle := Copy(FMyTitle, 1, AmpersandPos - 1) +
      Copy(FMyTitle, AmpersandPos + 1, Length(FMyTitle));
  Caption := FMyTitle;

  pnlWorkspot.Visible := (FShowMode = ShowEmployeesOnWorkspot);

  ActionShowEmployees(MyDrawObject);
  // 20013176
  TimerAutoClose.Interval := ORASystemDM.AutoCloseInterval;
  if TimerAutoClose.Interval > 0 then
    TimerAutoClose.Enabled := True;
    
  lblMessage.Caption := ' ' + Caption; // 20014289
end;

procedure TDialogShowAllEmployeesF.SetShowMode(const Value: TShowMode);
begin
  FShowMode := Value;
end;

// Convert format 'hh:mm:ss' to 'hh:mm'
function ConvertToHourMin(Value: String): String;
var
  Str: String;
  Pos1, Pos2: Integer;
begin
//  Result := FormatDateTime('hh:nn (dd-mm)', Value);
  if Value <> '' then
  begin
    Str := Value;
    Pos1 := Pos(':', Str);
    if Pos1 > 0 then
    begin
      Str[Pos1] := ' ';
      Pos2 := Pos(':', Str);
      if Pos2 > 0 then
      begin
        Str[Pos1] := ':';
        Value := Copy(Str, 1, Pos2-1);
      end;
    end;
  end;
  Result := Value;
end;

procedure TDialogShowAllEmployeesF.dxListCustomDrawCell(Sender: TObject;
  ACanvas: TCanvas; ARect: TRect; ANode: TdxTreeListNode;
  AColumn: TdxTreeListColumn; ASelected, AFocused, ANewItemRow: Boolean;
  var AText: String; var AColor: TColor; AFont: TFont;
  var AAlignment: TAlignment; var ADone: Boolean);
var
  MyEff: Double;
begin
  inherited;
  if (AColumn = dxListColumnCOLOR) then
    AColor := ANode.Values[dxListColumnCOLORVALUE.Index];

  if (AColumn is TdxTreeListTimeColumn) then
    AText := ConvertToHourMin(AText);

  // 20014450.50
  if (AColumn = dxListColumnCURREFF) or (AColumn = dxListColumnSHIFTEFF) then
  begin
    if ORASystemDM.PSShowEmpInfo and ORASystemDM.PSShowEmpEff then
    begin
      try
        if AText <> '' then
          MyEff := StrToInt(AText)
        else
          MyEff := 0;
      except
        MyEff := 0;
      end;
      if MyEff = 0 then
      begin
        AColor := clWhite;
        AFont.Color := clBlack;
      end
      else
      begin
        if MyEff < 100 then
          AColor := clMyRed
        else
          AColor := clMyGreen;
        AFont.Color := clWhite;
      end;
      AText := AText + ' %';
    end;
  end;
end;

procedure TDialogShowAllEmployeesF.SetMyTitle(const Value: String);
begin
  FMyTitle := Value;
end;

procedure TDialogShowAllEmployeesF.SetDrawObject(
  const Value: PDrawObject);
begin
  FDrawObject := Value;
end;

procedure TDialogShowAllEmployeesF.TimerAutoCloseTimer(Sender: TObject);
begin
  inherited;
  TimerAutoClose.Enabled := False;
  Close;
end;

procedure TDialogShowAllEmployeesF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  TimerAutoClose.Enabled := False;
end;

procedure TDialogShowAllEmployeesF.FormCreate(Sender: TObject);
begin
  inherited;
  btnCancel.Visible := False;
  // 20014450.50
  // Currently not used!
//  if ORASystemDM.PSShowEmpInfo and ORASystemDM.PSShowEmpEff then
//  begin
//    dxList.Bands[6].Visible := True;
//    dxListColumnCURREFF.Visible := True;
//    dxListColumnSHIFTEFF.Visible := True;
//  end
//  else
  begin
    dxList.Bands[6].Visible := False;
    dxListColumnCURREFF.Visible := False;
    dxListColumnSHIFTEFF.Visible := False;
  end;
end;

end.
