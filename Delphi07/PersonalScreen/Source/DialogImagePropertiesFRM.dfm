object DialogImagePropertiesF: TDialogImagePropertiesF
  Left = 245
  Top = 108
  BorderStyle = bsDialog
  Caption = 'Image Properties'
  ClientHeight = 269
  ClientWidth = 313
  Color = clBtnFace
  ParentFont = True
  OldCreateOrder = True
  Position = poScreenCenter
  OnActivate = FormActivate
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 8
    Top = 8
    Width = 297
    Height = 217
    Shape = bsFrame
  end
  object Label1: TLabel
    Left = 24
    Top = 27
    Width = 58
    Height = 13
    Caption = 'Left Position'
  end
  object Label2: TLabel
    Left = 24
    Top = 51
    Width = 59
    Height = 13
    Caption = 'Top Position'
  end
  object Label3: TLabel
    Left = 24
    Top = 77
    Width = 28
    Height = 13
    Caption = 'Width'
  end
  object Label4: TLabel
    Left = 24
    Top = 103
    Width = 31
    Height = 13
    Caption = 'Height'
  end
  object lblPlantCode: TLabel
    Left = 24
    Top = 144
    Width = 52
    Height = 13
    Caption = 'Plant Code'
  end
  object lblWorkspotCode: TLabel
    Left = 24
    Top = 168
    Width = 74
    Height = 13
    Caption = 'Workspot Code'
  end
  object lblWorkspotDescription: TLabel
    Left = 24
    Top = 192
    Width = 102
    Height = 13
    Caption = 'Workspot Description'
  end
  object OKBtn: TButton
    Left = 148
    Top = 236
    Width = 75
    Height = 25
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 0
    OnClick = OKBtnClick
  end
  object CancelBtn: TButton
    Left = 231
    Top = 236
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 1
  end
  object edtLeft: TEdit
    Left = 168
    Top = 24
    Width = 121
    Height = 21
    TabOrder = 2
    Text = 'edtLeft'
  end
  object edtTop: TEdit
    Left = 168
    Top = 48
    Width = 121
    Height = 21
    TabOrder = 3
    Text = 'edtTop'
  end
  object edtWidth: TEdit
    Left = 168
    Top = 72
    Width = 121
    Height = 21
    Enabled = False
    ReadOnly = True
    TabOrder = 4
    Text = 'edtWidth'
  end
  object edtHeight: TEdit
    Left = 168
    Top = 96
    Width = 121
    Height = 21
    Enabled = False
    ReadOnly = True
    TabOrder = 5
    Text = 'edtHeight'
  end
  object edtPlantCode: TEdit
    Left = 168
    Top = 140
    Width = 121
    Height = 21
    Enabled = False
    TabOrder = 6
    Text = 'edtPlantCode'
  end
  object edtWorkspotCode: TEdit
    Left = 168
    Top = 163
    Width = 121
    Height = 21
    Enabled = False
    TabOrder = 7
    Text = 'edtWorkspotCode'
  end
  object edtWorkspotDescription: TEdit
    Left = 168
    Top = 186
    Width = 121
    Height = 21
    Enabled = False
    TabOrder = 8
    Text = 'edtWorkspotDescription'
  end
end
