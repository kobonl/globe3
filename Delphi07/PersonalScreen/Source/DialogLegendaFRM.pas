unit DialogLegendaFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BaseDialogFRM, ActnList, StdCtrls, Buttons, ComCtrls,
  ExtCtrls, HomeFRM, dxCntner, Menus, StdActns, ImgList;

type
  TDialogLegendaF = class(TBaseDialogForm)
    pnlDraw: TPanel;
    GroupBox1: TGroupBox;
    Image1: TImage;
    Label1: TLabel;
    Image2: TImage;
    Label2: TLabel;
    Image3: TImage;
    Image4: TImage;
    Image5: TImage;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Image6: TImage;
    Label6: TLabel;
    GroupBox2: TGroupBox;
    Image7: TImage;
    Label7: TLabel;
    Label8: TLabel;
    GroupBox3: TGroupBox;
    Image8: TImage;
    Label9: TLabel;
    Image9: TImage;
    Image10: TImage;
    Label10: TLabel;
    Label11: TLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DialogLegendaF: TDialogLegendaF;

implementation

{$R *.DFM}

end.
