unit UProductionScreen;

interface

uses
  ShapeEx, StdCtrls, ExtCtrls, Classes;


type
  TObjectType = (otImage, otLineVert, otLineHorz, otRectangle, otPuppetBox);

type
  TProdScreenType = (pstNoTimeRec, pstMachineTimeRec, pstWorkspotTimeRec);
  TMachineType = (mtNone, mtWorkspot, mtMachine);

type
  TShowMode = (
    ShowEmployeesOnWorkspot,
    ShowAllEmployees,
    ShowEmployeesOnWrongWorkspot,
    ShowEmployeesNotScannedIn,
    ShowEmployeesAbsentWithReason,
    ShowEmployeesAbsentWithoutReason,
    ShowEmployeesWithFirstAid,
    ShowEmployeesTooLate
  );

type
  PTARectangle = ^TARectangle;
  TARectangle = record
    ATopLineHorz: TShapeEx;
    ARightLineVert: TShapeEx;
    ABottomLineHorz: TShapeEx;
    ALeftLineVert: TShapeEx;
  end;

type
  PTMachineTimeRec = ^TMachineTimeRec;
  TMachineTimeRec = record
    AMachineCode: String;
    AMachineDescription: String;
    AJobButton: TButton;
    ACurrentJobCode: String;
    ACurrentJobDescription: String;
  end;

type
  PTWorkspotTimerec = ^TWorkspotTimeRec;
  TWorkspotTimeRec = record
    AMachineCode: String;
    AMachineDescription: String;
    AJobButton: TButton;
    ACurrentJobCode: String;
    ACurrentJobDescription: String;
    APreviousJobCode: String;
    AInButton: TButton;
{    AOutButton: TButton; }
    AModeButton: TButton;
    APercentageLabel: TLabel;
    AActualNameLabel: TLabel;
    AActualValueLabel: TLabel;
    ATargetNameLabel: TLabel;
    ATargetValueLabel: TLabel;
    ADiffNameLabel: TLabel;
    ADiffValueLabel: TLabel;
    ADiffLineHorz: TShapeEx;
    ACurrentShiftNumber: Integer;
    ACurrentShiftStart: TDateTime;
    ACurrentShiftEnd: TDateTime;
  end;

type
//  PDrawObject = ^DrawObject;
  PDrawObject = class(TObject)
    AVisible: Boolean;
    AObject: TObject;
{    APanel: TPanel; }
    AType: TObjectType;
    ADeptRect: PTARectangle;
    ALabel: TLabel;
    ALightRed: TShape;
    ALightYellow: TShape;
    ALightGreen: TShape;
    ABorder: TShape;
    AEffMeterGreen: TShape;
    AEffMeterRed: TShape;
    AEffPercentage: Integer;
    AImageName: String;
    APlantCode: String;
    AWorkspotCode: String;
    AWorkspotDescription: String;
    APuppetShiftList: TList;
    ABigEffMeters: Boolean;
    AEmployeeList: TList;
    AEfficiency: Double;
    ANormProdQuantity: Double;
    ANormProdLevel: Double;
    ATotalWorkedTime: Integer;
    ATotalProdQuantity: Double;
    ATotalProdQuantityCurrent: Double;
    ATotalNormQuantity: Double;
    ATotalPercentage: Integer;
    AEffTotalsLabel: TLabel;
    ABlinkPuppets: Boolean;
    ABigEffLabel: TLabel;
    ANoData: Boolean;
    AEmployeeCount: Integer;
    AShowMode: TShowMode;
    ABlinkCompareJobs: Boolean;
    AProdScreenType: TProdScreenType;
    AMachineTimeRec: PTMachineTimeRec;
    AWorkspotTimeRec: PTWorkspotTimerec;
    APriority: Integer;
  public
    constructor Create;
    destructor Destroy; override;
  end;


implementation

{ DrawObject }

constructor PDrawObject.Create;
begin
  inherited Create;
//
end;

destructor PDrawObject.Destroy;
begin
//
  inherited Destroy;
end;

end.
