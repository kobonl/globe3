(*
  PIM-151
  - Ghost Count handling
*)
unit UGhostCount;

interface

uses
  Windows, Messages, SysUtils, Classes, ORASystemDMT, RealTimeEffDMT,
  UPersonalScreen, UProductionScreenDefs, DB, OracleData, Oracle;

procedure ActionGhostCountCheck(var ADrawObject: PDrawObject);
procedure ActionGhostCountCheckDBMain;
procedure ActionGhostCountCheckDB(var ADrawObject: PDrawObject);
procedure ActionGhostCountCheckDBAll(APSList: TList);

var
  GhostCountInit: Boolean=False;
  GhostCountBusy: Boolean=False;

implementation

uses
  UPimsConst, UGlobalFunctions;

procedure ActionGhostCountCheck(var ADrawObject: PDrawObject);
  procedure ActionGhostCountCurrent;
  var
    AJobCodeRec: PJobCodeRec;
  begin
    ADrawObject.AGhostCountBlink := False;
    // Is there a valid current job?
    if Assigned(ADrawObject.AWorkspotTimeRec) then
      if ADrawObject.AWorkspotTimeRec.ACurrentJobCode <> '' then
        if ADrawObject.AWorkspotTimeRec.ACurrentJobCode <> NOJOB then
          Exit;
    AJobCodeRec := ADrawObject.FindJobCodeListRec(NOJOB);
    if Assigned(AJobCodeRec) then
      if AJobCodeRec.Act_prodqty_current > 0 then
        ADrawObject.AGhostCountBlink := True;
  end; // ActionGhostCountCurrent
  procedure ActionGhostCountShift;
  var
    AJobCodeRec: PJobCodeRec;
  begin
    ADrawObject.AGhostCountBlink := False;
    AJobCodeRec := ADrawObject.FindJobCodeListRec(NOJOB);
    if Assigned(AJobCodeRec) then
      if AJobCodeRec.Act_prodqty_shift > 0 then
        ADrawObject.AGhostCountBlink := True;
  end; // ActionGhostCountShift
begin
  case DetermineEfficiencyPeriodMode(ADrawObject) of
  epCurrent: // Last 5-10 minutes
    begin
      ActionGhostCountCurrent;
    end;
  epSince, epShift: // Today, Shift
    begin
      ActionGhostCountShift;
    end;
  end; // case
end; // ActionGhostCountCheck

procedure ActionGhostCountCheckDBMain;
begin
  GhostCountInit := True;
  GhostCountBusy := True;
  try
    try
      with RealTimeEffDM do
      begin
        with odsRTCurrGhostAll do
        begin
          Close;
          Open;
        end;
        with odsRTShiftGhostAll do
        begin
          Close;
          Open;
        end;
      end;
    except
      on E:EOracleError do
      begin
        WErrorLog(E.Message);
      end;
      on E:Exception do
      begin
        WErrorLog(E.Message);
      end;
    end;
  finally
    GhostCountBusy := False;
  end;
end; // ActionGhostCountCheckDBMain

procedure ActionGhostCountCheckDB(var ADrawObject: PDrawObject);
var
  ByWorkspot: Boolean;
  function MyShiftNumber: Integer;
  begin
    Result := 1;
    if Assigned(ADrawObject.AWorkspotTimeRec) then
      Result := ADrawObject.AWorkspotTimeRec.ACurrentShiftNumber;
  end; // MyShiftNumber
  function ShiftTest: String;
  begin
    Result := '';
    if MyShiftNumber <> 0 then
      Result :=
        ' AND ((SHIFT_NUMBER = ' + IntToStr(MyShiftNumber) + ') OR ' +
        ' (SHIFT_NUMBER = 0))';
  end;
  procedure ActionGhostCountCurrent;
  begin
    ADrawObject.AGhostCountBlink := False;
    with RealTimeEffDM.odsRTCurrGhostAll do
    begin
      Filtered := False;
      if ByWorkspot then
        Filter :=
          '(PLANT_CODE = ' + Quotes(ADrawObject.APlantCode) + ') AND ' +
          '(WORKSPOT_CODE = ' + Quotes(ADrawObject.AWorkspotCode) + ') ' +
          ShiftTest
      else
        Filter :=
          '(PLANT_CODE = ' + Quotes(ADrawObject.APlantCode) + ') AND ' +
          '(MACHINE_CODE = ' + Quotes(ADrawObject.AWorkspotCode) + ') ' +
          ShiftTest;
      Filtered := True;
      while (not Eof) and (not ADrawObject.AGhostCountBlink) do
      begin
        if FieldByName('QTY').AsFloat > FieldByName('QTY2').AsFloat then
          ADrawObject.AGhostCountBlink := True;
        Next;
      end; // while
    end; // with
  end; // ActionGhostCountCurrent
  procedure ActionGhostCountShift;
  begin
    ADrawObject.AGhostCountBlink := False;
    with RealTimeEffDM.odsRTShiftGhostAll do
    begin
      Filtered := False;
      if ByWorkspot then
        Filter :=
          '(PLANT_CODE = ' + Quotes(ADrawObject.APlantCode) + ') AND ' +
          '(WORKSPOT_CODE = ' + Quotes(ADrawObject.AWorkspotCode) + ') ' +
          ShiftTest
      else
        Filter :=
          '(PLANT_CODE = ' + Quotes(ADrawObject.APlantCode) + ') AND ' +
          '(MACHINE_CODE = ' + Quotes(ADrawObject.AWorkspotCode) + ') ' +
          ShiftTest;
      Filtered := True;
      while (not Eof) and (not ADrawObject.AGhostCountBlink) do
      begin
        if FieldByName('QTY').AsFloat > FieldByName('QTY2').AsFloat then
          ADrawObject.AGhostCountBlink := True;
        Next;
      end; // while
    end; // with
  end; // ActionGhostCountShift
begin
  if not GhostCountInit then
    ActionGhostCountCheckDBMain;
  if GhostCountBusy then
    Exit;

  if ADrawObject.AProdScreenType = pstWorkspotTimeRec then
    ByWorkspot := True
  else
    ByWorkspot := False; // By Machine
  case DetermineEfficiencyPeriodMode(ADrawObject) of
  epCurrent: // Last 5-10 minutes
    begin
      ActionGhostCountCurrent;
    end;
  epSince, epShift: // Today, Shift
    begin
      ActionGhostCountShift;
    end;
  end; // case
end; // ActionGhostCountCheckDB

procedure ActionGhostCountCheckDBAll(APSList: TList);
var
  ADrawObject: PDrawObject;
  I: Integer;
begin
  try
    for I := 0 to APSList.Count - 1 do
    begin
      ADrawObject := APSList.Items[I];
      ActionGhostCountCheckDB(ADrawObject);
    end;
  except
  end;
end;

end.
