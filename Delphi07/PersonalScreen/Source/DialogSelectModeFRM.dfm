inherited DialogSelectModeF: TDialogSelectModeF
  Left = 369
  Top = 310
  Width = 514
  Height = 238
  Caption = 'Employee Overview'
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlImageBase: TPanel
    Width = 498
    TabOrder = 1
    inherited imgBasePims: TImage
      Left = 381
    end
  end
  inherited stbarBase: TStatusBar
    Top = 97
    Width = 498
  end
  inherited pnlInsertBase: TPanel
    Width = 498
    Height = 60
    Font.Color = clBlack
    Font.Height = -11
    object GroupBox1: TGroupBox
      Left = 0
      Top = 0
      Width = 498
      Height = 60
      Align = alClient
      Caption = 'Select Mode'
      TabOrder = 0
      object Label1: TLabel
        Left = 8
        Top = 24
        Width = 26
        Height = 13
        Caption = 'Mode'
      end
      object cmbBoxMode: TComboBox
        Left = 88
        Top = 20
        Width = 401
        Height = 21
        ItemHeight = 13
        TabOrder = 0
        Text = 'cmbBoxMode'
        Items.Strings = (
          'Show All Employees')
      end
    end
  end
  inherited pnlBottom: TPanel
    Top = 116
    Width = 498
    inherited btnOK: TBitBtn
      Left = 152
      OnClick = btnOkClick
    end
    inherited btnCancel: TBitBtn
      Left = 266
    end
  end
end
