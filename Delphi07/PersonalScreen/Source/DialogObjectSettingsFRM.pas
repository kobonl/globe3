(*
  MRA:24-FEB-2015 20015406
  - Settings on Object-level.
  - Note: Since-time cannot be changed on object-level because of complexity.
*)
unit DialogObjectSettingsFRM;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseDialogFRM, dxCntner, Menus, StdActns, ActnList, ImgList,
  StdCtrls, Buttons, ComCtrls, ExtCtrls, dxEditor, dxExEdtr, dxEdLib;

type
  TDialogObjectSettingsF = class(TBaseDialogForm)
    GroupBox1: TGroupBox;
    GroupBox4: TGroupBox;
    Label3: TLabel;
    dxSpinEditWorkspotScalePercentage: TdxSpinEdit;
    GroupBox7: TGroupBox;
    Label8: TLabel;
    dxSpinEditFontScalePercentage: TdxSpinEdit;
    GroupBox5: TGroupBox;
    Label4: TLabel;
    rGrpMode: TRadioGroup;
    procedure btnOkClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    FWorkspotScale: Integer;
    FFontScale: Integer;
    FEffPeriodMode: Integer;
    FPictureSettings: Boolean;
    DefaultHeight: Integer;
    procedure SetFontScale(const Value: Integer);
    procedure SetWorkspotScale(const Value: Integer);
    procedure SetEffPeriodMode(const Value: Integer);
  public
    { Public declarations }
    property WorkspotScale: Integer read FWorkspotScale write SetWorkspotScale;
    property FontScale: Integer read FFontScale write SetFontScale;
    property EffPeriodMode: Integer read FEffPeriodMode write SetEffPeriodMode;
    property PictureSettings: Boolean read FPictureSettings write FPictureSettings;
  end;

var
  DialogObjectSettingsF: TDialogObjectSettingsF;

implementation

{$R *.dfm}

uses
  UTranslateStringsPS;

{ TDialogObjectSettingsF }

procedure TDialogObjectSettingsF.SetWorkspotScale(const Value: Integer);
begin
  dxSpinEditWorkspotScalePercentage.Value := Value;
  FWorkspotScale := Value;
end;

procedure TDialogObjectSettingsF.SetFontScale(const Value: Integer);
begin
  dxSpinEditFontScalePercentage.Value := Value;
  FFontScale := Value;
end;

procedure TDialogObjectSettingsF.btnOkClick(Sender: TObject);
begin
  inherited;
  WorkspotScale := Trunc(dxSpinEditWorkspotScalePercentage.Value);
  FontScale := Trunc(dxSpinEditFontScalePercentage.Value);
  EffPeriodMode := rGrpMode.ItemIndex;
end;

procedure TDialogObjectSettingsF.SetEffPeriodMode(const Value: Integer);
begin
  rGrpMode.ItemIndex := Value;
  FEffPeriodMode := Value;
end;

procedure TDialogObjectSettingsF.FormShow(Sender: TObject);
begin
  inherited;
  if PictureSettings then
  begin
    GroupBox4.Caption := STransScale;
    GroupBox7.Visible := False;
    GroupBox5.Visible := False;
    Height := DefaultHeight - GroupBox5.Height;
  end
  else
  begin
    GroupBox4.Caption := STransWorkspotScale;
    GroupBox7.Visible := True;
    GroupBox5.Visible := true;
    Height := DefaultHeight;
  end;
end;

procedure TDialogObjectSettingsF.FormCreate(Sender: TObject);
begin
  inherited;
  DefaultHeight := Height;
end;

end.
