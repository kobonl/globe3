inherited DialogDebugInfoF: TDialogDebugInfoF
  Left = 326
  Top = 162
  Width = 697
  Height = 630
  Caption = 'Dialog Debug Info'
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlImageBase: TPanel
    Width = 681
    inherited imgBasePims: TImage
      Left = 385
    end
  end
  inherited stbarBase: TStatusBar
    Top = 489
    Width = 681
  end
  inherited pnlInsertBase: TPanel
    Width = 681
    Height = 452
    Font.Color = clBlack
    Font.Height = -13
    object Memo1: TMemo
      Left = 0
      Top = 0
      Width = 681
      Height = 452
      Align = alClient
      ScrollBars = ssBoth
      TabOrder = 0
    end
  end
  inherited pnlBottom: TPanel
    Top = 508
    Width = 681
    inherited btnOk: TBitBtn
      Left = 248
    end
    inherited btnCancel: TBitBtn
      Left = 362
    end
  end
end
