(*
  MRA:23-JAN-2014 TD-24046
  - Translate issues: Use a different method to translate.
*)
unit ProductionScreenWaitFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls;

type
  TProductionScreenWaitF = class(TForm)
    lblMessage: TLabel;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ProductionScreenWaitF: TProductionScreenWaitF;

implementation

{$R *.DFM}

procedure TProductionScreenWaitF.FormCreate(Sender: TObject);
begin
  Visible := False;
end;

end.
