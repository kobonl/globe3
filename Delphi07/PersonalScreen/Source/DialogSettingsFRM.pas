(*
  MRA:16-MAR-2012 20012858. New ProductionScreen combined with datacol.
  - Added setting for datacol.
  MRA:18-JUL-2012 20012858.160.
  - Added setting RefreshEmployees. When checked then
    an automatic refresh of employees should be done, otherwise not.
    This can be needed when employees scan in/out from a different
    workstation.
  MRA:18-SEP-2012 TD-21191
  - Addition of setting to change 'ReadTimeout'. This is the time-out used
    when the counter is read in msecs. The maximum time it tries to read
    the counter. Default this is set to 5000 msecs.
  MRA:11-JAN-2013 SO-20013476
  - Addition of setting for CurrentPeriod. Default 5 minutes.
  MRA:28-MAY-2013 20014289 New look Pims
  - Some pictures/colors are changed.
  MRA:23-JAN-2014 TD-24046
  - Translate issues: Use a different method to translate.
  MRA:8-MAY-2014 TD-24849
  - Personal Screen 'Current setting' should be between 5 min and 60 min
    but 999999999 is possible
  - NOTE: To solve this, disable the editor for some spin-edit-boxes.
          Otherwise it is possible to enter a too high value.
  MRA:12-JUN-2014 20014450
  - Machine hours registration
  MRA:1-MAY-2015 20014450.50 Part 2
  - Real Time Efficiency
  - Current period-setting must not be used anymore (make invisible)
  MRA:12-OCT-2015 PIM-90
  - Extra setting for read data from external at x seconds.
  MRA:1-JUN-2016 PIM-186
  - When screen is 640x480 be sure all fits within this size.
  - Removed: Sound-alarm so the dialog can be made smaller.
  MRA:20-JUN-2016 PIM-194
  - Use 3 colors for efficiency-bars.
  MRA:28-SEP-2016 PIM-223
  - Show/hide employee efficiency on scheme-level.
*)
unit DialogSettingsFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BaseDialogFRM, StdCtrls, ActnList, Buttons, ComCtrls,
  ExtCtrls, dxCntner, dxEditor, dxExEdtr, dxEdLib, Menus, StdActns, ImgList,
  ProductionScreenDMT, UPersonalScreen, UProductionScreenDefs, UPimsConst,
  jpeg;

type
  TDialogSettingsF = class(TBaseDialogForm)
    OpenDialog1: TOpenDialog;
    pnlTop: TPanel;
    pnlClient: TPanel;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    GroupBox4: TGroupBox;
    Label3: TLabel;
    dxSpinEditWorkspotScalePercentage: TdxSpinEdit;
    GroupBox5: TGroupBox;
    Label4: TLabel;
    rGrpUsePeriod: TRadioGroup;
    grpBxSinceTime: TGroupBox;
    dxTimeEditSinceTime: TdxTimeEdit;
    Label5: TLabel;
    GroupBox6: TGroupBox;
    Label6: TLabel;
    dxSpinEditRefreshTimeInterval: TdxSpinEdit;
    Label7: TLabel;
    GroupBox7: TGroupBox;
    Label8: TLabel;
    dxSpinEditFontScalePercentage: TdxSpinEdit;
    GroupBox8: TGroupBox;
    Label9: TLabel;
    dxSpinEditDatacolInterval: TdxSpinEdit;
    Label10: TLabel;
    Label11: TLabel;
    rgrpEffQuantityTime: TRadioGroup;
    Label12: TLabel;
    dxSpinEditUpdateDBInterval: TdxSpinEdit;
    Label13: TLabel;
    Label14: TLabel;
    dxSpinEditReadDelay: TdxSpinEdit;
    Label15: TLabel;
    TimerAutoClose: TTimer;
    cBoxRefreshEmployees: TCheckBox;
    Label16: TLabel;
    Label17: TLabel;
    dxSpinEditReadTimeout: TdxSpinEdit;
    Label18: TLabel;
    Label19: TLabel;
    dxSpinEditCurrentPeriod: TdxSpinEdit;
    Label20: TLabel;
    GroupBox9: TGroupBox;
    Label21: TLabel;
    dxSpinEditMachHrsMaxIdleTime: TdxSpinEdit;
    Label22: TLabel;
    GroupBox10: TGroupBox;
    Label23: TLabel;
    dxSpinEditReadExtDataSeconds: TdxSpinEdit;
    Label24: TLabel;
    GroupBox3: TGroupBox;
    lblRed: TLabel;
    dxSpinEditRed: TdxSpinEdit;
    lblOrange: TLabel;
    dxSpinEditOrange: TdxSpinEdit;
    gBoxShowEmpInfo: TGroupBox;
    cBoxShowEmpInfo: TCheckBox;
    cBoxShowEmpEff: TCheckBox;
    procedure btnOkClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnBrowseClick(Sender: TObject);
    procedure rGrpUsePeriodClick(Sender: TObject);
    procedure TimerAutoCloseTimer(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure cBoxShowEmpInfoClick(Sender: TObject);
  private
    { Private declarations }
    FUseSoundAlarm: Boolean;
    FSoundFilename: String;
    FSoundPath: String;
    FWorkspotScale: Integer;
    FEfficiencyPeriodSince: Boolean;
    FEfficiencyPeriodSinceTime: TDateTime;
    FRefreshTimeInterval: Integer;
    FFontScale: Integer;
    FEfficiencyPeriodMode: TEffPeriodMode;
    FDatacolInterval: Integer;
    FEffQuantityTime: TEffQuantityTime;
    FWriteToDBInterval: Integer;
    FReadDelay: Integer;
    FRefreshEmployees: Boolean;
    FReadTimeout: Integer;
    FCurrentPeriod: Integer;
    FMachHrsMaxIdleTime: Integer;
    FReadExtDataAtSeconds: Integer;
    procedure SetSoundAlarm(const Value: Boolean);
    procedure SetSoundFilename(const Value: String);
    procedure SetSoundPath(const Value: String);
    procedure SetWorkspotScale(const Value: Integer);
    procedure SetEfficiencyPeriodSince(const Value: Boolean);
    procedure SetEfficiencyPeriodSinceTime(const Value: TDateTime);
    procedure SetRefreshTimeInterval(const Value: Integer);
    procedure SetFontScale(const Value: Integer);
    procedure SetEfficiencyPeriodMode(const Value: TEffPeriodMode);
    procedure SetDatacolInterval(const Value: Integer);
    procedure SetEffQuantityTime(const Value: TEffQuantityTime);
    procedure SetWriteToDBInterval(const Value: Integer);
    procedure SetReadDelay(const Value: Integer);
    procedure SetRefreshEmployees(const Value: Boolean);
    procedure SetReadTimeout(const Value: Integer);
    procedure SetCurrentPeriod(const Value: Integer);
    procedure SetMachHrsMaxIdleTime(const Value: Integer);
  public
    { Public declarations }
    property SettingUseSoundAlarm: Boolean read FUseSoundAlarm
      write SetSoundAlarm;
    property SettingSoundFilename: String read FSoundFilename
      write SetSoundFilename;
    property MySoundPath: String write SetSoundPath;
    property SettingWorkspotScale: Integer read FWorkspotScale
      write SetWorkspotScale;
    property SettingFontScale: Integer read FFontScale
      write SetFontScale;
    property SettingEfficiencyPeriodSince: Boolean read FEfficiencyPeriodSince
      write SetEfficiencyPeriodSince;
    property SettingEfficiencyPeriodSinceTime: TDateTime
      read FEfficiencyPeriodSinceTime write SetEfficiencyPeriodSinceTime;
    property SettingRefreshTimeInterval: Integer read FRefreshTimeInterval
      write SetRefreshTimeInterval;
    property EfficiencyPeriodMode: TEffPeriodMode read FEfficiencyPeriodMode
      write SetEfficiencyPeriodMode;
    property DatacolInterval: Integer read FDatacolInterval
      write SetDatacolInterval;
    property EffQuantityTime: TEffQuantityTime read FEffQuantityTime
      write SetEffQuantityTime;
    property WriteToDBInterval: Integer read FWriteToDBInterval
      write SetWriteToDBInterval;
    property ReadDelay: Integer read FReadDelay write SetReadDelay;
    property RefreshEmployees: Boolean read FRefreshEmployees
      write SetRefreshEmployees;
    property ReadTimeout: Integer read FReadTimeout write SetReadTimeout;
    property CurrentPeriod: Integer read FCurrentPeriod write SetCurrentPeriod;
    property MachHrsMaxIdleTime: Integer read FMachHrsMaxIdleTime write SetMachHrsMaxIdleTime; // 20014450
    property ReadExtDataAtSeconds: Integer read FReadExtDataAtSeconds
      write FReadExtDataAtSeconds; // PIM-90
  end;

var
  DialogSettingsF: TDialogSettingsF;

implementation

uses ORASystemDMT, UPimsMessageRes;

{$R *.DFM}

{ TDialogSettingsF }

procedure TDialogSettingsF.SetSoundAlarm(const Value: Boolean);
begin
  FUseSoundAlarm := Value;
end;

procedure TDialogSettingsF.SetSoundFilename(const Value: String);
begin
  FSoundFilename := Value;
end;

procedure TDialogSettingsF.btnOkClick(Sender: TObject);
var
  GiveWarning: Boolean;
begin
  inherited;
  GiveWarning := False;
//  SettingUseSoundAlarm := cBoxUseSoundAlarm.Checked;
//  SettingSoundFilename := edtSoundFilename.Text;
  try
    SettingWorkspotScale := Trunc(dxSpinEditWorkspotScalePercentage.Value);
  except
    SettingWorkspotScale := 50;
  end;
  try
    SettingFontScale := Trunc(dxSpinEditFontScalePercentage.Value);
  except
    SettingFontScale := 50;
  end;
  EfficiencyPeriodMode := TEffPeriodMode(rGrpUsePeriod.ItemIndex);
  SettingEfficiencyPeriodSince := (rGrpUsePeriod.ItemIndex = 1);
  SettingEfficiencyPeriodSinceTime := dxTimeEditSinceTime.Time;
  try
    SettingRefreshTimeInterval := Trunc(dxSpinEditRefreshTimeInterval.Value);
  except
    SettingRefreshTimeInterval := 5; // secs
  end;
  // Personal Screen
  try
    DatacolInterval := Trunc(dxSpinEditDatacolInterval.Value);
  except
    DatacolInterval := 500; // msecs
  end;
{
  EffQuantityTime := TEffQuantityTime(rgrpEffQuantityTime.ItemIndex);
}
  try
    WriteToDBInterval := Trunc(dxSpinEditUpdateDBInterval.Value);
  except
    WriteToDBInterval := 2; // minutes
  end;
  try
    ReadDelay := Trunc(dxSpinEditReadDelay.Value);
  except
    ReadDelay := 50; // msecs
  end;
  RefreshEmployees := cBoxRefreshEmployees.Checked;
  try
    ReadTimeout := Trunc(dxSpinEditReadTimeout.Value);
  except
    ReadTimeout := 5000; // msecs
  end;
  // SO-20013476
  // 20014450.50
  if dxSpinEditCurrentPeriod.Visible then
  begin
    try
      if CurrentPeriod <> Trunc(dxSpinEditCurrentPeriod.Value) then
        GiveWarning := True;
      // TD-24849 Test for valid value is done in Setter
      CurrentPeriod := Trunc(dxSpinEditCurrentPeriod.Value);
      dxSpinEditCurrentPeriod.Value := CurrentPeriod;
    except
      CurrentPeriod := 5; // minutes
    end;
  end;
  // 20014450
  try
    MachHrsMaxIdleTime := Trunc(dxSpinEditMachHrsMaxIdleTime.Value);
  except
    MachHrsMaxIdleTime := 5; // minutes
  end;
  if GiveWarning then
  begin
    MessageDlg(SPimsChangeSettingWarning, mtWarning, [mbOk], 0);
//    DisplayMessage(SPimsChangeSettingWarning, mtWarning, [mbOk]);
  end;
  // PIM-90
  try
    ReadExtDataAtSeconds := Trunc(dxSpinEditReadExtDataSeconds.Value);
    if not ((ReadExtDataAtSeconds >= 0) and (ReadExtDataAtSeconds <= 59)) then
      ReadExtDataAtSeconds := DEFAULT_READ_EXT_DATA_AT_SECONDS;
  except
    ReadExtDataAtSeconds := DEFAULT_READ_EXT_DATA_AT_SECONDS;
  end;
  try
    // PIM-194
    ORASystemDM.RedBoundary := Trunc(dxSpinEditRed.Value);
    ORASystemDM.OrangeBoundary := Trunc(dxSpinEditOrange.Value);
  except
    ORASystemDM.RedBoundary := RED_BOUNDARY;
    ORASystemDM.OrangeBoundary := ORANGE_BOUNDARY;
  end;
  // PIM-223
  ORASystemDM.PSShowEmpInfo := cBoxShowEmpInfo.Checked;
  ORASystemDM.PSShowEmpEff := cBoxShowEmpEff.Checked;
  if not ORASystemDM.PSShowEmpInfo then
    ORASystemDM.PSShowEmpEff := False;
end;

procedure TDialogSettingsF.FormShow(Sender: TObject);
begin
  inherited;
  rGrpUsePeriodClick(Sender);
//  cBoxUseSoundAlarm.Checked := SettingUseSoundAlarm;
//  edtSoundFilename.Text := SettingSoundFilename;
  dxSpinEditWorkspotScalePercentage.Value := SettingWorkspotScale;
  dxSpinEditFontScalePercentage.Value := SettingFontScale;
{  if SettingEfficiencyPeriodSince then
    rGrpUsePeriod.ItemIndex := 1
  else
    rGrpUsePeriod.ItemIndex := 0; }
  rGrpUsePeriod.ItemIndex := Integer(FEfficiencyPeriodMode);
  dxTimeEditSinceTime.Time := SettingEfficiencyPeriodSinceTime;
  dxSpinEditRefreshTimeInterval.Value := SettingRefreshTimeInterval;
  // Personal Screen
  dxSpinEditDatacolInterval.Value := DatacolInterval;
{  rgrpEffQuantityTime.ItemIndex := Integer(EffQuantityTime); }
  dxSpinEditUpdateDBInterval.Value := WriteToDBInterval;
  dxSpinEditReadDelay.Value := ReadDelay;
  // 20013176
  TimerAutoClose.Interval := ORASystemDM.AutoCloseInterval;
  if TimerAutoClose.Interval > 0 then
    TimerAutoClose.Enabled := True;
  cBoxRefreshEmployees.Checked := RefreshEmployees;
  // TD21191
  dxSpinEditReadTimeout.Value := ReadTimeout;
  // SO-20013476
  dxSpinEditCurrentPeriod.Value := CurrentPeriod;
  // 20014450
  dxSpinEditMachHrsMaxIdleTime.Value := MachHrsMaxIdleTime;
  // 20014450.50 Make Current Period invisible here
  Label19.Visible := False;
  dxSpinEditCurrentPeriod.Visible := False;
  Label20.Visible := False;
  // PIM-90
  dxSpinEditReadExtDataSeconds.Value := ReadExtDataAtSeconds;
  // PIM-194
  dxSpinEditRed.MaxValue := MAX_COLOR_BOUNDARY;
  dxSpinEditRed.MinValue := MIN_COLOR_BOUNDARY;
  dxSpinEditOrange.MaxValue := MAX_COLOR_BOUNDARY;
  dxSpinEditOrange.MinValue := MIN_COLOR_BOUNDARY;
  dxSpinEditRed.Value := ORASystemDM.RedBoundary;
  dxSpinEditOrange.Value := ORASystemDM.OrangeBoundary;
  // PIM-223
  cBoxShowEmpInfo.Checked := ORASystemDM.PSShowEmpInfo;
  cBoxShowEmpEff.Checked := ORASystemDM.PSShowEmpEff;
  cBoxShowEmpInfoClick(Sender);
end;

procedure TDialogSettingsF.SetSoundPath(const Value: String);
begin
  FSoundPath := Value;
end;

procedure TDialogSettingsF.btnBrowseClick(Sender: TObject);
var
  Path: String;
  OtherPath: String;
begin
  inherited;
  Path := GetCurrentDir;
  try
    OpenDialog1.InitialDir := FSoundPath;
    OpenDialog1.Filename := SettingSoundFilename;
    if OpenDialog1.Execute then
    begin
//      SettingSoundFilename := ExtractFileName(OpenDialog1.Filename);
//      edtSoundFilename.Text := SettingSoundFilename;
      // If file came from another path, copy the file to
      // SoundPath
      OtherPath := ExtractFilePath(OpenDialog1.Filename);
      if OtherPath <> FSoundPath then
        CopyFile(PChar(OtherPath + SettingSoundFilename),
          PChar(FSoundPath + SettingSoundFilename), False);
    end;
  finally
    SetCurrentDir(Path);
  end;
end;

procedure TDialogSettingsF.SetWorkspotScale(const Value: Integer);
begin
  FWorkspotScale := Value;
end;

procedure TDialogSettingsF.SetEfficiencyPeriodSince(const Value: Boolean);
begin
  FEfficiencyPeriodSince := Value;
end;

procedure TDialogSettingsF.SetEfficiencyPeriodSinceTime(
  const Value: TDateTime);
begin
  FEfficiencyPeriodSinceTime := Value;
end;

procedure TDialogSettingsF.rGrpUsePeriodClick(Sender: TObject);
begin
  inherited;
  dxTimeEditSinceTime.Enabled := (rGrpUsePeriod.ItemIndex = 1);
end;

procedure TDialogSettingsF.SetRefreshTimeInterval(const Value: Integer);
begin
  FRefreshTimeInterval := Value;
end;

procedure TDialogSettingsF.SetFontScale(const Value: Integer);
begin
  FFontScale := Value;
end;

procedure TDialogSettingsF.SetEfficiencyPeriodMode(
  const Value: TEffPeriodMode);
begin
  rGrpUsePeriod.ItemIndex := Integer(Value);
  FEfficiencyPeriodMode := Value;
end;

procedure TDialogSettingsF.SetDatacolInterval(const Value: Integer);
begin
  dxSpinEditDatacolInterval.Value := Value;
  FDatacolInterval := Value;
end;

procedure TDialogSettingsF.SetEffQuantityTime(const Value: TEffQuantityTime);
begin
{  rgrpEffQuantityTime.ItemIndex := Integer(Value); }
  FEffQuantityTime := Value;
end;

procedure TDialogSettingsF.SetWriteToDBInterval(const Value: Integer);
begin
  dxSpinEditUpdateDBInterval.Value := Value;
  FWriteToDBInterval := Value;
end;

procedure TDialogSettingsF.SetReadDelay(const Value: Integer);
begin
  dxSpinEditReadDelay.Value := Value;
  FReadDelay := Value;
end;

procedure TDialogSettingsF.TimerAutoCloseTimer(Sender: TObject);
begin
  inherited;
  TimerAutoClose.Enabled := False;
  Close;
end;

procedure TDialogSettingsF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  TimerAutoClose.Enabled := False;
end;

procedure TDialogSettingsF.SetRefreshEmployees(const Value: Boolean);
begin
  cBoxRefreshEmployees.Checked := Value;
  FRefreshEmployees := Value;
end;

procedure TDialogSettingsF.SetReadTimeout(const Value: Integer);
begin
  dxSpinEditReadTimeout.Value := Value;
  FReadTimeout := Value;
end;

procedure TDialogSettingsF.SetCurrentPeriod(const Value: Integer);
var
  NewValue: Integer;
begin
  // TD-24849
  NewValue := Value;
  if NewValue < 5 then
    NewValue := 5
  else
    if NewValue > 60 then
      NewValue := 60;
  dxSpinEditCurrentPeriod.Value := NewValue;
  FCurrentPeriod := NewValue;
end;

procedure TDialogSettingsF.FormCreate(Sender: TObject);
begin
  inherited;
  lblMessage.Caption := ' ' + Caption; // 20014289
  // 20014450
  GroupBox9.Visible := ORASystemDM.RegisterMachineHours; // 20014450
  if not GroupBox9.Visible then
    Height := Height - GroupBox9.Height;
  // 20014450.50 Set colors to windows-colors.
//  btnBrowse.BackColor := GroupBox3.Color;
//  btnBrowse.ForeColor := GroupBox3.Font.Color;
end;

// 20014450
procedure TDialogSettingsF.SetMachHrsMaxIdleTime(const Value: Integer);
var
  NewValue: Integer;
begin
  NewValue := Value;
  if NewValue < 5 then
    NewValue := 5
  else
    if NewValue > 999 then
      NewValue := 999;
  dxSpinEditMachHrsMaxIdleTime.Value := NewValue;
  FMachHrsMaxIdleTime := NewValue;
end;

// PIM-223
procedure TDialogSettingsF.cBoxShowEmpInfoClick(Sender: TObject);
begin
  inherited;
  if cBoxShowEmpInfo.Checked then
  begin
    cBoxShowEmpEff.Enabled := True;
  end
  else
  begin
    cBoxShowEmpEff.Checked := False;
    cBoxShowEmpEff.Enabled := False;
  end;
end;

end.
