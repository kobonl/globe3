(*
  MRA: 8-JUN-2009. RV029.
    Use an ProductionScreen.INI-file to get the lastscheme-name from a
    section that is the computername, instead of getting this from registry.
    Note: Read/write all settings from/to this INI-file.
  MRA:JUN-2010. RV063.4. Order 550478. Personal Screen.
  - Addition of Machine/Workspot + Time recording.
  MRA:29-JUL-2010. RV063.5. Fix for current shown quantities: Mode=Current.
  - When employee just switched job, the shown quantities must still
    be based on previous job! Because the quantities are always from the
    previous period of the datacollection, because the actual period
    is not available yet.
    Rules: (when 'Current' is used).
    - When employee first scans in on a job:
      - There is no previous job, so it does not matter.
    - When employee switches job:
      - The previous period must be based on previous job, but
        the current shown job (in job-button) must show the new job.
        - The previous job should be determined by searching for the scan
          that is available in this previous period.
    - When employee finished his job (end-of-day):
      - The previous period must be based on job of last scan.
  MRA:28-SEP-2010. RV064.1. Bugfix.
  - Production Screen used as Personal Screen:
    - When used on 2 workstations it can happen a scan a new open
      scan is created, while there is already an open scan for
      the same employee.
    - Added a procedure that will check if there was interference.
      This is logged to a file.
    - Menu-option added named 'Show Status Info' that shows the
      info about the scan-interference.
  MRA:21-JAN-2011 RV075.1. Small change.
  - Production Screen 2.0.153.29
    - For Workspot/Timerecording-component:
      Lower part below job-button some pixels to make
      room for the select-rectangle.
    - JobButton is now placed in a different way.
  MRA:16-MAR-2012 20012858. New ProductionScreen combined with datacol.
  - Based on ProductionScreen a new project is made named: PersonalScreen.
  - All that is not needed is left out here or disabled.
  MRA:21-JUN-2012 20012858.70 Rework
  - When a blackbox is not available, which can happen when the Adam-device
    is turned off, or when IP-address is wrong or something else,
    then disable this blackbox, so it will be skipped.
    This prevents the program freezes.
  MRA:22-JUN-2012 20013379
  - Compare Jobs Handling.
    - Detect if a workspot has comparison  jobs
      - If so, then look for the first job with a norm for this workspot
        - When (during start) no-one is currently scanned in on this workspot:
          - Use this job as current job when the application is started.
          - Scan a fake-employee in to make it possible to calculate efficiency:
  - IMPORTANT: This fake-employee must however exist in database.
               Also the scan must exist.
               This means this fake-employee must be created if not exist yet.
               There can be multiple fake-employees!
               The scan should be created once for that day, and will never be
               closed.
               A check must be done if the scan does not exist yet,
               when the application is restarted on the same day.
  MRA:27-JUN-2012 Small change.
  - Change font when job-description is too long to show in Job-button.
  MRA:28-JUN-2012 20013379 Compare jobs
  - When a workspot is of type compare-workspot, then for the compare-job
    it should be possible to register countings without the need to have
    a real employee and real open scan.
  - This means:
    - Do not show an employee for this compare-workspot
    - Do not create an employee/scan in database for this compare-workspot
    - Use the compare-job of the compare-workspot as current job for
      that workspot
    - Use the norm of this compare-job for calculating the efficiency
  MRA:2-JUL-2012 20012858.80. Reconnect/Crash test.
  - When Adam-device is turned off before or during the run of the application,
    then it disables the devices for reading, but when closing the application,
    it still tries to close the socket-ports, resulting in access violations,
    and the application hangs.
  MRA:17-JUL-2012 20012858.150. Multi socketports
  - Use a list of socketports, for each unique host + port combination.
    In that way it has not have to assign the socketport, each
    time the host + port is different.
  MRA:18-JUL-2012 20012858.160.
  - Added setting RefreshEmployees. When checked then
    an automatic refresh of employees should be done, otherwise not.
    This can be needed when employees scan in/out from a different
    workstation.
  MRA:28-AUG-2012 TODO 21191
  - Prevent it is reading a wrong counter-value.
  - Prevent a second call to MyReadCounter by checking if TimerWaitForRead
    is already enabled.
  - Some SocketPort-function now return a boolean to indicate it could
    access the port or not.
  - IPExists does not work when user has no admin-rights. This function is now
    disabled.
  MRA:31-AUG-2012 2.0.164.053.1
  - When no-one is scanned in on a workspot, do not show efficiency.
  MRA:31-AUG-2012 2.0.164.053.2
  - When Refresh-button is clicked multiple times the application crashes.
  MRA:31-AUG-2012 TODO 21191
  - Changed SocketPortFind, to prevent it gives an error when Port is
    empty using StrToInt.
  - Empty ReadBuffer before the call to MyReadCounter.
  MRA:3-SEP-2012 TODO 21191
  - Refresh-action gives errors when used several times!
  MRA:4-SEP-2012 TODO 21191
  - When received counter value is 0, do not accept it!
  MRA:6-SEP-2012 TODO 21191
  - The MainTimer that was used to refresh the workspots shown in screen,
    disabled and enabled the timer used for reading the counters.
    Normally this did give no problem, but when there are connection-problems
    with the blackboxes, then it resulted in not going to the next counter,
    because it reentered the procedure without doing the rest of the code
    that followed the read-action.
    This was the cause it was not possible anymore to do anything else.
    To solve this: The actions for the timers are now re-arranged.
  MRA:10-SEP-2012 TODO-21191
  - Use check-digits for reading blackboxes.
  - An extra field 'Checkdigits' must be added for this purpose. It can
    be left empty. It must be in the range from '10' till
    '99' (and always 2 positions).
  MRA:11-SEP-2012 Bugfix related to 20013516.
  - For 'down'-job: It did not show the description on the job-button,
    instead of that it showed 'not-scanned-in'.    
  MRA:18-SEP-2012 TD-21191
  - Addition of setting to change 'ReadTimeout'. This is the time-out used
    when the counter is read in msecs. The maximum time it tries to read
    the counter. Default this is set to 5000 msecs.
  MRA:16-NOV-2012 TD-21429 Related to this order.
  - Position sub-dialogs in a better way.
  MRA:13-DEC-2012 20013489 Overnight Shift - Rework
  - When current time is before since time, then we must not look at yesterday!
    Or it will give too many hours. It also will give too many scans read
    when determining the 'employeelist'.
    It also can result in wrong shift-date for PQ-records!
    For example, if sincetime is 7:00 and current time is 5:00 then
    it looked at yesterday!
    To solve this situation, use current as since-time.
  MRA:2-JAN-2013. TD-21799.
  - Translations to Dutch.
  MRA:3-JAN-2012 20013489.10. Overnight-Shift-System.
  - When called from Timerecording or Personal Screen, only add the
    production hours, do no recalculate them!
  MRA:11-JAN-2013 SO-20013476
  - Addition of setting for CurrentPeriod. Default 5 minutes.
  MRA:11-MAR-2013 TD-21738 Bugfix.
  - Refresh emps gives wrong target for today.
    - It did recreate the emp-list, but it did not updated the screen,
      so nothing was shown (no employees).
  MRA:28-MAY-2013 20014289 New look Pims
  - Some pictures/colors are changed.
  MRA:31-OCT-2013 20014722
  - Export Job-change to file
  - Additional: Removed some logging.
  MRA:19-NOV-2013 TD-23620
  - Adjustments needed for screen size. When screen is too small, buttons
    are not all shown. A mimimum size should be 640 x 480.
  MRA:23-JAN-2014 TD-24046
  - Translate issues: Use a different method to translate.
  MRA:23-APR-2014 TD-24736.60 Rework
  - Make solution to change format of date/time optional based on
    setting named DateTimeFormatPatch. Default False (0).
  MRA:6-MAY-2014 TD-24819
  - Personal Screen and Comparison Jobs must use NOJOB for initial value
  MRA:12-MAY-2014 SO-20015330
  - Automatic reset of counters.
  MRA:30-MAY-2014 20015178
  - Measure Performance without employees
  MRA:12-JUN-2014 20014450
  - Machine hours registration
  MRA:18-AUG-2014 20015178.90 Rework
  - When there is no job yet and the first job is selected, it is not always
    created.
  - Cause: It looked till 'Now' for scans, but when a scan is created at
           e.g. 50 seconds after, then it will find nothing because the scan
           was rounded to the upper minute, and the current time (Now)
           is then BEFORE the scan-date.
           Example: Now=13:40:50 DateIn=13:41:00. This will not be found!
   - Solution: Add 30 seconds to current time and then round it, use
               that as 'till' time.
  MRA:22-AUG-2014 TD-25118 Edit Button Issue
  - When selecting File->New then the edit-buttons are shown but you cannot
    move any objects in the scheme, only after you click the edit-button
    you can do this: It should be 'down' before it works!
  MRA:8-SEP-2014 20013476
  - Make current variable, based on job-field SAMPLE_TIME_MINS.
  MRA:30-SEP-2014 TD-25679
  - Change default value for 'Update DB Interval', from 2 to 1 (minutes).
  MRA:6-OCT-2014 20015376
  - Link Job Code Workspot - Show actual quantities
  MRA:10-NOV-2014 20014826
  - Log error messages optionally to database.
  - Related to this order: Add property Debug (True or False) that can be
    switched on/off via INI-file.
  MRA:5-DEC-2014 SO-20015406
  - Add option to set workspot-scale and font-scale per workspot and
    efficiency-period-mode
  MRA:23-DEC-2014 SO-20016016
  - Make it possible on workspot-level to read data (refresh) from database
    instead of from blackboxes.
  MRA:19-FEB-2015 TD-26709 (MAL)
  - Personal Screen: Job-button gives no change of job
  - Added extra logging to see what happens during a job-change (for Debug=1)
  - Result: The customer (MAL) had added 3 plants but had not shifts added.
    When it cannot find shifts, it will also not find the scans after clicking
    job-button.
  MRA:6-MAR-2015 20015406.80 Rework
  - Allow Object.FontScale to be 0.
  - When 0 then do not show Actual/Target-info.
  MRA:10-MAR-2015 TD-26329
  - Show a message when the check-digit is wrong.
  MRA:16-MAR-2015 20015346
  - Store scans in seconds
  - Cut off time and breaks handling
  - Creation-date and Mutation-date handling for scans
  - Addition of 2 Pims-Settings:
    - TimerecordingInSecs
    - IgnoreBreaks
  MRA:18-MAR-2015 20015406.90 Rework
  - There was a problem when using machines in a scheme.
  MRA:24-APR-2015 20016447
  - Improvement Machine-workspots functionality
    - Allow workspot-job-changes (different from machine-job)
      - Show a color when that happens
    - Keep track of last machine-job
    - Related to this:
      - Do not show red rectangle when not
        in edit-mode
  MRA:1-MAY-2015 20014450.50 Part 2
  - Real Time Efficiency
  - Do not use current period on scheme-level or on job-level anymore,
    but use ADrawObject.ARealTimeIntervalMinutes for that purpose which
    can be set on Plant-level.
  - Always use calculate efficiency based on time (not on quantity)
  - Related to this order:
    - Also look if there were quantities during current, shift, since
      when calculating eff. based on time.
  - Related to this order:
    - It did not switch times off/during during In/Out-button handling,
      resulting in sometimes not showing all employees that scanned in/out.
      - ShowDetails was already called before finishing the In/Out.
  - Use other colors
  - Use ButtonWithColor to get a colored TBitBtn
  - Use ColorButton to get a colored TButton
  MRA:1-JUN-2015 PIM-42
  - Trap errors when writing to a file.
  MRA:8-JUN-2015 ABS-8116
  - Do not show/log error when workspot is defined as 'receive qty from
    external app'.
  MRA:22-JUL-2015 PIM-50
  - Add Job Comment
  MRA:23-SEP-2015 PIM-87
  - Add and assign debug-variable for GlobalDMT
  MRA:25-SEP-2015 PIM-12
  - When 'Refresh Employees' is handled it gives for a short time
    wrong values for Actual/Target/Efficiency.
  - When no-one is scanned in anymore then show 0.
  - Machine-workspot-changes: (related to 20016447)
    - When End-Of-Day is clicked on machine-level it did not refresh the
      employee-list at that moment, but 30 secs. later.
    - When changing job on machine-level, it also changed the workspots
      that were not the same as the machine-job.
    - When changing jobs on machine-level it showed temporarily red colors
      for the buttons.
  MRA:28-SEP-2015 PIM-12
  - Show optionally Break- Lunch-Button (Pers. Screen + TimeRecording),
    End-Of-Day-Button (TimeRecording), based on system settings.
  - Problem with calculating percentage based on time for current!
  MRA:12-OCT-2015 PIM-90
  - Read data from external application, extra setting added to determine
    at what seconds it should read this.
  MRA:22-OCT-2015 PIM-12.2
  - Bugfixing for linked-jobs. After all the changes for PIM-12 the link-jobs
    functionality goes wrong:
    - It did not store the quantitities to the WorkspotEffPerMinute-table
      (new functionality)
    Other problems for link-job-workspot:
    - The refresh of employees goes wrong!
    - It does not show the efficiency
    - It does not show Actual/Target
  - Related to this:
    - Do not switch off/on timers when showing some dialogs that show data
      to prevent the blackboxes give problems
  MRA:23-DEC-2015 PIM-12.3
  - Problem with access violations
  - When using 'auto refresh employees' then when someone is first scanned in
    in PS and later on scanned to another job via TR or another PS it still
    showed the last job instead (when no-one is scanned in anymore to the WS).
  MRA:28-DEC-2015 PIM-12.3
  - Problems with access violations
  - As a work-around a global exception handler is added to prevent it
    shows error-message-dialogs. Instead of that any global errors are logged.
  MRA:29-DEC-2015 PIM-111
  - Machine-Workspots bugfixing. When job is a down-job it shows 'Continue'
    when going back to previous job via Continue-button for machine.
  MRA:30-DEC-2015 PIM-116
  - During edit-mode it gives an access-violation when deleting a workspot.
  - IMPORTANT: Before 'free' of a TButton, be sure it is non-visible!
  -            Something to do with TColorButton?
  - To be sure: Before any component is freed, it is first made invisible.
  MRA:13-JAN-2016 ABS-18715
  - No Employee
  - There was a rounding-problem with scans resulting in no-employee in report
    production details.
  MRA:24-FEB-2016 PIM-12
  - Bugfix: The real-time-eff-table (workspoteffperminute) was not filled
    correct! It resulted in too high qty-values!
    To solve it, it is now done in a similar way as WriteToDB, by using the
    qty-to-store-list to store the quantities.
  - REMARK: The WriteToDB is now done after each whole minute, and when
    after closing/ (and other actions) the application.
  MRA:3-MAR-2016 PIM-147
  - When multiple employees are currently working on the same workspot:
    - When an employee is currently scanned in on a different job:
      - Show alternatively name/job in an interval of about 2 seconds
  - When multiple employees work on 1 workspot then determine
    the current job based on the most used.
  - When changing job on workspot-level via job-button:
    - When there are more than 1 employees scanned in, only change the job
      for employees that worked on the old job. This prevents an employee
      who has an individual break is also changed to the new job.
    - NOTE: Do not do the above for Down, Break, Lunch and End-of-day.
  MRA:4-MAR-2016 PIM-125
  - When no one is scanned in anymore, do not log errors.
  MRA:7-MAR-2016 PIM-111
  - Also read Down-jobs for job-list, to prevent it does show
    not-active when last job was a down-job and PS was restarted.
  - Do not add missing jobs (like down-jobs) here, this is already done
    during 'read all'.
  - When machine-workspots are used: When machine is changed to down-job, then
    switch all workspots also to this down-job, not only the ones that were
    equal to current job! It is assumed that all is down then.
  - When machine-workspots are used: When all workspots are changed to a
    different job (compared with machine-job) then when machine-job is currently
    down, it does not switch to previous job via continue-button. Solve this
    by taking the first available workspot-job.
  - Do not change a workspot to down (via machine) when it is already down.
  MRA:9-MAR-2016 PIM-111
  - For machine-workspot functionality:
    - It gave a wrong result when 1 workspot had already a different job and
      the second worked became different because of employees changing jobs
      via In/Out-button, then the colors were wrong, although it showed all
      jobs equal.
  MRA:18-MAR-2016 PIM-147
  - Show employee/job functionality.
  - Also show this when no counters are defined.
  - Show job in red.
  - Use TimerBlink to set the toggle for name/job. This is now enabled (was
    not used anymore before that).
  MRA:18-MAR-2016 PIM-155
  - Fake Employee-handling rework:
  - It gave an error when first scan was made, reason: When no scan was made
    yet for the day, it tried to add this twice.
  - Changes were needed to use the already existing routines instead of
    extra routines that gave problems because they were missing functionality
    that were added later. These routines are about:
    - Add the first open scan (at start-of-the-day) for the fake-employee
    - Update the internal list of employees (to ensure it is updated correct
      in the screen).
  MRA:29-MAR-2016 PIM-111
  - Machine Workspot handling:
    - It did not show the correct colors after clicking Refresh-button.
  MRA:2-MAY-2016 PIM-151
  - Show Ghost Counters
  MRA:1-JUN-2016 PIM-186
  - When screen is 640x480 be sure all fits within this size.
  MRA:20-JUN-2016 PIM-194
  - Use 3 colors for efficiency-bars.
  MRA:28-SEP-2016 PIM-223
  - Show/hide employee efficiency on scheme-level.
  MRA:15-NOV-2016 PIM-213
  - New screens showing per workspot the efficiency and how the
    operator performs.
*)
unit HomeFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ORASystemDMT, ShapeEx, Buttons, ToolWin, ComCtrls, ExtCtrls, Variants,
  ImgList, Menus, IniFiles, dxTL, StdCtrls, ActnList,
  Db, Registry, FileCtrl, MPlayer, {UProductionScreenConst, }PlanScanEmpDMT,
  {Grids, DBGrids, }{,} UScannedIDCard, BaseTopLogoFRM,
  StdActns, dxCntner, OracleData, Oracle,
  ProductionScreenDMT, TimeRecordingDMT, TimeRecordingFRM, PersonalScreenDMT,
  {UProductionScreen} UPersonalScreen, USocketPort, IdBaseComponent,
  IdComponent, IdTCPConnection, IdTCPClient, IdTelnet, IdException,
  UProductionScreenDefs, DateUtils, UPimsConst, dxfProgressBar, UWorkspotEmpEff,
  jpeg;

const
  LOGFILENAME='PSLOG';
  ERRORLOGFILENAME='PSERRORLOG';
  DEFAULT_WORKFILENAME='Pims.ini';
  RECTANGLE=8;
  IMAGE_SIZE=64;
  LIGHT_SIZE=13;
  MAXOBJECTS=99;
  BIGEFFMETERS_SCALE_PERCENTAGE=350;
  IMAGEPATH_DEFAULT='c:\temp\pims\';
  SCHEMEPATH_DEFAULT='c:\temp\';
  SOUNDPATH_DEFAULT='c:\temp\';
  EXPORTPATH_DEFAULT='c:\temp\';
  PICTUREPATH_DEFAULT='c:\temp\';
  DEFAULT_REFRESH_TIME_INTERVAL=5; // Time screen refreshes, in seconds
  DEFAULT_DATACOL_TIME_INTERVAL=300; // seconds
  DEFAULT_MULTIPLIER=2;
  MAX_PUPPET_PARTS=2; // was 5
  ProdScreenINIFilename='PersonalScreen.INI';
  DEFAULT_READ_COUNTER_INTERVAL=500; // Time in ms to read counter using timer
  DEFAULT_WRITETOFILE_INTERVAL = 15000;
  DEFAULT_WRITETODB_INTERVAL = 1; // In minutes! // TD-25679 From 2 -> 1
  DEFAULT_READDELAY = 10; // Socket-port-readdelay in ms.
  DEFAULT_REFRESH_EMPLOYEES = False; // RefreshEmployees
  DEFAULT_READTIMEOUT = 5000; // TD-21191
  DEFAULT_CURRENTPERIOD = 5; // SO-20013476
  EXPORTJOBCHANGE_INI_FILENAME='EXPORTJOBCHANGE.INI';
  MaxWidth = 640; // TD-23620
  DEFAULT_MACHHRSMAXIDLETIME = 5; // 20014450
  FONT_SIZE_FACTOR=2.6; // 20014450.50 Factor for resizing
  WS_EFF_CURRENTPERIOD = 15; // PIM-213

type
  TMyAction = procedure of object;

// PersonalScreen
(*
type
  PTPuppet = ^TPuppet;
  TPuppet = record
    Puppet: array[0..MAX_PUPPET_PARTS] of TShape;
    AColor: TColor;
    AEmployeeNumber: Integer;
    AEmployeeDescription: String;
    AEmployeeShortName: String;
    AEmployeeTeamCode: String;
    AEmpLevel: String;
    AScannedWS: String;
    AScannedDateTimeIn: TDateTime;
    APlannedWS: String;
    APlannedStartTime: TDateTime;
    APlannedEndTime: TDateTime;
    AEPlanLevel: String;
    AScanned: Boolean; // has employee scanned in (TimeRegScanning)
    AJobCode: String; // if employee is scanned in, what's his jobcode?
  end;

type
  PTPuppetShift = ^TPuppetShift;
  TPuppetShift = record
    APuppetList: TList;
    AShiftNumber: Integer;
  end;
*)

// For use during temporary creation of employeelist to ensure
// unique employees are taken.
type
  PTTempEmployee = ^TTempEmployee;
  TTempEmployee = record
    AEmployeeNumber: Integer;
  end;

// 20014450.50 Not used anymore
(*
// For use in DrawObject: AEmployeeList
// Shows employees under BIG-EFF-METER
type
  PTAEmployee = ^TAEmployee;
  TAEmployee = record
    AEmployeeNumber: Integer;
    AEmployeeNameLabel: TLabel;
    AWorkedMinutes: Integer;
    AWorkedMinutesLabel: TLabel;
    AWorkedPieces: Integer;
    AWorkedPiecesLabel: TLabel;
    ADateTimeOut: TDateTime;
  end;
*)

type
  THomeF = class(TBaseTopLogoForm)
    SaveDialog1: TSaveDialog;
    OpenDialog1: TOpenDialog;
    pnlDraw: TPanel;
    TimerMain: TTimer;
    PopupMainMenu: TPopupMenu;
    ShowAllEmployees1: TMenuItem;
    ShowEmployeesonwrongWokspot1: TMenuItem;
    ShowEmployeesnotscannedin1: TMenuItem;
    ShowEmployeesabsentwithreason1: TMenuItem;
    ShowEmployeesabsentwithoutreason1: TMenuItem;
    TimerBlink: TTimer;
    PopupWorkspotMenu: TPopupMenu;
    ShowChart: TMenuItem;
    ShowEmployees1: TMenuItem;
    MediaPlayer1: TMediaPlayer;
    TimerStatus: TTimer;
    ShowEmployeeswithFirstAid1: TMenuItem;
    ShowEmployeestoolate1: TMenuItem;
    lblHr: TLabel;
    lblPcs: TLabel;
    lblNormPcs: TLabel;
    ProductionScreenActionList: TActionList;
    actWorkspot: TAction;
    actEffiMeter: TAction;
    actLineHorz: TAction;
    actLineVert: TAction;
    actRectangle: TAction;
    actPuppetBox: TAction;
    actDelete: TAction;
    actSave: TAction;
    actEdit: TAction;
    tlbarGrid: TToolBar;
    tlbtnWorkSpot: TToolButton;
    tlbtnEffMeter: TToolButton;
    tlbtnLineHorz: TToolButton;
    tlbtnLineVert: TToolButton;
    tlbtnRectangle: TToolButton;
    tlbtnPuppetBox: TToolButton;
    tlbtnDelete: TToolButton;
    tlbtnSave: TToolButton;
    tlbtnEdit: TToolButton;
    actNew: TAction;
    actOpen: TAction;
    actSaveAs: TAction;
    New1: TMenuItem;
    Save1: TMenuItem;
    Open1: TMenuItem;
    SaveAs1: TMenuItem;
    N1: TMenuItem;
    HelpLegendaAct: TAction;
    Legenda1: TMenuItem;
    actShowStatusInfo: TAction;
    ShowStatusInfo1: TMenuItem;
    IdTelnet1XXX: TIdTelnet;
    TimerReadCounter: TTimer;
    TimerWaitForRead: TTimer;
    TimerWriteToDB: TTimer;
    TimerWriteToFile: TTimer;
    pnlBottom: TPanel;
    pnlButtonsRight: TPanel;
    pnlButtonsLeft: TPanel;
    actSaveLog: TAction;
    SaveLog1: TMenuItem;
    pnlBlackboxes: TPanel;
    pnlBB1: TPanel;
    pnlBB16: TPanel;
    pnlBB15: TPanel;
    pnlBB14: TPanel;
    pnlBB13: TPanel;
    pnlBB12: TPanel;
    pnlBB11: TPanel;
    pnlBB10: TPanel;
    pnlBB9: TPanel;
    pnlBB8: TPanel;
    pnlBB7: TPanel;
    pnlBB6: TPanel;
    pnlBB5: TPanel;
    pnlBB4: TPanel;
    pnlBB3: TPanel;
    pnlBB2: TPanel;
    lblErrorMessage: TLabel;
    BitBtnSaveAndExit: TBitBtn;
    BitBtnRefresh: TBitBtn;
    BitBtnGraph: TBitBtn;
    BitBtnList: TBitBtn;
    actPicture: TAction;
    tlbtnPicture: TToolButton;
    ImageList1: TImageList;
    Label1: TLabel;
    ImageGhost: TImage;
    actWSEmpEff: TAction;
    tlbtnWSEmpEff: TToolButton;
    procedure pnlDrawMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure ImageClick(Sender: TObject);
    procedure ImageMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure ImageMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure ImageMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure actOpenExecute(Sender: TObject);
    procedure actNewExecute(Sender: TObject);
    procedure actSaveAsExecute(Sender: TObject);
    procedure TimerMainTimer(Sender: TObject);
    procedure ActionShowChangeProperties(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FileExitActExecute(Sender: TObject);
    procedure TimerBlinkTimer(Sender: TObject);
    procedure ShowChartClick(Sender: TObject);
    procedure ShowEmployees1Click(Sender: TObject);
    procedure actFileSettingsExecute(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure TimerStatusTimer(Sender: TObject);
    procedure ShowAllEmployees1Click(Sender: TObject);
    procedure ShowEmployeesonwrongWokspot1Click(Sender: TObject);
    procedure ShowEmployeesnotscannedin1Click(Sender: TObject);
    procedure ShowEmployeesabsentwithreason1Click(Sender: TObject);
    procedure ShowEmployeesabsentwithoutreason1Click(Sender: TObject);
    procedure ShowEmployeeswithFirstAid1Click(Sender: TObject);
    procedure ShowEmployeestoolate1Click(Sender: TObject);
    procedure HelpLegendaActExecute(Sender: TObject);
    procedure actPuppetBoxExecute(Sender: TObject);
    procedure actWorkspotExecute(Sender: TObject);
    procedure actEffiMeterExecute(Sender: TObject);
    procedure actLineHorzExecute(Sender: TObject);
    procedure actLineVertExecute(Sender: TObject);
    procedure actRectangleExecute(Sender: TObject);
    procedure actSaveExecute(Sender: TObject);
    procedure actDeleteExecute(Sender: TObject);
    procedure actEditExecute(Sender: TObject);
    procedure ButtonInClick(Sender: TObject);
    procedure ButtonOutClick(Sender: TObject);
    procedure ButtonChangeJobClick(Sender: TObject);
    procedure ButtonChangeModeClick(Sender: TObject);
    procedure actShowStatusInfoExecute(Sender: TObject);
    procedure TimerReadCounterTimer(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure TimerWriteToDBTimer(Sender: TObject);
    procedure TimerWaitForReadTimer(Sender: TObject);
    procedure TimerWriteToFileTimer(Sender: TObject);
    procedure actSaveLogExecute(Sender: TObject);
    procedure FileCloseExecute(Sender: TObject);
    procedure BitBtnRefreshClick(Sender: TObject);
    procedure BitBtnSaveAndExitClick(Sender: TObject);
    procedure pnlBB1Click(Sender: TObject);
    procedure pnlBB2Click(Sender: TObject);
    procedure pnlBB3Click(Sender: TObject);
    procedure pnlBB4Click(Sender: TObject);
    procedure pnlBB5Click(Sender: TObject);
    procedure pnlBB6Click(Sender: TObject);
    procedure pnlBB7Click(Sender: TObject);
    procedure pnlBB8Click(Sender: TObject);
    procedure pnlBB9Click(Sender: TObject);
    procedure pnlBB10Click(Sender: TObject);
    procedure pnlBB11Click(Sender: TObject);
    procedure pnlBB12Click(Sender: TObject);
    procedure pnlBB13Click(Sender: TObject);
    procedure pnlBB14Click(Sender: TObject);
    procedure pnlBB15Click(Sender: TObject);
    procedure pnlBB16Click(Sender: TObject);
    procedure ListButtonClick(Sender: TObject);
    procedure GraphButtonClick(Sender: TObject);
    procedure actPictureExecute(Sender: TObject);
    procedure actWSEmpEffExecute(Sender: TObject);
  protected
    procedure WMTimeChange(var Message: TMessage); message WM_TIMECHANGE;
    procedure WMQueryEndSession(var Message: TWMQueryEndSession); message WM_QUERYENDSESSION;
  private
    { Private declarations }
    ASocketPortList: TList;
    SocketPortCount: Integer;
    MyAction: TMyAction;
    ThisCaption: String;
    RefreshTimeInterval: Integer; // Time screen refreshes
    LogPath: String;
    ImagePath: String;
    SchemePath: String;
    PicturePath: String;
    SoundPath: String;
    ExportPath: String;
    SoundFilename: String;
    UseSoundAlarm: Boolean;
    ExportSeparator: String;
    EfficiencyPeriodMode: TEffPeriodMode;
    EfficiencyPeriodSince: Boolean; // Use 'Current' or 'Since Time'.
    EfficiencyPeriodSinceTime: TDateTime; // H:M
    FDragOfX: Integer;
    FDragOfY: Integer;
    FDragging: Boolean;
    PSList: TList;
    ReadCounterList: TList;
    ReadCounterIndex: Integer;
    WorkspotScale: Integer;
    FontScale: Integer; // for big-efficiency-meters
    WorkFilename: String;
    CurrentDrawObject: PDrawObject;
    SelectionRectangle: TShapeEx;
    Dirty: Boolean; // has scheme changed by user? Ask for save!
    TimerStartTime, TimerEndTime: TDateTime;
    TimerOnTime: TDateTime;
    InitForm: Boolean;
    Busy: Boolean;
    Working: Boolean;
    AppRootPath: String;
    ASocketPort: TSocketPort;
    FEffQuantityTime: TEffQuantityTime;
    FWriteToDBInterval: Integer;
    FReadDelay: Integer;
    TimeRecordingCurrentDrawObject: PDrawObject;
    FRefreshEmployees: Boolean;
    FReadTimeout: Integer;
    FCurrentPeriod: Integer;
    FExportJobChange: Boolean;
    FExportJobChangeFolder: String;
    FMachHrsMaxIdleTime: Integer;
    FDebug: Boolean;
    FReadExtDataAtSeconds: Integer;
    FToggleNameJob: Boolean;
    procedure SetCaption;
    function CreateImage(const AImageName: String;
      const ABigEffMeters: Boolean): TImage;
    function CreateGhostImage: TImage; // PIM-151
    procedure GhostImageDisable;
    function CreateShapeEx(AShapeExType: TShapeExType): TShapeEx;
    function CreatePicture(AImageName: String): TImage;
    // PersonalScreen
(*    function CreateRectangle(ADrawObject: PDrawObject): PTARectangle;
    procedure DeleteRectangle(ADrawObject: PDrawObject);
    // PersonalScreen
    procedure ShowRectangle(ADrawObject: PDrawObject); *)
    function NewDrawObject(AProdScreenType: TProdScreenType;
      ABigEffMeters: Boolean; AObjectType: TObjectType;
      APlantCode, AWorkspotCode: String): PDrawObject; // PIM-114
    function NewItemIndex: Integer;
    procedure RenumberDrawObjects;
    procedure AddObjectToList(otType: TObjectType;
      AObject: TObject; APlantCode, AWorkspotCode, AWorkspotDescription,
      AImageName: String; ABigEffMeters: Boolean; AShowMode: Integer;
      AProdScreenType: TProdScreenType;
      AMachineType: TMachineType;
      AMachineCode, AMachineDescription: String;
      AWorkspotCounter: Integer);
    procedure DeleteDrawObjectFromList(ATag: Integer);
    procedure ShowObjectNumber(Sender: TObject);
    procedure ShowObjectProperties(X: Integer; Y: Integer);
    procedure FreeList;
    procedure SaveList(Filename: String);
    procedure OpenList(Filename: String);
    function ThisDrawObject(ATag: Integer): PDrawObject;
    procedure ShowChangeProperties(Sender: TObject);
    procedure ShowSelectionRectangle(Sender: TObject);
    procedure ShowDrawObject(ADrawObject: PDrawObject; Sender: TObject);
    procedure JobButtonAdjustSize(ADrawObject: PDrawObject; AJobButton: TButton); // 20015406
// PersonalScreen
(*
    function NewPuppet(AParent: TWinControl; APTEmployee: PTEmployee): PTPuppet;
    procedure DeletePuppet(var APuppet: PTPuppet);
    procedure DeletePuppetList(var APuppetList: TList);
    procedure DeletePuppetShiftList(var APuppetShiftList: TList);
    procedure ShowPuppet(APuppet: PTPuppet;
      ALeft, ATop: Integer; AColor: TColor);
    procedure ShowPuppets(ADrawObject: PDrawObject; Sender: TObject);
    function SearchCreateShift(var ADrawObject: PDrawObject;
      const AShiftNumber: Integer): Integer;
*)
    function PrecalcPercentage(ADrawObject: PDrawObject;
      APercentage: Double): Double;
    procedure ShowEffMeter(ADrawObject: PDrawObject; APercentage: Double;
      Sender: TObject);
(*    procedure ShowIndicators(ADrawObject: PDrawObject;
      ARed, AYellow, AGreen: TColor); *)
//    function DateTimeBefore(ADate: TDateTime; ASeconds: Integer): TDateTime;
(*    function SearchLastProductionQuantityRecord(ADrawObject: PDrawObject;
        var Quantity: Double): Boolean; *)
(*    function ActionEfficiencyXXX(ADrawObject: PDrawObject): TEffPeriodMode; *)
(*    procedure ActionConnectionIndicators(ADrawObject: PDrawObject); *)
    // PersonalScreen
(*    procedure ActionPuppets(ADrawObject: PDrawObject); *)
(*    procedure MainActionForTimerXXX; *)
    procedure MainActionForTimer;
    procedure ReadRegistry;
    procedure WriteRegistry;
    procedure ShowEmployees(const AShowMode: TShowMode);
    function AskForSave: Boolean;
    procedure PlaySound;
    procedure ShowFirstObject;
(*    procedure ClearEmployeeList(ADrawObject: PDrawObject); *) // PIM-213
(*    procedure ShowEmployeeList(ADrawObject: PDrawObject); *)
(*    function FindEmployee(
      AEmployeeList: TList; AEmployeeNumber: Integer): Boolean; *)
(*    procedure DeleteEmployeeList(var AEmployeeList: TList); *)
(*    procedure ShowActionTime; *)
(*    procedure CalculateTimeAndNormValues(
      ADrawObject: PDrawObject;
      AodsTimeRegScan: TOracleDataSet;
      var NormProdQuantity: Double;
      var NormProdLevel: Double;
      var MinutesTillNow: Integer;
      var QuantitySum: Double); *)
    function DetermineWorkspotShortName(PlantCode,
      WorkspotCode: String): String;
(*    function DetermineDepartmentDescription(PlantCode,
      DepartmentCode: String): String; *)
    procedure MainTimerSwitch(OnOff: Boolean; AAll: Boolean=False);
    // PersonalScreen
(*
    procedure ActionBlinkPuppets;
    procedure ActionPuppetBox(ADrawObject: PDrawObject);
    procedure ActionCreatePuppets(ADrawObject: PDrawObject;
      var AEmployeeList: TList);
*)
(*    procedure ActionCompareJobsInit(MyNow: TDateTime); *)
(*    procedure ActionCompareJobs(ADrawObject: PDrawObject; MyNow: TDateTime); *)
    procedure ReadINIFile;
    procedure WriteINIFile;
    procedure CreateMachineWorkspotList(AMyCurrentDrawObject: PDrawObject);
    procedure ShowStatusErrorMessage(AMsg: String);
    procedure ChangeJobForEmployees(
      AMyCurrentDrawObject: PDrawObject; // TD-26709
      APlantCode, AMachineCode, AWorkspotCode,
      ACurrentJobCode, ANewJobCode, ANewJobDescription: String;
      ACJMode: Integer);
    procedure ChangeJob;
    procedure ChangeMode;
    procedure ChangeModeByDrawObject(ADrawObject: PDrawObject);
    procedure TimeRecordingAction(AInScan: Boolean);
    procedure ShowMsg(AMsg: String);
    procedure CreatePersonalScreenObject(AProdScreenType: TProdScreenType;
      ADrawObject: PDrawObject);
    procedure DataInit(AAll: Boolean=True);
    procedure DataReadAll(ARecreateReadCounterList: Boolean=False);
    procedure DataReadAllForOneDrawObject;
    procedure ShowDetails(ADrawObject: PDrawObject);
    procedure ActionWriteToDB;
    procedure ActionWriteToFile;
    procedure ActionSaveLogMain(AAutomatic: Boolean=False);
    procedure ActionEditOn;
    procedure ActionEditOff;
    procedure MyActionRefresh;
    procedure MyActionOpenList;
    procedure MyActionNew;
    procedure MyActionSave;
    procedure MyActionSaveAs;
    procedure MyActionSaveLog;
    procedure MyActionSaveLogAutomatic;
    procedure MyActionTimeRecording;
    procedure MyActionChangeJob;
    procedure MyActionSaveAndExit;
    procedure MyActionFileSettings;
    procedure MyActionReadCounter;
    procedure MyActionRealTimeEfficiency;
    procedure MyActionListButtonClick;
    procedure MyActionGraphButtonClick;
    procedure MyActionShowEmployeeListClick;
    procedure MyActionReadExternalData;
    procedure ActionShowAllWorkspotDetails;
    procedure ActionShowWorkspotDetails;
    procedure ActionShowStatusBB;
    procedure ActionRealTimeEfficiency;
    function IsBusy: Boolean;
    function SocketPortFind(AHost, APort: String): TSocketPort;
    procedure ClearSocketPortList;
    procedure ReadExportJobChangeINIFile; // 20014722
    function ExportJobChangeToFile(ACurrentDrawObject: PDrawObject;
      APlantCode, AWorkspotCode, AJobCode: String;
      ATimestamp: TDateTime): Boolean; // 20014722
    procedure MyGlobalExceptionHandler(Sender : TObject; E : Exception );
    procedure ErrorLogReducer;
    function WSEmpEffCreate(ADrawObject: PDrawObject;
      AShortName: String): TpnlWSEmpEff;
    procedure ActionWSEmpEffRefresh;
{$IFDEF TEST}
    procedure AddFakeCounter(AI: Integer);
{$ENDIF}
  public
    { Public declarations }
    PIMSDatacolTimeInterval: Integer; // in seconds
    PIMSMultiplier: Integer; // Used to multiply with 'PIMSDatacolTimeInterval'
    APlanScanClass: TPlanScanClass;
    procedure WriteLog(AMsg: String);
    function DetermineEmployeeList(
      ADrawObject: PDrawObject;
      const AShowMode: TShowMode;
      const APlantCode: String;
      const AWorkspotCode: String;
      var AEmployeeList: TList;
      var AEmployeeCount: Integer): Boolean;
    procedure DetermineSinceDates(MyNow: TDateTime;
      var DateFrom, DateTo: TDateTime);
    procedure TryReconnectCounters;
    procedure RepositionSubForm(ASubForm: TForm);
    procedure ShowLinkJobDetails; // 20015376
    procedure WSEmpEffEditMode(AOn: Boolean);
    property EffQuantityTime: TEffQuantityTime read FEffQuantityTime
      write FEffQuantityTime;
    property WriteToDBInterval: Integer read FWriteToDBInterval
      write FWriteToDBInterval;
    property ReadDelay: Integer read FReadDelay write FReadDelay;
    property RefreshEmployees: Boolean read FRefreshEmployees
      write FRefreshEmployees;
    property ReadTimeout: Integer read FReadTimeout write FReadTimeout;
    property CurrentPeriod: Integer read FCurrentPeriod write FCurrentPeriod;
    property ExportJobChange: Boolean read FExportJobChange
      write FExportJobChange default False; // 20014722
    property ExportJobChangeFolder: String read FExportJobChangeFolder
      write FExportJobChangeFolder;
    property MachHrsMaxIdleTime: Integer read FMachHrsMaxIdleTime
      write FMachHrsMaxIdleTime; // 20014450
    property Debug: Boolean read FDebug write FDebug default False; // 20014826
    property ReadExtDataAtSeconds: Integer read FReadExtDataAtSeconds
      write FReadExtDataAtSeconds; // PIM-90
    property ToggleNameJob: Boolean read FToggleNameJob write FToggleNameJob; // PIM-147
  end;

procedure Wait(const MSecsToWait: Integer);
function PathCheck(Path: String): String;
function EmployeeCompare(Item1, Item2: Pointer): Integer;

var
  HomeF: THomeF;
  CloseNow: Boolean;

implementation

{$R *.DFM}

uses
  CalculateTotalHoursDMT,
  DialogWorkspotSelectFRM,
  DialogImagePropertiesFRM,
  DialogShowAllEmployeesFRM,
  DialogSettingsFRM,
  ProductionScreenWaitFRM,
  UGlobalFunctions,
  DialogDepartmentSelectFRM,
  UPimsMessageRes,
  DialogSelectModeFRM,
  //CAR user rights
  DialogLoginFRM,
  DialogLegendaFRM,
  DialogChangeJobDMT,
  DialogChangeJobFRM,
  GlobalDMT,
  DialogDebugInfoFRM,
  UTranslatePS,
  DialogObjectSettingsFRM,
  UTranslateStringsPS,
  DialogEmpListViewFRM,
  DialogEmpGraphFRM,
  RealTimeEffDMT,
  EnterJobCommentDMT,
  UGhostCount,
  WorkspotEmpEffDMT;

{ Init - Start }

procedure THomeF.FormCreate(Sender: TObject);
var
  Path: String;
  PimsRootPath: String;
begin
  inherited;
  Application.OnException := MyGlobalExceptionHandler;
  pnlDraw.Color := clWhite; // clPimsLBlue; // 20014450.50

  Label1.Visible := False; // PIM-12 !!!Testing!!!
{$IFDEF DEBUG}
  Label1.Visible := True;
{$ENDIF}

  // PIM-223
  ORASystemDM.PSShowEmpInfo := True;
  ORASystemDM.PSShowEmpEff := True;

  Debug := False; // 20014826
  MyAction := nil;
  Working := False;
  CloseNow := False;
  APlanScanClass := TPlanScanClass.Create;
  AppRootPath := GetCurrentDir;

  // 20014450.50 Change some colors here
//  BitBtnSaveAndExit.Color := clPimsBlue; // PIM-250
//  BitBtnRefresh.Color := clPimsBlue; // PIM-250
  
  // Multi
//  ASocketPort := TSocketPort.Create(IdTelnet1);
//  ASocketPort.MyLog := WErrorLog;
  ASocketPort := nil;
  ASocketPortList := TList.Create;
  SocketPortCount := 0;

  // Set error log filename for this application.
  UGErrorLogFilenameSet(ERRORLOGFILENAME);

  // MRA: 8-JUN-2009 Not needed. RV029.
//  ORASystemDM.ProdScreen := True;
  Dirty := False;
  Busy := False;
  PIMSDatacolTimeInterval := DEFAULT_DATACOL_TIME_INTERVAL;
  PIMSMultiplier := DEFAULT_MULTIPLIER;
  RefreshTimeInterval := DEFAULT_REFRESH_TIME_INTERVAL;
  WriteToDBInterval := DEFAULT_WRITETODB_INTERVAL;
  ReadDelay := DEFAULT_READDELAY;
  RefreshEmployees := DEFAULT_REFRESH_EMPLOYEES;
  ReadTimeout := DEFAULT_READTIMEOUT;
  CurrentPeriod := DEFAULT_CURRENTPERIOD; // SO-20013476
  MachHrsMaxIdleTime := DEFAULT_MACHHRSMAXIDLETIME; // 20014450
  PersonalScreenDM.MachHrsMaxIdleTime := MachHrsMaxIdleTime; // 20014450
  ExportJobChange := False; // 20014722
  SetCurrentPeriod(CurrentPeriod);
  TimerWaitForRead.Interval := ReadTimeout;
  ReadExtDataAtSeconds := DEFAULT_READ_EXT_DATA_AT_SECONDS; // PIM-90
  // Multi
//  ASocketPort.ReadDelay := ReadDelay;
  ThisCaption := Copy(Caption, 8, Length(Caption)); // 20014289
  FDragging := False;
  InitForm := True;
  WorkspotScale := DEFAULT_WORKSPOT_SCALE;
  FontScale := DEFAULT_WORKSPOT_SCALE;
  EfficiencyPeriodMode := epCurrent;
  EfficiencyPeriodSince := False;
  EfficiencyPeriodSinceTime := EncodeTime(7, 0, 0, 0);
  Path := GetCurrentDir;
  // Determine Path from where Pims has been started.
  PimsRootPath := PathCheck(Path);
  if Pos(UpperCase('\Bin'), UpperCase(Path)) > 0 then
    PimsRootPath := PathCheck(Copy(Path, 1,
      Pos(UpperCase('\Bin'), UpperCase(Path))));

  // Init variables
  LogPath := PimsRootPath + 'Log\';
  ImagePath := PimsRootPath + 'Bitmaps\';
  SchemePath := PimsRootPath + 'Schemes\';
  PicturePath := PimsRootPath + 'Pictures\';
  SoundPath := PimsRootPath + 'Sounds\';
  ExportPath := PimsRootPath + 'Export\';
  if not DirectoryExists(ImagePath) then
    ForceDirectories(ImagePath);
  if not DirectoryExists(SchemePath) then
    ForceDirectories(SchemePath);
  if not DirectoryExists(PicturePath) then
    ForceDirectories(PicturePath);
  if not DirectoryExists(SoundPath) then
    ForceDirectories(SoundPath);
  if not DirectoryExists(ExportPath) then
    ForceDirectories(ExportPath);
  ExportSeparator := ',';
  WorkFilename := DEFAULT_WORKFILENAME;
//  PIMSTimerInterval := TIMER_INTERVAL;

  ExportJobChangeFolder := ExportPath; // 20014722

  PSList := TList.Create;
  ReadCounterList := TList.Create;
  ReadCounterIndex := -1;
  CurrentDrawObject := nil;
  TimeRecordingCurrentDrawObject := nil;
  // Create DialogWorkspotSelect, so the contents are always ready
  // to select.
//  DialogWorkspotSelectF := TDialogWorkspotSelectF.Create(Application);
//  DialogWorkspotSelectF.RootPath := ImagePath;
  // Create DialogDepartmentSelect, so the contents are always ready
  // to select.
//  DialogDepartmentSelectF := TDialogDepartmentSelectF.Create(Application);
  // Following is a rectangle that can show the Current Selected Object
  SelectionRectangle := TShapeEx.Create(Application);
  SelectionRectangle.Pen.Width := 2;
  SelectionRectangle.Parent := pnlDraw;
  SelectionRectangle.Pen.Color := clMyRed;
  SelectionRectangle.Shape := TShapeExType(RECTANGLE);
  SelectionRectangle.Brush.Style := bsClear;
  SelectionRectangle.Visible := False;

  // Get Pims-Settings
  with ORASystemDM.odsPimsSettings do
  begin
    if not Active then
      Active := True;
    try
      if FieldByName('DATACOL_TIME_INTERVAL').AsString <> '' then
        PIMSDatacolTimeInterval :=
          FieldByName('DATACOL_TIME_INTERVAL').Value;
    except
      PIMSDatacolTimeInterval := DEFAULT_DATACOL_TIME_INTERVAL;
    end;
  end;
  PersonalScreenDM.PIMSDatacolTimeInterval := PIMSDatacolTimeInterval; // 20015178
  PersonalScreenDM.PIMSMultiplier := PIMSMultiplier; // 20015178

  with ProductionScreenDM do
  begin
    cdsWorkspotShortName.Open;
    odsJobcode.Active := True;
  end;

  // Init a list for getting daynr!
  InitDayList;
  // MR:02-05-2003 Create these forms here, to speed things up
  // Do this once in project!!!
//  DialogShowAllEmployeesF := TDialogShowAllEmployeesF.Create(Application);
//  DialogShowAllEmployeesF.Hide;

  // Change Job Dialog, create needed files here.
  // These are already created in project.
//  DialogChangeJobDM := TDialogChangeJobDM.Create(Application);
//  DialogChangeJobF := TDialogChangeJobF.Create(Application);
//  DialogChangeJobF.Hide;

  // Personal Screen
  TimerReadCounter.Interval := DEFAULT_READ_COUNTER_INTERVAL;
  // Personal Screen -> This is a global setting (PIMSSETTING.EFF_BO_QUANT_YN)
  if ORASystemDM.EffBoQuantYN = 'Y' then
    EffQuantityTime := efQuantity
  else
    EffQuantityTime := efTime;

  TimerWriteToFile.Interval := DEFAULT_WRITETOFILE_INTERVAL;

  // Create all sub-forms here!
  DialogChangeJobF := TDialogChangeJobF.Create(Application);
  DialogChangeJobF.Visible := False;
  DialogObjectSettingsF := TDialogObjectSettingsF.Create(Application);
  DialogObjectSettingsF.Visible := False;
  DialogEmpListViewF := TDialogEmpListViewF.Create(Application);
  DialogEmpListViewF.Visible := False;
  DialogEmpGraphF := TDialogEmpGraphF.Create(Application);
  DialogEmpGraphF.Visible := False;
  DialogShowAllEmployeesF := TDialogShowAllEmployeesF.Create(Application);
  DialogShowAllEmployeesF.Visible := False;
  DialogWorkspotSelectF := TDialogWorkspotSelectF.Create(Application);
  DialogWorkspotSelectF.RootPath := ImagePath;
  DialogWorkspotSelectF.Visible := False;
  DialogDepartmentSelectF := TDialogDepartmentSelectF.Create(Application);
  DialogDepartmentSelectF.Visible := False;
  DialogDebugInfoF := TDialogDebugInfoF.Create(Application);
  DialogDebugInfoF.Visible := False;
  ProductionScreenWaitF := TProductionScreenWaitF.Create(Application);
  ProductionScreenWaitF.Visible := False;

  // TimeRecording Form, create form here.
  TimeRecordingF := TTimeRecordingF.Create(Application);
  // TD-21429
  TimeRecordingF.Position := poDefault;
  TimeRecordingF.Hide;


  // Set log file name for this application.
  UGLogFilenameSet(LOGFILENAME);

  // TD-24736.60
  PersonalScreenDM.DateTimeFormatPatch := False;

  // Handle last found qty-to-store-list and store it in DB
  try
    PersonalScreenDM.ActionHandleLastQtyToStoreFile(LogPath);
  except
    //
  end;
  try
    // TODO 21191
    WLog('PS-Start-' + DisplayVersionInfo);
  except
  end;
  // 20014450.50
  // Already assume 1 minute has past to prevent it misses the first 1 minute
  LastTimeInsertWorkspotPerMinute := Now - 1/24/60; // minus 1 minute
  LastTimeReadExtData := Now - 1/24/60; // minus 1 minutes // PIM-90
end; // FormCreate

procedure THomeF.FormActivate(Sender: TObject);
begin
  inherited;
  if InitForm then
  begin
    TimerMain.Interval := RefreshTimeInterval * 1000; // (secs)
    OpenList(WorkFilename);

    // Openlist is already doing this:
//    MainTimerSwitch(True);

    SetCaption;
    InitForm := False;

    HomeF.SetFocus;
  end;
end; // FormActivate

{ Init - End }

procedure Wait(const MSecsToWait: Integer);
var
  TimeToWait,
  TimeToSendRequest : TDateTime;
begin
  TimeToWait   := MSecsToWait / MSecsPerDay;
  TimeToSendRequest := TimeToWait + SysUtils.Now;
  while SysUtils.Now < TimeToSendRequest do
    Sleep(0);
end; // Wait

{ Open Scheme - Start }

procedure THomeF.MyActionOpenList;
var
  Path: String;
  OtherPath: String;
begin
  // Not possible in edit-mode!
  if tlbtnEdit.Down then
    Exit;

  MyAction := nil;
//  OpenListNow := False;
  MainTimerSwitch(False, True);
  try
    ActionWriteToFile;
    ActionWriteToDB;
  except
  end;
  AskForSave;
  Path := GetCurrentDir;
  try
    OpenDialog1.InitialDir := SchemePath;
    OpenDialog1.Filename := WorkFilename;
    OpenDialog1.Filter := 'Ini-files|*.ini';
    if OpenDialog1.Execute then
    begin
      WorkFilename := ExtractFileName(OpenDialog1.Filename);
      // If file came from another path, copy the file to
      // SchemePath
      OtherPath := ExtractFilePath(OpenDialog1.Filename);
      if OtherPath <> SchemePath then
        CopyFile(PChar(OtherPath + WorkFilename),
          PChar(SchemePath + WorkFilename), False);
      // Now read the file
      OpenList(WorkFilename);
    end;
    SetCaption;
  finally
    SetCurrentDir(Path);
    MainTimerSwitch(True, True);
  end;
  Dirty := False;
end; // MyActionOpenList

// Open a Browse-dialog and open/load a scheme that was selected by user.
procedure THomeF.actOpenExecute(Sender: TObject);
begin
  if not IsBusy then
    MyActionOpenList
  else
    MyAction := MyActionOpenList;
end; // actOpenExecute

// Open/load the scheme and show the scheme.
procedure THomeF.OpenList(Filename: String);
var
  I: Integer;
  ADrawObject: PDrawObject;
  IniFile: TIniFile;
  Obj: String;
  ObjectName: String;
  BigEffMeters: Boolean;
  ProdScreenTypeDescription: String;
  ProdScreenType: TProdScreenType;
  ObjectType: TObjectType;
  PlantCode, WorkspotCode: String; // PIM-114
begin
  if tlbtnEdit.Down then
    Exit;

  // PIM-223
  ORASystemDM.PSShowEmpInfo := True;
  ORASystemDM.PSShowEmpEff := True;

  MainTimerSwitch(False, True);
  Screen.Cursor := crHourGlass;

  // TD-21799
  ProductionScreenWaitF.lblMessage.Caption := SPimsLoadingScheme;
  ProductionScreenWaitF.Show;
  Update;

  try
    // Must this be set to nil?
    // Because a diffent scheme is loaded?
    // CurrentDrawObject := nil;

    // TODO 21191 Must this also be done here?
//    ClearSocketPortList;

    FreeList;
    PSList := TList.Create;

    IniFile := TIniFile.Create(ExtractFilePath(SchemePath) +
      ExtractFileName(Filename));

    with IniFile do
    begin
      // Read Coordinates of Window for this Scheme
      HomeF.Top := ReadInteger('Main', 'Top', HomeF.Top);
      HomeF.Left := ReadInteger('Main', 'Left', HomeF.Left);
      HomeF.Width := ReadInteger('Main', 'Width', HomeF.Width);
      HomeF.Height := ReadInteger('Main', 'Height', HomeF.Height);
      // Read settings for this Scheme
      WorkspotScale := ReadInteger('Main', 'WorkspotScale', WorkspotScale);
      UPersonalScreen.WorkspotScale := WorkspotScale; // 20015406
      FontScale := ReadInteger('Main', 'FontScale', FontScale);
      UPersonalScreen.FontScale := FontScale; // 20015406

      // PIM-194
      if ValueExists('Main', 'RedBoundary') then
        ORASystemDM.RedBoundary :=
          ReadInteger('Main', 'RedBoundary', ORASystemDM.RedBoundary)
      else
        ORASystemDM.RedBoundary := RED_BOUNDARY;
      if ValueExists('Main', 'OrangeBoundary') then
        ORASystemDM.OrangeBoundary :=
          ReadInteger('Main', 'OrangeBoundary', ORASystemDM.OrangeBoundary)
      else
        ORASystemDM.OrangeBoundary := ORANGE_BOUNDARY;
      // 20013476
      // 20014450.50
{
      if ValueExists('Main', 'CurrentPeriod') then
        CurrentPeriod := ReadInteger('Main', 'CurrentPeriod', CurrentPeriod)
      else
        CurrentPeriod := DEFAULT_CURRENTPERIOD;
      SetCurrentPeriod(CurrentPeriod);
}
      // 20014450
      if ValueExists('Main', 'MachHrsMaxIdleTime') then
        MachHrsMaxIdleTime := ReadInteger('Main', 'MachHrsMaxIdleTime', MachHrsMaxIdleTime)
      else
        MachHrsMaxIdleTime := DEFAULT_MACHHRSMAXIDLETIME;
      PersonalScreenDM.MachHrsMaxIdleTime := MachHrsMaxIdleTime; // 20014450
      // MRA:24-JUN-2010. New setting for efficiency-period-mode:
      if ValueExists('Main', 'EfficiencyPeriodMode') then
      begin
        EfficiencyPeriodMode :=
          TEffPeriodMode(
            ReadInteger('Main', 'EfficiencyPeriodMode',
              Integer(EfficiencyPeriodMode)));
      end
      else
      begin
        // MRA:24-JUN-2010. For compatibility with older systems:
        EfficiencyPeriodSince := ReadBool('Main', 'EfficiencyPeriodSince',
          EfficiencyPeriodSince);
        if EfficiencyPeriodSince then
          EfficiencyPeriodMode := epSince
        else
          EfficiencyPeriodMode := epCurrent;
      end;
      UPersonalScreen.EffPeriodMode := EfficiencyPeriodMode; // 20015406
      try
        EfficiencyPeriodSinceTime := ReadDateTime('Main',
          'EfficiencyPeriodSinceTime', EfficiencyPeriodSinceTime);
      except
        EfficiencyPeriodSinceTime := Trunc(Now) + Frac(EncodeTime(7, 0, 0, 0));
      end;

      // PIM-223
      if ValueExists('Main', 'ShowEmpInfo') then
        ORASystemDM.PSShowEmpInfo :=
          ReadBool('Main', 'ShowEmpInfo', ORASystemDM.PSShowEmpInfo);
      if ValueExists('Main', 'ShowEmpEff') then
        ORASystemDM.PSShowEmpEff :=
          ReadBool('Main', 'ShowEmpEff', ORASystemDM.PSShowEmpEff);
      if not ORASystemDM.PSShowEmpInfo then
        ORASystemDM.PSShowEmpEff := False;

      I := 0;
      // Read objects for this Scheme
      while I < MAXOBJECTS do
      begin
        Application.ProcessMessages;
        Obj := 'Object' + IntToStr(I);
        ObjectName := ReadString(Obj, 'AImageName', '');
        if ObjectName <> '' then
        begin
          ProdScreenType := pstNoTimeRec;
          //
          // Personal Screen - Start
          //
          if ValueExists(Obj, 'AProdScreenType') then
          begin
            ProdScreenTypeDescription :=
              ReadString(Obj, 'AProdScreenType', 'NoTimeRec');
            if ProdScreenTypeDescription = 'MachineTimeRec' then
              ProdScreenType := pstMachineTimeRec
            else
              if ProdScreenTypeDescription = 'WorkspotTimeRec' then
                ProdScreenType := pstWorkspotTimeRec
              else
                if ProdScreenTypeDescription = 'WSEmpEff' then // PIM-213
                  ProdScreenType := pstWSEmpEff;
          end;
          //
          // Personal Screen - End
          //
          // Read BigEffMeters and AType first!
          // So we can use it during 'NewDrawObject' to see what has to
          // be created.
          BigEffMeters := (ReadString(Obj, 'ABigEffMeters', 'False') = 'True');
          ObjectType := TObjectType(ReadInteger(Obj, 'AType', 0));

          // PersonalScreen
          if not ((ObjectType = otPicture) or BigEffMeters or
            ((not BigEffMeters) and (ProdScreenType = pstWSEmpEff))
            and (ProdScreenType <> pstNoTimeRec)) then
          begin
            inc(I);
            Continue; // Skip
          end;

          // PIM-114 We need the plantcode and workspot for NewDrawObject.
          PlantCode := '';
          WorkspotCode := '';
          if (ObjectType <> otPicture) then
          begin
            PlantCode := ReadString(Obj, 'APlantCode', '');
            WorkspotCode := ReadString(Obj, 'AWorkspotCode', '');
          end;
          ADrawObject := NewDrawObject(ProdScreenType, BigEffMeters, ObjectType,
            PlantCode, WorkspotCode);
          ADrawObject.AType := ObjectType;
{$IFNDEF PERSONALSCREEN}
          // Do not show Personal Screen objects.
          if (ProdScreenType <> pstNoTimeRec) then
            ADrawObject.AVisible := False;
{$ENDIF}
          ADrawObject.AImageName := ObjectName;
          ADrawObject.ABigEffMeters := BigEffMeters;

          if (ADrawObject.AType = otPicture) then
          begin
            ADrawObject.AObject := CreatePicture(ADrawObject.AImageName);
            with (ADrawObject.AObject as TImage) do
            begin
              ADrawObject.AHeight := Height; // Store original height/width in object
              ADrawObject.AWidth := Width;
              Left := ReadInteger(Obj, 'Left', 0);
              Top := ReadInteger(Obj, 'Top', 0);
              if ValueExists(Obj, 'PictureScale') then
                ADrawObject.AWorkspotScale :=
                  ReadInteger(Obj, 'PictureScale', WorkspotScale);
            end;
          end
          else
          begin
            ADrawObject.APlantCode := ReadString(Obj, 'APlantCode', '');
            ADrawObject.AWorkspotCode := ReadString(Obj, 'AWorkspotCode', '');
            ADrawObject.AWorkspotDescription :=
              ReadString(Obj, 'AWorkspotDescription', '');
            ADrawObject.ALabel.Caption :=
              DetermineWorkspotShortName(ADrawObject.APlantCode,
                ADrawObject.AWorkspotCode);

            // 20015406
            if ProdScreenType = pstWSEmpEff then
            begin
              // Take 'main'-value for this!
              ADrawObject.AWorkspotScale := WorkspotScale;
              ADrawObject.AFontScale := FontScale;
              ADrawObject.AEffPeriodMode := epSince; // Must alway be Since (Today)!
              CurrentPeriod := WS_EFF_CURRENTPERIOD;
              ADrawObject.ARealTimeIntervalMinutes := CurrentPeriod;
              SetCurrentPeriod(CurrentPeriod);
            end
            else
            begin
              if ValueExists(Obj, 'WorkspotScale') then
                ADrawObject.AWorkspotScale :=
                  ReadInteger(Obj, 'WorkspotScale', WorkspotScale);
              if ValueExists(Obj, 'FontScale') then
                ADrawObject.AFontScale :=
                  ReadInteger(Obj, 'FontScale', FontScale);
              if ValueExists(Obj, 'EffPeriodMode') then
                ADrawObject.AEffPeriodMode :=
                  TEffPeriodMode(ReadInteger(Obj, 'EffPeriodMode', Integer(EffPeriodMode)));
              ChangeModeByDrawObject(ADrawObject);
            end;

            //
            // Personal Screen - Start
            //
            ADrawObject.AProdScreenType := ProdScreenType;
            case ProdScreenType of
            pstNoTimeRec:
              begin
                ADrawObject.ALabel.Caption :=
                  DetermineWorkspotShortName(ADrawObject.APlantCode,
                    ADrawObject.AWorkspotCode);
              end;
            pstMachineTimeRec:
              begin
                ADrawObject.AMachineTimeRec.AMachineCode :=
                  ReadString(Obj, 'AMachineCode', '');
                ADrawObject.AMachineTimeRec.AMachineDescription :=
                  ReadString(Obj, 'AMachineDescription', '');
                ADrawObject.ALabel.Caption :=
                  ADrawObject.AMachineTimeRec.AMachineDescription;
                // 20014450
                ADrawObject.AMachineTimeRec.AStartTimeStamp := NullDate;
                ADrawObject.AMachineTimeRec.AShiftNumber := 1;
                ADrawObject.AMachineTimeRec.AShiftDate := Date;
                ADrawObject.AMachineTimeRec.ADepartmentCode :=
                  PersonalScreenDM.MachineDepartment(ADrawObject.APlantCode,
                    ADrawObject.AMachineTimeRec.AMachineCode);
                ADrawObject.AMachineTimeRec.AJobCount := 0;
                ADrawObject.AMachineTimeRec.ADown := False;
              end;
            pstWorkspotTimeRec: // With or without machine
              begin
                ADrawObject.AWorkspotTimeRec.AMachineCode := ReadString(Obj,
                  'AMachineCode', '');
                ADrawObject.AWorkspotTimeRec.AMachineDescription :=
                  ReadString(Obj, 'AMachineDescription', '');
                if ADrawObject.AWorkspotTimeRec.AMachineDescription = '' then
                  ADrawObject.ALabel.Caption :=
                    DetermineWorkspotShortName(ADrawObject.APlantCode,
                      ADrawObject.AWorkspotCode)
                else
                  ADrawObject.ALabel.Caption :=
                    ADrawObject.AWorkspotTimeRec.AMachineDescription +
                      '-' + DetermineWorkspotShortName(ADrawObject.APlantCode,
                        ADrawObject.AWorkspotCode);
              end;
            end;
            //
            // Personal Screen - End
            //
            ADrawObject.ADefaultImage := nil; // PIM-151
            if ADrawObject.AType = otImage then
            begin
              ADrawObject.AObject := CreateImage(ObjectName, BigEffMeters);
              // PIM-213
              case ProdScreenType of
              pstWSEmpEff: // PIM-213
                begin
                  ADrawObject.ApnlWSEmpEff :=
                    WSEmpEffCreate(
                      ADrawObject,
                      DetermineWorkspotShortName(ADrawObject.APlantCode,
                        ADrawObject.AWorkspotCode)
                      );
                end;
              end;
              ADrawObject.ADefaultImage := TImage(ADrawObject.AObject); // PIM-151
              // Make this only visible when ADrawObject is visible.
              (ADrawObject.AObject as TImage).Visible := ADrawObject.AVisible;
              with (ADrawObject.AObject as TImage) do
              begin
                Left := ReadInteger(Obj, 'Left', 0);
                Top := ReadInteger(Obj, 'Top', 0);
                if (ProdScreenType <> pstWSEmpEff) then // PIM-213
                  ADrawObject.ApnlWSEmpEff := nil;
                if Assigned(ADrawObject.ApnlWSEmpEff) then // PIM-213
                begin
                  ADrawObject.ApnlWSEmpEff.Left := Left;
                  ADrawObject.ApnlWSEmpEff.Top := Top;
                end;
              end;
            end
            else
            begin
// PersonalScreen
(*
              if (ADrawObject.AType = otLineHorz) or
                (ADrawObject.AType = otLineVert) or
                (ADrawObject.AType = otRectangle) or
                (ADrawObject.AType = otPuppetBox) then
              begin
                if (ADrawObject.AType = otLineHorz) then
                  ADrawObject.AObject := CreateShapeEx(stLineHorz)
                else
                  if (ADrawObject.AType = otLineVert) then
                    ADrawObject.AObject := CreateShapeEx(stLineVert)
                  else
                    if (ADrawObject.AType = otRectangle) or
                      (ADrawObject.AType = otPuppetBox) then
                    begin
                      ADrawObject.AObject :=
                        CreateShapeEx(TShapeExType(RECTANGLE));
                      ADrawObject.ADeptRect := CreateRectangle(ADrawObject);
                    end;
                with (ADrawObject.AObject as TShapeEx) do
                begin
                  Left := ReadInteger(Obj, 'Left', 0);
                  Top := ReadInteger(Obj, 'Top', 0);
                  if (ADrawObject.AType = otRectangle) or
                    (ADrawObject.AType = otPuppetBox) then
                  begin
                    Width := ReadInteger(Obj, 'Width', IMAGE_SIZE);
                    Height := ReadInteger(Obj, 'Height', IMAGE_SIZE);
                  end;
                end;
              end;
*)
            end;
// PersonalScreen
(*
            if ADrawObject.AType = otPuppetBox then
              ADrawObject.AShowMode := TShowMode(ReadInteger(Obj, 'ShowMode',
                Integer(ShowAllEmployees)));
*)        end;
          ShowDrawObject(ADrawObject, ADrawObject.AObject);
          Update;
          PSList.Add(ADrawObject);
        end;
        inc(I);
      end;
    end;
    IniFile.Free;

//    MainActionForTimerXXX;
  finally
    // PersonalScreen - Based on machines/workspots from scheme that were read
    //                  do DataInit.
    //                  Is this the right place?
    // Init all data, except for what was already created/filled in during
    // loading of the scheme.
    DataInit;
    // TODO 21191 Use True to recreate the ReadCounterList.
    // Read all data till now.
    DataReadAll(True);
    if BBCount > 0 then // ABS-8116
      ShowStatusErrorMessage(SPimsTestingBlackBoxes);
    Screen.Cursor := crDefault;
    RenumberDrawObjects;
    ShowFirstObject;
    MachineWorkspotsInit(PSList); // 20016447
    ProductionScreenWaitF.Hide;
    Update;
//    MainActionForTimer;
    ActionShowAllWorkspotDetails;
    ActionShowAllWorkspotDetails; // 20016447 Do this a second time because of machine-current-job
    MainTimerSwitch(True, True);
  end;
end; // OpenList

{ Open Scheme - End }

{ Save Scheme - Start }

procedure THomeF.MyActionSave;
begin
  MyAction := nil;
  SaveList(WorkFilename);
  SetCaption;
end; // MyActionSave

procedure THomeF.MyActionSaveAs;
var
  Path: String;
begin
  inherited;
  MyAction := nil;
  Path := GetCurrentDir;
  try
    SaveDialog1.InitialDir := SchemePath;
    SaveDialog1.Filename := WorkFilename;
    if SaveDialog1.Execute then
    begin
      WorkFilename := ExtractFileName(SaveDialog1.Filename);
      SaveList(WorkFilename);
    end;
    SetCaption;
  finally
    SetCurrentDir(Path);
  end;
end; // MyActionSaveAs

// Show SaveAs-Dialog and save the scheme in the folder that
// was selected with the entered filename.
procedure THomeF.actSaveAsExecute(Sender: TObject);
begin
  if not IsBusy then
    MyActionSaveAs
  else
    MyAction := MyActionSaveAs;
end; // actSaveAsExecute

// Ask for save yes/no. If yes, then save the scheme.
function THomeF.AskForSave: Boolean;
begin
  Result := False;
  if Dirty then
  begin
    Result := DisplayMessage(SPimsSaveChanges,
      mtConfirmation, [mbYes, mbNo]) = mrYes;
    if Result then
      SaveList(WorkFilename);
    Dirty := False;
  end;
end; // AskForSave

// Save the scheme.
procedure THomeF.SaveList(Filename: String);
var
  I: Integer;
  ADrawObject: PDrawObject;
  IniFile: TIniFile;
  Obj: String;
  AnImage: TImage;
  AShapeEx: TShapeEx;
  Done: Boolean;
begin
  if not tlbtnEdit.Down then
    MainTimerSwitch(False);
  // TD-21799
  ProductionScreenWaitF.lblMessage.Caption := SPimsSavingScheme;
  ProductionScreenWaitF.Show;
  Update;

  if FileExists(ExtractFilePath(SchemePath) + ExtractFileName(Filename)) then
    DeleteFile(ExtractFilePath(SchemePath) + ExtractFileName(Filename));

  IniFile := TIniFile.Create(ExtractFilePath(SchemePath) +
    ExtractFileName(Filename));

  with IniFile do
  begin
    // Write Coordinates of Window for this Scheme
    WriteInteger('Main', 'Top', HomeF.Top);
    WriteInteger('Main', 'Left', HomeF.Left);
    WriteInteger('Main', 'Width', HomeF.Width);
    WriteInteger('Main', 'Height', HomeF.Height);
    // Write following for this Scheme
    WriteInteger('Main', 'WorkspotScale', WorkspotScale);
    WriteInteger('Main', 'FontScale', FontScale);

    // PIM-194
    WriteInteger('Main', 'RedBoundary', ORASystemDM.RedBoundary);
    WriteInteger('Main', 'OrangeBoundary', ORASystemDM.OrangeBoundary);

    // 20013476
    // 20014450.50
//    WriteInteger('Main', 'CurrentPeriod', CurrentPeriod);
    // 20014450
    WriteInteger('Main', 'MachHrsMaxIdleTime', MachHrsMaxIdleTime);
    // MRA:24-JUN-2010. For compatibility with older systems:
    if EfficiencyPeriodMode = epSince then
      WriteBool('Main', 'EfficiencyPeriodSince', True) //EfficiencyPeriodSince)
    else
      WriteBool('Main', 'EfficiencyPeriodSince', False); // EfficiencyPeriodSince)
    // MRA:24-JUN-2010. New setting:
    WriteInteger('Main', 'EfficiencyPeriodMode', Integer(EfficiencyPeriodMode));
    WriteDateTime('Main', 'EfficiencyPeriodSinceTime',
      EfficiencyPeriodSinceTime);

    // PIM-223
    WriteBool('Main', 'ShowEmpInfo', ORASystemDM.PSShowEmpInfo);
    WriteBool('Main', 'ShowEmpEff', ORASystemDM.PSShowEmpEff);

    // Write Objects for this Scheme
    for I:=0 to PSList.Count-1 do
    begin
      Done := False;
      Application.ProcessMessages;
      ADrawObject := PSList.Items[I];
      Obj := 'Object' + IntToStr(I);
      WriteInteger(Obj, 'AType', Integer(ADrawObject.AType));
      if ADrawObject.ABigEffMeters then
        WriteString(Obj, 'ABigEffMeters', 'True')
      else
        WriteString(Obj, 'ABigEffMeters', 'False');
      if (ADrawObject.AType = otPicture) then // 20014450.50
      begin
        AnImage := (ADrawObject.AObject as TImage);
        WriteInteger(Obj, 'Top', AnImage.Top);
        WriteInteger(Obj, 'Left', AnImage.Left);
        WriteInteger(Obj, 'Width', ADrawObject.AWidth); // Save always original width+height
        WriteInteger(Obj, 'Height', ADrawObject.AHeight); // The AWorkspotScale is used for scaling
        WriteString(Obj, 'AImageName', ADrawObject.AImageName);
        WriteInteger(Obj, 'PictureScale', ADrawObject.AWorkspotScale);
        Done := True;
      end
      else
        if ADrawObject.AProdScreenType = pstWSEmpEff then // PIM-213
        begin
          if Assigned(ADrawObject.ApnlWSEmpEff) then
          begin
            WriteInteger(Obj, 'Top', ADrawObject.ApnlWSEmpEff.Top);
            WriteInteger(Obj, 'Left', ADrawObject.ApnlWSEmpEff.Left);
          end;
        end
        else
          if ADrawObject.AObject is TImage then
          begin
            AnImage := (ADrawObject.AObject as TImage);
            WriteInteger(Obj, 'Top', AnImage.Top);
            WriteInteger(Obj, 'Left', AnImage.Left);
          end
          else
            if ADrawObject.AObject is TShapeEx then
            begin
              AShapeEx := (ADrawObject.AObject as TShapeEx);
              WriteInteger(Obj, 'Top', AShapeEx.Top);
              WriteInteger(Obj, 'Left', AShapeEx.Left);
              WriteInteger(Obj, 'Width', AShapeEx.Width);
              WriteInteger(Obj, 'Height', AShapeEx.Height);
              Done := True;
            end;
(*          else
         if ADrawObject.AType = otPuppetBox then
             WriteInteger(Obj, 'ShowMode', Integer(ADrawObject.AShowMode)); *)
      if not Done then
      begin
        //
        // Personal Screen - Start
        //
        case ADrawObject.AProdScreenType of
        pstNoTimeRec: // No Time Rec: Workspot
          begin
            WriteString(Obj, 'AProdScreenType', 'NoTimeRec');
          end;
        pstMachineTimeRec: // Machine + Time Rec
          begin
            WriteString(Obj, 'AProdScreenType', 'MachineTimeRec');
            WriteString(Obj, 'AMachineCode',
              ADrawObject.AMachineTimeRec.AMachineCode);
            WriteString(Obj, 'AMachineDescription',
              ADrawObject.AMachineTimeRec.AMachineDescription);
          end;
        pstWorkspotTimeRec: // Workspot + Time Rec
          begin
            WriteString(Obj, 'AProdScreenType', 'WorkspotTimeRec');
            WriteString(Obj, 'AMachineCode',
              ADrawObject.AWorkspotTimeRec.AMachineCode);
            WriteString(Obj, 'AMachineDescription',
              ADrawObject.AWorkspotTimeRec.AMachineDescription);
          end;
        pstWSEmpEff: // PIM-213
          begin
            WriteString(Obj, 'AProdScreenType', 'WSEmpEff');
          end;
        end;
        //
        // Personal Screen - End
        //
        WriteString(Obj, 'APlantCode', ADrawObject.APlantCode);
        WriteString(Obj, 'AWorkspotCode', ADrawObject.AWorkspotCode);
        WriteString(Obj, 'AWorkspotDescription',
          ADrawObject.AWorkspotDescription);
        WriteString(Obj, 'AImageName', ADrawObject.AImageName);
        if ADrawObject.AProdScreenType <> pstWSEmpEff then
        begin
          WriteInteger(Obj, 'WorkspotScale', ADrawObject.AWorkspotScale); // 20015406
          WriteInteger(Obj, 'FontScale', ADrawObject.AFontScale); // 20015406
          WriteInteger(Obj, 'EffPeriodMode', Integer(ADrawObject.AEffPeriodMode)); // 20015406
        end;
      end; // if not Done
    end;
  end;

  IniFile.Free;

  Dirty := False;

  ProductionScreenWaitF.Hide;
  Update;
  if not tlbtnEdit.Down then
    MainTimerSwitch(True);
end; // SaveList

{ Save Scheme - End }

{ Calculations - Start }

(*
procedure THomeF.CalculateTimeAndNormValues(
  ADrawObject: PDrawObject;
  AodsTimeRegScan: TOracleDataSet;
  var NormProdQuantity: Double;
  var NormProdLevel: Double;
  var MinutesTillNow: Integer;
  var QuantitySum: Double);
var
  // For ComputeBreaks-function
  EmployeeData: TScannedIDCard;
  STime, ETime: TDateTime;
  ProdMin, BreaksMin, PayedBreaks: Integer;
  JobCodeToFind: String;
begin
  with ProductionScreenDM do
  begin
    // Compute breaks
    EmployeeData.PlantCode :=
      AodsTimeRegScan.FieldByName('PLANT_CODE').Value;
    EmployeeData.EmployeeCode :=
      AodsTimeRegScan.FieldByName('EMPLOYEE_NUMBER').Value;
    EmployeeData.WorkSpotCode :=
      AodsTimeRegScan.FieldByName('WORKSPOT_CODE').Value;
    EmployeeData.ShiftNumber :=
      AodsTimeRegScan.FieldByName('SHIFT_NUMBER').Value;
    STime := AodsTimeRegScan.FieldByName('DATETIME_IN').AsDateTime;
    ETime := AodsTimeRegScan.FieldByName('DATETIME_OUT').AsDateTime;
    // Value is NULL
    // Or in FUTURE? MR:02-06-2003
    if (ETime = 0) or (ETime > Now) then
      ETime := Now;
    ProdMin := 0;
    AProdMinClass.ComputeBreaks(
      EmployeeData,
      STime,
      ETime,
      1,
      { var } ProdMin, BreaksMin, PayedBreaks);
      // If more of same employee are found, one employee
      // can have scanned in more than once!
      if not odsJobcode.Active then
        odsJobcode.Active := True;

      // RV063.5.
      JobCodeToFind := AodsTimeRegScan.FieldByName('JOB_CODE').AsString;
      // Personal Screen
      if ADrawObject.AProdScreenType = pstWorkspotTimeRec then
        if (EfficiencyPeriodMode = epCurrent) then
        begin
          if ADrawObject.AWorkspotTimeRec.APreviousJobCode <> '' then
            JobCodeToFind := ADrawObject.AWorkspotTimeRec.APreviousJobCode;
        end;
      if odsJobcode.Locate('PLANT_CODE;WORKSPOT_CODE;JOB_CODE',
        VarArrayOf([ADrawObject.APlantCode,
        ADrawObject.AWorkspotCode,
        JobCodeToFind {AodsTimeRegScan.FieldByName('JOB_CODE').AsString}]),
        []) then
      begin
        //CAR 22-04-2003
        if odsJobcode.FieldByName('SHOW_AT_PRODUCTIONSCREEN_YN').AsString = 'N' then
          Exit;
        //
        NormProdLevel := odsJobcode.FieldByName('NORM_PROD_LEVEL').Value;

        // Time without breaks!
        MinutesTillNow := ProdMin;

        // We only are interested in the given period,
        // or shorter.
        // For example in the last 5 minutes.
//        if not EfficiencyPeriodSince then
        if (EfficiencyPeriodMode = epCurrent) then
        begin
          if MinutesTillNow > Trunc(PIMSDatacolTimeInterval / 60) then
            MinutesTillNow := Trunc(PIMSDatacolTimeInterval / 60);
        end;

        // RV063.5. Always take complete PQ-period.
        if (MinutesTillNow < Trunc(PIMSDatacolTimeInterval / 60)) then
          MinutesTillNow := Trunc(PIMSDatacolTimeInterval / 60);

        ADrawObject.ATotalWorkedTime := ADrawObject.ATotalWorkedTime +
          MinutesTillNow;

        NormProdQuantity := NormProdQuantity +
          MinutesTillNow * NormProdLevel / 60;

        // Store this for BIG-EFF-METER-use
        ADrawObject.ATotalNormQuantity := NormProdQuantity;
    end; // if odsJobcode.FindKey
  end;
end; // CalculateTimeAndNormValues
*)

// MR:10-03-2004
procedure THomeF.DetermineSinceDates(MyNow: TDateTime;
  var DateFrom, DateTo: TDateTime);
var
  Hour1, Min1, Sec1, MSec1: Word;
  Hour2, Min2, Sec2, MSec2: Word;
begin
  // 20016447 Related to this order, sometimes it does not find scans,
  //          to solve it: look x seconds in the future.
  DateTo := MyNow + (1/86400) * 60;
  DecodeTime(GlobalEfficiencyPeriodSinceTime, Hour1, Min1, Sec1, MSec1);
  DecodeTime(DateTo, Hour2, Min2, Sec2, MSec2);
  // If Hour1 = 23 and Hour2 = 9 then we look at yesterday
  // 20013489 Overnight Shift
  // When current time is before since time, then we must not look at yesterday!
  // Or it will give too many hours. It also will give too many scans read
  // when determining the 'employeelist'.
  // It also can result in wrong shift-date for PQ-records!
  // For example, if sincetime is 7:00 and current time is 5:00 then
  // it looked at yesterday!
  // Instead of that we just look at current time (DateTo).
  if Hour1 > Hour2 then
    DateFrom := DateTo
  else
    DateFrom := Trunc(DateTo) + Frac(GlobalEfficiencyPeriodSinceTime);
end;

{ Calculations - End }

{ Show - Start }

// Show time it took to refresh the scheme in status-bar
(*
procedure THomeF.ShowActionTime;
begin
  // Personal Screen: Do not show this.
  Exit;
{
  TimerEndTime := SysUtils.Now;
  stBarBase.Panels[3].Text :=
    TimeToStr(TimerStartTime) + '-' + TimeToStr(TimerEndTime);
  Update;
}
end; // ShowActionTime
*)
// Show some info about the current object in the status-bar.
procedure THomeF.ShowObjectNumber(Sender: TObject);
begin
  if CurrentDrawObject = nil then
    Exit;

  if Sender = nil then
    Exit;

  if (Sender is TImage) then
  begin
    //
    // Personal Screen - Start
    //
    case CurrentDrawObject.AProdScreenType of
    pstNoTimeRec: // No Time Rec: Workspot
      begin
        stBarBase.Panels[0].Text := 'Object=' +
          IntToStr((Sender as TImage).Tag) + ' (' +
          CurrentDrawObject.AWorkspotCode + Str_Sep +
          CurrentDrawObject.AWorkspotDescription + ')';
      end;
    pstMachineTimeRec: // Machine + Time Rec
      begin
        stBarBase.Panels[0].Text := 'Object=' +
          IntToStr((Sender as TImage).Tag) + ' (' +
          CurrentDrawObject.AMachineTimeRec.AMachineCode + Str_Sep +
          CurrentDrawObject.AMachineTimeRec.AMachineDescription + ')';
      end;
    pstWorkspotTimeRec, pstWSEmpEff: // Workspot + Time Rec // PIM-213
      begin
        stBarBase.Panels[0].Text := 'Object=' +
          IntToStr((Sender as TImage).Tag) + ' (' +
          CurrentDrawObject.AWorkspotCode + Str_Sep +
          CurrentDrawObject.AWorkspotDescription + ')';
      end;
    end;
    //
    // Personal Screen - End
    //
  end
  else
    if (Sender is TShapeEx) then
      stBarBase.Panels[0].Text := 'Object=' +
        IntToStr((Sender as TShapeEx).Tag) + ' (' +
        CurrentDrawObject.AWorkspotCode + Str_Sep +
        CurrentDrawObject.AWorkspotDescription + ')';
end; // ShowObjectNumber

procedure THomeF.ShowChangeProperties(Sender: TObject);
begin
  if Sender = nil then
    Exit;
  ShowObjectNumber(Sender);
  if (Sender is TImage) then
  begin
    CurrentDrawObject := ThisDrawObject((Sender as TImage).Tag);
    DialogImagePropertiesF := TDialogImagePropertiesF.Create(Application);
    DialogImagePropertiesF.ImageTop := (Sender as TImage).Top;
    DialogImagePropertiesF.ImageLeft := (Sender as TImage).Left;
    DialogImagePropertiesF.ImageWidth := (Sender as TImage).Width;
    DialogImagePropertiesF.ImageHeight := (Sender as TImage).Height;
    DialogImagePropertiesF.PlantCode := CurrentDrawObject.APlantCode;
    DialogImagePropertiesF.WorkspotCode := CurrentDrawObject.AWorkspotCode;
    DialogImagePropertiesF.WorkspotDescription :=
      CurrentDrawObject.AWorkspotDescription;
    ModalResult := DialogImagePropertiesF.ShowModal;
    if ModalResult = mrOK then
    begin
      (Sender as TImage).Top := DialogImagePropertiesF.ImageTop;
      (Sender as TImage).Left := DialogImagePropertiesF.ImageLeft;
      (Sender as TImage).Height := DialogImagePropertiesF.ImageHeight;
      (Sender as TImage).Width := DialogImagePropertiesF.ImageWidth;
    end;
    DialogImagePropertiesF.Free;
  end
  else
    if (Sender is TShapeEx) then
    begin
      CurrentDrawObject := ThisDrawObject((Sender as TShapeEx).Tag);
      DialogImagePropertiesF := TDialogImagePropertiesF.Create(Application);
      DialogImagePropertiesF.ImageTop := (Sender as TShapeEx).Top;
      DialogImagePropertiesF.ImageLeft := (Sender as TShapeEx).Left;
      DialogImagePropertiesF.ImageWidth := (Sender as TShapeEx).Width;
      DialogImagePropertiesF.ImageHeight := (Sender as TShapeEx).Height;
      DialogImagePropertiesF.PlantCode := '';
      DialogImagePropertiesF.WorkspotCode := '';
      DialogImagePropertiesF.WorkspotDescription := '';
      ModalResult := DialogImagePropertiesF.ShowModal;
      if ModalResult = mrOK then
      begin
        (Sender as TShapeEx).Top := DialogImagePropertiesF.ImageTop;
        (Sender as TShapeEx).Left := DialogImagePropertiesF.ImageLeft;
        (Sender as TShapeEx).Height := DialogImagePropertiesF.ImageHeight;
        (Sender as TShapeEx).Width := DialogImagePropertiesF.ImageWidth;
      end;
      DialogImagePropertiesF.Free;
    end;
end; // ShowChangeProperties

procedure THomeF.ShowEffMeter(ADrawObject: PDrawObject; APercentage: Double;
  Sender: TObject);
const
  AWIDTH=42;
var
  APerc: Double;
  // Show efficiency meter that consists of a red and green rectangle.
  procedure ShowEfficiencyMeter;
  begin
    // To be sure the eff-meters are reset if 'APercentage = 0'
    // compare with '>= 0' instead of '> 0'.
    if APercentage >= 0 then // APercentage is 1 to 100
    begin
      APerc := AWIDTH / 100 * APercentage;
      ADrawObject.AEffPercentage := Round(APerc);

      // Green
      ADrawObject.AEffMeterRed.Visible := False;
      with ADrawObject.AEffMeterGreen do
      begin
        BringToFront;
        Visible := True;
        Left := (Sender as TImage).Left +
          (Sender as TImage).Width DIV 2;
        if ADrawObject.ABigEffMeters then
        begin
          // 20015406
          Top := (Sender as TImage).Top -
            Trunc(5 * ADrawObject.AWorkspotScale / 100);
          Height := Trunc(8 * BIGEFFMETERS_SCALE_PERCENTAGE / 100 *
            ADrawObject.AWorkspotScale / 100);
          Width := Trunc(ADrawObject.AEffPercentage *
            BIGEFFMETERS_SCALE_PERCENTAGE / 100 * ADrawObject.AWorkspotScale / 100);
        end
        else
        begin
          Top := (Sender as TImage).Top - Trunc(11 * ADrawObject.AWorkspotScale / 100);
          Width := Trunc(ADrawObject.AEffPercentage * ADrawObject.AWorkspotScale / 100);
        end;
      end;
    end
    else
    begin
      // Red
      if APercentage < 0 then // APercentage is -1 to -100
      begin
        // First make a positive number
        APercentage := APercentage * -1;

        APerc := AWIDTH / 100 * APercentage;
        ADrawObject.AEffPercentage := Round(APerc);

        ADrawObject.AEffMeterGreen.Visible := False;
        with ADrawObject.AEffMeterRed do
        begin
          BringToFront;
          Visible := True;
          if ADrawObject.ABigEffMeters then
          begin
            // 20015406
            Top := (Sender as TImage).Top - Trunc(5 * ADrawObject.AWorkspotScale / 100);
            Height := Trunc(8 * BIGEFFMETERS_SCALE_PERCENTAGE / 100 *
              ADrawObject.AWorkspotScale / 100);
            Left := (Sender as TImage).Left +
              (Sender as TImage).Width DIV 2 -
              Trunc(ADrawObject.AEffPercentage *
              BIGEFFMETERS_SCALE_PERCENTAGE / 100 * ADrawObject.AWorkspotScale / 100);
            Width := Trunc(ADrawObject.AEffPercentage *
              BIGEFFMETERS_SCALE_PERCENTAGE / 100 * ADrawObject.AWorkspotScale / 100);
          end
          else
          begin
            Top := (Sender as TImage).Top - Trunc(11 * ADrawObject.AWorkspotScale / 100);
            Left := (Sender as TImage).Left +
              (Sender as TImage).Width DIV 2 - Trunc(ADrawObject.AEffPercentage *
              ADrawObject.AWorkspotScale / 100);
            Width := Trunc(ADrawObject.AEffPercentage * ADrawObject.AWorkspotScale / 100);
          end;
        end;
        PlaySound;
      end;
    end;
  end; // ShowEfficiencyMeter
  // PIM-194
  procedure ShowProgressBar;
  begin
    if Assigned(ADrawObject.AProgressBar) then
    begin
      if ADrawObject.ABigEffMeters then
      begin
        with ADrawObject.AProgressBar do
        begin
          BringToFront;
          Max := ORASystemDM.OrangeBoundary;
          Font.Size := Trunc(pnlDraw.Font.Size * 1.5 * ADrawObject.AFontScale / 100);
          Position := Round(ADrawObject.ATotalPercentage);
          // This must be done to make it possible to show the ghost-image
          if (Position = 0) or (ADrawObject.ACurrentNumberOfEmployees = 0) then
            Visible := False
          else
          begin
            if (Position < 60) then
              Font.Color := clBlack
            else
              Font.Color := clWhite;
            if (Position <= ORASystemDM.RedBoundary) then
            begin
              BeginColor := clMyRed;
              EndColor := clMyRed;
            end
            else
              if (Position <= ORASystemDM.OrangeBoundary) then
              begin
                BeginColor := clMyOrange;
                EndColor := clMyOrange
              end
              else
              begin
                BeginColor := clMyGreen;
                EndColor := clMyGreen;
              end;
            Left := ADrawObject.ABorder.Left + 1;
            Top := ADrawObject.ABorder.Top + 1;
            Height := ADrawObject.ABorder.Height - 2;
            Width := ADrawobject.ABorder.Width - 2;
            Visible := True;
          end;
        end;
      end;
    end;
  end; // ShowProgressBar
begin
  if Sender = nil then
    Exit;
  if not ADrawObject.AVisible then
    Exit;
  ADrawObject.ATotalPercentage := APercentage;
  APercentage := PrecalcPercentage(ADrawObject, APercentage);

  if ADrawObject.ABigEffMeters then
  begin
    if ADrawObject.ANoData then
      ADrawObject.ABigEffLabel.Caption := SPimsNoData
    else
      ADrawObject.ABigEffLabel.Caption := '';
  end;

  // PIM-194
  ShowProgressBar;
  // ShowEfficiencyMeter;

end; // ShowEffMeter

// PersonalScreen
(*
procedure THomeF.ShowPuppets(ADrawObject: PDrawObject; Sender: TObject);
var
  I, J, ALeft, ATop, AMaxWidth: Integer;
  APuppetShift: PTPuppetShift;
  APuppet: PTPuppet;
begin
  if ADrawObject = nil then
    Exit;
  if Sender = nil then
    Exit;

  ATop := 0;
  ALeft := 0;
  AMaxWidth := 0;
  // If Sender = TShapeEx then Sender is a 'PuppetBox'.
  // In that case, the shifts don't matter, all puppets must be shown
  // in space of the rectangle.
  if (Sender is TShapeEx) then
  begin
    AMaxWidth := (Sender as TShapeEx).Left +
      ADrawObject.ADeptRect.ATopLineHorz.Width;
  end;
  // Show all puppets of all shifts
  if ADrawObject.APuppetShiftList.Count > 0 then
  begin
    if (Sender is TImage) then
      ATop := (Sender as TImage).Top + (Sender as TImage).Height +
        Trunc(24 * WorkspotScale / 100)
    else
      if (Sender is TShapeEx) then // PuppetBox
        ATop := (Sender as TShapeEx).Top + Trunc(5 * WorkspotScale / 100);
    for I := 0 to ADrawObject.APuppetShiftList.Count - 1 do
    begin
      if (Sender is TImage) then
        ALeft := (Sender as TImage).Left -
          Trunc(15 * WorkspotScale / 100)
      else
        if (Sender is TShapeEx) then // PuppetBox
          if (I = 0) then // Do only first time!
            ALeft := (Sender as TShapeEx).Left +
              Trunc(5 * WorkspotScale / 100);
      APuppetShift := ADrawObject.APuppetShiftList.Items[I];
      // One row shows all puppets of one Shift!
      if APuppetShift.APuppetList.Count > 0 then
      begin
        for J := 0 to APuppetShift.APuppetList.Count - 1 do
        begin
          APuppet := APuppetShift.APuppetList.Items[J];
          ShowPuppet(APuppet, ALeft, ATop, APuppet.AColor);
          ALeft := ALeft +
            Trunc(15 * WorkspotScale / 100);
          if (Sender is TShapeEx) then // PuppetBox
          begin
            if ((ALeft + Trunc(15 * WorkspotScale / 100)) >= AMaxWidth) then
            begin
              // Go to next row on following line, at left position.
              ALeft := (Sender as TShapeEx).Left +
                Trunc(5 * WorkspotScale / 100);
              ATop := ATop + Trunc(32 * WorkspotScale / 100);
            end;
          end;
        end;
      end;
      // Next shift on next row!
      if (Sender is TImage) then
        ATop := ATop + Trunc(32 * WorkspotScale / 100);
    end;
  end;
end; // ShowPuppets
*)

(*
procedure THomeF.ShowEmployeeList(ADrawObject: PDrawObject);
var
  APTAEmployee: PTAEmployee;
  I, Top, Left: Integer;
  FontSize: Integer;
  TopAdd: Integer;
begin
  if ADrawObject = nil then
    Exit;
  if not ADrawObject.ABigEffMeters then
    Exit;

  case ADrawObject.AProdScreenType of
  pstWorkspotTimeRec: // Personal Screen
    begin
      FontSize := Round(12 * FontScale / 100);
      TopAdd := Round(FontSize * 1.5);
      Top := ADrawObject.ABorder.Top + ADrawObject.ABorder.Height;
    end;
  else // Default
    begin
      FontSize := 10; // No scaling for font
      TopAdd := Round(FontSize * 1.5);
      Top := ADrawObject.ABorder.Top + ADrawObject.ABorder.Height +
        ADrawObject.AEffTotalsLabel.Font.Size * 2;
    end;
  end;

  if ADrawObject.AEmployeeList.Count > 0 then
  begin
    Left := ADrawObject.ABorder.Left;
    for I := 0 to ADrawObject.AEmployeeList.Count - 1 do
    begin
      APTAEmployee := ADrawObject.AEmployeeList.Items[I];
      // Name
      APTAEmployee.AEmployeeNameLabel.Font.Size := FontSize;
      APTAEmployee.AEmployeeNameLabel.Top := Top;
      APTAEmployee.AEmployeeNameLabel.Left := Left  +
        Round(ADrawObject.ABorder.Width * 1/30);
      // When clicking on employee-name, show employee list.
      APTAEmployee.AEmployeeNameLabel.OnClick := ShowEmployees1Click;
      //
      // Personal Screen - Start
      //
      if ADrawObject.AProdScreenType = pstWorkspotTimeRec then
      begin
        // Worked Pieces
        APTAEmployee.AWorkedPiecesLabel.Caption :=
          IntToStr(APTAEmployee.AWorkedPieces);
        APTAEmployee.AWorkedPiecesLabel.Width :=
          Round(ADrawObject.AWorkspotTimeRec.AModeButton.Width * 1/3);
        APTAEmployee.AWorkedPiecesLabel.Font.Size :=
          APTAEmployee.AEmployeeNameLabel.Font.Size;
        APTAEmployee.AWorkedPiecesLabel.Top := Top;
        APTAEmployee.AWorkedPiecesLabel.Left := Left +
          Round(ADrawObject.ABorder.Width * 4/6);
        // Worked Time
        APTAEmployee.AWorkedMinutesLabel.Caption :=
          IntMin2StringTime(APTAEmployee.AWorkedMinutes, True);
        APTAEmployee.AWorkedMinutesLabel.Font.Size :=
          APTAEmployee.AEmployeeNameLabel.Font.Size;
        APTAEmployee.AWorkedMinutesLabel.Top := Top;
        APTAEmployee.AWorkedMinutesLabel.Left := Left +
          Round(ADrawObject.ABorder.Width * 5/6);
      end;
      //
      // Personal Screen - End
      //
      // Next employee on next row
      Top := Top + TopAdd;
        //+ Trunc(APTAEmployee.AEmployeeNameLabel.Font.Size * 1.5); // 16
    end;
  end;
end; // ShowEmployeeList
*)

procedure THomeF.ShowDrawObject(ADrawObject: PDrawObject;
  Sender: TObject);
  procedure ShowShape(AShape: TShape; AVisible: Boolean;
    ATop, ALeft, AWidth, AHeight: Integer);
  begin
    AShape.Visible := AVisible;
    AShape.Top := (Sender as TImage).Top +
      Trunc(ATop * ADrawObject.AWorkspotScale / 100);
    AShape.Left := (Sender as TImage).Left +
      Trunc(ALeft * ADrawObject.AWorkspotScale / 100);
    AShape.Width := (Sender as TImage).Width +
      Trunc(AWidth * ADrawObject.AWorkspotScale / 100);
    AShape.Height := (Sender as TImage).Height +
      Trunc(AHeight * ADrawObject.AWorkspotScale / 100);
  end;
  procedure ShowButton(AButton: TButton; AVisible: Boolean;
    ATop, ALeft, AWidth, AHeight: Integer);
  begin
    AButton.Visible := AVisible;
    if ADrawObject.AProdScreenType = pstWSEmpEff then
    begin
    {
      AButton.Top := ATop;
      AButton.Left := ALeft;
      AButton.Width := AWidth;
      AButton.Height := AHeight;
    }
    end
    else
    begin
      AButton.Top := (Sender as TImage).Top +
        Trunc(ATop * ADrawObject.AWorkspotScale / 100);
      AButton.Left := (Sender as TImage).Left +
        Trunc(ALeft * ADrawObject.AWorkspotScale / 100);
      AButton.Width := (Sender as TImage).Width +
        Trunc(AWidth * ADrawObject.AWorkspotScale / 100);
      AButton.Height := (Sender as TImage).Height +
        Trunc(AHeight * ADrawObject.AWorkspotScale / 100);
    end;
  end;
  procedure ShowBorder(AVisible: Boolean;
    ATop, ALeft, AWidth, AHeight: Integer);
  begin
    // Border
    // 20015406
    ShowShape(ADrawObject.ABorder, AVisible, ATop, ALeft, AWidth, AHeight);
  end;
  // Personal Screen
  procedure ShowJobButton(AVisible: Boolean;
    ATop, ALeft, AWidth, AHeight: Integer);
    // Determine jobs from workspot belonging to the machine.
  var
    AJobButton: TButton;
    procedure ShowJobButtonColor; // 20016447
    begin
      if Assigned(ADrawObject.AWorkspotTimeRec) then
      begin
        // Show JobButton in different color
        if ADrawObject.AMachineWorkspotJobDifferent then
        begin
          ADrawObject.AWorkspotTimeRec.AJobButton.BackColor := clRed; // background
          ADrawObject.AWorkspotTimeRec.AJobButton.ForeColor := clYellow; // text
          ADrawObject.AWorkspotTimeRec.AJobButton.HoverColor := clNavy; // mouse over
        end
        else
        begin
          ADrawObject.AWorkspotTimeRec.AJobButton.BackColor :=
            ADrawObject.AWorkspotTimeRec.ABackColor;
          ADrawObject.AWorkspotTimeRec.AJobButton.ForeColor :=
            ADrawObject.AWorkspotTimeRec.AForeColor;
          ADrawObject.AWorkspotTimeRec.AJobButton.HoverColor :=
            ADrawObject.AWorkspotTimeRec.AHoverColor;
        end;
      end;
    end; // ShowJobButtonColor
    function DetermineJob(AOldJob: String): String;
    var
      PlantCode: String;
      MachineCode: String;
      ADrawObjectWS: PDrawObject;
      I: Integer;
      JobCount: Integer;
      LastJobCode: String;
      LastJobDescription: String;
    begin
      PlantCode := ADrawObject.APlantCode;
      MachineCode := ADrawObject.AMachineTimeRec.AMachineCode;
      JobCount := 0;
      LastJobCode := '';
      LastJobDescription := '';
      for I := 0 to PSList.Count - 1 do
      begin
        ADrawObjectWS := PSList.Items[I];
        if (ADrawObjectWS.AObject is TImage) then
        begin
          if ADrawObjectWS.AProdScreenType = pstWorkspotTimeRec then
            if (ADrawObjectWS.AWorkspotTimeRec.AMachineCode = MachineCode) and
              (ADrawObjectWS.APlantCode = PlantCode) then
            begin
              if (LastJobCode <> ADrawObjectWS.AWorkspotTimeRec.ACurrentJobCode) and
                (ADrawObjectWS.AWorkspotTimeRec.ACurrentJobCode <> '') then
              begin
                LastJobCode := ADrawObjectWS.AWorkspotTimeRec.ACurrentJobCode;
                LastJobDescription :=
                  ADrawObjectWS.AWorkspotTimeRec.ACurrentJobDescription;
                if LastJobCode <> '' then
                  inc(JobCount);
              end;
            end;
        end;
      end;
      if JobCount = 0 then
        Result := ''
      else
        if JobCount = 1 then
        begin
          // Set Current JobCode here!
          ADrawObject.AMachineTimeRec.ACurrentJobCode := LastJobCode;
          ADrawObject.AMachineTimeRec.ACurrentJobDescription :=
            LastJobDescription;
          Result := LastJobDescription
        end
        else
        begin
          // 20016447 Return the last remembered/found job
//          Result := SPimsMultiple;
          if ADrawObject.AMachineTimeRec.ACurrentJobDescription <> '' then
            Result := ADrawObject.AMachineTimeRec.ACurrentJobDescription
          else
          begin
            ADrawObject.AMachineTimeRec.ACurrentJobCode := LastJobCode;
            ADrawObject.AMachineTimeRec.ACurrentJobDescription :=
              LastJobDescription;
            Result := LastJobDescription
          end;
        end;
      // 20014450
      ADrawObject.AMachineTimeRec.AJobCount := JobCount;
      if ORASystemDM.RegisterMachineHours then
        if JobCount <> 1 then
        begin
          //ADrawObject.AMachineTimeRec.ACurrentJobCode := LastJobCode;
          //ADrawObject.AMachineTimeRec.ACurrentJobDescription :=
          //  LastJobDescription;
          Result := ADrawObject.AMachineTimeRec.ACurrentJobDescription;
        end;
      ADrawObject.AMachineJobChanged := AOldJob <> Result; // PIM-111
    end; // DetermineJob
  begin
    // Job Button
    case ADrawObject.AProdScreenType of
    pstMachineTimeRec:
      AJobButton := ADrawObject.AMachineTimeRec.AJobButton;
    else
      AJobButton := ADrawObject.AWorkspotTimeRec.AJobButton;
    end;
    // 20015406
    AJobButton.Visible := AVisible;
    AJobButton.Font.Size :=
      Trunc(pnlDraw.Font.Size * FONT_SIZE_FACTOR * ADrawObject.AWorkspotScale / 100);

    ShowButton(AJobButton, AVisible, ATop, ALeft, AWidth, AHeight);
    
    if ADrawObject.AProdScreenType = pstMachineTimeRec then
      AJobButton.Caption := DetermineJob(AJobButton.Caption) // PIM-111
    else
    begin
      // Assign a description for 'Down'-job here. Bugfix related to 20013516
      if ADrawObject.AWorkspotTimeRec.ACurrentJobCode = MECHANICAL_DOWN_JOB then
        ADrawObject.AWorkspotTimeRec.ACurrentJobDescription :=
          SPimsMechanicalDown
      else
        if ADrawObject.AWorkspotTimeRec.ACurrentJobCode = NO_MERCHANDIZE_JOB then
          ADrawObject.AWorkspotTimeRec.ACurrentJobDescription :=
            SPimsNoMerchandize;
      AJobButton.Caption := ADrawObject.AWorkspotTimeRec.ACurrentJobDescription;
    end;
    // PIM-12.3 Also check if there are still employees scanned in!
    if Assigned(ADrawObject.AWorkspotTimeRec) then
    begin
      if Assigned(ADrawObject.AEmployeeDisplayList) then
      begin
        if ADrawObject.AEmployeeDisplayList.Count = 0 then
          AJobButton.Caption := '';
      end
      else
        AJobButton.Caption := '';
    end;
    if AJobButton.Caption = '' then
      AJobButton.Caption := SPimsNotScannedIn; // SPimsNoJobs;
    AJobButton.Enabled := (AJobButton.Caption <> SPimsNotScannedIn) and // SPimsNoJobs
      (AJobButton.Caption <> SPimsMultiple);
    // TD-24819 Disable jobs-button for compare jobs + not-EmpScanIn
    if Assigned(ADrawObject.AWorkspotTimeRec) then
      if ADrawObject.AWorkspotTimeRec.AIsCompareWorkspot and
        (not ADrawObject.AWorkspotTimeRec.AEmpsScanIn) then
          AJobButton.Enabled := False;
    if AJobButton.Caption = SPimsNotScannedIn then
      AJobButton.Font.Color := clRed
    else
      AJobButton.Font.Color := clBlack;
    // 20015178
    if ORASystemDM.EnableMachineTimeRec then
      if Assigned(ADrawObject.AWorkspotTimeRec) then
        if ADrawObject.AWorkspotTimeRec.ATimeRecByMachine then
          AJobButton.Enabled := True;
    // 20014450
    if ORASystemDM.RegisterMachineHours then
      if ADrawObject.AProdScreenType = pstMachineTimeRec then
        AJobButton.Enabled := True;
    JobButtonAdjustSize(ADrawObject, AJobButton); // 20015406
    ShowJobButtonColor; // 20016447
  end; // ShowJobButton
  // Personal Screen
  procedure ShowInButton(AVisible: Boolean;
    ATop, ALeft, AWidth, AHeight: Integer);
  begin
    // 20015406
    ADrawObject.AWorkspotTimeRec.AInButton.Font.Size :=
      Trunc(pnlDraw.Font.Size * FONT_SIZE_FACTOR * ADrawObject.AWorkspotScale / 100);
    ShowButton(ADrawObject.AWorkspotTimeRec.AInButton,
      AVisible, ATop, ALeft, AWidth, AHeight);
  end;
  // Personal Screen
  procedure ShowModeButton(AVisible: Boolean;
    ATop, ALeft, AWidth, AHeight: Integer);
  begin
    // 20015406
    ADrawObject.AWorkspotTimeRec.AModeButton.Font.Size :=
      Trunc(pnlDraw.Font.Size * FONT_SIZE_FACTOR * ADrawObject.AWorkspotScale / 100);
    ShowButton(ADrawObject.AWorkspotTimeRec.AModeButton,
      AVisible, ATop, ALeft, AWidth, AHeight);
    // Mode must be determined
//    ADrawObject.AWorkspotTimeRec.AModeButton.Caption := SPimsModeButtonCurrent;
  end;
  procedure ShowLabel(AVisible: Boolean;
    ATop, ALeft, AWidth, AHeight: Integer);
  begin
    // Show Label
    // 20015406
    ADrawObject.ALabel.Visible := AVisible;
    ADrawObject.ALabel.Font.Size :=
      Trunc(pnlDraw.Font.Size * FONT_SIZE_FACTOR * ADrawObject.AWorkspotScale / 100);
    ADrawObject.ALabel.Top := (Sender as TImage).Top +
      Trunc(ATop * ADrawObject.AWorkspotScale / 100);
    ADrawObject.ALabel.Left := (Sender as TImage).Left +
      Trunc(ALeft * ADrawObject.AWorkspotScale / 100);
  end;
  procedure ShowEffTotalsLabel(AVisible: Boolean;
    ATop, ALeft, AWidth, AHeight: Integer);
  begin
    // Show Eff Totals Label
    // Totals for workspot
    // Show top-line in larger font
    // 20015406
    ADrawObject.AEffTotalsLabel.Visible := AVisible;
    ADrawObject.AEffTotalsLabel.Font.Size :=
      Trunc(pnlDraw.Font.Size * 1.5 * ADrawObject.AFontScale / 100);
    ADrawObject.AEffTotalsLabel.Top :=
      ADrawObject.ABorder.Top + ADrawObject.ABorder.Height +
      Trunc(ATop * ADrawObject.AWorkspotScale / 100);
    ADrawObject.AEffTotalsLabel.Left :=
      ADrawObject.ABorder.Left;
  end;
  procedure ShowPercentageLabel(AVisible: Boolean;
    ATop, ALeft, AWidth, AHeight: Integer);
  begin
    // Show percentage label
    ADrawObject.AWorkspotTimeRec.APercentageLabel.Visible := AVisible;
    ADrawObject.AWorkspotTimeRec.APercentageLabel.Top :=
      ADrawObject.ABorder.Top + Trunc(ADrawObject.ABorder.Height * 1/8); // 1/5
    // Left position depends on the shown percentage
    if ADrawObject.ATotalPercentage >= 100 then
      ADrawObject.AWorkspotTimeRec.APercentageLabel.Left :=
        ADrawObject.ABorder.Left + Trunc(ADrawObject.ABorder.Width * 1/4)
    else
      ADrawObject.AWorkspotTimeRec.APercentageLabel.Left :=
        ADrawObject.ABorder.Left + Trunc(ADrawObject.ABorder.Width * 3/4);
    // 20015406
    ADrawObject.AWorkspotTimeRec.APercentageLabel.Font.Size :=
      Trunc(pnlDraw.Font.Size * 1.5 * ADrawObject.AFontScale / 100);
  end;
  procedure ShowEffMeter(AVisible: Boolean;
    ATop, ALeft, AWidth, AHeight: Integer);
  begin
    // Efficiency Meter
    // RV075.1 ATop: Add ATop, not subtract.
    // 20015406

    ADrawObject.AEffMeterGreen.Visible := AVisible;
    ADrawObject.AEffMeterRed.Visible := AVisible;

    // EffMeterGreen
    ADrawObject.AEffMeterGreen.Top := (Sender as TImage).Top +
      Trunc(ATop * ADrawObject.AWorkspotScale / 100);
    ADrawObject.AEffMeterGreen.Left := (Sender as TImage).Left +
      (Sender as TImage).Width DIV 2;
    // 20015606 EffMeterGreen-Height must be a percentage of Border-Height
    ADrawObject.AEffMeterGreen.Height :=
      Round(ADrawObject.ABorder.Height * 74 / 100);

    // EffMeterRed
    ADrawObject.AEffMeterRed.Top := (Sender as TImage).Top +
      Trunc(ATop * ADrawObject.AWorkspotScale / 100);
    ADrawObject.AEffMeterRed.Left := (Sender as TImage).Left +
      (Sender as TImage).Width DIV 2 - Trunc(ADrawObject.AEffPercentage *
      BIGEFFMETERS_SCALE_PERCENTAGE / 100 * ADrawObject.AWorkspotScale / 100);
    // 20015606 EffMeterRed-Height must be a percentage of Border-Height
    ADrawObject.AEffMeterRed.Height :=
      Round(ADrawObject.ABorder.Height * 74 / 100);
  end;
  // PIM-194
  procedure ShowProgressBar(AVisible: Boolean;
    ATop, ALeft, AWidth, AHeight: Integer);
  begin
    if Assigned(ADrawObject.AProgressBar) then
    begin
      with ADrawObject.AProgressBar do
      begin
        if ADrawObject.AProgressBar.Position = 0 then
          Visible := False
        else
          Visible := AVisible;
        Font.Size :=
          Trunc(pnlDraw.Font.Size * 1.5 * ADrawObject.AFontScale / 100);
        if (Position < 60) then
          Font.Color := clBlack
        else
          Font.Color := clWhite;
        Top := ADrawObject.ABorder.Top + 1;
        Left := ADrawObject.ABorder.Left + 1;
        Height := ADrawObject.ABorder.Height - 2;
        Width := ADrawobject.ABorder.Width - 2;
      end;
    end;
  end; // ShowProgressBar
  procedure ShowBigEffLabel(AVisible: Boolean;
    ATop, ALeft, AWidth, AHeight: Integer);
  begin
    // Big Efficiency Meter Label
    // 20015406
    ADrawObject.ABigEffLabel.Visible := AVisible;
    ADrawObject.ABigEffLabel.Font.Size :=
      Trunc(pnlDraw.Font.Size * FONT_SIZE_FACTOR * ADrawObject.AWorkspotScale / 100);
    ADrawObject.ABigEffLabel.Left := ADrawObject.ABorder.Left +
      Trunc(ALeft * ADrawObject.AWorkspotScale / 100);
    ADrawObject.ABigEffLabel.Top := ADrawObject.ABorder.Top;
  end;
  // Personal Screen
  procedure ShowActualTargetLabel(AVisible: Boolean;
    ATop, ALeft, AWidth, AHeight: Integer);
  var
    TopLine: Integer;
    ValueLeft: Integer;
    ValueWidth: Integer;
    FontSize: Integer;
    TopAdd: Integer;
  begin
    // 20015406.80
    if ADrawObject.AFontScale = 0 then
    begin
      AVisible := False;
    end;
    // Show actual/target labels.
    // First Line
    // Actual Name
    // 20015406
    TopLine := 0;
    ValueLeft := Round(ADrawObject.AWorkspotTimeRec.AModeButton.Width * 2/3);
    ValueWidth := Round(ADrawObject.AWorkspotTimeRec.AModeButton.Width * 1/3);
    ValueWidth := ValueWidth + Round(ValueWidth * 1/10);
    ValueLeft := ValueLeft - Round(ValueWidth * 1/10);
    FontSize := Round(12 * ADrawObject.AFontScale / 100);
    TopAdd := Round(FontSize * 1.3);

    // 20015406
    ADrawObject.AWorkspotTimeRec.AActualNameLabel.Visible := AVisible;
    ADrawObject.AWorkspotTimeRec.AActualNameLabel.Font.Size := FontSize;
    ADrawObject.AWorkspotTimeRec.AActualNameLabel.Top :=
      TopLine +
      ADrawObject.ABorder.Top + ADrawObject.ABorder.Height +
      Trunc(ATop * ADrawObject.AWorkspotScale / 100);
    ADrawObject.AWorkspotTimeRec.AActualNameLabel.Left :=
      (Sender as TImage).Left + Trunc(ALeft * ADrawObject.AWorkspotScale / 100);
    // Actual Value
    // 20015406
    ADrawObject.AWorkspotTimeRec.AActualValueLabel.Visible := AVisible;
    ADrawObject.AWorkspotTimeRec.AActualValueLabel.Font.Size :=
      Trunc(12 * ADrawObject.AFontScale / 100);
    ADrawObject.AWorkspotTimeRec.AActualValueLabel.Top :=
      ADrawObject.ABorder.Top + ADrawObject.ABorder.Height +
      Trunc(ATop * ADrawObject.AWorkspotScale / 100);
    ADrawObject.AWorkspotTimeRec.AActualValueLabel.Left := ValueLeft +
      (Sender as TImage).Left + Trunc(ALeft * ADrawObject.AWorkspotScale / 100);
    ADrawObject.AWorkspotTimeRec.AActualValueLabel.Width := ValueWidth;

    // Second Line
    // Target Name
    TopLine := TopLine + TopAdd;

    // 20015406
    ADrawObject.AWorkspotTimeRec.ATargetNameLabel.Visible := AVisible;
    ADrawObject.AWorkspotTimeRec.ATargetNameLabel.Font.Size := FontSize;
    ADrawObject.AWorkspotTimeRec.ATargetNameLabel.Top :=
      TopLine +
      ADrawObject.ABorder.Top + ADrawObject.ABorder.Height +
      Trunc(ATop * ADrawObject.AWorkspotScale / 100);
    ADrawObject.AWorkspotTimeRec.ATargetNameLabel.Left :=
      (Sender as TImage).Left + Trunc(ALeft * ADrawObject.AWorkspotScale / 100);
    // Target Value
    // 20015406
    ADrawObject.AWorkspotTimeRec.ATargetValueLabel.Visible := AVisible;
    ADrawObject.AWorkspotTimeRec.ATargetValueLabel.Font.Size :=
      Trunc(12 * ADrawObject.AFontScale / 100);
    ADrawObject.AWorkspotTimeRec.ATargetValueLabel.Top :=
      TopLine +
      ADrawObject.ABorder.Top + ADrawObject.ABorder.Height +
      Trunc(ATop * ADrawObject.AWorkspotScale / 100);
    ADrawObject.AWorkspotTimeRec.ATargetValueLabel.Left := ValueLeft +
      (Sender as TImage).Left + Trunc(ALeft * ADrawObject.AWorkspotScale / 100);
    ADrawObject.AWorkspotTimeRec.ATargetValueLabel.Width := ValueWidth;

    // Third Line
    // Line
    TopLine := TopLine + TopAdd;

    // 20015406
    ADrawObject.AWorkspotTimeRec.ADiffLineHorz.Visible := AVisible;
    ADrawObject.AWorkspotTimeRec.ADiffLineHorz.Top :=
      TopLine +
      ADrawObject.ABorder.Top + ADrawObject.ABorder.Height +
      Trunc(ATop * ADrawObject.AWorkspotScale / 100) -
      Round(TopAdd * 1/10);
    ADrawObject.AWorkspotTimeRec.ADiffLineHorz.Left :=
      (Sender as TImage).Left + Trunc(ALeft * ADrawObject.AWorkspotScale / 100);
    ADrawObject.AWorkspotTimeRec.ADiffLineHorz.Width :=
      ADrawObject.AWorkspotTimeRec.AModeButton.Width;

    // Fourth Line
    // Extra/Short Name (Diff)

    // 20015406
    ADrawObject.AWorkspotTimeRec.ADiffNameLabel.Visible := AVisible;
    ADrawObject.AWorkspotTimeRec.ADiffNameLabel.Font.Size := FontSize;
    ADrawObject.AWorkspotTimeRec.ADiffNameLabel.Top :=
      TopLine +
      ADrawObject.ABorder.Top + ADrawObject.ABorder.Height +
      Trunc(ATop * ADrawObject.AWorkspotScale / 100);
    ADrawObject.AWorkspotTimeRec.ADiffNameLabel.Left :=
      (Sender as TImage).Left + Trunc(ALeft * ADrawObject.AWorkspotScale / 100);
    // Extra/Short Value (Diff)
    // 20015406
    ADrawObject.AWorkspotTimeRec.ADiffValueLabel.Visible := AVisible;
    ADrawObject.AWorkspotTimeRec.ADiffValueLabel.Font.Size :=
      Trunc(12 * ADrawObject.AFontScale / 100);
    ADrawObject.AWorkspotTimeRec.ADiffValueLabel.Top :=
      TopLine +
      ADrawObject.ABorder.Top + ADrawObject.ABorder.Height +
      Trunc(ATop * ADrawObject.AWorkspotScale / 100);
    ADrawObject.AWorkspotTimeRec.ADiffValueLabel.Left := ValueLeft +
      (Sender as TImage).Left + Trunc(ALeft * ADrawObject.AWorkspotScale / 100);
    ADrawObject.AWorkspotTimeRec.ADiffValueLabel.Width := ValueWidth;
  end;
  procedure ShowEmployeeDisplayList(AVisible: Boolean;
    ATop, ALeft, AWidth, AHeight: Integer);
  const
    SpaceFactorV = 25;
    SpaceFactorH = 35;
    TopStartFactor = 5;
  var
    I, Top, Left: Integer;
    AEmployeeDisplayRec: PTEmployeeDisplayRec;
    FontSize: Integer;
    TopAdd: Integer;
  begin
    // PIM-12.3
    if not Assigned(ADrawObject.AEmployeeDisplayList) then
      Exit;
    // PIM-12.3 Added try-except and try-finally
    try
      try
        TopAdd :=
          Round(ADrawObject.AWorkspotTimeRec.AInButton.Height) +
            Round(ADrawObject.AWorkspotTimeRec.AInButton.Height / SpaceFactorV);
        Top := ADrawObject.ABorder.Top + ADrawObject.ABorder.Height + 2;
        Left := ADrawObject.ABorder.Left;
        FontSize := 10;
        for I := 0 to ADrawObject.AEmployeeDisplayList.Count - 1 do
        begin
          AEmployeeDisplayRec := ADrawObject.AEmployeeDisplayList.Items[I];
          AEmployeeDisplayRec.AEmployeeNameLabel.Top := Top +
            Round(FontSize / TopStartFactor);
          AEmployeeDisplayRec.AEmployeeNameLabel.Left := Left  +
            Round(ADrawObject.ABorder.Width * 1/30);
          // PIM-223
          AEmployeeDisplayRec.AEmployeeNameLabel.Visible :=
            ORASystemDM.PSShowEmpInfo;
          AEmployeeDisplayRec.AWorkedMinutesLabel.Visible :=
            ORASystemDM.PSShowEmpInfo;

          // 20015178 Do not show name of employee, because it is a fake employee
          if ORASystemDM.EnableMachineTimeRec then
            if Assigned(ADrawObject.AWorkspotTimeRec) then
              if ADrawObject.AWorkspotTimeRec.ATimeRecByMachine then
              begin
                AEmployeeDisplayRec.AEmployeeNameLabel.Visible := False;
                AEmployeeDisplayRec.AWorkedMinutesLabel.Visible := False;
              end;
          if ORASystemDM.PSShowEmpInfo and ORASystemDM.PSShowEmpEff then // PIM-223
          begin
            // Percentage
            AEmployeeDisplayRec.APercentagePanel.Top := Top;
            AEmployeeDisplayRec.APercentagePanel.Left := Left +
              Round(ADrawObject.ABorder.Width - AEmployeeDisplayRec.APercentagePanel.Width);
            AEmployeeDisplayRec.APercentagePanel.Visible := AVisible;
            // ListButton
            if not AEmployeeDisplayRec.AListButton.Init then
            begin
              AEmployeeDisplayRec.AListButton.Glyph.Assign(BitBtnList.Glyph);
              AEmployeeDisplayRec.AListButton.Init := True;
            end;
            AEmployeeDisplayRec.AListButton.Font.Size :=
              AEmployeeDisplayRec.AEmployeeNameLabel.Font.Size;
            if (ADrawObject.AProdScreenType <> pstWSEmpEff) then
            begin
              AEmployeeDisplayRec.AListButton.Top := Top;
              AEmployeeDisplayRec.AListButton.Left :=
                ADrawObject.AWorkspotTimeRec.AInButton.Left;
            end;
            AEmployeeDisplayRec.AListButton.Visible := AVisible;
            // GraphButton
            if not AEmployeeDisplayRec.AGraphButton.Init then
            begin
              AEmployeeDisplayRec.AGraphButton.Glyph.Assign(BitBtnGraph.Glyph);
              AEmployeeDisplayRec.AGraphButton.Init := True;
            end;
            if (ADrawObject.AProdScreenType <> pstWSEmpEff) then
            begin
              AEmployeeDisplayRec.AGraphButton.Top := Top;
              AEmployeeDisplayRec.AGraphButton.Left :=
                ADrawObject.AWorkspotTimeRec.AInButton.Left +
                  Round(ADrawObject.AWorkspotTimeRec.AInButton.Width / 2) +
                    Round(ADrawObject.AWorkspotTimeRec.AInButton.Width / SpaceFactorH);
            end;
            AEmployeeDisplayRec.AGraphButton.Visible := AVisible;
          end // if PSShowEmpEff
          else
          begin // PIM-223
            AEmployeeDisplayRec.APercentagePanel.Visible := False;
            AEmployeeDisplayRec.AListButton.Visible := False;
            AEmployeeDisplayRec.AGraphButton.Visible := False;
          end;
          // Next employee on next row
          Top := Top + TopAdd;
        end;
      except
        on E:Exception do
          WErrorLog(E.Message);
      end;
    finally
    end;
  end; // ShowEmployeeDisplayList;
  procedure ShowPicture(AVisible: Boolean;
    ATop, ALeft, AWidth, AHeight: Integer);
  var
    Picture: TImage;
  begin
    Picture := (ADrawObject.AObject as TImage);
    Picture.Visible := AVisible;
    Picture.Top := (Sender as TImage).Top + ATop; // Trunc(ATop * ADrawObject.AWorkspotScale / 100);
    Picture.Left := (Sender as TImage).Left + ALeft; // Trunc(ALeft * ADrawObject.AWorkspotScale / 100);
    Picture.Width :=
      Round(ADrawObject.AWidth * ADrawObject.AWorkspotScale / 100); // ATrunc(AWidth * ADrawObject.AWorkspotScale / 100);
    Picture.Height :=
      Round(ADrawObject.AHeight * ADrawObject.AWorkspotScale / 100); // Trunc(AHeight * ADrawObject.AWorkspotScale / 100);
  end;
  // PIM-213
  procedure ShowWSEmpEffEmployees;
  var
    AEmployeeDisplayRec: PTEmployeeDisplayRec;
    I: Integer;
    // PIM-213
    procedure ActionWSEmpEff;
    var
      AEmployeeDisplayRec: PTEmployeeDisplayRec;
      A15MinEff, A30MinEff, A60MinEff, ATodayEff: Integer;
      Qty: Integer;
      I: Integer;
    begin
      if not (ADrawObject.AProdScreenType = pstWSEmpEff) then
        Exit;
      if not Assigned(ADrawObject.ApnlWSEmpEff) then
        Exit;

      if Assigned(ADrawObject.AEmployeeDisplayList) then
        if ADrawObject.AEmployeeDisplayList.Count > 0 then
        begin
          for I := 0 to ADrawObject.AEmployeeDisplayList.Count - 1 do
          begin
            AEmployeeDisplayRec := ADrawObject.AEmployeeDisplayList.Items[I];
            // Is employee still scanned in?
            // Always accept this here:
//            if WorkspotEmpEffDM.FindCurrentEmp(ADrawObject.APlantCode,
//              ADrawObject.AWorkspotCode, AEmployeeDisplayRec.EmployeeNumber) then
            begin
              AEmployeeDisplayRec.AEmployeeNameLabel.Visible := False;
              AEmployeeDisplayRec.AWorkedMinutesLabel.Visible := False;
              AEmployeeDisplayRec.APercentagePanel.Visible := False;
              WorkspotEmpEffDM.WorkspotEmpEffGetValues(ADrawObject.APlantCode,
                ADrawObject.AWorkspotCode, AEmployeeDisplayRec.EmployeeNumber,
                A15MinEff, A30MinEff, A60MinEff, ATodayEff);
//              ATodayEff := Round(ADrawObject.ATotalPercentage);
              A15MinEff := 0;
              if (ADrawObject.Act_time_current > 0) and
                (ADrawObject.ACurrentNumberOfEmployees > 0) and
                (ADrawObject.Act_prodqty_current > 0) then
                A15MinEff := Round((ADrawObject.Theo_time_current / 60) /
                  (ADrawObject.Act_time_current / 60) * 100);
              ADrawObject.ApnlWSEmpEff.ShowEff(
                AEmployeeDisplayRec.EmployeeNumber, A15MinEff, A30MinEff,
                  A60MinEff, ATodayEff);
              // PIM-223
              if ORASystemDM.PSShowEmpInfo then
                ADrawObject.ApnlWSEmpEff.
                  ShowEmpName(AEmployeeDisplayRec.EmployeeNumber)
              else
                ADrawObject.ApnlWSEmpEff.
                  HideEmpName(AEmployeeDisplayRec.EmployeeNumber);
            end;
          end;
        end;
      WorkspotEmpEffDM.WorkspotQtyGetValues(ADrawObject.APlantCode,
        ADrawObject.AWorkspotCode, Qty);
      ADrawObject.ApnlWSEmpEff.ShowSummary(Qty);
    end; // ActionWSEmpEff
    // Check and delete employees that are not scanned in anymore.
    procedure CheckDeleteEmployees;
    var
      I: Integer;
      ApnlEmpEff: TpnlEmpEff;
      function FindEmp(AEmployeeNumber: Integer): Boolean;
      var
        J: Integer;
        AEmployeeDisplayRec: PTEmployeeDisplayRec;
      begin
        Result := False;
        for J := 0 to ADrawObject.AEmployeeDisplayList.Count - 1 do
        begin
          AEmployeeDisplayRec := ADrawObject.AEmployeeDisplayList.Items[J];
          if AEmployeeDisplayRec.EmployeeNumber = AEmployeeNumber then
          begin
            Result := True;
            Break;
          end;
        end;
      end;
    begin
      for I := ADrawObject.ApnlWSEmpEff.EmpList.Count - 1 downto 0 do
      begin
        ApnlEmpEff := ADrawObject.ApnlWSEmpEff.EmpList.Items[I];
        if not FindEmp(ApnlEmpEff.EmpNumber) then
          ADrawObject.ApnlWSEmpEff.DelEmployee(ApnlEmpEff.EmpNumber);
      end;
    end;
  begin
    if tlbtnEdit.Down then
      Exit;
    if ADrawObject.AEmployeeDisplayListHasChanged then
    begin
      if Assigned(ADrawObject.AEmployeeDisplayList) then
      begin
        if ADrawObject.AEmployeeDisplayList.Count > 0 then
        begin
          for I := 0 to ADrawObject.AEmployeeDisplayList.Count - 1 do
          begin
            AEmployeeDisplayRec := ADrawObject.AEmployeeDisplayList.Items[I];
            if not Assigned(ADrawObject.ApnlWSEmpEff.FindEmployee(AEmployeeDisplayRec.EmployeeNumber)) then
              ADrawObject.ApnlWSEmpEff.AddEmployee(
                AEmployeeDisplayRec.EmployeeNumber,
                AEmployeeDisplayRec.AEmployeeNameLabel.Caption,
                ImageClick, ImageMouseDown, ImageMouseMove, ImageMouseUp,
                (ADrawObject.AObject as TImage).Tag,
                AEmployeeDisplayRec.AListButton,
                AEmployeeDisplayRec.AGraphButton)
            else
              ADrawObject.ApnlWSEmpEff.EmployeeAssignButtons(
                AEmployeeDisplayRec.EmployeeNumber,
                AEmployeeDisplayRec.AListButton,
                AEmployeeDisplayRec.AGraphButton
                );
          end;
        end;
      end;
      CheckDeleteEmployees;
      ADrawObject.ApnlWSEmpEff.RefreshEmployees;
    end;
    ActionWSEmpEff;
  end; // ShowWSEmpEffEmployees
  // PIM-213
  procedure ShowWSEmpEff(AVisible: Boolean;
    ATop, ALeft, AWidth, AHeight: Integer);
  begin
    if tlbtnEdit.Down then
      Exit;
    // Defer updates
//    SendMessage(Handle, WM_SETREDRAW, WPARAM(False), 0);
    try
      ADrawObject.ALabel.Caption := '';
      ADrawObject.ALabel.Visible := False;
      (Sender as TImage).Visible := False;
      (ADrawObject.AObject as TImage).Visible := False;
      if Assigned(ADrawObject.ADefaultImage) then
        ADrawObject.ADefaultImage.Visible := False;
      ADrawObject.AGhostImage.Visible := False;
      with ADrawObject.ApnlWSEmpEff do
      begin
        Visible := AVisible;
      end;
      ShowWSEmpEffEmployees;
      ADrawObject.ApnlWSEmpEff.ShowTime;
    finally
      // Make sure updates are re-enabled
//      SendMessage(Handle, WM_SETREDRAW, WPARAM(True), 0);
      // Invalidate;  // Might be required to reflect the changes
    end;
  end; // ShowWSEmpEff
begin
  if Assigned(ADrawObject) then
  begin
    if Sender = nil then
      Exit;
    if not ADrawObject.AVisible then
      Exit;
    CurrentDrawObject := ADrawObject;
    ShowObjectNumber(Sender);

    // 20014450.50 Show a picture
    if (ADrawObject.AType = otPicture) then
    begin
      ShowPicture(True, 0, 0, 0, 0);
      Exit;
    end;

    if (Sender is TImage) then
    begin
      // BIG Efficiency Meters
      if ADrawObject.ABigEffMeters then
      begin
        if not ADrawObject.AGhostCountBlink then // PIM-151
          // Don't show picture
          (Sender as TImage).Visible := False;

        // !!!TESTING!!!
//        (Sender as TImage).Visible := True;
        //
        // Personal Screen - Start
        //
        case ADrawObject.AProdScreenType of
        pstNoTimeRec: // No Time Rec: Workspot
          begin
            ShowBorder(True, -15, -143, 286, 15);
            ShowLabel(True, -15, -300, 0, 0);
            ShowEffTotalsLabel(True, 4, 0, 0, 0);
            ShowEffMeter(False, -5, 0, 0, 8); // RV075.1. 5 -> -5
            ShowProgressBar(False, -5, 0, 0, 8);
            ShowBigEffLabel(True, 0, 10, 0, 0);
          end;
        pstMachineTimeRec: // Machine + Time Rec
          begin
            ShowBorder(False, -10, -143, 286, 15);
{            ShowPanel(True, 65, 270, 545, 25); }
             // RV075.1.
//            ShowJobButton(True, -60,
//              Trunc((Sender as TImage).Width/2), 257, 15);
            ShowJobButton(True, -60, 0, 254, 15);
            ShowLabel(True, -60, -268, 0, 0);
            ShowEffTotalsLabel(False, 4, 0, 0, 0);
            ShowEffMeter(False, -5, 0, 0, 10); // RV075.1. 5 -> -5
            ShowProgressBar(False, -5, 0, 0, 10);
            ShowBigEffLabel(False, 0, 10, 0, 0);
          end;
        pstWorkspotTimeRec: // Workspot + Time Rec
          begin
            ShowBorder(True, -6, -143, 286, 15); // RV075.1. Top: -10 -> -6
            // RV075.1.
//            ShowJobButton(True, -60,
//              Trunc((Sender as TImage).Width/2), 257, 15);
            ShowJobButton(True, -60, 16, 254, 15); // -60, 0, 254, 15
            ShowInButton(True, -7, 180, 90, 15); // RV075.1. Top: -11 -> -7
            ShowModeButton(True, -7, -268, 90, 15); // RV075.1. Top: -11 -> -7
            ShowLabel(True, -60, -268, 0, 0);
            ShowEffTotalsLabel(False, 4, 0, 0, 0);
            ShowEffMeter(False, -1, 0, 0, 10); // RV075.1. Top: -5 -> -1
            ShowProgressBar(True, -1, 0, 0, 10);
            ShowBigEffLabel(True, 0, 10, 0, 0);
            ShowPercentageLabel(True, 4, 0, 0, 0);
            ShowActualTargetLabel(True, 0, -268, 0, 0);
            ShowEmployeeDisplayList(True, 0, 0, 0, 0);
          end;
        end; // ADrawObject.AProdScreenType
        //
        // Personal Screen - End
        //
(*        ShowEmployeeList(ADrawObject); *)
      end // if ADrawObject.ABigEffMeters then
      else
      begin  // No BigEffMeters
        if (ADrawObject.AProdScreenType = pstWSEmpEff) then // PIM-213
        begin
          ShowBorder(False, 0, 0, 0, 0);
          ShowLabel(False, 0, 0, 0, 0);
          ShowEffTotalsLabel(False, 0, 0, 0, 0);
          ShowEffMeter(False, 0, 0, 0, 8);
          ShowProgressBar(False, 0, 0, 0, 8);
          ShowBigEffLabel(False, 0, 0, 0, 0);
          ShowWSEmpEff(True, 0, 0, 0, 0);
          ShowJobButton(True, 0, 0, 0, 0);
          ShowInButton(True, 0, 0, 0, 0);
        end
        else
        begin // else if pstWSEmpEff
// PersonalScreen
(*
        // Light Red
        ADrawObject.ALightRed.Top := (Sender as TImage).Top +
          (Sender as TImage).Height +
          Trunc(2 * WorkspotScale / 100);
        ADrawObject.ALightRed.Left := (Sender as TImage).Left;
        ADrawObject.ALightRed.Visible := True;

        // Light Yellow
        ADrawObject.ALightYellow.Top := (Sender as TImage).Top +
          (Sender as TImage).Height +
          Trunc(2 * WorkspotScale / 100);
        ADrawObject.ALightYellow.Left := (Sender as TImage).Left +
          Trunc((Sender as TImage).Width / 2 -  ADrawObject.ALightYellow.Width
          / 2);
        ADrawObject.ALightYellow.Visible := True;

        // Light Green
        ADrawObject.ALightGreen.Top := (Sender as TImage).Top +
          (Sender as TImage).Height +
          Trunc(2 * WorkspotScale / 100);
        ADrawObject.ALightGreen.Left := (Sender as TImage).Left +
          Trunc((Sender as TImage).Width - ADrawObject.ALightGreen.Width);
        ADrawObject.ALightGreen.Visible := True;
*)
          // Efficiency Meter
          // 20015406
          ADrawObject.AEffMeterGreen.Top := (Sender as TImage).Top -
            Trunc(11 * ADrawObject.AWorkspotScale / 100);
          ADrawObject.AEffMeterGreen.Left := (Sender as TImage).Left +
            (Sender as TImage).Width DIV 2;
          ADrawObject.AEffMeterRed.Top := (Sender as TImage).Top -
            Trunc(11 * ADrawObject.AWorkspotScale / 100);
          ADrawObject.AEffMeterRed.Left := (Sender as TImage).Left +
            (Sender as TImage).Width DIV 2 - Trunc(ADrawObject.AEffPercentage *
            ADrawObject.AWorkspotScale / 100);

          // Border
          // 20015406
          ADrawObject.ABorder.Visible := True;
          ADrawObject.ABorder.Top := (Sender as TImage).Top -
            Trunc(16 * ADrawObject.AWorkspotScale / 100);
          ADrawObject.ABorder.Left := (Sender as TImage).Left -
            Trunc(16 * ADrawObject.AWorkspotScale / 100);
          ADrawObject.ABorder.Width := (Sender as TImage).Width +
            Trunc(32 * ADrawObject.AWorkspotScale / 100);
          ADrawObject.ABorder.Height := (Sender as TImage).Height +
            Trunc(38 * ADrawObject.AWorkspotScale / 100);

          // Label
          ADrawObject.ALabel.Visible := True;
          // Scale Font of Label, but it cannot get too small!
          // 20015406
          case ADrawObject.AWorkspotScale of
          0..39:    ADrawObject.ALabel.Font.Size := pnlDraw.Font.Size - 3;
          40..59:   ADrawObject.ALabel.Font.Size := pnlDraw.Font.Size - 2;
          60..99:   ADrawObject.ALabel.Font.Size := pnlDraw.Font.Size;
          100..199: ADrawObject.ALabel.Font.Size := pnlDraw.Font.Size + 1;
          200..299: ADrawObject.ALabel.Font.Size := pnlDraw.Font.Size + 2;
          else
            ADrawObject.ALabel.Font.Size := pnlDraw.Font.Size + 3;
          end;

          // 20015406
          ADrawObject.ALabel.Left := ADrawObject.ABorder.Left;
          ADrawObject.ALabel.Top := ADrawObject.ABorder.Top -
            Trunc(ADrawObject.ALabel.Height +
              ADrawObject.ALabel.Height / 2 * ADrawObject.AWorkspotScale / 100);

        // PersonalScreen
(*        ShowPuppets(ADrawObject, Sender); *)
        end; // else if not pstWSEmpEff
      end; // No BigEffMeters
    end // if (Sender is TImage) then
    else
    begin
      if (Sender is TShapeEx) then
      begin
        // PersonalScreen
(*
        ShowRectangle(ADrawObject);
        if ADrawObject.AType = otPuppetBox then
          ShowPuppets(ADrawObject, Sender);
*)
      end;
    end;
  end;
end; // ShowDrawObject

// Set SelectionRectangle to Current Object-Position
procedure THomeF.ShowSelectionRectangle(Sender: TObject);
  procedure ShowSelRect(AVisible: Boolean;
    ATop, ALeft, AWidth, AHeight: Integer);
  begin
    if not tlbtnEdit.Down then // 20016447 Only show in Edit-mode
      AVisible := False;
    // 20015406
    SelectionRectangle.Visible := AVisible;
    if CurrentDrawObject.AType = otPicture then // 20014450.50
    begin
      SelectionRectangle.Top := (Sender as TImage).Top - ATop;
      SelectionRectangle.Left := (Sender as TImage).Left - ALeft;
      SelectionRectangle.Width := Round(AWidth + CurrentDrawObject.AWidth *
        CurrentDrawObject.AWorkspotScale / 100);
      SelectionRectangle.Height := Round(AHeight + CurrentDrawObject.AHeight *
        CurrentDrawObject.AWorkspotScale / 100);
    end
    else
    begin
      // PIM-213
      if (CurrentDrawObject.AProdScreenType = pstWSEmpEff) then
      begin
        if Assigned(CurrentDrawObject.ApnlWSEmpEff) then
        begin
          SelectionRectangle.Top := CurrentDrawObject.ApnlWSEmpEff.Top - 1;
          SelectionRectangle.Left := CurrentDrawObject.ApnlWSEmpEff.Left - 1;
          SelectionRectangle.Width := CurrentDrawObject.ApnlWSEmpEff.Width + 2;
          SelectionRectangle.Height := CurrentDrawObject.ApnlWSEmpEff.Height + 2;
        end;
      end
      else
      begin
        SelectionRectangle.Top := (Sender as TImage).Top -
          Trunc(ATop * CurrentDrawObject.AWorkspotScale / 100);
        SelectionRectangle.Left := (Sender as TImage).Left -
          Trunc(ALeft * CurrentDrawObject.AWorkspotScale / 100);
        SelectionRectangle.Width := (Sender as TImage).Width +
         Trunc(AWidth * CurrentDrawObject.AWorkspotScale / 100);
         SelectionRectangle.Height := (Sender as TImage).Height +
          Trunc(AHeight * CurrentDrawObject.AWorkspotScale / 100);
      end;
    end;
  end;
begin
  if Sender = nil then
    Exit;
  ShowObjectNumber(Sender);
  if (Sender is TImage) then
  begin
    CurrentDrawObject := ThisDrawObject((Sender as TImage).Tag);
    if CurrentDrawObject <> nil then
    begin
      if not CurrentDrawObject.AVisible then
        Exit;
      if CurrentDrawObject.AType = otPicture then // 20014450.50
      begin
        ShowSelRect(True, 2, 2, 4, 4);
      end
      else
        if CurrentDrawObject.ABigEffMeters then
        begin
          //
          // Personal Screen - Start
          //
          case CurrentDrawObject.AProdScreenType of
          pstNoTimeRec: // No Time Rec: Workspot
            begin
              ShowSelRect(True, 17, 145, 291, 20);
            end;
          pstMachineTimeRec: // Machine + Time Rec
            begin
              ShowSelRect(True, 65, 270, 546, 25); // 65,270,545,25
            end;
          pstWorkspotTimeRec: // Workspot + Time Rec
            begin
              ShowSelRect(True, 65, 270, 546, 25); // 65,270,545,25
            end;
          end;
          //
          // Personal Screen - End
          //
        end
        else
        begin
          ShowSelRect(True, 19, 19, 39, 45);
        end;
    end;
  end
  else
    if (Sender is TShapeEx) then
    begin
      SelectionRectangle.Visible := True;
      // Note: Line-height
      SelectionRectangle.Top := (Sender as TShapeEx).Top - 3;
      SelectionRectangle.Left := (Sender as TShapeEx).Left - 3;
      SelectionRectangle.Width := (Sender as TShapeEx).Width + 6;
      SelectionRectangle.Height := (Sender as TShapeEx).Height + 6;
      CurrentDrawObject := ThisDrawObject((Sender as TShapeEx).Tag);
      if CurrentDrawObject <> nil then
      begin
// PersonalScreen
(*
        if CurrentDrawObject.AType = otLineHorz then
        begin
          SelectionRectangle.Top := SelectionRectangle.Top +
            SelectionRectangle.Height DIV 2 - 4;
          SelectionRectangle.Height := 9;
        end
        else
          if CurrentDrawObject.AType = otLineVert then
          begin
            SelectionRectangle.Left := SelectionRectangle.Left +
              SelectionRectangle.Width DIV 2 - 4;
            SelectionRectangle.Width := 9;
          end
          else
            if (CurrentDrawObject.AType = otRectangle) or
              (CurrentDrawObject.AType = otPuppetBox) then
            begin
              SelectionRectangle.Top :=
                CurrentDrawObject.ADeptRect.ATopLineHorz.Top - 2;
              SelectionRectangle.Left :=
                CurrentDrawObject.ADeptRect.ATopLineHorz.Left - 2;
              SelectionRectangle.Width :=
                CurrentDrawObject.ADeptRect.ATopLineHorz.Width + 5;
              SelectionRectangle.Height :=
                CurrentDrawObject.ADeptRect.ARightLineVert.Height + 5;
            end;
*)            
      end;
    end
    else
      SelectionRectangle.Visible := False;
end; // ShowSelectionRectangle

procedure THomeF.ShowFirstObject;
var
  ADrawObject: PDrawObject;
begin
  if PSList.Count > 0 then
  begin
    ADrawObject := PSList.Items[0];
    ShowSelectionRectangle(ADrawObject.AObject);
  end;
end; // ShowFirstObject

procedure THomeF.ShowObjectProperties(X: Integer; Y: Integer);
var
  I: Integer;
  ADrawObject: PDrawObject;
begin
  if (PSList = nil) then
    Exit;
  if tlbtnEdit.Down then // 20014450.50 Not in Edit-mode!
    Exit;
  if not TimerMain.Enabled then
    Exit; 

  try
    stBarBase.Panels[1].Text := 'Objects=' + IntToStr(PSList.Count);
    for I:=0 to PSList.Count-1 do
    begin
      ADrawObject := PSList.Items[I];
      if Assigned(ADrawObject) then
        if ADrawObject.AType = otImage then
        begin
          if ((ADrawObject.AObject as TImage).Top >= Y) and
             (((ADrawObject.AObject as TImage).Top <= Y +
              (ADrawObject.AObject as TImage).Height)) and
             ((ADrawObject.AObject as TImage).Left >= X) and
             (((ADrawObject.AObject as TImage).Left <= X +
              (ADrawObject.AObject as TImage).Width)) then
              stBarBase.Panels[0].Text := ADrawObject.AImageName;
        end;
    end;
  except
  end;
end; // ShowObjectProperties

(*
procedure THomeF.ShowRectangle(ADrawObject: PDrawObject);
var
  ATop, ALeft, AWidth, AHeight: Integer;
  ALineWidth: Integer;
begin
  if ADrawObject = nil then
    Exit;
  if (ADrawObject.AObject is TShapeEx) then
  begin
    if (ADrawObject.AType = otRectangle) or
      (ADrawObject.AType = otPuppetBox) then
    begin
      ATop := (ADrawObject.AObject as TShapeEx).Top;
      ALeft := (ADrawObject.AObject as TShapeEx).Left;
      AHeight := Trunc((ADrawObject.AObject as TShapeEx).Height *
        WorkspotScale / 100);
      AWidth := Trunc((ADrawObject.AObject as TShapeEx).Width *
        WorkspotScale / 100);

      ALineWidth := ADrawObject.ADeptRect.ATopLineHorz.Pen.Width;

      with ADrawObject.ADeptRect.ATopLineHorz do
      begin
        Top := ATop;
        Left := ALeft;
        Height := ALineWidth;
        Width := AWidth;
      end;

      with ADrawObject.ADeptRect.ARightLineVert do
      begin
        Top := ATop;
        Left := ALeft + AWidth - ADrawObject.ADeptRect.ARightLineVert.Pen.Width;
        Height := AHeight;
        Width := ALineWidth;
      end;

      with ADrawObject.ADeptRect.ABottomLineHorz do
      begin
        Top := ATop + AHeight - ADrawObject.ADeptRect.ABottomLineHorz.Pen.Width;
        Left := ALeft;
        Height := ALineWidth;
        Width := AWidth;
      end;

      with ADrawObject.ADeptRect.ALeftLineVert do
      begin
        Top := ATop;
        Left := ALeft;
        Height := AHeight;
        Width := ALineWidth;
      end;
      with ADrawObject.ALabel do
      begin
        Left := ALeft;
        Top := ATop - Height - Trunc(Font.Size / 4 * WorkspotScale / 100);
//        ADrawObject.ALabel.Width := (Sender as TShapeEx).Width *
//        Trunc(WorkspotScale / 100);
        Visible := True;
      end;

      if ADrawObject.ABigEffMeters then
        with ADrawObject.ABigEffLabel do
        begin
          Left := ALeft;
          Top := ATop - Height - Trunc(Font.Size / 4 * WorkspotScale / 100);
          Visible := True;
        end;
    end;
  end;
end; // ShowRectangle
*)

{ Show - End }

{ Create - Start }

// Create Workspot Image
function THomeF.CreateImage(const AImageName: String;
  const ABigEffMeters: Boolean): TImage;
var
  AnImage: TImage;
begin
  AnImage := TImage.Create(Application);
  AnImage.Parent := pnlDraw;
  if ABigEffMeters then
  begin
    AnImage.Left := 250;
    AnImage.Top := 50;
  end
  else
  begin
    AnImage.Left := 50;
    AnImage.Top := 50;
  end;
  AnImage.Canvas.Brush.Style := bsClear;
  AnImage.Picture.LoadFromFile(ImagePath + AImageName);
//  AnImage.Width := AnImage.Picture.Width;
//  AnImage.Height := AnImage.Picture.Height;
  if ABigEffMeters then
  begin
    AnImage.Width := Trunc(IMAGE_SIZE * WorkspotScale / 100 / 2);
    AnImage.Height := Trunc(IMAGE_SIZE * WorkspotScale / 100 / 2);
  end
  else
  begin
    AnImage.Width := Trunc(IMAGE_SIZE * WorkspotScale / 100);
    AnImage.Height := Trunc(IMAGE_SIZE * WorkspotScale / 100);
  end;
  AnImage.Stretch := True;
  AnImage.Tag := NewItemIndex;
  AnImage.OnClick := ImageClick;
  AnImage.OnDblClick := ActionShowChangeProperties;
  AnImage.PopupMenu := PopupWorkspotMenu;
  AnImage.OnMouseDown := ImageMouseDown;
  AnImage.OnMouseMove := ImageMouseMove;
  AnImage.OnMouseUp := ImageMouseUp;
  AnImage.Visible := True;
  Result := AnImage;
end; // CreateImage

// Create Vertical or Horizontal Line
function THomeF.CreateShapeEx(AShapeExType: TShapeExType): TShapeEx;
var
  MyShapeEx: TShapeEx;
begin
  MyShapeEx := TShapeEx.Create(Application);
  MyShapeEx.Pen.Width := 2;
  MyShapeEx.Shape := AShapeExType;
  MyShapeEx.Brush.Style := bsClear;
  MyShapeEx.Top := 50;
  MyShapeEx.Left := 50;
  if AShapeExType = TShapeExType(RECTANGLE) then
  begin
    MyShapeEx.Height := Trunc(200 * WorkspotScale / 100);
    MyShapeEx.Width := Trunc(400 * WorkspotScale / 100);
  end
  else
  begin
    if AShapeExType = stLineVert then
    begin
      MyShapeEx.Height := Trunc(200 * WorkspotScale / 100);
      MyShapeEx.Width := 9;
    end
    else
      if AShapeExType = stLineHorz then
      begin
        MyShapeEx.Height := 9;
        MyShapeEx.Width := Trunc(400 * WorkspotScale / 100);
      end;
  end;
  MyShapeEx.Tag := NewItemIndex;
  MyShapeEx.Parent := pnlDraw;
  MyShapeEx.OnClick := ImageClick;
  MyShapeEx.OnDblClick := ActionShowChangeProperties;
  MyShapeEx.OnMouseDown := ImageMouseDown;
  MyShapeEx.OnMouseMove := ImageMouseMove;
  MyShapeEx.OnMouseUp := ImageMouseUp;
  if AShapeExType = TShapeExType(RECTANGLE) then
    MyShapeEx.Visible := False;
  Result := MyShapeEx;
end; // CreateShapeEx

// PersonalScreen
(*
// Create Rectangle that consist of 4 lines in a record
function THomeF.CreateRectangle(ADrawObject: PDrawObject): PTARectangle;
var
  MyRect: PTARectangle;
begin
  MyRect := new(PTARectangle);
  MyRect.ATopLineHorz := CreateShapeEx(stLineHorz);
  MyRect.ARightLineVert := CreateShapeEx(stLineVert);
  MyRect.ABottomLineHorz := CreateShapeEx(stLineHorz);
  MyRect.ALeftLineVert := CreateShapeEx(stLineVert);
  ADrawObject.ABigEffLabel.Caption := '';
  if ADrawObject.AType = otPuppetBox then
  begin
    ADrawObject.ALabel.Caption := ADrawObject.AWorkspotCode;
    ADrawObject.ALabel.Font.Size := 8
  end
  else
  begin
    // NOTE: For the Rectangle, the WorkspotCode stands for the
    // DepartmentCode!
    ADrawObject.ALabel.Caption :=
      DetermineDepartmentDescription(ADrawObject.APlantCode,
        ADrawObject.AWorkspotCode);
    ADrawObject.ALabel.Font.Size := 15;
  end;
  ADrawObject.ALabel.Visible := True;
  Result := MyRect;
end; // CreateRectangle
*)

// PersonalScreen
(*
// Search the shift in APupperShiftList by ShiftNumber
// If not found, create it.
// Result is Index of shift.
function THomeF.SearchCreateShift(var ADrawObject: PDrawObject;
  const AShiftNumber: Integer): Integer;
var
  ShiftIndex: Integer;
  CreateNew: Boolean;
  APuppetShift: PTPuppetShift;
begin
  CreateNew := True;
  Result := 0;
  if not Assigned(ADrawObject.APuppetShiftList) then
    Exit;
  for ShiftIndex := 0 to ADrawObject.APuppetShiftList.Count - 1 do
  begin
    APuppetShift := ADrawObject.APuppetShiftList.Items[ShiftIndex];
    if APuppetShift.AShiftNumber = AShiftNumber then
    begin
      Result := ShiftIndex;
      CreateNew := False;
      Break;
    end;
  end;
  if CreateNew then
  begin
    ADrawObject.APuppetShiftList.Add(New(PTPuppetShift));
    ShiftIndex := ADrawObject.APuppetShiftList.Count;
    APuppetShift := ADrawObject.APuppetShiftList.Items[ShiftIndex-1];
    APuppetShift.AShiftNumber := AShiftNumber;
    APuppetShift.APuppetList := TList.Create;
    Result := ShiftIndex - 1;
  end;
end; // SearchCreateShift

procedure THomeF.ActionCreatePuppets(ADrawObject: PDrawObject;
  var AEmployeeList: TList);
var
  I, J: Integer;
  APTEmployee, APTEmployee2: PTEmployee;
  APuppetShift: PTPuppetShift;
  ShiftIndex: Integer;
begin
  // Now create the puppet-list

  // First Delete the Puppets
  DeletePuppetShiftList(ADrawObject.APuppetShiftList);
  ADrawObject.APuppetShiftList := TList.Create;

  // Now decide which employees have to be kept or left out.
  // This depends on the color of an employee,
  // if color is Yellow or Orange, a planned employee
  // should be left out.
  if AEmployeeList.Count > 0 then
    AEmployeeList.Sort(EmployeeCompare);
  for I := 0 to AEmployeeList.Count - 1 do
  begin
    APTEmployee := AEmployeeList.Items[I];
    if (APTEmployee.AColor = clMyYellow) or
      (APTEmployee.AColor = clMyOrange) then
    begin
      for J := 0 to AEmployeeList.Count - 1 do
      begin
        APTEmployee2 := AEmployeeList.Items[J];
        if (APTEmployee2.AKeep) and
          (APTEmployee.AShiftNumber = APTEmployee2.AShiftNumber) then
        begin
          if APTEmployee2.AColor = clGray then
          begin
            APTEmployee2.AKeep := False;
            Break;
          end;
        end;
      end;
    end;
  end;
  // Now create the puppetlist
  for I := 0 to AEmployeeList.Count - 1 do
  begin
    APTEmployee := AEmployeeList.Items[I];
    if APTEmployee.AKeep then
    begin
      ShiftIndex := SearchCreateShift(ADrawObject, APTEmployee.AShiftNumber);
      APuppetShift := ADrawObject.APuppetShiftList.Items[ShiftIndex];
      if APuppetShift <> nil then
        APuppetShift.APuppetList.Add(
          NewPuppet(pnlDraw, APTEmployee));
    end;
  end;
end; // ActionCreatePuppets
*)

{ Create - End }

{ Edit - Start }

// Action: Add Workspot-object
procedure THomeF.actWorkspotExecute(Sender: TObject);
begin
  inherited;
  DialogWorkspotSelectF.ActionWorkspot := False; // PIM-213
  DialogWorkspotSelectF.NoPicture := False; // PIM-213
  if DialogWorkspotSelectF.ShowModal = mrOK then
    AddObjectToList(otImage, nil,
      DialogWorkspotSelectF.PlantCode,
      DialogWorkspotSelectF.WorkspotCode,
      DialogWorkspotSelectF.WorkspotDescription,
      DialogWorkspotSelectF.ImageName,
      False, 0,
      pstNoTimeRec, mtNone, '', '', 0);
end; // actWorkspotExecute

// Action: Add Efficiencymeter-object
// Shows dialog where workspots can be selected.
// For Personal Screen: With option to choose for 3 types:
// - Workspots - No Time Rec
// - Machines - With Time Rec
// - Workspots - With Time Rec
procedure THomeF.actEffiMeterExecute(Sender: TObject);
var
  WorkspotCounter: Integer;
begin
  inherited;
{$IFDEF PERSONALSCREEN}
  DialogWorkspotSelectF.ActionWorkspot := False;
{$ELSE}
  DialogWorkspotSelectF.ActionWorkspot := True;
{$ENDIF}
  DialogWorkspotSelectF.NoPicture := True; // PIM-213
  if DialogWorkspotSelectF.ShowModal = mrOK then
  begin
    // Personal Screen
    // If ProdScreenType = pstMachineTimeRec then
    // add all workspots linked to the selected machine.
    case DialogWorkspotSelectF.ProdScreenType of
    pstMachineTimeRec:
      begin
        // First add an object that represents the machine.
        AddObjectToList(otImage, nil,
          DialogWorkspotSelectF.PlantCode,
          DialogWorkspotSelectF.WorkspotCode,
          DialogWorkspotSelectF.WorkspotDescription,
          DialogWorkspotSelectF.ImageName,
          True, 0,
          pstMachineTimeRec,
          mtMachine,
          DialogWorkspotSelectF.MachineCode,
          DialogWorkspotSelectF.MachineDescription,
          0);
        // Then add all objects for workspots linked to the selected machine.
        with ProductionScreenDM.oqWorkspotByMachine do
        begin
          ClearVariables;
          SetVariable('PLANT_CODE',   DialogWorkspotSelectF.PlantCode);
          SetVariable('MACHINE_CODE', DialogWorkspotSelectF.MachineCode);
          Execute;
          WorkspotCounter := 1;
          while not Eof do
          begin
            AddObjectToList(otImage, nil,
              DialogWorkspotSelectF.PlantCode,
              FieldAsString('WORKSPOT_CODE'),
              FieldAsString('DESCRIPTION'),
              DialogWorkspotSelectF.ImageName,
              True, 0,
              pstWorkspotTimeRec,
              mtWorkspot,
              DialogWorkspotSelectF.MachineCode,
              DialogWorkspotSelectF.MachineDescription,
              WorkspotCounter);
            inc(WorkspotCounter);
            Next;
          end;
        end;
      end;
    pstWorkspotTimeRec:
      begin
        // Add object for Workspot + Time Rec
        AddObjectToList(otImage, nil,
          DialogWorkspotSelectF.PlantCode,
          DialogWorkspotSelectF.WorkspotCode,
          DialogWorkspotSelectF.WorkspotDescription,
          DialogWorkspotSelectF.ImageName,
          True, 0,
          pstWorkspotTimeRec,
          mtWorkspot,
          DialogWorkspotSelectF.MachineCode,
          DialogWorkspotSelectF.MachineDescription,
          0);
      end;
    else
      // Add object for Workspot - No Time Rec
      AddObjectToList(otImage, nil,
        DialogWorkspotSelectF.PlantCode,
        DialogWorkspotSelectF.WorkspotCode,
        DialogWorkspotSelectF.WorkspotDescription,
        DialogWorkspotSelectF.ImageName,
        True, 0,
        pstNoTimeRec,
        mtWorkspot,
        DialogWorkspotSelectF.MachineCode,
        DialogWorkspotSelectF.MachineDescription,
        0);
    end; // case
  end;
end; // actEffiMeterExecute

// Action: Add Horizontalline-object
procedure THomeF.actLineHorzExecute(Sender: TObject);
begin
  inherited;
// PersonalScreen
(*
  AddObjectToList(otLineHorz, CreateShapeEx(stLineHorz),
    '', '', '', 'Line Horz', False, 0,
    pstNoTimeRec, mtNone, '', '', 0);
*)
end; // actLineHorzExecute

// Action: Add Verticalline-object
procedure THomeF.actLineVertExecute(Sender: TObject);
begin
  inherited;
// PersonalScreen
(*
  AddObjectToList(otLineVert, CreateShapeEx(stLineVert),
    '', '', '', 'Line Vert', False, 0,
    pstNoTimeRec, mtNone, '', '', 0);
*)
end;

// Action: Add Rectangle-object
procedure THomeF.actRectangleExecute(Sender: TObject);
// PersonalScreen
(*
var
  MyPlantCode: String;
  MyDepartmentCode: String;
  MyDepartmentDescription: String;
*)
begin
  inherited;
// PersonalScreen
(*
  MyPlantCode := '';
  MyDepartmentCode := '';
  MyDepartmentDescription := 'Department';
  if DialogDepartmentSelectF.ShowModal = mrOK then
  begin
    MyPlantCode := DialogDepartmentSelectF.PlantCode;
    MyDepartmentCode := DialogDepartmentSelectF.DepartmentCode;
    MyDepartmentDescription := DialogDepartmentSelectF.DepartmentDescription;
    AddObjectToList(otRectangle, CreateShapeEx(TShapeExType(RECTANGLE)),
      MyPlantCode, MyDepartmentCode, MyDepartmentDescription, 'Rectangle',
      False, 0,
      pstNoTimeRec, mtNone, '', '', 0);
  end;
*)
end;

// Action: Add Puppetbox-object
procedure THomeF.actPuppetBoxExecute(Sender: TObject);
begin
  inherited;
// PersonalScreen
(*
  DialogSelectModeF := TDialogSelectModeF.Create(Application);
  if DialogSelectModeF.ShowModal = mrOK then
  begin
    AddObjectToList(otPuppetBox, CreateShapeEx(TShapeExType(RECTANGLE)),
      '', DialogSelectModeF.ModeString, '',
      'Puppet Box', False, DialogSelectModeF.SelectedMode,
      pstNoTimeRec, mtNone, '', '', 0);
  end;
  DialogSelectModeF.Free;
*)
end; // actPuppetBoxExecute

// Action: Delete an object from scheme shown in screen
procedure THomeF.actDeleteExecute(Sender: TObject);
  // Personal Screen:
  // Delete Machine and all related workspots.
  function DeleteMachineWorkspots: Boolean;
  var
    I: Integer;
    ADrawObject: PDrawObject;
  begin
    Result := False;
    if (CurrentDrawObject.AObject is TImage) then
      if CurrentDrawObject.AProdScreenType = pstMachineTimeRec then
      begin
        Result := True;
        if DisplayMessage(SPimsDeleteMachineWorkspots, mtConfirmation,
          [mbYes, mbNo]) = mrYes then
        begin
          for I := PSList.Count - 1  downto 0 do
          begin
            ADrawObject := PSList.Items[I];
            if (ADrawObject.AObject is TImage) then
            begin
              if ADrawObject.AProdScreenType = pstWorkspotTimeRec then
              begin
                if (ADrawObject.APlantCode = CurrentDrawObject.APlantCode) and
                  (ADrawObject.AWorkspotTimeRec.AMachineCode =
                    CurrentDrawObject.AMachineTimeRec.AMachineCode) then
                begin
                  DeleteDrawObjectFromList(
                    (ADrawObject.AObject as TImage).Tag);
                end;
              end;
            end;
          end; // for
          DeleteDrawObjectFromList((CurrentDrawObject.AObject as TImage).Tag);
          CurrentDrawObject := nil;
        end; // if DisplayMessage
      end; // if CurrentDrawObject.AProdScreenType = pstMachineTimeRec
  end; // DeleteMachineWorkspots
begin
  inherited;
  if CurrentDrawObject <> nil then
  begin
    if not DeleteMachineWorkspots then // Personal Screen
      if DisplayMessage(tlbtnDelete.Hint + '?', mtConfirmation,
        [mbYes, mbNo]) = mrYes then
      begin
        if (CurrentDrawObject.AType = otPicture) then
        begin
          DeleteDrawObjectFromList((CurrentDrawObject.AObject as TImage).Tag);
          CurrentDrawObject := nil;
        end
        else
          if (CurrentDrawObject.AObject is TImage) then
          begin
            DeleteDrawObjectFromList((CurrentDrawObject.AObject as TImage).Tag);
            CurrentDrawObject := nil;
          end
          else
          begin
            if (CurrentDrawObject.AObject is TShapeEx) then
            begin
              DeleteDrawObjectFromList(
                (CurrentDrawObject.AObject as TShapeEx).Tag);
              CurrentDrawObject := nil;
            end;
        end;
      end; // if DisplayMessage
  end; // if CurrentDrawObject <> nil then
end; // actDeleteExecute

// Action: Save the scheme
procedure THomeF.actSaveExecute(Sender: TObject);
begin
  inherited;
  if not IsBusy then
    MyActionSave
  else
    MyAction := MyActionSave;
end; // actSaveExecute

procedure THomeF.ActionEditOn;
begin
//  WErrorLog('-> ActionEditOn');
  MainTimerSwitch(False);
  try
    ActionWriteToFile;
    ActionWriteToDB;
  except
  end;
  // TD-25118
  if tlbtnEdit.Enabled then
    tlbtnEdit.Down := True;
  if tlbtnWorkspot.Enabled then
    tlbtnWorkspot.Visible    := True;
  tlbtnEffMeter.Visible    := True;
  if tlbtnLineHorz.Enabled then
    tlbtnLineHorz.Visible    := True;
  if tlbtnLineVert.Enabled then
    tlbtnLineVert.Visible    := True;
  if tlbtnRectangle.Enabled then
    tlbtnRectangle.Visible   := True;
  if tlbtnPuppetBox.Enabled then
    tlbtnPuppetBox.Visible   := True;
  tlbtnSave.Visible        := True;
  tlbtnDelete.Visible      := True;
  tlbtnPicture.Visible     := True;
  tlbtnWSEmpEff.Visible := True;
  // Show some help!
  stBarBase.Panels[3].Text :=
    'Size: Ctrl / Alt + Left Mousebutton';
end; // ActionEditOn

procedure THomeF.ActionEditOff;
begin
//  WErrorLog('-> ActionEditOff');
  // TD-25118
  if tlbtnEdit.Enabled then
    tlbtnEdit.Down := False;
  tlbtnWorkspot.Visible    := False;
  tlbtnEffMeter.Visible    := False;
  tlbtnLineHorz.Visible    := False;
  tlbtnLineVert.Visible    := False;
  tlbtnRectangle.Visible   := False;
  tlbtnPuppetBox.Visible   := False;
  tlbtnSave.Visible        := False;
  tlbtnDelete.Visible      := False;
  tlbtnPicture.Visible     := False;
  tlbtnWSEmpEff.Visible    := False;
  stBarBase.Panels[3].Text := '';
  MainTimerSwitch(True);
end; // ActionEditOff

// Action: Switch to edit-mode and back
procedure THomeF.actEditExecute(Sender: TObject);
begin
  inherited;
  if tlbtnEdit.Down then
  begin
    GhostImageDisable; // PIM-151
    ActionEditOn
  end
  else
  begin
    ActionEditOff;
    ActionShowAllWorkspotDetails; // 20016447
  end;
  Refresh;
end; // actEditExecute

// Actions for Key-events:
// - F5 = Refresh
// - Other keys used in Edit-mode.
procedure THomeF.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  ATag: Integer;
  OldTop, OldLeft,
  OldWidth, OldHeight: Integer;
begin
  // Key = F5
  if Key = VK_F5 then { Refresh }
//    MainActionForTimer;
    ActionShowAllWorkspotDetails;

  if Key = VK_F10 then { Show Debug Info }
  begin
    actSaveLogExecute(Sender);
{
    DialogDebugInfoF.PSList := PSList;
    DialogDebugInfoF.ShowModal;
    DialogDebugInfoF.ShowDebugInfo;
}
  end;

  if not tlbtnEdit.Down then
    Exit;

  OldTop := 0;
  OldLeft := 0;
  OldWidth := 0;
  OldHeight := 0;

  if CurrentDrawObject <> nil then
  begin
    Sender := CurrentDrawObject.AObject;
    if (Sender is TImage) then
    begin
      OldTop := (Sender as TImage).Top;
      OldLeft := (Sender as TImage).Left;
      OldHeight := (Sender as TImage).Height;
      OldWidth := (Sender as TImage).Width;
    end;
    if (sender is TShapeEx) then
    begin
      OldTop := (Sender as TShapeEx).Top;
      OldLeft := (Sender as TShapeEx).Left;
      OldHeight := (Sender as TShapeEx).Height;
      OldWidth := (Sender as TShapeEx).Width;
    end;
  end;

  // Goto next Object
  // Key = ENTER
  if (Key = 13) then // ENTER pressed
  begin
    if CurrentDrawObject <> nil then
    begin
      ATag := -1;
      if (CurrentDrawObject.AObject is TImage) then
        ATag := (CurrentDrawObject.AObject as TImage).Tag
      else
        if (CurrentDrawObject.AObject is TShapeEx) then
          ATag := (CurrentDrawObject.AObject as TShapeEx).Tag;
      if ATag <> -1 then
      begin
        inc(ATag);
        CurrentDrawObject := ThisDrawObject(ATag);
        if (CurrentDrawObject = nil) then
        begin
          ATag := 1;
          CurrentDrawObject := ThisDrawObject(ATag);
        end;
      end;
    end;
    if CurrentDrawObject <> nil then
      ShowSelectionRectangle(CurrentDrawObject.AObject);
  end
  else
  begin
    // Move Object 1 pixel per step
    // CTRL + UP, DOWN, LEFT, RIGHT
    if (ssCtrl in Shift) then
    begin
      if CurrentDrawObject <> nil then
      begin
        Sender := CurrentDrawObject.AObject;
        case Key of
          VK_UP:
            if (Sender is TImage) then
              (Sender as TImage).Top := (Sender as TImage).Top - 1
            else
              if (Sender is TShapeEx) then
                (Sender as TShapeEx).Top := (Sender as TShapeEx).Top - 1;
          VK_DOWN:
            if (Sender is TImage) then
              (Sender as TImage).Top := (Sender as TImage).Top + 1
            else
              if (Sender is TShapeEx) then
                (Sender as TShapeEx).Top := (Sender as TShapeEx).Top + 1;
          VK_LEFT:
            if (Sender is TImage) then
              (Sender as TImage).Left := (Sender as TImage).Left - 1
            else
              if (Sender is TShapeEx) then
                (Sender as TShapeEx).Left := (Sender as TShapeEx).Left - 1;
          VK_RIGHT:
            if (Sender is TImage) then
              (Sender as TImage).Left := (Sender as TImage).Left + 1
            else
              if (Sender is TShapeEx) then
                (Sender as TShapeEx).Left := (Sender as TShapeEx).Left + 1;
        end;
      end;
      ShowDrawObject(CurrentDrawObject, Sender);
      ShowSelectionRectangle(Sender);
    end;
    // Size Object 1 pixel per step
    // SHIFT + UP, DOWN, LEFT, RIGHT
    if (ssShift in Shift) then
    begin
      if CurrentDrawObject <> nil then
      begin
        Sender := CurrentDrawObject.AObject;
        case Key of
          VK_UP:
            if (Sender is TShapeEx) then
              if ((Sender as TShapeEx).Shape = stLineVert) or
                ((Sender as TShapeEx).Shape = TShapeExType(RECTANGLE)) then
                (Sender as TShapeEx).Height := (Sender as TShapeEx).Height - 1;
          VK_DOWN:
            if (Sender is TShapeEx) then
              if ((Sender as TShapeEx).Shape = stLineVert) or
              ((Sender as TShapeEx).Shape = TShapeExType(RECTANGLE)) then
                (Sender as TShapeEx).Height := (Sender as TShapeEx).Height + 1;
          VK_LEFT:
            if (Sender is TShapeEx) then
              if ((Sender as TShapeEx).Shape = stLineHorz) or
              ((Sender as TShapeEx).Shape = TShapeExType(RECTANGLE)) then
                (Sender as TShapeEx).Width := (Sender as TShapeEx).Width - 1;
          VK_RIGHT:
            if (Sender is TShapeEx) then
              if ((Sender as TShapeEx).Shape = stLineHorz) or
              ((Sender as TShapeEx).Shape = TShapeExType(RECTANGLE)) then
                (Sender as TShapeEx).Width := (Sender as TShapeEx).Width + 1;
        end;
      end;
      ShowDrawObject(CurrentDrawObject, Sender);
      ShowSelectionRectangle(Sender);
    end;
  end;
  // Has something changed?
  if CurrentDrawObject <> nil then
  begin
    Sender := CurrentDrawObject.AObject;
    if not Dirty then
    begin
      if (Sender is TImage) then
        Dirty := (OldTop <> (Sender as TImage).Top) or
          (OldLeft <> (Sender as TImage).Left) or
          (OldHeight <> (Sender as TImage).Height) or
          (OldWidth <> (Sender as TImage).Width);
      if (Sender is TShapeEx) then
         Dirty := (OldTop <> (Sender as TShapeEx).Top) or
          (OldLeft <> (Sender as TShapeEx).Left) or
          (OldHeight <> (Sender as TShapeEx).Height) or
          (OldWidth <> (Sender as TShapeEx).Width);
    end;
  end;
end; // FormKeyDown

procedure THomeF.ImageMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
  MyDrawObject: PDrawObject;
begin
  if not Assigned(Sender) then
    Exit;

  if Button = mbLeft then
    if (Sender is TShape) or
      (Sender is TLabel) or
      (Sender is TPanel) or // PIM-213
      (Sender is TdxfProgressBar) then // PIM-213
      ImageClick(Sender);

  if tlbtnEdit.Down then
    if (Sender is TButton) then
      ImageClick(Sender);

  if not tlbtnEdit.Down then
    if (Sender is TShape) then
      if Button = mbRight then
      begin
        PopupWorkspotMenu.Popup(
          (Sender as TShape).Left + X + 120,
          (Sender as TShape).Top + Y + 120);
      end;

  // 20015406
  if tlbtnEdit.Down then
    if Button = mbRight then
    begin
      ImageClick(Sender);
      if Assigned(CurrentDrawObject) then
      begin
        MyDrawObject := CurrentDrawObject;
        DialogObjectSettingsF.PictureSettings :=
          (MyDrawObject.AType = otPicture);
        DialogObjectSettingsF.WorkspotScale := MyDrawObject.AWorkspotScale;
        DialogObjectSettingsF.FontScale := MyDrawObject.AFontScale;
        DialogObjectSettingsF.EffPeriodMode := Integer(MyDrawObject.AEffPeriodMode);
        if DialogObjectSettingsF.ShowModal = mrOK then
        begin
          MyDrawObject.AWorkspotScale := DialogObjectSettingsF.WorkspotScale;
          MyDrawObject.AFontScale := DialogObjectSettingsF.FontScale;
          MyDrawObject.AEffPeriodMode := TEffPeriodMode(DialogObjectSettingsF.EffPeriodMode);
          Dirty := True;
          ShowDrawObject(MyDrawObject, MyDrawObject.AObject);
          ShowSelectionRectangle(MyDrawObject.AObject);
          if MyDrawObject.AType <> otPicture then
            ChangeModeByDrawObject(MyDrawObject);
        end;
      end;
    end;

  if tlbtnEdit.Down then
    if Button = mbLeft then
    begin
      Dirty := True;
      FDragOfX := X;
      FDragOfY := Y;
      FDragging := True;
    end;
end;  // ImageMouseDown

procedure THomeF.ImageMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
begin
  if not tlbtnEdit.Down then
    Exit;
  if Sender = nil then
    Exit;
  if FDragging then
  begin
    if (Sender is TShape) then
      CurrentDrawObject := ThisDrawObject((Sender as TShape).Tag)
    else
      if (Sender is TLabel) then
        CurrentDrawObject := ThisDrawObject((Sender as TLabel).Tag)
      else
        if (Sender is TImage) then
        begin
          CurrentDrawObject := ThisDrawObject((Sender as TImage).Tag);
          // PIM-151
          if Assigned(CurrentDrawObject.ADefaultImage) then
            CurrentDrawObject.AObject := CurrentDrawObject.ADefaultImage;
        end
        else
          if (Sender is TShapeEx) then
            CurrentDrawObject := ThisDrawObject((Sender as TShapeEx).Tag)
          else
            if (Sender is TButton) then
              CurrentDrawObject := ThisDrawObject((Sender as TButton).Tag)
            else
              if (Sender is TBitBtn) then
                CurrentDrawObject := ThisDrawObject((Sender as TBitBtn).Tag)
              else
                if (Sender is TPanel) then
                  CurrentDrawObject := ThisDrawObject((Sender as TPanel).Tag)
                else
                  if (Sender is TdxfProgressBar) then
                    CurrentDrawObject := ThisDrawObject((Sender as TdxfProgressBar).Tag);

    // Resize Object (Resize Height for Rectangle)
    // CTRL + LeftMouseButton
    if (ssCtrl in Shift) and (ssLeft in Shift) then
    begin
      if Assigned(CurrentDrawObject) then
      begin
// PersonalScreen
(*
        if CurrentDrawObject.AObject is TShapeEx then
        begin
          case CurrentDrawObject.AType of
            otLineHorz:
              (CurrentDrawObject.AObject as TShapeEx).Width := X;
            otRectangle, otPuppetBox:
              begin
                (CurrentDrawObject.AObject as TShapeEx).Height :=
                  (CurrentDrawObject.AObject as TShapeEx).Height + Y -
                    FDragOfY;
              end;
            otLineVert:
              (CurrentDrawObject.AObject as TShapeEx).Height := Y;
          end;
        end;
*)
      end;
    end
    else
    begin
      // Resize Width (of Rectangle or LineHorz)
      // ALT + LeftMouseButton
// PersonalScreen      
      if (ssAlt in Shift) and (ssLeft in Shift) then
      begin
(*
        if CurrentDrawObject <> nil then
        begin
          if CurrentDrawObject.AObject is TShapeEx then
          begin
            case CurrentDrawObject.AType of
              otLineHorz:
                (CurrentDrawObject.AObject as TShapeEx).Width := X;
              otRectangle, otPuppetBox:
                begin
                  (CurrentDrawObject.AObject as TShapeEx).Width :=
                    (CurrentDrawObject.AObject as TShapeEx).Width + X -
                      FDragOfX;
                end;
            end;
          end;
        end;
*)
      end
      else
        // Move Object
        // LeftMouseButton
        if (ssLeft in Shift) then
        begin
          if Assigned(CurrentDrawObject) then
          begin
            if (CurrentDrawObject.AObject is TImage) then
            begin
              (CurrentDrawObject.AObject as TImage).Left :=
                (CurrentDrawObject.AObject as TImage).Left + X - FDragOfX;
              (CurrentDrawObject.AObject as TImage).Top :=
                (CurrentDrawObject.AObject as TImage).Top + Y - FDragOfY;
              // PIM-151
              if Assigned(CurrentDrawObject.AGhostImage) then
              begin
                CurrentDrawObject.AGhostImage.Left := (CurrentDrawObject.AObject as TImage).Left;
                CurrentDrawObject.AGhostImage.Top := (CurrentDrawObject.AObject as TImage).Top;
              end;
              // PIM-213
              if (CurrentDrawObject.AProdScreenType = pstWSEmpEff) then
                if Assigned(CurrentDrawObject.ApnlWSEmpEff) then
                begin
                  CurrentDrawObject.ApnlWSEmpEff.Left := CurrentDrawObject.ApnlWSEmpEff.Left + X - FDragOfX;
                  CurrentDrawObject.ApnlWSEmpEff.Top := CurrentDrawObject.ApnlWSEmpEff.Top + Y - FDragOfY;
                  (CurrentDrawObject.AObject as TImage).Left := CurrentDrawObject.ApnlWSEmpEff.Left;
                  (CurrentDrawObject.AObject as TImage).Top := CurrentDrawObject.ApnlWSEmpEff.Top;
                  (CurrentDrawObject.AObject as TImage).Visible := False;
                end;
            end
            else
              if (CurrentDrawObject.AObject is TShapeEx) then
              begin
                (CurrentDrawObject.AObject as TShapeEx).Left :=
                  (CurrentDrawObject.AObject as TShapeEx).Left + X - FDragOfX;
                (CurrentDrawObject.AObject as TShapeEx).Top :=
                  (CurrentDrawObject.AObject as TShapeEx).Top + Y - FDragOfY;
              end;
          end;
        end;
    end;
    if Assigned(CurrentDrawObject) then
    begin
      ShowDrawObject(CurrentDrawObject, CurrentDrawObject.AObject);
      ShowSelectionRectangle(CurrentDrawObject.AObject);
    end;
  end;
end; // ImageMouseMove

procedure THomeF.ImageMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  FDragging := False;
end; // ImageMouseUp

procedure THomeF.ImageClick(Sender: TObject);
begin
  if Sender = nil then
    Exit;

  if (Sender is TButton) then
  begin
    CurrentDrawObject := ThisDrawObject((Sender as TButton).Tag);
  end
  else
  if (Sender is TBitBtn) then
  begin
    CurrentDrawObject := ThisDrawObject((Sender as TBitBtn).Tag);
  end
  else
  if (Sender is TPanel) then
  begin
    CurrentDrawObject := ThisDrawObject((Sender as TPanel).Tag);
  end
  else
  if (Sender is TLabel) then
  begin
    stBarBase.Panels[0].Text := 'Object=' +
      IntToStr((Sender as TLabel).Tag);
    CurrentDrawObject := ThisDrawObject((Sender as TLabel).Tag);
    // 20014450.50 Skip this!
{
    if not tlbtnEdit.Down then
    begin
      if CurrentDrawObject <> nil then
        if (CurrentDrawObject.AProdScreenType <> pstMachineTimeRec) then // Personal Screen
          ShowChartClick(Sender);
    end;
}
  end
  else
  if (Sender is TShape) then
  begin
    stBarBase.Panels[0].Text := 'Object=' +
      IntToStr((Sender as TShape).Tag);
    CurrentDrawObject := ThisDrawObject((Sender as TShape).Tag);
  end
  else
  if (Sender is TImage) then
  begin
    stBarBase.Panels[0].Text := 'Object=' +
      IntToStr((Sender as TImage).Tag);
    CurrentDrawObject := ThisDrawObject((Sender as TImage).Tag);
  end
  else
  if (Sender is TShapeEx) then
  begin
    stBarBase.Panels[0].Text := 'Object=' +
      IntToStr((Sender as TShapeEx).Tag);
    CurrentDrawObject := ThisDrawObject((Sender as TShapeEx).Tag);
  end
  else
  if (Sender is TdxfProgressBar) then
  begin
    stBarBase.Panels[0].Text := 'Object=' +
      IntToStr((Sender as TdxfProgressBar).Tag);
    CurrentDrawObject := ThisDrawObject((Sender as TdxfProgressBar).Tag);
  end;


  if Assigned(CurrentDrawObject) then
    ShowSelectionRectangle(CurrentDrawObject.AObject);
end;  // ImageClick

procedure THomeF.pnlDrawMouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
begin
  if Sender = nil then
    Exit;
  ShowObjectProperties(X, Y);
  stBarBase.Panels[2].Text := 'Y=' + IntToStr(Y) + ' X=' + IntToStr(X);

  // Show Properties Dialog
  if (ssRight in Shift) then
    ShowChangeProperties(Sender);
end; // pnlDrawMouseMove

{ Edit - End }

{ Puppets - Start }

// PersonalScreen
(*
// Look if puppets (employees) of given workspot
// are scanned in and have the same jobcode as
// productionquantity-records.
procedure THomeF.ActionBlinkPuppets;
var
  PupI, PupJ: Integer;
  APuppetShift: PTPuppetShift;
  APuppet: PTPuppet;
  JOBFound: Boolean;
  PQFound: Boolean;
begin
  if CurrentDrawObject = nil then
    Exit;
  PQFound := False;
  JOBFound := False;
  with ProductionScreenDM.odsProductionQuantityBlinkPuppets do
  begin
    Close;
    ClearVariables;
    SetVariable('PLANT_CODE',    CurrentDrawObject.APlantCode);
    SetVariable('WORKSPOT_CODE', CurrentDrawObject.AWorkspotCode);
    // Calculate (interval * 2) before. For example 10 minutes before 'now'.
    SetVariable('DATEFROM', Now -
      (PIMSDatacolTimeInterval * PIMSMultiplier / 60 / 24 / 60));
//      DateTimeBefore(Now, (PIMSDatacolTimeInterval * PIMSMultiplier));
    SetVariable('DATETO',    Now);
    Open;
    Refresh;
    // Is there Production?
    if RecordCount > 0 then
    begin
      PQFound := True;
      First;
      while (not JOBFound) and (not Eof) do
      begin
        // Are there puppets scanned in on this Job?
        for PupI := 0 to CurrentDrawObject.APuppetShiftList.Count - 1 do
        begin
          APuppetShift := CurrentDrawObject.APuppetShiftList.Items[PupI];
          for PupJ := 0 to APuppetShift.APuppetList.Count - 1 do
          begin
            APuppet := APuppetShift.APuppetList.Items[PupJ];
            if APuppet.AScanned and (APuppet.AJobCode =
              FieldByName('JOB_CODE').AsString) then
              JOBFound := True;
          end;
        end;
        Next;
      end;
    end;
    Close;
  end;
  CurrentDrawObject.ABlinkPuppets := PQFound and (not JOBFound);
end; // ActionBlinkPuppets

procedure THomeF.ActionPuppets(ADrawObject: PDrawObject);
begin
  // This gives as result a 'APlanScanClass.EmployeeWSList'
  APlanScanClass.DetermineEmployeeWSListPLAN_SCAN_EMP(ADrawObject.APlantCode,
    ADrawObject.AWorkspotCode);

  ActionCreatePuppets(ADrawObject, APlanScanClass.EmployeeList);
  ShowPuppets(ADrawObject, ADrawObject.AObject);
end; // ActionPuppets

// Create a new puppet-object linked to an employee and set
// the employee-info to this puppet-object.
function THomeF.NewPuppet(AParent: TWinControl;
  APTEmployee: PTEmployee): PTPuppet;
var
  APuppet: PTPuppet;
begin
  APuppet := new(PTPuppet);
  // Head
  APuppet.Puppet[0] := TShape.Create(Application);
  with APuppet.Puppet[0] do
  begin
    Parent := AParent;
    Shape := stCircle;
    Height := Trunc(10 * WorkspotScale / 100);
    Width := Trunc(10 * WorkspotScale / 100);
    Left := Trunc(2 * WorkspotScale / 100);
    Top := Trunc(0  * WorkspotScale / 100);
    Visible := False;
    ShowHint := True;
    Hint := IntToStr(APTEmployee.AEmployeeNumber) + ' - ' +
      APTEmployee.AEmployeeName;
  end;
  // Body
  APuppet.Puppet[1] := TShape.Create(Application);
  with APuppet.Puppet[1] do
  begin
    Parent := AParent;
    Shape := stRectangle;
    Height := Trunc(11 * WorkspotScale / 100);
    Width := Trunc(14 * WorkspotScale / 100);
    Left := Trunc(0 * WorkspotScale / 100);
    Top := Trunc(10 * WorkspotScale / 100);
    Visible := False;
    ShowHint := True;
    Hint := IntToStr(APTEmployee.AEmployeeNumber) + ' - ' +
      APTEmployee.AEmployeeName;
  end;
  // Legs
  APuppet.Puppet[2] := TShape.Create(Application);
  with APuppet.Puppet[2] do
  begin
    Parent := AParent;
    Shape := stRectangle;
    Height := Trunc(10 * WorkspotScale / 100);
    Width := Trunc(8 * WorkspotScale / 100);
    Left := Trunc(3 * WorkspotScale / 100);
    Top := Trunc(20 * WorkspotScale / 100);
    Visible := False;
    ShowHint := True;
    Hint := IntToStr(APTEmployee.AEmployeeNumber) + ' - ' +
      APTEmployee.AEmployeeName;
  end;
{
  // Body
  APuppet.Puppet[1] := TShape.Create(Application);
  with APuppet.Puppet[1] do
  begin
    Parent := AParent;
    Shape := stRectangle;
    Height := Trunc(13 * WorkspotScale / 100);
    Width := Trunc(8 * WorkspotScale / 100);
    Left := Trunc(3 * WorkspotScale / 100);
    Top := Trunc(10 * WorkspotScale / 100);
    Visible := False;
    ShowHint := True;
    Hint := IntToStr(APTEmployee.AEmployeeNumber) + ' - ' +
      APTEmployee.AEmployeeName;
  end;
  // Left Arm
  APuppet.Puppet[2] := TShape.Create(Application);
  with APuppet.Puppet[2] do
  begin
    Parent := AParent;
    Shape := stRectangle;
    Height := Trunc(10 * WorkspotScale / 100);
    Width := Trunc(4 * WorkspotScale / 100);
    Left := Trunc(0 * WorkspotScale / 100);
    Top := Trunc(10 * WorkspotScale / 100);
    Visible := False;
    ShowHint := True;
    Hint := IntToStr(APTEmployee.AEmployeeNumber) + ' - ' +
      APTEmployee.AEmployeeName;
  end;
  // Right Arm
  APuppet.Puppet[3] := TShape.Create(Application);
  with APuppet.Puppet[3] do
  begin
    Parent := AParent;
    Shape := stRectangle;
    Height := Trunc(10 * WorkspotScale / 100);
    Width := Trunc(4 * WorkspotScale / 100);
    Left := Trunc(10 * WorkspotScale / 100);
    Top := Trunc(10 * WorkspotScale / 100);
    Visible := False;
    ShowHint := True;
    Hint := IntToStr(APTEmployee.AEmployeeNumber) + ' - ' +
      APTEmployee.AEmployeeName;
  end;
  // Left Leg
  APuppet.Puppet[4] := TShape.Create(Application);
  with APuppet.Puppet[4] do
  begin
    Parent := AParent;
    Shape := stRectangle;
    Height := Trunc(10 * WorkspotScale / 100);
    Width := Trunc(4 * WorkspotScale / 100);
    Left := Trunc(3 * WorkspotScale / 100);
    Top := Trunc(22 * WorkspotScale / 100);
    Visible := False;
    ShowHint := True;
    Hint := IntToStr(APTEmployee.AEmployeeNumber) + ' - ' +
      APTEmployee.AEmployeeName;
  end;
  // Right Leg
  APuppet.Puppet[5] := TShape.Create(Application);
  with APuppet.Puppet[5] do
  begin
    Parent := AParent;
    Shape := stRectangle;
    Height := Trunc(10 * WorkspotScale / 100);
    Width := Trunc(4 * WorkspotScale / 100);
    Left := Trunc(7 * WorkspotScale / 100);
    Top := Trunc(22 * WorkspotScale / 100);
    Visible := False;
    ShowHint := True;
    Hint := IntToStr(APTEmployee.AEmployeeNumber) + ' - ' +
      APTEmployee.AEmployeeName;
  end;
}
  APuppet.AColor := APTEmployee.AColor;
  APuppet.AEmployeeNumber := APTEmployee.AEmployeeNumber;
  APuppet.AEmployeeDescription := APTEmployee.AEmployeeName;
  APuppet.AEmployeeShortName := APTEmployee.AEmployeeShortName;
  APuppet.AEmployeeTeamCode := APTEmployee.ATeamCode;
  APuppet.AEmpLevel := APTEmployee.AEmpLevel;
  APuppet.AScannedWS := APTEmployee.AScanWorkspotCode;
  APuppet.AScannedDateTimeIn := APTEmployee.AScanDatetimeIn;
  APuppet.APlannedWS := APTEmployee.APlanWorkspotCode;
  APuppet.APlannedStartTime := APTEmployee.APlanStartDate;
  APuppet.APlannedEndTime := APTEmployee.APlanEndDate;
  APuppet.AEPlanLevel := APTEmployee.APlanLevel;
  APuppet.AScanned := APTEmployee.AScanned;
  APuppet.AJobCode := APTEmployee.AJobCode;
  Result := APuppet;
end; // NewPuppet

procedure THomeF.DeletePuppet(var APuppet: PTPuppet);
var
  I: Integer;
begin
  for I:=MAX_PUPPET_PARTS downto 0 do
    APuppet.Puppet[I].Free;
end; // DeletePuppet

procedure THomeF.DeletePuppetList(var APuppetList: TList);
var
  I: Integer;
  APuppet: PTPuppet;
begin
  for I:=APuppetList.Count - 1  downto 0 do
  begin
    APuppet := APuppetList.Items[I];
    DeletePuppet(APuppet);
    APuppetList.Remove(APuppet);
  end;
  APuppetList.Free;
end; // DeletePuppetList

procedure THomeF.DeletePuppetShiftList(var APuppetShiftList: TList);
var
  I: Integer;
  APuppetShift: PTPuppetShift;
begin
  if APuppetShiftList <> nil then
  begin
    for I:=APuppetShiftList.Count - 1 downto 0 do
    begin
      APuppetShift := APuppetShiftList.Items[I];
      DeletePuppetList(APuppetShift.APuppetList);
      APuppetShiftList.Remove(APuppetShift);
    end;
    APuppetShiftList.Free;
    APuppetShiftList := nil;
  end;
end; // DeletePuppetShiftList

procedure THomeF.ShowPuppet(APuppet: PTPuppet;
  ALeft, ATop: Integer; AColor: TColor);
begin
  // Head
  with APuppet.Puppet[0] do
  begin
    Brush.Color := AColor;
    Pen.Color := AColor;
    Left := ALeft + Trunc(2 * WorkspotScale / 100);
    Top := ATop;
    Visible := True;
  end;
  // Body
  with APuppet.Puppet[1] do
  begin
    Brush.Color := AColor;
    Pen.Color := AColor;
    Left := ALeft + Trunc(0 * WorkspotScale / 100);
    Top := ATop + Trunc(10 * WorkspotScale / 100);
    Visible := True;
  end;
  // Legs
  with APuppet.Puppet[2] do
  begin
    Brush.Color := AColor;
    Pen.Color := AColor;
    Left := ALeft + Trunc(3 * WorkspotScale / 100);
    Top := ATop + Trunc(20 * WorkspotScale / 100);
    Visible := True;
  end;
{
  with APuppet.Puppet[1] do
  begin
    Brush.Color := AColor;
    Pen.Color := AColor;
    Left := ALeft + Trunc(3 * WorkspotScale / 100);
    Top := ATop + Trunc(10 * WorkspotScale / 100);
    Visible := True;
  end;
  with APuppet.Puppet[2] do
  begin
    Brush.Color := AColor;
    Pen.Color := AColor;
    Left := ALeft;
    Top := ATop + Trunc(10 * WorkspotScale / 100);
    Visible := True;
  end;
  with APuppet.Puppet[3] do
  begin
    Brush.Color := AColor;
    Pen.Color := AColor;
    Left := ALeft + Trunc(10 * WorkspotScale / 100);
    Top := ATop + Trunc(10 * WorkspotScale / 100);
    Visible := True;
  end;
  with APuppet.Puppet[4] do
  begin
    Brush.Color := AColor;
    Pen.Color := AColor;
    Left := ALeft + Trunc(3 * WorkspotScale / 100);
    Top := ATop + Trunc(22 * WorkspotScale / 100);
    Visible := True;
  end;
  with APuppet.Puppet[5] do
  begin
    Brush.Color := AColor;
    Pen.Color := AColor;
    Left := ALeft + Trunc(7 * WorkspotScale / 100);
    Top := ATop + Trunc(22 * WorkspotScale / 100);
    Visible := True;
  end;
}
end; // ShowPuppet
*)

{ Puppets - End }

{ Personal Screen - Start }

procedure THomeF.CreatePersonalScreenObject(AProdScreenType: TProdScreenType;
  ADrawObject: PDrawObject);
begin
  // Can be: Personal screen
  case AProdScreenType of
  pstNoTimeRec: // No Time Rec. (Square or Big eff. meter).
    begin
      ADrawObject.AMachineTimeRec := nil;
      ADrawObject.AWorkspotTimeRec := nil;
      ADrawObject.APriority := 2;
    end;
  pstMachineTimeRec: // Machine + Time Rec.
    begin
      ADrawObject.CreateMachine(Application, NewItemIndex,
        ButtonChangeJobClick, ImageMouseDown, ImageMouseMove,
        ImageMouseUp, pnlDraw);
    end;
  // PIM-213 We need the 'workspot + timerec'-functionality for this.
  pstWorkspotTimeRec, pstWSEmpEff: // Workspot + Time Rec.
    begin
      ADrawObject.CreateWorkspot(Application, NewItemIndex,
        ButtonChangeJobClick, ButtonInClick, ButtonChangeModeClick,
        ImageMouseDown, ImageMouseMove,
        ImageMouseUp, pnlDraw, CreateShapeEx,
        EfficiencyPeriodMode);
    end;
  end;
end; // CreatePersonalScreenObject

// Create a list of machines + workspots, all belonging to 1 machine
procedure THomeF.CreateMachineWorkspotList(AMyCurrentDrawObject: PDrawObject);
var
  PlantCode: String;
  MachineCode: String;
  ADrawObjectWS: PDrawObject;
  I: Integer;
begin
  PersonalScreenDM.ClearMachineWorkspotList;
  PlantCode := AMyCurrentDrawObject.APlantCode;
  MachineCode := AMyCurrentDrawObject.AMachineTimeRec.AMachineCode;
  for I := 0 to PSList.Count - 1 do
  begin
    ADrawObjectWS := PSList.Items[I];
    if (ADrawObjectWS.AObject is TImage) then
    begin
      if ADrawObjectWS.AProdScreenType = pstWorkspotTimeRec then
        if (ADrawObjectWS.APlantCode = PlantCode) and
          (ADrawObjectWS.AWorkspotTimeRec.AMachineCode = MachineCode) then
        begin
          PersonalScreenDM.AddMachineWorkspotList(ADrawObjectWS.APlantCode,
            ADrawObjectWS.AWorkspotTimeRec.AMachineCode,
            ADrawObjectWS.AWorkspotCode, (ADrawObjectWS.AObject as TImage).Tag);
        end;
    end;
  end;
end; // CreateMachineWorkspotList

// Show error when using Timerecording-dialog
procedure THomeF.ShowStatusErrorMessage(AMsg: String);
begin
  stBarBase.Panels[3].Text := AMsg;
  ShowMsg(AMsg);
//  TimeRecordingF.AddErrorLog(AMsg); // TD-26709 Disabled for now.
  Update;
end; // ShowStatusErrorMessage

// Change job for employees working on 1 specific workspot.
// During this one or more scans are closed/opened using
// Timerecording-procedures.
procedure THomeF.ChangeJobForEmployees(
  AMyCurrentDrawObject: PDrawObject; // TD-26709
  APlantCode, AMachineCode,
  AWorkspotCode, ACurrentJobCode, ANewJobCode,
  ANewJobDescription: String; // 20016447
  ACJMode: Integer);
var
  DateFromCurrent, DateToCurrent, HalfADayBefore: TDateTime;
  WorkspotI: Integer;
  AMachineWorkspot: PTMachineWorkspot;
  LastIDCard, NextIDCard: TScannedIDCard;
  EditPrePlantText: String;
  IsScanning: Boolean;
  SavePrevPlant, SavePrevWK, SavePrevJob, SavePrevDate: String;
  ExportJobChangeDone: Boolean; // 20014722
  MyCurrentDrawObject: PDrawObject; // 20014722
  WSDrawObject: PDrawObject; // 20015178
  ChangeThisJob: Boolean; // PIM-12 / 20016447
  NewJobCodeBackup: String; // PIM-12 / 20016447
  FirstJobCodeFound: String; // PIM-111
  FirstJobDescriptionFound: String; // PIM-111
  // 20015178
  function FakeEmployeeCompareCurrentJob: Boolean;
  begin
    Result := False;
    if ORASystemDM.EnableMachineTimeRec then
    begin
      case MyCurrentDrawObject.AProdScreenType of
      pstMachineTimeRec: // Machine + Time Rec
        if Assigned(WSDrawObject) then
          if Assigned(WSDrawObject.AWorkspotTimeRec) then
            if WSDrawObject.AWorkspotTimeRec.ATimeRecByMachine then
              Result := True;
      pstWorkspotTimeRec, pstWSEmpEff: // Workspot + Time Rec
        if Assigned(MyCurrentDrawObject.AWorkspotTimeRec) then
          if MyCurrentDrawObject.AWorkspotTimeRec.ATimeRecByMachine then
            Result := True;
      end; // case
    end; // if
  end; // FakeEmployeeCompareCurrentJob
  // PIM-12 / 20016447 When job-changed via machine, do not refresh the
  //                   workspots in-between, but do it afterwards.
  procedure ChangeJobPerWorkspot(AThisWorkspotCode: String; AMachineJobChange: Boolean=False);
  var
    goOn: Boolean;
    ScansFound: Boolean; // 20015346 Related to this order
    // 20015178
    // PIM-155 Do this within ChangeJobPerWorkspot, when it cannot find any
    //         previous scans.
    //         Only call this when no previous scans were found,
    //         to insert 1 scan for fake employee.
    function FakeEmployeeJobHandler(ADrawObject: PDrawObject): Boolean;
      procedure Wait(const MSecsToWait: Integer);
      var
        TimeToWait,
        TimeToSendRequest : TDateTime;
      begin
        TimeToWait   := MSecsToWait / MSecsPerDay;
        TimeToSendRequest := TimeToWait + SysUtils.Now;
        while SysUtils.Now < TimeToSendRequest do
          Sleep(0);
      end; // Wait
    begin
      Result := False;
      if not Assigned(ADrawObject) then
        Exit;
      if ORASystemDM.EnableMachineTimeRec then
      begin
        if ADrawObject.AWorkspotTimeRec.ATimeRecByMachine then
        begin
          Application.ProcessMessages;
          // Add the fake employee, when needed to database.
          if ADrawObject.AWorkspotTimeRec.AFakeEmployeeNumberInit = -1 then
          begin
            PersonalScreenDM.CheckCreateFakeEmployeeInDB(
              ADrawObject.APlantCode, AWorkspotCode,
              ADrawObject.AWorkspotTimeRec.AFakeEmployeeNumber);
            ADrawObject.AWorkspotTimeRec.AFakeEmployeeNumberInit :=
              ADrawObject.AWorkspotTimeRec.AFakeEmployeeNumber;
          end; // if
          Application.ProcessMessages;
          // Add the job for the fake employee: If there was no job found, then
          // add a scan for NOJOB
          // 20015178.90 Add a scan for the NewJob (not for NOJOB)
          // PIM-155 IMPORTANT: This only ADDS a scan when there was no
          //                    previous found, it does NOT update an existing
          //                    scan.
          if ADrawObject.AWorkspotTimeRec.AFakeEmployeeNumberInit <> -1 then
          begin
            // PIM-155 Use TimeRecordingF-procedure to insert 1 scan
            // Insert 1 open scan here
            TimeRecordingF.CurrentNow := Now;
            EmptyIDCard(LastIDCard);
            EmptyIDCard(NextIDCard);
            NextIDCard.EmployeeCode :=
              ADrawObject.AWorkspotTimeRec.AFakeEmployeeNumber;
            NextIDCard.EmplName := IntToStr(NextIDCard.EmployeeCode);
            NextIDCard.ShiftNumber := 1;
            NextIDCard.PlantCode := ADrawObject.APlantCode;
            NextIDCard.WorkSpotCode := AWorkspotCode;
            NextIDCard.JobCode := ANewJobCode;
            NextIDCard.Job := ANewJobDescription;
            NextIDCard.DateIn :=
              RoundTimeConditional(TimeRecordingF.CurrentNow, 1);
            TimeRecordingF.InsertTRSNewScan(NextIDCard, True, '');
            PersonalScreenDM.ActionUpdateAfterScan(PSList,
              LastIDCard, NextIDCard);
{
            if not ((ACJMode = CJ_END_OF_DAY) or (ACJMode = CJ_LUNCH)) then
            begin
              Result := PersonalScreenDM.CheckAddScanFakeEmployeeInDB(
                ADrawObject.APlantCode, AWorkspotCode, ANewJobCode, Now, // 20015346
                ADrawObject.AWorkspotTimeRec.AFakeEmployeeNumber);
              Wait(500); // Added a wait, because otherwise it is not refreshing OK.
            end;
}
          end; // if
          Application.ProcessMessages;
          // 20015178.90
          if ORASystemDM.OracleSession.InTransaction then
            ORASystemDM.OracleSession.Commit;
          Application.ProcessMessages;
        end; // if
      end; // if
    end; // FakeEmployeeJobHandler;
    function CompareCurrentJob: Boolean;
    begin
      if ACJMode = CJ_END_OF_DAY then
        Result := True
      else
      begin
        // PIM-147 Exception: When there is only 1 employee???
        Result := (
          ProductionScreenDM.odsTimeRegScanningEFF.
          FieldByName('JOB_CODE').AsString <> ANewJobCode); // ACurrentJobCode); // 20015346
        // 20015178
        if not Result then
          Result := FakeEmployeeCompareCurrentJob;
      end;
    end; // CompareCurrentJob
(*
    // Check if there is a down-job for the workspot.
    // If not, the add it.
    // PIM-111 Related to this issue: This is already during read of all objects!
    function CheckAddJobForWorkspot: Boolean;
    begin
      Result := True;
      try
        with ProductionScreenDM do
        begin
          oqCheckJob.ClearVariables;
          oqCheckJob.SetVariable('PLANT_CODE',    APlantCode);
          oqCheckJob.SetVariable('WORKSPOT_CODE', AThisWorkspotCode);
          oqCheckJob.SetVariable('JOB_CODE',      ANewJobCode);
          oqCheckJob.Execute;
          if oqCheckJob.Eof then
          begin
            // Add the job for the workspot.
            oqInsertJob.ClearVariables;
            oqInsertJob.SetVariable('PLANT_CODE',    APlantCode);
            oqInsertJob.SetVariable('WORKSPOT_CODE', AThisWorkspotCode);
            oqInsertJob.SetVariable('JOB_CODE',      ANewJobCode);
            if ANewJobCode = MECHANICAL_DOWN_JOB then
              oqInsertJob.SetVariable('DESCRIPTION', SPimsMechanicalDown)
            else
              if ANewJobCode = NO_MERCHANDIZE_JOB then
                oqInsertJob.SetVariable('DESCRIPTION', SPimsNoMerchandize);
{              else // The Breakjob is not needed here.
                if ANewJobCode = BREAKJOB then
                  oqInsertJob.SetVariable('DESCRIPTION',
                    SPimsBreakJobDescription); }
            oqInsertJob.SetVariable('MUTATOR',
              ORASystemDM.CurrentProgramUser);
            oqInsertJob.Execute;
            if ORASystemDM.OracleSession.InTransaction then
              ORASystemDM.OracleSession.Commit;
          end;
        end;
      except
        on E:EOracleError do
        begin
          WErrorLog(E.Message);
          Result := False;
        end;
        on E: Exception do
        begin
          WErrorLog(E.Message);
          Result := False;
        end;
      end;
    end; // CheckAddJobForWorkspot
*)
    function DeterminePrevJob: Boolean;
    var
      WorkspotDrawObject: PDrawObject; // PIM-111
      JobCodeRec: PJobCodeRec; // PIM-111
    begin
      Result := False;
      with ProductionScreenDM do
      begin
        oqGetPrevTRJob.ClearVariables;
        oqGetPrevTRJob.SetVariable('PLANT_CODE',    APlantCode);
        oqGetPrevTRJob.SetVariable('WORKSPOT_CODE', AThisWorkspotCode);
        oqGetPrevTRJob.SetVariable('DOWNJOB1',      MECHANICAL_DOWN_JOB);
        oqGetPrevTRJob.SetVariable('DOWNJOB2',      NO_MERCHANDIZE_JOB);
        oqGetPrevTRJob.SetVariable('EMPLOYEE_NUMBER',
          odsTimeRegScanningEFF.FieldByName('EMPLOYEE_NUMBER').AsInteger);
        oqGetPrevTRJob.SetVariable('DATEFROM',      DateFromCurrent);
        oqGetPrevTRJob.SetVariable('DATETO',        IncSecond(DateToCurrent, 60)); // PIM-111 Look 60 seconds in the future);
        oqGetPrevTRJob.Execute;
        if not oqGetPrevTRJob.Eof then
        begin
          Result := True;
          ANewJobCode := oqGetPrevTRJob.FieldAsString('JOB_CODE');
          // PIM-111 Get Job Description
          WorkspotDrawObject :=
            FindDrawObject(PSList,  APlantCode, AThisWorkspotCode);
          if Assigned(WorkspotDrawObject) then
          begin
            JobCodeRec := WorkspotDrawObject.FindJobCodeListRec(ANewJobCode);
            if Assigned(JobCodeRec) then
              ANewJobDescription := JobCodeRec.JobCodeDescription;
          end;
        end;
      end;
    end; // DeterminePrevJob
    // Check if the job exists for the given workspot.
    function WorkspotJobExists: Boolean;
    begin
      with ProductionScreenDM do
      begin
        oqCheckJob.ClearVariables;
        oqCheckJob.SetVariable('PLANT_CODE',    APlantCode);
        oqCheckJob.SetVariable('WORKSPOT_CODE', AThisWorkspotCode);
        oqCheckJob.SetVariable('JOB_CODE',      ANewJobCode);
        oqCheckJob.Execute;
        Result := not oqCheckJob.Eof;
        if not Result then
          ShowStatusErrorMessage(SPimsJobDoesNotExistForWorkspot);
      end;
    end; // WorkspotJobExists
  begin
    ScansFound := False; // 20015346
    // TD-26709
    if Debug then
      WLog('ChangeJobPerWorkspot');
    with ProductionScreenDM do
    begin
      if Debug then
        WLog('DetermineCurrentOpenScans');
      // Determine current open scans.
      // ABS-18715 This must be set to Now because it is used later as DateIn or DateOut!!!
      DateToCurrent := Now;
      // Calculate a period of n-minutes before.
      DateFromCurrent := DateToCurrent -
        (PIMSDatacolTimeInterval * PIMSMultiplier / 60 / 24 / 60);
      // 12 hours before!
      HalfADayBefore := DateFromCurrent - 0.5;
      // Bugfix related to 20013516. It did not read the 'Down'-jobs.
      //   Reasons: It only read trs-records for 'show_at_productionscreen_yn'.
      //   This is wrong, it should read all trs-records here.
      if Debug then
        WLog('-> DateToCurrent=' + DateTimeToStr(DateToCurrent) +
          ' HalfADayBefore=' + DateTimeToStr(HalfADayBefore)
          );
      odsTimeRegScanningEFF.Close;
      odsTimeRegScanningEFF.ClearVariables;
      odsTimeRegScanningEFF.SetVariable('PLANT_CODE',      APlantCode);
      odsTimeRegScanningEFF.SetVariable('WORKSPOT_CODE',   AThisWorkspotCode);
      odsTimeRegScanningEFF.SetVariable('SHIFT_NUMBER',    -1);
//      odsTimeRegScanningEFF.SetVariable('DATEFROM',        DateFromCurrent); // 20015346
      // 20015178.90 Round it to the upper minute by adding 30 seconds to the current time.
      // This prevents it will not find it because it was rounded to upper minute.
      odsTimeRegScanningEFF.SetVariable('DATETO',          IncSecond(DateToCurrent, 60)); // ABS-18715
      odsTimeRegScanningEFF.SetVariable('HALFADAY_BEFORE', HalfADayBefore);
      odsTimeRegScanningEFF.Open;
      // WLog('odsTimeRegScanningEFF.RecordCount=' + IntToStr(odsTimeRegScanningEFF.RecordCount));
      while not odsTimeRegScanningEFF.Eof do
      begin
        if Debug then
          WLog('Close current open scan');
        // Close current open scan for current job.
        if CompareCurrentJob and
          (odsTimeRegScanningEFF.
           FieldByName('DATETIME_OUT').AsDateTime = NullDate) then
        begin
          ScansFound := True;
          goOn := True;
          EmptyIDCard(LastIDCard);
          PopulateIDCard(LastIDCard, odsTimeRegScanningEFF);
          // 20013489.10 Be sure to fill DateOut!
          LastIDCard.DateOut := RoundTimeConditional(DateToCurrent, 1); // 20015346
          EmptyIDCard(NextIDCard);
          PopulateIDCard(NextIDCard, odsTimeRegScanningEFF);
          NextIDCard.JobCode := ANewJobCode;
          NextIDCard.DateIn := RoundTimeConditional(DateToCurrent, 1); // 20015346
          if Debug then
            WLog('-> NextIDCard.DateIn: ' + DateTimeToStr(NextIDCard.DateIn));
          if ACJMode = CJ_END_OF_DAY then
          begin
            if Debug then
              WLog('-> End Of Day');
            EditPrePlantText := SpimsEndDay;
            NextIDCard.Job := SPimsEndDay;
          end
          else
          begin
            EditPrePlantText := '';
            case ACJMode of
            CJ_DOWN:
              begin
                if Debug then
                  WLog('-> Down');
                // PIM-111 Related to this issue: This is already during read of all objects!
                goOn := True; // CheckAddJobForWorkspot;
              end;
            CJ_CONTINUE:
              begin
                if Debug then
                  WLog('-> Continue');
                goOn := DeterminePrevJob;
                NextIDCard.JobCode := ANewJobCode;
              end;
            CJ_CHANGE_JOB:
              begin
                if Debug then
                  WLog('-> Change Job');
                // PIM-147 Only change job when old job is equal to current job.
                if odsTimeRegScanningEFF.RecordCount > 1 then
                  if LastIDCard.JobCode <> ACurrentJobCode then
                    GoOn := False;
                if GoOn then
                  goOn := WorkspotJobExists;
              end;
            CJ_BREAK:
              begin
                if Debug then
                  WLog('-> Break');
                // PIM-111 Related to this issue: This is already during read of all objects!
                goOn := True; // CheckAddJobForWorkspot;
              end;
            CJ_LUNCH:
              begin
                // No action needed: Same as end-of-day.
              end;
            end; // case
          end; // if
          if goOn then
          begin
            IsScanning := True;
            // Previous scan info
            SavePrevPlant := LastIDCard.PlantCode;
            SavePrevWK := LastIDCard.WorkSpotCode;
            SavePrevJob := LastIDCard.JobCode;
            SavePrevDate := FormatDateTime(LONGDATE, LastIDCard.DateOut);
            try
              // Close current scan and optional create a new scan.
              if Debug then
                WLog('-> HandleScanAction');
              TimeRecordingF.CurrentNow := DateToCurrent; // ABS-18715
              TimeRecordingF.HandleScanAction(
                LastIDCard, NextIDCard,
                EditPrePlantText,
                IsScanning,
                SavePrevPlant, SavePrevWK, SavePrevJob, SavePrevDate);
              if ORASystemDM.OracleSession.InTransaction then
                ORASystemDM.OracleSession.Commit;
              if Debug then
                WLog('-> UpdateAfterScan');
              PersonalScreenDM.ActionUpdateAfterScan(PSList,
                TimeRecordingDM.LastIDCard, TimeRecordingDM.NextIDCard);
              // PIM-12 / 20016447
              if not AMachineJobChange then
              begin
                if Debug then
                  WLog('-> ActionShowAllWorkspotDetails');
                ActionShowAllWorkspotDetails;
              end;
              // 20014722
              if ExportJobChange then
              begin
                // Only do this once! Because multiple employees are processed
                // here but all for same workspot + (new) job.
                if not ExportJobChangeDone then
                begin
                  try
                    // NextIDCard.WorkspotCode can be empty when
                    // employee is scanning out, but we need to know the
                    // WorkspotCode, so use AWorkspotCode as argument.
                    // NextIDCard.Jobcode can also be empty when employee
                    // is scanning out.
                    // Machine-level:
                    if MyCurrentDrawObject.AProdScreenType = pstMachineTimeRec then
                      ExportJobChangeDone :=
                        ExportJobChangeToFile(
                          FindDrawObject(PSList, APlantCode, AThisWorkspotCode),
                          APlantCode,
                          AThisWorkspotCode,
                          NextIDCard.JobCode, Now)
                    else // Workspot-level:
                      ExportJobChangeDone :=
                        ExportJobChangeToFile(MyCurrentDrawObject, APlantCode,
                        AWorkspotCode, {NextIDCard.WorkSpotCode,}
                        NextIDCard.JobCode, Now);
                  except
                    on E:EOracleError do
                      WErrorLog(E.Message + ' (Error during export job change).');
                    on E: Exception do
                      WErrorLog(E.Message + ' (Error during export job change).');
                  end;
                end; // if
              end; // if
            except
              on E:EOracleError do
              begin
                WErrorLog(E.Message + ' (Error during job change).');
              end;
              on E: Exception do
              begin
                WErrorLog(E.Message + ' (Error during job change).');
              end;
            end;
          end;
        end;
        odsTimeRegScanningEFF.Next;
      end; // while
      if not ScansFound then // 20015346
        if not FakeEmployeeJobHandler(AMyCurrentDrawObject) then // PIM-155
          ANewJobCode := '';
    end; // with
  end; // ChangeJobPerWorkspot
begin
  if Debug then
    WLog('ChangeJobForEmployees P=' + APlantCode + ' W=' + AWorkspotCode +
      ' OJ=' + ACurrentJobCode + ' NJ=' + ANewJobCode);

  // Be sure the CurrentDrawObject is taken from an argument to prevent it is
  // changed in-between.
  CurrentDrawObject := AMyCurrentDrawObject;   // TD-26709
  CurrentDrawObject.ARecreateEmployeeDisplayList := True; // 20014550.50
  MyCurrentDrawObject := AMyCurrentDrawObject; // TD-26709
  if not Assigned(MyCurrentDrawObject) then
    Exit;
  Screen.Cursor := crHourGlass;
  try
    case MyCurrentDrawObject.AProdScreenType of
    pstMachineTimeRec: // Machine + Time Rec
      begin
        // 1 machine with 1 or more workspots
        // PIM-12 / 20016447 Only change workspot-job when it was the same
        //                   as current machine-job!
        // PIM-12 / 20016447 The 'ANewJobCode' can be emptied in-between
        //                   We need it for later use for machine's currentjob.
        NewJobCodeBackup := ANewJobCode;
        FirstJobCodeFound := ''; // PIM-111
        for WorkspotI := 0 to PersonalScreenDM.MachineWorkspotList.Count - 1 do
        begin
          AMachineWorkspot := PersonalScreenDM.MachineWorkspotList[WorkspotI];
          ExportJobChangeDone := False; // 20014722
          WSDrawObject := FindDrawObject(PSList,
            MyCurrentDrawObject.APlantCode, AMachineWorkspot.AWorkspotCode); // 20015178
          // In case no previous job can be found, just take the first one
          // available.
          if (FirstJobCodeFound = '') and
            (WSDrawObject.AWorkspotTimeRec.ACurrentJobCode <> '') then
          begin
            FirstJobCodeFound := WSDrawObject.AWorkspotTimeRec.ACurrentJobCode;
            FirstJobDescriptionFound := WSDrawObject.AWorkspotTimeRec.ACurrentJobDescription;
          end;
//          FakeEmployeeJobHandler(WSDrawObject); // 20015178 // PIM-155
          // 20016447 Only change workspot-job when it was the same
          //          as current machine-job
          ChangeThisJob := False;
          // PIM-111 For machine-workspot-functionality:
          //         If new job is a DOWN-job then always change the job!
          if (ACurrentJobCode <> '') and (ACJMode <> CJ_DOWN) then
          begin
            if (ACurrentJobCode = WSDrawObject.AWorkspotTimeRec.ACurrentJobCode) then
              ChangeThisJob := True;
          end
          else
          begin
            // PIM-111 Check if current job is already equal to down-job,
            //         in which case there is no need to change it.
            if (ACJMode = CJ_DOWN) and
              ((WSDrawObject.AWorkspotTimeRec.ACurrentJobCode = MECHANICAL_DOWN_JOB) or
               (WSDrawObject.AWorkspotTimeRec.ACurrentJobCode = NO_MERCHANDIZE_JOB)) then
                ChangeThisJob := False
            else
              ChangeThisJob := True;
          end;
          if ChangeThisJob then
            ChangeJobPerWorkspot(AMachineWorkspot.AWorkspotCode, True);
          // PIM-12 End-Of-Day was clicked on machine-level, be sure to recreate
          //        the employeelist for all workspots that are linked to it.
          if ACJMode = CJ_END_OF_DAY then
            WSDrawObject.ARecreateEmployeeDisplayList := True;
//          ActionEfficiency(ThisDrawObject(AMachineWorkspot.ATag));
        end;
        // 20016447 Set new job to machine here!
        if (NewJobCodeBackup = '') and (ACJMode <> CJ_END_OF_DAY) then
          NewJobCodeBackup := ANewJobCode; // PIM-111
        if (NewJobCodeBackup = '') then // PIM-111 Still empty?
        begin
          NewJobCodeBackup := FirstJobCodeFound;
          ANewJobDescription := FIrstJobDescriptionFound;
        end;
        if (NewJobCodeBackup <> '') then // PIM-111 Be sure it is not empty!
        begin
          MyCurrentDrawObject.AMachineTimeRec.ACurrentJobCode := NewJobCodeBackup;
          MyCurrentDrawObject.AMachineTimeRec.ACurrentJobDescription := ANewJobDescription;
        end;
        ActionShowAllWorkspotDetails;
        Application.ProcessMessages;
      end;
    pstWorkspotTimeRec, pstWSEmpEff: // Workspot + Time Rec
      begin
        // 1 workspot
        ExportJobChangeDone := False; // 20014722
        WSDrawObject := nil; // 20015178
        // PIM-155 Only if FakeEmployeeJobHandler returns False we
        //         can call ChangeJobPerWorkspot, to prevent it is going
        //         to add a new scan that was already done by this function.
        // PIM-155 Handle the Fake Employee from ChangeJobPerWorkspot
//        if not FakeEmployeeJobHandler(MyCurrentDrawObject) then // 20015178 // PIM-155
        ChangeJobPerWorkspot(AWorkspotCode);
        ActionShowAllWorkspotDetails;
        Application.ProcessMessages;
//        ActionEfficiency(
//          ThisDrawObject((CurrentDrawObject.AObject as TImage).Tag));
      end;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end; // ChangeJobForEmployees

// Change Job: Show ChangeJob-Dialog where job can be changed for
// one or more employees, several button-actions are possible.
procedure THomeF.ChangeJob;
var
  NewWorkspotCode, NewJobCode: String;
  ModalResult: Integer;
  MachineDrawObject: PDrawObject; // 20014450
  OldJobCode: String; // 20013476
  NewJobDescription: String; // 20016447
  MyCurrentDrawObject: PDrawObject; // 20014550.50 Related to this order
  // 20014450.50
(*
  procedure AssignSampleTimeMinsJob(AJobCode: String;
    ACurrent: Boolean; AInit: Boolean=False); // 20013476
  var
    AJobCodeRec: PJobCodeRec;
  begin
    if AInit then
    begin
      if not ACurrent then
      begin
        CurrentDrawObject.APrevJobCode := '';
        CurrentDrawObject.APrevJobSampleTimeMins := 0;
      end
      else
      begin
        CurrentDrawObject.ACurJobSampleTimeMins := 0;
      end;
    end
    else
    begin
      AJobCodeRec := CurrentDrawObject.FindJobCodeListRec(AJobCode);
      if Assigned(AJobCodeRec) then
      begin
        if not ACurrent then
        begin
          CurrentDrawObject.APrevJobCode := AJobCodeRec.JobCode;
          CurrentDrawObject.APrevJobSampleTimeMins :=
            AJobCodeRec.Sample_time_mins;
        end
        else
        begin
          CurrentDrawObject.ACurJobSampleTimeMins :=
            AJobCodeRec.Sample_time_mins;
        end;
      end;
    end;
  end; // AssignSampleTimeMinsJob
*)
(*
  procedure MySetCurrentPeriod; // 20013476
  begin
    // 20014450.50 Use a fixed value based on plant-level
    SetCurrentPeriod(Round(MyCurrentDrawObject.ARealTimeIntervalMinutes));
  end; // MySetCurrentPeriod
*)  
  // Look if current job is down-job.
  function IsDown: Boolean;
  begin
    Result := False;
    case MyCurrentDrawObject.AProdScreenType of
    pstMachineTimeRec: // Machine + Time Rec
      Result :=
        (MyCurrentDrawObject.AMachineTimeRec.ACurrentJobCode =
        MECHANICAL_DOWN_JOB) or
        (MyCurrentDrawObject.AMachineTimeRec.ACurrentJobCode =
        NO_MERCHANDIZE_JOB);
    pstWorkspotTimeRec, pstWSEmpEff: // Workspot + Time Rec
      Result :=
        (MyCurrentDrawObject.AWorkspotTimeRec.ACurrentJobCode =
        MECHANICAL_DOWN_JOB) or
        (MyCurrentDrawObject.AWorkspotTimeRec.ACurrentJobCode =
        NO_MERCHANDIZE_JOB);
    end; // case
  end; // IsDown
  // Look if current job is break-job.
  function IsBreak: Boolean;
  begin
    Result := False;
    case MyCurrentDrawObject.AProdScreenType of
    pstMachineTimeRec: // Machine + Time Rec
      Result :=
        (MyCurrentDrawObject.AMachineTimeRec.ACurrentJobCode = BREAKJOB);
    pstWorkspotTimeRec, pstWSEmpEff: // Workspot + Time Rec
      Result :=
        (MyCurrentDrawObject.AWorkspotTimeRec.ACurrentJobCode = BREAKJOB);
    end; // case
  end; // IsBreak
  // PIM-50
  procedure EnterJobCommentAction;
  var
    AEmployeeDisplayRec: PTEmployeeDisplayRec;
    EmployeeNumber: Integer;
  begin
    EmployeeNumber := -1;
    // Get first employee
    if Assigned(MyCurrentDrawObject.AEmployeeDisplayList) then
      if MyCurrentDrawObject.AEmployeeDisplayList.Count > 0 then
      begin
        AEmployeeDisplayRec := MyCurrentDrawObject.AEmployeeDisplayList.Items[0];
        EmployeeNumber := AEmployeeDisplayRec.EmployeeNumber;
      end;
    EnterJobCommentDM.EnterJobComment(
      MyCurrentDrawObject.APlantCode,
      MyCurrentDrawObject.AWorkspotCode,
      MyCurrentDrawObject.AWorkspotTimeRec.ACurrentJobCode,
      EmployeeNumber, HomeF);
  end; // EnterJobCommentAction
begin
  MyCurrentDrawObject := TimeRecordingCurrentDrawObject;
  MachineDrawObject := MyCurrentDrawObject;
  OldJobCode := ''; // 20013476
  NewJobCode := ''; // 20013476
  if not Assigned(MyCurrentDrawObject) then
    Exit;
  MyCurrentDrawObject.ARecreateEmployeeDisplayList := True; // 20014550.50
  ModalResult := ModalCancel;
  DialogChangeJobDM.ProdScreenType := MyCurrentDrawObject.AProdScreenType;
  DialogChangeJobF.ProdScreenType := MyCurrentDrawObject.AProdScreenType;
  DialogChangeJobF.UseDownButton(True);
  DialogChangeJobF.UseContinueButton(True);
  // Also use Break and Lunch-buttons
  DialogChangeJobF.UseBreakButton(ORASystemDM.BreakBtnVisible); // PIM-12
  DialogChangeJobF.UseLunchButton(ORASystemDM.LunchBtnVisible); // PIM-12
  DialogChangeJobF.UseEndOfDayButton(ORASystemDM.PSEndOfDayBtnVisible); // PIM-12
  DialogChangeJobDM.PlantCode := MyCurrentDrawObject.APlantCode;
  DialogChangeJobF.AssignPosition(HomeF);
  case MyCurrentDrawObject.AProdScreenType of
  pstMachineTimeRec: // Machine + Time Rec
    begin
      CreateMachineWorkspotList(MachineDrawObject);
      DialogChangeJobDM.MachineCode :=
        MyCurrentDrawObject.AMachineTimeRec.AMachineCode;
      DialogChangeJobDM.MachineDescription :=
        MyCurrentDrawObject.AMachineTimeRec.AMachineDescription;
      DialogChangeJobDM.WorkspotCode := '';
      DialogChangeJobDM.CurrentJobCode :=
        MyCurrentDrawObject.AMachineTimeRec.ACurrentJobCode;
      DialogChangeJobF.IsDown := IsDown;
      DialogChangeJobF.IsBreak := IsBreak;
      DialogChangeJobF.JobCount :=
        MyCurrentDrawObject.AMachineTimeRec.AJobCount; // 20014450
      OldJobCode := MyCurrentDrawObject.AMachineTimeRec.ACurrentJobCode; // 20013476
      // TD-21429
      RepositionSubForm(DialogChangeJobF);
      DialogChangeJobF.EndOfDayBlink := False;
      ModalResult := DialogChangeJobF.ShowModal;
      case ModalResult of
      ModalOK:
        begin
          // Switch job for 1 machine with multiple workspots.
          NewWorkspotCode := DialogChangeJobF.WorkspotCode;
          NewJobCode := DialogChangeJobF.JobCode;
          NewJobDescription := DialogChangeJobF.JobDescription; // 20016449
          ChangeJobForEmployees(MyCurrentDrawObject,
            MyCurrentDrawObject.APlantCode,
            MyCurrentDrawObject.AMachineTimeRec.AMachineCode,
            MyCurrentDrawObject.AWorkspotCode,
            MyCurrentDrawObject.AMachineTimeRec.ACurrentJobCode,
            NewJobCode, NewJobDescription, CJ_CHANGE_JOB);
        end;
      ModalCancel:
        begin
          // Do nothing
        end;
      ModalEndOfDay:
        begin
          // End of Day (Lunch is same as 'End of Day')
          ChangeJobForEmployees(MyCurrentDrawObject,
            MyCurrentDrawObject.APlantCode,
            MyCurrentDrawObject.AMachineTimeRec.AMachineCode,
            MyCurrentDrawObject.AWorkspotCode,
            MyCurrentDrawObject.AMachineTimeRec.ACurrentJobCode,
            NewJobCode, '', CJ_END_OF_DAY);
        end;
      ModalMechanicalDown:
        begin
          // Change job to Down-job: 'mechanical down'.
          NewJobCode := MECHANICAL_DOWN_JOB;
          ChangeJobForEmployees(
            MyCurrentDrawObject,
            MyCurrentDrawObject.APlantCode,
            MyCurrentDrawObject.AMachineTimeRec.AMachineCode,
            MyCurrentDrawObject.AWorkspotCode,
            MyCurrentDrawObject.AMachineTimeRec.ACurrentJobCode,
            NewJobCode, STransMechDown, CJ_DOWN);
        end;
      ModalNoMerchandizeDown:
        begin
          // Change job to Down-job: 'no merchandize'.
          NewJobCode := NO_MERCHANDIZE_JOB;
          ChangeJobForEmployees(
            MyCurrentDrawObject,
            MyCurrentDrawObject.APlantCode,
            MyCurrentDrawObject.AMachineTimeRec.AMachineCode,
            MyCurrentDrawObject.AWorkspotCode,
            MyCurrentDrawObject.AMachineTimeRec.ACurrentJobCode,
            NewJobCode, STransNoMerch, CJ_DOWN);
        end;
      ModalContinue:
        begin
          // Change job to previous job (before the Down-job).
          if (MyCurrentDrawObject.AMachineTimeRec.ACurrentJobCode =
            MECHANICAL_DOWN_JOB) or
            (MyCurrentDrawObject.AMachineTimeRec.ACurrentJobCode =
            NO_MERCHANDIZE_JOB)
            then
            ChangeJobForEmployees(
              MyCurrentDrawObject,
              MyCurrentDrawObject.APlantCode,
              MyCurrentDrawObject.AMachineTimeRec.AMachineCode,
              MyCurrentDrawObject.AWorkspotCode,
              MyCurrentDrawObject.AMachineTimeRec.ACurrentJobCode,
              NewJobCode, STransContinue, CJ_CONTINUE);
        end;
      ModalBreak:
        begin
          // Change job to Break-job: 'Break'.
          NewJobCode := BREAKJOB;
          ChangeJobForEmployees(
            MyCurrentDrawObject,
            MyCurrentDrawObject.APlantCode,
            MyCurrentDrawObject.AMachineTimeRec.AMachineCode,
            MyCurrentDrawObject.AWorkspotCode,
            MyCurrentDrawObject.AMachineTimeRec.ACurrentJobCode,
            NewJobCode, STransBreak, CJ_BREAK);
        end;
      end; // case
      // 20014450
      if ORASystemDM.RegisterMachineHours then
      begin
        case ModalResult of
        ModalMechanicalDown,
        ModalNoMerchandizeDown:
          if (NewJobCode = MECHANICAL_DOWN_JOB) or
            (NewJobCode = NO_MERCHANDIZE_JOB) then
          begin
            MachineDrawObject.AMachineTimeRec.AReasonCode := NewJobCode;
            MachineDrawObject.AMachineTimeRec.AStartTimeStamp := Now;
            MachineDrawObject.AMachineTimeRec.ADown := True;
            MachineDrawObject.AMachineTimeRec.ACurrentJobCode := NewJobCode;
            if MachineDrawObject.AMachineTimeRec.ACurrentJobCode = MECHANICAL_DOWN_JOB then
              MachineDrawObject.AMachineTimeRec.ACurrentJobDescription :=
                SPimsMechanicalDown
            else
              if MachineDrawObject.AMachineTimeRec.ACurrentJobCode = NO_MERCHANDIZE_JOB then
                MachineDrawObject.AMachineTimeRec.ACurrentJobDescription :=
                  SPimsNoMerchandize;
            PersonalScreenDM.UpdateMachineDownTime(MachineDrawObject);
          end;
        ModalOK,
        ModalEndOfDay,
        ModalLunch,
        ModalContinue,
        ModalBreak:
          if MachineDrawObject.AMachineTimeRec.ADown then
          begin
            PersonalScreenDM.UpdateMachineDownTime(MachineDrawObject);
            MachineDrawObject.AMachineTimeRec.ACurrentJobCode := NewJobCode;
            if NewJobCode = '' then
              MachineDrawObject.AMachineTimeRec.ACurrentJobDescription :=
                NewJobCode;
            MachineDrawObject.AMachineTimeRec.AReasonCode := NewJobCode;
            MachineDrawObject.AMachineTimeRec.ADown := False;
          end;
        end; // case
      end;
      // 20014450.50
{      if NewJobCode <> '' then // 20013476
      begin
        AssignSampleTimeMinsJob(OldJobCode, False);
        AssignSampleTimeMinsJob(NewJobCode, True);
      end; }
    end; // machine
  pstWorkspotTimeRec, pstWSEmpEff: // Workspot + Time Rec // PIM-213
    begin
      DialogChangeJobDM.MachineCode := '';
      DialogChangeJobDM.WorkspotCode := MyCurrentDrawObject.AWorkspotCode;
      DialogChangeJobDM.WorkspotDescription :=
        MyCurrentDrawObject.AWorkspotDescription;
      DialogChangeJobDM.CurrentJobCode :=
        MyCurrentDrawObject.AWorkspotTimeRec.ACurrentJobCode;
      DialogChangeJobDM.TimeRecByMachine :=
        MyCurrentDrawObject.AWorkspotTimeRec.ATimeRecByMachine; // 20015178
      DialogChangeJobF.IsDown := IsDown;
      DialogChangeJobF.IsBreak := IsBreak;
      OldJobCode := MyCurrentDrawObject.AWorkspotTimeRec.ACurrentJobCode; // 20013476
      // TD-21429
      RepositionSubForm(DialogChangeJobF);
      DialogChangeJobF.EndOfDayBlink := False;
      ModalResult := DialogChangeJobF.ShowModal;
      // PIM-50
      if ORASystemDM.AddJobComment then
      begin
        case ModalResult of
        ModalOK,
        ModalEndOfDay,
        ModalLunch,
        ModalContinue,
        ModalBreak:
          begin
            EnterJobCommentAction;
          end;
        end; // case
      end; // if
      case ModalResult of
      ModalOK:
        begin
          // Switch job for 1 workspot.
          NewWorkspotCode := ''; // same workspot
          NewJobCode := DialogChangeJobF.JobCode;
          NewJobDescription := DialogChangeJobF.JobDescription; // 20016449
          ChangeJobForEmployees(
            MyCurrentDrawObject,
            MyCurrentDrawObject.APlantCode, '',
            MyCurrentDrawObject.AWorkspotCode,
            MyCurrentDrawObject.AWorkspotTimeRec.ACurrentJobCode,
            NewJobCode, NewJobDescription, CJ_CHANGE_JOB);
        end;
      ModalCancel:
        begin
          // Do nothing
        end;
      ModalEndOfDay,
      ModalLunch:
        begin
          // End of Day. Close scans for 1 workspot.
          // (Lunch is same as 'End of Day')
          ChangeJobForEmployees(
            MyCurrentDrawObject,
            MyCurrentDrawObject.APlantCode, '',
            MyCurrentDrawObject.AWorkspotCode,
            MyCurrentDrawObject.AWorkspotTimeRec.ACurrentJobCode,
            NewJobCode, '', CJ_END_OF_DAY);
        end;
      ModalMechanicalDown:
        begin
          // Change job to Down-job: 'mechanical down'.
          NewJobCode := MECHANICAL_DOWN_JOB;
          ChangeJobForEmployees(
            MyCurrentDrawObject,
            MyCurrentDrawObject.APlantCode, '',
            MyCurrentDrawObject.AWorkspotCode,
            MyCurrentDrawObject.AWorkspotTimeRec.ACurrentJobCode,
            NewJobCode, STransMechDown, CJ_DOWN);
        end;
      ModalNoMerchandizeDown:
        begin
          // Change job to Down-job: 'no merchandize'.
          NewJobCode := NO_MERCHANDIZE_JOB;
          ChangeJobForEmployees(
            MyCurrentDrawObject,
            MyCurrentDrawObject.APlantCode, '',
            MyCurrentDrawObject.AWorkspotCode,
            MyCurrentDrawObject.AWorkspotTimeRec.ACurrentJobCode,
            NewJobCode, STransNoMerch, CJ_DOWN);
        end;
      ModalContinue:
        begin
          // Change job to previous job (before the Down-job).
          if (MyCurrentDrawObject.AWorkspotTimeRec.ACurrentJobCode =
            MECHANICAL_DOWN_JOB) or
            (MyCurrentDrawObject.AWorkspotTimeRec.ACurrentJobCode =
            NO_MERCHANDIZE_JOB) then
            ChangeJobForEmployees(
              MyCurrentDrawObject,
              MyCurrentDrawObject.APlantCode, '',
              MyCurrentDrawObject.AWorkspotCode,
              MyCurrentDrawObject.AWorkspotTimeRec.ACurrentJobCode,
              NewJobCode, STransContinue, CJ_CONTINUE);
        end;
      ModalBreak:
        begin
          // Change job to Break-job: 'Break'.
          NewJobCode := BREAKJOB;
          ChangeJobForEmployees(
            MyCurrentDrawObject,
            MyCurrentDrawObject.APlantCode, '',
            MyCurrentDrawObject.AWorkspotCode,
            MyCurrentDrawObject.AWorkspotTimeRec.ACurrentJobCode,
            NewJobCode, STransBreak, CJ_BREAK);
        end;
      end;
      // 20014450.50
{      if NewJobCode <> '' then // 20013476
      begin
        AssignSampleTimeMinsJob(OldJobCode, False);
        AssignSampleTimeMinsJob(NewJobCode, True);
      end; }
    end; // workspot
  end; // case
  DialogChangeJobF.ResetPosition;
  MyCurrentDrawObject.ARecreateEmployeeDisplayList := True; // 20014550.50
  if ModalResult <> ModalCancel then
    ActionShowAllWorkspotDetails;
//    TimerMainTimer(nil);
  try
    PersonalScreenDM.ReassignCompareJob(PSList); // TD-24819
  except
    // Ignore error
  end;
end; // ChangeJob

// Show Timerecording-dialog, so employee can scan in/out on current workspot.
procedure THomeF.TimeRecordingAction(AInScan: Boolean);
begin
  CurrentDrawObject := TimeRecordingCurrentDrawObject;
  if CurrentDrawObject = nil then
    Exit;
  MarkAllRecreateEmployeeDisplayList(PSList); // 20014550.50
  // Fill parameters needed for Timerecording Form.
  // Assign current Plant and Workspot and Jobcode for Timerecording Form
  TimeRecordingDM.ProdScreenType := CurrentDrawObject.AProdScreenType;
  TimeRecordingDM.PlantCode := CurrentDrawObject.APlantCode;
  case CurrentDrawObject.AProdScreenType of
  pstMachineTimeRec: // Machine + Time Rec
    begin
      CreateMachineWorkspotList(CurrentDrawObject);
      TimeRecordingDM.MachineCode :=
        CurrentDrawObject.AMachineTimeRec.AMachineCode;
      TimeRecordingDM.MachineDescription :=
        CurrentDrawObject.AMachineTimeRec.AMachineDescription;
      TimeRecordingDM.WorkspotCode := '';
      TimeRecordingDM.CurrentJobCode :=
        CurrentDrawObject.AMachineTimeRec.ACurrentJobCode;
    end;
  pstWorkspotTimeRec, pstWSEmpEff: // Workspot + Time Rec
    begin
      TimeRecordingDM.MachineCode := '';
      TimeRecordingDM.WorkspotCode := CurrentDrawObject.AWorkspotCode;
      TimeRecordingDM.WorkspotDescription :=
        CurrentDrawObject.AWorkspotDescription;
      TimeRecordingDM.CurrentJobCode :=
        CurrentDrawObject.AWorkspotTimeRec.ACurrentJobCode;
    end;
  end;
// TODO: How to get a correct ModalResult from TimeRecording ?
//       Now it is always 'mrCancel'!
  RepositionSubForm(TimeRecordingF);
  TimeRecordingF.ShowModal;
  PersonalScreenDM.ActionUpdateAfterScan(PSList,
    TimeRecordingDM.LastIDCard, TimeRecordingDM.NextIDCard);
{  if PersonalScreenDM.ActionUpdateAfterScan(PSList,
    TimeRecordingDM.LastIDCard, TimeRecordingDM.NextIDCard) then // PIM-50
    if ORASystemDM.AddJobComment then
      EnterJobCommentDM.EnterJobComment(TimeRecordingDM.LastIDCard.PlantCode,
        TimeRecordingDM.LastIDCard.WorkSpotCode,
        TimeRecordingDM.LastIDCard.JobCode,
        TimeRecordingDM.LastIDCard.EmployeeCode, HomeF); }
  // 20016016 Be sure the screen is updated to show who has scanned in.
  ActionShowAllWorkspotDetails;
  Application.ProcessMessages;
  // 20014722
  if ExportJobChange then
  begin
    try
      // Is there a switch to a different job on a different workspot?
      if TimeRecordingDM.LastIDCard.WorkSpotCode <> '' then
      begin
        if TimeRecordingDM.LastIDCard.WorkSpotCode <>
          TimeRecordingCurrentDrawObject.AWorkspotCode then
        begin
          // This is the PREVIOUS workspot!
          ExportJobChangeToFile(
            FindDrawObject(PSList,
              TimeRecordingDM.LastIDCard.PlantCode,
              TimeRecordingDM.LastIDCard.WorkSpotCode),
            TimeRecordingDM.LastIDCard.PlantCode,
            TimeRecordingDM.LastIDCard.WorkSpotCode,
            '', Now);
        end;
      end;
      ExportJobChangeToFile(TimeRecordingCurrentDrawObject,
        TimeRecordingCurrentDrawObject.APlantCode,
        TimeRecordingCurrentDrawObject.AWorkspotCode,
        TimeRecordingDM.NextIDCard.JobCode, Now);
    except
      on E: Exception do
        WErrorLog(E.Message + ' (Error during export job change).');
    end;
  end;
  try
    PersonalScreenDM.ReassignCompareJob(PSList); // TD-24819
  except
    // Ignore error
  end;
end; // TimeRecordingAction

// Toggle mode to Current, Since, Shift.
procedure THomeF.ChangeMode;
begin
  if CurrentDrawObject.AWorkspotTimeRec.AModeButton.Caption =
    SPimsModeButtonCurrent then
  begin
    CurrentDrawObject.AWorkspotTimeRec.AModeButton.Caption :=
      SPimsModeButtonSince
  end
  else
    if CurrentDrawObject.AWorkspotTimeRec.AModeButton.Caption =
      SPimsModeButtonSince then
      CurrentDrawObject.AWorkspotTimeRec.AModeButton.Caption :=
        SPimsModeButtonShift
    else
      CurrentDrawObject.AWorkspotTimeRec.AModeButton.Caption :=
        SPimsModeButtonCurrent;
  // PersonalScreen
(*  ActionEfficiency(CurrentDrawObject); *)
  ShowDetails(CurrentDrawObject);
end; // ChangeMode

// Button In
procedure THomeF.ButtonInClick(Sender: TObject);
begin
  inherited;
  if (Sender is TButton) then
    TimeRecordingCurrentDrawObject := ThisDrawObject((Sender as TButton).Tag);
  if not IsBusy then
    MyActionTimeRecording
  else
    MyAction := MyActionTimeRecording;
end; // ButtonInClick

// Button Out - THIS IS NOT USED!
procedure THomeF.ButtonOutClick(Sender: TObject);
begin
  inherited;
//  
end; // ButtonOutClick

// Button Change Job
procedure THomeF.ButtonChangeJobClick(Sender: TObject);
begin
  inherited;
  if (Sender is TButton) then
    TimeRecordingCurrentDrawObject := ThisDrawObject((Sender as TButton).Tag);
  if not IsBusy then
    MyActionChangeJob
  else
    MyAction := MyActionChangeJob;
{
  try
    MainTimerSwitch(False);
    if (Sender is TButton) then
      CurrentDrawObject := ThisDrawObject((Sender as TButton).Tag);
    if not tlbtnEdit.Down then
      ChangeJob;
  finally
    MainTimerSwitch(True);
  end;
}
end; // ButtonChangeJobClick

// Button Change Mode
procedure THomeF.ButtonChangeModeClick(Sender: TObject);
begin
  inherited;
  if (Sender is TButton) then
    CurrentDrawObject := ThisDrawObject((Sender as TButton).Tag);
  if not tlbtnEdit.Down then
    ChangeMode;
end; // ButtonChangeModeClick

// Show a message at top-part of window.
procedure THomeF.ShowMsg(AMsg: String);
var
  NewMsg: String;
begin
  if AMsg = '' then
    NewMsg := ' ' + ThisCaption;
  lblMessage.Caption := NewMsg;
//  Update;
//  if AMsg <> '' then // TD-26329 This does not work!
//    Wait(1000);
end;

{ Personal Screen - End }

{ Finish - Start }

procedure THomeF.FormClose(Sender: TObject; var Action: TCloseAction);
var
  ABlackBoxRec: PBlackBoxRec;
  OK: Boolean;
  ASocketPort: TSocketPort;
  I: Integer;
  // SO-20015330
  procedure ResetCounters;
  var
    ADrawObject: PDrawObject;
    I, J: Integer;
  begin
    try
      for I := 0 to PSList.Count - 1 do
      begin
        ADrawObject := PSList.Items[I];
        if Assigned(ADrawObject.ABlackBoxList) then
        begin
          for J := 0 to ADrawObject.ABlackBoxList.Count - 1 do
          begin
            ABlackBoxRec := ADrawObject.ABlackBoxList.Items[J];
            if ABlackBoxRec.Reset then
            begin
              ASocketPort := SocketPortFind(ABlackBoxRec.IPAddress,
                ABlackBoxRec.Port);
              if Assigned(ASocketPort) then
              begin
                ASocketPort.ModuleAddress := StrToInt(ABlackBoxRec.ModuleAddress);
                if ASocketPort.PortConnect(ASocketPort.Host, ASocketPort.Port,
                  ASocketPort.ModuleAddress) then
                begin
                  case ABlackBoxRec.CounterNumber of
                  1: ASocketPort.ClearCounter0;
                  2: ASocketPort.ClearCounter1;
                  end; // case
                  ASocketPort.Wait(ReadDelay);
                end; // if
              end; // if
            end; // if
          end; // for J
        end; // if
      end; // for I
    except
      on E: Exception do
      begin
        WErrorLog(E.Message);
      end;
    end;
  end; // ResetCounters
begin
  if not Assigned(MyAction) then
    if IsBusy then
    begin
      MyAction := MyActionSaveAndExit;
      Action := caNone;
      Exit;
    end;
  MyAction := nil;
{
  CloseNow := True;
  if Busy then
  begin
    Action := caNone;
    Exit;
  end;
}
  TimerWaitForRead.Enabled := False;
  TimerReadCounter.Enabled := False;
  TimerStatus.Enabled := False;
  TimerBlink.Enabled := False;
  TimerMain.Enabled := False;
  TimerWriteToDB.Enabled := False;
  TimerWriteToFile.Enabled := False;

  TimeRecordingF.Timer5Sec.Enabled := False;
  TimeRecordingF.TimerAutoClose.Enabled := False;
  TimeRecordingF.Timer1Sec.Enabled := False;

  try
    AskForSave;
  except
    on E: Exception do
    begin
      WErrorLog(E.Message);
    end;
  end;

  // TD-21799
  ProductionScreenWaitF.lblMessage.Caption := SPimsSavingAndClosing;
  ProductionScreenWaitF.Show;
  Update;
  Application.ProcessMessages;
  Update;

  try
    ActionWriteToFile;
    Application.ProcessMessages;
    ActionWriteToDB;
    Application.ProcessMessages;
  except
    on E: Exception do
    begin
      WErrorLog(E.Message);
    end;
  end;

  // Save F10-info to log-file.
  if Debug then
    MyActionSaveLogAutomatic;

  // SO-20015330
  try
    ResetCounters;
  except
    on E: Exception do
    begin
      WErrorLog(E.Message);
    end;
  end;
  ProductionScreenWaitF.Hide;
  Update;

  try
    // Multi
    for I := 0 to ASocketPortList.Count - 1 do
    begin
      ASocketPort := ASocketPortList.Items[I];
      try
        // MRA: This gives sometimes an access violation
        //      Why is not clear.
        // 20012858.80.
        // Before closing the port, test if IP-address exists.
        OK := True;
        ABlackBoxRec := BlackBoxFind(ReadCounterList, ASocketPort.Host);
        if ABlackBoxRec.Disabled then
          OK := IPExists(ASocketPort.Host);
        if OK then
        begin
          ASocketPort.Wait(1000);
          Application.ProcessMessages;
          try
            ASocketPort.PortDisconnect;
          except
            on E:EAccessViolation do
            begin
              WErrorLog(E.Message);
            end;
            on E: EIdClosedSocket do
            begin
              WErrorLog(E.Message);
            end;
            on E: Exception do
            begin
              WErrorLog(E.Message);
            end;
          end;
          Application.ProcessMessages;
        end;
      except
        on E:EAccessViolation do
        begin
          WErrorLog(E.Message);
        end;
        on E: EIdClosedSocket do
        begin
          WErrorLog(E.Message);
        end;
        on E: Exception do
        begin
          WErrorLog(E.Message);
        end;
      end;
    end; // for
  finally
   // Multi
//    ASocketPort.Wait(1000);
    Application.ProcessMessages;
    // Write last used workfilename to registry
    WriteRegistry;
    Application.ProcessMessages;
    ProductionScreenWaitF.Hide;
    Update;

    // 20012858.80.
    // Don't do this here, but during 'Destroy' !
{
    with ProductionScreenDM do
    begin
      cdsWorkspotShortName.Close;
      odsJobcode.Active := False;
    end;
    SelectionRectangle.Free;
    DialogWorkspotSelectF.Close;
    DialogWorkspotSelectF.Free;
    DialogDepartmentSelectF.Close;
    DialogDepartmentSelectF.Free;
    DialogShowAllEmployeesF.Free;
    APlanScanClass.Free;
}
  end;
end; // FormClose

{ Finish - End }

procedure THomeF.WMTimeChange(var Message: TMessage);
begin
  PimsTimeInitialised := False;
//  ShowMessage('WMTime changed');
//  TMessage(Message).Result := Integer(True);
end;

procedure THomeF.WriteLog(AMsg: String);
var
  TFile: TextFile;
  Exists: Boolean;
  LogName: String;
  Error1, Error2: Integer;
begin
  // Switch off I/O error checking - PIM-42
  {$I-}
  AMsg := DateTimeToStr(Now) + ' - ' + AMsg;
  LogName := LogPath + ORASystemDM.CurrentComputerName + '_' + LOGFILENAME;
  Error1 := 0;

  try
    Exists := FileExists(LogName);
    AssignFile(TFile, LogName);
    Error1 := IOResult;
    if Error1 = 0 then
      try
        if not Exists then
          Rewrite(TFile)
        else
          Append(TFile);
        Error2 := IOResult;
        if Error2 = 0 then
        begin
          WriteLn(TFile, AMsg);
        end;
      except
        // We cannot log here because this is the write-log-routine - PIM-42
  //      MemoLog.Lines.Add('Error! Cannot find/access file: ' + LogPath);
  //      Update;
      end;
  finally
    if Error1 = 0 then
      CloseFile(TFile);
  end;
  // Switch IO checking back on - PIM-42
  {$I+}
end; // WriteLog

function PathCheck(Path: String): String;
begin
  if Path <> '' then
    if Path[length(Path)] <> '\' then
      Path := Path + '\';
  Result := Path;
end; // PathCheck

// Recalculate percentage to a value:
// -1 to -100 for Red Meter
// 1 to 100 for Green Meter
function THomeF.PrecalcPercentage(
  ADrawObject: PDrawObject; APercentage: Double): Double;
const
  Low=60;
  High=140;
var
  LowRange, HighRange: Integer;
begin
  if APercentage <> 0 then
  begin
    LowRange := 100 - Low;
    HighRange := High - 100;
    // Pre-calculate percentage
    if APercentage <= Low then
      APercentage := Low;
    if APercentage >= High then
      APercentage := High;
    // 60 (Low) to 100 should be Red-indicator
    // 101 to 140 (High) should be Green-indicator
    if (APercentage >= Low) and (APercentage < 100) then
    begin
      // -1 to -39 (-1 to -LowRange)
      APercentage := APercentage - Low;
      APercentage := LowRange - APercentage;
      APercentage := APercentage * -1;
      // Now recalculate to a value from -1 to -100
      APercentage := Round(APercentage * 100 / LowRange);
    end
    else
    begin
      if (APercentage >= 100) and (APercentage <= High) then
      begin
        // 1 to 40 (1 to HighRange)
        APercentage := APercentage - 100;
        // Now recalculate to a value from -1 to -100
        APercentage := Round(APercentage * 100 / HighRange);
      end;
    end;
  end
  else
  begin
    if (not ADrawObject.ANoData) and
      (ADrawObject.AEmployeeCount > 0) then
      APercentage := -100;
  end;
  Result := APercentage;
end; // PrecalcPercentage

// Return DrawObject based on Tag of object in list.
function THomeF.ThisDrawObject(ATag: Integer): PDrawObject;
var
  I, Index: Integer;
  ADrawObject: PDrawObject;
begin
  Index := -1;
  for I := 0 to PSList.Count - 1 do
  begin
    ADrawObject := PSList.Items[I];
    if (ADrawObject.AObject is TImage) then
    begin
      if (ADrawObject.AObject as TImage).Tag = ATag then
      begin
        Index := I;
        Break;
      end;
    end
    else
      if (ADrawObject.AObject is TShapeEx) then
      begin
        if (ADrawObject.AObject as TShapeEx).Tag = ATag then
        begin
          Index := I;
          Break;
        end;
      end;
  end;
  if Index = -1 then
    Result := nil
  else
    Result := PSList.Items[Index];
end; // ThisDrawObject

(*
procedure THomeF.DeleteRectangle(ADrawObject: PDrawObject);
begin
  if (ADrawObject.AType = otRectangle) or
    (ADrawObject.AType = otPuppetBox) then
  begin
    with ADrawObject.ADeptRect.ATopLineHorz do
    begin
      OnClick := nil;
      OnDblClick := nil;
      OnMouseDown := nil;
      OnMouseMove := nil;
      OnMouseUp := nil;
      Free;
    end;
    with ADrawObject.ADeptRect.ARightLineVert do
    begin
      OnClick := nil;
      OnDblClick := nil;
      OnMouseDown := nil;
      OnMouseMove := nil;
      OnMouseUp := nil;
      Free;
    end;
    with ADrawObject.ADeptRect.ABottomLineHorz do
    begin
      OnClick := nil;
      OnDblClick := nil;
      OnMouseDown := nil;
      OnMouseMove := nil;
      OnMouseUp := nil;
      Free;
    end;
    with ADrawObject.ADeptRect.ALeftLineVert do
    begin
      OnClick := nil;
      OnDblClick := nil;
      OnMouseDown := nil;
      OnMouseMove := nil;
      OnMouseUp := nil;
      Free;
    end;
  end;
end; // DeleteRectangle
*)

procedure THomeF.ActionShowChangeProperties(Sender: TObject);
begin
  // Not Needed!
  Exit;
  if Sender = nil then
    Exit;
  try
    if not tlbtnEdit.Down then
      MainTimerSwitch(False);
    ShowChangeProperties(Sender);
    if CurrentDrawObject <> nil then
    begin
      ShowDrawObject(CurrentDrawObject, Sender);
      ShowSelectionRectangle(Sender);
    end;
  finally
    if not tlbtnEdit.Down then
      MainTimerSwitch(True);
  end;
end; // ActionShowChangeProperties

function THomeF.NewDrawObject(AProdScreenType: TProdScreenType;
  ABigEffMeters: Boolean;  AObjectType: TObjectType;
  APlantCode, AWorkspotCode: String): PDrawObject;
var
  ADrawObject: PDrawObject;
begin
//  new(ADrawObject); // Use this for a record-variable.
  ADrawObject := PDrawObject.Create; // Use this for a class-variable.
//   TObjectType = (otImage, otLineVert, otLineHorz, otRectangle, otPuppetBox);

  ADrawObject.APlantCode := APlantCode; // PIM-114
  ADrawObject.AWorkspotCode := AWorkspotCode; // PIM-114

  ADrawObject.AVisible := True;

  if AObjectType = otPicture then
    ADrawObject.AWorkspotScale := 100; // 20014450.50 Needed on object-level

  if (AObjectType = otImage) then
  begin
    if (not ABigEffMeters) then
    begin
// PersonalScreen    
(*
      // Square
      // Normal square object with small eff. meter and red/yellow/green lights
      ADrawObject.ALightRed := TShape.Create(Application);
      with ADrawObject.ALightRed do
      begin
        Shape := stCircle;
        Brush.Color := clMyRed;
        Height := Trunc(LIGHT_SIZE * WorkspotScale / 100);
        Width := Trunc(LIGHT_SIZE * WorkspotScale / 100);
        Visible := False;
        Parent := pnlDraw;
      end;

      ADrawObject.ALightYellow := TShape.Create(Application);
      with ADrawObject.ALightYellow do
      begin
        Shape := stCircle;
        Brush.Color := clMyYellow;
        Height := Trunc(LIGHT_SIZE * WorkspotScale / 100);
        Width := Trunc(LIGHT_SIZE * WorkspotScale / 100);
        Visible := False;
        Parent := pnlDraw;
      end;

      ADrawObject.ALightGreen := TShape.Create(Application);
      with ADrawObject.ALightGreen do
      begin
        Shape := stCircle;
        Brush.Color := clMyGreen;
        Height := Trunc(LIGHT_SIZE * WorkspotScale / 100);
        Width := Trunc(LIGHT_SIZE * WorkspotScale / 100);
        Visible := False;
        Parent := pnlDraw;
      end;
*)
    end
    else
    begin
      //
    end; // Big Eff. Meter.
    // Big Eff. Meter.
    ADrawObject.ABigEffLabel := TLabel.Create(Application);
    if AProdScreenType <> pstWSEmpEff then // PIM-213
      with ADrawObject.ABigEffLabel do
      begin
        Parent := pnlDraw;
        Tag := NewItemIndex;
        OnClick := ImageClick;
        OnMouseDown := ImageMouseDown;
        OnMouseMove := ImageMouseMove;
        OnMouseUp := ImageMouseUp;
        Alignment := taLeftJustify;
      end;
    ADrawObject.AEffTotalsLabel := TLabel.Create(Application);
    with ADrawObject.AEffTotalsLabel do
    begin
      Tag := NewItemIndex;
      OnMouseDown := ImageMouseDown;
      OnMouseMove := ImageMouseMove;
      OnMouseUp := ImageMouseUp;
      Parent := pnlDraw;
      Visible := False;
    end;
    ADrawObject.AEffMeterRed := TShape.Create(Application);
    with ADrawObject.AEffMeterRed do
    begin
      Tag := NewItemIndex;
      Visible := False;
      Height := Trunc(8 * WorkspotScale / 100);
      Brush.Color := clMyRed;
      Pen.Color := clMyRed;
      OnMouseDown := ImageMouseDown;
      OnMouseMove := ImageMouseMove;
      OnMouseUp := ImageMouseUp;
// TShape has no OnClick-Event!
//    OnClick := ImageClick;
      Parent := pnlDraw;
    end;

    ADrawObject.AEffMeterGreen := TShape.Create(Application);
    with ADrawObject.AEffMeterGreen do
    begin
      Tag := NewItemIndex;
      Visible := False;
      Height := Trunc(8 * WorkspotScale / 100);
      Brush.Color := clMyGreen;
      Pen.Color := clMyGreen;
      OnMouseDown := ImageMouseDown;
      OnMouseMove := ImageMouseMove;
      OnMouseUp := ImageMouseUp;
      Parent := pnlDraw;
    end;

    ADrawObject.AProgressBar := TdxfProgressBar.Create(Application);
    with ADrawObject.AProgressBar do
    begin
      Tag := NewItemIndex;
      Visible := False;
      Height := Trunc(8 * WorkspotScale / 100);
      Max := 100;
      Min := 0;
      BeginColor := clMyRed;
      EndColor := clMyGreen;
      Font.Color := clBlack;
      Font.Style := [fsBold];
      ShowText := True;
      ShowTextStyle := stsPosition;
      Style := sExSolid;
      BevelOuter := bvNone;
      OnMouseDown := ImageMouseDown;
      OnMouseMove := ImageMouseMove;
      OnMouseUp := ImageMouseUp;
      Parent := pnlDraw;
    end;
  end; // if (AObjectType = otImage) then

  ADrawObject.ALabel := TLabel.Create(Application);
  if AProdScreenType <> pstWSEmpEff then // PIM-213
    with ADrawObject.ALabel do
    begin
      Parent := pnlDraw;
      Tag := NewItemIndex;
      OnClick := ImageClick;
      OnMouseDown := ImageMouseDown;
      OnMouseMove := ImageMouseMove;
      OnMouseUp := ImageMouseUp;
      Alignment := taLeftJustify;
      Visible := False;
    end;

  ADrawObject.AEffPercentage := 0;

  ADrawObject.ABorder := TShape.Create(Application);
  with ADrawObject.ABorder do
  begin
    Tag := NewItemIndex;
    Pen.Color := clGreen;
    Shape := stRectangle;
    Brush.Style := bsClear;
    OnMouseDown := ImageMouseDown;
    OnMouseMove := ImageMouseMove;
    OnMouseUp := ImageMouseUp;
    Parent := pnlDraw;
    Visible := False;
  end;

// PersonalScreen
  if (AObjectType in [otImage{, otPuppetBox}]) then
  begin
(*    ADrawObject.APuppetShiftList := TList.Create; *)
    ADrawObject.AEmployeeList := TList.Create;
    ADrawObject.AEmployeeDisplayList := TList.Create;
  end;

  ADrawObject.ABigEffMeters := False;

  ADrawObject.ABlinkPuppets := False;

  ADrawObject.ABlinkCompareJobs := False;

  ADrawObject.ANoData := False;

  ADrawObject.AProdScreenType := AProdScreenType;

  ADrawObject.AMachineWorkspotJobDifferent := False; // 20016447

  // TD-24819
  ADrawObject.ACompareJobList := TList.Create;

  ADrawObject.AGhostCountBlink := False; // PIM-151
  ADrawObject.ADefaultImage := nil; // PIM-151
  ADrawObject.AGhostImage := nil; // PIM-151
  
{  ADrawObject.APanel := nil; }

  if (AObjectType = otImage) then
  begin
    ADrawObject.AGhostImage := CreateGhostImage; // PIM-151
    // Can be: Personal Screen
    CreatePersonalScreenObject(AProdScreenType, ADrawObject);
  end; // if (AObjectType = otImage) then

  Result := ADrawObject;
end; // NewDrawObject

procedure THomeF.AddObjectToList(otType: TObjectType;
  AObject: TObject; APlantCode, AWorkspotCode, AWorkspotDescription,
  AImageName: String; ABigEffMeters: Boolean; AShowMode: Integer;
  AProdScreenType: TProdScreenType;
  AMachineType: TMachineType;
  AMachineCode, AMachineDescription: String;
  AWorkspotCounter: Integer);
var
  ADrawObject: PDrawObject;
begin
  Dirty := True;

  ADrawObject := NewDrawObject(AProdScreenType, ABigEffMeters, otType,
    APlantCode, AWorkspotCode); // PIM-114
  if otType = otImage then
  begin
    ADrawObject.AObject := CreateImage(AImageName, ABigEffMeters)
  end
  else
    ADrawObject.AObject := AObject;
  ADrawObject.AType := otType;

  // 20014450.50
  if otType = otPicture then
  begin
    (ADrawObject.AObject as TImage).Visible := True;
    ADrawObject.AHeight := (ADrawObject.AObject as TImage).Height;
    ADrawObject.AWidth := (ADrawObject.AObject as TImage).Width;
  end;

  // Personal Screen:
  // ProdScreenType: NoTimeRec, MachineTimeRec, WorkspotTimeRec
  ADrawObject.AProdScreenType := AProdScreenType;

  if AWorkspotCounter > 0 then
  begin
    // Personal Screen
    case ADrawObject.AProdScreenType of
    pstWorkspotTimeRec: // Workspot + Time Rec (With or without machine)
      begin
        if (ADrawObject.AObject is TImage) then
          (ADrawObject.AObject as TImage).Top :=
            (ADrawObject.AObject as TImage).Top +
              Round(ADrawObject.ABorder.Height * 2.2) * (AWorkspotCounter);
      end;
    end;
  end;

  // Plant
  ADrawObject.APlantCode := APlantCode;

  // Workspot
  ADrawObject.AWorkspotCode := AWorkspotCode;
  ADrawObject.AWorkspotDescription := AWorkspotDescription;

  //
  // Personal Screen - Start
  //
  // Label with description Machine or Workspot
  case ADrawObject.AProdScreenType of
  pstNoTimeRec: // No Time Rec: Workspot
    begin
      ADrawObject.ALabel.Caption :=
        DetermineWorkspotShortName(ADrawObject.APlantCode,
          ADrawObject.AWorkspotCode);
      ADrawObject.ALabel.Hint := AWorkspotDescription;
      ADrawObject.ABorder.Hint := AWorkspotDescription;
    end;
  pstMachineTimeRec: // Machine + Time Rec
    begin
      ADrawObject.AMachineTimeRec.AMachineCode := AMachineCode;
      ADrawObject.AMachineTimeRec.AMachineDescription := AMachineDescription;
      ADrawObject.ALabel.Caption :=
        ADrawObject.AMachineTimeRec.AMachineDescription;
      ADrawObject.ALabel.Hint := ADrawObject.ALabel.Caption;
      ADrawObject.ABorder.Hint := ADrawObject.ALabel.Caption;
      ADrawObject.AMachineTimeRec.AJobButton.Visible := True;
      ADrawObject.AMachineTimeRec.ACurrentJobCode := '';
      ADrawObject.AMachineTimeRec.ACurrentJobDescription := '';
      // 20014450
      ADrawObject.AMachineTimeRec.AStartTimeStamp := NullDate;
      ADrawObject.AMachineTimeRec.AShiftNumber := 1;
      ADrawObject.AMachineTimeRec.AShiftDate := Date;
      ADrawObject.AMachineTimeRec.ADepartmentCode :=
        PersonalScreenDM.MachineDepartment(APlantCode, AMachineCode);
      ADrawObject.AMachineTimeRec.AJobCount := 0;
      ADrawObject.AMachineTimeRec.ADown := False;
    end;
  pstWorkspotTimeRec: // Workspot + Time Rec (With or without machine)
    begin
      ADrawObject.AWorkspotTimeRec.AMachineCode := AMachineCode;
      ADrawObject.AWorkspotTimeRec.AMachineDescription := AMachineDescription;
      if ADrawObject.AWorkspotTimeRec.AMachineDescription = '' then
        ADrawObject.ALabel.Caption :=
          DetermineWorkspotShortName(ADrawObject.APlantCode,
            ADrawObject.AWorkspotCode)
      else
        ADrawObject.ALabel.Caption :=
          ADrawObject.AWorkspotTimeRec.AMachineDescription + '-' +
            DetermineWorkspotShortName(ADrawObject.APlantCode,
              ADrawObject.AWorkspotCode);
      ADrawObject.ALabel.Hint := ADrawObject.ALabel.Caption;
      ADrawObject.ABorder.Hint := ADrawObject.ALabel.Caption;
      ADrawObject.AWorkspotTimeRec.AJobButton.Visible := True;
      ADrawObject.AWorkspotTimeRec.AInButton.Visible := True;
{      ADrawObject.AWorkspotTimeRec.AOutButton.Visible := True; }
      ADrawObject.AWorkspotTimeRec.AModeButton.Visible := True;
      ADrawObject.AWorkspotTimeRec.APercentageLabel.Visible := True;
      ADrawObject.AWorkspotTimeRec.AActualNameLabel.Visible := True;
      ADrawObject.AWorkspotTimeRec.AActualValueLabel.Visible := True;
      ADrawObject.AWorkspotTimeRec.ATargetNameLabel.Visible := True;
      ADrawObject.AWorkspotTimeRec.ATargetValueLabel.Visible := True;
      ADrawObject.AWorkspotTimeRec.ADiffNameLabel.Visible := True;
      ADrawObject.AWorkspotTimeRec.ADiffValueLabel.Visible := True;
      ADrawObject.AWorkspotTimeRec.ADiffLineHorz.Visible := False;
      ADrawObject.AWorkspotTimeRec.ACurrentJobCode := '';
      ADrawObject.AWorkspotTimeRec.ACurrentJobDescription := '';
    end;
  pstWSEmpEff: // PIM-213
    begin
      ADrawObject.ApnlWSEmpEff :=
        WSEmpEffCreate(ADrawObject,
          DetermineWorkspotShortName(ADrawObject.APlantCode,
          ADrawObject.AWorkspotCode)
          );
    end;
  end;
  //
  // Personal Screen - End
  //
  
  // PersonalScreen
(*
  if (otType = otRectangle) or (otType = otPuppetBox) then
    ADrawObject.ADeptRect := CreateRectangle(ADrawObject);
*)
  // Show Hint
  ADrawObject.ALabel.ShowHint := True;
  ADrawObject.ALabel.ParentShowHint := False;
  // Set Workspotdescription as Hint to ABorder
  ADrawObject.ABorder.ShowHint := True;
  ADrawObject.ABorder.ParentShowHint := False;

  ADrawObject.AImageName := AImageName;
  ADrawObject.ABigEffMeters := ABigEffMeters;
  ADrawObject.AShowMode := TShowMode(AShowMode);

  ShowDrawObject(ADrawObject, ADrawObject.AObject);
  PSList.Add(ADrawObject);
  RenumberDrawObjects;
  MachineWorkspotsInit(PSList); // PIM-111
end; // AddObjectToList

procedure THomeF.DeleteDrawObjectFromList(ATag: Integer);
var
  I, ItemIndex: Integer;
  ADrawObject: PDrawObject;
  AnImage: TImage;
  AShapeEx: TShapeEx;
begin
  Dirty := True;

  ItemIndex := -1;
  ADrawObject := nil;
  for I := 0 to PSList.Count-1 do
  begin
    ADrawObject := PSList.Items[I];
    if ADrawObject.AObject is TImage then
    begin
      AnImage := (ADrawObject.AObject as TImage);
      if AnImage.Tag = ATag then
      begin
        AnImage.OnClick := nil;
        AnImage.OnDblClick := nil;
        AnImage.PopupMenu := nil;
        AnImage.OnMouseDown := nil;
        AnImage.OnMouseMove := nil;
        AnImage.OnMouseUp := nil;
        AnImage.Visible := False; // PIM-116
        AnImage.Free;
        ItemIndex := I;
        Break;
      end;
    end // if
    else
      if ADrawObject.AObject is TShapeEx then
      begin
        AShapeEx := (ADrawObject.AObject as TShapeEx);
        if AShapeEx.Tag = ATag then
        begin
          AShapeEx.OnClick := nil;
          AShapeEx.OnDblClick := nil;
          AShapeEx.OnMouseDown := nil;
          AShapeEx.OnMouseMove := nil;
          AShapeEx.OnMouseUp := nil;
          AShapeEx.Visible := False; // PIM-116
          AShapeEx.Free;
          ItemIndex := I;
          Break;
        end;
      end; // if
  end; // for
  if ItemIndex <> -1 then
  begin
    if Assigned(ADrawObject) then
    begin
      ADrawObject.AObject := nil;
      ADrawObject.AGhostImage := nil; // PIM-151
      if (ADrawObject.AType = otImage) then
      begin
        if Assigned(ADrawObject.AMachineTimeRec) then
        begin
          // PIM-116 First set On-events to nil.
          // Important: Also Set Visible to False or it gives an access
          //            violation during Free!
          with ADrawObject.AMachineTimeRec.AJobButton do
          begin
            OnClick := nil;
            OnMouseDown := nil;
            OnMouseMove := nil;
            OnMouseUp := nil;
            Visible := False;
          end;
          ADrawObject.AMachineTimeRec.AJobButton.Free;
          Dispose(ADrawObject.AMachineTimeRec);
        end; // if Assigned(ADrawObject.AMachineTimeRec) then
        if Assigned(ADrawObject.AWorkspotTimeRec) then
        begin
          // Personal screen
          ADrawObject.FreeLists;
          DeleteReadCounterRecForDrawObject(ReadCounterList, ADrawObject);

          // PIM-116 First set On-events to nil.
          // Important: Also Set Visible to False or it gives an access
          //            violation during Free!
          if Assigned(ADrawObject.AWorkspotTimeRec.AJobButton) then
            with ADrawObject.AWorkspotTimeRec.AJobButton do
            begin
              OnClick := nil;
              OnMouseDown := nil;
              OnMouseMove := nil;
              OnMouseUp := nil;
              Visible := False;
            end;
          if Assigned(ADrawObject.AWorkspotTimeRec.AInButton) then
            with ADrawObject.AWorkspotTimeRec.AInButton do
            begin
              OnClick := nil;
              OnMouseDown := nil;
              OnMouseMove := nil;
              OnMouseUp := nil;
              Visible := False;
            end;
          with ADrawObject.AWorkspotTimeRec.AModeButton do
          begin
            OnClick := nil;
            OnMouseDown := nil;
            OnMouseMove := nil;
            OnMouseUp := nil;
            Visible := False;
          end;
          with ADrawObject.AWorkspotTimeRec.APercentageLabel do
          begin
            OnMouseDown := nil;
            OnMouseMove := nil;
            OnMouseUp := nil;
            Visible := False;
          end;
          with ADrawObject.AWorkspotTimeRec.AActualNameLabel do
          begin
            OnMouseDown := nil;
            OnMouseMove := nil;
            OnMouseUp := nil;
            Visible := False;
          end;
          with ADrawObject.AWorkspotTimeRec.AActualValueLabel do
          begin
            OnMouseDown := nil;
            OnMouseMove := nil;
            OnMouseUp := nil;
            Visible := False;
          end;
          with ADrawObject.AWorkspotTimeRec.ATargetNameLabel do
          begin
            OnMouseDown := nil;
            OnMouseMove := nil;
            OnMouseUp := nil;
            Visible := False;
          end;
          with ADrawObject.AWorkspotTimeRec.ATargetValueLabel do
          begin
            OnMouseDown := nil;
            OnMouseMove := nil;
            OnMouseUp := nil;
            Visible := False;
          end;
          with ADrawObject.AWorkspotTimeRec.ADiffNameLabel do
          begin
            OnMouseDown := nil;
            OnMouseMove := nil;
            OnMouseUp := nil;
            Visible := False;
          end;
          with ADrawObject.AWorkspotTimeRec.ADiffValueLabel do
          begin
            OnMouseDown := nil;
            OnMouseMove := nil;
            OnMouseUp := nil;
            Visible := False;
          end;
          with ADrawObject.AWorkspotTimeRec.ADiffLineHorz do
          begin
            OnMouseDown := nil;
            OnMouseMove := nil;
            OnMouseUp := nil;
            Visible := False;
          end;

          if Assigned(ADrawObject.AWorkspotTimeRec.AJobButton) then
            ADrawObject.AWorkspotTimeRec.AJobButton.Free;
          if Assigned(ADrawObject.AWorkspotTimeRec.AInButton) then
            ADrawObject.AWorkspotTimeRec.AInButton.Free;
          ADrawObject.AWorkspotTimeRec.AModeButton.Free;
          ADrawObject.AWorkspotTimeRec.APercentageLabel.Free;
          ADrawObject.AWorkspotTimeRec.AActualNameLabel.Free;
          ADrawObject.AWorkspotTimeRec.AActualValueLabel.Free;
          ADrawObject.AWorkspotTimeRec.ATargetNameLabel.Free;
          ADrawObject.AWorkspotTimeRec.ATargetValueLabel.Free;
          ADrawObject.AWorkspotTimeRec.ADiffNameLabel.Free;
          ADrawObject.AWorkspotTimeRec.ADiffValueLabel.Free;
          ADrawObject.AWorkspotTimeRec.ADiffLineHorz.Free;

          Dispose(ADrawObject.AWorkspotTimeRec);
        end; // if Assigned(ADrawObject.AWorkspotTimeRec) then
        // PIM-213
        if (ADrawObject.AProdScreenType = pstWSEmpEff) then
        begin
          if Assigned(ADrawObject.ApnlWSEmpEff) then
          begin
            // ClearEmployeeList(ADrawObject);
            ADrawObject.InitEmployeeDisplayList(False);
            ADrawObject.ApnlWSEmpEff.Visible := False;
            ADrawObject.ApnlWSEmpEff.Free;
          end;
        end // if (ADrawObject.AProdScreenType = pstWSEmpEff) then
        else
        begin
          if ADrawObject.ABigEffMeters then
          begin
            ADrawObject.ABigEffLabel.Visible := False; // PIM-116
            ADrawObject.ABigEffLabel.Free;
          end;
          ADrawObject.AEffTotalsLabel.Visible := False; // PIM-116
          ADrawObject.AEffTotalsLabel.Free;
          ADrawObject.AEffMeterGreen.Visible := False; // PIM-116
          ADrawObject.AEffMeterGreen.Free;
          ADrawObject.AEffMeterRed.Visible := False; // PIM-116
          ADrawObject.AEffMeterRed.Free;
          // PIM-194
          if Assigned(ADrawObject.AProgressBar) then
          begin
            ADrawObject.AProgressBar.Visible := False;
            ADrawObject.AProgressBar.Free;
          end;
        end;
        ADrawObject.ALabel.Visible := False; // PIM-116
        ADrawObject.ALabel.Free;
        ADrawObject.ABorder.Visible := False; // PIM-116
        ADrawObject.ABorder.Free;
      end; // if (ADrawObject.AType = otImage) then
    end; // if Assigned(ADrawObject) then
    PSList.Delete(ItemIndex);
  end; // if ItemIndex <> -1 then
  RenumberDrawObjects;
end; // DeleteDrawObjectFromList

procedure THomeF.RenumberDrawObjects;
var
  I, J: Integer;
  AEmployeeDisplayRec: PTEmployeeDisplayRec;
  ADrawObject: PDrawObject;
  function PriorityCompare(Item1, Item2: Pointer): Integer;
  var
    ADrawObject1, ADrawObject2: PDrawObject;
  begin
    Result := 0;
    ADrawObject1 := PDrawObject(Item1);
    ADrawObject2 := PDrawObject(Item2);
    if ADrawObject1.APriority > ADrawObject2.APriority then
      Result := 1
    else
      if ADrawObject1.APriority < ADrawObject2.APriority then
        Result := -1;
  end;
begin
  for I := 0 to PSList.Count - 1 do
  begin
    ADrawObject := PSList.Items[I];
    ADrawObject.ALabel.Tag := I+1;
    if ADrawObject.AType = otImage then
      if ADrawObject.ABigEffMeters then
        ADrawObject.ABigEffLabel.Tag := I+1;
    ADrawObject.ABorder.Tag := I+1;
    if ADrawObject.AType = otImage then
    begin
      if ADrawObject.AProdScreenType = pstWSEmpEff then // PIM-213
        if Assigned(ADrawObject.ApnlWSEmpEff) then
          ADrawObject.ApnlWSEmpEff.Renumber(I+1);
      if Assigned(ADrawObject.AEffMeterGreen) then
        ADrawObject.AEffMeterGreen.Tag := I+1;
      if Assigned(ADrawObject.AEffMeterRed) then
        ADrawObject.AEffMeterRed.Tag := I+1;
      if Assigned(ADrawObject.AProgressBar) then // PIM-194
        ADrawObject.AProgressBar.Tag := I+1;
    end;
    if (ADrawObject.AObject is TImage) then
      (ADrawObject.AObject as TImage).Tag := I+1;
    if (ADrawObject.AObject is TShapeEx) then
      (ADrawObject.AObject as TShapeEx).Tag := I+1;
    if ADrawObject.AType = otImage then
      if Assigned(ADrawObject.AEffTotalsLabel) then
        ADrawObject.AEffTotalsLabel.Tag := I+1;
    if ADrawObject.AType = otImage then
      if ADrawObject.ABigEffMeters then
      begin
        //
        // Personal Screen - Start
        //
        if Assigned(ADrawObject.AGhostImage) then
          ADrawObject.AGhostImage.Tag := I+1; // PIM-151
        case ADrawObject.AProdScreenType of
        pstMachineTimerec:
          begin
            ADrawObject.AMachineTimeRec.AJobButton.Tag := I+1;
          end;
        pstWorkspotTimeRec:
          begin
            ADrawObject.AWorkspotTimeRec.AJobButton.Tag := I+1;
            ADrawObject.AWorkspotTimeRec.AInButton.Tag := I+1;
            ADrawObject.AWorkspotTimeRec.AModeButton.Tag := I+1;
            ADrawObject.AWorkspotTimeRec.APercentageLabel.Tag := I+1;
            ADrawObject.AWorkspotTimeRec.AActualNameLabel.Tag := I+1;
            ADrawObject.AWorkspotTimeRec.AActualValueLabel.Tag := I+1;
            ADrawObject.AWorkspotTimeRec.ATargetNameLabel.Tag := I+1;
            ADrawObject.AWorkspotTimeRec.ATargetValueLabel.Tag := I+1;
            ADrawObject.AWorkspotTimeRec.ADiffNameLabel.Tag := I+1;
            ADrawObject.AWorkspotTimeRec.ADiffValueLabel.Tag := I+1;
            // 20014450.50
            if Assigned(ADrawObject.AEmployeeDisplayList) then
            begin
              for J := 0 to ADrawObject.AEmployeeDisplayList.Count - 1 do
              begin
                AEmployeeDisplayRec :=
                  ADrawObject.AEmployeeDisplayList.Items[J];
                AEmployeeDisplayRec.AEmployeeNameLabel.Tag := I+1;
                AEmployeeDisplayRec.AWorkedMinutesLabel.Tag := I+1;
                AEmployeeDisplayRec.APercentagePanel.Tag := I+1;
                AEmployeeDisplayRec.AListButton.Tag := I+1;
                AEmployeeDisplayRec.AGraphButton.Tag := I+1;
              end;
            end;
          end;
        end;
        //
        // Personal Screen - End
        //
      end;
  end; // for
end; // RenumberDrawObjects

function THomeF.NewItemIndex: Integer;
var
  I, Max: Integer;
  ADrawObject: PDrawObject;
  AnImage: TImage;
  AShapeEx: TShapeEx;
begin
  Max := 0;
  for I:=0 to PSList.Count-1 do
  begin
    ADrawObject := PSList.Items[I];
    if (ADrawObject.AObject is TImage) then
    begin
      AnImage := (ADrawObject.AObject as TImage);
      if AnImage.Tag > Max then
        Max := AnImage.Tag;
    end
    else
      if (ADrawObject.AObject is TShapeEx) then
      begin
        AShapeEx := (ADrawObject.AObject as TShapeEx);
        if AShapeEx.Tag > Max then
          Max := AShapeEx.Tag;
      end;
  end;
  Result := Max + 1;
end; // NewItemIndex

procedure THomeF.FreeList;
var
  I: Integer;
  ADrawObject: PDrawObject;
begin
  for I := PSList.Count - 1  downto 0 do
  begin
    ADrawObject := PSList.Items[I];
    if (ADrawObject.AObject is TImage) then
      DeleteDrawObjectFromList((ADrawObject.AObject as TImage).Tag)
    else
      if (ADrawObject.AObject is TShapeEx) then
        DeleteDrawObjectFromList((ADrawObject.AObject as TShapeEx).Tag);
  end;
  ClearReadCounterList(ReadCounterList);
  PSList.Clear;
  PSList.Free;
end; // FreeList

procedure THomeF.SetCaption;
begin
  // MR:23-01-2008 RV002: Also show ComputerName.
  Caption := 'ORCL - ' + ORASystemDM.OracleSession.LogonDatabase + ' - ' +
    ThisCaption + ' - ' +
    ORASystemDM.CurrentComputerName + ' - ' +
    SchemePath + WorkFilename;
end; // SetCaption

function THomeF.DetermineWorkspotShortName(PlantCode,
  WorkspotCode: String): String;
begin
  Result := WorkspotCode;
  with ProductionScreenDM do
  begin
    with cdsWorkspotShortName do
    begin
      if FindKey([PlantCode, WorkspotCode]) then
        Result := FieldByName('SHORT_NAME').Value;
    end;
  end;
end; // DetermineWorkspotShortName

// PersonalScreen
(*
function THomeF.DetermineDepartmentDescription(PlantCode,
  DepartmentCode: String): String;
var
  WasActive: Boolean;
begin
  Result := DepartmentCode;
  with ProductionScreenDM do
  begin
    with odsDepartment do
    begin
      WasActive := Active;
      if not WasActive then
        Active := True;
      try
        if Locate('PLANT_CODE;DEPARTMENT_CODE',
          VarArrayOf([PlantCode, DepartmentCode]), []) then
          Result := FieldByName('DESCRIPTION').Value;
      except
        Result := DepartmentCode;
      end;
      if not WasActive then
        Active := False;
    end;
  end;
end; // DetermineDepartmentDescription
*)

procedure THomeF.MyActionNew;
begin
  MyAction := nil;

  ActionEditOn; // This will turn the timers off and write to file/db.
  try
    try
      AskForSave;
      FreeList;
      PSList := TList.Create;
      Dirty := False;
      Workfilename := DEFAULT_WORKFILENAME;
      SetCaption;
    except
    end;
  finally
  end;
end; // MyActionNew

// Show 'Ask-for-save'-dialog when current scheme was changed
// but not saved. After that: Clear the screen/scheme, so
// user can make a new scheme.
procedure THomeF.actNewExecute(Sender: TObject);
begin
  inherited;
//  NewNow := True;
  if not IsBusy then
    MyActionNew
  else
    MyAction := MyActionNew;
end; // actNewExecute

(*
// Search For Last (in DateTime) ProductionQuantity Record
function THomeF.SearchLastProductionQuantityRecord(
  ADrawObject: PDrawObject;
  var Quantity: Double): Boolean;
var
  ADate, DateFrom, DateTo: TDateTime;
begin
  Result := False;
  Quantity := 0;
  ADate := Now;

  // DateFrom must be n seconds before PQDate.
//  DateFrom := DateTimeBefore(ADate, PIMSTimerInterval);
//  DateTo := ADate;
  with ProductionScreenDM do
  begin
    with odsProductionQuantity do
    begin
//      DateFrom := Trunc(ADate) - 30;
      DateFrom := ADate -
        (PIMSDatacolTimeInterval * PIMSMultiplier / 60 / 24 / 60);
      DateTo := ADate;
      // Now search for all records in given period
      Close;
      ClearVariables;
      SetVariable('PLANT_CODE',    ADrawObject.APlantCode);
      SetVariable('WORKSPOT_CODE', ADrawObject.AWorkspotCode);
      SetVariable('DATEFROM',      DateFrom);
      SetVariable('DATETO',        DateTo);
      Open;
      Refresh;
      if RecordCount > 0 then
      begin
        Last;
        Result := True;
        Quantity := FieldByName('QUANTITYSUM').Value;
      end;
      Close;
    end;
  end;
end;
*)

(*
procedure THomeF.ShowIndicators(ADrawObject: PDrawObject;
  ARed, AYellow, AGreen: TColor);
begin
  ADrawObject.ALightRed.Brush.Color := ARed;
  ADrawObject.ALightYellow.Brush.Color := AYellow;
  ADrawObject.ALightGreen.Brush.Color := AGreen;
end; // ShowIndicators
*)

(*
procedure THomeF.ActionConnectionIndicators(ADrawObject: PDrawObject);
var
  Quantity: Double;
begin
  // ConnectionIndicator
  if SearchLastProductionQuantityRecord(ADrawObject, Quantity) then
  begin
    if Quantity = 0 then
      // Yellow and Green light on
      ShowIndicators(ADrawObject, clWhite, clMyYellow, clMyGreen)
    else
      // Green light on
      ShowIndicators(ADrawObject, clWhite, clWhite, clMyGreen);
  end
  else
    // Red light on
    ShowIndicators(ADrawObject, clMyRed, clWhite, clWhite);
end; // ActionConnectionIndicators
*)

// PIM-213
(*
procedure THomeF.ClearEmployeeList(ADrawObject: PDrawObject);
var
  APTAEmployee: PTAEmployee;
  I: Integer;
begin
  if Assigned(ADrawObject.AEmployeeList) then
  begin
    for I := ADrawObject.AEmployeeList.Count - 1  downto 0 do
    begin
      APTAEmployee := ADrawObject.AEmployeeList.Items[I];
      if Assigned(APTAEmployee.AEmployeeNameLabel) then
      begin
        APTAEmployee.AEmployeeNameLabel.Visible := False;
        APTAEmployee.AEmployeeNameLabel.Free;
      end;
      ADrawObject.AEmployeeList.Remove(APTAEmployee);
      Dispose(APTAEmployee);
    end;
  end;
end;
*)
(*
function THomeF.FindEmployee(
  AEmployeeList: TList; AEmployeeNumber: Integer): Boolean;
var
  APTTempEmployee: PTTempEmployee;
  I: Integer;
begin
  Result := False;
  for I:=0 to AEmployeeList.Count-1 do
  begin
    APTTempEmployee := AEmployeeList.Items[I];
    if APTTempEmployee.AEmployeeNumber = AEmployeeNumber then
    begin
      Result := True;
      Break;
    end;
  end;
end;
*)

(*
procedure THomeF.DeleteEmployeeList(
  var AEmployeeList: TList);
var
  APTTempEmployee: PTTempEmployee;
  I: Integer;
begin
  for I := AEmployeeList.Count - 1 downto 0 do
  begin
    APTTempEmployee := AEmployeeList.Items[I];
    Dispose(APTTempEmployee);
    AEmployeeList.Remove(APTTempEmployee);
  end;
  AEmployeeList.Free;
end;
*)

function EmployeeCompare(Item1, Item2: Pointer): Integer;
var
  Employee1, Employee2: PTEmployee;
begin
  Result := 0;
  Employee1 := Item1;
  Employee2 := Item2;
  if (Employee1.AEmployeeNumber < Employee2.AEmployeeNumber) then
    Result := -1
  else
    if (Employee1.AEmployeeNumber > Employee2.AEmployeeNumber) then
      Result := 1;
end;

(*
procedure THomeF.MainActionForTimerXXX;
var
  ADrawObject: PDrawObject;
  LastDrawObject: PDrawObject;
  I: Integer;
  MyNow: TDateTime;
begin
  try
    // Check connection and reconnect if not connected.
    ORASystemDM.OracleSession.CheckConnection(True);
    if not ORASystemDM.OracleSession.Connected then
      Exit;
  except
    //
  end;

  MyNow := Now;
  LastDrawObject := nil;
  ShowMsg('');

  try
    Busy := True;
    MainTimerSwitch(False);

    // Create lists for planned/scanned employees
    PlanScanEmpDM.CurrentNow := MyNow;
    APlanScanClass.OpenEmployee;
    APlanScanClass.OpenEmployeePlanning(MyNow);
    APlanScanClass.OpenTimeRegScanning(MyNow);
    APlanScanClass.CreateWorkspotsPerEmployeeList(MyNow);
    ActionCompareJobsInit(MyNow);
    TimerStartTime := SysUtils.Now;
    LastDrawObject := CurrentDrawObject;
    ShowActionTime;
    for I:=0 to PSList.Count - 1 do
    begin
      Application.ProcessMessages;
      ADrawObject := PSList.Items[I];
      if (ADrawObject.AObject is TImage) then
      begin
        with ProductionScreenDM do
        begin
          ShowSelectionRectangle(ADrawObject.AObject);
          Update;
          ShowActionTime;
{
          if not ADrawObject.ABigEffMeters then
          begin
            ShowActionTime;
            // Show Connection Indicators
            ActionConnectionIndicators(ADrawObject);
          end;
}
          // Calculate Efficiency
//          ActionEfficiency(ADrawObject);
          // Determine Employees/Puppets that are planned/scanned today
// PersonalScreen
{
          if not ADrawObject.ABigEffMeters then
          begin
            ActionPuppets(ADrawObject);
          end;
          // Decide if puppets should blink:
          // There is production, but no employees scanned in for
          // the jobs.
          ActionBlinkPuppets;
}
          // Decide if there are jobs to compare
          ActionCompareJobs(ADrawObject, MyNow);

          // Create a EmployeeList for BIG-EFF-METERS
{        CreateEmployeeList(ADrawObject); }
        end;
      end
      else
      begin
// PersonalScreen
{
        if ADrawObject.AType = otPuppetBox then
          ActionPuppetBox(ADrawObject);
}
      end;
    end;
  finally
    TimerEndTime := SysUtils.Now;
    if LastDrawObject <> nil then
      ShowSelectionRectangle(LastDrawObject.AObject);
    ShowActionTime;
    if not tlbtnEdit.Down then
      MainTimerSwitch(True);
    Busy := False;
  end;
end; // MainActionForTimerXXX
*)

procedure THomeF.TimerMainTimer(Sender: TObject);
begin
  inherited;
  if not tlbtnEdit.Down then
    MainActionForTimer;
end;

procedure THomeF.FormShow(Sender: TObject);
begin
  inherited;
  ShowMsg('');

  ToggleNameJob := True; // PIM-147

  // Set Button Tool Bars to Edit/Non-Edit
  actEditExecute(Sender);

  UseSoundAlarm := False;
  SoundFilename := 'success.wav';
  ReadRegistry;

  // 20014722
  if ExportJobChange then
    ReadExportJobChangeINIFile;

  // TD-21191
  TimerWaitForRead.Interval := ReadTimeout;

  // CAR User rights  10-10-2003
  if DialogLoginF.AplEditYN = UNCHECKEDVALUE then
  begin
    actEdit.Enabled := False;
    actNew.Enabled := False;
    actSave.Enabled := False;
    actSaveAs.Enabled := False;
    FileSettingsAct.Enabled := False;
  end;
  // Timer to write to DB
  TimerWriteToDB.Interval := WriteToDBInterval * 60000;
  TimerWriteToDB.Enabled := True;

  // Timer to write to File
  TimerWriteToFile.Enabled := True;

  TimerBlink.Enabled := True; // PIM-147
  
  GlobalDM.Debug := Debug; // PIM-87
end; // FormShow

procedure THomeF.ReadINIFile;
var
  Ini: TIniFile;
  Section: String;
  LastScheme: String;
begin
  if not FileExists(PathCheck(AppRootPath) + ProdScreenINIFilename) then
    WriteINIFile;
  Ini := TIniFile.Create(PathCheck(AppRootPath) + ProdScreenINIFilename);
  try
    Section :=  ORASystemDM.CurrentComputerName;
    LastScheme := WorkFileName;
    LastScheme := Ini.ReadString(Section, 'LastScheme', LastScheme);
    UseSoundAlarm := Ini.ReadBool(Section, 'UseSoundAlarm', UseSoundAlarm);
    SoundFilename := Ini.ReadString(Section, 'SoundFilename', SoundFilename);
    ExportSeparator := Ini.ReadString(Section, 'ExportSeparator',
      ExportSeparator);
    // This is a global workspotscale, it will also
    // be saved for each file.
    WorkspotScale := Ini.ReadInteger(Section, 'WorkspotScale', WorkspotScale);
    FontScale := Ini.ReadInteger(Section, 'FontScale', FontScale);
    RefreshTimeInterval := Ini.ReadInteger(Section, 'RefreshTimeInterval',
      RefreshTimeInterval);
    // Personal Screen
    TimerReadCounter.Interval := Ini.ReadInteger(Section, 'DatacolInterval',
      TimerReadCounter.Interval);
    WriteToDBInterval := Ini.ReadInteger(Section, 'UpdateDBInterval',
      WriteToDBInterval);
    TimerWriteToDB.Interval := WriteToDBInterval * 60000;
    ReadDelay := Ini.ReadInteger(Section, 'ReadDelay', ReadDelay);
    RefreshEmployees := Ini.ReadBool(Section, 'RefreshEmployees',
      RefreshEmployees);
    // TD-21191
    ReadTimeout := Ini.ReadInteger(Section, 'ReadTimeout', ReadTimeout);
    // 20014722
    if Ini.ValueExists(Section, 'ExportJobChange') then
    begin
      ExportJobChange :=
        Ini.ReadBool(Section, 'ExportJobChange', ExportJobChange);
    end;
    // 20014826
    if Ini.ValueExists(Section, 'Debug') then
    begin
      Debug := Ini.ReadBool(Section, 'Debug', Debug);
    end;
    // PIM-90
    if Ini.ValueExists(Section, 'ReadExternalDataAtSeconds') then
      ReadExtDataAtSeconds := Ini.ReadInteger(Section,
        'ReadExternalDataAtSeconds', ReadExtDataAtSeconds);

//    ASocketPort.ReadDelay := ReadDelay;
{
    try
      EffQuantityTime := TEffQuantityTime(
        Ini.ReadInteger(Section, 'EffQuantityTime', Integer(EffQuantityTime)));
    except
      EffQuantityTime := efQuantity;
    end;
}

    // TD-24736.60
    if Ini.ValueExists(Section, 'DateTimeFormatPatch') then
      PersonalScreenDM.DateTimeFormatPatch := Ini.ReadBool(Section,
        'DateTimeFormatPatch', PersonalScreenDM.DateTimeFormatPatch);

    if LastScheme <> '' then
      WorkFileName := ExtractFileName(LastScheme);
  finally
    Ini.Free;
  end;
end;  // ReadINIFile

procedure THomeF.WriteINIFile;
var
  Ini: TIniFile;
  Section: String;
begin
  Ini := TIniFile.Create(PathCheck(AppRootPath) + ProdScreenINIFilename);
  try
    Section :=  ORASystemDM.CurrentComputerName;
    Ini.WriteString(Section, 'LastScheme', ExtractFileName(WorkFilename));
    Ini.WriteBool(Section, 'UseSoundAlarm', UseSoundAlarm);
    Ini.WriteString(Section, 'SoundFilename', SoundFilename);
    Ini.WriteString(Section, 'ExportSeparator', ExportSeparator);
    // This is a global workspotscale, it will also
    // be saved for each file.
    Ini.WriteInteger(Section, 'WorkspotScale', WorkspotScale);
    Ini.WriteInteger(Section, 'FontScale', FontScale);
    Ini.WriteInteger(Section, 'RefreshTimeInterval', RefreshTimeInterval);
    // Personal Screen
    Ini.WriteInteger(Section, 'DatacolInterval', TimerReadCounter.Interval);
//    Ini.WriteInteger(Section, 'EffQuantityTime', Integer(EffQuantityTime));
    Ini.WriteInteger(Section, 'UpdateDBInterval', WriteToDBInterval);
    Ini.WriteInteger(Section, 'ReadDelay', ReadDelay);
    Ini.WriteBool(Section, 'RefreshEmployees', RefreshEmployees);
    Ini.WriteInteger(Section, 'ReadTimeout', ReadTimeout);
    // TD-24736.60
    Ini.WriteBool(Section, 'DateTimeFormatPatch',
      PersonalScreenDM.DateTimeFormatPatch);
    // 20014826
    Ini.WriteBool(Section, 'Debug', Debug);
    // PIM-90
    Ini.WriteInteger(Section, 'ReadExternalDataAtSeconds', ReadExtDataAtSeconds);
  finally
    Ini.Free;
  end;
end; // WriteINIFile

procedure THomeF.ReadRegistry;
(*
var
  Reg: TRegistry;
  LastScheme: String;
*)
begin
  try
    ReadINIFile;
  except
    // Ignore error
  end;
(*
  Reg := TRegistry.Create;
  try
    Reg.RootKey := HKEY_LOCAL_MACHINE;
    if Reg.OpenKey('\Software\ABS\Pims', True) then
    begin
      LastScheme := Reg.ReadString('LastScheme');
      UseSoundAlarm := Reg.ReadBool('UseSoundAlarm');
      SoundFilename := Reg.ReadString('SoundFilename');
      ExportSeparator := Reg.ReadString('ExportSeparator');
      // This is a global workspotscale, it will also
      // be saved for each file.
      WorkspotScale := Reg.ReadInteger('WorkspotScale');
      FontScale := Reg.ReadInteger('FontScale');
      RefreshTimeInterval := Reg.ReadInteger('RefreshTimeInterval');
    end;
    if LastScheme <> '' then
      WorkFileName := ExtractFileName(LastScheme);
  finally
    Reg.CloseKey;
    Reg.Free;
  end;
*)
end;

procedure THomeF.WriteRegistry;
(*
var
  Reg: TRegistry;
*)
begin
  try
    WriteINIFile;
  except
    // Ignore error
  end;
(*
  Reg := TRegistry.Create;
  try
    Reg.RootKey := HKEY_LOCAL_MACHINE;
    if Reg.OpenKey('\Software\ABS\Pims', True) then
    begin
      Reg.WriteString('LastScheme', ExtractFileName(WorkFilename));
      Reg.WriteBool('UseSoundAlarm', UseSoundAlarm);
      Reg.WriteString('SoundFilename', SoundFilename);
      Reg.WriteString('ExportSeparator', ExportSeparator);
      // This is a global workspotscale, it will also
      // be saved for each file.
      Reg.WriteInteger('WorkspotScale', WorkspotScale);
      Reg.WriteInteger('FontScale', FontScale);
      Reg.WriteInteger('RefreshTimeInterval', RefreshTimeInterval);
    end;
  finally
    Reg.CloseKey;
    Reg.Free;
  end;
*)
end;

// PersonalScreen
// Show a dialog with employees, based on 'ShowMode' the list
// can show different employees, like 'show all', 'show too late',
// 'show absent with reason', etcetera.
procedure THomeF.ShowEmployees(const AShowMode: TShowMode);
begin
  if AShowMode = ShowEmployeesOnWorkspot then // Workspot-specific
    if CurrentDrawObject = nil then
      Exit;

//  DialogShowAllEmployeesF := TDialogShowAllEmployeesF.Create(Application);
  if AShowMode = ShowEmployeesOnWorkspot then
  begin
    try
      DialogShowAllEmployeesF.MyTitle :=
        PopupWorkspotMenu.Items[1].Caption;
    except
      DialogShowAllEmployeesF.MyTitle := '';
    end;
    DialogShowAllEmployeesF.PlantCode := CurrentDrawObject.APlantCode;
    DialogShowAllEmployeesF.WorkspotCode := CurrentDrawObject.AWorkspotCode;
    DialogShowAllEmployeesF.WorkspotDescription :=
      CurrentDrawObject.AWorkspotDescription;
    DialogShowAllEmployeesF.MyDrawObject := CurrentDrawObject;
  end
  else
  begin
    try
      DialogShowAllEmployeesF.MyTitle :=
        PopupMainMenu.Items[Integer(AShowMode) - 1].Caption;
    except
      DialogShowAllEmployeesF.MyTitle := '';
    end;
  end;

  try
    // PIM12.2
//    if not tlbtnEdit.Down then
//      MainTimerSwitch(False);
    DialogShowAllEmployeesF.ShowMode := AShowMode;
    // TD-21429
    RepositionSubForm(DialogShowAllEmployeesF);
    DialogShowAllEmployeesF.ShowModal;
  finally
  // PIM12.2
//    if not tlbtnEdit.Down then
//      MainTimerSwitch(True);
  end;
end; // ShowEmployees

procedure THomeF.ShowAllEmployees1Click(Sender: TObject);
begin
  inherited;
//  ShowEmployees(ShowAllEmployees);
end;

procedure THomeF.ShowEmployeesonwrongWokspot1Click(Sender: TObject);
begin
  inherited;
//  ShowEmployees(ShowEmployeesOnWrongWorkspot);
end;

procedure THomeF.ShowEmployeesnotscannedin1Click(Sender: TObject);
begin
  inherited;
//  ShowEmployees(ShowEmployeesNotScannedIn);
end;

procedure THomeF.ShowEmployeesabsentwithreason1Click(Sender: TObject);
begin
  inherited;
//  ShowEmployees(ShowEmployeesAbsentWithReason);
end;

procedure THomeF.ShowEmployeesabsentwithoutreason1Click(Sender: TObject);
begin
  inherited;
//  ShowEmployees(ShowEmployeesAbsentWithoutReason);
end;

procedure THomeF.ShowEmployeeswithFirstAid1Click(Sender: TObject);
begin
  inherited;
//  ShowEmployees(ShowEmployeesWithFirstAid);
end;

procedure THomeF.FileExitActExecute(Sender: TObject);
begin
  inherited;
  AskForSave;
end;

// Blink the red Efficiency-meter
// Blink the Puppets if necessary
procedure THomeF.TimerBlinkTimer(Sender: TObject);
var
  MainI: Integer;
  ADrawObject: PDrawObject;
// PupI, PupJ, PupK: Integer;
// PersonalScreen
{
  APuppetShift: PTPuppetShift;
  APuppet: PTPuppet;
}
  procedure ShowGhostCountBlink;
  begin
    try
      // PIM-194 Related to this order, do not show ghost-image when someone
      //         is scanned in.
      if ADrawObject.ACurrentNumberOfEmployees > 0 then
      begin
        if Assigned(ADrawObject.AGhostImage) then
          ADrawObject.AGhostImage.Visible := False;
        Exit;
      end;
      // PIM-151
      // Show and blink Ghost if needed
      if (ADrawObject.AObject is TImage) then
      begin
        ActionGhostCountCheck(ADrawObject);
        if not ADrawObject.AGhostCountBlink then
          ActionGhostCountCheckDB(ADrawObject);
        // Toggle between Ghost and default image
        if ADrawObject.AGhostCountBlink then
        begin
          // For BigEffMeter: Do not assign it to AObject, only make it
          // visible or not.
          if ADrawObject.ABigEffMeters then
          begin
            if not ADrawObject.AGhostImage.Visible then
            begin
              // Show Ghost
              ADrawObject.AGhostImage.Top := ADrawObject.ABorder.Top;
              ADrawObject.AGhostImage.Left :=
                ADrawObject.ABorder.Left +
                  Round(ADrawObject.ABorder.Width / 2 - ADrawObject.ABorder.Height / 2);
              ADrawObject.AGhostImage.Width := ADrawObject.ABorder.Height;
              ADrawObject.AGhostImage.Height := ADrawObject.ABorder.Height;
              ADrawObject.AGhostImage.Visible := True;
            end
            else
            begin
              // Do not show ghost
              ADrawObject.AGhostImage.Visible := False;
            end;
          end;
        end // if ADrawObject.AGhostCountBlink
        else
        begin
          // Do not show ghost
          if ADrawObject.ABigEffMeters then
            ADrawObject.AGhostImage.Visible := False;
        end;
        Application.ProcessMessages;
      end; // if (ADrawObject.AObject is TImage)
    finally
    end;
  end; // ShowGhostCounterBlink;
begin
  inherited;
  // PIM-147
  if not tlbtnEdit.Down then
  begin
    ToggleNameJob := not ToggleNameJob;

    for MainI := 0 to PSList.Count - 1 do
    begin
      ADrawObject := PSList.Items[MainI];
      ShowGhostCountBlink;
    end; // for
  end; // if
{
  if not tlbtnEdit.Down then
  begin
    for MainI := 0 to PSList.Count - 1 do
    begin
      ADrawObject := PSList.Items[MainI];
      // Blink EffMeter if needed
      if ADrawObject.AType = otImage then
      begin
        if ADrawObject.AEffMeterRed.Visible then
        begin
          if ADrawObject.AEffMeterRed.Brush.Color = clMyRed then
            ADrawObject.AEffMeterRed.Brush.Color := clWhite
          else
            ADrawObject.AEffMeterRed.Brush.Color := clMyRed;
        end;
      end;
}
// PersonalScreen
{
      // Blink Puppets if needed
      if ADrawObject.ABlinkPuppets then
      begin
        if ADrawObject.APuppetShiftList.Count > 0 then
        begin
          for PupI := 0 to ADrawObject.APuppetShiftList.Count - 1 do
          begin
            APuppetShift := ADrawObject.APuppetShiftList.Items[PupI];
            if APuppetShift.APuppetList.Count > 0 then
            begin
              for PupJ := 0 to APuppetShift.APuppetList.Count - 1 do
              begin
                APuppet := APuppetShift.APuppetList.Items[PupJ];
                for PupK := 0 to MAX_PUPPET_PARTS do
                begin
                  if APuppet.Puppet[PupK].Brush.Color = APuppet.AColor then
                  begin
                    APuppet.Puppet[PupK].Brush.Color := clWhite;
                    APuppet.Puppet[PupK].Pen.Color := clWhite;
                  end
                  else
                  begin
                    APuppet.Puppet[PupK].Brush.Color := APuppet.AColor;
                    APuppet.Puppet[PupK].Pen.Color := APuppet.AColor;
                  end;
                end;
              end;
            end;
          end;
        end;
      end;
}
{
      // Blink border if needed
      if ADrawObject.ABlinkCompareJobs then
      begin
        ADrawObject.ABorder.Pen.Width := 3; // Somewhat thicker
        if ADrawObject.ABorder.Pen.Color = clGreen then
          ADrawObject.ABorder.Pen.Color := clRed
        else
          ADrawObject.ABorder.Pen.Color := clGreen;
      end
      else
      begin
        ADrawObject.ABorder.Pen.Width := 1;
        ADrawObject.ABorder.Pen.Color := clGreen;
      end;
    end;
  end;
}
end; // TimerBlinkTimer

procedure THomeF.ShowChartClick(Sender: TObject);
var
  NewEfficiencyPeriodMode: TEffPeriodMode;
begin
  inherited;
  // PersonalScreen
//  Exit; // 20014450.50 Show a chart

  if CurrentDrawObject = nil then
    Exit;
  try
  // PIM-12.2
//    if not tlbtnEdit.Down then
//      MainTimerSwitch(False);

//    NewEfficiencyPeriodMode := ActionEfficiency(CurrentDrawObject);
    NewEfficiencyPeriodMode := epCurrent;

    // This must already be created in project!
    DialogEmpGraphF.EmployeeNumber := CurrentDrawObject.AListEmployeeNumber;
    DialogEmpGraphF.EmployeeName :=
      CurrentDrawObject.DetermineEmployeeName(CurrentDrawObject.AListEmployeeNumber);
    DialogEmpGraphF.DatacolTimeInterval := PIMSDatacolTimeInterval;
    DialogEmpGraphF.PlantCode := CurrentDrawObject.APlantCode;
    DialogEmpGraphF.WorkspotCode := CurrentDrawObject.AWorkspotCode;
    DialogEmpGraphF.WorkspotDescription :=
      CurrentDrawObject.AWorkspotDescription;
    DialogEmpGraphF.MyExportPath := ExportPath;
    DialogEmpGraphF.MyExportSeparator := ExportSeparator;
    DialogEmpGraphF.RefreshTimeInterval := RefreshTimeInterval;
//    DialogEmpGraphF.EfficiencyPeriodMode := NewEfficiencyPeriodMode;
    DialogEmpGraphF.EfficiencyPeriodSince := EfficiencyPeriodSince;
    DialogEmpGraphF.EfficiencyPeriodSinceTime := GlobalEfficiencyPeriodSinceTime;
    //
    // Personal Screen - Start
    //
    if CurrentDrawObject.AProdScreenType = pstWorkspotTimeRec then
    begin
      if NewEfficiencyPeriodMode = epShift then
      begin
        DialogEmpGraphF.EfficiencyPeriodShiftStart :=
          CurrentDrawObject.AWorkspotTimeRec.ACurrentShiftStart;
        DialogEmpGraphF.EfficiencyPeriodSinceTime :=
          CurrentDrawObject.AWorkspotTimeRec.ACurrentShiftStart;
      end;
    end;
    //
    // Personal Screen - End
    //
    DialogEmpGraphF.TotalPercentage := CurrentDrawObject.ATotalPercentage;
    DialogEmpGraphF.ShowModal;
    ExportSeparator := DialogEmpGraphF.MyExportSeparator;
  finally
//    DialogEmpGraphF.Free; // Do not free this!
  // PIM12.2
//    if not tlbtnEdit.Down then
//      MainTimerSwitch(True);
  end;
end; // ShowChartClick

procedure THomeF.PlaySound;
begin
  if not UseSoundAlarm then
    Exit;
  if FileExists(SoundPath + SoundFilename) then
  begin
    with MediaPlayer1 do
    begin
      Filename := SoundPath + SoundFilename;
      DeviceType := dtWaveAudio;
      Open;
      Play;
    end;
  end;
end; // PlaySound

procedure THomeF.actFileSettingsExecute(Sender: TObject);
begin
  if not IsBusy then
    MyActionFileSettings
  else
    MyAction := MyActionFileSettings;
end; // actFileSettingsExecute

procedure THomeF.MainTimerSwitch(OnOff: Boolean; AAll: Boolean=False);
begin
  // MRA:27-AUG-2012 Only turn this timer off, never on!
  if not OnOff then
  begin
    TimerWaitForRead.Enabled := OnOff;
    Busy := False;
  end;

  TimerMain.Enabled := OnOff;
  TimerReadCounter.Enabled := OnOff;
  // Do not reset these 2 timers: They are
  // autom. switched when 'readcounter' is activated.
{
  if tlbnEdit.Down then
  begin
    TimerWriteToDB.Enabled := OnOff;
    TimerWriteToFile.Enabled := OnOff;
  end;
}
  // TODO 21191
  // For certain actions like 'OpenList' and 'Refresh' the
  // following timers MUST be turned OFF!
  if AAll then
  begin
    TimerWriteToDB.Enabled := OnOff;
    TimerWriteToFile.Enabled := OnOff;
  end;

  if TimerMain.Enabled then
    TimerOnTime := SysUtils.Now;
end; // MainTimerSwitch

function ZeroFill(Str: String; Len: Integer): String;
begin
  while Length(Str) < Len do
    Str := '0' + Str;
  Result := Str;
end; // ZeroFill

procedure THomeF.TimerStatusTimer(Sender: TObject);
const
  SecsADay = 86400;
var
  DurationInSec, Min, Sec: Cardinal;
  Hrs, Mins, Secs, MSecs: Word;
  TimeStr: String;
  MyNow: TDateTime;
begin
  inherited;
  MyNow := Now;
  if TimerMain.Enabled then
  begin
    DurationInSec := Trunc((SysUtils.Now - TimerOnTime) * SecsADay);
    Min := DurationInSec MOD 3600 DIV 60;
    Sec := DurationInSec MOD 3600 MOD 60 MOD 60;
    TimeStr :=
      ZeroFill(IntToStr(Min), 2) + ':' +
      ZeroFill(IntToStr(Sec), 2); // + ':' +
  end;
  if tlbtnEdit.Down then
    Exit;
  stBarbase.Panels[5].Text := DateTimeToStr(MyNow);
  if DialogShowAllEmployeesF <> nil then
    DialogShowAllEmployeesF.stbarBase.Panels[2].Text := stBarbase.Panels[5].Text;
  DecodeTime(MyNow, Hrs, Mins, Secs, MSecs);
  // 20014450.50
  if (Secs <= 5) then
  begin
    if Trunc((MyNow - LastTimeInsertWorkspotPerMinute) * SecsADay) > 60 then
    begin
      if not IsBusy then
        ActionRealTimeEfficiency
      else
        MyAction := MyActionRealTimeEfficiency;
    end;
  end;

  // PIM-90
  if ReadExternalData then
    if (Secs >= ReadExtDataAtSeconds) and (Secs <= ReadExtDataAtseconds + 10) then
    begin
      if Trunc((MyNow - LastTimeReadExtData) * SecsADay) > 60 then
      begin
        if not IsBusy then
          DataReadAllForOneDrawObject
        else
          MyAction := MyActionReadExternalData;
      end;
    end;
end; // TimerStatusTimer

// PersonalScreen
procedure THomeF.ShowEmployeestoolate1Click(Sender: TObject);
begin
  inherited;
//  ShowEmployees(ShowEmployeesTooLate);
end; // ShowEmployeestoolate1Click

function EmpCompare(Item1, Item2: Pointer): Integer;
var
  Employee1, Employee2: PTEmployee;
begin
  Result := 0;
  Employee1 := Item1;
  Employee2 := Item2;
  if (Employee1.AEmployeeNumber < Employee2.AEmployeeNumber) then
    Result := -1
  else
    if (Employee1.AEmployeeNumber > Employee2.AEmployeeNumber) then
      Result := 1;
end; // EmpCompare

function THomeF.DetermineEmployeeList(
  ADrawObject: PDrawObject;
  const AShowMode: TShowMode;
  const APlantCode: String;
  const AWorkspotCode: String;
  var AEmployeeList: TList;
  var AEmployeeCount: Integer): Boolean;
var
  MyNow: TDateTime;
  I: Integer;
  APTEmployee: PTEmployee;
  // PersonalScreen
{
  AEmployee: TEmployee;
  J: Integer;
  APuppetShift: PTPuppetShift;
  APuppet: PTPuppet;
}
  procedure FreeEmpList;
  var
    I: Integer;
    APTEmployee: PTEmployee;
  begin
    for I := AEmployeeList.Count - 1  downto 0 do
    begin
      APTEmployee := AEmployeeList.Items[I];
      AEmployeeList.Remove(APTEmployee);
      Dispose(APTEmployee);
    end;
    AEmployeeList.Clear;
    AEmployeeList.Free;
  end;
  procedure AddEmp(var AEmployeeList: TList; APTEmployee: PTEmployee);
  var
    APTNewEmployee: PTEmployee;
  begin
    new(APTNewEmployee);
    APTNewEmployee.AEmployeeNumber := APTEmployee.AEmployeeNumber;
    APTNewEmployee.AEmployeeShortName := APTEmployee.AEmployeeShortName;
    APTNewEmployee.AEmployeeName := APTEmployee.AEmployeeName;
    APTNewEmployee.ATeamCode := APTEmployee.ATeamCode;
    APTNewEmployee.AShiftNumber := APTEmployee.AShiftNumber;
    APTNewEmployee.AEmpLevel := APTEmployee.AEmpLevel;
    APTNewEmployee.AStandAvailStartDate := 0;
    APTNewEmployee.AStandAvailEndDate := 0;
    APTNewEmployee.AEmpAvailStartDate := 0;
    APTNewEmployee.AEmpAvailEndDate := 0;
    APTNewEmployee.AAbsenceReasonCode := '';
    APTNewEmployee.AAbsenceReasonDescription := '';
    APTNewEmployee.AFirstAidYN := APTEmployee.AFirstAidYN;
    APTNewEmployee.AFirstAidExpDate := APTEmployee.AFirstAidExpDate;
    APTNewEmployee.APlanWorkspotCode := APTEmployee.APlanWorkspotCode;
    APTNewEmployee.AScanWorkspotCode := APTEmployee.AScanWorkspotCode;
    APTNewEmployee.AScanDateTimeIn := APTEmployee.AScanDatetimeIn;
    APTNewEmployee.APlanStartDate := APTEmployee.APlanStartDate;
    APTNewEmployee.APlanEndDate := APTEmployee.APlanEndDate;
    APTNewEmployee.APlanLevel := APTEmployee.APlanLevel;
    APTNewEmployee.AStandAvailStartDate := APTEmployee.AStandAvailStartDate;
    APTNewEmployee.AStandAvailEndDate := APTEmployee.AStandAvailEndDate;
    APTNewEmployee.AEmpAvailStartDate := APTEmployee.AEmpAvailStartDate;
    APTNewEmployee.AEmpAvailEndDate := APTEmployee.AEmpAvailEndDate;
    APTNewEmployee.AAbsenceReasonCode := APTEmployee.AAbsenceReasonCode;
    APTNewEmployee.AAbsenceReasonDescription :=
      APTEmployee.AAbsenceReasonDescription;
    APTNewEmployee.AColor := APTEmployee.AColor;
    APTNewEmployee.ACurrEff := APTEmployee.ACurrEff; // 20014550.50
    APTNewEmployee.AShiftEff := APTEmployee.AShiftEff; // 20014550.50
    APTNewEmployee.AKeep := True;
    AEmployeeList.Add(APTNewEmployee);
  end;
begin
  MyNow := Now;

  if AEmployeeList <> nil then
    FreeEmpList;
  AEmployeeList := TList.Create;
  AEmployeeCount := 0;

// PersonalScreen
(*
  // Employees Scanned and Planned on given Workspot
  // Take the list from the PUPPET-list if this is available.
  // Otherwise it will be determined again.
  if (AShowMode = ShowEmployeesOnWorkspot) then
  begin
    // Show all puppets of all shifts
    if ADrawObject.APuppetShiftList.Count > 0 then
    begin
      for I := 0 to ADrawObject.APuppetShiftList.Count - 1 do
      begin
        APuppetShift := ADrawObject.APuppetShiftList.Items[I];
        // One row shows all puppets of one Shift!
        if APuppetShift.APuppetList.Count > 0 then
        begin
          for J := 0 to APuppetShift.APuppetList.Count - 1 do
          begin
            APuppet := APuppetShift.APuppetList.Items[J];
            AEmployee.AEmployeeNumber := APuppet.AEmployeeNumber;
            AEmployee.AEmployeeName := APuppet.AEmployeeDescription;
            AEmployee.AEmployeeShortName := APuppet.AEmployeeShortName;
            AEmployee.ATeamCode := APuppet.AEmployeeTeamCode;
            AEmployee.AShiftNumber := 0;
            AEmployee.AEmpLevel := APuppet.AEmpLevel;
            AEmployee.AScanWorkspotCode := APuppet.AScannedWS;
            AEmployee.AScanDatetimeIn := APuppet.AScannedDateTimeIn;
            AEmployee.APlanWorkspotCode := APuppet.APlannedWS;
            AEmployee.APlanStartDate := APuppet.APlannedStartTime;
            AEmployee.APlanEndDate := APuppet.APlannedEndTime;
            AEmployee.APlanLevel := APuppet.AEPlanLevel;
            AEmployee.AColor := APuppet.AColor;
            AEmployee.ACurrEff := APuppet.ACurrEff; // 20014550.50
            AEmployee.AShiftEff := APuppet.AShiftEff; // 20014550.50
            AddEmp(AEmployeeList, @AEmployee);
            inc(AEmployeeCount);
          end;
        end;
      end;
      Result := (AEmployeeCount > 0);
      Exit; // The puppetlist was available, leave now.
    end;
  end;
*)
  // Create lists for planned/scanned employees
  PlanScanEmpDM.CurrentNow := MyNow;
  APlanScanClass.OpenEmployee;
  APlanScanClass.OpenEmployeePlanning(MyNow);
  APlanScanClass.OpenTimeRegScanning(MyNow);
//  APlanScanClass.CreateWorkspotsPerEmployeeList(MyNow); // TD-22082
  APlanScanClass.OpenWorkspotsPerEmployee; // TD-22082

  if (AShowMode = ShowEmployeesNotScannedIn) or
    (AShowMode = ShowEmployeesAbsentWithReason) or
    (AShowMode = ShowEmployeesAbsentWithoutReason) then
  begin
    APlanScanClass.OpenStandAvail(MyNow);
//    APlanScanClass.DetermineStandAvailEmployees(MyNow);
//    APlanScanClass.CreateOnlyStandAvailClientDataSet;
  end;
  if (AShowMode = ShowEmployeesAbsentWithReason) or
    (AShowMode = ShowEmployeesAbsentWithoutReason) then
  begin
    APlanScanClass.OpenEmployeeAvail(MyNow);
//    APlanScanClass.DetermineEmployeeAvailEmployees(MyNow);
//    if (AShowMode = ShowEmployeesAbsentWithReason) then
//      APlanScanClass.CreateOnlyEmpAvailEmployeesClientDataSetWithReason
//    else
//      APlanScanClass.CreateOnlyEmpAvailEmployeesClientDataSetWithoutReason;
  end;

  case AShowMode of
    ShowEmployeesOnWorkspot: // Employees scanned/planned on workspot
      begin
        if APlanScanClass.DetermineEmployeeWSListPLAN_SCAN_EMP(
          APlantCode, AWorkspotCode) then
        begin
          for I := 0 to APlanScanClass.EmployeeList.Count - 1 do
          begin
            APTEmployee := APlanScanClass.EmployeeList.Items[I];
            AddEmp(AEmployeeList, APTEmployee);
            inc(AEmployeeCount);
          end;
        end;
      end;
    ShowAllEmployees: // All: Scanned and Planned and Rest of employees
      begin
        if APlanScanClass.DetermineEmployeeWSListALL_EMP('', '') then
        begin
          for I := 0 to APlanScanClass.EmployeeList.Count - 1 do
          begin
            APTEmployee := APlanScanClass.EmployeeList.Items[I];
            AddEmp(AEmployeeList, APTEmployee);
            inc(AEmployeeCount);
          end;
        end;
      end;
    ShowEmployeesOnWrongWorkspot: // Scanned but on wrong Planned Workspot
      begin
        if APlanScanClass.DetermineEmployeeWSListPLAN_SCAN_EMP('', '') then
        begin
          for I := 0 to APlanScanClass.EmployeeList.Count - 1 do
          begin
            APTEmployee := APlanScanClass.EmployeeList.Items[I];
            if (APTEmployee.APlanWorkspotCode <> '') and
              (APTEmployee.AScanWorkspotCode <> '') then
              if (APTEmployee.APlanWorkspotCode <>
                APTEmployee.AScanWorkspotCode) then
              begin
                AddEmp(AEmployeeList, APTEmployee);
                inc(AEmployeeCount);
              end;
          end;
        end;
      end;
    ShowEmployeesAbsentWithReason: // Absent with a reason
       // Employees not scanned, planned, not available,
       // but absent with reason
      begin
        if APlanScanClass.DetermineEmployeeWSListEMP_AVAIL_EMP_REASON(
          '', '') then
        begin
          for I := 0 to APlanScanClass.EmployeeList.Count - 1 do
          begin
            APTEmployee := APlanScanClass.EmployeeList.Items[I];
            AddEmp(AEmployeeList, APTEmployee);
            inc(AEmployeeCount);
          end;
        end;
      end;
    ShowEmployeesAbsentWithoutReason: // Absent without a reason
       // Employees not scanned, but planned
      begin
        if APlanScanClass.DetermineEmployeeWSListEMP_AVAIL_EMP_NO_REASON(
          '', '') then
        begin
          for I := 0 to APlanScanClass.EmployeeList.Count - 1 do
          begin
            APTEmployee := APlanScanClass.EmployeeList.Items[I];
            AddEmp(AEmployeeList, APTEmployee);
            inc(AEmployeeCount);
          end;
        end;
//        if (ScannedWS = '') and (PlannedWS <> '') then
//          if (StndAvailStartTime <> 0) and (StndAvailEndTime <> 0) then
//            if (EmpAvailStartTime = 0) and (EmpAvailEndTime = 0) then
//              if (AbsenceReasonCode = '') then
//                ShowRow := True;
      end;
    ShowEmployeesNotScannedIn: // Not Scanned or Planned, but available
      begin
        if APlanScanClass.DetermineEmployeeWSListSTAND_AVAIL_EMP('', '') then
        begin
          for I := 0 to APlanScanClass.EmployeeList.Count - 1 do
          begin
            APTEmployee := APlanScanClass.EmployeeList.Items[I];
            AddEmp(AEmployeeList, APTEmployee);
            inc(AEmployeeCount);
          end;
        end;
      end;
    ShowEmployeesWithFirstAid: // Employee with first aid
      begin
        if APlanScanClass.DetermineEmployeeWSListALL_EMP('', '') then
        begin
          for I := 0 to APlanScanClass.EmployeeList.Count - 1 do
          begin
            APTEmployee := APlanScanClass.EmployeeList.Items[I];
            if APTEmployee.AFirstAidYN = 'Y' then
            begin
              AddEmp(AEmployeeList, APTEmployee);
              inc(AEmployeeCount);
            end;
          end;
        end;
      end;
    ShowEmployeesTooLate: // Employee too late
      // Show planned + scanned
      begin
        // Compare only FIRST Scan with FIRST Plan-timeblock
        if APlanScanClass.DetermineEmployeeWSListPLAN_SCAN_EMP('', '') then
        begin
          for I := 0 to APlanScanClass.EmployeeList.Count - 1 do
          begin
            APTEmployee := APlanScanClass.EmployeeList.Items[I];
            if (APTEmployee.APlanWorkspotCode <> '') and
              (APTEmployee.AScanWorkspotCode <> '') then
              if (Frac(APTEmployee.AScanDatetimeIn) >
                Frac(APTEmployee.APlanStartDate)) then
              begin
                AddEmp(AEmployeeList, APTEmployee);
                inc(AEmployeeCount);
              end;
          end;
        end;
      end;
  end; // case
  if AEmployeeCount > 0 then
    AEmployeeList.Sort(EmpCompare);
  Result := (AEmployeeCount > 0);
end; // DetermineEmployeeList

// PersonalScreen
(*
// Create a list of employees based on 'ShowMode', and
// show them as puppets.
procedure THomeF.ActionPuppetBox(ADrawObject: PDrawObject);
var
  // Use the 'AEmployeeList' from the 'ADrawObject', not a local
  // variable, because the 'DetermineEmployeeList' will first delete
  // a previous contents. And with a local variable this will not be done.
//  AEmpList: TList;
  AEmpCount: Integer;
begin
  if ADrawObject.AType = otPuppetBox then
  begin
//    AEmpList := nil;
    AEmpCount := 0;
    if DetermineEmployeeList(ADrawObject, ADrawObject.AShowMode, '', '',
      {AEmpList,}
      ADrawObject.AEmployeeList,
      AEmpCount) then
    begin
      if AEmpCount > 0 then
        ActionCreatePuppets(ADrawObject,
          {AEmpList}
          ADrawObject.AEmployeeList
          );
      ShowPuppets(ADrawObject, ADrawObject.AObject);
    end;
  end;
end; // ActionPuppetBox
*)

(*
// MR:01-03-2004
procedure THomeF.ActionCompareJobsInit(MyNow: TDateTime);
var
  DateFrom, DateTo: TDateTime;
begin
  // MR:10-03-2004
  DetermineSinceDates(MyNow, DateFrom, DateTo);
//  DateFrom := MyNow -
//    (PIMSDatacolTimeInterval * 1 {PIMSMultiplier } / 60 / 24 / 60);
//  DateTo := MyNow;
  ProductionScreenDM.CompareJobInit(DateFrom, DateTo);
end; // ActionCompareJobsInit
*)

(*
// MR:01-03-2004
procedure THomeF.ActionCompareJobs(ADrawObject: PDrawObject; MyNow: TDateTime);
var
  DateFrom, DateTo: TDateTime;
  MaxDeviation: Double;
  ComparisonReject: Boolean;
  JobCode: String;
  JobPieces, SumJobPieces: Double;
begin
  // MR:10-03-2004
  DetermineSinceDates(MyNow, DateFrom, DateTo);
//  DateFrom := MyNow -
//    (PIMSDatacolTimeInterval * 1 {PIMSMultiplier } / 60 / 24 / 60);
//  DateTo := MyNow;
  ADrawObject.ABlinkCompareJobs := False;
  if ProductionScreenDM.CompareJob(ADrawObject.APlantCode,
    ADrawObject.AWorkspotCode, DateFrom, DateTo,
    MaxDeviation, ComparisonReject, JobCode, JobPieces, SumJobPieces) then
  begin
    if ((SumJobPieces > 0) and (MaxDeviation > 0)) then
    begin
        // MR:21-04-2004 No use of percentage
//        if ((SumJobPieces - JobPieces) / SumJobPieces * 100 < MaxDeviation) then
      if (Abs(SumJobPieces - JobPieces) > MaxDeviation) then
        ADrawObject.ABlinkCompareJobs := True;
    end;
  end;
end; // ActionCompareJobs
*)

procedure THomeF.HelpLegendaActExecute(Sender: TObject);
begin
  inherited;
  try
    if not tlbtnEdit.Down then
      MainTimerSwitch(False);
    DialogLegendaF := TDialogLegendaF.Create(nil);
    DialogLegendaF.ShowModal;
  finally
    DialogLegendaF.Free;
    if not tlbtnEdit.Down then
      MainTimerSwitch(True);
  end;
end; // HelpLegendaActExecute

// RV064.1.
procedure THomeF.actShowStatusInfoExecute(Sender: TObject);
begin
  inherited;
  try
    ProductionScreenDM.CheckInterference;
  except
  end;
end; // actShowStatusInfoExecute

// New Personal Screen

procedure THomeF.DataInit(AAll: Boolean=True);
begin
  PersonalScreenDM.DataInit(PSList, AAll);
  ReadCounterIndex := -1;
end; // DataInit

// 20016016
procedure THomeF.DataReadAllForOneDrawObject;
var
  DateFrom, DateTo: TDateTime;
  AOneDrawObject: PDrawObject;
  I: Integer;
begin
  try
    try
      DetermineSinceDates(Now, DateFrom, DateTo);
      PersonalScreenDM.ReadDateFrom := DateFrom;
      PersonalScreenDM.ReadDateTo := DateTo;
      for I := 0 to PSList.Count - 1 do
      begin
        AOneDrawObject := PSList.Items[I];
        if Assigned(AOneDrawObject.AWorkspotTimeRec) then
          if AOneDrawObject.AWorkspotTimeRec.AReceiveQtyFromExternalApp then
          begin
            PersonalScreenDM.DataReadAll(PSList, ReadCounterList, False,
              False, AOneDrawObject, True);
            ShowDetails(AOneDrawObject);
          end;
      end;
    except
    end;
  finally
    LastTimeReadExtData := Now; // PIM-90
  end;
end; // DateReadAllForOneDrawObject

// Read all data for today from database till now
procedure THomeF.DataReadAll(ARecreateReadCounterList: Boolean=False);
var
  DateFrom, DateTo: TDateTime;
  I: Integer;
  AReadCounterRec: PReadCounterRec;
  // Multi
  procedure AssignSocketPort(AHost: String; APort: String;
    AModuleAddress: String);
  var
    MySocketPort: TSocketPort;
    MyIdTelNet: TIdTelNet;
  begin
    MyIdTelnet := TIdTelNet.Create(nil);
    MySocketPort := TSocketPort.Create(MyIdTelnet);
    if Assigned(MySocketPort) then
    begin
      ASocketPortList.Add(MySocketPort);
      MySocketPort.Host := AHost;
      MySocketPort.Port := StrToInt(APort);
      MySocketPort.ReadDelay := ReadDelay;
      MySocketPort.ReadBuffer := '';
      try
        MySocketPort.ModuleAddress := StrToInt(AModuleAddress);
      except
        MySocketPort.ModuleAddress := 0;
      end;
      MySocketPort.MyLog := WErrorLog;
    end;
  end;
  function SocketPortExists(AHost: String; APort: String): Boolean;
  var
    J: Integer;
    MySocketPort: TSocketPort;
  begin
    Result := False;
    if ASocketPortList.Count > 0 then
      for J := 0 to ASocketPortList.Count - 1 do
      begin
        MySocketPort := ASocketPortList.Items[J];
        if (MySocketPort.Host = AHost) and
          (MySocketPort.Port = StrToInt(APort)) then
          Result := True;
      end;
  end;
begin
  // Determine for TODAY (indicated by Since).
  DetermineSinceDates(Now, DateFrom, DateTo);
  PersonalScreenDM.ReadDateFrom := DateFrom;
  PersonalScreenDM.ReadDateTo := DateTo;
  PersonalScreenDM.DataReadAll(PSList, ReadCounterList, False,
    ARecreateReadCounterList);
  // TODO 21191 -> Do this only for OpenList-action.
  if ARecreateReadCounterList then
  begin
    // Multi
    if Assigned(ReadCounterList) then
      if ReadCounterList.Count > 0 then
        for I := 0 to ReadCounterList.Count - 1 do
        begin
          AReadCounterRec := ReadCounterList[I];
          // Check if Host and Port are not empty
          if (AReadCounterRec.BlackBoxRec.IPAddress <> '') and
            (AReadCounterRec.BlackBoxRec.Port <> '') then
          begin
            if not SocketPortExists(AReadCounterRec.BlackBoxRec.IPAddress,
              AReadCounterRec.BlackBoxRec.Port) then
            begin
              AssignSocketPort(AReadCounterRec.BlackBoxRec.IPAddress,
                AReadCounterRec.BlackBoxRec.Port,
               AReadCounterRec.BlackBoxRec.ModuleAddress);
              inc(SocketPortCount);
            end;
          end;
        end;
  end;
end; // DataReadAll

// Show details per workspot in the scheme on the screen
procedure THomeF.ShowDetails(ADrawObject: PDrawObject);
const
  SpaceFactorV = 25;
  SpaceFactorH = 35;
  TopStartFactor = 5;
var
{$IFDEF DEBUG}
  DebugEffPercentage: Double;
  DebugString: String;
{$ENDIF}
  EfficiencyPeriodMode: TEffPeriodMode;
  procedure ShowEmployeeDisplayList;
  var
    I, Top, Left: Integer;
    AEmployeeDisplayRec: PTEmployeeDisplayRec;
    FontSize: Integer;
    TopAdd: Integer;
    AEmployeeRec: PEmployeeRec;
  begin
    // PIM-12.3
    if not Assigned(ADrawObject.AEmployeeDisplayList) then
      Exit;
    // PIM-12.3 Added try-except and try-finally
    try
     try
        FontSize := Round(14 * ADrawObject.AFontScale / 100); // 16
        TopAdd :=
          Round(ADrawObject.AWorkspotTimeRec.AInButton.Height) +
            Round(ADrawObject.AWorkspotTimeRec.AInButton.Height / SpaceFactorV);
        Top := ADrawObject.ABorder.Top + ADrawObject.ABorder.Height + 2;
        Left := ADrawObject.ABorder.Left;
        for I := 0 to ADrawObject.AEmployeeDisplayList.Count - 1 do
        begin
          AEmployeeDisplayRec := ADrawObject.AEmployeeDisplayList.Items[I];
          AEmployeeRec :=
            ADrawObject.FindEmployeeRec(
              AEmployeeDisplayRec.EmployeeNumber); // PIM-147
          if Assigned(AEmployeeRec) then
          begin
            AEmployeeDisplayRec.JobCode := AEmployeeRec.JobCode;
            AEmployeeDisplayRec.JobDescription := AEmployeeRec.JobDescription;
          end;
          // Name
          // PIM-147 When job of employee differs from workspot-job then
          //         show alternativaly employee-name and job-description
          //         Only do this for more than 1 employee?
          //         NOTE: When there is 1 employee then also show it,
          //               or you get a conflict when trying to change this
          //               job via job-button.
          if ADrawObject.AEmployeeDisplayList.Count > 0 then
          begin
            if (AEmployeeDisplayRec.JobCode <> '') and
              (AEmployeeDisplayRec.JobCode <>
                ADrawObject.AWorkspotTimeRec.ACurrentJobCode) then
            begin
              // Toggle between employeename and jobdescription
              if ToggleNameJob then
              begin
                AEmployeeDisplayRec.AEmployeeNameLabel.Caption :=
                  AEmployeeDisplayRec.EmployeeName;
                AEmployeeDisplayRec.AEmployeeNameLabel.Font.Color :=
                  AEmployeeDisplayRec.DefaultFontColor;
              end
              else
              begin
                AEmployeeDisplayRec.AEmployeeNameLabel.Caption :=
                  AEmployeeDisplayRec.JobDescription;
                AEmployeeDisplayRec.AEmployeeNameLabel.Font.Color :=
                  AEmployeeDisplayRec.ToggleFontColor;
              end;
            end
            else
            begin
              AEmployeeDisplayRec.AEmployeeNameLabel.Caption :=
                AEmployeeDisplayRec.EmployeeName;
              AEmployeeDisplayRec.AEmployeeNameLabel.Font.Color :=
                AEmployeeDisplayRec.DefaultFontColor;
            end;
          end
          else
          begin
            AEmployeeDisplayRec.AEmployeeNameLabel.Caption :=
              AEmployeeDisplayRec.EmployeeName;
            AEmployeeDisplayRec.AEmployeeNameLabel.Font.Color :=
              AEmployeeDisplayRec.DefaultFontColor;
          end;

          // Name
          AEmployeeDisplayRec.AEmployeeNameLabel.Font.Size := FontSize;
          AEmployeeDisplayRec.AEmployeeNameLabel.Top := Top +
            Round(FontSize / TopStartFactor);
          AEmployeeDisplayRec.AEmployeeNameLabel.Left := Left  +
            Round(ADrawObject.ABorder.Width * 1/30);
          // PIM-223
          AEmployeeDisplayRec.AEmployeeNameLabel.Visible :=
            ORASystemDM.PSShowEmpInfo;
          AEmployeeDisplayRec.AWorkedMinutesLabel.Visible :=
            ORASystemDM.PSShowEmpInfo;

          // 20015178 Do not show name of employee, because it is a fake employee
          if ORASystemDM.EnableMachineTimeRec then
            if Assigned(ADrawObject.AWorkspotTimeRec) then
              if ADrawObject.AWorkspotTimeRec.ATimeRecByMachine then
              begin
                AEmployeeDisplayRec.AEmployeeNameLabel.Visible := False;
                AEmployeeDisplayRec.AWorkedMinutesLabel.Visible := False; // PIM-223
              end;
          //
          // Personal Screen - Start
          //
          // Worked Pieces
(*
          APTAEmployee.AWorkedPiecesLabel.Caption :=
            IntToStr(APTAEmployee.AWorkedPieces);
          APTAEmployee.AWorkedPiecesLabel.Width :=
            Round(ADrawObject.AWorkspotTimeRec.AModeButton.Width * 1/3);
          APTAEmployee.AWorkedPiecesLabel.Font.Size :=
            APTAEmployee.AEmployeeNameLabel.Font.Size;
          APTAEmployee.AWorkedPiecesLabel.Top := Top;
          APTAEmployee.AWorkedPiecesLabel.Left := Left +
            Round(ADrawObject.ABorder.Width * 4/6);
*)

          if ORASystemDM.PSShowEmpInfo and ORASystemDM.PSShowEmpEff then // PIM-223
          begin
            // 20014450.50
            // Emp Efficiency
            with PersonalScreenDM do
            begin
              AEmployeeDisplayRec.APercentage := 0;
              case EfficiencyPeriodMode of
              epCurrent: // Last 5-10 minutes
              begin
                // For Current we just use the workspot-eff for the employees!
                AEmployeeDisplayRec.APercentage := Round(ADrawObject.ATotalPercentage);
              end;
              epSince, epShift: // Today, Shift
              begin
                with RealTimeEffDM.oqRTShiftEmpEff do
                begin
                  ClearVariables;
                  SetVariable('PLANT_CODE', ADrawObject.APlantCode);
                  if Assigned(ADrawObject.AWorkspotTimeRec) then
                    SetVariable('SHIFT_NUMBER',
                      ADrawObject.AWorkspotTimeRec.ACurrentShiftNumber)
                  else
                    SetVariable('SHIFT_NUMBER', 1);
                  SetVariable('EMPLOYEE_NUMBER', AEmployeeDisplayRec.EmployeeNumber);
                  Execute;
                  if not Eof then
                    AEmployeeDisplayRec.APercentage := Round(FieldAsFloat('EFF'));
                end;
              end;
              end; // case
            end; // with
            // 20014450.50
            // Percentage
            // PIM-194
            AEmployeeDisplayRec.APercentagePanel.Color :=
              ORASystemDM.NewEffColor(AEmployeeDisplayRec.APercentage);
{            if AEmployeeDisplayRec.APercentage < 100 then
              AEmployeeDisplayRec.APercentagePanel.Color := clMyRed
            else
              AEmployeeDisplayRec.APercentagePanel.Color := clMyGreen; }
            AEmployeeDisplayRec.APercentagePanel.Caption :=
              Format(' %d %%', [AEmployeeDisplayRec.APercentage]);
            AEmployeeDisplayRec.APercentagePanel.Font.Size :=
              AEmployeeDisplayRec.AEmployeeNameLabel.Font.Size;
            AEmployeeDisplayRec.APercentagePanel.Height :=
              ADrawObject.AWorkspotTimeRec.AInButton.Height;
            AEmployeeDisplayRec.APercentagePanel.Width :=
              Round(ADrawObject.AWorkspotTimeRec.AInButton.Width * 3/4);
            AEmployeeDisplayRec.APercentagePanel.Top := Top;
            AEmployeeDisplayRec.APercentagePanel.Left := Left +
              Round(ADrawObject.ABorder.Width - AEmployeeDisplayRec.APercentagePanel.Width);
            // ListButton
            if not AEmployeeDisplayRec.AListButton.Init then
            begin
              AEmployeeDisplayRec.AListButton.Glyph.Assign(BitBtnList.Glyph);
              AEmployeeDisplayRec.AListButton.Init := True;
            end;
            AEmployeeDisplayRec.AListButton.Font.Size :=
              AEmployeeDisplayRec.AEmployeeNameLabel.Font.Size;
            if (ADrawObject.AProdScreenType <> pstWSEmpEff) then
            begin
              AEmployeeDisplayRec.AListButton.Top := Top;
              AEmployeeDisplayRec.AListButton.Left :=
                ADrawObject.AWorkspotTimeRec.AInButton.Left;
              AEmployeeDisplayRec.AListButton.Height :=
                ADrawObject.AWorkspotTimeRec.AInButton.Height;
              AEmployeeDisplayRec.AListButton.Width :=
                Round(ADrawObject.AWorkspotTimeRec.AInButton.Width / 2) -
                  Round(ADrawObject.AWorkspotTimeRec.AInButton.Width / SpaceFactorH);
            end;
            AEmployeeDisplayRec.AListButton.Visible := True;
            // GraphButton
            if not AEmployeeDisplayRec.AGraphButton.Init then
            begin
              AEmployeeDisplayRec.AGraphButton.Glyph.Assign(BitBtnGraph.Glyph);
              AEmployeeDisplayRec.AGraphButton.Init := True;
            end;
            AEmployeeDisplayRec.AGraphButton.Font.Size :=
              AEmployeeDisplayRec.AEmployeeNameLabel.Font.Size;
            if (ADrawObject.AProdScreenType <> pstWSEmpEff) then
            begin
              AEmployeeDisplayRec.AGraphButton.Top := Top;
              AEmployeeDisplayRec.AGraphButton.Left :=
                ADrawObject.AWorkspotTimeRec.AInButton.Left +
                  Round(ADrawObject.AWorkspotTimeRec.AInButton.Width / 2) +
                    Round(ADrawObject.AWorkspotTimeRec.AInButton.Width / SpaceFactorH);
              AEmployeeDisplayRec.AGraphButton.Height :=
                ADrawObject.AWorkspotTimeRec.AInButton.Height;
              AEmployeeDisplayRec.AGraphButton.Width :=
                Round(ADrawObject.AWorkspotTimeRec.AInButton.Width / 2) -
                  Round(ADrawObject.AWorkspotTimeRec.AInButton.Width / SpaceFactorH);
            end;
            AEmployeeDisplayRec.AGraphButton.Visible := True;
          end // if PSShowEmpEff
          else
          begin // if PSShowEmpEff
            // 20014450.50
            // Worked Time
            case EfficiencyPeriodMode of
            epCurrent: // Last 5-10 minutes
              AEmployeeDisplayRec.AWorkedMinutesLabel.Caption :=
                IntMin2StringTime(Round(
                  ADrawObject.ARealTimeIntervalMinutes * 60) div 60, True);
            epSince: // Whole day (show all)
              AEmployeeDisplayRec.AWorkedMinutesLabel.Caption :=
                IntMin2StringTime(Round(AEmployeeRec.Act_Time_Day) div 60, True);
            epShift: // Only current shift
              AEmployeeDisplayRec.AWorkedMinutesLabel.Caption :=
                IntMin2StringTime(Round(AEmployeeRec.Act_Time_Shift) div 60, True);
            end; // case
            AEmployeeDisplayRec.AWorkedMinutesLabel.Font.Size :=
              AEmployeeDisplayRec.AEmployeeNameLabel.Font.Size;
            AEmployeeDisplayRec.AWorkedMinutesLabel.Top := Top;
          // 20014450.50
          // 20013476
    {      if ADrawObject.AUseSampleTime then
            AEmployeeDisplayRec.AWorkedMinutesLabel.Left := Left +
              Round(ADrawObject.ABorder.Width * 8/10)
          else }
              AEmployeeDisplayRec.AWorkedMinutesLabel.Left := Left +
    //            Round(ADrawObject.ABorder.Width * 5/6);
                Round(ADrawObject.ABorder.Width -
                  AEmployeeDisplayRec.AWorkedMinutesLabel.Width);
            AEmployeeDisplayRec.AWorkedMinutesLabel.Visible := True;
          end; // if PSShowEmpEff
          // Next employee on next row
          Top := Top + TopAdd;
            //+ Trunc(APTAEmployee.AEmployeeNameLabel.Font.Size * 1.5); // 16
        end; // for
      except
        on E:Exception do
          WErrorLog(E.Message);
      end;
    finally
    end;
  end; // ShowEmployeeDisplayList;
  procedure BuildEmployeeDisplayList;
  var
    I: Integer;
    AEmployeeRec: PEmployeeRec;
    EmployeeIndex: Integer;
  begin
    // Now create a new employeedisplay-list, based on employee-list.
    for I := 0 to ADrawObject.AEmployeeList.Count - 1 do
    begin
      AEmployeeRec := ADrawObject.AEmployeeList.Items[I];
      // 20013379
      if not AEmployeeRec.Visible then
        Continue;
      // Add the employee when it is not in the 'EmployeeDisplayList':
      EmployeeIndex :=
        ADrawObject.FindEmployeeDisplayListIndex(AEmployeeRec.EmployeeNumber);
      if EmployeeIndex = -1 then
      begin
        // When Current, then only show employees who are currently scanned in.
        // If not, then skip it.
        // 20014450.50 Always show only currently scanned-in employees
//        if EfficiencyPeriodMode = epCurrent then
        begin
          if (not AEmployeeRec.CurrentlyScannedIn) then
            Continue; // skip
        end;
        // When Shift, then only show employee for current shift
        if EfficiencyPeriodMode = epShift then
        begin
          if Assigned(ADrawObject.AWorkspotTimeRec) then
            if ADrawObject.AWorkspotTimeRec.ACurrentShiftNumber <>
              AEmployeeRec.ShiftNumber then
              Continue; // skip
        end;
        // 20014450.50
        ADrawObject.CreateEmployee(Application,
          AEmployeeRec,
          ListButtonClick, GraphButtonClick,
          ShowEmployees1Click,          
          ImageMouseDown, ImageMouseMove,
          ImageMouseUp, pnlDraw,
          EfficiencyPeriodMode);
      end; // if
    end; // for
  end; // BuildEmployeeDisplayList
  procedure ShowCurrentJob;
  begin
    ADrawObject.AWorkspotTimeRec.AJobButton.Caption :=
      ADrawObject.AWorkspotTimeRec.ACurrentJobDescription;
    JobButtonAdjustSize(ADrawObject, ADrawObject.AWorkspotTimeRec.AJobButton); // 20015406
  end; // ShowCurrentJob;
  procedure ShowActualTarget;
  var
    DiffQuantity: Double;
  begin
    DiffQuantity := 0;
    // PIM-12 When someone is currently scanned in then show quantities,
    //        otherwise show 0.
    if (ADrawObject.ACurrentNumberOfEmployees > 0)  then
    begin
      case EfficiencyPeriodMode of
      epCurrent: // Last 5-10 minutes
        begin
          ADrawObject.AWorkspotTimeRec.AActualValueLabel.Caption :=
            Format('%d', [ADrawObject.Act_prodqty_current]);
          ADrawObject.AWorkspotTimeRec.ATargetValueLabel.Caption :=
            Format('%d', [ADrawObject.Theo_prodqty_current]);
          DiffQuantity := ADrawObject.Theo_prodqty_current -
            ADrawObject.Act_prodqty_current;
        end;
      epSince: // Whole day (show all)
        begin
          ADrawObject.AWorkspotTimeRec.AActualValueLabel.Caption :=
            Format('%d', [ADrawObject.Act_prodqty_day]);
          ADrawObject.AWorkspotTimeRec.ATargetValueLabel.Caption :=
            Format('%d', [ADrawObject.Theo_prodqty_day]);
          DiffQuantity := ADrawObject.Theo_prodqty_day -
            ADrawObject.Act_prodqty_day;
        end;
      epShift: // Only current shift
        begin
          ADrawObject.AWorkspotTimeRec.AActualValueLabel.Caption :=
            Format('%d', [ADrawObject.Act_prodqty_shift]);
          ADrawObject.AWorkspotTimeRec.ATargetValueLabel.Caption :=
            Format('%d', [ADrawObject.Theo_prodqty_shift]);
          DiffQuantity := ADrawObject.Theo_prodqty_shift -
            ADrawObject.Act_prodqty_shift;
        end;
      end; // case
    end
    else
    begin
      ADrawObject.AWorkspotTimeRec.AActualValueLabel.Caption :=
        Format('%d', [0]);
      ADrawObject.AWorkspotTimeRec.ATargetValueLabel.Caption :=
        Format('%d', [0]);
    end;
    // Extra / Short quantity
    if DiffQuantity <= 0 then
      ADrawObject.AWorkspotTimeRec.ADiffNameLabel.Caption := SPimsExtra
    else
      ADrawObject.AWorkspotTimeRec.ADiffNameLabel.Caption := SPimsShort;
    ADrawObject.AWorkspotTimeRec.ADiffValueLabel.Caption :=
      Format('%.0f', [Abs(DiffQuantity)]);
  end; // ShowActualTarget
  procedure ShowEfficiency;
    procedure ShowPercentageLabel;
    begin
      // 20014550.50 First ROUND the percentage!
      ADrawObject.AWorkspotTimeRec.APercentageLabel.Caption :=
        IntToStr(Round(ADrawObject.ATotalPercentage)) + ' %';
    end; // ShowPercentageLabel
  begin
    // PIM-12 Be sure this is determined before show efficiency!
    ADrawObject.DetermineTheoProdQtyCurrent;
    ADrawObject.DetermineTheoTimeCurrent;
    case EfficiencyPeriodMode of
    epCurrent: // Last 5-10 minutes
      begin
        case EffQuantityTime of
        efQuantity: // Based on quantity
          // 2.0.164.053.1
          if (ADrawObject.Theo_prodqty_current > 0) and
            (ADrawObject.ACurrentNumberOfEmployees > 0)  then
            ADrawObject.AEffPercentage :=
              Round(ADrawObject.Act_prodqty_current /
                ADrawObject.Theo_prodqty_current * 100)
          else
            ADrawObject.AEffPercentage := 0;
        efTime: // Based on time
          begin
            // xxx_time_xxx is stored in secs!
            // 2.0.164.053.1
            // 20014450.50 Related to this order:
            // Also look if there was a quantity during current period!
            if (ADrawObject.Act_time_current > 0) and
              (ADrawObject.ACurrentNumberOfEmployees > 0) and
              (ADrawObject.Act_prodqty_current > 0) then // 20014450.50
              ADrawObject.AEffPercentage :=
                Round((ADrawObject.Theo_time_current / 60) /
                  (ADrawObject.Act_time_current / 60) * 100)
            else
              ADrawObject.AEffPercentage := 0;
            // Debugging! Put Label1 in the screen
{$IFDEF DEBUG}
  if (ADrawObject.Theo_prodqty_current > 0) and
    (ADrawObject.ACurrentNumberOfEmployees > 0)  then
    DebugEffPercentage :=
      Round(ADrawObject.Act_prodqty_current /
        ADrawObject.Theo_prodqty_current * 100)
  else
    DebugEffPercentage := 0;
  if  Assigned(ADrawObject.AWorkspotTimeRec) then
    DebugString := ADrawObject.AWorkspotCode + ';' +
      ADrawObject.AWorkspotTimeRec.ACurrentJobCode + ';' +
      ADrawObject.AWorkspotTimeRec.ACurrentJobDescription + ';'
  else
    DebugString := '';
  Label1.Caption :=
    DebugString +
    'ActQtyCur=' + IntToStr(Round(ADrawObject.Act_prodqty_current)) + ';' +
    'TheoQtyCur=' + IntToStr(Round(ADrawObject.Theo_prodqty_current)) + ';' +
    'Q.Perc=' + IntToStr(Round(DebugEffPercentage)) + ';' +
    'ActTimeCur=' + IntToStr(Round(ADrawObject.Act_time_current)) + ';' +
    'TheoTimeCur=' + IntToStr(Round(ADrawObject.Theo_time_current)) + ';' +
    'T.Perc=' + IntToStr(Round(ADrawObject.AEffPercentage));
  WLog(Label1.Caption);
{$ENDIF}
            Update;
          end;
        end; // case
      end; // epCurrent
    epSince: // Whole day (show all)
      begin
        case EffQuantityTime of
        efQuantity: // Based on quantity
          // Based on quantity
          // 2.0.164.053.1
          if (ADrawObject.Theo_prodqty_day > 0) and
            (ADrawObject.ACurrentNumberOfEmployees > 0) then
            ADrawObject.AEffPercentage :=
              Round(ADrawObject.Act_prodqty_day /
                ADrawObject.Theo_prodqty_day * 100)
          else
            ADrawObject.AEffPercentage := 0;
        efTime: // Based on time
          begin
            // xxx_time_xxx is stored in secs!
            // 2.0.164.053.1
            // 20014450.50 Related to this order:
            // Also look if there was a quantity during current period!
            if (ADrawObject.Act_time_day > 0) and
              (ADrawObject.ACurrentNumberOfEmployees > 0) and
              (ADrawObject.Act_prodqty_day > 0) then // 20014450.50
              ADrawObject.AEffPercentage :=
                Round((ADrawObject.Theo_time_day / 60) /
                  (ADrawObject.Act_time_day / 60) * 100)
            else
              ADrawObject.AEffPercentage := 0;
(*
WriteLog(
  'WS=' + ADrawObject.AWorkspotCode +
  ' Theo_time_day=' + IntToStr(ADrawObject.Theo_time_day) +
  ' Act_time_day=' + IntToStr(ADrawObject.Act_time_day) +
  ' AEffPercentage=' + IntToStr(ADrawObject.AEffPercentage)
  );
*)
          end;
        end; // case
{$IFDEF DEBUG}
  if (ADrawObject.Theo_prodqty_day > 0) and
    (ADrawObject.ACurrentNumberOfEmployees > 0) then
    DebugEffPercentage :=
    Round(ADrawObject.Act_prodqty_day /
      ADrawObject.Theo_prodqty_day * 100)
  else
    DebugEffPercentage := 0;
  if  Assigned(ADrawObject.AWorkspotTimeRec) then
    DebugString := ADrawObject.AWorkspotCode + ';' +
      ADrawObject.AWorkspotTimeRec.ACurrentJobCode + ';' +
      ADrawObject.AWorkspotTimeRec.ACurrentJobDescription + ';'
  else
    DebugString := '';
  Label1.Caption :=
    DebugString +
    'ActQtyDay=' + IntToStr(Round(ADrawObject.Act_prodqty_day)) + ';' +
    'TheoQtyDay=' + IntToStr(Round(ADrawObject.Theo_prodqty_day)) + ';' +
    'Q.Perc=' + IntToStr(Round(DebugEffPercentage)) + ';' +
    'ActTimeDay=' + IntToStr(Round(ADrawObject.Act_time_day)) + ';' +
    'TheoTimeDay=' + IntToStr(Round(ADrawObject.Theo_time_day)) + ';' +
    'T.Perc=' + IntToStr(Round(ADrawObject.AEffPercentage));
  WLog(Label1.Caption);
{$ENDIF}
      end; // epSince
    epShift: // Only current shift
      begin
        case EffQuantityTime of
        efQuantity: // Based on quantity
          // 2.0.164.053.1
          if (ADrawObject.Theo_prodqty_shift > 0) and
            (ADrawObject.ACurrentNumberOfEmployees > 0)  then
            ADrawObject.AEffPercentage :=
              Round(ADrawObject.Act_prodqty_shift /
                ADrawObject.Theo_prodqty_shift * 100)
          else
            ADrawObject.AEffPercentage := 0;
        efTime: // Based on time
          // xxx_time_xxx is stored in secs!
          // 2.0.164.053.1
          // 20014450.50 Related to this order:
          // Also look if there was a quantity during current period!
          if (ADrawObject.Act_time_shift > 0) and
            (ADrawObject.ACurrentNumberOfEmployees > 0) and
            (ADrawObject.Act_prodqty_shift > 0) then // 20014450.50
            ADrawObject.AEffPercentage :=
              Round((ADrawObject.Theo_time_shift / 60) /
                (ADrawObject.Act_time_shift / 60) * 100)
          else
            ADrawObject.AEffPercentage := 0;
        end; // case
{$IFDEF DEBUG}
  if (ADrawObject.Theo_prodqty_shift > 0) and
    (ADrawObject.ACurrentNumberOfEmployees > 0)  then
    DebugEffPercentage :=
    Round(ADrawObject.Act_prodqty_shift /
    ADrawObject.Theo_prodqty_shift * 100)
  else
    DebugEffPercentage := 0;
  if  Assigned(ADrawObject.AWorkspotTimeRec) then
    DebugString := ADrawObject.AWorkspotCode + ';' +
      ADrawObject.AWorkspotTimeRec.ACurrentJobCode + ';' +
      ADrawObject.AWorkspotTimeRec.ACurrentJobDescription + ';'
  else
    DebugString := '';
  Label1.Caption :=
    DebugString +
    'ActQtyShift=' + IntToStr(Round(ADrawObject.Act_prodqty_shift)) + ';' +
    'TheoQtyShift=' + IntToStr(Round(ADrawObject.Theo_prodqty_shift)) + ';' +
    'Q.Perc=' + IntToStr(Round(DebugEffPercentage)) + ';' +
    'ActTimeShift=' + IntToStr(Round(ADrawObject.Act_time_shift)) + ';' +
    'TheoTimeShift=' + IntToStr(Round(ADrawObject.Theo_time_shift)) + ';' +
    'T.Perc=' + IntToStr(Round(ADrawObject.AEffPercentage));
  WLog(Label1.Caption);
{$ENDIF}
      end; // epShift
    end; // case
    // Show the meters
    ShowEffMeter(ADrawObject, ADrawObject.AEffPercentage, ADrawObject.AObject);
    ShowPercentageLabel;
  end; // ShowEfficiency
begin
  if not Assigned(ADrawObject) then // PIM-159 Related to this issue. This can be nil!
    Exit;
  // Do this only for Workspot-objects:
  // PIM-12.3
  if ADrawObject.AProdScreenType = pstWorkspotTimeRec then
    if not Assigned(ADrawObject.AEmployeeList) then
      Exit;
  // PIM-12.3
  try
    //
  finally
    case ADrawObject.AProdScreenType of
    pstWorkspotTimeRec, pstWSEmpEff:
      begin
        // Based on 'modebutton' first determine if it is about Today/Shift/Current
        EfficiencyPeriodMode := DetermineEfficiencyPeriodMode(ADrawObject);
        // Show list of employees under efficiency-bar
        // Based on Today/Shift/Current
        if (ADrawObject.AEmployeeList.Count > 0) or
          (ADrawObject.AWorkspotTimeRec.ACompareJobCode <> '') then
        begin
          // 20014450.50 Related to this order:
          // Only recreate + build the EmployeeDisplayList when it is really
          // needed! Otherwise there can be access violations because it now
          // has buttons!
          if ADrawObject.ARecreateEmployeeDisplayList then
          begin
            ADrawObject.AEmployeeDisplayListHasChanged := True;
            if ADrawObject.EmployeeListChangedTest then
            begin
              // PIM-12.3
              try
                //
              finally
{$IFDEF DEBUG3}
  WDebugLog('-> BuildEmployeeDisplayList');
{$ENDIF}
                ADrawObject.InitEmployeeDisplayList(False);
                ADrawObject.AEmployeeDisplayListHasChanged := True;
                BuildEmployeeDisplayList;
              end;
            end
            else
              ADrawObject.AEmployeeDisplayListHasChanged := False;
          end;
          ADrawObject.ARecreateEmployeeDisplayList := False;
          // 20014450.50 Note: Same Curr. Eff. value is also used per emp.
          // So call first ShowEfficiency and then ShowEmployeeDisplayList,
          // or it is one value behind!
          ShowEfficiency;
          ShowEmployeeDisplayList;
          ShowActualTarget;
  //        ShowCurrentJob;
          ShowDrawObject(ADrawObject, ADrawObject.AObject);
        end; // if ADrawObject.AEmployeeList.Count > 0 then
      end; // if ADrawObject.AProdScreenType = pstWorkspotTimeRec then
    pstMachineTimeRec:
      begin
        ShowDrawObject(ADrawObject, ADrawObject.AObject);
      end;
    end; // case
  end; // finally
end; // ShowDetails

procedure THomeF.MainActionForTimer;
(*
var
  ADrawObject: PDrawObject;
  I: Integer;
*)
begin
  // Do not do anything here!
  Exit;
(*
  TimerMain.Enabled := False;

  ShowMsg('');

  try
    Busy := True;
    // Do not do this! This turns too much timers off/on
//    MainTimerSwitch(False);
    TimerStartTime := SysUtils.Now;
//    LastDrawObject := CurrentDrawObject;
    ShowActionTime;
    // Show all details per workspot in the scheme on the screen
    for I := 0 to PSList.Count - 1 do
    begin
      Application.ProcessMessages;
      ADrawObject := PSList.Items[I];
      ShowSelectionRectangle(ADrawObject.AObject);
      ShowDetails(ADrawObject);
    end;
  finally
    TimerEndTime := SysUtils.Now;
    ShowActionTime;
    // Do not do this! This turns too much timers off/on
//    if not tlbtnEdit.Down then
//      MainTimerSwitch(True);
    Busy := False;
    if not tlbtnEdit.Down then
      TimerMain.Enabled := True;
  end;
*)
end; // MainActionForTimer

// Timer for reading counters from blackboxes
procedure THomeF.TimerReadCounterTimer(Sender: TObject);
begin
  inherited;
//  ToggleNameJob := not ToggleNameJob; // PIM-147 // Do this in TimeBlink
  // NOTE: TimerMain turns the TimerReadCounter on!!!
  TimerMain.Enabled := False;
  TimerReadCounter.Enabled := False;
  // First do this to be sure the next counter will be read.
  if ReadCounterIndex < ReadCounterList.Count - 1 then
    ReadCounterIndex := ReadCounterIndex + 1
  else
    ReadCounterIndex := 0;
  // Read 1 counter at a time in timer-interval.
  try
    Working := True;
    try
      MyActionReadCounter;
      HandleLinkJobsActualValues(PSList); // 20015376
      HandleCompareJobsTime(PSList); // 20015376            
      ActionShowWorkspotDetails;
    except
    end;
    Application.ProcessMessages;
    // MRA: When there is no connection with BlackBox, then it
    // jumps to begin of this procedure, instead of going on
    // with the lines below. It looks like this timer is turned on again???
  finally
    Working := False;
    // Based on timers that are not enabled anymore,
    // write something to file/DB.
    Application.ProcessMessages;
    if not TimerWriteToFile.Enabled then
    begin
      try
        // Do not turn off/on all timers!
        TimerMain.Enabled := False;
//        MainTimerSwitch(False);
        ActionWriteToFile;
      finally
        TimerWriteToFile.Enabled := True;
        // Do not turn off/on all timers!
//        if not tlbtnEdit.Down then
//          MainTimerSwitch(True);
      end;
    end;
    Application.ProcessMessages;
    if not TimerWriteToDB.Enabled then
    begin
      try
        // Do not turn off/on all timers!
        TimerMain.Enabled := False;
//        MainTimerSwitch(False);
//        ActionWriteToDB; // PIM-12 We do via ActionRealTimeEfficiency
        // PIM-90 This is done somewhere else
{
        try
          // 20016016
          DataReadAllForOneDrawObject;
        except
        end;
}        
        try
          // 20012858.80.
          TryReconnectCounters;
        except
        end;
      finally
        TimerWriteToDB.Enabled := True;
        // Do not turn off/on all timers!
//        if not tlbtnEdit.Down then
//          MainTimerSwitch(True);
      end;
    end;
{    if ReadCounterIndex < ReadCounterList.Count - 1 then
      ReadCounterIndex := ReadCounterIndex + 1
    else
      ReadCounterIndex := 0; }
    Application.ProcessMessages;
    // Handle actions here, to be sure the last counter has been read.
    if Assigned(MyAction) then
      MyAction;
    Application.ProcessMessages;
    TimerMain.Enabled := True;
    TimerReadCounter.Enabled := True;
  end; // finally
end; // TimerReadCounterTimer

procedure THomeF.FormDestroy(Sender: TObject);
begin
  try
    // Multi
    ClearSocketPortList;
    ASocketPortList.Free;
    with ProductionScreenDM do
    begin
      cdsWorkspotShortName.Close;
      odsJobcode.Active := False;
    end;
    SelectionRectangle.Visible := False; // PIM-116
    SelectionRectangle.Free;
    DialogWorkspotSelectF.Close;
    DialogWorkspotSelectF := nil;
    DialogDepartmentSelectF.Close;
    DialogDepartmentSelectF := nil;
    DialogShowAllEmployeesF := nil;
    APlanScanClass.Free;

    ReadCounterList.Free;
//    ASocketPort.Free;
  except
    on E:EAccessViolation do
    begin
      WErrorLog(E.Message);
    end;
    on E:Exception do
    begin
      WErrorLog(E.Message);
    end;
  end;
  inherited;
end; // FormDestroy

procedure THomeF.TimerWaitForReadTimer(Sender: TObject);
begin
  inherited;
  TimerWaitForRead.Enabled := False;
  Busy := False;
end;

// Timers must be disabled/enabled before/after, not in this action!
// Write to Database
procedure THomeF.ActionWriteToDB;
var
  DateFrom, DateTo: TDateTime;
begin
  try
//    MainTimerSwitch(False);
//    TimerWriteToFile.Enabled := False;
    // Do this in a different panel 3 -> 4
    stBarBase.Panels[4].Text := 'UpdateDB';
    Update;
    try
      PersonalScreenDM.ActionWriteToDB(PSList, LogPath);
    except
      on E:EOracleError do
      begin
        WErrorLog(E.Message);
      end;
      on E:Exception do
      begin
        WErrorLog(E.Message);
      end;
    end; // except
    // Refresh Employees - Only do this based on a workstation-setting!
    if RefreshEmployees then
    begin
{$IFDEF DEBUG}
  WDebugLog('-> RefreshEmployees');
{$ENDIF}
      // Here the employeelist is refresh, which can be needed when
      // employees scan-in from other workstations.
      try
        // Determine for TODAY (indicated by Since).
        DetermineSinceDates(Now, DateFrom, DateTo);
        PersonalScreenDM.ReadDateFrom := DateFrom;
        PersonalScreenDM.ReadDateTo := DateTo;
        PersonalScreenDM.RefreshEmployeeList(PSList, ReadCounterList);
        // TD-21738 Bugfix.
        // Bugfix for RefreshEmployees
        // Show also the updated info!
        // PIM-12 Bugfix
        // This must be done later or not at all, or it shows wrong values
        // actual/target and efficiency for a short time!
        // Do not do this at all, it will be done automatically later.
//        ActionShowAllWorkspotDetails;
      except
        on E:EOracleError do
        begin
          WErrorLog(E.Message);
        end;
        on E:Exception do
        begin
          WErrorLog(E.Message);
        end;
      end; // except
    end; // if
    // PIM-12 Write To WorkspotEfficiency-table
    try
//      ActionRealTimeEfficiencyTillNow(PSList, Now); // 20014450.50
    except
      on E:EOracleError do
      begin
        WErrorLog(E.Message);
      end;
      on E:Exception do
      begin
        WErrorLog(E.Message);
      end;
    end;
    try
      ActionGhostCountCheckDBMain; // PIM-151
    except
      on E:EOracleError do
      begin
        WErrorLog(E.Message);
      end;
      on E:Exception do
      begin
        WErrorLog(E.Message);
      end;
    end;
    // PIM-213
    try
      ActionWSEmpEffRefresh;
    except
    end;
  finally
    // Do this in a different panel 3 -> 4
    stBarBase.Panels[4].Text := 'UpdateDB-Ready';
    Update;
//    MainTimerSwitch(True);
//    TimerWriteToFile.Enabled := True;
  end; // finally
  // PIM-125
  ErrorLogReducer;
end; // ActionWriteToDB

// Timer for Write to database
procedure THomeF.TimerWriteToDBTimer(Sender: TObject);
begin
  inherited;
  TimerWriteToDB.Enabled := False;
(*
  try
    // First wait till system is not busy (not reading counters anymore).
    ActionWriteToDB;
  except
    on E:Exception do
    begin
      WErrorLog(E.Message);
    end;
  end;
*)
end;

// Timers must be disabled/enabled before/after, not in this action!
procedure THomeF.ActionWriteToFile;
begin
  try
//    MainTimerSwitch(False);
    // Do this in a different panel 3 -> 4
    stBarBase.Panels[4].Text := 'UpdateFile';
    Update;
    try
      PersonalScreenDM.ActionWriteToFile(PSList, LogPath);
    except
      on E:EOracleError do
      begin
        WErrorLog(E.Message);
      end;
      on E:Exception do
      begin
        WErrorLog(E.Message);
      end;
    end; // except
  finally
    // Do this in a different panel 3 -> 4
    stBarBase.Panels[4].Text := 'UpdateFile-Ready';
    Update;
//    MainTimerSwitch(True);
  end; // finally
end; // ActionWriteToFile

procedure THomeF.TimerWriteToFileTimer(Sender: TObject);
begin
  inherited;
  TimerWriteToFile.Enabled := False;
{
  try
    ActionWriteToFile;
  except
    on E:Exception do
    begin
      WErrorLog(E.Message);
    end;
  end;
}
end; // TimerWriteToFileTimer

procedure THomeF.MyActionRefresh;
begin
  // Not possible in Edit-mode
  if tlbtnEdit.Down then
    Exit;

  // 2.0.164.053.2
  BitBtnRefresh.Enabled := False;
  MyAction := nil;
//  RefreshNow := False;
  // Do this in a different panel 3 -> 4
  stBarBase.Panels[4].Text := 'Refresh';
  Update;
  // Refresh
  Screen.Cursor := crHourGlass;
  try
    // Be sure ALL timers turned OFF
    MainTimerSwitch(False, True);
    try
      // First write all
      ActionWriteToFile;
      ActionWriteToDB;
    except
    end;
    try
      // TODO 21191 Do not init all for refresh!
      DataInit(False);
      // Read all data till now.
      DataReadAll;
      RenumberDrawObjects;
      ShowFirstObject;
    except
    end;
  finally
// TODO 21191
//try
//  WLog('-> ActionRefresh');
//except
//end;
{$IFDEF DEBUG}
  WLog('-> ActionRefresh');
{$ENDIF}
    Screen.Cursor := crDefault;
//    MainActionForTimer;
    ActionShowAllWorkspotDetails;
    MainTimerSwitch(True, True);
    // 2.0.164.053.2
    BitBtnRefresh.Enabled := True;
    stBarBase.Panels[4].Text := 'Refresh-Ready';
    Update;
  end;
end; // MyActionRefresh

// Save F10-info to log-file.
procedure THomeF.ActionSaveLogMain(AAutomatic: Boolean=False);
begin
  // TD-21799
  ProductionScreenWaitF.lblMessage.Caption := SPimsSavingLog;
  ProductionScreenWaitF.Show;
  Update;
  try
    try
      DialogDebugInfoF.PSList := PSList;
      DialogDebugInfoF.ShowDebugInfo;
    except
      on E: Exception do
      begin
        WErrorLog(E.Message);
      end;
    end;
  finally
    ProductionScreenWaitF.Hide;
    Update;
    if not AAutomatic then
      ShowMessage('Log file has been saved as:' + #13 +
        UGlobalLogPath);
  end;
end; // ActionSaveLogMain

procedure THomeF.actSaveLogExecute(Sender: TObject);
begin
  inherited;
  if not IsBusy then
    MyActionSaveLog
  else
    MyAction := MyActionSaveLog;
end; // actSaveLogExecute

procedure THomeF.MyActionTimeRecording;
begin
  MyAction := nil;
  if not tlbtnEdit.Down then
  begin
//    WErrorLog('-> ActionTimeRecording');
    // 20014550.50 It did not switch off/on the maintimer here!
    //             resulting in a problem with displaying:
    //             - sometimes it did not display the new situation, because
    //               that was already done before this was finished.
    try
      MainTimerSwitch(False);
      try
      finally
        TimeRecordingAction(True);
      end;
    finally
      MainTimerSwitch(True);
    end;
  end;
end; // MyActionTimeRecording

procedure THomeF.MyActionChangeJob;
begin
  MyAction := nil;
  if tlbtnEdit.Down then
    Exit;
  try
//    WErrorLog('-> ActionChangeJob');
    MainTimerSwitch(False);
    ChangeJob;
  finally
    MainTimerSwitch(True);
  end;
end; // MyActionChangeJob

procedure THomeF.MyActionSaveAndExit;
begin
  // Do not do this here, but during Close!
//  MyAction := nil;
  Close;
end; // MyActionSaveAndExit

procedure THomeF.MyActionSaveLog;
begin
  MyAction := nil;
  ActionSaveLogMain;
end; // MyActionSaveLog

procedure THomeF.MyActionSaveLogAutomatic;
begin
  MyAction := nil;
  ActionSaveLogMain(True);
end; // MyActionSaveLogAutomatic

procedure THomeF.MyActionFileSettings;
var
  OldWorkspotScale: Integer;
  OldFontScale: Integer;
  OldEfficiencyPeriodMode: TEffPeriodMode;
  OldEfficiencyPeriodSince: Boolean;
  OldEfficiencyPeriodSinceTime: TDateTime;
  OldCurrentPeriod: Integer;
  OldMachHrsMaxIdleTime: Integer;
  OldReadExtDataAtSeconds: Integer; // PIM-90
  OldShowEmpInfo: Boolean; // PIM-223
  OldShowEmpEff: Boolean; // PIM-223
begin
  inherited;
  MyAction := nil;
  if not tlbtnEdit.Down then
    MainTimerSwitch(False);

  DialogSettingsF := TDialogSettingsF.Create(Application);
  ATranslateHandlerPS.TranslateDialogSettings;
  DialogSettingsF.SettingUseSoundAlarm := UseSoundAlarm;
  DialogSettingsF.SettingSoundFilename := SoundFilename;
  DialogSettingsF.MySoundPath := SoundPath;
  DialogSettingsF.SettingWorkspotScale := WorkspotScale;
  DialogSettingsF.SettingFontScale := FontScale;
  DialogSettingsF.SettingEfficiencyPeriodSince := EfficiencyPeriodSince;
  DialogSettingsF.SettingEfficiencyPeriodSinceTime := EfficiencyPeriodSinceTime;
  DialogSettingsF.SettingRefreshTimeInterval := RefreshTimeInterval;
  DialogSettingsF.EfficiencyPeriodMode := EfficiencyPeriodMode;
  // Personal Screen
  DialogSettingsF.DatacolInterval := TimerReadCounter.Interval;
//  DialogSettingsF.EffQuantityTime := EffQuantityTime;
  DialogSettingsF.WriteToDBInterval := WriteToDBInterval;
  DialogSettingsF.ReadDelay := ReadDelay;
  DialogSettingsF.RefreshEmployees := RefreshEmployees;
  DialogSettingsF.ReadTimeout := ReadTimeout; // TD-21191
  DialogSettingsF.CurrentPeriod := CurrentPeriod; // SO-20013476
  DialogSettingsF.MachHrsMaxIdleTime := MachHrsMaxIdleTime; // 20014450
  DialogSettingsF.ReadExtDataAtSeconds := ReadExtDataAtSeconds; // PIM-90
  OldWorkspotScale := WorkspotScale;
  OldFontScale := FontScale;
  OldEfficiencyPeriodMode := EfficiencyPeriodMode;
  OldEfficiencyPeriodSince :=  EfficiencyPeriodSince;
  OldEfficiencyPeriodSinceTime := EfficiencyPeriodSinceTime;
  OldCurrentPeriod := CurrentPeriod;
  OldMachHrsMaxIdleTime := MachHrsMaxIdleTime;
  OldReadExtDataAtSeconds := ReadExtDataAtSeconds; // PIM-90
  OldShowEmpInfo := ORASystemDM.PSShowEmpInfo; // PIM-223
  OldShowEmpEff := ORASystemDM.PSShowEmpEff; // PIM-223
  if DialogSettingsF.ShowModal = mrOK then
  begin
    UseSoundAlarm := DialogSettingsF.SettingUseSoundAlarm;
    SoundFilename := DialogSettingsF.SettingSoundFilename;
    WorkspotScale := DialogSettingsF.SettingWorkspotScale;
    FontScale := DialogSettingsF.SettingFontScale;
    EfficiencyPeriodMode := DialogSettingsF.EfficiencyPeriodMode;
    EfficiencyPeriodSince := DialogSettingsF.SettingEfficiencyPeriodSince;
    EfficiencyPeriodSinceTime :=
      DialogSettingsF.SettingEfficiencyPeriodSinceTime;
    if OldEfficiencyPeriodMode <> EfficiencyPeriodMode then
      Dirty := True;
    if OldEfficiencyPeriodSince <> EfficiencyPeriodSince then
      Dirty := True;
    if OldEfficiencyPeriodSinceTime <> EfficiencyPeriodSinceTime then
      Dirty := True;
    RefreshTimeInterval := DialogSettingsF.SettingRefreshTimeInterval;
    // Personal Screen
    TimerReadCounter.Interval := DialogSettingsF.DatacolInterval;
//    EffQuantityTime := DialogSettingsF.EffQuantityTime;
    WriteToDBInterval := DialogSettingsF.WriteToDBInterval;
    TimerWriteToDB.Interval := WriteToDBInterval * 60000;
    ReadDelay := DialogSettingsF.ReadDelay;
    RefreshEmployees := DialogSettingsF.RefreshEmployees;
    // TD-21191
    ReadTimeout := DialogSettingsF.ReadTimeout;
    TimerWaitForRead.Interval := ReadTimeout;
    // SO-20013476 Do not change the setting for running app, it must be restarted.
    CurrentPeriod := DialogSettingsF.CurrentPeriod;
    if OldCurrentperiod <> CurrentPeriod then
      Dirty := True;
//    ASocketPort.ReadDelay := ReadDelay;
    // 20014450
    MachHrsMaxIdleTime := DialogSettingsF.MachHrsMaxIdleTime;
    PersonalScreenDM.MachHrsMaxIdleTime := MachHrsMaxIdleTime;
    if OldMachHrsMaxIdleTime <> MachHrsMaxIdleTime then
      Dirty := True;
    ReadExtDataAtSeconds := DialogSettingsF.ReadExtDataAtSeconds;
    if OldReadExtDataAtSeconds <> ReadExtDataAtSeconds then
      Dirty := True;
    if OldShowEmpInfo <> ORASystemDM.PSShowEmpInfo then // PIM-223
      Dirty := True;
    if OldShowEmpEff <> ORASystemDM.PSShowEmpEff then // PIM-223
      Dirty := True;
    WriteRegistry;
    if not ((WorkspotScale = OldWorkspotScale) and
      (FontScale = OldFontScale)) then
    begin
      SaveList(WorkFilename);
      OpenList(WorkFilename);
    end;
  end;
  DialogSettingsF.Free;

  if not tlbtnEdit.Down then
    MainTimerSwitch(True);
end; // MyActionFileSettings

procedure THomeF.MyActionReadCounter;
var
  ABlackBoxRec: PBlackBoxRec;
  AReadCounterRec: PReadCounterRec;
  Aborted: Boolean;
{$IFDEF TEST}
  AddTestCounter: Boolean;
{$ENDIF}
  procedure ShowCounterValue;
  begin
    if Assigned(ASocketPort) then
      if ReadCounterIndex <> -1 then
      begin
        stBarBase.Panels[3].Text := IntToStr(ReadCounterIndex+1) + ':' +
          IntToStr(ASocketPort.CounterValue);
      end;
    Update;
{$IFDEF DEBUG2}
  WDebugLog(stBarBase.Panels[3].Text);
{$ENDIF}
  end; // ShowCounterValue
  function MyReadCounter(ACounter: Integer; var AAborted: Boolean): Boolean;
    function DateTimeToMilliseconds(aDateTime: TDateTime): Int64;
    var
      TimeStamp: TTimeStamp;
    begin
      {Call DateTimeToTimeStamp to convert DateTime to TimeStamp:}
      TimeStamp := DateTimeToTimeStamp (aDateTime);
      {Multiply and add to complete the conversion:}
      Result := Int64 (TimeStamp.Date) * MSecsPerDay + TimeStamp.Time;
    end;
  begin
    TimerStartTime := SysUtils.Now;
    Result := False;
    AAborted := False;
    // MRA:27-AUG-2012 When this timer is enabled, it means the system is
    // already busy with reading a counter.
    if TimerWaitForRead.Enabled then
      Exit;
    try
      try
        if not Assigned(ASocketPort) then
        begin
          Exit;
        end;

{$IFDEF DEBUG2}
  ASocketPort.Debug := True;
{$ENDIF}

        // Be sure the port could be opened!
        if ASocketPort.PortConnect(ASocketPort.Host, ASocketPort.Port,
          ASocketPort.ModuleAddress) then
        begin
          case ACounter of
          1:
            begin
              ASocketPort.Counter0Read := False;
              TimerWaitForRead.Enabled := True;
              Busy := True;
              ASocketPort.ReadIDIn := ASocketPort.ReadIDIn + 1;
              ASocketPort.MyReadCounterSwitch := True;
              if ASocketPort.ReadCounterorFrequencyValue0 then
              begin
                while TimerWaitForRead.Enabled and (not ASocketPort.Counter0Read) do
                  Application.ProcessMessages;
                if ASocketPort.Counter0Read then
                begin
                  Result := True;
                  if ASocketPort.ReadIDIn = ASocketPort.ReadIDOut then
                    ShowCounterValue;
                end
                else
                begin
                  // TD-21191
                  // If Busy is False here, then the TimerWaitForRead was
                  // interrupted because a user clicked a button.
                  // In that case use 'AAborted' to see if it was aborted.
                  if Busy then
                  begin
                    WErrorLog(ASocketPort.PortInfo + ': ' + SPimsCounterOneNotRead);
                    ShowStatusErrorMessage(ASocketPort.PortInfo +
                      ': ' + SPimsCounterOneNotRead);
                  end
                  else
                  begin
                    AAborted := True;
                    WErrorLog('Aborted.');
                  end;
(*
              inc(MissedCounter);
              lblMissedCounter.Caption := IntToStr(MissedCounter);
              Log('Counter 1 not read! M=' + IntToStr(MissedCounter));
              Update;
*)
                end;
              end // if ASocketPort.ReadCounterorFrequencyValue0
              else
              begin
  WErrorLog(IntToStr(ReadCounterIndex+1) +
    ': ReadCounterorFrequencyValue0 failed.');
              end;
            end;
          2:
            begin
              ASocketPort.Counter1Read := False;
              TimerWaitForRead.Enabled := True;
              Busy := True;
              ASocketPort.ReadIDIn := ASocketPort.ReadIDIn + 1;
              ASocketPort.MyReadCounterSwitch := True;
              if ASocketPort.ReadCounterorFrequencyValue1 then
              begin
                while TimerWaitForRead.Enabled and (not ASocketPort.Counter1Read) do
                  Application.ProcessMessages;
                if ASocketPort.Counter1Read then
                begin
                  Result := True;
                  if ASocketPort.ReadIDIn = ASocketPort.ReadIDOut then
                    ShowCounterValue;
                end
                else
                begin
                  // TD-21191
                  // If Busy is False here, then the TimerWaitForRead was
                  // interrupted because a user clicked a button.
                  // In that case use 'AAborted' to see if it was aborted.
                  if Busy then
                  begin
                    WErrorLog(ASocketPort.PortInfo + ': ' + SPimsCounterTwoNotRead);
                    ShowStatusErrorMessage(ASocketPort.PortInfo +
                      ': ' + SPimsCounterTwoNotRead);
                  end
                  else
                  begin
                    AAborted := True;
                    WErrorLog('Aborted.');
                  end;
(*
             inc(MissedCounter);
             lblMissedCounter.Caption := IntToStr(MissedCounter);
             Log('Counter 2 not read! M=' + IntToStr(MissedCounter));
             Update;
*)
                end;
              end // if ASocketPort.ReadCounterorFrequencyValue1
              else
              begin
  WErrorLog(IntToStr(ReadCounterIndex+1) +
    ': ReadCounterorFrequencyValue1 failed.');
              end;
            end;
          end; // case
          ASocketPort.Wait(ASocketPort.ReadDelay);
        end // if PortConnect
        else
        begin
  WErrorLog(IntToStr(ReadCounterIndex+1) + ': PortConnect failed.');
        end;
      except
        Result := False;
      end;
      // When the counter could not be read then Result is False.
      // This can mean there was a connection-problem.
      if (not Result) and (not AAborted) then
      begin
        try
          ABlackBoxRec.Disabled := True;
          WErrorLog(ASocketPort.PortInfo + ': ' + SPimsBlackBoxDisabling);
          WErrorLog(ASocketPort.PortInfo + ': ' + SPimsErrorDuringReadCounter);
          WErrorLog(IntToStr(ReadCounterIndex+1) + ':' +
            SPimsBlackBoxIsDisabled);
          ShowStatusErrorMessage(IntToStr(ReadCounterIndex+1) + ':' +
            SPimsBlackBoxIsDisabled);
//          ShowStatusErrorMessage(ASocketPort.PortInfo +
//            ': ' + SPimsErrorDuringReadCounter);
          Application.ProcessMessages;
          // MRA:27-AUG-2012
          // Why close the port here? Disabled.
{
            ASocketPort.IdTelnet.Connected then
              ASocketPort.PortDisconnect;
}
        except
        end;
      end;
    finally
      TimerEndTime := SysUtils.Now;

if not Result then
  WErrorLog(
    IntToStr(DateTimeToMilliseconds(TimerStartTime) -
      DateTimeToMilliseconds(TimerEndTime)) + ' msecs');

      Busy := False;
      TimerWaitForRead.Enabled := False;
      ASocketPort.MyReadCounterSwitch := False;
    end;
  end; // MyReadCounter
  procedure ShowFifoList(ADrawObject: PDrawObject);
  var
    I, J: Integer;
    AJobCodeRec: PJobCodeRec;
    AFifoRec: PTFifoRec;
  begin
    for I := 0 to ADrawObject.AJobCodeList.Count - 1 do
    begin
      AJobCodeRec := ADrawObject.AJobCodeList[I];
      if Assigned(AJobCodeRec.Fifo_list) then
      begin
        for J := 0 to AJobCodeRec.Fifo_list.Count - 1 do
        begin
          AFifoRec := AJobCodeRec.Fifo_list[J];
          WriteLog(
//            FormatDateTime('nn:ss:zzz', AFifoRec.TimeStamp) + ';' +
            ADrawObject.APlantCode + ';' +
            ADrawObject.AWorkspotCode + ';' +
            AJobCodeRec.JobCode + ';' +
            AJobCodeRec.JobCodeDescription + ';' +
            DateTimeToStr(AFifoRec.TimeStamp) + ';' +
            IntToStr(AFifoRec.Qty) + ';' +
            FloatToStr(AFifoRec.TimeElapsed));
        end;
      end;
    end;
  end; // ShowFifoList
  // TD-21191
  // Check if checkdigits that are defined for a counter are found
  // in the counter itself, at first 4 positions.
  // Example:
  // - Checkdigits is set to '1004'.
  // - Counter is '1004049512'.
  // - This is accepted and counter is changed to 49512 (checkditits removed).
  function CheckOnCheckDigits: Boolean;
  var
    CVString: String;
    // SO-20015330
    procedure CheckForReset;
    var
      Len: Integer;
    begin
      Len := Length(ABlackBoxRec.CheckDigits) + 1;
      try
        if CVString <> '' then
          if CVString[Len] = '9' then
            ABlackBoxRec.Reset := True;
      except
        ABlackBoxRec.Reset := False;
      end;
    end; // CheckForReset
  begin
    Result := False;
    CVString := IntToStr(ASocketPort.CounterValue); // SO-20015330
    if ABlackBoxRec.CheckDigits <> '' then
    begin
      try
        if Copy(CVString, 1, Length(ABlackBoxRec.CheckDigits)) =
          ABlackBoxRec.CheckDigits then
        begin
          // Do not remove the checkdigits from countervalue, just
          // keep it like it was.
//          ASocketPort.CounterValue :=
//            StrToInt(Copy(CVString, 5, Length(CVString)));
          Result := True;
        end
        else
        begin
          WLog('-> CV [' + CVString +
            '] Wrong checkdigits [' + ABlackBoxRec.CheckDigits + ']');
          // TD-26329 This must NOT be done, it stays too long in the screen.
//          lblErrorMessage.Caption := '  ERROR: Wrong checkdigit!'; // TD-26329
          ShowStatusErrorMessage('Wrong checkdigit!'); // TD-26329
        end;
      except
        WLog('-> CheckDigits Error. CV=[' + CVString + ']');
        ShowStatusErrorMessage('Wrong checkdigit!'); // TD-26329
        Result := False;
      end;
    end
    else
      Result := True;
    if Result then // SO-20015330
      CheckForReset;
  end; // CheckOnCheckDigits
begin
  try
    // Check if there are items in ReadCounterList
    // If not, then show a message in status-bar, but continue.
    // This can happen when the setup is not ready yet, but
    // user wants to make/change a scheme.
    if ReadCounterList.Count = 0 then
    begin
  //    MainTimerSwitch(False, True);
  //    WErrorLog(SPimsNoCountersDefined);
      // This also logs it to error-log.
      if (BBCount > 0) then // ABS-8116
        ShowStatusErrorMessage(SPimsNoCountersDefined);
      Exit;
  //    Busy := False;
  //    Close;
    end;
    try
      try
        if ReadCounterIndex <> -1 then
          AReadCounterRec := ReadCounterList.Items[ReadCounterIndex]
        else
          AReadCounterRec := ReadCounterList.Items[0];
        ABlackBoxRec := AReadCounterRec.BlackBoxRec;
        // Multi
        // TODO 21191
        ASocketPort := SocketPortFind(ABlackBoxRec.IPAddress,
          ABlackBoxRec.Port);
        if not Assigned(ASocketPort) then
          Exit;

        // 20012858.80.
        // This takes too much time!
{
        try
          if not IPExists(ABlackBoxRec.IPAddress) then
            ABlackBoxRec.Disabled := True
          else
             ABlackBoxRec.Disabled := False;
        except
        end;
}
        // 20012858.70 Rework
        // This must be done when a blackbox could not be reached, which
        // can happen when it is turned off, or if the IP-address is wrong.
        if ABlackBoxRec.Disabled then
        begin
          WErrorLog(IntToStr(ReadCounterIndex+1) + ':' +
            SPimsBlackBoxIsDisabled);
          ShowStatusErrorMessage(IntToStr(ReadCounterIndex+1) + ':' +
            SPimsBlackBoxIsDisabled);

{          ShowStatusErrorMessage(ABlackBoxRec.IPAddress + '/' +
            ABlackBoxRec.Port + '/' + ABlackBoxRec.ModuleAddress + '/' +
            IntToStr(ABlackBoxRec.CounterNumber) +
            ': ' + SPimsBlackBoxIsDisabled);
}
          Exit;
        end;
        if not ((ABlackBoxRec.IPAddress = '') or (ABlackBoxRec.Port = ''))  then
        begin
          // Multi
          // Host and Port are already assigned
{
          // Assign host/port/address/counter to socket-object
          ASocketPort.Host := ABlackBoxRec.IPAddress;
          try
            ASocketPort.Port := StrToInt(ABlackBoxRec.Port);
          except
            ASocketPort.Port := 0;
          end;
}
          try
            ASocketPort.ModuleAddress := StrToInt(ABlackBoxRec.ModuleAddress);
          except
            ASocketPort.ModuleAddress := 0;
          end;
          ASocketPort.CounterNumber := ABlackBoxRec.CounterNumber;
          // TODO 21191 Empty ReadBuffer here.
          ASocketPort.ReadBuffer := '';
          // Read the counter
          if MyReadCounter(ASocketPort.CounterNumber, Aborted) then
          begin
            if not Aborted then
            begin
              // TD-21191
              if CheckOnCheckDigits then
              begin
                // TODO 21191 When countervalue is 0 do not accept it!
                if ASocketPort.CounterValue = 0 then
                begin
                  try
                    WLog('-> Read CV=0 Skipped');
                  except
                  end;
                end
                else
                begin
{$IFDEF TEST}
  if ABlackBoxRec.AddFakeCounterNow then // if Round(Random(100)) <= 3 then
  begin
    ABlackBoxRec.AddFakeCounterNow := False;
    ABlackBoxRec.TestCounter := ABlackBoxRec.TestCounter + 1;
    AddTestCounter := True;
  end
  else
    AddTestCounter := False;
{$ENDIF}
                  // Memorize the counter for later use
                  ABlackBoxRec.ActualCounterValue := ASocketPort.CounterValue;
{$IFDEF TEST}
  if AddTestCounter then
    ABlackBoxRec.ActualCounterValue :=
      ABlackBoxRec.ActualCounterValue + ABlackBoxRec.TestCounter;
{$ENDIF}
                  ABlackBoxRec.ActualCounterDateTime := Now;
                  AReadCounterRec.DrawObject.MemorizeCounterValue(
                    AReadCounterRec.DrawObject, ABlackBoxRec);
                end;
              end;
            end; // if
            // TEST
            // ShowFifoList(AReadCounterRec.DrawObject);
          end // if
          else
          begin
            if not Aborted then
            begin
              ABlackBoxRec.Disabled := True;
              WErrorLog(ASocketPort.PortInfo + ': ' + SPimsBlackBoxDisabling);
              WErrorLog(ASocketPort.PortInfo + ': ' + SPimsErrorDuringReadCounter);
              WErrorLog(IntToStr(ReadCounterIndex+1) + ':' +
                SPimsBlackBoxIsDisabled);
              ShowStatusErrorMessage(IntToStr(ReadCounterIndex+1) + ':' +
                SPimsBlackBoxIsDisabled);
{            ShowStatusErrorMessage(ASocketPort.PortInfo +
               ': ' + SPimsErrorDuringReadCounter); }
              Application.ProcessMessages;
            end; // if
          end;
        end; // if
      except
        //
      end;
    finally
    end;
  finally
    ActionShowStatusBB;
  end;
end; // MyActionReadCounter;

procedure THomeF.FileCloseExecute(Sender: TObject);
begin
  inherited;
  if not IsBusy then
    MyActionSaveAndExit
  else
    MyAction := MyActionSaveAndExit;
end; // FileCloseExecute

procedure THomeF.WMQueryEndSession(var Message: TWMQueryEndSession);
begin
  // Respond to shutdown message form the system
  Message.Result := Integer(True);
  MyAction := MyActionSaveAndExit;
  Close;
end; // WMQueryEndSession

// Returns if system is busy with reading counters
function THomeF.IsBusy: Boolean;
begin
  Result := TimerWaitForRead.Enabled and
    TimerReadCounter.Enabled and Working;
end;

// MRA:27-JUN-2012 Small change.
// Change font of jobbutton if text is too long.
procedure THomeF.JobButtonAdjustSize(ADrawObject: PDrawObject; AJobButton: TButton); // 20015406
var
  Len: Integer;
begin
  // 20015406
  Len := Length(AJobButton.Caption);
  if (Len < 18) then
    AJobButton.Font.Size :=
      Trunc(pnlDraw.Font.Size * FONT_SIZE_FACTOR * ADrawObject.AWorkspotScale / 100)
  else
    if (Len >= 18) and (Len < 24) then
      AJobButton.Font.Size :=
        Trunc(pnlDraw.Font.Size * 2.0 * ADrawObject.AWorkspotScale / 100)
    else
      if (Len >= 24) and (Len < 27) then
        AJobButton.Font.Size :=
          Trunc(pnlDraw.Font.Size * 1.8 * ADrawObject.AWorkspotScale / 100)
      else
        if (Len >= 27) then
          AJobButton.Font.Size :=
            Trunc(pnlDraw.Font.Size * 1.4 * ADrawObject.AWorkspotScale / 100);
  // PIM-213
  if ADrawObject.AProdScreenType = pstWSEmpEff then
    if ADrawObject.ApnlWSEmpEff.EmpList.Count <= 1 then
      AJobButton.Font.Size := Round(AJobButton.Font.Size * 8/10);
end; // JobButtonAdjustSize

// 20012858.80.
// Try to reconnect a disabled blackbox (counter)
procedure THomeF.TryReconnectCounters;
var
  I, J: Integer;
  ABlackBoxRec: PBlackBoxRec;
  AReadCounterRec: PReadCounterRec;
  IPList: TStringList;
begin
  IPList := TStringList.Create;
  try
    try
      if Assigned(ReadCounterList) then
      begin
        for I := 0 to ReadCounterList.Count - 1 do
        begin
          AReadCounterRec := ReadCounterList.Items[I];
          ABlackBoxRec := AReadCounterRec.BlackBoxRec;
          if ABlackBoxRec.Disabled then
            if IPList.IndexOf(ABlackBoxRec.IPAddress) = -1 then
              IPList.Add(ABlackBoxRec.IPAddress);
        end;
        for I := 0 to IPList.Count - 1 do
        begin
          if IPExists(IPList.Strings[I]) then
          begin
            // Enable 1 or more blackboxes with same IP-address
            for J := 0 to ReadCounterList.Count - 1 do
            begin
              AReadCounterRec := ReadCounterList.Items[J];
              ABlackBoxRec := AReadCounterRec.BlackBoxRec;
              if ABlackBoxRec.IPAddress = IPList.Strings[I] then
                ABlackBoxRec.Disabled := False;
            end;
          end;
        end;
      end;
    except
      //
    end;
  finally
    IPList.Free;
  end;
end; // TryReconnectCounter

// TODO 21191 Be sure Host and Port both have a value.
function THomeF.SocketPortFind(AHost, APort: String): TSocketPort;
var
  I: Integer;
  MySocketPort: TSocketPort;
begin
  Result := nil;
  try
    if (AHost <> '') and (APort <> '') then
      for I := 0 to ASocketPortList.Count - 1 do
      begin
        MySocketPort := ASocketPortList.Items[I];
        if (MySocketPort.Host = AHost) and
          (MySocketPort.Port = StrToInt(APort)) then
          Result := MySocketPort;
      end;
  except
    Result := nil;
  end;
end;

procedure THomeF.ClearSocketPortList;
var
  I: Integer;
  ASocketPort: TSocketPort;
  ABlackBoxRec: PBlackBoxRec;
  OK: Boolean;
begin
  try
    for I := 0 to ASocketPortList.Count - 1 do
    begin
      ASocketPort := ASocketPortList.Items[I];
      ABlackBoxRec := BlackBoxFind(ReadCounterList, ASocketPort.Host);
      OK := True;
      if ABlackBoxRec.Disabled then
        OK := IPExists(ASocketPort.Host);
      try
        try
          if OK then
            if ASocketPort.IdTelnet.Connected then
              ASocketPort.IdTelnet.Disconnect;
        except
        end;
      finally
        ASocketPort.IdTelnet.Free;
      end;
      ASocketPort.Free;
    end;
  except
  end;
end; // ClearSocketPortList

// Show all workspot details in the screen
procedure THomeF.ActionShowAllWorkspotDetails;
var
  ADrawObject: PDrawObject;
  I: Integer;
  MachineJobChanged: Boolean; // PIM-111
begin
  ShowMsg('');
  MachineJobChanged := False;
  // Show all details per workspot in the scheme on the screen
  if PSList.Count > 0 then
  begin
    MachineWorkspotsCheckJobs(PSList); // 20016447
    for I := 0 to PSList.Count - 1 do
    begin
      Application.ProcessMessages;
      ADrawObject := PSList.Items[I];
      ShowSelectionRectangle(ADrawObject.AObject);
      ShowDetails(ADrawObject);
      if not MachineJobChanged then
        MachineJobChanged := ADrawObject.AMachineJobChanged;
    end;
  end;
  // PIM-111 Call this here, because the machine-current-job can be empty
  //         till now.
  if not MachineJobChanged then
    MachineJobChanged := MachineWorkspotsCheckJobs(PSList);
  if MachineJobChanged then // PIM-111 For machine-workspot functionality
  begin
    // Show all details per workspot in the scheme on the screen
    if PSList.Count > 0 then
    begin
      MachineWorkspotsCheckJobs(PSList); // 20016447
      for I := 0 to PSList.Count - 1 do
      begin
        Application.ProcessMessages;
        ADrawObject := PSList.Items[I];
        ShowSelectionRectangle(ADrawObject.AObject);
        ShowDetails(ADrawObject);
      end;
    end;
  end;
end; // ActionShowAllWorkspotDetails

// Show details only 1 workspot
procedure THomeF.ActionShowWorkspotDetails;
var
  AReadCounterRec: PReadCounterRec;
begin
  try
  finally
    ShowMsg('');
    if Assigned(ReadCounterList) then
      if PSList.Count > 0 then
        if ReadCounterList.Count > 0 then
        begin
          if ReadCounterIndex <> -1 then
          begin
            AReadCounterRec := ReadCounterList[ReadCounterIndex];
            ShowDetails(AReadCounterRec.DrawObject);
          end;
        end
        else
        begin
          // PIM-147 When no counters are defined, then still show all workspots
          ActionShowAllWorkspotDetails;
          Application.ProcessMessages;
        end;
    ShowLinkJobDetails; // 20015376
 end;
end; // ActionShowWorkspotDetails

procedure THomeF.ActionShowStatusBB;
var
  I: Integer;
  AReadCounterRec: PReadCounterRec;
  MyColor: TColor;
  MyVisible: Boolean;
  MyFontColor: TColor; // PIM-250
  procedure HandleColors(Sender: TObject);
  var
    APanel: TPanel;
  begin
    APanel := (Sender as TPanel);
    APanel.Color := MyColor;
    APanel.Font.Color := MyFontColor;
    APanel.Visible := MyVisible;
  end;
begin
  if Assigned(ReadCounterList) then
    if ReadCounterList.Count > 0 then
      for I := 0 to 15 do
      begin
        MyColor := clBtnFace; // clPimsBlue; // clBtnFace; // PIM-250 Use clBtnFace here
        MyFontColor := clBlack; // PIM-250
        MyVisible := False;
        if I < ReadCounterList.Count then
        begin
          MyVisible := True;
          AReadCounterRec := ReadCounterList[I];
          if AReadCounterRec.BlackBoxRec.Disabled then
          begin
            MyColor := clDarkRed; // clRed; // PIM-250 Use clDarkRed here.
            MyFontColor := clWhite; // PIM-250
          end;
        end;
        case I+1 of
         1: HandleColors(pnlBB1);
         2: HandleColors(pnlBB2);
         3: HandleColors(pnlBB3);
         4: HandleColors(pnlBB4);
         5: HandleColors(pnlBB5);
         6: HandleColors(pnlBB6);
         7: HandleColors(pnlBB7);
         8: HandleColors(pnlBB8);
         9: HandleColors(pnlBB9);
        10: HandleColors(pnlBB10);
        11: HandleColors(pnlBB11);
        12: HandleColors(pnlBB12);
        13: HandleColors(pnlBB13);
        14: HandleColors(pnlBB14);
        15: HandleColors(pnlBB15);
        16: HandleColors(pnlBB16);
        end;
      end;
  Update;
end; // ActionShowStatusBB;

// TD-21429
procedure THomeF.RepositionSubForm(ASubForm: TForm);
var
  NewTop, NewLeft: Integer;
begin
  // TD-21429 Centre sub-form relative to HomeF.
  if ASubForm.Name <> 'TimeRecordingF' then
  begin
    if HomeF.Width < MaxWidth then // TD-23620
    begin
      ASubForm.Height := HomeF.Height;
      ASubForm.Width := HomeF.Width;
    end
    else
    begin
      ASubForm.Height := (HomeF.Height - HomeF.Height div 10);
      ASubForm.Width := (HomeF.Width - HomeF.Width div 10);
      if ASubForm.Height > HomeF.Height then
        ASubForm.Height := HomeF.Height;
      if ASubForm.Width > HomeF.Width then
        ASubForm.Width := HomeF.Width;
    end;
  end
  else
  begin
    if HomeF.Width < MaxWidth then // TD-23620
    begin
      ASubForm.Height := HomeF.Height;
      ASubForm.Width := HomeF.Width;
    end
    else
    begin
      ASubForm.Height := (HomeF.Height - HomeF.Height div 10);
      ASubForm.Width := (HomeF.Width - HomeF.Width div 10);
      if ASubForm.Height > HomeF.Height then
        ASubForm.Height := HomeF.Height;
      if ASubForm.Width > HomeF.Width then
        ASubForm.Width := HomeF.Width;
    end;
  end;
  // PIM-186
  if ASubForm.Constraints.MaxHeight > 0 then
    if ASubForm.Height > ASubForm.Constraints.MaxHeight then
      ASubForm.Height := ASubForm.Constraints.MaxHeight;
  if ASubForm.Constraints.MaxWidth > 0 then
    if ASubForm.Width > ASubForm.Constraints.MaxWidth then
      ASubForm.Width := ASubForm.Constraints.MaxWidth;
  NewTop := HomeF.Top + (HomeF.Height - ASubForm.Height) div 2;
  NewLeft := HomeF.Left + (HomeF.Width - ASubForm.Width) div 2;
  if NewTop < 0 then
    NewTop := 0;
  ASubForm.Top := NewTop;
  if NewLeft < 0 then
    NewLeft := 0;
  ASubForm.Left := NewLeft;
end; // RepositionSubForm

// 20014289
procedure THomeF.BitBtnRefreshClick(Sender: TObject);
begin
  inherited;
  if not IsBusy then
    MyActionRefresh
  else
    MyAction := MyActionRefresh;
//  RefreshNow := True;
end;

// 20014289
procedure THomeF.BitBtnSaveAndExitClick(Sender: TObject);
begin
  inherited;
  // Close, during this (FormClose), it will save and exit.
  if not IsBusy then
    MyActionSaveAndExit
  else
    MyAction := MyActionSaveAndExit;
end;

// 20014722
procedure THomeF.ReadExportJobChangeINIFile;
var
  Ini: TIniFile;
  Section: String;
begin
  if not FileExists(PathCheck(AppRootPath) + EXPORTJOBCHANGE_INI_FILENAME) then
    Exit;
  Ini := TIniFile.Create(PathCheck(AppRootPath) + EXPORTJOBCHANGE_INI_FILENAME);
  try
    Section := 'MAIN';
    if Ini.ValueExists(Section, 'ExportFolder') then
    begin
      ExportJobChangeFolder :=
        Ini.ReadString(Section, 'ExportFolder', ExportJobChangeFolder);
    end;
  finally
    Ini.Free;
  end;
end; // ReadExportJobChangeINIFile

// 20014722 -> If 'ExportJobChange' is True, then call this procedure.
function THomeF.ExportJobChangeToFile(
  ACurrentDrawObject: PDrawObject;
  APlantCode, AWorkspotCode, AJobCode: String;
  ATimestamp: TDateTime): Boolean;
var
  ExportPath: String;
  Line: String;
  I: Integer;
  AEmployeeRec: PEmployeeRec;
  EmployeesScannedIn: Boolean;
  function PathCheck(Path: String): String;
  begin
    if Path <> '' then
      if Path[length(Path)] <> '\' then
        Path := Path + '\';
    Result := Path;
  end; // PathCheck
  function NewFilename: String;
  begin
    // <ComputerName>_JCF_YYYYMMDDHHMMSS.txt
    Result := ORASystemDM.CurrentComputerName + '_' +
      'JCF_' + FormatDateTime('yyyymmddhhnnss', Now) + '.txt';
  end; // NewFilename
  procedure ExportLine;
  var
    TFile: TextFile;
    Exists: Boolean;
    Error1, Error2: Integer;
  begin
    // Switch off I/O error checking - PIM-42
    {$I-}
    Error1 := 0;
    try
      Exists := FileExists(ExportPath);
      AssignFile(TFile, ExportPath);
      Error1 := IOResult;
      if Error1 = 0 then
        try
          if not Exists then
            Rewrite(TFile)
          else
            Append(TFile);
          Error2 := IOResult;
          if Error2 = 0 then
            WriteLn(TFile, Line);
        except
          on E: EInOutError do
            WErrorLog(E.Message + ' (Error during ExportJobChangeToLine)');
          on E: Exception do
            WErrorLog(E.Message + ' (Error during ExportJobChangeToLine)');
        end;
    finally
      if Error1 = 0 then
        CloseFile(TFile);
    end;
    // Switch IO checking back on - PIM-42
    {$I+}
  end; // ExportLine
begin
  Result := False;
  // Do not export an empty workspotcode, which can happen when 1 employee
  // scanned out?
  // This should not be possible...
  if AWorkspotCode = '' then
    Exit;
  // When jobcode is empty, it can mean employees have scanned out.
  // We must only export an empty jobcode, when there are no employees left
  // who are currently scanned in on a job for this workspot.
  if AJobCode = '' then
  begin
    EmployeesScannedIn := False;
    // REMARK: 'ACurrentNumberOfEmployees' is not filled correctly!
    if Assigned(ACurrentDrawObject) then
    begin
      if Assigned(ACurrentDrawObject.AEmployeeList) then
        for I := 0 to ACurrentDrawObject.AEmployeeList.Count - 1 do
        begin
          AEmployeeRec := ACurrentDrawObject.AEmployeeList.Items[I];
          if AEmployeeRec.CurrentlyScannedIn then
          begin
            EmployeesScannedIn := True;
            Break;
          end;
       end;
      // There are still emplyees scanned in, so do not export it yet.
      if EmployeesScannedIn then
        Exit;
    end;
  end;
  try
    Line :=
      AWorkspotCode + ';' +
      AJobCode + ';' +
      FormatDateTime('yyyymmddhhnnss', ATimestamp);
    // Be sure the export folder exists
    if not DirectoryExists(ExportJobChangeFolder) then
      ForceDirectories(ExportJobChangeFolder);
    ExportPath := PathCheck(ExportJobChangeFolder) + NewFilename;
    ExportLine;
    Result := True;
  except
    on E: Exception do
      WErrorLog(E.Message + ' (Error during export job change).');
  end;
end; // ExportJobChangeToFile

{$IFDEF TEST}
// TEST -> Only used for getting fake counters during testing.
procedure THomeF.AddFakeCounter(AI: Integer);
var
  AReadCounterRec: PReadCounterRec;
begin
  if Assigned(ReadCounterList) then
    if ReadCounterList.Count > 0 then
      if AI < ReadCounterList.Count then
      begin
        AReadCounterRec := ReadCounterList[AI];
        // Only set a boolean, adding of fake-counter will be done later.
        AReadCounterRec.BlackBoxRec.AddFakeCounterNow := True;
{
        AReadCounterRec.BlackBoxRec.TestCounter :=
          AReadCounterRec.BlackBoxRec.TestCounter + 1;
        AReadCounterRec.BlackBoxRec.ActualCounterValue :=
          AReadCounterRec.BlackBoxRec.ActualCounterValue +
            1;
        AReadCounterRec.BlackBoxRec.ActualCounterDateTime := Now;
        AReadCounterRec.DrawObject.MemorizeCounterValue(
          AReadCounterRec.DrawObject, AReadCounterRec.BlackBoxRec);
}
      end;
end;
{$ENDIF}

procedure THomeF.pnlBB1Click(Sender: TObject);
begin
  inherited;
{$IFDEF TEST}
  AddFakeCounter(0);
{$ENDIF}
end;

procedure THomeF.pnlBB2Click(Sender: TObject);
begin
  inherited;
{$IFDEF TEST}
  AddFakeCounter(1);
{$ENDIF}
end;

procedure THomeF.pnlBB3Click(Sender: TObject);
begin
  inherited;
{$IFDEF TEST}
  AddFakeCounter(2);
{$ENDIF}
end;

procedure THomeF.pnlBB4Click(Sender: TObject);
begin
  inherited;
{$IFDEF TEST}
  AddFakeCounter(3);
{$ENDIF}
end;

procedure THomeF.pnlBB5Click(Sender: TObject);
begin
  inherited;
{$IFDEF TEST}
  AddFakeCounter(4);
{$ENDIF}
end;

procedure THomeF.pnlBB6Click(Sender: TObject);
begin
  inherited;
{$IFDEF TEST}
  AddFakeCounter(5);
{$ENDIF}
end;

procedure THomeF.pnlBB7Click(Sender: TObject);
begin
  inherited;
{$IFDEF TEST}
  AddFakeCounter(6);
{$ENDIF}
end;

procedure THomeF.pnlBB8Click(Sender: TObject);
begin
  inherited;
{$IFDEF TEST}
  AddFakeCounter(7);
{$ENDIF}
end;

procedure THomeF.pnlBB9Click(Sender: TObject);
begin
  inherited;
{$IFDEF TEST}
  AddFakeCounter(8);
{$ENDIF}
end;

procedure THomeF.pnlBB10Click(Sender: TObject);
begin
  inherited;
{$IFDEF TEST}
  AddFakeCounter(9);
{$ENDIF}
end;

procedure THomeF.pnlBB11Click(Sender: TObject);
begin
  inherited;
{$IFDEF TEST}
  AddFakeCounter(10);
{$ENDIF}
end;

procedure THomeF.pnlBB12Click(Sender: TObject);
begin
  inherited;
{$IFDEF TEST}
  AddFakeCounter(11);
{$ENDIF}
end;

procedure THomeF.pnlBB13Click(Sender: TObject);
begin
  inherited;
{$IFDEF TEST}
  AddFakeCounter(12);
{$ENDIF}
end;

procedure THomeF.pnlBB14Click(Sender: TObject);
begin
  inherited;
{$IFDEF TEST}
  AddFakeCounter(13);
{$ENDIF}
end;

procedure THomeF.pnlBB15Click(Sender: TObject);
begin
  inherited;
{$IFDEF TEST}
  AddFakeCounter(14);
{$ENDIF}
end;

procedure THomeF.pnlBB16Click(Sender: TObject);
begin
  inherited;
{$IFDEF TEST}
  AddFakeCounter(15);
{$ENDIF}
end;

// 20015376
procedure THomeF.ShowLinkJobDetails;
var
  I: Integer;
  ADrawObject: PDrawObject;
  ALinkJobRec: PLinkJobRec;
begin
  // 20015376 Show details for link jobs here
  if Assigned(LinkJobList) then
  begin
    for I := 0 to LinkJobList.Count - 1 do
    begin
      ALinkJobRec := LinkJobList.Items[I];
      ADrawObject := FindDrawObject(PSList, ALinkJobRec.PlantCode,
        ALinkJobRec.WorkspotCode);
      // PIM-12 It does not show employees after 1 'cycle'???
{      if ADrawObject.AEmployeeList.Count > 0 then
        if ADrawObject.AEmployeeDisplayList.Count = 0 then
          ADrawObject.ARecreateEmployeeDisplayList := True; }
      ShowDetails(ADrawObject);
    end;
  end;
end; // ShowLinkJobDetails

// 20015406
procedure THomeF.ChangeModeByDrawObject(ADrawObject: PDrawObject);
begin
  // 20015406.90
  if Assigned(ADrawObject.AWorkspotTimeRec) then
  begin
    case ADrawObject.AEffPeriodMode of
    epCurrent:
      ADrawObject.AWorkspotTimeRec.AModeButton.Caption := SPimsModeButtonCurrent;
    epSince  :
      ADrawObject.AWorkspotTimeRec.AModeButton.Caption := SPimsModeButtonSince;
    epShift  :
      ADrawObject.AWorkspotTimeRec.AModeButton.Caption := SPimsModeButtonShift;
    end;
  end;
end;

// 20014450.50
procedure THomeF.MyActionRealTimeEfficiency;
begin
  try
    MyAction := nil;
    ActionRealTimeEfficiency; // 20014450.50 // PIM-12
  finally
  end;
end; // MyActionRealTimeEfficiency

// PIM-90
procedure THomeF.MyActionReadExternalData;
begin
  try
    MyAction := nil;
    DataReadAllForOneDrawObject;
  finally
  end;
end; // MyActionReadExternalData

procedure THomeF.ListButtonClick(Sender: TObject);
begin
  if tlbtnEdit.Down then // 20014450.50 Not in Edit-mode!
    Exit;
  if (Sender is UPimsConst.TBitBtn) then
  begin
    CurrentDrawObject := ThisDrawObject((Sender as UPimsConst.TBitBtn).Tag);
    CurrentDrawObject.AListEmployeeNumber := (Sender as UPimsConst.TBitBtn).MyTag;
    if not IsBusy then
      MyActionListButtonClick
    else
      MyAction := MyActionListButtonClick;
  end;
end;

procedure THomeF.MyActionListButtonClick;
begin
  if tlbtnEdit.Down then // 20014450.50 Not in Edit-mode!
    Exit;
  MyAction := nil;
  // PIM-12.2
  // No need to switch off/on the timer
//  if not tlbtnEdit.Down then
//    MainTimerSwitch(False);
  try
    // PIM-12.3 try-except added
    try
      if Assigned(CurrentDrawObject) then
      begin
        DialogEmpListViewF.EmployeeNumber := CurrentDrawObject.AListEmployeeNumber;
        DialogEmpListViewF.EmployeeName :=
          CurrentDrawObject.DetermineEmployeeName(CurrentDrawObject.AListEmployeeNumber);
        DialogEmpListViewF.ShowModal;
      end;
    except
      // Ignore error
    end;
  finally
//    if not tlbtnEdit.Down then
//      MainTimerSwitch(True);
  end;
end; // MyActionListButtonClick

procedure THomeF.GraphButtonClick(Sender: TObject);
begin
  if tlbtnEdit.Down then // 20014450.50 Not in Edit-mode!
    Exit;
  if (Sender is UPimsConst.TBitBtn) then
  begin
    CurrentDrawObject := ThisDrawObject((Sender as UPimsConst.TBitBtn).Tag);
    CurrentDrawObject.AListEmployeeNumber := (Sender as UPimsConst.TBitBtn).MyTag;
    if not IsBusy then
      MyActionGraphButtonClick
    else
      MyAction := MyActionGraphButtonClick;
  end;
end;

procedure THomeF.MyActionGraphButtonClick;
begin
  if tlbtnEdit.Down then // 20014450.50 Not in Edit-mode!
    Exit;
  MyAction := nil;
  // PIM-12.2
  // No need to switch off/on the timer
//  if not tlbtnEdit.Down then
//    MainTimerSwitch(False);
  try
    // PIM-12.3 try-except added
    try
      if Assigned(CurrentDrawObject) then
        ShowChartClick(nil);
    except
      // Ignore error
    end;
  finally
//    if not tlbtnEdit.Down then
//      MainTimerSwitch(True);
  end; // MyActionGraphButtonClick
end;

procedure THomeF.ShowEmployees1Click(Sender: TObject);
begin
  inherited;
  if tlbtnEdit.Down then // 20014450.50 Not in Edit-mode!
    Exit;
  if (Sender is TLabel) then
  begin
    CurrentDrawObject := ThisDrawObject((Sender as TLabel).Tag);
    if not IsBusy then
      MyActionShowEmployeeListClick
    else
      MyAction := MyActionShowEmployeeListClick;
  end;
end; // ShowEmployees1Click

procedure THomeF.MyActionShowEmployeeListClick;
begin
  if tlbtnEdit.Down then // 20014450.50 Not in Edit-mode!
    Exit;

  MyAction := nil;
  // PIM-12.2
  // No need to switch off/on the timer
//  if not tlbtnEdit.Down then
//    MainTimerSwitch(False);
  try
    // PIM-12.3 try-except added
    try
      if Assigned(CurrentDrawObject) then
        ShowEmployees(ShowEmployeesOnWorkspot);
    except
      // Ignore error
    end;
  finally
//    if not tlbtnEdit.Down then
//      MainTimerSwitch(True);
  end;
end; // MyActionShowEmployeeListClick

// 20014450.50
procedure THomeF.actPictureExecute(Sender: TObject);
var
  Path, OtherPath: String;
  WorkFilename: String;
begin
  inherited;
  // Only possible in Edit-mode
  if not tlbtnEdit.Down then
    Exit;
  Path := GetCurrentDir;
  try
    OpenDialog1.InitialDir := PicturePath;
    OpenDialog1.Filename := '';
    OpenDialog1.Filter := 'Picture-files|*.bmp';
    if OpenDialog1.Execute then
    begin
      WorkFilename := ExtractFileName(OpenDialog1.Filename);
      // If file came from another path, copy the file to
      // PicturePath
      OtherPath := ExtractFilePath(OpenDialog1.Filename);
      if OtherPath <> PicturePath then
        CopyFile(PChar(OtherPath + WorkFilename),
          PChar(PicturePath + WorkFilename), False);
      AddObjectToList(otPicture,
        CreatePicture(WorkFilename), '', '', '', WorkFilename, False, 0,
        pstNoTimeRec, mtNone, '', '', 0);
    end;
    SetCaption;
  finally
    SetCurrentDir(Path);
  end;
end; // actPictureExecute

// 20014450.50
function THomeF.CreatePicture(AImageName: String): TImage;
var
  MyImage: TImage;
begin
  MyImage := TImage.Create(Application);
  MyImage.Top := 50;
  MyImage.Left := 50;
  MyImage.Height := Trunc(200 * WorkspotScale / 100);
  MyImage.Width := Trunc(400 * WorkspotScale / 100);
  MyImage.Tag := NewItemIndex;
  MyImage.Parent := pnlDraw;
  MyImage.OnClick := ImageClick;
  MyImage.OnDblClick := ActionShowChangeProperties;
  MyImage.OnMouseDown := ImageMouseDown;
  MyImage.OnMouseMove := ImageMouseMove;
  MyImage.OnMouseUp := ImageMouseUp;
  MyImage.Stretch := True;
  MyImage.Visible := False;
  try
    MyImage.Picture.LoadFromFile(PicturePath + AImageName);
    MyImage.Height := MyImage.Picture.Height;
    MyImage.Width := MyImage.Picture.Width;
  except
  end;
  Result := MyImage;
end; // CreatePicture

// PIM-12.3
procedure THomeF.MyGlobalExceptionHandler(Sender: TObject; E: Exception);
begin
  WErrorLog(E.Message);
end;

// PIM-12
procedure THomeF.ActionRealTimeEfficiency;
begin
  // This will do: WriteToDB + Write to realtime-eff-table.
  ActionWriteToDB;
end; // ActionRealTimeEfficiency

// PIM-125
procedure THomeF.ErrorLogReducer;
var
  ASocketPort: TSocketPort;
  I: Integer;
begin
  try
    ORASystemDM.LogErrors := EmployeesCurrentlyScannedInCheck(PSList);
    if Assigned(ASocketPortList) then
      for I := 0 to ASocketPortList.Count - 1 do
      begin
        ASocketPort := ASocketPortList.Items[I];
        if ORASystemDM.LogErrors then
          ASocketPort.MyLog := WErrorLog
        else
          ASocketPort.MyLog := nil;
      end;
  except
  end;
end; // ErrorLogReducer

// PIM-151
function THomeF.CreateGhostImage: TImage;
var
  AnImage: TImage;
begin
  AnImage := TImage.Create(Application);
  AnImage.Parent := pnlDraw;
  AnImage.Left := 50;
  AnImage.Top := 50;
  AnImage.Canvas.Brush.Style := bsClear;
  AnImage.Picture.Assign(ImageGhost.Picture);
  AnImage.Width := Trunc(IMAGE_SIZE * WorkspotScale / 100);
  AnImage.Height := Trunc(IMAGE_SIZE * WorkspotScale / 100);
  AnImage.Stretch := True;
  AnImage.Tag := NewItemIndex;
  AnImage.OnClick := ImageClick;
  AnImage.OnDblClick := ActionShowChangeProperties;
  AnImage.OnMouseDown := ImageMouseDown;
  AnImage.OnMouseMove := ImageMouseMove;
  AnImage.OnMouseUp := ImageMouseUp;
  AnImage.Visible := False;
  Result := AnImage;
end; // CreateGhostImage

// PIM-151
procedure THomeF.GhostImageDisable;
var
  I: Integer;
  MyDrawObject: PDrawObject;
begin
  for I := 0 to PSList.Count - 1 do
  begin
    MyDrawObject := PSList.Items[i];
    if not MyDrawObject.ABigEffMeters then
      if (MyDrawObject.AObject is TImage) then
      begin
        if Assigned(MyDrawObject.ADefaultImage) then
        begin
          MyDrawObject.AObject := MyDrawObject.ADefaultImage;
         (MyDrawObject.AObject as TImage).Visible := True;
        end;
      end;
    if Assigned(MyDrawObject.AGhostImage) then
      MyDrawObject.AGhostImage.Visible := False;
  end;
  Application.ProcessMessages;
end; // GhostImageDisable

// PIM-213
procedure THomeF.actWSEmpEffExecute(Sender: TObject);
begin
  inherited;
  if DialogWorkspotSelectF.RootPath = '' then
    DialogWorkspotSelectF.RootPath := ImagePath;
  DialogWorkspotSelectF.ActionWorkspot := True;
  DialogWorkspotSelectF.NoPicture := True;
  if DialogWorkspotSelectF.ShowModal = mrOK then
    AddObjectToList(otImage,
      nil,
      DialogWorkspotSelectF.PlantCode,
      DialogWorkspotSelectF.WorkspotCode,
      DialogWorkspotSelectF.WorkspotDescription,
      DialogWorkspotSelectF.ImageName,
      False,
      0,
      pstWSEmpEff,
      mtNone,
      '',
      '',
      0
      );
end; // actWSEmpEffExecute

// PIM-213
function THomeF.WSEmpEffCreate(ADrawObject: PDrawObject;
  AShortName: String): TpnlWSEmpEff;
begin
  Result :=
    TpnlWSEmpEff.WSCreate(pnlDraw, ADrawObject.APlantCode,
      ADrawObject.AWorkspotCode, AShortName, WorkspotScale, FontScale,
      ImageClick, ImageMouseDown, ImageMouseMove, ImageMouseUp,
      (ADrawObject.AObject as TImage).Tag,
      True);
  CurrentPeriod := WS_EFF_CURRENTPERIOD;
  ADrawObject.ARealTimeIntervalMinutes := CurrentPeriod;
  SetCurrentPeriod(CurrentPeriod);
  ADrawObject.AEffPeriodMode := epSince; // Must always set to this (Today)!
  if Assigned(ADrawObject.AWorkspotTimeRec) then
  begin
    with ADrawObject.AWorkspotTimeRec.AJobButton do
    begin
      Parent := Result.pnlJobButton;
      Top := 0;
      Left := 0;
      Height := Result.pnlJobButton.Height;
      Width := Result.pnlJobButton.Width;
      Align := alClient;
      Visible := True;
    end;
    with ADrawObject.AWorkspotTimeRec.AInButton do
    begin
      Parent := Result.pnlInButton;
      Top := 0;
      Left := 0;
      Height := Result.pnlInButton.Height;
      Width := Result.pnlInButton.Width;
      Align := alClient;
      Visible := True;
    end;
  end;
  Result.Tag := (ADrawObject.AObject as TImage).Tag;
  Result.OnClick := ImageClick;
  Result.OnMouseDown := ImageMouseDown;
  Result.OnMouseMove := ImageMouseMove;
  Result.OnMouseUp := ImageMouseUp;
end; // WSEmpEffCreate

// PIM-213
procedure THomeF.WSEmpEffEditMode(AOn: Boolean);
var
  I: Integer;
  MyDrawObject: PDrawObject;
begin
  for I := 0 to PSList.Count - 1 do
  begin
    MyDrawObject := PSList.Items[i];
    if MyDrawObject.AProdScreenType = pstWSEmpEff then
    begin
      if Assigned(MyDrawObject.ApnlWSEmpEff) then
      begin
        MyDrawObject.ApnlWSEmpEff.EditMode(AOn);
        if AOn then
          MyDrawObject.ApnlWSEmpEff.pnlWSSummary.Caption :=
            MyDrawObject.ApnlWSEmpEff.lblWorkspot.Caption;
      end;
    end;
  end;
end; // WSEmpEffEditMode

// PIM-213
procedure THomeF.ActionWSEmpEffRefresh;
var
  I: Integer;
  MyDrawObject: PDrawObject;
begin
  for I := 0 to PSList.Count - 1 do
  begin
    MyDrawObject := PSList.Items[i];
    if MyDrawObject.AProdScreenType = pstWSEmpEff then
    begin
      if Assigned(MyDrawObject.ApnlWSEmpEff) then
      begin
        WorkspotEmpEffDM.WorkspotEmpEffRefresh;
        Break;
      end;
    end;
  end;
end; // ActionWSEmpEffRefresh

end.
