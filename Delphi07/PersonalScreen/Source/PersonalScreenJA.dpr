(*
  Changes:
    RV043.1. Auto login option.
    - When argument is used like -u:pims then
      it should start with login-name e.g. 'pims', and get the password
      automatically from database.
    MRA:JUN-2010. RV063.4. Order 550478. Personal Screen.
    - Addition of Machine/Workspot + Time recording.
    MRA:21-JUN-2012 20013176
    - Timerecording & card reader:
      - This asks for an employee-number when the card-ID
        is not known yet.
      - The above must default NOT be done, but only with a parameter
        '-A' it must ask for the employee-number.
    MRA:13-APR-2015 20016449
    - Related to this order: Add ClientName parameter
*)

program PersonalScreenJA;

uses
  Forms,
  Windows,
  SysUtils,
  Dialogs,
  Classes,
  BasePimsFRM in '..\..\Pims Shared\BasePimsFRM.pas' {BasePimsForm},
  BaseTopMenuFRM in '..\..\Pims Shared\BaseTopMenuFRM.pas' {BaseTopMenuForm},
  BaseTopLogoFRM in '..\..\Pims Shared\BaseTopLogoFRM.pas' {BaseTopLogoForm},
  BaseDialogFRM in '..\..\Pims Shared\BaseDialogFRM.pas' {BaseDialogForm},
  AboutFRM in '..\..\Pims Shared\AboutFRM.pas' {AboutForm},
  OrderInfoFRM in '..\..\Pims Shared\OrderInfoFRM.pas' {OrderInfoForm},
  UPimsConst in '..\..\Pims Shared\UPimsConst.pas',
  UPimsMessageRes in '..\..\Pims Shared\UPimsMessageRes.pas',
  UPimsVersion in '..\..\Pims Shared\UPimsVersion.pas',
  UScannedIDCard in '..\..\Pims Shared\UScannedIDCard.pas',
  SplashFRM in '..\..\Pims Shared\SplashFRM.pas' {SplashForm},
  ORASystemDMT in '..\..\Pims Shared\ORASystemDMT.pas' {ORASystemDM: TDataModule},
  CalculateTotalHoursDMT in '..\..\Pims Shared\CalculateTotalHoursDMT.pas' {CalculateTotalHoursDM, TDataModule},
  UGlobalFunctions in '..\..\Pims Shared\UGlobalFunctions.pas',
  DialogSelectionFRM in '..\..\Pims Shared\DialogSelectionFRM.PAS' {DialogSelectionForm},
  DialogLoginDMT in '..\..\Pims Shared\DialogLoginDMT.pas' {DialogLoginDM: TDataModule},
  DialogLoginFRM in '..\..\Pims Shared\DialogLoginFRM.pas' {DialogLoginF},
  ULoginConst in '..\..\Pims Shared\ULoginConst.pas',
  EncryptIt in '..\..\Pims Shared\EncryptIt.pas',
  GlobalDMT in '..\..\Pims Shared\GlobalDMT.pas' {GlobalDM: TDataModule},
  HomeFRM in 'HomeFRM.pas' {HomeF},
  DialogImagePropertiesFRM in 'DialogImagePropertiesFRM.pas' {DialogImagePropertiesF},
  DialogWorkspotSelectFRM in 'DialogWorkspotSelectFRM.pas' {DialogWorkspotSelectF},
  DialogShowAllEmployeesFRM in '..\..\Pims Shared\DialogShowAllEmployeesFRM.pas' {DialogShowAllEmployeesF},
  DialogPrintChartFRM in 'DialogPrintChartFRM.pas' {DialogPrintChartF},
  DialogSettingsFRM in 'DialogSettingsFRM.pas' {DialogSettingsF},
  DialogChartSettingsFRM in 'DialogChartSettingsFRM.pas' {DialogChartSettingsF},
  ProductionScreenDMT in 'ProductionScreenDMT.pas' {ProductionScreenDM: TDataModule},
  ProductionScreenWaitFRM in 'ProductionScreenWaitFRM.pas' {ProductionScreenWaitF},
  DialogDepartmentSelectFRM in 'DialogDepartmentSelectFRM.pas' {DialogDepartmentSelectF},
  PlanScanEmpDMT in '..\..\Pims Shared\PlanScanEmpDMT.pas' {PlanScanEmpDM: TDataModule},
  DialogSelectModeFRM in 'DialogSelectModeFRM.pas' {DialogSelectModeF},
  DialogLegendaFRM in 'DialogLegendaFRM.pas' {DialogLegendaF},
  BasePimsSerialFRM in '..\..\Pims Shared\BasePimsSerialFRM.pas' {BasePimsSerialForm},
  TimeRecordingDMT in '..\..\TimeRecording\Source\TimeRecordingDMT.pas' {TimeRecordingDM: TDataModule},
  TimeRecordingFRM in '..\..\TimeRecording\Source\TimeRecordingFRM.pas' {TimeRecordingF},
  SelectorFRM in '..\..\TimeRecording\Source\SelectorFRM.pas' {SelectorF},
  PersonalScreenDMT in '..\..\Pims Shared\PersonalScreenDMT.pas' {PersonalScreenDM: TDataModule},
  DialogChangeJobDMT in '..\..\Pims Shared\DialogChangeJobDMT.pas' {DialogChangeJobDM: TDataModule},
  DialogChangeJobFRM in '..\..\Pims Shared\DialogChangeJobFRM.pas' {DialogChangeJobF},
  DialogSelectDownTypeFRM in '..\..\Pims Shared\DialogSelectDownTypeFRM.pas' {DialogSelectDownTypeF},
  DialogShowStatusInfoFRM in 'DialogShowStatusInfoFRM.pas' {DialogShowStatusInfoF},
  UProductionScreen in 'UProductionScreen.pas',
  UPersonalScreen in 'UPersonalScreen.pas',
  USocketPort in '..\..\Pims Shared\USocketPort.pas',
  DialogDebugInfoFRM in 'DialogDebugInfoFRM.pas' {DialogDebugInfoF},
  PCSCRaw in '..\..\TimeRecording\Source\PCSCRaw.pas',
  PcscDef in '..\..\TimeRecording\Source\PcscDef.pas',
  Reader in '..\..\TimeRecording\Source\Reader.pas',
  UPCSCCardReader in '..\..\TimeRecording\Source\UPCSCCardReader.pas',
  DialogAskEmployeeNumberFRM in '..\..\TimeRecording\Source\DialogAskEmployeeNumberFRM.pas' {DialogAskEmployeeNumberF},
  UProductionScreenDefs in '..\..\Pims Shared\UProductionScreenDefs.pas',
  UTranslateTimeRec in '..\..\TimeRecording\Source\UTranslateTimeRec.pas',
  UTranslateStringsTimeRec in '..\..\TimeRecording\Source\UTranslateStringsTimeRec.pas',
  UTranslatePS in 'UTranslatePS.pas',
  UTranslateStringsPS in 'UTranslateStringsPS.pas',
  UTranslateShared in '..\..\Pims Shared\UTranslateShared.pas',
  UTranslateStringsShared in '..\..\Pims Shared\UTranslateStringsShared.pas',
  DialogObjectSettingsFRM in 'DialogObjectSettingsFRM.pas' {DialogObjectSettingsF},
  UTimeZone in '..\..\Pims Shared\UTimeZone.pas',
  ColorButton in '..\..\Pims Shared\ColorButton.pas',
  DialogEmpListViewFRM in '..\..\Pims Shared\DialogEmpListViewFRM.pas' {DialogEmpListViewF},
  DialogEmpGraphFRM in '..\..\Pims Shared\DialogEmpGraphFRM.pas' {DialogEmpGraphF},
  RealTimeEffDMT in '..\..\Pims Shared\RealTimeEffDMT.pas' {RealTimeEffDM: TDataModule},
  UTranslateRealTimeEff in '..\..\Pims Shared\UTranslateRealTimeEff.pas',
  UTranslateStringsRealTimeEff in '..\..\Pims Shared\UTranslateStringsRealTimeEff.pas',
  DialogEnterJobCommentFRM in '..\..\Pims Shared\DialogEnterJobCommentFRM.pas' {DialogEnterJobCommentF},
  EnterJobCommentDMT in '..\..\Pims Shared\EnterJobCommentDMT.pas' {EnterJobCommentDM: TDataModule},
  DialogMessageFRM in '..\..\TimeRecording\Source\DialogMessageFRM.pas' {DialogMessageF},
  UGhostCount in 'UGhostCount.pas',
  HybridDMT in '..\..\Pims Shared\HybridDMT.pas' {HybridDM: TDataModule},
  UWorkspotEmpEff in '..\..\Pims Shared\UWorkspotEmpEff.pas',
  WorkspotEmpEffDMT in '..\..\Pims Shared\WorkspotEmpEffDMT.pas' {WorkspotEmpEffDM: TDataModule},
  DialogShiftCheckDMT in '..\..\Pims Shared\DialogShiftCheckDMT.pas' {DialogShiftCheckDM: TDataModule},
  DialogShiftCheckFRM in '..\..\Pims Shared\DialogShiftCheckFRM.pas' {DialogShiftCheckForm};

{$R *.RES}

// How to get an alternative CLIENTNAME based on param?
function NewClientName: String;
var
  I, IPos: Integer;
  Param1, PStr: String;
  function MySetEnvironmentVariable(AName, AValue: String): Boolean;
  begin
    Result := SetEnvironmentVariable(PChar(AName), PChar(AValue));
  end;
begin
  Result := '';
  Param1 := '-C:';
  for I := 1 to ParamCount do
  begin
    PStr := UpperCase(ParamStr(I));
    IPos := Pos(Param1, PStr);
    if IPos > 0 then
    begin
      Result := Copy(PStr, IPos + Length(Param1), Length(Pstr));
      Break;
    end;
  end;
  if Result <> '' then
    MySetEnvironmentVariable('CLIENTNAME', Result);
end;

// RV043.1. Auto login option. get the user from parameter, for example:
//          -u:pims
function AutoLoginUser: String;
var
  I, IPos: Integer;
  Param1, PStr: String;
begin
  Result := '';
  Param1 := '-U:';
  for I := 1 to ParamCount do
  begin
    PStr := UpperCase(ParamStr(I));
    IPos := Pos(Param1, PStr);
    if IPos > 0 then
    begin
      Result := Copy(PStr, IPos + Length(Param1), Length(Pstr));
      Break;
    end;
  end;
end;

// 20013176
function AutoAskForEmployeeNumber: Boolean;
var
  I, IPos: Integer;
  Param1, PStr: String;
begin
  Result := False;
  Param1 := '-A';
  for I := 1 to ParamCount do
  begin
    PStr := UpperCase(ParamStr(I));
    IPos := Pos(Param1, PStr);
    if IPos > 0 then
    begin
      Result := True;
      Break;
    end;
  end;
end; // AutoAskForEmployeeNumber

begin
  Application.Initialize;
  // set property in order to not update database before login dialog
  // when SystemDM is created
  Application.Title := 'ORCL - PIMS - PERSONAL SCREEN';
  NewClientName;
  Application.CreateForm(TORASystemDM, ORASystemDM);
  Application.CreateForm(THybridDM, HybridDM);
  Application.CreateForm(TWorkspotEmpEffDM, WorkspotEmpEffDM);
  Application.CreateForm(TDialogShiftCheckDM, DialogShiftCheckDM);
  ORASystemDM.AppName := APPNAME_PS;
  if not Application.Terminated then
  begin
    DialogLoginF := TDialogLoginF.Create(Application);
    ATranslateHandlerPS.TranslateInit;
    // RV043.1. Auto Login.
    if (AutoLoginUser <> '') then
    begin
      DialogLoginF.AutoLogin := True;
      DialogLoginF.AutoLoginUser := AutoLoginUser;
    end
    else
    begin
      DialogLoginF.AutoLogin := False;
      DialogLoginF.AutoLoginUser := '';
    end;
    //verifyPassword and login as SYSDBA
    if DialogLoginF.VerifyPassword then
    begin
      Application.CreateForm(TEnterJobCommentDM, EnterJobCommentDM);
      Application.CreateForm(TRealTimeEffDM, RealTimeEffDM);
      Application.CreateForm(TCalculateTotalHoursDM, CalculateTotalHoursDM);
      Application.CreateForm(TGlobalDM, GlobalDM);
      Application.CreateForm(TPersonalScreenDM, PersonalScreenDM);
      Application.CreateForm(TDialogChangeJobDM, DialogChangeJobDM);
      Application.CreateForm(TProductionScreenDM, ProductionScreenDM);
      Application.CreateForm(TPlanScanEmpDM, PlanScanEmpDM);
      Application.CreateForm(TTimeRecordingDM, TimeRecordingDM);
      TimeRecordingDM.PersonalScreen := True;
      Application.CreateForm(THomeF, HomeF);
      DialogAskEmployeeNumberF := nil;
      if AutoAskForEmployeeNumber then
      begin
        Application.CreateForm(TDialogAskEmployeeNumberF,
          DialogAskEmployeeNumberF);
        DialogAskEmployeeNumberF.Visible := False;
      end;
      Application.CreateForm(TDialogShiftCheckForm, DialogShiftCheckForm);
      DialogShiftCheckForm.Visible := False;
      ATranslateHandlerTimeRec.TranslateAll;
      ATranslateHandlerPS.TranslateAll;
      ATranslateHandlerShared.TranslateAll;
      ATranslateHandlerRealTimeEff.TranslateAll;
    end;
    Application.Run;
  end;
end.
