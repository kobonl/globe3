(*
  MRA:23-JAN-2014 TD-24046
  - Translate issues: Use a different method to translate.
    - Do NOT do this in separate files where the translation is done
      hard-coded!
    - New method:
      - Use 1 resource-strings-file that contains the translation-strings.
      - Use 1 translate-unit that assigns these strings to the properties
        in the forms.
      - In this way there is only 1 form that can be translated in multiple
        languages when the application is run.
      - The project-file defines the language using a condition.
  MRA:28-APR-2014 TD-24046 (rework)
  - Addition of 'DialogSelectDownType' that was missing.
  - Addition of init. + final. section. 
  MRA:1-JUN-2016 PIM-186
  - When screen is 640x480 be sure all fits within this size.
  MRA:28-SEP-2016 PIM-223
  - Show/hide employee efficiency on scheme-level.
*)
unit UTranslatePS;

interface

type
  TTranslateHandlerPS = class
  private
  public
    constructor Create; overload;
    destructor Destroy; override;
    procedure TranslateAll;
    procedure TranslateDialogSettings;
    procedure TranslateDialogSelectDownType;
    procedure TranslateInit;
  end;

var
  ATranslateHandlerPS: TTranslateHandlerPS;

implementation

uses
  UTranslateStringsPS, UTranslateStringsShared, HomeFRM, DialogSettingsFRM,
  ProductionScreenWaitFRM, DialogChangeJobFRM, BaseDialogFRM, DialogLoginFRM,
  DialogSelectionFRM, DialogShowAllEmployeesFRM, BaseTopMenuFRM,
  DialogWorkspotSelectFRM, DialogSelectDownTypeFRM, DialogObjectSettingsFRM;

{ TTranslateHandlerPS }

constructor TTranslateHandlerPS.Create;
begin
  inherited;
//
end;

destructor TTranslateHandlerPS.Destroy;
begin
//
  inherited;
end;

procedure TTranslateHandlerPS.TranslateDialogSettings;
begin
  if Assigned(DialogSettingsF) then
    with DialogSettingsF do
    begin
      Caption := STransSettings;
      lblMessage.Caption := STransSettings;
      GroupBox1.Caption := STransGlobalSettings;
//      GroupBox3.Caption := STransSoundAlarm;
//      Label1.Caption := STransSoundFilename;
//      Label2.Caption := STransUseSoundAlarm;
//      btnBrowse.Caption := STransBrowse;
      GroupBox6.Caption := STransRefrehTimeInterval;
      Label6.Caption := STransInterval;
      Label7.Caption := STransSeconds;
      GroupBox2.Caption := STransSchemeSettings;
      GroupBox4.Caption := STransWorkspotScale;
      Label3.Caption := STransPercentage;
      GroupBox5.Caption := STransEffMeters;
      Label4.Caption := STransUsePeriod;
      rGrpUsePeriod.Items.Strings[0] := STransCurrent;
      rGrpUsePeriod.Items.Strings[1] := STransSince;
      rGrpUsePeriod.Items.Strings[2] := STransShift;
      grpBxSinceTime.Caption := STransSince;
      Label5.Caption := STransTime;
      GroupBox7.Caption := STransFontScale;
      Label8.Caption := STransPercentage;
      GroupBox8.Caption := STransPersonalScreenSettings;
      Label9.Caption := STransDatacolInterval;
      Label10.Caption := STransMSecs;
      Label11.Caption := STransEffBasedOn;
      Label12.Caption := STransUpdateDBInterval;
      Label13.Caption := STransMinutes;
      Label14.Caption := STransReadDelay;
      Label15.Caption := STransMSecs;
      Label16.Caption := STransRefreshEmployees;
      Label17.Caption := STransReadTimeout;
      Label18.Caption := STransMSecs;
      Label19.Caption := STransCurrentPeriod;
      Label20.Caption := STransMinutes;
      rgrpEffQuantityTime.Items.Strings[0] := STransQuantity;
      rgrpEffQuantityTime.Items.Strings[1] := STransTime;
      cBoxRefreshEmployees.Caption := STransRefreshEmployees;
      btnOk.Caption := STransOK;
      btnCancel.Caption := STransCancel;
      GroupBox3.Caption := STransEffColorBoundaries;
      lblRed.Caption := STransRed;
      lblOrange.Caption := STransOrange;
      gBoxShowEmpInfo.Caption := STransShow;
      cBoxShowEmpInfo.Caption := STransEmpInfo;
      cBoxShowEmpEff.Caption := STransEmpEff;
    end;
end;

procedure TTranslateHandlerPS.TranslateDialogSelectDownType;
begin
  if Assigned(DialogSelectDownTypeF) then
    with DialogSelectDownTypeF do
    begin
      Caption := STransSelectDownType;
      GroupBox1.Caption := STransSelectDownType;
      btnMechanicalDown.Caption := STransMechDown;
      btnNoMerchandize.Caption := STransNoMerch;
      btnOk.Caption := STransOK;
      btnCancel.Caption := STransCancel;
    end;
end;

procedure TTranslateHandlerPS.TranslateAll;
begin
  if Assigned(BaseTopMenuForm) then
    with BaseTopMenuForm do
    begin
      HelpAboutABSAct.Caption := STransAboutABSGroup;
      HelpABSHomePageAct.Caption := STransABSGroupHomePage;
      HelpOrderingInfoAct.Caption := STransOrderInfo;
      HelpPimsHomePageAct.Caption := STransPimsHomePage;
      HelpContentsAct.Caption := STransContents;
      HelpIndexAct.Caption := STransIndex;
      FileExit1.Caption := STransExit1;
      FileSettingsAct.Caption := STransSettings;
      FilePrintAct.Caption := STransPrint;
      FileClose.Caption := STransExit2;
      miFile1.Caption := STransFile;
      miHelp1.Caption := STransHelp;
    end;
  if Assigned(HomeF) then
    with HomeF do
    begin
      Caption := STransPimsPersonalScreen;
      lblHr.Caption := STransHR;
      lblPcs.Caption := STransPCS;
      lblNormPcs.Caption := STransNormPCS;
      BitBtnSaveAndExit.Caption := STransSaveExit;
      BitBtnRefresh.Caption := STransRefresh;
      ShowAllEmployees1.Caption := STransShowAllEmp;
      ShowEmployeesonwrongWokspot1.Caption := STransShowEmpWrongWorkspot;
      ShowEmployeesnotscannedin1.Caption := STransShowEmpNotScannedIn;
      ShowEmployeesabsentwithreason1.Caption := STransShowEmpAbsentWithReason;
      ShowEmployeesabsentwithoutreason1.Caption := STransShowEmpAbsentWithoutReason;
      ShowEmployeeswithFirstAid1.Caption := STransShowEmpWithFirstAid;
      ShowEmployeestoolate1.Caption := STransShowEmpTooLate;
      ShowChart.Caption := STransShowChart;
      ShowEmployees1.Caption := STransShowEmployees;
      actWorkspot.Caption := STransWorkspot;
      actWorkspot.Hint := STransWorkspot;
      actEffiMeter.Caption := STransEfficiencyMeter;
      actEffiMeter.Hint := STransEfficiencyMeter;
      actLineHorz.Caption := STransLineHorz;
      actLineHorz.Hint := STransLineHorz;
      actLineVert.Caption := STransLineVert;
      actLineVert.Hint := STransLineVert;
      actRectangle.Caption := STransRectangle;
      actRectangle.Hint := STransRectangle;
      actPuppetBox.Caption := STransEmployeeOverview;
      actPuppetBox.Hint := STransEmployeeOverview;
      actDelete.Caption := STransDelete;
      actDelete.Hint := STransDelete;
      actSave.Caption := STransSave;
      actSave.Hint := STransSave;
      actEdit.Caption := STransEdit;
      actEdit.Hint := STransEdit;
      actNew.Caption := STransNew;
      actNew.Hint := STransNew;
      actOpen.Caption := STransOpen;
      actOpen.Hint := STransOpen;
      actSaveAs.Caption := STransSaveAs;
      actSaveAs.Hint := STransSaveAs;
      HelpLegendaAct.Caption := STransLegenda;
      HelpLegendaAct.Hint := STransLegenda;
      actShowStatusInfo.Caption := STransShowStatusInfo;
      actShowStatusInfo.Hint := STransShowStatusInfo;
      actSaveLog.Caption := STransSaveLog;
      actSaveLog.Hint := STransSaveLog;
      FileSettingsAct.Caption := STransSettings;
      HelpAboutABSAct.Caption := STransAboutABSGroup;
      HelpABSHomePageAct.Caption := STransABSGroupHomePage;
      HelpOrderingInfoAct.Caption := STransOrderInfo;
      HelpPimsHomePageAct.Caption := STransPimsHomePage;
      HelpContentsAct.Caption := STransContents;
      HelpIndexAct.Caption := STransIndex;
      FileExit1.Caption := STransExit1;
      FileSettingsAct.Caption := STransSettings;
      FilePrintAct.Caption := STransPrint;
      FileClose.Caption := STransExit2;
      miFile1.Caption := STransFile;
      miHelp1.Caption := STransHelp;
    end;
  TranslateDialogSettings;
  if Assigned(ProductionScreenWaitF) then
    with ProductionScreenWaitF do
    begin
      Caption := STransPimsPersonalScreen;
    end;
  if Assigned(BaseDialogForm) then
    with BaseDialogForm do
    begin
       btnOKSmall.Caption := STransOK;
       btnCancelSmall.Caption := STransCancel;
    end;
  if Assigned(DialogChangeJobF) then
    with DialogChangeJobF do
    begin
      Caption := STransChangeJob;
      dxDBGrid1DESCRIPTION.Caption := STransChangeJob2;
      btnDown.Caption := STransDown;
      btnContinue.Caption := STransContinue;
      BitBtnOk.Caption := STransOK;
      BitBtnCancel.Caption := STransCancel;
      BitBtnEndOfDay.Caption := STransEndOfDay;
      BtnBreak.Caption := STransBreak;
      btnLunch.Caption := STransLunch;
    end;
  if Assigned(DialogShowAllEmployeesF) then
    with DialogShowAllEmployeesF do
    begin
      Caption := STransEmpsOnWorkspot;
      dxList.Bands.Items[0].Caption := STransEmployee;
      dxList.Bands.Items[1].Caption := STransScanned;
      dxList.Bands.Items[2].Caption := STransPlanned;
      dxList.Bands.Items[3].Caption := STransStandAvail;
      dxList.Bands.Items[4].Caption := STransAbsWithReason;
      dxList.Bands.Items[5].Caption := STransFirstAid;
      dxList.Bands.Items[6].Caption := STransEfficiency;
      dxListColumnNUMBER.Caption := STransNumber;
      dxListColumnNAME.Caption := STransName;
      dxListColumnSHORTNAME.Caption := STransShort;
      dxListColumnTEAM.Caption := STransTeam;
      dxListColumnSCANNEDWS.Caption := STransWS;
      dxListColumnPLANNEDWS.Caption := STransWS;
      dxListColumnSCANNEDDATEIN.Caption := STransTimeIn;
      dxListColumnPLANNEDSTARTTIME.Caption := STransStartTime;
      dxListColumnPLANNEDENDTIME.Caption := STransEndTime;
      dxListColumnELEVEL.Caption := STransLevel;
      dxListColumnEPLANLEVEL.Caption := STransLevel;
      dxListColumnSASTARTTIME.Caption := STransStartTime;
      dxListColumnEASTARTTIME.Caption := STransStartTime;
      dxListColumnSAENDTIME.Caption := STransEndTime;
      dxListColumnEAENDTIME.Caption := STransEndTime;
      dxListColumnABSENCEREASONCODE.Caption := STransAbsReasonCode;
      dxListColumnABSENCEREASONDESCR.Caption := STransAbsReasonDescr;
      dxListColumnFIRSTAIDEXPIRATIONDATE.Caption := STransExpDate;
      dxListColumnCURREFF.Caption := STransCurrent;
      dxListColumnSHIFTEFF.Caption := STransToday;
      btnOk.Caption := STransOK;
      btnCancel.Caption := STransCancel;
    end;
  if Assigned(DialogWorkspotSelectF) then
    with DialogWorkspotSelectF do
    begin
      Caption := STransWorkspotSelect;
      lblMessage.Caption := STransWorkspotSelect;
      Label3.Caption := STransPlant;
      Label1.Caption := STransWorkspot;
      Label2.Caption := STransPicture;
      LabelMachine.Caption := STransMachine;
      dxTreeList1Column1.Caption := STransPicture;
      rgProdScreenType.Caption := STransTypeOfProdScreen;
//      rgProdScreenType.Items.Strings[0] := STransProdBarsWithoutTR;
      rgProdScreenType.Items.Strings[0] := STransProdBarsTRMachineLevel;
      rgProdScreenType.Items.Strings[1] := STransProdBarsTRWorskpotLevel;
      btnOk.Caption := STransOK;
      btnCancel.Caption := STransCancel;
    end;
  if Assigned(DialogObjectSettingsF) then
    with DialogObjectSettingsF do
    begin
      Caption := STransSettings;
      GroupBox1.Caption := STransSettings;
      GroupBox4.Caption := STransWorkspotScale;
      Label3.Caption := STransPercentage;
      GroupBox7.Caption := STransFontScale;
      Label8.Caption := STransPercentage;
      GroupBox5.Caption := STransEffMeters;
      Label4.Caption := STransUsePeriod;
      rGrpMode.Items.Strings[0] := STransCurrent;
      rGrpMode.Items.Strings[1] := STransSince;
      rGrpMode.Items.Strings[2] := STransShift;
      btnOk.Caption := STransOK;
      btnCancel.Caption := STransCancel;
    end;
  TranslateDialogSelectDownType;
end; // TranslateAll

procedure TTranslateHandlerPS.TranslateInit;
begin
  if Assigned(DialogSelectionForm) then
    with DialogSelectionForm do
    begin
      btnOk.Caption := STransOK;
      btnCancel.Caption := STransCancel;
      btnOKSmall.Caption := STransOK;
      btnCancelSmall.Caption := STransCancel;
      stbarBase.SimpleText := STransABSLBS;
    end;
  if Assigned(DialogLoginF) then
    with DialogLoginF do
    begin
      Caption := STransDatabaseLogin;
      Label2.Caption := STransPassword;
      Label1.Caption := STransUsername;
      Label3.Caption := STransDatabasePims;
      btnOk.Caption := STransOK;
      btnCancel.Caption := STransCancel;
    end;
end;

initialization
  { Initialization section goes here }
  ATranslateHandlerPS := TTranslateHandlerPS.Create;

finalization
  { Finalization section goes here }
  ATranslateHandlerPS.Free;

end.
