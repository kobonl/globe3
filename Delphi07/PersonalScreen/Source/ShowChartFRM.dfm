inherited ShowChartF: TShowChartF
  Left = 257
  Top = 126
  Width = 697
  Height = 487
  Caption = 'Graph'
  Position = poOwnerFormCenter
  OnClose = FormClose
  OnKeyDown = FormKeyDown
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlImageBase: TPanel
    Width = 681
    ParentColor = True
    inherited imgBasePims: TImage
      Left = 385
    end
    object lblMessage: TLabel
      Left = 6
      Top = 6
      Width = 91
      Height = 23
      Caption = 'lblMessage'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -19
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
  end
  inherited stbarBase: TStatusBar
    Top = 409
    Width = 681
    Panels = <
      item
        Style = psOwnerDraw
        Text = 'ABS-Group'
        Width = 120
      end
      item
        Style = psOwnerDraw
        Width = 100
      end
      item
        Style = psOwnerDraw
        Width = 150
      end
      item
        Style = psOwnerDraw
        Width = 110
      end
      item
        Style = psOwnerDraw
        Width = 110
      end
      item
        Style = psOwnerDraw
        Width = 120
      end>
    SimplePanel = False
    OnDrawPanel = stbarBaseDrawPanel
  end
  inherited pnlInsertBase: TPanel
    Width = 681
    Height = 372
    Font.Color = clBlack
    Font.Height = -12
    ParentColor = True
    object Splitter1: TSplitter
      Left = 0
      Top = 145
      Width = 681
      Height = 3
      Cursor = crVSplit
      Align = alTop
    end
    object pnlAllTopInfo: TPanel
      Left = 0
      Top = 0
      Width = 681
      Height = 145
      Align = alTop
      ParentColor = True
      TabOrder = 0
      object pnlTopInfo: TPanel
        Left = 1
        Top = 1
        Width = 679
        Height = 37
        Align = alTop
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object pnlDateInput: TPanel
          Left = 0
          Top = 0
          Width = 679
          Height = 35
          Align = alTop
          BevelOuter = bvNone
          ParentColor = True
          TabOrder = 0
          object Panel1: TPanel
            Left = 603
            Top = 0
            Width = 76
            Height = 35
            Align = alRight
            BevelOuter = bvNone
            ParentColor = True
            TabOrder = 0
            object GroupBox3: TGroupBox
              Left = 0
              Top = 0
              Width = 76
              Height = 35
              Align = alClient
              Caption = 'Graph'
              TabOrder = 0
              object cBoxScale: TCheckBox
                Left = 6
                Top = 15
                Width = 54
                Height = 17
                Caption = 'Scale'
                TabOrder = 0
                OnClick = cBoxScaleClick
              end
            end
          end
          object GroupBox2: TGroupBox
            Left = 0
            Top = 0
            Width = 603
            Height = 35
            Align = alClient
            Caption = 'Workspots'
            TabOrder = 1
            object cBoxShowEmpl: TCheckBox
              Left = 466
              Top = 13
              Width = 127
              Height = 17
              Caption = 'Show Workspots'
              TabOrder = 0
              OnClick = cBoxShowEmplClick
            end
          end
        end
      end
      object pnlEmployees: TPanel
        Left = 1
        Top = 38
        Width = 679
        Height = 106
        Align = alClient
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object dxList: TdxTreeList
          Left = 0
          Top = 0
          Width = 679
          Height = 106
          Bands = <
            item
              Caption = 'Workspots'
            end>
          DefaultLayout = True
          HeaderPanelRowCount = 1
          Align = alClient
          TabOrder = 0
          Options = [aoColumnSizing, aoColumnMoving, aoEditing, aoTabThrough, aoRowSelect, aoAutoWidth, aoAutoSort]
          TreeLineColor = clGrayText
          ShowBands = True
          ShowGrid = True
          ShowRoot = False
          object dxListColumnMACHINE: TdxTreeListColumn
            Caption = 'MACHINE'
            BandIndex = 0
            RowIndex = 0
          end
          object dxListColumnWORKSPOT: TdxTreeListColumn
            Caption = 'WORKSPOT'
            BandIndex = 0
            RowIndex = 0
          end
          object dxListColumnJOB: TdxTreeListColumn
            Caption = 'JOB'
            BandIndex = 0
            RowIndex = 0
          end
        end
      end
    end
    object pnlChart: TPanel
      Left = 0
      Top = 148
      Width = 681
      Height = 224
      Align = alClient
      BevelOuter = bvNone
      ParentColor = True
      TabOrder = 1
      object Chart1: TChart
        Left = 0
        Top = 0
        Width = 681
        Height = 191
        AllowPanning = pmHorizontal
        AllowZoom = False
        BackWall.Brush.Color = clWhite
        BackWall.Brush.Style = bsClear
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clBlack
        Title.Font.Height = -12
        Title.Font.Name = 'Arial'
        Title.Font.Style = [fsBold]
        Title.Text.Strings = (
          'Production Output')
        OnClickSeries = Chart1ClickSeries
        Chart3DPercent = 10
        View3D = False
        View3DOptions.Elevation = 356
        OnAfterDraw = Chart1AfterDraw
        Align = alClient
        BevelWidth = 0
        ParentColor = True
        TabOrder = 0
        AutoSize = True
        object pnlInfo: TPanel
          Left = 532
          Top = 73
          Width = 148
          Height = 88
          BevelOuter = bvNone
          ParentColor = True
          TabOrder = 0
          object lblEfficiencyDescription: TLabel
            Left = 2
            Top = 4
            Width = 109
            Height = 13
            Caption = 'lblEfficiencyDescription'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object lblEfficiency: TLabel
            Left = 2
            Top = 20
            Width = 56
            Height = 13
            Caption = 'lblEfficiency'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object lblTotalProdDescription: TLabel
            Left = 2
            Top = 48
            Width = 78
            Height = 13
            Caption = 'Total Production'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object lblTotalProd: TLabel
            Left = 2
            Top = 64
            Width = 56
            Height = 13
            Caption = 'lblTotalProd'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
        end
        object Panel5: TPanel
          Left = 0
          Top = 0
          Width = 19
          Height = 191
          Align = alLeft
          BevelOuter = bvNone
          ParentColor = True
          TabOrder = 1
          object Panel6: TPanel
            Left = 0
            Top = 150
            Width = 19
            Height = 41
            Align = alBottom
            BevelOuter = bvNone
            ParentColor = True
            TabOrder = 0
            object lblEmpCnt: TLabel
              Left = 1
              Top = 10
              Width = 22
              Height = 23
              Caption = 'EC'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Height = -19
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
            end
          end
        end
        object Series2: TAreaSeries
          Marks.ArrowLength = 8
          Marks.Visible = False
          SeriesColor = clWhite
          ShowInLegend = False
          Title = 'Background'
          AreaBrush = bsClear
          AreaLinesPen.Color = clGray
          DrawArea = True
          LinePen.Color = clWhite
          LinePen.Visible = False
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          Pointer.Visible = False
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1.000000000000000000
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Y'
          YValues.Multiplier = 1.000000000000000000
          YValues.Order = loNone
        end
        object Series4: TAreaSeries
          Marks.ArrowLength = 8
          Marks.Visible = False
          SeriesColor = 9400152
          Title = 'Nr of Empl. (x100)'
          DrawArea = True
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          Pointer.Visible = False
          Stairs = True
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1.000000000000000000
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Y'
          YValues.Multiplier = 1.000000000000000000
          YValues.Order = loNone
        end
        object Series3: TLineSeries
          Marks.ArrowLength = 8
          Marks.Visible = False
          SeriesColor = 636154
          Title = 'Norm Prod. Level'
          LinePen.Width = 3
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          Pointer.Visible = False
          Stairs = True
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1.000000000000000000
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Y'
          YValues.Multiplier = 1.000000000000000000
          YValues.Order = loNone
        end
        object Series1: TLineSeries
          Marks.ArrowLength = 8
          Marks.Visible = False
          SeriesColor = 1649359
          Title = 'Output'
          Dark3D = False
          LinePen.Width = 3
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          Pointer.Visible = False
          Stairs = True
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1.000000000000000000
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Y'
          YValues.Multiplier = 1.000000000000000000
          YValues.Order = loNone
        end
      end
      object Panel2: TPanel
        Left = 0
        Top = 191
        Width = 681
        Height = 33
        Align = alBottom
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object Panel3: TPanel
          Left = 531
          Top = 0
          Width = 150
          Height = 33
          Align = alRight
          BevelOuter = bvNone
          ParentColor = True
          TabOrder = 0
          object btnCloseGraph: TButton
            Left = 32
            Top = 2
            Width = 114
            Height = 30
            Caption = 'Close Graph'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -17
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            OnClick = btnCloseGraphClick
          end
        end
        object Panel4: TPanel
          Left = 0
          Top = 0
          Width = 531
          Height = 33
          Align = alClient
          BevelOuter = bvNone
          ParentColor = True
          TabOrder = 1
          object sbtnHorScrollRight: TSpeedButton
            Left = 34
            Top = 4
            Width = 25
            Height = 25
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              0400000000000001000000000000000000001000000010000000000000000000
              800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
              3333333333333333333333333333333333333333333333333333333333333333
              3333333333333333333333333333333333333333333FF3333333333333003333
              3333333333773FF3333333333309003333333333337F773FF333333333099900
              33333FFFFF7F33773FF30000000999990033777777733333773F099999999999
              99007FFFFFFF33333F7700000009999900337777777F333F7733333333099900
              33333333337F3F77333333333309003333333333337F77333333333333003333
              3333333333773333333333333333333333333333333333333333333333333333
              3333333333333333333333333333333333333333333333333333}
            NumGlyphs = 2
            OnClick = sbtnHorScrollRightClick
          end
          object sbtnHorScrollLeft: TSpeedButton
            Left = 3
            Top = 4
            Width = 25
            Height = 25
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              0400000000000001000000000000000000001000000010000000000000000000
              800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
              3333333333333333333333333333333333333333333333333333333333333333
              3333333333333FF3333333333333003333333333333F77F33333333333009033
              333333333F7737F333333333009990333333333F773337FFFFFF330099999000
              00003F773333377777770099999999999990773FF33333FFFFF7330099999000
              000033773FF33777777733330099903333333333773FF7F33333333333009033
              33333333337737F3333333333333003333333333333377333333333333333333
              3333333333333333333333333333333333333333333333333333333333333333
              3333333333333333333333333333333333333333333333333333}
            NumGlyphs = 2
            OnClick = sbtnHorScrollLeftClick
          end
          object lblHelp: TLabel
            Left = 64
            Top = 8
            Width = 35
            Height = 14
            Caption = 'lblHelp'
          end
        end
      end
    end
  end
  inherited imgList16x16: TImageList
    Left = 216
    Top = 8
  end
  inherited imgNoYes: TImageList
    Left = 248
    Top = 8
  end
  inherited StandardMenuActionList: TActionList
    Left = 400
    Top = 8
    inherited FileSettingsAct: TAction
      OnExecute = FileSettingsActExecute
    end
    inherited FilePrintAct: TAction
      OnExecute = FilePrintActExecute
    end
    object FileExportAct: TAction
      Category = 'File'
      Caption = 'Export Chart'
      OnExecute = FileExportActExecute
    end
  end
  inherited mmPims: TMainMenu
    Left = 288
    Top = 8
    inherited miFile1: TMenuItem
      object Print1: TMenuItem [0]
        Action = FilePrintAct
      end
      object ExportChart1: TMenuItem [1]
        Action = FileExportAct
      end
    end
  end
  inherited StyleController: TdxEditStyleController
    Left = 328
    Top = 8
  end
  inherited StyleControllerRequired: TdxEditStyleController
    Left = 368
    Top = 8
  end
  object TimerProductionChart: TTimer
    Enabled = False
    Interval = 60000
    OnTimer = TimerProductionChartTimer
    Left = 144
    Top = 8
  end
  object SaveDialog1: TSaveDialog
    DefaultExt = 'txt'
    Filter = 'Export-Textfile|*.txt'
    Options = [ofOverwritePrompt, ofHideReadOnly, ofEnableSizing]
    Left = 568
    Top = 312
  end
  object TimerStatus: TTimer
    Enabled = False
    OnTimer = TimerStatusTimer
    Left = 176
    Top = 7
  end
end
