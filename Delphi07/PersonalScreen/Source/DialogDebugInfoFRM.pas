(*
  MRA:3-DEC-2012 Bugfix.
  - Small bugfix for showing hh:mm:ss.
  MRA:21-NOV-2013 20013196
  - Multiple counters on one workspot
    - Show the link-info here for jobs.
  MRA:12-MAY-2014 SO-20015330
  - Automatic reset of counters.
  MRA:12-MAY-2014 SO-20015331
  - Ignore Interface Codes handling
  MRA:30-MAY-2014 20015178
  - Measure Performance without employees
  MRA:8-SEP-2014 20013476
  - Make current variable, based on job-field SAMPLE_TIME_MINS.
  MRA:6-OCT-2014 20015376
  - Link Job Code Workspot - Show actual quantities
  MRA:23-DEC-2014 SO-20016016
  - Make it possible on workspot-level to read data (refresh) from database
    instead of from blackboxes.
  MRA:4-MAY-2015 SO-20014450.50
  - Real Time Efficiency
*)
unit DialogDebugInfoFRM;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BaseDialogFRM, dxCntner, Menus, StdActns, ActnList, ImgList,
  StdCtrls, Buttons, ComCtrls, ExtCtrls, PersonalScreenDMT, UPersonalScreen,
  UGlobalFunctions;

type
  TDialogDebugInfoF = class(TBaseDialogForm)
    Memo1: TMemo;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    FPSList: TList;
  public
    { Public declarations }
    procedure ClearLog;
    procedure AddLog(AMsg: String); overload;
    procedure AddLog(AMsg: String; AValue: Integer); overload;
    procedure AddLog(AMsg: String; AValue: Boolean); overload;
    procedure AddLog(AMsg: String; AValue: TDateTime); overload;
    procedure AddLog(AMsg: String; AValue: String); overload;
    procedure AddLogInt(AMsg: String; AValue: Integer);
    procedure AddLogTime(AMsg: String; AValue: Integer);
    function BoolStr(ABoolean: Boolean): String;
    procedure ShowDebugInfo;
    property PSList: TList read FPSList write FPSList;
  end;

var
  DialogDebugInfoF: TDialogDebugInfoF;

implementation

uses ORASystemDMT;

{$R *.dfm}

{ TDialogDebugInfo }

function TDialogDebugInfoF.BoolStr(ABoolean: Boolean): String;
begin
  if ABoolean then
    Result := 'True'
  else
    Result := 'False';
end;

procedure TDialogDebugInfoF.ClearLog;
begin
  Memo1.Lines.Clear;
end;

procedure TDialogDebugInfoF.AddLog(AMsg: String);
begin
  Memo1.Lines.Add(AMsg);
  WLog(AMsg);
end;

procedure TDialogDebugInfoF.AddLog(AMsg: String; AValue: Integer);
begin
  AddLog(AMsg + IntToStr(AValue));
end;

procedure TDialogDebugInfoF.AddLog(AMsg: String; AValue: Boolean);
begin
  AddLog(AMsg + BoolStr(AValue));
end;

procedure TDialogDebugInfoF.AddLog(AMsg: String; AValue: TDateTime);
begin
  AddLog(AMsg + DateTimeToStr(AValue));
end;

procedure TDialogDebugInfoF.AddLog(AMsg, AValue: String);
begin
  AddLog(AMsg + AValue);
end;

procedure TDialogDebugInfoF.AddLogInt(AMsg: String; AValue: Integer);
begin
  AddLog(AMsg + IntToStr(AValue));
end;

procedure TDialogDebugInfoF.AddLogTime(AMsg: String; AValue: Integer);
begin
  // Show: hours:minutes:seconds
  // MRA:3-DEC-2012 Bugfix: Do not use 'Round(AValue / 60)' but 'AValue div 60'.
  AddLog(AMsg + IntToStr(AValue) + ' (' +
    IntMin2StringTime({Round(AValue / 60)} AValue div 60, True) +
        ':' + Format('%.2d', [AValue mod 60]) + ')');
end;

procedure TDialogDebugInfoF.ShowDebugInfo;
var
  I, J, K: Integer;
  ADrawObject: PDrawObject;
  AJobCodeRec: PJobCodeRec;
  AFifoRec: PTFifoRec;
  AEmployeeRec: PEmployeeRec;
  ABlackBoxRec: PBlackBoxRec;
  AQtyToStoreRec: PQtyToStoreRec;
  ALinkJobRec, AJobRec: PLinkJobRec;
  ACompareJobRec: PCompareJobRec;
  AIgnoreInterfaceCodeRec: PIgnoreInterfaceCodeRec; // 20015331
begin
  ClearLog;
  AddLog('->DEBUG INFO-START');
  AddLog('ORASystemDM.EnableMachineTimeRec=', BoolStr(ORASystemDM.EnableMachineTimeRec)); // 20015178
  for I := 0 to PSList.Count - 1 do
  begin
    ADrawObject := PSList.Items[I];
    if Assigned(ADrawObject.AWorkspotTimeRec) then
    begin
      AddLog('->WORKSPOT');
      AddLog('ADrawObject.APlantCode=', ADrawObject.APlantCode);
      AddLog('ADrawObject.AWorkspotCode=', ADrawObject.AWorkspotCode);
      AddLog('ADrawObject.AWorkspotDescription=', ADrawObject.AWorkspotDescription);
      AddLog('ADrawObject.ACurrentNumberOfEmployees=', ADrawObject.ACurrentNumberOfEmployees);
      AddLog('ADrawObject.ALastCounterReadTime=', ADrawObject.ALastCounterReadTime);
      AddLog('ADrawObject.Last_Counter_Value=', ADrawObject.Last_Counter_Value);
      AddLog('ADrawObject.Act_prodqty_day=', ADrawObject.Act_prodqty_day);
      AddLog('ADrawObject.Act_prodqty_shift=', ADrawObject.Act_prodqty_shift);
      AddLog('ADrawObject.Act_prodqty_current=', ADrawObject.Act_prodqty_current);
      AddLog('ADrawObject.Theo_prodqty_day=', ADrawObject.Theo_prodqty_day);
      AddLog('ADrawObject.Theo_prodqty_shift=', ADrawObject.Theo_prodqty_shift);
      AddLog('ADrawObject.Theo_prodqty_current=', ADrawObject.Theo_prodqty_current);
      AddLogTime('ADrawObject.Act_time_day=', Round(ADrawObject.Act_time_day));
      AddLogTime('ADrawObject.Act_time_shift=', Round(ADrawObject.Act_time_shift));
      AddLogTime('ADrawObject.Act_time_current=', Round(ADrawObject.Act_time_current));
      AddLogTime('ADrawObject.Theo_time_day=', Round(ADrawObject.Theo_time_day));
      AddLogTime('ADrawObject.Theo_time_shift=', Round(ADrawObject.Theo_time_shift));
      AddLogTime('ADrawObject.Theo_time_current=', Round(ADrawObject.Theo_time_current));
      AddLog('ADrawObject.AWorkspotTimeRec.ACurrentJobCode=', ADrawObject.AWorkspotTimeRec.ACurrentJobCode);
      AddLog('ADrawObject.AWorkspotTimeRec.ACurrentJobDescription=', ADrawObject.AWorkspotTimeRec.ACurrentJobDescription);
      AddLog('ADrawObject.AWorkspotTimeRec.ACurrentShiftNumber=', ADrawObject.AWorkspotTimeRec.ACurrentShiftNumber);
      AddLog('ADrawObject.AWorkspotTimeRec.ACurrentShiftStart=', ADrawObject.AWorkspotTimeRec.ACurrentShiftStart);
      AddLog('ADrawObject.AWorkspotTimeRec.AMultipleShifts=', ADrawObject.AWorkspotTimeRec.AMultipleShifts);
      AddLog('ADrawObject.AWorkspotTimeRec.APreviousJobCode=', ADrawObject.AWorkspotTimeRec.APreviousJobCode);
      AddLog('ADrawObject.AWorkspotTimeRec.AIsCompareWorkspot=', BoolStr(ADrawObject.AWorkspotTimeRec.AIsCompareWorkspot));
      AddLog('ADrawObject.AWorkspotTimeRec.ACompareJobCode=', ADrawObject.AWorkspotTimeRec.ACompareJobCode);
      AddLog('ADrawObject.AWorkspotTimeRec.ATimeRecByMachine=', BoolStr(ADrawObject.AWorkspotTimeRec.ATimeRecByMachine)); // 20015178
      AddLog('ADrawObject.AWorkspotTimeRec.AFakeEmployeeNumber=', ADrawObject.AWorkspotTimeRec.AFakeEmployeeNumber); // 20015178
      AddLog('ADrawObject.APrevJobSampleTimeMins=', ADrawObject.APrevJobSampleTimeMins); // 20013476
      AddLog('ADrawObject.ACurJobSampleTimeMins=', ADrawObject.ACurJobSampleTimeMins); // 20013476
      AddLog('ADrawObject.ACurrentSampleTimeMins=', ADrawObject.ACurrentSampleTimeMins); // 20013476
      AddLog('ADrawObject.AWorkspotTimeRec.AReceiveQtyFromExternalApp=', ADrawObject.AWorkspotTimeRec.AReceiveQtyFromExternalApp); // 20016016
      AddLogInt('ADrawObject.ARealTimeIntervalMinutes=', Round(ADrawObject.ARealTimeIntervalMinutes)); // 20014450.50
      AddLog('-');
      // Jobs
      AddLog('->JOBS');
      if Assigned(ADrawObject.AJobCodeList) then
        for J := 0 to ADrawObject.AJobCodeList.Count - 1 do
        begin
          AJobCodeRec := ADrawObject.AJobCodeList[J];
          AddLog('- AJobCodeRec.JobCode=', AJobCodeRec.JobCode);
          AddLog('- AJobCodeRec.JobCodeDescription=', AJobCodeRec.JobCodeDescription);
          AddLog('- AJobCodeRec.InterfaceCode=', AJobCodeRec.InterfaceCode);
          AddLog('- AJobCodeRec.Norm_prod_level=', AJobCodeRec.Norm_prod_level);
          AddLog('- AJobCodeRec.Last_Counter_Value=', AJobCodeRec.Last_Counter_Value);
          AddLog('- AJobCodeRec.Act_prodqty_day=', AJobCodeRec.Act_prodqty_day);
          AddLog('- AJobCodeRec.Act_prodqty_shift=', AJobCodeRec.Act_prodqty_shift);
          AddLog('- AJobCodeRec.Act_prodqty_current=', AJobCodeRec.Act_prodqty_current);
          AddLog('- AJobCodeRec.Theo_prodqty_day=', AJobCodeRec.Theo_prodqty_day);
          AddLog('- AJobCodeRec.Theo_prodqty_shift=', AJobCodeRec.Theo_prodqty_shift);
          AddLog('- AJobCodeRec.Theo_prodqty_current=', AJobCodeRec.Theo_prodqty_current);
          AddLogTime('- AJobCodeRec.Act_time_day=', Round(AJobCodeRec.Act_time_day));
          AddLogTime('- AJobCodeRec.Act_time_shift=', Round(AJobCodeRec.Act_time_shift));
          AddLogTime('- AJobCodeRec.Act_time_current=', Round(AJobCodeRec.Act_time_current));
          AddLogTime('- AJobCodeRec.Theo_time_day=', Round(AJobCodeRec.Theo_time_day));
          AddLogTime('- AJobCodeRec.Theo_time_shift=', Round(AJobCodeRec.Theo_time_shift));
          AddLogTime('- AJobCodeRec.Theo_time_current=', Round(AJobCodeRec.Theo_time_current));
          AddLog('- AJobCodeRec.Link_WorkspotCode=', AJobCodeRec.Link_WorkspotCode); // 20013196
          AddLog('- AJobCodeRec.Link_JobCode=', AJobCodeRec.Link_JobCode); // 20013196
          AddLog('- AJobCodeRec.Sample_time_mins=', AJobCodeRec.Sample_time_mins); // 20013476
          AddLogInt('- AJobCodeRec.ARealTimeIntervalMinutes=', Round(AJobCodeRec.ARealTimeIntervalMinutes)); // 20014450.50
          AddLog('-');
          // Fifo list
          if Assigned(AJobCodeRec.Fifo_list) then
            if AJobCodeRec.Fifo_list.Count > 0 then
            begin
              AddLog('->FIFO LIST');
              for K := 0 to AJobCodeRec.Fifo_list.Count - 1 do
              begin
                AFifoRec := AJobCodeRec.Fifo_list.Items[K];
                AddLog('-- AFifoRec.TimeStamp=', AFifoRec.TimeStamp);
                AddLog('-- AFifoRec.Qty=', AFifoRec.Qty);
                AddLogTime('-- AFifoRec.TimeElapsed=', Round(AFifoRec.TimeElapsed));
                AddLog('--');
              end;
            end;
          end;
      AddLog('->COMPAREJOBS');
      if Assigned(ADrawObject.ACompareJobList) then
        for J := 0 to ADrawObject.ACompareJobList.Count - 1 do
        begin
          ACompareJobRec := ADrawObject.ACompareJobList.Items[J];
          AddLog('-- ACompareJobRec.WorkspotCode=', ACompareJobRec.WorkspotCode);
          AddLog('-- ACompareJobRec.JobCode=', ACompareJobRec.JobCode);
        end;
      AddLog('->BLACKBOXES');
      // Blackbox
      if Assigned(ADrawObject.ABlackBoxList) then
        for J := 0 to ADrawObject.ABlackBoxList.Count - 1 do
        begin
          ABlackBoxRec := ADrawObject.ABlackBoxList.Items[J];
          AddLog('-- ABlackBoxRec.InterfaceCode=', ABlackBoxRec.InterfaceCode);
          AddLog('-- ABlackBoxRec.IPAddress=', ABlackBoxRec.IPAddress);
          AddLog('-- ABlackBoxRec.Port=', ABlackBoxRec.Port);
          AddLog('-- ABlackBoxRec.ModuleAddress=', ABlackBoxRec.ModuleAddress);
          AddLog('-- ABlackBoxRec.CounterNumber=', ABlackBoxRec.CounterNumber);
          AddLog('-- ABlackBoxRec.ActualCounterValue=', ABlackBoxRec.ActualCounterValue);
          AddLog('-- ABlackBoxRec.ActualCounterDateTime=', ABlackBoxRec.ActualCounterDateTime);
          AddLog('-- ABlackBoxRec.Reset=', BoolStr(ABlackBoxRec.Reset)); // 20015330
          AddLog('--');
        end;
      AddLog('->QTY TO STORE');
      // Qty To Store
      if Assigned(ADrawObject.QtyToStoreList) then
        for J := 0 to ADrawObject.QtyToStoreList.Count - 1 do
        begin
          AQtyToStoreRec := ADrawObject.QtyToStoreList.Items[J];
          AddLog('- AQtyToStoreRec.Timestamp=', AQtyToStoreRec.Timestamp);
          AddLog('- AQtyToStoreRec.JobCode=', AQtyToStoreRec.JobCode);
          AddLog('- AQtyToStoreRec.ShiftNumber=', AQtyToStoreRec.ShiftNumber);
          AddLog('- AQtyToStoreRec.Quantity=', AQtyToStoreRec.Quantity);
          AddLog('- AQtyToStoreRec.Countervalue=', AQtyToStoreRec.Countervalue);
          AddLog('-');
        end;
      AddLog('->EMPLOYEES');
      // Employees
      if Assigned(ADrawObject.AEmployeeList) then
        for J := 0 to ADrawObject.AEmployeeList.Count - 1 do
        begin
          AEmployeeRec := ADrawObject.AEmployeeList[J];
          AddLog('-- AEmployeeRec.EmployeeNumber=', AEmployeeRec.EmployeeNumber);
          AddLog('-- AEmployeeRec.EmployeeName=', AEmployeeRec.EmployeeName);
          AddLog('-- AEmployeeRec.ShiftNumber=', AEmployeeRec.ShiftNumber);
          AddLogTime('-- AEmployeeRec.Act_Time_Day=', Round(AEmployeeRec.Act_Time_Day));
          AddLogTime('-- AEmployeeRec.Act_Time_Shift=', Round(AEmployeeRec.Act_Time_Shift));
          AddLog('-- AEmployeeRec.CurrentlyScannedIn=', AEmployeeRec.CurrentlyScannedIn);
          AddLog('-- AEmployeeRec.TeamCode=', AEmployeeRec.TeamCode);
          AddLog('-- AEmployeeRec.Level=', AEmployeeRec.Level);
          AddLog('-- AEmployeeRec.DateTimeIn=', AEmployeeRec.DateTimeIn);
          AddLog('-- AEmployeeRec.DateTimeOut=', AEmployeeRec.DateTimeOut);
          AddLog('-- AEmployeeRec.PlannedWorkspot=', AEmployeeRec.PlannedWorkspot);
          AddLog('-- AEmployeeRec.PlannedStartTime=', AEmployeeRec.PlannedStartTime);
          AddLog('-- AEmployeeRec.PlannedEndTime=', AEmployeeRec.PlannedEndTime);
          AddLog('-- AEmployeeRec.PlannedLevel=', AEmployeeRec.PlannedLevel);
          AddLog('--');
        end;
      AddLog('-----------');
    end; // if
  end; // for
  // 20015331
  // IgnoreInterfaceCodes
  if Assigned(IgnoreInterfaceCodeList) then
    if IgnoreInterfaceCodeList.Count > 0 then
    begin
      AddLog('-> IgnoreInterfaceCodeList');
      for K := 0 to IgnoreInterfaceCodeList.Count - 1 do
      begin
        AIgnoreInterfaceCodeRec := IgnoreInterfaceCodeList.Items[K];
        AddLog('--AIgnoreInterfaceRec.PlantCode=',
          AIgnoreInterfaceCodeRec.PlantCode);
        AddLog('--AIgnoreInterfaceRec.WorkspotCode=',
          AIgnoreInterfaceCodeRec.WorkspotCode);
        AddLog('--AIgnoreInterfaceRec.JobCode=',
          AIgnoreInterfaceCodeRec.JobCode);
        AddLog('--AIgnoreInterfaceRec.InterfaceCode=',
          AIgnoreInterfaceCodeRec.InterfaceCode);
        AddLog('--');
      end;
    end;
  // LinkJobs
  if Assigned(LinkJobList) then
    if LinkJobList.Count > 0 then
    begin
      AddLog('-> LINKJOBS');
      for J := 0 to LinkJobList.Count - 1 do
      begin
        ALinkJobRec := LinkJobList.Items[J];
        AddLog('-- ALinkJobRec.PlantCode=', ALinkJobRec.PlantCode);
        AddLog('-- ALinkJobRec.WorkspotCode=', ALinkJobRec.WorkspotCode);
        AddLog('-- ALinkJobRec.JobCode=', ALinkJobRec.JobCode);
        // 20015376
        if Assigned(ALinkJobRec.JobList) then
        begin
          AddLog('---> JOBS LINKED TO LINKJOB:');
          for K := 0 to ALinkJobRec.JobList.Count - 1 do
          begin
            AJobRec := ALinkJobRec.JobList.Items[K];
            AddLog('---- AJobRec.PlantCode=', AJobRec.PlantCode);
            AddLog('---- AJobRec.WorkspotCode=', AJobRec.WorkspotCode);
            AddLog('---- AJobRec.JobCode=', AJobRec.JobCode);
          end;
        end;
      end;
    end;
  AddLog('->DEBUG INFO-END');
end;

procedure TDialogDebugInfoF.FormShow(Sender: TObject);
begin
  inherited;
  ShowDebugInfo;
end;

end.
