object Form1: TForm1
  Left = 453
  Top = 209
  Width = 590
  Height = 595
  Caption = 'pcProxPlusUSBTest'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 574
    Height = 41
    Align = alTop
    TabOrder = 0
    object Button1: TButton
      Left = 16
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Start'
      TabOrder = 0
      OnClick = Button1Click
    end
    object CheckBoxRosslare: TCheckBox
      Left = 104
      Top = 12
      Width = 97
      Height = 17
      Caption = 'Rosslare'
      TabOrder = 1
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 41
    Width = 574
    Height = 515
    Align = alClient
    TabOrder = 1
    object Memo1: TMemo
      Left = 1
      Top = 1
      Width = 572
      Height = 513
      Align = alClient
      ScrollBars = ssBoth
      TabOrder = 0
    end
  end
  object TimerPCProxPlus: TTimer
    Enabled = False
    OnTimer = TimerPCProxPlusTimer
    Left = 520
    Top = 40
  end
end
