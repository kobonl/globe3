unit HomeFRM;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, PCPROXAPI, UPCProcsPlusReader, ExtCtrls;

type
  TForm1 = class(TForm)
    TimerPCProxPlus: TTimer;
    Panel1: TPanel;
    Button1: TButton;
    Panel2: TPanel;
    Memo1: TMemo;
    CheckBoxRosslare: TCheckBox;
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure TimerPCProxPlusTimer(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    PCProcsPlusReader: TPCProcsPlusReader;
    procedure MyOpenScanner;
    procedure MyCloseScanner;
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
begin
  if Button1.Caption = 'Start' then
  begin
    Button1.Caption := 'Stop';
    TimerPCProxPlus.Enabled := True;
    try
      PCPROXAPI.CheckLoadPCProxAPI_DLL();
      PCProcsPlusReader := TPCProcsPlusReader.Create;
      PCProcsPlusReader.Rosslare := CheckBoxRosslare.Checked;
      if PCPROXAPI.PCProxAPI_DLLLoaded then
        MyCloseScanner;
    except
      Memo1.Lines.Add('Error during openening card-reader');
    end;
    MyOpenScanner;
  end
  else
  begin
    Button1.Caption := 'Start';
    MyCloseScanner;
    TimerPCProxPlus.Enabled := False;
  end;
end;

procedure TForm1.MyOpenScanner;
begin
  if Assigned(PCProcsPlusReader) then
  begin
    Memo1.Lines.Add('Opening Reader...');
    PCProcsPlusReader.Connect;
    if PCPROXAPI.PCProxAPI_DLLLoaded then
      TimerPCProxPlus.Enabled := True;
    if PCProcsPlusReader.Connected then
      Memo1.Lines.Add('Opened')
    else
      Memo1.Lines.Add('Error: Could not open reader');
  end;
end;

procedure TForm1.MyCloseScanner;
begin
  if Assigned(PCProcsPlusReader) then
  begin
    Memo1.Lines.Add('Closing Reader...');
    PCProcsPlusReader.DisConnect;
    if PCPROXAPI.PCProxAPI_DLLLoaded then
      TimerPCProxPlus.Enabled := False;
  end;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  PCProcsPlusReader := nil;
end;

procedure TForm1.TimerPCProxPlusTimer(Sender: TObject);
var
  MyID: String;
begin
  if Assigned(PCProcsPlusReader) then
  begin
    PCProcsPlusReader.Rosslare := CheckBoxRosslare.Checked;
    PCProcsPlusReader.ErrorMsg := '';
    PCProcsPlusReader.GetActiveID;
    if PCProcsPlusReader.ID <> '' then
    begin
      // Conversion is done during read, do not do it here!
      MyID := PCProcsPlusReader.ID;
      Memo1.Lines.Add('ID: [' + MyID + ']');
//      if EditIDCard.Text <> '' then
//        StartScanningPost(EditIDCard.Text);
    end;
    if PCProcsPlusReader.ErrorMsg <> '' then
      Memo1.Lines.Add(PCProcsPlusReader.ErrorMsg);
    Sleep(250);
  end;
end;

procedure TForm1.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  MyCloseScanner;
  TimerPCProxPlus.Enabled := False;
end;

end.
