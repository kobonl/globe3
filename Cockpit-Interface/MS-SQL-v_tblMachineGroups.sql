USE [JEGR_DB]
GO

--drop view [dbo].[v_tblMachineGroups];
create view [dbo].[v_tblMachineGroups] as
SELECT mg1.[RecNum]
      ,mg1.[idJensen]
      ,mg2.[glabsRef]
      ,mg1.[MachineArea]
      ,mg1.[ShortDescription]
      ,cast(mg1.[LongDescription] as nvarchar(999)) LongDescription
      ,mg1.[DisplayInMaster]
      ,mg1.[UnitsKPI]
      ,mg1.[HourlyKPI]
      ,mg1.[DailyKPI]
      ,mg1.[GraphKPI]
      ,mg1.[StartTime]
      ,mg1.[EndTime]
  FROM [JEGR_DB].[dbo].[tblMachineGroups] mg1 inner join [JEGR_DB].[dbo].[tblMachineGroupsGlabsRef] mg2 on
    mg1.idJensen = mg2.machineGroup_idJensen
  
  GO
  
  