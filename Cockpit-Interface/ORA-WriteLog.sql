create or replace procedure writelog(lfp_logmessage    in abslog.logmessage%type,
                      lfp_computer_name in varchar2,
                      lfp_logsource in varchar2,
                      lfp_priority      in abslog.priority%type) is
    pragma autonomous_transaction;
  begin
    /*
    Write message in ABSLOG with these priorities
    1 = Message
    2 = Warning
    3 = Error
    4 = Stack trace
    5 = Debug info
    */
    dbms_output.put_line(lfp_logmessage);
    insert into abslog
      (abslog_id, computer_name, abslog_timestamp, logsource, priority, systemuser, logmessage)
    values
      (seq_abslog.nextval, lfp_computer_name, sysdate /*abslog_timestamp*/, lfp_logsource
       /*logsource*/, lfp_priority /*priority*/, 'Cockpit' /*systemuser*/,
       substr(lfp_logmessage, 1, 1000)); -- Change from 255 to 1000
    --commit log
    commit;
  end writelog;
/  