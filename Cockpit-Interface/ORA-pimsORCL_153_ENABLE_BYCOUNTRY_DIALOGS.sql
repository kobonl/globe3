-- 550497
-- Export salary CleanLeaseFortex

-- Enable the By Country dialogs

INSERT INTO PIMSMENUGROUP 
SELECT t.group_name, 121508, 'Y', 'Y', 'N', 121500, 'Countries', 8, SYSDATE, SYSDATE, 'MRA' 
FROM pimsusergroup t;
INSERT INTO PIMSMENUGROUP 
SELECT t.group_name, 121509, 'Y', 'Y', 'N', 121500, 'Absence types per country', 9, SYSDATE, SYSDATE, 'MRA' 
FROM pimsusergroup t;
INSERT INTO PIMSMENUGROUP 
SELECT t.group_name, 121510, 'Y', 'Y', 'N', 121500, 'Absence reasons per country', 10, SYSDATE, SYSDATE, 'MRA' 
FROM pimsusergroup t;
INSERT INTO PIMSMENUGROUP 
SELECT t.group_name, 121511, 'Y', 'Y', 'N', 121500, 'Types of hour per country', 11, SYSDATE, SYSDATE, 'MRA' 
FROM pimsusergroup t;

COMMIT;


