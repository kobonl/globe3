@echo off

set wrap off
set termout off
set verify off
set trimspool on
set linesize 500
set longchunksize 200000
set long 200000
set pages 0
column txt format a120

set result_file=result.log

set user_name=PIMS
set password=PIMS
set net_service_name=ABS1

del %result_file%

(
echo @RestartJobs
echo @ShowJobs
echo exit
) | sqlplus -s %user_name%/%password%@%net_service_name% > %result_file%
 
