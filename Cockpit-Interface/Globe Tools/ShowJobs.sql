column c1 heading "Job Name" Format a15
column c2 heading "Last start date" Format a20
column c3 heading "Next run date" Format a20

SELECT TRIM(T.JOB_NAME) || ' ; ' || TRIM(T.LAST_START_DATE) || ' ; ' || TRIM(T.NEXT_RUN_DATE)
FROM DBA_SCHEDULER_JOBS T
WHERE
(
  (T.JOB_NAME LIKE '%COCKPIT_JOB')
       OR
  (T.JOB_NAME LIKE '%PIMS_JOB')
)
AND
T.OWNER = 'PIMS';

