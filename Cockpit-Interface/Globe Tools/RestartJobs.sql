BEGIN
  DBMS_SCHEDULER.DROP_JOB('OPS_2_COCKPIT_JOB', TRUE);
end;
/
BEGIN
  DBMS_SCHEDULER.DROP_JOB('DEPT_2_PIMS_JOB', TRUE);
end;
/
BEGIN
  DBMS_SCHEDULER.DROP_JOB('MACH_2_PIMS_JOB', TRUE);
end;
/
BEGIN
  DBMS_SCHEDULER.DROP_JOB('WORKSPOT_2_PIMS_JOB', TRUE);
end;
/
BEGIN
  DBMS_SCHEDULER.DROP_JOB('SCANS_2_COCKPIT_JOB', TRUE);
end;
/


BEGIN
DBMS_SCHEDULER.CREATE_JOB (
   job_name             => 'OPS_2_COCKPIT_JOB',
   job_type             => 'PLSQL_BLOCK',
   job_action           => 'BEGIN PACKAGE_COCKPIT.operators_2_cockpit; END;',
   start_date           => sysdate,
   repeat_interval      => 'FREQ=HOURLY; BYMINUTE=0; BYSECOND=0', 
   enabled              =>  TRUE,
   comments             => 'Operators to cockpit');
END;
/

BEGIN
DBMS_SCHEDULER.CREATE_JOB (
   job_name             => 'DEPT_2_PIMS_JOB',
   job_type             => 'PLSQL_BLOCK',
   job_action           => 'BEGIN PACKAGE_COCKPIT.department_2_pims; END;',
   start_date           => sysdate,
   repeat_interval      => 'FREQ=HOURLY; BYMINUTE=1; BYSECOND=0', 
   enabled              =>  TRUE,
   comments             => 'Departments to Pims');
END;
/

BEGIN
DBMS_SCHEDULER.CREATE_JOB (
   job_name             => 'MACH_2_PIMS_JOB',
   job_type             => 'PLSQL_BLOCK',
   job_action           => 'BEGIN PACKAGE_COCKPIT.machine_2_pims; END;',
   start_date           => sysdate,
   repeat_interval      => 'FREQ=HOURLY; BYMINUTE=2; BYSECOND=0', 
   enabled              =>  TRUE,
   comments             => 'Machines to Pims');
END;
/

BEGIN
DBMS_SCHEDULER.CREATE_JOB (
   job_name             => 'WORKSPOT_2_PIMS_JOB',
   job_type             => 'PLSQL_BLOCK',
   job_action           => 'BEGIN PACKAGE_COCKPIT.workspot_2_pims; END;',
   start_date           => sysdate,
   repeat_interval      => 'FREQ=HOURLY; BYMINUTE=3; BYSECOND=0', 
   enabled              =>  TRUE,
   comments             => 'Workspots to Pims');
END;
/
BEGIN
DBMS_SCHEDULER.CREATE_JOB (
   job_name             => 'SCANS_2_COCKPIT_JOB',
   job_type             => 'PLSQL_BLOCK',
   job_action           => 'BEGIN PACKAGE_COCKPIT.scans_2_cockpit; END;',
   start_date           => sysdate,
   repeat_interval      => 'FREQ=SECONDLY;INTERVAL=2', -- repeat every n seconds
   enabled              =>  TRUE,
   comments             => 'Scans to Cockpit');
END;
/

