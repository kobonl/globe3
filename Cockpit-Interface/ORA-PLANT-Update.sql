--
-- GLOB3-158
-- Missing CardID in the Operators transfer from PIMS database to Cockpit Database
--

declare
    column_exists exception;
    pragma exception_init (column_exists , -01430);
begin
    execute immediate 'alter table PLANT add FAC varchar(3)';
    exception when column_exists then null;
end;
/

commit;
 