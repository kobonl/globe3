create or replace view vc_i_workspots as
select /*vc_i_workspots version 0.2*/
 substr(to_char(m2."glabsRef"), 1, 4) || to_char(u.counter) workspot_code,
 pt."PLANTCODE" plant_code,
 to_char(u.counter) || ' ' || substr(m."ShortDescription", 1, 17) short_name,
 nvl(substr(m."LongDescription", 1, 27), m."ShortDescription") || ' ' || to_char(u.counter) description,
 sysdate creation_date, sysdate mutation_date, 'Cockpit' mutator,
 substr(to_char(mg2."glabsRef"), 1, 6) department_code, substr(to_char(m2."glabsRef"), 1, 4) machine_code,
 null date_inactive, 'Y' use_jobcode_yn, 'Y' productive_hour_yn, 'Y' measure_productivity_yn,
 'Y' quant_piece_yn, 'Y' automatic_datacol_yn
from   dbo.v_tblmachines@gtlab m, dbo.v_tblmachinegroups@gtlab mg, dbo.viewplant@gtlab pt,
       dbo.tblmachinesglabsref@gtlab m2, dbo.tblmachinegroupsglabsref@gtlab mg2,
       (select rownum counter from user_tables) u
where  u.counter <= m."Positions"
and    mg."idJensen" = m."MachineGroup_idJensen"
and    m."idJensen" = m2."machine_idJensen"
and    mg."idJensen" = mg2."machineGroup_idJensen"
and    substr(to_char(m2."glabsRef"), 1, 4) is not null
and    substr(to_char(mg2."glabsRef"), 1, 6) is not null



