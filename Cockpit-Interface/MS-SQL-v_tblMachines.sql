USE [JEGR_DB]
GO

/****** Object:  View [dbo].[v_tblMachines]    Script Date: 4-10-2016 11:38:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--drop view [dbo].[v_tblMachines]
--GO
create view [dbo].[v_tblMachines] as
SELECT m1.[RecNum]
      ,m1.[idJensen]
      ,m2.[glabsRef]
      ,m1.[ShortDescription]
      ,cast(m1.[LongDescription] as nvarchar(999)) LongDescription
	  ,m1.[MachineGroup_idJensen]
	  ,m1.[Positions]
  FROM [JEGR_DB].[dbo].[tblMachines] m1 inner join [JEGR_DB].[dbo].[tblMachinesGlabsRef] m2 on
    m1.idJensen = m2.machine_idJensen
GO
