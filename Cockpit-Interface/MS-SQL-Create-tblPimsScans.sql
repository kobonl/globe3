USE [JEGR_DB]
GO

/****** Object:  Table [dbo].[tblPimsScans]    Script Date: 6-4-2017 11:17:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tblPimsScans](
	[EmployeeNumber] [int] NOT NULL,
	[DateTimeIn] [datetime] NOT NULL,
	[MachineCode] [nvarchar](6) NOT NULL,
	[WorkspotCode] [nvarchar](6) NULL,
	[EmployeeID] [int] NOT NULL,
	[DateTimeOut] [datetime] NULL,
	[LogDate] [datetime] NOT NULL,
	[PlanInfo] [nvarchar](1) NOT NULL,
	[State] [nvarchar](1) NOT NULL,
	[Job_Code] [nvarchar](6) NOT NULL,
	[Done] [int] NOT NULL,
 CONSTRAINT [PK_tblPimsScans] PRIMARY KEY NONCLUSTERED 
(
	[EmployeeNumber] ASC,
	[DateTimeIn] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO



