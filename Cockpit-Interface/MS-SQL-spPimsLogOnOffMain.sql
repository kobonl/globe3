USE [JEGR_DB]
GO
/****** Object:  StoredProcedure [dbo].[spPimsLogOnOffMain]    Script Date: 30-3-2017 10:25:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =======================================================
-- Author:              Gary Cailes (Ge-Mac Systems Ltd)
-- Create date:         10 June 2016
-- Description:         Prototype pims operator log 
--						interface
-- =======================================================

CREATE PROCEDURE [dbo].[spPimsLogOnOffMain]
(
    @OperatorGlabsRef nvarchar(max)    
    ,@TimeStamp datetime = NULL
    ,@MachineCode nvarchar(6)		
    ,@SubID int = -1			
    ,@State int					 
    ,@PlanInfo int				 
)
AS
BEGIN
  SET NOCOUNT ON; 

  DECLARE @ReturnCode int
  DECLARE @Msg nvarchar(256)
  DECLARE @Comp nvarchar(256)
  DECLARE @LogSource nvarchar(256)
  DECLARE @Prio int = 3 
  DECLARE @ScanInfo nvarchar(256)
  DECLARE @MachineID int = 0;

  SELECT @MachineID = machine_idJensen FROM JEGR_DB.dbo.tblmachinesGlabsRef 
		WHERE glabsRef = @MachineCode
  IF @MachineID IS NULL SET @MachineID = -1
  
  SET @Comp = '1'
  SET @LogSource = 'spPimsLogOnOff'

  EXEC @ReturnCode = JEGR_DB.dbo.[spPimsLogOnOff]
     @OperatorGlabsRef
    ,@TimeStamp
    ,@MachineID		
    ,@SubID
    ,@State					 
    ,@PlanInfo

  IF @ReturnCode > 0 
  BEGIN
    IF @TimeStamp IS NULL 
    BEGIN
      SET @TimeStamp = GetDate()
    END    
    SET @ScanInfo = 'Operator=' + @OperatorGlabsRef + ' Scan=' + CONVERT(varchar(23), @TimeStamp, 20)
  END
  
  IF @ReturnCode = 1
    SET @Msg = @ScanInfo + '. Operator is unknown.'
  ELSE IF @ReturnCode = 2
    SET @Msg = @ScanInfo + '. SubID ' + CONVERT(varchar, @SubID) + ' is too high.'
  ELSE IF @ReturnCode = 3
    SET @Msg = @ScanInfo + '. Cannot find machine record.'
  ELSE IF @ReturnCode > 3
    SET @Msg = @ScanInfo + '. Unknown error (' + CONVERT(varchar, @ReturnCode) + ').'
  IF @ReturnCode > 0
  BEGIN
    EXEC ('BEGIN writelog(?,?,?,?); END;', @Msg , @Comp, @LogSource, @Prio) at ABS1
  END
  RETURN @ReturnCode
END

GO

