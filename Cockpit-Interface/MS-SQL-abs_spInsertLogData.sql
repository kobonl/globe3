USE [JEGR_DB]
GO
/****** Object:  StoredProcedure [dbo].[abs_spInsertLogData]    Script Date: 20-4-2017 13:20:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =======================================================
-- Author:              ABS
-- Create date:         17 March 2017
-- Description:         Wrapper for Standard Data Logging
-- =======================================================
CREATE PROCEDURE [dbo].[abs_spInsertLogData]
(
            @RemoteID int = -1
           ,@CompanyID int
           ,@TimeStamp datetime = NULL
           ,@MachineID int
           ,@PositionID int = -1
           ,@SubID int = -1
           ,@SubIDName nvarchar(50) = NULL
           ,@RegType int
           ,@SubRegType int
           ,@SubRegTypeID int = -1
           ,@State int = -1
           ,@MessageA nvarchar(max) = NULL
           ,@MessageB nvarchar(max) = NULL
           ,@BatchID int = -1
           ,@SourceID int = -1
           ,@ProcessCode int = -1
           ,@ProcessName nvarchar(50) = N''
           ,@ExtCustNo int = -1
           ,@SortCategoryID int = -1
           ,@ArtNo int = -1
           ,@OperatorNo int = -1
           ,@Value decimal(18,3) = 0
           ,@Unit int = -1
           ,@AllowDuplicateRemoteID bit = 1
)
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @RC INT,
			@CustNo INT;
	--lookup th idJensen if possible
	SELECT @CustNo = ISNULL(MAX(idJensen),@ExtCustNo) FROM dbo.tblCustomers WHERE ExtRef = CONVERT(varchar, @ExtCustNo) 
	
	EXEC @RC = dbo.[spInsertLogData] 
		@RemoteID
	   ,@CompanyID
	   ,@TimeStamp
	   ,@MachineID
	   ,@PositionID
	   ,@SubID
	   ,@SubIDName
	   ,@RegType
	   ,@SubRegType
	   ,@SubRegTypeID
	   ,@State
	   ,@MessageA
	   ,@MessageB
	   ,@BatchID
	   ,@SourceID
	   ,@ProcessCode
	   ,@ProcessName
	   ,@CustNo
	   ,@SortCategoryID
	   ,@ArtNo
	   ,@OperatorNo
	   ,@Value
	   ,@Unit
	   ,@AllowDuplicateRemoteID
		
	RETURN @RC	-- Return the number of affected rows.		
END	