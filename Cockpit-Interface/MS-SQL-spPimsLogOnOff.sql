USE [JEGR_DB]
GO

/****** Object:  StoredProcedure [dbo].[spPimsLogOnOff]    Script Date: 28-9-2017 08:15:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ======================================================= 
-- Author:              Gary Cailes (Ge-Mac Systems Ltd) 
-- Create date:         10 June 2016 
-- Description:         Prototype pims operator log  
--						interface 
-- ======================================================= 
 
CREATE PROCEDURE [dbo].[spPimsLogOnOff] 
( 
    @OperatorGlabsRef nvarchar(max) -- I will use this to lookup the operator id 
    ,@TimeStamp datetime = NULL	  -- if you don't give me this I will use current time 
    ,@MachineID int				  -- required field 
    ,@SubID int = -1			  -- specify subid or -1 will default to 1 
    ,@State int					  -- required field 0=log out 1=log in. 
	,@PlanInfo int				  -- not yet implemented 
) 
AS 
BEGIN 
	SET NOCOUNT ON; 
 
	DECLARE @ReturnCode int = 4 
 
	DECLARE @Positions int, @OperatorID int, @CompanyID int = 60 
 
	IF @SubID = -1 SET @SubID = 1 
 
	SELECT @Positions = Positions FROM JEGR_DB.dbo.tblmachines 
		WHERE idJensen = @MachineID 
	/* Is MachineID valid? */ 
	IF @Positions IS NULL SET @ReturnCode = 3 --@Positions < 1 means cannot find machine record 
	/* Is SubID Valid? */ 
	ELSE IF @Positions < @SubID SET @ReturnCode = 2 --@Positions < @SubID means its too high. 
	ELSE 
	BEGIN 
		/* OperatorGlabsRef Valid? */ 
		SET @ReturnCode = 1 
		SELECT @OperatorID = operator_idJensen FROM JEGR_DB.dbo.tblOperatorsGlabsRef 
			WHERE glabsRef = @OperatorGlabsRef 
		if @OperatorID IS NOT NULL 
		BEGIN 
			/* Determine whether or not a time was passed */ 
			SET @ReturnCode = 0 
			IF @TimeStamp IS NULL 
			BEGIN 
				SET @TimeStamp = GETDATE() 
			END 
                        
	                EXEC JEGR_DB.dbo.[spInsertLogData]  
				-1 
			     ,@CompanyID 
			     ,@TimeStamp 
			     ,@MachineID 
			     ,-1 
			     ,@SubID 
			     ,N'' 
			     ,6 
			     ,0 
			     ,0 
			     ,@State 
			     ,N'Operator' 
			     ,N'Operator'			       
		  	     ,-1 
			     ,-1 
			     ,-1 
			     ,N'' 
			     ,-1 
			     ,-1 
			     ,-1 
			     ,@OperatorID 
			     ,0 
			     ,-1 
			     ,1 
			INSERT INTO JEGR_DB.[dbo].[tblRemoteOperatorLog] 
					   ([LogTime] 
					   ,[MachineID] 
					   ,[OperatorID] 
					   ,[SubID] 
					   ,[State] 
					   ,[jgLogDataRemoteID] 
					   ,[UpdateTime] 
					   ,[SubregType] 
					   ,[SubregTypeID]) 
				 VALUES 
					   (@TimeStamp 
					   ,@MachineID 
					   ,@OperatorID 
					   ,@SubID 
					   ,@State 
					   ,null 
					   ,null 
					   ,6 
					   ,0) 
                        /* Is scan made/changed during current time? With margin of plus/min x minutes. */
                        IF @Timestamp >= DateAdd(mi, -15, GetDate()) and @Timestamp <= DateAdd(mi, 15, GetDate())
                        BEGIN
			  EXECUTE JEGR_DB.[dbo].[spMachineSubIDLogOnOff]  
			     @MachineID 
			    ,@SubID 
			    ,@OperatorID 
			    ,@State 
                        END
		END 
	END		 
	RETURN @ReturnCode/* zero if successful, non zero if not */ 
END 

GO

