create or replace package package_cockpit is
  --Version 0.4
  procedure operators_2_cockpit;
  procedure department_2_pims;
  procedure machine_2_pims;
  procedure workspot_2_pims;
  procedure scans_2_cockpit;
  procedure save_scans_2_cockpit;
  procedure all_tags_2_cockpit;
  function cardDecToHex(dec varchar2) return varchar2;
end package_cockpit;
/
create or replace package body package_cockpit is

  procedure delete_tags_retired_employees;
  procedure delete_tags_bo_deleted_idcards;

  --internal procedures
  procedure write_log(lfp_logmessage    in abslog.logmessage%type,
                      lfp_computer_name in varchar2,
                      lfp_priority      in abslog.priority%type) is
    pragma autonomous_transaction;
  begin
    /*
    Write message in ABSLOG with these priorities
    1 = Message
    2 = Warning
    3 = Error
    4 = Stack trace
    5 = Debug info
    */
--    dbms_output.put_line(lfp_logmessage);
    insert into abslog
      (abslog_id, computer_name, abslog_timestamp, logsource, priority, systemuser, logmessage)
    values
      (seq_abslog.nextval, lfp_computer_name, sysdate /*abslog_timestamp*/, 'package_cockpit'
       /*logsource*/, lfp_priority /*priority*/, 'Cockpit' /*systemuser*/,
       substr(lfp_logmessage, 1, 1000)); -- Change from 255 to 1000
    --commit log
    commit;
  end write_log;

  function cardDecToHex(dec varchar2) return varchar2 is
    ai integer;
    bi integer;
    ci integer;
    di integer;
    hex varchar2(40);
    cardnostr varchar2(40);
    seriesnostr varchar2(40);
    cardno integer;
    seriesno integer;
  begin
    hex := dec;
    if Length(dec) >= 8 then
      begin
        ai := 0;
        bi := 0;
        ci := 0;
        di := 0;
        if Length(dec) > 5 then
          cardnostr := substr(dec, (Length(dec) - 5)+1, 5);
          seriesnostr := substr(dec, 1, Length(dec) - 5);
--          dbms_output.put_line(cardnostr || ';' || seriesnostr);
          cardno := to_number(cardnostr);
          seriesno := to_number(seriesnostr);
--          dbms_output.put_line(cardno || ';' || seriesno);
          ai := mod(cardno, 256);
          bi := trunc(cardno/256);
          ci := mod(seriesno, 256);
          di := trunc(seriesno/256);
--          dbms_output.put_line(ai || ';' || bi || ';' || ci || ';' || di);
        end if;
        hex := trim(to_char(ai, '0X')) || trim(to_char(bi, '0X')) || trim(to_char(ci, '0X')) || trim(to_char(di, '0X'));
      exception
        when others then
--          dbms_output.put_line('Error during conversion');
          hex := dec;
      end;
    end if;
    return hex;
  end cardDecToHex;

  procedure update_tag(tagdata varchar2, referencetable varchar2, referenceid integer) is
    lv_tagdata      varchar2(40);
    lv_tagid        integer;
    lv_message      varchar2(1000);
    lv_computername varchar2(32);
    lv_fac          varchar2(3);
  begin
    begin
      lv_fac := '';
      begin
        select p.fac
        into lv_fac
        from plant p
        where p.plant_code = (select e.plant_code from employee e where e.employee_id = referenceid);
      exception
        when no_data_found then
          lv_fac := '';
        when others then
          lv_fac := '';
      end;
      if (lv_fac is null) or (lv_fac = '') then
        lv_fac := '';
      end if;
      lv_message := 'update tag data';
      lv_computername := 'operators_2_cockpit';
      if length(tagdata) = 5 then
        lv_tagdata := cardDecToHex(lv_fac || tagdata);
      else
        lv_tagdata := cardDecToHex(tagdata);
      end if;
      update dbo.tbltags@gtlab ta
      set ta."TagData" = lv_tagdata
      where ta."ReferenceID" = referenceid;
      if sql%rowcount = 0 then
        begin
          lv_tagid := 1;
          -- Use nvl to prevent it gives NULL as result!
          select nvl(max(ta."TagID"), 0)
          into lv_tagid
          from dbo.tbltags@gtlab ta;
          if lv_tagid = 0 then
            lv_tagid := 1;
          else
            lv_tagid := lv_tagid + 1;
          end if;
          insert into dbo.tbltags@gtlab
            ("TagID", "TagData", "ReferenceTable", "ReferenceID")
          values
            (lv_tagid, lv_tagdata, referencetable, referenceid);
        end;
      end if;
    exception
      when no_data_found then
        begin
          rollback;
          write_log('No data found ' || lv_message, lv_computername, 3);
        end;
      when others then
        begin
          rollback;
          write_log(lv_message || sqlerrm, lv_computername, 3);
        end;
    end;
    commit;
  end update_tag;

  procedure operators_2_cockpit is
    lv_message        varchar2(1000);
    lv_computername   varchar2(32);
    lv_shortdescription nvarchar2(60);
    lv_longdescription nvarchar2(60);

    procedure initialize is
    begin
      lv_computername := 'operators_2_cockpit';
    end initialize;
  begin
    initialize;
    --loop all employees + idcards connected to employee
    lv_message := 'loop employee PIMS';
    for x in (select e.employee_id, e.employee_number glabsref, substr(e.description, 1, 30) shortdescription,
                     e.description longdescription,
                     case
                       when e.enddate < trunc(sysdate) then
                        1
                       else
                        0
                     end retired, e.mutationdate,
                     (select min(i.idcard_number) from idcard i where i.employee_number = e.employee_number) idcardnr
              from   employee e) loop
      begin
        --update dbo.tbloperators
        lv_message := 'update dbo.tbloperators employee_id ' || to_char(x.employee_id);
        lv_shortdescription := x.shortdescription;
        lv_longdescription := x.longdescription;
        update dbo.tbloperators@gtlab op
        set    op."Retired" = x.retired, op."ShortDescription" = lv_shortdescription,
               op."LongDescription" = lv_longdescription
        where  op."idJensen" = x.employee_id;
        if sql%rowcount = 0 then
          begin
            --insert into dbo.tbloperators
            lv_message := 'insert into dbo.tbloperators employee_id ' || to_char(x.employee_id);
            insert into dbo.tbloperators@gtlab
              ("idJensen", "Retired", "ShortDescription", "LongDescription")
            values
              (x.employee_id, x.retired, lv_shortdescription, lv_longdescription);
          end;
        end if;
        -- update dbo.tbloperatorsgtlabs
        update dbo.tbloperatorsglabsref@gtlab op
        set op."glabsRef" = x.glabsref
        where op."operator_idJensen" = x.employee_id;
        if sql%rowcount = 0 then
          begin
            --insert into tbloperatorsglabsref
            insert into dbo.tbloperatorsglabsref@gtlab
              ("operator_idJensen", "glabsRef")
            values
              (x.employee_id, x.glabsref);
          end;
        end if;
        commit;
        -- update dbo.tbltags
        if x.idcardnr is not null then
          update_tag(x.idcardnr, 'tblOperators', x.employee_id);
        end if;
      exception
        when no_data_found then
          begin
            rollback;
            write_log('No data found ' || lv_message, lv_computername, 3);
          end;
        when others then
          begin
            rollback;
            write_log(lv_message || sqlerrm, lv_computername, 3);
          end;
      end;
    end loop; --x
    commit;
    -- loop idcards that are changed today and update them when needed
    lv_message := 'loop idtag PIMS';
    for x in (select i.idcard_number, e.employee_id
              from idcard i left join employee e on
              i.employee_number = e.employee_number
              where i.mutationdate >= trunc(sysdate)) loop
      begin
        update_tag(x.idcard_number, 'tblOperators', x.employee_id);
      exception
        when no_data_found then
          begin
            rollback;
            write_log('No data found ' || lv_message, lv_computername, 3);
          end;
        when others then
          begin
            rollback;
            write_log(lv_message || sqlerrm, lv_computername, 3);
          end;
      end;
    end loop; --x
    commit;
    delete_tags_retired_employees;
    delete_tags_bo_deleted_idcards;    
  exception
    when no_data_found then
      begin
        rollback;
        write_log('No data found ' || lv_message, lv_computername, 3);
      end;
    when others then
      begin
        rollback;
        write_log(lv_message || sqlerrm, lv_computername, 3);
      end;
  end operators_2_cockpit;

  procedure department_2_pims is
    lv_message      varchar2(1000);
    lv_computername varchar2(32);

    procedure initialize is
    begin
      lv_computername := 'department_2_pims';
    end initialize;
  begin
    initialize;
    for y in (select * from vc_i_department) loop
      begin
        --update department
        lv_message := 'update department ' || y.department_code || ' plant ' || y.plant_code;
        update department t
        set    t.description = y.description,
               t.direct_hour_yn = y.direct_hour_yn, t.mutationdate = y.mutationdate,
               t.mutator = y.mutator, t.plan_on_workspot_yn = y.plan_on_workspot_yn
        where  t.department_code = y.department_code
        and    t.plant_code = y.plant_code;
        if sql%rowcount = 0 then
          begin
            --insert into department
            lv_message := 'insert into department ' || y.department_code || ' plant ' ||
                          y.plant_code;
            insert into department
              (department_code, plant_code, description, businessunit_code, creationdate,
               direct_hour_yn, mutationdate, mutator, plan_on_workspot_yn)
            values
              (y.department_code, y.plant_code, y.description, y.businessunit_code, y.creationdate,
               y.direct_hour_yn, y.mutationdate, y.mutator, y.plan_on_workspot_yn);
          end;
        end if;
      exception
        when no_data_found then
          begin
            rollback;
            write_log('No data found ' || lv_message, lv_computername, 3);
          end;
        when others then
          begin
            rollback;
            write_log(lv_message || sqlerrm, lv_computername, 3);
          end;
      end;
    end loop; --y
    commit;
  exception
    when no_data_found then
      begin
        rollback;
        write_log('No data found ' || lv_message, lv_computername, 3);
      end;
    when others then
      begin
        rollback;
        write_log(lv_message || sqlerrm, lv_computername, 3);
      end;
  end department_2_pims;

  procedure machine_2_pims is
    lv_message      varchar2(1000);
    lv_computername varchar2(32);

    procedure initialize is
    begin
      lv_computername := 'machine_2_pims';
    end initialize;
  begin
    initialize;
    for z in (select vm.machine_code, vm.plant_code, vm.description, vm.short_name, vm.creation_date,
                     vm.mutation_date, vm.mutator, vm.department_code
              from   vc_i_machine vm) loop
      begin
        --update machine
        lv_message := 'update machine ' || z.machine_code || ' plant ' || z.plant_code;
        update machine m
        set    m.description = z.description, m.short_name = z.short_name,
               m.creationdate = z.creation_date, m.mutationdate = z.mutation_date,
               m.mutator = z.mutator, m.department_code = z.department_code
        where  m.machine_code = z.machine_code
        and    m.plant_code = z.plant_code;
        if sql%rowcount = 0 then
          begin
            --insert into machine
            lv_message := 'insert into machine ' || z.machine_code || ' plant ' || z.plant_code;
            insert into machine
              (plant_code, machine_code, description, short_name, creationdate, mutationdate,
               mutator, department_code)
            values
              (z.plant_code, z.machine_code, z.description, z.short_name, z.creation_date,
               z.mutation_date, z.mutator, z.department_code);
          end;
        end if;
      exception
        when no_data_found then
          begin
            rollback;
            write_log('No data found ' || lv_message, lv_computername, 3);
          end;
        when others then
          begin
            rollback;
            write_log(lv_message || sqlerrm, lv_computername, 3);
          end;
      end;
    end loop; --y
    commit;
  exception
    when no_data_found then
      begin
        rollback;
        write_log('No data found ' || lv_message, lv_computername, 3);
      end;
    when others then
      begin
        rollback;
        write_log(lv_message || sqlerrm, lv_computername, 3);
      end;
  end machine_2_pims;

  procedure workspot_2_pims is
    lv_message      varchar2(1000);
    lv_computername varchar2(32);

    procedure initialize is
    begin
      lv_computername := 'workspot_2_pims';
    end initialize;
  begin
    initialize;
    for y in (select * from vc_i_workspots) loop
      begin
        --update workspot
        lv_message := 'update workspots ' || y.workspot_code || ' plant ' || y.plant_code;
        update workspot t
        set    t.short_name = y.short_name, t.description = y.description,
               t.creationdate = y.creation_date, t.mutationdate = y.mutation_date,
               t.mutator = y.mutator, t.department_code = y.department_code,
               t.machine_code = y.machine_code, t.date_inactive = y.date_inactive,
               t.use_jobcode_yn = y.use_jobcode_yn, t.productive_hour_yn = y.productive_hour_yn,
               t.measure_productivity_yn = y.measure_productivity_yn, t.quant_piece_yn = y.quant_piece_yn,
               t.automatic_datacol_yn = y.automatic_datacol_yn
        where  t.workspot_code = y.workspot_code
        and    t.plant_code = y.plant_code;
        if sql%rowcount = 0 then
          begin
            --insert into workspots
            lv_message := 'insert into workspot ' || y.workspot_code || ' plant ' || y.plant_code;
            insert into workspot
              (workspot_code, plant_code, short_name, description, creationdate, mutationdate,
               mutator, department_code, machine_code, date_inactive, use_jobcode_yn,
               productive_hour_yn, measure_productivity_yn, quant_piece_yn, automatic_datacol_yn)
            values
              (y.workspot_code, y.plant_code, y.short_name, y.description, y.creation_date,
               y.mutation_date, y.mutator, y.department_code, y.machine_code, y.date_inactive,
               y.use_jobcode_yn, y.productive_hour_yn, y.measure_productivity_yn, y.quant_piece_yn,
               y.automatic_datacol_yn);
          end;
        end if;
      exception
        when no_data_found then
          begin
            rollback;
            write_log('No data found ' || lv_message, lv_computername, 3);
          end;
        when others then
          begin
            rollback;
            write_log(lv_message || sqlerrm, lv_computername, 3);
          end;
      end;
    end loop; --y
    commit;
  exception
    when no_data_found then
      begin
        rollback;
        write_log('No data found ' || lv_message, lv_computername, 3);
      end;
    when others then
      begin
        rollback;
        write_log(lv_message || sqlerrm, lv_computername, 3);
      end;
  end workspot_2_pims;

  -- Internal function
  function spOperatorLogPims(AMachineCode varchar2, AWorkspotCode varchar2,
    AEmployeeNumber Smallint, ALogDate Timestamp, APlanInfo Integer, AState Integer) return Boolean is
    Result Boolean;
    ret integer;
    lv_message varchar2(1000);
    lv_computername varchar2(32);
    lv_glabsRef varchar2(12);
    PRAGMA AUTONOMOUS_TRANSACTION;
  begin
    Result := False;
    lv_message := 'spPimsLogOnOff';
    lv_computername := 'scans_2_cockpit';
    lv_glabsRef := cast(AEmployeeNumber as varchar2);
    begin
      ret := DBMS_HS_PASSTHROUGH.EXECUTE_IMMEDIATE@GTLAB(
        'dbo.spPimsLogOnOffMain @OperatorGlabsRef  = "' || lv_glabsRef ||
        '", @TimeStamp = "' ||
        to_char(ALogDate, 'YYYY-MM-DD HH24:MI:SS') ||
        '", @MachineCode = "' ||
        AMachineCode ||
        '", @SubID = "' ||
        AWorkspotCode ||
        '", @State = "' ||
        AState ||
        '", @PlanInfo = "' ||
        APlanInfo || '"');
      commit;
--      write_log(lv_message || ' ret=' || ret, lv_computername, 3);
      if ret = 0 then
        begin
          commit;
          Result := True;
        end;
      end if;
    exception
      when others then
      begin
        if SQLCODE = -28500 then -- Supress ORA-28500
          begin
--            rollback;
            write_log(lv_message || sqlerrm, lv_computername, 3);
          end;
        else
          begin
--            rollback;
            write_log(lv_message || sqlerrm, lv_computername, 3);
          end;
        end if;
      end;
    end;
    rollback;
    return Result;
  end spOperatorLogPims;

  procedure scans_2_cockpit is
    lv_message        varchar2(1000);
    lv_computername   varchar2(32);
    lv_logdate        timestamp;
    lv_machinecode    varchar2(16);
    lv_state          integer;
    lv_workspotcode   varchar2(16);
    lv_employeenumber smallint;
    lv_planinfo       integer;
    lv_datetimein     timestamp;
    procedure initialize is
    begin
      lv_computername := 'scans_2_cockpit';
    end initialize;
    procedure InsertScan is
    begin
      begin
        -- insert log info
        if spOperatorLogPims(lv_machinecode, lv_workspotcode, lv_employeenumber, lv_logdate, lv_planinfo, lv_state) then
          begin
            if lv_state = 1 then -- Log-in
              update timeregscanning t
              set t.in_yn = 'Y'
              where t.employee_number = lv_employeenumber and
                  t.datetime_in = lv_datetimein;
            else -- Log-out
              update timeregscanning t
              set t.in_yn = 'Y',
              t.out_yn = 'Y'
              where t.employee_number = lv_Employeenumber and
                  t.datetime_in = lv_datetimein;
            end if;
          end;
        end if;
      exception
        when others then
        begin
          write_log(lv_message || sqlerrm, lv_computername, 3);
        end;
      end;
    end InsertScan;
  begin
    initialize;
    --loop all scans for shift date
    lv_message := 'loop scans PIMS';

/*
   @MachineCode
  ,@WorkSpotCode
  ,@EmployeeNumber
  ,@Logdate
  ,@PlanInfo
  ,@State
*/
    for x in (select w.machine_code MachineCode,
                     substr(t.workspot_code, length(w.machine_code)+1) WorkspotCode,
                     e.employee_id EmployeeID,
                     e.employee_number EmployeeNumber,
                     t.datetime_in DateTimeIn,
                     t.datetime_out DateTimeOut,
                     t.mutationdate LogDate,
                     0 PlanInfo,
                     case
                       when t.datetime_out is null then 1 -- log-in
                       when t.in_yn = 'Y' and t.out_yn = 'N' then 0 -- log-out
                       else 2 -- log in+out at once
                     end State,
                     t.job_code
              from timeregscanning t
                inner join employee e on
                  t.employee_number = e.employee_number
                inner join workspot w on
                  t.plant_code = w.plant_code and
                  t.workspot_code = w.workspot_code
              where t.shift_date >= trunc(sysdate-90) and
                    t.job_code = 'ATWORK' and
                    t.workspot_code is not null and
                    w.machine_code is not null and
                    (
                     (
                      (t.in_yn = 'N' and t.processed_yn = 'N')
                      or
                      (t.out_yn = 'N' and t.processed_yn = 'Y')
                     )
                    or
                     (t.in_yn is null or t.out_yn is null)
                    )
              order by t.DateTime_In
                  ) loop
      begin
        if x.state = 1 or x.state = 2 then
          lv_logdate := x.datetimein;
          lv_state := 1;
        else
          lv_logdate := x.datetimeout;
          lv_state := 0;
        end if;
        lv_machinecode := x.machinecode;
        lv_message := 'insert operator log info for employee ' || to_char(x.employeeid) || ' log-date ' || to_char(lv_logdate);
        lv_workspotcode := x.workspotcode;
        lv_employeenumber := x.employeenumber;
        lv_planinfo := x.planinfo;
        lv_datetimein := x.datetimein;
        InsertScan;
        -- When scan-in+out is done both, here we insert scan-out.
        if x.state = 2 then
          lv_state := 0;
          lv_logdate := x.datetimeout;
          lv_message := 'insert operator log info for employee ' || to_char(x.employeeid) || ' log-date ' || to_char(lv_logdate);
          InsertScan;
        end if;
      end;
    end loop; --x
    commit;
  exception
    when no_data_found then
      begin
        rollback;
        write_log('No data found ' || lv_message, lv_computername, 3);
      end;
    when others then
      begin
        rollback;
        write_log(lv_message || sqlerrm, lv_computername, 3);
      end;
  end scans_2_cockpit;

  procedure save_scans_2_cockpit is
    lv_message      varchar2(1000);
    lv_computername varchar2(32);
    lv_logdate      timestamp;
    lv_found        boolean;
    procedure initialize is
    begin
      lv_computername := 'save_scans_2_cockpit';
    end initialize;
  begin
    initialize;
    --loop all scans for shift date
    lv_message := 'loop scans PIMS';

    -- change the in_yn or out_yn for scans that were done (done=1)
    -- based on dbo.tblPimsScans
    -- Afterwards delete these records.
    begin
      lv_found := false;
      for x in
        (select t."EmployeeNumber", t."DateTimeIn", t."State", t."Done"
        from dbo.tblPimsScans@gtlab t
        where t."Done" = 1)
      loop
        begin
          lv_found := true;
          if x."State" = 1 then -- Log-in
            update timeregscanning t
            set t.in_yn = 'Y'
            where t.employee_number = x."EmployeeNumber" and
            t.datetime_in = x."DateTimeIn";
          else -- Log-out
            update timeregscanning t
            set t.in_yn = 'Y',
            t.out_yn = 'Y'
            where t.employee_number = x."EmployeeNumber" and
            t.datetime_in = x."DateTimeIn";
          end if;
        end;
      end loop;
      if lv_found then
        commit;
      end if;
      delete from dbo.tblPimsScans@gtlab t
      where t."Done" = 1;
      if SQL%ROWCOUNT <> 0 then
        commit;
      end if;
    exception
     when no_data_found then
     begin
       rollback;
     end;
     when others then
     begin
        rollback;
        write_log(lv_message || sqlerrm, lv_computername, 3);
      end;
    end;

/*
   @MachineCode
  ,@WorkSpotCode
  ,@EmployeeNumber
  ,@Logdate
  ,@PlanInfo
  ,@State
*/
    for x in (select w.machine_code MachineCode,
                     substr(t.workspot_code, length(w.machine_code)+1) WorkspotCode,
                     e.employee_id EmployeeID,
                     e.employee_number EmployeeNumber,
                     t.datetime_in DateTimeIn,
                     t.datetime_out DateTimeOut,
                     t.mutationdate LogDate,
                     0 PlanInfo,
                     case
                       when t.datetime_out is null then 1 -- log-in
                       else 0 -- log-out
                     end State,
                     t.job_code
              from timeregscanning t
                inner join employee e on
                  t.employee_number = e.employee_number
                inner join workspot w on
                  t.plant_code = w.plant_code and
                  t.workspot_code = w.workspot_code
              where t.shift_date >= trunc(sysdate-90) and
                    t.job_code = 'ATWORK' and
                    t.workspot_code is not null and
                    w.machine_code is not null and
                    (
                      (t.in_yn = 'N' and t.processed_yn = 'N')
                      or
                      (t.out_yn = 'N' and t.processed_yn = 'Y')
                    )
                    or
                    (t.in_yn is null or t.out_yn is null)
              order by t.DateTime_In
                  ) loop
      begin
        begin
          -- insert log info
          if x.state = 1 then
            lv_logdate := x.datetimein;
          else
            lv_logdate := x.datetimeout;
          end if;
          lv_message := 'save scans to cockpit ' || to_char(x.employeeid) || ' log-date ' || to_char(lv_logdate);
          -- udate any existing record
          update dbo.tblPimsScans@gtlab ps
          set    ps."Done" = 0, ps."DateTimeOut" = x.DateTimeOut, ps."LogDate" = lv_logdate, ps."State" = x.State
          where  ps."EmployeeNumber" = x.employeenumber and
            ps."DateTimeIn" = x.Datetimein;
          if SQL%ROWCOUNT <> 0 then
            commit;
          else
            -- insert into dbo.tblPimsScans
            insert into dbo.tblPimsScans@gtlab
              ("EmployeeNumber", "DateTimeIn", "MachineCode", "WorkspotCode", "EmployeeID", "DateTimeOut", "LogDate", "PlanInfo", "State", "Job_Code", "Done" )
            values
              (x.EmployeeNumber, x.DateTimeIn, x.MachineCode, x.WorkspotCode, x.EmployeeID, x.Datetimeout, x.Logdate, x.Planinfo, x.State, x.job_code, 0);
            commit;
          end if;
        exception
          when others then
          begin
            rollback;
            write_log(lv_message || sqlerrm, lv_computername, 3);
          end;
        end;
      end;
    end loop; --x
  exception
    when no_data_found then
      begin
        rollback;
        write_log('No data found ' || lv_message, lv_computername, 3);
      end;
    when others then
      begin
        rollback;
        write_log(lv_message || sqlerrm, lv_computername, 3);
      end;
  end save_scans_2_cockpit;

  procedure all_tags_2_cockpit is
    lv_message      varchar2(1000);
    lv_computername varchar2(32);
  begin
    -- loop idcards that are changed today and update them when needed
    lv_message := 'loop idtag PIMS';
    lv_computername := 'all_tags_2_cockpit';
    for x in (select i.idcard_number, e.employee_id
              from idcard i left join employee e on
              i.employee_number = e.employee_number) loop
      begin
        update_tag(x.idcard_number, 'tblOperators', x.employee_id);
      exception
        when no_data_found then
          begin
            rollback;
            write_log('No data found ' || lv_message, lv_computername, 3);
          end;
        when others then
          begin
            rollback;
            write_log(lv_message || sqlerrm, lv_computername, 3);
          end;
      end;
    end loop; --x
    commit;
  end all_tags_2_cockpit;

  procedure delete_tags_retired_employees is
    lv_message      varchar2(1000);
    lv_computername varchar2(32);
  begin
    -- loop retired employees and delete tags
    lv_message := 'delete tags regired employees';
    lv_computername := 'delete_tags_retired';
    for x in (select e.employee_id
              from employee e
              where (e.enddate < trunc(sysdate))) loop
    begin
      delete from tblTags@gtlab t where t."ReferenceID" = x.employee_id;
      exception
        when no_data_found then
          begin
            rollback;
            write_log('No data found ' || lv_message, lv_computername, 3);
          end;
        when others then
          begin
            rollback;
            write_log(lv_message || sqlerrm, lv_computername, 3);
          end;
    end;
    end loop; --x
    commit;
  end delete_tags_retired_employees;
  
  -- delete tags based on deleted idcards
  procedure delete_tags_bo_deleted_idcards is
    refID Integer;
  begin
    -- check if there are employees that have no idcards (deleted?)
    -- when found, then delete them from tbltags.
  for x in (
    select e.employee_id from employee e left outer join idcard i on
      e.employee_number = i.employee_number
    where i.idcard_number is null
  )
  loop
    begin
      select t."ReferenceID"
      into refID
      from tbltags@gtlab t 
      where t."ReferenceID" = x.employee_id;
      if refID is not null then
        delete from tbltags@gtlab t where t."ReferenceID" = x.employee_id;
        commit;
      end if;
    exception
      when no_data_found then
        begin
          null; 
        end;
      when others then
        begin
          null;
        end;
    end;
  end loop;
  end delete_tags_bo_deleted_idcards;
  
end package_cockpit;
/
