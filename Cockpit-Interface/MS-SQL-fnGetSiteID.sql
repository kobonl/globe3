USE [JEGR_DB]
GO

/****** Object:  UserDefinedFunction [dbo].[fnGetSiteID]    Script Date: 28-9-2017 08:06:28 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ============================================= 
-- Author:		Gary Cailes, Ge-Mac 
-- Create date: 1st July 2016 
-- Description:	Get Int SiteID from Settings 
-- ============================================= 
CREATE FUNCTION [dbo].[fnGetSiteID]  
( 
) 
RETURNS int 
AS 
BEGIN 
	DECLARE @Setting nvarchar(max), @SiteID int 
 
	SELECT @Setting = DefaultValue FROM JEGR_DB.dbo.tblSettings 
	WHERE SettingName = 'SiteID' 
	SET @Setting = IsNull(@Setting, '-1') 
 
	SET @SiteID = CAST(@Setting as int) 
 
	RETURN @SiteID 
 
END 

GO
