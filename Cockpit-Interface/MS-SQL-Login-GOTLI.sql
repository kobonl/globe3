/*    ==Scripting Parameters==

    Source Server Version : SQL Server 2012 (11.0.6020)
    Source Database Engine Edition : Microsoft SQL Server Standard Edition
    Source Database Engine Type : Standalone SQL Server

    Target Server Version : SQL Server 2012
    Target Database Engine Edition : Microsoft SQL Server Standard Edition
    Target Database Engine Type : Standalone SQL Server
*/

USE [master]
GO

/* For security reasons the login is created disabled and with a random password. */
/****** Object:  Login [GOTLI]    Script Date: 09-Oct-2017 16:32:33 ******/
CREATE LOGIN [GOTLI] WITH PASSWORD=N'zazjbteej0hjjorYFr4RGl4B4tdX3LtJRdHe6Xfb9WE=', DEFAULT_DATABASE=[master], DEFAULT_LANGUAGE=[us_english], CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF
GO

ALTER LOGIN [GOTLI] DISABLE
GO

ALTER SERVER ROLE [sysadmin] ADD MEMBER [GOTLI]
GO


