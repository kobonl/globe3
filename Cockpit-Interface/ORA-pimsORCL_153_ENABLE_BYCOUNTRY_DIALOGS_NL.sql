-- 550497
-- Export salary CleanLeaseFortex

-- Enable the By Country dialogs

INSERT INTO PIMSMENUGROUP 
SELECT t.group_name, 121508, 'Y', 'Y', 'N', 121500, 'Landen', 8, SYSDATE, SYSDATE, 'MRA' 
FROM pimsusergroup t;
INSERT INTO PIMSMENUGROUP 
SELECT t.group_name, 121509, 'Y', 'Y', 'N', 121500, 'Afwezigheidssoorten per land', 9, SYSDATE, SYSDATE, 'MRA' 
FROM pimsusergroup t;
INSERT INTO PIMSMENUGROUP 
SELECT t.group_name, 121510, 'Y', 'Y', 'N', 121500, 'Afwezigheidsredenen per land', 10, SYSDATE, SYSDATE, 'MRA' 
FROM pimsusergroup t;
INSERT INTO PIMSMENUGROUP 
SELECT t.group_name, 121511, 'Y', 'Y', 'N', 121500, 'Urensoorten per land', 11, SYSDATE, SYSDATE, 'MRA' 
FROM pimsusergroup t;

COMMIT;


