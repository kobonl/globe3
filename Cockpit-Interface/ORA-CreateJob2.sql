BEGIN
DBMS_SCHEDULER.CREATE_JOB (
   job_name             => 'SCANS_2_COCKPIT_JOB',
   job_type             => 'PLSQL_BLOCK',
   job_action           => 'BEGIN PACKAGE_COCKPIT.scans_2_cockpit; END;',
   start_date           => sysdate,
   repeat_interval      => 'FREQ=SECONDLY;INTERVAL=2', -- repeat every n seconds
   enabled              =>  TRUE,
   comments             => 'Scans to Cockpit');
END;
/
