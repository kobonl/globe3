USE [JEGR_DB]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

INSERT INTO [dbo].[tblSettings]
           ([SettingID], [SettingName], [SettingDesc], [SettingType], [DefaultValue])
     VALUES
           (120, 'SiteID', 'SiteID', 1, '1')
GO
INSERT INTO [dbo].[tblSettings]
           ([SettingID], [SettingName], [SettingDesc], [SettingType], [DefaultValue])
     VALUES
           (54, 'ExtRefMode', 'ExtRefMode', 1, '1')
GO
INSERT INTO [dbo].[tblSettings]
           ([SettingID], [SettingName], [SettingDesc], [SettingType], [DefaultValue])
     VALUES
           (55, 'ReadOnlyMachines', 'ReadOnlyMachines', 2, 'False')
GO
INSERT INTO [dbo].[tblSettings]
           ([SettingID], [SettingName], [SettingDesc], [SettingType], [DefaultValue])
     VALUES
           (56, 'ReadOnlyMachinesGroups', 'ReadOnlyMachinesGroups', 2, 'False')
GO
INSERT INTO [dbo].[tblSettings]
           ([SettingID], [SettingName], [SettingDesc], [SettingType], [DefaultValue])
     VALUES
           (57, 'NoDeleteMachines', 'NoDeleteMachines', 2, 'True')
GO
INSERT INTO [dbo].[tblSettings]
           ([SettingID], [SettingName], [SettingDesc], [SettingType], [DefaultValue])
     VALUES
           (58, 'NoDeleteMachineGroups', 'NoDeleteMachineGroups', 2, 'True')
GO
INSERT INTO [dbo].[tblSettings]
           ([SettingID], [SettingName], [SettingDesc], [SettingType], [DefaultValue])
     VALUES
           (59, 'ReadOnlyOperators', 'ReadOnlyOperators', 2, 'True')
GO
