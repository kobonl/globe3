/*    ==Scripting Parameters==

    Source Server Version : SQL Server 2016 (13.0.5081)
    Source Database Engine Edition : Microsoft SQL Server Express Edition
    Source Database Engine Type : Standalone SQL Server

    Target Server Version : SQL Server 2016
    Target Database Engine Edition : Microsoft SQL Server Express Edition
    Target Database Engine Type : Standalone SQL Server
*/

USE [JEGR_DB]
GO

/****** Object:  View [dbo].[v_tblMachines]    Script Date: 19-7-2019 12:11:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create view [dbo].[v_tblOperators] as
SELECT o.[idJensen]
      ,o.[ShortDescription]
	  ,cast(o.[LongDescription] as nvarchar(999)) LongDescription
      ,o.[Retired]
  FROM [JEGR_DB].[dbo].[tblOperators] o 
    
GO
