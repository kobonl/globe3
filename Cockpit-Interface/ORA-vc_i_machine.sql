create or replace view vc_i_machine as
select /*vc_i_machine version 0.1*/
 substr(to_char(m2."glabsRef"), 1, 4) machine_code, pt."PLANTCODE" plant_code,
 substr(m."ShortDescription", 1, 20) short_name, nvl(substr(m."LongDescription", 1, 30), m."ShortDescription") description,
 sysdate creation_date, sysdate mutation_date, 'Cockpit' mutator,
 substr(to_char(mg2."glabsRef"), 1, 6) department_code
from   dbo.v_tblmachines@gtlab m, dbo.v_tblmachinegroups@gtlab mg, dbo.viewplant@gtlab pt, dbo.tblmachinesglabsref@gtlab m2, dbo.tblmachinegroupsglabsref@gtlab mg2
where  mg."idJensen" = m."MachineGroup_idJensen" and m."idJensen" = m2."machine_idJensen" and mg."idJensen" = mg2."machineGroup_idJensen"
and    substr(to_char(m2."glabsRef"), 1, 4) is not null
/

