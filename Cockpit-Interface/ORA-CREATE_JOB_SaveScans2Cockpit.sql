BEGIN
DBMS_SCHEDULER.CREATE_JOB (
   job_name             => 'SAVE_SCANS_2_COCKPIT_JOB',
   job_type             => 'PLSQL_BLOCK',
   job_action           => 'BEGIN PACKAGE_COCKPIT.save_scans_2_cockpit; END;',
   start_date           => sysdate,
   repeat_interval      => 'FREQ=SECONDLY;INTERVAL=5', -- repeat every 5 seconds
   enabled              =>  TRUE,
   comments             => 'Save Scans to Cockpit');
END;
/
