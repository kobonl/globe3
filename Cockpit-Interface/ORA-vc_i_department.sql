create or replace view vc_i_department as
select /*vc_i_department version 0.1*/
 substr(to_char(mg2."glabsRef"), 1, 6) department_code, pt."PLANTCODE" plant_code,
 substr(x."ShortDescription", 1, 20) description,
 (select min(bu.businessunit_code) from businessunit bu where bu.plant_code = pt."PLANTCODE") businessunit_code,
 sysdate creationdate, 'Y' direct_hour_yn, sysdate mutationdate, 'Cockpit' mutator,
 'Y' plan_on_workspot_yn
from   dbo.v_tblmachinegroups@gtlab x, dbo.viewplant@gtlab pt, dbo.tblmachinegroupsglabsref@gtlab mg2
where  x."idJensen" = mg2."machineGroup_idJensen" and
substr(to_char(mg2."glabsRef"), 1, 6) is not null and x."idJensen" in (select y."MachineGroup_idJensen" from dbo.tblmachines@gtlab y)

