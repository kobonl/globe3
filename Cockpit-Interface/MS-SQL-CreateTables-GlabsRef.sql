/****** Create Table [dbo].[tblMachinesGlabsRef]   ******/

USE [JEGR_DB]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tblMachinesGlabsRef](
	[recNum] [int] IDENTITY(1,1) NOT NULL,
	[machine_idJensen] [smallint] NOT NULL,
	[glabsRef] [nvarchar](10) NULL,
 CONSTRAINT [PK_tblMachinesGlabsRef] PRIMARY KEY CLUSTERED 
(
	[recNum] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY],
 CONSTRAINT [UK_machine_idJensen] UNIQUE NONCLUSTERED 
(
	[machine_idJensen] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[tblMachinesGlabsRef]  WITH CHECK ADD  CONSTRAINT [FK_tblMachines_tblMachinesGlabsRef] FOREIGN KEY([machine_idJensen])
REFERENCES [dbo].[tblMachines] ([idJensen])
GO

ALTER TABLE [dbo].[tblMachinesGlabsRef] CHECK CONSTRAINT [FK_tblMachines_tblMachinesGlabsRef]
GO


/****** Create Table [dbo].[tblMachineGroupsGlabsRef] ******/

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tblMachineGroupsGlabsRef](
	[recNum] [int] IDENTITY(1,1) NOT NULL,
	[machineGroup_idJensen] [smallint] NOT NULL,
	[glabsRef] [nvarchar](10) NULL,
 CONSTRAINT [PK_tblMachineGroupsGlabsRef] PRIMARY KEY CLUSTERED 
(
	[recNum] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY],
 CONSTRAINT [UK_machineGroup_idJensen] UNIQUE NONCLUSTERED 
(
	[machineGroup_idJensen] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[tblMachineGroupsGlabsRef]  WITH CHECK ADD  CONSTRAINT [FK_tblMachineGroups_tblMachineGroupsGlabsRef] FOREIGN KEY([machineGroup_idJensen])
REFERENCES [dbo].[tblMachineGroups] ([idJensen])
GO

ALTER TABLE [dbo].[tblMachineGroupsGlabsRef] CHECK CONSTRAINT [FK_tblMachineGroups_tblMachineGroupsGlabsRef]


/****** Create Table [dbo].[tblOperatorsGlabsRef] ******/

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tblOperatorsGlabsRef](
	[recNum] [int] IDENTITY(1,1) NOT NULL,
	[operator_idJensen] [smallint] NOT NULL,
	[glabsRef] [nvarchar](10) NULL,
 CONSTRAINT [PK_tblOperatorsGlabsRef] PRIMARY KEY CLUSTERED 
(
	[recNum] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY],
 CONSTRAINT [UK_operator_idJensen] UNIQUE NONCLUSTERED 
(
	[operator_idJensen] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[tblOperatorsGlabsRef]  WITH CHECK ADD  CONSTRAINT [FK_tblOperators_tblOperatorsGlabsRef] FOREIGN KEY([operator_idJensen])
REFERENCES [dbo].[tblOperators] ([idJensen])
GO

ALTER TABLE [dbo].[tblOperatorsGlabsRef] CHECK CONSTRAINT [FK_tblOperators_tblOperatorsGlabsRef]
GO

/****** Update database version to 2.30 ******/

UPDATE  [dbo].[tblSettings] SET [DefaultValue] = 2.30 where [SettingName] = 'DatabaseVersion'
GO

/****** Insert data with null glabsRef  values ******/
INSERT INTO [dbo].[tblMachinesGlabsRef] ([machine_idJensen])
SELECT [idJensen]
FROM [dbo].[tblMachines];

GO

INSERT INTO [dbo].[tblMachineGroupsGlabsRef] ([machineGroup_idJensen])
SELECT [idJensen]
FROM [dbo].[tblMachineGroups];

GO