USE [JEGR_DB]
GO
/****** Object:  StoredProcedure [dbo].[spPimsScans2Cockpit]    Script Date: 6-4-2017 11:52:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================
-- Author:              Marcel Rand (ABS / Gotli Labs)
-- Create date:         6 April 2017
-- Description:         Scans from Pims to Cockpit interface 
-- =======================================================

CREATE PROCEDURE [dbo].[spPimsScans2Cockpit]
AS
BEGIN
  SET NOCOUNT ON;  

  DECLARE @ReturnCode int
  DECLARE @WorkspotCode nvarchar(6)
  DECLARE @EmployeeID int
  DECLARE @EmployeeNumber int
  DECLARE @DateTimeIn datetime
  DECLARE @DateTimeOut datetime
  DECLARE @LogDate datetime
  DECLARE @lv_PlanInfo nvarchar(1)
  DECLARE @lv_State int
  DECLARE @Job_Code nvarchar(6)
  DECLARE @lv_logdate datetime
  DECLARE @lv_MachineCode nvarchar(6)
  DECLARE @Done int
  DECLARE @DateArg nvarchar(32);
  DECLARE @ErrorMessage nvarchar(256)
  DECLARE @Comp nvarchar(256)
  DECLARE @LogSource nvarchar(256)
  DECLARE @Prio int = 3 

  SET @Comp = '1'
  SET @LogSource = 'spPimsLogOnOffMain'

  SET @ReturnCode = 0;

  DECLARE Scan_cursor CURSOR FOR
  SELECT [EmployeeNumber], [DateTimeIn], [MachineCode], [WorkspotCode], [EmployeeID], [DateTimeOut], [LogDate], [PlanInfo], [State], [Job_Code], [Done]
  FROM JEGR_DB.dbo.tblPimsScans WHERE [Done] = 0;

  OPEN Scan_cursor  
  FETCH NEXT FROM Scan_cursor INTO @EmployeeNumber, @DateTimeIn, @lv_MachineCode, @WorkspotCode,
  @EmployeeID, @DateTimeOut, @LogDate, @lv_PlanInfo, @lv_State, @Job_Code, @Done;

--  IF @@FETCH_STATUS <> 0   
--      PRINT '         <<None>>'       

  WHILE @@FETCH_STATUS = 0  
  BEGIN  
    if @lv_State = 1
      SET @lv_logdate = @DateTimeIn
    else
      SET @lv_logdate = @DateTimeOut

	BEGIN TRY
      EXEC @ReturnCode = [dbo].[spPimsLogOnOffMain]
		  @OperatorGlabsRef = @EmployeeNumber,
		  @TimeStamp = @lv_logdate,
		  @MachineCode = @lv_MachineCode,
		  @SubID = @WorkspotCode,
		  @State = @lv_State,
		  @PlanInfo = @lv_PlanInfo

	END TRY
	BEGIN CATCH
      SET @ErrorMessage = ERROR_MESSAGE();
	  if @ErrorMessage IS NULL
	    SET @ErrorMessage = '600';
      EXEC ('BEGIN writelog(?,?,?,?); END;', @ErrorMessage , @Comp, @LogSource, @Prio) at ABS1;
	  SET @ReturnCode = 600;
	END CATCH
  
    if @ReturnCode = 0 
    begin
	  UPDATE JEGR_DB.dbo.[tblPimsScans]
	  SET [Done] = 1
	  WHERE [EmployeeNumber] = @EmployeeNumber AND [DateTimeIn] = @DateTimeIn;

--      if @State = 1
--        EXEC ('BEGIN update timeregscanning t set t.in_yn = ''Y'' where t.employee_number = ? and t.datetime_in = ?; END;', @EmployeeNumber, @DateTimeIn) at ABS1
--      else 
--        EXEC ('BEGIN update timeregscanning t set t.in_yn = ''Y'', t.out_yn = ''Y'' where t.employee_number = ? and t.datetime_in = ?; END;', @EmployeeNumber, @DateTimeIn) at ABS1
    end
  
    FETCH NEXT FROM Scan_cursor INTO @EmployeeNumber, @DateTimeIn, @lv_MachineCode, @WorkspotCode,
    @EmployeeID, @DateTimeOut, @LogDate, @lv_PlanInfo, @lv_State, @Job_Code, @Done;
  END
  CLOSE Scan_cursor;  
  DEALLOCATE Scan_cursor;  

  return @ReturnCode
END

GO

