USE [JEGR_DB]
GO

/****** Object:  Table [dbo].[tblJGLogDataBackup]    Script Date: 30-8-2017 10:47:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tblJGLogDataBackup](
	[recNum] [int] IDENTITY(1,1) NOT NULL,
	[RemoteID] [int] NOT NULL,
	[CompanyID] [int] NOT NULL,
	[TimeStamp] [datetime] NOT NULL,
	[MachineID] [int] NOT NULL,
	[PositionID] [int] NOT NULL,
	[SubID] [int] NOT NULL,
	[SubIDName] [nvarchar](50) NULL,
	[RegType] [int] NOT NULL,
	[SubRegType] [int] NOT NULL,
	[SubRegTypeID] [int] NOT NULL,
	[State] [int] NOT NULL,
	[MessageA] [nvarchar](max) NULL,
	[MessageB] [nvarchar](max) NULL,
	[BatchID] [int] NOT NULL,
	[SourceID] [int] NOT NULL,
	[ProcessCode] [int] NOT NULL,
	[ProcessName] [nvarchar](50) NULL,
	[CustNo] [int] NOT NULL,
	[SortCategoryID] [int] NULL,
	[ArtNo] [int] NOT NULL,
	[OperatorNo] [int] NOT NULL,
	[Value] [decimal](18, 3) NOT NULL,
	[Unit] [int] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

/****** Object:  Index [PKPrimaryKeyJGLogDataBackup]    Script Date: 30-8-2017 10:48:23 ******/
CREATE UNIQUE NONCLUSTERED INDEX [PKPrimaryKeyJGLogDataBackup] ON [dbo].[tblJGLogDataBackup]
(
	[recNum] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
GO

/****** Object:  Index [IdxTimestampJGLogDataBackup]    Script Date: 30-8-2017 10:47:44 ******/
CREATE NONCLUSTERED INDEX [IdxTimestampJGLogDataBackup] ON [dbo].[tblJGLogDataBackup]
(
	[TimeStamp] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
GO


/* Procedure to get data from other server */

USE [JEGR_DB]
GO

/****** Object:  StoredProcedure [dbo].[spGetRemoteData]    Script Date: 31-8-2017 08:00:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spGetRemoteData]
AS
BEGIN
    /* SET IDENTITY_INSERT [tblJGLogData] ON */
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    DECLARE @ReturnCode int
    DECLARE @ErrorMessage nvarchar(256)
	DECLARE @Error int

	DECLARE @LastTimestamp datetime

	DECLARE @lvrecNum int
	DECLARE @lvRemoteID int
	DECLARE @lvCompanyID int
	DECLARE @lvTimeStamp datetime
	DECLARE @lvMachineID int
	DECLARE @lvPositionID int
	DECLARE @lvSubID int
	DECLARE @lvSubIDName nvarchar(50)
	DECLARE @lvRegType int
	DECLARE @lvSubRegType int
	DECLARE @lvSubRegTypeID int
	DECLARE @lvState int
	DECLARE @lvMessageA nvarchar(max)
	DECLARE @lvMessageB nvarchar(max)
	DECLARE @lvBatchID int
	DECLARE @lvSourceID int
	DECLARE @lvProcessCode int
	DECLARE @lvProcessName nvarchar(50)
	DECLARE @lvCustNo int
	DECLARE @lvSortCategoryID int
	DECLARE @lvArtNo int
	DECLARE @lvOperatorNo int
	DECLARE @lvValue decimal(18, 3)
	DECLARE @lvUnit int

    SET @ReturnCode = 0;
	SET @Error = 0;

	SET @LastTimestamp = NULL;

	-- What was last record stored HERE ?
	SET @LastTimestamp = (SELECT MAX(t.Timestamp) FROM [dbo].[tblJGLogData] t WHERE t.TimeStamp < GETDATE()+1);

	IF (@LastTimestamp IS NULL)
	BEGIN
	  SET @LastTimestamp = GETDATE()
	END
	ELSE
	  IF (@LastTimestamp < GETDATE() - 1)
	  BEGIN
  	    SET @LastTimestamp = GETDATE()
  	  END
    
    DECLARE Data_cursor CURSOR FOR
    SELECT 
          recNum,
          RemoteID,
          CompanyID,
          TimeStamp,
          MachineID,
          PositionID,
          SubID,
          SubIDName,
          RegType,
          SubRegType,
          SubRegTypeID,
          State,
          MessageA,
          MessageB,
          BatchID,
          SourceID,
          ProcessCode,
          ProcessName,
          CustNo,
          SortCategoryID,
          ArtNo,
          OperatorNo,
          Value,
          Unit
	FROM [TESTSQLSERVER].[JEGR_DB].[dbo].[tblJGLogDataBackup]
	WHERE Timestamp >= @LastTimestamp;

    OPEN Data_cursor  
    FETCH NEXT FROM Data_cursor INTO 
          @lvrecNum,
          @lvRemoteID,
          @lvCompanyID,
          @lvTimeStamp,
          @lvMachineID,
          @lvPositionID,
          @lvSubID,
          @lvSubIDName,
          @lvRegType,
          @lvSubRegType,
          @lvSubRegTypeID,
          @lvState,
          @lvMessageA,
          @lvMessageB,
          @lvBatchID,
          @lvSourceID,
          @lvProcessCode,
          @lvProcessName,
          @lvCustNo,
          @lvSortCategoryID,
          @lvArtNo,
          @lvOperatorNo,
          @lvValue,
          @lvUnit;

  WHILE @@FETCH_STATUS = 0  
  BEGIN  
	BEGIN TRY
      EXEC @ReturnCode = [dbo].[spInsertLogData] 
            @RemoteID = @lvRemoteID
           ,@CompanyID = @lvCompanyID
           ,@TimeStamp = @lvTimeStamp
           ,@MachineID = @lvMachineID
           ,@PositionID = @lvPositionID
           ,@SubID = @lvSubId
           ,@SubIDName = @lvSubIDName
           ,@RegType = @lvRegType
           ,@SubRegType = @lvSubRegType
           ,@SubRegTypeID = @lvSubRegTypeID
           ,@State = @lvState
           ,@MessageA = @lvMessageA
           ,@MessageB = @lvMessageB
           ,@BatchID = @lvBatchID
           ,@SourceID = @lvSourceID
           ,@ProcessCode = @lvProcessCode
           ,@ProcessName = @lvProcessName
           ,@CustNo = @lvCustNo
           ,@SortCategoryID = @lvSortCategoryID
           ,@ArtNo = @lvArtNo
           ,@OperatorNo = @lvOperatorNo
           ,@Value = @lvValue
           ,@Unit = @lvUnit
           ,@AllowDuplicateRemoteID  = 0
	END TRY
	BEGIN CATCH
      SET @ErrorMessage = ERROR_MESSAGE();
	  SET @Error = 1;
	  PRINT @ErrorMessage;
	END CATCH
    FETCH NEXT FROM Data_cursor INTO 
          @lvrecNum,
          @lvRemoteID,
          @lvCompanyID,
          @lvTimeStamp,
          @lvMachineID,
          @lvPositionID,
          @lvSubID,
          @lvSubIDName,
          @lvRegType,
          @lvSubRegType,
          @lvSubRegTypeID,
          @lvState,
          @lvMessageA,
          @lvMessageB,
          @lvBatchID,
          @lvSourceID,
          @lvProcessCode,
          @lvProcessName,
          @lvCustNo,
          @lvSortCategoryID,
          @lvArtNo,
          @lvOperatorNo,
          @lvValue,
          @lvUnit;
  END
  CLOSE Data_cursor;  
  DEALLOCATE Data_cursor;  

  return @ReturnCode

END
GO



/* Get data for last 5 minutes */
/*
select * from [dbo].[tblJGLogData] t 
where (t.Timestamp >= DATEADD(minute, -5,  GETDATE())) and (t.Timestamp < GETDATE())
*/
-- Ironers:
/*
select * from [dbo].[tblJGLogData] t 
where t.MachineID in (2124,2125,2126) and
(t.Timestamp >= DATEADD(minute, -5,  GETDATE())) and (t.Timestamp < GETDATE())
*/


/* Get data for last 5 minutes, store it in backup-table */

USE [JEGR_DB]
GO
/****** Object:  StoredProcedure [dbo].[spPimsLogDataBackup]    Script Date: 1-9-2017 09:07:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spPimsLogDataBackup]
AS
BEGIN
    SET IDENTITY_INSERT [tblJGLogDataBackup] ON
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    DECLARE @ErrorMessage nvarchar(256)

	BEGIN TRY
	  insert into [dbo].[tblJGLogDataBackup]
	  (recNum,
       RemoteID,
       CompanyID,
       TimeStamp,
       MachineID,
       PositionID,
       SubID,
       SubIDName,
       RegType,
       SubRegType,
       SubRegTypeID,
       State,
       MessageA,
       MessageB,
       BatchID,
       SourceID,
       ProcessCode,
       ProcessName,
       CustNo,
       SortCategoryID,
       ArtNo,
       OperatorNo,
       Value,
       Unit
      )
      select 
 	    recNum,
        RemoteID,
        CompanyID,
        TimeStamp,
        MachineID,
        PositionID,
        SubID,
        SubIDName,
        RegType,
        SubRegType,
        SubRegTypeID,
        State,
        MessageA,
        MessageB,
        BatchID,
        SourceID,
        ProcessCode,
        ProcessName,
        CustNo,
        SortCategoryID,
        ArtNo,
        OperatorNo,
        Value,
        Unit
	  from [dbo].[tblJGLogData] t WITH (NOLOCK)
      where t.MachineID in (2124,2125,2126) and
      (t.Timestamp >= DATEADD(minute, -5,  GETDATE())) and (t.Timestamp < GETDATE()) and 
	  t.recNum not in (select t2.recNum from [dbo].[tblJGLogDataBackup] t2) 
    END TRY
    BEGIN CATCH
       SET @ErrorMessage = ERROR_MESSAGE();
	   EXEC ('BEGIN writelog(?,?,?,?); END;', @ErrorMessage , '1', 'spPimsLogDataBackup', '3') at ABS1;
	END CATCH
END
GO

