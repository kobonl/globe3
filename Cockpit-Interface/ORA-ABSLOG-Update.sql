drop index XIE3ABSLOG;

alter table ABSLOG modify LOGMESSAGE VARCHAR2(1000);


create index XIE3ABSLOG on ABSLOG (TRUNC(ABSLOG_TIMESTAMP), LOGSOURCE) tablespace INDX;
 
commit;
 